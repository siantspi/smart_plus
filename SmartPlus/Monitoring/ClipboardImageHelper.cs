﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace SmartPlus
{
    class ClipboardImageHelper : IDisposable
    {
        MainForm mainForm;
        public Constant.CopyImageFormat ImageFormat;
        Metafile metafile = null;
        Bitmap bmp = null;

        public ClipboardImageHelper(MainForm MainForm, Constant.CopyImageFormat ImageFormat, Rectangle ImageRect)
        {
            mainForm = MainForm;
            this.ImageFormat = ImageFormat;
            switch (ImageFormat)
            {
                case Constant.CopyImageFormat.BMP:
                    bmp = new Bitmap(ImageRect.Width, ImageRect.Height);
                    break;
                case Constant.CopyImageFormat.EMFplus:
                    metafile = new Metafile(new MemoryStream(), ClipboardHelper.GetDC(IntPtr.Zero), ImageRect, MetafileFrameUnit.Pixel, EmfType.EmfPlusDual);
                    break;
            }
        }

        public Graphics GetGraphics()
        {
            switch (ImageFormat)
            {
                case Constant.CopyImageFormat.BMP:
                    if (bmp != null) return Graphics.FromImage(bmp);
                    break;
                case Constant.CopyImageFormat.EMFplus:
                    if (metafile != null) return Graphics.FromImage(metafile);
                    break;
            }
            return null;
        }

        public bool PutImageOnClipboard()
        {
            switch (ImageFormat)
            {
                case Constant.CopyImageFormat.BMP:
                    if (bmp != null)
                    {
                        Clipboard.Clear();
                        Clipboard.SetImage(bmp);
                        return Clipboard.ContainsImage();
                    }
                    break;

                case Constant.CopyImageFormat.EMFplus:
                    if (metafile != null)
                    {
                        Clipboard.Clear();
                        return ClipboardHelper.PutEnhMetafileOnClipboard(mainForm.Handle, metafile);
                    }
                    break;
            }
            return false;
        }

        #region Члены IDisposable

        public void Dispose()
        {
            if (metafile != null) metafile.Dispose();
            if (bmp != null) bmp.Dispose();
        }

        #endregion
    }
}
