using System;
using System.Collections.Generic;
using System.Text;

namespace RDF
{
    public class RdfAccessViolationException : RdfException
    {
        public RdfAccessViolationException() { }
        public RdfAccessViolationException(string message) : base(message) { }
        public RdfAccessViolationException(string message, Exception inner) : base(message, inner) { }
        protected RdfAccessViolationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
