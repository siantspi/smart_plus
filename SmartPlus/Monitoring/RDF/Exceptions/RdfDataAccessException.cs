using System;
using System.Collections.Generic;
using System.Text;

namespace RDF
{
    public class RdfDataAccessException : RdfException
    {
        public RdfDataAccessException() { }
        public RdfDataAccessException(string message) : base(message) { }
        public RdfDataAccessException(string message, Exception inner) : base(message, inner) { }
        protected RdfDataAccessException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
