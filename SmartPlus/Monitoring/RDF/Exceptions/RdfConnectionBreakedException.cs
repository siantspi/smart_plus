using System;
using System.Collections.Generic;
using System.Text;

namespace RDF.Exceptions
{
    public class RdfConnectionBreakedException : RdfException
    {
        public RdfConnectionBreakedException() { }
        public RdfConnectionBreakedException(string message) : base(message) { }
        public RdfConnectionBreakedException(string message, Exception inner) : base(message, inner) { }
        protected RdfConnectionBreakedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
