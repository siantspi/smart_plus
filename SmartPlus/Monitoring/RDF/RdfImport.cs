using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using RDF.Objects;

namespace RDF
{
    public static class RdfImport
    {
        public enum RdfObjectTypes
        {
            //otAngularity_Test,
            otContours = 1,
            otDHPV = 2,
            otDHV = 3,
            otDL = 4,
            otDPV = 5,
            otDV = 6,
            otDVZ = 7,
            //otKeyRename,
            //otKeyRenameHistory,
            otMaps = 8,
            otMer = 9,
            otMerFond = 10,
            otMerStays = 11,
            otOperating_Practices = 12,
            otProjectWellInfo = 13,
            otRDFDataVersion = 14,
            otRDFString = 15,
            otRDFStrList = 16,
            otRDF_UpdateArchive = 17,
            otRDF_UpdateInfo = 18,
            otRDF_User = 19,
            otSkv_Coord = 20,
            otSkv_GIS = 21,
            otSkv_Log = 22,
            otSkv_Logs_Hdr = 23,
            otSkv_Log_Hdr_o = 24,
            otSkv_LogZ = 25,
            otSkv_Log_o = 26,
            otSkv_Perf = 27,
            otSkv_Zamer = 28,
            otSpr = 29,
            otMerOP = 30,
            otMerProd = 31,
            otMerInj = 32,
            otSkv_Core = 33,
            otUpdateArchive = 34,
            otSkv_Core_Test = 35
        };

        public enum ErrorCodes
        {
            DB_OK = 0,
            DB_FAIL = 1,
            DB_NOUSER = 2,
            DB_WRONG_PASSWORD = 3,
            DB_ACCESS_DENIED = 4,
            DB_CONNECTION_BREAKED = 11
        };
    }
}
