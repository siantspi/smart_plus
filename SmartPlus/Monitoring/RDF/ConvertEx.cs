using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace RDF
{
    public static class ConvertEx
    {
        static byte[] st_buff = new byte[1024];

        public static Int32 GetInt32(Stream ms)
        {
            byte[] b = new byte[4];
            ms.Read(b, 0, 4);
            return BitConverter.ToInt32(b,0);
        }

        public static Int32 GetInt32BigEndian(Stream ms) {
            byte[] b = new byte[4];
            byte temp;
            ms.Read(b, 0, 4);
            temp = b[0];
            b[0] = b[3];
            b[3] = temp;
            temp = b[1];
            b[1] = b[2];
            b[2] = temp;
            return BitConverter.ToInt32(b, 0);
        }

        public static UInt16 GetUInt16(Stream ms)
        {
            byte[] b = new byte[2];
            ms.Read(b, 0, 2);
            return BitConverter.ToUInt16(b, 0);
        }

        public static Int16 GetInt16(Stream ms)
        {
            byte[] b = new byte[2];
            ms.Read(b, 0, 2);
            return BitConverter.ToInt16(b, 0);
        }

        public static Byte GetByte(Stream ms)
        {            
            return (byte)ms.ReadByte();
        }

        public static Double GetDouble(Stream ms)
        {
            byte[] b = new byte[8];
            ms.Read(b, 0, 8);            
            return BitConverter.ToDouble(b, 0);
        }

        public static Single GetSingle(Stream ms)
        {
            byte[] b = new byte[4];
            ms.Read(b, 0, 4);
            return BitConverter.ToSingle(b, 0);
        }

        public static string ReadStrUI16(Stream ms, Encoding encoding)
        {
            ushort num = GetUInt16(ms);
            byte[] b = new byte[num];
            ms.Read(b, 0, num);
            string rs = encoding.GetString(b);            
            return rs;
        }
        public static string ReadStrInt32(Stream ms, Encoding encoding)
        {
            int num = GetInt32(ms);
            if (num > 0 && num < 1000)
            {
                byte[] b = new byte[num];
                ms.Read(b, 0, num);
                return encoding.GetString(b);
            }
            return string.Empty;
        }

        public static string ReadString(Stream ms, Encoding encoding, int count)
        {            
            byte[] b = new byte[count];
            ms.Read(b, 0, count);
            int i=0;
            for (i = 0; i < b.Length; i++)
                if (b[i] == 0)
                    break;
            if (i > b.Length) i = b.Length;
            string rs = encoding.GetString(b, 0, i);
            return rs;
        }

        public static void WriteToMs(Stream ms, byte[] data)
        {
            ms.Write(data, 0, data.Length);
        }
        
        public static void CopyStream(System.IO.Stream src, System.IO.Stream dest)
        {
            byte[] buf = new byte[src.Length - src.Position];
            int len = src.Read(buf, 0, buf.Length);
            dest.Write(buf, 0, len);
            dest.Flush();
        }

        public static void CopyMemoryStream(System.IO.Stream src, System.IO.Stream dest)
        {
            ((MemoryStream)src).WriteTo(dest);
            dest.Flush();
        }

        public static void CopyStream(System.IO.Stream src, System.IO.Stream dest, int count)
        {
            //byte[] buf = new byte[count];
            //int len = src.Read(buf, 0, count);
            //dest.Write(buf, 0, len);
            //dest.Flush();
            int i = 0, len, size = 1024;
            byte[] buff = new byte[size];
            while (i < count)
            {
                if (i + 1024 > count) size = count - i;
                len = src.Read(buff, 0, size);
                dest.Write(buff, 0, len);
                i += len;
            }
            buff = null;
            dest.Flush();
        }
        public static void CopyStream(Stream InStream, Stream OutStream, bool SeekBegin)
        {
            int buffSize = 512 * 1024;
            int count = (int)InStream.Length;
            if (!SeekBegin) count -= (int)InStream.Position;

            if (buffSize > count) buffSize = count;
            byte[] buff = new byte[buffSize];
            int sumCount = 0, currCount;
            if (SeekBegin) InStream.Seek(0, SeekOrigin.Begin);
            do
            {
                currCount = InStream.Read(buff, 0, buffSize);
                OutStream.Write(buff, 0, currCount);
                sumCount += currCount;
            }
            while (sumCount < count);
            OutStream.Flush();
            OutStream.Seek(0, SeekOrigin.Begin);
        }
        public static T UnPackStream<T>(Stream stream, T unpackedBinary) where T : Stream, new()
        {
            T buff = new T();
            zlib.ZOutputStream outZStream = new zlib.ZOutputStream(buff);
            stream.Seek(0, SeekOrigin.Begin);

            CopyStream(stream, outZStream);

            unpackedBinary.Seek(0, SeekOrigin.Begin);

            //T res = new T();

            CopyMemoryStream(buff, unpackedBinary);

            unpackedBinary.Seek(0, SeekOrigin.Begin);
            outZStream.Close();
            //unpackedBinary.Close();

            return unpackedBinary;
        }
        public static T UnPackStream<T>(Stream stream) where T : Stream, new()
        {
            T unpackedBinary = new T();
            zlib.ZOutputStream outZStream = new zlib.ZOutputStream(unpackedBinary);
            stream.Seek(0, SeekOrigin.Begin);

            CopyStream(stream, outZStream);

            unpackedBinary.Seek(0, SeekOrigin.Begin);

            T res = new T();

            CopyMemoryStream(unpackedBinary, res);

            res.Seek(0, SeekOrigin.Begin);
            outZStream.Close();
            unpackedBinary.Close();

            return res;
        }

        public static void UnPackStream(Stream stream, ref MemoryStream unpackedBinary)
        {
            zlib.ZOutputStream outZStream = new zlib.ZOutputStream(unpackedBinary);
            stream.Seek(0, SeekOrigin.Begin);
            CopyMemoryStream(stream, outZStream);
            unpackedBinary.Seek(0, SeekOrigin.Begin);
            outZStream = null;
        }

        //public static MemoryStream UnPackStream(Stream stream) 
        //{
        //    MemoryStream unpackedBinary = new MemoryStream();
        //    zlib.ZOutputStream outZStream = new zlib.ZOutputStream(unpackedBinary);
        //    stream.Seek(0, SeekOrigin.Begin);

        //    CopyStream(stream, outZStream);

        //    unpackedBinary.Seek(0, SeekOrigin.Begin);

        //    MemoryStream res = new MemoryStream();

        //    CopyMemoryStream(unpackedBinary, res);

        //    res.Seek(0, SeekOrigin.Begin);
        //    outZStream.Close();
        //    unpackedBinary.Close();

        //    return res; 
        //}

        //public static MemoryStream UnPackStream(MemoryStream stream, int unpack_len_block, Assist.MemoryCache MemCache) 
        //{
        //    MemoryStream res = null;
        //    if (MemCache != null)
        //    {
        //        MemoryStream unpackedBinary = MemCache.GetEmptyCache(unpack_len_block);
        //        zlib.ZOutputStream outZStream = new zlib.ZOutputStream(unpackedBinary);
        //        stream.Seek(0, SeekOrigin.Begin);

        //        CopyStream(stream, outZStream, MemCache);

        //        unpackedBinary.Seek(0, SeekOrigin.Begin);

        //        //res = new MemoryStream();
        //        res = MemCache.GetEmptyCache((int)unpackedBinary.Length);
        //        unpackedBinary.Seek(0, SeekOrigin.Begin);

        //        CopyMemoryStream(unpackedBinary, res);

        //        res.Seek(0, SeekOrigin.Begin);

        //        //outZStream.Close();
        //        outZStream = null;
        //        MemCache.ReleaseCache(unpackedBinary);
        //        //unpackedBinary.Close();
        //    }
        //    return res;
        //}

        public static MemoryStream PackStream(Stream stream) 
        {

            MemoryStream packedBinary = new MemoryStream();
            zlib.ZOutputStream outZStream = new zlib.ZOutputStream(packedBinary, zlib.zlibConst.Z_DEFAULT_COMPRESSION);
            stream.Seek(0, SeekOrigin.Begin);

            CopyStream(stream, outZStream);

            outZStream.Flush();
            outZStream.finish();

            packedBinary.Seek(0, System.IO.SeekOrigin.Begin);

            MemoryStream pack = new MemoryStream();
            CopyStream(packedBinary, pack);

            outZStream.Close();
            pack.Seek(0, SeekOrigin.Begin);
            return pack;
        }
        public static void PackStream(Stream stream, ref MemoryStream packedBinary)
        {

            MemoryStream pack = new MemoryStream();
            zlib.ZOutputStream outZStream = new zlib.ZOutputStream(pack, zlib.zlibConst.Z_DEFAULT_COMPRESSION);
            stream.Seek(0, SeekOrigin.Begin);

            CopyStream(stream, outZStream);

            outZStream.Flush();
            outZStream.finish();

            pack.Seek(0, SeekOrigin.Begin);

            packedBinary.Seek(0, SeekOrigin.Begin);
            CopyStream(pack, packedBinary);

            outZStream.Close();
            packedBinary.Seek(0, SeekOrigin.Begin);
        }
        public static MemoryStream PackStream(Stream stream, int CompressionLevel)
        {

            MemoryStream packedBinary = new MemoryStream();
            zlib.ZOutputStream outZStream = new zlib.ZOutputStream(packedBinary, CompressionLevel);
            stream.Seek(0, SeekOrigin.Begin);

            CopyStream(stream, outZStream);

            outZStream.Flush();
            outZStream.finish();

            packedBinary.Seek(0, System.IO.SeekOrigin.Begin);

            MemoryStream pack = new MemoryStream();
            CopyStream(packedBinary, pack);

            outZStream.Close();
            pack.Seek(0, SeekOrigin.Begin);
            return pack;

            //throw new NotSupportedException();
        }

        public static T PackStream<T>(Stream stream) where T : Stream, new()
        {

            T packedBinary = new T();
            zlib.ZOutputStream outZStream = new zlib.ZOutputStream(packedBinary, zlib.zlibConst.Z_DEFAULT_COMPRESSION);
            stream.Seek(0, SeekOrigin.Begin);

            CopyStream(stream, outZStream);

            outZStream.Flush();
            outZStream.finish();

            packedBinary.Seek(0, System.IO.SeekOrigin.Begin);

            T pack = new T();
            CopyStream(packedBinary, pack);

            outZStream.Close();
            pack.Seek(0, SeekOrigin.Begin);
            return pack;

            //throw new NotSupportedException();
        }

        //public static MemoryStream PackStream(MemoryStream stream, Assist.MemoryCache MemCache)
        //{

        //    MemoryStream packedBinary = MemCache.GetEmptyCache(stream.Capacity);
        //    zlib.ZOutputStream outZStream = new zlib.ZOutputStream(packedBinary, zlib.zlibConst.Z_DEFAULT_COMPRESSION);
        //    stream.Seek(0, SeekOrigin.Begin);

        //    CopyStream(stream, outZStream, MemCache);

        //    outZStream.Flush();
        //    outZStream.finish();

        //    packedBinary.Seek(0, System.IO.SeekOrigin.Begin);

        //    MemoryStream pack = MemCache.GetEmptyCache((int)packedBinary.Length);
        //    packedBinary.Seek(0, System.IO.SeekOrigin.Begin);

        //    CopyMemoryStream(packedBinary, pack);

        //    //outZStream.Close();
        //    pack.Seek(0, SeekOrigin.Begin);
        //    MemCache.ReleaseCache(packedBinary);
        //    return pack;
        //}

        public static T GetStruct<T>(Stream ms)
        {
            T item = default(T);

            Int32 sizeItem = System.Runtime.InteropServices.Marshal.SizeOf(item);
            IntPtr lpBuf = System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeItem);
            unsafe
            {
                System.IO.UnmanagedMemoryStream UnmanMS = new System.IO.UnmanagedMemoryStream((byte*)lpBuf, sizeItem, sizeItem, System.IO.FileAccess.Write);

                try
                {

                    ConvertEx.CopyStream(ms, UnmanMS, sizeItem);
                    item = (T)System.Runtime.InteropServices.Marshal.PtrToStructure(lpBuf, typeof(T));
                }
                finally
                {
                    UnmanMS.Close();
                    System.Runtime.InteropServices.Marshal.FreeHGlobal(lpBuf);
                }
            }


            return item;

        }

        //public static RDF.Objects.Maps.MinMax GetStructMinMax(Stream ms)
        //{
        //    RDF.Objects.Maps.MinMax item = default(RDF.Objects.Maps.MinMax);

        //    Int32 sizeItem = System.Runtime.InteropServices.Marshal.SizeOf(item);
        //    IntPtr lpBuf = System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeItem);
        //    unsafe
        //    {
        //        System.IO.UnmanagedMemoryStream UnmanMS = new System.IO.UnmanagedMemoryStream((byte*)lpBuf, sizeItem, sizeItem, System.IO.FileAccess.Write);

        //        try
        //        {

        //            ConvertEx.CopyStream(ms, UnmanMS, sizeItem);
        //            item = (RDF.Objects.Maps.MinMax)System.Runtime.InteropServices.Marshal.PtrToStructure(lpBuf, typeof(RDF.Objects.Maps.MinMax));
        //        }
        //        finally
        //        {
        //            UnmanMS.Close();
        //            System.Runtime.InteropServices.Marshal.FreeHGlobal(lpBuf);
        //        }
        //    }
        //    return item;
        //}

        public static bool IsRdfDataSetPacked(ref MemoryStream ms) {
            ms.Seek(0, SeekOrigin.Begin);
            int nPacked = ConvertEx.GetInt32(ms);
            if (nPacked != -1109386858) return true;
            else return false;
        }

        public static bool UnpackRDFDataSet(ref MemoryStream ms) {
            int nPacked, len;
            byte[] buf = new byte[ms.Length - 19];
            MemoryStream tempBuf = new MemoryStream();
            ms.Seek(4, SeekOrigin.Begin);
            len = ms.Read(buf, 0, buf.Length);
            tempBuf.Write(buf, 0, len);
            tempBuf.Flush();
            tempBuf.Seek(0, SeekOrigin.Begin);
            ms.Close();
            ms = ConvertEx.UnPackStream<MemoryStream>(tempBuf);
            //ms = ConvertEx.UnPackStream(tempBuf);
            tempBuf.Close();
            ms.Seek(0, SeekOrigin.Begin);
            nPacked = ConvertEx.GetInt32(ms);
            if (nPacked == -1109386858) return true;
            else return false;
        }
    }
}
