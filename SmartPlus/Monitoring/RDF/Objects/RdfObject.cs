using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace RDF.Objects
{
    
    /// <summary>
    /// Summary description for RDFObject.
    /// </summary>
    //
    public class RdfObject<T> : IRdfObject
    {
        public delegate void UnpackItemDelegate<U>(ref T item, U packedItem);

        /// <summary>
        /// Array of  items
        /// </summary>
        protected T[] _items = null;

        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>The items.</value>
        public T[] Items
        {
            get { return _items; }
        }

        /// <summary>
        /// Gets the count of items.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get 
            {
                if (_items == null) return 0;
                // throw new InvalidOperationException("RDFObject not readed");

                return _items.Length; 
            }
        }

        /// <summary>
        /// Gets the RDF object type Id
        /// </summary>
        protected virtual int ObjectTypeId
        {
            get 
            {
                throw new System.NotImplementedException();
                //return 0; 
            }
        }

        /// <summary>
        /// Reads objects from specified RDF database.
        /// </summary>
        /// <param name="connector">RDF connector.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public virtual bool Read(RdfConnector connector, string key)
        {
            
            return Read(connector, key, false);
        }

        /// <summary>
        /// Reads objects from specified RDF database.
        /// </summary>
        /// <param name="connector">RDF connector.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public virtual bool Read(RdfConnector connector, string key, bool unpack)
        {            
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            ms = connector.ReadBinary(key, false);
            if (ms == null)
                return false;            
            
            //////////////////////////////////////////////
            /// retrieve items from imported object
            ms.Seek(0, System.IO.SeekOrigin.Begin);
            //_items = RetrieveItems(ms, unpack);

            //////////////////////////////////////////////          
            return true;
        }

        /// <summary>
        /// Reads objects from specified RDF database.
        /// </summary>
        /// <param name="connector">RDF connector.</param>
        /// <param name="key">The key.</param>
        /// <param name="objectTypeId">RDF object type ID.</param>
        /// <returns></returns>
        public virtual bool Read(RdfConnector connector, string key, int objectTypeId)
        {
           return Read(connector, key);           
        }
    }
}
