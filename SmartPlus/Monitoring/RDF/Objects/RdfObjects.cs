using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using RDF;
using SmartPlus.Update;

namespace RDF.Objects
{
    #region SkvCoord objects
    public struct SkvCoordItem
    {
        /// <summary>
        /// �����
        /// </summary>
        public int PlastId;
        /// <summary>
        /// X ���������� ����������� �������� ����� �����
        /// </summary>
        public double X;
        /// <summary>
        /// Y ���������� ����������� �������� ����� �����
        /// </summary>
        public double Y;
    }
    public sealed class SkvCoord : RdfObject<SkvCoordItem>
    {
        public static int Version = 0;
        protected override int ObjectTypeId
        {
            get { return (int)RdfImport.RdfObjectTypes.otSkv_Coord; }
        }
        public struct SkvCoordAlt
        {
            /// <summary>
            /// ���������� ����� �������� �� ��� X
            /// </summary>
            public double X;
            /// <summary>
            /// ���������� ����� �������� �� ��� Y
            /// </summary>
            public double Y;
            /// <summary>
            /// ���������
            /// </summary>
            public float Alt;
        }
        public SkvCoordAlt Altitude;

        /// <param name="connector">RDF connector.</param>
        /// <param name="key">The key.</param>
        public override bool Read(RdfConnector connector, string key)
        {          
            MemoryStream ms;
            ms = connector.ReadBinary(key, false);
            if (ms == null) return false;
            
            ms.Seek(0, SeekOrigin.Begin);
            BinaryReader br = new BinaryReader(ms);

            Int32 itemCount = br.ReadInt32();
            Int32 structSize = br.ReadByte();

            if (itemCount <= 0) return false;

            _items = new SkvCoordItem[itemCount];
            try
            {
                Altitude = new SkvCoordAlt();
                for (int i = 0; i < itemCount; i++)
                {
                    _items[i] = RetrieveItem(br);
                }
                Altitude.X = br.ReadInt32();
                Altitude.Y = br.ReadInt32();
                Altitude.Alt = ConvertEx.GetSingle(ms);
            }
            finally
            {
                br.Close();
            }
            return true;
        }
        public void SetItems(SkvCoordItem[] skvItems) 
        {
            _items = skvItems;
        }
        protected SkvCoordItem RetrieveItem(BinaryReader br)
        {
            SkvCoordItem item;
            item.PlastId = br.ReadUInt16();
            item.X = br.ReadInt32();
            item.Y = br.ReadInt32();
            return item;
        }
        public int GetIndexByPlastCode(int PlastCode)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Items[i].PlastId == PlastCode)
                {
                    return i;
                }
            }
            return -1;
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(Altitude.Alt);
            bw.Write(Altitude.X);
            bw.Write(Altitude.Y);
            bw.Write(Count);
            for (int i = 0; i < Count; i++)
            {
                bw.Write(Items[i].PlastId);
                bw.Write(Items[i].X);
                bw.Write(Items[i].Y);
            }
        }
        public void Read(BinaryReader br)
        {
            Altitude.Alt = br.ReadSingle();
            Altitude.X = br.ReadDouble();
            Altitude.Y = br.ReadDouble();
            int count = br.ReadInt32();
            if (count > 0)
            {
                _items = new SkvCoordItem[count];
                for (int i = 0; i < count; i++)
                {
                    
                    _items[i].PlastId = br.ReadInt32();
                    _items[i].X = br.ReadDouble();
                    _items[i].Y = br.ReadDouble();
                }
            }
        }
    }

    #endregion

    #region SkvGis objects
    
    public struct SkvGisItem
    {
        /// <summary>
        /// �����
        /// </summary>
        public UInt16 PlastId;
        /// <summary>
        /// �����+��������
        /// </summary>
        public UInt16 PlastZoneId;
        /// <summary>
        /// ������ ����������
        /// </summary>
        public int ZIndex;
        /// <summary>
        /// ������� ������ ������
        /// </summary>
        public float H;
        /// <summary>
        /// �������
        /// </summary>
        public float L;
        /// <summary>
        /// ���. �������, �����
        /// </summary>
        public float Habs;
        /// <summary>
        /// ���. �������
        /// </summary>
        public float Labs;
        /// <summary>
        /// ���������
        /// </summary>
        public UInt16 LitId;
        /// <summary>
        /// ���������/�����������
        /// </summary>
        public bool Collector;
        /// <summary>
        /// �������� ��������� ���������
        /// </summary>
        public byte Sat0;
        /// <summary>
        /// �������� ��������� �������
        /// </summary>
        public byte Sat;
        /// <summary>
        /// ��, ��*�
        /// </summary>
        public float Ro;
        /// <summary>
        /// �����_��, ���� ��.
        /// </summary>
        public float APS;
        /// <summary>
        /// �������. �����-� ��
        /// </summary>
        public float aGK;
        /// <summary>
        /// �������. �����-� ���
        /// </summary>
        public float aNKT;
        /// <summary>
        /// ����. ����������, �� (���� �����)
        /// </summary>
        public float Kpor;
        /// <summary>
        /// ����. �������������, %
        /// </summary>
        public float Kpr;
        /// <summary>
        /// ����. �����������������, %
        /// </summary>
        public float Kn;
        /// <summary>
        /// ����. ����������������, %
        /// </summary>
        public float Kgas;
        /// <summary>
        /// ����. �����������, %
        /// </summary>
        public float Kgl;
    }

    public sealed class SkvGIS : RdfObject<SkvGisItem>
    {
        public static int Version = 0;
        protected override int ObjectTypeId
        {
            get { return (int)RdfImport.RdfObjectTypes.otSkv_GIS; }
        }

        public override bool Read(RdfConnector connector, string key)
        {
            MemoryStream ms = new MemoryStream();
            ms = connector.ReadBinary(key, false);

            if (ms == null) return false;

            ms.Seek(0, SeekOrigin.Begin);
            BinaryReader br = new BinaryReader(ms);

            Int32 itemCount = br.ReadInt32();
            Int32 structSize = br.ReadByte();
            int i;
            if (itemCount < 0) return false;

            _items = new SkvGisItem[itemCount];

            try
            {
                for (i = 0; i < itemCount; i++)
                {
                    _items[i] = RetrieveItem(br);
                }
            }
            finally
            {
                br.Close();
                ms.Dispose();
                int k = -1;
                for (i = 0; i < itemCount; i++)
                {
                    if (_items[i].H + _items[i].L + _items[i].Habs + _items[i].Labs <= 0)
                    {
                        k = i;
                    }
                    else break;
                }
                if (k != -1)
                {
                    SkvGisItem[] newItems = new SkvGisItem[itemCount - k - 1];
                    for (i = k + 1; i < itemCount; i++)
                    {
                        newItems[i - k - 1] = _items[i];
                    }
                    _items = newItems;
                }
            }
            return true;
        }

        public void SetItems(SkvGisItem[] items) { _items = items; }

        public SkvGisItem RetrieveItem(SkvGisItem item)
        {
            SkvGisItem res;
            res.PlastId = item.PlastId;
            res.PlastZoneId = item.PlastZoneId;
            res.ZIndex = item.ZIndex;
            res.H = item.H;
            res.L = item.L;
            res.Habs = item.Habs;
            res.Labs = item.Labs;
            res.LitId = item.LitId;
            res.Collector = item.Collector;
            res.Sat0 = item.Sat0;
            res.Sat = item.Sat;
            res.Ro = item.Ro;
            res.APS = item.APS;
            res.aGK = item.aGK;
            res.aNKT = item.aNKT;
            res.Kpor = item.Kpor;
            res.Kpr = item.Kpr;
            res.Kn = item.Kn;
            res.Kgas = item.Kgas;
            res.Kgl = item.Kgl;
            return item;
        }
        public SkvGisItem RetrieveItem(BinaryReader br)
        {
            SkvGisItem item;
            item.PlastId = br.ReadUInt16();
            item.PlastZoneId = br.ReadUInt16();
            item.ZIndex = br.ReadInt32();
            item.H = br.ReadSingle();
            item.L = br.ReadSingle();
            item.Habs = br.ReadSingle();
            item.Labs = br.ReadSingle();
            item.LitId = br.ReadUInt16();
            item.Collector = br.ReadBoolean();
            item.Sat0 = br.ReadByte();
            item.Sat = br.ReadByte();
            item.Ro = br.ReadSingle();
            item.APS = br.ReadSingle();
            item.aGK = br.ReadSingle();
            item.aNKT = br.ReadSingle();
            item.Kpor = br.ReadSingle();
            item.Kpr = br.ReadSingle();
            item.Kn = br.ReadSingle();
            item.Kgas = br.ReadSingle();
            item.Kgl = br.ReadSingle();
            return item;
        }
        public bool RetainItem(BinaryWriter bw, int index)
        {
            if ((index > -1) && (index < Items.Length))
            {
                bw.Write(Items[index].PlastId);
                bw.Write(Items[index].PlastZoneId);
                bw.Write(Items[index].ZIndex);
                bw.Write(Items[index].H);
                bw.Write(Items[index].L);
                bw.Write(Items[index].Habs);
                bw.Write(Items[index].Labs);
                bw.Write(Items[index].LitId);
                bw.Write(Items[index].Collector);
                bw.Write(Items[index].Sat0);
                bw.Write(Items[index].Sat);
                bw.Write(Items[index].Ro);
                bw.Write(Items[index].APS);
                bw.Write(Items[index].aGK);
                bw.Write(Items[index].aNKT);
                bw.Write(Items[index].Kpor);
                bw.Write(Items[index].Kpr);
                bw.Write(Items[index].Kn);
                bw.Write(Items[index].Kgas);
                bw.Write(Items[index].Kgl);
                return true;
            }
            return false;
        }

        public float GetAbsDepthByPlastCode(int PlastCode, bool Top)
        {
            int ind1, ind2, k;
            ind1 = (Top ? 0 : Items.Length - 1);
            ind2 = (Top ? Items.Length : -1);
            k = (Top ? 1 : -1);
            for (int i = ind1; i != ind2; i += k)
            {
                if (Items[i].PlastId == PlastCode)
                {
                    if (Top && Items[i].Habs > 0)
                    {
                        return Items[i].Habs;
                    }
                    else if (!Top && (Items[i].Habs + Items[i].Labs > 0))
                    {
                        return Items[i].Habs + Items[i].Labs;
                    }
                }
            }
            return -1;
        }

        public List<SkvGisItem> GetCrossIntervals(List<SkvPerfItem> perfItems)
        {
            List<SkvGisItem> gisItems = new List<SkvGisItem>();
            if (perfItems.Count > 0)
            {
                for (int i = 0; i < Items.Length; i++)
                {
                    for (int j = 0; j < perfItems.Count; j++)
                    {
                        if (!(perfItems[j].Bottom < Items[i].H || perfItems[j].Top > Items[i].H + Items[i].L))
                        {
                            gisItems.Add(Items[i]);
                            break;
                        }
                    }
                }
            }
            return gisItems;
        }
    }
    
    #endregion

    #region SkvCore objects 

    public struct SkvCoreItem
    {
        /// <summary>
        /// �������� ������, ������, �
        /// </summary>
        public float Top;
        /// <summary>
        /// �������� ������, �����, �
        /// </summary>
        public float Bottom;
        /// <summary>
        /// ����� �����, �
        /// </summary>
        public float CoreRecovery;
        /// <summary>
        /// ���������� ��������� �����, ���.�
        /// </summary>
        public float CoreStored;
        /// <summary>
        /// ���������� ������ ��������, ��
        /// </summary>
        public byte StoredBoxCount;
        /// <summary>
        /// ��������� �����
        /// </summary>
        public byte CoreState;
        /// <summary>
        /// ��������� ���������� �����
        /// </summary>
        public byte CoreMarkState;
    }

    public sealed class SkvCore : RdfObject<SkvCoreItem>
    {
        public static int Version = 0;
        protected override int ObjectTypeId
        {
            get { return (int)RdfImport.RdfObjectTypes.otSkv_Core; }
        }

        public override bool Read(RdfConnector connector, string key)
        {
            return false;
        }

        public void SetItems(SkvCoreItem[] items) { _items = items; }

        public SkvCoreItem RetrieveItem(SkvCoreItem item)
        {
            SkvCoreItem res;
            res.Top = item.Top;
            res.Bottom = item.Bottom;
            res.CoreRecovery = item.CoreRecovery;
            res.CoreStored = item.CoreStored;
            res.StoredBoxCount = item.StoredBoxCount;
            res.CoreState = item.CoreState;
            res.CoreMarkState = item.CoreMarkState;
            return item;
        }
        public SkvCoreItem RetrieveItem(BinaryReader br)
        {
            SkvCoreItem item;
            item.Top = br.ReadSingle();
            item.Bottom = br.ReadSingle();
            item.CoreRecovery = br.ReadSingle();
            item.CoreStored = br.ReadSingle();
            item.StoredBoxCount = br.ReadByte();
            item.CoreState = br.ReadByte();
            item.CoreMarkState = br.ReadByte();
            return item;
        }
        public bool RetainItem(BinaryWriter bw, int index)
        {
            if ((index > -1) && (index < Items.Length))
            {
                bw.Write(Items[index].Top);
                bw.Write(Items[index].Bottom);
                bw.Write(Items[index].CoreRecovery);
                bw.Write(Items[index].CoreStored);
                bw.Write(Items[index].StoredBoxCount);
                bw.Write(Items[index].CoreState);
                bw.Write(Items[index].CoreMarkState);
                return true;
            }
            return false;
        }
    }

    #endregion

    #region SkvCoreTest objects

    public struct SkvCoreTestItem
    {
        /// <summary>
        /// ����� �������
        /// </summary>
        public string Name;
        /// <summary>
        /// ���� ����������� �����
        /// </summary>
        public DateTime Date;
        /// <summary>
        /// �������� ������, ������, �
        /// </summary>
        public float Top;
        /// <summary>
        /// �������� ������, �����, �
        /// </summary>
        public float Bottom;
        /// <summary>
        /// ����� �����, �
        /// </summary>
        public float CoreRecovery;
        /// <summary>
        /// ���������� �� �����, �
        /// </summary>
        public float DistanceTop;
        /// <summary>
        /// ���������
        /// </summary>
        public string Litology;
        /// <summary>
        /// �������� ���������
        /// </summary>
        public byte SatId;
        /// <summary>
        /// ���������� �������� ������������������, %
        /// </summary>
        public float KporLiquid;
        /// <summary>
        /// ���������� �������� �� �����, %
        /// </summary>
        public float KporHelium;
        /// <summary>
        /// ������������� �� ������� ��, ��
        /// </summary>
        public float KprAirPP;
        /// <summary>
        /// ������������� �� ������� ��, ��
        /// </summary>
        public float KprAirPR;
        /// <summary>
        /// ������������� �� ����� ��, ��
        /// </summary>
        public float KprHeliumPP;
        /// <summary>
        /// ������������� �� ����� ��, ��
        /// </summary>
        public float KprHeliumPR;
        /// <summary>
        /// ��������, psi
        /// </summary>
        public float Pressure;
        /// <summary>
        /// ������������� �������, %
        /// </summary>
        public float CarbonateTiff;
        /// <summary>
        /// ������������� �������, %
        /// </summary>
        public float CarbonateDolomite;
        /// <summary>
        /// ������������� ������������� �������, %
        /// </summary>
        public float CarbonateInsoluble;
        /// <summary>
        /// ��� �� ����������, ��
        /// </summary>
        public float AdaBeforeExtract;
        /// <summary>
        /// ��� ����� ����������, ��
        /// </summary>
        public float AdaAfterExtract;
        /// <summary>
        /// �������������
        /// </summary>
        public float Wettability;
        /// <summary>
        /// �������� ����������
        /// </summary>
        public float Porosity;
        /// <summary>
        /// ���������� ����������������, %
        /// </summary>
        public float SatWatFinal;
        /// <summary>
        /// ���������������� ���������
        /// </summary>
        public float DensityMineral;
        /// <summary>
        /// �������� ���������
        /// </summary>
        public float DensityVolume;
        /// <summary>
        /// ������������������ ������, ����� 500 ���, %
        /// </summary>
        public float PartSize500;
        /// <summary>
        /// ������������������ ������, ����� 250 ���, %
        /// </summary>
        public float PartSize250;
        /// <summary>
        /// ������������������ ������, ����� 100 ���, %
        /// </summary>
        public float PartSize100;
        /// <summary>
        /// ������������������ ������, ����� 50 ���, %
        /// </summary>
        public float PartSize50;
        /// <summary>
        /// ������������������ ������, ����� 10 ���, %
        /// </summary>
        public float PartSize10;
        /// <summary>
        /// ������������������ ������, ����� 10 ���, %
        /// </summary>
        public float PartSize0;
        /// <summary>
        /// ��������������� K, ��/��
        /// </summary>
        public float RadioActivityK;
        /// <summary>
        /// ��������������� K, %
        /// </summary>
        public float RadioActivityKPercent;
        /// <summary>
        /// ��������������� Th, ��/��
        /// </summary>
        public float RadioActivityTh;
        /// <summary>
        /// ��������������� Th, %
        /// </summary>
        public float RadioActivityThPercent;
        /// <summary>
        /// ��������������� Ra, ��/��
        /// </summary>
        public float RadioActivityRa;
        /// <summary>
        /// ��������������� Ra, %
        /// </summary>
        public float RadioActivityRaPercent;
        /// <summary>
        /// ��������������� A, ��/��
        /// </summary>
        public float RadioActivityA;
        /// <summary>
        /// �����������
        /// </summary>
        public string Comment;
    }

    public sealed class SkvCoreTest : RdfObject<SkvCoreTestItem>
    {
        public static int Version = 0;
        protected override int ObjectTypeId
        {
            get { return (int)RdfImport.RdfObjectTypes.otSkv_Core_Test; }
        }

        public override bool Read(RdfConnector connector, string key)
        {
            //MemoryStream ms = new MemoryStream();
            //ms = connector.ReadBinary(key, false);

            //if (ms == null) return false;

            //ms.Seek(0, SeekOrigin.Begin);
            //BinaryReader br = new BinaryReader(ms);

            //Int32 itemCount = br.ReadInt32();
            //Int32 structSize = br.ReadByte();
            //int i;
            //if (itemCount < 0) return false;

            //_items = new SkvGisItem[itemCount];

            //try
            //{
            //    for (i = 0; i < itemCount; i++)
            //    {
            //        _items[i] = RetrieveItem(br);
            //    }
            //}
            //finally
            //{
            //    br.Close();
            //    ms.Dispose();
            //    int k = -1;
            //    for (i = 0; i < itemCount; i++)
            //    {
            //        if (_items[i].H + _items[i].L + _items[i].Habs + _items[i].Labs <= 0)
            //        {
            //            k = i;
            //        }
            //        else break;
            //    }
            //    if (k != -1)
            //    {
            //        SkvGisItem[] newItems = new SkvGisItem[itemCount - k - 1];
            //        for (i = k + 1; i < itemCount; i++)
            //        {
            //            newItems[i - k - 1] = _items[i];
            //        }
            //        _items = newItems;
            //    }
            //}
            return false;
        }

        public void SetItems(SkvCoreTestItem[] items) { _items = items; }

        public SkvCoreTestItem RetrieveItem(SkvCoreTestItem item)
        {
            SkvCoreTestItem res;
            res.Date = item.Date;
            res.Name = item.Name;
            res.Top = item.Top;
            res.Bottom = item.Bottom;
            res.CoreRecovery = item.CoreRecovery;
            res.DistanceTop = item.DistanceTop;
            res.Litology = item.Litology;
            res.SatId = item.SatId;
            res.KporLiquid = item.KporLiquid;
            res.KporHelium = item.KporHelium;
            res.KprAirPP = item.KprAirPP;
            res.KprAirPR = item.KprAirPR;
            res.KprHeliumPP = item.KprHeliumPP;
            res.KprHeliumPR = item.KprHeliumPR;
            res.Pressure = item.Pressure;
            res.CarbonateTiff = item.CarbonateTiff;
            res.CarbonateDolomite = item.CarbonateDolomite;
            res.CarbonateInsoluble = item.CarbonateInsoluble;
            res.AdaBeforeExtract = item.AdaBeforeExtract;
            res.AdaAfterExtract = item.AdaAfterExtract;
            res.Wettability = item.Wettability;
            res.Porosity = item.Porosity;
            res.SatWatFinal = item.SatWatFinal;
            res.DensityMineral = item.DensityMineral;
            res.DensityVolume = item.DensityVolume;
            res.PartSize500 = item.PartSize500;
            res.PartSize250 = item.PartSize250;
            res.PartSize100 = item.PartSize100;
            res.PartSize50 = item.PartSize50;
            res.PartSize10 = item.PartSize10;
            res.PartSize0 = item.PartSize0;
            res.RadioActivityK = item.RadioActivityK;
            res.RadioActivityKPercent = item.RadioActivityKPercent;
            res.RadioActivityTh = item.RadioActivityTh;
            res.RadioActivityThPercent = item.RadioActivityThPercent;
            res.RadioActivityRa = item.RadioActivityRa;
            res.RadioActivityRaPercent = item.RadioActivityRaPercent;
            res.RadioActivityA = item.RadioActivityA;
            res.Comment = item.Comment;
            return item;
        }
        public SkvCoreTestItem RetrieveItem(BinaryReader br)
        {
            SkvCoreTestItem item;
            item.Date = DateTime.FromOADate(br.ReadDouble());
            item.Name = br.ReadString();
            item.Top = br.ReadSingle();
            item.Bottom = br.ReadSingle();
            item.CoreRecovery = br.ReadSingle();
            item.DistanceTop = br.ReadSingle();
            item.Litology = br.ReadString();
            item.SatId = br.ReadByte();
            item.KporLiquid = br.ReadSingle();
            item.KporHelium = br.ReadSingle();
            item.KprAirPP = br.ReadSingle();
            item.KprAirPR = br.ReadSingle();
            item.KprHeliumPP = br.ReadSingle();
            item.KprHeliumPR = br.ReadSingle();
            item.Pressure = br.ReadSingle();
            item.CarbonateTiff = br.ReadSingle();
            item.CarbonateDolomite = br.ReadSingle();
            item.CarbonateInsoluble = br.ReadSingle();
            item.AdaBeforeExtract = br.ReadSingle();
            item.AdaAfterExtract = br.ReadSingle();
            item.Wettability = br.ReadSingle();
            item.Porosity = br.ReadSingle();
            item.SatWatFinal = br.ReadSingle();
            item.DensityMineral = br.ReadSingle();
            item.DensityVolume = br.ReadSingle();
            item.PartSize500 = br.ReadSingle();
            item.PartSize250 = br.ReadSingle();
            item.PartSize100 = br.ReadSingle();
            item.PartSize50 = br.ReadSingle();
            item.PartSize10 = br.ReadSingle();
            item.PartSize0 = br.ReadSingle();
            item.RadioActivityK = br.ReadSingle();
            item.RadioActivityKPercent = br.ReadSingle();
            item.RadioActivityTh = br.ReadSingle();
            item.RadioActivityThPercent = br.ReadSingle();
            item.RadioActivityRa = br.ReadSingle();
            item.RadioActivityRaPercent = br.ReadSingle();
            item.RadioActivityA = br.ReadSingle();
            item.Comment = br.ReadString();
            return item;
        }
        public bool RetainItem(BinaryWriter bw, int index)
        {
            if ((index > -1) && (index < Items.Length))
            {
                bw.Write(Items[index].Date.ToOADate());
                bw.Write(Items[index].Name);
                bw.Write(Items[index].Top);
                bw.Write(Items[index].Bottom);
                bw.Write(Items[index].CoreRecovery);
                bw.Write(Items[index].DistanceTop);
                bw.Write(Items[index].Litology);
                bw.Write(Items[index].SatId);
                bw.Write(Items[index].KporLiquid);
                bw.Write(Items[index].KporHelium);
                bw.Write(Items[index].KprAirPP);
                bw.Write(Items[index].KprAirPR);
                bw.Write(Items[index].KprHeliumPP);
                bw.Write(Items[index].KprHeliumPR);
                bw.Write(Items[index].Pressure);
                bw.Write(Items[index].CarbonateTiff);
                bw.Write(Items[index].CarbonateDolomite);
                bw.Write(Items[index].CarbonateInsoluble);
                bw.Write(Items[index].AdaBeforeExtract);
                bw.Write(Items[index].AdaAfterExtract);
                bw.Write(Items[index].Wettability);
                bw.Write(Items[index].Porosity);
                bw.Write(Items[index].SatWatFinal);
                bw.Write(Items[index].DensityMineral);
                bw.Write(Items[index].DensityVolume);
                bw.Write(Items[index].PartSize500);
                bw.Write(Items[index].PartSize250);
                bw.Write(Items[index].PartSize100);
                bw.Write(Items[index].PartSize50);
                bw.Write(Items[index].PartSize10);
                bw.Write(Items[index].PartSize0);
                bw.Write(Items[index].RadioActivityK);
                bw.Write(Items[index].RadioActivityKPercent);
                bw.Write(Items[index].RadioActivityTh);
                bw.Write(Items[index].RadioActivityThPercent);
                bw.Write(Items[index].RadioActivityRa);
                bw.Write(Items[index].RadioActivityRaPercent);
                bw.Write(Items[index].RadioActivityA);
                bw.Write(Items[index].Comment);
                return true;
            }
            return false;
        }
    }

    #endregion

    #region SkvPerf objects

    public struct SkvPerfItem
    {
        public DateTime Date;
        public UInt16 PlastCode; 
        public UInt16 PerfTypeId;
        public UInt16 PerforatorTypeId;
        public UInt16 NumHoles;
        public float Top;
        public float Bottom;
        public bool Closed;
        public UInt16 PerfIsolateTypeId;
        public byte Source;
    }

    public sealed class SkvPerf : RdfObject<SkvPerfItem>
    {
        public static int Version = 2;
        protected override int ObjectTypeId
        {
            get { return (int)RdfImport.RdfObjectTypes.otSkv_Perf; }
        }

        public override bool Read(RdfConnector connector, string key)
        {
            MemoryStream ms = new MemoryStream();
            ms = connector.ReadBinary(key, false);

            if (ms == null) return false;

            ms.Seek(16, SeekOrigin.Begin);
            BinaryReader br = new BinaryReader(ms);

            Int32 itemCount = br.ReadInt32();
            Int32 structSize = br.ReadByte();
            int i;
            if (itemCount < 0) return false;

            _items = new SkvPerfItem[itemCount];

            try
            {
                for (i = 0; i < itemCount; i++)
                {
                    _items[i] = RetrieveItem(br);
                }
            }
            finally
            {
                br.Close();
                ms.Dispose();
            }
            return true;
        }

        public SkvPerfItem RetrieveItem(SkvPerfItem item)
        {
            SkvPerfItem res;
            res.Date = item.Date;
            res.PlastCode = item.PlastCode;
            res.PerfTypeId = item.PerfTypeId;
            res.PerforatorTypeId = item.PerforatorTypeId;
            res.NumHoles = item.NumHoles;
            res.Top = item.Top;
            res.Bottom = item.Bottom;
            res.Closed = item.Closed;
            res.PerfIsolateTypeId = item.PerfIsolateTypeId;
            res.Source = item.Source;
            return item;
        }

        public SkvPerfItem RetrieveItem(BinaryReader br)
        {
            SkvPerfItem item;
            item.Date = RdfSystem.UnpackDateI(br.ReadInt32());
            item.PlastCode = br.ReadUInt16();
            item.PerfTypeId = br.ReadUInt16();
            item.PerforatorTypeId = br.ReadUInt16();
            item.NumHoles = br.ReadUInt16();
            item.Top = br.ReadSingle();
            item.Bottom = br.ReadSingle();
            item.Closed = br.ReadBoolean();
            item.PerfIsolateTypeId = br.ReadUInt16();
            item.Source = br.ReadByte();
            return item;
        }

        public bool RetainItem(BinaryWriter bw, int index)
        {
            if ((index > -1) && (index < Items.Length))
            {
                bw.Write((int)RdfSystem.PackDate(Items[index].Date));
                bw.Write(Items[index].PlastCode);
                bw.Write(Items[index].PerfTypeId);
                bw.Write(Items[index].PerforatorTypeId);
                bw.Write(Items[index].NumHoles);
                bw.Write(Items[index].Top);
                bw.Write(Items[index].Bottom);
                bw.Write(Items[index].Closed);
                bw.Write(Items[index].PerfIsolateTypeId);
                bw.Write(Items[index].Source);
                return true;
            }
            return false;
        }

        public void SetItems(SkvPerfItem[] items) { _items = items; }

        static public bool IntersectIntervals(SkvPerfItem item1, SkvPerfItem item2)
        {
            return !((item1.Bottom < item2.Top) || (item1.Top > item2.Bottom));
        }
        static public List<SkvPerfItem> DiffIntervals(SkvPerfItem item1, SkvPerfItem item2)
        {
            SkvPerfItem item;
            List<SkvPerfItem> result = new List<SkvPerfItem>();
            if (item1.Top < item2.Top && item2.Bottom < item1.Bottom)
            {
                item = item1;
                item.Bottom = item2.Top;
                result.Add(item);
                item = item1;
                item.Top = item2.Bottom;
                result.Add(item);
            }
            else if (item2.Top <= item1.Top && item1.Top < item2.Bottom && item2.Bottom < item1.Bottom)
            {
                item = item1;
                item.Top = item2.Bottom;
                result.Add(item);
            }
            else if (item1.Top < item2.Top && item2.Top < item1.Bottom && item1.Bottom <= item2.Bottom)
            {
                item = item1;
                item.Bottom = item2.Top;
                result.Add(item);
            }
            else if ((item2.Top <= item1.Top && item2.Bottom <= item1.Top) || (item1.Bottom <= item2.Top && item1.Bottom <= item2.Bottom))
            {
                result.Add(item1);
            }
            return result;
        }
        public List<SkvPerfItem> GetOpenIntervals(DateTime MaxDate)
        {
            List<SkvPerfItem> result = new List<SkvPerfItem>();
            bool findIsolate = false;
            for (int i = 0; i < Items.Length; i++)
            {
                if (Items[i].Date <= MaxDate)
                {
                    if (Items[i].PerfTypeId != 10000)
                    {
                        findIsolate = false;
                        for (int j = i + 1; j < Items.Length; j++)
                        {
                            if ((Items[j].PerfTypeId == 10000) && (SkvPerf.IntersectIntervals(Items[i], Items[j])))
                            {
                                findIsolate = true;
                                break;
                            }
                        }
                        if (!findIsolate) result.Add(Items[i]);
                    }
                }
            }
            return result;
        }
        public List<SkvPerfItem> GetClosedIntervals(DateTime MaxDate)
        {
            List<SkvPerfItem> result = new List<SkvPerfItem>();
            bool findNotIsolate = false;
            for (int i = 0; i < Items.Length; i++)
            {
                if (Items[i].Date <= MaxDate)
                {
                    if (Items[i].PerfTypeId == 10000)
                    {
                        findNotIsolate = false;
                        for (int j = i + 1; j < Items.Length; j++)
                        {
                            if ((Items[j].PerfTypeId != 10000) && (SkvPerf.IntersectIntervals(Items[i], Items[j])))
                            {
                                findNotIsolate = true;
                                break;
                            }
                        }
                        if (!findNotIsolate) result.Add(Items[i]);
                    }
                }
            }
            return result;
        }
    }

    #endregion

    #region Mer objects

    public class MerComp
    {
        public bool UseOil;
        public bool UseNatGas;
        public bool UseGasCondensate;
        public bool UseInj;
        public bool UseInjGas;
        MerCompItem[] Items;
        public int Count
        {
            get
            {
                if (Items == null)
                {
                    return 0;
                }
                else
                {
                    return Items.Length;
                }
            }
        }
        public MerCompItem this[int index]
        {
            get
            {
                if (index > -1 || index < Count)
                {
                    return Items[index];
                }
                else
                {
                    throw new IndexOutOfRangeException("������ �� ��������� ���������");
                }
            }
        }

        public int GetIndexByDate(DateTime date)
        {
            if (Items != null)
            {
                for (int i = 0; i < Items.Length; i++)
                {
                    if (Items[i].Date == date) return i;
                }
            }
            return -1;
        }
        public MerComp(Mer Mer) : this(Mer, 0, Mer.Count - 1) { }
        public MerComp(Mer Mer, int Index1, int Index2)
        {
            if ((Mer != null) && (Index1 <= Index2) && (Mer.Count > 0))
            {
                DateTime currDate;
                DateTime dt = Mer.Items[Index1].Date, dt2 = Mer.Items[Index2].Date;
                int count = (dt2.Year - dt.Year) * 12 + dt2.Month - dt.Month + 1;
                Items = new MerCompItem[count];
                int i = 0, j = 0;
                currDate = dt;
                while (i < Mer.Count)
                {
                    dt = Mer.Items[i].Date;
                    while (currDate < dt)
                    {
                        Items[j] = new MerCompItem(currDate);
                        currDate = currDate.AddMonths(1);
                        j++;
                    }
                    Items[j] = new MerCompItem(dt);
                    while (i < Mer.Count && Mer.Items[i].Date == dt)
                    {
                        Items[j].RetriveItem(Mer, i);
                        if ((!UseOil) && (Mer.Items[i].CharWorkId == 11)) UseOil = true;
                        if ((!UseNatGas) && (Mer.Items[i].CharWorkId == 12)) UseNatGas = true;
                        if ((!UseGasCondensate) && (Mer.Items[i].CharWorkId == 15)) UseGasCondensate = true;
                        if ((!UseInj) && (Mer.Items[i].CharWorkId == 20)) UseInj = true;
                        if ((!UseInjGas) && (Mer.Items[i].CharWorkId == 20) && (Mer.CountEx > 0))
                        {
                            int agent = Mer.GetItemEx(i).AgentInjId;
                            if((agent > 80) && (agent < 90)) UseInjGas = true;
                        }
                        i++;
                    }
                    j++;
                    currDate = currDate.AddMonths(1);
                }
            }
        }

        public void Clear()
        {
            if (Items != null)
            {
                for (int i = 0; i < Items.Length; i++)
                {
                    Items[i].PlastItems.Clear();
                    Items[i].TimeItems.Clear();
                    Items[i].CharWorkIds.Clear();
                    Items[i] = null;
                }
            }
            Items = null;
        }
    }
    public class MerCompItem
    {
        public DateTime Date;
        public List<int> CharWorkIds;
        public int StateId;
        public int MethodId;

        public List<MerCompPlastItem> PlastItems;
        public MerWorkTimeCollection TimeItems;

        public double SumLiquid
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Liq;
                }
                return sum;
            }
        }
        public double SumOil
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Oil;
                }
                return sum;
            }
        }
        public double SumScoop
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Scoop;
                }
                return sum;
            }
        }
        public double SumLiquidV
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].LiqV;
                }
                return sum;
            }
        }
        public double SumOilV
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].OilV;
                }
                return sum;
            }
        }
        public double SumInjection
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Inj;
                }
                return sum;
            }
        }
        public double SumInjectionGas
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].InjGas;
                }
                return sum;
            }
        }
        public double SumGas
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Gas;
                }
                return sum;
            }
        }
        public double SumNaturalGas
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].NatGas;
                }
                return sum;
            }
        }
        public double SumGasCondensate
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Condensate;
                }
                return sum;
            }
        }

        public MerCompItem(DateTime Date)
        {
            this.Date = Date;
            PlastItems = new List<MerCompPlastItem>();
            CharWorkIds = new List<int>();
            TimeItems = new MerWorkTimeCollection();
        }
        public void RetriveItem(Mer mer, int index)
        {
            if (StateId != mer.Items[index].StateId) StateId = mer.Items[index].StateId;
            if (MethodId != mer.Items[index].MethodId) MethodId = mer.Items[index].MethodId;
            if (CharWorkIds.IndexOf(mer.Items[index].CharWorkId) == -1) CharWorkIds.Add(mer.Items[index].CharWorkId);
            MerCompPlastItem plastItem = GetItem(mer.Items[index].PlastId);
            TimeItems.Add(mer, index);

            plastItem.Liq += mer.Items[index].Wat + mer.Items[index].Oil;
            plastItem.Oil += mer.Items[index].Oil;
            plastItem.LiqV += mer.Items[index].WatV + mer.Items[index].OilV;
            plastItem.OilV += mer.Items[index].OilV;
            plastItem.Gas += mer.Items[index].Gas;
            if (mer.Items[index].CharWorkId == 13) plastItem.Scoop += mer.Items[index].Wat;
            if (mer.CountEx > 0)
            {
                plastItem.NatGas += mer.GetItemEx(index).NaturalGas;
                int agent = mer.GetItemEx(index).AgentInjId;
                if ((agent > 80) && (agent < 90))
                {
                    plastItem.InjGas += mer.GetItemEx(index).Injection;
                }
                else
                {
                    plastItem.Inj += mer.GetItemEx(index).Injection;
                }
                plastItem.Condensate += mer.GetItemEx(index).GasCondensate;
            }
        }
        public MerCompPlastItem GetItem(int PlastCode)
        {
            int ind = -1;
            for (int i = 0; i < PlastItems.Count; i++)
            {
                if (PlastItems[i].PlastCode == PlastCode)
                {
                    ind = i;
                }
            }
            if (ind == -1)
            {
                ind = PlastItems.Count;
                PlastItems.Add(new MerCompPlastItem(this));
                PlastItems[ind].PlastCode = PlastCode;
            }
            return PlastItems[ind];
        }
        public void Clear()
        {
            Date = DateTime.MinValue;
            CharWorkIds.Clear();
            StateId = 0;
            MethodId = 0;
            PlastItems.Clear();
        }
    }
    public class MerCompPlastItem
    {
        public MerCompItem ParentItem;
        public int PlastCode;
        public double Liq;
        public double Oil;
        public double LiqV;
        public double OilV;
        public double Inj;
        public double InjGas;
        public double NatGas;
        public double Condensate;
        public double Gas;
        public double Scoop;
        public MerCompPlastItem(MerCompItem MerExItem)
        {
            this.ParentItem = MerExItem;
        }
    }
    public class MerWorkTimeCollection
    {
        List<int> AllSumIndexes;
        List<MerWorkTimeItem> Items;
        public MerWorkTimeCollection()
        {
            Items = new List<MerWorkTimeItem>();
            AllSumIndexes = new List<int>();
        }
        public int Count { get { return (Items == null) ? 0 : Items.Count; } }

        public void Add(Mer mer, int Index)
        {
            bool find = false;
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Equals(mer, Index))
                {
                    find = true;
                    break;
                }
            }
            if (!find)
            {
                MerWorkTimeItem timeObj = new MerWorkTimeItem();
                timeObj.Retrieve(mer, Index);
                AddItem(timeObj);
            }
        }
        public void Add(MerWorkTimeItem Item)
        {
            bool find = false;
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Equals(Item, true))
                {
                    find = true;
                    break;
                }
            }
            if (!find)
            {
                MerWorkTimeItem timeObj = new MerWorkTimeItem();
                timeObj.Retrieve(Item);
                AddItem(timeObj);
            }
        }
        public void AddItem(MerWorkTimeItem Item)
        {
            int ind = Items.Count;
            if (AllSumIndexes.Count == 0)
            {
                AllSumIndexes.Add(ind);
            }
            else
            {
                bool find = false;
                for (int i = 0; i < Items.Count; i++)
                {
                    if (Items[i].Equals(Item, false))
                    {
                        find = true;
                        break;
                    }
                }
                if (!find)
                {
                    AllSumIndexes.Add(ind);
                }
            }
            Items.Add(Item);
        }
        public void Clear()
        {
            Items.Clear();
            AllSumIndexes.Clear();
        }

        public MerWorkTimeItem GetAllTime()
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                item.Date = Items[AllSumIndexes[i]].Date;
                item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                item.CollTime += Items[AllSumIndexes[i]].CollTime;
                item.StayTime += Items[AllSumIndexes[i]].StayTime;
            }
            return item;
        }
        public MerWorkTimeItem GetAllTime(int CharWorkId, bool InverseCondition)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                if (((!InverseCondition) && (Items[AllSumIndexes[i]].CharWorkId == CharWorkId)) ||
                     ((InverseCondition) && (Items[AllSumIndexes[i]].CharWorkId != CharWorkId)))
                {
                    item.Date = Items[AllSumIndexes[i]].Date;
                    item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                    item.CollTime += Items[AllSumIndexes[i]].CollTime;
                    item.StayTime += Items[AllSumIndexes[i]].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetAllProductionTime()
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                if (Items[AllSumIndexes[i]].CharWorkId == 11 || Items[AllSumIndexes[i]].CharWorkId == 12 || Items[AllSumIndexes[i]].CharWorkId == 15)
                {
                    item.Date = Items[AllSumIndexes[i]].Date;
                    item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                    item.CollTime += Items[AllSumIndexes[i]].CollTime;
                    item.StayTime += Items[AllSumIndexes[i]].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetAllInjectionTime()
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                if (Items[AllSumIndexes[i]].CharWorkId == 20)
                {
                    item.Date = Items[AllSumIndexes[i]].Date;
                    item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                    item.CollTime += Items[AllSumIndexes[i]].CollTime;
                    item.StayTime += Items[AllSumIndexes[i]].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetAllScoopTime()
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                if (Items[AllSumIndexes[i]].CharWorkId == 13)
                {
                    item.Date = Items[AllSumIndexes[i]].Date;
                    item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                    item.CollTime += Items[AllSumIndexes[i]].CollTime;
                    item.StayTime += Items[AllSumIndexes[i]].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem this[int index] {get { return Items[index];} }
        public MerWorkTimeItem GetTime(int CharWorkId, bool InverseCondition)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if (((!InverseCondition) && (Items[i].CharWorkId == CharWorkId)) || 
                     ((InverseCondition) && (Items[i].CharWorkId != CharWorkId)))
                {
                    item.Date = Items[i].Date;
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetTime(int PlastId, int CharWorkId, bool InverseCondition)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i].PlastId == PlastId) && 
                     (((!InverseCondition) && (Items[i].CharWorkId == CharWorkId)) || 
                       ((InverseCondition) && (Items[i].CharWorkId != CharWorkId))))
                {
                    item.Date = Items[i].Date;
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetProductionTime(int PlastId)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i].PlastId == PlastId) &&
                     (Items[i].CharWorkId == 11 || Items[i].CharWorkId == 12 || Items[i].CharWorkId == 15))
                {
                    item.Date = Items[i].Date;
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            //if (item.WorkTime + item.CollTime > 744)
            //{
            //    throw new Exception();
            //}
            return item;
        }
        public MerWorkTimeItem GetInjectionTime(int PlastId)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i].PlastId == PlastId) && (Items[i].CharWorkId == 20))
                {
                    item.Date = Items[i].Date;
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            //if (item.WorkTime + item.CollTime > 744)
            //{
            //    throw new Exception();
            //}
            return item;
        }
        public MerWorkTimeItem GetScoopTime(int PlastId)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i].PlastId == PlastId) && (Items[i].CharWorkId == 13))
                {
                    item.Date = Items[i].Date;
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetProductionTime(List<int> PlastIds)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            MerWorkTimeItem wt;
            for (int i = 0; i < PlastIds.Count; i++)
            {
                wt = GetProductionTime(PlastIds[i]);
                if (wt.Date.Year > 1)
                {
                    item.Date = wt.Date;
                    item.WorkTime += wt.WorkTime;
                    item.CollTime += wt.CollTime;
                    item.StayTime += wt.StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetInjectionTime(List<int> PlastIds)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            MerWorkTimeItem wt;
            for (int i = 0; i < PlastIds.Count; i++)
            {
                wt = GetInjectionTime(PlastIds[i]);
                if (wt.Date.Year > 1)
                {
                    item.Date = wt.Date;
                    item.WorkTime += wt.WorkTime;
                    item.CollTime += wt.CollTime;
                    item.StayTime += wt.StayTime;
                }
            }
            return item;
        }
    }
    public struct MerWorkTimeItem
    {
        public DateTime Date;
        public int PlastId;
        public int CharWorkId;
        public int MethodId;
        public int StateId;
        public int WorkTime;
        public int CollTime;
        public int StayTime;

        public void Retrieve(Mer mer, int Index)
        {
            Date = mer.Items[Index].Date;
            PlastId = mer.Items[Index].PlastId;
            CharWorkId = mer.Items[Index].CharWorkId;
            StateId = mer.Items[Index].StateId;
            MethodId = mer.Items[Index].MethodId;
            WorkTime = mer.Items[Index].WorkTime;
            CollTime = mer.Items[Index].CollTime;
            StayTime = mer.Items[Index].StayTime;
        }
        public bool Equals(Mer mer, int Index)
        {
            return ((Date.Year == mer.Items[Index].Date.Year) &&
                    (Date.Month == mer.Items[Index].Date.Month) &&
                    (PlastId == mer.Items[Index].PlastId) &&
                    (CharWorkId == mer.Items[Index].CharWorkId) &&
                    (StateId == mer.Items[Index].StateId) &&
                    (MethodId == mer.Items[Index].MethodId) &&
                    (WorkTime == mer.Items[Index].WorkTime) &&
                    (CollTime == mer.Items[Index].CollTime));
        }
        public void Retrieve(MerWorkTimeItem Item)
        {
            Date = Item.Date;
            PlastId = Item.PlastId;
            CharWorkId = Item.CharWorkId;
            StateId = Item.StateId;
            MethodId = Item.MethodId;
            WorkTime = Item.WorkTime;
            CollTime = Item.CollTime;
            StayTime = Item.StayTime;
        }
        public bool Equals(MerWorkTimeItem Item, bool TestByPlast)
        {
            return ((Date.Year == Item.Date.Year) &&
                    (Date.Month == Item.Date.Month) &&
                    ((!TestByPlast) || (PlastId == Item.PlastId)) &&
                    (CharWorkId == Item.CharWorkId) &&
                    (StateId == Item.StateId) &&
                    (MethodId == Item.MethodId) &&
                    (WorkTime == Item.WorkTime) &&
                    (CollTime == Item.CollTime));
        }
        public int GetMaxHours()
        {
            return (Date.Year == 1) ? 0 : (Date.AddMonths(1) - Date).Days * 24;
        }
        public int GetWorkHours()
        {
            int maxHours = GetMaxHours();
            return (WorkTime + CollTime > maxHours) ? maxHours : WorkTime + CollTime;
        }
    }

    public struct MerItem
    {
        /// <summary>
        /// ����
        /// </summary>
        public DateTime Date;
        /// <summary>
        /// �����, ��� �� �����������
        /// </summary>
        public int PlastId;
        /// <summary>
        /// �������� ������, ��� �� �����������
        /// </summary>
        public int CharWorkId;
        /// <summary>
        /// ��������� ��������, ��� �� �����������
        /// </summary>
        public int StateId;
        /// <summary>
        /// ������ ������������, ��� �� �����������
        /// </summary>
        public int MethodId;
        /// <summary>
        /// ����� ������, � �����
        /// </summary>
        public int WorkTime;
        /// <summary>
        /// ����� �����, � �����
        /// </summary>
        public int CollTime;
        /// <summary>
        /// �����, �
        /// </summary>
        public double Oil;
        /// <summary>
        /// ����, � /�������, ���������, �3
        /// </summary>
        public double Wat;
        /// <summary>
        /// ���, �3
        /// </summary>
        public double Gas;
        /// <summary>
        /// �����, �3
        /// </summary>
        public double OilV;
        /// <summary>
        /// ����, �3
        /// </summary>
        public double WatV;
        /// <summary>
        /// ������� �������, ��� �� �����������
        /// </summary>
        public int StayReasonId;
        /// <summary>
        /// ����� �������, � �����
        /// </summary>
        public int StayTime;
        /// <summary>
        /// ��� ������ ������
        /// </summary>
        public int AgentProdId;
        //public int Tag;
        public override int GetHashCode()
        {
            return (int)((PlastId + CharWorkId + StateId + MethodId + WorkTime + CollTime + StayReasonId + StayTime + AgentProdId) + Oil + Wat + Gas + OilV + WatV);
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(Date.ToOADate()); // double
            bw.Write(PlastId);
            bw.Write(CharWorkId);
            bw.Write(StateId);
            bw.Write(MethodId);
            bw.Write(WorkTime);
            bw.Write(CollTime);
            bw.Write(Oil);
            bw.Write(Wat);
            bw.Write(Gas);
            bw.Write(OilV);
            bw.Write(WatV);
            bw.Write(StayReasonId);
            bw.Write(StayTime);
            bw.Write(AgentProdId);
        }
        public void Read(BinaryReader br)
        {
            Date = DateTime.FromOADate(br.ReadDouble());
            PlastId = br.ReadInt32();
            CharWorkId = br.ReadInt32();
            StateId = br.ReadInt32();
            MethodId = br.ReadInt32();
            WorkTime = br.ReadInt32();
            CollTime = br.ReadInt32();
            Oil = br.ReadDouble();
            Wat = br.ReadDouble();
            Gas = br.ReadDouble();
            OilV = br.ReadDouble();
            WatV = br.ReadDouble();
            StayReasonId = br.ReadInt32();
            StayTime = br.ReadInt32();
            AgentProdId = br.ReadInt32();

        }
    }
    public struct MerItemEx
    {
        /// <summary>
        /// ��� ������ �������
        /// </summary>
        public int AgentInjId;
        /// <summary>
        /// �������, �3
        /// </summary>
        public double Injection;
        /// <summary>
        /// ��������� ���, �3
        /// </summary>
        public double NaturalGas;
        /// <summary>
        /// �������������, �
        /// </summary>
        public double GasCondensate;
        /// <summary>
        /// ��� ������ �������
        /// </summary>
        public double OtherFluidV;
        /// <summary>
        /// ��� �� �����, �3
        /// </summary>
        public double CapGas;
        /// <summary>
        /// ������� item
        /// </summary>
        /// 
        public static MerItemEx Empty
        {
            get { return new MerItemEx(); } 
        }
        public override int GetHashCode()
        {
            return (int)(AgentInjId + Injection + NaturalGas + GasCondensate + OtherFluidV + CapGas);
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(AgentInjId);
            bw.Write(Injection);
            bw.Write(NaturalGas);
            bw.Write(GasCondensate);
            bw.Write(OtherFluidV);
            bw.Write(CapGas);
        }
        public void Read(BinaryReader br)
        {
            AgentInjId = br.ReadInt32();
            Injection = br.ReadDouble();
            NaturalGas = br.ReadDouble();
            GasCondensate = br.ReadDouble();
            OtherFluidV = br.ReadDouble();
            CapGas = br.ReadDouble();
        }
    }

    public sealed class Mer : RdfObject<MerItem>
    {
        public static int Version = 1;
        public const int ItemLen = 80;
        public const int ItemExLen = 44;

        static Mer()
        {
            Version = 1;
        }

        MerItemEx[] _itemsEx = null;

        public MerItemEx GetItemEx(int index)
        {
            if (_itemsEx == null) return MerItemEx.Empty;
            return _itemsEx[index];
        }
        public int CountEx
        {
            get
            {
                if (_itemsEx == null) return 0;
                return _itemsEx.Length;
            }
        }

        protected override int ObjectTypeId
        {
            get { return (int)RdfImport.RdfObjectTypes.otMer; }
        }

        // READ WRITE RDF CONNECTOR
        public override bool Read(RdfConnector connector, string key)
        {
            MemoryStream ms = new MemoryStream();
            ms = connector.ReadBinary(key, false);

            if (ms == null) return false;

            ms.Seek(0, SeekOrigin.Begin);
            BinaryReader br = new BinaryReader(ms);

            Int32 itemCount = br.ReadInt32();
            Int32 structSize = br.ReadByte();


            if (itemCount < 0) return false;

            _items = new MerItem[itemCount];
            bool createEx = false;
            try
            {
                for (int i = 0; i < itemCount; i++)
                {
                    _items[i].Date = RdfSystem.UnpackDateI(br.ReadInt32());
                    _items[i].PlastId = br.ReadInt32();
                    _items[i].CharWorkId = br.ReadInt32();
                    _items[i].StateId = br.ReadInt32();
                    _items[i].MethodId = br.ReadInt32();
                    _items[i].WorkTime = br.ReadUInt16();
                    _items[i].CollTime = br.ReadUInt16();
                    _items[i].Oil = br.ReadDouble();
                    _items[i].Wat = br.ReadDouble();
                    _items[i].Gas = br.ReadDouble();
                    _items[i].StayReasonId = br.ReadInt32();
                    _items[i].StayTime = br.ReadUInt16();
                    if (!createEx) createEx = (_items[i].CharWorkId == 20) && (_items[i].Wat > 0);
                    br.ReadInt32(); // tag
                }
                if (createEx)
                {
                    _itemsEx = new MerItemEx[itemCount];
                    for (int i = 0; i < itemCount; i++)
                    {
                        if (_items[i].CharWorkId == 20)
                        {
                            _itemsEx[i].Injection = _items[i].Wat;
                        }
                    }
                }
            }
            finally
            {
                br.Close();
                ms.Dispose();
            }
            return true;
        }
        public bool Write(RdfConnector connector, string key)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);

            Int32 itemCount = this.Items.Length;
            byte structSize = 58;

            bw.Write(itemCount);
            bw.Write(structSize);

            for (int i = 0; i < itemCount; i++)
            {
                bw.Write((int)RdfSystem.PackDate(Items[i].Date));
                bw.Write(Items[i].PlastId);
                bw.Write(Items[i].CharWorkId);
                bw.Write(Items[i].StateId);
                bw.Write(Items[i].MethodId);
                bw.Write((ushort)Items[i].WorkTime);
                bw.Write((ushort)Items[i].CollTime);
                bw.Write(Items[i].Oil);
                bw.Write(Items[i].Wat);
                bw.Write(Items[i].Gas);
                bw.Write(Items[i].StayReasonId);
                bw.Write((ushort)Items[i].StayTime);
                bw.Write(0);
            }

            try
            {
                ms.Seek(0, SeekOrigin.Begin);
                connector.WriteBinary(key, ms, false);
                bw.Close();
                ms.Dispose();
                return true;
            }
            catch 
            {
                bw.Close();
                ms.Dispose();
                return false;
            }
        }
        public bool ReadV(RdfConnector connector, string key)
        {
            int i;
            MemoryStream ms = new MemoryStream();
            ms = connector.ReadBinary(key, false);

            if (ms == null) return false;

            ms.Seek(0, SeekOrigin.Begin);
            BinaryReader br = new BinaryReader(ms);

            Int16 Version = br.ReadInt16();

            if (Version != 1) return false;
            br.ReadDouble(); // read GUID
            br.ReadDouble(); // read GUID

            Version = br.ReadInt16();
            int fieldsCount = br.ReadInt32();
            if (fieldsCount == 0) return false;
            string fieldName, fieldCap;
            string[] fieldNames = new string[fieldsCount];
            for (i = 0; i < fieldsCount; i++)
            {
                Version = br.ReadInt16();
                fieldNames[i] = ConvertEx.ReadStrUI16(ms, RdfConnector.encoding);
                fieldCap = ConvertEx.ReadStrUI16(ms, RdfConnector.encoding);
                br.ReadByte();  // fieldType
                br.ReadInt32(); // fieldSize
                br.ReadByte();  // Frequired
            }
            byte packed = br.ReadByte();
            if (packed != 0) return false;
            int rowCount = br.ReadInt32();

            _items = new MerItem[rowCount];
            _itemsEx = null;

            MerItemEx[] itemsEx = new MerItemEx[rowCount];
            try
            {
                for (int f = 0; f < fieldsCount; f++)
                {
                    switch (fieldNames[f])
                    {
                        case "Date":
                            for (i = 0; i < rowCount; i++) _items[i].Date = DateTime.FromOADate(br.ReadDouble());
                            break;
                        case "kPlast":
                            for (i = 0; i < rowCount; i++) _items[i].PlastId = br.ReadInt32();
                            break;
                        case "kCharWork":
                            for (i = 0; i < rowCount; i++) _items[i].CharWorkId = br.ReadInt32();
                            break;
                        case "kState":
                            for (i = 0; i < rowCount; i++) _items[i].StateId = br.ReadInt32();
                            break;
                        case "kMethod":
                            for (i = 0; i < rowCount; i++) _items[i].MethodId = br.ReadInt32();
                            break;
                        case "WorkTime":
                            for (i = 0; i < rowCount; i++) _items[i].WorkTime = (ushort)br.ReadInt32();
                            break;
                        case "CollTime":
                            for (i = 0; i < rowCount; i++) _items[i].CollTime = (ushort)br.ReadInt32();
                            break;
                        case "StayTime":
                            for (i = 0; i < rowCount; i++) _items[i].StayTime = (ushort)br.ReadInt32();
                            break;
                        case "kStayReason":
                            for (i = 0; i < rowCount; i++) _items[i].StayReasonId = br.ReadInt32();
                            break;
                        case "Oil":
                            for (i = 0; i < rowCount; i++) _items[i].Oil = br.ReadDouble();
                            break;
                        case "Wat":
                            for (i = 0; i < rowCount; i++) _items[i].Wat = br.ReadDouble();
                            break;
                        case "Gas":
                            for (i = 0; i < rowCount; i++) _items[i].Gas = br.ReadDouble();
                            break;
                        case "NaturalGas":
                            for (i = 0; i < rowCount; i++) itemsEx[i].NaturalGas = br.ReadDouble();
                            break;
                        case "GasCondensate":
                            for (i = 0; i < rowCount; i++) itemsEx[i].GasCondensate = br.ReadDouble();
                            break;
                        case "CapGas":
                            for (i = 0; i < rowCount; i++) itemsEx[i].CapGas = br.ReadDouble();
                            break;
                        case "kAgentProd":
                            for (i = 0; i < rowCount; i++) _items[i].AgentProdId = br.ReadInt32();
                            break;
                        case "OilV":
                            for (i = 0; i < rowCount; i++) _items[i].OilV = br.ReadDouble();
                            break;
                        case "WatV":
                            for (i = 0; i < rowCount; i++) _items[i].WatV = br.ReadDouble();
                            break;
                        case "OtherFluidV":
                            for (i = 0; i < rowCount; i++) itemsEx[i].OtherFluidV = br.ReadDouble();
                            break;
                        case "kAgentInj":
                            for (i = 0; i < rowCount; i++) itemsEx[i].AgentInjId = br.ReadInt32();
                            break;
                        case "Injection":
                            for (i = 0; i < rowCount; i++) itemsEx[i].Injection = br.ReadDouble();
                            break;
                    }
                }
                for (i = 0; i < rowCount; i++)
                {
                    if (itemsEx[i].AgentInjId + itemsEx[i].Injection + itemsEx[i].CapGas + itemsEx[i].GasCondensate + itemsEx[i].NaturalGas + itemsEx[i].OtherFluidV > 0)
                    {
                        this._itemsEx = itemsEx;
                        break;
                    }
                }
            }
            finally
            {
                br.Close();
                ms.Dispose();
            }
            return true;
        }

        // READ WRITE
        public void Read(BinaryReader br, bool NotLoadData)
        {
            int count = br.ReadInt32();
            int countEx = br.ReadInt32();
            if (!NotLoadData)
            {
                int i;
                _items = new MerItem[count];
                for (i = 0; i < count; i++)
                {
                    ReadItem(br, i);
                }
                _itemsEx = null;
                if (countEx > 0)
                {
                    _itemsEx = new MerItemEx[countEx];
                    for (i = 0; i < countEx; i++)
                    {
                        ReadItemEx(br, i);
                    }
                }
            }
            else
            {
                if (count > 0 || countEx > 0) _items = new MerItem[1];
                br.BaseStream.Seek(count * ItemLen + countEx * ItemExLen, SeekOrigin.Current);
            }
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(Count);
            bw.Write(CountEx);
            int i;
            for (i = 0; i < Count; i++)
            {
                WriteItem(bw, i);
            }
            for (i = 0; i < CountEx; i++)
            {
                WriteItemEx(bw, i);
            }
        }
        void ReadItem(BinaryReader br, int Index)
        {
            _items[Index].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
            _items[Index].PlastId = br.ReadInt32();
            _items[Index].CharWorkId = br.ReadInt32();
            _items[Index].StateId = br.ReadInt32();
            _items[Index].MethodId = br.ReadInt32();
            _items[Index].WorkTime = br.ReadInt32();
            _items[Index].CollTime = br.ReadInt32();
            _items[Index].Oil = br.ReadDouble();
            _items[Index].Wat = br.ReadDouble();
            _items[Index].OilV = br.ReadDouble();
            _items[Index].WatV = br.ReadDouble();
            _items[Index].Gas = br.ReadDouble();
            _items[Index].StayReasonId = br.ReadInt32();
            _items[Index].StayTime = br.ReadInt32();
            _items[Index].AgentProdId = br.ReadInt32();           
        }
        void WriteItem(BinaryWriter bw, int Index)
        {
            bw.Write((int)RdfSystem.PackDateTime(_items[Index].Date));
            bw.Write(_items[Index].PlastId);
            bw.Write(_items[Index].CharWorkId);
            bw.Write(_items[Index].StateId);
            bw.Write(_items[Index].MethodId);
            bw.Write(_items[Index].WorkTime);
            bw.Write(_items[Index].CollTime);
            bw.Write(_items[Index].Oil);
            bw.Write(_items[Index].Wat);
            bw.Write(_items[Index].OilV);
            bw.Write(_items[Index].WatV);
            bw.Write(_items[Index].Gas);
            bw.Write(_items[Index].StayReasonId);
            bw.Write(_items[Index].StayTime);
            bw.Write(_items[Index].AgentProdId);
        }
        void ReadItemEx(BinaryReader br, int Index)
        {
            if (_itemsEx == null)
            {
                throw new NullReferenceException("������ ItemEx �� ������!");
            }
            _itemsEx[Index].AgentInjId = br.ReadInt32();
            _itemsEx[Index].Injection = br.ReadDouble();
            _itemsEx[Index].NaturalGas = br.ReadDouble();
            _itemsEx[Index].GasCondensate = br.ReadDouble();
            _itemsEx[Index].OtherFluidV = br.ReadDouble();
            _itemsEx[Index].CapGas = br.ReadDouble();
        }
        void WriteItemEx(BinaryWriter bw, int Index)
        {
            if (_itemsEx == null)
            {
                throw new NullReferenceException("������ ItemEx �� ������!");
            }
            bw.Write(_itemsEx[Index].AgentInjId);
            bw.Write(_itemsEx[Index].Injection);
            bw.Write(_itemsEx[Index].NaturalGas);
            bw.Write(_itemsEx[Index].GasCondensate);
            bw.Write(_itemsEx[Index].OtherFluidV);
            bw.Write(_itemsEx[Index].CapGas);
        }

        public int GetBinLength()
        {
            int len = 8; // Count + CountEx;
            if (Count > 0)
            {
                len += Count * ItemLen; // Items
                len += CountEx * ItemExLen; // ItemsEx
            }
            return len;
        }
        public void SetItems(MerItem[] items, MerItemEx[] itemsEx) { _items = items; _itemsEx = itemsEx; }

        // GET DATA
        public int GetIndex(DateTime Date)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Items[i].Date.Year == Date.Year && Items[i].Date.Month == Date.Month)
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetIndex(int startIndex, DateTime Date)
        {
            for (int i = startIndex; i < Count; i++)
            {
                if (Items[i].Date.Year == Date.Year && Items[i].Date.Month == Date.Month)
                {
                    return i;
                }
            }
            return -1;
        }
        public int[] GetIndexes(DateTime Date)
        {
            int[] result = new int[2];
            result[0] = -1;
            result[1] = -1;
            for (int i = 0; i < Count; i++)
            {
                if ((Items[i].Date.Year == Date.Year) && (Items[i].Date.Month == Date.Month))
                {
                    if (result[0] == -1) result[0] = i;
                    result[1] = i;
                }
                else if (result[0] != -1)
                {
                    break;
                }

            }
            return result;
        }
        public int GetAccumParams(int Index1, int Index2, ref MerItem Item, ref MerItemEx ItemEx)
        {
            int i;
            if (Index1 < 0) Index1 = 0;
            if (Index2 > Count) Index2 = Count;
            int year = Items[Index1].Date.Year, month = Items[Index1].Date.Month;
            bool ex = CountEx > 0;
            for (i = Index1; i < Index2; i++)
            {
                Item.Oil += _items[i].Oil;
                Item.Wat += _items[i].Wat;
                Item.OilV += _items[i].OilV;
                Item.WatV += _items[i].WatV;
                if (ex)
                {
                    ItemEx.CapGas += _itemsEx[i].CapGas;
                    ItemEx.GasCondensate += _itemsEx[i].GasCondensate;
                    ItemEx.Injection += _itemsEx[i].Injection;
                    ItemEx.NaturalGas += _itemsEx[i].NaturalGas;
                    ItemEx.OtherFluidV += _itemsEx[i].OtherFluidV;
                }
            }
            return i;
        }
        public MerComp GetSumParams(Mer mer, int Index1, int Index2)
        {
            if (Index1 < 0) Index1 = 0;
            if (Index2 > Count) Index2 = Count;
            return new MerComp(mer, Index1, Index2);
        }

        // Delta Updates
        public List<DeltaRecordMer> GetDeltaUpdates(Mer newMer)
        {
            List<DeltaRecordMer> deltaRecords = new List<DeltaRecordMer>();
            DeltaRecordMer deltaRec;
            int oldIndex, newIndex, index;
            oldIndex = 0;
            for (newIndex = 0; newIndex < newMer.Count; newIndex++)
            {
                index = this.GetIndex(oldIndex, newMer.Items[newIndex].Date);

                if (oldIndex == index)
                {
                    if (CountEx != newMer.CountEx || 
                        this.Items[oldIndex].GetHashCode() != newMer.Items[newIndex].GetHashCode() || 
                        (this.CountEx > 0 && this.GetItemEx(oldIndex).GetHashCode() != newMer.GetItemEx(newIndex).GetHashCode()))
                    {
                        deltaRec = new DeltaRecordMer(DeltaOperationType.Edit, oldIndex, newMer.Items[newIndex], newMer.GetItemEx(newIndex));
                        deltaRecords.Add(deltaRec);
                    }
                    oldIndex++;
                }
                else if (index == -1) // ��������� ������
                {
                    if (oldIndex < Count)
                    {
                        deltaRec = new DeltaRecordMer(DeltaOperationType.Insert, oldIndex, newMer.Items[newIndex], newMer.GetItemEx(newIndex));
                    }
                    else
                    {
                        deltaRec = new DeltaRecordMer(DeltaOperationType.Add, -1, newMer.Items[newIndex], newMer.GetItemEx(newIndex));
                    }
                    deltaRecords.Add(deltaRec);
                }
                else if (newIndex < index)
                {
                    deltaRec = new DeltaRecordMer(DeltaOperationType.Remove, oldIndex, null, null);
                    deltaRecords.Add(deltaRec);
                    newIndex--;
                    oldIndex++;
                }
            }
            // ������� �������
            for (; oldIndex < Count; oldIndex++)
            {
                deltaRec = new DeltaRecordMer(DeltaOperationType.Remove, oldIndex, null, null);
                deltaRecords.Add(deltaRec);
            }
            return deltaRecords;
        }
        public void AddDeltaUpdate(DeltaOperationType DeltaType, int oldIndex, MerItem item, MerItemEx itemEx)
        {
            List<MerItem> newItems = null;
            List<MerItemEx> newItemsEx = null;
            bool needEx = itemEx.AgentInjId > -1;
            if (DeltaType != DeltaOperationType.Edit)
            {
                newItems = new List<MerItem>(this.Items);
                newItemsEx= new List<MerItemEx>();
                if (this._itemsEx != null)
                {
                    newItemsEx.AddRange(this._itemsEx);
                }
                if (needEx && newItemsEx.Count == 0)
                {
                    newItemsEx = new List<MerItemEx>(new MerItemEx[Items.Length]);
                }
            }
            switch (DeltaType)
            {
                case DeltaOperationType.Add:
                    newItems.Add(item);       
                    if (needEx) newItemsEx.Add(itemEx);
                    break;
                case DeltaOperationType.Insert:
                    if (oldIndex == -1 || oldIndex > this.Count) throw new Exception("������ ���������� delta ����������");
                    newItems.Insert(oldIndex, item);
                    if (needEx) newItemsEx.Insert(oldIndex, itemEx);
                    break;
                case DeltaOperationType.Edit:
                    if (oldIndex == -1 || oldIndex > this.Count) throw new Exception("������ ���������� delta ����������");
                    this._items[oldIndex] = item;
                    if (needEx)
                    {
                        if (_itemsEx == null) _itemsEx = new MerItemEx[this.Count];
                        this._itemsEx[oldIndex] = itemEx;
                    }
                    break;
                case DeltaOperationType.Remove:
                    if (oldIndex == -1 || oldIndex > this.Count) throw new Exception("������ ���������� delta ����������");
                    newItems.RemoveAt(oldIndex);
                    if (needEx) newItemsEx.RemoveAt(oldIndex);
                    break;
            }
            if (DeltaType != DeltaOperationType.Edit)
            {
                SetItems(newItems.ToArray(), (newItemsEx.Count > 0) ? newItemsEx.ToArray() : null);
            }
        }
    }
    #endregion

    #region SkvLog object
    
    public class SkvLog : RdfObject<float>
    {
        public static int Version = 0;
        protected override int ObjectTypeId
        {
            get { return (int)RdfImport.RdfObjectTypes.otSkv_Log; }
        }

        /// <summary>
        /// ��� ��������� (��� ������)
        /// </summary>
        public int MnemonikaId;
        /// <summary>
        /// ��� ������� ���������
        /// </summary>
        public int DemensionId;
        /// <summary>
        /// �� ������ ����� �������� ������
        /// </summary>
        public string LasFileName;
        /// <summary>
        /// ���� ���������� ������������
        /// </summary>
        public DateTime Date;
        /// <summary>
        /// ��������� �������
        /// </summary>
        public float StartDepth;
        /// <summary>
        /// �������� �������
        /// </summary>
        public float EndDepth;
        /// <summary>
        /// ��� ���������
        /// </summary>
        public float Step;
        /// <summary>
        /// ���������� ��������
        /// </summary>
        public float NullValue;

        public float MinValue = -1;
        public float MaxValue = -1;

        public override bool Read(RdfConnector connector, string key)
        {            
            MemoryStream ms;

            ms = connector.ReadBinary(key, false);

            if ((ms == null) || (ms.Length == 0)) return false;

            ms.Seek(0, SeekOrigin.Begin);
            BinaryReader br = new BinaryReader(ms);
            int itemCount = br.ReadInt32();
            int structSize = br.ReadByte();
            int zipLen = br.ReadInt32();

            if (itemCount <= 0) return false;

            _items = new float[itemCount];

            MemoryStream tempMs = new MemoryStream();
            ConvertEx.CopyStream(ms, tempMs, zipLen);
            MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(tempMs);
            BinaryReader unpackBR = new BinaryReader(unpackMS);
            unpackMS.Seek(0, SeekOrigin.Begin);
            try
            {
                for (int i = 0; i < itemCount; i++)
                {
                    _items[i] = unpackBR.ReadSingle();
                }
            }
            finally
            {
                unpackBR.Close();
                unpackMS.Dispose();
                tempMs.Dispose();
            }

            Date = RdfSystem.UnpackDateI(ConvertEx.GetInt32(ms));
            StartDepth = ConvertEx.GetSingle(ms);
            EndDepth = ConvertEx.GetSingle(ms);
            Step = ConvertEx.GetSingle(ms);
            NullValue = ConvertEx.GetSingle(ms);
            MnemonikaId = ConvertEx.GetInt32(ms);
            this.DemensionId = ConvertEx.GetInt32(ms);
            LasFileName = ConvertEx.ReadStrUI16(ms, RdfConnector.encoding);
            br.Close();
            ms.Dispose();
            if ((StartDepth == 0) || (EndDepth == 0) || (Step == 0)) return false;
            return true;
        }

        public bool RetriveItems(BinaryReader br, bool LoadValues)
        {
            MnemonikaId = br.ReadInt32();
            DemensionId = br.ReadInt32();
            Date = DateTime.FromOADate(br.ReadDouble());
            StartDepth = br.ReadSingle();
            EndDepth = br.ReadSingle();
            Step = br.ReadSingle();
            NullValue = br.ReadSingle();
            LasFileName = br.ReadString();
            int i, count = br.ReadInt32();
            if ((count > 0) && (LoadValues))
            {
                _items = new float[count];
                for (i = 0; i < count; i++)
                {
                    _items[i] = br.ReadSingle();
                    if ((MinValue == -1) || (MinValue > _items[i])) MinValue = _items[i];
                    if ((MaxValue == -1) || (MaxValue < _items[i])) MaxValue = _items[i];
                }
            }
            if (!LoadValues) br.BaseStream.Seek(count * 4, SeekOrigin.Current);
            return true;
        }
        public bool RetainItems(BinaryWriter bw)
        {
            bw.Write(MnemonikaId);
            bw.Write(DemensionId);
            bw.Write(Date.ToOADate());
            bw.Write(StartDepth);
            bw.Write(EndDepth);
            bw.Write(Step);
            bw.Write(NullValue);
            bw.Write(LasFileName);
            bw.Write(Items.Length);
            for (int i = 0; i < Items.Length; i++)
            {
                bw.Write(Items[i]);
            }
            return true;
        }
        public void SetItems(float[] items)
        {
            this._items = items;
        }
    }
    
    #endregion
}