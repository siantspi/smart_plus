using System;
using System.Collections.Generic;
using System.Text;

namespace RDF.Objects
{
    /// <summary>
    /// Summary description for IRDFObject interface
    /// </summary>
    public interface IRdfObject
    {
        /// <summary>
        /// Reads the specified connector.
        /// </summary>
        /// <param name="connector">The connector.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        bool Read(RdfConnector connector, string key);

        /// <summary>
        /// Reads the specified connector.
        /// WARNING TO USE!!!
        /// </summary>
        /// <param name="connector">The connector.</param>
        /// <param name="key">The key.</param>
        /// <param name="objectTypeID">The object type ID.</param>
        /// <returns></returns>
        bool Read(RdfConnector connector, string key, int objectTypeID);
    }

    /// <summary>
    /// Summary description for IRDFObject interface
    /// </summary>
    public interface IRdfObjectItem
    {
        /// <summary>
        /// retrive Item
        /// </summary>
        /// <param name="connector">The connector.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        bool RetriveItem(System.IO.BinaryReader br);
    }
}
