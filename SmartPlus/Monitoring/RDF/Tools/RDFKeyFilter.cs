using System;
using System.Collections.Generic;
using System.Text;

namespace RDF
{
    /// <summary>
    /// ������������ ������� ������������ RDF-������ �� ��������� �������
    /// </summary>
    public class KeyFilter
    {
        /// <summary>
        /// ������-����������� � ���� RDF
        /// </summary>
        protected RdfConnector conn = null;
        /// <summary>
        /// �������� �������� ���������� �������� �����
        /// </summary>
        protected string[][] validValues = null;
        /// <summary>
        /// ��������������� ������ ������ ���������� ������ �����������
        /// </summary>
        protected string[][] dirLists = null;
        /// <summary>
        /// ������� ������� � ������� <see cref="dirLists"/>
        /// </summary>
        protected int[] curPositions;

        /// <summary>
        /// ������� ����������� ������������� ������ RDF-����
        /// </summary>
        /// <param name="conn">�������� ����������� � RDF-����</param>
        /// <param name="mask">�������� ���������� �������� �����. 
        /// �������� ����� (������� ������ �����������) ��������� �������� '@', 
        /// ������ ���������� �������� �������� ����������� �������� '\n'.
        /// ��������: ����� "spr@1\n2@*" ������ ����� "spr@1@all", "spr@2@none" � �.�.
        /// </param>
        public KeyFilter(RdfConnector conn, string mask)
        {
            this.conn = conn;
            string[] filters = mask.Split('@');
            int n = filters.Length;
            dirLists = new string[n][];
            validValues = new string[n][];
            curPositions = new int[n];
            for (int i = n - 1; i >= 0; i--)
            {
                curPositions[i] = -1;
                string[] values = filters[i].Split('\n');
                if (values.Length == 1 && values[0] == "*" || values[0] == String.Empty)
                    values = null;
                else
                    Array.Sort(values);
                validValues[i] = values;
            }
        }

        public static string ArrKeyToStr(string[] arrKey)
        {
            return String.Join("@", arrKey);
        }

        /// <summary>
        /// ��������� ���������� �����, ���������������� ��������� �������
        /// </summary>
        /// <returns>���������� "������� � ������������" �������� �����, ���� ���������� ���� ������,
        /// ����� null
        /// </returns>
        public string GetNextKey() { return ArrKeyToStr(GetNextKeyComponents()); }

        /// <summary>
        /// ��������� ���������� �����, ���������������� ��������� �������
        /// </summary>
        /// <returns>���������� ������ ������������ ���� �����, ���� ���������� ���� ������,
        /// ����� null
        /// </returns>
        public string[] GetNextKeyComponents()
        {
            int n = validValues.Length; // max. nesting level 
            int i = (curPositions[0] < 0) ? 0 : n - 1; // "0" first time, "n-1" later
            while (i >= 0 && i < n)
            {
                if (curPositions[i] > 0)
                {
                    curPositions[i]--; // try other value at this level
                    i++; // next level
                }
                else if (curPositions[i] == 0)
                {
                    curPositions[i] = -1; // no more values at this level
                    i--; // previous level
                }
                else if (curPositions[i] < 0)
                {
                    LoadValuesList(i);
                    curPositions[i]--; // last value in array
                    if (curPositions[i] < 0)
                        i--; // no allowable values loaded, go to previous level of nesting
                    else
                        i++; // next level
                }
            }
            if (i < 0)
                return null;
            else // i==n
            {
                string[] key = new string[n];
                for (int j = n - 1; j >= 0; j--)
                    key[j] = dirLists[j][curPositions[j]];
                return key;
            }
        }

        protected void LoadValuesList(int level)
        {
            string key = (level == 0) ? "" : dirLists[0][curPositions[0]];
            for (int i = 1; i < level; i++)
                key = key + '@' + dirLists[i][curPositions[i]];
            string[] srcList = conn.GetList(key);
            string[] dstList = null;
            if (validValues[level] == null || validValues[level].Length == 0)
                // ������ ����������� ����� -> ��������� ����� ��������
                dstList = srcList;
            else
            {
                // ������������ ����� ���������� ��������, ������������ �������� � null
                int n = 0;
                for (int i = srcList.Length - 1; i >= 0; i--)
                    if (srcList[i] != null && Array.BinarySearch(validValues[level], srcList[i]) >= 0)
                        n++;
                    else
                        srcList[i] = null;
                // ������ ������ �� ���������� �������� ( != null )
                dstList = new string[n];
                for (int i = srcList.Length - 1; i >= 0; i--)
                    if (srcList[i] != null)
                        dstList[--n] = srcList[i];
            }
            Array.Sort(dstList);
            Array.Reverse(dstList);
            dirLists[level] = dstList;
            curPositions[level] = dstList.Length;
        }
    }
}
