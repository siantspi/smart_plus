using System;
using System.Collections.Generic;
using System.Text;

namespace RDF.Tools
{
	public class RdfLock : IDisposable
	{
		private const string _lockKeyPrefix = "#lock#";

		private string _key;
		private bool _locked = false;
		private RdfConnector _con;

		public RdfLock(RdfConnector connector, string key)
		{
			_key = key;
			_con = connector;
		}

		private static string _guid = string.Format("{0}:{1}", Environment.UserName, AppDomain.CurrentDomain.Id);
		private static string Guid
		{
			get
			{
				return _guid;
			}
		}

		public bool Lock()
		{
			string[] items = _con.GetList(_key);
			if (!Array.Exists(items, delegate(string item) { return item.StartsWith(_lockKeyPrefix); }))
			{
				//_con.
				_locked = true;
			}

			return _locked;
		}

		public bool Unlock()
		{
			if (_locked)
			{
				;
			}

			return true;
		}


		#region IDisposable Members

		public void Dispose()
		{
			Unlock();
		}

		#endregion
	}
}
