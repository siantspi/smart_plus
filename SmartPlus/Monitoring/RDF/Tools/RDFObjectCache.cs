using System;
using System.Collections.Generic;
using System.Text;
using RDF.Objects;

namespace RDF
{
    /// <summary>
    /// Summary description for RDFObjectCache
    /// </summary>
    //[Serializable]
    class RdfObjectCache
    {
        /// <summary>
        /// Maximum objects count (for cache manage)
        /// </summary>
        private int _maxObjectsCount = 10000;

        /// <summary>
        /// Gets or sets the max objects count.
        /// </summary>
        /// <value>The max objects count.</value>
        public int MaxObjectsCount
        {
            get { return _maxObjectsCount; }
            set { _maxObjectsCount = value; }
        }

        /// <summary>
        /// objects storage
        /// </summary>
        
        private Dictionary<string, object> _objects = new Dictionary<string, object>();

        /// <summary>
        /// Clears cache.
        /// </summary>
        public void Clear()
        {
            _objects.Clear();
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();
        }

        /// <summary>
        /// Gets the object.
        /// </summary>
        /// <param name="connector">The connector.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public object GetObject<T>(RdfConnector connector, string key) where T : IRdfObject, new()
        {
            object obj;
            string objKey = string.Format("{0}@<{1}>:{2}", connector.DbName, typeof(T).Name, key);

            if (_objects.TryGetValue(objKey, out obj))
            {
                return obj;
            }
            else
            {
                T rdfObj = new T();

                try
                {
                    if (rdfObj.Read(connector, key))
                    {
                        Manage();
                        _objects.Add(objKey, rdfObj);
                        return rdfObj;
                    }
                    else
                    {
                        //throw new RDFException(string.Format("������ ��� �������� ������� ���� '{0}' � ������ '{1}'", typeof(T).Name, key));
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is RdfAccessViolationException)
                        throw ex;

                    throw new RdfDataAccessException(
                        string.Format("������ ��� �������� ������� �� ����� '{0}' � ������� '{1}:{2}", 
                            key, connector.UserName, connector.DbName),
                        ex);
                }
            }
        }

        /// <summary>
        /// Gets the object.
        /// </summary>
        /// <param name="connector">The connector.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public object GetObject<T>(RdfConnector connector, string key, int objectTypeID) where T : IRdfObject, new()
        {
            object obj;
            string objKey = string.Format("{0}@<{1}>:{2}", connector.DbName, typeof(T).Name, key);

            if (_objects.TryGetValue(objKey, out obj))
            {
                return obj;
            }
            else
            {
                T rdfObj = new T();

                try
                {
                    if (rdfObj.Read(connector, key, objectTypeID))
                    {
                        Manage();
                        _objects.Add(objKey, rdfObj);
                        return rdfObj;
                    }
                    else
                    {
                        //throw new RDFException(string.Format("������ ��� �������� ������� ���� '{0}' � ������ '{1}'", typeof(T).Name, key));
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw new RdfDataAccessException(
                        string.Format("������ ��� �������� ������� �� ����� '{0}' � ������� '{1}:{2}",
                            key, connector.UserName, connector.DbName),
                        ex);
                }
            }
        }

        private void Manage()
        {
            if (_objects.Count > _maxObjectsCount)
            {
                Clear();
            }
        }

        /// <summary>
        /// Exists the specified connector.
        /// </summary>
        /// <param name="connector">The connector.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public bool Exist<T>(RdfConnector connector, string key)
        {
            string objKey = string.Format("{0}@<{1}>:{2}", connector.DbName, typeof(T).Name, key);
            return _objects.ContainsKey(objKey);
        }

        /// <summary>
        /// Adds the object.
        /// </summary>
        /// <param name="connector">The connector.</param>
        /// <param name="key">The key.</param>
        /// <param name="obj">The obj.</param>
        public void AddObject<T>(RdfConnector connector, string key, T obj) where T : IRdfObject, new()
        {
            string objKey = string.Format("{0}@<{1}>:{2}", connector.DbName, typeof(T).Name, key);
            _objects.Add(objKey, obj);
        }

        /// <summary>
        /// Tries the get object.
        /// </summary>
        /// <param name="connector">The connector.</param>
        /// <param name="key">The key.</param>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public bool TryGetObject<T>(RdfConnector connector, string key, out T obj) where T : IRdfObject, new()
        {
            string objKey = string.Format("{0}@<{1}>:{2}", connector.DbName, typeof(T).Name, key);

            object o;
            bool res = _objects.TryGetValue(objKey, out o);
            obj = (T)o;

            return res;
        }

        /// <summary>
        /// Saves the specified file name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        public void Save(string fileName)
        {
            using (System.IO.FileStream f = System.IO.File.OpenWrite(fileName))
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                bf.Serialize(f, _objects);
                
                f.Close();
            }
        }

        /// <summary>
        /// Loads the specified file name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        public void Load(string fileName)
        {
            System.IO.FileStream f = System.IO.File.OpenRead(fileName);
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            _objects = (Dictionary<string, object>)bf.Deserialize(f);
            f.Close();
        }
    }
}
