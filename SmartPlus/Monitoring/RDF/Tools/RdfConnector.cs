using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.IO;


namespace RDF
{
    /// <summary>
    /// Summary description for RdfConnector class.
    /// </summary>
    public class RdfConnector
    {
        TcpClient _tcpclient = null;
        Stream stream = null;
        private static Encoding _encoding = Encoding.GetEncoding(1251);

        public delegate void ReconnectHandler(RdfConnector conn);
        ReconnectHandler reconnHandler;

        public RdfConnector() { init(null); }

        public RdfConnector(ReconnectHandler rh) { init(rh); }

        void init(ReconnectHandler rh)
        {
            _tcpclient = new TcpClient(AddressFamily.InterNetwork);
            _tcpclient.NoDelay = true;
            _binded = false;
            reconnHandler = rh;
        }

        int reconnectLock = 0;
        bool Reconnect()
        {
            if (reconnHandler == null || System.Threading.Interlocked.CompareExchange(ref reconnectLock, 1, 0) == 0)
                return false;
            try
            {
                reconnHandler(this);
                return true;
            }
            finally { System.Threading.Interlocked.Decrement(ref reconnectLock); }
        }

        /// <summary>
        /// Exception ��� ������ ������� �� �������������� �����
        /// </summary>
        public bool throwExceptionKeyNotExistOnRead = false;

        public static Encoding encoding
        {
            get { return _encoding; }
        }

        string _server;
        public string Server { get { return _server; } }

        int _Port;
        public int Port { get { return _Port; } }

        bool _binded;
        public bool Binded { get { return _binded; } }

        string _pass;

        /// <summary>
        /// Connector handle to RDF database
        /// </summary>
        private IntPtr _rdfConnectror;

        private string _userName = string.Empty;

        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public string UserName
        {
            get { return _userName; }
        }
        private string _dbName = string.Empty;

        /// <summary>
        /// Gets the name of the db.
        /// </summary>
        /// <value>The name of the db.</value>
        public string DbName
        {
            get { return _dbName; }
        }

        /// <summary>
        /// Gets the RDF connectror.
        /// </summary>
        /// <value>The RDF connectror.</value>
        public IntPtr RdfConnectror
        {
            get { return _rdfConnectror; }
        }

        /// <summary>
        /// Gets the last error.
        /// </summary>
        /// <value>The last error.</value>
        public RDF.RdfImport.ErrorCodes LastError
        {
            get
            {
                return (RdfImport.ErrorCodes.DB_OK);
            }
        }

        /// <summary>
        /// command from RDFConst class
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private bool SendSimpleCommand(Int32 command)
        {
            CommandRDF com = new CommandRDF(1, command, 1, 0, 0);
            MemoryStream stream;
            Int32 rs = SendMessage(com, null, null, out stream);
            return (rs == 1);
        }

        public bool Bind()
        {
            _binded = SendSimpleCommand(RDFConst.ID_BINDTIMER);
            return _binded;
        }

        public bool Unbind()
        {
            _binded = SendSimpleCommand(RDFConst.ID_UNBINDTIMER);
            return _binded;
        }

        private string ReadStr(byte[] data, int offset)
        {
            ushort num = BitConverter.ToUInt16(data, offset);
            string rs = _encoding.GetString(data, offset + 2, num);
            return rs;
        }

        private MemoryStream ReadWellCome()
        {
            MemoryStream ms = new MemoryStream();
            Int32 LengthPacket = Int32.MaxValue;
            byte[] b = new byte[1024];
            int rc = stream.Read(b, 0, b.Length);
            ms.Write(b, 0, rc);
            return ms;
        }

        private MemoryStream Read()
        {
            for (int i = 1; i >= 0; i--)
                try
                {
                    MemoryStream ms = new MemoryStream();
                    byte[] b = new byte[1024];
                    int rc = stream.Read(b, 0, b.Length);
                    //��������� ���������
                    ms.Write(b, 0, rc);
                    if (rc == 0)
                        throw new System.IO.EndOfStreamException();
                    CommandRDF com = CommandRDF.Extract(ms.ToArray());
                    Int32 LengthPacket = CommandRDF.LengthPacketFromServer(com);
                    //��������� ������
                    while (ms.Length < LengthPacket)
                    {
                        b = new byte[LengthPacket];
                        //while (true)
                        //{
                        rc = stream.Read(b, 0, b.Length);
                        //if (rc == 0)
                        //    break;
                        ms.Write(b, 0, rc);
                        //}
                        System.Threading.Thread.Sleep(2);
                    }
                    return ms;
                }
                catch (Exception ex)
                {
                    if (i == 0 || Connected || !Reconnect())
                        throw new RdfException("Reading error", ex);
                }
            return null;
        }

        private void Write(byte[] data)
        {
            for (int i = 1; i >= 0; i--)
                try
                {
                    stream.Write(data, 0, data.Length);
                    return;
                }
                catch (Exception ex)
                {
                    if (i == 0 || Connected || !Reconnect())
                        throw new RdfException("Reading error", ex);
                }
        }

        /// <summary>
        /// Command For Server
        /// </summary>
        /// <param name="com"></param>
        /// <param name="Key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private byte[] getCommand(CommandRDF com, String Key, byte[] data)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            com.WriteCommand(ms);

            if (!string.IsNullOrEmpty(Key))
            {
                ConvertEx.WriteToMs(ms, BitConverter.GetBytes((ushort)Key.Length));
                ConvertEx.WriteToMs(ms, _encoding.GetBytes(Key));
            }

            if ((data != null) && (data.Length > 0))
                ConvertEx.WriteToMs(ms, data);

            return ms.ToArray();
        }

        /// <summary>
        /// Send Message To Server
        /// </summary>
        /// <param name="com"></param>
        /// <param name="Key"></param>
        /// <param name="data"></param>
        /// <param name="resMemS"></param>
        /// <returns>State of operation </returns>
        private Int32 SendMessage(CommandRDF com, String Key, byte[] data, out MemoryStream resMemS)
        {
            //������
            byte[] writeCom = getCommand(com, Key, data);
            Write(writeCom);

            //�����
            MemoryStream ms = Read();
            com = CommandRDF.Extract(ms.ToArray());
            if (com == null)
            {
                resMemS = null;
                return 0;
            }
            if (com.State > 0 && com.DataLen > 0)
            {
                resMemS = new MemoryStream();
                resMemS.Write(ms.ToArray(), CommandRDF.SIZE_STRUCT, (int)ms.Length - CommandRDF.SIZE_STRUCT);
            }
            else resMemS = null;
            return com.State;
        }

        /// <summary>
        /// Read data from Key
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="resMemS"></param>
        /// <returns></returns>
        private Int32 Read(string Key, out MemoryStream resMemS)
        {
            CommandRDF com = new CommandRDF(1, RDFConst.ID_READ, 1, Key.Length, 0);
            Int32 rs = SendMessage(com, Key, null, out resMemS);
            if (rs <= 0)
                if (!IsKeyExist(Key))
                {
                    if (throwExceptionKeyNotExistOnRead)
                        throw new RdfException(ErrorID.KEY_NOT_EXISTS, _server, _Port, UserName, Key);
                }
                else throw new RdfException(ErrorID.READ_ERROR, _server, _Port, UserName, Key);

            return rs;
        }

        public static bool ValidateServerCertificate(
              object sender,
              System.Security.Cryptography.X509Certificates.X509Certificate certificate,
              System.Security.Cryptography.X509Certificates.X509Chain chain,
              System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public void Connect(string IP, int Port, string user, string pass, byte[] userKey, bool useSSL)
        {
            if ((user != null) && (user == "guest") && (string.IsNullOrEmpty(pass)))
                pass = "guest";
            Disconnect();

            _tcpclient = new TcpClient(AddressFamily.InterNetwork);
            _tcpclient.NoDelay = true;
            _tcpclient.Connect(IP, Port);
            try
            {
                if (useSSL)
                {
                    System.Net.Security.SslStream sslStream =
                        new System.Net.Security.SslStream(_tcpclient.GetStream(), true, ValidateServerCertificate, null);
                    sslStream.AuthenticateAsClient(IP);
                    if (!sslStream.IsEncrypted)
                        throw new System.Security.SecurityException("Connection is not safe (!encrypted)");
                    stream = sslStream;
                }
                else
                    stream = _tcpclient.GetStream();
                MemoryStream ms = ReadWellCome();
                string wellcome = _encoding.GetString(ms.ToArray());
                wellcome = wellcome.ToLower();
                if (wellcome.IndexOf("rdf") < 0)
                    throw new RdfException(ErrorID.WRONG_SRV, IP, Port);

                bool ok = false;

                if (userKey == null)
                {
                    if ((Read("$system@users@" + user, out ms) >= 0) && (ms.Length > 0))
                    {
                        byte[] b = ms.ToArray();
                        int i = 0;
                        while ((b[i++] != 6) && (i < b.Length)) ;
                        if (i < b.Length)
                        {
                            string login = ReadStr(b, i);

                            i += login.Length + 2;

                            string comment = ReadStr(b, i);

                            i += comment.Length + 2;

                            string pas = ReadStr(b, i);

                            if (pass.Equals(pas))
                                ok = true;
                        }
                    }
                }
                else
                {
                    string userLow = user.ToLowerInvariant();
                    if (Read(userLow, out ms) > 0 && ms.Length > 0)
                    {
                        ok = true;
                    }
                }

                if (!ok)
                    throw new RdfException(ErrorID.WRONG_PASS, IP, Port, user);
                else
                {
                    _server = IP;
                    _Port = Port;
                    _userName = user;
                    _pass = pass;
                    _dbName = "\\\\" + _server + ":" + Port.ToString();
                }
            }
            catch
            {
                Disconnect();
                throw;
            }

        }

        /// <summary>
        /// Gets the last connection list.
        /// </summary>
        /// <returns></returns>
        public static string[] GetLastConnectionList()
        {
            Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\RDF\RDFH_CLIENT\LastConnections");
            if (key == null)
                return new string[] { };

            string[] valNames = key.GetValueNames();
            string[] values = new string[valNames.Length];
            for (int i = 0; i < valNames.Length; i++)
                values[i] = key.GetValue(valNames[i]).ToString();

            return values;
        }

        static bool ParseDBName(string db, out string server, out int Port)
        {
            server = null;
            Port = 0;
            if (!db.StartsWith(@"\\"))
                return false;

            int pos = db.IndexOf(':');
            if (pos < 0)
                return false;

            server = db.Substring(2, pos - 2);
            try
            {
                Port = Convert.ToInt32(db.Substring(pos + 1));
            }
            catch { return false; }

            return true;
        }

        public bool Connect(string user, string pass, string dbName, byte[] userKey)
        { return Connect(user, pass, dbName, userKey, false); }

        public bool Connect(string user, string pass, string dbName, byte[] userKey, bool useSSL)
        {
            if (!ParseDBName(dbName, out _server, out _Port))
                throw new RdfException("�������������� ������ ������ �� �������� ����������");

            Connect(_server, _Port, user, pass, userKey, useSSL);
            return Connected;
        }

        /// <summary>
        /// Connects the specified user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="pass">The pass.</param>
        /// <param name="dbName">Name of the db.</param>
        public bool Connect(string user, string pass, string dbName)
        {
            return Connect(user, pass, dbName, null);
        }

        /// <summary>
        /// Disconnects this instance.
        /// </summary>
        public void Disconnect()
        {
            if (_binded)
                Unbind();

            if (Connected)
            {
                if (stream != null)
                {
                    stream.Close();
                    stream = null;
                }
                _tcpclient.Close();
            }
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="RdfConnector"/> is reclaimed by garbage collection.
        /// </summary>
        ~RdfConnector()
        {
            Disconnect();
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="RdfConnector"/> is connected.
        /// </summary>
        /// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
        public bool Connected
        {
            get
            {
                return stream != null && _tcpclient.Connected;
            }
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string[] GetList(string key)
        {
            string[] list = new string[0];
            MemoryStream ms = new MemoryStream();
            int rs = SendMessage(new CommandRDF(1, RDFConst.ID_GetList, 1, key.Length, 0), key, null, out ms);

            if ((rs <= 0) || (ms.Length <= 0))
                return list;
            try
            {
                ms.Seek(0, SeekOrigin.Begin);
                Int32 count = ConvertEx.GetInt32(ms);
                if ((count > 0) && (count < 1000000))
                {
                    list = new string[count];

                    for (int i = 0; i < count; i++)
                        list[i] = ConvertEx.ReadStrUI16(ms, RdfConnector.encoding);
                }
            }
            catch(Exception ex)
            {
#if DEBUG
                throw new Exception("������ ������ RDF", ex);
#endif
            }
            return list;
        }

        /// <summary>
        /// Determines whether [is key exist] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        /// 	<c>true</c> if [is key exist] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsKeyExist(string key)
        {
            CommandRDF com = new CommandRDF(1, RDFConst.ID_KEYEXISTS, 1, key.Length, 0);
            MemoryStream stream;
            Int32 rs = SendMessage(com, key, null, out stream);
            return (rs == 1);
        }

        public bool DeleteKey(string key)
        {
            CommandRDF com = new CommandRDF(1, RDFConst.ID_DELETE_KEY, 1, key.Length, 0);
            MemoryStream stream;
            Int32 rs = SendMessage(com, key, null, out stream);
            return rs == 1;
        }

        /// <summary>
        /// Determines whether [has sub keys] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        /// 	<c>true</c> if [has sub keys] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasSubKeys(string key)
        {
            CommandRDF com = new CommandRDF(1, RDFConst.ID_KEYEXISTS, 1, key.Length, 0);
            MemoryStream stream;
            Int32 rs = SendMessage(com, key, null, out stream);
            return (rs == 1);
        }

        public void ServerLog(string eventInfo, string msg)
        {
            MemoryStream ms = new MemoryStream(msg.Length + 2);
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write((UInt16)msg.Length);
            bw.Write(_encoding.GetBytes(msg));
            CommandRDF com = new CommandRDF(1, RDFConst.ID_WRITELOG, 1, eventInfo.Length, (int)ms.Length);
            Int32 rs = SendMessage(com, eventInfo, ms.ToArray(), out ms);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            return SendSimpleCommand(RDFConst.ID_SAVE);
        }

        /// <summary>
        /// Writes the binary.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="bytes">The bytes.</param>
        /// <returns></returns>
        public bool WriteBinary(string key, byte[] bytes)
        {
            MemoryStream stream = new MemoryStream(bytes);
            return WriteBinary(key, stream, false);
        }

        /// <summary>
        /// Reads the binary.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="bytes">The bytes.</param>
        /// <returns></returns>
        public bool ReadBinary(string key, out byte[] bytes)
        {
            MemoryStream ms = ReadBinary(key, false);
            bytes = null;
            if (ms == null)
                return false;
            ms.Seek(0, SeekOrigin.Begin);
            bytes = ms.ToArray();
            return true;
        }

        /// <summary>
        /// Reads the binary.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public System.IO.MemoryStream ReadBinary(string key, bool packed)
        {
            MemoryStream ms = new MemoryStream();
            Int32 rs = Read(key, out ms);
            if (rs > 0)
            {
                if(!packed) 
                    return ms;

                MemoryStream mem = new MemoryStream();
                ms.Seek(0, SeekOrigin.Begin);
                mem = ConvertEx.UnPackStream<MemoryStream>(ms);
                mem.Seek(0, SeekOrigin.Begin);
                return mem;
            }

            return null;
        }

        public bool WriteBinary(string key, System.IO.Stream stream, bool pack)
        {
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            MemoryStream ms = new MemoryStream();
            if (pack)
            {
                ms = ConvertEx.PackStream(stream);
            }

            else ConvertEx.CopyStream(stream, ms);

            CommandRDF com = new CommandRDF(1, RDFConst.ID_WRITEBUF, 1, key.Length, (Int32)(ms.Length));
            MemoryStream resMemS;
            Int32 rs = SendMessage(com, key, ms.ToArray(), out resMemS);

            return (rs == 1);
        }

        public bool CreateKey(string key)
        {
            CommandRDF com = new CommandRDF(1, RDFConst.ID_CREATEKEY, 1, key.Length, 0);
            MemoryStream msRes;
            int rs = SendMessage(com, key, null, out msRes);
            return rs == 1;
        }
    }

    static class RDFConst
    {
        public const string WellcomeToRDF = "welcome to rdf server..\r\n";
        public const int ID_GetList = 100 + 1;
        public const int ID_KEYEXISTS = 100 - 37;
        public const int ID_HASSUBKEY = 100 - 41;
        public const int ID_READ = 100 + 2;
        //int ID_READMSB   = 100+3;
        public const int ID_READKEY = 100 + 7;
        //public int ID_READPACKKEY = 100 + 8;
        //int ID_WRITEMSB     = 100-36;
        //int ID_MOVEKEY      = 100-38;
        public const int ID_READBUF = 100 + 5;
        //public int ID_READMSBT = 100 + 4;
        //public int ID_READINFO = 100 + 6;
        //public int ID_WRITE = 100 - 1;
        public const int ID_WRITEBUF = 100 - 40;
        public const int ID_SAVE = 100 - 2;
        public const int ID_DELETE_KEY = 100 - 3;
        public const int ID_CREATEKEY = 100 - 39;
        //public int ID_GETKEYSTATE = 100 + 9;
        //int ID_BEGINWRITEFILE  = ;
        //int ID_CONTINUEWRITEFILE: WinWriteFile(ID_CONTINUEWRITEFILE,client);
        //int ID_EXTRACTDAMP      : dbExtractDamp(client);
        //int ID_EXECUTEFILE      : WinExecuteFile(client);
        //nt ID_GETSERVERSTREE   : dbGetServersTree(client);
        //int ID_UPDATECHILD      : dbUpdateChildServers(client);
        //int ID_QUERYUPDATE      : dbQueryUpdateByParent(client);
        //int ID_STOPUPDATE       : dbStopUpdateByParent(client);
        public const int ID_BINDTIMER = 100 - 52;
        public const int ID_UNBINDTIMER = 100 - 53;
        //public int ID_PACK_STARTED = 100 - 34;
        //public int ID_PACK_FINISHED = 100 - 35;
        public const int ID_WRITELOG = 100 - 54;
    }

    class CommandRDF
    {
        public Int32 cmd = 0;
        public Int32 id = 0;
        public Int32 State = 0;
        public Int32 KeyLen = 0;
        public Int32 DataLen = 0;

        public CommandRDF(Int32 _cmd, Int32 _id, Int32 _State, Int32 _KeyLen, Int32 _DataLen)
        {
            cmd = _cmd;
            id = _id;
            State = _State;
            if (_KeyLen > 0) KeyLen = _KeyLen + 2;
            else KeyLen = 0;
            DataLen = _DataLen;
        }
        public CommandRDF()
        {

        }

        public static Int32 LengthPacketFromServer(CommandRDF command)
        {
            return SIZE_STRUCT + command.DataLen;
        }

        public void WriteCommand(System.IO.MemoryStream ms)
        {
            ConvertEx.WriteToMs(ms, BitConverter.GetBytes(cmd));
            ConvertEx.WriteToMs(ms, BitConverter.GetBytes(id));
            ConvertEx.WriteToMs(ms, BitConverter.GetBytes(State));
            ConvertEx.WriteToMs(ms, BitConverter.GetBytes(KeyLen));
            ConvertEx.WriteToMs(ms, BitConverter.GetBytes(DataLen));
        }

        public const int SIZE_STRUCT = 20;

        public static CommandRDF Extract(byte[] b)
        {
            if (b.Length < SIZE_STRUCT)
                return null;
            CommandRDF com = new CommandRDF();
            com.cmd = BitConverter.ToInt32(b, 0);
            com.id = BitConverter.ToInt32(b, 4);
            com.State = BitConverter.ToInt32(b, 8);
            com.KeyLen = BitConverter.ToInt32(b, 12) + 2;// + 2 ��� ������ 2 �����
            com.DataLen = BitConverter.ToInt32(b, 16);
            return com;
        }
    }

    public enum ErrorID { NONE, READ_ERROR, WRITE_ERROR, WRONG_PASS, WRONG_SRV, KEY_NOT_EXISTS };

    public class RdfException : Exception
    {
        private ErrorID _errorID;

        public ErrorID RDFErrorID { get { return _errorID; } }

        public RdfException() { }
        public RdfException(ErrorID error, params object[] Params) : base(getErrorMes(error, Params)) { _errorID = error; }
        public RdfException(string message) : base(message) { }
        public RdfException(string message, Exception inner) : base(message, inner) { }
        protected RdfException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

        private static string getErrorMes(ErrorID er, params object[] Params)
        {
            String IP = "server not defined in exception";
            if ((Params.Length > 0) && (IP != null)) IP = (string)Params[0];
            string Port = "port not defined in exception";
            if ((Params.Length > 1) && (Params[1] != null)) Port = ((Int32)Params[1]).ToString();
            String user = "user not defined in exception";
            if ((Params.Length > 2) && (Params[2] != null)) user = (string)Params[2];
            String Key = "key not defined in exception";
            if ((Params.Length > 3) && (Params[3] != null)) Key = (string)Params[3];

            string prefix = "RDF: ";

            switch (er)
            {
                case ErrorID.NONE:
                    return prefix + IP;

                case ErrorID.READ_ERROR:
                    return prefix + "������ ��� ������ ������� �� ����� " + Key + " ;�� " + IP + ":" + Port;

                case ErrorID.WRITE_ERROR:
                    return prefix + "������ ��� ������ ������� �� ����� " + Key + " ;�� " + IP + ":" + Port;

                case ErrorID.WRONG_PASS:
                    return prefix + "������������ ������������ ��� ������: " + user + "@" + IP + ":" + Port;

                case ErrorID.WRONG_SRV:
                    return prefix + IP + ":" + Port + " �� �������� RDF-��������";

                case ErrorID.KEY_NOT_EXISTS:
                    return prefix + "���� �� ���������� " + Key + " � " + IP + ":" + Port;

                default: return prefix + "����������� ������ � rdfClient.dll " + er.ToString();
            }
        }
    }
}



