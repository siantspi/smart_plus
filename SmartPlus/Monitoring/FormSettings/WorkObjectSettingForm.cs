﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public partial class WorkObjectSettingForm : Form
    {
        MainForm MainForm;
        C2DLayer Layer;
        BaseObj Object;
        Palette palette;
        Font StatParamNameFont, StatValueFont;
        ComboBoxExColor MarkerColorComboBox;
        ComboBoxExColor ContourColorComboBox;
        ToolTip toolTip, balloonTip;
        List<WorkObjectOperations> CurrentOps;
        BackgroundWorker worker;
        WorkObjectOperations CurrentWork;

        int ImageXlastSelection, ImageYlastSelection;

        public WorkObjectSettingForm(MainForm MainForm)
        {
            Object = null;
            
            InitializeComponent();
            this.MainForm = MainForm;
            this.Owner = MainForm;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(MainForm.Location.X + MainForm.ClientRectangle.Width / 2 - this.Width / 2, MainForm.Location.Y + MainForm.ClientRectangle.Height / 2 - this.Height / 2);
            CurrentWork = WorkObjectOperations.None;
            StatParamNameFont = new Font("Tahoma", 8.25f, FontStyle.Bold);
            StatValueFont = new Font("Tahoma", 8.25f);
            this.FormClosing += new FormClosingEventHandler(ObjectSettingForm_FormClosing);

            CurrentOps = new List<WorkObjectOperations>();
            WorkComment.Text = string.Empty;

            // Worker
            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);

            // Marker
            MarkerColorComboBox = new ComboBoxExColor();
            MarkerColorComboBox.Location = new System.Drawing.Point(96, 13);
            MarkerColorComboBox.SelectedColor = System.Drawing.Color.White;
            MarkerColorComboBox.Size = new System.Drawing.Size(213, 24);
            MarkerDrawGroup.Controls.Add(MarkerColorComboBox);
            MarkerUsePalette.CheckedChanged += new EventHandler(MarkerUsePalette_CheckedChanged);
            MarkerPaletteButton.Click += new EventHandler(PaletteEdit);

            // WellList
            WLLevelNGDU.CheckedChanged +=new EventHandler(WLLevelNGDU_CheckedChanged);
            WLLevelOilfield.CheckedChanged +=new EventHandler(WLLevelOilfield_CheckedChanged);
            WLLevelArea.CheckedChanged +=new EventHandler(WLLevelArea_CheckedChanged);
            WLLevelSkv.CheckedChanged +=new EventHandler(WLLevelSkv_CheckedChanged);

            // Contour
            ContourColorComboBox = new ComboBoxExColor();
            ContourColorComboBox.Location = new Point(99, 19);
            ContourColorComboBox.Size = new Size(200, 23);
            ContourDrawingGroup.Controls.Add(ContourColorComboBox);
            CntrOpNNTLayerBox.ReadOnly = true;
            CntrOpPVTLayerBox.ReadOnly = true;

            // Tool Tips
            toolTip = new ToolTip();
            toolTip.SetToolTip(CntrOpNIZGridLoad, "Загрузить из рабочей папки");
            toolTip.SetToolTip(CntrOpNIZPVTLoad, "Загрузить из рабочей папки");

            balloonTip = new ToolTip();
            balloonTip.IsBalloon = true;
            balloonTip.ToolTipIcon = ToolTipIcon.Info;

            // Image
            ImageXlastSelection = 0;
            ImageYlastSelection = 0;

            // Grid
            GridPaletteEdit.Click += new EventHandler(PaletteEdit);
            Tabs.SelectedIndexChanged += new EventHandler(Tabs_SelectedIndexChanged);
            this.LocationChanged += new EventHandler(ObjectSettingForm_LocationChanged);
        }

        // Worker
        //
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (CurrentWork)
            {
                case WorkObjectOperations.ContourCalcNIZ:
                    object[] obj = (object[])e.Argument;
                    if(obj.Length == 3)
                    {
                        C2DLayer layer = (C2DLayer)obj[0];
                        Grid grid = (Grid)obj[1];
                        PVTWorkObject pvt = (PVTWorkObject)obj[2];
                        CntrOpNIZ(worker, e, layer, grid, pvt);
                    }
                    break;
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WorkComment.Text = string.Empty;
            CalcButton.Enabled = true;
            if (!e.Cancelled)
            {
                switch (CurrentWork)
                {
                    case WorkObjectOperations.ContourCalcNIZ:
                        CntrOpNizComplete(e.Result);
                        break;
                }
            }
            CurrentWork = WorkObjectOperations.None;
        }

        // Form Func
        //
        void ObjectSettingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (worker != null && worker.IsBusy) worker.CancelAsync();
            if (balloonTip != null)
            {
                balloonTip.Dispose();
                balloonTip = null;
            }
            if ((this.DialogResult == DialogResult.OK) && (Object != null))
            {
                bool change = false;
                switch (Object.TypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.MARKER:
                        if (TestChange((Marker)Object))
                        {
                            SaveSettings((Marker)Object);
                            change = true;
                        }
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.GRID:
                        if (TestChange((Grid)Object))
                        {
                            SaveSettings((Grid)Object);
                            change = true;
                        }
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                        if (TestChange((Image)Object))
                        {
                            SaveSettings((Image)Object);
                            change = true;
                        }
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.LAYER:
                        if (((C2DLayer)this.Object).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL &&
                            TestChange(Layer))
                        {
                            SaveSettings(Layer);
                            change = true;
                        }
                        else if (((C2DLayer)this.Object).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR &&
                            TestChangeContour(Layer))
                        {
                            SaveSettingsContour(Layer);
                            change = true;
                        }
                        break;
                }
                if (change && (MainForm != null) && (MainForm.canv != null) && (Layer != null))
                {
                    if (Object.TypeID != Constant.BASE_OBJ_TYPES_ID.IMAGE)
                    {
                        MainForm.canv.WorkLayerActive = Layer;
                        MainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER);
                    }
                    else
                    {
                        MainForm.canv.WriteWorkLayer(Layer);
                    }
                    MainForm.canv.DrawLayerList();
                    if (Object.TypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                    {
                        MainForm.CorSchReloadMarkers();
                    }
                }
            }
        }
        void ObjectSettingForm_LocationChanged(object sender, EventArgs e)
        {
            ClearBallonTip();
        }
        public void Show(BaseObj Object)
        {
            MarkerPanel.Visible = false;
            WellListPanel.Visible = false;
            ContourPanel.Visible = false;
            GridPanel.Visible = false;
            ImagePanel.Visible = false;
            CntrOpsPage.Parent = null;
            
            Layer = null;
            switch (Object.TypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.LAYER:
                    Layer = (C2DLayer)Object;
                    FillCurrentOpsByObj(Layer.ObjTypeID);
                    switch (((C2DLayer)Object).ObjTypeID)
                    {
                        case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                            Deposit dep;
                            for (int i = 0; i < Layer.ObjectsList.Count; i++)
                            {
                                dep = (Deposit)Layer.ObjectsList[i]; 
                                if (dep.Selected)
                                {
                                    this.Object = dep;
                                    LoadSettings(dep);
                                    break;
                                }
                            }
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.MARKER:
                            this.Object = (Marker)Layer.ObjectsList[0];
                            LoadSettings((Marker)Layer.ObjectsList[0]);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                            this.Object = Object;
                            LoadSettings(Layer);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                            CntrOpsPage.Parent = Tabs;
                            this.Object = Layer;
                            LoadSettingsContour(Layer);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                            this.Object = (Image)Layer.ObjectsList[0];
                            LoadSettings((Image)this.Object);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.GRID:
                            this.Object = (Grid)Layer.ObjectsList[0];
                            LoadSettings((Grid)this.Object);
                            break;
                    }
                    break;
            }
            LoadStatistics();
            this.Show();
        }

        // Layer
        //
        public void RemoveLayer(C2DLayer Layer)
        {
            if (this.Layer == Layer)
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
            if (CntrOpNNTLayerBox.Tag == Layer) CntrOpNNTLayerBox.Clear();
            if (CntrOpPVTLayerBox.Tag == Layer) CntrOpPVTLayerBox.Clear();
        }

        // Tool Tip
        //
        void ShowBallonToolTip(int x, int y, string Text)
        {
            if (balloonTip != null) balloonTip.Dispose();
            balloonTip = new ToolTip();
            balloonTip.ToolTipTitle = "Внимание!";
            balloonTip.ToolTipIcon = ToolTipIcon.Info;
            balloonTip.UseFading = true;
            balloonTip.UseAnimation = true;
            balloonTip.IsBalloon = true;
            balloonTip.Show(Text, this, x, y, 1500);
        }
        void ClearBallonTip()
        {
            if (balloonTip != null)
            {
                balloonTip.Dispose();
                balloonTip = null;
            }
        }

        // Tabs
        //
        void Tabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearBallonTip();
        }

        // Statistics TAB
        //
        void AddStatParamName(string ParamName)
        {
            StatBox.SelectionFont = StatParamNameFont;
            StatBox.AppendText(ParamName);
        }
        void AddStatValue(string StatValue)
        {
            StatBox.SelectionFont = StatValueFont;
            StatBox.AppendText(StatValue + "\n");
        }
        public void LoadStatistics()
        {
            StatBox.Clear();
            if (Object != null)
            {
                switch (Object.TypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                        Deposit dep = (Deposit)Object;
                        //AddStatParamName("Количество скважин: ");
                        //AddStatValue(string.Format("{0} шт.", (dep.WellList != null) ? dep.WellList.Length : 0));
                        //string str = string.Empty;
                        //if(dep.WellList != null)
                        //{
                        //    for (int i = 0; i < dep.WellList.Length;i++)
                        //    {
                        //        if (str.Length > 0) str += ", ";
                        //        str += dep.WellList[i].Name;
                        //    }
                        //    AddStatParamName("Имена скважин: ");
                        //    AddStatValue(str);
                        //}
                        AddStatParamName("Площадь залежи: ");
                        AddStatValue(string.Format("{0:0.###} м2", dep.Params.Square));
                        AddStatParamName("Начальная нефтенас.толщина: ");
                        AddStatValue(string.Format("{0:0.###} м", dep.Params.InitHoil));
                        AddStatParamName("Начальные балансовае запасы: ");
                        AddStatValue(string.Format("{0:0.###} тыс.т.", dep.Params.InitGeoOilReserv / 1000));
                        AddStatParamName("Начальные извлекаемые запасы: ");
                        AddStatValue(string.Format("{0:0.###} тыс.т.", dep.Params.InitGeoOilReserv * dep.pvt.KIN / 1000));
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.MARKER:
                        Marker mrk = (Marker)Object;
                        AddStatParamName("Количество скважин: ");
                        AddStatValue(string.Format("{0} шт.", mrk.Count));
                        if (mrk.MinDepth < mrk.MaxDepth)
                        {
                            AddStatParamName("Минимальная глубина: ");
                            AddStatValue(string.Format("{0} м.", mrk.MinDepth));
                            AddStatParamName("Максимальная глубина: ");
                            AddStatValue(string.Format("{0} м.", mrk.MaxDepth));
                        }
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.GRID:
                        Grid grid = (Grid)Object;
                        AddStatParamName("Шаг сетки: ");
                        AddStatValue(string.Format("{0}x{1}", grid.dx, grid.dy));
                        AddStatParamName("Размеры Xmin, Xmax: ");
                        AddStatValue(string.Format("{0:0.##}, {1:0.##}", grid.Xmin, grid.Xmax));
                        AddStatParamName("Размеры Ymin, Ymax: ");
                        AddStatValue(string.Format("{0:0.##}, {1:0.##}", grid.Ymin, grid.Ymax));
                        AddStatParamName("Размеры: Zmin, Zmax: ");
                        AddStatValue(string.Format("{0:0.##}, {1:0.##}", grid.Zmin, grid.Zmax));
                        AddStatParamName("Значение бланка: ");
                        AddStatValue(string.Format("{0:0.##}", grid.BlankVal));
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                        Image img = (Image)Object;
                        AddStatParamName("Разрешение: ");
                        AddStatValue(string.Format("{0}x{1}", img.xDpi, img.yDpi));
                        //AddStatParamName("Координаты : "); 
                        //AddStatValue(string.Format("{0:0.##}, {1:0.##}", img.X, img.Y));
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.LAYER:
                        switch (Layer.ObjTypeID)
                        {
                            case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                                AddStatParamName("Количество скважин: ");
                                AddStatValue(string.Format("{0} шт.", Layer.ObjectsList.Count));
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                                C2DLayer layer = (C2DLayer)Object;
                                if (layer.ObjectsList.Count > 0)
                                {
                                    AddStatParamName("Количество внутренних контуров: ");
                                    AddStatValue(string.Format("{0} шт.", layer.ObjectsList.Count));
                                }
                                Contour cntr;
                                int sumPoints = 0, k;
                                double sumSquare = 0, sumPerimeter = 0;
                                for (int i = 0; i < layer.ObjectsList.Count; i++)
                                {
                                    cntr = (Contour)layer.ObjectsList[i];
                                    sumPoints += cntr._points.Count;
                                    k = (cntr.GetClockWiseByX(true)) ? 1 : -1;
                                    if (layer.ObjectsList.Count == 1) k = 1;
                                    sumSquare += k * cntr._points.GetSquare();
                                    sumPerimeter += cntr.GetPerimeter();
                                }
                                
                                AddStatParamName("Количество точек: ");
                                AddStatValue(string.Format("{0} шт.", sumPoints));
                                AddStatParamName("Площадь контура: ");
                                AddStatValue(string.Format("{0:#,##0.##} м2.[Метод трапеций]", sumSquare));
                                AddStatParamName("Длина контура: ");
                                AddStatValue(string.Format("{0:#,##0.##} м.", sumPerimeter));
                                break;
                        }
                        break;
                }
            }
        }

        // Min Max Object
        PointD GetMinMax()
        {
            PointD res = new PointD();
            res.X = 0; res.Y = 0;
            if (Object != null)
            {
                switch (Object.TypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.MARKER:
                        res.X = ((Marker)Object).MinDepth;
                        res.Y = ((Marker)Object).MaxDepth;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.GRID:
                        res.X = ((Grid)Object).Zmin;
                        res.Y = ((Grid)Object).Zmax;
                        break;
                }
            }
            return res;
        }
        // Deposit
        //
        public void LoadSettings(Deposit dep)
        {
            this.Text = string.Format("Свойства - Залежь [{0}]", dep.Name);
            Tabs.SelectedIndex = 1;
        }

        // Marker
        //
        public void LoadSettings(Marker mrk)
        {
            this.Text = string.Format("Настройки - Маркер [{0}]", mrk.Name);
            MarkerPanel.Visible = true;
            MarkerUsePalette.Checked = mrk.UsePalette;
            MarkerPaletteButton.Enabled = mrk.UsePalette;
            MarkerColorComboBox.SelectedColor = mrk.PenBound.Color;
            if (mrk.UsePalette)
            {
                this.palette = mrk.palette;
            }
        }
        public void SaveSettings(Marker mrk)
        {
            mrk.UsePalette = MarkerUsePalette.Checked;
            if (mrk.PenBound.Color != MarkerColorComboBox.SelectedColor)
            {
                mrk.PenBound = new Pen(MarkerColorComboBox.SelectedColor, 7F);
            }
            if (mrk.UsePalette)
            {
                mrk.palette = this.palette;
            }
        }
        public bool TestChange(Marker mrk)
        {
            return ((MarkerUsePalette.Checked != mrk.UsePalette) ||
                    (MarkerColorComboBox.SelectedColor.ToArgb() != mrk.PenBound.Color.ToArgb()) ||
                    (MarkerUsePalette.Checked && !this.palette.Equals(mrk.palette)));
        }
        void MarkerUsePalette_CheckedChanged(object sender, EventArgs e)
        {
            if ((this.palette == null) && (Object != null) && (MarkerUsePalette.Checked))
            {
                Marker mrk = (Marker)Object;
                if (mrk.palette == null)
                {
                    mrk.DefaultPalette();
                    this.palette = mrk.palette;
                    mrk.palette = null;
                }
                else
                {
                    this.palette = mrk.palette;
                }
            }
            MarkerPaletteButton.Enabled = MarkerUsePalette.Checked;
        }
        void PaletteEdit(object sender, EventArgs e)
        {
            if(this.palette != null)
            {
                PointD minMax = GetMinMax();
                PaletteEditorForm editor = new PaletteEditorForm(this.palette, minMax.X, minMax.Y);
                if (editor.ShowDialog(this.Owner) == DialogResult.OK)
                {
                    this.palette = editor.palette;
                }
            }
        }

        // WellList
        //
        public void LoadSettings(C2DLayer wellList)
        {
            this.Text = string.Format("Настройки - Список скважин [{0}]", wellList.Name);
            WellListPanel.Visible = true;
            SetCheckBoxesByLevel(wellList.VisibleLvl.GetVisibleLvl());
            WLDrawName.Checked = wellList.VisibleName;
            WLDrawCaption.Checked = wellList.DrawCaption;
            WLDrawOverBubble.Checked = wellList.DrawOverBubble;
        }
        public void SaveSettings(C2DLayer wellList)
        {
            int level;
            if (WLLevelNGDU.Checked)
            {
                level = 3;
            }
            else
            {
                if (WLLevelOilfield.Checked)
                {
                    level = 2;
                }
                else
                {
                    if (WLLevelArea.Checked)
                    {
                        level = 1;
                    }
                    else
                    {
                        level = 0;
                    }
                }
            }
            wellList.VisibleLvl.SetVisibleLvl(level);
            wellList.VisibleName = WLDrawName.Checked;
            wellList.DrawCaption = WLDrawCaption.Checked;
            wellList.DrawOverBubble = WLDrawOverBubble.Checked;
        }
        public bool TestChange(C2DLayer wellList)
        {
            int level;
            if (WLLevelNGDU.Checked)
            {
                level = 3;
            }
            else
            {
                if (WLLevelOilfield.Checked)
                {
                    level = 2;
                }
                else
                {
                    if (WLLevelArea.Checked)
                    {
                        level = 1;
                    }
                    else
                    {
                        level = 0;
                    }
                }
            }
            return ((level != wellList.VisibleLvl.GetVisibleLvl()) ||
                    (WLDrawName.Checked != wellList.VisibleName) ||
                    (WLDrawCaption.Checked != wellList.DrawCaption) ||
                    (WLDrawOverBubble.Checked != wellList.DrawOverBubble));
        }
        void SetCheckBoxesByLevel(int Level)
        {
            switch (Level)
            {
                case 0:
                    WLLevelNGDU.Checked = false;
                    WLLevelOilfield.Checked = false;
                    WLLevelArea.Checked = false;
                    WLLevelSkv.Checked = true;
                    break;
                case 1:
                    WLLevelNGDU.Checked = false;
                    WLLevelOilfield.Checked = false;
                    WLLevelArea.Checked = true;
                    WLLevelSkv.Checked = true;
                    break;
                case 2:
                    WLLevelNGDU.Checked = false;
                    WLLevelOilfield.Checked = true;
                    WLLevelArea.Checked = true;
                    WLLevelSkv.Checked = true;
                    break;
                case 3:
                    WLLevelNGDU.Checked = true;
                    WLLevelOilfield.Checked = true;
                    WLLevelArea.Checked = true;
                    WLLevelSkv.Checked = true;
                    break;
            }
        }
        private void WLLevelNGDU_CheckedChanged(object sender, EventArgs e)
        {
            if (WLLevelNGDU.Checked)
            {
                WLLevelOilfield.Checked = true;
                WLLevelArea.Checked = true;
            }
        }
        private void WLLevelOilfield_CheckedChanged(object sender, EventArgs e)
        {
            if (WLLevelOilfield.Checked)
            {
                WLLevelArea.Checked = true;
            }
            else
            {
                WLLevelNGDU.Checked = false;
            }

        }
        private void WLLevelArea_CheckedChanged(object sender, EventArgs e)
        {
            if (!WLLevelArea.Checked)
            {
                WLLevelNGDU.Checked = false;
                WLLevelOilfield.Checked = false;
            }
        }
        private void WLLevelSkv_CheckedChanged(object sender, EventArgs e)
        {
            if (!WLLevelArea.Checked)
            {
                WLLevelNGDU.Checked = false;
                WLLevelOilfield.Checked = false;
                WLLevelArea.Checked = false;
            }
        }

        // Contour
        //
        public void LoadSettingsContour(C2DLayer cntrs)
        {
            this.Text = string.Format("Настройки - Контур [{0}]", cntrs.Name);
            ContourPanel.Visible = true;
            if (cntrs.ObjectsList.Count > 0)
            {
                ContourColorComboBox.SelectedColor = ((Contour)cntrs.ObjectsList[0]).LineColor;
            }
            ContourShowNameCheck.Checked = cntrs.VisibleName;
        }
        public void SaveSettingsContour(C2DLayer cntrs)
        {
            for (int i = 0; i < cntrs.ObjectsList.Count; i++)
            {
                ((Contour)cntrs.ObjectsList[i]).LineColor = ContourColorComboBox.SelectedColor;
            }
            cntrs.VisibleName = ContourShowNameCheck.Checked;
        }
        public bool TestChangeContour(C2DLayer cntrs)
        {
            return (cntrs.ObjectsList.Count == 0) || (((Contour)cntrs.ObjectsList[0]).LineColor != ContourColorComboBox.SelectedColor) || (cntrs.VisibleName != ContourShowNameCheck.Checked);
        }

        // GRID
        //
        public void LoadSettings(Grid grid)
        {
            this.Text = string.Format("Настройки - Грид [{0}]", Layer.Name);
            GridPanel.Visible = true;
            GridDrawLines.Checked = grid.DrawGridLines;
            if ((grid.palette == null) || (grid.palette.Count == 0))
            {
                grid.DefaultPalette();
            }
            this.palette = new Palette(grid.palette);
        }
        public void SaveSettings(Grid grid)
        {
            if (!this.palette.Equals(grid.palette))
            {
                grid.UpdatePalette(this.palette);
            }
            grid.DrawGridLines = GridDrawLines.Checked;
        }
        public bool TestChange(Grid grid)
        {
            return ((!grid.palette.Equals(this.palette)) ||
                    (grid.DrawGridLines != GridDrawLines.Checked));
        }

        // IMAGE
        //
        public void LoadSettings(Image img)
        {
            this.Text = string.Format("Настройки - Изображение [{0}]", Layer.Name);
            ImagePanel.Visible = true;
            ImageXdpi.Value = Convert.ToDecimal(img.xDpi);
            ImageYdpi.Value = Convert.ToDecimal(img.yDpi);
        }
        public void SaveSettings(Image img)
        {
            img.xDpi = Convert.ToSingle(ImageXdpi.Value);
            img.yDpi = Convert.ToSingle(ImageYdpi.Value);
        }
        public bool TestChange(Image img)
        {
            return ((Convert.ToSingle(ImageXdpi.Value) != img.xDpi) ||
                    (Convert.ToSingle(ImageYdpi.Value) != img.yDpi));
        }

        // Buttons
        //
        private void OkButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        // Operation List
        //
        private void OpListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearBallonTip();
            if ((OpListBox.SelectedIndex > -1) && (OpListBox.SelectedIndex < CurrentOps.Count))
            {
                SetVisibleOpsGroup(CurrentOps[OpListBox.SelectedIndex]);
            }
        }

        // Calc Operation Button
        //
        private void CalcButton_Click(object sender, EventArgs e)
        {
            if ((OpListBox.SelectedIndex > -1) && (OpListBox.SelectedIndex < CurrentOps.Count))
            {
                CurrentWork = CurrentOps[OpListBox.SelectedIndex];
                bool runTask = false;
                switch (CurrentOps[OpListBox.SelectedIndex])
                {
                    case WorkObjectOperations.ContourCalcNIZ:
                        if ((Layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) && (CntrOpNNTLayerBox.Tag != null) && (CntrOpPVTLayerBox.Tag != null))
                        {
                            object[] obj = new object[3];
                            obj[0] = Layer;
                            ((Grid)((C2DLayer)CntrOpNNTLayerBox.Tag).ObjectsList[0]).Name = ((C2DLayer)CntrOpNNTLayerBox.Tag).Name;
                            obj[1] = (Grid)((C2DLayer)CntrOpNNTLayerBox.Tag).ObjectsList[0];
                            obj[2] = (PVTWorkObject)((C2DLayer)CntrOpPVTLayerBox.Tag).ObjectsList[0];
                            worker.RunWorkerAsync(obj);
                            runTask = true;
                        }
                        break;
                }
                if (runTask)
                {
                    WorkComment.Text = "Выполняется операция.\nПожалуйста подождите...";
                    CalcButton.Enabled = false;
                }
            }
        }

        // Operations
        //
        string GetOperationName(WorkObjectOperations Op)
        {
            switch (Op)
            {
                case WorkObjectOperations.ContourCalcNIZ:
                    return "Расчет запасов по контуру";
            }
            return string.Empty;
        }
        void FillCurrentOpsByObj(Constant.BASE_OBJ_TYPES_ID ObjType)
        {
            CurrentOps.Clear();
            switch (ObjType)
            {
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    CurrentOps.Add(WorkObjectOperations.ContourCalcNIZ);
                    break;
            }
            OpListBox.Items.Clear();
            for (int i = 0; i < CurrentOps.Count; i++)
            {
                OpListBox.Items.Add(GetOperationName(CurrentOps[i]));
            }
            if (OpListBox.Items.Count > 0) OpListBox.SelectedIndex = 0;
        }
        void SetVisibleOpsGroup(WorkObjectOperations Op)
        {
            CntrOpNIZGroup.Visible = false;
            switch (Op)
            {
                case WorkObjectOperations.ContourCalcNIZ:
                    CntrOpNIZGroup.Visible = true;
                    break;
            }
        }
       
        #region Cntr Calc NIZ Operation
        private void CntrOpNIZGridLoad_Click(object sender, EventArgs e)
        {
            bool LayersViewed = false, WorkLayersViewed = false, MestViewed = false;
            C2DLayer layer = null;
            TreeView tw;
            for (int i = 0; i < 2; i++)
            {
                if ((i == 0 && MainForm.canv.LastSelectedTreeView == MainForm.canv.twWork) || (i == 1 && !WorkLayersViewed))
                if (MainForm.canv.twWork.SelectedNodes.Count > 0)
                {
                    TreeNode tn;
                    for (int j = 0; j < MainForm.canv.twWork.SelectedNodes.Count; j++)
                    {
                        tn = MainForm.canv.twWork.SelectedNodes[j];
                        if (tn.Tag != null)
                        {
                            if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                            {
                                layer = (C2DLayer)tn.Tag;
                                break;
                            }
                        }
                    }
                }
                if (layer != null) break;
                tw = MainForm.canv.twLayers;
                if ((i == 0 && MainForm.canv.LastSelectedTreeView == tw) || (i == 1 && !LayersViewed))
                {
                    if ((tw.SelectedNode != null) && (tw.SelectedNode.Tag != null) && (((BaseObj)tw.SelectedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    {
                        if (((C2DLayer)tw.SelectedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID &&
                            ((Grid)(((C2DLayer)tw.SelectedNode.Tag).ObjectsList[0])).GridType == 2)
                        {
                            layer = (C2DLayer)tw.SelectedNode.Tag;
                            break;
                        }
                    }
                }
                tw = MainForm.canv.twActiveOilField;
                if ((i == 0 && MainForm.canv.LastSelectedTreeView == tw) || (i == 1 && !MestViewed))
                {
                    if ((tw.SelectedNode != null) && (tw.SelectedNode.Tag != null) && (((BaseObj)tw.SelectedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    {
                        if (((C2DLayer)tw.SelectedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID &&
                            ((Grid)(((C2DLayer)tw.SelectedNode.Tag).ObjectsList[0])).GridType == 2)
                        {
                            layer = (C2DLayer)tw.SelectedNode.Tag;
                            break;
                        }
                    }
                }
            }
            if (layer != null)
            {
                string tree = string.Empty;
                if (layer.node != null && layer.node.TreeView != null)
                {
                    if (layer.node.TreeView == MainForm.canv.twWork)
                    {
                        tree = " (Рабочая папка)";
                    }
                    else
                    {
                        tree = " (Инспектор)";
                    }
                }
                CntrOpNNTLayerBox.TextBox.Text = layer.Name + tree;
                CntrOpNNTLayerBox.Tag = layer;

            }
            
            if (CntrOpNNTLayerBox.Tag == null)
            {
                int h = this.DesktopBounds.Height - this.ClientRectangle.Height - 6;
                int x = CntrOpNIZGroup.Location.X + CntrOpNIZGridLoad.Location.X;
                int y = CntrOpNIZGroup.Location.Y + CntrOpNIZGridLoad.Location.Y - h;
                ShowBallonToolTip(x + 5, y, "Выделите сетку ННТ в рабочей папке или в Инспекторе");
            }
        }
        private void CntrOpNIZPVTLoad_Click(object sender, EventArgs e)
        {
            if (MainForm.canv.twWork.SelectedNodes.Count > 0)
            {
                C2DLayer layer;
                TreeNode tn;
                for (int i = 0; i < MainForm.canv.twWork.SelectedNodes.Count; i++)
                {
                    tn = MainForm.canv.twWork.SelectedNodes[i];
                    if (tn.Tag != null)
                    {
                        layer = (C2DLayer)tn.Tag;
                        if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT)
                        {
                            CntrOpPVTLayerBox.TextBox.Text = layer.Name;
                            CntrOpPVTLayerBox.Tag = layer;
                            break;
                        }
                    }
                }
            }
            if (CntrOpPVTLayerBox.Tag == null)
            {
                int h = this.DesktopBounds.Height - this.ClientRectangle.Height - 6;
                int x = CntrOpNIZGroup.Location.X + CntrOpNIZPVTLoad.Location.X;
                int y = CntrOpNIZGroup.Location.Y + CntrOpNIZPVTLoad.Location.Y - h;
                ShowBallonToolTip(x + 5, y, "Выделите данные PVT в рабочей папке");
            }
        }
        void CntrOpNIZ(BackgroundWorker worker, DoWorkEventArgs e, C2DLayer Layer, Grid grid, PVTWorkObject pvt)
        {
            e.Result = null;
            if ((Layer != null) && (grid != null) && (pvt != null))
            {
                double Square = 0, SumValues = 0, sq = 0, val = 0;
                int k;
                Contour cntr;
                if(!grid.DataLoaded) 
                {
                    int ind = MainForm.canv.MapProject.GetOFIndex(grid.OilFieldCode);
                    if (ind != -1)
                    {
                        ((OilField)MainForm.canv.MapProject.OilFields[ind]).LoadGridDataFromCacheByIndex(null, null, grid.Index, false);
                    }
                }
                for (int i = 0; i < Layer.ObjectsList.Count; i++)
                {
                    cntr = (Contour)Layer.ObjectsList[i];
                    if ((i == 0) && (Layer.ObjectsList.Count == 1) && (!cntr.GetClockWiseByX(false)))
                    {
                        cntr = new Contour(cntr);
                        cntr.Invert();
                    }
                    grid.CalcSumValues(worker, e, cntr, out sq, out val);
                    k = (cntr.GetClockWiseByX(true)) ? 1 : -1;
                    Square += k * sq;
                    SumValues += k * val;
                }
                object[] result = new object[10];
                double x = pvt.Item.OilDensity * pvt.Item.Porosity * pvt.Item.OilInitialSaturation / pvt.Item.OilVolumeFactor;
                result[0] = x * SumValues / 1000;
                result[1] = x * SumValues * pvt.Item.KIN / 1000;
                result[2] = Square;
                result[3] = SumValues;
                result[4] = Layer.Name;
                result[5] = grid.Name;
                result[6] = pvt.Name;
                string comment = "Интегрирование сетки производится приблизительным методом (метод палетки).";
                if (pvt.Item.StratumCode != grid.StratumCode)
                {
                    var dict = (StratumDictionary)MainForm.GetLastProject().DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                    StratumTreeNode node = dict.GetStratumTreeNode(pvt.Item.StratumCode);
                    StratumTreeNode node2 = dict.GetStratumTreeNode(grid.StratumCode);
                    if ((node.GetParentLevelByCode(grid.StratumCode) == -1) && (node2.GetParentLevelByCode(pvt.Item.StratumCode) == -1))
                    {
                        comment += string.Format("\nВнимание! Пласт PVT не соотвествует пласту сетки [{0} - {1}]", node.Name, node2.Name);
                    }
                }
                result[7] = comment;
                e.Result = result;
            }
        }
        void CntrOpNizComplete(object Result)
        {
            if (Result != null)
            {
                object[] result = (object[])Result;
                if (result.Length == 10)
                {
                    OutputTextForm form = MainForm.OutBox;
                    if (form.CountRows > 0) form.Clear();
                    form.AddBoldText("Операция: "); form.AddText(GetOperationName(CurrentWork)); form.NewLine();
                    form.AddBoldText("Контур: "); form.AddText((string)result[4]); form.NewLine();
                    form.AddBoldText("Сетка: "); form.AddText((string)result[5]); form.NewLine();
                    form.AddBoldText("PVT: "); form.AddText((string)result[6]); form.NewLine();
                    form.NewLine();
                    form.AddBoldText("Начальные балансовые запасы: "); form.AddText(string.Format("{0:#,##0.###} тыс.т.", (double)result[0])); form.NewLine();
                    form.AddBoldText("Начальные извлекаемые запасы: "); form.AddText(string.Format("{0:#,##0.###} тыс.т.", (double)result[1])); form.NewLine();
                    form.AddBoldText("Площадь сетки по контуру: "); form.AddText(string.Format("{0:#,##0.###} м2", (double)result[2])); form.NewLine();
                    form.AddBoldText("Значение по контуру: "); form.AddText(string.Format("{0:#,##0.###}", (double)result[3])); form.NewLine();
                    if (((string)result[7]).Length > 0)
                    {
                        form.NewLine();
                        form.AddBoldText("Комментарий:\n");
                        form.AddText((string)result[7]);
                    }
                    form.Text = "Расчет запасов по контуру";
                    form.ShowDialog();
                }
            }
        }
        #endregion

        private void ImageXdpi_MouseDown(object sender, MouseEventArgs e)
        {
            TextBoxBase textBox = ImageXdpi.Controls[1] as TextBoxBase;
            if (textBox != null)
            {
                if (textBox.Bounds.Contains(e.Location))
                {
                    int pos = textBox.Text.IndexOf('.');
                    if (textBox.SelectionStart <= pos)
                    {
                        ImageXdpi.Increment = 1;
                    }
                    else
                    {
                        int k = textBox.SelectionStart - pos;
                        if (k < 0) k = 0; else if (k > ImageXdpi.DecimalPlaces) k = ImageXdpi.DecimalPlaces;
                        ImageXdpi.Increment = new Decimal(Math.Pow(0.1, k));
                    }
                    ImageXlastSelection = textBox.SelectionStart;
                }
                else
                {
                    textBox.SelectionStart = ImageXlastSelection;
                }
            }
        }

        private void ImageYdpi_MouseDown(object sender, MouseEventArgs e)
        {
            TextBoxBase textBox = ImageYdpi.Controls[1] as TextBoxBase;
            if (textBox != null)
            {
                if (textBox.Bounds.Contains(e.Location))
                {
                    int pos = textBox.Text.IndexOf('.');
                    if (textBox.SelectionStart <= pos)
                    {
                        ImageYdpi.Increment = 1;
                    }
                    else
                    {
                        int k = textBox.SelectionStart - pos;
                        if (k < 0) k = 0; else if (k > ImageYdpi.DecimalPlaces) k = ImageYdpi.DecimalPlaces;
                        ImageYdpi.Increment = new Decimal(Math.Pow(0.1, k));
                    }
                    ImageYlastSelection = textBox.SelectionStart;
                }
                else
                {
                    textBox.SelectionStart = ImageYlastSelection;
                }
            }
        }
    }

    public enum WorkObjectOperations
    {
        None = -1,
        ContourCalcNIZ = 0
    }
}

