﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class CorSchemeSettingsForm : Form
    {
        ComboBoxExColor LogLineColor, LogBackColor;
        ComboBoxLineType LogLineType;
        int SelColIndex, SelObjIndex;
        MainForm MainForm;
        TreeNode SelectedNode;
        CorrelationScheme CorScheme;
        Plane Scheme, SavedScheme;

        public CorSchemeSettingsForm(MainForm MainForm)
        {
            InitializeComponent();
            LogLineColor = new ComboBoxExColor();
            DrawGroup.Controls.Add(LogLineColor);
            LogLineColor.Location = new Point(85, 51);
            LogLineColor.Size = new Size(196, 23);

            LogLineType = new ComboBoxLineType();
            DrawGroup.Controls.Add(LogLineType);
            LogLineType.Location = new Point(85, 84);
            LogLineType.Size = new Size(196, 23);

            LogBackColor = new ComboBoxExColor();
            SpottingGroup.Controls.Add(LogBackColor);
            LogBackColor.Enabled = false;
            LogBackColor.Location = new Point(47, 71);
            LogBackColor.Size = new Size(182, 23);

            this.MainForm = MainForm;
            this.Owner = MainForm;
            this.ColumnsTree.ExpandAll();
            ColumnsTree.SelectedNode = ColumnsTree.Nodes[0];
            SelectedNode = null;

            this.FormClosing += new FormClosingEventHandler(CorSchemeSettingsForm_FormClosing);
            
            // Columms tree
            ColumnsTree.AfterSelect += new TreeViewEventHandler(ColumnsTree_AfterSelect);

            // Depth Settings
            DepthColMinUse.CheckedChanged += new EventHandler(DepthColMinUse_CheckedChanged);
            DepthColMaxUse.CheckedChanged += new EventHandler(DepthColMaxUse_CheckedChanged);
            DepthColMax.ValueChanged += new EventHandler(DepthColMax_ValueChanged);
            DepthColMin.ValueChanged += new EventHandler(DepthColMin_ValueChanged);

            // Gis
            GisColMaxUse.CheckedChanged += new EventHandler(GisColMaxUse_CheckedChanged);
            GisColMinUse.CheckedChanged += new EventHandler(GisColMinUse_CheckedChanged);
            GisColMin.ValueChanged += new EventHandler(GisColMin_ValueChanged);
            GisColMax.ValueChanged += new EventHandler(GisColMax_ValueChanged);

            // Perforation
            PerfColMinUse.CheckedChanged += new EventHandler(PerfColMinUse_CheckedChanged);
            PerfColMaxUse.CheckedChanged += new EventHandler(PerfColMaxUse_CheckedChanged);
            PerfColMin.ValueChanged += new EventHandler(PerfColMin_ValueChanged);
            PerfColMax.ValueChanged += new EventHandler(PerfColMax_ValueChanged);

            // Log Settings
            LogScaleMinMaxAuto.CheckedChanged += new EventHandler(ScaleMinMax_CheckedChanged);
            LogScaleMinMaxManual.CheckedChanged += new EventHandler(ScaleMinMax_CheckedChanged);
            LogScaleMinMaxManualByStep.CheckedChanged += new EventHandler(ScaleMinMaxManualByStep_CheckedChanged);
            LogScaleMin.ValueChanged += new EventHandler(LogScaleMin_ValueChanged);
            LogScaleMax.ValueChanged += new EventHandler(LogScaleMax_ValueChanged);
            LogBackUse.CheckedChanged += new EventHandler(LogBackUse_CheckedChanged);
            LogColumnMinUse.CheckedChanged += new EventHandler(LogColumnMinUse_CheckedChanged);
            LogColumnMaxUse.CheckedChanged += new EventHandler(LogColumnMaxUse_CheckedChanged);
            LogScaleType.SelectedIndexChanged += new EventHandler(LogScaleType_SelectedIndexChanged);
            LogColumnMin.ValueChanged += new EventHandler(LogColumnMin_ValueChanged);
            LogColumnMax.ValueChanged += new EventHandler(LogColumnMax_ValueChanged);

            // Obj GIS Settings
            ObjGisMinUse.CheckedChanged += new EventHandler(ObjGisMinUse_CheckedChanged);
            ObjGisMaxUse.CheckedChanged += new EventHandler(ObjGisMaxUse_CheckedChanged);
            ObjGisMax.ValueChanged += new EventHandler(ObjGisMax_ValueChanged);
            ObjGisMin.ValueChanged += new EventHandler(ObjGisMin_ValueChanged);
        }

        void LogScaleMax_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxValues(LogScaleMin, LogScaleMax);
        }
        void LogScaleMin_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxValues(LogScaleMin, LogScaleMax);
        }

        // EVENTS
        void CorSchemeSettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.Cancel)
            {
                CorScheme.BaseScheme = SavedScheme;
                CorScheme.UpdatePlanesByBaseScheme();
            }
            else if (this.DialogResult == DialogResult.OK)
            {
                MainForm.fAppSettings.WriteCorSchemeBaseToAppSettings(MainForm.baseSchemes);
            }
        }
        
        // SHOW
        public void ShowForm(CorrelationScheme CorScheme)
        {
            this.CorScheme = CorScheme;
            this.Scheme = CorScheme.BaseScheme;
            SavedScheme = CorScheme.BaseScheme.Clone();
            ShowMarkersConnection.Checked = CorScheme.BaseScheme.ShowMarkersConnection;
            ShowMarkersInfo.Checked = CorScheme.BaseScheme.ShowMarkersInfo;
            LoadColumnVisible();
            LoadDepthSpace();
            LoadDepthInit();
            this.StartPosition = FormStartPosition.Manual;
            Point pt = MainForm.Location;
            Size size = MainForm.ClientRectangle.Size;
            if (MainForm.corSchPane.DockedState == Infragistics.Win.UltraWinDock.DockedState.Floating)
            {
                pt = MainForm.corSchPane.DockAreaPane.FloatingLocation;
                size = MainForm.corSchPane.DockAreaPane.Size;
            }
            int x = pt.X + size.Width / 2 - this.Width / 2;
            int y = pt.Y + size.Height / 2 - this.Height / 2;
            if (x < 0) x = 0; if (y < 0) y = 0;
            this.Location = new Point(x, y);

            this.Show();
        }
        public void LoadDepthInit()
        {
            if (Scheme != null)
            {
                DepthCalcInitFromTrajectory.Checked = Scheme.InitDepthByTrajectory;
            }
        }
        public void SaveDepthInit()
        {
            if (Scheme != null)
            {
                Scheme.InitDepthByTrajectory = DepthCalcInitFromTrajectory.Checked;
            }
        }
        public void LoadColumnVisible()
        {
            if (Scheme != null)
            {
                for (int i = 0; i < Scheme.Columns.Length; i++)
                {
                    PlaneColumn col = Scheme.Columns[i];
                    if ((col.Count == 1) && (col[0].TypeID == Constant.Plane.ObjectType.DEPTH_MARKS))
                    {
                        ColumnsTree.Nodes[0].Checked = col[0].Visible;
                    }
                    else if ((col.Count == 4) && (col[0].TypeID == Constant.Plane.ObjectType.GIS))
                    {
                        for (int j = 0; j < col.Count; j++)
                        {
                            ColumnsTree.Nodes[1].Nodes[j].Checked = col[j].Visible;
                        }
                    }
                    else if ((col.Count == 1) && (col[0].TypeID == Constant.Plane.ObjectType.PERFORATION))
                    {
                        ColumnsTree.Nodes[2].Checked = col[0].Visible;
                    }
                    else if ((col.Count > 2) && (col[1].TypeID == Constant.Plane.ObjectType.LOG))
                    {
                        for (int j = 0; j < col.Count; j++)
                        {
                            ColumnsTree.Nodes[3].Nodes[j].Checked = col[col.Count - 1 - j].Visible;
                        }
                    }
                }
            }
        }
        public void SaveColumnVisible()
        {
            if (Scheme != null)
            {
                bool visible = false;
                for (int i = 0; i < Scheme.Columns.Length; i++)
                {
                    PlaneColumn col = Scheme.Columns[i];
                    if ((col.Count == 1) && (col[0].TypeID == Constant.Plane.ObjectType.DEPTH_MARKS))
                    {
                        col[0].Visible = ColumnsTree.Nodes[0].Checked;
                    }
                    else if ((col.Count == 4) && (col[0].TypeID == Constant.Plane.ObjectType.GIS))
                    {
                        for (int j = 0; j < col.Count; j++)
                        {
                            col[j].Visible = ColumnsTree.Nodes[1].Nodes[j].Checked;
                        }
                    }
                    else if ((col.Count == 1) && (col[0].TypeID == Constant.Plane.ObjectType.PERFORATION))
                    {
                        col[0].Visible = ColumnsTree.Nodes[2].Checked;
                    }
                    else if ((col.Count > 2) && (col[1].TypeID == Constant.Plane.ObjectType.LOG))
                    {
                        for (int j = 0; j < col.Count; j++)
                        {
                            col[col.Count - 1 - j].Visible = ColumnsTree.Nodes[3].Nodes[j].Checked;
                        }
                    }
                    visible = false;
                    for (int j = 0; j < col.Count; j++)
                    {
                        visible = visible || ((col[j].TypeID != Constant.Plane.ObjectType.CORE) && col[j].Visible);
                        // Керн не используется при проверке видимости столбца
                    }
                    col.Visible = visible;
                }
            }
        }
        public void LoadDepthSpace()
        {
            if (Scheme != null)
            {
                DepthTopSpace.Value = Scheme.TopSpace;
                DepthBottomSpace.Value = Scheme.BottomSpace;
            }
        }
        public void SaveDepthSpace()
        {
            if (Scheme != null)
            {
                Scheme.TopSpace = (int)DepthTopSpace.Value;
                Scheme.BottomSpace = (int)DepthBottomSpace.Value;
            }
        }
        public void UpdateDataTypeGroups()
        {
            if ((Scheme != null) && (SelColIndex != -1) && (SelObjIndex != -1))
            {
                bool find = false;

                PlaneColumn col = Scheme.Columns[SelColIndex];
                if ((col.Count == 1) && (col[SelObjIndex].TypeID == Constant.Plane.ObjectType.DEPTH_MARKS))
                {
                    LoadSetting((PlaneDepths)col[SelObjIndex], col);
                    DepthSettingsGroup.Visible = true;
                    GisSettingsGroup.Visible = false;
                    PerfSettingsGroup.Visible = false;
                    LogsSettingsGroup.Visible = false;
                    ObjGisSettingsGroup.Visible = false;
                    find = true;
                }
                else if ((col.Count == 4) && (col[SelObjIndex].TypeID == Constant.Plane.ObjectType.GIS))
                {
                    LoadSetting((PlaneGis)col[0], col);
                    DepthSettingsGroup.Visible = false;
                    GisSettingsGroup.Visible = true;
                    PerfSettingsGroup.Visible = false;
                    LogsSettingsGroup.Visible = false;
                    ObjGisSettingsGroup.Visible = false;
                    find = true;
                }
                else if ((col.Count == 1) && (col[SelObjIndex].TypeID == Constant.Plane.ObjectType.PERFORATION))
                {
                    LoadSetting((PlanePerf)col[SelObjIndex], col);
                    DepthSettingsGroup.Visible = false;
                    GisSettingsGroup.Visible = false;
                    PerfSettingsGroup.Visible = true;
                    LogsSettingsGroup.Visible = false;
                    ObjGisSettingsGroup.Visible = false;
                    find = true;
                }
                else if (col[SelObjIndex].TypeID == Constant.Plane.ObjectType.LOG)
                {
                    LoadSetting((PlaneLog)col[SelObjIndex], col);
                    DepthSettingsGroup.Visible = false;
                    GisSettingsGroup.Visible = false;
                    PerfSettingsGroup.Visible = false;
                    LogsSettingsGroup.Visible = true;
                    ObjGisSettingsGroup.Visible = false;
                    find = true;
                }
                else if (col[SelObjIndex].TypeID == Constant.Plane.ObjectType.OIL_OBJ)
                {
                    LoadSetting((PlaneOilObj)col[SelObjIndex], col);
                    DepthSettingsGroup.Visible = false;
                    GisSettingsGroup.Visible = false;
                    PerfSettingsGroup.Visible = false;
                    LogsSettingsGroup.Visible = false;
                    ObjGisSettingsGroup.Visible = true;
                    find = true; 
                }
                if (!find)
                {
                    DepthSettingsGroup.Visible = false;
                    GisSettingsGroup.Visible = false;
                    PerfSettingsGroup.Visible = false;
                    LogsSettingsGroup.Visible = false;
                    ObjGisSettingsGroup.Visible = false;
                }
            }
        }

        // Columns Tree
        void SetSelColAndObjIndexes()
        {
            SelColIndex = -1;
            SelObjIndex = -1;
            if (ColumnsTree.SelectedNode != null)
            {
                if (ColumnsTree.SelectedNode.Level == 0)
                {
                    SelColIndex = ColumnsTree.SelectedNode.Index;
                    SelObjIndex = 0;
                }
                else
                {
                    SelColIndex = ColumnsTree.SelectedNode.Parent.Index;
                    SelObjIndex = ColumnsTree.SelectedNode.Index;
                    if (SelColIndex == 1)
                    {
                        SelObjIndex = 0;
                    }
                    else if ((SelColIndex == 3) && (SelColIndex < this.Scheme.Count) && (Scheme.Columns[SelColIndex].Count - 1 - SelObjIndex >= 0))
                    {
                        SelObjIndex = Scheme.Columns[SelColIndex].Count - 1 - SelObjIndex;
                    }
                }
            }
        }
        void ColumnsTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (SelectedNode != e.Node)
            {
                SelectedNode = e.Node;
                SetSelColAndObjIndexes();
                UpdateDataTypeGroups();
            }
        }
        
        // COMMON FUNC
        void TestMinMaxColumnWidth(CheckBox MinUse, NumericUpDown Min, CheckBox MaxUse, NumericUpDown Max)
        {
            Max.Minimum = (MinUse.Checked) ? Min.Value : 0;
            Min.Maximum = (MaxUse.Checked) ? Max.Value : 1000;
        }
        void TestMinMaxValues(NumericUpDown Min, NumericUpDown Max)
        {
            Max.Minimum = Min.Value;
            Min.Maximum = Max.Value;
            double val = (double)Math.Abs(LogScaleMin.Value);
            if ((val > 0) && (val < 1))
            {
                int j = 0;
                double k = 0.1;
                while (val < 1)
                {
                    val *= 10;
                    k /= 10;
                    j++;
                }
                LogScaleMin.Increment = (decimal)k * 10;
                LogScaleMin.DecimalPlaces = j;
            }
            else
            {
                LogScaleMin.Increment = 1;
                LogScaleMin.DecimalPlaces = 0;
            }
        }
        void LoadColumnWidth(PlaneColumn Column, CheckBox MinUse, NumericUpDown Min, CheckBox MaxUse, NumericUpDown Max)
        {
            if (Column.MinWidth > -1)
            {
                MinUse.Checked = true;
                Min.Enabled = true;
                Min.Value = (decimal)Column.MinWidth;
            }
            else
            {
                MinUse.Checked = false;
                Min.Enabled = false;
                Min.Value = Min.Minimum;
            }
            if (Column.MaxWidth > -1)
            {
                MaxUse.Checked = true;
                Max.Enabled = true;
                Max.Value = (decimal)Column.MaxWidth;
            }
            else
            {
                MaxUse.Checked = false;
                Max.Enabled = false;
                Max.Value = Max.Minimum;
            }
            TestMinMaxColumnWidth(MinUse, Min, MaxUse, Max);
        }
        void SaveColumnWidth(PlaneColumn col, CheckBox MinUse, NumericUpDown Min, CheckBox MaxUse, NumericUpDown Max)
        {
            col.MinWidth = (MinUse.Checked) ? (float)Min.Value : -1;
            col.MaxWidth = (MaxUse.Checked) ? (float)Max.Value : -1;
        }

        // Depth settings
        void LoadSetting(PlaneDepths Depth, PlaneColumn Column)
        {
            // Column Width
            LoadColumnWidth(Column, DepthColMinUse, DepthColMin, DepthColMaxUse, DepthColMax);
        }
        void SaveSetting(PlaneDepths Depth, PlaneColumn Column)
        {
            SaveColumnWidth(Column, DepthColMinUse, DepthColMin, DepthColMaxUse, DepthColMax);
        }
        
        // Depth component events
        void DepthColMaxUse_CheckedChanged(object sender, EventArgs e)
        {
            DepthColMin.Enabled = DepthColMinUse.Checked;
            TestMinMaxColumnWidth(DepthColMinUse, DepthColMin, DepthColMaxUse, DepthColMax);
        }
        void DepthColMinUse_CheckedChanged(object sender, EventArgs e)
        {
            DepthColMax.Enabled = DepthColMaxUse.Checked;
            TestMinMaxColumnWidth(DepthColMinUse, DepthColMin, DepthColMaxUse, DepthColMax);
        }
        void DepthColMin_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(DepthColMinUse, DepthColMin, DepthColMaxUse, DepthColMax);
        }
        void DepthColMax_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(DepthColMinUse, DepthColMin, DepthColMaxUse, DepthColMax);
        }
        
        // Gis settings
        void LoadSetting(PlaneGis Gis, PlaneColumn Column)
        {
            // Column Width
            LoadColumnWidth(Column, GisColMinUse, GisColMin, GisColMaxUse, GisColMax);
        }
        void SaveSetting(PlaneGis Gis, PlaneColumn Column)
        {
            SaveColumnWidth(Column, GisColMinUse, GisColMin, GisColMaxUse, GisColMax);
        }

        // Gis component events
        void GisColMinUse_CheckedChanged(object sender, EventArgs e)
        {
            GisColMin.Enabled = GisColMinUse.Checked;
            TestMinMaxColumnWidth(GisColMinUse, GisColMin, GisColMaxUse, GisColMax);
        }
        void GisColMaxUse_CheckedChanged(object sender, EventArgs e)
        {
            GisColMax.Enabled = GisColMaxUse.Checked;
            TestMinMaxColumnWidth(GisColMinUse, GisColMin, GisColMaxUse, GisColMax);
        }
        void GisColMax_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(GisColMinUse, GisColMin, GisColMaxUse, GisColMax);
        }
        void GisColMin_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(GisColMinUse, GisColMin, GisColMaxUse, GisColMax);
        }
        
        // Perf settings
        void LoadSetting(PlanePerf Perf, PlaneColumn Column)
        {
            // Column Width
            LoadColumnWidth(Column, PerfColMinUse, PerfColMin, PerfColMaxUse, PerfColMax);
        }
        void SaveSetting(PlanePerf Perf, PlaneColumn Column)
        {
            SaveColumnWidth(Column, PerfColMinUse, PerfColMin, PerfColMaxUse, PerfColMax);
        }
        
        // Perf component events
        void PerfColMinUse_CheckedChanged(object sender, EventArgs e)
        {
            PerfColMin.Enabled = PerfColMinUse.Checked;
            TestMinMaxColumnWidth(PerfColMinUse, PerfColMin, PerfColMaxUse, PerfColMax);
        }
        void PerfColMaxUse_CheckedChanged(object sender, EventArgs e)
        {
            PerfColMax.Enabled = PerfColMaxUse.Checked;
            TestMinMaxColumnWidth(PerfColMinUse, PerfColMin, PerfColMaxUse, PerfColMax);
        }
        void PerfColMax_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(PerfColMinUse, PerfColMin, PerfColMaxUse, PerfColMax);
        }
        void PerfColMin_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(PerfColMinUse, PerfColMin, PerfColMaxUse, PerfColMax);
        }

        // Logs settings
        void UpdateLogScaleMinMaxControls()
        {
            if (LogScaleMinMaxAuto.Checked)
            {
                LogScaleMinMaxManualByStep.Enabled = false;
                LogScaleStep.Enabled = false; lScaleStep.Enabled = false;
                LogScaleMin.Enabled = false; lScaleMin.Enabled = false;
                LogScaleMax.Enabled = false; lScaleMax.Enabled = false;
            }
            else if (LogScaleMinMaxManual.Checked)
            {
                if (LogScaleType.SelectedIndex == (int)Constant.Plane.ScaleType.LOG10 ||
                    LogScaleType.SelectedIndex == (int)Constant.Plane.ScaleType.INVERT_LOG10)
                {
                    LogScaleMinMaxManualByStep.Enabled = false;
                }
                else
                {
                    LogScaleMinMaxManualByStep.Enabled = true;
                }
                LogScaleMin.Enabled = true; lScaleMin.Enabled = true;
                LogScaleMax.Enabled = true; lScaleMax.Enabled = true;
                if (LogScaleMinMaxManualByStep.Checked && LogScaleMinMaxManualByStep.Enabled)
                {
                    LogScaleStep.Enabled = true;
                    lScaleStep.Enabled = true;
                    LogScaleMax.Enabled = false;
                    lScaleMax.Enabled = false;
                }
                else
                {
                    LogScaleStep.Enabled = false;
                    lScaleStep.Enabled = false;
                    LogScaleMax.Enabled = true;
                    lScaleMax.Enabled = true;
                }
            }
        }
        void LoadSetting(PlaneLog Log, PlaneColumn Column)
        {
            if (Log != null)
            {
                LogMask.Text = Log.MnemonicMask;
                LoadColumnWidth(Column, LogColumnMinUse, LogColumnMin, LogColumnMaxUse, LogColumnMax);
                
                // Line
                LogLineWidth.Value = Log.LineWidth;
                LogLineColor.SelectedColor = Log.LineColor;
                LogLineType.SelectedDashStyle = Log.LineStyle;

                // Back
                LogGridHorizontUse.Checked = Log.DrawGridHor;
                LogGridVerticalUse.Checked = Log.DrawGridVert;
                LogBackUse.Checked = Log.FillRegion;
                LogBackColor.SelectedColor = Log.BackColor;
                LogBackTransparent.Value =  (int)((255 - Log.BackTransparent) * 100f / 255f);
                LogBackInvert.Checked = Log.BackInverted;

                // Scale
                if (Log.scale != null)
                {
                    LogScaleType.SelectedIndex = (int)Log.scale.Type;
                    LogScaleMinMaxAuto.Checked = Log.scale.MinMaxAuto;
                    LogScaleMinMaxManual.Checked = !Log.scale.MinMaxAuto;
                    LogScaleMinMaxManualByStep.Checked = Log.scale.MinMaxUseStep;
                    if (Log.scale.MinMaxStep > -1) LogScaleStep.Value = (decimal)Log.scale.MinMaxStep;
                    if (Log.scale.MinValue != float.MaxValue)
                    {
                        LogScaleMin.Value = (decimal)Log.scale.MinValue;
                    }
                    else
                    {
                        LogScaleMin.Value = 0; 
                    }
                    if (Log.scale.MaxValue != float.MinValue)
                    {
                        LogScaleMax.Value = (decimal)Log.scale.MaxValue;
                    }
                    else
                    {
                        LogScaleMax.Value = 100;
                    }
                }
            }
        }
        void SaveSetting(PlaneLog Log, PlaneColumn Column)
        {
            SaveColumnWidth(Column, LogColumnMinUse, LogColumnMin, LogColumnMaxUse, LogColumnMax);
            
            // Line
            Log.LineWidth = (int)LogLineWidth.Value;
            Log.LineColor = LogLineColor.SelectedColor;
            Log.LineStyle = LogLineType.SelectedDashStyle;

            // Back
            Log.DrawGridHor = LogGridHorizontUse.Checked;
            Log.DrawGridVert = LogGridVerticalUse.Checked;
            Log.FillRegion = LogBackUse.Checked;
            Log.BackColor = LogBackColor.SelectedColor;
            Log.BackTransparent = (int)((float)(100 - LogBackTransparent.Value) * 255f / 100f);
            Log.BackInverted = LogBackInvert.Checked;

            // Scale
            if (Log.scale != null)
            {
                Log.scale.color = LogLineColor.SelectedColor;
                Log.scale.Type = (Constant.Plane.ScaleType)LogScaleType.SelectedIndex;
                Log.scale.MinMaxAuto = LogScaleMinMaxAuto.Checked;

                if (LogScaleMinMaxManual.Checked)
                {
                    Log.scale.MinMaxUseStep = LogScaleMinMaxManualByStep.Checked;
                    float min = (float)LogScaleMin.Value, max, step = -1;

                    if (LogScaleMinMaxManualByStep.Checked && LogScaleMinMaxManualByStep.Enabled)
                    {
                        step = (float)LogScaleStep.Value;
                        max = min + step * 10;
                    }
                    else
                    {
                        max = (float)LogScaleMax.Value;
                        step = (max - min) / 10.0f;
                    }
                    Log.scale.MinValue = min;
                    Log.scale.MaxValue = max;
                    Log.scale.MinMaxStep = step;
                }
            }
        }

        // Logs component events
        void LogColumnMinUse_CheckedChanged(object sender, EventArgs e)
        {
            LogColumnMin.Enabled = LogColumnMinUse.Checked;
            TestMinMaxColumnWidth(LogColumnMinUse, LogColumnMin, LogColumnMaxUse, LogColumnMax);
        }
        void LogColumnMaxUse_CheckedChanged(object sender, EventArgs e)
        {
            LogColumnMax.Enabled = LogColumnMaxUse.Checked;
            TestMinMaxColumnWidth(LogColumnMinUse, LogColumnMin, LogColumnMaxUse, LogColumnMax);
        }
        void LogColumnMax_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(LogColumnMinUse, LogColumnMin, LogColumnMaxUse, LogColumnMax);
        }
        void LogColumnMin_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(LogColumnMinUse, LogColumnMin, LogColumnMaxUse, LogColumnMax);
        }
        void LogBackUse_CheckedChanged(object sender, EventArgs e)
        {
            if (LogBackUse.Checked)
            {
                LogBackColor.Enabled = true;
                LogBackTransparent.Enabled = true;
                LogBackInvert.Enabled = true;
                lSpottingTrans.Enabled = true;
                lSpottingColor.Enabled = true;
            }
            else
            {
                LogBackColor.Enabled = false;
                LogBackTransparent.Enabled = false;
                LogBackInvert.Enabled = false;
                lSpottingTrans.Enabled = false;
                lSpottingColor.Enabled = false;
            }
        }
        void ScaleMinMax_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLogScaleMinMaxControls();
        }
        void ScaleMinMaxManualByStep_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLogScaleMinMaxControls();
        }
        void LogScaleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateLogScaleMinMaxControls();
        }
        
        // Obj GIS Settings
        void ObjGisMaxUse_CheckedChanged(object sender, EventArgs e)
        {
            ObjGisMax.Enabled = ObjGisMaxUse.Checked;
            TestMinMaxColumnWidth(ObjGisMinUse, ObjGisMin, ObjGisMaxUse, ObjGisMax);
        }
        void ObjGisMinUse_CheckedChanged(object sender, EventArgs e)
        {
            ObjGisMin.Enabled = ObjGisMinUse.Checked;
            TestMinMaxColumnWidth(ObjGisMinUse, ObjGisMin, ObjGisMaxUse, ObjGisMax);
        }
        void ObjGisMin_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(ObjGisMinUse, ObjGisMin, ObjGisMaxUse, ObjGisMax);
        }
        void ObjGisMax_ValueChanged(object sender, EventArgs e)
        {
            TestMinMaxColumnWidth(ObjGisMinUse, ObjGisMin, ObjGisMaxUse, ObjGisMax);
        }
        void LoadSetting(PlaneOilObj OilObjects, PlaneColumn Column)
        {
            // Column Width
            LoadColumnWidth(Column, ObjGisMinUse, ObjGisMin, ObjGisMaxUse, ObjGisMax);
        }
        void SaveSetting(PlaneOilObj OilObjects, PlaneColumn Column)
        {
            SaveColumnWidth(Column, ObjGisMinUse, ObjGisMin, ObjGisMaxUse, ObjGisMax);
        }

        // Buttons
        private void OkButton_Click(object sender, EventArgs e)
        {
            // Update Settings in BaseScheme
            if ((SelColIndex != -1) && (SelObjIndex != -1))
            {
                switch (Scheme.Columns[SelColIndex][SelObjIndex].TypeID)
                {
                    case Constant.Plane.ObjectType.DEPTH_MARKS:
                        SaveSetting((PlaneDepths)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                    case Constant.Plane.ObjectType.GIS:
                        SaveSetting((PlaneGis)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                    case Constant.Plane.ObjectType.PERFORATION:
                        SaveSetting((PlanePerf)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                    case Constant.Plane.ObjectType.LOG:
                        SaveSetting((PlaneLog)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                    case Constant.Plane.ObjectType.OIL_OBJ:
                        SaveSetting((PlaneOilObj)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                }
            }
            Scheme.ShowMarkersConnection = ShowMarkersConnection.Checked;
            Scheme.ShowMarkersInfo = ShowMarkersInfo.Checked;
            SaveColumnVisible();
            SaveDepthSpace();
            SaveDepthInit();
            Scheme.SetNewWidth(Scheme.Width);
            CorScheme.UpdatePlanesByBaseScheme();
            this.Close();
        }
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            // Update Settings in BaseScheme
            if ((SelColIndex != -1) && (SelObjIndex != -1))
            {
                switch (Scheme.Columns[SelColIndex][SelObjIndex].TypeID)
                {
                    case Constant.Plane.ObjectType.DEPTH_MARKS:
                        SaveSetting((PlaneDepths)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                    case Constant.Plane.ObjectType.GIS:
                        SaveSetting((PlaneGis)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                    case Constant.Plane.ObjectType.PERFORATION:
                        SaveSetting((PlanePerf)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                    case Constant.Plane.ObjectType.LOG:
                        SaveSetting((PlaneLog)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                    case Constant.Plane.ObjectType.OIL_OBJ:
                        SaveSetting((PlaneOilObj)Scheme.Columns[SelColIndex][SelObjIndex], Scheme.Columns[SelColIndex]);
                        break;
                }
            }
            Scheme.ShowMarkersConnection = ShowMarkersConnection.Checked;
            Scheme.ShowMarkersInfo = ShowMarkersInfo.Checked;
            SaveColumnVisible();
            SaveDepthSpace();
            SaveDepthInit();
            Scheme.SetNewWidth(Scheme.Width);
            CorScheme.UpdatePlanesByBaseScheme();
        }
    }
}
