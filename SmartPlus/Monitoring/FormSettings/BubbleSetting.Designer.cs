﻿namespace SmartPlus
{
    partial class BubbleSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BubbleSetting));
            this.tabSetting = new System.Windows.Forms.TabControl();
            this.tpBubbleCurrent = new System.Windows.Forms.TabPage();
            this.gbBCSet = new System.Windows.Forms.GroupBox();
            this.tbBCK = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbBCLast = new System.Windows.Forms.GroupBox();
            this.lBCLastInterval = new System.Windows.Forms.Label();
            this.lmonth = new System.Windows.Forms.Label();
            this.tbBCLastAveMonth = new System.Windows.Forms.TextBox();
            this.lAver = new System.Windows.Forms.Label();
            this.cbBCLastDate = new System.Windows.Forms.CheckBox();
            this.dtpBCLastDate = new System.Windows.Forms.DateTimePicker();
            this.tpBubbleAccum = new System.Windows.Forms.TabPage();
            this.gbBASet = new System.Windows.Forms.GroupBox();
            this.tbBAK = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbBALast = new System.Windows.Forms.GroupBox();
            this.lBALastInterval = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbBALastAveMonth = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbBALastDate = new System.Windows.Forms.CheckBox();
            this.dtpBALastDate = new System.Windows.Forms.DateTimePicker();
            this.gbBAStart = new System.Windows.Forms.GroupBox();
            this.lBAStartInterval = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbBAStartAveMonth = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbBAStartDate = new System.Windows.Forms.CheckBox();
            this.dtpBAStartDate = new System.Windows.Forms.DateTimePicker();
            this.tpBubbleHistory = new System.Windows.Forms.TabPage();
            this.gbDataSource = new System.Windows.Forms.GroupBox();
            this.dtpSourceComment = new System.Windows.Forms.Label();
            this.rbSourceChess = new System.Windows.Forms.RadioButton();
            this.rbSourceMer = new System.Windows.Forms.RadioButton();
            this.gbBHSet = new System.Windows.Forms.GroupBox();
            this.gbRegim = new System.Windows.Forms.GroupBox();
            this.rbViewByColumn = new System.Windows.Forms.RadioButton();
            this.rbViewBySector = new System.Windows.Forms.RadioButton();
            this.tbBHK = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gbBHLast = new System.Windows.Forms.GroupBox();
            this.lBHLastInterval = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbBHLastAveMonth = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbBHLastDate = new System.Windows.Forms.CheckBox();
            this.dtpBHLastDate = new System.Windows.Forms.DateTimePicker();
            this.gbBHStart = new System.Windows.Forms.GroupBox();
            this.lBHStartInterval = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbBHStartAveMonth = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbBHStartDate = new System.Windows.Forms.CheckBox();
            this.dtpBHStartDate = new System.Windows.Forms.DateTimePicker();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.cbBCGazInThousand = new System.Windows.Forms.CheckBox();
            this.cbBAGazInThousand = new System.Windows.Forms.CheckBox();
            this.tabSetting.SuspendLayout();
            this.tpBubbleCurrent.SuspendLayout();
            this.gbBCSet.SuspendLayout();
            this.gbBCLast.SuspendLayout();
            this.tpBubbleAccum.SuspendLayout();
            this.gbBASet.SuspendLayout();
            this.gbBALast.SuspendLayout();
            this.gbBAStart.SuspendLayout();
            this.tpBubbleHistory.SuspendLayout();
            this.gbDataSource.SuspendLayout();
            this.gbBHSet.SuspendLayout();
            this.gbRegim.SuspendLayout();
            this.gbBHLast.SuspendLayout();
            this.gbBHStart.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabSetting
            // 
            this.tabSetting.Controls.Add(this.tpBubbleCurrent);
            this.tabSetting.Controls.Add(this.tpBubbleAccum);
            this.tabSetting.Controls.Add(this.tpBubbleHistory);
            this.tabSetting.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabSetting.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabSetting.ImageList = this.imgList;
            this.tabSetting.Location = new System.Drawing.Point(0, 0);
            this.tabSetting.Name = "tabSetting";
            this.tabSetting.SelectedIndex = 0;
            this.tabSetting.Size = new System.Drawing.Size(730, 402);
            this.tabSetting.TabIndex = 0;
            // 
            // tpBubbleCurrent
            // 
            this.tpBubbleCurrent.BackColor = System.Drawing.Color.White;
            this.tpBubbleCurrent.Controls.Add(this.gbBCSet);
            this.tpBubbleCurrent.Controls.Add(this.gbBCLast);
            this.tpBubbleCurrent.ImageIndex = 0;
            this.tpBubbleCurrent.Location = new System.Drawing.Point(4, 24);
            this.tpBubbleCurrent.Name = "tpBubbleCurrent";
            this.tpBubbleCurrent.Padding = new System.Windows.Forms.Padding(3);
            this.tpBubbleCurrent.Size = new System.Drawing.Size(722, 374);
            this.tpBubbleCurrent.TabIndex = 0;
            this.tpBubbleCurrent.Text = "Карта текущих отборов";
            // 
            // gbBCSet
            // 
            this.gbBCSet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBCSet.BackColor = System.Drawing.Color.White;
            this.gbBCSet.Controls.Add(this.cbBCGazInThousand);
            this.gbBCSet.Controls.Add(this.label10);
            this.gbBCSet.Controls.Add(this.tbBCK);
            this.gbBCSet.Controls.Add(this.label4);
            this.gbBCSet.Location = new System.Drawing.Point(10, 101);
            this.gbBCSet.Name = "gbBCSet";
            this.gbBCSet.Size = new System.Drawing.Size(706, 267);
            this.gbBCSet.TabIndex = 17;
            this.gbBCSet.TabStop = false;
            this.gbBCSet.Text = " Параметры построения ";
            // 
            // tbBCK
            // 
            this.tbBCK.Location = new System.Drawing.Point(223, 16);
            this.tbBCK.Name = "tbBCK";
            this.tbBCK.Size = new System.Drawing.Size(127, 23);
            this.tbBCK.TabIndex = 1;
            this.tbBCK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbBCK.TextChanged += new System.EventHandler(this.tbBCK_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Коэффициент пропорциональности:";
            // 
            // gbBCLast
            // 
            this.gbBCLast.BackColor = System.Drawing.Color.White;
            this.gbBCLast.Controls.Add(this.lBCLastInterval);
            this.gbBCLast.Controls.Add(this.lmonth);
            this.gbBCLast.Controls.Add(this.tbBCLastAveMonth);
            this.gbBCLast.Controls.Add(this.lAver);
            this.gbBCLast.Controls.Add(this.cbBCLastDate);
            this.gbBCLast.Controls.Add(this.dtpBCLastDate);
            this.gbBCLast.Location = new System.Drawing.Point(10, 5);
            this.gbBCLast.Name = "gbBCLast";
            this.gbBCLast.Size = new System.Drawing.Size(350, 90);
            this.gbBCLast.TabIndex = 15;
            this.gbBCLast.TabStop = false;
            this.gbBCLast.Text = " Построить на дату ";
            // 
            // lBCLastInterval
            // 
            this.lBCLastInterval.AutoSize = true;
            this.lBCLastInterval.ForeColor = System.Drawing.Color.DimGray;
            this.lBCLastInterval.Location = new System.Drawing.Point(7, 46);
            this.lBCLastInterval.Name = "lBCLastInterval";
            this.lBCLastInterval.Size = new System.Drawing.Size(66, 15);
            this.lBCLastInterval.TabIndex = 15;
            this.lBCLastInterval.Text = "Интревал: ";
            // 
            // lmonth
            // 
            this.lmonth.AutoSize = true;
            this.lmonth.Location = new System.Drawing.Point(316, 23);
            this.lmonth.Name = "lmonth";
            this.lmonth.Size = new System.Drawing.Size(28, 15);
            this.lmonth.TabIndex = 14;
            this.lmonth.Text = "мес";
            // 
            // tbBCLastAveMonth
            // 
            this.tbBCLastAveMonth.ForeColor = System.Drawing.Color.Gray;
            this.tbBCLastAveMonth.Location = new System.Drawing.Point(246, 20);
            this.tbBCLastAveMonth.Name = "tbBCLastAveMonth";
            this.tbBCLastAveMonth.Size = new System.Drawing.Size(64, 23);
            this.tbBCLastAveMonth.TabIndex = 13;
            this.tbBCLastAveMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbBCLastAveMonth.TextChanged += new System.EventHandler(this.tbBCAverageMonth_TextChanged);
            // 
            // lAver
            // 
            this.lAver.AutoSize = true;
            this.lAver.Location = new System.Drawing.Point(169, 23);
            this.lAver.Name = "lAver";
            this.lAver.Size = new System.Drawing.Size(71, 15);
            this.lAver.TabIndex = 12;
            this.lAver.Text = "Средний за";
            // 
            // cbBCLastDate
            // 
            this.cbBCLastDate.AutoSize = true;
            this.cbBCLastDate.Checked = true;
            this.cbBCLastDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBCLastDate.Location = new System.Drawing.Point(10, 65);
            this.cbBCLastDate.Name = "cbBCLastDate";
            this.cbBCLastDate.Size = new System.Drawing.Size(225, 19);
            this.cbBCLastDate.TabIndex = 11;
            this.cbBCLastDate.Text = "Построить на последнюю дату МЭР";
            this.cbBCLastDate.UseVisualStyleBackColor = true;
            this.cbBCLastDate.CheckedChanged += new System.EventHandler(this.cbBCLastDate_CheckedChanged);
            // 
            // dtpBCLastDate
            // 
            this.dtpBCLastDate.CustomFormat = "dd.MM.yyyy";
            this.dtpBCLastDate.Enabled = false;
            this.dtpBCLastDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBCLastDate.Location = new System.Drawing.Point(10, 20);
            this.dtpBCLastDate.Name = "dtpBCLastDate";
            this.dtpBCLastDate.Size = new System.Drawing.Size(153, 23);
            this.dtpBCLastDate.TabIndex = 9;
            this.dtpBCLastDate.ValueChanged += new System.EventHandler(this.dtpBCLastDate_ValueChanged);
            // 
            // tpBubbleAccum
            // 
            this.tpBubbleAccum.BackColor = System.Drawing.Color.White;
            this.tpBubbleAccum.Controls.Add(this.gbBASet);
            this.tpBubbleAccum.Controls.Add(this.gbBALast);
            this.tpBubbleAccum.Controls.Add(this.gbBAStart);
            this.tpBubbleAccum.ImageIndex = 1;
            this.tpBubbleAccum.Location = new System.Drawing.Point(4, 24);
            this.tpBubbleAccum.Name = "tpBubbleAccum";
            this.tpBubbleAccum.Padding = new System.Windows.Forms.Padding(3);
            this.tpBubbleAccum.Size = new System.Drawing.Size(722, 374);
            this.tpBubbleAccum.TabIndex = 1;
            this.tpBubbleAccum.Text = "Карта накопленных отборов";
            // 
            // gbBASet
            // 
            this.gbBASet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBASet.BackColor = System.Drawing.Color.White;
            this.gbBASet.Controls.Add(this.cbBAGazInThousand);
            this.gbBASet.Controls.Add(this.tbBAK);
            this.gbBASet.Controls.Add(this.label1);
            this.gbBASet.Location = new System.Drawing.Point(10, 101);
            this.gbBASet.Name = "gbBASet";
            this.gbBASet.Size = new System.Drawing.Size(706, 267);
            this.gbBASet.TabIndex = 16;
            this.gbBASet.TabStop = false;
            this.gbBASet.Text = " Параметры построения ";
            // 
            // tbBAK
            // 
            this.tbBAK.Location = new System.Drawing.Point(223, 16);
            this.tbBAK.Name = "tbBAK";
            this.tbBAK.Size = new System.Drawing.Size(127, 23);
            this.tbBAK.TabIndex = 1;
            this.tbBAK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbBAK.TextChanged += new System.EventHandler(this.tbBAK_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Коэффициент пропорциональности:";
            // 
            // gbBALast
            // 
            this.gbBALast.BackColor = System.Drawing.Color.White;
            this.gbBALast.Controls.Add(this.lBALastInterval);
            this.gbBALast.Controls.Add(this.label5);
            this.gbBALast.Controls.Add(this.tbBALastAveMonth);
            this.gbBALast.Controls.Add(this.label6);
            this.gbBALast.Controls.Add(this.cbBALastDate);
            this.gbBALast.Controls.Add(this.dtpBALastDate);
            this.gbBALast.Location = new System.Drawing.Point(366, 5);
            this.gbBALast.Name = "gbBALast";
            this.gbBALast.Size = new System.Drawing.Size(350, 90);
            this.gbBALast.TabIndex = 15;
            this.gbBALast.TabStop = false;
            this.gbBALast.Text = " Дата окончания интервала расчета ";
            // 
            // lBALastInterval
            // 
            this.lBALastInterval.AutoSize = true;
            this.lBALastInterval.ForeColor = System.Drawing.Color.DimGray;
            this.lBALastInterval.Location = new System.Drawing.Point(9, 46);
            this.lBALastInterval.Name = "lBALastInterval";
            this.lBALastInterval.Size = new System.Drawing.Size(63, 15);
            this.lBALastInterval.TabIndex = 19;
            this.lBALastInterval.Text = "Интревал:";
            this.lBALastInterval.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(318, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 15);
            this.label5.TabIndex = 18;
            this.label5.Text = "мес";
            this.label5.Visible = false;
            // 
            // tbBALastAveMonth
            // 
            this.tbBALastAveMonth.ForeColor = System.Drawing.Color.Gray;
            this.tbBALastAveMonth.Location = new System.Drawing.Point(248, 20);
            this.tbBALastAveMonth.Name = "tbBALastAveMonth";
            this.tbBALastAveMonth.Size = new System.Drawing.Size(64, 23);
            this.tbBALastAveMonth.TabIndex = 17;
            this.tbBALastAveMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbBALastAveMonth.Visible = false;
            this.tbBALastAveMonth.TextChanged += new System.EventHandler(this.tbBALastAveMonth_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(171, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 16;
            this.label6.Text = "Средний за";
            this.label6.Visible = false;
            // 
            // cbBALastDate
            // 
            this.cbBALastDate.AutoSize = true;
            this.cbBALastDate.Checked = true;
            this.cbBALastDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBALastDate.Location = new System.Drawing.Point(12, 65);
            this.cbBALastDate.Name = "cbBALastDate";
            this.cbBALastDate.Size = new System.Drawing.Size(228, 19);
            this.cbBALastDate.TabIndex = 11;
            this.cbBALastDate.Text = "Использовать последнюю дату МЭР";
            this.cbBALastDate.UseVisualStyleBackColor = true;
            this.cbBALastDate.CheckedChanged += new System.EventHandler(this.cbBALastDate_CheckedChanged);
            // 
            // dtpBALastDate
            // 
            this.dtpBALastDate.CustomFormat = "dd.MM.yyyy";
            this.dtpBALastDate.Enabled = false;
            this.dtpBALastDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBALastDate.Location = new System.Drawing.Point(12, 20);
            this.dtpBALastDate.Name = "dtpBALastDate";
            this.dtpBALastDate.Size = new System.Drawing.Size(153, 23);
            this.dtpBALastDate.TabIndex = 9;
            this.dtpBALastDate.ValueChanged += new System.EventHandler(this.dtpBALastDate_ValueChanged);
            // 
            // gbBAStart
            // 
            this.gbBAStart.BackColor = System.Drawing.Color.White;
            this.gbBAStart.Controls.Add(this.lBAStartInterval);
            this.gbBAStart.Controls.Add(this.label2);
            this.gbBAStart.Controls.Add(this.tbBAStartAveMonth);
            this.gbBAStart.Controls.Add(this.label3);
            this.gbBAStart.Controls.Add(this.cbBAStartDate);
            this.gbBAStart.Controls.Add(this.dtpBAStartDate);
            this.gbBAStart.Location = new System.Drawing.Point(10, 5);
            this.gbBAStart.Name = "gbBAStart";
            this.gbBAStart.Size = new System.Drawing.Size(350, 90);
            this.gbBAStart.TabIndex = 14;
            this.gbBAStart.TabStop = false;
            this.gbBAStart.Text = " Дата начала интервала расчета ";
            // 
            // lBAStartInterval
            // 
            this.lBAStartInterval.AutoSize = true;
            this.lBAStartInterval.ForeColor = System.Drawing.Color.DimGray;
            this.lBAStartInterval.Location = new System.Drawing.Point(7, 46);
            this.lBAStartInterval.Name = "lBAStartInterval";
            this.lBAStartInterval.Size = new System.Drawing.Size(66, 15);
            this.lBAStartInterval.TabIndex = 19;
            this.lBAStartInterval.Text = "Интревал: ";
            this.lBAStartInterval.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(316, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 15);
            this.label2.TabIndex = 18;
            this.label2.Text = "мес";
            this.label2.Visible = false;
            // 
            // tbBAStartAveMonth
            // 
            this.tbBAStartAveMonth.ForeColor = System.Drawing.Color.Gray;
            this.tbBAStartAveMonth.Location = new System.Drawing.Point(246, 20);
            this.tbBAStartAveMonth.Name = "tbBAStartAveMonth";
            this.tbBAStartAveMonth.Size = new System.Drawing.Size(64, 23);
            this.tbBAStartAveMonth.TabIndex = 17;
            this.tbBAStartAveMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbBAStartAveMonth.Visible = false;
            this.tbBAStartAveMonth.TextChanged += new System.EventHandler(this.tbBAStartAveMonth_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(169, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Средний за";
            this.label3.Visible = false;
            // 
            // cbBAStartDate
            // 
            this.cbBAStartDate.AutoSize = true;
            this.cbBAStartDate.Checked = true;
            this.cbBAStartDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBAStartDate.Location = new System.Drawing.Point(10, 65);
            this.cbBAStartDate.Name = "cbBAStartDate";
            this.cbBAStartDate.Size = new System.Drawing.Size(204, 19);
            this.cbBAStartDate.TabIndex = 11;
            this.cbBAStartDate.Text = "Использовать первую дату МЭР";
            this.cbBAStartDate.UseVisualStyleBackColor = true;
            this.cbBAStartDate.CheckedChanged += new System.EventHandler(this.cbBAStartDate_CheckedChanged);
            // 
            // dtpBAStartDate
            // 
            this.dtpBAStartDate.CustomFormat = "dd.MM.yyyy";
            this.dtpBAStartDate.Enabled = false;
            this.dtpBAStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBAStartDate.Location = new System.Drawing.Point(10, 20);
            this.dtpBAStartDate.Name = "dtpBAStartDate";
            this.dtpBAStartDate.Size = new System.Drawing.Size(153, 23);
            this.dtpBAStartDate.TabIndex = 9;
            this.dtpBAStartDate.ValueChanged += new System.EventHandler(this.dtpBAStartDate_ValueChanged);
            // 
            // tpBubbleHistory
            // 
            this.tpBubbleHistory.BackColor = System.Drawing.Color.White;
            this.tpBubbleHistory.Controls.Add(this.gbDataSource);
            this.tpBubbleHistory.Controls.Add(this.gbBHSet);
            this.tpBubbleHistory.Controls.Add(this.gbBHLast);
            this.tpBubbleHistory.Controls.Add(this.gbBHStart);
            this.tpBubbleHistory.ImageIndex = 2;
            this.tpBubbleHistory.Location = new System.Drawing.Point(4, 24);
            this.tpBubbleHistory.Name = "tpBubbleHistory";
            this.tpBubbleHistory.Size = new System.Drawing.Size(722, 374);
            this.tpBubbleHistory.TabIndex = 2;
            this.tpBubbleHistory.Text = "Карта изменений отборов";
            // 
            // gbDataSource
            // 
            this.gbDataSource.Controls.Add(this.dtpSourceComment);
            this.gbDataSource.Controls.Add(this.rbSourceChess);
            this.gbDataSource.Controls.Add(this.rbSourceMer);
            this.gbDataSource.Location = new System.Drawing.Point(10, 101);
            this.gbDataSource.Name = "gbDataSource";
            this.gbDataSource.Size = new System.Drawing.Size(350, 267);
            this.gbDataSource.TabIndex = 18;
            this.gbDataSource.TabStop = false;
            this.gbDataSource.Text = " Источник данных";
            // 
            // dtpSourceComment
            // 
            this.dtpSourceComment.Location = new System.Drawing.Point(6, 198);
            this.dtpSourceComment.Name = "dtpSourceComment";
            this.dtpSourceComment.Size = new System.Drawing.Size(337, 57);
            this.dtpSourceComment.TabIndex = 2;
            // 
            // rbSourceChess
            // 
            this.rbSourceChess.AutoSize = true;
            this.rbSourceChess.Location = new System.Drawing.Point(10, 47);
            this.rbSourceChess.Name = "rbSourceChess";
            this.rbSourceChess.Size = new System.Drawing.Size(83, 19);
            this.rbSourceChess.TabIndex = 1;
            this.rbSourceChess.Text = "Шахматка";
            this.rbSourceChess.UseVisualStyleBackColor = true;
            this.rbSourceChess.CheckedChanged += new System.EventHandler(this.rbSourceChess_CheckedChanged);
            // 
            // rbSourceMer
            // 
            this.rbSourceMer.AutoSize = true;
            this.rbSourceMer.Checked = true;
            this.rbSourceMer.Location = new System.Drawing.Point(10, 22);
            this.rbSourceMer.Name = "rbSourceMer";
            this.rbSourceMer.Size = new System.Drawing.Size(50, 19);
            this.rbSourceMer.TabIndex = 0;
            this.rbSourceMer.TabStop = true;
            this.rbSourceMer.Text = "МЭР";
            this.rbSourceMer.UseVisualStyleBackColor = true;
            this.rbSourceMer.CheckedChanged += new System.EventHandler(this.rbSourceMer_CheckedChanged);
            // 
            // gbBHSet
            // 
            this.gbBHSet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBHSet.BackColor = System.Drawing.Color.White;
            this.gbBHSet.Controls.Add(this.gbRegim);
            this.gbBHSet.Controls.Add(this.tbBHK);
            this.gbBHSet.Controls.Add(this.label7);
            this.gbBHSet.Location = new System.Drawing.Point(366, 101);
            this.gbBHSet.Name = "gbBHSet";
            this.gbBHSet.Size = new System.Drawing.Size(350, 267);
            this.gbBHSet.TabIndex = 17;
            this.gbBHSet.TabStop = false;
            this.gbBHSet.Text = " Параметры построения ";
            // 
            // gbRegim
            // 
            this.gbRegim.Controls.Add(this.rbViewByColumn);
            this.gbRegim.Controls.Add(this.rbViewBySector);
            this.gbRegim.Location = new System.Drawing.Point(4, 45);
            this.gbRegim.Name = "gbRegim";
            this.gbRegim.Size = new System.Drawing.Size(342, 78);
            this.gbRegim.TabIndex = 15;
            this.gbRegim.TabStop = false;
            this.gbRegim.Text = " Режим отображения приростов ";
            // 
            // rbViewByColumn
            // 
            this.rbViewByColumn.AutoSize = true;
            this.rbViewByColumn.Checked = true;
            this.rbViewByColumn.Location = new System.Drawing.Point(10, 47);
            this.rbViewByColumn.Name = "rbViewByColumn";
            this.rbViewByColumn.Size = new System.Drawing.Size(175, 19);
            this.rbViewByColumn.TabIndex = 1;
            this.rbViewByColumn.TabStop = true;
            this.rbViewByColumn.Text = "Отображать \"столбиками\"";
            this.rbViewByColumn.UseVisualStyleBackColor = true;
            this.rbViewByColumn.CheckedChanged += new System.EventHandler(this.rbViewByColumn_CheckedChanged);
            // 
            // rbViewBySector
            // 
            this.rbViewBySector.AutoSize = true;
            this.rbViewBySector.Location = new System.Drawing.Point(10, 22);
            this.rbViewBySector.Name = "rbViewBySector";
            this.rbViewBySector.Size = new System.Drawing.Size(167, 19);
            this.rbViewBySector.TabIndex = 0;
            this.rbViewBySector.Text = "Отображать по секторам";
            this.rbViewBySector.UseVisualStyleBackColor = true;
            this.rbViewBySector.CheckedChanged += new System.EventHandler(this.rbViewBySector_CheckedChanged);
            // 
            // tbBHK
            // 
            this.tbBHK.Location = new System.Drawing.Point(223, 16);
            this.tbBHK.Name = "tbBHK";
            this.tbBHK.Size = new System.Drawing.Size(123, 23);
            this.tbBHK.TabIndex = 1;
            this.tbBHK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbBHK.TextChanged += new System.EventHandler(this.tbBHK_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(210, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "Коэффициент пропорциональности:";
            // 
            // gbBHLast
            // 
            this.gbBHLast.BackColor = System.Drawing.Color.White;
            this.gbBHLast.Controls.Add(this.lBHLastInterval);
            this.gbBHLast.Controls.Add(this.label11);
            this.gbBHLast.Controls.Add(this.tbBHLastAveMonth);
            this.gbBHLast.Controls.Add(this.label12);
            this.gbBHLast.Controls.Add(this.cbBHLastDate);
            this.gbBHLast.Controls.Add(this.dtpBHLastDate);
            this.gbBHLast.Location = new System.Drawing.Point(366, 5);
            this.gbBHLast.Name = "gbBHLast";
            this.gbBHLast.Size = new System.Drawing.Size(350, 90);
            this.gbBHLast.TabIndex = 13;
            this.gbBHLast.TabStop = false;
            this.gbBHLast.Text = " Дата окончания расчета ";
            // 
            // lBHLastInterval
            // 
            this.lBHLastInterval.AutoSize = true;
            this.lBHLastInterval.ForeColor = System.Drawing.Color.DimGray;
            this.lBHLastInterval.Location = new System.Drawing.Point(9, 46);
            this.lBHLastInterval.Name = "lBHLastInterval";
            this.lBHLastInterval.Size = new System.Drawing.Size(66, 15);
            this.lBHLastInterval.TabIndex = 19;
            this.lBHLastInterval.Text = "Интревал: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(318, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "мес";
            // 
            // tbBHLastAveMonth
            // 
            this.tbBHLastAveMonth.ForeColor = System.Drawing.Color.Gray;
            this.tbBHLastAveMonth.Location = new System.Drawing.Point(248, 20);
            this.tbBHLastAveMonth.Name = "tbBHLastAveMonth";
            this.tbBHLastAveMonth.Size = new System.Drawing.Size(64, 23);
            this.tbBHLastAveMonth.TabIndex = 17;
            this.tbBHLastAveMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbBHLastAveMonth.TextChanged += new System.EventHandler(this.tbBHLastAveMonth_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(171, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 15);
            this.label12.TabIndex = 16;
            this.label12.Text = "Средний за";
            // 
            // cbBHLastDate
            // 
            this.cbBHLastDate.AutoSize = true;
            this.cbBHLastDate.Checked = true;
            this.cbBHLastDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBHLastDate.Location = new System.Drawing.Point(12, 65);
            this.cbBHLastDate.Name = "cbBHLastDate";
            this.cbBHLastDate.Size = new System.Drawing.Size(228, 19);
            this.cbBHLastDate.TabIndex = 11;
            this.cbBHLastDate.Text = "Использовать последнюю дату МЭР";
            this.cbBHLastDate.UseVisualStyleBackColor = true;
            this.cbBHLastDate.CheckedChanged += new System.EventHandler(this.cbBHLastDate_CheckedChanged);
            // 
            // dtpBHLastDate
            // 
            this.dtpBHLastDate.CustomFormat = "dd.MM.yyyy";
            this.dtpBHLastDate.Enabled = false;
            this.dtpBHLastDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBHLastDate.Location = new System.Drawing.Point(12, 20);
            this.dtpBHLastDate.Name = "dtpBHLastDate";
            this.dtpBHLastDate.Size = new System.Drawing.Size(153, 23);
            this.dtpBHLastDate.TabIndex = 9;
            this.dtpBHLastDate.ValueChanged += new System.EventHandler(this.dtpBHLastDate_ValueChanged);
            // 
            // gbBHStart
            // 
            this.gbBHStart.BackColor = System.Drawing.Color.White;
            this.gbBHStart.Controls.Add(this.lBHStartInterval);
            this.gbBHStart.Controls.Add(this.label8);
            this.gbBHStart.Controls.Add(this.tbBHStartAveMonth);
            this.gbBHStart.Controls.Add(this.label9);
            this.gbBHStart.Controls.Add(this.cbBHStartDate);
            this.gbBHStart.Controls.Add(this.dtpBHStartDate);
            this.gbBHStart.Location = new System.Drawing.Point(10, 5);
            this.gbBHStart.Name = "gbBHStart";
            this.gbBHStart.Size = new System.Drawing.Size(350, 90);
            this.gbBHStart.TabIndex = 12;
            this.gbBHStart.TabStop = false;
            this.gbBHStart.Text = " Дата начала расчета ";
            // 
            // lBHStartInterval
            // 
            this.lBHStartInterval.AutoSize = true;
            this.lBHStartInterval.ForeColor = System.Drawing.Color.DimGray;
            this.lBHStartInterval.Location = new System.Drawing.Point(7, 46);
            this.lBHStartInterval.Name = "lBHStartInterval";
            this.lBHStartInterval.Size = new System.Drawing.Size(66, 15);
            this.lBHStartInterval.TabIndex = 19;
            this.lBHStartInterval.Text = "Интревал: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(316, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 15);
            this.label8.TabIndex = 18;
            this.label8.Text = "мес";
            // 
            // tbBHStartAveMonth
            // 
            this.tbBHStartAveMonth.ForeColor = System.Drawing.Color.Gray;
            this.tbBHStartAveMonth.Location = new System.Drawing.Point(246, 20);
            this.tbBHStartAveMonth.Name = "tbBHStartAveMonth";
            this.tbBHStartAveMonth.Size = new System.Drawing.Size(64, 23);
            this.tbBHStartAveMonth.TabIndex = 17;
            this.tbBHStartAveMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbBHStartAveMonth.TextChanged += new System.EventHandler(this.tbBHStartAveMonth_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(169, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "Средний за";
            // 
            // cbBHStartDate
            // 
            this.cbBHStartDate.AutoSize = true;
            this.cbBHStartDate.Checked = true;
            this.cbBHStartDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBHStartDate.Location = new System.Drawing.Point(10, 65);
            this.cbBHStartDate.Name = "cbBHStartDate";
            this.cbBHStartDate.Size = new System.Drawing.Size(205, 19);
            this.cbBHStartDate.TabIndex = 11;
            this.cbBHStartDate.Text = "Использовать дату начала года";
            this.cbBHStartDate.UseVisualStyleBackColor = true;
            this.cbBHStartDate.CheckedChanged += new System.EventHandler(this.cbBHStartDate_CheckedChanged);
            // 
            // dtpBHStartDate
            // 
            this.dtpBHStartDate.CustomFormat = "dd.MM.yyyy";
            this.dtpBHStartDate.Enabled = false;
            this.dtpBHStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBHStartDate.Location = new System.Drawing.Point(10, 20);
            this.dtpBHStartDate.Name = "dtpBHStartDate";
            this.dtpBHStartDate.Size = new System.Drawing.Size(153, 23);
            this.dtpBHStartDate.TabIndex = 9;
            this.dtpBHStartDate.ValueChanged += new System.EventHandler(this.dtpBHStartDate_ValueChanged);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "bubble2.png");
            this.imgList.Images.SetKeyName(1, "multi_bubble2.png");
            this.imgList.Images.SetKeyName(2, "bubble_hystory2.png");
            // 
            // bOk
            // 
            this.bOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(540, 408);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 1;
            this.bOk.Text = "ОK";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(636, 408);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 2;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 15);
            this.label10.TabIndex = 2;
            // 
            // cbBCGazInThousand
            // 
            this.cbBCGazInThousand.AutoSize = true;
            this.cbBCGazInThousand.Checked = true;
            this.cbBCGazInThousand.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBCGazInThousand.Location = new System.Drawing.Point(10, 37);
            this.cbBCGazInThousand.Name = "cbBCGazInThousand";
            this.cbBCGazInThousand.Size = new System.Drawing.Size(170, 19);
            this.cbBCGazInThousand.TabIndex = 3;
            this.cbBCGazInThousand.Text = "Отображать газ в тыс. м3";
            this.cbBCGazInThousand.UseVisualStyleBackColor = true;
            // 
            // cbBAGazInThousand
            // 
            this.cbBAGazInThousand.AutoSize = true;
            this.cbBAGazInThousand.Checked = true;
            this.cbBAGazInThousand.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBAGazInThousand.Location = new System.Drawing.Point(10, 37);
            this.cbBAGazInThousand.Name = "cbBAGazInThousand";
            this.cbBAGazInThousand.Size = new System.Drawing.Size(170, 19);
            this.cbBAGazInThousand.TabIndex = 4;
            this.cbBAGazInThousand.Text = "Отображать газ в тыс. м3";
            this.cbBAGazInThousand.UseVisualStyleBackColor = true;
            // 
            // BubbleSetting
            // 
            this.AcceptButton = this.bOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(730, 445);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.tabSetting);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BubbleSetting";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки карт отборов";
            this.tabSetting.ResumeLayout(false);
            this.tpBubbleCurrent.ResumeLayout(false);
            this.gbBCSet.ResumeLayout(false);
            this.gbBCSet.PerformLayout();
            this.gbBCLast.ResumeLayout(false);
            this.gbBCLast.PerformLayout();
            this.tpBubbleAccum.ResumeLayout(false);
            this.gbBASet.ResumeLayout(false);
            this.gbBASet.PerformLayout();
            this.gbBALast.ResumeLayout(false);
            this.gbBALast.PerformLayout();
            this.gbBAStart.ResumeLayout(false);
            this.gbBAStart.PerformLayout();
            this.tpBubbleHistory.ResumeLayout(false);
            this.gbDataSource.ResumeLayout(false);
            this.gbDataSource.PerformLayout();
            this.gbBHSet.ResumeLayout(false);
            this.gbBHSet.PerformLayout();
            this.gbRegim.ResumeLayout(false);
            this.gbRegim.PerformLayout();
            this.gbBHLast.ResumeLayout(false);
            this.gbBHLast.PerformLayout();
            this.gbBHStart.ResumeLayout(false);
            this.gbBHStart.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabSetting;
        private System.Windows.Forms.TabPage tpBubbleCurrent;
        private System.Windows.Forms.TabPage tpBubbleAccum;
        private System.Windows.Forms.TabPage tpBubbleHistory;
        private System.Windows.Forms.GroupBox gbBHLast;
        private System.Windows.Forms.CheckBox cbBHLastDate;
        private System.Windows.Forms.DateTimePicker dtpBHLastDate;
        private System.Windows.Forms.GroupBox gbBHStart;
        private System.Windows.Forms.CheckBox cbBHStartDate;
        private System.Windows.Forms.DateTimePicker dtpBHStartDate;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.GroupBox gbBCLast;
        private System.Windows.Forms.CheckBox cbBCLastDate;
        private System.Windows.Forms.DateTimePicker dtpBCLastDate;
        private System.Windows.Forms.GroupBox gbBALast;
        private System.Windows.Forms.CheckBox cbBALastDate;
        private System.Windows.Forms.DateTimePicker dtpBALastDate;
        private System.Windows.Forms.GroupBox gbBAStart;
        private System.Windows.Forms.CheckBox cbBAStartDate;
        private System.Windows.Forms.DateTimePicker dtpBAStartDate;
        private System.Windows.Forms.Label lmonth;
        private System.Windows.Forms.TextBox tbBCLastAveMonth;
        private System.Windows.Forms.Label lAver;
        private System.Windows.Forms.Label lBCLastInterval;
        private System.Windows.Forms.Label lBALastInterval;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbBALastAveMonth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lBAStartInterval;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbBAStartAveMonth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lBHLastInterval;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbBHLastAveMonth;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lBHStartInterval;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbBHStartAveMonth;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox gbBASet;
        private System.Windows.Forms.TextBox tbBAK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbBCSet;
        private System.Windows.Forms.TextBox tbBCK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbBHSet;
        private System.Windows.Forms.GroupBox gbRegim;
        private System.Windows.Forms.RadioButton rbViewByColumn;
        private System.Windows.Forms.RadioButton rbViewBySector;
        private System.Windows.Forms.TextBox tbBHK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbDataSource;
        private System.Windows.Forms.Label dtpSourceComment;
        private System.Windows.Forms.RadioButton rbSourceChess;
        private System.Windows.Forms.RadioButton rbSourceMer;
        private System.Windows.Forms.CheckBox cbBCGazInThousand;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox cbBAGazInThousand;
    }
}