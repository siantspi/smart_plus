﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
namespace SmartPlus
{
    public partial class BubbleSetting : Form
    {
        Project proj;
        public DateTime BCLastDate;
        public DateTime BAStartDate;
        public DateTime BALastDate;
        public DateTime BHStartDate;
        public DateTime BHLastDate;
        public DateTime MaxMerDate;
        public bool BHViewBySector;
        public bool BHDataSource;
        public int UpdateBubbleMap;

        public int BCLastMonth;
        public int BAStartMonth;
        public int BALastMonth;
        public int BHStartMonth;
        public int BHLastMonth;

        public float BCK;
        public float BAK;
        public float BHK;

        bool tbBCLastEmpty;
        bool tbBAStartEmpty;
        bool tbBALastEmpty;
        bool tbBHStartEmpty;
        bool tbBHLastEmpty;

        bool CodeMod;
        public bool CancelClose;
        public Constant.BUBBLE_MAP_TYPE LastBubbleEdit;
        
        public BubbleSetting()
        {
            InitializeComponent();

            proj = null;
            dtpBCLastDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpBAStartDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpBALastDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpBHStartDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpBHLastDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            BCLastDate = DateTime.MaxValue;
            BAStartDate = DateTime.MinValue;
            BALastDate = DateTime.MaxValue;
            BHStartDate = DateTime.MinValue;
            BHLastDate = DateTime.MaxValue;
            BHViewBySector = false;
            UpdateBubbleMap = -1;

            BCK = 0.5f;
            BAK = 0.1f;
            BHK = 0.5f;

            tbBCK.Text = BCK.ToString();
            tbBAK.Text = BAK.ToString();
            tbBHK.Text = BHK.ToString();

            BCLastMonth = 0;
            BAStartMonth = 0;
            BALastMonth = 0;
            BHStartMonth = 0;
            BHLastMonth = 0;

            tbBCLastEmpty = true;
            tbBAStartEmpty = true;
            tbBALastEmpty = true;
            tbBHStartEmpty = true;
            tbBHLastEmpty = true;
            CodeMod = false;
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.NOT_SET;

            tbBCLastAveMonth.KeyDown += new KeyEventHandler(tbBCAverageMonth_KeyDown);
            tbBAStartAveMonth.KeyDown += new KeyEventHandler(tbBAStartAveMonth_KeyDown);
            tbBALastAveMonth.KeyDown += new KeyEventHandler(tbBALastAveMonth_KeyDown);
            tbBHStartAveMonth.KeyDown += new KeyEventHandler(tbBHStartAveMonth_KeyDown);
            tbBHLastAveMonth.KeyDown += new KeyEventHandler(tbBHLastAveMonth_KeyDown);
            this.FormClosing += new FormClosingEventHandler(BubbleSetting_FormClosing);
        }

        void BubbleSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                if (cbBCLastDate.Checked)
                    BCLastDate = DateTime.MaxValue;
                else
                    BCLastDate = dtpBCLastDate.Value;

                if (cbBAStartDate.Checked)
                    BAStartDate = DateTime.MinValue;
                else
                    BAStartDate = dtpBAStartDate.Value;

                if (cbBALastDate.Checked)
                    BALastDate = DateTime.MaxValue;
                else
                    BALastDate = dtpBALastDate.Value;

                if (cbBHStartDate.Checked)
                    BHStartDate = DateTime.MinValue;
                else
                    BHStartDate = dtpBHStartDate.Value;

                if (cbBHLastDate.Checked)
                    BHLastDate = DateTime.MaxValue;
                else
                    BHLastDate = dtpBHLastDate.Value;

                try
                {
                    BCLastMonth = Convert.ToInt32(tbBCLastAveMonth.Text);
                    BAStartMonth = Convert.ToInt32(tbBAStartAveMonth.Text);
                    BALastMonth = Convert.ToInt32(tbBALastAveMonth.Text);
                    BHStartMonth = Convert.ToInt32(tbBHStartAveMonth.Text);
                    BHLastMonth = Convert.ToInt32(tbBHLastAveMonth.Text);
                    BCK = (float)Convert.ToDouble(tbBCK.Text);
                    BAK = (float)Convert.ToDouble(tbBAK.Text);
                    BHK = (float)Convert.ToDouble(tbBHK.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Не все поля заполнены правильно!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
                if (dtpBAStartDate.Value > dtpBALastDate.Value)
                {
                    MessageBox.Show("Дата начала периода карты накопленных отборов долждна быть меньше даты окончания периода!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
                if (dtpBHStartDate.Value > dtpBHLastDate.Value)
                {
                    MessageBox.Show("Дата начала периода карты изменений отборов долждна быть меньше даты окончания периода!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true; 
                }
                if (rbViewBySector.Checked)
                {
                    BHViewBySector = true;
                }
                else if (rbViewByColumn.Checked)
                {
                    BHViewBySector = false;
                } 
            }
            else if (this.DialogResult == DialogResult.Cancel)
            {

                #region Cancel Date
                if (BCLastDate == DateTime.MaxValue)
                {
                    cbBCLastDate.Checked = true;
                }
                else
                {
                    dtpBCLastDate.Value = BCLastDate;
                    cbBCLastDate.Checked = false;
                }
                if (BAStartDate == DateTime.MinValue)
                {
                    cbBAStartDate.Checked = true;
                }
                else
                {
                    dtpBAStartDate.Value = BAStartDate;
                    cbBAStartDate.Checked = false;
                }
                if (BALastDate == DateTime.MaxValue)
                {
                    cbBALastDate.Checked = true;
                }
                else
                {
                    dtpBALastDate.Value = BALastDate;
                    cbBALastDate.Checked = false;
                }
                if (BHStartDate == DateTime.MinValue)
                {
                    cbBHStartDate.Checked = true;
                }
                else
                {
                    dtpBHStartDate.Value = BHStartDate;
                    cbBHStartDate.Checked = false;
                }
                if (BHLastDate == DateTime.MaxValue)
                {
                    cbBHLastDate.Checked = true;
                }
                else
                {
                    dtpBHLastDate.Value = BHLastDate;
                    cbBHLastDate.Checked = false;
                }
                #endregion

                #region Cancel Ave Month
                CodeMod = true;
                tbBCLastAveMonth.Text = "";
                tbBAStartAveMonth.Text = "";
                tbBALastAveMonth.Text = "";
                tbBHStartAveMonth.Text = "";
                tbBHLastAveMonth.Text = "";
                CodeMod = false;

                if (BCLastMonth > 1) tbBCLastAveMonth.Text = BCLastMonth.ToString();
                if (BAStartMonth > 1) tbBAStartAveMonth.Text = BAStartMonth.ToString();
                if (BALastMonth > 1) tbBALastAveMonth.Text = BALastMonth.ToString();
                if (BHStartMonth > 1) tbBHStartAveMonth.Text = BHStartMonth.ToString();
                if (BHLastMonth > 1) tbBHLastAveMonth.Text = BHLastMonth.ToString();
                tbBCLastEmpty = SetMonthInterval(tbBCLastEmpty, cbBCLastDate, dtpBCLastDate, lBCLastInterval, tbBCLastAveMonth, false);
                tbBAStartEmpty = SetMonthInterval(tbBAStartEmpty, cbBAStartDate, dtpBAStartDate, lBAStartInterval, tbBAStartAveMonth, false);
                tbBALastEmpty = SetMonthInterval(tbBALastEmpty, cbBALastDate, dtpBALastDate, lBALastInterval, tbBALastAveMonth, false);
                tbBHStartEmpty = SetMonthInterval(tbBHStartEmpty, cbBHStartDate, dtpBHStartDate, lBHStartInterval, tbBHStartAveMonth, BHDataSource);
                tbBHLastEmpty = SetMonthInterval(tbBHLastEmpty, cbBHLastDate, dtpBHLastDate, lBHLastInterval, tbBHLastAveMonth, BHDataSource);
                #endregion

                if (BHViewBySector)
                    rbViewBySector.Checked = true;
                else
                    rbViewByColumn.Checked = true;

                tbBCK.Text = BCK.ToString();
                tbBAK.Text = BAK.ToString();
                tbBHK.Text = BHK.ToString(); 
            }
        }

        public void SetProject(Project MainProject)
        {
            proj = MainProject;
            SetMinMaxDate(proj.minMerDate, proj.maxMerDate);
            CodeMod = false;
        }
        public void ClearMinMaxDate()
        {
            SetMinMaxDate(proj.minMerDate, proj.maxMerDate);
        }
        public void SetMinMaxDate(DateTime minDT, DateTime maxDT)
        {
            if ((minDT < maxDT) && (minDT.Year != DateTime.MinValue.Year) && (maxDT.Year != DateTime.MaxValue.Year))
            {
                MaxMerDate = maxDT;
                if (minDT != DateTime.MinValue)
                {
                    dtpBCLastDate.MinDate = minDT;
                    dtpBAStartDate.MinDate = minDT;
                    dtpBALastDate.MinDate = minDT;
                    dtpBHStartDate.MinDate = minDT;
                    dtpBHLastDate.MinDate = minDT;
                }
                if (maxDT.Year != 9999)
                {
                    dtpBCLastDate.MaxDate = maxDT.AddMonths(1).AddDays(-1);
                    dtpBAStartDate.MaxDate = maxDT.AddMonths(1).AddDays(-1);
                    dtpBALastDate.MaxDate = maxDT.AddMonths(1).AddDays(-1);
                    if (!BHDataSource)
                    {
                        dtpBHStartDate.MaxDate = maxDT.AddMonths(1).AddDays(-1);
                        dtpBHLastDate.MaxDate = maxDT.AddMonths(1).AddDays(-1);
                    }
                    else
                    {
                        dtpBHStartDate.MaxDate = DateTime.Now;
                        dtpBHLastDate.MaxDate = DateTime.Now;
                    }
                }
                
                dtpBCLastDate.Value = maxDT; BCLastDate = maxDT;
                dtpBAStartDate.Value = minDT; BAStartDate = minDT;
                dtpBALastDate.Value = maxDT; BALastDate = maxDT;
                
                DateTime StartYear = new DateTime(maxDT.Year, 1, 1);

                if (dtpBHStartDate.MinDate <= StartYear)
                {
                    dtpBHStartDate.Value = StartYear;
                    BHStartDate = StartYear;
                }
                else
                {
                    dtpBHStartDate.Value = dtpBHStartDate.MinDate;
                    BHStartDate = dtpBHStartDate.MinDate;
                }
                dtpBHLastDate.Value = dtpBHLastDate.MaxDate;
                BHLastDate = dtpBHLastDate.MaxDate;
            }
        }
        public void ClearProject()
        {
            proj = null;
        }

        public DialogResult ShowSettingDialog()
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.NOT_SET;
            UpdateBubbleMap = -1;
            return this.ShowDialog();
        }

        private void SetCheckBox(CheckBox cb, DateTimePicker dtp, bool MinDate)
        {
            dtp.Enabled = !cb.Checked;
            if (cb.Checked)
            {
                if (MinDate)
                {
                    if (cb == cbBHStartDate)
                        dtp.Value = new DateTime(dtp.MaxDate.Year, 1, 1);
                    else
                        dtp.Value = new DateTime(dtp.MinDate.Year, dtp.MinDate.Month, 1);
                }
                else
                {
                    //dtp.Value = new DateTime(dtp.MaxDate.Year, dtp.MaxDate.Month, 1);
                    dtp.Value = new DateTime(MaxMerDate.Year, MaxMerDate.Month, 1);
                }
            }
        }
        private bool SetMonthInterval(bool IsMonthEmpty, CheckBox cb, DateTimePicker dtp, Label lInterval, TextBox tbMonth, bool ByDays)
        {
            if (CodeMod) return IsMonthEmpty;
            UpdateBubbleMap = 0;
            if (tbMonth.Text == "")
            {
                CodeMod = true;
                IsMonthEmpty = true;
                tbMonth.Text = "1";
                CodeMod = false;
                //tbMonth.SelectionStart = 1;
                tbMonth.ForeColor = Color.Gray;
                lInterval.ForeColor = Color.DimGray;

                lInterval.Text = "Интервал: " + dtp.Value.Date.ToShortDateString();
            }
            else
            {
                lInterval.ForeColor = Color.DimGray;
                string str = tbMonth.Text;
                int month;
                DateTime dtMin;
                try
                {
                    month = Convert.ToInt32(str);
                    if (month > 1)
                    {
                        if (ByDays)
                        {
                            dtMin = dtp.Value.AddDays(-month + 1);
                        }
                        else
                        {
                            dtMin = dtp.Value.AddMonths(-month + 1);
                        }
                        if(dtMin < dtp.MinDate) dtMin = dtp.MinDate;
                        lInterval.Text = "Интервал: " + dtMin.Date.ToShortDateString() +
                                                " - " + dtp.Value.Date.ToShortDateString();
                    }
                    else
                    {
                        lInterval.Text = "Интервал: " + dtp.Value.Date.ToShortDateString();
                    }
                    tbMonth.ForeColor = Color.Black;
                }
                catch (Exception)
                {
                    lInterval.Text = "Введите правильное число месяцев!";
                    lInterval.ForeColor = Color.Red;
                }
            }
            return IsMonthEmpty;
        }
        private void MyKeyDown(ref bool Empty, TextBox tb, ref KeyEventArgs e)
        {
            UpdateBubbleMap = 0;
            if ((Empty) && ((e.KeyCode == Keys.D0) || (e.KeyCode == Keys.NumPad0)))
                e.SuppressKeyPress = true;
            else if (Empty)
            {
                Empty = false;
                CodeMod = true;
                tb.Text = "";
                CodeMod = false;
            }
        }

        // OK Cancel
        private void bOk_Click(object sender, EventArgs e)
        {

        }
        private void bCancel_Click(object sender, EventArgs e)
        {

        }

        private void cbBCLastDate_CheckedChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT;
            SetCheckBox(cbBCLastDate, dtpBCLastDate, false);
            tbBCLastEmpty = SetMonthInterval(tbBCLastEmpty, cbBCLastDate, dtpBCLastDate, lBCLastInterval, tbBCLastAveMonth, false);
        }
        private void cbBAStartDate_CheckedChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
            SetCheckBox(cbBAStartDate, dtpBAStartDate, true);
            tbBAStartEmpty = SetMonthInterval(tbBAStartEmpty, cbBAStartDate, dtpBAStartDate, lBAStartInterval, tbBAStartAveMonth, false);
        }
        private void cbBALastDate_CheckedChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
            SetCheckBox(cbBALastDate, dtpBALastDate, false);
            tbBALastEmpty = SetMonthInterval(tbBALastEmpty, cbBALastDate, dtpBALastDate, lBALastInterval, tbBALastAveMonth, false);
        }
        private void cbBHStartDate_CheckedChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            SetCheckBox(cbBHStartDate, dtpBHStartDate, true);
            tbBHStartEmpty = SetMonthInterval(tbBHStartEmpty, cbBHStartDate, dtpBHStartDate, lBHStartInterval, tbBHStartAveMonth, BHDataSource);
        }
        private void cbBHLastDate_CheckedChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            SetCheckBox(cbBHLastDate, dtpBHLastDate, false);
            tbBHLastEmpty = SetMonthInterval(tbBHLastEmpty, cbBHLastDate, dtpBHLastDate, lBHLastInterval, tbBHLastAveMonth, BHDataSource);
        }

        private void dtpBCLastDate_ValueChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT;
            DateTime dt = new DateTime(dtpBCLastDate.Value.Year, dtpBCLastDate.Value.Month, 1);
            if (dt < dtpBCLastDate.MinDate) dt = dtpBCLastDate.MinDate;
            dtpBCLastDate.Value = dt;
            tbBCLastEmpty = SetMonthInterval(tbBCLastEmpty, cbBCLastDate, dtpBCLastDate, lBCLastInterval, tbBCLastAveMonth, false);
        }
        private void dtpBAStartDate_ValueChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
            DateTime dt = new DateTime(dtpBAStartDate.Value.Year, dtpBAStartDate.Value.Month, 1);
            if (dt < dtpBAStartDate.MinDate) dt = dtpBAStartDate.MinDate;
            dtpBAStartDate.Value = dt;
            tbBAStartEmpty = SetMonthInterval(tbBAStartEmpty, cbBAStartDate, dtpBAStartDate, lBAStartInterval, tbBAStartAveMonth, false);
        }
        private void dtpBALastDate_ValueChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
            DateTime dt = new DateTime(dtpBALastDate.Value.Year, dtpBALastDate.Value.Month, 1);
            if (dt < dtpBALastDate.MinDate) dt = dtpBALastDate.MinDate;
            dtpBALastDate.Value = dt;
            tbBALastEmpty = SetMonthInterval(tbBALastEmpty, cbBALastDate, dtpBALastDate, lBALastInterval, tbBALastAveMonth,false);
        }
        private void dtpBHStartDate_ValueChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            DateTime dt;
            if (!BHDataSource)
            {
                dt = new DateTime(dtpBHStartDate.Value.Year, dtpBHStartDate.Value.Month, 1);
            }
            else
            {
                dt = dtpBHStartDate.Value;
            }
            if (dt < dtpBHStartDate.MinDate) dt = dtpBHStartDate.MinDate;
            dtpBHStartDate.Value = dt;
            tbBHStartEmpty = SetMonthInterval(tbBHStartEmpty, cbBHStartDate, dtpBHStartDate, lBHStartInterval, tbBHStartAveMonth, BHDataSource);
        }
        private void dtpBHLastDate_ValueChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            DateTime dt;
            if (!BHDataSource)
            {
                dt = new DateTime(dtpBHLastDate.Value.Year, dtpBHLastDate.Value.Month, 1);
            }
            else
            {
                dt = dtpBHLastDate.Value;
            }
            if (dt < dtpBHLastDate.MinDate) dt = dtpBHLastDate.MinDate;
            dtpBHLastDate.Value = dt;
            tbBHLastEmpty = SetMonthInterval(tbBHLastEmpty, cbBHLastDate, dtpBHLastDate, lBHLastInterval, tbBHLastAveMonth, BHDataSource);
        }

        private void tbBCAverageMonth_TextChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT;
            tbBCLastEmpty = SetMonthInterval(tbBCLastEmpty, cbBCLastDate, dtpBCLastDate, lBCLastInterval, tbBCLastAveMonth, false);
        }
        private void tbBAStartAveMonth_TextChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
            tbBAStartEmpty = SetMonthInterval(tbBAStartEmpty, cbBAStartDate, dtpBAStartDate, lBAStartInterval, tbBAStartAveMonth, false);
        }
        private void tbBALastAveMonth_TextChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
            tbBALastEmpty = SetMonthInterval(tbBALastEmpty, cbBALastDate, dtpBALastDate, lBALastInterval, tbBALastAveMonth, false);
        }
        private void tbBHStartAveMonth_TextChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            tbBHStartEmpty = SetMonthInterval(tbBHStartEmpty, cbBHStartDate, dtpBHStartDate, lBHStartInterval, tbBHStartAveMonth, BHDataSource);
        }
        private void tbBHLastAveMonth_TextChanged(object sender, EventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            tbBHLastEmpty = SetMonthInterval(tbBHLastEmpty, cbBHLastDate, dtpBHLastDate, lBHLastInterval, tbBHLastAveMonth, BHDataSource);
        }

        void tbBCAverageMonth_KeyDown(object sender, KeyEventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT;
            MyKeyDown(ref tbBCLastEmpty, tbBCLastAveMonth, ref e);
        }
        void tbBAStartAveMonth_KeyDown(object sender, KeyEventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
            MyKeyDown(ref tbBAStartEmpty, tbBAStartAveMonth, ref e);
        }
        void tbBALastAveMonth_KeyDown(object sender, KeyEventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
            MyKeyDown(ref tbBALastEmpty, tbBALastAveMonth, ref e);
        }
        void tbBHStartAveMonth_KeyDown(object sender, KeyEventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            MyKeyDown(ref tbBHStartEmpty, tbBHStartAveMonth, ref e);
        }
        void tbBHLastAveMonth_KeyDown(object sender, KeyEventArgs e)
        {
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            MyKeyDown(ref tbBHLastEmpty, tbBHLastAveMonth, ref e);
        }

        private void rbViewBySector_CheckedChanged(object sender, EventArgs e)
        {
            if (UpdateBubbleMap == -1) UpdateBubbleMap = 1;
            if ((LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.NOT_SET) && (LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY))
                UpdateBubbleMap = 0;
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
        }
        private void rbViewByColumn_CheckedChanged(object sender, EventArgs e)
        {
            if (UpdateBubbleMap == -1) UpdateBubbleMap = 1;
            if ((LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.NOT_SET) && (LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY))
                UpdateBubbleMap = 0;
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
        }

        private void tbBCK_TextChanged(object sender, EventArgs e)
        {
            if (UpdateBubbleMap == -1) UpdateBubbleMap = 1;
            if ((LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.NOT_SET) && (LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT))
                UpdateBubbleMap = 0;
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT;
            
        }
        private void tbBAK_TextChanged(object sender, EventArgs e)
        {
            if (UpdateBubbleMap == -1) UpdateBubbleMap = 1;
            if ((LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.NOT_SET) && (LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM))
                UpdateBubbleMap = 0;
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM;
        }
        private void tbBHK_TextChanged(object sender, EventArgs e)
        {
            if (UpdateBubbleMap == -1) UpdateBubbleMap = 1;
            if ((LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.NOT_SET) && (LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY))
                UpdateBubbleMap = 0;
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
        }

        private void rbSourceMer_CheckedChanged(object sender, EventArgs e)
        {
            if (UpdateBubbleMap == -1) UpdateBubbleMap = 1;
            if ((LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.NOT_SET) && (LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY))
                UpdateBubbleMap = 0;
            BHDataSource = false;
            dtpSourceComment.Text = string.Empty;
            label8.Text = "мес.";
            label11.Text = "мес.";
            dtpBHStartDate.MaxDate = MaxMerDate;
            dtpBHLastDate.MaxDate = MaxMerDate;
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            tbBHStartEmpty = SetMonthInterval(tbBHStartEmpty, cbBHStartDate, dtpBHStartDate, lBHStartInterval, tbBHStartAveMonth, BHDataSource);
            tbBHLastEmpty = SetMonthInterval(tbBHLastEmpty, cbBHLastDate, dtpBHLastDate, lBHLastInterval, tbBHLastAveMonth, BHDataSource);
        }
        private void rbSourceChess_CheckedChanged(object sender, EventArgs e)
        {
            if (UpdateBubbleMap == -1) UpdateBubbleMap = 1;
            if ((LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.NOT_SET) && (LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY))
                UpdateBubbleMap = 0;
            BHDataSource = true;
            dtpSourceComment.Text = "Карта отборов по шахматке будет\nотображаться только для всех пластов!";
            label8.Text = "дн.";
            label11.Text = "дн.";
            dtpBHStartDate.MaxDate = DateTime.Now;
            dtpBHLastDate.MaxDate = DateTime.Now;
            LastBubbleEdit = Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY;
            tbBHStartEmpty = SetMonthInterval(tbBHStartEmpty, cbBHStartDate, dtpBHStartDate, lBHStartInterval, tbBHStartAveMonth, BHDataSource);
            tbBHLastEmpty = SetMonthInterval(tbBHLastEmpty, cbBHLastDate, dtpBHLastDate, lBHLastInterval, tbBHLastAveMonth, BHDataSource);
        }
    }
}
