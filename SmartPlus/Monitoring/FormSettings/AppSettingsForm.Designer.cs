﻿namespace SmartPlus
{
    partial class AppSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Общие");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Цвета объектов разработки");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Графики");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Обновление");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.twSettingTitle = new System.Windows.Forms.TreeView();
            this.pPlastColors = new System.Windows.Forms.Panel();
            this.dgvOilObjColor = new System.Windows.Forms.DataGridView();
            this.N = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clr = new System.Windows.Forms.DataGridViewImageColumn();
            this.intClr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ind = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsModified = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.CommonPanel = new System.Windows.Forms.Panel();
            this.CommonSet_AttachAreasToFilter = new System.Windows.Forms.CheckBox();
            this.Common_Set_LiquidInCube = new System.Windows.Forms.CheckBox();
            this.CommonSet_FillOilfieldNgduRegion = new System.Windows.Forms.CheckBox();
            this.CommonSet_OpenProjectFolderButton = new System.Windows.Forms.Button();
            this.CommonSet_HideWellsWithoutCoordByFilter = new System.Windows.Forms.CheckBox();
            this.CommonSet_ProjectFolderLabel = new System.Windows.Forms.Label();
            this.CommonSet_ShowProjectFolderLabel = new System.Windows.Forms.Label();
            this.CommonSet_AttachContousToFilter = new System.Windows.Forms.CheckBox();
            this.Common_Set_CopyImageFormatBox = new System.Windows.Forms.ComboBox();
            this.Common_Set_lCopyFormat = new System.Windows.Forms.Label();
            this.CommonSet_ShowAllAreas = new System.Windows.Forms.CheckBox();
            this.ChartPanel = new System.Windows.Forms.Panel();
            this.Chart_CalcZabPressureAverage = new System.Windows.Forms.CheckBox();
            this.Chart_UseInjWellsPressure = new System.Windows.Forms.CheckBox();
            this.UpdaterPanel = new System.Windows.Forms.Panel();
            this.UpdateHTTPS_labelConnectionState = new System.Windows.Forms.Label();
            this.Update_Load_Colors_CheckBox = new System.Windows.Forms.CheckBox();
            this.Chek_Connection_Button = new System.Windows.Forms.Button();
            this.Update_Server_TextBox = new System.Windows.Forms.TextBox();
            this.Update_UserName_TextBox = new System.Windows.Forms.TextBox();
            this.Update_Password_TextBox = new System.Windows.Forms.TextBox();
            this.Update_Pasword_Label = new System.Windows.Forms.Label();
            this.Update_UserName_Label = new System.Windows.Forms.Label();
            this.Update_HTTPS_CheckBox = new System.Windows.Forms.CheckBox();
            this.Update_Server_Label = new System.Windows.Forms.Label();
            this.pPlastColors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOilObjColor)).BeginInit();
            this.CommonPanel.SuspendLayout();
            this.ChartPanel.SuspendLayout();
            this.UpdaterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // twSettingTitle
            // 
            this.twSettingTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.twSettingTitle.HideSelection = false;
            this.twSettingTitle.Location = new System.Drawing.Point(3, 3);
            this.twSettingTitle.Name = "twSettingTitle";
            treeNode1.Name = "Common";
            treeNode1.Text = "Общие";
            treeNode2.Name = "OilObjColors";
            treeNode2.Text = "Цвета объектов разработки";
            treeNode3.Name = "Chart";
            treeNode3.Text = "Графики";
            treeNode4.Name = "Update";
            treeNode4.Text = "Обновление";
            this.twSettingTitle.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4});
            this.twSettingTitle.ShowLines = false;
            this.twSettingTitle.ShowRootLines = false;
            this.twSettingTitle.Size = new System.Drawing.Size(179, 482);
            this.twSettingTitle.TabIndex = 0;
            this.twSettingTitle.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.twSettingTitle_BeforeSelect);
            this.twSettingTitle.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.twSettingTitle_AfterSelect);
            // 
            // pPlastColors
            // 
            this.pPlastColors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pPlastColors.Controls.Add(this.dgvOilObjColor);
            this.pPlastColors.Location = new System.Drawing.Point(188, 3);
            this.pPlastColors.Name = "pPlastColors";
            this.pPlastColors.Size = new System.Drawing.Size(518, 442);
            this.pPlastColors.TabIndex = 1;
            // 
            // dgvOilObjColor
            // 
            this.dgvOilObjColor.BackgroundColor = System.Drawing.Color.White;
            this.dgvOilObjColor.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOilObjColor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvOilObjColor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOilObjColor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.N,
            this.plName,
            this.clr,
            this.intClr,
            this.ind,
            this.IsModified});
            this.dgvOilObjColor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOilObjColor.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvOilObjColor.Location = new System.Drawing.Point(0, 0);
            this.dgvOilObjColor.Name = "dgvOilObjColor";
            this.dgvOilObjColor.RowHeadersVisible = false;
            this.dgvOilObjColor.RowTemplate.Height = 18;
            this.dgvOilObjColor.Size = new System.Drawing.Size(518, 442);
            this.dgvOilObjColor.TabIndex = 0;
            this.dgvOilObjColor.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOilObjColor_CellDoubleClick);
            // 
            // N
            // 
            this.N.HeaderText = "№";
            this.N.Name = "N";
            this.N.Width = 50;
            // 
            // plName
            // 
            this.plName.HeaderText = "Пласт";
            this.plName.Name = "plName";
            this.plName.Width = 200;
            // 
            // clr
            // 
            this.clr.HeaderText = "Цвет";
            this.clr.Name = "clr";
            this.clr.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // intClr
            // 
            this.intClr.HeaderText = "intClr";
            this.intClr.Name = "intClr";
            this.intClr.Visible = false;
            // 
            // ind
            // 
            this.ind.HeaderText = "Index";
            this.ind.Name = "ind";
            this.ind.Visible = false;
            // 
            // IsModified
            // 
            this.IsModified.HeaderText = "IsModified";
            this.IsModified.Name = "IsModified";
            this.IsModified.Visible = false;
            // 
            // bOk
            // 
            this.bOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(520, 451);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 2;
            this.bOk.Text = "OK";
            this.bOk.UseVisualStyleBackColor = true;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(616, 451);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 3;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // CommonPanel
            // 
            this.CommonPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CommonPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CommonPanel.Controls.Add(this.CommonSet_AttachAreasToFilter);
            this.CommonPanel.Controls.Add(this.Common_Set_LiquidInCube);
            this.CommonPanel.Controls.Add(this.CommonSet_FillOilfieldNgduRegion);
            this.CommonPanel.Controls.Add(this.CommonSet_OpenProjectFolderButton);
            this.CommonPanel.Controls.Add(this.CommonSet_HideWellsWithoutCoordByFilter);
            this.CommonPanel.Controls.Add(this.CommonSet_ProjectFolderLabel);
            this.CommonPanel.Controls.Add(this.CommonSet_ShowProjectFolderLabel);
            this.CommonPanel.Controls.Add(this.CommonSet_AttachContousToFilter);
            this.CommonPanel.Controls.Add(this.Common_Set_CopyImageFormatBox);
            this.CommonPanel.Controls.Add(this.Common_Set_lCopyFormat);
            this.CommonPanel.Controls.Add(this.CommonSet_ShowAllAreas);
            this.CommonPanel.Location = new System.Drawing.Point(188, 3);
            this.CommonPanel.Name = "CommonPanel";
            this.CommonPanel.Size = new System.Drawing.Size(518, 442);
            this.CommonPanel.TabIndex = 4;
            // 
            // CommonSet_AttachAreasToFilter
            // 
            this.CommonSet_AttachAreasToFilter.AutoSize = true;
            this.CommonSet_AttachAreasToFilter.Location = new System.Drawing.Point(3, 32);
            this.CommonSet_AttachAreasToFilter.Name = "CommonSet_AttachAreasToFilter";
            this.CommonSet_AttachAreasToFilter.Size = new System.Drawing.Size(370, 17);
            this.CommonSet_AttachAreasToFilter.TabIndex = 11;
            this.CommonSet_AttachAreasToFilter.Text = "Привязать отображение ячеек заводнения к фильтру объектов";
            this.CommonSet_AttachAreasToFilter.UseVisualStyleBackColor = true;
            // 
            // Common_Set_LiquidInCube
            // 
            this.Common_Set_LiquidInCube.AutoSize = true;
            this.Common_Set_LiquidInCube.Location = new System.Drawing.Point(3, 127);
            this.Common_Set_LiquidInCube.Name = "Common_Set_LiquidInCube";
            this.Common_Set_LiquidInCube.Size = new System.Drawing.Size(234, 17);
            this.Common_Set_LiquidInCube.TabIndex = 8;
            this.Common_Set_LiquidInCube.Text = "Отображать данные по жидкости в м3";
            this.Common_Set_LiquidInCube.UseVisualStyleBackColor = true;
            // 
            // CommonSet_FillOilfieldNgduRegion
            // 
            this.CommonSet_FillOilfieldNgduRegion.AutoSize = true;
            this.CommonSet_FillOilfieldNgduRegion.Location = new System.Drawing.Point(3, 104);
            this.CommonSet_FillOilfieldNgduRegion.Name = "CommonSet_FillOilfieldNgduRegion";
            this.CommonSet_FillOilfieldNgduRegion.Size = new System.Drawing.Size(391, 17);
            this.CommonSet_FillOilfieldNgduRegion.TabIndex = 7;
            this.CommonSet_FillOilfieldNgduRegion.Text = "Закрашивать области НГДУ и месторождений при отдалении карты";
            this.CommonSet_FillOilfieldNgduRegion.UseVisualStyleBackColor = true;
            // 
            // CommonSet_OpenProjectFolderButton
            // 
            this.CommonSet_OpenProjectFolderButton.Location = new System.Drawing.Point(3, 229);
            this.CommonSet_OpenProjectFolderButton.Name = "CommonSet_OpenProjectFolderButton";
            this.CommonSet_OpenProjectFolderButton.Size = new System.Drawing.Size(166, 24);
            this.CommonSet_OpenProjectFolderButton.TabIndex = 10;
            this.CommonSet_OpenProjectFolderButton.Text = "Открыть папку проекта";
            this.CommonSet_OpenProjectFolderButton.UseVisualStyleBackColor = true;
            this.CommonSet_OpenProjectFolderButton.Click += new System.EventHandler(this.CommonSet_OpenProjectFolderButton_Click);
            // 
            // CommonSet_HideWellsWithoutCoordByFilter
            // 
            this.CommonSet_HideWellsWithoutCoordByFilter.AutoSize = true;
            this.CommonSet_HideWellsWithoutCoordByFilter.Location = new System.Drawing.Point(3, 80);
            this.CommonSet_HideWellsWithoutCoordByFilter.Name = "CommonSet_HideWellsWithoutCoordByFilter";
            this.CommonSet_HideWellsWithoutCoordByFilter.Size = new System.Drawing.Size(483, 17);
            this.CommonSet_HideWellsWithoutCoordByFilter.TabIndex = 5;
            this.CommonSet_HideWellsWithoutCoordByFilter.Text = "Скрывать скважины не имеющие пластопересечения на объект в фильтре объектов";
            this.CommonSet_HideWellsWithoutCoordByFilter.UseVisualStyleBackColor = true;
            // 
            // CommonSet_ProjectFolderLabel
            // 
            this.CommonSet_ProjectFolderLabel.AutoSize = true;
            this.CommonSet_ProjectFolderLabel.Location = new System.Drawing.Point(3, 213);
            this.CommonSet_ProjectFolderLabel.Name = "CommonSet_ProjectFolderLabel";
            this.CommonSet_ProjectFolderLabel.Size = new System.Drawing.Size(59, 13);
            this.CommonSet_ProjectFolderLabel.TabIndex = 9;
            this.CommonSet_ProjectFolderLabel.Text = "NoAvaible";
            // 
            // CommonSet_ShowProjectFolderLabel
            // 
            this.CommonSet_ShowProjectFolderLabel.AutoSize = true;
            this.CommonSet_ShowProjectFolderLabel.Location = new System.Drawing.Point(0, 188);
            this.CommonSet_ShowProjectFolderLabel.Name = "CommonSet_ShowProjectFolderLabel";
            this.CommonSet_ShowProjectFolderLabel.Size = new System.Drawing.Size(172, 13);
            this.CommonSet_ShowProjectFolderLabel.TabIndex = 8;
            this.CommonSet_ShowProjectFolderLabel.Text = "Расположение папки проекта:";
            // 
            // CommonSet_AttachContousToFilter
            // 
            this.CommonSet_AttachContousToFilter.AutoSize = true;
            this.CommonSet_AttachContousToFilter.Location = new System.Drawing.Point(3, 57);
            this.CommonSet_AttachContousToFilter.Name = "CommonSet_AttachContousToFilter";
            this.CommonSet_AttachContousToFilter.Size = new System.Drawing.Size(323, 17);
            this.CommonSet_AttachContousToFilter.TabIndex = 4;
            this.CommonSet_AttachContousToFilter.Text = "Привязать отображение контуров к фильтру объектов";
            this.CommonSet_AttachContousToFilter.UseVisualStyleBackColor = true;
            // 
            // Common_Set_CopyImageFormatBox
            // 
            this.Common_Set_CopyImageFormatBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Common_Set_CopyImageFormatBox.FormattingEnabled = true;
            this.Common_Set_CopyImageFormatBox.Items.AddRange(new object[] {
            "BMP",
            "EMF+"});
            this.Common_Set_CopyImageFormatBox.Location = new System.Drawing.Point(301, 155);
            this.Common_Set_CopyImageFormatBox.Name = "Common_Set_CopyImageFormatBox";
            this.Common_Set_CopyImageFormatBox.Size = new System.Drawing.Size(99, 21);
            this.Common_Set_CopyImageFormatBox.TabIndex = 3;
            // 
            // Common_Set_lCopyFormat
            // 
            this.Common_Set_lCopyFormat.AutoSize = true;
            this.Common_Set_lCopyFormat.Location = new System.Drawing.Point(2, 158);
            this.Common_Set_lCopyFormat.Name = "Common_Set_lCopyFormat";
            this.Common_Set_lCopyFormat.Size = new System.Drawing.Size(293, 13);
            this.Common_Set_lCopyFormat.TabIndex = 2;
            this.Common_Set_lCopyFormat.Text = "Формат копирования изображения в буфер обмена:";
            // 
            // CommonSet_ShowAllAreas
            // 
            this.CommonSet_ShowAllAreas.AutoSize = true;
            this.CommonSet_ShowAllAreas.Location = new System.Drawing.Point(3, 9);
            this.CommonSet_ShowAllAreas.Name = "CommonSet_ShowAllAreas";
            this.CommonSet_ShowAllAreas.Size = new System.Drawing.Size(301, 17);
            this.CommonSet_ShowAllAreas.TabIndex = 1;
            this.CommonSet_ShowAllAreas.Text = "Отображать ячейки заводнения по всем объектам";
            this.CommonSet_ShowAllAreas.UseVisualStyleBackColor = true;
            // 
            // ChartPanel
            // 
            this.ChartPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChartPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ChartPanel.Controls.Add(this.Chart_CalcZabPressureAverage);
            this.ChartPanel.Controls.Add(this.Chart_UseInjWellsPressure);
            this.ChartPanel.Location = new System.Drawing.Point(188, 3);
            this.ChartPanel.Name = "ChartPanel";
            this.ChartPanel.Size = new System.Drawing.Size(518, 442);
            this.ChartPanel.TabIndex = 7;
            // 
            // Chart_CalcZabPressureAverage
            // 
            this.Chart_CalcZabPressureAverage.AutoSize = true;
            this.Chart_CalcZabPressureAverage.Location = new System.Drawing.Point(5, 32);
            this.Chart_CalcZabPressureAverage.Name = "Chart_CalcZabPressureAverage";
            this.Chart_CalcZabPressureAverage.Size = new System.Drawing.Size(312, 17);
            this.Chart_CalcZabPressureAverage.TabIndex = 17;
            this.Chart_CalcZabPressureAverage.Text = "Рассчитывать среднее значение забойного давления";
            this.Chart_CalcZabPressureAverage.UseVisualStyleBackColor = true;
            // 
            // Chart_UseInjWellsPressure
            // 
            this.Chart_UseInjWellsPressure.AutoSize = true;
            this.Chart_UseInjWellsPressure.Location = new System.Drawing.Point(4, 9);
            this.Chart_UseInjWellsPressure.Name = "Chart_UseInjWellsPressure";
            this.Chart_UseInjWellsPressure.Size = new System.Drawing.Size(387, 17);
            this.Chart_UseInjWellsPressure.TabIndex = 16;
            this.Chart_UseInjWellsPressure.Text = "Учитывать нагнетательные скважины в рассчете средних давлений";
            this.Chart_UseInjWellsPressure.UseVisualStyleBackColor = true;
            // 
            // UpdaterPanel
            // 
            this.UpdaterPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UpdaterPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UpdaterPanel.Controls.Add(this.UpdateHTTPS_labelConnectionState);
            this.UpdaterPanel.Controls.Add(this.Update_Load_Colors_CheckBox);
            this.UpdaterPanel.Controls.Add(this.Chek_Connection_Button);
            this.UpdaterPanel.Controls.Add(this.Update_Server_TextBox);
            this.UpdaterPanel.Controls.Add(this.Update_UserName_TextBox);
            this.UpdaterPanel.Controls.Add(this.Update_Password_TextBox);
            this.UpdaterPanel.Controls.Add(this.Update_Pasword_Label);
            this.UpdaterPanel.Controls.Add(this.Update_UserName_Label);
            this.UpdaterPanel.Controls.Add(this.Update_HTTPS_CheckBox);
            this.UpdaterPanel.Controls.Add(this.Update_Server_Label);
            this.UpdaterPanel.Location = new System.Drawing.Point(188, 3);
            this.UpdaterPanel.Name = "UpdaterPanel";
            this.UpdaterPanel.Size = new System.Drawing.Size(518, 442);
            this.UpdaterPanel.TabIndex = 18;
            // 
            // UpdateHTTPS_labelConnectionState
            // 
            this.UpdateHTTPS_labelConnectionState.AutoSize = true;
            this.UpdateHTTPS_labelConnectionState.Font = new System.Drawing.Font("Arial Black", 8.5F);
            this.UpdateHTTPS_labelConnectionState.Location = new System.Drawing.Point(3, 171);
            this.UpdateHTTPS_labelConnectionState.Name = "UpdateHTTPS_labelConnectionState";
            this.UpdateHTTPS_labelConnectionState.Size = new System.Drawing.Size(117, 17);
            this.UpdateHTTPS_labelConnectionState.TabIndex = 9;
            this.UpdateHTTPS_labelConnectionState.Text = "ConnectionState";
            // 
            // Update_Load_Colors_CheckBox
            // 
            this.Update_Load_Colors_CheckBox.AutoSize = true;
            this.Update_Load_Colors_CheckBox.Location = new System.Drawing.Point(6, 119);
            this.Update_Load_Colors_CheckBox.Name = "Update_Load_Colors_CheckBox";
            this.Update_Load_Colors_CheckBox.Size = new System.Drawing.Size(234, 17);
            this.Update_Load_Colors_CheckBox.TabIndex = 8;
            this.Update_Load_Colors_CheckBox.Text = "Загружать цвета объектов разработки";
            this.Update_Load_Colors_CheckBox.UseVisualStyleBackColor = true;
            // 
            // Chek_Connection_Button
            // 
            this.Chek_Connection_Button.Location = new System.Drawing.Point(2, 142);
            this.Chek_Connection_Button.Name = "Chek_Connection_Button";
            this.Chek_Connection_Button.Size = new System.Drawing.Size(150, 26);
            this.Chek_Connection_Button.TabIndex = 7;
            this.Chek_Connection_Button.Text = "Проверить соединение";
            this.Chek_Connection_Button.UseVisualStyleBackColor = true;
            this.Chek_Connection_Button.Click += new System.EventHandler(this.Chek_Connection_Button_Click);
            // 
            // Update_Server_TextBox
            // 
            this.Update_Server_TextBox.Location = new System.Drawing.Point(126, 37);
            this.Update_Server_TextBox.Name = "Update_Server_TextBox";
            this.Update_Server_TextBox.Size = new System.Drawing.Size(131, 22);
            this.Update_Server_TextBox.TabIndex = 6;
            // 
            // Update_UserName_TextBox
            // 
            this.Update_UserName_TextBox.Location = new System.Drawing.Point(126, 64);
            this.Update_UserName_TextBox.Name = "Update_UserName_TextBox";
            this.Update_UserName_TextBox.Size = new System.Drawing.Size(131, 22);
            this.Update_UserName_TextBox.TabIndex = 4;
            // 
            // Update_Password_TextBox
            // 
            this.Update_Password_TextBox.Location = new System.Drawing.Point(126, 92);
            this.Update_Password_TextBox.Name = "Update_Password_TextBox";
            this.Update_Password_TextBox.Size = new System.Drawing.Size(131, 22);
            this.Update_Password_TextBox.TabIndex = 5;
            this.Update_Password_TextBox.UseSystemPasswordChar = true;
            // 
            // Update_Pasword_Label
            // 
            this.Update_Pasword_Label.AutoSize = true;
            this.Update_Pasword_Label.Location = new System.Drawing.Point(3, 92);
            this.Update_Pasword_Label.Name = "Update_Pasword_Label";
            this.Update_Pasword_Label.Size = new System.Drawing.Size(50, 13);
            this.Update_Pasword_Label.TabIndex = 3;
            this.Update_Pasword_Label.Text = "Пароль:";
            // 
            // Update_UserName_Label
            // 
            this.Update_UserName_Label.AutoSize = true;
            this.Update_UserName_Label.Location = new System.Drawing.Point(2, 64);
            this.Update_UserName_Label.Name = "Update_UserName_Label";
            this.Update_UserName_Label.Size = new System.Drawing.Size(108, 13);
            this.Update_UserName_Label.TabIndex = 2;
            this.Update_UserName_Label.Text = "Имя пользователя:";
            // 
            // Update_HTTPS_CheckBox
            // 
            this.Update_HTTPS_CheckBox.AutoSize = true;
            this.Update_HTTPS_CheckBox.Location = new System.Drawing.Point(6, 12);
            this.Update_HTTPS_CheckBox.Name = "Update_HTTPS_CheckBox";
            this.Update_HTTPS_CheckBox.Size = new System.Drawing.Size(161, 17);
            this.Update_HTTPS_CheckBox.TabIndex = 1;
            this.Update_HTTPS_CheckBox.Text = "Обновление через HTTPS";
            this.Update_HTTPS_CheckBox.UseVisualStyleBackColor = true;
            // 
            // Update_Server_Label
            // 
            this.Update_Server_Label.AutoSize = true;
            this.Update_Server_Label.Location = new System.Drawing.Point(3, 36);
            this.Update_Server_Label.Name = "Update_Server_Label";
            this.Update_Server_Label.Size = new System.Drawing.Size(119, 13);
            this.Update_Server_Label.TabIndex = 0;
            this.Update_Server_Label.Text = "Сервер обновлений:";
            // 
            // AppSettingsForm
            // 
            this.AcceptButton = this.bOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(709, 487);
            this.Controls.Add(this.CommonPanel);
            this.Controls.Add(this.ChartPanel);
            this.Controls.Add(this.UpdaterPanel);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.pPlastColors);
            this.Controls.Add(this.twSettingTitle);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AppSettingsForm";
            this.ShowInTaskbar = false;
            this.Text = "Настройки приложения";
            this.pPlastColors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOilObjColor)).EndInit();
            this.CommonPanel.ResumeLayout(false);
            this.CommonPanel.PerformLayout();
            this.ChartPanel.ResumeLayout(false);
            this.ChartPanel.PerformLayout();
            this.UpdaterPanel.ResumeLayout(false);
            this.UpdaterPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView twSettingTitle;
        private System.Windows.Forms.Panel pPlastColors;
        private System.Windows.Forms.DataGridView dgvOilObjColor;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn N;
        private System.Windows.Forms.DataGridViewTextBoxColumn plName;
        private System.Windows.Forms.DataGridViewImageColumn clr;
        private System.Windows.Forms.DataGridViewTextBoxColumn intClr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ind;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsModified;
        private System.Windows.Forms.Panel CommonPanel;
        private System.Windows.Forms.CheckBox CommonSet_ShowAllAreas;
        private System.Windows.Forms.ComboBox Common_Set_CopyImageFormatBox;
        private System.Windows.Forms.Label Common_Set_lCopyFormat;
        private System.Windows.Forms.CheckBox CommonSet_AttachContousToFilter;
        private System.Windows.Forms.CheckBox CommonSet_HideWellsWithoutCoordByFilter;
        private System.Windows.Forms.Panel ChartPanel;
        private System.Windows.Forms.CheckBox Chart_CalcZabPressureAverage;
        private System.Windows.Forms.CheckBox Chart_UseInjWellsPressure;
        private System.Windows.Forms.Panel UpdaterPanel;
        private System.Windows.Forms.TextBox Update_Server_TextBox;
        private System.Windows.Forms.TextBox Update_UserName_TextBox;
        private System.Windows.Forms.TextBox Update_Password_TextBox;
        private System.Windows.Forms.Label Update_Pasword_Label;
        private System.Windows.Forms.Label Update_UserName_Label;
        private System.Windows.Forms.CheckBox Update_HTTPS_CheckBox;
        private System.Windows.Forms.Label Update_Server_Label;
        private System.Windows.Forms.CheckBox Common_Set_LiquidInCube;
        private System.Windows.Forms.CheckBox CommonSet_FillOilfieldNgduRegion;
        private System.Windows.Forms.Button Chek_Connection_Button;
        private System.Windows.Forms.Button CommonSet_OpenProjectFolderButton;
        private System.Windows.Forms.Label CommonSet_ProjectFolderLabel;
        private System.Windows.Forms.Label CommonSet_ShowProjectFolderLabel;
        private System.Windows.Forms.CheckBox Update_Load_Colors_CheckBox;
        private System.Windows.Forms.Label UpdateHTTPS_labelConnectionState;
        private System.Windows.Forms.CheckBox CommonSet_AttachAreasToFilter;
    }
}