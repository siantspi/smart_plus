﻿namespace SmartPlus
{
    partial class CorSchemeSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Глубины");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("ГИС");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Керн");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Исследования ЗКЦ");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("История перфорации");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("ГИС, Керн, ЗКЦ", new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Перфорация");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("НГК");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("ИК");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("ГК");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("БК");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("ПС");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Объекты ГИС");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Каротаж+Объекты ГИС", new System.Windows.Forms.TreeNode[] {
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13});
            this.ColumnsTree = new System.Windows.Forms.TreeView();
            this.DepthSettingsGroup = new System.Windows.Forms.GroupBox();
            this.MarkerGroup = new System.Windows.Forms.GroupBox();
            this.ShowMarkersInfo = new System.Windows.Forms.CheckBox();
            this.ShowMarkersConnection = new System.Windows.Forms.CheckBox();
            this.DepthBottomSpace = new System.Windows.Forms.NumericUpDown();
            this.DepthTopSpace = new System.Windows.Forms.NumericUpDown();
            this.lDepthBottomSpace = new System.Windows.Forms.Label();
            this.lDepthTopSpace = new System.Windows.Forms.Label();
            this.DepthColumnGroup = new System.Windows.Forms.GroupBox();
            this.DepthColMaxUse = new System.Windows.Forms.CheckBox();
            this.DepthColMinUse = new System.Windows.Forms.CheckBox();
            this.DepthColMax = new System.Windows.Forms.NumericUpDown();
            this.DepthColMin = new System.Windows.Forms.NumericUpDown();
            this.GisSettingsGroup = new System.Windows.Forms.GroupBox();
            this.GisColumnGroup = new System.Windows.Forms.GroupBox();
            this.GisColMaxUse = new System.Windows.Forms.CheckBox();
            this.GisColMinUse = new System.Windows.Forms.CheckBox();
            this.GisColMax = new System.Windows.Forms.NumericUpDown();
            this.GisColMin = new System.Windows.Forms.NumericUpDown();
            this.LogsSettingsGroup = new System.Windows.Forms.GroupBox();
            this.ColumnGroup = new System.Windows.Forms.GroupBox();
            this.LogColumnMaxUse = new System.Windows.Forms.CheckBox();
            this.LogColumnMinUse = new System.Windows.Forms.CheckBox();
            this.LogColumnMax = new System.Windows.Forms.NumericUpDown();
            this.LogColumnMin = new System.Windows.Forms.NumericUpDown();
            this.ScaleGroup = new System.Windows.Forms.GroupBox();
            this.ScaleMinMaxGroup = new System.Windows.Forms.GroupBox();
            this.LogScaleMax = new System.Windows.Forms.NumericUpDown();
            this.LogScaleMin = new System.Windows.Forms.NumericUpDown();
            this.LogScaleStep = new System.Windows.Forms.NumericUpDown();
            this.lScaleStep = new System.Windows.Forms.Label();
            this.LogScaleMinMaxManualByStep = new System.Windows.Forms.CheckBox();
            this.lScaleMax = new System.Windows.Forms.Label();
            this.lScaleMin = new System.Windows.Forms.Label();
            this.LogScaleMinMaxManual = new System.Windows.Forms.RadioButton();
            this.LogScaleMinMaxAuto = new System.Windows.Forms.RadioButton();
            this.LogScaleType = new System.Windows.Forms.ComboBox();
            this.lScaleType = new System.Windows.Forms.Label();
            this.DrawGroup = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LogGridHorizontUse = new System.Windows.Forms.CheckBox();
            this.LogGridVerticalUse = new System.Windows.Forms.CheckBox();
            this.LogLineWidth = new System.Windows.Forms.NumericUpDown();
            this.SpottingGroup = new System.Windows.Forms.GroupBox();
            this.LogBackInvert = new System.Windows.Forms.CheckBox();
            this.lSpottingColor = new System.Windows.Forms.Label();
            this.LogBackTransparent = new System.Windows.Forms.NumericUpDown();
            this.lSpottingTrans = new System.Windows.Forms.Label();
            this.LogBackUse = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lLineWidth = new System.Windows.Forms.Label();
            this.LogMask = new System.Windows.Forms.TextBox();
            this.lMask = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.PerfSettingsGroup = new System.Windows.Forms.GroupBox();
            this.PerfColumnGroup = new System.Windows.Forms.GroupBox();
            this.PerfColMaxUse = new System.Windows.Forms.CheckBox();
            this.PerfColMinUse = new System.Windows.Forms.CheckBox();
            this.PerfColMax = new System.Windows.Forms.NumericUpDown();
            this.PerfColMin = new System.Windows.Forms.NumericUpDown();
            this.ObjGisSettingsGroup = new System.Windows.Forms.GroupBox();
            this.ObjGisColumnGroup = new System.Windows.Forms.GroupBox();
            this.ObjGisMaxUse = new System.Windows.Forms.CheckBox();
            this.ObjGisMinUse = new System.Windows.Forms.CheckBox();
            this.ObjGisMax = new System.Windows.Forms.NumericUpDown();
            this.ObjGisMin = new System.Windows.Forms.NumericUpDown();
            this.DepthCalcInitFromTrajectory = new System.Windows.Forms.CheckBox();
            this.DepthSettingsGroup.SuspendLayout();
            this.MarkerGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepthBottomSpace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepthTopSpace)).BeginInit();
            this.DepthColumnGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepthColMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepthColMin)).BeginInit();
            this.GisSettingsGroup.SuspendLayout();
            this.GisColumnGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GisColMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GisColMin)).BeginInit();
            this.LogsSettingsGroup.SuspendLayout();
            this.ColumnGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogColumnMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogColumnMin)).BeginInit();
            this.ScaleGroup.SuspendLayout();
            this.ScaleMinMaxGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogScaleMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogScaleMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogScaleStep)).BeginInit();
            this.DrawGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogLineWidth)).BeginInit();
            this.SpottingGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogBackTransparent)).BeginInit();
            this.PerfSettingsGroup.SuspendLayout();
            this.PerfColumnGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PerfColMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PerfColMin)).BeginInit();
            this.ObjGisSettingsGroup.SuspendLayout();
            this.ObjGisColumnGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObjGisMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObjGisMin)).BeginInit();
            this.SuspendLayout();
            // 
            // ColumnsTree
            // 
            this.ColumnsTree.CheckBoxes = true;
            this.ColumnsTree.Dock = System.Windows.Forms.DockStyle.Left;
            this.ColumnsTree.FullRowSelect = true;
            this.ColumnsTree.HideSelection = false;
            this.ColumnsTree.Location = new System.Drawing.Point(0, 0);
            this.ColumnsTree.Name = "ColumnsTree";
            treeNode1.Checked = true;
            treeNode1.Name = "DepthColumn";
            treeNode1.Text = "Глубины";
            treeNode2.Checked = true;
            treeNode2.Name = "Gis";
            treeNode2.Text = "ГИС";
            treeNode3.Checked = true;
            treeNode3.Name = "Core";
            treeNode3.Text = "Керн";
            treeNode4.Checked = true;
            treeNode4.Name = "Leak";
            treeNode4.Text = "Исследования ЗКЦ";
            treeNode5.Name = "PerfHistory";
            treeNode5.Text = "История перфорации";
            treeNode6.Checked = true;
            treeNode6.Name = "GisColumn";
            treeNode6.Text = "ГИС, Керн, ЗКЦ";
            treeNode7.Checked = true;
            treeNode7.Name = "PerfColumn";
            treeNode7.Text = "Перфорация";
            treeNode8.Checked = true;
            treeNode8.Name = "LogNGK";
            treeNode8.Text = "НГК";
            treeNode9.Checked = true;
            treeNode9.Name = "LogIK";
            treeNode9.Text = "ИК";
            treeNode10.Checked = true;
            treeNode10.Name = "LogGK";
            treeNode10.Text = "ГК";
            treeNode11.Checked = true;
            treeNode11.Name = "LogBK";
            treeNode11.Text = "БК";
            treeNode12.Checked = true;
            treeNode12.Name = "LogPS";
            treeNode12.Text = "ПС";
            treeNode13.Checked = true;
            treeNode13.Name = "LogObjGIS";
            treeNode13.Text = "Объекты ГИС";
            treeNode14.Checked = true;
            treeNode14.Name = "LogColumn";
            treeNode14.Text = "Каротаж+Объекты ГИС";
            this.ColumnsTree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode6,
            treeNode7,
            treeNode14});
            this.ColumnsTree.ShowLines = false;
            this.ColumnsTree.Size = new System.Drawing.Size(181, 515);
            this.ColumnsTree.TabIndex = 0;
            // 
            // DepthSettingsGroup
            // 
            this.DepthSettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DepthSettingsGroup.Controls.Add(this.MarkerGroup);
            this.DepthSettingsGroup.Controls.Add(this.DepthCalcInitFromTrajectory);
            this.DepthSettingsGroup.Controls.Add(this.DepthBottomSpace);
            this.DepthSettingsGroup.Controls.Add(this.DepthTopSpace);
            this.DepthSettingsGroup.Controls.Add(this.lDepthBottomSpace);
            this.DepthSettingsGroup.Controls.Add(this.lDepthTopSpace);
            this.DepthSettingsGroup.Controls.Add(this.DepthColumnGroup);
            this.DepthSettingsGroup.Location = new System.Drawing.Point(187, 0);
            this.DepthSettingsGroup.Name = "DepthSettingsGroup";
            this.DepthSettingsGroup.Size = new System.Drawing.Size(539, 467);
            this.DepthSettingsGroup.TabIndex = 1;
            this.DepthSettingsGroup.TabStop = false;
            this.DepthSettingsGroup.Text = " Настройки - Глубины ";
            // 
            // MarkerGroup
            // 
            this.MarkerGroup.Controls.Add(this.ShowMarkersInfo);
            this.MarkerGroup.Controls.Add(this.ShowMarkersConnection);
            this.MarkerGroup.Location = new System.Drawing.Point(1, 199);
            this.MarkerGroup.Name = "MarkerGroup";
            this.MarkerGroup.Size = new System.Drawing.Size(529, 87);
            this.MarkerGroup.TabIndex = 25;
            this.MarkerGroup.TabStop = false;
            this.MarkerGroup.Text = " Маркеры ";
            // 
            // ShowMarkersInfo
            // 
            this.ShowMarkersInfo.AutoSize = true;
            this.ShowMarkersInfo.Location = new System.Drawing.Point(5, 48);
            this.ShowMarkersInfo.Name = "ShowMarkersInfo";
            this.ShowMarkersInfo.Size = new System.Drawing.Size(231, 19);
            this.ShowMarkersInfo.TabIndex = 25;
            this.ShowMarkersInfo.Text = "Отображать информацию маркеров";
            this.ShowMarkersInfo.UseVisualStyleBackColor = true;
            // 
            // ShowMarkersConnection
            // 
            this.ShowMarkersConnection.AutoSize = true;
            this.ShowMarkersConnection.Location = new System.Drawing.Point(5, 23);
            this.ShowMarkersConnection.Name = "ShowMarkersConnection";
            this.ShowMarkersConnection.Size = new System.Drawing.Size(252, 19);
            this.ShowMarkersConnection.TabIndex = 24;
            this.ShowMarkersConnection.Text = "Соединять соседние маркеры корсхемы";
            this.ShowMarkersConnection.UseVisualStyleBackColor = true;
            // 
            // DepthBottomSpace
            // 
            this.DepthBottomSpace.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.DepthBottomSpace.Location = new System.Drawing.Point(227, 168);
            this.DepthBottomSpace.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.DepthBottomSpace.Name = "DepthBottomSpace";
            this.DepthBottomSpace.Size = new System.Drawing.Size(120, 23);
            this.DepthBottomSpace.TabIndex = 23;
            // 
            // DepthTopSpace
            // 
            this.DepthTopSpace.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.DepthTopSpace.Location = new System.Drawing.Point(227, 139);
            this.DepthTopSpace.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.DepthTopSpace.Name = "DepthTopSpace";
            this.DepthTopSpace.Size = new System.Drawing.Size(120, 23);
            this.DepthTopSpace.TabIndex = 22;
            // 
            // lDepthBottomSpace
            // 
            this.lDepthBottomSpace.AutoSize = true;
            this.lDepthBottomSpace.Location = new System.Drawing.Point(3, 170);
            this.lDepthBottomSpace.Name = "lDepthBottomSpace";
            this.lDepthBottomSpace.Size = new System.Drawing.Size(216, 15);
            this.lDepthBottomSpace.TabIndex = 21;
            this.lDepthBottomSpace.Text = "Отступ от нижней абс.отметки ГИС, м";
            // 
            // lDepthTopSpace
            // 
            this.lDepthTopSpace.AutoSize = true;
            this.lDepthTopSpace.Location = new System.Drawing.Point(3, 141);
            this.lDepthTopSpace.Name = "lDepthTopSpace";
            this.lDepthTopSpace.Size = new System.Drawing.Size(218, 15);
            this.lDepthTopSpace.TabIndex = 19;
            this.lDepthTopSpace.Text = "Отступ от верхней абс.отметки ГИС, м";
            // 
            // DepthColumnGroup
            // 
            this.DepthColumnGroup.Controls.Add(this.DepthColMaxUse);
            this.DepthColumnGroup.Controls.Add(this.DepthColMinUse);
            this.DepthColumnGroup.Controls.Add(this.DepthColMax);
            this.DepthColumnGroup.Controls.Add(this.DepthColMin);
            this.DepthColumnGroup.Location = new System.Drawing.Point(5, 16);
            this.DepthColumnGroup.Name = "DepthColumnGroup";
            this.DepthColumnGroup.Size = new System.Drawing.Size(529, 79);
            this.DepthColumnGroup.TabIndex = 18;
            this.DepthColumnGroup.TabStop = false;
            this.DepthColumnGroup.Text = "Ширина колонки:";
            // 
            // DepthColMaxUse
            // 
            this.DepthColMaxUse.AutoSize = true;
            this.DepthColMaxUse.Location = new System.Drawing.Point(6, 51);
            this.DepthColMaxUse.Name = "DepthColMaxUse";
            this.DepthColMaxUse.Size = new System.Drawing.Size(90, 19);
            this.DepthColMaxUse.TabIndex = 7;
            this.DepthColMaxUse.Text = "Максимум:";
            this.DepthColMaxUse.UseVisualStyleBackColor = true;
            // 
            // DepthColMinUse
            // 
            this.DepthColMinUse.AutoSize = true;
            this.DepthColMinUse.Location = new System.Drawing.Point(6, 22);
            this.DepthColMinUse.Name = "DepthColMinUse";
            this.DepthColMinUse.Size = new System.Drawing.Size(85, 19);
            this.DepthColMinUse.TabIndex = 6;
            this.DepthColMinUse.Text = "Минимум:";
            this.DepthColMinUse.UseVisualStyleBackColor = true;
            // 
            // DepthColMax
            // 
            this.DepthColMax.Location = new System.Drawing.Point(101, 50);
            this.DepthColMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.DepthColMax.Name = "DepthColMax";
            this.DepthColMax.Size = new System.Drawing.Size(120, 23);
            this.DepthColMax.TabIndex = 5;
            // 
            // DepthColMin
            // 
            this.DepthColMin.Location = new System.Drawing.Point(101, 21);
            this.DepthColMin.Name = "DepthColMin";
            this.DepthColMin.Size = new System.Drawing.Size(120, 23);
            this.DepthColMin.TabIndex = 3;
            // 
            // GisSettingsGroup
            // 
            this.GisSettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GisSettingsGroup.Controls.Add(this.GisColumnGroup);
            this.GisSettingsGroup.Location = new System.Drawing.Point(187, 0);
            this.GisSettingsGroup.Name = "GisSettingsGroup";
            this.GisSettingsGroup.Size = new System.Drawing.Size(539, 467);
            this.GisSettingsGroup.TabIndex = 6;
            this.GisSettingsGroup.TabStop = false;
            this.GisSettingsGroup.Text = " Настройки - ГИС, Керн, ЗКЦ";
            // 
            // GisColumnGroup
            // 
            this.GisColumnGroup.Controls.Add(this.GisColMaxUse);
            this.GisColumnGroup.Controls.Add(this.GisColMinUse);
            this.GisColumnGroup.Controls.Add(this.GisColMax);
            this.GisColumnGroup.Controls.Add(this.GisColMin);
            this.GisColumnGroup.Location = new System.Drawing.Point(5, 16);
            this.GisColumnGroup.Name = "GisColumnGroup";
            this.GisColumnGroup.Size = new System.Drawing.Size(529, 79);
            this.GisColumnGroup.TabIndex = 18;
            this.GisColumnGroup.TabStop = false;
            this.GisColumnGroup.Text = "Ширина колонки:";
            // 
            // GisColMaxUse
            // 
            this.GisColMaxUse.AutoSize = true;
            this.GisColMaxUse.Location = new System.Drawing.Point(6, 51);
            this.GisColMaxUse.Name = "GisColMaxUse";
            this.GisColMaxUse.Size = new System.Drawing.Size(90, 19);
            this.GisColMaxUse.TabIndex = 7;
            this.GisColMaxUse.Text = "Максимум:";
            this.GisColMaxUse.UseVisualStyleBackColor = true;
            // 
            // GisColMinUse
            // 
            this.GisColMinUse.AutoSize = true;
            this.GisColMinUse.Location = new System.Drawing.Point(6, 22);
            this.GisColMinUse.Name = "GisColMinUse";
            this.GisColMinUse.Size = new System.Drawing.Size(85, 19);
            this.GisColMinUse.TabIndex = 6;
            this.GisColMinUse.Text = "Минимум:";
            this.GisColMinUse.UseVisualStyleBackColor = true;
            // 
            // GisColMax
            // 
            this.GisColMax.Location = new System.Drawing.Point(101, 50);
            this.GisColMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.GisColMax.Name = "GisColMax";
            this.GisColMax.Size = new System.Drawing.Size(120, 23);
            this.GisColMax.TabIndex = 5;
            // 
            // GisColMin
            // 
            this.GisColMin.Location = new System.Drawing.Point(101, 21);
            this.GisColMin.Name = "GisColMin";
            this.GisColMin.Size = new System.Drawing.Size(120, 23);
            this.GisColMin.TabIndex = 3;
            // 
            // LogsSettingsGroup
            // 
            this.LogsSettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LogsSettingsGroup.Controls.Add(this.ColumnGroup);
            this.LogsSettingsGroup.Controls.Add(this.ScaleGroup);
            this.LogsSettingsGroup.Controls.Add(this.DrawGroup);
            this.LogsSettingsGroup.Controls.Add(this.LogMask);
            this.LogsSettingsGroup.Controls.Add(this.lMask);
            this.LogsSettingsGroup.Location = new System.Drawing.Point(188, 0);
            this.LogsSettingsGroup.Name = "LogsSettingsGroup";
            this.LogsSettingsGroup.Size = new System.Drawing.Size(539, 467);
            this.LogsSettingsGroup.TabIndex = 5;
            this.LogsSettingsGroup.TabStop = false;
            this.LogsSettingsGroup.Text = " Настройки - Каротаж";
            // 
            // ColumnGroup
            // 
            this.ColumnGroup.Controls.Add(this.LogColumnMaxUse);
            this.ColumnGroup.Controls.Add(this.LogColumnMinUse);
            this.ColumnGroup.Controls.Add(this.LogColumnMax);
            this.ColumnGroup.Controls.Add(this.LogColumnMin);
            this.ColumnGroup.Location = new System.Drawing.Point(4, 45);
            this.ColumnGroup.Name = "ColumnGroup";
            this.ColumnGroup.Size = new System.Drawing.Size(529, 79);
            this.ColumnGroup.TabIndex = 17;
            this.ColumnGroup.TabStop = false;
            this.ColumnGroup.Text = "Ширина колонки:";
            // 
            // LogColumnMaxUse
            // 
            this.LogColumnMaxUse.AutoSize = true;
            this.LogColumnMaxUse.Location = new System.Drawing.Point(6, 51);
            this.LogColumnMaxUse.Name = "LogColumnMaxUse";
            this.LogColumnMaxUse.Size = new System.Drawing.Size(90, 19);
            this.LogColumnMaxUse.TabIndex = 7;
            this.LogColumnMaxUse.Text = "Максимум:";
            this.LogColumnMaxUse.UseVisualStyleBackColor = true;
            // 
            // LogColumnMinUse
            // 
            this.LogColumnMinUse.AutoSize = true;
            this.LogColumnMinUse.Location = new System.Drawing.Point(6, 22);
            this.LogColumnMinUse.Name = "LogColumnMinUse";
            this.LogColumnMinUse.Size = new System.Drawing.Size(85, 19);
            this.LogColumnMinUse.TabIndex = 6;
            this.LogColumnMinUse.Text = "Минимум:";
            this.LogColumnMinUse.UseVisualStyleBackColor = true;
            // 
            // LogColumnMax
            // 
            this.LogColumnMax.Location = new System.Drawing.Point(101, 50);
            this.LogColumnMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.LogColumnMax.Name = "LogColumnMax";
            this.LogColumnMax.Size = new System.Drawing.Size(120, 23);
            this.LogColumnMax.TabIndex = 5;
            // 
            // LogColumnMin
            // 
            this.LogColumnMin.Location = new System.Drawing.Point(101, 21);
            this.LogColumnMin.Name = "LogColumnMin";
            this.LogColumnMin.Size = new System.Drawing.Size(120, 23);
            this.LogColumnMin.TabIndex = 3;
            // 
            // ScaleGroup
            // 
            this.ScaleGroup.Controls.Add(this.ScaleMinMaxGroup);
            this.ScaleGroup.Controls.Add(this.LogScaleType);
            this.ScaleGroup.Controls.Add(this.lScaleType);
            this.ScaleGroup.Location = new System.Drawing.Point(6, 290);
            this.ScaleGroup.Name = "ScaleGroup";
            this.ScaleGroup.Size = new System.Drawing.Size(529, 173);
            this.ScaleGroup.TabIndex = 16;
            this.ScaleGroup.TabStop = false;
            this.ScaleGroup.Text = " Шкала ";
            // 
            // ScaleMinMaxGroup
            // 
            this.ScaleMinMaxGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ScaleMinMaxGroup.Controls.Add(this.LogScaleMax);
            this.ScaleMinMaxGroup.Controls.Add(this.LogScaleMin);
            this.ScaleMinMaxGroup.Controls.Add(this.LogScaleStep);
            this.ScaleMinMaxGroup.Controls.Add(this.lScaleStep);
            this.ScaleMinMaxGroup.Controls.Add(this.LogScaleMinMaxManualByStep);
            this.ScaleMinMaxGroup.Controls.Add(this.lScaleMax);
            this.ScaleMinMaxGroup.Controls.Add(this.lScaleMin);
            this.ScaleMinMaxGroup.Controls.Add(this.LogScaleMinMaxManual);
            this.ScaleMinMaxGroup.Controls.Add(this.LogScaleMinMaxAuto);
            this.ScaleMinMaxGroup.Location = new System.Drawing.Point(289, 9);
            this.ScaleMinMaxGroup.Name = "ScaleMinMaxGroup";
            this.ScaleMinMaxGroup.Size = new System.Drawing.Size(236, 158);
            this.ScaleMinMaxGroup.TabIndex = 9;
            this.ScaleMinMaxGroup.TabStop = false;
            this.ScaleMinMaxGroup.Text = " Граничные значения ";
            // 
            // LogScaleMax
            // 
            this.LogScaleMax.Enabled = false;
            this.LogScaleMax.Location = new System.Drawing.Point(85, 129);
            this.LogScaleMax.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.LogScaleMax.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.LogScaleMax.Name = "LogScaleMax";
            this.LogScaleMax.Size = new System.Drawing.Size(145, 23);
            this.LogScaleMax.TabIndex = 12;
            // 
            // LogScaleMin
            // 
            this.LogScaleMin.Enabled = false;
            this.LogScaleMin.Location = new System.Drawing.Point(85, 100);
            this.LogScaleMin.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.LogScaleMin.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.LogScaleMin.Name = "LogScaleMin";
            this.LogScaleMin.Size = new System.Drawing.Size(145, 23);
            this.LogScaleMin.TabIndex = 9;
            // 
            // LogScaleStep
            // 
            this.LogScaleStep.Enabled = false;
            this.LogScaleStep.Location = new System.Drawing.Point(85, 70);
            this.LogScaleStep.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.LogScaleStep.Name = "LogScaleStep";
            this.LogScaleStep.Size = new System.Drawing.Size(145, 23);
            this.LogScaleStep.TabIndex = 8;
            // 
            // lScaleStep
            // 
            this.lScaleStep.AutoSize = true;
            this.lScaleStep.Enabled = false;
            this.lScaleStep.Location = new System.Drawing.Point(6, 72);
            this.lScaleStep.Name = "lScaleStep";
            this.lScaleStep.Size = new System.Drawing.Size(33, 15);
            this.lScaleStep.TabIndex = 7;
            this.lScaleStep.Text = "Шаг:";
            // 
            // LogScaleMinMaxManualByStep
            // 
            this.LogScaleMinMaxManualByStep.AutoSize = true;
            this.LogScaleMinMaxManualByStep.Enabled = false;
            this.LogScaleMinMaxManualByStep.Location = new System.Drawing.Point(6, 47);
            this.LogScaleMinMaxManualByStep.Name = "LogScaleMinMaxManualByStep";
            this.LogScaleMinMaxManualByStep.Size = new System.Drawing.Size(192, 19);
            this.LogScaleMinMaxManualByStep.TabIndex = 6;
            this.LogScaleMinMaxManualByStep.Text = "Задать с шагом от минимума";
            this.LogScaleMinMaxManualByStep.UseVisualStyleBackColor = true;
            // 
            // lScaleMax
            // 
            this.lScaleMax.AutoSize = true;
            this.lScaleMax.Enabled = false;
            this.lScaleMax.Location = new System.Drawing.Point(6, 131);
            this.lScaleMax.Name = "lScaleMax";
            this.lScaleMax.Size = new System.Drawing.Size(71, 15);
            this.lScaleMax.TabIndex = 3;
            this.lScaleMax.Text = "Максимум:";
            // 
            // lScaleMin
            // 
            this.lScaleMin.AutoSize = true;
            this.lScaleMin.Enabled = false;
            this.lScaleMin.Location = new System.Drawing.Point(6, 102);
            this.lScaleMin.Name = "lScaleMin";
            this.lScaleMin.Size = new System.Drawing.Size(66, 15);
            this.lScaleMin.TabIndex = 2;
            this.lScaleMin.Text = "Минимум:";
            // 
            // LogScaleMinMaxManual
            // 
            this.LogScaleMinMaxManual.AutoSize = true;
            this.LogScaleMinMaxManual.Location = new System.Drawing.Point(99, 22);
            this.LogScaleMinMaxManual.Name = "LogScaleMinMaxManual";
            this.LogScaleMinMaxManual.Size = new System.Drawing.Size(131, 19);
            this.LogScaleMinMaxManual.TabIndex = 1;
            this.LogScaleMinMaxManual.Text = "Заданные вручную";
            this.LogScaleMinMaxManual.UseVisualStyleBackColor = true;
            // 
            // LogScaleMinMaxAuto
            // 
            this.LogScaleMinMaxAuto.AutoSize = true;
            this.LogScaleMinMaxAuto.Checked = true;
            this.LogScaleMinMaxAuto.Location = new System.Drawing.Point(22, 22);
            this.LogScaleMinMaxAuto.Name = "LogScaleMinMaxAuto";
            this.LogScaleMinMaxAuto.Size = new System.Drawing.Size(50, 19);
            this.LogScaleMinMaxAuto.TabIndex = 0;
            this.LogScaleMinMaxAuto.TabStop = true;
            this.LogScaleMinMaxAuto.Text = "Авто";
            this.LogScaleMinMaxAuto.UseVisualStyleBackColor = true;
            // 
            // LogScaleType
            // 
            this.LogScaleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LogScaleType.FormattingEnabled = true;
            this.LogScaleType.ItemHeight = 15;
            this.LogScaleType.Items.AddRange(new object[] {
            "Линейная",
            "Логарифмическая (Log10)",
            "Обратная линейная",
            "Обратная логарифмическая (Log10)"});
            this.LogScaleType.Location = new System.Drawing.Point(41, 22);
            this.LogScaleType.MaxDropDownItems = 4;
            this.LogScaleType.Name = "LogScaleType";
            this.LogScaleType.Size = new System.Drawing.Size(235, 23);
            this.LogScaleType.TabIndex = 8;
            // 
            // lScaleType
            // 
            this.lScaleType.AutoSize = true;
            this.lScaleType.Location = new System.Drawing.Point(6, 25);
            this.lScaleType.Name = "lScaleType";
            this.lScaleType.Size = new System.Drawing.Size(29, 15);
            this.lScaleType.TabIndex = 7;
            this.lScaleType.Text = "Тип:";
            // 
            // DrawGroup
            // 
            this.DrawGroup.Controls.Add(this.label1);
            this.DrawGroup.Controls.Add(this.LogGridHorizontUse);
            this.DrawGroup.Controls.Add(this.LogGridVerticalUse);
            this.DrawGroup.Controls.Add(this.LogLineWidth);
            this.DrawGroup.Controls.Add(this.SpottingGroup);
            this.DrawGroup.Controls.Add(this.label4);
            this.DrawGroup.Controls.Add(this.lLineWidth);
            this.DrawGroup.Location = new System.Drawing.Point(4, 123);
            this.DrawGroup.Name = "DrawGroup";
            this.DrawGroup.Size = new System.Drawing.Size(529, 168);
            this.DrawGroup.TabIndex = 15;
            this.DrawGroup.TabStop = false;
            this.DrawGroup.Text = " Настройка отображения ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 22;
            this.label1.Text = "Тип линии:";
            // 
            // LogGridHorizontUse
            // 
            this.LogGridHorizontUse.AutoSize = true;
            this.LogGridHorizontUse.Location = new System.Drawing.Point(9, 139);
            this.LogGridHorizontUse.Name = "LogGridHorizontUse";
            this.LogGridHorizontUse.Size = new System.Drawing.Size(223, 19);
            this.LogGridHorizontUse.TabIndex = 21;
            this.LogGridHorizontUse.Text = "Отображать горизонтальную сетку";
            this.LogGridHorizontUse.UseVisualStyleBackColor = true;
            // 
            // LogGridVerticalUse
            // 
            this.LogGridVerticalUse.AutoSize = true;
            this.LogGridVerticalUse.Location = new System.Drawing.Point(9, 114);
            this.LogGridVerticalUse.Name = "LogGridVerticalUse";
            this.LogGridVerticalUse.Size = new System.Drawing.Size(209, 19);
            this.LogGridVerticalUse.TabIndex = 20;
            this.LogGridVerticalUse.Text = "Отображать вертикальную сетку";
            this.LogGridVerticalUse.UseVisualStyleBackColor = true;
            // 
            // LogLineWidth
            // 
            this.LogLineWidth.Location = new System.Drawing.Point(110, 22);
            this.LogLineWidth.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.LogLineWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LogLineWidth.Name = "LogLineWidth";
            this.LogLineWidth.Size = new System.Drawing.Size(171, 23);
            this.LogLineWidth.TabIndex = 19;
            this.LogLineWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // SpottingGroup
            // 
            this.SpottingGroup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SpottingGroup.Controls.Add(this.LogBackInvert);
            this.SpottingGroup.Controls.Add(this.lSpottingColor);
            this.SpottingGroup.Controls.Add(this.LogBackTransparent);
            this.SpottingGroup.Controls.Add(this.lSpottingTrans);
            this.SpottingGroup.Controls.Add(this.LogBackUse);
            this.SpottingGroup.Location = new System.Drawing.Point(287, 9);
            this.SpottingGroup.Name = "SpottingGroup";
            this.SpottingGroup.Size = new System.Drawing.Size(236, 152);
            this.SpottingGroup.TabIndex = 18;
            this.SpottingGroup.TabStop = false;
            this.SpottingGroup.Text = " Заливка ";
            // 
            // LogBackInvert
            // 
            this.LogBackInvert.AutoSize = true;
            this.LogBackInvert.Enabled = false;
            this.LogBackInvert.Location = new System.Drawing.Point(9, 101);
            this.LogBackInvert.Name = "LogBackInvert";
            this.LogBackInvert.Size = new System.Drawing.Size(158, 19);
            this.LogBackInvert.TabIndex = 5;
            this.LogBackInvert.Text = "Инвертировать заливку";
            this.LogBackInvert.UseVisualStyleBackColor = true;
            // 
            // lSpottingColor
            // 
            this.lSpottingColor.AutoSize = true;
            this.lSpottingColor.Enabled = false;
            this.lSpottingColor.Location = new System.Drawing.Point(6, 74);
            this.lSpottingColor.Name = "lSpottingColor";
            this.lSpottingColor.Size = new System.Drawing.Size(35, 15);
            this.lSpottingColor.TabIndex = 3;
            this.lSpottingColor.Text = "Цвет:";
            // 
            // LogBackTransparent
            // 
            this.LogBackTransparent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LogBackTransparent.Enabled = false;
            this.LogBackTransparent.Location = new System.Drawing.Point(116, 38);
            this.LogBackTransparent.Name = "LogBackTransparent";
            this.LogBackTransparent.Size = new System.Drawing.Size(113, 23);
            this.LogBackTransparent.TabIndex = 2;
            // 
            // lSpottingTrans
            // 
            this.lSpottingTrans.AutoSize = true;
            this.lSpottingTrans.Enabled = false;
            this.lSpottingTrans.Location = new System.Drawing.Point(6, 42);
            this.lSpottingTrans.Name = "lSpottingTrans";
            this.lSpottingTrans.Size = new System.Drawing.Size(104, 15);
            this.lSpottingTrans.TabIndex = 1;
            this.lSpottingTrans.Text = "Прозрачность, %:";
            // 
            // LogBackUse
            // 
            this.LogBackUse.AutoSize = true;
            this.LogBackUse.Location = new System.Drawing.Point(9, 15);
            this.LogBackUse.Name = "LogBackUse";
            this.LogBackUse.Size = new System.Drawing.Size(152, 19);
            this.LogBackUse.TabIndex = 0;
            this.LogBackUse.Text = "Использовать заливку";
            this.LogBackUse.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 17;
            this.label4.Text = "Цвет линии:";
            // 
            // lLineWidth
            // 
            this.lLineWidth.AutoSize = true;
            this.lLineWidth.Location = new System.Drawing.Point(6, 24);
            this.lLineWidth.Name = "lLineWidth";
            this.lLineWidth.Size = new System.Drawing.Size(98, 15);
            this.lLineWidth.TabIndex = 15;
            this.lLineWidth.Text = "Толщина линии:";
            // 
            // LogMask
            // 
            this.LogMask.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LogMask.Location = new System.Drawing.Point(114, 16);
            this.LogMask.Name = "LogMask";
            this.LogMask.ReadOnly = true;
            this.LogMask.Size = new System.Drawing.Size(419, 23);
            this.LogMask.TabIndex = 2;
            // 
            // lMask
            // 
            this.lMask.AutoSize = true;
            this.lMask.Location = new System.Drawing.Point(6, 19);
            this.lMask.Name = "lMask";
            this.lMask.Size = new System.Drawing.Size(105, 15);
            this.lMask.TabIndex = 1;
            this.lMask.Text = "Маска каротажа:";
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(444, 473);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(90, 30);
            this.ApplyButton.TabIndex = 2;
            this.ApplyButton.Text = "Применить";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(540, 473);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(90, 30);
            this.OkButton.TabIndex = 3;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(636, 473);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 4;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // PerfSettingsGroup
            // 
            this.PerfSettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PerfSettingsGroup.Controls.Add(this.PerfColumnGroup);
            this.PerfSettingsGroup.Location = new System.Drawing.Point(187, 0);
            this.PerfSettingsGroup.Name = "PerfSettingsGroup";
            this.PerfSettingsGroup.Size = new System.Drawing.Size(539, 467);
            this.PerfSettingsGroup.TabIndex = 7;
            this.PerfSettingsGroup.TabStop = false;
            this.PerfSettingsGroup.Text = " Настройки - Перфорация";
            // 
            // PerfColumnGroup
            // 
            this.PerfColumnGroup.Controls.Add(this.PerfColMaxUse);
            this.PerfColumnGroup.Controls.Add(this.PerfColMinUse);
            this.PerfColumnGroup.Controls.Add(this.PerfColMax);
            this.PerfColumnGroup.Controls.Add(this.PerfColMin);
            this.PerfColumnGroup.Location = new System.Drawing.Point(5, 16);
            this.PerfColumnGroup.Name = "PerfColumnGroup";
            this.PerfColumnGroup.Size = new System.Drawing.Size(529, 79);
            this.PerfColumnGroup.TabIndex = 18;
            this.PerfColumnGroup.TabStop = false;
            this.PerfColumnGroup.Text = "Ширина колонки:";
            // 
            // PerfColMaxUse
            // 
            this.PerfColMaxUse.AutoSize = true;
            this.PerfColMaxUse.Location = new System.Drawing.Point(6, 51);
            this.PerfColMaxUse.Name = "PerfColMaxUse";
            this.PerfColMaxUse.Size = new System.Drawing.Size(90, 19);
            this.PerfColMaxUse.TabIndex = 7;
            this.PerfColMaxUse.Text = "Максимум:";
            this.PerfColMaxUse.UseVisualStyleBackColor = true;
            // 
            // PerfColMinUse
            // 
            this.PerfColMinUse.AutoSize = true;
            this.PerfColMinUse.Location = new System.Drawing.Point(6, 22);
            this.PerfColMinUse.Name = "PerfColMinUse";
            this.PerfColMinUse.Size = new System.Drawing.Size(85, 19);
            this.PerfColMinUse.TabIndex = 6;
            this.PerfColMinUse.Text = "Минимум:";
            this.PerfColMinUse.UseVisualStyleBackColor = true;
            // 
            // PerfColMax
            // 
            this.PerfColMax.Location = new System.Drawing.Point(101, 50);
            this.PerfColMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.PerfColMax.Name = "PerfColMax";
            this.PerfColMax.Size = new System.Drawing.Size(120, 23);
            this.PerfColMax.TabIndex = 5;
            // 
            // PerfColMin
            // 
            this.PerfColMin.Location = new System.Drawing.Point(101, 21);
            this.PerfColMin.Name = "PerfColMin";
            this.PerfColMin.Size = new System.Drawing.Size(120, 23);
            this.PerfColMin.TabIndex = 3;
            // 
            // ObjGisSettingsGroup
            // 
            this.ObjGisSettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ObjGisSettingsGroup.Controls.Add(this.ObjGisColumnGroup);
            this.ObjGisSettingsGroup.Location = new System.Drawing.Point(187, 0);
            this.ObjGisSettingsGroup.Name = "ObjGisSettingsGroup";
            this.ObjGisSettingsGroup.Size = new System.Drawing.Size(539, 467);
            this.ObjGisSettingsGroup.TabIndex = 8;
            this.ObjGisSettingsGroup.TabStop = false;
            this.ObjGisSettingsGroup.Text = " Настройки - Объекты ГИС ";
            // 
            // ObjGisColumnGroup
            // 
            this.ObjGisColumnGroup.Controls.Add(this.ObjGisMaxUse);
            this.ObjGisColumnGroup.Controls.Add(this.ObjGisMinUse);
            this.ObjGisColumnGroup.Controls.Add(this.ObjGisMax);
            this.ObjGisColumnGroup.Controls.Add(this.ObjGisMin);
            this.ObjGisColumnGroup.Location = new System.Drawing.Point(5, 16);
            this.ObjGisColumnGroup.Name = "ObjGisColumnGroup";
            this.ObjGisColumnGroup.Size = new System.Drawing.Size(529, 79);
            this.ObjGisColumnGroup.TabIndex = 18;
            this.ObjGisColumnGroup.TabStop = false;
            this.ObjGisColumnGroup.Text = "Размеры колонки:";
            // 
            // ObjGisMaxUse
            // 
            this.ObjGisMaxUse.AutoSize = true;
            this.ObjGisMaxUse.Location = new System.Drawing.Point(6, 51);
            this.ObjGisMaxUse.Name = "ObjGisMaxUse";
            this.ObjGisMaxUse.Size = new System.Drawing.Size(90, 19);
            this.ObjGisMaxUse.TabIndex = 7;
            this.ObjGisMaxUse.Text = "Максимум:";
            this.ObjGisMaxUse.UseVisualStyleBackColor = true;
            // 
            // ObjGisMinUse
            // 
            this.ObjGisMinUse.AutoSize = true;
            this.ObjGisMinUse.Location = new System.Drawing.Point(6, 22);
            this.ObjGisMinUse.Name = "ObjGisMinUse";
            this.ObjGisMinUse.Size = new System.Drawing.Size(85, 19);
            this.ObjGisMinUse.TabIndex = 6;
            this.ObjGisMinUse.Text = "Минимум:";
            this.ObjGisMinUse.UseVisualStyleBackColor = true;
            // 
            // ObjGisMax
            // 
            this.ObjGisMax.Location = new System.Drawing.Point(101, 50);
            this.ObjGisMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ObjGisMax.Name = "ObjGisMax";
            this.ObjGisMax.Size = new System.Drawing.Size(120, 23);
            this.ObjGisMax.TabIndex = 5;
            // 
            // ObjGisMin
            // 
            this.ObjGisMin.Location = new System.Drawing.Point(101, 21);
            this.ObjGisMin.Name = "ObjGisMin";
            this.ObjGisMin.Size = new System.Drawing.Size(120, 23);
            this.ObjGisMin.TabIndex = 3;
            // 
            // DepthCalcInitFromTrajectory
            // 
            this.DepthCalcInitFromTrajectory.AutoSize = true;
            this.DepthCalcInitFromTrajectory.Location = new System.Drawing.Point(11, 105);
            this.DepthCalcInitFromTrajectory.Name = "DepthCalcInitFromTrajectory";
            this.DepthCalcInitFromTrajectory.Size = new System.Drawing.Size(308, 19);
            this.DepthCalcInitFromTrajectory.TabIndex = 24;
            this.DepthCalcInitFromTrajectory.Text = "Рассчитывать абсолютные отметки по траектории";
            this.DepthCalcInitFromTrajectory.UseVisualStyleBackColor = true;
            // 
            // CorSchemeSettingsForm
            // 
            this.AcceptButton = this.OkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 515);
            this.Controls.Add(this.DepthSettingsGroup);
            this.Controls.Add(this.GisSettingsGroup);
            this.Controls.Add(this.LogsSettingsGroup);
            this.Controls.Add(this.ObjGisSettingsGroup);
            this.Controls.Add(this.PerfSettingsGroup);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.ColumnsTree);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "CorSchemeSettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки корсхемы";
            this.DepthSettingsGroup.ResumeLayout(false);
            this.DepthSettingsGroup.PerformLayout();
            this.MarkerGroup.ResumeLayout(false);
            this.MarkerGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepthBottomSpace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepthTopSpace)).EndInit();
            this.DepthColumnGroup.ResumeLayout(false);
            this.DepthColumnGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepthColMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepthColMin)).EndInit();
            this.GisSettingsGroup.ResumeLayout(false);
            this.GisColumnGroup.ResumeLayout(false);
            this.GisColumnGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GisColMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GisColMin)).EndInit();
            this.LogsSettingsGroup.ResumeLayout(false);
            this.LogsSettingsGroup.PerformLayout();
            this.ColumnGroup.ResumeLayout(false);
            this.ColumnGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogColumnMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogColumnMin)).EndInit();
            this.ScaleGroup.ResumeLayout(false);
            this.ScaleGroup.PerformLayout();
            this.ScaleMinMaxGroup.ResumeLayout(false);
            this.ScaleMinMaxGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogScaleMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogScaleMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogScaleStep)).EndInit();
            this.DrawGroup.ResumeLayout(false);
            this.DrawGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogLineWidth)).EndInit();
            this.SpottingGroup.ResumeLayout(false);
            this.SpottingGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogBackTransparent)).EndInit();
            this.PerfSettingsGroup.ResumeLayout(false);
            this.PerfColumnGroup.ResumeLayout(false);
            this.PerfColumnGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PerfColMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PerfColMin)).EndInit();
            this.ObjGisSettingsGroup.ResumeLayout(false);
            this.ObjGisColumnGroup.ResumeLayout(false);
            this.ObjGisColumnGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObjGisMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObjGisMin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView ColumnsTree;
        private System.Windows.Forms.GroupBox DepthSettingsGroup;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.GroupBox LogsSettingsGroup;
        private System.Windows.Forms.TextBox LogMask;
        private System.Windows.Forms.Label lMask;
        private System.Windows.Forms.GroupBox ScaleGroup;
        private System.Windows.Forms.GroupBox ScaleMinMaxGroup;
        private System.Windows.Forms.NumericUpDown LogScaleMax;
        private System.Windows.Forms.NumericUpDown LogScaleMin;
        private System.Windows.Forms.NumericUpDown LogScaleStep;
        private System.Windows.Forms.Label lScaleStep;
        private System.Windows.Forms.CheckBox LogScaleMinMaxManualByStep;
        private System.Windows.Forms.Label lScaleMax;
        private System.Windows.Forms.Label lScaleMin;
        private System.Windows.Forms.RadioButton LogScaleMinMaxManual;
        private System.Windows.Forms.RadioButton LogScaleMinMaxAuto;
        private System.Windows.Forms.ComboBox LogScaleType;
        private System.Windows.Forms.Label lScaleType;
        private System.Windows.Forms.GroupBox DrawGroup;
        private System.Windows.Forms.CheckBox LogGridHorizontUse;
        private System.Windows.Forms.CheckBox LogGridVerticalUse;
        private System.Windows.Forms.NumericUpDown LogLineWidth;
        private System.Windows.Forms.GroupBox SpottingGroup;
        private System.Windows.Forms.CheckBox LogBackInvert;
        private System.Windows.Forms.Label lSpottingColor;
        private System.Windows.Forms.NumericUpDown LogBackTransparent;
        private System.Windows.Forms.Label lSpottingTrans;
        private System.Windows.Forms.CheckBox LogBackUse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lLineWidth;
        private System.Windows.Forms.GroupBox ColumnGroup;
        private System.Windows.Forms.NumericUpDown LogColumnMax;
        private System.Windows.Forms.NumericUpDown LogColumnMin;
        private System.Windows.Forms.CheckBox LogColumnMaxUse;
        private System.Windows.Forms.CheckBox LogColumnMinUse;
        private System.Windows.Forms.GroupBox DepthColumnGroup;
        private System.Windows.Forms.CheckBox DepthColMaxUse;
        private System.Windows.Forms.CheckBox DepthColMinUse;
        private System.Windows.Forms.NumericUpDown DepthColMax;
        private System.Windows.Forms.NumericUpDown DepthColMin;
        private System.Windows.Forms.GroupBox GisSettingsGroup;
        private System.Windows.Forms.GroupBox GisColumnGroup;
        private System.Windows.Forms.CheckBox GisColMaxUse;
        private System.Windows.Forms.CheckBox GisColMinUse;
        private System.Windows.Forms.NumericUpDown GisColMax;
        private System.Windows.Forms.NumericUpDown GisColMin;
        private System.Windows.Forms.GroupBox PerfSettingsGroup;
        private System.Windows.Forms.GroupBox PerfColumnGroup;
        private System.Windows.Forms.CheckBox PerfColMaxUse;
        private System.Windows.Forms.CheckBox PerfColMinUse;
        private System.Windows.Forms.NumericUpDown PerfColMax;
        private System.Windows.Forms.NumericUpDown PerfColMin;
        private System.Windows.Forms.GroupBox ObjGisSettingsGroup;
        private System.Windows.Forms.GroupBox ObjGisColumnGroup;
        private System.Windows.Forms.CheckBox ObjGisMaxUse;
        private System.Windows.Forms.CheckBox ObjGisMinUse;
        private System.Windows.Forms.NumericUpDown ObjGisMax;
        private System.Windows.Forms.NumericUpDown ObjGisMin;
        private System.Windows.Forms.Label lDepthBottomSpace;
        private System.Windows.Forms.Label lDepthTopSpace;
        private System.Windows.Forms.NumericUpDown DepthBottomSpace;
        private System.Windows.Forms.NumericUpDown DepthTopSpace;
        private System.Windows.Forms.CheckBox ShowMarkersConnection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox MarkerGroup;
        private System.Windows.Forms.CheckBox ShowMarkersInfo;
        private System.Windows.Forms.CheckBox DepthCalcInitFromTrajectory;
    }
}