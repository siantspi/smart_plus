﻿namespace SmartPlus
{
    partial class WorkObjectSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tabs = new System.Windows.Forms.TabControl();
            this.MainPage = new System.Windows.Forms.TabPage();
            this.ContourPanel = new System.Windows.Forms.Panel();
            this.ContourDrawingGroup = new System.Windows.Forms.GroupBox();
            this.ContourShowNameCheck = new System.Windows.Forms.CheckBox();
            this.lContour = new System.Windows.Forms.Label();
            this.MarkerPanel = new System.Windows.Forms.Panel();
            this.MarkerDrawGroup = new System.Windows.Forms.GroupBox();
            this.MarkerPaletteButton = new System.Windows.Forms.Button();
            this.lMarker1 = new System.Windows.Forms.Label();
            this.MarkerUsePalette = new System.Windows.Forms.CheckBox();
            this.ImagePanel = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ImageYdpi = new System.Windows.Forms.NumericUpDown();
            this.ImageXdpi = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GridPanel = new System.Windows.Forms.Panel();
            this.GridDrawingBox = new System.Windows.Forms.GroupBox();
            this.GridDrawLines = new System.Windows.Forms.CheckBox();
            this.GridPaletteEdit = new System.Windows.Forms.Button();
            this.WellListPanel = new System.Windows.Forms.Panel();
            this.WLDrawingGroup = new System.Windows.Forms.GroupBox();
            this.WLDrawOverBubble = new System.Windows.Forms.CheckBox();
            this.WLDrawCaption = new System.Windows.Forms.CheckBox();
            this.WLDrawName = new System.Windows.Forms.CheckBox();
            this.WLVisibleLvlGroup = new System.Windows.Forms.GroupBox();
            this.WLLevelSkv = new System.Windows.Forms.CheckBox();
            this.WLLevelArea = new System.Windows.Forms.CheckBox();
            this.WLLevelOilfield = new System.Windows.Forms.CheckBox();
            this.WLLevelNGDU = new System.Windows.Forms.CheckBox();
            this.StatisticPage = new System.Windows.Forms.TabPage();
            this.StatBox = new System.Windows.Forms.RichTextBox();
            this.CntrOpsPage = new System.Windows.Forms.TabPage();
            this.WorkComment = new System.Windows.Forms.Label();
            this.CalcButton = new System.Windows.Forms.Button();
            this.CntrOpNIZGroup = new System.Windows.Forms.GroupBox();
            this.CntrOpNIZPVTLoad = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.CntrOpNIZGridLoad = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.OperationsGroup = new System.Windows.Forms.GroupBox();
            this.OpListBox = new System.Windows.Forms.ListBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.CntrOpPVTLayerBox = new SmartPlus.TextBoxEx();
            this.CntrOpNNTLayerBox = new SmartPlus.TextBoxEx();
            this.Tabs.SuspendLayout();
            this.MainPage.SuspendLayout();
            this.ContourPanel.SuspendLayout();
            this.ContourDrawingGroup.SuspendLayout();
            this.MarkerPanel.SuspendLayout();
            this.MarkerDrawGroup.SuspendLayout();
            this.ImagePanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageYdpi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageXdpi)).BeginInit();
            this.GridPanel.SuspendLayout();
            this.GridDrawingBox.SuspendLayout();
            this.WellListPanel.SuspendLayout();
            this.WLDrawingGroup.SuspendLayout();
            this.WLVisibleLvlGroup.SuspendLayout();
            this.StatisticPage.SuspendLayout();
            this.CntrOpsPage.SuspendLayout();
            this.CntrOpNIZGroup.SuspendLayout();
            this.OperationsGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.MainPage);
            this.Tabs.Controls.Add(this.StatisticPage);
            this.Tabs.Controls.Add(this.CntrOpsPage);
            this.Tabs.Location = new System.Drawing.Point(3, 2);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(546, 449);
            this.Tabs.TabIndex = 0;
            // 
            // MainPage
            // 
            this.MainPage.Controls.Add(this.ImagePanel);
            this.MainPage.Controls.Add(this.ContourPanel);
            this.MainPage.Controls.Add(this.MarkerPanel);
            this.MainPage.Controls.Add(this.GridPanel);
            this.MainPage.Controls.Add(this.WellListPanel);
            this.MainPage.Location = new System.Drawing.Point(4, 24);
            this.MainPage.Name = "MainPage";
            this.MainPage.Padding = new System.Windows.Forms.Padding(3);
            this.MainPage.Size = new System.Drawing.Size(538, 421);
            this.MainPage.TabIndex = 0;
            this.MainPage.Text = "Общие";
            this.MainPage.UseVisualStyleBackColor = true;
            // 
            // ContourPanel
            // 
            this.ContourPanel.Controls.Add(this.ContourDrawingGroup);
            this.ContourPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContourPanel.Location = new System.Drawing.Point(3, 3);
            this.ContourPanel.Name = "ContourPanel";
            this.ContourPanel.Size = new System.Drawing.Size(532, 415);
            this.ContourPanel.TabIndex = 10;
            // 
            // ContourDrawingGroup
            // 
            this.ContourDrawingGroup.Controls.Add(this.ContourShowNameCheck);
            this.ContourDrawingGroup.Controls.Add(this.lContour);
            this.ContourDrawingGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContourDrawingGroup.Location = new System.Drawing.Point(0, 0);
            this.ContourDrawingGroup.Name = "ContourDrawingGroup";
            this.ContourDrawingGroup.Size = new System.Drawing.Size(532, 415);
            this.ContourDrawingGroup.TabIndex = 0;
            this.ContourDrawingGroup.TabStop = false;
            this.ContourDrawingGroup.Text = " Отображение ";
            // 
            // ContourShowNameCheck
            // 
            this.ContourShowNameCheck.AutoSize = true;
            this.ContourShowNameCheck.Location = new System.Drawing.Point(18, 51);
            this.ContourShowNameCheck.Name = "ContourShowNameCheck";
            this.ContourShowNameCheck.Size = new System.Drawing.Size(219, 19);
            this.ContourShowNameCheck.TabIndex = 1;
            this.ContourShowNameCheck.Text = "Отображать имя контура на карте";
            this.ContourShowNameCheck.UseVisualStyleBackColor = true;
            // 
            // lContour
            // 
            this.lContour.AutoSize = true;
            this.lContour.Location = new System.Drawing.Point(15, 22);
            this.lContour.Name = "lContour";
            this.lContour.Size = new System.Drawing.Size(73, 15);
            this.lContour.TabIndex = 0;
            this.lContour.Text = "Цвет линии:";
            // 
            // MarkerPanel
            // 
            this.MarkerPanel.Controls.Add(this.MarkerDrawGroup);
            this.MarkerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MarkerPanel.Location = new System.Drawing.Point(3, 3);
            this.MarkerPanel.Name = "MarkerPanel";
            this.MarkerPanel.Size = new System.Drawing.Size(532, 415);
            this.MarkerPanel.TabIndex = 0;
            // 
            // MarkerDrawGroup
            // 
            this.MarkerDrawGroup.Controls.Add(this.MarkerPaletteButton);
            this.MarkerDrawGroup.Controls.Add(this.lMarker1);
            this.MarkerDrawGroup.Controls.Add(this.MarkerUsePalette);
            this.MarkerDrawGroup.Location = new System.Drawing.Point(3, 3);
            this.MarkerDrawGroup.Name = "MarkerDrawGroup";
            this.MarkerDrawGroup.Size = new System.Drawing.Size(526, 409);
            this.MarkerDrawGroup.TabIndex = 0;
            this.MarkerDrawGroup.TabStop = false;
            this.MarkerDrawGroup.Text = " Отображение ";
            // 
            // MarkerPaletteButton
            // 
            this.MarkerPaletteButton.Location = new System.Drawing.Point(219, 43);
            this.MarkerPaletteButton.Name = "MarkerPaletteButton";
            this.MarkerPaletteButton.Size = new System.Drawing.Size(90, 30);
            this.MarkerPaletteButton.TabIndex = 2;
            this.MarkerPaletteButton.Text = "Палитра...";
            this.MarkerPaletteButton.UseVisualStyleBackColor = true;
            // 
            // lMarker1
            // 
            this.lMarker1.AutoSize = true;
            this.lMarker1.Location = new System.Drawing.Point(3, 19);
            this.lMarker1.Name = "lMarker1";
            this.lMarker1.Size = new System.Drawing.Size(87, 15);
            this.lMarker1.TabIndex = 1;
            this.lMarker1.Text = "Цвет маркера:";
            // 
            // MarkerUsePalette
            // 
            this.MarkerUsePalette.AutoSize = true;
            this.MarkerUsePalette.Location = new System.Drawing.Point(6, 50);
            this.MarkerUsePalette.Name = "MarkerUsePalette";
            this.MarkerUsePalette.Size = new System.Drawing.Size(207, 19);
            this.MarkerUsePalette.TabIndex = 0;
            this.MarkerUsePalette.Text = "Использовать палитру на карте:";
            this.MarkerUsePalette.UseVisualStyleBackColor = true;
            // 
            // ImagePanel
            // 
            this.ImagePanel.Controls.Add(this.groupBox1);
            this.ImagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImagePanel.Location = new System.Drawing.Point(3, 3);
            this.ImagePanel.Name = "ImagePanel";
            this.ImagePanel.Size = new System.Drawing.Size(532, 415);
            this.ImagePanel.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ImageYdpi);
            this.groupBox1.Controls.Add(this.ImageXdpi);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(532, 415);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Отображение ";
            // 
            // ImageYdpi
            // 
            this.ImageYdpi.DecimalPlaces = 2;
            this.ImageYdpi.Location = new System.Drawing.Point(222, 46);
            this.ImageYdpi.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ImageYdpi.Name = "ImageYdpi";
            this.ImageYdpi.Size = new System.Drawing.Size(99, 23);
            this.ImageYdpi.TabIndex = 10;
            this.ImageYdpi.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ImageYdpi_MouseDown);
            // 
            // ImageXdpi
            // 
            this.ImageXdpi.DecimalPlaces = 2;
            this.ImageXdpi.Location = new System.Drawing.Point(52, 47);
            this.ImageXdpi.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ImageXdpi.Name = "ImageXdpi";
            this.ImageXdpi.Size = new System.Drawing.Size(100, 23);
            this.ImageXdpi.TabIndex = 9;
            this.ImageXdpi.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ImageXdpi_MouseDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(201, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Коэффициенты масштабирования:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(183, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "YDpi";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "XDpi";
            // 
            // GridPanel
            // 
            this.GridPanel.Controls.Add(this.GridDrawingBox);
            this.GridPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridPanel.Location = new System.Drawing.Point(3, 3);
            this.GridPanel.Name = "GridPanel";
            this.GridPanel.Size = new System.Drawing.Size(532, 415);
            this.GridPanel.TabIndex = 11;
            // 
            // GridDrawingBox
            // 
            this.GridDrawingBox.Controls.Add(this.GridDrawLines);
            this.GridDrawingBox.Controls.Add(this.GridPaletteEdit);
            this.GridDrawingBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDrawingBox.Location = new System.Drawing.Point(0, 0);
            this.GridDrawingBox.Name = "GridDrawingBox";
            this.GridDrawingBox.Size = new System.Drawing.Size(532, 415);
            this.GridDrawingBox.TabIndex = 0;
            this.GridDrawingBox.TabStop = false;
            this.GridDrawingBox.Text = " Отображение ";
            // 
            // GridDrawLines
            // 
            this.GridDrawLines.AutoSize = true;
            this.GridDrawLines.Location = new System.Drawing.Point(9, 58);
            this.GridDrawLines.Name = "GridDrawLines";
            this.GridDrawLines.Size = new System.Drawing.Size(148, 19);
            this.GridDrawLines.TabIndex = 2;
            this.GridDrawLines.Text = "Рисовать линии сетки";
            this.GridDrawLines.UseVisualStyleBackColor = true;
            // 
            // GridPaletteEdit
            // 
            this.GridPaletteEdit.Location = new System.Drawing.Point(9, 22);
            this.GridPaletteEdit.Name = "GridPaletteEdit";
            this.GridPaletteEdit.Size = new System.Drawing.Size(90, 30);
            this.GridPaletteEdit.TabIndex = 1;
            this.GridPaletteEdit.Text = "Палитра...";
            this.GridPaletteEdit.UseVisualStyleBackColor = true;
            // 
            // WellListPanel
            // 
            this.WellListPanel.Controls.Add(this.WLDrawingGroup);
            this.WellListPanel.Controls.Add(this.WLVisibleLvlGroup);
            this.WellListPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WellListPanel.Location = new System.Drawing.Point(3, 3);
            this.WellListPanel.Name = "WellListPanel";
            this.WellListPanel.Size = new System.Drawing.Size(532, 415);
            this.WellListPanel.TabIndex = 4;
            // 
            // WLDrawingGroup
            // 
            this.WLDrawingGroup.Controls.Add(this.WLDrawOverBubble);
            this.WLDrawingGroup.Controls.Add(this.WLDrawCaption);
            this.WLDrawingGroup.Controls.Add(this.WLDrawName);
            this.WLDrawingGroup.Location = new System.Drawing.Point(242, 3);
            this.WLDrawingGroup.Name = "WLDrawingGroup";
            this.WLDrawingGroup.Size = new System.Drawing.Size(281, 226);
            this.WLDrawingGroup.TabIndex = 3;
            this.WLDrawingGroup.TabStop = false;
            this.WLDrawingGroup.Text = " Отображение ";
            // 
            // WLDrawOverBubble
            // 
            this.WLDrawOverBubble.AutoSize = true;
            this.WLDrawOverBubble.Location = new System.Drawing.Point(6, 72);
            this.WLDrawOverBubble.Name = "WLDrawOverBubble";
            this.WLDrawOverBubble.Size = new System.Drawing.Size(214, 19);
            this.WLDrawOverBubble.TabIndex = 13;
            this.WLDrawOverBubble.Text = "Отображать поверх карт отборов";
            this.WLDrawOverBubble.UseVisualStyleBackColor = true;
            // 
            // WLDrawCaption
            // 
            this.WLDrawCaption.AutoSize = true;
            this.WLDrawCaption.Location = new System.Drawing.Point(6, 47);
            this.WLDrawCaption.Name = "WLDrawCaption";
            this.WLDrawCaption.Size = new System.Drawing.Size(202, 19);
            this.WLDrawCaption.TabIndex = 12;
            this.WLDrawCaption.Text = "Отображать подписи значений";
            this.WLDrawCaption.UseVisualStyleBackColor = true;
            // 
            // WLDrawName
            // 
            this.WLDrawName.AutoSize = true;
            this.WLDrawName.Location = new System.Drawing.Point(6, 22);
            this.WLDrawName.Name = "WLDrawName";
            this.WLDrawName.Size = new System.Drawing.Size(185, 19);
            this.WLDrawName.TabIndex = 11;
            this.WLDrawName.Text = "Отображать имена скважин";
            this.WLDrawName.UseVisualStyleBackColor = true;
            // 
            // WLVisibleLvlGroup
            // 
            this.WLVisibleLvlGroup.Controls.Add(this.WLLevelSkv);
            this.WLVisibleLvlGroup.Controls.Add(this.WLLevelArea);
            this.WLVisibleLvlGroup.Controls.Add(this.WLLevelOilfield);
            this.WLVisibleLvlGroup.Controls.Add(this.WLLevelNGDU);
            this.WLVisibleLvlGroup.Location = new System.Drawing.Point(9, 3);
            this.WLVisibleLvlGroup.Name = "WLVisibleLvlGroup";
            this.WLVisibleLvlGroup.Size = new System.Drawing.Size(227, 226);
            this.WLVisibleLvlGroup.TabIndex = 2;
            this.WLVisibleLvlGroup.TabStop = false;
            this.WLVisibleLvlGroup.Text = " Уровень видимости";
            // 
            // WLLevelSkv
            // 
            this.WLLevelSkv.AutoSize = true;
            this.WLLevelSkv.Checked = true;
            this.WLLevelSkv.CheckState = System.Windows.Forms.CheckState.Checked;
            this.WLLevelSkv.Enabled = false;
            this.WLLevelSkv.Location = new System.Drawing.Point(6, 97);
            this.WLLevelSkv.Name = "WLLevelSkv";
            this.WLLevelSkv.Size = new System.Drawing.Size(123, 19);
            this.WLLevelSkv.TabIndex = 8;
            this.WLLevelSkv.Text = "Уровень скважин";
            this.WLLevelSkv.UseVisualStyleBackColor = true;
            // 
            // WLLevelArea
            // 
            this.WLLevelArea.AutoSize = true;
            this.WLLevelArea.Location = new System.Drawing.Point(6, 72);
            this.WLLevelArea.Name = "WLLevelArea";
            this.WLLevelArea.Size = new System.Drawing.Size(174, 19);
            this.WLLevelArea.TabIndex = 7;
            this.WLLevelArea.Text = "Уровень ячеек заводнения";
            this.WLLevelArea.UseVisualStyleBackColor = true;
            // 
            // WLLevelOilfield
            // 
            this.WLLevelOilfield.AutoSize = true;
            this.WLLevelOilfield.Location = new System.Drawing.Point(6, 47);
            this.WLLevelOilfield.Name = "WLLevelOilfield";
            this.WLLevelOilfield.Size = new System.Drawing.Size(165, 19);
            this.WLLevelOilfield.TabIndex = 6;
            this.WLLevelOilfield.Text = "Уровень месторождений";
            this.WLLevelOilfield.UseVisualStyleBackColor = true;
            // 
            // WLLevelNGDU
            // 
            this.WLLevelNGDU.AutoSize = true;
            this.WLLevelNGDU.Location = new System.Drawing.Point(6, 22);
            this.WLLevelNGDU.Name = "WLLevelNGDU";
            this.WLLevelNGDU.Size = new System.Drawing.Size(103, 19);
            this.WLLevelNGDU.TabIndex = 5;
            this.WLLevelNGDU.Text = "Уровень НГДУ";
            this.WLLevelNGDU.UseVisualStyleBackColor = true;
            // 
            // StatisticPage
            // 
            this.StatisticPage.Controls.Add(this.StatBox);
            this.StatisticPage.Location = new System.Drawing.Point(4, 24);
            this.StatisticPage.Name = "StatisticPage";
            this.StatisticPage.Padding = new System.Windows.Forms.Padding(3);
            this.StatisticPage.Size = new System.Drawing.Size(538, 421);
            this.StatisticPage.TabIndex = 1;
            this.StatisticPage.Text = "Статистика";
            this.StatisticPage.UseVisualStyleBackColor = true;
            // 
            // StatBox
            // 
            this.StatBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatBox.Location = new System.Drawing.Point(3, 3);
            this.StatBox.Name = "StatBox";
            this.StatBox.Size = new System.Drawing.Size(532, 417);
            this.StatBox.TabIndex = 0;
            this.StatBox.Text = "";
            // 
            // CntrOpsPage
            // 
            this.CntrOpsPage.Controls.Add(this.WorkComment);
            this.CntrOpsPage.Controls.Add(this.CalcButton);
            this.CntrOpsPage.Controls.Add(this.CntrOpNIZGroup);
            this.CntrOpsPage.Controls.Add(this.OperationsGroup);
            this.CntrOpsPage.Location = new System.Drawing.Point(4, 24);
            this.CntrOpsPage.Name = "CntrOpsPage";
            this.CntrOpsPage.Padding = new System.Windows.Forms.Padding(3);
            this.CntrOpsPage.Size = new System.Drawing.Size(538, 421);
            this.CntrOpsPage.TabIndex = 2;
            this.CntrOpsPage.Text = "Операции";
            this.CntrOpsPage.UseVisualStyleBackColor = true;
            // 
            // WorkComment
            // 
            this.WorkComment.Location = new System.Drawing.Point(202, 385);
            this.WorkComment.Name = "WorkComment";
            this.WorkComment.Size = new System.Drawing.Size(234, 30);
            this.WorkComment.TabIndex = 3;
            this.WorkComment.Text = "comment";
            // 
            // CalcButton
            // 
            this.CalcButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CalcButton.Location = new System.Drawing.Point(442, 385);
            this.CalcButton.Name = "CalcButton";
            this.CalcButton.Size = new System.Drawing.Size(90, 30);
            this.CalcButton.TabIndex = 0;
            this.CalcButton.Text = "Расчет";
            this.CalcButton.UseVisualStyleBackColor = true;
            this.CalcButton.Click += new System.EventHandler(this.CalcButton_Click);
            // 
            // CntrOpNIZGroup
            // 
            this.CntrOpNIZGroup.Controls.Add(this.CntrOpNIZPVTLoad);
            this.CntrOpNIZGroup.Controls.Add(this.CntrOpPVTLayerBox);
            this.CntrOpNIZGroup.Controls.Add(this.label5);
            this.CntrOpNIZGroup.Controls.Add(this.CntrOpNIZGridLoad);
            this.CntrOpNIZGroup.Controls.Add(this.CntrOpNNTLayerBox);
            this.CntrOpNIZGroup.Controls.Add(this.label4);
            this.CntrOpNIZGroup.Location = new System.Drawing.Point(202, 6);
            this.CntrOpNIZGroup.Name = "CntrOpNIZGroup";
            this.CntrOpNIZGroup.Size = new System.Drawing.Size(330, 373);
            this.CntrOpNIZGroup.TabIndex = 2;
            this.CntrOpNIZGroup.TabStop = false;
            this.CntrOpNIZGroup.Text = "Параметры операции ";
            this.CntrOpNIZGroup.Visible = false;
            // 
            // CntrOpNIZPVTLoad
            // 
            this.CntrOpNIZPVTLoad.Image = global::SmartPlus.Properties.Resources.MoveNextBlue16;
            this.CntrOpNIZPVTLoad.Location = new System.Drawing.Point(9, 88);
            this.CntrOpNIZPVTLoad.Name = "CntrOpNIZPVTLoad";
            this.CntrOpNIZPVTLoad.Size = new System.Drawing.Size(30, 22);
            this.CntrOpNIZPVTLoad.TabIndex = 6;
            this.CntrOpNIZPVTLoad.UseVisualStyleBackColor = true;
            this.CntrOpNIZPVTLoad.Click += new System.EventHandler(this.CntrOpNIZPVTLoad_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "PVT данные:";
            // 
            // CntrOpNIZGridLoad
            // 
            this.CntrOpNIZGridLoad.Image = global::SmartPlus.Properties.Resources.MoveNextBlue16;
            this.CntrOpNIZGridLoad.Location = new System.Drawing.Point(9, 40);
            this.CntrOpNIZGridLoad.Name = "CntrOpNIZGridLoad";
            this.CntrOpNIZGridLoad.Size = new System.Drawing.Size(30, 22);
            this.CntrOpNIZGridLoad.TabIndex = 3;
            this.CntrOpNIZGridLoad.UseVisualStyleBackColor = true;
            this.CntrOpNIZGridLoad.Click += new System.EventHandler(this.CntrOpNIZGridLoad_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(202, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Сетка начальных неф.нас. толщин:";
            // 
            // OperationsGroup
            // 
            this.OperationsGroup.Controls.Add(this.OpListBox);
            this.OperationsGroup.Location = new System.Drawing.Point(6, 6);
            this.OperationsGroup.Name = "OperationsGroup";
            this.OperationsGroup.Size = new System.Drawing.Size(190, 409);
            this.OperationsGroup.TabIndex = 0;
            this.OperationsGroup.TabStop = false;
            this.OperationsGroup.Text = " Список операций ";
            // 
            // OpListBox
            // 
            this.OpListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OpListBox.FormattingEnabled = true;
            this.OpListBox.ItemHeight = 15;
            this.OpListBox.Location = new System.Drawing.Point(6, 22);
            this.OpListBox.Name = "OpListBox";
            this.OpListBox.Size = new System.Drawing.Size(178, 379);
            this.OpListBox.TabIndex = 0;
            this.OpListBox.SelectedIndexChanged += new System.EventHandler(this.OpListBox_SelectedIndexChanged);
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(353, 457);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(90, 30);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "ОК";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(449, 457);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CntrOpPVTLayerBox
            // 
            this.CntrOpPVTLayerBox.BackColor = System.Drawing.Color.White;
            this.CntrOpPVTLayerBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CntrOpPVTLayerBox.Location = new System.Drawing.Point(45, 88);
            this.CntrOpPVTLayerBox.Name = "CntrOpPVTLayerBox";
            this.CntrOpPVTLayerBox.ReadOnly = false;
            this.CntrOpPVTLayerBox.Size = new System.Drawing.Size(279, 22);
            this.CntrOpPVTLayerBox.TabIndex = 5;
            this.CntrOpPVTLayerBox.ToolTipText = null;
            // 
            // CntrOpNNTLayerBox
            // 
            this.CntrOpNNTLayerBox.BackColor = System.Drawing.Color.White;
            this.CntrOpNNTLayerBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CntrOpNNTLayerBox.Location = new System.Drawing.Point(45, 40);
            this.CntrOpNNTLayerBox.Name = "CntrOpNNTLayerBox";
            this.CntrOpNNTLayerBox.ReadOnly = false;
            this.CntrOpNNTLayerBox.Size = new System.Drawing.Size(279, 22);
            this.CntrOpNNTLayerBox.TabIndex = 2;
            this.CntrOpNNTLayerBox.ToolTipText = null;
            // 
            // WorkObjectSettingForm
            // 
            this.AcceptButton = this.OkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 498);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.Tabs);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WorkObjectSettingForm";
            this.Text = "Настройки";
            this.Tabs.ResumeLayout(false);
            this.MainPage.ResumeLayout(false);
            this.ContourPanel.ResumeLayout(false);
            this.ContourDrawingGroup.ResumeLayout(false);
            this.ContourDrawingGroup.PerformLayout();
            this.MarkerPanel.ResumeLayout(false);
            this.MarkerDrawGroup.ResumeLayout(false);
            this.MarkerDrawGroup.PerformLayout();
            this.ImagePanel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageYdpi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageXdpi)).EndInit();
            this.GridPanel.ResumeLayout(false);
            this.GridDrawingBox.ResumeLayout(false);
            this.GridDrawingBox.PerformLayout();
            this.WellListPanel.ResumeLayout(false);
            this.WLDrawingGroup.ResumeLayout(false);
            this.WLDrawingGroup.PerformLayout();
            this.WLVisibleLvlGroup.ResumeLayout(false);
            this.WLVisibleLvlGroup.PerformLayout();
            this.StatisticPage.ResumeLayout(false);
            this.CntrOpsPage.ResumeLayout(false);
            this.CntrOpNIZGroup.ResumeLayout(false);
            this.CntrOpNIZGroup.PerformLayout();
            this.OperationsGroup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage MainPage;
        private System.Windows.Forms.TabPage StatisticPage;
        private System.Windows.Forms.Panel MarkerPanel;
        private System.Windows.Forms.GroupBox MarkerDrawGroup;
        private System.Windows.Forms.RichTextBox StatBox;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button MarkerPaletteButton;
        private System.Windows.Forms.Label lMarker1;
        private System.Windows.Forms.CheckBox MarkerUsePalette;
        private System.Windows.Forms.Panel WellListPanel;
        private System.Windows.Forms.GroupBox WLDrawingGroup;
        private System.Windows.Forms.CheckBox WLDrawOverBubble;
        private System.Windows.Forms.CheckBox WLDrawCaption;
        private System.Windows.Forms.CheckBox WLDrawName;
        private System.Windows.Forms.GroupBox WLVisibleLvlGroup;
        private System.Windows.Forms.CheckBox WLLevelSkv;
        private System.Windows.Forms.CheckBox WLLevelArea;
        private System.Windows.Forms.CheckBox WLLevelOilfield;
        private System.Windows.Forms.CheckBox WLLevelNGDU;
        private System.Windows.Forms.Panel ContourPanel;
        private System.Windows.Forms.GroupBox ContourDrawingGroup;
        private System.Windows.Forms.Label lContour;
        private System.Windows.Forms.Panel GridPanel;
        private System.Windows.Forms.GroupBox GridDrawingBox;
        private System.Windows.Forms.Button GridPaletteEdit;
        private System.Windows.Forms.CheckBox GridDrawLines;
        private System.Windows.Forms.Panel ImagePanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ImageYdpi;
        private System.Windows.Forms.NumericUpDown ImageXdpi;
        private System.Windows.Forms.TabPage CntrOpsPage;
        private System.Windows.Forms.GroupBox CntrOpNIZGroup;
        private System.Windows.Forms.GroupBox OperationsGroup;
        private System.Windows.Forms.ListBox OpListBox;
        private System.Windows.Forms.Button CalcButton;
        private TextBoxEx CntrOpNNTLayerBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CntrOpNIZGridLoad;
        private System.Windows.Forms.Button CntrOpNIZPVTLoad;
        private TextBoxEx CntrOpPVTLayerBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label WorkComment;
        private System.Windows.Forms.CheckBox ContourShowNameCheck;
    }
}