﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using RDF.Objects;
using System.IO;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public partial class AppSettingsForm : Form
    {
        public AppSetting AppSett;
        MainForm mainForm;
        // OILOBJ COLORS
        ArrayList bmpList;
        public bool OilObjColorsChanged;
        public bool LiquidInCubeChanged;
        public bool AttachAreasToFilterChanged;

        public AppSettingsForm(MainForm MainForm)
        {
            InitializeComponent();
            
            mainForm = MainForm;
            AppSett = new AppSetting(MainForm.UserAppSettingsFolder);
            AppSett.Load();

            this.Owner = mainForm;
            this.StartPosition = FormStartPosition.CenterParent;
            bmpList = new ArrayList();
            OilObjColorsChanged = false;
            AttachAreasToFilterChanged = false;
            twSettingTitle.Nodes[0].Tag = pPlastColors;
            dgvOilObjColor.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dgvOilObjColor.ColumnHeadersHeight = 18;
            dgvOilObjColor.EnableHeadersVisualStyles = false;
            dgvOilObjColor.AllowUserToResizeRows = false;
            dgvOilObjColor.AllowUserToAddRows = false;
            dgvOilObjColor.AllowUserToDeleteRows = false;
            dgvOilObjColor.RowHeadersVisible = false;
            dgvOilObjColor.ReadOnly = true;
            Common_Set_CopyImageFormatBox.SelectedIndex = 0;
            ReadOilObjColorsFromSection();

            UpdateHTTPS_labelConnectionState.Text = "";
            
            twSettingTitle.NodeMouseClick += new TreeNodeMouseClickEventHandler(twSettingTitle_NodeMouseClick);
            this.Shown += new EventHandler(AppSettingsForm_Shown);
            this.FormClosing += new FormClosingEventHandler(AppSettingsForm_FormClosing);
        }

        void AppSettingsForm_Shown(object sender, EventArgs e)
        {
            AttachAreasToFilterChanged = false;
            ReadCommonSettingFromSection();
            ReadOilObjColorsTable(true);
            SetSelectedPage(0);
            ShowSmartPlusFolder();
        }
        void twSettingTitle_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            SetSelectedPage(e.Node.Index);
        }
        void AppSettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!mainForm.AppRestart)
            {
                if (this.DialogResult == DialogResult.Cancel)
                {
                    OilObjColorsChanged = false;
                    LiquidInCubeChanged = false;
                    ReadOilObjColorsTable(false);
                }
                else if (this.DialogResult == DialogResult.OK)
                {
                    LiquidInCubeChanged = mainForm.AppSettings.ShowDataLiquidInCube != Common_Set_LiquidInCube.Checked;
                    AttachAreasToFilterChanged = (mainForm.AppSettings.AttachAreasToFilter != CommonSet_AttachAreasToFilter.Checked);
                    WriteOilObjColorsToSection();
                    WriteCommonSettingToSection();
                }
            }
        }
        public void SetSelectedPage(int PageIndex)
        {
            if ((twSettingTitle.Nodes.Count > 0) && (PageIndex < twSettingTitle.Nodes.Count))
            {
                twSettingTitle.SelectedNode = twSettingTitle.Nodes[PageIndex];
            }
            switch (PageIndex)
            {
                case 0:
                    CommonPanel.Visible = true;
                    pPlastColors.Visible = false;
                    ChartPanel.Visible = false;
                    UpdaterPanel.Visible = false;
                    break;
                case 1:
                    pPlastColors.Visible = true;
                    CommonPanel.Visible = false;
                    ChartPanel.Visible = false;
                    UpdaterPanel.Visible = false;
                    break;
                case 2:
                    ChartPanel.Visible = true;
                    CommonPanel.Visible = false;
                    pPlastColors.Visible = false;
                    UpdaterPanel.Visible = false;
                    break;
                case 3:
                    ChartPanel.Visible = false;
                    CommonPanel.Visible = false;
                    pPlastColors.Visible = false;
                    UpdaterPanel.Visible = true;
                    break;
            }
        }

        #region OIL OBJECTS COLORS
        public void ReadOilObjColorsTable(bool UseAppSetting)
        {
            Project proj = mainForm.GetLastProject();
            if (mainForm != null && proj != null)
            {
                var dict = (StratumDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                DataGridViewRow row;
                Bitmap bmp;
                SolidBrush brush;
                Graphics gr;
                int color;
                int i, j, OilObjCount = 0;
                OilObjColorsChanged = false;
                int[] OilObjCodes = null;
                int[] OilObjColors = null;
                if (dict != null)
                {
                    dgvOilObjColor.Rows.Clear();
                    if ((AppSett != null) && (UseAppSetting))
                    {
                        AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.OIL_OBJ_COLORS);
                        if (section != null)
                        {
                            section.Data.Seek(0, SeekOrigin.Begin);
                            BinaryReader br = new BinaryReader(section.Data);
                            int ver = br.ReadInt32();
                            if (ver == 0)
                            {
                                OilObjCount = br.ReadInt32();
                                OilObjCodes = new int[OilObjCount];
                                OilObjColors = new int[OilObjCount];
                                for (i = 0; i < OilObjCount; i++)
                                {
                                    OilObjCodes[i] = br.ReadInt32();
                                    OilObjColors[i] = br.ReadInt32();
                                }

                            }
                        }
                    }
                    for (i = 0; i < dict.stratumTree.Count; i++)
                    {
                        if (dict.stratumTree[i] != null)
                        {
                            row = new DataGridViewRow();
                            row.CreateCells(dgvOilObjColor);
                            row.Height = dgvOilObjColor.RowTemplate.Height;
                            row.Cells[0].Value = dgvOilObjColor.Rows.Count;
                            row.Cells[1].Value = dict.stratumTree[i].Name;
                            color = dict.stratumTree[i].ARGB;
                            if (color == -1) color = dict.stratumTree[i].GetParentStratumColor();
                            row.Cells[5].Value = 0;
                            if (OilObjCodes != null)
                            {
                                for (j = 0; j < OilObjCount; j++)
                                {
                                    if (dict.stratumTree[i].StratumCode == OilObjCodes[j])
                                    {
                                        color = OilObjColors[j];
                                        row.Cells[5].Value = 1;
                                        break;
                                    }
                                }
                            }
                            row.Cells[3].Value = color;
                            brush = new SolidBrush(Color.FromArgb(color));
                            bmp = new Bitmap(16, 16);
                            gr = Graphics.FromImage(bmp);
                            gr.FillRectangle(brush, 0, 0, 16, 16);
                            gr.DrawRectangle(Pens.Black, 0, 0, 16, 16);
                            row.Cells[2].Value = bmp;
                            row.Cells[4].Value = i;
                            bmpList.Add(bmp);
                            
                            dgvOilObjColor.Rows.Add(row);
                            brush.Dispose();
                        }
                    }

                }
            }
        }
        public void ReadOilObjColorsFromSection()
        {
            if (mainForm != null && mainForm.indActiveProject > -1)
            {
                var dict = (StratumDictionary)mainForm.GetLastProject().DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                int i, OilObjCount = 0;
                int[] OilObjCodes = null;
                int[] OilObjColors = null;

                if (dict != null)
                {
                    if (AppSett != null)
                    {
                        AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.OIL_OBJ_COLORS);
                        if (section != null)
                        {
                            section.Data.Seek(0, SeekOrigin.Begin);
                            BinaryReader br = new BinaryReader(section.Data);
                            int ver = br.ReadInt32();
                            if (ver == 0)
                            {
                                OilObjCount = br.ReadInt32();
                                OilObjCodes = new int[OilObjCount];
                                OilObjColors = new int[OilObjCount];
                                for (i = 0; i < OilObjCount; i++)
                                {
                                    OilObjCodes[i] = br.ReadInt32();
                                    OilObjColors[i] = br.ReadInt32();
                                }
                            }
                        }
                    }
                    if (OilObjCodes != null)
                    {
                        StratumTreeNode node;
                        for (i = 0; i < OilObjCodes.Length; i++)
                        {
                            node = dict.GetStratumTreeNode(OilObjCodes[i]);
                            if (node != null)
                            {
                                node.ARGB = OilObjColors[i];
                            }
                        }
                    }
                }
            }
        }
        public void WriteOilObjColorsToSection()
        {
            Project proj = mainForm.GetLastProject();
            if (proj == null) return;
            var dict = (StratumDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            if ((AppSett != null) && (dict != null))
            {
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.OIL_OBJ_COLORS);
                DataGridViewRow row;
                MemoryStream ms = new MemoryStream();
                if (section == null)
                {
                    section = new AppSettingSection((int)Constant.APP_SETTINGS_TYPE.OIL_OBJ_COLORS, ms);
                    AppSett.AddSection(section);
                }
                else
                {
                    section.Data.Dispose();
                    section.Data = ms;
                }
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(0); // записываем версию
                List<int> list = new List<int>();
                int i;
                for (i = 0; i < dgvOilObjColor.Rows.Count; i++)
                {
                    row = dgvOilObjColor.Rows[i];
                    if ((int)row.Cells[5].Value == 1)
                    {
                        list.Add((int)row.Cells[4].Value);
                    }
                }
                bw.Write(list.Count);
                StratumTreeNode node;
                for (i = 0; i < list.Count; i++)
                {
                    node = dict.stratumTree.GetNodeByCode(list[i]);
                    if (node != null)
                    {
                        bw.Write(node.StratumCode);
                        bw.Write(node.ARGB);
                    }
                }
            }
        }
        public void WriteOilObjColorsToDict()
        {
            var dict = (StratumDictionary)mainForm.GetLastProject().DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            if (dict != null)
            {
                DataGridViewRow row;
                StratumTreeNode node;
                int i, ind;
                for (i = 0; i < dgvOilObjColor.Rows.Count; i++)
                {
                    row = dgvOilObjColor.Rows[i];
                    ind = (int)row.Cells[4].Value;
                    if ((int)row.Cells[5].Value == 1)
                    {
                        node = dict.stratumTree[ind];
                        node.ARGB = (int)row.Cells[3].Value;
                    }
                }
            }
        }
        private void twSettingTitle_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode tn = ((TreeView)sender).SelectedNode;
            if (tn.Tag != null) ((Panel)tn.Tag).Visible = true;
        }
        private void twSettingTitle_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            //if (e.Node == twSettingTitle.SelectedNode) e.Cancel = true;
        }
        private void dgvOilObjColor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 2) && (e.RowIndex > -1))
            {
                var dict = (StratumDictionary)mainForm.GetLastProject().DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                StratumTreeNode node;
                int ind = (int)dgvOilObjColor.Rows[e.RowIndex].Cells[4].Value;
                if (ind < dict.stratumTree.Count)
                {
                    node = dict.stratumTree[ind];
                    int color = node.ARGB;
                    using (ColorDialog dlg = new ColorDialog())
                    {
                        dlg.AllowFullOpen = true;
                        dlg.AnyColor = true;
                        dlg.SolidColorOnly = true;
                        dlg.Color = Color.FromArgb(color);
                        if (dlg.ShowDialog() == DialogResult.OK)
                        {
                            if (color != dlg.Color.ToArgb())
                            {
                                Bitmap bmp = (Bitmap)bmpList[e.RowIndex];
                                SolidBrush br = new SolidBrush(dlg.Color);
                                node.ARGB = dlg.Color.ToArgb();
                                Graphics gr = Graphics.FromImage(bmp);
                                gr.FillRectangle(br, 0, 0, 16, 16);
                                gr.DrawRectangle(Pens.Black, 0, 0, 16, 16);
                                dgvOilObjColor.Rows[e.RowIndex].Cells[2].Value = bmp;
                                dgvOilObjColor.Rows[e.RowIndex].Cells[3].Value = dlg.Color.ToArgb();
                                dgvOilObjColor.Rows[e.RowIndex].Cells[5].Value = 1;
                                br.Dispose();
                                OilObjColorsChanged = true;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region COR SCHEME BASE
        public PlaneBaseSchemes ReadCorSchemeBaseFromAppSettings()
        {
            if (mainForm != null && AppSett != null)
            {
                bool loaded = false;
                PlaneBaseSchemes baseSchemes = new PlaneBaseSchemes();
                PlaneBaseSchemes defaultSchemes = new PlaneBaseSchemes();
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.COR_SCHEME_BASE);
                if (section != null)
                {
                    section.Data.Seek(0, SeekOrigin.Begin);
                    try
                    {
                        BinaryReader br = new BinaryReader(section.Data);
                        int ver = br.ReadInt32();
                        baseSchemes.ReadFromAppSettings(br, ver);
                        loaded = true;
                    }
                    catch
                    {
                        loaded = false;
                    }
                }
                if (!loaded) baseSchemes.CreateBaseSchemes();
                // добавляем новые объекты
                defaultSchemes.CreateBaseSchemes();
                baseSchemes.RemoveDoubleObjects();
                baseSchemes.AddNewObjects(defaultSchemes);
                return baseSchemes;
            }
            return null;
        }
        public void WriteCorSchemeBaseToAppSettings(PlaneBaseSchemes BaseSchemes)
        {
            if (AppSett != null && BaseSchemes != null)
            {
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.COR_SCHEME_BASE);
                MemoryStream ms = new MemoryStream();
                if (section == null)
                {
                    section = new AppSettingSection((int)Constant.APP_SETTINGS_TYPE.COR_SCHEME_BASE, ms);
                    AppSett.AddSection(section);
                }
                else
                {
                    section.Data.Dispose();
                    section.Data = ms;
                }
                BinaryWriter bw = new BinaryWriter(section.Data);
                bw.Write(5); // версия настроек корсхем
                BaseSchemes.WriteToAppSettings(bw);
                AppSett.Write();
            }
        }
        #endregion

        #region GTM AUTOMATCH FORM
        public void ReadGtmAutoMatchFormSettings(SmartPlus.Dialogs.GTMAutoMatchForm GtmForm)
        {
            if (mainForm != null && AppSett != null)
            {
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.GTM_AUTOMATCH);
                if (section != null)
                {
                    section.Data.Seek(0, SeekOrigin.Begin);
                    try
                    {
                        BinaryReader br = new BinaryReader(section.Data, Encoding.UTF8);
                        int version = br.ReadInt32();
                        GtmForm.ReadSetting(version, br);
                    }
                    catch
                    {
                    }
                }
            }
        }
        public void WriteGtmAutoMatchFormSettings(SmartPlus.Dialogs.GTMAutoMatchForm GtmForm)
        {
            if (AppSett != null && GtmForm != null)
            {
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.GTM_AUTOMATCH);
                MemoryStream ms = new MemoryStream();
                if (section == null)
                {
                    section = new AppSettingSection((int)Constant.APP_SETTINGS_TYPE.GTM_AUTOMATCH, ms);
                    AppSett.AddSection(section);
                }
                else
                {
                    section.Data.Dispose();
                    section.Data = ms;
                }
                BinaryWriter bw = new BinaryWriter(section.Data, Encoding.UTF8);
                bw.Write(0); // версия настроек
                GtmForm.WriteSetting(bw);
                AppSett.Write();
            }
        }
        #endregion

        #region COMMON SETTINGS & UPDATER SETTINGS
        private void ReadCommonSettingFromSection()
        {
            if (AppSett != null)
            {
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.COMMON_SETTINGS);
                if (section != null && mainForm.AppSettings != null)
                {
                    CommonSet_ShowAllAreas.Checked = mainForm.AppSettings.ShowAllOilfieldAreas;
                    int x = (int)mainForm.AppSettings.CopyClipboardFormat;
                    if (x < Common_Set_CopyImageFormatBox.Items.Count)
                    {
                        Common_Set_CopyImageFormatBox.SelectedIndex = (int)mainForm.AppSettings.CopyClipboardFormat;
                    }
                    CommonSet_AttachContousToFilter.Checked = mainForm.AppSettings.AttachContoursToFilter;
                    CommonSet_HideWellsWithoutCoordByFilter.Checked = mainForm.AppSettings.HideWellsWithoutCoordByFilter;
                    CommonSet_FillOilfieldNgduRegion.Checked = mainForm.AppSettings.FillOilfieldNgduRegions;
                    Common_Set_LiquidInCube.Checked = mainForm.AppSettings.ShowDataLiquidInCube;
                    CommonSet_AttachAreasToFilter.Checked = mainForm.AppSettings.AttachAreasToFilter;
                    Chart_UseInjWellsPressure.Checked = mainForm.AppSettings.UseInjWellsPressure;
                    Chart_CalcZabPressureAverage.Checked = mainForm.AppSettings.CalcZabPressureAverage;
                    Update_HTTPS_CheckBox.Checked = mainForm.AppSettings.UpdatebyHttps;
                    Update_Password_TextBox.Text = mainForm.AppSettings.UpdatebyHttpsPassword;
                    Update_UserName_TextBox.Text = mainForm.AppSettings.UpdatebyHttpsUserName;
                    Update_Server_TextBox.Text = mainForm.AppSettings.UpdatebyHttpsServerName;
                    Update_Load_Colors_CheckBox.Checked = mainForm.AppSettings.UpdatebyHttpsLoadColorScheme;

                }
            }
        }
        private void WriteCommonSettingToSection()
        {
            if (AppSett != null)
            {
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.COMMON_SETTINGS);
                MemoryStream ms = new MemoryStream();
                if (section == null)
                {
                    section = new AppSettingSection((int)Constant.APP_SETTINGS_TYPE.COMMON_SETTINGS, ms);
                    AppSett.AddSection(section);
                }
                else
                {
                    section.Data.Dispose();
                    section.Data = ms;
                }
                if (mainForm.AppSettings == null) mainForm.AppSettings = new AppCommonSettings();
                mainForm.AppSettings.ShowAllOilfieldAreas = CommonSet_ShowAllAreas.Checked;
                mainForm.AppSettings.CopyClipboardFormat = (Constant.CopyImageFormat)Common_Set_CopyImageFormatBox.SelectedIndex;
                mainForm.AppSettings.AttachContoursToFilter = CommonSet_AttachContousToFilter.Checked;
                mainForm.AppSettings.HideWellsWithoutCoordByFilter = CommonSet_HideWellsWithoutCoordByFilter.Checked;
                mainForm.AppSettings.FillOilfieldNgduRegions = CommonSet_FillOilfieldNgduRegion.Checked;
                mainForm.AppSettings.ShowDataLiquidInCube = Common_Set_LiquidInCube.Checked;
                mainForm.AppSettings.AttachAreasToFilter = CommonSet_AttachAreasToFilter.Checked;
                mainForm.AppSettings.UseInjWellsPressure = Chart_UseInjWellsPressure.Checked;
                mainForm.AppSettings.CalcZabPressureAverage = Chart_CalcZabPressureAverage.Checked;
                mainForm.AppSettings.UpdatebyHttps = Update_HTTPS_CheckBox.Checked;
                mainForm.AppSettings.UpdatebyHttpsPassword = Update_Password_TextBox.Text;
                mainForm.AppSettings.UpdatebyHttpsUserName = Update_UserName_TextBox.Text;
                mainForm.AppSettings.UpdatebyHttpsServerName = Update_Server_TextBox.Text;
                mainForm.AppSettings.UpdatebyHttpsLoadColorScheme = Update_Load_Colors_CheckBox.Checked;
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(AppCommonSettings.Version);
                mainForm.AppSettings.Write(bw);
                AppSett.Write();
            }
        }
        public AppCommonSettings ReadAppCommonSettings()
        {
            if (mainForm != null && AppSett != null)
            {
                AppCommonSettings settings = new AppCommonSettings();
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.COMMON_SETTINGS);
                if (section != null)
                {
                    section.Data.Seek(0, SeekOrigin.Begin);
                    try
                    {
                        BinaryReader br = new BinaryReader(section.Data);
                        if (AppCommonSettings.Version == br.ReadInt32())
                        {
                            settings.Read(br);
                            return settings;
                        }
                    }
                    catch
                    {
                    }
                }
                else
                {
                    settings.ShowFindTooltip = true;
                    return settings;
                }
            }
            return null;
        }
        public void WriteAppCommonSettings(AppCommonSettings AppSettings)
        {
            if (mainForm != null && AppSett != null && AppSettings != null)
            {
                AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.COMMON_SETTINGS);
                MemoryStream ms = new MemoryStream();
                if (section == null)
                {
                    section = new AppSettingSection((int)Constant.APP_SETTINGS_TYPE.COMMON_SETTINGS, ms);
                    AppSett.AddSection(section);
                }
                else
                {
                    section.Data.Dispose();
                    section.Data = ms;
                }
                BinaryWriter bw = new BinaryWriter(section.Data);
                bw.Write(AppCommonSettings.Version);
                AppSettings.Write(bw);
                AppSett.Write();
            }
        }
        private void ShowSmartPlusFolder()
        {
            this.CommonSet_ProjectFolderLabel.Text = mainForm.UserSmartPlusFolder;
        }
        #endregion

        private void Chek_Connection_Button_Click(object sender, EventArgs e)
        {
            BackgroundWorker worker;
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_CheckHttpsConnection);
            worker.RunWorkerAsync();            
        }

        private void worker_CheckHttpsConnection(object sender, DoWorkEventArgs e)
        {
            bool server_available = true;
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                SetUpdateHTTPS_labelConnectionState("Проверка соединения...", Color.Black);
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                try
                {
                    System.Net.NetworkInformation.PingReply pingResult = ping.Send(this.Update_Server_TextBox.Text, 3000);
                    if (pingResult.Status != System.Net.NetworkInformation.IPStatus.Success)
                    {
                        server_available = false;
                    }
                }
                catch
                {
                    server_available = false;
                }
            }
            else
            {
                server_available = false;
                SetUpdateHTTPS_labelConnectionState("Сервер недоступен." + Environment.NewLine + "Проверьте сетевое соединение!", Color.Red);
            }

            if (server_available)
            {
                HttpFileReciever http = new HttpFileReciever();
                if (http.CheckServerNamePassword("https://" + this.Update_Server_TextBox.Text + "//" + "SmartPlus.txt",
                    this.Update_UserName_TextBox.Text, this.Update_Password_TextBox.Text)
                    || http.CheckServerNamePassword("https://" + this.Update_Server_TextBox.Text + "//" + "Projects.txt",
                    this.Update_UserName_TextBox.Text, this.Update_Password_TextBox.Text))
                {
                    SetUpdateHTTPS_labelConnectionState("Сервер доступен", Color.Black);
                }
                else
                {
                    SetUpdateHTTPS_labelConnectionState("Сервер недоступен."  + Environment.NewLine +  "Проверьте правильность имени пользователя и пароля", Color.Red);
                }
            }
            else
            {
                SetUpdateHTTPS_labelConnectionState("Сервер недоступен." + Environment.NewLine + " Проверьте соединение или имя сервера", Color.Red);
            }
        }

        private void SetUpdateHTTPS_labelConnectionState(string text, Color color)
        {
            if (UpdateHTTPS_labelConnectionState.InvokeRequired)
                {
                    UpdateHTTPS_labelConnectionState.Invoke(new Action<string>((s) => UpdateHTTPS_labelConnectionState.Text = s), text);
                    UpdateHTTPS_labelConnectionState.Invoke(new Action<Color>((s) => UpdateHTTPS_labelConnectionState.ForeColor = s), color);
                }
                else
                {
                    UpdateHTTPS_labelConnectionState.Text = text;
                    UpdateHTTPS_labelConnectionState.ForeColor = color;
                }
        }

        private void CommonSet_SetProjectFolderButton_Click(object sender, EventArgs e)
        {

        }

        private void CommonSet_OpenProjectFolderButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(mainForm.UserSmartPlusFolder);
        }

    }
    public class AppSettingSection
    {
        public Constant.APP_SETTINGS_TYPE Type;
        public MemoryStream Data;
        public AppSettingSection(int SectionType, MemoryStream ms)
        {
            Type = (Constant.APP_SETTINGS_TYPE)SectionType;
            Data = ms;
        }
    }
    public class AppSetting
    {
        public string InitDir;
        List<AppSettingSection> Items;
        public AppSetting(string InitDirectory)
        {
            InitDir = InitDirectory;
            Items = new List<AppSettingSection>();
        }
        public void AddSection(AppSettingSection section)
        {
            Items.Add(section);
        }
        public AppSettingSection GetSectionByType(Constant.APP_SETTINGS_TYPE SectionType)
        {
            if (Items.Count > 0)
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    if (Items[i].Type == SectionType)
                    {
                        return Items[i];
                    }
                }
            }
            return null;
        }
        public bool Load()
        {
            bool res = false;
            if ((InitDir != "") && (Directory.Exists(InitDir)))
            {
                string fileName = InitDir + "\\settings.bin";
                if (File.Exists(fileName))
                {
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    try
                    {
                        int i, j, SettType, Count_byte;
                        int CountSectinon = br.ReadInt32();
                        for (i = 0; i < CountSectinon; i++)
                        {
                            SettType = br.ReadInt32();
                            Count_byte = br.ReadInt32();
                            MemoryStream ms = new MemoryStream(Count_byte);
                            BinaryWriter bw = new BinaryWriter(ms);
                            for (j = 0; j < Count_byte; j++)
                            {
                                bw.Write(br.ReadByte());
                            }
                            AppSettingSection section = new AppSettingSection(SettType, ms);
                            AddSection(section);
                        }
                        res = true;
                    }
                    catch
                    {
                    }
                    finally
                    {
                        br.Close();
                    }
                }
            }
            return res;
        }
        public bool Write()
        {
            bool res = false;
            
            if (InitDir != "")
            {
                string fileName = InitDir + "\\settings.bin";
                try
                {
                    if (!Directory.Exists(InitDir)) Directory.CreateDirectory(InitDir);
                    FileStream fs = new FileStream(fileName, FileMode.Create);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(Items.Count);
                    AppSettingSection section;
                    for (int i = 0; i < Items.Count; i++)
                    {
                        section = (AppSettingSection)Items[i];
                        bw.Write((int)section.Type);
                        bw.Write((int)section.Data.Length);
                        section.Data.Seek(0, SeekOrigin.Begin);
                        section.Data.WriteTo(fs);
                    }
                    bw.Close();
                    res = true;
                }
                catch
                {
                    if (File.Exists(fileName)) File.Delete(fileName);
                    res = false;
                }
            }
            return res;
        }
    }
    public class AppCommonSettings
    {
        public static int Version = 0;
        public bool ShowFindTooltip;
        public bool ShowAllOilfieldAreas;
        public bool AttachContoursToFilter;
        public bool AttachAreasToFilter;
        public bool HideWellsWithoutCoordByFilter;
        public bool ShowDataLiquidInCube;
        public bool FillOilfieldNgduRegions;
        public Constant.CopyImageFormat CopyClipboardFormat;
        public string CopyClipboardFormatName
        {
            get
            {
                string format = string.Empty;
                switch (CopyClipboardFormat)
                {
                    case Constant.CopyImageFormat.BMP:
                        format = "BMP";
                        break;
                    case Constant.CopyImageFormat.EMFplus:
                        format = "EMF+";
                        break;
                }
                return format;
            }
        }

        // Chart Settings
        public bool UseInjWellsPressure;
        public bool CalcZabPressureAverage;

        //Updater Settings
        public bool UpdatebyHttps;
        public string UpdatebyHttpsUserName;
        public string UpdatebyHttpsPassword;
        public string UpdatebyHttpsServerName;
        public bool UpdatebyHttpsLoadColorScheme;
        
        public AppCommonSettings() 
        {
            ShowFindTooltip = false;
            ShowAllOilfieldAreas = false;
            CopyClipboardFormat = Constant.CopyImageFormat.BMP;
            AttachContoursToFilter = false;
            HideWellsWithoutCoordByFilter = false;
            ShowDataLiquidInCube = false;
            UseInjWellsPressure = true;
            CalcZabPressureAverage = false;
	    	FillOilfieldNgduRegions = true;
			AttachAreasToFilter = false;
            UpdatebyHttps = false;
            UpdatebyHttpsPassword = "";
            UpdatebyHttpsUserName = "";
            UpdatebyHttpsServerName = "";
            UpdatebyHttpsLoadColorScheme = false;

        }
        public void Read(BinaryReader br)
        {
            try
            {
                ShowFindTooltip = br.ReadBoolean();
                ShowAllOilfieldAreas = br.ReadBoolean();
                CopyClipboardFormat = (Constant.CopyImageFormat)br.ReadByte();
                if (br.BaseStream.Position < br.BaseStream.Length) AttachContoursToFilter = br.ReadBoolean();
                if (br.BaseStream.Position < br.BaseStream.Length) HideWellsWithoutCoordByFilter = br.ReadBoolean();
                if (br.BaseStream.Position < br.BaseStream.Length) ShowDataLiquidInCube = br.ReadBoolean();
                if (br.BaseStream.Position < br.BaseStream.Length) UseInjWellsPressure = br.ReadBoolean();
                if (br.BaseStream.Position < br.BaseStream.Length) CalcZabPressureAverage = br.ReadBoolean();
                if (br.BaseStream.Position < br.BaseStream.Length) UpdatebyHttps = br.ReadBoolean();
                if (br.BaseStream.Position < br.BaseStream.Length) UpdatebyHttpsUserName = br.ReadString();
                if (br.BaseStream.Position < br.BaseStream.Length) UpdatebyHttpsPassword = br.ReadString();
                if (br.BaseStream.Position < br.BaseStream.Length) UpdatebyHttpsServerName = br.ReadString();
                if (br.BaseStream.Position < br.BaseStream.Length) UpdatebyHttpsLoadColorScheme = br.ReadBoolean();
				if (br.BaseStream.Position < br.BaseStream.Length) FillOilfieldNgduRegions = br.ReadBoolean();
				if (br.BaseStream.Position < br.BaseStream.Length) AttachAreasToFilter = br.ReadBoolean();
            }
            catch
            {
            }
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(ShowFindTooltip);
            bw.Write(ShowAllOilfieldAreas);
            bw.Write((byte)CopyClipboardFormat);
            bw.Write(AttachContoursToFilter);
            bw.Write(HideWellsWithoutCoordByFilter);
            bw.Write(ShowDataLiquidInCube);
            bw.Write(UseInjWellsPressure);
            bw.Write(CalcZabPressureAverage);
            bw.Write(UpdatebyHttps);
            bw.Write(UpdatebyHttpsUserName);
            bw.Write(UpdatebyHttpsPassword);
            bw.Write(UpdatebyHttpsServerName);
            bw.Write(UpdatebyHttpsLoadColorScheme);
            bw.Write(FillOilfieldNgduRegions);
            bw.Write(AttachAreasToFilter);
        }
    }
}

