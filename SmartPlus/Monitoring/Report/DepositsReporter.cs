﻿using RDF.Objects;
using SmartPlus.DictionaryObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SmartPlus.Report
{
#if BASHNEFT

    class DepositsReporter
    {
        public static bool IndexOfArray(List<object[]> list, object[] row)
        {
            if (list.Count > 0 && row != null && list[0].Length == row.Length)
            {
                int counter = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    counter = 0;
                    for (int j = 0; j < row.Length; j++)
                    {
                        if ((list[i][j] == null && row[j] == null) || (list[i][j] != null && row[j] != null && list[i][j].ToString() == row[j].ToString())) counter++;
                    }
                    if (counter == row.Length) return true;
                }
            }
            return false;
        }

        internal class WellPerfDateItem
        {
            public DateTime dateStart, dateEnd;
            public List<SkvPerfItem> perf;
            public List<WellGisProdItem> gisProd;
            public List<int> merCharWorks;
            public List<int> merPlasts;
            public List<double> merWorkTimes;
            public bool ClosingIntervals;

            public WellPerfDateItem()
            {
                dateStart = DateTime.MinValue;
                dateEnd = DateTime.MaxValue;
                perf = new List<SkvPerfItem>();
                gisProd = new List<WellGisProdItem>();
                merPlasts = new List<int>();
                merCharWorks = new List<int>();
                merWorkTimes = new List<double>();
                ClosingIntervals = false;
            }
        }
        internal class WellGisProdItem
        {
            public SkvGisItem gis;
            public bool InContour;
            public List<int> plasts;
            public List<double> K;
            public double accumOil;
            public double accumWater;
            public double lastYearOil;
            public double lastYearLiq;

            public DateTime startDate;
            public double startQliq;
            public double startQoil;
            public double currentQliq;
            public double currentQoil;
            public double startWorkTime;
            public double currentWorkTime;

            public WellGisProdItem()
            {
                plasts = new List<int>();
                K = new List<double>();
                
                startDate = DateTime.MinValue;
            }
        }

        public static void ReportGisIntervals(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            var litDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LITOLOGY);
            var satDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
            OilField of;
            Well w;
            Deposit dep;
            int i, ind;
            double xG, yG;
            bool inContour;
            MSOffice.Excel excel = null;
            List<object[]> errors = new List<object[]>();
            List<object[]> outList = new List<object[]>();
            List<object[]> gisList = new List<object[]>();
            List<object[]> gisStat = new List<object[]>();
            object[] row, err;
            string str;
            excel = new MSOffice.Excel();
            excel.EnableEvents = false;
            excel.Visible = false;
            excel.NewWorkbook();

            for (int ofInd = 0; ofInd < project.OilFields.Count; ofInd++)
            {
                of = project.OilFields[ofInd];
                if (of.Deposits.Count > 0)
                {
                    of.LoadGisFromCache(worker, e);

                    for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                    {
                        w = of.Wells[wInd];
                        if (w.Missing) continue;
                        inContour = false;
                        for (int dInd = 0; dInd < of.Deposits.Count; dInd++)
                        {
                            dep = of.Deposits[dInd];


                            ind = -1;
                            if (w.coord != null)
                            {
                                for (i = 0; i < w.coord.Count; i++)
                                {
                                    if (w.coord.Items[i].PlastId == dep.StratumCode)
                                    {
                                        ind = i;
                                        break;
                                    }
                                }
                                if (ind == -1)
                                {
                                    for (i = 0; i < w.coord.Count; i++)
                                    {
                                        if (w.coord.Items[i].PlastId == dep.StratumParentCode)
                                        {
                                            ind = i;
                                            break;
                                        }
                                    }
                                }
                                if (ind != -1)
                                {
                                    of.CoefDict.GlobalCoordFromLocal(w.coord.Items[ind].X, w.coord.Items[ind].Y, out xG, out yG);
                                }
                                else
                                {
                                    err = new object[5];
                                    err[0] = of.Name;
                                    err[1] = dep.Name;
                                    err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                                    err[3] = w.Name;
                                    err[4] = "Не найдено пластопересечение";
                                    if (!IndexOfArray(errors, err)) errors.Add(err);
                                    xG = w.X;
                                    yG = w.Y;
                                }
                            }
                            else
                            {
                                err = new object[5];
                                err[0] = of.Name;
                                err[3] = w.Name;
                                err[4] = "Не загружены координаты";
                                if (!IndexOfArray(errors, err)) errors.Add(err);
                                xG = w.X;
                                yG = w.Y;
                            }
                            inContour = dep.contour.PointBodyEntry(xG, yG) || inContour;

                            gisList.Clear();
                            if (w.gis != null)
                            {
                                for (i = 0; i < w.gis.Count; i++)
                                {
                                    if (stratumDict.EqualsStratum(dep.StratumCode, w.gis.Items[i].PlastId) || stratumDict.EqualsStratum(dep.StratumCode, w.gis.Items[i].PlastZoneId))
                                    {
                                        if (w.gis.Items[i].Collector && w.gis.Items[i].Sat0 == 1)
                                        {
                                            row = new object[11];
                                            row[0] = of.Name;
                                            row[1] = stratumDict.GetShortNameByCode(dep.StratumCode);
                                            row[2] = dep.Name;
                                            row[3] = w.Name;
                                            row[4] = stratumDict.GetShortNameByCode(w.gis.Items[i].PlastId);
                                            row[5] = w.gis.Items[i].H;
                                            row[6] = w.gis.Items[i].H + w.gis.Items[i].L;
                                            row[7] = w.gis.Items[i].L;
                                            row[8] = w.gis.Items[i].Collector ? "Коллектор" : "Неколлектор";
                                            row[9] = satDict.GetShortNameByCode(w.gis.Items[i].Sat0);
                                            row[10] = litDict.GetShortNameByCode(w.gis.Items[i].LitId);
                                            gisList.Add(row);
                                        }
                                    }
                                }
                                outList.AddRange(gisList.ToArray());
                            }
                            else
                            {
                                err = new object[5];
                                err[0] = of.Name;
                                err[3] = w.Name;
                                err[4] = "Не загружены ГИС по скважине";
                                if (!IndexOfArray(errors, err)) errors.Add(err);
                            }
                        }
                        if (gisList.Count > 0 && !inContour)
                        {
                            err = new object[5];
                            err[0] = of.Name;
                            err[1] = string.Empty;
                            str = string.Empty;
                            for (i = 0; i < gisList.Count; i++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += gisList[i][4];
                            }
                            err[2] = str;
                            err[3] = w.Name;
                            err[4] = "Скважина с пропластком ГИС не входит в контур";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                    }
                    of.ClearGisData(false);
                }
            }
            if (excel != null)
            {
                if (outList.Count > 0)
                {
                    row = new object[11];
                    row[0] = "Месторождение";
                    row[1] = "Объект залежи";
                    row[2] = "Залежь";
                    row[3] = "Скважина";
                    row[4] = "Пласт по ГИС";
                    row[5] = "Hmd";
                    row[6] = "Hmd + L";
                    row[7] = "L";
                    row[8] = "Коллектор/Неколлектор";
                    row[9] = "Насыщение";
                    row[10] = "Литология";
                    outList.Insert(0, row);
                    excel.SetRowsArray(0, 0, outList);
                }
                excel.SetActiveSheet(1);
                if (errors.Count > 0)
                {
                    err = new object[5];
                    err[0] = "Месторождение";
                    err[1] = "Залежь";
                    err[2] = "Объект залежи";
                    err[3] = "Скважина";
                    err[4] = "Ошибка";
                    errors.Insert(0, err);
                    excel.SetRowsArray(0, 0, errors);
                }
                //excel.SetActiveSheet(2);
                //if (gisStat.Count > 0)
                //{
                //    for (j = 0; j < gisStat.Count; j++)
                //    {
                //        gisStat[j][0] = satDict.GetShortNameByCode(Convert.ToInt32(gisStat[j][0]));
                //    }
                //    row = new object[2];
                //    row[0] = "Объект";
                //    row[1] = "Количество";
                //    gisStat.Insert(0, row);
                //    excel.SetRowsArray(0, 0, gisStat);
                //}
                excel.EnableEvents = true;
                excel.Visible = true;
            }
        }

        public static void ReportDepositOIZ(BackgroundWorker worker, DoWorkEventArgs e, Project project, List<int> NGDUList)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Выгрузка отчета 'Выработка по залежам'";
            userState.WorkCurrentTitle = "Выгрузка отчета 'Выработка по залежам'";

            var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            var litDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LITOLOGY);
            var satDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
            var stateDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STATE);

            StratumTreeNode node;
            WellPerfDateItem dateItem = null;
            List<WellPerfDateItem> dateItems = new List<WellPerfDateItem>();
            OilField of;
            Well w, w2;
            Deposit dep;
            int i, j, ind, ind2;
            double xG, yG;
            bool inContour, find, inOutContour;
            MSOffice.Excel excel = null;
            List<object[]> errors = new List<object[]>();
            List<object[]> outList = new List<object[]>();
            List<object[]> outDepositList = new List<object[]>();
            List<object[]> outProdDepositList = new List<object[]>();
            List<object[]> outCoveredIntervalList = new List<object[]>();
            object[] row, err;
            string str;
            excel = new MSOffice.Excel();
            excel.EnableEvents = false;
            excel.Visible = false;
            excel.NewWorkbook();
            bool testInContour;
            double sumOil, notGisIntervalsOil, notInContourOil, sumH;
            List<int> notDistribMerPlasts = new List<int>();
            List<double> notDistribOil = new List<double>();

            List<WellPerfDateItem>[] allDateItems;
            List<int>[] NotInContourPlasts, NotInContourPlastsWithProd;

            List<List<Deposit>> prodDeposits = new List<List<Deposit>>();
            float[] initSquare = null;
            DateTime date1 = project.maxMerDate.AddMonths(-12), date2 = project.maxMerDate.AddMonths(1);
            DateTime datePerf1, datePerf2;
            double AllWellOil = 0;
            bool outAllWellOil = false;
            Polygon poly;
            List<Well> wellList = new List<Well>();

            for (int ofInd = 0; ofInd < project.OilFields.Count; ofInd++)
            {
                of = project.OilFields[ofInd];
                if (NGDUList.Count > 0 && NGDUList.IndexOf(of.ParamsDict.NGDUCode) == -1) continue;

#if DEBUG
                if (of.Name != "АРЛАНСКОЕ") continue;
#endif
                if (of.Deposits.Count > 0)
                {
                    // заполняем перфорацию по скважинам
                    of.LoadGisFromCache(worker, e);
                    of.LoadPerfFromCache(worker, e);
                    of.LoadMerFromCache(worker, e, false);
                    
                    wellList.AddRange(of.Wells.ToArray());

                    if (of.Name == "АРЛАНСКОЕ") // crutch
                    {
                        ind = project.GetOFIndex("ЮСУПОВСКАЯ ПЛОЩАДЬ");
                        if (ind > -1)
                        {
                            wellList.AddRange(project.OilFields[ind].Wells.ToArray());
                            project.OilFields[ind].LoadGisFromCache(worker, e);
                            project.OilFields[ind].LoadPerfFromCache(worker, e);
                            project.OilFields[ind].LoadMerFromCache(worker, e, false);
                        }
                    }

                    initSquare = new float[of.Deposits.Count];

                    allDateItems = new List<WellPerfDateItem>[wellList.Count];
                    NotInContourPlasts = new List<int>[wellList.Count];
                    NotInContourPlastsWithProd = new List<int>[wellList.Count];

                    // сборка залежей по объектам разработки
                    userState.Element = of.Name;
                    userState.WorkCurrentTitle = "Обработка залежей по объектам разработки..";
                    worker.ReportProgress(0, userState);

                    prodDeposits.Clear();
                    bool insert;
                    List<Deposit> sortedDeposits = new List<Deposit>();
                    for (i = 0; i < of.Deposits.Count; i++)
                    {
                        insert = false;
                        for (j = 0; j < sortedDeposits.Count; j++)
                        {
                            if (of.Deposits[i].contour._points.Count < sortedDeposits[j].contour._points.Count)
                            {
                                sortedDeposits.Insert(j, of.Deposits[i]);
                                insert = true;
                                break;
                            }
                        }
                        if (!insert)
                        {
                            sortedDeposits.Add(of.Deposits[i]);
                        }
                    }
                    sortedDeposits.Reverse();
                    for (i = 0; i < sortedDeposits.Count; i++)
                    {
                        ind = -1;
                        for (j = 0; j < prodDeposits.Count; j++)
                        {
                            for (int k = 0; k < prodDeposits[j].Count; k++)
                            {
                                if (sortedDeposits[i].StratumParentCode == prodDeposits[j][k].StratumParentCode &&
                                    prodDeposits[j][k].contour.IntersectBounds(sortedDeposits[i].contour))
                                {
                                    poly = new Polygon();
                                    Contour cntr = prodDeposits[j][k].contour.GetSimplyContour(50);
                                    poly.AddContour(cntr);
                                    cntr = sortedDeposits[i].contour.GetSimplyContour(50);
                                    poly.AddContour(cntr);
                                    poly.OperateByContours(ContoursOperation.Crossing);
                                    List<Contour> cntrs = poly.GetContours();
                                    if (cntrs.Count > 0)
                                    {
                                        ind = j;
                                        break;
                                    }
                                }
                            }
                            if (ind > -1) break;
                        }
                        if (ind == -1)
                        {
                            ind = prodDeposits.Count;
                            prodDeposits.Add(new List<Deposit>());
                        }
                        initSquare[i] = sortedDeposits[i].Params.Square;
                        sortedDeposits[i].Params.Square = (float)sortedDeposits[i].contour._points.GetSquare();
                        prodDeposits[ind].Add(sortedDeposits[i]);
                        worker.ReportProgress((int)((i + 1) * 100 / (sortedDeposits.Count * 2)), userState);
                    }

                    // второй прогон
                    for (j = 0; j < prodDeposits.Count; j++)
                    {
                        for (int j2 = 0; j2 < prodDeposits.Count; j2++)
                        {
                            if (j2 != j && prodDeposits[j][0].StratumParentCode == prodDeposits[j2][0].StratumParentCode)
                            {
                                ind = -1;
                                for (int k = 0; k < prodDeposits[j].Count; k++)
                                {
                                    for (int k2 = 0; k2 < prodDeposits[j2].Count; k2++)
                                    {
                                        if (prodDeposits[j][k].contour.IntersectBounds(prodDeposits[j2][k2].contour))
                                        {
                                            poly = new Polygon();
                                            Contour cntr = prodDeposits[j][k].contour.GetSimplyContour(50);
                                            poly.AddContour(cntr);
                                            cntr = prodDeposits[j2][k2].contour.GetSimplyContour(50);
                                            poly.AddContour(cntr);
                                            poly.OperateByContours(ContoursOperation.Crossing);
                                            List<Contour> cntrs = poly.GetContours();
                                            if (cntrs.Count > 0)
                                            {
                                                prodDeposits[j].Add(prodDeposits[j2][k2]);
                                                prodDeposits[j2].RemoveAt(k2);
                                                k2--;
                                            }
                                        }
                                    }
                                }
                                if (prodDeposits[j2].Count == 0)
                                {
                                    prodDeposits.RemoveAt(j2);
                                    j2--;
                                }
                            }
                        }
                        worker.ReportProgress((int)((j + 1 + of.Deposits.Count) * 100 / (of.Deposits.Count * 2)), userState);
                    }
                    userState.WorkCurrentTitle = "Обработка данных по скважинам..";
                    for (int wInd = 0; wInd < wellList.Count; wInd++)
                    {
                        w = wellList[wInd];
                        if (w.Missing) continue;
                        
                        dateItems.Clear();
                        AllWellOil = 0;
                        outAllWellOil = false;
                        for (int k = 0; k < w.mer.Count; k++)
                        {
                            AllWellOil += w.mer.Items[k].Oil;
                        }
                        if (AllWellOil == 0) continue;

                        for (int dInd = 0; dInd < of.Deposits.Count; dInd++)
                        {
                            dep = of.Deposits[dInd];
                            
                            if (dep.contour.PointBodyEntry(w.X, w.Y))
                            {
                                inOutContour = false;
                                if (dep.contour.cntrsOut != null)
                                {
                                    for (int k = 0; k < dep.contour.cntrsOut.Count; k++)
                                    {
                                        inOutContour = dep.contour.cntrsOut[k].PointBodyEntry(w.X, w.Y);
                                        if (inOutContour) break;
                                    }
                                }
                                if (!inOutContour)
                                {
                                    if (dep.AllWellList == null)
                                    {
                                        dep.AllWellList = new Well[1];
                                        dep.AllWellList[0] = w;
                                    }
                                    else
                                    {
                                        find = false;
                                        for (i = 0; i < dep.AllWellList.Length; i++)
                                        {
                                            if (dep.AllWellList[i] == w)
                                            {
                                                find = true;
                                                break;
                                            }
                                        }
                                        if (!find)
                                        {
                                            Well[] newWells = new Well[dep.AllWellList.Length + 1];
                                            for (i = 0; i < dep.AllWellList.Length; i++)
                                            {
                                                newWells[i] = dep.AllWellList[i];
                                            }
                                            newWells[newWells.Length - 1] = w;
                                            dep.AllWellList = newWells;
                                        }
                                    }
                                }
                            }
                        }
                        if (!w.CoordLoaded)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[4] = w.Name;
                            err[5] = "Координаты";
                            err[7] = "Не загружены координаты по скважине";
                            err[6] = (!outAllWellOil) ? AllWellOil : 0;
                            if (AllWellOil > 0 && (!IndexOfArray(errors, err))) errors.Add(err);
                            outAllWellOil = true;
                            continue;
                        }
                        if (!w.GisLoaded)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[4] = w.Name;
                            err[5] = "ГИС";
                            err[7] = "Не загружены гис по скважине";
                            err[6] = (!outAllWellOil) ? AllWellOil : 0;
                            if (AllWellOil > 0 && (!IndexOfArray(errors, err))) errors.Add(err);
                            outAllWellOil = true;
                        }

                        #region Проверка данных по ГИС и залежам
                        inContour = false;
                        find = false;
                        int inContourCounter, gisCounter;
                        NotInContourPlasts[w.Index] = new List<int>();
                        NotInContourPlastsWithProd[w.Index] = new List<int>();

                        if (w.GisLoaded)
                        {
                            inContourCounter = 0;
                            gisCounter = 0;
                            for (i = 0; i < w.gis.Count; i++)
                            {
                                if (w.gis.Items[i].Collector && w.gis.Items[i].Sat0 == 1) gisCounter++;
                                ind = -1;
                                for (j = 0; j < w.coord.Count; j++)
                                {
                                    if (w.coord.Items[j].PlastId == w.gis.Items[i].PlastId)
                                    {
                                        ind = j;
                                        break;
                                    }
                                }
                                if (ind == -1)
                                {
                                    node = stratumDict.GetStratumTreeNode(w.gis.Items[i].PlastId);
                                    for (j = 0; j < w.coord.Count; j++)
                                    {
                                        if (node.GetParentLevelByCode(w.coord.Items[j].PlastId) > -1)
                                        {
                                            ind = j;
                                            break;
                                        }
                                    }
                                }
                                if (ind != -1)
                                {
                                    of.CoefDict.GlobalCoordFromLocal(w.coord.Items[ind].X, w.coord.Items[ind].Y, out xG, out yG);
                                }
                                else
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    err[2] = stratumDict.GetShortNameByCode(w.gis.Items[i].PlastId);
                                    err[4] = w.Name;
                                    err[5] = "Координаты";
                                    err[7] = "Скважина с пропластком ГИС не имеет пластопересечения";
                                    if (!IndexOfArray(errors, err)) errors.Add(err);
                                    xG = w.X;
                                    yG = w.Y;
                                }
                                testInContour = false;
                                    
                                for (int dInd = 0; dInd < of.Deposits.Count; dInd++)
                                {
                                    dep = of.Deposits[dInd];

                                    if (stratumDict.EqualsStratum(dep.StratumCode, w.gis.Items[i].PlastId) || stratumDict.EqualsStratum(dep.StratumCode, w.gis.Items[i].PlastZoneId))
                                    {
                                        inContour = dep.contour.PointBodyEntry(xG, yG);

                                        if (inContour)
                                        {
                                            inOutContour = false;
                                            if (dep.contour.cntrsOut != null)
                                            {
                                                for (int k = 0; k < dep.contour.cntrsOut.Count; k++)
                                                {
                                                    inOutContour = dep.contour.cntrsOut[k].PointBodyEntry(xG, yG);
                                                    if (inOutContour) break;
                                                }
                                            }
                                            if (!inOutContour)
                                            {
                                                testInContour = testInContour || inContour;

                                                if (dep.WellList == null)
                                                {
                                                    dep.WellList = new Well[1];
                                                    dep.WellList[0] = w;
                                                }
                                                else
                                                {
                                                    find = false;
                                                    for (int k = 0; k < dep.WellList.Length; k++)
                                                    {
                                                        if (w == dep.WellList[k])
                                                        {
                                                            find = true;
                                                            break;
                                                        }
                                                    }
                                                    if (!find)
                                                    {
                                                        Well[] wells = new Well[dep.WellList.Length + 1];
                                                        for (int k = 0; k < dep.WellList.Length; k++)
                                                        {
                                                            wells[k] = dep.WellList[k];
                                                        }
                                                        dep.WellList = wells;
                                                        dep.WellList[dep.WellList.Length - 1] = w;
                                                    }
                                                }
                                            }
                                            else if (w.gis.Items[i].Collector && w.gis.Items[i].Sat0 == 1)
                                            {
                                                err = new object[8];
                                                err[0] = of.ParamsDict.NGDUName;
                                                err[1] = of.Name;
                                                err[2] = stratumDict.GetShortNameByCode(w.gis.Items[i].PlastId);
                                                err[3] = dep.Name;
                                                err[4] = w.Name;
                                                err[5] = "ГИС";
                                                err[7] = "Скважина с пропластком ГИС входит в контур замещения";
                                                if (!IndexOfArray(errors, err)) errors.Add(err);
                                            }
                                        }
                                    }
                                }
                                if (!testInContour)
                                {
                                    if (w.gis.Items[i].Collector && w.gis.Items[i].Sat0 == 1) inContourCounter++;
                                    if (NotInContourPlasts[w.Index].IndexOf(w.gis.Items[i].PlastId) == -1)
                                    {
                                        NotInContourPlasts[w.Index].Add(w.gis.Items[i].PlastId);
                                    }
                                }
                            }
                            if (gisCounter == inContourCounter && gisCounter > 0)
                            {
                                err = new object[8];
                                err[0] = of.ParamsDict.NGDUName;
                                err[1] = of.Name;
                                err[4] = w.Name;
                                err[5] = "ГИС";
                                if (w.MerLoaded)
                                {
                                    err[6] = stateDict.GetShortNameByCode(w.mer.Items[w.mer.Count - 1].StateId);
                                }
                                err[7] = "Скважина с нефтенас.интервалами не входит ни в одну залежь";
                                if (!IndexOfArray(errors, err)) errors.Add(err);
                            }
                        }
                        #endregion

                        dateItems = new List<WellPerfDateItem>();
                        if (w.GisLoaded && w.PerfLoaded)
                        {
                            for (i = 0; i < w.gis.Count; i++)
                            {
                                if ((w.gis.Items[i].Habs <= 0 || w.gis.Items[i].Labs <= 0) && w.gis.Items[i].Collector && w.gis.Items[i].Sat0 == 1)
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    err[4] = w.Name;
                                    err[5] = "ГИС";
                                    err[6] = string.Format("[{0:0.##};{1:0.##}]", w.gis.Items[i].Habs, w.gis.Items[i].Labs);
                                    err[7] = "Нулевые или отрицательные Abs отметки ГИС";
                                    if (!IndexOfArray(errors, err)) errors.Add(err);
                                }
                            }
                            dateItem = new WellPerfDateItem();
                            for (i = 0; i < w.perf.Count; i++)
                            {
                                if (w.perf.Items[i].Top >= w.perf.Items[i].Bottom)
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    err[4] = w.Name;
                                    err[5] = "Перфорация";
                                    err[6] = string.Format("[{0:0.##};{1:0.##}]", w.perf.Items[i].Top, w.perf.Items[i].Bottom);
                                    err[7] = "Неверные отметки глубин перфорации";
                                    if (!IndexOfArray(errors, err)) errors.Add(err);
                                }

                                if (dateItem.dateStart == DateTime.MinValue || (dateItem.dateStart.Year == w.perf.Items[i].Date.Year && dateItem.dateStart.Month == w.perf.Items[i].Date.Month))
                                {
                                    dateItem.dateStart = w.perf.Items[i].Date;
                                }
                                else
                                {
                                    if (dateItems.Count > 0)
                                    {
                                        dateItems[dateItems.Count - 1].dateEnd = dateItem.dateStart;
                                    }
                                    dateItems.Add(dateItem);
                                    dateItem = new WellPerfDateItem();
                                    dateItem.dateStart = w.perf.Items[i].Date;
                                    if (dateItems.Count > 0) dateItem.perf.AddRange(dateItems[dateItems.Count - 1].perf.ToArray());
                                }
                                if (w.perf.Items[i].PerfTypeId != 10000)
                                {
                                    dateItem.perf.Add(w.perf.Items[i]);
                                }
                                else
                                {
                                    List<SkvPerfItem> perfs = new List<SkvPerfItem>();
                                    for (int k = 0; k < dateItem.perf.Count; k++)
                                    {
                                        if (SkvPerf.IntersectIntervals(dateItem.perf[k], w.perf.Items[i]))
                                        {
                                            if (dateItems.Count > 0) dateItems[dateItems.Count - 1].ClosingIntervals = true;
                                            SkvPerfItem perfItem = dateItem.perf[k];
                                            dateItem.perf.RemoveAt(k);
                                            perfs.AddRange(SkvPerf.DiffIntervals(perfItem, w.perf.Items[i]).ToArray());
                                            k--;
                                        }
                                    }
                                    dateItem.perf.AddRange(perfs.ToArray());
                                }
                            }
                            if (dateItem.dateStart.Year > 1900)
                            {
                                if (dateItems.Count > 0)
                                {
                                    dateItems[dateItems.Count - 1].dateEnd = dateItem.dateStart;
                                }
                                dateItems.Add(dateItem);
                            }
                            for (i = 0; i < dateItems.Count; i++)
                            {
                                if (dateItems[i].ClosingIntervals)
                                {
                                    dateItem = new WellPerfDateItem();
                                    dateItem.dateStart = dateItems[i].dateEnd;
                                    dateItem.dateEnd = dateItems[i].dateEnd.AddMonths(1);
                                    dateItem.dateEnd = new DateTime(dateItem.dateEnd.Year, dateItem.dateEnd.Month, 1);

                                    dateItem.perf.AddRange(dateItems[i].perf.ToArray());
                                    if (i < dateItems.Count - 1)
                                    {
                                        dateItems[i + 1].dateStart = dateItem.dateEnd;
                                        dateItem.perf.AddRange(dateItems[i + 1].perf.ToArray());
                                    }
                                    dateItems.Insert(i + 1, dateItem);
                                    i++;
                                }
                            }
                            for (i = 0; i < dateItems.Count; i++)
                            {
                                datePerf1 = new DateTime(dateItems[i].dateStart.Year, dateItems[i].dateStart.Month, 1);
                                datePerf2 = new DateTime(dateItems[i].dateEnd.Year, dateItems[i].dateEnd.Month, 1);
                                
                                for (int k = 0; k < w.mer.Count; k++)
                                {
                                    if (datePerf1 <= w.mer.Items[k].Date && w.mer.Items[k].Date < datePerf2)
                                    {
                                        if (w.mer.Items[k].Oil > 0 && dateItems[i].merPlasts.IndexOf(w.mer.Items[k].PlastId) == -1)
                                        {
                                            dateItems[i].merPlasts.Add(w.mer.Items[k].PlastId);
                                        }
                                        
                                        if (datePerf1 < w.mer.Items[k].Date)
                                        {
                                            ind = dateItems[i].merCharWorks.IndexOf(w.mer.Items[k].CharWorkId);
                                            if (ind == -1)
                                            {
                                                ind = dateItems[i].merCharWorks.Count;
                                                dateItems[i].merCharWorks.Add(w.mer.Items[k].CharWorkId);
                                                dateItems[i].merWorkTimes.Add(0);
                                            }
                                            dateItems[i].merWorkTimes[ind] += w.mer.Items[k].WorkTime;
                                        }
                                    }
                                    else if (w.mer.Items[k].Date >= datePerf2)
                                    {
                                        break;
                                    }
                                }
                                List<SkvGisItem> gisList = w.gis.GetCrossIntervals(dateItems[i].perf);
                                dateItems[i].gisProd = new List<WellGisProdItem>();
                                find = false;
                                for (j = 0; j < gisList.Count; j++)
                                {
                                    if (gisList[j].Collector && gisList[j].Sat0 == 1)
                                    {
                                        find = true;
                                        break;
                                    }
                                }
                                for (j = 0; j < gisList.Count; j++)
                                {
                                    if (!gisList[j].Collector)
                                    {
                                        err = new object[8];
                                        err[0] = of.ParamsDict.NGDUName;
                                        err[1] = of.Name;
                                        err[2] = stratumDict.GetShortNameByCode(gisList[j].PlastId);
                                        err[4] = w.Name;
                                        err[5] = "ГИС";
                                        err[6] = string.Format("[{0:0.##};{1:0.##}]", gisList[j].Habs, gisList[j].Habs + gisList[j].Labs) + "";
                                        err[7] = "Проперфорирован не коллектор";
                                        if (!IndexOfArray(errors, err)) errors.Add(err);
                                        if (find) continue;
                                    }
                                    if (gisList[j].Sat0 != 1)
                                    {
                                        err = new object[8];
                                        err[0] = of.ParamsDict.NGDUName;
                                        err[1] = of.Name;
                                        err[2] = stratumDict.GetShortNameByCode(gisList[j].PlastId);
                                        err[4] = w.Name;
                                        err[5] = "ГИС";
                                        ind= satDict.GetIndexByCode(gisList[j].Sat0);
                                        if (ind > -1) err[6] = satDict[ind].FullName;
                                        err[7] = "Проперфорирован не нефтенасыщенный интервал";
                                        if (!IndexOfArray(errors, err)) errors.Add(err);
                                        if (find) continue;
                                    }
                                    WellGisProdItem gisProdItem = new WellGisProdItem();
                                    gisProdItem.gis = gisList[j];
                                    gisProdItem.InContour = NotInContourPlasts[w.Index].IndexOf(gisProdItem.gis.PlastId) == -1;
                                    dateItems[i].gisProd.Add(gisProdItem);
                                }
                                sumH = 0;
                                List<int> indexes = new List<int>();
                                for (int k = 0; k < dateItems[i].merPlasts.Count; k++)
                                {
                                    indexes.Clear();
                                    sumH = 0;
                                    for (j = 0; j < dateItems[i].gisProd.Count; j++)
                                    {
                                        node = stratumDict.GetStratumTreeNode(dateItems[i].gisProd[j].gis.PlastId);
                                        if (node == null) continue;

                                        if (node.StratumCode == dateItems[i].merPlasts[k] || node.GetParentLevelByCode(dateItems[i].merPlasts[k]) > -1 ||
                                            (of.OilFieldCode == 109 && node.StratumCode == 48 && dateItems[i].merPlasts[k] == 39) ||
                                            (of.OilFieldCode == 69 && node.StratumCode == 48 && dateItems[i].merPlasts[k] == 38)) // crutch
                                        {
                                            dateItems[i].gisProd[j].plasts.Add(dateItems[i].merPlasts[k]);
                                            dateItems[i].gisProd[j].K.Add(0);
                                            sumH += dateItems[i].gisProd[j].gis.Labs;
                                            indexes.Add(j);
                                        }
                                    }
                                    for (j = 0; j < dateItems[i].gisProd.Count; j++)
                                    {
                                        node = stratumDict.GetStratumTreeNode(dateItems[i].gisProd[j].gis.PlastId);
                                        if (node == null) continue;

                                        if (node.StratumCode == dateItems[i].merPlasts[k] || node.GetParentLevelByCode(dateItems[i].merPlasts[k]) > -1 ||
                                            (of.OilFieldCode == 109 && node.StratumCode == 48 && dateItems[i].merPlasts[k] == 39) ||
                                            (of.OilFieldCode == 69 && node.StratumCode == 48 && dateItems[i].merPlasts[k] == 38)) // crutch
                                        {
                                            ind = dateItems[i].gisProd[j].plasts.IndexOf(dateItems[i].merPlasts[k]);
                                            if (ind != -1)
                                            {
                                                if (sumH > 0)
                                                {
                                                    dateItems[i].gisProd[j].K[ind] = dateItems[i].gisProd[j].gis.Labs / sumH;
                                                }
                                                else
                                                {
                                                    dateItems[i].gisProd[j].K[ind] = 0;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // суммируем добычу
                            if (dateItems.Count > 0)
                            {
                                j = 0;
                                
                                datePerf1 = new DateTime(dateItems[j].dateStart.Year, dateItems[j].dateStart.Month, 1);
                                datePerf2 = new DateTime(dateItems[j].dateEnd.Year, dateItems[j].dateEnd.Month, 1);
                                notGisIntervalsOil = 0;
                                notInContourOil = 0;
                                notDistribMerPlasts.Clear();
                                notDistribOil.Clear();

                                for (i = 0; i < w.mer.Count; i++)
                                {
                                    if (w.mer.Items[i].Oil > 0 && w.mer.Items[i].Date < datePerf1)
                                    {
                                        if ((dateItems[j].dateStart - w.mer.Items[i].Date).TotalDays > 20)
                                        {
                                            sumOil = 0;
                                            while (i < w.mer.Count && w.mer.Items[i].Date < datePerf1)
                                            {
                                                sumOil += w.mer.Items[i].Oil;
                                                i++;
                                            }
                                            err = new object[8];
                                            err[0] = of.ParamsDict.NGDUName;
                                            err[1] = of.Name;
                                            err[4] = w.Name;
                                            err[5] = "МЭР";
                                            err[6] = sumOil;
                                            err[7] = "Дата добычи по МЭР более ранняя чем начальная дата перфорации";
                                            errors.Add(err);
                                            if (i >= w.mer.Count) break;
                                        }
                                        else
                                        {
                                            datePerf1 = w.mer.Items[i].Date;
                                        }
                                    }
                                    if (j < dateItems.Count && datePerf1 <= w.mer.Items[i].Date && w.mer.Items[i].Date < datePerf2)
                                    {
                                        find = false;
                                        
                                        for (int k = 0; k < dateItems[j].gisProd.Count; k++)
                                        {
                                            ind = dateItems[j].gisProd[k].plasts.IndexOf(w.mer.Items[i].PlastId);

                                            if (ind > -1 && dateItems[j].gisProd[k].K[ind] > 0)
                                            {
                                                if (!dateItems[j].gisProd[k].InContour)
                                                {
                                                    if (NotInContourPlastsWithProd[w.Index].IndexOf(dateItems[j].gisProd[k].gis.PlastId) == -1)
                                                    {
                                                        NotInContourPlastsWithProd[w.Index].Add(dateItems[j].gisProd[k].gis.PlastId);
                                                    }
                                                    notInContourOil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                                }
                                                if (w.mer.Items[i].CharWorkId == 11 || w.mer.Items[i].CharWorkId == 15)
                                                {
                                                    if (w.mer.Items[i].Oil > 0)
                                                    {
                                                        ind2 = -1;
                                                        for (int x = 0; x < j; x++)
                                                        {
                                                            for (int t = 0; t < dateItems[x].gisProd.Count; t++)
                                                            {
                                                                if (dateItems[x].gisProd[t].gis.PlastId == dateItems[j].gisProd[k].gis.PlastId && dateItems[x].gisProd[t].startDate != DateTime.MinValue)
                                                                {
                                                                    ind2 = t;
                                                                    break;
                                                                }
                                                            }
                                                            if (ind2 != -1) break;
                                                        }
                                                        if (ind2 == -1 && (dateItems[j].gisProd[k].startDate == DateTime.MinValue || dateItems[j].gisProd[k].startDate == w.mer.Items[i].Date))
                                                        {
                                                            dateItems[j].gisProd[k].startDate = w.mer.Items[i].Date;
                                                            dateItems[j].gisProd[k].startQliq += dateItems[j].gisProd[k].K[ind] * (w.mer.Items[i].Oil + w.mer.Items[i].Wat);
                                                            dateItems[j].gisProd[k].startQoil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                                            dateItems[j].gisProd[k].startWorkTime += w.mer.Items[i].WorkTime + w.mer.Items[i].CollTime;
                                                        }
                                                        if (w.mer.Items[i].Date == project.maxMerDate)
                                                        {
                                                            dateItems[j].gisProd[k].currentQliq += dateItems[j].gisProd[k].K[ind] * (w.mer.Items[i].Oil + w.mer.Items[i].Wat);
                                                            dateItems[j].gisProd[k].currentQoil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                                            dateItems[j].gisProd[k].currentWorkTime += w.mer.Items[i].WorkTime + w.mer.Items[i].CollTime;
                                                        }
                                                    }
                                                }
                                                
                                                if (w.mer.Items[i].Date >= date1 && w.mer.Items[i].Date < date2)
                                                {
                                                    dateItems[j].gisProd[k].lastYearOil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                                    if (w.mer.Items[i].CharWorkId == 11 || w.mer.Items[i].CharWorkId == 15)
                                                    {
                                                        dateItems[j].gisProd[k].lastYearLiq += dateItems[j].gisProd[k].K[ind] * (w.mer.Items[i].Oil + w.mer.Items[i].Wat);
                                                    }
                                                }
                                                dateItems[j].gisProd[k].accumOil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                                dateItems[j].gisProd[k].accumWater += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Wat;
                                                find = true;
                                            }
                                        }
                                        
                                        if (!find && w.mer.Items[i].Oil > 0)
                                        {
                                            //if (j > 0 && w.mer.Items[i].Date.Year == dateItems[j - 1].dateEnd.Year && w.mer.Items[i].Date.Month == dateItems[j - 1].dateEnd.Month)
                                            //{
                                            //    find = false;
                                            //    for (int k = 0; k < dateItems[j - 1].gisProd.Count; k++)
                                            //    {
                                            //        ind = dateItems[j - 1].gisProd[k].plasts.IndexOf(w.mer.Items[i].PlastId);
                                            //        if (ind > -1 && dateItems[j - 1].gisProd[k].K[ind] > 0)
                                            //        {
                                            //            if (!dateItems[j - 1].gisProd[k].InContour)
                                            //            {
                                            //                notInContourOil += dateItems[j - 1].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                            //            }
                                            //            if (w.mer.Items[i].Date >= date1 && w.mer.Items[i].Date < date2)
                                            //            {
                                            //                dateItems[j - 1].gisProd[k].lastYearOil += dateItems[j - 1].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                            //                if (w.mer.Items[i].CharWorkId != 20)
                                            //                {
                                            //                    dateItems[j - 1].gisProd[k].lastYearLiq += dateItems[j - 1].gisProd[k].K[ind] * (w.mer.Items[i].Oil + w.mer.Items[i].Wat);
                                            //                }
                                            //            }
                                            //            dateItems[j - 1].gisProd[k].accumOil += dateItems[j - 1].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                            //            dateItems[j - 1].gisProd[k].accumWater += dateItems[j - 1].gisProd[k].K[ind] * w.mer.Items[i].Wat;
                                            //            find = true;
                                            //        }
                                            //    }
                                            //}
                                            if (!find)
                                            {
                                                ind = notDistribMerPlasts.IndexOf(w.mer.Items[i].PlastId);
                                                if (ind == -1)
                                                {
                                                    notDistribMerPlasts.Add(w.mer.Items[i].PlastId);
                                                    notDistribOil.Add(0);
                                                    ind = notDistribMerPlasts.Count - 1;
                                                }
                                                notDistribOil[ind] += w.mer.Items[i].Oil;
                                            }
                                        }
                                    }
                                    else if (j >= dateItems.Count && w.mer.Items[i].Oil > 0)
                                    {
                                        err = new object[8];
                                        err[0] = of.ParamsDict.NGDUName;
                                        err[1] = of.Name;
                                        err[4] = w.Name;
                                        err[5] = "МЭР";
                                        err[7] = "Для добычи по МЭР нет перфорации";
                                        if (!IndexOfArray(errors, err)) errors.Add(err);
                                    }
                                    if (w.mer.Items[i].Date >= datePerf2)
                                    {
                                        i--;
                                        j++;

                                        if (j < dateItems.Count)
                                        {
                                            datePerf1 = new DateTime(dateItems[j].dateStart.Year, dateItems[j].dateStart.Month, 1);
                                            datePerf2 = new DateTime(dateItems[j].dateEnd.Year, dateItems[j].dateEnd.Month, 1);
                                        }
                                    }
                                }

                                if (notGisIntervalsOil > 0)
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    err[4] = w.Name;
                                    err[5] = "ГИС";
                                    err[7] = "На интервалы перфорации не найдены интервалы ГИС";
                                    if (!IndexOfArray(errors, err)) errors.Add(err);
                                }

                                if (notInContourOil > 0)
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    str = string.Empty;
                                    for (int k = 0; k < NotInContourPlastsWithProd[w.Index].Count; k++)
                                    {
                                        if (str.Length > 0) str += ", ";
                                        str += stratumDict.GetShortNameByCode(NotInContourPlastsWithProd[w.Index][k]);
                                    }
                                    err[2] = str;
                                    err[4] = w.Name;
                                    err[5] = "ГИС";
                                    err[6] = notInContourOil;
                                    err[7] = "На интервалы ГИС c добычей не найдены залежи";
                                    if (!IndexOfArray(errors, err)) errors.Add(err);
                                }
                                if (notDistribMerPlasts.Count > 0)
                                {
                                    for (int k = 0; k < notDistribMerPlasts.Count; k++)
                                    {
                                        err = new object[8];
                                        err[0] = of.ParamsDict.NGDUName;
                                        err[1] = of.Name;
                                        err[2] = stratumDict.GetShortNameByCode(notDistribMerPlasts[k]);
                                        err[4] = w.Name;
                                        err[5] = "МЭР";
                                        err[6] = notDistribOil[k];
                                        err[7] = "Добыча по МЭР не имеет проперфорированных интервалов";
                                        errors.Add(err);
                                    }
                                }
                            }
                            for (i = 0; i < dateItems.Count; i++)
                            {
                                ind = -1;
                                for (int k = 0; k < dateItems[i].merCharWorks.Count;k++)
                                {
                                    if(dateItems[i].merCharWorks[k] != 20 && dateItems[i].merWorkTimes[k] > 0)
                                    {
                                        ind = k;
                                        break;
                                    }
                                }
                                if(ind > -1)
                                {
                                    for (int k = 0; k < dateItems[i].gisProd.Count; k++)
                                    {
                                        if (dateItems[i].gisProd[k].accumOil == 0 && dateItems[i].gisProd[k].accumWater == 0 && dateItems[i].gisProd[k].gis.Labs > 0)
                                        {
                                            err = new object[8];
                                            err[0] = of.ParamsDict.NGDUName;
                                            err[1] = of.Name;
                                            err[2] = stratumDict.GetShortNameByCode(dateItems[i].gisProd[k].gis.PlastId);
                                            err[4] = w.Name;
                                            err[5] = "ПЕРФ";
                                            err[6] = string.Format("[{0:dd.MM.yyyy};{1:dd.MM.yyyy}]", dateItems[i].dateStart, (dateItems[i].dateEnd.Year > DateTime.Now.Year ? DateTime.Now : dateItems[i].dateEnd));
                                            err[7] = "Для перфорированного интервала нет добычи по МЭР";
                                            if (!IndexOfArray(errors, err)) errors.Add(err);
                                        }
                                    }
                                }
                            }
                        }

                        allDateItems[w.Index] = new List<WellPerfDateItem>(dateItems.ToArray());

                        if (!w.PerfLoaded)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[4] = w.Name;
                            err[5] = "Перфорация";
                            err[6] = (!outAllWellOil) ? AllWellOil : 0;
                            err[7] = "Не загружена перфорация по скважине";
                            if (AllWellOil > 0 && !IndexOfArray(errors, err)) errors.Add(err);
                            outAllWellOil = true;
                        }
                        userState.Element = string.Format("{0} [{1}]", of.Wells[wInd].Name, of.Name);
                        worker.ReportProgress((int)((wInd + 1) * 100 / of.Wells.Count), userState);
                    }

                    List<Well> workWellList = new List<Well>();
                    double depLiq, sumSquare;
                    userState.Element = "Обработка залежей по скважинным данным..";
                    for (int dInd = 0; dInd < of.Deposits.Count; dInd++)
                    {
                        dep = of.Deposits[dInd];

                        #region Проверка параметров залежи
                        if (dep.Params.Square == 0)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Не заполнен параметр 'Площадь' залежи";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        if (dep.Params.InitHoil == 0)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Не заполнен параметр 'Нач.нефтенас.толщина' залежи";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        if (dep.Params.OilVolume == 0)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Не заполнен параметр 'Нефтенас.объем' залежи";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        if (dep.pvt.Porosity == 0)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Не заполнен параметр 'Пористость' залежи";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        if (dep.pvt.OilInitialSaturation == 0)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Не заполнен параметр 'Нач.нефтенасыщенность' залежи";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        if (dep.pvt.OilDensity == 0)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Не заполнен параметр 'Плотность нефти' залежи";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        if (dep.Params.InitGeoOilReserv == 0)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Не заполнен параметр 'Начальные балансовые запасы' залежи";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        if (dep.pvt.KIN == 0)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Не заполнен параметр 'Начальные извлекаемые запасы' залежи";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        #endregion

                        if (dep.WellList == null)
                        {
                            err = new object[8];
                            err[0] = of.ParamsDict.NGDUName;
                            err[1] = of.Name;
                            err[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                            err[3] = dep.Name;
                            err[4] = string.Empty;
                            err[5] = "Залежи";
                            err[7] = "Залежь не имеет ни одной скважины";
                            if (!IndexOfArray(errors, err)) errors.Add(err);
                        }
                        sumOil = 0;
                        depLiq = 0;

                        workWellList.Clear();
                        DateTime dt;
                        if (dep.WellList != null)
                        {
                            List<SkvGisItem> coveredGisItems = new List<SkvGisItem>();
                            MerComp merComp;

                            for (i = 0; i < dep.WellList.Length; i++)
                            {
                                w = dep.WellList[i];
                                coveredGisItems.Clear();
                                if (w.GisLoaded) coveredGisItems.AddRange(w.gis.Items);
                                for (j = 0; j < allDateItems[w.Index].Count; j++)
                                {
                                    for (int k = 0; k < allDateItems[w.Index][j].gisProd.Count; k++)
                                    {
                                        if (stratumDict.EqualsStratum(dep.StratumCode, allDateItems[w.Index][j].gisProd[k].gis.PlastId) || stratumDict.EqualsStratum(dep.StratumCode, allDateItems[w.Index][j].gisProd[k].gis.PlastZoneId))
                                        {
                                            for (int x = 0; x < coveredGisItems.Count; x++)
                                            {
                                                if (allDateItems[w.Index][j].gisProd[k].gis.H == coveredGisItems[x].H && allDateItems[w.Index][j].gisProd[k].gis.L == coveredGisItems[x].L &&
                                                    allDateItems[w.Index][j].gisProd[k].gis.Habs == coveredGisItems[x].Habs && allDateItems[w.Index][j].gisProd[k].gis.Labs == coveredGisItems[x].Labs)
                                                {
                                                    coveredGisItems.RemoveAt(x);
                                                    break;
                                                }
                                            }
                                            dep.Params.AccumOil += allDateItems[w.Index][j].gisProd[k].lastYearOil;
                                            depLiq += allDateItems[w.Index][j].gisProd[k].lastYearLiq;

                                            row = new object[13];
                                            row[0] = of.ParamsDict.NGDUName;
                                            row[1] = of.Name;
                                            row[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                                            row[3] = dep.Name;
                                            row[4] = w.Name;
                                            row[5] = allDateItems[w.Index][j].dateStart.ToShortDateString();
                                            if (allDateItems[w.Index][j].dateEnd > project.maxMerDate)
                                            {
                                                allDateItems[w.Index][j].dateEnd = project.maxMerDate;
                                            }
                                            dt = allDateItems[w.Index][j].dateEnd;
                                            if (dt.Year > DateTime.Now.Year) dt = DateTime.Now;
                                            row[6] = dt.ToShortDateString();
                                            row[7] = stratumDict.GetShortNameByCode(allDateItems[w.Index][j].gisProd[k].gis.PlastId);
                                            row[8] = allDateItems[w.Index][j].gisProd[k].gis.Habs;
                                            row[9] = allDateItems[w.Index][j].gisProd[k].gis.Labs;
                                            if (allDateItems[w.Index][j].gisProd[k].K.Count != 1)
                                            {
                                                str = string.Empty;
                                                for (int h = 0; h < allDateItems[w.Index][j].gisProd[k].K.Count; h++)
                                                {
                                                    if (str.Length > 0) str += "; ";
                                                    str += allDateItems[w.Index][j].gisProd[k].K[h].ToString();
                                                }
                                                row[10] = str;
                                            }
                                            else
                                            {
                                                row[10] = allDateItems[w.Index][j].gisProd[k].K[0];
                                            }
                                            str = string.Empty;
                                            for (int h = 0; h < allDateItems[w.Index][j].gisProd[k].plasts.Count; h++)
                                            {
                                                if (str.Length > 0) str += "; ";
                                                str += stratumDict.GetShortNameByCode(allDateItems[w.Index][j].gisProd[k].plasts[h]);
                                            }
                                            row[11] = str;
                                            row[12] = allDateItems[w.Index][j].gisProd[k].accumOil;
                                            outList.Add(row);
                                            sumOil += allDateItems[w.Index][j].gisProd[k].accumOil;
                                            if (workWellList.IndexOf(w) == -1) workWellList.Add(w);
                                        }
                                    }
                                }
                                if (coveredGisItems.Count > 0)
                                {
                                    bool oilZone = true;
                                    sumH = 0;
                                    List<Well> aroundWorkWells = new List<Well>();
                                    for (j = 0; j < coveredGisItems.Count; j++)
                                    {
                                        if (stratumDict.EqualsStratum(dep.StratumCode, coveredGisItems[j].PlastId) || stratumDict.EqualsStratum(dep.StratumCode, coveredGisItems[j].PlastZoneId))
                                        {
                                            if (coveredGisItems[j].Sat0 == 1 && coveredGisItems[j].Collector)
                                            {
                                                sumH += coveredGisItems[j].Labs;
                                                ind = -1;
                                                for (int k = 0; k < w.gis.Count; k++)
                                                {
                                                    if (coveredGisItems[j].H == w.gis.Items[k].H && coveredGisItems[j].L == w.gis.Items[k].L &&
                                                        coveredGisItems[j].Habs == w.gis.Items[k].Habs && coveredGisItems[j].Labs == w.gis.Items[k].Labs)
                                                    {
                                                        ind = k;
                                                        break;
                                                    }
                                                }
                                                if (oilZone && ind > -1 && ind < w.gis.Count - 1 && w.gis.Items[ind].H + w.gis.Items[ind].L == w.gis.Items[ind + 1].H && w.gis.Items[ind + 1].Collector &&
                                                    (w.gis.Items[ind + 1].Sat0 == 2 || w.gis.Items[ind + 1].Sat0 == 3 || w.gis.Items[ind + 1].Sat0 == 7 || w.gis.Items[ind + 1].Sat0 == 12))
                                                {
                                                    oilZone = false;
                                                }
                                            }
                                        }
                                    }
                                    int radius = 500;
                                    if (sumH > 0)
                                    {
                                        merComp = new MerComp(w.mer);

                                        while (radius <= 1000)
                                        {
                                            for (j = 0; j < dep.WellList.Length; j++)
                                            {
                                                if (i != j && Math.Sqrt(Math.Pow(w.X - dep.WellList[j].X, 2) + Math.Pow(w.Y - dep.WellList[j].Y, 2)) < radius)
                                                {
                                                    find = false;
                                                    for (int k = 0; k < allDateItems[dep.WellList[j].Index].Count; k++)
                                                    {
                                                        for (int x = 0; x < allDateItems[dep.WellList[j].Index][k].gisProd.Count; x++)
                                                        {
                                                            if (stratumDict.EqualsStratum(allDateItems[dep.WellList[j].Index][k].gisProd[x].gis.PlastId, dep.StratumCode) || stratumDict.EqualsStratum(allDateItems[dep.WellList[j].Index][k].gisProd[x].gis.PlastZoneId, dep.StratumCode))
                                                            {
                                                                aroundWorkWells.Add(dep.WellList[j]);
                                                                find = true;
                                                                break;
                                                            }
                                                        }
                                                        if (find) break;
                                                    }
                                                }
                                            }
                                            if (aroundWorkWells.Count > 0) break;
                                            radius+= 500;
                                        }
                                        if (aroundWorkWells.Count == 0) radius = 1000;

                                        double[] aroundParams = new double[8];
                                        double[] tempParams = new double[8];
                                        for (j = 0; j < aroundWorkWells.Count;j++)
                                        {
                                            w2 = aroundWorkWells[j];
                                            for (int k = 0; k < allDateItems[w2.Index].Count; k++)
                                            {
                                                for (int x = 0; x < tempParams.Length; x++) tempParams[x] = 0;

                                                for (int x = 0; x < allDateItems[w2.Index][k].gisProd.Count;x++)
                                                {
                                                    if (stratumDict.EqualsStratum(allDateItems[w2.Index][k].gisProd[x].gis.PlastId, dep.StratumCode) || stratumDict.EqualsStratum(allDateItems[w2.Index][k].gisProd[x].gis.PlastZoneId, dep.StratumCode))
                                                    {
                                                        if (allDateItems[w2.Index][k].gisProd[x].startWorkTime > 0)
                                                        {
                                                            tempParams[0] += allDateItems[w2.Index][k].gisProd[x].startQliq * 24 / allDateItems[w2.Index][k].gisProd[x].startWorkTime;
                                                            tempParams[1] += allDateItems[w2.Index][k].gisProd[x].startQoil * 24 / allDateItems[w2.Index][k].gisProd[x].startWorkTime;
                                                            tempParams[2] += allDateItems[w2.Index][k].gisProd[x].startQliq;
                                                            tempParams[3] += allDateItems[w2.Index][k].gisProd[x].startQoil;
                                                        }
                                                        if (allDateItems[w2.Index][k].gisProd[x].currentWorkTime > 0)
                                                        {
                                                            tempParams[4] += allDateItems[w2.Index][k].gisProd[x].currentQliq * 24 / allDateItems[w2.Index][k].gisProd[x].currentWorkTime;
                                                            tempParams[5] += allDateItems[w2.Index][k].gisProd[x].currentQoil * 24 / allDateItems[w2.Index][k].gisProd[x].currentWorkTime;
                                                            tempParams[6] += allDateItems[w2.Index][k].gisProd[x].currentQliq;
                                                            tempParams[7] += allDateItems[w2.Index][k].gisProd[x].currentQoil;
                                                        }
                                                    }
                                                }
                                                if (tempParams[2] > 0)
                                                {
                                                    aroundParams[0] += tempParams[0];
                                                    aroundParams[1] += tempParams[1];
                                                    if (tempParams[2] > 0)
                                                    {
                                                        aroundParams[3] = (tempParams[2] - tempParams[3]) / tempParams[2];
                                                    }
                                                    aroundParams[2]++;
                                                }
                                                if (tempParams[5] > 0)
                                                {
                                                    aroundParams[4] += tempParams[4];
                                                    aroundParams[5] += tempParams[5];
                                                    if (tempParams[6] > 0)
                                                    {
                                                        aroundParams[7] = (tempParams[6] - tempParams[7]) / tempParams[6];
                                                    }
                                                    aroundParams[6]++;
                                                }
                                            }
                                        }

                                        row = new object[20];
                                        row[0] = of.ParamsDict.NGDUName;
                                        row[1] = of.Name;
                                        row[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                                        row[3] = stratumDict.GetShortNameByCode(dep.StratumParentCode);
                                        row[4] = dep.Name;
                                        row[5] = w.Name;
                                        row[6] = oilZone ? "ЧНЗ" : "ВНЗ";
                                        row[7] = sumH;
                                        ind = merComp.GetIndexByDate(project.maxMerDate);
                                        if (ind > -1)
                                        {
                                            row[8] = stateDict.GetShortNameByCode(merComp[ind].StateId);
                                            str = string.Empty;
                                            for (int k = 0; k < merComp[ind].PlastItems.Count; k++)
                                            {
                                                if (str.Length > 0) str += ", ";
                                                str += stratumDict.GetShortNameByCode(merComp[ind].PlastItems[k].PlastCode);
                                            }
                                            row[9] = str;
                                            MerWorkTimeItem wtProd = merComp[ind].TimeItems.GetAllProductionTime();
                                            if(wtProd.WorkTime + wtProd.CollTime > 0)
                                            {
                                                row[10] = merComp[ind].SumLiquid * 24 / (wtProd.WorkTime + wtProd.CollTime);
                                                row[11] = merComp[ind].SumOil * 24 / (wtProd.WorkTime + wtProd.CollTime);
                                            }
                                            else
                                            {
                                                row[10] = 0;
                                                row[11] = 0;
                                            }
                                        }

                                        row[12] = radius;
                                        str = string.Empty;
                                        for (int k = 0; k < aroundWorkWells.Count; k++)
                                        {
                                            if (str.Length > 0) str += ", ";
                                            str += aroundWorkWells[k].Name;
                                        }
                                        row[13] = str;
                                        row[14] = (aroundParams[2] > 0) ? aroundParams[0] / aroundParams[2] : 0;
                                        row[15] = (aroundParams[2] > 0) ? aroundParams[1] / aroundParams[2] : 0;
                                        row[16] = (aroundParams[3] > 0) ? aroundParams[3] * 100 : 0;

                                        row[17] = (aroundParams[6] > 0) ? aroundParams[4] / aroundParams[6] : 0;
                                        row[18] = (aroundParams[6] > 0) ? aroundParams[5] / aroundParams[6] : 0;
                                        row[19] = (aroundParams[7] > 0) ? aroundParams[7] * 100 : 0;

                                        outCoveredIntervalList.Add(row);

                                        merComp.Clear();
                                    }
                                }
                            }
                        }

                        row = new object[20];
                        row[0] = of.ParamsDict.NGDUName;
                        row[1] = of.Name;
                        row[2] = stratumDict.GetShortNameByCode(dep.StratumCode);
                        row[3] = stratumDict.GetShortNameByCode(dep.StratumParentCode);
                        row[4] = dep.Name;
                        row[5] = dep.Params.OilVolume;
                        row[6] = dep.Params.InitGeoOilReserv;
                        row[7] = initSquare[dInd] * 1000;

                        sumSquare = dep.Params.Square;
                        if (dep.contour.cntrsOut != null)
                        {
                            for(j = 0;j < dep.contour.cntrsOut.Count;j++)
                            {
                                sumSquare -= dep.contour.cntrsOut[j]._points.GetSquare();
                            }
                        }
                        row[8] = sumSquare;

                        row[9] = dep.pvt.KIN;
                        row[10] = dep.pvt.KIN * dep.Params.InitGeoOilReserv;
                        if (dep.WellList == null || dep.WellList.Length == 0)
                        {
                            row[11] = 0;
                        }
                        else
                        {
                            row[11] = dep.WellList.Length;
                            str = string.Empty;
                            for (j = 0; j < dep.WellList.Length; j++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += dep.WellList[j].Name;
                            }
                            row[12] = str;
                        }
                        if (workWellList.Count == 0)
                        {
                            row[13] = 0;
                        }
                        else
                        {
                            row[13] = workWellList.Count;
                            str = string.Empty;
                            for (j = 0; j < workWellList.Count; j++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += workWellList[j].Name;
                            }
                            row[14] = str;
                        }
                        row[15] = sumOil;
                        row[16] = (dep.pvt.KIN * dep.Params.InitGeoOilReserv > 0) ? sumOil * 100 / (dep.pvt.KIN * dep.Params.InitGeoOilReserv) : 0;
                        row[17] = dep.Params.AccumOil;
                        if (dep.Params.AccumOil > 0) row[18] = (dep.pvt.KIN * dep.Params.InitGeoOilReserv - sumOil) / dep.Params.AccumOil;
                        if (depLiq > 0) row[19] = (depLiq - dep.Params.AccumOil) * 100.0 / depLiq;
                        outDepositList.Add(row);
                        userState.Element = of.Name;
                        worker.ReportProgress((int)((dInd + 1) * 100 / of.Wells.Count), userState);
                    }

                    // параметры залежей по об.разработки
                    int maxDeposit = -1;
                    double[] depParams = new double[6];
                    List<Well> allWellList = new List<Well>();
                    List<int> equalsStratums = new List<int>();
                    DateTime lastDate1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-12);
                    DateTime lastDate2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    for (int dInd = 0; dInd < prodDeposits.Count; dInd++)
                    {
                        workWellList.Clear();
                        allWellList.Clear();
                        for (i = 0; i < depParams.Length; i++) depParams[i] = 0;
                        maxDeposit = -1;
                        equalsStratums.Clear();
                        for (i = 0; i < prodDeposits[dInd].Count; i++)
                        {
                            dep = prodDeposits[dInd][i];
                            
                            if (maxDeposit == -1 || prodDeposits[dInd][maxDeposit].Params.Square < dep.Params.Square)
                            {
                                maxDeposit = -1;
                            }

                            depParams[0] += dep.Params.InitGeoOilReserv;
                            depParams[1] += dep.Params.InitGeoOilReserv * dep.pvt.KIN;
                            
                            if (dep.AllWellList != null)
                            {
                                for (j = 0; j < dep.AllWellList.Length; j++)
                                {
                                    w = dep.AllWellList[j];
                                    if (allWellList.IndexOf(w) == -1) allWellList.Add(w);

                                    if (w.MerLoaded && workWellList.IndexOf(w) == -1)
                                    {
                                        find = false;
                                        for (int k = 0; k < w.mer.Count; k++)
                                        {
                                            if (w.mer.Items[k].CharWorkId == 20) continue;
                                            if (equalsStratums.IndexOf(w.mer.Items[k].PlastId) == -1)
                                            {
                                                if (stratumDict.EqualsStratum(dep.StratumParentCode, w.mer.Items[k].PlastId) ||
                                                    (of.OilFieldCode == 69 && dep.StratumCode == 48 && w.mer.Items[k].PlastId == 38) ||
                                                    (of.OilFieldCode == 109 && dep.StratumCode == 48 && w.mer.Items[k].PlastId == 39)) // crutch
                                                {
                                                    equalsStratums.Add(w.mer.Items[k].PlastId);
                                                }
                                            }
                                            if (equalsStratums.IndexOf(w.mer.Items[k].PlastId) > -1)
                                            {
                                                if (workWellList.IndexOf(w) == -1) workWellList.Add(w);

                                                depParams[2] += w.mer.Items[k].Oil;
                                                depParams[3] += w.mer.Items[k].Oil + w.mer.Items[k].Wat;
                                                if (lastDate1 <= w.mer.Items[k].Date && w.mer.Items[k].Date <= lastDate2)
                                                {
                                                    depParams[4] += w.mer.Items[k].Oil;
                                                    depParams[5] += w.mer.Items[k].Oil + w.mer.Items[k].Wat;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // составные залежей
                        if (maxDeposit == -1) maxDeposit = 0;
                        str = string.Format("{0}({1})", prodDeposits[dInd][maxDeposit].Name, stratumDict.GetShortNameByCode(prodDeposits[dInd][maxDeposit].StratumCode));
                        for (i = 0; i < prodDeposits[dInd].Count; i++)
                        {
                            if (i != maxDeposit)
                            {
                                str += ",";
                                str += string.Format("{0}({1})", prodDeposits[dInd][i].Name, stratumDict.GetShortNameByCode(prodDeposits[dInd][i].StratumCode));
                            }
                        }

                        row = new object[18];
                        row[0] = of.ParamsDict.NGDUName;
                        row[1] = of.Name;
                        row[2] = stratumDict.GetShortNameByCode(prodDeposits[dInd][maxDeposit].StratumCode);
                        row[3] = stratumDict.GetShortNameByCode(prodDeposits[dInd][maxDeposit].StratumParentCode);
                        row[4] = prodDeposits[dInd][maxDeposit].Name;
                        row[5] = str;
                        row[6] = depParams[0];
                        if (depParams[0] > 0) row[7] = depParams[1] / depParams[0];
                        row[8] = depParams[1];

                        if (allWellList == null || allWellList.Count == 0)
                        {
                            row[9] = 0;
                        }
                        else
                        {
                            row[9] = allWellList.Count;
                            str = string.Empty;
                            for (j = 0; j < allWellList.Count; j++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += allWellList[j].Name;
                            }
                            row[10] = str;
                        }
                        if (workWellList.Count == 0)
                        {
                            row[11] = 0;
                        }
                        else
                        {
                            row[11] = workWellList.Count;
                            str = string.Empty;
                            for (j = 0; j < workWellList.Count; j++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += workWellList[j].Name;
                            }
                            row[12] = str;
                        }
                        row[13] = depParams[2];
                        row[14] = (depParams[2] > 0 && depParams[1] > 0) ? depParams[2] * 100 / depParams[1] : 0;
                        row[15] = depParams[4];
                        row[16] = (depParams[1] > 0 && depParams[4] > 0) ? (depParams[1] - depParams[2]) / depParams[4] : 0;
                        row[17] = (depParams[5] > 0) ? (depParams[5] - depParams[4]) * 100f / depParams[5] : 0;

                        //row[12] = sumOil;
                        //row[13] = (dep.pvt.KIN * dep.Params.InitGeoOilReserv > 0) ? sumOil * 100 / (dep.pvt.KIN * dep.Params.InitGeoOilReserv) : 0;
                        //row[14] = dep.Params.AccumOil;
                        //if (dep.Params.AccumOil > 0) row[15] = (dep.pvt.KIN * dep.Params.InitGeoOilReserv - sumOil) / dep.Params.AccumOil;
                        //if (depLiq > 0) row[16] = (depLiq - dep.Params.AccumOil) * 100.0 / depLiq;
                        outProdDepositList.Add(row);
                    }

                    if (of.Name == "АРЛАНСКОЕ") // crutch
                    {
                        ind = project.GetOFIndex("ЮСУПОВСКАЯ ПЛОЩАДЬ");
                        if (ind > -1)
                        {
                            project.OilFields[ind].ClearGisData(false);
                            project.OilFields[ind].ClearPerfData(false);
                            project.OilFields[ind].ClearMerData(false);
                        }
                    }
                    of.ClearGisData(false);
                    of.ClearPerfData(false);
                    of.ClearMerData(true);
                    wellList.Clear();
                }
            }
            if (excel != null)
            {
                while (excel.GetWorkSheetsCount() < 5)
                {
                    excel.AddNewWorkSheet();
                }
                excel.SetActiveSheet(0);
                excel.SetActiveSheetName("По объектам разработки");
                if (outProdDepositList.Count > 0)
                {
                    row = new object[18];
                    row[0] = "НГДУ";
                    row[1] = "Месторождение";
                    row[2] = "Объект залежи";
                    row[3] = "Объект разработки";
                    row[4] = "Залежь";
                    row[5] = "Составные залежи";
                    row[6] = "Геолог.запасы, т";
                    row[7] = "КИН(пересчитанный)";
                    row[8] = "Извлек.запасы";
                    row[9] = "Количество скв.";
                    row[10] = "Номера скважин";
                    row[11] = "Количество раб.скв.";
                    row[12] = "Номера раб.скважин";
                    row[13] = "Нак.нефть, т";
                    row[14] = "Отбор от низ, %";
                    row[15] = "Отбор за 12 мес.";
                    row[16] = "Кратность, лет";
                    row[17] = "Обводненность, за 12 мес.";
                    outProdDepositList.Insert(0, row);
                    excel.SetRowsArray(0, 0, outProdDepositList);
                }
                
                excel.SetActiveSheet(1);
                excel.SetActiveSheetName("По залежам");
                if (outDepositList.Count > 0)
                {
                    row = new object[20];
                    row[0] = "НГДУ";
                    row[1] = "Месторождение";
                    row[2] = "Объект залежи";
                    row[3] = "Объект разработки";
                    row[4] = "Залежь";
                    row[5] = "Нефтенасыщ.объем";
                    row[6] = "Геолог.запасы, т";
                    row[7] = "Площадь залежи, м2";
                    row[8] = "Площадь(расчетн.), м2";
                    row[9] = "КИН";
                    row[10] = "Извлек.запасы";
                    row[11] = "Количество скв.";
                    row[12] = "Номера скважин";
                    row[13] = "Количество раб.скв.";
                    row[14] = "Номера раб.скважин";
                    row[15] = "Нак.нефть, т";
                    row[16] = "Отбор от низ, %";
                    row[17] = "Отбор за 12 мес.";
                    row[18] = "Кратность, лет";
                    row[19] = "Обводненность, за 12 мес.";
                    outDepositList.Insert(0, row);
                    excel.SetRowsArray(0, 0, outDepositList);
                }
                
                excel.SetActiveSheet(2);
                excel.SetActiveSheetName("По скважинам");
                if (outList.Count > 0)
                {
                    row = new object[13];
                    row[0] = "НГДУ";
                    row[1] = "Месторождение";
                    row[2] = "Объект залежи";
                    row[3] = "Залежь";
                    row[4] = "Скважина";
                    row[5] = "Дата перфорации начало";
                    row[6] = "Дата перфорации конец";
                    row[7] = "Объект ГИС";
                    row[8] = "Habs";
                    row[9] = "Labs";
                    row[10] = "Коэффициент добычи";
                    row[11] = "Пласты МЭР";
                    row[12] = "Суммарная добыча";
                    outList.Insert(0, row);
                    excel.SetRowsArray(0, 0, outList);
                }
                
                excel.SetActiveSheet(3);
                excel.SetActiveSheetName("Невскрытые интервалы");
                if (errors.Count > 0)
                {
                    row = new object[20];
                    row[0] = "НГДУ";
                    row[1] = "Месторождение";
                    row[2] = "Объект залежи";
                    row[3] = "Объект разработки";
                    row[4] = "Залежь";
                    row[5] = "Скважина";
                    row[6] = "ЧНЗ/ВНЗ";
                    row[7] = "Суммарная невскрытая толщина, м";
                    row[8] = "Состояние скважины";
                    row[9] = "Пласты работы";
                    row[10] = "Дебит жидкости, т/сут";
                    row[11] = "Дебит нефти, т/сут";
                    row[12] = "Радиус поиска окружения, м";
                    row[13] = "Скважины работающие в окружении";
                    row[14] = "Qж.ср.запуск., т/сут";
                    row[15] = "Qн.ср.запуск., т/сут";
                    row[16] = "Обв.ср.запуск., %";
                    row[17] = "Qж.ср.тек., т/сут";
                    row[18] = "Qн.ср.тек., т/сут";
                    row[19] = "Обв.ср.тек., %";
                    outCoveredIntervalList.Insert(0, row);
                    excel.SetRowsArray(0, 0, outCoveredIntervalList);
                }
                excel.SetActiveSheet(4);
                excel.SetActiveSheetName("Ошибки");
                if (errors.Count > 0)
                {
                    err = new object[8];
                    err[0] = "НГДУ";
                    err[1] = "Месторождение";
                    err[2] = "Объект залежи";
                    err[3] = "Залежь";
                    err[4] = "Скважина";
                    err[5] = "Тип данных ошибки";
                    err[6] = "Данные";
                    err[7] = "Комментарий";
                    errors.Insert(0, err);
                    excel.SetRowsArray(0, 0, errors);
                }
                excel.EnableEvents = true;
                excel.Visible = true;
            }
        }
    }
#endif
}
