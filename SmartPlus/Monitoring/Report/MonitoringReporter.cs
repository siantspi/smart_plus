﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus;
using RDF.Objects;
using System.IO;
using System.ComponentModel;
using SmartPlus.DictionaryObjects;
namespace SmartPlus.Report
{
#if BASHNEFT
    class MonitoringReporter
    {
        public static double[][] GetPressureByWells(List<Well> wells, StratumDictionary dict, List<int> PlastCodes, DateTime StartDate, int Month, int UpDelayTime)
        {
            return GetPressureByWells(wells, dict, PlastCodes, StartDate, Month, UpDelayTime, false);
        }
        public static double[][] GetPressureByWells(List<Well> wells, StratumDictionary dict, List<int> PlastCodes, DateTime StartDate, int Month, int UpDelayTime, bool DontUsePiezometric)
        {
            if (Month < 1) return new double[0][];

            double[][] Pressure = new double[Month][];
            DateTime dt = new DateTime(StartDate.Year, StartDate.Month, 1), minDate = dt;
            for (int i = 0; i < Pressure.Length; i++)
            {
                Pressure[i] = new double[4];
                Pressure[i][0] = dt.ToOADate();
                dt = dt.AddMonths(1);
            }

            WellResearchItem rItem, lastItem, last0Item;
            int indMonth;
            int j, j2, k;
            Well w;
            bool find, isDragged;
            DateTime lastDate, currDate;
            for (int wInd = 0; wInd < wells.Count; wInd++)
            {
                w = wells[wInd];
                if (w.ResearchLoaded)
                {
                    find = false; // find Inj wells
                    if (w.MerLoaded)
                    {
                        indMonth = w.mer.GetIndex(StartDate);
                        if (indMonth != -1)
                        {
                            dt = w.mer.Items[indMonth].Date.AddMonths(Month);
                            for (int i = indMonth; i < w.mer.Count; i++)
                            {
                                if (w.mer.Items[i].Date > dt) break;
                                if (w.mer.Items[i].CharWorkId == 20 || (DontUsePiezometric && (w.mer.Items[i].StateId == 5 || w.mer.Items[i].StateId == 6)))
                                {
                                    find = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (find) continue; // without Inj Wells and Piezometric
                    j2 = 0; 
                    lastItem = new WellResearchItem();
                    last0Item = new WellResearchItem();
                    lastDate = DateTime.MinValue;
                    dt = DateTime.FromOADate(Pressure[j2][0]);
                    for (j = 0; j < w.research.Count; j++)
                    {
                         rItem = w.research[j];
                         if (rItem.Date < minDate) continue;

                         if ((rItem.PressPerforation > 0) && ((rItem.ResearchCode == 2) || (rItem.ResearchCode == 4) || (rItem.ResearchCode == 6)))
                         {
                             find = false;
                             for (k = 0; k < PlastCodes.Count; k++)
                             {
                                 if (dict.EqualsStratum(rItem.PlastCode, PlastCodes[k]))
                                 {
                                     find = true;
                                     break;
                                 }
                             }
                             if (!find) continue;

                             currDate = new DateTime(rItem.Date.Year, rItem.Date.Month, 1);
                             if (rItem.ResearchCode != 2 || rItem.TimeDelay >= UpDelayTime)
                             {
                                 if (lastItem.PressPerforation > 0)
                                 {
                                     isDragged = false;
                                     while (j2 > -1 && j2 < Pressure.Length)
                                     {
                                         dt = DateTime.FromOADate(Pressure[j2][0]);
                                         if ((dt == currDate && isDragged) || (dt > currDate)) 
                                         { 
                                             j2--; 
                                             break; 
                                         }
                                         if (dt >= lastDate)
                                         {
                                             Pressure[j2][1] += lastItem.PressPerforation;
                                             Pressure[j2][2]++;
                                             if (dt == lastDate) Pressure[j2][3]++;
                                             isDragged = true;
                                         }
                                         j2++;
                                     }
                                 }
                                 last0Item = new WellResearchItem();
                                 lastDate = new DateTime(rItem.Date.Year, rItem.Date.Month, 1);
                                 lastItem = rItem;
                             }
                             else if (rItem.ResearchCode == 2 && rItem.TimeDelay < UpDelayTime)
                             {
                                 if (last0Item.PressPerforation == 0 || (rItem.Date - last0Item.Date).TotalDays < 4)
                                 {
                                     last0Item = rItem;
                                 }
                                 else
                                 {
                                     currDate = new DateTime(last0Item.Date.Year, last0Item.Date.Month, 1);
                                     if (lastItem.PressPerforation > 0)
                                     {
                                         isDragged = false;
                                         while (j2 > -1 && j2 < Pressure.Length)
                                         {
                                             dt = DateTime.FromOADate(Pressure[j2][0]);
                                             if ((dt == currDate && isDragged) || (dt > currDate))
                                             {
                                                 j2--;
                                                 break;
                                             }
                                             if (dt >= lastDate)
                                             {
                                                 Pressure[j2][1] += lastItem.PressPerforation;
                                                 Pressure[j2][2]++;
                                                 if (dt == lastDate) Pressure[j2][3]++;
                                                 isDragged = true;
                                             }
                                             j2++;
                                         }
                                     }
                                     lastDate = currDate;
                                     lastItem = last0Item;
                                     last0Item = new WellResearchItem();
                                 }
                             }
                         }
                    }
                }
            }
            return Pressure;
        }
        public static double[][] GetPressureByWells(List<Well> wells, StratumDictionary dict, int PlastCode, DateTime StartDate, int Month, int UpDelayTime)
        {
            if (Month < 1) return new double[0][];

            double[][] Pressure = new double[Month][];
            DateTime dt = new DateTime(StartDate.Year, StartDate.Month, 1), minDate = StartDate;
            for (int i = 0; i < Pressure.Length; i++)
            {
                Pressure[i] = new double[4];
                Pressure[i][0] = dt.ToOADate();
                dt = dt.AddMonths(1);
            }
            
            WellResearchItem rItem, lastItem;
            int indMonth, indMonth2;
            int j, j2, k;
            Well w;
            bool find;
            for (int wInd = 0; wInd < wells.Count; wInd++)
            {
                w = wells[wInd];
                if (w.ResearchLoaded)
                {
                    find = false; // find Inj Wells
                    if (w.MerLoaded)
                    {
                        indMonth = w.mer.GetIndex(StartDate);
                        if (indMonth != -1)
                        {
                            dt = w.mer.Items[indMonth].Date.AddMonths(Month);
                            for (int i = indMonth; i < w.mer.Count; i++)
                            {
                                if (w.mer.Items[i].Date > dt) break;
                                if (w.mer.Items[i].CharWorkId == 20)
                                {
                                    find = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (find) continue; // without Inj Wells

                    for (j = 0; j < w.research.Count; j++)
                    {
                        rItem = w.research[j];
                        if ((rItem.PressPerforation > 0) &&
                            ((rItem.ResearchCode == 2 && rItem.TimeDelay > UpDelayTime) || (rItem.ResearchCode == 4) || (rItem.ResearchCode == 6)) &&
                            (dict.EqualsStratum(rItem.PlastCode, PlastCode)))
                        {
                            indMonth = (rItem.Date.Year - minDate.Year) * 12 + rItem.Date.Month - minDate.Month;
                            find = false;
                            indMonth2 = -1;
                            for (j2 = j + 1; j2 < w.research.Count; j2++)
                            {
                                lastItem = w.research[j2];
                                if ((lastItem.PressPerforation > 0) &&
                                    ((lastItem.ResearchCode == 2) || (lastItem.ResearchCode == 4) || (lastItem.ResearchCode == 6)) &&
                                    (dict.EqualsStratum(lastItem.PlastCode, PlastCode)))
                                {
                                    if (lastItem.ResearchCode != 2 || lastItem.TimeDelay > UpDelayTime)
                                    {
                                        indMonth2 = (lastItem.Date.Year - minDate.Year) * 12 + lastItem.Date.Month - minDate.Month;
                                        find = true;
                                        break;
                                    }
                                }
                            }
                            if (!find) indMonth2 = Pressure.Length;

                            if ((indMonth > -1) && (indMonth < Pressure.Length))
                            {
                                Pressure[indMonth][1] += rItem.PressPerforation;
                                Pressure[indMonth][2]++;
                                Pressure[indMonth][3]++;
                            }
                            for (k = indMonth + 1; (k < indMonth2) && (k < Pressure.Length); k++)
                            {
                                if (k < 0) continue;
                                Pressure[k][1] += rItem.PressPerforation;
                                Pressure[k][2]++;
                            }
                            j = j2 - 1;
                        }
                    }
                }
            }
            return Pressure;
        }
        public static double[][] GetZabPressureByWells(List<Well> wells, StratumDictionary dict, int PlastCode, DateTime StartDate, int Month)
        {
            if (Month < 1) return new double[0][];

            double[][] Pressure = new double[Month][];
            DateTime dt = StartDate, minDate = StartDate;
            for (int i = 0; i < Pressure.Length; i++)
            {
                Pressure[i] = new double[4];
                Pressure[i][0] = dt.ToOADate();
                dt = dt.AddMonths(1);
            }
            WellResearchItem rItem, rItem2;
            int indMonth, indMonth2;
            int j, j2, k;
            Well w;
            for (int wInd = 0; wInd < wells.Count; wInd++)
            {
                w = wells[wInd];
                if (w.ResearchLoaded)
                {
                    for (j = 0; j < w.research.Count; j++)
                    {
                        rItem = w.research[j];
                        if ((rItem.PressPerforation > 0) && ((rItem.ResearchCode == 1) || (rItem.ResearchCode == 3)) && 
                            (dict.EqualsStratum(rItem.PlastCode, PlastCode)))
                        {
                            indMonth = (rItem.Date.Year - minDate.Year) * 12 + rItem.Date.Month - minDate.Month;
                            indMonth2 = -1;
                            for (j2 = j + 1; j2 < w.research.Count; j2++)
                            {
                                rItem2 = w.research[j2];
                                if ((rItem2.PressPerforation > 0) && ((rItem2.ResearchCode == 1) || (rItem2.ResearchCode == 3)) && 
                                    (dict.EqualsStratum(rItem2.PlastCode, PlastCode)))
                                {
                                    indMonth2 = (rItem2.Date.Year - minDate.Year) * 12 + rItem2.Date.Month - minDate.Month;
                                    break;
                                }
                            }
                            if (indMonth2 == -1) indMonth2 = Pressure.Length;

                            if ((indMonth > -1) && (indMonth < Pressure.Length))
                            {
                                Pressure[indMonth][1] += rItem.PressPerforation;
                                Pressure[indMonth][2]++;
                                Pressure[indMonth][3]++;
                                for (k = indMonth + 1; (k < indMonth2) && (k < Pressure.Length); k++)
                                {
                                    Pressure[k][1] += rItem.PressPerforation;
                                    Pressure[k][2]++;
                                }
                            }
                            j = j2 - 1;
                        }
                    }
                }
            }
            return Pressure;
        }
        public static void GetPressureByWells(List<Well> wells, StratumDictionary dict, int PlastCode, double[][] Pressure)
        {
            if ((Pressure.Length > 0) && (Pressure[0].Length > 2))
            {
                DateTime minDate = DateTime.FromOADate(Pressure[0][0]);
                WellResearchItem rItem, rItem2;
                int indMonth, indMonth2;
                int j, j2, k;
                Well w;
                for (int wInd = 0; wInd < wells.Count; wInd++)
                {
                    w = wells[wInd];
                    if (w.ResearchLoaded)
                    {
                        for (j = 0; j < w.research.Count; j++)
                        {
                            rItem = w.research[j];
                            if ((rItem.PressPerforation > 0) &&
                                ((rItem.ResearchCode == 2 && rItem.TimeDelay > 0) || (rItem.ResearchCode == 4) || (rItem.ResearchCode == 6)) &&
                                (dict.EqualsStratum(rItem.PlastCode, PlastCode)))
                            {
                                indMonth = (rItem.Date.Year - minDate.Year) * 12 + rItem.Date.Month - minDate.Month;
                                indMonth2 = -1;
                                for (j2 = j + 1; j2 < w.research.Count; j2++)
                                {
                                    rItem2 = w.research[j2];
                                    if ((rItem2.PressPerforation > 0) &&
                                        ((rItem2.ResearchCode == 2 && rItem2.TimeDelay > 0) || (rItem2.ResearchCode == 4) || (rItem2.ResearchCode == 6)) &&
                                        (dict.EqualsStratum(rItem2.PlastCode, PlastCode)))
                                    {
                                        indMonth2 = (rItem2.Date.Year - minDate.Year) * 12 + rItem2.Date.Month - minDate.Month;
                                        break;
                                    }
                                }
                                if (indMonth2 == -1) indMonth2 = Pressure.Length;

                                if ((indMonth > -1) && (indMonth < Pressure.Length))
                                {
                                    Pressure[indMonth][1] += rItem.PressPerforation;
                                    Pressure[indMonth][2]++;
                                    Pressure[indMonth][3]++;
                                    for (k = indMonth + 1; (k < indMonth2) && (k < Pressure.Length); k++)
                                    {
                                        Pressure[k][1] += rItem.PressPerforation;
                                        Pressure[k][2]++;
                                    }
                                }
                                j = j2 - 1;
                            }
                        }
                    }
                }
            }
        }
        public static List<Well> GetWellsWithGTM(List<Well> Wells, List<int> FilterGtmCodes, List<int> PlastCodes, List<int> Years, out int CountGtm)
        {
            CountGtm = 0;
            List<Well> gtmWells = new List<Well>();
            bool IsInjection, IsFindStratum;
            DateTime dt;
            MerComp comp;
            for (int i = 0; i < Wells.Count; i++)
            {
                if (!Wells[i].MerLoaded || !Wells[i].GTMLoaded) continue;

                IsInjection = false;
                comp = new MerComp(Wells[i].mer);
                for (int j = 0; j < comp.Count; j++)
                {
                    if (comp[j].CharWorkIds.IndexOf(20) > -1)
                    {
                        IsInjection = true;
                        break;
                    }
                }
                if (IsInjection) continue;

                for (int j = 0; j < Wells[i].gtm.Count; j++)
                {
                    if (Years.IndexOf(Wells[i].gtm[j].Date.Year) == -1) continue;
                    if (FilterGtmCodes.IndexOf(Wells[i].gtm[j].GtmCode) != -1)
                    {
                        IsFindStratum = PlastCodes.IndexOf(-1) > -1;
                        dt = new DateTime(Wells[i].gtm[j].Date.Year, Wells[i].gtm[j].Date.Month, 1);
                        if (dt.AddMonths(1) <= comp[comp.Count - 1].Date)
                        {
                            dt = dt.AddMonths(1);
                        }
                        if (!IsFindStratum)
                        {
                            for (int im = 0; im < comp.Count; im++)
                            {
                                if (comp[im].Date < dt) continue;
                                if (comp[im].Date > dt) break;

                                for (int ip = 0; ip < comp[im].PlastItems.Count; ip++)
                                {
                                    if (PlastCodes.IndexOf(comp[im].PlastItems[ip].PlastCode) > -1)
                                    {
                                        IsFindStratum = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (IsFindStratum)
                        {
                            CountGtm++;
                            if (gtmWells.IndexOf(Wells[i]) == -1) gtmWells.Add(Wells[i]);
                        }
                    }
                }
                comp.Clear();
            }
            return gtmWells;
        }
        public static double[] GetGtmWellsParams(List<Well> Wells, List<int> FilterGtmCodes, List<int> PlastCodes, List<int> Years)
        {
            double[] result = new double[4];
            /// 0 - Qn среднее За 3 месяца до
            /// 1 - Qn среднее За 3 месяца после
            /// 2 - % падения дебита
            /// 3 - Месяцев протянутых

            int i, index;
            DateTime prevDate, gtmDate, nextDate, dt;
            double[][] values = new double[24][];
            for (i = 0; i < values.Length; i++)
            {
                values[i] = new double[3];
            }
            for (int iw = 0; iw < Wells.Count; iw++)
            {
                prevDate = DateTime.MinValue;
                gtmDate = DateTime.MinValue;
                nextDate = DateTime.MinValue;
                for (int ig = 0; ig < Wells[iw].gtm.Count; ig++)
                {
                    dt = Wells[iw].gtm[ig].Date;
                    if (Years.IndexOf(dt.Year) == -1) continue;
                    if (FilterGtmCodes.IndexOf(Wells[iw].gtm[ig].GtmCode) == -1) continue;

                    gtmDate = dt;
                    nextDate = gtmDate.AddMonths(11);
                    if (ig + 1 < Wells[iw].gtm.Count)
                    {
                        dt = Wells[iw].gtm[ig + 1].Date;
                        index = (dt.Year - gtmDate.Year) * 12 + dt.Month - gtmDate.Month + 1;
                        if (index == 0)
                        {
                            nextDate = DateTime.MinValue;
                            break;
                        }
                        else if (index < 12)
                        {
                            nextDate = new DateTime(dt.Year, dt.Month, 1);
                        }
                    }
                    prevDate = gtmDate.AddMonths(-12);
                    if (ig - 1 > -1)
                    {
                        dt = Wells[iw].gtm[ig - 1].Date;
                        index = (gtmDate.Year - dt.Year) * 12 + gtmDate.Month - dt.Month + 1;

                        if (index == 0)
                        {
                            prevDate = DateTime.MinValue;
                            break;
                        }
                        else if (index < 12)
                        {
                            prevDate = new DateTime(dt.Year, dt.Month, 1);
                        }
                    }
                    break;
                }
                MerWorkTimeItem wt;
                MerComp comp = new MerComp(Wells[iw].mer);
                for (i = 0; i < comp.Count; i++)
                {
                    if (comp[i].Date < prevDate) continue;
                    if (comp[i].Date >= nextDate) break;
                    index = 12 + (comp[i].Date.Year - gtmDate.Year) * 12 + comp[i].Date.Month - gtmDate.Month;
                    if (index < 0 || index > 23) continue;

                    for (int j = 0; j < comp[i].PlastItems.Count; j++)
                    {
                        if (PlastCodes.IndexOf(-1) != -1 || PlastCodes.IndexOf(comp[i].PlastItems[j].PlastCode) != -1)
                        {
                            values[index][0] += comp[i].PlastItems[j].Oil;
                            values[index][2]++;
                        }
                    }

                    wt = (PlastCodes.IndexOf(-1) != -1) ? comp[i].TimeItems.GetAllProductionTime() : comp[i].TimeItems.GetProductionTime(PlastCodes);
                    values[index][1] += wt.GetWorkHours();
                }
                comp.Clear();
            }
            int countPrev = 0, countNext = 0;
            for (i = 0; i < 3; i++)
            {
                if (values[11 - i][1] != 0)
                {
                    countPrev++;
                    result[0] += values[11 - i][0] * 24 / values[11 - i][1];
                }
                if (values[12 + i][1] != 0)
                {
                    countNext++;
                    result[1] += values[12 + i][0] * 24 / values[12 + i][1];
                }
            }

            if (countPrev > 0) result[0] /= countPrev;
            if (countNext > 0) result[1] /= countNext;
            int count = 0;
            for (i = 23; i > 11; i--)
            {
                if (values[i][2] > 0) break;
                count++;
            }
            double q1, q2;
            result[3] = count;
            if (count > 0)
            {
                int j = i;
                result[2] = 0;
                count = 0;
                for (i = j; (i > 12) && (count < 3); i--)
                {
                    q1 = (values[i - 1][1] == 0) ? 0 : values[i - 1][0] * 24 / values[i - 1][1];
                    q2 = (values[i][1] == 0) ? 0 : values[i][0] * 24 / values[i][1];
                    result[2] += q1 - q2;
                    count++;
                }
                result[2] /= 3;
                for (i = j + 1; i < 24; i++)
                {
                    q1 = (values[i - 1][1] != 0) ? values[i - 1][0] * 24 / values[i - 1][1] : 0;
                    values[i][0] = (q1 != 0) ? q1 - result[2] : 0;
                    values[i][1] = 24;
                }
            }
            result[2] = (values[23][1] != 0) ? values[23][0] * 24 / values[23][1] : 0;
            result[2] = (result[1] - result[0] == 0) ? 0 : (1 - (result[2] - result[0]) / (result[1] - result[0])) * 100.0;
            result[2] = Math.Round(result[2], 3);
            return result;
        }
        /// <summary>
        /// Поиск окружения работающего на пласты на определенную дату
        /// </summary>
        /// <param name="w">Скважина</param>
        /// <param name="plastList">Пласты работы</param>
        /// <param name="dt">Дата МЭР</param>
        /// <param name="Radius">Радиус поиска</param>
        /// <param name="CharWorkType">Тип скважин [0 - любая; 1 - доб; 2 - наг; 3 - не нагн]</param>
        /// <returns></returns>
        private static List<Well> GetEnviromentByDate(Project project, Well w, List<int> PlastCodes, DateTime dt, int Radius, int CharWorkType)
        {
            int i, j, k, s;
            List<Well> res = new List<Well>();
            DateTime startDt = new DateTime(dt.Year, dt.Month, dt.Day);
            var dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);

            if (w.MerLoaded)
            {
                if (PlastCodes.Count > 0)
                {
                    bool find = false;
                    List<Well> wellList;
                    int findCharWork = -1;
                    Well w2;
                    double d;
                    OilField of = project.OilFields[w.OilFieldIndex];
                    wellList = GetEnviroment(project, w, Radius, true);
                    if (wellList.Count > 0)
                    {
                        for (k = 0; k < wellList.Count; k++)
                        {
                            w2 = wellList[k];
                            find = false;
                            findCharWork = -1;
                            startDt = new DateTime(dt.Year, dt.Month, dt.Day);
                            if (w2.MerLoaded)
                            {
                                for (i = 0; i < w2.mer.Count; i++)
                                {
                                    if (w2.mer.Items[i].Date < startDt)
                                    {
                                        continue;
                                    }
                                    else if (w2.mer.Items[i].Date == startDt)
                                    {
                                        s = 1;
                                        while (s < 4)
                                        {
                                            for (j = i; (j < w2.mer.Count) && (w2.mer.Items[j].Date == startDt); j++)
                                            {
                                                findCharWork = w2.mer.Items[j].CharWorkId;
                                                if (w2.mer.Items[j].WorkTime + w2.mer.Items[j].CollTime > 0)
                                                {
                                                    if ((CharWorkType == 0) ||
                                                       ((CharWorkType == 1) && ((w2.mer.Items[j].CharWorkId == 11) ||
                                                        (w2.mer.Items[j].CharWorkId == 12) || (w2.mer.Items[j].CharWorkId == 15))) ||
                                                       ((CharWorkType == 2) && (w2.mer.Items[j].CharWorkId == 20)) ||
                                                       ((CharWorkType == 3) && (w2.mer.Items[j].CharWorkId != 20)))
                                                    {
                                                        for (int p = 0; p < PlastCodes.Count; p++)
                                                        {
                                                            if (dict.EqualsStratum(PlastCodes[p], w2.mer.Items[j].PlastId))
                                                            {
                                                                if (res == null) res = new List<Well>();
                                                                res.Add(w2);
                                                                find = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (find) break;
                                            }
                                            i = j;
                                            startDt = dt.AddMonths(1);
                                            s++;
                                            if (find) break;
                                        }
                                        break;
                                    }
                                }
                            }
                            if ((CharWorkType == 3) && (!find) && (findCharWork != 20))
                            {
                                if (res == null) res = new List<Well>();
                                res.Add(w2);
                            }
                        }
                    }
                }
            }
            return res;
        }
        private static List<Well> GetEnviroment(Project project, Well w, int Radius, bool TestMer)
        {
            double d;
            Well w2;
            OilField of = project.OilFields[w.OilFieldIndex];
            List<Well> result = new List<Well>();
            for (int i = 0; i < of.Wells.Count; i++)
            {
                w2 = of.Wells[i];
                if ((w2 != w) && (w2.CoordLoaded) && (!TestMer || w2.MerLoaded))
                {
                    d = Math.Sqrt(Math.Pow(w2.X - w.X, 2) + Math.Pow(w2.Y - w.Y, 2));
                    if (d < Radius) result.Add(w2);
                }
            }
            return result;
        }
        private static void AddArray(double[] AddedArray, double[] MainArray) { for (int i = 0; i < MainArray.Length; i++) MainArray[i] += AddedArray[i]; }
        private static void ClearArray(double[] array) { for (int i = 0; i < array.Length; i++) array[i] = 0; }
        /// <summary>
        /// Выгрузка параметров из МЭР [0-Qж; 1-Qn; 2-W; 3-WTprod; 4-Liq; 5-Oil; 6-Inj; 7-WTinj;8 - QжM3;9 - QнM3;10 - LiqM3;11 - OilM3]
        /// </summary>
        /// <param name="w">Скважина</param>
        /// <param name="dt">Дата</param>
        /// <returns></returns>
        private static double[] GetRegimByDate(Well w, DateTime dt)
        {
            // 0-Qж 1-Qn 2-W 3-WTprod 4-Liq 5-oil 6-inj 7-WTinj 8 - QжM3 9 - QнM3 10 - LiqM3 11 -OilM3
            double[] res = new double[12];
            if (w.MerLoaded)
            {
                MerComp merComp = null;
                if (w.merEx != null)
                {
                    merComp = w.merEx;
                }
                else
                {
                    merComp = new MerComp(w.mer);
                }

                for (int i = 0; i < merComp.Count; i++)
                {
                    if (merComp[i].Date < dt)
                    {
                        continue;
                    }
                    else if (merComp[i].Date == dt)
                    {
                        MerWorkTimeItem wt;
                        res[4] = merComp[i].SumLiquid;
                        res[5] = merComp[i].SumOil;
                        res[10] = merComp[i].SumLiquidV;
                        res[11] = merComp[i].SumOilV;

                        res[6] = merComp[i].SumInjection;

                        wt = merComp[i].TimeItems.GetTime(20, true);
                        res[3] = wt.WorkTime + wt.CollTime;
                        wt = merComp[i].TimeItems.GetTime(20, false);
                        res[7] = wt.WorkTime + wt.CollTime;
                        if (res[3] > 0)
                        {
                            res[0] = res[4] * 24 / res[3];
                            res[1] = res[5] * 24 / res[3];
                            res[8] = res[10] * 24 / res[3];
                            res[9] = res[11] * 24 / res[3];
                        }
                        if (res[7] > 0) res[2] = res[6] * 24 / res[7];
                        break;
                    }
                }
            }
            return res;
        }
        /// <summary>
        /// Выгрузка параметров из МЭР [0-Qж; 1-Qn; 2-W; 3-WTprod; 4-Liq; 5-Oil; 6-Inj; 7-WTinj;8 - QжM3;9 - QнM3;10 - LiqM3;11 - OilM3]
        /// </summary>
        /// <param name="w">Скважина</param>
        /// <param name="dt">Дата</param>
        /// <param name="PlastCode">Пласт</param>
        /// <returns></returns>
        private static double[] GetRegimByDate(Well w, DateTime dt, StratumDictionary dict, int PlastCode)
        {
            // 0-Qж 1-Qn 2-W 3-WTprod 4-Liq 5-oil 6-inj 7-WTinj 8 - QжM3 9 - QнM3 10 - LiqM3 11 -OilM3
            double[] res = new double[12];
            if (w.MerLoaded)
            {
                MerComp merComp = null;
                if (w.merEx != null)
                {
                    merComp = w.merEx;
                }
                else
                {
                    merComp = new MerComp(w.mer);
                }

                for (int i = 0; i < merComp.Count; i++)
                {
                    if (merComp[i].Date < dt)
                    {
                        continue;
                    }
                    else if (merComp[i].Date == dt)
                    {
                        StratumTreeNode node;
                        MerWorkTimeItem wt;
                        for (int j = 0; j < merComp[i].PlastItems.Count; j++)
                        {
                            if (dict.EqualsStratum(merComp[i].PlastItems[j].PlastCode, PlastCode))
                            {
                                res[4] += merComp[i].PlastItems[j].Liq;
                                res[5] += merComp[i].PlastItems[j].Oil;
                                res[10] += merComp[i].PlastItems[j].LiqV;
                                res[11] += merComp[i].PlastItems[j].OilV;

                                res[6] = merComp[i].PlastItems[j].Inj;

                                wt = merComp[i].TimeItems.GetTime(merComp[i].PlastItems[j].PlastCode, 20, true);
                                res[3] = wt.WorkTime + wt.CollTime;
                                wt = merComp[i].TimeItems.GetTime(merComp[i].PlastItems[j].PlastCode, 20, false);
                                res[7] = wt.WorkTime + wt.CollTime;
                                if (res[3] > 0)
                                {
                                    res[0] = res[4] * 24 / res[3];
                                    res[1] = res[5] * 24 / res[3];
                                    res[8] = res[10] * 24 / res[3];
                                    res[9] = res[11] * 24 / res[3];
                                }
                                if (res[7] > 0)
                                {
                                    res[2] = res[6] * 24 / res[7];
                                }
                            }
                        }
                        break;
                    }
                }
            }
            return res;
        }

        #region Отчет по  керну
        public static void ReportCore(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета по керну";
            userState.WorkCurrentTitle = "Создание отчета по керну";
            SmartPlus.MSOffice.Excel excel = null;

            try
            {
                List<object[]> array = new List<object[]>();
                List<object[]> ofArray = new List<object[]>();
                List<object[]> array2 = new List<object[]>();
                List<object[]> ofArray2 = new List<object[]>();
                object[] row = null;
                OilField of;
                Well w;
                int i, j, gisIndex ,index, code;

                float int1, int2;
                var dictStratum = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM); 
                StratumTreeNode node;

                List<int> stratumList = new List<int>();
                List<int> stratumList2 = new List<int>();
                List<object[]> stratumParams = new List<object[]>();
                List<int[]> stratumParams2 = new List<int[]>();

                for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                {
                    of = project.OilFields[ofInd];
#if DEBUG
                    //if (of.Name != "ЮГОМАШЕВСКОЕ") continue;
#endif
                    of.LoadCoreFromCache(worker, e);
                    of.LoadCoreTestFromCache(worker, e);
                    of.LoadGisFromCache(worker, e);

                    stratumList.Clear();
                    stratumParams.Clear();
                    stratumList2.Clear();
                    stratumParams2.Clear();

                    for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                    {
                        w = of.Wells[wInd];
                        if (!w.GisLoaded) continue;

                        if (w.CoreLoaded)
                        {
                            for (i = 0; i < w.core.Count; i++)
                            {
                                for (j = 0; j < w.gis.Count; j++)
                                {
                                    if (!(w.gis.Items[j].H + w.gis.Items[j].L < w.core.Items[i].Top || w.core.Items[i].Bottom < w.gis.Items[j].H))
                                    {
                                        int1 = Math.Max(w.core.Items[i].Top, w.gis.Items[j].H);
                                        int2 = Math.Min(w.gis.Items[j].H + w.gis.Items[j].L, w.core.Items[i].Bottom);

                                        if (int2 > int1)
                                        {
                                            node = dictStratum.GetStratumTreeNode(w.gis.Items[j].PlastId);
                                            code = -1;
                                            if (node != null)
                                            {
                                                for (int k = 0; k < of.MerStratumCodes.Count; k++)
                                                {
                                                    if (node.GetParentLevelByCode(of.MerStratumCodes[k]) > -1)
                                                    {
                                                        code = of.MerStratumCodes[k];
                                                    }
                                                }
                                            }
                                            if (code > -1)
                                            {
                                                index = stratumList.IndexOf(code);
                                                if (index == -1)
                                                {
                                                    index = stratumList.Count;
                                                    stratumList.Add(code);
                                                    stratumParams.Add(new object[2]);
                                                    stratumParams[index][0] = new List<Well>();
                                                    stratumParams[index][1] = 0.0f;
                                                }
                                                if (((List<Well>)stratumParams[index][0]).IndexOf(w) == -1)
                                                {
                                                    ((List<Well>)stratumParams[index][0]).Add(w);
                                                }
                                                stratumParams[index][1] = (float)stratumParams[index][1] + int2 - int1;
                                            }
                                            row = new object[9];
                                            row[0] = of.Name;
                                            row[1] = w.Name;
                                            row[2] = w.core.Items[i].Top;
                                            row[3] = w.core.Items[i].Bottom;
                                            row[4] = dictStratum.GetShortNameByCode(w.gis.Items[j].PlastId);
                                            if (code > -1) row[5] = dictStratum.GetShortNameByCode(code);
                                            row[6] = w.gis.Items[j].H;
                                            row[7] = w.gis.Items[j].H + w.gis.Items[j].L;
                                            row[8] = int2 - int1;

                                            array.Add(row);
                                        }
                                    }
                                }
                            }
                        }
                        if (w.CoreTestLoaded)
                        {
                            for (i = 0; i < w.coreTest.Count; i++)
                            {
                                gisIndex = -1;
                                for (j = 0; j < w.gis.Count; j++)
                                {
                                    if (!(w.gis.Items[j].H + w.gis.Items[j].L < w.coreTest.Items[i].Top || w.coreTest.Items[i].Bottom < w.gis.Items[j].H))
                                    {
                                        int1 = Math.Max(w.coreTest.Items[i].Top, w.gis.Items[j].H);
                                        int2 = Math.Min(w.gis.Items[j].H + w.gis.Items[j].L, w.coreTest.Items[i].Bottom);

                                        if (int2 > int1)
                                        {
                                            gisIndex = j;
                                            break;
                                        }
                                    }
                                }
                                if (gisIndex == -1) continue;

                                node = dictStratum.GetStratumTreeNode(w.gis.Items[gisIndex].PlastId);
                                code = -1;
                                if (node != null)
                                {
                                    for (int k = 0; k < of.MerStratumCodes.Count; k++)
                                    {
                                        if (node.GetParentLevelByCode(of.MerStratumCodes[k]) > -1)
                                        {
                                            code = of.MerStratumCodes[k];
                                        }
                                    }
                                }
                                if (code > -1)
                                {
                                    index = stratumList2.IndexOf(code);
                                    if (index == -1)
                                    {
                                        index = stratumList2.Count;
                                        stratumList2.Add(code);
                                        stratumParams2.Add(new int[10]);
                                    }
                                    stratumParams2[index][0]++;

                                    if (w.coreTest.Items[i].KporHelium > 0 || w.coreTest.Items[i].KporLiquid > 0)
                                    {
                                        stratumParams2[index][1]++;
                                    }
                                    if (w.coreTest.Items[i].KprAirPP > 0 || w.coreTest.Items[i].KprAirPR > 0 ||
                                        w.coreTest.Items[i].KprHeliumPP > 0 || w.coreTest.Items[i].KprHeliumPR > 0)
                                    {
                                        stratumParams2[index][2]++;
                                    }
                                    if (w.coreTest.Items[i].SatWatFinal > 0)
                                    {
                                        stratumParams2[index][3]++;
                                    }
                                    if (w.coreTest.Items[i].CarbonateDolomite > 0 || w.coreTest.Items[i].CarbonateInsoluble > 0 || w.coreTest.Items[i].CarbonateTiff > 0)
                                    {
                                        stratumParams2[index][4]++;
                                    }
                                }
                                row = new object[16];
                                row[0] = of.Name;
                                row[1] = w.Name;
                                row[2] = w.coreTest.Items[i].Top;
                                row[3] = w.coreTest.Items[i].Bottom;
                                row[4] = dictStratum.GetShortNameByCode(w.gis.Items[gisIndex].PlastId);
                                if (code > -1) row[5] = dictStratum.GetShortNameByCode(code);
                                row[6] = w.coreTest.Items[i].Name;
                                if (w.coreTest.Items[i].KporHelium > 0 || w.coreTest.Items[i].KporLiquid > 0)
                                {
                                    row[7] = (w.coreTest.Items[i].KporHelium > 0) ? w.coreTest.Items[i].KporHelium : w.coreTest.Items[i].KporLiquid;
                                }
                                if (w.coreTest.Items[i].KprAirPP > 0 || w.coreTest.Items[i].KprAirPR > 0 ||
                                    w.coreTest.Items[i].KprHeliumPP > 0 || w.coreTest.Items[i].KprHeliumPR > 0)
                                {
                                    if (w.coreTest.Items[i].KprAirPP > 0)
                                    {
                                        row[8] = w.coreTest.Items[i].KprAirPP;
                                    }
                                    else if (w.coreTest.Items[i].KprAirPR > 0)
                                    {
                                        row[8] = w.coreTest.Items[i].KprAirPR;
                                    }
                                    else if (w.coreTest.Items[i].KprHeliumPP > 0)
                                    {
                                        row[8] = w.coreTest.Items[i].KprHeliumPP;
                                    }
                                    else if (w.coreTest.Items[i].KprHeliumPR > 0)
                                    {
                                        row[8] = w.coreTest.Items[i].KprHeliumPR;
                                    }
                                }
                                if (w.coreTest.Items[i].SatWatFinal > 0)
                                {
                                    row[9] = w.coreTest.Items[i].SatWatFinal;
                                }
                                if (w.coreTest.Items[i].CarbonateDolomite > 0 || w.coreTest.Items[i].CarbonateInsoluble > 0 || w.coreTest.Items[i].CarbonateTiff > 0)
                                {
                                    if (w.coreTest.Items[i].CarbonateDolomite > 0)
                                    {
                                        row[10] = w.coreTest.Items[i].CarbonateDolomite;
                                    }
                                    else if (w.coreTest.Items[i].CarbonateInsoluble > 0)
                                    {
                                        row[10] = w.coreTest.Items[i].CarbonateInsoluble;
                                    }
                                    else if (w.coreTest.Items[i].CarbonateTiff > 0)
                                    {
                                        row[10] = w.coreTest.Items[i].CarbonateTiff;
                                    }
                                }

                                array2.Add(row);
                            }
                        }
                    }
                    for(i = 0; i < stratumList.Count;i++)
                    {
                        row = new object[4];
                        row[0] = of.Name;
                        row[1] = dictStratum.GetShortNameByCode(stratumList[i]);
                        row[2] = ((List<Well>)stratumParams[i][0]).Count;
                        row[3] = (float)stratumParams[i][1];
                        ofArray.Add(row);
                    }
                    for (i = 0; i < stratumList2.Count; i++)
                    {
                        row = new object[12];
                        row[0] = of.Name;
                        row[1] = dictStratum.GetShortNameByCode(stratumList2[i]);
                        row[2] = stratumParams2[i][0];
                        row[3] = stratumParams2[i][1];
                        row[4] = stratumParams2[i][2];
                        row[5] = stratumParams2[i][3];
                        row[6] = stratumParams2[i][4];
                        row[7] = stratumParams2[i][5];
                        row[8] = stratumParams2[i][6];
                        row[9] = stratumParams2[i][7];
                        row[10] = stratumParams2[i][8];
                        row[11] = stratumParams2[i][9];
                        ofArray2.Add(row);
                    }
                    userState.Element = of.Name;
                    worker.ReportProgress(ofInd * 100 / project.OilFields.Count, userState);
                    of.ClearCoreData(false);
                    of.ClearCoreTestData(false);
                    of.ClearGisData(true);
                }
                userState.WorkCurrentTitle = "Выгрузка данных в Microsoft Excel";
                if ((array.Count > 0 || ofArray.Count > 0 || array2.Count > 0) && excel == null)
                {
                    excel = new MSOffice.Excel();
                    excel.NewWorkbook();
                    while (excel.GetWorkSheetsCount() < 4) excel.AddNewWorkSheet();
                }
                if (array.Count > 0)
                {
                    excel.SetActiveSheet(0);
                    row = new object[9];
                    row[0] = "Месторождение";
                    row[1] = "Скважина";
                    row[2] = "Верх отбора керна, м";
                    row[3] = "Низ отбора керна, м";
                    row[4] = "Пласт ГИС";
                    row[5] = "Объект разработки";
                    row[6] = "Верх пласта, м";
                    row[7] = "Низ пласта, м";
                    row[8] = "Пересечение";
                    array.Insert(0, row);
                    excel.SetRowsArray(worker, e, 0, 0, array);
                }
                if (ofArray.Count > 0)
                {
                    excel.SetActiveSheet(1);
                    row = new object[4];
                    row[0] = "Месторождение";
                    row[1] = "Объект разработки";
                    row[2] = "Количество скважин";
                    row[3] = "Проходка, м";
                    ofArray.Insert(0, row);
                    excel.SetRowsArray(worker, e, 0, 0, ofArray);
                }
                if (array2.Count > 0)
                {
                    excel.SetActiveSheet(2);
                    row = new object[16];
                    row[0] = "Месторождение";
                    row[1] = "Скважина";
                    row[2] = "Верх отбора керна, м";
                    row[3] = "Низ отбора керна, м";
                    row[4] = "Пласт ГИС";
                    row[5] = "Объект разработки";
                    row[6] = "Имя образца";
                    row[7] = "Пористость";
                    row[8] = "Проницаемость";
                    row[9] = "Остат.водонасыщ";
                    row[10] = "Карбонатность";
                    row[11] = "УЭС";
                    row[12] = "Квыт";
                    row[13] = "ОФП";
                    row[14] = "Капиллярометр";
                    row[15] = "Геомех";
                    array2.Insert(0, row);
                    excel.SetRowsArray(0, 0, array2);
                }
                if (ofArray2.Count > 0)
                {

                    excel.SetActiveSheet(3);
                    row = new object[12];
                    row[0] = "Месторождение";
                    row[1] = "Объект разработки";
                    row[2] = "Количество образцов";
                    row[3] = "Пористость";
                    row[4] = "Проницаемость";
                    row[5] = "Остат.водонасыщ";
                    row[6] = "Карбонатность";
                    row[7] = "УЭС";
                    row[8] = "Квыт";
                    row[9] = "ОФП";
                    row[10] = "Капиллярометр";
                    row[11] = "Геомех";
                    ofArray2.Insert(0, row);
                    excel.SetRowsArray(worker, e, 0, 0, ofArray2);
                }

            }
            catch (Exception ex)
            {
                if (excel != null)
                {
                    excel.Quit();
                    excel = null;
                }
                throw new Exception("Ошибка выгрузки отчета по Шахматке", ex);
            }
            finally
            {
                if (excel != null)
                {
                    excel.EnableEvents = true;
                    excel.Visible = true;
                    excel.Disconnect();
                }
            }
        }
        #endregion

        #region Отчет по шахматке
        public static void ReportChess(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета по шахматке";
            userState.WorkCurrentTitle = "Создание отчета по шахматке";
            SmartPlus.MSOffice.Excel excel = null;
            bool ShowExcel = false;
            try
            {
                List<object[]> array = new List<object[]>();
                List<object[]> ofArray = new List<object[]>();
                string[] dateStr = project.tempStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (dateStr.Length < 3) return;
                DateTime Date1 = Convert.ToDateTime(dateStr[0]), date;
                DateTime Date2 = Convert.ToDateTime(dateStr[1]);
                DateTime Date3 = Convert.ToDateTime(dateStr[2]);
                DateTime dt, maxOfDate = DateTime.MinValue;
                OilField of;
                Well w;
                ChessItem item;
                int i, rowIndex = 0, index;
                OilFieldAreaDictionary dict = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                double dQn21, dQn7;
                double[] ofParams = new double[12];
                bool b21day, b7days, bDay;
                for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                {
                    maxOfDate = DateTime.MinValue;
                    of = project.OilFields[ofInd];
#if DEBUG
                    //if (ofInd > 5) break;
                    //if (of.Name != "КАРАЧА-ЕЛГИНСКОЕ") continue;
#endif
                    of.LoadChessFromCache(worker, e);
                    for (i = 0; i < ofParams.Length; i++) { ofParams[i] = 0; }
                    for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                    {
                        w = of.Wells[wInd];
                        if (w.ChessLoaded && w.chess.Count > 0)
                        {
                            if (maxOfDate < w.chess[w.chess.Count - 1].Date)
                            {
                                maxOfDate = w.chess[w.chess.Count - 1].Date;
                            }
                        }
                    }

                    for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                    {
                        w = of.Wells[wInd];
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        if (w.ChessLoaded && w.chess.Count > 0)
                        {
                            rowIndex = array.Count;
                            object[] row = new object[15];
                            row[0] = of.ParamsDict.NGDUName;
                            row[1] = of.Name;
                            row[2] = dict.GetShortNameByCode(w.OilFieldAreaCode);
                            row[3] = w.Name;

                            date = Date1;
                            if (Date1.Year == 9999) date = maxOfDate;

                            index = -1;
                            dQn21 = 0;
                            b21day = false;
                            for (i = 0; i < w.chess.Count; i++)
                            {
                                if (w.chess[i].Date == Date1)
                                {
                                    index = i;
                                    break;
                                }
                            }
                            if (index > -1)
                            {
                                item = w.chess[index];
                                b21day = true;

                                if (item.StayTime < 24)
                                {
                                    row[4] = item.Qliq;
                                    row[5] = item.Qoil;
                                    dQn21 = item.Qoil;
                                    ofParams[0] += item.Qliq;
                                    ofParams[1] += item.Qoil;
                                }
                                ofParams[2] += item.Qliq * (24 - item.StayTime) / 24;
                                ofParams[3] += item.Qoil * (24 - item.StayTime) / 24;
                                if (item.Qliq * (24 - item.StayTime) / 24 > 0)
                                {
                                    row[6] = (item.Qliq * (24 - item.StayTime) / 24 - item.Qoil * (24 - item.StayTime) / 24) * 100.0 / (item.Qliq * (24 - item.StayTime) / 24);
                                }
                                else
                                {
                                    row[6] = 0;
                                }
                            }
                            dQn7 = 0;
                            b7days = false;

                            index = -1;
                            for (i = 0; i < w.chess.Count; i++)
                            {
                                if (w.chess[i].Date == Date2)
                                {
                                    index = i;
                                    break;
                                }
                            }
                            if (index > -1)
                            {
                                item = w.chess[index];
                                b7days = true;
                                //if (b21day)
                                //{
                                if (item.StayTime < 24)
                                {
                                    row[7] = item.Qliq;
                                    row[8] = item.Qoil;
                                    dQn7 = item.Qoil;
                                    ofParams[4] += item.Qliq;
                                    ofParams[5] += item.Qoil;
                                }
                                ofParams[6] += item.Qliq * (24 - item.StayTime) / 24;
                                ofParams[7] += item.Qoil * (24 - item.StayTime) / 24;
                                //}
                                if (item.Qliq * (24 - item.StayTime) / 24 > 0)
                                {
                                    row[9] = (item.Qliq * (24 - item.StayTime) / 24 - item.Qoil * (24 - item.StayTime) / 24) * 100.0 / (item.Qliq * (24 - item.StayTime) / 24);
                                }
                                else
                                {
                                    row[9] = 0;
                                }
                            }
                            // данные на дату
                            index = -1;
                            dt = date;
                            bDay = false;
                            for (i = 0; i < w.chess.Count; i++)
                            {
                                if (w.chess[i].Date == Date3)
                                {
                                    index = i;
                                    break;
                                }
                            }
                            if (index > -1)
                            {
                                bDay = true;
                                item = w.chess[index];
                                row[13] = item.Qoil - dQn21;

                                if (item.StayTime < 24)
                                {
                                    row[10] = item.Qliq;
                                    row[11] = item.Qoil;
                                    row[14] = item.Qoil - dQn7;
                                    ofParams[8] += item.Qliq;
                                    ofParams[9] += item.Qoil;
                                }
                                ofParams[10] += item.Qliq * (24 - item.StayTime) / 24;
                                ofParams[11] += item.Qoil * (24 - item.StayTime) / 24;

                                if (item.Qliq * (24 - item.StayTime) / 24 > 0)
                                {
                                    row[12] = (item.Qliq * (24 - item.StayTime) / 24 - item.Qoil * (24 - item.StayTime) / 24) * 100.0 / (item.Qliq * (24 - item.StayTime) / 24);
                                }
                                else
                                {
                                    row[12] = 0;
                                }
                            }
                            if (b21day || b7days || bDay)
                            {
                                array.Add(row);
                            }
                        }
                        userState.Element = of.Name;
                        worker.ReportProgress(wInd, userState);
                    }
                    int j;
                    of.ClearChessData(false);
                    rowIndex = ofArray.Count;
                    ofArray.Add(new object[13]);
                    ofArray[rowIndex][0] = of.ParamsDict.NGDUName;
                    ofArray[rowIndex][1] = of.Name;
                    for (i = 0, j = 2; i < ofParams.Length; i += 4, j += 3)
                    {
                        ofArray[rowIndex][j] = ofParams[i];
                        ofArray[rowIndex][j + 1] = ofParams[i + 1];
                        if ((ofParams[i] > 0) && (ofParams[i + 2] > 0))
                        {
                            ofArray[rowIndex][j + 2] = (ofParams[i + 2] - ofParams[i + 3]) * 100.0 / ofParams[i + 2];
                        }
                        else
                        {
                            ofArray[rowIndex][j + 2] = 0;
                        }
                    }
                    ofArray[rowIndex][11] = ofParams[9] - ofParams[1];
                    ofArray[rowIndex][12] = ofParams[9] - ofParams[5];
                }
                userState.WorkCurrentTitle = "Выгрузка данных в Microsoft Excel";
                if (array.Count > 0)
                {
                    int j, k, endRow = -1;
                    excel = new SmartPlus.MSOffice.Excel();
                    e.Result = excel;
                    excel.EnableEvents = false;
                    excel.NewWorkbook();
                    excel.SetActiveSheetName("По скважинам");
                    object[,] varRow = (object[,])Array.CreateInstance(typeof(object), new int[2] { 50, array[0].Length }, new int[2] { 0, 0 });
                    object[,] heap = (object[,])Array.CreateInstance(typeof(object), new int[2] { 2, array[0].Length }, new int[2] { 0, 0 });
                    heap[0, 0] = "НГДУ";
                    heap[0, 1] = "Месторождение";
                    heap[0, 2] = "Площадь";
                    heap[0, 3] = "Скважина";
                    heap[0, 4] = string.Format("Qж на {0:dd.MM.yyyy}", Date1);
                    heap[0, 5] = string.Format("Qн на {0:dd.MM.yyyy}", Date1);
                    heap[0, 6] = string.Format("% на {0:dd.MM.yyyy}", Date1);
                    heap[0, 7] = string.Format("Qж на {0:dd.MM.yyyy}", Date2);
                    heap[0, 8] = string.Format("Qн на {0:dd.MM.yyyy}", Date2);
                    heap[0, 9] = string.Format("% на {0:dd.MM.yyyy}", Date2);
                    heap[0, 10] = string.Format("Qж на {0:dd.MM.yyyy}", Date3);
                    heap[0, 11] = string.Format("Qн на {0:dd.MM.yyyy}", Date3);
                    heap[0, 12] = string.Format("% на {0:dd.MM.yyyy}", Date3);
                    heap[0, 13] = string.Format("Разность Qн({0:dd.MM.yyyy}) - Qн({1:dd.MM.yyyy})", Date3, Date1);
                    heap[0, 14] = string.Format("Разность Qн({0:dd.MM.yyyy}) - Qн({1:dd.MM.yyyy})", Date3, Date2);
                    heap[1, 4] = "т/сут";
                    heap[1, 5] = "т/сут";
                    heap[1, 6] = "%";
                    heap[1, 7] = "т/сут";
                    heap[1, 8] = "т/сут";
                    heap[1, 9] = "%";
                    heap[1, 10] = "т/сут";
                    heap[1, 11] = "т/сут";
                    heap[1, 12] = "%";

                    excel.SetRange(0, 0, 1, array[0].Length - 1, heap);
                    for (i = 0; i < array.Count; i += 50)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            excel.Quit();
                            return;
                        }
                        for (k = 0; k < 50; k++)
                        {
                            if ((i + k >= array.Count) || (array[i + k] == null))
                            {
                                endRow = i + k - 1;
                                break;
                            }
                            else
                            {
                                for (j = 0; j < array[i].Length; j++)
                                {
                                    varRow[k, j] = array[i + k][j];
                                }
                                endRow = i + 50 - 1;
                            }
                        }
                        excel.SetRange(i + 2, 0, endRow + 2, array[0].Length - 1, varRow);
                        worker.ReportProgress(i, userState);
                    }
                    if (excel.GetWorkSheetsCount() > 1)
                    {
                        excel.SetActiveSheet(1);
                        excel.SetActiveSheetName("По месторождениям");
                        heap[0, 0] = "НГДУ";
                        heap[0, 1] = "Месторождение";

                        heap[0, 2] = string.Format("Qж на {0:dd.MM.yyyy}", Date1);
                        heap[0, 3] = string.Format("Qн на {0:dd.MM.yyyy}", Date1);
                        heap[0, 4] = string.Format("% на {0:dd.MM.yyyy}", Date1);
                        heap[0, 5] = string.Format("Qж на {0:dd.MM.yyyy}", Date2);
                        heap[0, 6] = string.Format("Qн на {0:dd.MM.yyyy}", Date2);
                        heap[0, 7] = string.Format("% на {0:dd.MM.yyyy}", Date2);
                        heap[0, 8] = string.Format("Qж на {0:dd.MM.yyyy}", Date3);
                        heap[0, 9] = string.Format("Qн на {0:dd.MM.yyyy}", Date3);
                        heap[0, 10] = string.Format("% на {0:dd.MM.yyyy}", Date3);
                        heap[0, 11] = string.Format("Разность Qн({0:dd.MM.yyyy}) - Qн({1:dd.MM.yyyy})", Date3, Date1);
                        heap[0, 12] = string.Format("Разность Qн({0:dd.MM.yyyy}) - Qн({1:dd.MM.yyyy})", Date3, Date2);
                        heap[1, 2] = "т/сут";
                        heap[1, 3] = "т/сут";
                        heap[1, 4] = "%";
                        heap[1, 5] = "т/сут";
                        heap[1, 6] = "т/сут";
                        heap[1, 7] = "%";
                        heap[1, 8] = "т/сут";
                        heap[1, 9] = "т/сут";
                        heap[1, 10] = "%";
                        heap[1, 11] = "";
                        heap[1, 12] = "";
                        excel.SetRange(0, 0, 1, ofArray[0].Length - 1, heap);
                        for (i = 0; i < ofArray.Count; i += 50)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                excel.Quit();
                                e.Result = null;
                                return;
                            }
                            for (k = 0; k < 50; k++)
                            {
                                if ((i + k >= ofArray.Count) || (ofArray[i + k] == null))
                                {
                                    endRow = i + k - 1;
                                    break;
                                }
                                else
                                {
                                    for (j = 0; j < ofArray[i].Length; j++)
                                    {
                                        varRow[k, j] = ofArray[i + k][j];
                                    }
                                    endRow = i + 50 - 1;
                                }
                            }
                            excel.SetRange(i + 2, 0, endRow + 2, ofArray[0].Length - 1, varRow);
                            worker.ReportProgress(i, userState);
                        }
                    }
                    ShowExcel = true;
                }
            }
            catch (Exception ex)
            {
                if (excel != null)
                {
                    excel.Quit();
                    excel = null;
                }
                throw new Exception("Ошибка выгрузки отчета по Шахматке", ex);
            }
            finally
            {
                if (excel != null)
                {
                    if (ShowExcel)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                    }
                    excel.Disconnect();
                }
            }
        }
        #endregion

        #region Выгрузка суммарной базовой добычи
        public static void ReportProductionWithoutGtm(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета по базовой добыче";
            userState.WorkCurrentTitle = "Создание отчета по базовой добыче";
            SmartPlus.MSOffice.Excel excel = null;
            bool ShowExcel = false;
            try
            {
                int i, j, ind;
                Well w;
                OilField of;
                MerComp comp;
                double[][] vals = new double[2][];
                vals[0] = new double[12];
                vals[1] = new double[12];
                bool findGtm, worked;
                StratumDictionary dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                StratumTreeNode main = dict.GetStratumTreeNode(184); // CTур
                StratumTreeNode node = new StratumTreeNode();
                MerWorkTimeItem wtItemProd, wtItemInj;
                excel = new SmartPlus.MSOffice.Excel();
                excel.EnableEvents = false;
                excel.NewWorkbook();
                DateTime dt = new DateTime(2012, 1, 1);
                excel.SetValue(0, 0, "Параметр");

                for (i = 0; i < 12; i++) excel.SetValue(0, i + 2, dt.AddMonths(i).ToShortDateString());

                for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                {
                    of = project.OilFields[ofInd];
                    of.LoadMerFromCache(worker, e);
                    of.LoadGTMFromCache(worker, e);

                    for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                    {
                        w = of.Wells[wInd];
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        findGtm = false;
                        if(w.GTMLoaded)
                        {
                            for(i = 0; i  < w.gtm.Count;i++)
                            {
                                if(w.gtm[i].Date.Year == 2012) 
                                {
                                    findGtm = true;
                                    break;
                                }
                            }
                        }
                        
                        if (!findGtm && w.MerLoaded)
                        {
                            comp = new MerComp(w.mer);
                            for (i = 0; i < comp.Count; i++)
                            {
                                if (comp[i].Date.Year != 2012) continue;
                                ind = comp[i].Date.Month - 1;
                                worked = false;
                                for(j = 0; j < comp[i].PlastItems.Count;j++)
                                {
                                    if(node.StratumCode != comp[i].PlastItems[j].PlastCode)
                                    {
                                        node = dict.GetStratumTreeNode(comp[i].PlastItems[j].PlastCode);
                                    }
                                    if (dict.EqualsStratum(main, node))
                                    {
                                        worked = true;
                                        vals[0][ind] += comp[i].PlastItems[j].Oil;
                                    }
                                }
                                if (worked)
                                {
                                    wtItemProd = comp[i].TimeItems.GetAllProductionTime();
                                    wtItemInj = comp[i].TimeItems.GetAllInjectionTime();
                                    if (wtItemProd.WorkTime + wtItemProd.CollTime > 0)
                                    {
                                        vals[1][ind]++;
                                    }
                                }
                            }
                            comp.Clear();
                        }
                        userState.Element = of.Name;
                        worker.ReportProgress(wInd, userState);
                    }
                    excel.SetValue(ofInd * 2 + 1, 0, of.Name);
                    excel.SetValue(ofInd * 2 + 2, 0, of.Name);
                    excel.SetValue(ofInd * 2 + 1, 1, "Добыча нефти, т");
                    excel.SetValue(ofInd * 2 + 2, 1, "Фонд добывающих скважин, шт");

                    for (i = 0; i < 12; i++)
                    {
                        excel.SetValue(ofInd * 2 + 1, + i + 2, vals[0][i].ToString());
                        excel.SetValue(ofInd * 2 + 2, + i + 2, vals[1][i].ToString());
                        vals[0][i] = 0;
                        vals[1][i] = 0;
                    }                    
                    of.ClearMerData(false);
                    of.ClearGTMData(false);
                }
            }
            catch (Exception ex)
            {
                if (excel != null)
                {
                    excel.Quit();
                }
                throw new Exception("Ошибка выгрузки отчета по Шахматке", ex);
            }
            finally
            {
                if (excel != null)
                {
                    excel.EnableEvents = true;
                    excel.Visible = true;
                    excel.Disconnect();
                }
            }
        }
        #endregion

        #region Отчет Анализ заводнения
        static double[] GetAccumRegim(List<Well> wells)
        {
            double[] res = new double[4];
            MerComp mer;
            for (int i = 0; i < wells.Count; i++)
            {
                if (wells[i].MerLoaded)
                {
                    if (wells[i].merEx != null)
                    {
                        mer = wells[i].merEx;
                    }
                    else
                    {
                        mer = new MerComp(wells[i].mer);
                    }
                    for (int j = 0; j < mer.Count; j++)
                    {
                        res[0] += mer[j].SumLiquid;
                        res[1] += mer[j].SumOil;
                        res[2] += mer[j].SumLiquid - mer[j].SumOil;
                        res[3] += mer[j].SumInjection;
                    }
                }
            }
            return res;
        }
        static double[] GetAccumRegim(List<Well> wells, StratumDictionary stratumDict, int PlastCode)
        {
            double[] res = new double[4];
            MerComp mer;
            for (int i = 0; i < wells.Count; i++)
            {
                if (wells[i].MerLoaded)
                {
                    if (wells[i].merEx != null)
                    {
                        mer = wells[i].merEx;
                    }
                    else
                    {
                        mer = new MerComp(wells[i].mer);
                    }
                    for (int j = 0; j < mer.Count; j++)
                    {
                        for (int k = 0; k < mer[j].PlastItems.Count; k++)
                        {
                            if (stratumDict.EqualsStratum(mer[j].PlastItems[k].PlastCode, PlastCode))
                            {
                                res[0] += mer[j].PlastItems[k].Liq;
                                res[1] += mer[j].PlastItems[k].Oil;
                                res[2] += mer[j].PlastItems[k].Liq - mer[j].PlastItems[k].Oil;
                                res[3] += mer[j].PlastItems[k].Inj;
                            }
                        }
                    }
                }
            }
            return res;
        }
        static Area GetAreaByWell(OilField of, Well w)
        {
            if ((of.Areas != null) && (w.AreaIndex > -1) && (w.AreaIndex < of.Areas.Count))
            {
                return of.Areas[w.AreaIndex];
            }
            return null;
        }
        static WellPad GetWellPad(OilField of, Well w)
        {
            WellPad wp;
            for (int i = 0; i < of.WellPads.Count; i++)
            {
                wp = (WellPad)of.WellPads[i];
                for (int j = 0; j < wp.wells.Count; j++)
                {
                    if (wp.wells[j] == w)
                    {
                        return wp;
                    }
                }
            }
            return null;
        }
        public static void ReportFloodingAnalysis(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета 'Анализ заводнения'";
            userState.WorkCurrentTitle = "Создание отчета 'Анализ заводнения'";

            int i, j, ind, count, col, p, n, m;
            List<object[]> array = new List<object[]>();
            List<object[]> array2 = new List<object[]>();
            DateTime dt;
            OilField of;
            Well w, w2;
            double[] regim, sumRegim = new double[12], accRegim;
            List<int> PlastIds = new List<int>();
            var dict = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            var gtmDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.GTM_TYPE);
            var stateDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STATE);
            var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);

            PointD[] points;
            PointD[] pointsLiq, pointsOil, pointsPress, pointsWat;
            double k, b, r2;
            int lastState;
            string str;
            List<int> CurrentPlastCode = new List<int>(1);
            List<Well> wellList, pressList;
            List<Well> gtmList = new List<Well>(), baseList = new List<Well>();
            bool ShowExcel = false;
            bool find;
            string gtmNames, gtmTypes, gisStr, baseNames;
            StratumTreeNode plNode;
            int PlastCode;
            WellPad wp;
            double water;
            Area area;
            DateTime minDate, maxDate;
            string gtmInjStr;
            double AccumInj, AccumWat, AccumOil;
            double LastInj, LastWat, LastOil;

            int AroundMeters;
            SmartPlus.MSOffice.Excel excel = null;
            DateTime InitMaxDate;
            //InitMaxDate = project.maxMerDate;
            InitMaxDate = new DateTime(2013, 12, 1);
            //InitMaxDate = new DateTime(2012, 12, 1);

            try
            {
                for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                {
                    of = project.OilFields[ofInd];
#if DEBUG
                    //if (ofInd > 2) break;
                    //if (of.Name != "ЮГОМАШЕВСКОЕ") continue;
#endif
                    of.LoadMerFromCache(worker, e);
                    of.LoadGTMFromCache(worker, e);
                    of.LoadPerfFromCache(worker, e);
                    of.LoadGisFromCache(worker, e);
                    of.LoadWellResearchFromCache(worker, e);

                    //for (int wellInd = 0; wellInd < of.WellList.Length; wellInd++)
                    //{
                    //    w = (Well)of.WellList[wellInd];
                    //    w.merEx = new MerComp(w.mer);
                    //    w.mer.SetItems(null, null);
                    //    w.mer = null;
                    //}

                    for (int wellInd = 0; wellInd < of.Wells.Count; wellInd++)
                    {
                        w = of.Wells[wellInd];
                        w.merEx = new MerComp(w.mer);
                        userState.Element = string.Format("{0}[{1}]", w.Name, of.Name);
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        wp = GetWellPad(of, w);
                        area = GetAreaByWell(of, w);

                        #region Скважины без нагнетания
                        ind = w.merEx.GetIndexByDate(InitMaxDate);
                        if ((w.merEx.Count > 0) && (ind > -1) && (w.merEx[ind].Date == InitMaxDate) &&
                            (w.merEx[ind].CharWorkIds.IndexOf(11) != -1) &&
                            (w.merEx[ind].TimeItems.GetAllTime().WorkTime > 0))
                        {
                            // Объекты разработки за период
                            PlastIds.Clear();
                            for (i = ind; i > -1; i--)
                            {
                                if (w.merEx[i].Date < InitMaxDate.AddYears(-1)) break;
                                for (j = 0; j < w.merEx[i].PlastItems.Count; j++)
                                {
                                    if (PlastIds.IndexOf(w.merEx[i].PlastItems[j].PlastCode) == -1)
                                    {
                                        PlastIds.Add(w.merEx[i].PlastItems[j].PlastCode);
                                    }
                                }
                            }
                            // Расчет давления по годам
                            // скважины в окружении для давления
                            wellList = GetEnviromentByDate(project, w, PlastIds, InitMaxDate, 500, 2);
                            if (wellList == null || wellList.Count == 0)
                            {
                                gtmInjStr = string.Empty;
                                if (w.GTMLoaded)
                                {
                                    find = false;
                                    for (j = 0; j < w.gtm.Count; j++)
                                    {
                                        if (w.gtm[j].Date.Year == InitMaxDate.Year)
                                        {
                                            find = true;
                                            if (gtmInjStr.Length > 0) gtmInjStr += "; ";
                                            gtmInjStr += string.Format("{0:dd.MM.yyyy}({1})", w.gtm[j].Date, gtmDict.GetShortNameByCode(w.gtm[j].GtmCode));
                                        }
                                    }
                                }
                                minDate = new DateTime(InitMaxDate.Year, 1, 1);

                                // Перфорированные пласты
                                gisStr = string.Empty;
                                if (w.PerfLoaded && w.GisLoaded)
                                {
                                    List<int> gisPlast = new List<int>();
                                    List<SkvPerfItem> openPerf = w.perf.GetOpenIntervals(InitMaxDate);
                                    for (j = 0; j < w.gis.Count; j++)
                                    {
                                        for (i = 0; i < openPerf.Count; i++)
                                        {
                                            if (!((openPerf[i].Bottom < w.gis.Items[j].H) || (openPerf[i].Top > (w.gis.Items[j].H + w.gis.Items[j].L))))
                                            {
                                                if (gisPlast.IndexOf(w.gis.Items[j].PlastId) == -1)
                                                {
                                                    gisPlast.Add(w.gis.Items[j].PlastId);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    gisStr = string.Empty;
                                    for (i = 0; i < gisPlast.Count; i++)
                                    {
                                        plNode = stratumDict.GetStratumTreeNode(gisPlast[i]);
                                        if (plNode != null)
                                        {
                                            if (gisStr.Length > 0) gisStr += "; ";
                                            gisStr += plNode.Name;
                                        }
                                    }
                                }
                                for (n = 0; n < PlastIds.Count; n++)
                                {
                                    PlastCode = PlastIds[n];
                                    object[] row = new object[39];
                                    row[0] = of.ParamsDict.NGDUName;
                                    row[1] = of.Name;
                                    row[2] = dict.GetShortNameByCode(w.OilFieldAreaCode);
                                    if (wp != null) row[3] = wp.Name;
                                    row[4] = w.Name;
                                    row[7] = gisStr;

                                    // Состояние на текущую дату
                                    lastState = -1;
                                    ind = w.merEx.GetIndexByDate(InitMaxDate);
                                    if ((w.merEx.Count > 0) && (ind > -1) && (w.merEx[ind].Date == InitMaxDate))
                                    {
                                        lastState = w.merEx[ind].StateId;
                                    }
                                    row[5] = stateDict.GetShortNameByCode(lastState);
                                    row[6] = gtmInjStr;

                                    plNode = stratumDict.GetStratumTreeNode(PlastCode);
                                    if (plNode != null) row[8] = plNode.Name;

                                    // Нагнетательные в окружении 1000 м
                                    CurrentPlastCode.Clear();
                                    CurrentPlastCode.Add(PlastCode);
                                    wellList = GetEnviromentByDate(project, w, CurrentPlastCode, InitMaxDate, 1000, 2);
                                    str = string.Empty;
                                    for (i = 0; (wellList != null) && (i < wellList.Count); i++)
                                    {
                                        if (i > 0) str += "; ";
                                        str += wellList[i].Name;
                                    }
                                    row[9] = str;

                                    wellList = new List<Well>();
                                    wellList.Add(w);

                                    // Расчет давления по годам
                                    if (wellList != null)
                                    {
                                        dt = new DateTime(InitMaxDate.Year, 1, 1);
                                        dt = dt.AddYears(-2);
                                        double[][] Pressure = new double[12 * 3][];
                                        for (i = 0; i < Pressure.Length; i++)
                                        {
                                            Pressure[i] = new double[4];
                                            Pressure[i][0] = dt.ToOADate();
                                            dt = dt.AddMonths(1);
                                        }
                                        GetPressureByWells(wellList, stratumDict, PlastCode, Pressure);
                                        for (i = 0; i < 3; i++)
                                        {
                                            pointsPress = new PointD[12];
                                            count = 0;
                                            for (j = 0; j < 12; j++)
                                            {
                                                pointsPress[j].X = j;
                                                if (Pressure[i * 12 + j][2] > 0)
                                                {
                                                    pointsPress[j].Y = Pressure[i * 12 + j][1] / Pressure[i * 12 + j][2];
                                                    count += (int)Pressure[i * 12 + j][3];
                                                }
                                            }
                                            Geometry.GetLinearTrend(pointsPress, out k, out b, out r2);
                                            row[10 + i * 3] = k;
                                            row[10 + i * 3 + 1] = r2;
                                            row[10 + i * 3 + 2] = count;
                                        }
                                        if (Pressure != null && Pressure.Length > 0 && Pressure[Pressure.Length - 1][2] > 0)
                                        {
                                            row[19] = Pressure[Pressure.Length - 1][1] / Pressure[Pressure.Length - 1][2];
                                        }
                                    }
                                    count = (InitMaxDate.Year - minDate.Year) * 12 + InitMaxDate.Month - minDate.Month + 1;

                                    // радиус поиска скважин
                                    if (wellList != null)
                                    {
                                        // накопленные показатели
                                        accRegim = GetAccumRegim(wellList, stratumDict, PlastCode);
                                        row[20] = accRegim[0] / 1000;
                                        row[21] = accRegim[1] / 1000;
                                        row[22] = accRegim[2] / 1000;
                                        AccumOil = accRegim[1];
                                        AccumWat = accRegim[2];

                                        ClearArray(sumRegim);
                                        for (i = 0; i < wellList.Count; i++)
                                        {
                                            regim = GetRegimByDate(wellList[i], minDate, stratumDict, PlastCode);
                                            AddArray(regim, sumRegim);
                                        }
                                        row[23] = sumRegim[8];
                                        row[27] = sumRegim[1];
                                        row[31] = sumRegim[10];
                                        row[33] = sumRegim[5];
                                        if (sumRegim[4] > 0) row[35] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];


                                        ClearArray(sumRegim);
                                        for (i = 0; i < wellList.Count; i++)
                                        {
                                            regim = GetRegimByDate(wellList[i], InitMaxDate, stratumDict, PlastCode);
                                            AddArray(regim, sumRegim);
                                        }
                                        row[24] = sumRegim[8];
                                        row[28] = sumRegim[1];
                                        row[32] = sumRegim[10];
                                        row[34] = sumRegim[5];
                                        if (sumRegim[4] > 0) row[36] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                        j = 0;
                                        pointsLiq = new PointD[count];
                                        pointsOil = new PointD[count];
                                        pointsWat = new PointD[count];

                                        dt = minDate;
                                        while (dt <= InitMaxDate)
                                        {
                                            ClearArray(sumRegim);
                                            for (ind = 0; ind < wellList.Count; ind++)
                                            {
                                                regim = GetRegimByDate(wellList[ind], dt, stratumDict, PlastCode);
                                                AddArray(regim, sumRegim);
                                            }
                                            pointsLiq[j].X = j;
                                            pointsLiq[j].Y = sumRegim[8];
                                            pointsOil[j].X = j;
                                            pointsOil[j].Y = sumRegim[1];
                                            pointsWat[j].X = j;
                                            if (sumRegim[4] > 0) pointsWat[j].Y = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];
                                            dt = dt.AddMonths(1);
                                            j++;
                                        }
                                        Geometry.GetLinearTrend(pointsLiq, out k, out b, out r2);
                                        row[25] = k;
                                        row[26] = r2;
                                        Geometry.GetLinearTrend(pointsOil, out k, out b, out r2);
                                        row[29] = k;
                                        row[30] = r2;
                                        Geometry.GetLinearTrend(pointsWat, out k, out b, out r2);
                                        row[37] = k;
                                        row[38] = r2;
                                    }
                                    array2.Add(row);
                                }
                            }
                        }
                        #endregion
                        ind = w.merEx.GetIndexByDate(InitMaxDate);
                        // Закачка(Приемистость) тек / нач, 
                        if ((w.merEx.Count > 0) && (ind > -1) && (w.merEx[ind].Date == InitMaxDate) &&
                            (w.merEx[ind].CharWorkIds.IndexOf(20) != -1))
                        {
                            // Объекты разработки за период
                            PlastIds.Clear();
                            for (i = ind; i > -1; i--)
                            {
                                if (w.merEx[i].Date < InitMaxDate.AddYears(-1)) break;
                                for (j = 0; j < w.merEx[i].PlastItems.Count; j++)
                                {
                                    if (PlastIds.IndexOf(w.merEx[i].PlastItems[j].PlastCode) == -1)
                                    {
                                        PlastIds.Add(w.merEx[i].PlastItems[j].PlastCode);
                                    }
                                }
                            }
                            gtmInjStr = string.Empty;
                            DateTime firstGTMdate = DateTime.MinValue;
                            List<DateTime> gtmDates = new List<DateTime>();
                            if (w.GTMLoaded)
                            {
                                find = false;
                                for (j = 0; j < w.gtm.Count; j++)
                                {
                                    if (w.gtm[j].Date.Year == InitMaxDate.Year)
                                    {
                                        if (firstGTMdate == DateTime.MinValue) firstGTMdate = w.gtm[j].Date;
                                        find = true;
                                        if (gtmInjStr.Length > 0) gtmInjStr += "; ";
                                        gtmInjStr += string.Format("{0:dd.MM.yyyy}({1})", w.gtm[j].Date, gtmDict.GetShortNameByCode(w.gtm[j].GtmCode));
                                        gtmDates.Add(new DateTime(w.gtm[j].Date.Year, w.gtm[j].Date.Month, 1));
                                    }
                                }
                            }

                            for (n = 0; n < PlastIds.Count; n++)
                            {
                                PlastCode = PlastIds[n];
                                CurrentPlastCode.Clear();
                                CurrentPlastCode.Add(PlastCode);
                                object[] row = new object[105];
                                row[0] = of.ParamsDict.NGDUName;
                                row[1] = of.Name;
                                row[2] = dict.GetShortNameByCode(w.OilFieldAreaCode);
                                if (wp != null) row[3] = wp.Name;
                                row[4] = w.Name;

                                // Дата ввода
                                i = ind;
                                lastState = -1;
                                minDate = w.merEx[0].Date;
                                maxDate = DateTime.MaxValue;
                                while (i > -1)
                                {
                                    if (w.merEx[i].CharWorkIds.IndexOf(20) == -1) break;
                                    for (j = 0; j < w.merEx[i].PlastItems.Count; j++)
                                    {
                                        if (w.merEx[i].PlastItems[j].PlastCode == PlastCode)
                                        {
                                            if (lastState == -1) lastState = w.merEx[i].StateId;
                                            if (maxDate == DateTime.MaxValue) maxDate = w.merEx[i].Date;
                                            minDate = w.merEx[i].Date;
                                            break;
                                        }
                                    }
                                    i--;
                                }
                                if (maxDate == DateTime.MaxValue) maxDate = w.merEx[ind].Date;
                                if (i < 0) i = 0;
                                dt = w.merEx[i].Date.AddMonths(1);
                                row[5] = minDate.ToShortDateString();
                                row[6] = stateDict.GetShortNameByCode(lastState);
                                dt = new DateTime(maxDate.Year, 1, 1);
                                if (minDate < dt) minDate = dt;
                                row[7] = gtmInjStr;

                                // Перфорированные пласты
                                gisStr = string.Empty;
                                if (w.PerfLoaded && w.GisLoaded)
                                {
                                    List<int> gisPlast = new List<int>();
                                    List<SkvPerfItem> openPerf = w.perf.GetOpenIntervals(maxDate);
                                    List<SkvPerfItem> closedPerf = w.perf.GetClosedIntervals(maxDate);

                                    List<int> gisNotPerfPlast = new List<int>(); List<float> gisNotPerfPlastH = new List<float>();
                                    List<int> gisClosedPlast = new List<int>(); List<float> gisClosedPlastH = new List<float>();

                                    bool added = false;
                                    string NotPerf, IsolatePerf;

                                    for (j = 0; j < w.gis.Count; j++)
                                    {
                                        for (i = 0; i < openPerf.Count; i++)
                                        {
                                            if (!((openPerf[i].Bottom < w.gis.Items[j].H) || (openPerf[i].Top > (w.gis.Items[j].H + w.gis.Items[j].L))))
                                            {
                                                if (gisPlast.IndexOf(w.gis.Items[j].PlastId) == -1)
                                                {
                                                    added = true;
                                                    gisPlast.Add(w.gis.Items[j].PlastId);
                                                    break;
                                                }
                                            }
                                        }
                                        for (i = 0; i < openPerf.Count; i++)
                                        {
                                            if (!((openPerf[i].Bottom < w.gis.Items[j].H) || (openPerf[i].Top > (w.gis.Items[j].H + w.gis.Items[j].L))))
                                            {
                                                if (gisPlast.IndexOf(w.gis.Items[j].PlastId) == -1)
                                                {
                                                    gisPlast.Add(w.gis.Items[j].PlastId);
                                                }
                                            }
                                        }
                                        for (i = 0; i < closedPerf.Count; i++)
                                        {
                                            if (!((closedPerf[i].Bottom < w.gis.Items[j].H) || (closedPerf[i].Top > (w.gis.Items[j].H + w.gis.Items[j].L))))
                                            {
                                                added = true;
                                                ind = gisClosedPlast.IndexOf(w.gis.Items[j].PlastId);
                                                if (ind == -1)
                                                {
                                                    ind = gisClosedPlast.Count;
                                                    gisClosedPlast.Add(w.gis.Items[j].PlastId);
                                                    gisClosedPlastH.Add(w.gis.Items[j].L);
                                                }
                                                else
                                                {
                                                    gisClosedPlastH[ind] += w.gis.Items[j].L;
                                                }
                                                break;
                                            }
                                        }
                                        if ((!added) && (w.gis.Items[j].Collector) &&
                                            ((w.gis.Items[j].Sat0 == 1) || (w.gis.Items[j].Sat0 == 3) || (w.gis.Items[j].Sat0 == 5) || (w.gis.Items[j].Sat0 == 7)))
                                        {
                                            ind = gisNotPerfPlast.IndexOf(w.gis.Items[j].PlastId);
                                            if (ind == -1)
                                            {
                                                ind = gisNotPerfPlast.Count;
                                                gisNotPerfPlast.Add(w.gis.Items[j].PlastId);
                                                gisNotPerfPlastH.Add(w.gis.Items[j].L);
                                            }
                                            else
                                            {
                                                gisNotPerfPlastH[ind] += w.gis.Items[j].L;
                                            }
                                        }
                                    }
                                    gisStr = string.Empty;
                                    for (i = 0; i < gisPlast.Count; i++)
                                    {
                                        plNode = stratumDict.GetStratumTreeNode(gisPlast[i]);
                                        if (plNode != null)
                                        {
                                            if (gisStr.Length > 0) gisStr += "; ";
                                            gisStr += plNode.Name;
                                        }
                                    }
                                    NotPerf = string.Empty;
                                    for (i = 0; i < gisNotPerfPlast.Count; i++)
                                    {
                                        plNode = stratumDict.GetStratumTreeNode(gisNotPerfPlast[i]);
                                        if (plNode != null)
                                        {
                                            if (NotPerf.Length > 0) NotPerf += "; ";
                                            NotPerf += string.Format("{0}({1:#.##})", plNode.Name, gisNotPerfPlastH[i]);
                                        }
                                    }
                                    IsolatePerf = string.Empty;
                                    for (i = 0; i < gisClosedPlast.Count; i++)
                                    {
                                        plNode = stratumDict.GetStratumTreeNode(gisClosedPlast[i]);
                                        if (plNode != null)
                                        {
                                            if (IsolatePerf.Length > 0) IsolatePerf += "; ";
                                            IsolatePerf += string.Format("{0}({1:#.##})", plNode.Name, gisClosedPlastH[i]);
                                        }
                                    }
                                    row[8] = NotPerf;
                                    row[9] = IsolatePerf;
                                    row[10] = gisStr;
                                }

                                gisStr = string.Empty;
                                plNode = stratumDict.GetStratumTreeNode(PlastCode);
                                if (plNode != null) gisStr = plNode.Name;
                                row[11] = gisStr;
                                regim = GetRegimByDate(w, minDate, stratumDict, PlastCode);
                                row[12] = regim[2];
                                row[17] = regim[6];
                                regim = GetRegimByDate(w, new DateTime(maxDate.Year, 1, 1), stratumDict, PlastCode);
                                row[13] = regim[2];
                                row[18] = regim[6];
                                regim = GetRegimByDate(w, maxDate, stratumDict, PlastCode);
                                row[14] = regim[2];
                                row[19] = regim[6];
                                LastInj = regim[6];
                                if (gtmDates.Count == 1)
                                {
                                    regim = GetRegimByDate(w, gtmDates[0].AddMonths(-1), stratumDict, PlastCode);
                                    row[15] = regim[2];
                                    regim = GetRegimByDate(w, gtmDates[0].AddMonths(1), stratumDict, PlastCode);
                                    row[16] = regim[2];
                                }
                                else if (gtmDates.Count > 1)
                                {
                                    row[15] = string.Empty;
                                    row[16] = string.Empty;
                                    for (j = 0; j < gtmDates.Count; j++)
                                    {
                                        regim = GetRegimByDate(w, gtmDates[j].AddMonths(-1), stratumDict, PlastCode);
                                        if (((string)row[15]).Length > 0) row[15] += ";";
                                        row[15] += regim[2].ToString();
                                        regim = GetRegimByDate(w, gtmDates[j].AddMonths(1), stratumDict, PlastCode);
                                        if (((string)row[16]).Length > 0) row[16] += ";";
                                        row[16] += regim[2].ToString();
                                    }
                                }



                                dt = minDate;
                                count = (maxDate.Year - minDate.Year) * 12 + maxDate.Month - minDate.Month + 1;

                                // Тренд изменения приемистости (линейный)
                                if (count > 1)
                                {
                                    j = 0;
                                    points = new PointD[count];
                                    while (dt <= maxDate)
                                    {
                                        regim = GetRegimByDate(w, dt, stratumDict, PlastCode);
                                        points[j].X = j;
                                        points[j].Y = regim[2];
                                        dt = dt.AddMonths(1);
                                        j++;
                                    }
                                    Geometry.GetLinearTrend(points, out k, out b, out r2);
                                    row[20] = k;
                                    row[21] = r2;
                                }
                                // Накопленная закачка 
                                wellList = new List<Well>(); wellList.Add(w);
                                accRegim = GetAccumRegim(wellList, stratumDict, PlastCode);
                                AccumInj = accRegim[3];
                                row[22] = accRegim[3] / 1000;


                                // Нагнетательные в окружении 1000 м
                                wellList = GetEnviromentByDate(project, w, CurrentPlastCode, maxDate, 1000, 2);
                                str = string.Empty;
                                for (i = 0; (wellList != null) && (i < wellList.Count); i++)
                                {
                                    if (i > 0) str += "; ";
                                    str += wellList[i].Name;
                                }
                                row[23] = str;


                                // добывающие в окружении 500 м
                                AroundMeters = 500;
                                wellList = GetEnviromentByDate(project, w, CurrentPlastCode, maxDate, AroundMeters, 1);
                                if (wellList == null || wellList.Count < 2)
                                {
                                    AroundMeters = 1000;
                                    wellList = GetEnviromentByDate(project, w, CurrentPlastCode, maxDate, AroundMeters, 1);
                                }

                                // Расчет давления по годам
                                // скважины в окружении для давления
                                pressList = GetEnviromentByDate(project, w, CurrentPlastCode, maxDate, AroundMeters, 3);
                                str = string.Empty;
                                for (i = 0; (pressList != null) && (i < pressList.Count); i++)
                                {
                                    if (i > 0) str += "; ";
                                    str += pressList[i].Name;
                                }
                                row[24] = str; // скважин в окружении для расчета давления
                                if (pressList != null)
                                {
                                    dt = new DateTime(minDate.Year, 1, 1);
                                    dt = dt.AddYears(-2);
                                    double[][] Pressure = new double[12 * 3][];
                                    for (i = 0; i < Pressure.Length; i++)
                                    {
                                        Pressure[i] = new double[4];
                                        Pressure[i][0] = dt.ToOADate();
                                        dt = dt.AddMonths(1);
                                    }
                                    GetPressureByWells(pressList, stratumDict, PlastCode, Pressure);
                                    for (i = 0; i < 3; i++)
                                    {
                                        pointsPress = new PointD[12];
                                        count = 0;
                                        for (j = 0; j < 12; j++)
                                        {
                                            pointsPress[j].X = j;
                                            if (Pressure[i * 12 + j][2] > 0)
                                            {
                                                pointsPress[j].Y = Pressure[i * 12 + j][1] / Pressure[i * 12 + j][2];
                                                count += (int)Pressure[i * 12 + j][3];
                                            }
                                        }
                                        Geometry.GetLinearTrend(pointsPress, out k, out b, out r2);
                                        row[25 + i * 3] = k;
                                        row[25 + i * 3 + 1] = r2;
                                        row[25 + i * 3 + 2] = count;
                                    }
                                    if (Pressure != null && Pressure.Length > 0 && Pressure[Pressure.Length - 1][2] > 0)
                                    {
                                        row[34] = Pressure[Pressure.Length - 1][1] / Pressure[Pressure.Length - 1][2];
                                    }
                                }


                                count = (InitMaxDate.Year - minDate.Year) * 12 + InitMaxDate.Month - minDate.Month + 1;

                                // радиус поиска скважин
                                row[35] = AroundMeters;
                                // не нагнетательные в окружении 500 м
                                row[36] = pressList.Count;
                                if (wellList != null)
                                {
                                    str = string.Empty;
                                    col = 37;
                                    row[col] = wellList.Count;
                                    for (i = 0; i < wellList.Count; i++)
                                    {
                                        if (area == null) area = GetAreaByWell(of, wellList[i]);
                                        if (i > 0) str += "; ";

                                        #region Пласты перфорации
                                        gisStr = string.Empty;
                                        if (wellList[i].PerfLoaded && wellList[i].GisLoaded)
                                        {
                                            List<int> gisPlast = new List<int>();
                                            List<SkvPerfItem> openPerf = wellList[i].perf.GetOpenIntervals(maxDate);
                                            for (p = 0; p < openPerf.Count; p++)
                                            {
                                                for (j = 0; j < wellList[i].gis.Count; j++)
                                                {
                                                    if (!((openPerf[p].Bottom < wellList[i].gis.Items[j].H) || (openPerf[p].Top > (wellList[i].gis.Items[j].H + wellList[i].gis.Items[j].L))))
                                                    {
                                                        if (gisPlast.IndexOf(wellList[i].gis.Items[j].PlastId) == -1)
                                                        {
                                                            gisPlast.Add(wellList[i].gis.Items[j].PlastId);
                                                        }
                                                    }
                                                }
                                            }
                                            gisStr = string.Empty;
                                            for (p = 0; p < gisPlast.Count; p++)
                                            {
                                                plNode =stratumDict.GetStratumTreeNode(gisPlast[p]);
                                                if (plNode != null)
                                                {
                                                    if (gisStr.Length > 0) gisStr += "; ";
                                                    gisStr += plNode.Name;
                                                }
                                            }
                                        }
                                        #endregion
                                        str += wellList[i].Name;
                                        if (gisStr.Length > 0) str += "(" + gisStr + ")";
                                    }
                                    row[col + 1] = str;
                                    // накопленные показатели
                                    accRegim = GetAccumRegim(wellList, stratumDict, PlastCode);
                                    row[col + 2] = accRegim[0] / 1000;
                                    row[col + 3] = accRegim[1] / 1000;
                                    row[col + 4] = accRegim[2] / 1000;
                                    AccumOil = accRegim[1];
                                    AccumWat = accRegim[2];

                                    ClearArray(sumRegim);
                                    for (i = 0; i < wellList.Count; i++)
                                    {
                                        regim = GetRegimByDate(wellList[i], minDate, stratumDict, PlastCode);
                                        AddArray(regim, sumRegim);
                                    }
                                    LastWat = sumRegim[4] - sumRegim[5];
                                    LastOil = sumRegim[5];
                                    row[col + 7] = sumRegim[8];
                                    row[col + 11] = sumRegim[1];
                                    row[col + 15] = sumRegim[10];
                                    row[col + 17] = sumRegim[5];
                                    if (sumRegim[4] > 0) row[col + 19] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];
                                    row[col + 5] = 0;
                                    row[col + 6] = 0;
                                    if (area != null)
                                    {
                                        if (LastOil + LastWat > 0) row[col + 5] = LastInj * area.pvt.WaterVolumeFactor / (LastOil * area.pvt.OilVolumeFactor / area.pvt.OilDensity + LastWat * area.pvt.WaterVolumeFactor / area.pvt.WaterDensity);
                                        if (AccumOil + AccumWat > 0) row[col + 6] = AccumInj * area.pvt.WaterVolumeFactor / (AccumOil * area.pvt.OilVolumeFactor / area.pvt.OilDensity + AccumWat * area.pvt.WaterVolumeFactor / area.pvt.WaterDensity);
                                    }


                                    ClearArray(sumRegim);
                                    for (i = 0; i < wellList.Count; i++)
                                    {
                                        regim = GetRegimByDate(wellList[i], InitMaxDate, stratumDict, PlastCode);
                                        AddArray(regim, sumRegim);
                                    }
                                    row[col + 8] = sumRegim[8];
                                    row[col + 12] = sumRegim[1];
                                    row[col + 16] = sumRegim[10];
                                    row[col + 18] = sumRegim[5];
                                    if (sumRegim[4] > 0) row[col + 20] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                    j = 0;
                                    pointsLiq = new PointD[count];
                                    pointsOil = new PointD[count];
                                    pointsWat = new PointD[count];

                                    dt = minDate;
                                    while (dt <= maxDate)
                                    {
                                        ClearArray(sumRegim);
                                        for (ind = 0; ind < wellList.Count; ind++)
                                        {
                                            regim = GetRegimByDate(wellList[ind], dt, stratumDict, PlastCode);
                                            AddArray(regim, sumRegim);
                                        }
                                        pointsLiq[j].X = j;
                                        pointsLiq[j].Y = sumRegim[8];
                                        pointsOil[j].X = j;
                                        pointsOil[j].Y = sumRegim[1];
                                        pointsWat[j].X = j;
                                        if (sumRegim[4] > 0) pointsWat[j].Y = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];
                                        dt = dt.AddMonths(1);
                                        j++;
                                    }
                                    Geometry.GetLinearTrend(pointsLiq, out k, out b, out r2);
                                    row[col + 9] = k;
                                    row[col + 10] = r2;
                                    Geometry.GetLinearTrend(pointsOil, out k, out b, out r2);
                                    row[col + 13] = k;
                                    row[col + 14] = r2;
                                    Geometry.GetLinearTrend(pointsWat, out k, out b, out r2);
                                    row[col + 21] = k;
                                    row[col + 22] = r2;

                                    // Скважины с обводненностью более 95 %, 97 %
                                    str = string.Empty;
                                    for (i = 0; i < wellList.Count; i++)
                                    {
                                        w2 = wellList[i];
                                        regim = GetRegimByDate(w2, InitMaxDate);
                                        if (regim[4] > 0)
                                        {
                                            water = (regim[4] - regim[5]) / regim[4];
                                            if (water > 0.95)
                                            {
                                                if (str.Length > 0) str += ";";
                                                str += w2.Name + "(" + (water * 100).ToString("0.##") + ")";
                                            }
                                        }
                                    }
                                    row[col + 23] = str;



                                    // Выделяем скважины с ГТМ
                                    baseList.Clear();
                                    gtmList.Clear();
                                    baseNames = string.Empty;
                                    gtmNames = string.Empty;
                                    gtmTypes = string.Empty;
                                    for (i = 0; i < wellList.Count; i++)
                                    {
                                        w2 = wellList[i];
                                        find = false;
                                        if (w2.GTMLoaded)
                                        {
                                            str = string.Empty;
                                            for (j = 0; j < w2.gtm.Count; j++)
                                            {
                                                if (w2.gtm[j].Date.Year == InitMaxDate.Year)
                                                {
                                                    find = true;
                                                    if (str.Length > 0) str += "; ";
                                                    str += string.Format("{0:dd.MM.yyyy}({1})", w2.gtm[j].Date, gtmDict.GetShortNameByCode(w2.gtm[j].GtmCode));
                                                }
                                            }
                                            if (find)
                                            {
                                                if (gtmNames.Length > 0) gtmNames += "\n";
                                                gtmNames += w2.Name;
                                                if (gtmTypes.Length > 0) gtmTypes += "\n";
                                                gtmTypes += str;
                                                gtmList.Add(w2);
                                            }
                                        }
                                        if (!find)
                                        {
                                            if (baseNames.Length > 0) baseNames += "; ";
                                            baseNames += w2.Name;
                                            baseList.Add(w2);
                                        }
                                    }
                                    // Базовый фонд окружения
                                    if (baseList.Count > 0)
                                    {
                                        col = 59;
                                        row[col] = baseList.Count;
                                        row[col + 1] = baseNames;
                                        ClearArray(sumRegim);
                                        for (i = 0; i < baseList.Count; i++)
                                        {
                                            regim = GetRegimByDate(baseList[i], minDate, stratumDict, PlastCode);
                                            AddArray(regim, sumRegim);
                                        }
                                        row[col + 2] = sumRegim[8];
                                        row[col + 6] = sumRegim[1];
                                        row[col + 10] = sumRegim[10];
                                        row[col + 12] = sumRegim[5];
                                        if (sumRegim[4] > 0) row[col + 14] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                        ClearArray(sumRegim);
                                        for (i = 0; i < baseList.Count; i++)
                                        {
                                            regim = GetRegimByDate(baseList[i], InitMaxDate, stratumDict, PlastCode);
                                            AddArray(regim, sumRegim);
                                        }
                                        row[col + 3] = sumRegim[8];
                                        row[col + 7] = sumRegim[1];
                                        row[col + 11] = sumRegim[10];
                                        row[col + 13] = sumRegim[5];
                                        if (sumRegim[4] > 0) row[col + 15] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                        // находим линейные тренды
                                        j = 0;
                                        pointsLiq = new PointD[count];
                                        pointsOil = new PointD[count];
                                        pointsPress = new PointD[count];
                                        pointsWat = new PointD[count];

                                        dt = minDate;
                                        while (dt <= InitMaxDate)
                                        {
                                            ClearArray(sumRegim);
                                            for (ind = 0; ind < baseList.Count; ind++)
                                            {
                                                regim = GetRegimByDate(baseList[ind], dt, stratumDict, PlastCode);
                                                AddArray(regim, sumRegim);
                                            }
                                            pointsLiq[j].X = j;
                                            pointsLiq[j].Y = sumRegim[8];
                                            pointsOil[j].X = j;
                                            pointsOil[j].Y = sumRegim[1];
                                            pointsWat[j].X = j;
                                            if (sumRegim[4] > 0) pointsWat[j].Y = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                            dt = dt.AddMonths(1);
                                            j++;
                                        }
                                        Geometry.GetLinearTrend(pointsLiq, out k, out b, out r2);
                                        row[col + 4] = k;
                                        row[col + 5] = r2;
                                        Geometry.GetLinearTrend(pointsOil, out k, out b, out r2);
                                        row[col + 8] = k;
                                        row[col + 9] = r2;
                                        Geometry.GetLinearTrend(pointsWat, out k, out b, out r2);
                                        row[col + 16] = k;
                                        row[col + 17] = r2;
                                        if (firstGTMdate != DateTime.MinValue)
                                        {
                                            ClearArray(sumRegim);
                                            for (i = 0; i < baseList.Count; i++)
                                            {
                                                regim = GetRegimByDate(baseList[i], new DateTime(firstGTMdate.Year, 1, 1), stratumDict, PlastCode);
                                                AddArray(regim, sumRegim);
                                            }
                                            row[col + 18] = sumRegim[0];
                                            row[col + 19] = sumRegim[1];
                                            if (sumRegim[4] > 0) row[col + 20] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                            ClearArray(sumRegim);
                                            for (i = 0; i < baseList.Count; i++)
                                            {
                                                regim = GetRegimByDate(baseList[i], new DateTime(firstGTMdate.AddMonths(-1).Year, firstGTMdate.AddMonths(-1).Month, 1), stratumDict, PlastCode);
                                                AddArray(regim, sumRegim);
                                            }
                                            row[col + 21] = sumRegim[0];
                                            row[col + 22] = sumRegim[1];
                                            if (sumRegim[4] > 0) row[col + 23] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                            ClearArray(sumRegim);
                                            for (i = 0; i < baseList.Count; i++)
                                            {
                                                regim = GetRegimByDate(baseList[i], new DateTime(firstGTMdate.AddMonths(3).Year, firstGTMdate.AddMonths(3).Month, 1), stratumDict, PlastCode);
                                                AddArray(regim, sumRegim);
                                            }
                                            row[col + 24] = sumRegim[0];
                                            row[col + 25] = sumRegim[1];
                                            if (sumRegim[4] > 0) row[col + 26] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                            ClearArray(sumRegim);
                                            for (i = 0; i < baseList.Count; i++)
                                            {
                                                regim = GetRegimByDate(baseList[i], InitMaxDate, stratumDict, PlastCode);
                                                AddArray(regim, sumRegim);
                                            }
                                            row[col + 27] = sumRegim[0];
                                            row[col + 28] = sumRegim[1];
                                            if (sumRegim[4] > 0) row[col + 29] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];
                                        }
                                    }


                                    // Окружение с ГТМ
                                    if (gtmList.Count > 0)
                                    {
                                        col = 77 + 14;
                                        row[col] = gtmList.Count;
                                        row[col + 1] = gtmNames;
                                        row[col + 2] = gtmTypes;
                                        ClearArray(sumRegim);
                                        for (i = 0; i < gtmList.Count; i++)
                                        {
                                            regim = GetRegimByDate(gtmList[i], minDate, stratumDict, PlastCode);
                                            AddArray(regim, sumRegim);
                                        }
                                        row[col + 3] = sumRegim[8];
                                        row[col + 5] = sumRegim[1];
                                        row[col + 7] = sumRegim[10];
                                        row[col + 9] = sumRegim[5];
                                        if (sumRegim[4] > 0) row[col + 11] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];

                                        ClearArray(sumRegim);
                                        for (i = 0; i < gtmList.Count; i++)
                                        {
                                            regim = GetRegimByDate(gtmList[i], InitMaxDate, stratumDict, PlastCode);
                                            AddArray(regim, sumRegim);
                                        }
                                        row[col + 4] = sumRegim[8];
                                        row[col + 6] = sumRegim[1];
                                        row[col + 8] = sumRegim[10];
                                        row[col + 9] = sumRegim[5];
                                        if (sumRegim[4] > 0) row[col + 12] = (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];
                                    }
                                }
                                array.Add(row);
                            }
                        }
                        w.merEx.Clear();
                        w.merEx = null;
                        worker.ReportProgress(wellInd, userState);
                    }
                    of.ClearWellResearchData(false);
                    of.ClearGisData(false);
                    of.ClearPerfData(false);
                    of.ClearGTMData(false);
                    of.ClearMerData(true);
                }
                userState.WorkCurrentTitle = "Выгрузка данных в Microsoft Excel";
                if (array.Count > 0)
                {
                    int r, endRow = -1;
                    excel = new SmartPlus.MSOffice.Excel();
                    e.Result = excel;
                    excel.EnableEvents = false;
                    excel.NewWorkbook();
                    excel.SetActiveSheetName("Анализ заводнения");
                    object[,] varRow = (object[,])Array.CreateInstance(typeof(object), new int[2] { 50, array[0].Length }, new int[2] { 0, 0 });
                    object[,] heap = (object[,])Array.CreateInstance(typeof(object), new int[2] { 2, array[0].Length }, new int[2] { 0, 0 });
                    col = 0;
                    heap[0, col++] = "НГДУ";
                    heap[0, col++] = "Месторождение";
                    heap[0, col++] = "Площадь";
                    heap[0, col++] = "Куст";
                    heap[0, col++] = "Скважина";
                    heap[0, col++] = "Дата ППД";
                    heap[0, col++] = string.Format("Состояние на {0:dd.MM.yyyy}", InitMaxDate);
                    heap[0, col++] = "ГТМ на скважине";
                    heap[0, col++] = "Невскрытые интервалы";
                    heap[0, col++] = "Изолированные интервалы";
                    heap[0, col++] = "Вскрытые пласты";
                    heap[0, col++] = "Объекты разработки";

                    heap[0, col++] = "W на начало закачки"; heap[1, col - 1] = "м3/сут";
                    heap[0, col++] = string.Format("W на {0:dd.MM.yyyy}", new DateTime(InitMaxDate.Year, 1, 1)); heap[1, col - 1] = "м3/сут";
                    heap[0, col++] = string.Format("W на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "м3/сут";
                    heap[0, col++] = "W до ГТМ"; heap[1, col - 1] = "м3/сут";
                    heap[0, col++] = "W после ГТМ"; heap[1, col - 1] = "м3/сут";
                    heap[0, col++] = "Зак на начало закачки"; heap[1, col - 1] = "м3";
                    heap[0, col++] = string.Format("Зак на {0:dd.MM.yyyy}", new DateTime(InitMaxDate.Year, 1, 1)); heap[1, col - 1] = "м3";
                    heap[0, col++] = string.Format("Зак на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "м3";
                    heap[0, col] = "Тренд W"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                    heap[0, col++] = "Нак.закачка"; heap[1, col - 1] = "тыс.м3";
                    heap[0, col++] = "Нагнет.скважины в окружении 1000 м";
                    heap[0, col++] = "Cкважины для расчета давлений в окружении м";
                    heap[0, col] = string.Format("Тренд давления {0}", InitMaxDate.AddYears(-2).Year); heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2"; heap[1, col++] = "Кол-во точек";
                    heap[0, col] = string.Format("Тренд давления {0}", InitMaxDate.AddYears(-1).Year); heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2"; heap[1, col++] = "Кол-во точек";
                    heap[0, col] = string.Format("Тренд давления {0}", InitMaxDate.Year); heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2"; heap[1, col++] = "Кол-во точек";
                    heap[0, col++] = "Давление текущее среднее"; heap[1, col - 1] = "атм";
                    heap[0, col++] = "Радиус поиска окружения"; heap[1, col - 1] = "м";
                    heap[0, col++] = "Количество не нагн.скв. в окружении м";
                    heap[0, col++] = "Количество доб.скв. в окружении м";
                    heap[0, col++] = "Названия скв.окружения";
                    heap[0, col++] = "Накопленная жидкость"; heap[1, col - 1] = "тыс.т";
                    heap[0, col++] = "Накопленная нефть"; heap[1, col - 1] = "тыс. т";
                    heap[0, col++] = "Накопленная вода"; heap[1, col - 1] = "тыс.т";
                    heap[0, col++] = "Компенсация текущая";
                    heap[0, col++] = "Компенсация накопленная";
                    heap[0, col++] = "Qж.окр. на начало закачки"; heap[1, col - 1] = "м3/сут";
                    heap[0, col++] = string.Format("Qж.окр. на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "м3/сут";
                    heap[0, col] = "Тренд Qж.окр"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                    heap[0, col++] = "Qн.окр. на начало закачки"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = string.Format("Qн.окр. на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "т/сут";
                    heap[0, col] = "Тренд Qн.окр"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                    heap[0, col++] = "Жидк.окр. на начало закачки"; heap[1, col - 1] = "м3";
                    heap[0, col++] = string.Format("Жидк.окр. на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "м3";
                    heap[0, col++] = "Нефть.окр. на начало закачки"; heap[1, col - 1] = "т";
                    heap[0, col++] = string.Format("Нефть.окр. на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "т";
                    heap[0, col++] = "Обводн.окр. на начало закачки"; heap[1, col - 1] = "%(вес)";
                    heap[0, col++] = string.Format("Обводн.окр. на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "%(вес)";
                    heap[0, col] = "Тренд %(вес).окр"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                    heap[0, col++] = "Скв.в окружении с обв. более 95 %";
                    heap[0, col++] = "Количество доб.скв.БАЗА в окружении 500 м";
                    heap[0, col++] = "Базовый фонд";
                    heap[0, col++] = "Qж.окр.БАЗА на начало закачки"; heap[1, col - 1] = "м3/сут";
                    heap[0, col++] = string.Format("Qж.окр.БАЗА на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "м3/сут";
                    heap[0, col] = "Тренд Qж.окр.БАЗА"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                    heap[0, col++] = "Qн.окр.БАЗА на начало закачки"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = string.Format("Qн.окр.БАЗА на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "т/сут";
                    heap[0, col] = "Тренд Qн.окр.БАЗА"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                    heap[0, col++] = "Жидк.окр.БАЗА на начало закачки"; heap[1, col - 1] = "м3";
                    heap[0, col++] = string.Format("Жидк.окр.БАЗА на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "м3";
                    heap[0, col++] = "Нефть.окр.БАЗА на начало закачки"; heap[1, col - 1] = "т";
                    heap[0, col++] = string.Format("Нефть.окр.БАЗА на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "т";
                    heap[0, col++] = "Обводн.окр.БАЗА на начало закачки"; heap[1, col - 1] = "%(вес)";
                    heap[0, col++] = string.Format("Обводн.окр.БАЗА на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "%(вес)";
                    heap[0, col] = "Тренд %(вес).окр.БАЗА"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";

                    heap[0, col++] = "Жидк.окр.БАЗА на начало года ГТМ"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = "Нефть.окр.БАЗА на начало года ГТМ"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = "Обводн.окр.БАЗА на начало года ГТМ"; heap[1, col - 1] = "%(вес)";

                    heap[0, col++] = "Жидк.окр.БАЗА за 1 мес до ГТМ"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = "Нефть.окр.БАЗА за 1 мес до ГТМ"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = "Обводн.окр.БАЗА за 1 мес до ГТМ"; heap[1, col - 1] = "%(вес)";

                    heap[0, col++] = "Жидк.окр.БАЗА через 3 мес после ГТМ"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = "Нефть.окр.БАЗА через 3 мес после ГТМ"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = "Обводн.окр.БАЗА через 3 мес после ГТМ"; heap[1, col - 1] = "%(вес)";

                    heap[0, col++] = "Жидк.окр.БАЗА за посл.месяц"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = "Нефть.окр.БАЗА за посл.месяц"; heap[1, col - 1] = "т.сут";
                    heap[0, col++] = "Обводн.окр.БАЗА за посл.месяц"; heap[1, col - 1] = "%(вес)";

                    heap[0, col++] = "Количество доб.скв.ГТМ в окружении 500 м";
                    heap[0, col++] = "Скважины с ГТМ";
                    heap[0, col++] = string.Format("ГТМ проведенные в {0} г.", InitMaxDate.Year);
                    heap[0, col++] = "Qж.окр.ГТМ на начало закачки"; heap[1, col - 1] = "м3/сут";
                    heap[0, col++] = string.Format("Qж.окр.ГТМ на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "м3/сут";
                    //heap[0, col] = "Тренд Qж.окр.ГТМ"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                    heap[0, col++] = "Qн.окр.ГТМ нна начало закачки"; heap[1, col - 1] = "т/сут";
                    heap[0, col++] = string.Format("Qн.окр.ГТМ на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "т/сут";
                    //heap[0, col] = "Тренд Qн.окр.ГТМ"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                    heap[0, col++] = "Жидк.окр.ГТМ на начало закачки"; heap[1, col - 1] = "м3";
                    heap[0, col++] = string.Format("Жидк.окр.ГТМ на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "м3";
                    heap[0, col++] = "Нефть.окр.ГТМ на начало закачки"; heap[1, col - 1] = "т";
                    heap[0, col++] = string.Format("Нефть.окр.ГТМ на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "т";
                    heap[0, col++] = "Обводн.окр.ГТМ на начало закачки"; heap[1, col - 1] = "%(вес)";
                    heap[0, col++] = string.Format("Обводн.окр.ГТМ на {0:dd.MM.yyyy}", InitMaxDate.AddMonths(1)); heap[1, col - 1] = "%(вес)";
                    //heap[0, col] = "Тренд %(вес).окр.ГТМ"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";

                    excel.SetRange(0, 0, 1, array[0].Length - 1, heap);
                    for (i = 0; i < array.Count; i += 50)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            excel.Quit();
                            return;
                        }
                        for (r = 0; r < 50; r++)
                        {
                            if ((i + r >= array.Count) || (array[i + r] == null))
                            {
                                endRow = i + r - 1;
                                break;
                            }
                            else
                            {
                                for (j = 0; j < array[i].Length; j++)
                                {
                                    varRow[r, j] = array[i + r][j];
                                }
                                endRow = i + 50 - 1;
                            }
                        }
                        excel.SetRange(i + 2, 0, endRow + 2, array[0].Length - 1, varRow);
                        worker.ReportProgress(i, userState);
                    }
                    if (array2.Count > 0 && excel.GetWorkSheetsCount() > 1)
                    {
                        excel.SetActiveSheet(1);
                        excel.SetActiveSheetName("Скважины без нагнетания");
                        varRow = (object[,])Array.CreateInstance(typeof(object), new int[2] { 50, array2[0].Length }, new int[2] { 0, 0 });
                        heap = (object[,])Array.CreateInstance(typeof(object), new int[2] { 2, array2[0].Length }, new int[2] { 0, 0 });
                        col = 0;
                        heap[0, col++] = "НГДУ";
                        heap[0, col++] = "Месторождение";
                        heap[0, col++] = "Площадь";
                        heap[0, col++] = "Куст";
                        heap[0, col++] = "Скважина";
                        heap[0, col++] = string.Format("Состояние на {0:dd.MM.yyyy}", InitMaxDate);
                        heap[0, col++] = "ГТМ на скважине";
                        heap[0, col++] = "Проперфорированные пласты";
                        heap[0, col++] = "Объект разработки";
                        heap[0, col++] = "Нагнет.скважины в окружении 1000 м";
                        heap[0, col] = string.Format("Тренд давления {0}", InitMaxDate.AddYears(-2).Year); heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2"; heap[1, col++] = "Кол-во точек";
                        heap[0, col] = string.Format("Тренд давления {0}", InitMaxDate.AddYears(-1).Year); heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2"; heap[1, col++] = "Кол-во точек";
                        heap[0, col] = string.Format("Тренд давления {0}", InitMaxDate.Year); heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2"; heap[1, col++] = "Кол-во точек";
                        heap[0, col++] = "Давление текущее среднее"; heap[1, col - 1] = "атм";
                        heap[0, col++] = "Накопленная жидкость"; heap[1, col - 1] = "тыс.т";
                        heap[0, col++] = "Накопленная нефть"; heap[1, col - 1] = "тыс. т";
                        heap[0, col++] = "Накопленная вода"; heap[1, col - 1] = "тыс.т";
                        heap[0, col++] = "Qж.окр. на начало закачки"; heap[1, col - 1] = "м3/сут";
                        heap[0, col++] = string.Format("Qж.окр. на {0:dd.MM.yyyy}", InitMaxDate); heap[1, col - 1] = "м3/сут";
                        heap[0, col] = "Тренд Qж.окр"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                        heap[0, col++] = "Qн.окр. на начало закачки"; heap[1, col - 1] = "т/сут";
                        heap[0, col++] = string.Format("Qн.окр. на {0:dd.MM.yyyy}", InitMaxDate); heap[1, col - 1] = "т/сут";
                        heap[0, col] = "Тренд Qн.окр"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";
                        heap[0, col++] = "Жидк.окр. на начало закачки"; heap[1, col - 1] = "м3";
                        heap[0, col++] = string.Format("Жидк.окр. на {0:dd.MM.yyyy}", InitMaxDate); heap[1, col - 1] = "м3";
                        heap[0, col++] = "Нефть.окр. на начало закачки"; heap[1, col - 1] = "т";
                        heap[0, col++] = string.Format("Нефть.окр. на {0:dd.MM.yyyy}", InitMaxDate); heap[1, col - 1] = "т";
                        heap[0, col++] = "Обводн.окр. на начало закачки"; heap[1, col - 1] = "%(вес)";
                        heap[0, col++] = string.Format("Обводн.окр. на {0:dd.MM.yyyy}", InitMaxDate); heap[1, col - 1] = "%(вес)";
                        heap[0, col] = "Тренд %(вес).окр"; heap[1, col++] = "Угловой коэффициент"; heap[1, col++] = "R2";

                        excel.SetRange(0, 0, 1, array2[0].Length - 1, heap);
                        for (i = 0; i < array2.Count; i += 50)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                excel.Quit();
                                return;
                            }
                            for (r = 0; r < 50; r++)
                            {
                                if ((i + r >= array2.Count) || (array2[i + r] == null))
                                {
                                    endRow = i + r - 1;
                                    break;
                                }
                                else
                                {
                                    for (j = 0; j < array2[i].Length; j++)
                                    {
                                        varRow[r, j] = array2[i + r][j];
                                    }
                                    endRow = i + 50 - 1;
                                }
                            }
                            excel.SetRange(i + 2, 0, endRow + 2, array2[0].Length - 1, varRow);
                        }
                    }
                    ShowExcel = true;
                }
            }
#if !DEBUG
            catch (Exception ex)
            {
                if (excel != null)
                {
                    excel.Quit();
                }
                throw new Exception("Ошибка выгрузки отчета 'Анализ заводнения'", ex);
            }
#endif
            finally
            {
                if (excel != null)
                {
                    if (ShowExcel)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                    }
                    excel.Disconnect();
                }
            }
        }
        #endregion

        #region Запасы по Вороного по списку скважин
        static void FillPlastTree(Project project, OilField of, List<int> InitPlastCodes, out List<int> MainPlastCodes, out List<List<int>> ChildPlastCodes)
        {
            var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            List<int> PlastCodes = new List<int>();
            StratumTreeNode node1;
            bool find;
            for (int i = 0; i < InitPlastCodes.Count; i++)
            {
                node1 = stratumDict.GetStratumTreeNode(InitPlastCodes[i]);
                for (int j = 0; j < of.MerStratumCodes.Count; j++)
                {
                    if (InitPlastCodes[i] == of.MerStratumCodes[j] || (node1.GetParentLevelByCode(of.MerStratumCodes[j]) > -1))
                    {
                        if (PlastCodes.IndexOf(of.MerStratumCodes[j]) == -1)
                        {
                            PlastCodes.Add(of.MerStratumCodes[j]);
                        }
                    }
                }
            }
            PlastCodes.Sort();
            List<StratumTreeNode> PlastNodes = new List<StratumTreeNode>();
            for (int i = 0; i < PlastCodes.Count; i++)
            {
                PlastNodes.Add(stratumDict.GetStratumTreeNode(PlastCodes[i]));
            }
            MainPlastCodes = new List<int>();
            ChildPlastCodes = new List<List<int>>();


            for (int i = 0; i < PlastNodes.Count; i++)
            {
                find = false;
                for (int j = 0; j < PlastCodes.Count; j++)
                {
                    if (i != j && (PlastNodes[i].GetParentLevelByCode(PlastCodes[j]) > -1))
                    {
                        find = true;
                        break;
                    }
                }
                if (!find)
                {
                    MainPlastCodes.Add(PlastNodes[i].StratumCode);
                    ChildPlastCodes.Add(new List<int>());
                }
            }
            for (int i = 0; i < PlastNodes.Count; i++)
            {
                if (MainPlastCodes.IndexOf(PlastNodes[i].StratumCode) == -1)
                {
                    for (int j = 0; j < MainPlastCodes.Count; j++)
                    {
                        if (PlastNodes[i].GetParentLevelByCode(MainPlastCodes[j]) > -1)
                        {
                            ChildPlastCodes[j].Add(PlastNodes[i].StratumCode);
                        }
                    }
                }
            }
        }
        public static void ReportVoronoiReserves(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета запасов по областям Вороного";
            userState.WorkCurrentTitle = "Создание отчета запасов по областям Вороного";
            SmartPlus.MSOffice.Excel excel = null;
            //if(!File.Exists(project.tempStr)) return;
            
            int i, j, ind;
            OilField of;
            Well w;

            try
            {
                var areaDict = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                // Загрузка списка скважин
                List<Well> wells = new List<Well>();
                List<OilField> oilfields = new List<OilField>();
                List<Well> env;
                excel = new SmartPlus.MSOffice.Excel();
                string str;
                //if (File.Exists(project.tempStr))
                //{
                //    excel.OpenWorkbook(project.tempStr);
                //    i = 0;
                    
                //    string cell = excel.GetValue(i, 0).ToString();
                //    while (cell.Length > 0)
                //    {
                //        ind = project.GetOFIndex(cell);
                //        if (ind > -1)
                //        {
                //            of = project.OilFields[ind];
                //            if (oilfields.IndexOf(of) == -1) oilfields.Add(of);
                //            str = excel.GetValue(i, 1).ToString();
                //            ind = areaDict.GetCodeByShortName(str);
                //            if (ind > 0)
                //            {
                //                str = excel.GetValue(i, 2).ToString();
                //                ind = of.GetWellIndex(str.ToUpper(), ind);
                //                if (ind > -1) wells.Add(of.Wells[ind]);
                //            }
                //        }
                //        i++;
                //        cell = excel.GetValue(i, 0).ToString();
                //    }
                //    excel.CloseWorkbook(false);
                //}
                excel.NewWorkbook();
                List<object[]> array = new List<object[]>();
                object[] row;
                VoronoiMap vMap = null;
                List<Well> initWells = new List<Well>();
                List<int> wellIndexes = new List<int>();
                List<int> PlastCodes = new List<int>();
                List<int> AllPlastCodes = new List<int>();
                StratumTreeNode node;
                List<Grid> grids = new List<Grid>();
                bool find;
                DateTime initDate = project.maxMerDate;
                string outDate;
                double outOil, outW;
                for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                {
                    of = project.OilFields[ofInd];
#if DEBUG
                    //if (of.Name != "АНДРЕЕВСКОЕ") continue;
                    //if (ofInd > 2) break;
                    //if (of.ParamsDict.NGDUName != "Чекмагушнефть") continue;
                    //if (of.Deposits.Count == 0) continue;
#endif

                    of.LoadMerFromCache(worker, e, false);
                    grids.Clear();

                    wellIndexes.Clear();
                    PlastCodes.Clear();
                    initWells.Clear();

                    for (int wellInd = 0; wellInd < of.Wells.Count; wellInd++)
                    {
                        w = of.Wells[wellInd];
                        //if (wells.IndexOf(w) == -1) continue;
                        if (w.OilFieldCode != of.OilFieldCode) continue;
                        
                        userState.Element = string.Format("{0}[{1}]", w.Name, of.Name);
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }

                        env = GetEnviroment(project, w, 500, true);
                        
                        if (env.Count > 0)
                        {
                            wellIndexes.Clear();
                            PlastCodes.Clear();
                            wellIndexes.Add(w.Index);
                            //for(i = 0; i < env.Count;i++)
                            //{
                            //    wellIndexes.Add(env[i].Index);
                            //}
                            string plasts = string.Empty;

                            if (w.MerLoaded)
                            {
                                find = false;
                                for (j = w.mer.Count - 1; j >= 0; j--)
                                {
                                    if (w.mer.Items[j].Date == initDate)
                                    {
                                        find = true;
                                        if(PlastCodes.IndexOf(w.mer.Items[j].PlastId) == -1)
                                        {
                                            PlastCodes.Add(w.mer.Items[j].PlastId);
                                        }
                                    }
                                    else if (find)
                                    {
                                        break;
                                    }
                                }
                                double[] regim = GetRegimByDate(w, initDate);
                                //if (regim[8] < 15 || (regim[10] > 0 && ((regim[10] - regim[11]) * 100f / regim[10]) > 80)) continue;
                                outDate = initDate.ToShortDateString();
                                outOil = regim[8];
                                if (regim[10] > 0) outW = (regim[10] - regim[11]) * 100f / regim[10];
                            }
                            PlastCodes.Clear();
                            PlastCodes.AddRange(of.MerStratumCodes.ToArray());
                            List<int> mainPlastCodes; List<List<int>> childPlastCodes;
                            FillPlastTree(project, of, PlastCodes, out mainPlastCodes, out childPlastCodes);
                            //PlastCodes.Clear();
                            //AllPlastCodes.Clear();
                            //for (i = 0; i < mainPlastCodes.Count; i++)
                            //{
                            //    PlastCodes.Add(mainPlastCodes[i]);
                            //    AllPlastCodes.Add(mainPlastCodes[i]);
                            //    AllPlastCodes.AddRange(childPlastCodes[i].ToArray());
                            //}
                            for (int p = 0; p < mainPlastCodes.Count; p++)
                            {
                                row = new object[25];
                                row[0] = of.ParamsDict.NGDUName;
                                row[1] = of.Name;
                                row[2] = areaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                row[3] = w.Name;

                                // загружаем сетки
                                if (PlastCodes.Count > 0)
                                {
                                    for (i = 0; i < of.Grids.Count; i++)
                                    {
                                        if (of.Grids[i].GridType == 2 && (mainPlastCodes[p] == of.Grids[i].StratumCode))
                                        {
                                            if (grids.IndexOf(of.Grids[i]) == -1) grids.Add(of.Grids[i]);
                                        }
                                    }
                                    if (grids.Count == 0) // сетки не найдена пробуем найти вышележащий пласт
                                    {
                                        node = stratumDict.GetStratumTreeNode(mainPlastCodes[p]);
                                        if (node != null)
                                        {
                                            for (i = 0; i < of.Grids.Count; i++)
                                            {
                                                if (of.Grids[i].GridType == 2 &&
                                                   (node.GetParentLevelByCode(of.Grids[i].StratumCode) > -1) &&
                                                   (of.MerStratumCodes.IndexOf(of.Grids[i].StratumCode) == -1))
                                                {
                                                    if (grids.IndexOf(of.Grids[i]) == -1) grids.Add(of.Grids[i]);
                                                }
                                            }
                                        }
                                    }
                                    for (i = 0; i < grids.Count; i++)
                                    {
                                        if (!grids[i].DataLoaded) of.LoadGridDataFromCacheByIndex(worker, e, grids[i].Index, false);
                                    }
                                }



                                double sumOil = 0;
                                if (w.MerLoaded)
                                {
                                    for (i = 0; i < w.mer.Count; i++)
                                    {
                                        if (mainPlastCodes[p] == w.mer.Items[i].PlastId || childPlastCodes[p].IndexOf(w.mer.Items[i].PlastId) > -1)
                                        {
                                            sumOil += w.mer.Items[i].Oil;
                                        }
                                    }
                                }

                                row[6] = sumOil;
                                sumOil = 0;
                                for (j = 0; j < env.Count; j++)
                                {
                                    if (env[j].MerLoaded)
                                    {
                                        for (i = 0; i < env[j].mer.Count; i++)
                                        {
                                            if (mainPlastCodes[p] == env[j].mer.Items[i].PlastId || childPlastCodes[p].IndexOf(env[j].mer.Items[i].PlastId) > -1)
                                            {
                                                sumOil += env[j].mer.Items[i].Oil;
                                            }
                                        }
                                    }
                                }
                                row[9] = sumOil;

                                if (mainPlastCodes.Count > 0)
                                {
                                    try
                                    {
                                        childPlastCodes[p].Insert(0, mainPlastCodes[p]);
                                        vMap = new VoronoiMap();
                                        env.Add(w);
                                        vMap.ComputeVoronoiMap(worker, e, project.maxMerDate, project, env, childPlastCodes[p], 500, true, null);
                                        double[] wReserves = new double[3];
                                        double[] envReserves = new double[3];
                                        str = string.Empty;
                                        int envCounter = 0;
                                        if (vMap.Cells.Length > 0)
                                        {
                                            for (i = 0; i < vMap.Cells.Length; i++)
                                            {
                                                if (vMap.Cells[i].Well.srcWell == w)
                                                {
                                                    wReserves[0] = vMap.Cells[i].NBZ;
                                                    wReserves[1] = vMap.Cells[i].NIZ;
                                                    wReserves[2] = vMap.Cells[i].AccumOil;
                                                }
                                                else if (vMap.Cells[i].Well.srcWell == env[i])
                                                {
                                                    envReserves[0] += vMap.Cells[i].NBZ;
                                                    envReserves[1] += vMap.Cells[i].NIZ;
                                                    envReserves[2] += vMap.Cells[i].AccumOil;
                                                    if (str.Length > 0) str += ", ";
                                                    str += vMap.Cells[i].Well.srcWell.Name;
                                                    envCounter++;
                                                }
                                            }
                                        }
                                        row[4] = stratumDict.GetShortNameByCode(mainPlastCodes[p]);

                                        row[5] = wReserves[0];
                                        row[6] = wReserves[1];
                                        row[7] = wReserves[2];
                                        row[8] = wReserves[1] - wReserves[2];

                                        row[9] = str;
                                        row[10] = envCounter;
                                        row[11] = envReserves[0];
                                        row[12] = envReserves[1];
                                        row[13] = envReserves[2];
                                        row[14] = envReserves[1] - envReserves[2];
                                    }
                                    catch(Exception ex)
                                    {
                                        row[4] = "Error : " + ex.Message.Replace("\r", "").Replace("\n", "");
                                    }
                                }
                                if ((double)row[6] != 0) array.Add(row);
                            }
                        }
                    }
                    for (i = 0; i < grids.Count; i++)
                    {
                        if (grids[i].DataLoaded) grids[i].FreeDataMemory();
                    }
                    of.ClearMerData(true);
                }
                row = new object[25];
                row[0] = "НГДУ";
                row[1] = "Месторождение";
                row[2] = "Площадь";
                row[3] = "Скважина";
                row[4] = "Пласт работы";
                row[5] = "НБЗ";
                row[6] = "НИЗ";
                row[7] = "Нак. нефть";
                row[8] = "ОИЗ";
                row[9] = "Скважины окружения";
                row[10] = "Количество скв. окружения";
                row[11] = "НБЗ окружения";
                row[12] = "НИЗ окружения";
                row[13] = "Нак. нефть окружения";
                row[14] = "ОИЗ окружения";

                array.Insert(0, row);
                excel.SetRowsArray(0, 0, array);
            }
            //catch (Exception ex)
            //{
            //    if (excel != null)
            //    {
            //        excel.Quit();
            //    }
            //    throw new Exception("Ошибка выгрузки отчета!", ex);
            //}
            finally
            {
                if (excel != null)
                {
                    excel.EnableEvents = true;
                    excel.Visible = true;
                    excel.Disconnect();
                }
            }
        }
        
        #endregion

        #region Отчет по скважинам с датами
        internal class WellDates
        {
            public Well w;
            public DateTime dt;
            public object[] data;
        }
        public static void ReportWellDates(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета по скважинам с датами";
            userState.WorkCurrentTitle = "Создание отчета по скважинам с датами";
            MSOffice.Excel excel = null;
            bool ShowExcel = false;
            try
            {
                char[] parser = new char[] { ';' };
                string[] fileNames = project.tempStr.Split(parser);
                OilField of = null;
                Well w, w2;
                int ind;
                DateTime dt, startDt;
                List<object[]> heap = new List<object[]>();
                List<object[]> array = new List<object[]>();
                List<object[]> array2 = new List<object[]>();
                object[] row, row2;
                if (fileNames.Length == 1)
                {
                    var dictArea = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                    var dictPlast = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                    var dictGTM = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.GTM_TYPE);
                    var dictCharWork = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.CHARWORK);
                    excel = new MSOffice.Excel();
                    excel.OpenWorkbook(fileNames[0]);

                    List<WellDates> wellPPD = new List<WellDates>();
                    List<WellDates> wellGTM = new List<WellDates>();
                    List<Well> envWells;
                    List<Well> envWellsGTM = new List<Well>();
                    List<Well> envWellsWOGTM = new List<Well>();
                    WellDates wd;
                    string lastOilField = "", lastArea = "";
                    string[] parseStr;
                    List<string> gtmList = new List<string>();
                    List<int> wellIndexes = new List<int>();

                    #region грузим список скважин ппд
                    int i = 0, areaCode = -1;
                    parseStr = new string[4];
                    object[] data;
                    while (excel.GetValue(i, 0).ToString().Length > 0)
                    {
                        if (parseStr.Length > 3)
                        {
                            parseStr[0] = excel.GetValue(i, 0).ToString().Trim().ToUpper();
                            parseStr[1] = excel.GetValue(i, 1).ToString().Trim();
                            parseStr[2] = excel.GetValue(i, 2).ToString().Trim().ToUpper();
                            parseStr[3] = excel.GetValue(i, 3).ToString().Trim();
                            data = new object[3];
                            data[0] = excel.GetValue(i, 4).ToString();
                            data[1] = excel.GetValue(i, 5).ToString();
                            data[2] = excel.GetValue(i, 6).ToString();
                            if (parseStr[0] != lastOilField)
                            {
                                of = null;
                                ind = project.GetOFIndex(parseStr[0]);
                                if (ind != -1)
                                {
                                    of = project.OilFields[ind];
                                    lastOilField = parseStr[0];
                                }
                            }
                            if (parseStr[1] != lastArea)
                            {
                                areaCode = dictArea.GetCodeByShortName(parseStr[1]);
                                lastArea = parseStr[1];
                            }
                            if (of != null)
                            {
                                ind = of.GetWellIndex(parseStr[2], areaCode);
                                if (ind == -1) ind = of.GetWellIndex(parseStr[2]);
                                if (ind != -1)
                                {
                                    wd = new WellDates();
                                    wd.w = of.Wells[ind];
                                    wd.dt = Convert.ToDateTime(parseStr[3]);
                                    wd.data = data;
                                    wellPPD.Add(wd);
                                }
                            }
                        }
                        i++;
                    }
                    excel.CloseWorkbook(false);
                    #endregion

                    excel.NewWorkbook();

                    lastOilField = "";
                    int ofIndex = -1;
                    double[] sumRegim = new double[6];
                    double[][] pressure;
                    double[] regim;
                    string str;
                    int j, ppd;
                    DateTime envDate, lastDate = project.maxMerDate;
                    for (ppd = 0; ppd < wellPPD.Count; ppd++)
                    {
                        w = wellPPD[ppd].w;
                        envWellsGTM.Clear();
                        if (ofIndex != w.OilFieldIndex)
                        {
                            if (of != null) of.ClearMerData(true);
                            of = project.OilFields[w.OilFieldIndex];
                            //if (of.Name != "ИГРОВСКОЕ") continue;
                            if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                            ofIndex = w.OilFieldIndex;
                        }
                        if (w.MerLoaded)
                        {
                            of.LoadPerfFromCache(w.Index);
                            of.LoadGisFromCache(w.Index);

                            row = new object[50];
                            row2 = new object[50];


                            row[0] = of.ParamsDict.NGDUName;
                            row[1] = of.Name;
                            row[2] = dictArea.GetShortNameByCode(w.OilFieldAreaCode);
                            row[3] = w.Name;
                            row[4] = wellPPD[ppd].dt;
                            if (((string)wellPPD[ppd].data[0]).Length > 0) row[5] = Convert.ToSingle((string)wellPPD[ppd].data[0]);
                            if (((string)wellPPD[ppd].data[1]).Length > 0) row[6] = Convert.ToSingle((string)wellPPD[ppd].data[1]);
                            row[7] = (string)wellPPD[ppd].data[2];
                            startDt = new DateTime(wellPPD[ppd].dt.Year, wellPPD[ppd].dt.Month, 1);
                            envDate = startDt.AddMonths(1);



                            List<int> plastList = new List<int>();
                            int charworkID = -1, charworkID2 = -1;
                            for (i = 0; i < w.mer.Count; i++)
                            {
                                if (w.mer.Items[i].Date == startDt.AddMonths(-1))
                                {
                                    charworkID = w.mer.Items[i].CharWorkId;
                                }
                                else if (w.mer.Items[i].Date == startDt)
                                {
                                    for (j = i; (j < w.mer.Count) && (w.mer.Items[j].Date == startDt); j++)
                                    {
                                        if (plastList.IndexOf(w.mer.Items[j].PlastId) == -1)
                                        {
                                            plastList.Add(w.mer.Items[j].PlastId);
                                        }
                                    }
                                }
                                else if (w.mer.Items[i].Date == startDt.AddMonths(1))
                                {
                                    charworkID2 = w.mer.Items[i].CharWorkId;
                                    break;
                                }
                            }
                            if (plastList.Count > 0)
                            {
                                if (w.PerfLoaded && w.GisLoaded)
                                {
                                    List<SkvPerfItem> perfItems = w.perf.GetOpenIntervals(startDt);
                                    List<SkvGisItem> gisItems = w.gis.GetCrossIntervals(perfItems);
                                    //StratumTreeNode node;
                                    double sum = 0;
                                    for (j = 0; j < 2; j++)
                                    {
                                        for (i = 0; i < gisItems.Count; i++)
                                        {
                                            if (j == 1 || (gisItems[i].Sat0 != 14 && gisItems[i].Sat0 != 0))
                                            {
                                                //for (j = 0; j < plastList.Count; j++)
                                                //{
                                                //    node = dictPlast.GetStratumTreeNode(gisItems[i].PlastId);

                                                //    if (gisItems[i].PlastId == plastList[j] || (node != null && node.GetParentLevelByCode(plastList[j]) > -1))
                                                //    {
                                                sum += gisItems[i].L;
                                                //    }
                                                //}
                                            }
                                        }
                                        if (sum > 0) break;
                                    }
                                    row[8] = Math.Round(sum, 2);
                                    if (j == 1) row[9] = "Нет проперф. коллекторов";
                                }
                                str = string.Empty;
                                for (i = 0; i < plastList.Count; i++)
                                {
                                    str += dictPlast.GetShortNameByCode(plastList[i]);
                                    if (i < plastList.Count - 1) str += ",";
                                }
                            }
                            else
                            {
                                str = "-";
                            }
                            row[10] = str;
                            if (charworkID > -1) row[11] = dictCharWork.GetShortNameByCode(charworkID);
                            if (charworkID2 > -1) row[12] = dictCharWork.GetShortNameByCode(charworkID2);

                            for (int h = -3; h < 0; h++)
                            {
                                #region Режим суммарный за h мес до
                                dt = startDt.AddMonths(h);
                                regim = GetRegimByDate(w, dt);
                                row[16 + h] = regim[2];
                                #endregion
                            }
                            for (int h = 1; h < 4; h++)
                            {
                                #region Режим суммарный через h мес после
                                dt = startDt.AddMonths(h);
                                regim = GetRegimByDate(w, dt);
                                row[15 + h] = regim[2];
                                #endregion
                            }

                            bool isRadius1000 = false;
                            envWells = GetEnviromentByDate(project, w, plastList, envDate, 500, 1);
                            if (envWells == null || envWells.Count == 1)
                            {
                                envWells = GetEnviromentByDate(project, w, plastList, envDate, 1000, 1);
                                isRadius1000 = true;
                            }
                            for (i = 0; i < 19; i++) row2[i] = row[i];

                            if ((envWells != null) && (envWells.Count > 0))
                            {
                                row[19] = envWells.Count;
                                row2[19] = envWells.Count;
                                str = string.Empty;
                                wellIndexes.Clear();
                                for (i = 0; i < envWells.Count; i++)
                                {
                                    str += envWells[i].UpperCaseName;
                                    if (i < envWells.Count - 1) str += ", ";
                                    wellIndexes.Add(envWells[i].Index);
                                    if (!envWells[i].GTMLoaded) of.LoadGTMFromCache(envWells[i].Index);
                                }
                                of.LoadWellResearchFromCache(worker, e, wellIndexes);
                                row[20] = str;
                                row[21] = (isRadius1000) ? 1000 : 500;

                                row2[20] = str;
                                row2[21] = (isRadius1000) ? 1000 : 500;

                                bool find;
                                envWellsWOGTM.Clear();
                                envWellsGTM.Clear();
                                for (i = 0; i < envWells.Count; i++)
                                {
                                    w2 = envWells[i];
                                    row = new object[50];
                                    for (j = 0; j < 22; j++)
                                    {
                                        row[j] = row2[j];
                                    }
                                    row[22] = w2.Name;
                                    find = false;
                                    if (w2.GTMLoaded)
                                    {
                                        for (j = 0; j < w2.gtm.Count; j++)
                                        {
                                            if ((wellPPD[ppd].dt.AddMonths(-3) <= w2.gtm[j].Date) && (w2.gtm[j].Date <= wellPPD[ppd].dt.AddMonths(3)))
                                            {
                                                envWellsGTM.Add(w2);
                                                find = true;
                                                gtmList.Add(dictGTM.GetFullNameByCode(w2.gtm[j].GtmCode));
                                                break;
                                            }
                                        }
                                    }
                                    if (!find) envWellsWOGTM.Add(w2);
                                    row[23] = (find) ? "ГТМ" : "База";

                                    for (int h = -3; h < 0; h++)
                                    {
                                        #region Режим суммарный за h мес до
                                        dt = startDt.AddMonths(h);
                                        regim = GetRegimByDate(w2, dt);
                                        row[27 + h] = regim[0];
                                        row[33 + h] = regim[1];
                                        row[39 + h] = (regim[4] == 0) ? 0 : (regim[4] - regim[5]) * 100 / regim[4];
                                        #endregion
                                    }
                                    for (int h = 1; h < 4; h++)
                                    {
                                        #region Режим суммарный через h мес после
                                        dt = startDt.AddMonths(h);
                                        regim = GetRegimByDate(w2, dt);
                                        row[26 + h] = regim[0];
                                        row[32 + h] = regim[1];
                                        row[38 + h] = (regim[4] == 0) ? 0 : (regim[4] - regim[5]) * 100 / regim[4];
                                        #endregion
                                    }
                                    array.Add(row);
                                }

                                //row[27] = envWellsWOGTM.Count;

                                //str = string.Empty;
                                //if (envWellsWOGTM.Count > 0)
                                //{
                                //    for (i = 0; i < envWellsWOGTM.Count; i++)
                                //    {
                                //        str += envWellsWOGTM[i].UpperCaseName;
                                //        if (i < envWellsWOGTM.Count - 1) str += ",";
                                //    }
                                //    //row[28] = str;

                                //    #region // Режим суммарные за 1 месяц до
                                //    //sumRegim[0] = 0; sumRegim[1] = 0; sumRegim[2] = 0; sumRegim[3] = 0; sumRegim[4] = 0; sumRegim[5] = 0;
                                //    //dt = startDt.AddMonths(-1);
                                //    //while (dt < startDt)
                                //    //{
                                //    //    for (i = 0; i < envWellsWOGTM.Count; i++)
                                //    //    {
                                //    //        regim = GetRegimByDate(envWellsWOGTM[i], dt);
                                //    //        sumRegim[0] += regim[0]; sumRegim[1] += regim[1];
                                //    //        sumRegim[2] += regim[2]; sumRegim[3] += regim[3];
                                //    //        sumRegim[4] += regim[4]; sumRegim[5] += regim[5];
                                //    //    }
                                //    //    dt = dt.AddMonths(1);
                                //    //}
                                //    //row[29] = sumRegim[2];
                                //    //row[30] = sumRegim[0];
                                //    //row[31] = sumRegim[1];
                                //    //row[32] = (sumRegim[4] == 0) ? 0 : (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];
                                //    #endregion

                                //    #region // Режим суммарный через 1 мес
                                //    //sumRegim[0] = 0; sumRegim[1] = 0; sumRegim[2] = 0; sumRegim[3] = 0; sumRegim[4] = 0; sumRegim[5] = 0;
                                //    //dt = startDt.AddMonths(1);
                                //    //for (i = 0; i < envWellsWOGTM.Count; i++)
                                //    //{
                                //    //    regim = GetRegimByDate(envWellsWOGTM[i], dt);
                                //    //    sumRegim[0] += regim[0]; sumRegim[1] += regim[1];
                                //    //    sumRegim[2] += regim[2]; sumRegim[3] += regim[3];
                                //    //    sumRegim[4] += regim[4]; sumRegim[5] += regim[5];
                                //    //}
                                //    //row[33] = sumRegim[2];
                                //    //row[34] = sumRegim[0];
                                //    //row[35] = sumRegim[1];
                                //    //row[36] = (sumRegim[4] == 0) ? 0 : (sumRegim[4] - sumRegim[5]) * 100 / sumRegim[4];
                                //    #endregion
                                //}
                                List<Well> wells;
                                for (int k = 0; k < 2; k++)
                                {
                                    wells = (k == 0) ? envWellsWOGTM : envWellsGTM;
                                    str = string.Empty;
                                    for (i = 0; i < wells.Count; i++)
                                    {
                                        str += wells[i].UpperCaseName;
                                        if (i < wells.Count - 1) str += ", ";
                                    }
                                    row = new object[50];
                                    for (j = 0; j < 21; j++) row[j] = row2[j];
                                    row[19] = wells.Count;
                                    row[20] = str;
                                    row[22] = (k == 0) ? "БАЗА" : "ГТМ";
                                    pressure = GetPressureByWells((k == 0) ? envWellsWOGTM : envWellsGTM, dictPlast, plastList, startDt.AddYears(-2), 36, 0);
                                    if (pressure != null)
                                    {
                                        row[23] = (pressure[17][2] > 0) ? pressure[17][1] / pressure[17][2] : 0;
                                        row[24] = (pressure[18][2] > 0) ? pressure[18][1] / pressure[18][2] : 0;
                                        row[25] = (pressure[19][2] > 0) ? pressure[19][1] / pressure[19][2] : 0;
                                        row[26] = (pressure[20][2] > 0) ? pressure[20][1] / pressure[20][2] : 0;
                                        row[27] = (pressure[21][2] > 0) ? pressure[21][1] / pressure[21][2] : 0;
                                        row[28] = (pressure[22][2] > 0) ? pressure[22][1] / pressure[22][2] : 0;
                                        row[29] = (pressure[24][2] > 0) ? pressure[24][1] / pressure[24][2] : 0;
                                        row[30] = (pressure[25][2] > 0) ? pressure[25][1] / pressure[25][2] : 0;
                                        row[31] = (pressure[26][2] > 0) ? pressure[26][1] / pressure[26][2] : 0;
                                        row[32] = (pressure[27][2] > 0) ? pressure[27][1] / pressure[27][2] : 0;
                                        row[33] = (pressure[28][2] > 0) ? pressure[28][1] / pressure[28][2] : 0;
                                        row[34] = (pressure[29][2] > 0) ? pressure[29][1] / pressure[29][2] : 0;
                                    }
                                    array2.Add(row);
                                }

                                for (i = 0; i < envWells.Count; i++)
                                {
                                    if (envWells[i].GTMLoaded)
                                    {
                                        envWells[i].gtm = null;
                                    }
                                }
                            }
                        }
                    }
                    if (array.Count > 0)
                    {
                        row = new object[50];
                        row[0] = "НГДУ";
                        row[1] = "Месторождение";
                        row[2] = "Площадь";
                        row[3] = "Скважина";
                        row[4] = "Дата";
                        row[5] = "Объем кислоты";
                        row[7] = "Доп.мероприятия";
                        row[8] = "Проперф. толщина";
                        row[9] = "Комм.по ГИС";
                        row[10] = "Объекты на дату";
                        row[11] = "Характер работы за месяц до";
                        row[12] = "Характер работы через месяц после";
                        
                        row[13] = "За 3 месяца до даты"; // 13 14 15
                        row[16] = "Через 3 месяца после"; // 16 17 18

                        row[19] = "Количество скважин в округе";
                        row[20] = "Скважины в окружении";
                        row[21] = "R окружения";
                        row[22] = "Скважина";
                        row[23] = "ГТМ/База";

                        row[24] = "Параметры ДО";       // 24 25 26 
                        row[27] = "Параметры ПОСЛЕ";    // 27 28 29
                        row[30] = "Параметры ДО";       // 30 31 32
                        row[33] = "Параметры ПОСЛЕ";    // 33 34 35
                        row[36] = "Параметры ДО";       // 36 37 38
                        row[39] = "Параметры ПОСЛЕ";    // 39 40 41
                        
                        heap.Add(row);
                        row = new object[50];
                        row[5] = "План";
                        row[6] = "Факт";
 
                        
                        row[13] = "W";
                        row[14] = "W";
                        row[15] = "W";
                        row[16] = "W";
                        row[17] = "W";
                        row[18] = "W";
                        
                        
                        row[24] = "Qж";
                        row[25] = "Qж";
                        row[26] = "Qж";
                        row[27] = "Qж";
                        row[28] = "Qж";
                        row[29] = "Qж";
                        
                        
                        row[30] = "Qн";
                        row[31] = "Qн";
                        row[32] = "Qн";
                        row[33] = "Qн";
                        row[34] = "Qн";
                        row[35] = "Qн";
                        
                        
                        row[36] = "%";
                        row[37] = "%";
                        row[38] = "%";
                        row[39] = "%";
                        row[40] = "%";
                        row[41] = "%";

                        heap.Add(row);
                        row = new object[50];
                        row[5] = "м3";
                        row[6] = "м3";

                        row[13] = "м3/сут";
                        row[14] = "м3/сут";
                        row[15] = "м3/сут";
                        row[16] = "м3/сут";
                        row[17] = "м3/сут";
                        row[18] = "м3/сут";
                        
                        
                        row[24] = "т/сут";
                        row[25] = "т/сут";
                        row[26] = "т/сут";
                        row[27] = "т/сут";
                        row[28] = "т/сут";
                        row[29] = "т/сут";

                        
                        row[30] = "т/сут";
                        row[31] = "т/сут";
                        row[32] = "т/сут";
                        row[33] = "т/сут";
                        row[34] = "т/сут";
                        row[35] = "т/сут";

                        
                        row[36] = "%";
                        row[37] = "%";
                        row[38] = "%";
                        row[39] = "%";
                        row[40] = "%";
                        row[41] = "%";

                        heap.Add(row);
                        excel.SetRowsArray(0, 0, heap);
                        excel.SetRowsArray(worker, e, 3, 0, array);

                        heap.Clear();
                        row = new object[50];
                        row[0] = "НГДУ";
                        row[1] = "Месторождение";
                        row[2] = "Площадь";
                        row[3] = "Скважина";
                        row[4] = "Дата";
                        row[5] = "Объем кислоты";
                        row[7] = "Доп.мероприятия";
                        row[8] = "Проперф. толщина";
                        row[9] = "Комм.по ГИС";
                        row[10] = "Объекты на дату";
                        row[11] = "Характер работы за месяц до";
                        row[12] = "Характер работы через месяц после";

                        row[13] = "За 3 месяца до даты"; // 13 14 15
                        row[16] = "Через 3 месяца после"; // 16 17 18

                        row[19] = "Количество скважин в округе";
                        row[20] = "Скважины в окружении";
                        row[21] = "R окружения";
                        row[22] = "ГТМ/БАЗА";
                        row[23] = "Давления по окружению за 6 мес ДО";
                        row[29] = "Давления по окружению за 6 мес ПОСЛЕ";

                        heap.Add(row);
                        row = new object[50];
                        
                        
                        row[13] = "W";
                        row[14] = "W";
                        row[15] = "W";
                        row[16] = "W";
                        row[17] = "W";
                        row[18] = "W";
                        
                        
                        row[23] = "атм";
                        row[24] = "атм";
                        row[25] = "атм";
                        row[26] = "атм";
                        row[27] = "атм";
                        row[28] = "атм";
                        row[29] = "атм";
                        row[30] = "атм";
                        row[31] = "атм";
                        row[32] = "атм";
                        row[33] = "атм";
                        row[34] = "атм";

                        heap.Add(row);
                        excel.SetActiveSheet(1);
                        excel.SetRowsArray(0, 0, heap);
                        excel.SetRowsArray(worker, e, 2, 0, array2);
                    }
                }
                ShowExcel = true;
            }
#if !DEBUG
            catch (Exception ex)
            {
                if (excel != null)
                {
                    excel.Quit();
                    excel = null;
                }
                throw new Exception("Ошибка выгрузки отчета по скважинам и датам", ex);
            }
#endif
            finally
            {
                if (excel != null)
                {
                    if (ShowExcel)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                    }
                    excel.Disconnect();
                }
            }
        }
        #endregion

        #region Отчет по обводненности и выработке
        public static void ReportVoronoiReserves2(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета запасов по областям Вороного";
            userState.WorkCurrentTitle = "Создание отчета запасов по областям Вороного";
            SmartPlus.MSOffice.Excel excel = null;

            //if (!File.Exists(project.tempStr)) return;

            int i, j, ind;
            OilField of;
            Well w;

            try
            {
                var areaDict = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                // Загрузка списка скважин
                List<Well> wells = new List<Well>();
                List<OilField> oilfields = new List<OilField>();
                List<Well> env;
                excel = new SmartPlus.MSOffice.Excel();
                excel.NewWorkbook();
                List<object[]> array = new List<object[]>();

                VoronoiMap vMap = null;
                List<Well> initWells = new List<Well>();
                List<int> wellIndexes = new List<int>();
                List<int> PlastCodes = new List<int>();
                StratumTreeNode node;
                List<Grid> grids = new List<Grid>();
                bool find;
                DateTime initDate = project.maxMerDate;
                initDate = new DateTime(2009, 12, 1);

                for (int ofInd = 0; ofInd < project.OilFields.Count; ofInd++)
                {
                    of = project.OilFields[ofInd];
#if DEBUG
                    //if (ofInd > 10) break;
                    //if (of.Name != "АМИРОВСКОЕ") continue;
#endif
                    try
                    {
                        of.LoadMerFromCache(worker, e, false);
                    }
                    catch
                    { }
                    grids.Clear();

                    wellIndexes.Clear();
                    PlastCodes.Clear();
                    initWells.Clear();

                    for (int wellInd = 0; wellInd < of.Wells.Count; wellInd++)
                    {
                        w = of.Wells[wellInd];

                        userState.Element = string.Format("{0}[{1}]", w.Name, of.Name);
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }

                        env = GetEnviroment(project, w, 500, true);

                        if (env.Count > 0)
                        {
                            wellIndexes.Clear();
                            PlastCodes.Clear();
                            wellIndexes.Add(w.Index);

                            string plasts = string.Empty;
                            object[] row = new object[25];
                            row[0] = of.ParamsDict.NGDUName;
                            row[1] = of.Name;
                            row[2] = areaDict.GetShortNameByCode(w.OilFieldAreaCode);
                            row[3] = w.Name;

                            if (w.MerLoaded)
                            {
                                find = false;
                                for (j = w.mer.Count - 1; j >= 0; j--)
                                {
                                    if (w.mer.Items[j].Date == initDate)
                                    {
                                        find = true;
                                        if (PlastCodes.IndexOf(w.mer.Items[j].PlastId) == -1)
                                        {
                                            PlastCodes.Add(w.mer.Items[j].PlastId);
                                        }
                                    }
                                    else if (find)
                                    {
                                        break;
                                    }
                                }
                                double[] regim = GetRegimByDate(w, initDate);
                                if (regim[8] < 15 || (regim[10] == 0) || (((regim[10] - regim[11]) * 100f / regim[10]) <= 80)) continue;
                                row[12] = initDate.ToShortDateString();
                                row[13] = regim[8];
                                if (regim[10] > 0) row[14] = (regim[10] - regim[11]) * 100f / regim[10];
                            }

                            List<int> mainPlastCodes; List<List<int>> childPlastCodes;
                            FillPlastTree(project, of, PlastCodes, out mainPlastCodes, out childPlastCodes);
                            PlastCodes.Clear();
                            for (i = 0; i < mainPlastCodes.Count; i++)
                            {
                                PlastCodes.Add(mainPlastCodes[i]);
                                PlastCodes.AddRange(childPlastCodes[i].ToArray());
                            }
                            // загружаем сетки
                            if (PlastCodes.Count > 0)
                            {
                                for (i = 0; i < of.Grids.Count; i++)
                                {
                                    if (of.Grids[i].GridType == 2 && (PlastCodes[0] == of.Grids[i].StratumCode))
                                    {
                                        if (grids.IndexOf(of.Grids[i]) == -1) grids.Add(of.Grids[i]);
                                    }
                                }
                                if (grids.Count == 0) // сетки не найдена пробуем найти вышележащий пласт
                                {
                                    node = stratumDict.GetStratumTreeNode(PlastCodes[0]);
                                    if (node != null)
                                    {
                                        for (i = 0; i < of.Grids.Count; i++)
                                        {
                                            if (of.Grids[i].GridType == 2 &&
                                               (node.GetParentLevelByCode(of.Grids[i].StratumCode) > -1) &&
                                               (of.MerStratumCodes.IndexOf(of.Grids[i].StratumCode) == -1))
                                            {
                                                if (grids.IndexOf(of.Grids[i]) == -1) grids.Add(of.Grids[i]);
                                            }
                                        }
                                    }
                                }
                                for (i = 0; i < grids.Count; i++)
                                {
                                    if (!grids[i].DataLoaded) of.LoadGridDataFromCacheByIndex(worker, e, grids[i].Index, false);
                                }
                            }



                            double sumOil = 0;
                            if (w.MerLoaded)
                            {
                                for (i = 0; i < w.mer.Count; i++)
                                {
                                    if (PlastCodes.IndexOf(w.mer.Items[i].PlastId) > -1)
                                    {
                                        sumOil += w.mer.Items[i].Oil;
                                    }
                                    if (w.mer.Items[i].Date > initDate) break;
                                }
                            }

                            row[6] = sumOil;
                            sumOil = 0;
                            for (j = 0; j < env.Count; j++)
                            {
                                if (env[j].MerLoaded)
                                {
                                    for (i = 0; i < env[j].mer.Count; i++)
                                    {
                                        if (PlastCodes.IndexOf(env[j].mer.Items[i].PlastId) > -1)
                                        {
                                            sumOil += env[j].mer.Items[i].Oil;
                                        }
                                        if (env[j].mer.Items[i].Date > initDate) break;
                                    }
                                }
                            }
                            row[9] = sumOil;

                            if (PlastCodes.Count > 0)
                            {
                                try
                                {
                                    vMap = new VoronoiMap();
                                    env.Add(w);
                                    vMap.ComputeVoronoiMap(worker, e, project.maxMerDate, project, env, PlastCodes, 500, true, null);
                                    double[] wReserves = new double[3];
                                    double[] envReserves = new double[3];
                                    string str = string.Empty;
                                    if (vMap.Cells.Length > 0)
                                    {
                                        for (i = 0; i < vMap.Cells.Length; i++)
                                        {
                                            if (vMap.Cells[i].Well.srcWell == w)
                                            {
                                                wReserves[0] = vMap.Cells[i].NBZ;
                                                wReserves[1] = vMap.Cells[i].NIZ;
                                                wReserves[2] = vMap.Cells[i].AccumOil;
                                            }
                                            else if (vMap.Cells[i].Well.srcWell == env[i])
                                            {
                                                envReserves[0] += vMap.Cells[i].NBZ;
                                                envReserves[1] += vMap.Cells[i].NIZ;
                                                envReserves[2] += vMap.Cells[i].AccumOil;
                                                if (str.Length > 0) str += ", ";
                                                str += vMap.Cells[i].Well.srcWell.Name;
                                            }
                                        }
                                    }
                                    row[4] = wReserves[0];
                                    row[5] = wReserves[1];
                                    row[6] = wReserves[2];
                                    row[7] = envReserves[0];
                                    row[8] = envReserves[1];
                                    row[9] = envReserves[2];
                                    row[10] = str;
                                    for (i = 0; i < PlastCodes.Count; i++)
                                    {
                                        if (plasts.Length > 0) plasts += ", ";
                                        plasts += stratumDict.GetShortNameByCode(PlastCodes[i]);
                                    }
                                    row[11] = plasts;
                                    //if (wReserves[2] >= wReserves[1] * 0.8) continue;
                                }
                                catch
                                {
                                    row[4] = "Error";
                                }
                            }
                            array.Add(row);
                        }
                    }
                    for (i = 0; i < grids.Count; i++)
                    {
                        if (grids[i].DataLoaded) grids[i].FreeDataMemory();
                    }
                    of.ClearMerData(true);
                }
                if (array.Count > 0)
                {
                    int r, endRow = -1;
                    object[,] varRow = (object[,])Array.CreateInstance(typeof(object), new int[2] { 50, array[0].Length }, new int[2] { 0, 0 });
                    for (i = 0; i < array.Count; i += 50)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            excel.Quit();
                            return;
                        }
                        for (r = 0; r < 50; r++)
                        {
                            if ((i + r >= array.Count) || (array[i + r] == null))
                            {
                                endRow = i + r - 1;
                                break;
                            }
                            else
                            {
                                for (j = 0; j < array[i].Length; j++)
                                {
                                    varRow[r, j] = array[i + r][j];
                                }
                                endRow = i + 50 - 1;
                            }
                        }
                        excel.SetRange(i + 2, 0, endRow + 2, array[0].Length - 1, varRow);
                        worker.ReportProgress(i, userState);
                    }
                }

            }
            //catch (Exception ex)
            //{
            //    if (excel != null)
            //    {
            //        excel.Quit();
            //    }
            //    throw new Exception("Ошибка выгрузки отчета!", ex);
            //}
            finally
            {
                if (excel != null)
                {
                    excel.EnableEvents = true;
                    excel.Visible = true;
                    excel.Disconnect();
                }
            }
        }
        #endregion

        #region Отчет для Дария
        class WellGTMParamItem
        {
            public int GTMType;
            public DateTime Date;
            public double[] AddLiq;
            public double[] AddOil;
        }
        public static void ReportBaseProduction(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета 'Анализ базовой добычи'";
            userState.WorkCurrentTitle = "Создание отчета 'Анализ базовой добычи'";

            int i, j, k, p, s;
            OilField of = null;
            Well w;
            string ofName = string.Empty, areaName = string.Empty, wellName = string.Empty, gtmName = string.Empty;
            int areaCode = -1, gtmId, count;
            bool error = false;
            int ind, wellIndex = -1;

            OilFieldAreaDictionary dictArea = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            MSOffice.Excel excel = new MSOffice.Excel();
            excel.EnableEvents = false;
            excel.Visible = false;
            object[,] rowLiq, rowOil;
            string formula1, formula2, formula3, formula4, formula5;
            bool ShowExcel = false;
            object[,] range;
            List<Well> wellsGtm = new List<Well>();
            List<List<WellGTMParamItem>> wellGtmParams = new List<List<WellGTMParamItem>>();
            WellGTMParamItem gtmParamItem;
            int book1, book2;
            try
            {
                if (project.tempStr.Length > 0)
                {
                    string[] fileNames = project.tempStr.Split(new char[] { ';' });
                    if (fileNames.Length > 1)
                    {
                        if (File.Exists(fileNames[0]) && File.Exists(fileNames[1]))
                        {
                            book1 = excel.GetWorkbooksCount();
                            excel.OpenWorkbook(fileNames[0]);
                            book2 = excel.GetWorkbooksCount();
                            excel.OpenWorkbook(fileNames[1]);
                            excel.SetActiveWorkbook(book1);
                            excel.SetActiveSheet(0);
                            error = excel.GetActiveSheetName() != "Запуск";
                            excel.SetActiveWorkbook(book2);
                            error = Convert.ToInt32(excel.GetValue(0, 3)) != project.maxMerDate.Year;                            
                            if(error)
                            {
                                excel.CloseWorkbook(false);
                                excel.SetActiveWorkbook(0);
                                excel.CloseWorkbook(false);
                                throw new Exception("Неправильные файлы Excel!");
                            }
                            formula1 = "=СУММ(";
                            formula2 = "=СУММ(";
                            formula3 = "=СУММ(";
                            for (s = 1; s <= 12; s++)
                            {
                                formula1 += string.Format("R[-{0}]C", s * 10 - 1);
                                formula2 += string.Format("R[-{0}]C", s * 10 - 2);
                                formula3 += string.Format("R[-{0}]C", s * 10 - 2);
                                if (s < 12)
                                {
                                    formula1 += ";";
                                    formula2 += ";";
                                    formula3 += ";";
                                }
                            }
                            formula1 += ")";
                            formula2 += ")";
                            formula3 += ")";
                            formula4 = "=R[137]C-R[131]C";
                            formula5 = "=R[138]C-R[131]C";

                            int lastRow = excel.GetLastUsedRow();
                            for (i = 6; i < lastRow; i += 18)
                            {
                                if(excel.GetValue(i, 3).ToString() != ofName)
                                {
                                    if (ofName.Length > 0 && of != null)
                                    {
                                        // выгружаем данные по месторождению
                                        if (wellsGtm.Count > 0)
                                        {
                                            of.LoadSumParamsFromCache(worker, e);
                                        }

                                        excel.SetActiveWorkbook(book1);

                                        ind = -1;
                                        count = excel.GetWorkSheetsCount();
                                        for (j = 0; j < count; j++)
                                        {
                                            excel.SetActiveSheet(j);
                                            if (excel.GetActiveSheetName().StartsWith(of.Name, StringComparison.OrdinalIgnoreCase))
                                            {
                                                ind = j;
                                                break;
                                            }
                                        }
                                        if (ind != -1)
                                        {
                                            excel.RangeCopy(7, 0, 153, 13);
                                            excel.RangePaste(155, 0, 301, 13, MSOffice.Excel.XLPasteType.xlPasteAll, MSOffice.Excel.XLPasteSpecialOperation.None, false, false);
                                            excel.CancelCopy();
                                            range = excel.GetRangeValues(155, 1, 301, 13);
                                            for (j = 1; j <= range.GetLength(0); j++)
                                            {
                                                for (k = 1; k <= range.GetLength(1); k++)
                                                {
                                                    range[j, k] = null;
                                                }
                                            }
                                            excel.SetRange(155, 1, 301, 13, range);
                                            excel.SetFormula(156, 1, "=СУММ(RC[1]:RC[12])");
                                            excel.SetFormula(157, 1, "=СУММ(RC[1]:RC[12])");

                                            excel.SetFormula(286, 1, "=СУММ(RC[1]:RC[12])");
                                            excel.SetFormula(287, 1, "=СУММ(RC[1]:RC[12])");
                                            excel.SetFormula(288, 1, "=СУММ(RC[1]:RC[12])");

                                            excel.SetFormula(293, 1, "=СУММ(RC[1]:RC[12])");
                                            excel.SetFormula(295, 1, "=СУММ(RC[1]:RC[12])");

                                            for (j = 19; j < 139; j += 10)
                                            {
                                                excel.SetFormula(j + 147 + 1, 1, "=СУММ(RC[1]:RC[12])");
                                                excel.SetFormula(j + 147 + 3, 1, "=СУММ(RC[1]:RC[12])");
                                                excel.SetFormula(j + 147 + 4, 1, "=СУММ(RC[1]:RC[12])");
                                                double[] sumLiq = new double[12];
                                                double[] sumOil = new double[12];
                                                int[] sumCount = new int[12];
                                                for (k = 0; k < wellsGtm.Count; k++)
                                                {
                                                    for (p = 0; p < wellGtmParams[k].Count; p++)
                                                    {
                                                        if (wellGtmParams[k][p].GTMType == j)
                                                        {
                                                            for (s = 0; s < 12; s++) sumLiq[s] += wellGtmParams[k][p].AddLiq[s];
                                                            for (s = 0; s < 12; s++) sumOil[s] += wellGtmParams[k][p].AddOil[s];
                                                            sumCount[wellGtmParams[k][p].Date.Month - 1]++;
                                                        }
                                                    }
                                                }
                                                for (s = 0; s < 12; s++)
                                                {
                                                    excel.SetValue(j + 147 + 1, 2 + s, sumCount[s]);
                                                    excel.SetValue(j + 147 + 3, 2 + s, sumOil[s] / 1000);
                                                    excel.SetValue(j + 147 + 4, 2 + s, sumLiq[s] / 1000);
                                                }
                                            }
                                            ind = -1;
                                            if (of.sumParams != null && of.sumParams.Count > 0)
                                            {
                                                for (p = 0; p < of.sumParams[0].MonthlyItems.Length; p++)
                                                {
                                                    if(of.sumParams[0].MonthlyItems[p].Date.Month == 1 && of.sumParams[0].MonthlyItems[p].Date.Year == project.maxMerDate.Year)
                                                    {
                                                        ind = p;
                                                        break;
                                                    }
                                                }
                                            }
                                            for (s = 0; s < 12; s++)
                                            {
                                                excel.SetFormula(286, s + 1, formula1);
                                                excel.SetFormula(287, s + 1, formula2);
                                                excel.SetFormula(288, s + 1, formula3);
                                                excel.SetFormula(156, s + 1, formula4);
                                                excel.SetFormula(157, s + 1, formula5);
                                                if (ind != -1 && ind + s < of.sumParams[0].MonthlyItems.Length)
                                                {
                                                    excel.SetValue(293, s + 2, of.sumParams[0].MonthlyItems[ind + s].Oil / 1000);
                                                    excel.SetValue(295, s + 2, of.sumParams[0].MonthlyItems[ind + s].Liq / 1000);
                                                }
                                                else
                                                {
                                                    excel.SetValue(293, s + 2, 0);
                                                    excel.SetValue(295, s + 2, 0);
                                                }
                                            }
                                        }
                                        of.ClearSumParams(true);
                                        excel.SetActiveWorkbook(book2);
                                    }
                                    ofName = excel.GetValue(i, 3).ToString();
                                    ind = project.GetOFIndex(ofName);
                                    of = null;
                                    if(ind != -1)
                                    {
                                        of = project.OilFields[ind];
                                    }
                                    else
                                    {
                                        System.Windows.Forms.MessageBox.Show("Не найдено месторождение " + ofName);
                                    }
                                    wellsGtm.Clear();
                                    wellGtmParams.Clear();
                                    wellIndex = -1;
                                }


                                areaName = excel.GetValue(i, 4).ToString();
                                areaCode = dictArea.GetCodeByShortName(areaName);
                                wellName = excel.GetValue(i, 5).ToString();
                                ind = (areaCode != -1) ? of.GetWellIndex(wellName.Trim().ToUpper(), areaCode) : of.GetWellIndex(wellName.Trim().ToUpper());
                                if (ind == -1) continue;

                                w = of.Wells[ind];
                                if (wellsGtm.Count == 0 || wellsGtm[wellsGtm.Count - 1] != w)
                                {
                                    wellIndex = wellsGtm.Count;
                                    wellsGtm.Add(w);
                                    wellGtmParams.Add(new List<WellGTMParamItem>());
                                }

                                gtmName = excel.GetValue(i, 7).ToString();
                                
                                #region Перевод  Gtm type
                                gtmId = -1;
                                if (gtmName.StartsWith("Реперф"))
                                {
                                    gtmId = 119;
                                }
                                else if (gtmName.StartsWith("ГРП"))
                                {
                                    gtmId = 29;
                                }
                                else if (gtmName.StartsWith("ОПЗ скв") || gtmName.StartsWith("Нормализация забоя"))
                                {
                                    gtmId = 99;
                                }
                                else if (gtmName.StartsWith("Оптимизация"))
                                {
                                    gtmId = 89;
                                }
                                else if (gtmName.StartsWith("Ввод из бездействия с прошлых лет"))
                                {
                                    gtmId = 79;
                                }
                                else if (gtmName.StartsWith("Переход на другой объект"))
                                {
                                    gtmId = 49;
                                }
                                else if (gtmName.StartsWith("ЛАР"))
                                {
                                    gtmId = 129;
                                }
                                else if (gtmName.StartsWith("РИР"))
                                {
                                    gtmId = 109;
                                }
                                else if (gtmName.StartsWith("Зарезка боковых стволов"))
                                {
                                    gtmId = 39;
                                }
                                else if (gtmName.StartsWith("Ввод новых скважин из бурения"))
                                {
                                    gtmId = 19;
                                }
                                else if (gtmName.StartsWith("Ввод новых скважин из прочих"))
                                {
                                    gtmId = 59;
                                }
                                else if (gtmName.StartsWith("Углубление"))
                                {
                                    gtmId = 69;
                                }
                                else
                                {
                                    gtmId = 1;
                                }
                                #endregion

                                if (wellIndex != -1 && gtmId != -1)
                                {
                                    gtmParamItem = new WellGTMParamItem();
                                    gtmParamItem.GTMType = gtmId;
                                    gtmParamItem.Date = Convert.ToDateTime(excel.GetValue(i, 9));
                                    gtmParamItem.Date = new DateTime(gtmParamItem.Date.Year, gtmParamItem.Date.Month, 1);
                                    gtmParamItem.AddLiq = new double[12];
                                    gtmParamItem.AddOil = new double[12];

                                    rowOil = excel.GetRangeValues(i + 13, 20, i + 13, 31);
                                    rowLiq = excel.GetRangeValues(i + 14, 20, i + 14, 31);
                                    
                                    for (j = 0; j < 12; j++)
                                    {
                                        gtmParamItem.AddLiq[j] = Convert.ToDouble(rowLiq[1, j + 1]);
                                        gtmParamItem.AddOil[j] = Convert.ToDouble(rowOil[1, j + 1]);
                                    }

                                    wellGtmParams[wellIndex].Add(gtmParamItem);
                                }
                                userState.Element = string.Format("{0} [{1}]", wellName, ofName);
                                if (worker != null) worker.ReportProgress((int)(i * 100.0 / lastRow), userState);
                            }
                            ShowExcel = true;
                        }
                    }
                }

            }
#if !DEBUG
            catch (Exception ex)
            {
                if (excel != null)
                {
                    excel.Quit();
                }
                throw new Exception("Ошибка выгрузки отчета 'Анализ заводнения'", ex);
            }
#endif
            finally
            {
                if (excel != null)
                {
                    if (ShowExcel)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                    }
                    excel.Disconnect();
                }
            }
        }
        #endregion

        #region Автоматчинг проницаемости
        internal class GtmMatch
        {
            public int GtmType;
            public int GtmIndex;
            public bool DontUseTpss;
            public int ActionType;
            public DateTime DateGtm, Date;
            public List<int> PlastCodes;
            public SkvGisItem[] GisPlastCodes;

            public PVTParamsItem pvt;
            public double K, Skin, Smin, Smax, Ani;
            public List<WellResearchItem> zabPressure;

            public double Re;
            public double Rc;
            public const double Ct = 0.00022;


            // 0 - Pres 
            // 1 - Pwf
            // 2 - Hэфф
            // 3 - Qliq
            // 4 - QliqV
            // 5 - Qoil
            // 6 - QoilV
            // 7 - %
            // 8 - counter
            // 9 - oil Density
            // 10 - wat Density
            // 11 - Lэфф
            // 12 - liquid viscosity
            // 13 - JD - test

            public double[] Values;
            public List<string> errors;

            public GtmMatch(int gtmType, int actionType, int GtmIndex, DateTime dateGtm, double Smin, double Smax, double StartSkin, double Re, double Rc)
            {
                this.GtmType = gtmType;
                this.PlastCodes = new List<int>();
                this.GisPlastCodes = new SkvGisItem[0];
                this.ActionType = actionType;
                this.GtmIndex = GtmIndex;
                this.DateGtm = dateGtm;
                this.Smin = Smin;
                this.Smax = Smax;
                this.Skin = StartSkin;
                this.Re = Re;
                this.Rc = Rc;
                Values = new double[15];
                Ani = 0.1;                      // анизотропия проницаемости по умолчанию
                errors = new List<string>();
                zabPressure = new List<WellResearchItem>();
                DontUseTpss = false;
                pvt = PVTParamsItem.Empty;
            }

            public int GetDays()
            {
                return (int)Math.Round(Math.Abs((Date - DateGtm).TotalDays));
            }
            public int GetTpss(double K)
            {
                return (K > 0) ? (int)Math.Round(36.84 * pvt.Porosity * Values[12] * Ct * Re * Re / K, 0) : 0;
            }
            double GetPI_Nur(double days, double K, double S)
            {
                double a = Math.Log(4 * Values[2] * days * 0.864 / (1.781 * (double)pvt.Porosity * Values[12] * Ct * Rc * Rc* 100)) + 2 * S;
                return (4 * Math.PI * K * Values[2] * 10e-4 * 0.36 * 24) / (Values[12] * ((double)pvt.OilVolumeFactor * (1 - Values[7]) + 1.01 * Values[7]) * a);
            }
            public double GetJd(bool Horizontal, double S)
            {
                if (Horizontal)
                {
                    double L = Values[11];
                    double h = Values[2];
                    double iani = Math.Sqrt(1 / Ani);
                    double a = (L / 2) * Math.Sqrt(0.5 + Math.Sqrt(0.25 + Math.Pow(Re * 2 / L, 4)));
                    double Pd1 = (a + Math.Sqrt(a * a - (L / 2) * (L / 2))) / (L / 2);
                    double Pd2 = iani * h / L * (Math.Log(iani * h / (0.1 * (iani + 1))) + S);
                    return 1 / (Math.Log(Pd1) + Pd2);
                }
                else
                {
                    return 1 / (Math.Log(Re / Rc) + S - 0.75);
                }
            }
            public double GetPI(bool Horizontal, double K, double S)
            {
                double fw = Values[7], fo = 1 - fw;
                double kh = K * Values[2];
                double visLiq = pvt.OilViscosity * (1 - fw) + fw * pvt.WaterViscosity;
                double Jd = GetJd(Horizontal, S);
                return Jd * kh / (18.41 * visLiq * (pvt.OilVolumeFactor * (1 - fw) + 1.01 * fw));
            }

            public double GetQLiquid(bool Horizontal, double K, double S, double Pwf, bool UsedTpss)
            {
                if (K == 0 || pvt.IsEmpty) return 0;

                double fw = Values[7], fo = 1 - fw;
                double Jd = GetJd(Horizontal, S);
                double PI = GetPI(Horizontal, K, S);
                double Pb = pvt.SaturationPressure, Pres = Values[0], Pcor;

                if (UsedTpss)
                {
                    double PI_Nur = GetPI_Nur(GetDays(), K, S);
                    Pres = (Pwf - Pres * 2 * Math.Log(Re / Rc)) / (1 - 2 * Math.Log(Re / Rc));
                    PI = PI_Nur;
                }
                
                if (Pres < Pb) Pb = Pres;
                double Qlb = PI * (Pres - Pb), Ql, Ql_max;

                if (fw == 1 || Pwf > Pb)
                {
                    Ql = PI * (Pres - Pwf);
                }
                else
                {
                    Ql_max = Qlb + (PI * Pb) / 1.8;
                    Pcor = fw * (Pres - Ql_max / PI);
                    if (Pwf > Pcor)
                    {
                        double a = 1 + (Pwf - (fw * Pres)) / (0.125 * fo * Pb);
                        double b = fw / (0.125 * fo * Pb * PI);
                        double c = (2 * a * b) + 80 / (Ql_max - Qlb);
                        double d = a * a - (80 * Qlb / (Ql_max - Qlb)) - 81;
                        if (b == 0)
                        {
                            Ql = Math.Abs(d / c);
                        }
                        else
                        {
                            Ql = (-c + Math.Sqrt(c * c - 4 * b * b * d)) / (2 * b * b);
                        }
                    }
                    else
                    {
                        double k1 = 0.001 * Ql_max;
                        double k2 = fw * (k1 / PI) + fo * 0.125 * Pb * (-1 + Math.Sqrt(1 + 80 * ((0.001 * Ql_max) / (Ql_max - Qlb))));
                        Ql = (Pcor - Pwf) / (k2 / k1) + Ql_max;
                    }
                }
                return Ql;
            }
            public double GetPermeability(bool Horizontal, double Qliquid, double S, bool UsedTpss)
            {
                double K = 0, h = 1;
                if (Values[0] > Values[1] && Values[2] > 0 && !pvt.IsEmpty)
                {
                    double qLiq = 0;
                    while (Math.Abs(qLiq - Qliquid) > 0.0001)
                    {
                        K += h;
                        qLiq = GetQLiquid(Horizontal, K, S, Values[1], false);
                        if (qLiq < 0) return -1;
                        if (h > 0 && qLiq > Qliquid || h < 0 && qLiq < Qliquid)
                        {
                            h /= -2;
                        }
                    }
                    int days = GetDays(), tpss = GetTpss(K);
                    if (UsedTpss && !DontUseTpss && days < tpss)
                    {
                        qLiq = 0; h = 1;
                        while (Math.Abs(qLiq - Qliquid) > 0.0001)
                        {
                            K += h;
                            qLiq = GetQLiquid(Horizontal, K, S, Values[1], true);
                            if (qLiq < 0) return -1;
                            if (h > 0 && qLiq > Qliquid || h < 0 && qLiq < Qliquid)
                            {
                                h /= -2;
                            }
                        }
                    }
                }
                return K;
            }
            public double GetSkin(bool Horizontal, double Qliquid, double K, bool UsedTpss)
            {
                double S = this.Skin;
                if (S < this.Smin) S = this.Smin; else if (S > Smax) S = this.Smax;

                if (Values[0] > Values[1] && Values[2] > 0 && !pvt.IsEmpty)
                {
                    int days = GetDays(), tpss = GetTpss(K);
                    bool IsUsedTpss = !DontUseTpss && UsedTpss && days < tpss;
                    double qLiq = GetQLiquid(Horizontal, K, S, Values[1], IsUsedTpss);
                    double h = (qLiq > Qliquid) ? h = 0.5 : -0.5;

                    while (Math.Abs(qLiq - Qliquid) > 0.0001 && S > -10)
                    {   
                        S += h;
                        qLiq = GetQLiquid(Horizontal, K, S, Values[1], IsUsedTpss);
                        if (h < 0 && qLiq > Qliquid || h > 0 && qLiq < Qliquid)
                        {
                            h /= -2;
                        }
                    }
                }
                if (S < -10) S = -10;
                return S;
            }
            public double GetPwf(bool Horizontal, double Qliquid, double K, double S, bool UsedTpss)
            {
                double Pwf = Values[1];

                if (Values[0] > Values[1] && Values[2] > 0 && !pvt.IsEmpty)
                {
                    int days = GetDays(), tpss = GetTpss(K);
                    bool IsUsedTpss = !DontUseTpss && UsedTpss && days < tpss;
                    double qLiq = GetQLiquid(Horizontal, K, S, Pwf, IsUsedTpss);
                    double h = (qLiq > Qliquid) ? h = 1 : -1;

                    while (Math.Abs(qLiq - Qliquid) > 0.0001 && Pwf < Values[0] && Pwf > 0)
                    {
                        Pwf += h;
                        qLiq = GetQLiquid(Horizontal, K, S, Pwf, IsUsedTpss);
                        if (h < 0 && qLiq > Qliquid || h > 0 && qLiq < Qliquid)
                        {
                            h /= -2;
                        }
                    }
                }
                return Pwf;
            }
        }

        public static void ReportGtmAutoMatchPermeability(BackgroundWorker worker, DoWorkEventArgs e, Project project, string TemplatePath, DateTime startDate, DateTime endDate, int Radius, List<Dialogs.GTMAutoMatchItem> gtmInitItems, C2DLayer layer, bool calcMultiPlast, bool dontUsePiezometric, double Re, double Rc)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Подбор проницаемости по скважинам ГТМ";
            
            int i, j, k, t, ind, index1, index2;
            bool find, IsWellInjection;
            List<int> oilfields = new List<int>();
            List<List<Well>> wells = new List<List<Well>>();
            OilField of;
            Well w;
            MSOffice.Excel excel;
            List<int> GrpCodes = new List<int>(new int[] { 5000, 5001, 5002 });
            List<int> CarbonCodes = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });

            var oilfieldAreaDict = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            var gtmTypeDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.GTM_TYPE);
            var actionTypeDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.WELL_ACTION_TYPE);
            int progressCounter = 0, wellCount = 0;

            if (layer == null)
            {
                for (i = 1; i < project.OilFields.Count; i++)
                {
                    oilfields.Add(i);
                    wells.Add(new List<Well>());
                    wells[wells.Count - 1].AddRange(project.OilFields[i].Wells);
                    wellCount += project.OilFields[i].Wells.Count + 2;
                }
            }
            else
            {
                for (i = 0; i < layer.ObjectsList.Count; i++)
                {
                    w = ((PhantomWell)layer.ObjectsList[i]).srcWell;
                    ind = oilfields.IndexOf(w.OilFieldIndex);
                    if (ind == -1)
                    {
                        ind = oilfields.Count;
                        oilfields.Add(w.OilFieldIndex);
                        wells.Add(new List<Well>());
                        wellCount += 2;
                    }
                    wells[ind].Add(w);
                    wellCount++;
                }
            }
            List<int> gtmCodes = new List<int>();
            List<int> allGtmCodes = new List<int>();
            for (i = 0; i < gtmInitItems.Count; i++)
            {
                allGtmCodes.AddRange(gtmInitItems[i].GtmCodes);
                if(gtmInitItems[i].Checked)
                {
                    gtmCodes.AddRange(gtmInitItems[i].GtmCodes);
                }
            }
            List<int> wellIndexes = new List<int>();
            WellResearchItem resItem;
            object[] row;
            DateTime dt, dt2;
            ChessItem chessItem;
            SkvPerfItem perfItem;
            List<object[]> errors = new List<object[]>();
            List<object[]> out_list = new List<object[]>();
            int outRowIndex = 2, outColumnIndex = 4;
            int gtmInd;

            if(!File.Exists(TemplatePath)) return;
            userState.WorkCurrentTitle = "Загрузка шаблона в Excel";
            worker.ReportProgress(0, userState);

            excel = new MSOffice.Excel();
            excel.OpenWorkbook(TemplatePath);
            excel.EnableEvents = false;
            excel.Visible = false;
            excel.SetActiveSheet(0);
            excel.SetActiveSheetName("Матчинг ГТМ");
            
            try
            {
                for (int ofInd = 0; ofInd < oilfields.Count; ofInd++)
                {   
                    of = project.OilFields[oilfields[ofInd]];
#if DEBUG
                    //if (of.Name != "АРЛАНСКОЕ") continue;
#endif
                    of.LoadGTMFromCache(worker, e, true);
                    wellIndexes.Clear();
                    for (i = 0; i < wells[ofInd].Count; i++)
                    {
                        w = wells[ofInd][i];
                        if (layer == null)
                        {
                            if (w.GTMLoaded)
                            {
                                for (j = 0; j < w.gtm.Count; j++)
                                {
                                    if (startDate <= w.gtm[j].Date && w.gtm[j].Date <= endDate && gtmCodes.IndexOf(w.gtm[j].GtmCode) > -1)
                                    {
                                        wellIndexes.Add(w.Index);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            wellIndexes.Add(w.Index);
                        }
                    }
                    if (wellIndexes.Count > 0)
                    {
                        // нашли скважины с нужными ГТМ
                        List<int> indexesForLoad = new List<int>(wellIndexes.ToArray());
                        userState.WorkCurrentTitle = "Загрузка данных по скважинам";
                        userState.Element = of.Name;
                        worker.ReportProgress((int)(progressCounter++ * 100f / wellCount), userState);
                        of.LoadWellActionFromCache(worker, e, true);
                        of.LoadMerFromCache(null, null, indexesForLoad);
                        of.LoadWellResearchFromCache(null, null, indexesForLoad);
                        of.LoadChessFromCache(null, null, indexesForLoad);
                        of.LoadGisFromCache(null, null, indexesForLoad);
                        of.LoadPerfFromCache(null, null, indexesForLoad);

                        Area area = null;
                        List<SkvPerfItem> perfItems = new List<SkvPerfItem>();
                        List<SkvGisItem> gisItems = new List<SkvGisItem>();

                        userState.WorkCurrentTitle = "Расчет значений по скважинам";
                        for (int wInd = 0; wInd < wellIndexes.Count; wInd++)
                        {
                            w = of.Wells[wellIndexes[wInd]];

                            userState.Element = string.Format("{0} [{1}]", w.Name, of.Name);
                            worker.ReportProgress((int)(progressCounter++ * 100f / wellCount), userState);

                            if (worker != null && worker.CancellationPending)
                            {
                                return;
                            }
                            //if (layer == null && (!w.GTMLoaded || !w.ResearchLoaded || !w.ChessLoaded || !w.MerLoaded)) continue;


                            bool IsWellHorizontal = w.Name.EndsWith("Г", StringComparison.OrdinalIgnoreCase) && w.Name.Length > 1 && Char.IsDigit(w.Name[w.Name.Length - 2]);

                            IsWellInjection = false;
                            if (!w.ActionLoaded) w.action = new WellAction();

                            // составление списка расчетных точек
                            GtmMatch item, item2;
                            List<GtmMatch> GtmItems = new List<GtmMatch>();
                            outColumnIndex = 4;
                            int gtmIndex = -1;
                            
                            // переносим ремонты ГРП в ГТМ для ХМАО
                            if (of.ParamsDict.NGDUName == "ХМАО" && w.ActionLoaded)
                            {
                                List<int> grpActionCodes = new List<int>(new int[] { 268, 314 });
                                ind = 0;
                                WellGTMItem gtmItem;
                                List<WellGTMItem> gtmItems = new List<WellGTMItem>();
                                if (w.GTMLoaded) gtmItems.AddRange(w.gtm.ToArray());

                                for (i = 0; i < w.action.Count; i++)
                                {
                                    if (grpActionCodes.IndexOf(w.action[i].WellActionCode) > -1)
                                    {
                                        gtmItem.Date = w.action[i].Date;
                                        gtmItem.GtmCode = 5001;

                                        while (ind < gtmItems.Count && w.action[i].Date > gtmItems[ind].Date)
                                        {
                                            ind++;
                                        }

                                        if (ind < gtmItems.Count && w.action[i].Date.Date != gtmItems[ind].Date.Date)
                                        {
                                            gtmItems.Insert(ind, gtmItem);
                                        }
                                        else if (gtmItems.Count == 0 || w.action[i].Date.Date != gtmItems[gtmItems.Count - 1].Date.Date)
                                        {
                                            gtmItems.Add(gtmItem);
                                        }
                                    }
                                }
                                if (w.gtm == null) w.gtm = new WellGTM();
                                if (gtmItems.Count > w.gtm.Count)
                                {
                                    w.gtm.SetItems(gtmItems.ToArray());
                                }
                            }
                            for (i = 0, j = 0; w.GTMLoaded && i < w.gtm.Count; i++)
                            {
                                if (startDate <= w.gtm[i].Date && w.gtm[i].Date <= endDate)
                                {
                                    ind = -1;
                                    for (k = 0; k < gtmInitItems.Count; k++)
                                    {
                                        for (t = 0; t < gtmInitItems[k].GtmCodes.Length; t++)
                                        {
                                            if (gtmInitItems[k].GtmCodes[t] == w.gtm[i].GtmCode)
                                            {
                                                ind = k;
                                                break;
                                            }
                                        }
                                        if (ind != -1) break;
                                    }
                                    if (GtmItems.Count == 0 && ind == -1) continue;

                                    if (GtmItems.Count > 0 && GtmItems[GtmItems.Count - 1].ActionType > 0)
                                    {
                                        index1 = w.chess.GetIndexByDate(GtmItems[GtmItems.Count - 1].DateGtm, 0);
                                        index2 = w.chess.GetIndexByDate(w.gtm[i].Date, index1);
                                        find = false;
                                        for (k = index1; k < index2; k++)
                                        {
                                            if (w.chess[k].StayTime != 24)
                                            {
                                                find = true;
                                                break;
                                            }
                                        }
                                        if (!find) GtmItems.RemoveAt(GtmItems.Count - 1);
                                    }

                                    if (allGtmCodes.IndexOf(w.gtm[i].GtmCode) > -1) gtmIndex = GtmItems.Count;

                                    item = new GtmMatch(w.gtm[i].GtmCode, -1, gtmIndex, w.gtm[i].Date, (ind > -1) ? gtmInitItems[ind].MinSkin : 0, (ind > -1) ? gtmInitItems[ind].MaxSkin : 200, (ind > -1) ? gtmInitItems[ind].StartSkin : 0, Re, Rc);
                                    if (ind == -1 && GtmItems.Count > 0) item.Smin = GtmItems[GtmItems.Count - 1].Smin;

                                    GtmItems.Add(item);

                                    dt = w.gtm[i].Date.AddDays(3);
                                    dt2 = (i < w.gtm.Count - 1) ? w.gtm[i + 1].Date.AddDays(-3) : DateTime.MaxValue;
                                    while (j < w.action.Count && (i == w.gtm.Count - 1 || w.action[j].Date < dt2))
                                    {
                                        if (dt < w.action[j].Date)
                                        {
                                            find = false;
                                            if (w.chess != null)
                                            {
                                                index1 = w.chess.GetIndexByDate(item.DateGtm, 0);
                                                if (index1 == -1) index1 = 0;
                                                index2 = w.chess.GetIndexByDate(w.action[j].Date, index1);
                                                for (k = index1; k < index2; k++)
                                                {
                                                    if (w.chess[k].StayTime != 24)
                                                    {
                                                        find = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (find)
                                            {
                                                item = new GtmMatch(-1, w.action[j].WellActionCode, gtmIndex, w.action[j].Date, 0, 200, 0, Re, Rc);
                                                if (GtmItems.Count > 0) item.Smin = GtmItems[GtmItems.Count - 1].Smin;
                                                GtmItems.Add(item);
                                                dt = w.action[j].Date.AddDays(3);
                                            }
                                        }
                                        j++;
                                    }
                                }
                            }
                            if (GtmItems.Count < 3 && w.chess != null)
                            {
                                DateTime maxChessDate = w.chess[w.chess.Count - 1].Date;
                                for (gtmInd = 0; gtmInd < GtmItems.Count; gtmInd++)
                                {
                                    item = GtmItems[gtmInd];
                                    dt = (gtmInd < GtmItems.Count - 1) ? GtmItems[gtmInd + 1].DateGtm : maxChessDate;
                                    double days = Math.Abs((dt - item.DateGtm).TotalDays);
                                    if (days > 60)
                                    {

                                        item2 = new GtmMatch(-1, 9999, item.GtmIndex, item.DateGtm.AddDays((int)(days / 3)), item.Smin, 200, 0, Re, Rc);
                                        GtmItems.Insert(gtmInd + 1, item2);
                                        gtmInd++;

                                        item2 = new GtmMatch(-1, 9999, item.GtmIndex, item.DateGtm.AddDays((int)(2 * days / 3)), item.Smin, 200, 0, Re, Rc);
                                        GtmItems.Insert(gtmInd + 1, item2);
                                        gtmInd++;
                                    }
                                }
                            }
                            if (GtmItems.Count == 0)
                            {
                                item = new GtmMatch(-1, 9999, 0, of.maxMerDate.AddMonths(-1), 0, 0, 0, Re, Rc);
                                item.errors.Add("Не найден опорный ГТМ в указанный интервал времени");
                                GtmItems.Add(item);
                            }
                            // проверка на работу на 2 пласта
                            ind = -1;
                            List<int> workPlastCodes = new List<int>();
                            int researchIndex = -1;
                            int merIndex = -1, chessIndex = -1;
                            double[][] PressureEnv = null;

                            for (gtmInd = 0; gtmInd < GtmItems.Count; gtmInd++)
                            {
                                item = GtmItems[gtmInd];
                                dt = item.DateGtm.AddMonths(1);
                                if (dt > project.maxMerDate) dt = project.maxMerDate;
                                    
                                ind = (w.MerLoaded) ? w.mer.GetIndex(merIndex + 1, dt) : -1;
                                workPlastCodes.Clear();
                                if (ind != -1)
                                {
                                    for (j = ind; j < w.mer.Count && dt.Year == w.mer.Items[j].Date.Year && dt.Month == w.mer.Items[j].Date.Month; j++)
                                    {
                                        if (w.mer.Items[j].CharWorkId == 20)
                                        {
                                            workPlastCodes.Add(-3);
                                            break;
                                        }
                                        if (workPlastCodes.IndexOf(w.mer.Items[j].PlastId) == -1)
                                        {
                                            workPlastCodes.Add(w.mer.Items[j].PlastId);
                                        }
                                    }
                                    if (workPlastCodes.Count > 1 && !calcMultiPlast) { item.errors.Add("Работа скважины на 2 пласта"); continue; }
                                    if (workPlastCodes.Count == 1 && workPlastCodes[0] == -3) { item.errors.Add("Нагнетательная скважина"); IsWellInjection = true; continue; }
                                }

                                // определяем Hэфф для скважины
                                gisItems.Clear();
                                perfItems.Clear();
                                if (w.GisLoaded && w.PerfLoaded && item.GtmType > 0)
                                {
                                    perfItems = w.perf.GetOpenIntervals(item.DateGtm.AddMonths(1));
                                    gisItems = w.gis.GetCrossIntervals(perfItems);
                                    List<SkvGisItem> gisCollItems = new List<SkvGisItem>();

                                    bool allItervalsNotCollector = true;
                                    for (j = 0; j < gisItems.Count; j++)
                                    {
                                        if (gisItems[j].Collector && gisItems[j].Sat0 == 1)
                                        {
                                            allItervalsNotCollector = false;
                                            break;
                                        }
                                    }
                                    gisCollItems.Clear();
                                    for (j = 0; j < gisItems.Count; j++)
                                    {
                                        if (allItervalsNotCollector || (gisItems[j].Collector && gisItems[j].Sat0 == 1))
                                        {
                                            gisCollItems.Add(gisItems[j]);
                                            item.Values[2] += gisItems[j].Labs; // Hэфф
                                            item.Values[11] += gisItems[j].L;   // Lэфф
                                        }
                                    }
                                    item.GisPlastCodes = gisCollItems.ToArray();
                                    if (!IsWellHorizontal && GrpCodes.IndexOf(item.GtmType) > -1)
                                    {
                                        for (j = 0; j < perfItems.Count; j++)
                                        {
                                            perfItem = perfItems[j];
                                            for (k = 0; k < w.gis.Count; k++)
                                            {
                                                if (w.gis.Items[k].H > perfItem.Bottom) break;
                                                if (w.gis.Items[k].H < perfItem.Top && perfItem.Top < w.gis.Items[k].H + w.gis.Items[k].L)
                                                {
                                                    perfItem.Top = w.gis.Items[k].H;
                                                    perfItems[j] = perfItem;
                                                }
                                                else if (w.gis.Items[k].H < perfItem.Bottom && perfItem.Bottom < w.gis.Items[k].H + w.gis.Items[k].L)
                                                {
                                                    perfItem.Bottom = w.gis.Items[k].H + w.gis.Items[k].L;
                                                    perfItems[j] = perfItem;
                                                }
                                            }
                                            perfItem.Top -= 2;
                                            perfItem.Bottom += 2;
                                            perfItems[j] = perfItem;
                                        }
                                        gisItems = w.gis.GetCrossIntervals(perfItems);
                                        gisCollItems.Clear();
                                        item.Values[2] = 0;
                                        for (j = 0; j < gisItems.Count; j++)
                                        {
                                            if (allItervalsNotCollector || (gisItems[j].Collector && gisItems[j].Sat0 == 1))
                                            {
                                                gisCollItems.Add(gisItems[j]);
                                                item.Values[2] += gisItems[j].Labs; // Hэфф
                                            }
                                        }
                                        item.GisPlastCodes = gisCollItems.ToArray();
                                    }
                                    for (j = gtmInd + 1; j < GtmItems.Count; j++)
                                    {
                                        if (GtmItems[j].GtmType > 0) break;
                                        if (item.GisPlastCodes.Length > 0)
                                        {
                                            gisCollItems.Clear();
                                            gisCollItems.AddRange(item.GisPlastCodes);
                                            GtmItems[j].GisPlastCodes = gisCollItems.ToArray();
                                        }
                                        GtmItems[j].Values[2] = item.Values[2];
                                        GtmItems[j].Values[11] = item.Values[11];
                                    }
                                }

                                if (item.Values[2] == 0) item.errors.Add("Нулевая Hэфф");


                                List<PVTParamsItem> pvtItems = new List<PVTParamsItem>();
                                for (int x = 0; x < workPlastCodes.Count; x++)
                                {
                                    StratumTreeNode plastNode = stratumDict.GetStratumTreeNode(workPlastCodes[x]);
                                    if (plastNode != null)
                                    {
                                        merIndex = ind - 1;
                                        if (merIndex < -1) merIndex = -1;
                                        area = null;
                                        for (j = 0; j < of.Areas.Count; j++)
                                        {
                                            if (workPlastCodes[x] == of.Areas[j].PlastCode || plastNode.GetParentLevelByCode(of.Areas[j].PlastCode) > -1)
                                            {
                                                for (k = 0; k < of.Areas[j].WellList.Length; k++)
                                                {
                                                    if (of.Areas[j].WellList[k] == w)
                                                    {
                                                        area = of.Areas[j];
                                                        break;
                                                    }
                                                }
                                                if (area != null) break;
                                            }
                                        }
                                    }
                                    if (area != null)
                                    {
                                        area.pvt.StratumCode = area.PlastCode;
                                        pvtItems.Add(area.pvt);                                        
                                    }
                                    else
                                    {
                                        item.errors.Add("Не найдена ячейка заводнения для пласта " + stratumDict.GetShortNameByCode(workPlastCodes[x]));
                                        break;
                                    }
                                    if (GtmItems.Count > 0 && GtmItems[0].Ani != 0.001)
                                    {
                                        // проверяем на карбонаты
                                        find = false;
                                        for (j = 0; j < CarbonCodes.Count; j++)
                                        {
                                            if (plastNode.GetParentLevelByCode(CarbonCodes[j]) > -1)
                                            {
                                                find = true;
                                                break;
                                            }
                                        }
                                        if (find)
                                        {
                                            for (j = 0; j < GtmItems.Count; j++)
                                            {
                                                GtmItems[j].Ani = 0.001;
                                            }
                                        }
                                    }
                                }
                                if (pvtItems.Count == 1)
                                {
                                    item.pvt = pvtItems[0];
                                }
                                else if (calcMultiPlast && pvtItems.Count > 1)
                                {
                                    PVTParamsItem[] pvtArray = pvtItems.ToArray();
                                    StratumTreeNode node = null;
                                    double porosity = 0;
                                    double sumPhiPor = 0, allSumPhiPor = 0, sumH = 0, allSumH = 0;
                                    double MaxPb = 0;
                                    item.pvt = PVTParamsItem.Empty;
                                    for (j = 0; j < pvtArray.Length; j++)
                                    {
                                        sumPhiPor = 0;
                                        sumH = 0;
                                        if (pvtArray[j].SaturationPressure > MaxPb) MaxPb = pvtArray[j].SaturationPressure;
                                        for (k = 0; k < item.GisPlastCodes.Length; k++)
                                        {
                                            node = stratumDict.GetStratumTreeNode(item.GisPlastCodes[k].PlastId);
                                            if (node == null)
                                            {
                                                item.errors.Add("При усреднении PVT не найден пласт в справочнике " + stratumDict.GetShortNameByCode(item.GisPlastCodes[k].PlastId));
                                                break;
                                            }
                                            if (node.StratumCode == pvtArray[j].StratumCode || node.GetParentLevelByCode(pvtArray[j].StratumCode) > -1)
                                            {
                                                if (item.GisPlastCodes[k].Kpor >= 1 && item.GisPlastCodes[k].Kpor < 100)
                                                {
                                                    item.GisPlastCodes[k].Kpor /= 100f;
                                                }
                                                else if (item.GisPlastCodes[k].Kpor >= 100)
                                                {
                                                    item.GisPlastCodes[k].Kpor = 0;
                                                }
                                                porosity = (item.GisPlastCodes[k].Kpor <= 0) ? pvtArray[j].Porosity : item.GisPlastCodes[k].Kpor;
                                                sumPhiPor += porosity * item.GisPlastCodes[k].Labs;
                                                sumH += item.GisPlastCodes[k].Labs;
                                            }
                                        }
                                        item.pvt.DisplacementEfficiency += pvtItems[j].DisplacementEfficiency * sumPhiPor;
                                        item.pvt.InitialPressure += pvtItems[j].InitialPressure * sumPhiPor;
                                        item.pvt.KIN += pvtItems[j].KIN * sumPhiPor;
                                        item.pvt.OilDensity += pvtItems[j].OilDensity * sumPhiPor;
                                        item.pvt.OilInitialSaturation += pvtItems[j].OilInitialSaturation * sumPhiPor;
                                        item.pvt.OilViscosity += pvtItems[j].OilViscosity * sumPhiPor;
                                        item.pvt.OilVolumeFactor += pvtItems[j].OilVolumeFactor * sumPhiPor;
                                        item.pvt.Permeability += pvtItems[j].Permeability * sumPhiPor;
                                        item.pvt.Porosity += pvtItems[j].Porosity * sumH;
                                        item.pvt.WaterDensity += pvtItems[j].WaterDensity * sumPhiPor;
                                        item.pvt.WaterViscosity += pvtItems[j].WaterViscosity * sumPhiPor;
                                        item.pvt.WaterVolumeFactor += pvtItems[j].WaterVolumeFactor * sumPhiPor;
                                        allSumPhiPor += sumPhiPor;
                                        allSumH += sumH;
                                        if (node == null) break;
                                    }
                                    item.pvt.SaturationPressure = MaxPb;
                                    if (sumPhiPor > 0)
                                    {
                                        item.pvt.OilFieldAreaCode = w.OilFieldAreaCode;
                                        item.pvt.StratumCode = 0;
                                        item.pvt.DisplacementEfficiency /= allSumPhiPor;
                                        item.pvt.InitialPressure /= allSumPhiPor;
                                        item.pvt.KIN /= allSumPhiPor;
                                        item.pvt.OilDensity /= allSumPhiPor;
                                        item.pvt.OilInitialSaturation /= allSumPhiPor;
                                        item.pvt.OilViscosity /= allSumPhiPor;
                                        item.pvt.OilVolumeFactor /= allSumPhiPor;
                                        item.pvt.Permeability /= allSumPhiPor;
                                        item.pvt.Porosity /= allSumH;
                                        item.pvt.WaterDensity /= allSumPhiPor;
                                        item.pvt.WaterViscosity /= allSumPhiPor;
                                        item.pvt.WaterVolumeFactor /= allSumPhiPor;
                                    }
                                }
                                item.PlastCodes.AddRange(workPlastCodes.ToArray());

                                item.zabPressure.Clear();
                                dt = item.DateGtm.AddDays(4);
                                if (item.GtmType > 0 && allGtmCodes.IndexOf(item.GtmType) > -1 && w.chess != null)
                                {
                                    double sum = 0;
                                    List<ChessItem> chessTestItems = new List<ChessItem>();
                                    for (j = chessIndex + 1; j < w.chess.Count; j++)
                                    {
                                        chessItem = w.chess[j];
                                        if (chessItem.Date < dt) continue;
                                        if (chessItem.Date > dt.AddDays(14)) break;
                                        if (gtmInd < GtmItems.Count - 1 && GtmItems[gtmInd + 1].ActionType != 9999 && chessItem.Date >= GtmItems[gtmInd + 1].DateGtm) break;
                                        if (chessItem.QliqV > 0 && chessItem.StayTime != 24)
                                        {
                                            chessTestItems.Add(chessItem);
                                            sum += chessItem.QliqV;
                                        }
                                    }
                                    if (chessTestItems.Count > 0)
                                    {
                                        sum /= chessTestItems.Count;
                                        int count = 0;
                                        for (j = 0; j < chessTestItems.Count; j++)
                                        {
                                            if (Math.Abs((chessTestItems[j].QliqV - sum) / sum) > 0.15)
                                            {
                                                count++;
                                            }
                                        }
                                        item.DontUseTpss = ((double)count / chessTestItems.Count) < 0.2;
                                    }
                                }
                                DateTime firstQliqDate = DateTime.MaxValue;
                                for (j = chessIndex + 1; w.ChessLoaded && j < w.chess.Count; j++)
                                {
                                    chessItem = w.chess[j];
                                    if (chessItem.Date < dt) continue;
                                    if (gtmInd < GtmItems.Count - 1 && chessItem.Date < GtmItems[gtmInd + 1].DateGtm)
                                    {
                                        chessIndex = j;
                                    }
                                    else if (gtmInd < GtmItems.Count - 1 && GtmItems[gtmInd + 1].ActionType != 9999 && chessItem.Date >= GtmItems[gtmInd + 1].DateGtm)
                                    {
                                        break;
                                    }
                                    if (chessItem.StayTime != 24)
                                    {
                                        firstQliqDate = chessItem.Date;
                                        j--;
                                        break;
                                    }
                                }
                                if (firstQliqDate == DateTime.MaxValue)
                                {
                                    item.errors.Add("Не найдены замеры жидкости по шахматке");
                                    continue;
                                }
                                dt = item.DateGtm.AddYears(-1);
                                for (j = researchIndex + 1; w.ResearchLoaded && j < w.research.Count; j++)
                                {
                                    resItem = w.research[j];

                                    if (workPlastCodes.IndexOf(resItem.PlastCode) == -1 || resItem.Date < dt) continue;
                                    if (gtmInd < GtmItems.Count - 1 && resItem.Date < GtmItems[gtmInd + 1].DateGtm)
                                    {
                                        researchIndex = j;
                                    }
                                    else if (gtmInd < GtmItems.Count - 1 && GtmItems[gtmInd + 1].ActionType != 9999 && resItem.Date >= GtmItems[gtmInd + 1].DateGtm)
                                    {
                                        break;
                                    }

                                    if ((resItem.ResearchCode == 1 || resItem.ResearchCode == 3) && (item.DateGtm <= resItem.Date) && (firstQliqDate <= resItem.Date))
                                    {
                                        if (item.zabPressure.Count > 0)
                                        {
                                            dt = item.zabPressure[item.zabPressure.Count - 1].Date;
                                            if (dt.Year == resItem.Date.Year && dt.Month == resItem.Date.Month && dt.Day == resItem.Date.Day)
                                            {
                                                item.zabPressure[item.zabPressure.Count - 1] = resItem;
                                            }
                                            else
                                            {
                                                item.zabPressure.Add(resItem);
                                            }
                                        }
                                        else
                                        {
                                            item.zabPressure.Add(resItem);
                                        }
                                    }
                                }

                                // ищем по окружению для всех скважин
                                List<Well> enviroment = GetEnviroment(project, w, Radius, false);

                                if (enviroment.Count > 0)
                                {
                                    List<int> envIndexes = new List<int>();
                                    for (k = 0; k < enviroment.Count; k++)
                                    {
                                        envIndexes.Add(enviroment[k].Index);
                                    }
                                    of.LoadWellResearchFromCache(null, null, envIndexes);
                                    of.LoadMerFromCache(null, null, envIndexes);
                                    enviroment.Insert(0, w);
                                    PressureEnv = GetPressureByWells(enviroment, stratumDict, workPlastCodes, item.DateGtm.AddYears(-1), 12, 23, dontUsePiezometric);
                                    for (k = 1; k < enviroment.Count; k++)
                                    {
                                        if (wellIndexes.IndexOf(enviroment[k].Index) == -1)
                                        {
                                            enviroment[k].mer.SetItems(null, null);
                                            enviroment[k].mer = null;
                                        }
                                    }
                                }
                                if (PressureEnv == null || PressureEnv[PressureEnv.Length - 1][2] == 0)
                                {
                                    item.errors.Add("Недостаточно данных по пластовому давлению");
                                    continue;
                                }
                                if (item.zabPressure.Count < 3)
                                {
                                    item.errors.Add("Недостаточно данных по забойному давлению");
                                    continue;
                                }

                                // поиск стабильного участка по Рзаб
                                index1 = -1;
                                for (k = 1; k < item.zabPressure.Count - 1; k++)
                                {
                                    if ((Math.Abs(item.zabPressure[k - 1].PressPerforation - item.zabPressure[k].PressPerforation) / item.zabPressure[k].PressPerforation <= 0.10) &&
                                        (Math.Abs(item.zabPressure[k + 1].PressPerforation - item.zabPressure[k].PressPerforation) / item.zabPressure[k].PressPerforation <= 0.10))
                                    {
                                        index1 = k;
                                        break;
                                    }
                                }

                                if (index1 == -1)
                                {
                                    item.errors.Add("Не найден участок стабильного Рзаб");
                                    continue;
                                }
                                else
                                {
                                    item.Date = item.zabPressure[index1].Date;
                                    item.Values[1] = (item.zabPressure[index1 - 1].PressPerforation + item.zabPressure[index1].PressPerforation + item.zabPressure[index1 + 1].PressPerforation) / 3;
                                    if (gtmInd < GtmItems.Count - 2)
                                    {
                                        int gtmInd2 = gtmInd + 1;
                                        if (GtmItems[gtmInd2].ActionType == 9999 && GtmItems[gtmInd2].DateGtm < item.Date)
                                        {
                                            GtmItems.RemoveAt(gtmInd2);
                                            gtmInd2--;
                                        }
                                        gtmInd2++;
                                        if (GtmItems[gtmInd2].ActionType == 9999 && GtmItems[gtmInd2].DateGtm < item.Date)
                                        {
                                            GtmItems.RemoveAt(gtmInd2);
                                            gtmInd2--;
                                        }
                                    }
                                }

                                if (PressureEnv != null && PressureEnv[PressureEnv.Length - 1][2] > 0)
                                {
                                    item.Values[0] = PressureEnv[PressureEnv.Length - 1][1] / PressureEnv[PressureEnv.Length - 1][2];
                                }

                                if (item.Values[0] < item.Values[1]) item.errors.Add("Рпл меньше Рзаб");
                                double defWatDensity = 0, defOilDensity = 0;
                                for (j = chessIndex + 1; w.ChessLoaded && j < w.chess.Count; j++)
                                {
                                    chessItem = w.chess[j];
                                    if (chessItem.Date < item.DateGtm || chessItem.StayTime == 24) continue;
                                    if (chessItem.Date > item.Date) break;
                                    if (gtmInd < GtmItems.Count - 1 && GtmItems[gtmInd + 1].ActionType != 9999 && chessItem.Date >= GtmItems[gtmInd + 1].DateGtm) break;

                                    if (chessItem.Date <= item.Date)
                                    {
                                        item.Values[3] += w.chess[j].Qliq;
                                        item.Values[4] += w.chess[j].QliqV;
                                        item.Values[5] += w.chess[j].Qoil;
                                        item.Values[6] += w.chess[j].QoilV;
                                        item.Values[8]++;
                                    }
                                    if (w.chess[j].QoilV > 0)
                                    {
                                        defOilDensity = w.chess[j].Qoil / w.chess[j].QoilV;
                                        if (w.chess[j].QliqV - w.chess[j].QoilV > 0)
                                        {
                                            defWatDensity = (w.chess[j].Qliq - w.chess[j].Qoil) / (w.chess[j].QliqV - w.chess[j].QoilV);
                                        }
                                    }
                                }
                                chessIndex = j;
                                if (item.Values[8] > 0)
                                {
                                    item.Values[3] /= item.Values[8];
                                    item.Values[4] /= item.Values[8];
                                    item.Values[5] /= item.Values[8];
                                    item.Values[6] /= item.Values[8];
                                    if (item.Values[6] > 0)
                                    {
                                        item.Values[9] = item.Values[5] / item.Values[6];
                                        if (item.Values[4] - item.Values[6] > 0)
                                        {
                                            item.Values[10] = (item.Values[3] - item.Values[5]) / (item.Values[4] - item.Values[6]);
                                        }
                                    }
                                    if (item.Values[4] > 0)
                                    {
                                        item.Values[7] = (item.Values[4] - item.Values[6]) / item.Values[4];
                                        item.Values[12] = item.pvt.OilViscosity * (1 - item.Values[7]) + item.Values[7] * item.pvt.WaterViscosity;
                                    }
                                }
                                if (item.Values[9] == 0) item.Values[9] = defOilDensity;
                                if (item.Values[10] == 0) item.Values[10] = defWatDensity;
                            }
                            if (!IsWellInjection)
                            {
                                // расставляем gtmIndex
                                gtmIndex = 0;
                                for (j = 0; j < GtmItems.Count; j++)
                                {
                                    if (allGtmCodes.IndexOf(GtmItems[j].GtmType) > -1) gtmIndex = j;
                                    GtmItems[j].GtmIndex = gtmIndex;
                                    if (GtmItems[j].PlastCodes.Count > 0 && (GtmItems[j].Values[9] == 0 || GtmItems[j].Values[10] == 0))
                                    {
                                        for (k = 0; k < GtmItems.Count; k++)
                                        {
                                            if (k != j && GtmItems[j].PlastCodes.Count == GtmItems[k].PlastCodes.Count && GtmItems[k].Values[9] > 0 && GtmItems[k].Values[10] > 0)
                                            {
                                                find = true;
                                                for (int x = 0; x < GtmItems[j].PlastCodes.Count; x++)
                                                {
                                                    if (GtmItems[k].PlastCodes.IndexOf(GtmItems[j].PlastCodes[x]) == -1)
                                                    {
                                                        find = false;
                                                        break;
                                                    }
                                                }
                                                if (find)
                                                {
                                                    GtmItems[j].Values[9] = GtmItems[k].Values[9];
                                                    GtmItems[j].Values[10] = GtmItems[k].Values[10];
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                // Матчинг
                                bool isGtm = false;
                                double Stest, Ktest, Sgtm;
                                double minSkin;
                                for (j = 0; j < GtmItems.Count; j++)
                                {
                                    if (worker != null && worker.CancellationPending)
                                    {
                                        break;
                                    }
                                    item = GtmItems[j];
                                    isGtm = (item.GtmType > 0 && allGtmCodes.IndexOf(item.GtmType) > -1);
                                    if (j > 0 && !isGtm && GtmItems[item.GtmIndex].K == 0) continue;
                                    if (j == 0 || (isGtm && GtmItems[j - 1].K == 0))
                                    {
                                        item.K = item.GetPermeability(IsWellHorizontal, item.Values[4], item.Skin, true);
                                        if (item.K < 0)
                                        {
                                            item.errors.Add("Невозможно рассчитать дебит жидкости");
                                            break;
                                        }
                                        for (k = j + 1; k < GtmItems.Count; k++) GtmItems[k].K = item.K;
                                    }
                                    else
                                    {
                                        item.Skin = item.GetSkin(IsWellHorizontal, item.Values[4], item.K, isGtm);

                                        if (item.Skin < item.Smin || item.Skin > item.Smax)
                                        {
                                            if (j != item.GtmIndex)
                                            {
                                                Stest = (item.Skin < item.Smin) ? item.Smin : item.Smax;
                                                Ktest = item.GetPermeability(IsWellHorizontal, item.Values[4], Stest, isGtm);
                                                if (Ktest < 0)
                                                {
                                                    item.errors.Add("Невозможно рассчитать дебит жидкости");
                                                    break;
                                                }
                                                Sgtm = GtmItems[item.GtmIndex].GetSkin(IsWellHorizontal, GtmItems[item.GtmIndex].Values[4], Ktest, true);
                                                if (Sgtm == -10)
                                                {
                                                    item.errors.Add("Невозможно рассчитать дебит жидкости");
                                                    break;
                                                }
                                                if (Sgtm > Stest) // гонка скинов - корректируем Pwf
                                                {
                                                    double Pwf = item.GetPwf(IsWellHorizontal, item.Values[4], item.K, Stest, isGtm);
                                                    if (Math.Abs((Pwf - item.Values[1]) / item.Values[1]) <= 0.1)
                                                    {
                                                        item.Values[1] = Pwf;
                                                        item.Skin = item.GetSkin(IsWellHorizontal, item.Values[4], item.K, isGtm);
                                                        item.errors.Add("Корректировка Рзаб");
                                                    }
                                                    else
                                                    {
                                                        item.errors.Add("Корр.Рзаб.Невозможно подобрать скин в пределах интервала");
                                                    }
                                                    continue;
                                                }
                                            }

                                            find = false;
                                            Stest = (item.Skin < item.Smin) ? item.Smin : item.Smax;
                                            Ktest = item.GetPermeability(IsWellHorizontal, item.Values[4], Stest, isGtm);
                                            if (Ktest < 0)
                                            {
                                                item.errors.Add("Невозможно рассчитать дебит жидкости");
                                                break;
                                            }
                                            for (k = 0; k < j; k++)
                                            {
                                                Sgtm = GtmItems[k].GetSkin(IsWellHorizontal, GtmItems[k].Values[4], Ktest, GtmItems[k].GtmIndex == k);
                                                if (Sgtm < GtmItems[k].Smin || Sgtm > GtmItems[k].Smax)
                                                {
                                                    item.errors.Add("Невозможно подобрать скин в пределах интервала");
                                                    find = true;
                                                    break;
                                                }
                                            }
                                            if (find) continue;

                                            double h = (item.Skin < item.Smin) ? 0.5 : -0.5;
                                            item.Skin = (item.Skin < item.Smin) ? item.Smin : item.Smax;

                                            item.K = item.GetPermeability(IsWellHorizontal, item.Values[4], item.Skin, isGtm);
                                            if (item.K < 0)
                                            {
                                                item.errors.Add("Невозможно рассчитать дебит жидкости");
                                                break;
                                            }
                                            for (k = 0; k < GtmItems.Count; k++) GtmItems[k].K = item.K;
                                            for (k = 0; k < j; k++)
                                            {
                                                item2 = GtmItems[k];
                                                item2.Skin = item2.GetSkin(IsWellHorizontal, item2.Values[4], item.K, item2.GtmIndex == k);
                                                if (item2.Skin == -10)
                                                {
                                                    item.errors.Add("Невозможно рассчитать дебит жидкости");
                                                    break;
                                                }
                                                if (item2.GtmIndex == k)
                                                {
                                                    minSkin = item2.Skin;
                                                    for (int x = 0; x < k; x++)
                                                    {
                                                        if (x == GtmItems[x].GtmIndex && minSkin > GtmItems[x].Skin)
                                                        {
                                                            minSkin = GtmItems[x].Skin;
                                                        }
                                                    }
                                                    for (int x = k + 1; x < GtmItems.Count && x != GtmItems[x].GtmIndex; x++) GtmItems[x].Smin = minSkin;
                                                    j = k;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (j == GtmItems[j].GtmIndex)
                                    {
                                        minSkin = GtmItems[j].Skin;
                                        for (k = 0; k < j; k++)
                                        {
                                            if (k == GtmItems[k].GtmIndex && minSkin > GtmItems[k].Skin)
                                            {
                                                minSkin = GtmItems[k].Skin;
                                            }
                                        }
                                        for (k = j + 1; k < GtmItems.Count && k != GtmItems[k].GtmIndex; k++) GtmItems[k].Smin = minSkin;
                                    }
                                }
                            }
                            if (!IsWellInjection || layer != null)
                            {
                                if (GtmItems.Count > 0)
                                {
                                    if (outRowIndex > 1)
                                    {
                                        excel.RangeCopy(2, 0, 31, 4);
                                        excel.RangePaste(outRowIndex, 0, outRowIndex + 29, 4, MSOffice.Excel.XLPasteType.xlPasteAll, MSOffice.Excel.XLPasteSpecialOperation.None, false, false);
                                        excel.RangeClear(outRowIndex, 4, outRowIndex + 29, 4);
                                    }
                                    out_list.Clear();
                                    for (j = 0; j < 30; j++)
                                    {
                                        row = new object[3];
                                        row[0] = of.Name;
                                        row[1] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                        row[2] = w.Name;
                                        out_list.Add(row);
                                    }
                                    excel.SetRowsArray(outRowIndex, 0, out_list);
                                }

                                for (gtmInd = 0; gtmInd < GtmItems.Count; gtmInd++)
                                {
                                    item = GtmItems[gtmInd];
                                    if (outColumnIndex > 4)
                                    {
                                        excel.RangeCopy(outRowIndex, 4, outRowIndex + 29, 4);
                                        excel.RangePaste(outRowIndex, outColumnIndex, outRowIndex + 29, outColumnIndex, MSOffice.Excel.XLPasteType.xlPasteAll, MSOffice.Excel.XLPasteSpecialOperation.None, false, false);
                                        excel.RangeClear(outRowIndex, outColumnIndex, outRowIndex + 29, outColumnIndex);
                                    }

                                    // pvt параметры
                                    excel.SetValue(outRowIndex, outColumnIndex, item.Values[9]);
                                    excel.SetValue(outRowIndex + 1, outColumnIndex, item.Values[10]);
                                    excel.SetValue(outRowIndex + 2, outColumnIndex, item.pvt.WaterViscosity);
                                    excel.SetValue(outRowIndex + 3, outColumnIndex, item.pvt.OilViscosity);
                                    excel.SetValue(outRowIndex + 4, outColumnIndex, item.Values[12]);
                                    excel.SetValue(outRowIndex + 5, outColumnIndex, item.pvt.OilVolumeFactor);
                                    excel.SetValue(outRowIndex + 6, outColumnIndex, item.pvt.Porosity);
                                    excel.SetValue(outRowIndex + 7, outColumnIndex, Re);
                                    excel.SetValue(outRowIndex + 8, outColumnIndex, Rc);
                                    excel.SetValue(outRowIndex + 9, outColumnIndex, item.Values[2]);
                                    excel.SetValue(outRowIndex + 10, outColumnIndex, item.pvt.SaturationPressure);
                                    excel.SetValue(outRowIndex + 11, outColumnIndex, IsWellHorizontal ? "Горизонт." : "Верт.");
                                    if (IsWellHorizontal)
                                    {
                                        excel.SetValue(outRowIndex + 12, outColumnIndex, item.Values[11]);
                                        excel.SetValue(outRowIndex + 13, outColumnIndex, item.Ani);
                                    }
                                    if (item.GtmType > -1)
                                    {
                                        excel.SetValue(outRowIndex + 14, outColumnIndex, gtmTypeDict.GetShortNameByCode(item.GtmType));
                                    }
                                    else if (item.ActionType > -1)
                                    {
                                        excel.SetValue(outRowIndex + 14, outColumnIndex, actionTypeDict.GetFullNameByCode(item.ActionType));
                                        excel.SetHorisontalAlignment(outRowIndex + 14, outColumnIndex, outRowIndex + 14, outColumnIndex, MSOffice.Excel.XLConst.xlLeft);
                                    }

                                    excel.SetValue(outRowIndex + 15, outColumnIndex, item.DateGtm);

                                    if (item.Date.Year > 1900)
                                    {
                                        excel.SetValue(outRowIndex + 16, outColumnIndex, item.Date);
                                    }
                                    else
                                    {
                                        excel.SetValue(outRowIndex + 16, outColumnIndex, null);
                                    }

                                    string str = string.Empty;
                                    for (j = 0; j < item.errors.Count; j++)
                                    {
                                        if (j > 0) str += Environment.NewLine;
                                        str += item.errors[j];
                                    }
                                    excel.SetValue(outRowIndex + 17, outColumnIndex, str);
                                    excel.SetValue(outRowIndex + 18, outColumnIndex, string.Format("[{0:0.##};{1:0.##}]", item.Smin, item.Smax));
                                    excel.SetValue(outRowIndex + 19, outColumnIndex, item.K);
                                    excel.SetValue(outRowIndex + 20, outColumnIndex, item.Skin);
                                    if (item.Date.Year > 1) excel.SetValue(outRowIndex + 21, outColumnIndex, item.GetDays());
                                    if (item.GtmType > 0) excel.SetValue(outRowIndex + 22, outColumnIndex, item.GetTpss(item.K));
                                    excel.SetValue(outRowIndex + 23, outColumnIndex, item.Values[0]);
                                    excel.SetValue(outRowIndex + 24, outColumnIndex, item.Values[1]);
                                    excel.SetValue(outRowIndex + 25, outColumnIndex, item.Values[4]);
                                    excel.SetValue(outRowIndex + 26, outColumnIndex, item.Values[5]);
                                    excel.SetValue(outRowIndex + 27, outColumnIndex, item.Values[7]);
                                    excel.SetValue(outRowIndex + 28, outColumnIndex, item.GetJd(IsWellHorizontal, item.Skin));
                                    excel.SetValue(outRowIndex + 29, outColumnIndex, item.GetPI(IsWellHorizontal, item.K, item.Skin));
                                    outColumnIndex++;
                                }
                                outRowIndex += 30;
                            }
                        }
                        of.ClearGisData(false);
                        of.ClearPerfData(false);
                        of.ClearMerData(false);
                        of.ClearChessData(false);
                        of.ClearWellResearchData(false);
                        of.ClearWellActionData(false);
                    }
                    of.ClearGTMData(true);
                }
                worker.ReportProgress(100);
            }
            finally
            {
                excel.EnableEvents = true;
                excel.Visible = true;
                excel.Disconnect();
            }
        }
        #endregion

        #region Деление добычи по объектам ГИС
        public static void ReportProductionSeparateByGIS(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            var litDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LITOLOGY);
            var satDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
            var stateDict = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STATE);
            var oilfieldAreaDict = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);

            StratumTreeNode node;
            SmartPlus.Report.DepositsReporter.WellPerfDateItem dateItem = null;
            List<SmartPlus.Report.DepositsReporter.WellPerfDateItem> dateItems = new List<SmartPlus.Report.DepositsReporter.WellPerfDateItem>();
            OilField of;
            Well w;
            int i, j, ind, ind2;
            bool find;
            MSOffice.Excel excel = null;
            List<object[]> errors = new List<object[]>();
            List<object[]> outList = new List<object[]>();
            List<object[]> outDepositList = new List<object[]>();
            List<object[]> outProdDepositList = new List<object[]>();
            List<object[]> outCoveredIntervalList = new List<object[]>();
            object[] row, err;
            string str;
            excel = new MSOffice.Excel();
            excel.EnableEvents = false;
            excel.Visible = false;
            excel.NewWorkbook();
            double sumOil, notGisIntervalsOil, notInContourOil, sumH;
            List<int> notDistribMerPlasts = new List<int>();
            List<double> notDistribOil = new List<double>();

            List<SmartPlus.Report.DepositsReporter.WellPerfDateItem>[] allDateItems;
            List<int>[] NotInContourPlasts, NotInContourPlastsWithProd;

            List<List<Deposit>> prodDeposits = new List<List<Deposit>>();
            float[] initSquare = null;
            DateTime date1 = project.maxMerDate.AddMonths(-12), date2 = project.maxMerDate.AddMonths(1);
            DateTime datePerf1, datePerf2;
            double AllWellOil = 0;
            bool outAllWellOil = false;
            bool error = false;
            for (int ofInd = 0; ofInd < project.OilFields.Count; ofInd++)
            {
                of = project.OilFields[ofInd];
#if DEBUG
                if (of.Name != "АРЛАНСКОЕ") continue;
#endif
                // заполняем перфорацию по скважинам
                of.LoadGisFromCache(worker, e);
                of.LoadPerfFromCache(worker, e);
                of.LoadMerFromCache(worker, e, false);

                initSquare = new float[of.Deposits.Count];

                allDateItems = new List<SmartPlus.Report.DepositsReporter.WellPerfDateItem>[of.Wells.Count];
                NotInContourPlasts = new List<int>[of.Wells.Count];
                NotInContourPlastsWithProd = new List<int>[of.Wells.Count];

                for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                {
                    w = of.Wells[wInd];
                    if (w.Missing) continue;

                    error = false;
                    dateItems.Clear();
                    AllWellOil = 0;
                    outAllWellOil = false;
                    for (int k = 0; k < w.mer.Count; k++)
                    {
                        AllWellOil += w.mer.Items[k].Oil;
                    }
                    if (AllWellOil == 0) continue;

                    if (!w.GisLoaded)
                    {
                        err = new object[8];
                        err[0] = of.ParamsDict.NGDUName;
                        err[1] = of.Name;
                        err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                        err[4] = w.Name;
                        err[5] = "ГИС";
                        err[6] = (!outAllWellOil) ? AllWellOil : 0;
                        err[7] = "Не загружены гис по скважине";
                        if (AllWellOil > 0 && (!DepositsReporter.IndexOfArray(errors, err))) errors.Add(err);
                        outAllWellOil = true;
                    }

                    dateItems = new List<SmartPlus.Report.DepositsReporter.WellPerfDateItem>();
                    if (w.GisLoaded && w.PerfLoaded)
                    {
                        for (i = 0; i < w.gis.Count; i++)
                        {
                            if ((w.gis.Items[i].Habs <= 0 || w.gis.Items[i].Labs <= 0) && w.gis.Items[i].Collector && w.gis.Items[i].Sat0 == 1)
                            {
                                err = new object[8];
                                err[0] = of.ParamsDict.NGDUName;
                                err[1] = of.Name;
                                err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                err[4] = w.Name;
                                err[5] = "ГИС";
                                err[6] = string.Format("'[{0:0.##};{1:0.##}]", w.gis.Items[i].Habs, w.gis.Items[i].Labs);
                                err[7] = "Нулевые или отрицательные Abs отметки ГИС";
                                if (!DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                            }
                        }
                        dateItem = new SmartPlus.Report.DepositsReporter.WellPerfDateItem();
                        for (i = 0; i < w.perf.Count; i++)
                        {
                            if (w.perf.Items[i].Top >= w.perf.Items[i].Bottom)
                            {
                                err = new object[8];
                                err[0] = of.ParamsDict.NGDUName;
                                err[1] = of.Name;
                                err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                err[4] = w.Name;
                                err[5] = "Перфорация";
                                err[6] = string.Format("'[{0:0.##};{1:0.##}]", w.perf.Items[i].Top, w.perf.Items[i].Bottom);
                                err[7] = "Неверные отметки глубин перфорации";
                                if (!DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                            }

                            if (dateItem.dateStart == DateTime.MinValue || (dateItem.dateStart.Year == w.perf.Items[i].Date.Year && dateItem.dateStart.Month == w.perf.Items[i].Date.Month))
                            {
                                dateItem.dateStart = w.perf.Items[i].Date;
                            }
                            else
                            {
                                if (dateItems.Count > 0)
                                {
                                    dateItems[dateItems.Count - 1].dateEnd = dateItem.dateStart;
                                }
                                dateItems.Add(dateItem);
                                dateItem = new SmartPlus.Report.DepositsReporter.WellPerfDateItem();
                                dateItem.dateStart = w.perf.Items[i].Date;
                                if (dateItems.Count > 0) dateItem.perf.AddRange(dateItems[dateItems.Count - 1].perf.ToArray());
                            }
                            if (w.perf.Items[i].PerfTypeId != 10000)
                            {
                                dateItem.perf.Add(w.perf.Items[i]);
                            }
                            else
                            {
                                List<SkvPerfItem> perfs = new List<SkvPerfItem>();
                                for (int k = 0; k < dateItem.perf.Count; k++)
                                {
                                    if (SkvPerf.IntersectIntervals(dateItem.perf[k], w.perf.Items[i]))
                                    {
                                        if (dateItems.Count > 0) dateItems[dateItems.Count - 1].ClosingIntervals = true;
                                        SkvPerfItem perfItem = dateItem.perf[k];
                                        dateItem.perf.RemoveAt(k);
                                        perfs.AddRange(SkvPerf.DiffIntervals(perfItem, w.perf.Items[i]).ToArray());
                                        k--;
                                    }
                                }
                                dateItem.perf.AddRange(perfs.ToArray());
                            }
                        }
                        if (dateItem.dateStart.Year > 1900)
                        {
                            if (dateItems.Count > 0)
                            {
                                dateItems[dateItems.Count - 1].dateEnd = dateItem.dateStart;
                            }
                            dateItems.Add(dateItem);
                        }
                        for (i = 0; i < dateItems.Count; i++)
                        {
                            if (dateItems[i].ClosingIntervals)
                            {
                                dateItem = new SmartPlus.Report.DepositsReporter.WellPerfDateItem();
                                dateItem.dateStart = dateItems[i].dateEnd;
                                dateItem.dateEnd = dateItems[i].dateEnd.AddMonths(1);
                                dateItem.dateEnd = new DateTime(dateItem.dateEnd.Year, dateItem.dateEnd.Month, 1);

                                dateItem.perf.AddRange(dateItems[i].perf.ToArray());
                                if (i < dateItems.Count - 1)
                                {
                                    dateItems[i + 1].dateStart = dateItem.dateEnd;
                                    dateItem.perf.AddRange(dateItems[i + 1].perf.ToArray());
                                }
                                dateItems.Insert(i + 1, dateItem);
                                i++;
                            }
                        }
                        for (i = 0; i < dateItems.Count; i++)
                        {
                            datePerf1 = new DateTime(dateItems[i].dateStart.Year, dateItems[i].dateStart.Month, 1);
                            datePerf2 = new DateTime(dateItems[i].dateEnd.Year, dateItems[i].dateEnd.Month, 1);

                            for (int k = 0; k < w.mer.Count; k++)
                            {
                                if (datePerf1 <= w.mer.Items[k].Date && w.mer.Items[k].Date < datePerf2)
                                {
                                    if (w.mer.Items[k].Oil > 0 && dateItems[i].merPlasts.IndexOf(w.mer.Items[k].PlastId) == -1)
                                    {
                                        dateItems[i].merPlasts.Add(w.mer.Items[k].PlastId);
                                    }

                                    if (datePerf1 < w.mer.Items[k].Date)
                                    {
                                        ind = dateItems[i].merCharWorks.IndexOf(w.mer.Items[k].CharWorkId);
                                        if (ind == -1)
                                        {
                                            ind = dateItems[i].merCharWorks.Count;
                                            dateItems[i].merCharWorks.Add(w.mer.Items[k].CharWorkId);
                                            dateItems[i].merWorkTimes.Add(0);
                                        }
                                        dateItems[i].merWorkTimes[ind] += w.mer.Items[k].WorkTime;
                                    }
                                }
                                else if (w.mer.Items[k].Date >= datePerf2)
                                {
                                    break;
                                }
                            }
                            List<SkvGisItem> gisList = w.gis.GetCrossIntervals(dateItems[i].perf);
                            dateItems[i].gisProd = new List<SmartPlus.Report.DepositsReporter.WellGisProdItem>();
                            find = false;
                            for (j = 0; j < gisList.Count; j++)
                            {
                                if (gisList[j].Collector && gisList[j].Sat0 == 1)
                                {
                                    find = true;
                                    break;
                                }
                            }
                            for (j = 0; j < gisList.Count; j++)
                            {
                                if (!gisList[j].Collector)
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                    err[3] = stratumDict.GetShortNameByCode(gisList[j].PlastId);
                                    err[4] = w.Name;
                                    err[5] = "ГИС";
                                    err[6] = string.Format("'[{0:0.##};{1:0.##}]", gisList[j].Habs, gisList[j].Habs + gisList[j].Labs) + "";
                                    err[7] = "Проперфорирован не коллектор";
                                    if (!DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                                    if (find) continue;
                                }
                                if (gisList[j].Sat0 != 1)
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                    err[3] = stratumDict.GetShortNameByCode(gisList[j].PlastId);
                                    err[4] = w.Name;
                                    err[5] = "ГИС";
                                    ind = satDict.GetIndexByCode(gisList[j].Sat0);
                                    if (ind > -1) err[6] = satDict[ind].FullName;
                                    err[7] = "Проперфорирован не нефтенасыщенный интервал";
                                    if (!DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                                    if (find) continue;
                                }
                                var gisProdItem = new SmartPlus.Report.DepositsReporter.WellGisProdItem();
                                gisProdItem.gis = gisList[j];
                                //gisProdItem.InContour = NotInContourPlasts[w.Index].IndexOf(gisProdItem.gis.PlastId) == -1;
                                dateItems[i].gisProd.Add(gisProdItem);
                            }
                            sumH = 0;
                            List<int> indexes = new List<int>();
                            for (int k = 0; k < dateItems[i].merPlasts.Count; k++)
                            {
                                indexes.Clear();
                                sumH = 0;
                                for (j = 0; j < dateItems[i].gisProd.Count; j++)
                                {
                                    node = stratumDict.GetStratumTreeNode(dateItems[i].gisProd[j].gis.PlastId);
                                    if (node == null) continue;

                                    if (node.StratumCode == dateItems[i].merPlasts[k] || node.GetParentLevelByCode(dateItems[i].merPlasts[k]) > -1 ||
                                        (of.OilFieldCode == 109 && node.StratumCode == 48 && dateItems[i].merPlasts[k] == 39) ||
                                        (of.OilFieldCode == 69 && node.StratumCode == 48 && dateItems[i].merPlasts[k] == 38)) // crutch
                                    {
                                        dateItems[i].gisProd[j].plasts.Add(dateItems[i].merPlasts[k]);
                                        dateItems[i].gisProd[j].K.Add(0);
                                        sumH += dateItems[i].gisProd[j].gis.Labs;
                                        indexes.Add(j);
                                    }
                                }
                                for (j = 0; j < dateItems[i].gisProd.Count; j++)
                                {
                                    node = stratumDict.GetStratumTreeNode(dateItems[i].gisProd[j].gis.PlastId);
                                    if (node == null) continue;

                                    if (node.StratumCode == dateItems[i].merPlasts[k] || node.GetParentLevelByCode(dateItems[i].merPlasts[k]) > -1 ||
                                        (of.OilFieldCode == 109 && node.StratumCode == 48 && dateItems[i].merPlasts[k] == 39) ||
                                        (of.OilFieldCode == 69 && node.StratumCode == 48 && dateItems[i].merPlasts[k] == 38)) // crutch
                                    {
                                        ind = dateItems[i].gisProd[j].plasts.IndexOf(dateItems[i].merPlasts[k]);
                                        if (ind != -1)
                                        {
                                            if (sumH > 0)
                                            {
                                                dateItems[i].gisProd[j].K[ind] = dateItems[i].gisProd[j].gis.Labs / sumH;
                                            }
                                            else
                                            {
                                                dateItems[i].gisProd[j].K[ind] = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // суммируем добычу
                        if (dateItems.Count > 0)
                        {
                            j = 0;

                            datePerf1 = new DateTime(dateItems[j].dateStart.Year, dateItems[j].dateStart.Month, 1);
                            datePerf2 = new DateTime(dateItems[j].dateEnd.Year, dateItems[j].dateEnd.Month, 1);
                            notGisIntervalsOil = 0;
                            notInContourOil = 0;
                            notDistribMerPlasts.Clear();
                            notDistribOil.Clear();

                            for (i = 0; i < w.mer.Count; i++)
                            {
                                if (w.mer.Items[i].Oil > 0 && w.mer.Items[i].Date < datePerf1)
                                {
                                    if ((dateItems[j].dateStart - w.mer.Items[i].Date).TotalDays > 20)
                                    {
                                        sumOil = 0;
                                        while (i < w.mer.Count && w.mer.Items[i].Date < datePerf1)
                                        {
                                            sumOil += w.mer.Items[i].Oil;
                                            i++;
                                        }
                                        err = new object[8];
                                        err[0] = of.ParamsDict.NGDUName;
                                        err[1] = of.Name;
                                        err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                        err[4] = w.Name;
                                        err[5] = "МЭР";
                                        err[6] = sumOil;
                                        err[7] = "Дата добычи по МЭР более ранняя чем начальная дата перфорации";
                                        errors.Add(err);
                                        error = true;
                                        if (i >= w.mer.Count) break;
                                    }
                                    else
                                    {
                                        datePerf1 = w.mer.Items[i].Date;
                                    }
                                }
                                if (j < dateItems.Count && datePerf1 <= w.mer.Items[i].Date && w.mer.Items[i].Date < datePerf2)
                                {
                                    find = false;

                                    for (int k = 0; k < dateItems[j].gisProd.Count; k++)
                                    {
                                        ind = dateItems[j].gisProd[k].plasts.IndexOf(w.mer.Items[i].PlastId);

                                        if (ind > -1 && dateItems[j].gisProd[k].K[ind] > 0)
                                        {
                                            //if (!dateItems[j].gisProd[k].InContour)
                                            //{
                                            //    if (NotInContourPlastsWithProd[w.Index].IndexOf(dateItems[j].gisProd[k].gis.PlastId) == -1)
                                            //    {
                                            //        NotInContourPlastsWithProd[w.Index].Add(dateItems[j].gisProd[k].gis.PlastId);
                                            //    }
                                            //    notInContourOil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                            //}
                                            if (w.mer.Items[i].CharWorkId == 11 || w.mer.Items[i].CharWorkId == 15)
                                            {
                                                if (w.mer.Items[i].Oil > 0)
                                                {
                                                    ind2 = -1;
                                                    for (int x = 0; x < j; x++)
                                                    {
                                                        for (int t = 0; t < dateItems[x].gisProd.Count; t++)
                                                        {
                                                            if (dateItems[x].gisProd[t].gis.PlastId == dateItems[j].gisProd[k].gis.PlastId && dateItems[x].gisProd[t].startDate != DateTime.MinValue)
                                                            {
                                                                ind2 = t;
                                                                break;
                                                            }
                                                        }
                                                        if (ind2 != -1) break;
                                                    }
                                                    if (ind2 == -1 && (dateItems[j].gisProd[k].startDate == DateTime.MinValue || dateItems[j].gisProd[k].startDate == w.mer.Items[i].Date))
                                                    {
                                                        dateItems[j].gisProd[k].startDate = w.mer.Items[i].Date;
                                                        dateItems[j].gisProd[k].startQliq += dateItems[j].gisProd[k].K[ind] * (w.mer.Items[i].Oil + w.mer.Items[i].Wat);
                                                        dateItems[j].gisProd[k].startQoil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                                        dateItems[j].gisProd[k].startWorkTime += w.mer.Items[i].WorkTime + w.mer.Items[i].CollTime;
                                                    }
                                                    if (w.mer.Items[i].Date == project.maxMerDate)
                                                    {
                                                        dateItems[j].gisProd[k].currentQliq += dateItems[j].gisProd[k].K[ind] * (w.mer.Items[i].Oil + w.mer.Items[i].Wat);
                                                        dateItems[j].gisProd[k].currentQoil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                                        dateItems[j].gisProd[k].currentWorkTime += w.mer.Items[i].WorkTime + w.mer.Items[i].CollTime;
                                                    }
                                                }
                                            }

                                            if (w.mer.Items[i].Date >= date1 && w.mer.Items[i].Date < date2)
                                            {
                                                dateItems[j].gisProd[k].lastYearOil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                                if (w.mer.Items[i].CharWorkId == 11 || w.mer.Items[i].CharWorkId == 15)
                                                {
                                                    dateItems[j].gisProd[k].lastYearLiq += dateItems[j].gisProd[k].K[ind] * (w.mer.Items[i].Oil + w.mer.Items[i].Wat);
                                                }
                                            }
                                            dateItems[j].gisProd[k].accumOil += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                            dateItems[j].gisProd[k].accumWater += dateItems[j].gisProd[k].K[ind] * w.mer.Items[i].Wat;
                                            find = true;
                                        }
                                    }

                                    if (!find && w.mer.Items[i].Oil > 0)
                                    {
                                        //if (j > 0 && w.mer.Items[i].Date.Year == dateItems[j - 1].dateEnd.Year && w.mer.Items[i].Date.Month == dateItems[j - 1].dateEnd.Month)
                                        //{
                                        //    find = false;
                                        //    for (int k = 0; k < dateItems[j - 1].gisProd.Count; k++)
                                        //    {
                                        //        ind = dateItems[j - 1].gisProd[k].plasts.IndexOf(w.mer.Items[i].PlastId);
                                        //        if (ind > -1 && dateItems[j - 1].gisProd[k].K[ind] > 0)
                                        //        {
                                        //            if (!dateItems[j - 1].gisProd[k].InContour)
                                        //            {
                                        //                notInContourOil += dateItems[j - 1].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                        //            }
                                        //            if (w.mer.Items[i].Date >= date1 && w.mer.Items[i].Date < date2)
                                        //            {
                                        //                dateItems[j - 1].gisProd[k].lastYearOil += dateItems[j - 1].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                        //                if (w.mer.Items[i].CharWorkId != 20)
                                        //                {
                                        //                    dateItems[j - 1].gisProd[k].lastYearLiq += dateItems[j - 1].gisProd[k].K[ind] * (w.mer.Items[i].Oil + w.mer.Items[i].Wat);
                                        //                }
                                        //            }
                                        //            dateItems[j - 1].gisProd[k].accumOil += dateItems[j - 1].gisProd[k].K[ind] * w.mer.Items[i].Oil;
                                        //            dateItems[j - 1].gisProd[k].accumWater += dateItems[j - 1].gisProd[k].K[ind] * w.mer.Items[i].Wat;
                                        //            find = true;
                                        //        }
                                        //    }
                                        //}
                                        if (!find)
                                        {
                                            ind = notDistribMerPlasts.IndexOf(w.mer.Items[i].PlastId);
                                            if (ind == -1)
                                            {
                                                notDistribMerPlasts.Add(w.mer.Items[i].PlastId);
                                                notDistribOil.Add(0);
                                                ind = notDistribMerPlasts.Count - 1;
                                            }
                                            notDistribOil[ind] += w.mer.Items[i].Oil;
                                        }
                                    }
                                }
                                else if (j >= dateItems.Count && w.mer.Items[i].Oil > 0)
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                    err[4] = w.Name;
                                    err[5] = "МЭР";
                                    err[7] = "Для добычи по МЭР нет перфорации";
                                    error = true;
                                    if (!DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                                }
                                if (w.mer.Items[i].Date >= datePerf2)
                                {
                                    i--;
                                    j++;

                                    if (j < dateItems.Count)
                                    {
                                        datePerf1 = new DateTime(dateItems[j].dateStart.Year, dateItems[j].dateStart.Month, 1);
                                        datePerf2 = new DateTime(dateItems[j].dateEnd.Year, dateItems[j].dateEnd.Month, 1);
                                    }
                                }
                            }

                            if (notGisIntervalsOil > 0)
                            {
                                err = new object[8];
                                err[0] = of.ParamsDict.NGDUName;
                                err[1] = of.Name;
                                err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                err[4] = w.Name;
                                err[5] = "ГИС";
                                err[7] = "На интервалы перфорации не найдены интервалы ГИС";
                                if (!DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                            }

                            if (notInContourOil > 0)
                            {
                                err = new object[8];
                                err[0] = of.ParamsDict.NGDUName;
                                err[1] = of.Name;
                                err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                str = string.Empty;
                                for (int k = 0; k < NotInContourPlastsWithProd[w.Index].Count; k++)
                                {
                                    if (str.Length > 0) str += ", ";
                                    str += stratumDict.GetShortNameByCode(NotInContourPlastsWithProd[w.Index][k]);
                                }
                                err[3] = str;
                                err[4] = w.Name;
                                err[5] = "ГИС";
                                err[6] = notInContourOil;
                                err[7] = "На интервалы ГИС c добычей не найдены залежи";
                                if (!DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                            }
                            if (notDistribMerPlasts.Count > 0)
                            {
                                for (int k = 0; k < notDistribMerPlasts.Count; k++)
                                {
                                    err = new object[8];
                                    err[0] = of.ParamsDict.NGDUName;
                                    err[1] = of.Name;
                                    err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                    err[3] = stratumDict.GetShortNameByCode(notDistribMerPlasts[k]);
                                    err[4] = w.Name;
                                    err[5] = "МЭР";
                                    err[6] = notDistribOil[k];
                                    err[7] = "Добыча по МЭР не имеет проперфорированных интервалов";
                                    error = true;
                                    errors.Add(err);
                                }
                            }
                        }
                        for (i = 0; i < dateItems.Count; i++)
                        {
                            ind = -1;
                            for (int k = 0; k < dateItems[i].merCharWorks.Count; k++)
                            {
                                if (dateItems[i].merCharWorks[k] != 20 && dateItems[i].merWorkTimes[k] > 0)
                                {
                                    ind = k;
                                    break;
                                }
                            }
                            if (ind > -1)
                            {
                                for (int k = 0; k < dateItems[i].gisProd.Count; k++)
                                {
                                    if (dateItems[i].gisProd[k].accumOil == 0 && dateItems[i].gisProd[k].accumWater == 0 && dateItems[i].gisProd[k].gis.Labs > 0)
                                    {
                                        err = new object[8];
                                        err[0] = of.ParamsDict.NGDUName;
                                        err[1] = of.Name;
                                        err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                                        err[3] = stratumDict.GetShortNameByCode(dateItems[i].gisProd[k].gis.PlastId);
                                        err[4] = w.Name;
                                        err[5] = "ПЕРФ";
                                        err[6] = string.Format("[{0:dd.MM.yyyy};{1:dd.MM.yyyy}]", dateItems[i].dateStart, (dateItems[i].dateEnd.Year > DateTime.Now.Year ? DateTime.Now : dateItems[i].dateEnd));
                                        err[7] = "Для перфорированного интервала нет добычи по МЭР";
                                        if (!DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                                    }
                                }
                            }
                        }
                    }
                    if (dateItems.Count > 0)
                    {
                        SmartPlus.Report.DepositsReporter.WellPerfDateItem sumItem = new DepositsReporter.WellPerfDateItem();
                        SmartPlus.Report.DepositsReporter.WellGisProdItem item;
                        for (i = 0; i < dateItems.Count; i++)
                        {
                            for (j = 0; j < dateItems[i].gisProd.Count; j++)
                            {
                                ind = -1;
                                for(int k = 0; k < sumItem.gisProd.Count;k++)
                                {
                                    if (dateItems[i].gisProd[j].gis.PlastId == sumItem.gisProd[k].gis.PlastId)
                                    {
                                        ind = k;
                                        break;
                                    }
                                }
                                if (ind == -1)
                                {
                                    item = new DepositsReporter.WellGisProdItem();
                                    ind = sumItem.gisProd.Count;
                                    item.gis = dateItems[i].gisProd[j].gis;
                                    sumItem.gisProd.Add(item);
                                }
                                sumItem.gisProd[ind].accumOil += dateItems[i].gisProd[j].accumOil;
                                sumItem.gisProd[ind].accumWater += dateItems[i].gisProd[j].accumWater;
                            }
                        }
                        for (i = 0; i < sumItem.gisProd.Count; i++)
                        {
                            row = new object[8];
                            row[0] = of.ParamsDict.NGDUName;
                            row[1] = of.Name;
                            row[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                            row[3] = w.Name;
                            row[4] = stratumDict.GetShortNameByCode(sumItem.gisProd[i].gis.PlastId);
                            row[5] = sumItem.gisProd[i].accumOil;
                            row[6] = sumItem.gisProd[i].accumWater;
                            if (error) row[7] = "Ошибка";
                            outList.Add(row);
                        }
                    }
                    allDateItems[w.Index] = new List<SmartPlus.Report.DepositsReporter.WellPerfDateItem>(dateItems.ToArray());

                    if (!w.PerfLoaded)
                    {
                        err = new object[8];
                        err[0] = of.ParamsDict.NGDUName;
                        err[1] = of.Name;
                        err[2] = oilfieldAreaDict.GetShortNameByCode(w.OilFieldAreaCode);
                        err[4] = w.Name;
                        err[5] = "Перфорация";
                        err[6] = (!outAllWellOil) ? AllWellOil : 0;
                        err[7] = "Не загружена перфорация по скважине";
                        if (AllWellOil > 0 && !DepositsReporter.IndexOfArray(errors, err)) errors.Add(err);
                        outAllWellOil = true;
                    }
                }

                of.ClearGisData(false);
                of.ClearPerfData(false);
                of.ClearMerData(true);
            }
            if (excel != null)
            {
                while (excel.GetWorkSheetsCount() < 2)
                {
                    excel.AddNewWorkSheet();
                }

                excel.SetActiveSheet(0);
                excel.SetActiveSheetName("По скважинам");
                if (outList.Count > 0)
                {
                    row = new object[8];
                    row[0] = "НГДУ";
                    row[1] = "Месторождение";
                    row[2] = "Площадь";
                    row[3] = "Скважина";
                    row[4] = "Пласт";
                    row[5] = "Нак.нефть, т";
                    row[6] = "Нак.вода, т";
                    row[7] = "Ошибки при распределении добычи";
                    outList.Insert(0, row);
                    excel.SetRowsArray(0, 0, outList);
                }

                excel.SetActiveSheet(1);
                excel.SetActiveSheetName("Ошибки");
                if (errors.Count > 0)
                {
                    err = new object[8];
                    err[0] = "НГДУ";
                    err[1] = "Месторождение";
                    err[2] = "Площадь";
                    err[3] = "Пласт";
                    err[4] = "Скважина";
                    err[5] = "Тип данных ошибки";
                    err[6] = "Данные";
                    err[7] = "Комментарий";
                    errors.Insert(0, err);
                    excel.SetRowsArray(0, 0, errors);
                }

                excel.EnableEvents = true;
                excel.Visible = true;
            }
        }
        #endregion

     }
#endif
}
