﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus;
using RDF.Objects;
using System.IO;
using System.ComponentModel;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.Report
{
#if BASHNEFT
    struct OilfieldParams
    {
        public OilField of;
        public double[] DropPercent;
        public List<WellGTMParamsCollection> WellGtmList;
        public int GetWellGtmIndex(Well w)
        {
            for (int i = 0; i < WellGtmList.Count; i++)
            {
                if (w == WellGtmList[i].well)
                {
                    return i;
                }
            }
            return -1;
        }
        public void ReadFromCache(BinaryReader br, Project project)
        {
            int ind = br.ReadInt32();
            of = project.OilFields[ind];
            int count = br.ReadInt32();
            DropPercent = new double[count];
            for (int i = 0; i < count; i++)
            {
                DropPercent[i] = br.ReadDouble();
            }
            WellGtmList = new List<WellGTMParamsCollection>();
            WellGTMParamsCollection wellGtm;
            count = br.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                wellGtm = new WellGTMParamsCollection();
                wellGtm.ReadFromCache(br, of);
                WellGtmList.Add(wellGtm);
            }
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(of.Index);
            bw.Write(DropPercent.Length);
            for (int i = 0; i < DropPercent.Length;i++)
            {
                bw.Write(DropPercent[i]);
            }
            bw.Write(WellGtmList.Count);
            for(int i = 0; i < WellGtmList.Count;i++)
            {
                WellGtmList[i].WriteToCache(bw);
            }
        }
    }
    class WellGTMParamsCollection
    {
        public Well well;
        List<WellGTMParams> GtmParams;
        List<WellPlastPart> PlastParts;
        public int Count { get { return GtmParams.Count; } }

        public WellGTMParamsCollection()
        {
            well = null;
            GtmParams = new List<WellGTMParams>();
            PlastParts = new List<WellPlastPart>();
        }
        public void ReadFromCache(BinaryReader br, OilField of)
        {
            int ind = br.ReadInt32();
            well = of.Wells[ind];
            int count = br.ReadInt32(), count2;
            WellGTMParams item;
            for (int i = 0; i < count; i++)
            {
                item.GtmIndex = br.ReadInt32();
                count2 = br.ReadInt32();
                item.AfterProdOil = new double[count2];
                for (int j = 0; j < count2; j++)
                {
                    item.AfterProdOil[j] = br.ReadDouble();
                }
                count2 = br.ReadInt32();
                item.AfterProdLquid = new double[count2];
                for (int j = 0; j < count2; j++)
                {
                    item.AfterProdLquid[j] = br.ReadDouble();
                }
                GtmParams.Add(item);
            }
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(well.Index);
            bw.Write(GtmParams.Count);
            for (int i = 0; i < GtmParams.Count; i++)
            {
                bw.Write(GtmParams[i].GtmIndex);
                bw.Write(GtmParams[i].AfterProdOil.Length);
                for (int j = 0; j < GtmParams[i].AfterProdOil.Length; j++)
                {
                    bw.Write(GtmParams[i].AfterProdOil[j]);
                }
                bw.Write(GtmParams[i].AfterProdLquid.Length);
                for (int j = 0; j < GtmParams[i].AfterProdLquid.Length; j++)
                {
                    bw.Write(GtmParams[i].AfterProdLquid[j]);
                }
            }
        }
        public WellGTMParams this[int index]
        {
            get
            {
                return GtmParams[index];
            }
        }
        public int AddGtmParams(int GtmIndex)
        {
            WellGTMParams item;
            item.GtmIndex = GtmIndex;
            item.AfterProdOil = new double[12 * 3];
            item.AfterProdLquid = new double[12 * 3];
            GtmParams.Add(item);
            return GtmParams.Count - 1;
        }
        public int GetGtmParamsIndex(int GtmIndex)
        {
            for (int i = 0; i < GtmParams.Count; i++)
            {
                if (GtmIndex == GtmParams[i].GtmIndex)
                {
                    return i;
                }
            }
            return -1;
        }

        public WellPlastPart GetPlastPart(int PlastCode)
        {
            WellPlastPart part = null;
            for (int i = 0; i < PlastParts.Count; i++)
            {
                if (PlastParts[i].PlastCode == PlastCode)
                {
                    part = PlastParts[i];
                    break;
                }
            }
            if (part == null)
            {
                part = new WellPlastPart();
                part.PlastCode = PlastCode;
                PlastParts.Add(part);
            }
            return part;
        }
        public WellPlastPart GetSumPlastPart(List<int> PlastCodes)
        {
            List<WellPlastPart> parts = new List<WellPlastPart>();
            for (int i = 0; i < PlastParts.Count; i++)
            {
                if (PlastCodes.IndexOf(PlastParts[i].PlastCode) > -1)
                {
                    parts.Add(PlastParts[i]);
                }
            }
            WellPlastPart sumPart = new WellPlastPart();
            for (int i = 0; i < parts.Count; i++)
            {
                for(int j = 0; j < sumPart.Part.Length;j++)
                {
                    sumPart.Part[j] += parts[i].Part[j];
                }
            }
            return sumPart;
        }
        public void RecalcPlastPart()
        {
            double[] sumOil = new double[3];
            for (int i = 0; i < PlastParts.Count; i++)
            {
                sumOil[0] += PlastParts[i].Part[0];
                sumOil[1] += PlastParts[i].Part[1];
                sumOil[2] += PlastParts[i].Part[2];
            }
            for (int i = 0; i < PlastParts.Count; i++)
            {
                if (sumOil[0] > 0) PlastParts[i].Part[0] /= sumOil[0];
                if (sumOil[1] > 0) PlastParts[i].Part[1] /= sumOil[1];
                if (sumOil[2] > 0) PlastParts[i].Part[2] /= sumOil[2];
            }
        }
    }
    class WellPlastPart
    {
        public int PlastCode;
        public double[] Part;
        public WellPlastPart()
        {
            Part = new double[3];
        }
    }
    struct WellGTMParams
    {
        public int GtmIndex;
        public double[] AfterProdOil;
        public double[] AfterProdLquid;
    }
    class AreasGTMReporter
    {
        // Отчет по ячейкам по годам 2010 - 2012 гг.
        public static void ReportAreasGTM(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            OilfieldParams[] OfParams;
            char[] parser = new char[] { ';' };
            string[] fileNames = project.tempStr.Split(parser);
            OilField of = null;
            Area area;
            Well w;
            int i, j, k, ind, year;
            SmartPlus.MSOffice.Excel excel;
            StratumTreeNode stratumNode;
            object[] row;
            string str;
            List<Well> wells;
            MerComp comp;
            MerWorkTimeItem wtProd, wtInj;
            double sumLiq, sumOil;
            List<object[]>[] out_list = new List<object[]>[3];
            out_list[0] = new List<object[]>();
            out_list[1] = new List<object[]>();
            out_list[2] = new List<object[]>();
            List<Well>[] gtmWells = new List<Well>[3];
            List<Well>[] withoutGtmWells = new List<Well>[3];
            double[][] vals;
            double[][] Pressure;
            string[] GtmNames = new string[] { "Всего", "ВНС", "ЗБС", "ГРП", "ПВЛГ", "ВБД", "ИДН", "ОПЗ", "Реперфорация", "РИР" };
            string[][] GtmWellNames = new string[3][];

            int startYear = 2010;

            for(i = 0; i < GtmWellNames.Length;i++) 
            {
                GtmWellNames[i] = new string[10];
            }
            List<int> UsedGtmCodes = new List<int>(new int[] {1000, 1002, 1003, 2000, 2001, 2002, 2003, 3000, 3001, 3002, 3003, 3004, 3005, 5000, 5001, 5002, 6000, 6001, 6002, 6003, 7000, 8000, 8001, 8002, 8003, 8004, 8005, 8006, 9000, 9001, 9002, 9003, 9004, 10000, 10001, 10002, 10004, 11000, 11001, 11002, 11003, 11004 });
            /// GTM codes
            /// 100x - ВНС из бурения
            /// 200x - ВНС из др.фонда
            /// 300х - ЗБС
            /// 500х - ГРП
            /// 600х - ПВЛГ
            /// 7000 - ВБД
            /// 800х - Оптимизация ПО
            /// 900х - РИР
            /// 1000х - ОПЗ
            /// 1100х - Реперфорация

            if (fileNames.Length == 1)
            {
                OfParams = new OilfieldParams[project.OilFields.Count];
                var dictArea = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                var dictStratum = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                vals = new double[3][];
                for(i = 0; i < vals.Length;i++)
                {
                    vals[i] = new double[100];
                }
                /// fill OfParams
                for (i = 0; i < project.OilFields.Count; i++)
                {
                    OfParams[i].of = project.OilFields[i];
                    OfParams[i].DropPercent = new double[3];
                    OfParams[i].WellGtmList = new List<WellGTMParamsCollection>();
                }
                if (File.Exists(fileNames[0] + "\\bp.csv"))
                {
                    string[] Drops = File.ReadAllLines(fileNames[0] + "\\bp.csv");
                    string[] split;

                    for (int ofInd = 0; ofInd < OfParams.Length; ofInd++)
                    {
                        split = Drops[ofInd].Split(parser);
                        OfParams[ofInd].DropPercent[0] = Convert.ToDouble(split[1]);
                        OfParams[ofInd].DropPercent[1] = Convert.ToDouble(split[2]);
                        OfParams[ofInd].DropPercent[2] = Convert.ToDouble(split[3]);
                    }
                }

                #region Чтение из Excel Drop Percent - отключено
                List<string> err = new List<string>();
                if (false && Directory.Exists(fileNames[0]))
                {
                    excel = new SmartPlus.MSOffice.Excel();
                    excel.EnableEvents = false;
                    excel.Visible = false;
                    FileInfo[] files;
                    int sheetsCount;
                    int indNGDU;
                    DirectoryInfo[] root = new DirectoryInfo[2];
                    root[0] = new DirectoryInfo(fileNames[0] + "\\2011");
                    root[1] = new DirectoryInfo(fileNames[0] + "\\2012");
                    for (k = 0; k < 2; k++)
                    {
                        DirectoryInfo[] ngdu = root[k].GetDirectories();

                        for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                        {
                            of = project.OilFields[ofInd];
                            indNGDU = -1;
                            for (i = 0; i < ngdu.Length; i++)
                            {
                                if (ngdu[i].Name.IndexOf(of.ParamsDict.NGDUName, StringComparison.OrdinalIgnoreCase) > -1)
                                {
                                    indNGDU = i;
                                    break;
                                }
                            }
                            if (indNGDU == -1)
                            {
                                if (err.IndexOf("НГДУ " + of.ParamsDict.NGDUName) == -1) err.Add("НГДУ " + of.ParamsDict.NGDUName);
                                continue;
                            }
                            files = ngdu[indNGDU].GetFiles();
                            ind = -1;
                            for (i = 0; i < files.Length; i++)
                            {
                                if (files[i].Name.StartsWith(of.Name, StringComparison.OrdinalIgnoreCase))
                                {
                                    ind = i;
                                    break;
                                }
                            }
                            if (ind == -1)
                            {
                                if (err.IndexOf("Месторождение " + of.Name) == -1) err.Add("Месторождение " + of.Name);
                                continue;
                            }
                            excel.OpenWorkbook(files[ind].FullName);
                            sheetsCount = excel.GetWorkSheetsCount();
                            ind = -1;
                            for (i = sheetsCount - 1; i >= 0; i--)
                            {
                                excel.SetActiveSheet(i);
                                if (excel.GetActiveSheetName() == "Свод")
                                {
                                    ind = i;
                                    break;
                                }
                            }
                            if (ind != -1)
                            {
                                OfParams[ofInd].DropPercent[k + 1] = Convert.ToDouble(excel.GetValue(12, 14));
                                if (Math.Abs(OfParams[ofInd].DropPercent[k + 1]) > 1) OfParams[ofInd].DropPercent[k + 1] = 0;
                            }
                            excel.CloseWorkbook(false);
                            worker.ReportProgress((int)(ofInd * 100 / project.OilFields.Count));
                        }
                    }
                    excel.Quit();
                    string[] s = new string[project.OilFields.Count];
                    for (int ofInd = 0; ofInd < project.OilFields.Count; ofInd++)
                    {
                        s[ofInd] = string.Format("{0};{1};{2};{3}", project.OilFields[ofInd].Name, OfParams[ofInd].DropPercent[0], OfParams[ofInd].DropPercent[1], OfParams[ofInd].DropPercent[2]);
                    }
                    File.WriteAllLines("C:\\Users\\bashirovia\\Desktop\\Таблица ЯЗ\\БП\\bp.csv", s);
                    File.WriteAllLines("C:\\Users\\bashirovia\\Desktop\\Таблица ЯЗ\\БП\\err.csv", err.ToArray());
                }
                #endregion

                #region Чтение из Excel Доп добыча ГТМ
                int indRow, month, gtmIndex, wellGtmIndex;
                string ofName = string.Empty, areaName = string.Empty, wellName = string.Empty;
                WellGTMParamsCollection wellParams;
                err.Clear();
                
                List<WellGTMParams> wellParamsList = new List<WellGTMParams>();
                if (File.Exists(fileNames[0] + "\\cache.bin"))
                {
                    FileStream fs = new FileStream(fileNames[0] + "\\cache.bin", FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    
                    OfParams = new OilfieldParams[br.ReadInt32()];
                    for (i = 0; i < OfParams.Length; i++)
                    {
                        OfParams[i].ReadFromCache(br, project);
                    }
                    fs.Close();
                }
                else if (File.Exists(fileNames[0] + "\\2010.csv"))
                {
                    StreamReader sr;
                    string[] cells;
                    int areaCode;
                    for (k = 0; k < 3; k++)
                    {
                        indRow = 3;
                        if (File.Exists(fileNames[0] + "\\201" + k.ToString() + ".csv"))
                        {
                            sr = new StreamReader(fileNames[0] + "\\201" + k.ToString() + ".csv", Encoding.GetEncoding(1251));
                            of = null;
                            w = null;
                            while (!sr.EndOfStream)
                            {
                                cells = sr.ReadLine().Split(parser);
                                ofName = cells[0].Trim();
                                if (of == null || !of.Name.Equals(ofName, StringComparison.OrdinalIgnoreCase))
                                {
                                    if (of != null) of.ClearGTMData(false);
                                    ind = project.GetOFIndex(ofName);
                                    if (ind == -1)
                                    {
                                        err.Add(string.Format("Не найдено месторождение " + ofName));
                                        while (!sr.EndOfStream && cells[0].Trim() == ofName)
                                        {
                                            cells = sr.ReadLine().Split(parser);
                                        }
                                        continue;
                                    }
                                    of = project.OilFields[ind];
                                    of.LoadGTMFromCache(worker, e);
                                }
                                if (of != null)
                                {
                                    areaName = cells[1].Trim();
                                    wellName = cells[2].Trim();
                                    ind = dictArea.GetIndexByShortName(areaName);
                                    if (ind == -1)
                                    {
                                        err.Add(string.Format("Не найдена площадь  " + areaName));
                                        while (!sr.EndOfStream && cells[1].Trim() == areaName)
                                        {
                                            cells = sr.ReadLine().Split(parser);
                                        }
                                        continue;
                                    }
                                    ind = of.GetWellIndex(wellName.ToUpper(), dictArea[ind].Code);
                                    if (ind == -1)
                                    {
                                        err.Add(string.Format("Не найдена скважина {0}, {1}, {2} ", wellName, of.Name, areaName));
                                        while (!sr.EndOfStream && cells[2].Trim() == wellName)
                                        {
                                            cells = sr.ReadLine().Split(parser);
                                        }
                                        continue;
                                    }
                                    w = of.Wells[ind];
                                    month = Convert.ToInt32(cells[3].Trim());
                                    if (month > 0 && month < 13)
                                    {
                                        gtmIndex = -1;
                                        int ib = 0;
                                        areaCode = w.OilFieldAreaCode;
                                        while (ib < 4)
                                        {
                                            if (w != null && w.GTMLoaded)
                                            {
                                                for (j = 0; j < w.gtm.Count; j++)
                                                {
                                                    if (w.gtm[j].Date.Year == k + startYear && w.gtm[j].Date.Month == month)
                                                    {
                                                        gtmIndex = j;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (gtmIndex != -1) break;
                                            ib++;
                                            ind = of.GetWellIndex(wellName.ToUpper() + "С" + ib.ToString(), areaCode);
                                            if (ind == -1) ind = of.GetWellIndex(wellName.ToUpper() + "C" + ib.ToString(), areaCode);
                                            if (ind != -1) w = of.Wells[ind];
                                        }
                                        if (gtmIndex == -1)
                                        {
                                            err.Add(string.Format("Не найден ГТМ на дату {0:MM.yyyy}. Cкважина {1}, {2}, {3} ", new DateTime(k + startYear, month, 1), wellName, of.Name, areaName));
                                            if(!sr.EndOfStream) cells = sr.ReadLine().Split(parser);
                                            continue;
                                        }

                                        wellGtmIndex = OfParams[of.Index].GetWellGtmIndex(w);
                                        if (wellGtmIndex == -1)
                                        {
                                            wellGtmIndex = OfParams[of.Index].WellGtmList.Count;
                                            OfParams[of.Index].WellGtmList.Add(new WellGTMParamsCollection());
                                        }
                                        
                                        wellParams = OfParams[of.Index].WellGtmList[wellGtmIndex];
                                        wellParams.well = w;

                                        ind = wellParams.GetGtmParamsIndex(gtmIndex);
                                        if (ind != -1)
                                        {
                                            err.Add(string.Format("Двойная запись ГТМ на дату {0:MM.yyyy}. Cкважина {1}, {2}, {3} ", w.gtm[ind].Date, wellName, of.Name, areaName));
                                            if (!sr.EndOfStream) cells = sr.ReadLine().Split(parser);
                                            continue;
                                        }


                                        ind = wellParams.AddGtmParams(gtmIndex);

                                        for (i = 0; i < 12; i++)
                                        {
                                            str = cells[4 + i].Trim();
                                            wellParams[ind].AfterProdOil[k * 12 + i] = (str.Length == 0) ? 0 : Convert.ToDouble(str);
                                            str = cells[16 + i].Trim();
                                            wellParams[ind].AfterProdLquid[k * 12 + i] = (str.Length == 0) ? 0 : Convert.ToDouble(str);
                                        }

                                        OfParams[of.Index].WellGtmList[wellGtmIndex] = wellParams;
                                    }
                                }
                                indRow++;
                            }
                        }
                    }
                    File.WriteAllLines(fileNames[0] + "\\err2.csv", err.ToArray(), Encoding.GetEncoding(1251));
                    FileStream fs = new FileStream(fileNames[0] + "\\cache.bin", FileMode.Create);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(OfParams.Length);
                    for (i = 0; i < OfParams.Length; i++)
                    {
                        OfParams[i].WriteToCache(bw);
                    }
                    fs.Close();
                }

                #endregion

                List<List<int>> GtmCodes = new List<List<int>>();
                GtmCodes.Add(UsedGtmCodes);
                GtmCodes.Add(new List<int>(new int[] { 1000, 1002, 1003, 2000, 2001, 2002, 2003 })); // ВНС
                GtmCodes.Add(new List<int>(new int[] { 3000, 3001, 3002, 3003, 3004, 3005 }));       // ЗБС
                GtmCodes.Add(new List<int>(new int[] { 5000, 5001, 5002 }));                         // ГРП
                GtmCodes.Add(new List<int>(new int[] { 6000, 6001, 6002, 6003 }));                   // ПВЛГ
                GtmCodes.Add(new List<int>(new int[] { 7000 }));                                     // ВБД
                GtmCodes.Add(new List<int>(new int[] { 8000, 8001, 8002, 8003, 8004, 8005, 8006 })); // ИДН
                GtmCodes.Add(new List<int>(new int[] { 10000, 10001, 10002, 10004 }));               // ОПЗ
                GtmCodes.Add(new List<int>(new int[] { 11000, 11001, 11002, 11003, 11004 }));        // Реперфорация
                GtmCodes.Add(new List<int>(new int[] { 9000, 9001, 9002, 9003, 9004 }));             // РИР
                int countGtm;
                double Square, Volume, NBZ, AverageThickness;
                List<Grid> grids = new List<Grid>();
                List<Grid> loadedGrids = new List<Grid>();
                double[] values;
                if (of != null) of.ClearGTMData(true);

                List<int> AllPlastCodes = new List<int>();
                List<PVTParamsItem> allPVT = new List<PVTParamsItem>();
                List<List<Well>> allWellList = new List<List<Well>>();
                List<double[][]> AllParams = new List<double[][]>();
                double[][] plastParams = null, allPlastParams = null;
                allPlastParams = new double[3][];
                for (i = 0; i < allPlastParams.Length; i++)
                {
                    allPlastParams[i] = new double[104];
                }
                List<int> oilfieldAreaObjects = new List<int>();
                List<int> areaPlastCodes = new List<int>();
                List<int> gridPlastCodes = new List<int>();
                for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                {
                    of = project.OilFields[ofInd];
                    //if (of.Name != "АБДУЛОВСКОЕ") continue;
                    //if (of.Name != "ИЛЬИНСКОЕ") continue;
                    //if (ofInd > 2) break;

                    of.LoadMerFromCache(worker, e, false);
                    of.LoadGTMFromCache(worker, e);
                    of.LoadWellResearchFromCache(worker, e);
                    of.LoadPVTParamsFromCache(worker, e);
                    AllPlastCodes.Clear();
                    AllParams.Clear();
                    allPVT.Clear();
                    allWellList.Clear();
                    AllPlastCodes.Add(-1);
                    oilfieldAreaObjects.Clear();
                    for (i = 0; i < allPlastParams.Length; i++)
                    {
                        for (j = 0; j < allPlastParams[i].Length; j++)
                        {
                            allPlastParams[i][j] = 0;
                        }
                    }
                    AllParams.Add(allPlastParams);
                    allPVT.Add(PVTParamsItem.Empty);
                    allWellList.Add(new List<Well>());
                    for (int aInd = 0; aInd < of.Areas.Count; aInd++)
                    {
                        if (oilfieldAreaObjects.IndexOf(of.Areas[aInd].PlastCode) == -1)
                        {
                            oilfieldAreaObjects.Add(of.Areas[aInd].PlastCode);
                        }
                    }
                    for (int aInd = 0; aInd < of.Areas.Count; aInd++)
                    {
                        area = (Area)of.Areas[aInd];
                        if (int.TryParse(area.Name, out i)) continue;

                        for (i = 0; i < vals.Length; i++)
                        {
                            for (j = 0; j < vals[i].Length; j++)
                            {
                                vals[i][j] = 0;
                            }
                        }
                        areaPlastCodes.Clear();
                        for (i = 0; i < of.MerStratumCodes.Count; i++)
                        {
                            stratumNode = dictStratum.GetStratumTreeNode(of.MerStratumCodes[i]);

                            if (area.PlastCode == of.MerStratumCodes[i])
                            {
                                areaPlastCodes.Add(of.MerStratumCodes[i]);
                            }
                            else if(stratumNode.GetParentLevelByCode(area.PlastCode) > -1 && oilfieldAreaObjects.IndexOf(of.MerStratumCodes[i]) == -1)
                            {
                                areaPlastCodes.Add(of.MerStratumCodes[i]);
                            }
                        }
                        /// Параметры массива
                        /// 0 - Добыча нефти за год, т
                        /// 1 - Добыча жидкости за год, т
                        /// 2 - Среднедейств фонд доб
                        /// 3 - Среднедейств фонд наг
                        /// 4 - Среднее пластовое давление на начало года - allParams - НБЗ
                        /// 5 - Среднее пластовое давление на конец года - allParams - ОИЗ
                        /// 6 - Среднее забойное давление на начало года - allParams - Square
                        /// 7 - Среднее забойное давление на конец года - allParams - Volume
                        /// 8 - Среднее забойное давление на конец года по базе - allParams - WellCount
                        /// 9 - % падения дебита нефти из БП
                        /// 10 - Сумм нефть на начало года
                        /// 11 - Сумм жидкость на начало года
                        /// 12 - Сумм время работы на начало года
                        /// 13 - Средн дебит нефти на начало года
                        /// 14 - Средн дебит жидкости на начало года
                        /// 
                        /// 15 - Сумм нефть на конец года
                        /// 16 - Сумм жидкость на конец года
                        /// 17 - Сумм время работы на конец года
                        /// 18 - Средн дебит нефти на конец года
                        /// 19 - Средн дебит жидкости на конец года
                        /// 
                        /// 20 - Сумм нефть по базе на конец года
                        /// 21 - Сумм жидкость по базе на конец года
                        /// 22 - Средн дебит нефти по базе на конец года
                        /// 23 - Средн дебит жидкости по базе на конец года
                        /// 24 - Добыча жидкости за год, м3
                        /// 25 - Закачка на начало года
                        /// 26 - Закачка на конец года
                        /// 27 - Закачка за год
                        /// 28 - Нефть м3 на начало года
                        /// 29 - Вода м3 на начало года
                        /// 30 - Нефть м3 на конец года
                        /// 31 - Вода м3 на конец года
                        /// 32 - Нефть м3 за год
                        /// 33 - Вода м3 за год
                        /// 34 - Накопленная нефть на конец года
                        /// 35 - Накопленная вода на конец года
                        /// 36 - ДДН от ГТМ
                        /// 37 - 97 данные ГТМ

                        plastParams = null;
                        ind = AllPlastCodes.IndexOf(area.PlastCode);
                        if (ind == -1)
                        {
                            ind = AllPlastCodes.Count;
                            AllPlastCodes.Add(area.PlastCode);
                            plastParams = new double[3][];
                            for (i = 0; i < plastParams.Length; i++)
                            {
                                plastParams[i] = new double[104];
                            }
                            AllParams.Add(plastParams);
                            allPVT.Add(area.pvt);
                            allWellList.Add(new List<Well>());
                        }
                        allWellList[ind].AddRange(area.WellList);
                        plastParams = AllParams[ind];

                        WellPlastPart plastPart;
                        for (int wInd = 0; wInd < area.WellList.Length; wInd++)
                        {
                            w = (Well)area.WellList[wInd];
                            if (!w.MerLoaded) continue;
                            if (allWellList[0].IndexOf(w) == -1) allWellList[0].Add(w);

                            comp = new MerComp(w.mer);
                            wellGtmIndex = OfParams[of.Index].GetWellGtmIndex(w);
                            if (wellGtmIndex != -1)
                            {
                                for (i = 0; i < comp.Count; i++)
                                {
                                    if (comp[i].Date.Year >= startYear && comp[i].Date.Year < startYear + 3)
                                    {
                                        year = comp[i].Date.Year - startYear;
                                        for (j = 0; j < comp[i].PlastItems.Count; j++)
                                        {
                                            plastPart = OfParams[of.Index].WellGtmList[wellGtmIndex].GetPlastPart(comp[i].PlastItems[j].PlastCode);
                                            plastPart.Part[year] += comp[i].PlastItems[j].Oil;
                                        }
                                    }
                                }
                                OfParams[of.Index].WellGtmList[wellGtmIndex].RecalcPlastPart();
                                plastPart = OfParams[of.Index].WellGtmList[wellGtmIndex].GetSumPlastPart(areaPlastCodes);
                                for (year = 0; year < 3; year++)
                                {
                                    for (j = 0; j < OfParams[of.Index].WellGtmList[wellGtmIndex].Count; j++)
                                    {
                                        for (k = 0; k < 12; k++)
                                        {
                                            vals[year][36] += OfParams[of.Index].WellGtmList[wellGtmIndex][j].AfterProdOil[year * 12 + k] * plastPart.Part[year];
                                        }
                                    }
                                }
                            }
                            for (i = 0; i < comp.Count; i++)
                            {
                                year = comp[i].Date.Year - startYear;
                                if (year < 0)
                                {
                                    for (int ip = 0; ip < comp[i].PlastItems.Count; ip++)
                                    {
                                        if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                        vals[0][34] += comp[i].PlastItems[ip].Oil;
                                        vals[0][35] += comp[i].PlastItems[ip].Liq - comp[i].PlastItems[ip].Oil;
                                        vals[1][34] += comp[i].PlastItems[ip].Oil;
                                        vals[1][35] += comp[i].PlastItems[ip].Liq - comp[i].PlastItems[ip].Oil;
                                        vals[2][34] += comp[i].PlastItems[ip].Oil;
                                        vals[2][35] += comp[i].PlastItems[ip].Liq - comp[i].PlastItems[ip].Oil;
                                    }
                                }
                                else if (year < 3)
                                {
                                    for (int ip = 0; ip < comp[i].PlastItems.Count; ip++)
                                    {
                                        if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                        vals[year][34] += comp[i].PlastItems[ip].Oil;
                                        vals[year][35] += comp[i].PlastItems[ip].Liq - comp[i].PlastItems[ip].Oil;
                                    }
                                }
                                
                                // На начало года
                                if (comp[i].Date.Year < startYear - 1) continue;
                                year = comp[i].Date.Year - (startYear - 1);
                                if (year < 3 && comp[i].Date.Month == 12)
                                {
                                    sumLiq = 0; sumOil = 0;
                                    for (int ip = 0; ip < comp[i].PlastItems.Count; ip++)    
                                    {
                                        if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                        vals[year][10] += comp[i].PlastItems[ip].Oil;
                                        vals[year][11] += comp[i].PlastItems[ip].Liq;
                                        vals[year][25] += comp[i].PlastItems[ip].Inj;
                                        vals[year][28] += comp[i].PlastItems[ip].OilV;
                                        vals[year][29] += comp[i].PlastItems[ip].LiqV - comp[i].PlastItems[ip].OilV;
                                        sumOil += comp[i].PlastItems[ip].Oil;
                                        sumLiq += comp[i].PlastItems[ip].Liq;
                                    }
                                    wtProd = comp[i].TimeItems.GetProductionTime(areaPlastCodes);
                                    vals[year][12] += wtProd.GetWorkHours();
                                    if (wtProd.WorkTime > 0)
                                    {
                                        vals[year][13] += sumOil * 24 / wtProd.GetWorkHours();
                                        vals[year][14] += sumLiq * 24 / wtProd.GetWorkHours();
                                    }
                                }
                                
                                year = comp[i].Date.Year - startYear;
                                if (year < 0) continue;
                                // На конец года
                                if ((year < 3) && (comp[i].Date.Month == 12 || comp[i].Date == project.maxMerDate))
                                {
                                    sumLiq = 0; sumOil = 0;
                                    for (int ip = 0; ip < comp[i].PlastItems.Count; ip++)
                                    {
                                        if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                        vals[year][15] += comp[i].PlastItems[ip].Oil;
                                        vals[year][16] += comp[i].PlastItems[ip].Liq;
                                        vals[year][26] += comp[i].PlastItems[ip].Inj;
                                        vals[year][30] += comp[i].PlastItems[ip].OilV;
                                        vals[year][31] += comp[i].PlastItems[ip].LiqV - comp[i].PlastItems[ip].OilV;
                                        sumOil += comp[i].PlastItems[ip].Oil;
                                        sumLiq += comp[i].PlastItems[ip].Liq;
                                    }

                                    wtProd = comp[i].TimeItems.GetProductionTime(areaPlastCodes);
                                    vals[year][17] += wtProd.GetWorkHours();
                                    if (wtProd.WorkTime > 0)
                                    {
                                        vals[year][18] += sumOil * 24 / wtProd.GetWorkHours();
                                        vals[year][19] += sumLiq * 24 / wtProd.GetWorkHours();
                                    }
                                    // Добыча/дебиты по базе
                                    sumLiq = 0; sumOil = 0;
                                    for (int ip = 0; ip < comp[i].PlastItems.Count; ip++)
                                    {
                                        if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                        sumLiq += comp[i].PlastItems[ip].Liq;
                                        sumOil += comp[i].PlastItems[ip].Oil;
                                    }
                                    wellGtmIndex = OfParams[of.Index].GetWellGtmIndex(w);
                                    if (wellGtmIndex != -1)
                                    {
                                        plastPart = OfParams[of.Index].WellGtmList[wellGtmIndex].GetSumPlastPart(areaPlastCodes);
                                        for (j = 0; j < OfParams[of.Index].WellGtmList[wellGtmIndex].Count; j++)
                                        {
                                            sumLiq -= OfParams[of.Index].WellGtmList[wellGtmIndex][j].AfterProdLquid[year * 12 + 11] * plastPart.Part[year];
                                            sumOil -= OfParams[of.Index].WellGtmList[wellGtmIndex][j].AfterProdOil[year * 12 + 11] * plastPart.Part[year];
                                        }
                                    }
                                    vals[year][20] += sumOil;
                                    vals[year][21] += sumLiq;
                                    // Дебиты по базе
                                    if (wtProd.WorkTime > 0)
                                    {
                                        vals[year][22] += sumOil * 24 / (wtProd.WorkTime + wtProd.CollTime);
                                        vals[year][23] += sumLiq * 24 / (wtProd.WorkTime + wtProd.CollTime);
                                    }
                                }

                                if (comp[i].Date.Year > startYear + 2) continue;
                                // Сумм добыча
                                for (int ip = 0; ip < comp[i].PlastItems.Count; ip++)
                                {
                                    if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                    vals[year][0] += comp[i].PlastItems[ip].Oil;
                                    vals[year][1] += comp[i].PlastItems[ip].Liq;
                                    vals[year][24] += comp[i].PlastItems[ip].LiqV;
                                    vals[year][27] += comp[i].PlastItems[ip].Inj;
                                    vals[year][32] += comp[i].PlastItems[ip].OilV;
                                    vals[year][33] += comp[i].PlastItems[ip].LiqV - comp[i].PlastItems[ip].OilV;
                                }

                                // Ср.действ. фонд
                                if ((comp[i].Date.Month > 9 && comp[i].Date.Month < 13) || 
                                     (comp[i].Date.Year == project.maxMerDate.Year && 
                                      comp[i].Date.Month > project.maxMerDate.Month - 3 && 
                                      comp[i].Date.Month <= project.maxMerDate.Month))
                                {
                                    wtProd = comp[i].TimeItems.GetProductionTime(areaPlastCodes);
                                    wtInj = comp[i].TimeItems.GetInjectionTime(areaPlastCodes);
                                    if (wtProd.WorkTime > 0) vals[year][2]++;
                                    if (wtInj.WorkTime > 0) vals[year][3]++;
                                }
                            }
                        }
                        for (year = 0; year < 3; year++)
                        {
                            #region All params
                            for (k = 0; k < vals[year].Length; k++)
                            {
                                if ((k > 3 && k < 10) || (k > 27 && k < 34)) continue;
                                plastParams[year][k] += vals[year][k];
                                allPlastParams[year][k] += vals[year][k];
                            }
                            plastParams[year][103] += area.WellList.Length;
                            plastParams[year][9] = vals[year][9];
                            plastParams[year][25] += vals[year][25] * area.pvt.WaterVolumeFactor;
                            plastParams[year][26] += vals[year][26] * area.pvt.WaterVolumeFactor;
                            plastParams[year][27] += vals[year][27] * area.pvt.WaterVolumeFactor;
                            plastParams[year][28] += vals[year][28] * area.pvt.OilVolumeFactor;
                            plastParams[year][29] += vals[year][29] * area.pvt.WaterVolumeFactor;
                            plastParams[year][30] += vals[year][30] * area.pvt.OilVolumeFactor;
                            plastParams[year][31] += vals[year][31] * area.pvt.WaterVolumeFactor;
                            plastParams[year][32] += vals[year][32] * area.pvt.OilVolumeFactor;
                            plastParams[year][33] += vals[year][33] * area.pvt.WaterVolumeFactor;
                            allPlastParams[year][103] += area.WellList.Length;
                            allPlastParams[year][9] = vals[year][9];
                            allPlastParams[year][25] += vals[year][25] * area.pvt.WaterVolumeFactor;
                            allPlastParams[year][26] += vals[year][26] * area.pvt.WaterVolumeFactor;
                            allPlastParams[year][27] += vals[year][27] * area.pvt.WaterVolumeFactor;
                            allPlastParams[year][28] += vals[year][28] * area.pvt.OilVolumeFactor;
                            allPlastParams[year][29] += vals[year][29] * area.pvt.WaterVolumeFactor;
                            allPlastParams[year][30] += vals[year][30] * area.pvt.OilVolumeFactor;
                            allPlastParams[year][31] += vals[year][31] * area.pvt.WaterVolumeFactor;
                            allPlastParams[year][32] += vals[year][32] * area.pvt.OilVolumeFactor;
                            allPlastParams[year][33] += vals[year][33] * area.pvt.WaterVolumeFactor;
                            #endregion
                            vals[year][2] /= 3; 
                            vals[year][3] /= 3;
                        }
                        if (area.AllWellList != null && area.AllWellList.Length > 0)
                        {
                            Pressure = MonitoringReporter.GetPressureByWells(new List<Well>(area.AllWellList), dictStratum, area.PlastCode, new DateTime(startYear - 1, 12, 1), 12 * 3 + 1, 0);
                            for (year = 0; year < 3; year++)
                            {
                                if (Pressure[year * 12][2] > 0) vals[year][4] = Pressure[year * 12][1] / Pressure[year * 12][2];
                                if (Pressure[(year + 1) * 12][2] > 0) vals[year][5] = Pressure[(year + 1) * 12][1] / Pressure[(year + 1) * 12][2];
                            }
                        }
                        if (area.WellList.Length > 0)
                        {
                            wells = new List<Well>(area.WellList);

                            for (year = 0; year < 3; year++)
                            {
                                gtmWells[year] = MonitoringReporter.GetWellsWithGTM(wells, UsedGtmCodes, new List<int>(new int[] { -1 }), new List<int>(new int[] { startYear + year }), out countGtm);
                                withoutGtmWells[year] = new List<Well>(wells);
                                withoutGtmWells[year].RemoveAll(delegate(Well matchWell) { return gtmWells[year].IndexOf(matchWell) != -1; });

                                Pressure = MonitoringReporter.GetZabPressureByWells(wells, dictStratum, area.PlastCode, new DateTime(startYear - 1, 12, 1), 12 * 3 + 1);
                                if (Pressure[year * 12][2] > 0) vals[year][6] = Pressure[year * 12][1] / Pressure[year * 12][2];
                                if (Pressure[(year + 1) * 12][2] > 0) vals[year][7] = Pressure[(year + 1) * 12][1] / Pressure[(year + 1) * 12][2];
                                Pressure = MonitoringReporter.GetZabPressureByWells(withoutGtmWells[year], dictStratum, area.PlastCode, new DateTime(startYear - 1, 12, 1), 12 * 3 + 1);
                                if (Pressure[(year + 1) * 12][2] > 0) vals[year][8] = Pressure[(year + 1) * 12][1] / Pressure[(year + 1) * 12][2];
                            }
                        }
                        // Запасы
                        Square = 0;
                        Volume = 0;
                        AverageThickness = 0;
                        NBZ = 0;
                        
                        // ищем сетки
                        bool find = false;
                        gridPlastCodes.Clear();
                        for (j = 0; j < areaPlastCodes.Count; j++)
                        {
                            grids.Clear();
                            for (i = 0; i < of.Grids.Count; i++)
                            {
                                if (of.Grids[i].GridType == 2 && of.Grids[i].StratumCode == areaPlastCodes[j])
                                {
                                    find = false;
                                    stratumNode = dictStratum.GetStratumTreeNode(areaPlastCodes[j]);
                                    for (k = 0; k < areaPlastCodes.Count; k++)
                                    {
                                        if (k != j && stratumNode.GetParentLevelByCode(areaPlastCodes[k]) > -1)
                                        {
                                            find = true;
                                        }
                                    }
                                    if (!find)
                                    {
                                        grids.Add(of.Grids[i]);
                                        if (gridPlastCodes.IndexOf(of.Grids[i].StratumCode) == -1)
                                        {
                                            gridPlastCodes.Add(of.Grids[i].StratumCode);
                                        }
                                    }
                                }
                            }
                            if (grids.Count > 0)
                            {
                                NBZ = 0;
                                for (i = 0; i < grids.Count; i++)
                                {
                                    if (!grids[i].DataLoaded)
                                    {
                                        of.LoadGridDataFromCacheByIndex(worker, e, grids[i].Index, false);
                                        loadedGrids.Add(grids[i]);
                                    }
                                    grids[i].CalcSumValues(worker, e, area.contour, out Square, out Volume);
                                    if (Square > 0) AverageThickness = Volume / Square;
                                    NBZ = area.pvt.OilDensity * area.pvt.Porosity * area.pvt.OilInitialSaturation * Volume / area.pvt.OilVolumeFactor;
                                    if (NBZ > 0) break;
                                }
                                for (year = 0; year < 3; year++)
                                {
                                    plastParams[year][98] += NBZ;
                                    plastParams[year][99] += NBZ * area.pvt.KIN;
                                    plastParams[year][100] += NBZ * area.pvt.KIN - vals[year][34];
                                    plastParams[year][101] += Square;
                                    plastParams[year][102] += Volume;
                                    allPlastParams[year][98] += NBZ;
                                    allPlastParams[year][99] += NBZ * area.pvt.KIN;
                                    allPlastParams[year][100] += NBZ * area.pvt.KIN - vals[year][34];
                                    allPlastParams[year][101] += Square;
                                    allPlastParams[year][102] += Volume;
                                }
                            }
                        }
                        Square = area.contour._points.GetSquare();

                        // параметры гтм
                        wells = new List<Well>(area.WellList);
                        for (year = 0; year < 3; year++)
                        {
                            vals[year][9] = OfParams[of.Index].DropPercent[year];
                            for (k = 0; k < GtmCodes.Count; k++)
                            {
                                GtmWellNames[year][k] = string.Empty;
                            }
                            for(k = 0; k < GtmCodes.Count;k++)
                            {
                                gtmWells[0] = MonitoringReporter.GetWellsWithGTM(wells, GtmCodes[k], areaPlastCodes, new List<int>(new int[] { startYear + year }), out countGtm);
                                for (i = 0; i < gtmWells[0].Count; i++)
                                {
                                    if (GtmWellNames[year][k].Length > 0) GtmWellNames[year][k] += ", ";
                                    GtmWellNames[year][k] += gtmWells[0][i].Name;
                                }
                                if (gtmWells[0].Count > 0)
                                {
                                    values = MonitoringReporter.GetGtmWellsParams(gtmWells[0], GtmCodes[k], areaPlastCodes, new List<int>(new int[] { startYear + year }));
                                    vals[year][37 + k * 6] = values[0];
                                    vals[year][38 + k * 6] = values[1];
                                    vals[year][39 + k * 6] = values[3];
                                    vals[year][40 + k * 6] = values[2];
                                    vals[year][41 + k * 6] = gtmWells[0].Count;
                                    vals[year][42 + k * 6] = countGtm;
                                }
                                else if (k == 0)
                                {
                                    break;
                                }
                            }
                        }
                        #region Выгрузка в отчет
                        for (year = 0; year < 3; year++)
                        {
                            // заполняем выходную инфу
                            row = new object[172];
                            row[0] = of.ParamsDict.NGDUName;
                            row[1] = of.Name;
                            row[2] = dictArea.GetShortNameByCode(area.OilFieldAreaCode);
                            row[3] = area.Name;
                            stratumNode = dictStratum.GetStratumTreeNode(area.PlastCode);
                            row[4] = stratumNode == null ? string.Empty : stratumNode.Name;
                            str = string.Empty;
                            for (i = 0; i < areaPlastCodes.Count; i++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += dictStratum.GetShortNameByCode(areaPlastCodes[i]);
                            }
                            row[5] = str;
                            if(!area.pvt.IsEmpty)
                            {
                                row[6] = area.pvt.InitialPressure;
                                row[7] = area.pvt.SaturationPressure;
                                row[8] = area.pvt.Permeability;
                            }
                            row[9] = area.WellList.Length;
                            values = vals[year];
                            row[10] = values[0];
                            row[11] = values[1];
                            row[12] = values[36];
                            row[13] = values[2];
                            row[14] = values[3];
                            row[15] = values[4];
                            row[16] = values[6];
                            row[17] = (values[12] > 0) ? values[10] * 24 / values[12] : 0;
                            row[18] = values[13];
                            row[19] = (values[12] > 0) ? values[11] * 24 / values[12] : 0;
                            row[20] = values[14];
                            row[21] = (values[11] > 0) ? (values[11]  - values[10]) / values[11] : 0;
                            row[22] = values[5];
                            row[23] = values[7];
                            row[24] = (values[17] > 0) ? values[15] * 24 / values[17] : 0;
                            row[25] = values[18];
                            row[26] = (values[17] > 0) ? values[16] * 24 / values[17] : 0;
                            row[27] = values[19];
                            row[28] = (values[16] > 0) ? (values[16] - values[15]) / values[16] : 0;
                            

                            row[34] = values[8];
                            row[35] = (values[17] > 0) ? values[20] * 24 / values[17] : 0;
                            row[36] = values[22];
                            row[37] = (values[17] > 0) ? values[21] * 24 / values[17] : 0;
                            row[38] = values[23];
                            row[39] = (values[21] > 0) ? (values[21] - values[20]) / values[21] : 0;

                            row[50] = values[9];

                            row[58] = 0;
                            row[59] = 0;
                            row[60] = 0;
                            if (values[28] + values[29] != 0 && values[25] != 0)
                            {
                                row[58] = 100 * (values[25] * area.pvt.WaterVolumeFactor / (values[28] * area.pvt.OilVolumeFactor + values[29] * area.pvt.WaterVolumeFactor));
                            }
                            else if (values[25] != 0)
                            {
                                row[58] = "-";
                            }
                            if (values[30] + values[31] != 0 && values[26] != 0)
                            {
                                row[59] = 100 * (values[26] * area.pvt.WaterVolumeFactor / (values[30] * area.pvt.OilVolumeFactor + values[31] * area.pvt.WaterVolumeFactor));
                            }
                            else if (values[26] != 0)
                            {
                                row[59] = "-";
                            }
                            if (values[32] + values[33] != 0 && values[27] != 0)
                            {
                                row[60] = 100 * (values[27] * area.pvt.WaterVolumeFactor / (values[32] * area.pvt.OilVolumeFactor + values[33] * area.pvt.WaterVolumeFactor));
                            }
                            else if (values[27] != 0)
                            {
                                row[60] = "-";
                            }

                            str = string.Empty;
                            for (i = 0; i < gridPlastCodes.Count; i++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += dictStratum.GetShortNameByCode(gridPlastCodes[i]);
                            }
                            row[61] = str;
                            row[62] = NBZ;
                            row[63] = area.pvt.KIN;
                            row[64] = NBZ * area.pvt.KIN;
                            row[65] = NBZ * area.pvt.KIN - values[34];
                            row[66] = values[34];
                            row[67] = (values[34] > 0) ? values[35] / values[34] : 0;
                            row[68] = Square / 1000000; // in km2
                            row[69] = AverageThickness;
                            if (NBZ == 0)
                            {
                                row[71] = 0;
                            }
                            else
                            {
                                row[71] = (values[15] == 0) ? 100 : (NBZ * area.pvt.KIN - values[34]) / (values[15] * 4);
                            }
                            
                            for(i = 0; i < GtmNames.Length; i++)
                            {
                                row[72 + i * 10] = GtmNames[i];
                                row[73 + i * 10] = values[42 + i * 6];
                                row[74 + i * 10] = values[41 + i * 6];
                                row[75 + i * 10] = GtmWellNames[year][i];
                                row[76 + i * 10] = values[37 + i * 6];
                                row[77 + i * 10] = values[38 + i * 6];
                                row[80 + i * 10] = values[40 + i * 6];
                                row[81 + i * 10] = values[39 + i * 6];
                            }
                            out_list[year].Add(row);
                        }
                        #endregion
                    }
                    // параметры гтм
                    #region В целом по месторождению
                    for (int ip = AllPlastCodes.Count - 1; ip >= 0; ip--)
                    {
                        plastParams = AllParams[ip];
                        wells = allWellList[ip];
                        areaPlastCodes.Clear();
                        if (AllPlastCodes[ip] == -1)
                        {
                            areaPlastCodes.Add(-1);
                        }
                        else
                        {
                            for (i = 0; i < of.MerStratumCodes.Count; i++)
                            {
                                stratumNode = dictStratum.GetStratumTreeNode(of.MerStratumCodes[i]);

                                if (AllPlastCodes[ip] == of.MerStratumCodes[i])
                                {
                                    areaPlastCodes.Add(of.MerStratumCodes[i]);
                                }
                                else if (stratumNode.GetParentLevelByCode(AllPlastCodes[ip]) > -1 && oilfieldAreaObjects.IndexOf(of.MerStratumCodes[i]) == -1)
                                {
                                    areaPlastCodes.Add(of.MerStratumCodes[i]);
                                }
                            }
                        }
                        for (year = 0; year < 3; year++)
                        {
                            for (k = 0; k < GtmCodes.Count; k++)
                            {
                                gtmWells[0] = MonitoringReporter.GetWellsWithGTM(wells, GtmCodes[k], areaPlastCodes, new List<int>(new int[] { startYear + year}), out countGtm);
                                GtmWellNames[year][k] = string.Empty;
                                for (i = 0; i < gtmWells[0].Count; i++)
                                {
                                    if (GtmWellNames[year][k].Length > 0) GtmWellNames[year][k] += ", ";
                                    GtmWellNames[year][k] += gtmWells[0][i].Name;
                                }
                                if (gtmWells[0].Count > 0)
                                {
                                    values = MonitoringReporter.GetGtmWellsParams(gtmWells[0], GtmCodes[k], areaPlastCodes, new List<int>(new int[] { startYear + year }));
                                    plastParams[year][37 + k * 6] = values[0];
                                    plastParams[year][38 + k * 6] = values[1];
                                    plastParams[year][39 + k * 6] = values[3];
                                    plastParams[year][40 + k * 6] = values[2];
                                    plastParams[year][41 + k * 6] += gtmWells[0].Count;
                                    plastParams[year][42 + k * 6] += countGtm;
                                }
                                else if (k == 0)
                                {
                                    break;
                                }
                            }
                        }
                        
                        for (year = 0; year < 3; year++)
                        {
                            row = new object[172];
                            row[0] = of.ParamsDict.NGDUName;
                            row[1] = of.Name;
                            row[2] = string.Empty;
                            row[3] = "Все ячейки";
                            stratumNode = dictStratum.GetStratumTreeNode(AllPlastCodes[ip]);
                            row[4] = stratumNode == null ? string.Empty : stratumNode.Name;
                            if (ip == 0) row[4] = "Все пласты";
                            str = string.Empty;
                            for (i = 0; i < areaPlastCodes.Count; i++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += dictStratum.GetShortNameByCode(areaPlastCodes[i]);
                            }
                            row[5] = str;
                            if (!allPVT[ip].IsEmpty)
                            {
                                row[6] = allPVT[ip].InitialPressure;
                                row[7] = allPVT[ip].SaturationPressure;
                                row[8] = allPVT[ip].Permeability;
                            }
                            values = plastParams[year];
                            row[9] = values[103];
                            row[10] = values[0];
                            row[11] = values[1];
                            row[12] = values[36];
                            row[13] = values[2] / 3;
                            row[14] = values[3] / 3;
                            
                            row[17] = (values[12] > 0) ? values[10] * 24 / values[12] : 0;
                            row[18] = values[13];
                            row[19] = (values[12] > 0) ? values[11] * 24 / values[12] : 0;
                            row[20] = values[14];
                            row[21] = (values[11] > 0) ? (values[11] - values[10]) / values[11] : 0;
                            
                            row[24] = (values[17] > 0) ? values[15] * 24 / values[17] : 0;
                            row[25] = values[18];
                            row[26] = (values[17] > 0) ? values[16] * 24 / values[17] : 0;
                            row[27] = values[19];
                            row[28] = (values[16] > 0) ? (values[16] - values[15]) / values[16] : 0;

                            row[35] = (values[17] > 0) ? values[20] * 24 / values[17] : 0;
                            row[36] = values[22];
                            row[37] = (values[17] > 0) ? values[21] * 24 / values[17] : 0;
                            row[38] = values[23];
                            row[39] = (values[21] > 0) ? (values[21] - values[20]) / values[21] : 0;

                            row[50] = values[9];

                            row[58] = 0;
                            row[59] = 0;
                            row[60] = 0;
                            if (values[28] + values[29] != 0 && values[25] != 0)
                            {
                                row[58] = 100 * (values[25] / (values[28] + values[29]));
                            }
                            else if (values[25] != 0)
                            {
                                row[58] = "-";
                            }
                            if (values[30] + values[31] != 0 && values[26] != 0)
                            {
                                row[59] = 100 * (values[26] / (values[30] + values[31]));
                            }
                            else if (values[26] != 0)
                            {
                                row[59] = "-";
                            }
                            if (values[32] + values[33] != 0 && values[27] != 0)
                            {
                                row[60] = 100 * (values[27] / (values[32] + values[33]));
                            }
                            else if (values[27] != 0)
                            {
                                row[60] = "-";
                            }
                            row[62] = values[98];
                            //row[63] = 0;
                            row[64] = values[99];
                            row[65] = values[100];
                            row[66] = values[34];
                            row[67] = (values[34] > 0) ? values[35] / values[34] : 0;
                            row[68] = values[101] / 1000000; // in km2
                            row[69] = values[102] / values[101];
                            if (values[99] == 0)
                            {
                                row[71] = 0;
                            }
                            else
                            {
                                row[71] = (values[15] == 0) ? 100 : values[100] / (values[15] * 4);
                            }

                            for (i = 0; i < GtmNames.Length; i++)
                            {
                                row[72 + i * 10] = GtmNames[i];
                                row[73 + i * 10] = values[42 + i * 6];
                                row[74 + i * 10] = values[41 + i * 6];
                                row[75 + i * 10] = GtmWellNames[year][i];
                                row[76 + i * 10] = values[37 + i * 6];
                                row[77 + i * 10] = values[38 + i * 6];
                                row[80 + i * 10] = values[40 + i * 6];
                                row[81 + i * 10] = values[39 + i * 6];
                            }
                            out_list[year].Add(row);
                        }
                    }
                    #endregion

                    for (i = 0; i < loadedGrids.Count; i++)
                    {
                        if (loadedGrids[i].DataLoaded) loadedGrids[i].FreeDataMemory();
                    }
                    loadedGrids.Clear();
                    of.PVTList = null;
                    of.ClearWellResearchData(false);
                    of.ClearGTMData(false);
                    of.ClearMerData(true);
                }
                //userState.WorkCurrentTitle = "Выгрузка данных в Microsoft Excel";
                if (out_list[0].Count > 0 || out_list[1].Count > 0 || out_list[2].Count > 0)
                {
                    int r, endRow = -1;
                    excel = new SmartPlus.MSOffice.Excel();
                    e.Result = excel;
                    excel.EnableEvents = false;
                    excel.NewWorkbook();
                    for (year = 0; year < 3; year++)
                    {
                        excel.SetActiveSheet(year);
                        excel.SetActiveSheetName((startYear + year).ToString());
                        object[,] varRow = (object[,])Array.CreateInstance(typeof(object), new int[2] { 50, out_list[year][0].Length }, new int[2] { 0, 0 });

                        for (i = 0; i < out_list[year].Count; i += 50)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                excel.Quit();
                                return;
                            }
                            for (r = 0; r < 50; r++)
                            {
                                if ((i + r >= out_list[year].Count) || (out_list[year][i + r] == null))
                                {
                                    endRow = i + r - 1;
                                    break;
                                }
                                else
                                {
                                    for (j = 0; j < out_list[year][i].Length; j++)
                                    {
                                        varRow[r, j] = out_list[year][i + r][j];
                                    }
                                    endRow = i + 50 - 1;
                                }
                            }
                            excel.SetRange(i + 2, 0, endRow + 2, out_list[year][0].Length - 1, varRow);
                        }
                    }
                    if (excel != null)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                        excel.Disconnect();
                    }
                }
            }
        }

        // Выгрзука прогноза по ячейкам заводнения
        public static void ReportAreasPredictData(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Выгрузка отчета 'Прогноз по ячейкам заводнения'..";
            userState.WorkCurrentTitle = "Выгрузка отчета 'Прогноз по ячейкам заводнения'..";

            char[] parser = new char[] { ';' };
            string[] fileNames = project.tempStr.Split(parser);
            OilField of = null;
            Area area;
            Well w;
            int i, j, k, t, month;
            SmartPlus.MSOffice.Excel excel = null;
            StratumTreeNode stratumNode;
            object[] row;
            string str;
            int fund;
            DateTime dt;
            MerComp comp;
            MerWorkTimeItem wtProd, wtInj;
            List<object[]> out_list = new List<object[]>();
            List<object[]> out_list2 = new List<object[]>();
            List<object[]> out_list2_gtm = new List<object[]>();
            List<object[]> out_selaedWells = new List<object[]>();

            List<Well>[] gtmWells = new List<Well>[3];
            List<Well>[] withoutGtmWells = new List<Well>[3];
            double[][] vals;
            double[][] Pressure;
            //string[] GtmNames = new string[] { "Всего", "ВНС", "ЗБС", "ГРП", "ПВЛГ", "ВБД", "ИДН", "ОПЗ", "Реперфорация", "РИР" };
            //string[][] GtmWellNames = new string[3][];
            bool find;
            
            DateTime startDate = project.maxMerDate.AddMonths(-25);
            int startYear = startDate.Year;

            string[] paramNames = new string[] { "Пластовое давление",
                                                 "Приемистость[МЭР]",
                                                 "Дебит нефти[МЭР]",
                                                 "Дебит жидкости[МЭР]",
                                                 "Обводненность вес.[МЭР]",
                                                 "Фонд нагнетательных скважин[МЭР]",
                                                 "Фонд добывающих скважин[МЭР]",
                                                 "Рзаб",
                                                 "НИЗ, т",
                                                 "Добыча нефти, т",
                                                 "Накопленная добыча нефти, т",
                                                 "Компенсация, %" };

            List<int> UsedGtmCodes = new List<int>(new int[] { 1000, 1002, 1003, 2000, 2001, 2002, 2003, 3000, 3001, 3002, 3003, 3004, 3005, 5000, 5001, 5002, 6000, 6001, 6002, 6003, 7000, 8000, 8001, 8002, 8003, 8004, 8005, 8006, 9000, 9001, 9002, 9003, 9004, 10000, 10001, 10002, 10004, 11000, 11001, 11002, 11003, 11004 });
            /// GTM codes
            /// 100x - ВНС из бурения
            /// 200x - ВНС из др.фонда
            /// 300х - ЗБС
            /// 500х - ГРП
            /// 600х - ПВЛГ
            /// 7000 - ВБД
            /// 800х - Оптимизация ПО
            /// 900х - РИР
            /// 1000х - ОПЗ
            /// 1100х - Реперфорация
            /// 
            string[] GtmWellNames;
            GtmWellNames = new string[10];
            List<List<int>> GtmCodes = new List<List<int>>();
            GtmCodes.Add(UsedGtmCodes);
            GtmCodes.Add(new List<int>(new int[] { 1000, 1002, 1003, 2000, 2001, 2002, 2003 })); // ВНС
            GtmCodes.Add(new List<int>(new int[] { 3000, 3001, 3002, 3003, 3004, 3005 }));       // ЗБС
            GtmCodes.Add(new List<int>(new int[] { 5000, 5001, 5002 }));                         // ГРП
            GtmCodes.Add(new List<int>(new int[] { 6000, 6001, 6002, 6003 }));                   // ПВЛГ
            GtmCodes.Add(new List<int>(new int[] { 7000 }));                                     // ВБД
            GtmCodes.Add(new List<int>(new int[] { 8000, 8001, 8002, 8003, 8004, 8005, 8006 })); // ИДН
            GtmCodes.Add(new List<int>(new int[] { 10000, 10001, 10002, 10004 }));               // ОПЗ
            GtmCodes.Add(new List<int>(new int[] { 11000, 11001, 11002, 11003, 11004 }));        // Реперфорация
            GtmCodes.Add(new List<int>(new int[] { 9000, 9001, 9002, 9003, 9004 }));             // РИР

            if (fileNames.Length > 0)
            {
                var dictArea = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                var dictStratum = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                userState.WorkCurrentTitle = "Загрузка шаблона отчета...";
                worker.ReportProgress(0, userState);

                excel = new SmartPlus.MSOffice.Excel();
                e.Result = excel;
                excel.EnableEvents = false;
                excel.OpenWorkbook(fileNames[0]);

                int ByAreas = -1, SvodSheet = -1, InitDataSheet = -1, TRsheet = -1;
                int sheetCount = excel.GetWorkSheetsCount();
                for (i = 0; i < sheetCount; i++)
                {
                    excel.SetActiveSheet(i);
                    if (excel.GetActiveSheetName() == "По ячейкам")
                    {
                        ByAreas = i;
                        continue;
                    }
                    if (excel.GetActiveSheetName() == "Исходные данные")
                    {
                        InitDataSheet = i;
                        continue;
                    }
                    if (excel.GetActiveSheetName() == "СВОД")
                    {
                        SvodSheet = i;
                        continue;
                    }
                    if (excel.GetActiveSheetName() == "ТР АНК Башнефть")
                    {
                        TRsheet = i;
                        continue;
                    }
                }
                if (SvodSheet == -1 || InitDataSheet == -1 || TRsheet == -1 || ByAreas == -1)
                {
                    excel.EnableEvents = true;
                    excel.Quit();
                    System.Windows.Forms.MessageBox.Show("Неверный формат шаблона отчета!", "Ошибка!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return;
                }
                double[] values, gtmVals;
                vals = new double[19][];
                double[][] div = new double[2][];

                for (i = 0; i < vals.Length; i++)
                {
                    vals[i] = new double[32];
                }
                div[0] = new double[6]; div[1] = new double[6];

                double[][] rang = new double[5][];
                List<double> sortPress = new List<double>();
                List<double> sortLiq = new List<double>();
                List<double> sortComp = new List<double>();
                for (i = 0; i < rang.Length; i++)
                {
                    rang[i] = new double[26];
                }

                List<int> oilfieldAreaObjects = new List<int>();
                List<int> areaPlastCodes = new List<int>();
                List<int> lastAreaPlastCodes = new List<int>();
                List<int> gridPlastCodes = new List<int>();
                double AccumOil, sum;
                int ip, count;
                double Square, Volume, NBZ, AverageThickness;
                double CoefCorrelation;
                List<Grid> grids = new List<Grid>();
                List<Grid> loadedGrids = new List<Grid>();
                List<Well> wells;
                int rowCounter = 1;
                for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                {
                    of = project.OilFields[ofInd];
#if DEBUG
                    //if (of.Name != "АРЛАНСКОЕ") continue;
                    //if (of.Name != "ИЛЬИНСКОЕ") continue;
                    //if (ofInd > 2) break;
#endif
                    of.LoadPVTParamsFromCache(worker, e);
                    of.LoadMerFromCache(worker, e, false);
                    of.LoadGTMFromCache(worker, e);
                    of.LoadWellResearchFromCache(worker, e);
                    of.LoadGisFromCache(worker, e);
                    of.LoadPerfFromCache(worker, e);

                    for (int aInd = 0; aInd < of.Areas.Count; aInd++)
                    {
                        area = (Area)of.Areas[aInd];

                        if (of.Name == "КУНГАКСКОЕ" && area.Name.IndexOf("ТТНК") > -1) continue;
                        if (int.TryParse(area.Name, out i)) continue;

                        for (i = 0; i < vals.Length; i++)
                        {
                            for (j = 0; j < vals[i].Length; j++)
                            {
                                vals[i][j] = 0;
                            }
                        }
                        
                        areaPlastCodes.Clear();
                        for (i = 0; i < of.MerStratumCodes.Count; i++)
                        {
                            stratumNode = dictStratum.GetStratumTreeNode(of.MerStratumCodes[i]);

                            if (area.PlastCode == of.MerStratumCodes[i])
                            {
                                areaPlastCodes.Add(of.MerStratumCodes[i]);
                            }
                            else if (stratumNode.GetParentLevelByCode(area.PlastCode) > -1 && oilfieldAreaObjects.IndexOf(of.MerStratumCodes[i]) == -1)
                            {
                                areaPlastCodes.Add(of.MerStratumCodes[i]);
                            }
                        }
                        if (areaPlastCodes.IndexOf(area.PlastCode) == -1)
                        {
                            areaPlastCodes.Add(area.PlastCode);
                        }
                        count = 0;
                        for (i = 0; i < lastAreaPlastCodes.Count; i++)
                        {
                            if (areaPlastCodes.IndexOf(lastAreaPlastCodes[i]) == -1)
                            {
                                count = 0;
                                break;
                            }
                            count++;
                        }
                        if (count != lastAreaPlastCodes.Count)
                        {
                            for (i = 0; i < loadedGrids.Count; i++)
                            {
                                if (loadedGrids[i].DataLoaded) loadedGrids[i].FreeDataMemory();
                            }
                            loadedGrids.Clear();
                        }
                        lastAreaPlastCodes.Clear();
                        lastAreaPlastCodes.AddRange(areaPlastCodes.ToArray());
                        /// Параметры массива
                        /// 0 - Среднее пластовое давление
                        /// 1 - Приемистость
                        /// 2 - Дебит нефти
                        /// 3 - Дебит жидкости
                        /// 4 - Обводненность
                        /// 5 - Фонд наг
                        /// 6 - Фонд доб
                        /// 7 - Среднее забойное давление
                        /// 8 - НИЗ, тыс.т
                        /// 9 - Добыча нефти
                        /// 10 - Накопленная добыча нефти
                        /// 11 - Добыча жидкости
                        /// 12 - Закачка
                        /// 13 - Время работы доб
                        /// 14 - Время работы наг
                        /// 15 - Добыча воды в м3 в пласт.усл.
                        /// 16 - Добыча нефти в м3 в пласт.усл.
                        /// 17 - Компенсация, %
                        /// 18 - Кпрод
                        int countPredict = 6;
                        int last = vals[1].Length - countPredict - 1;
                        int lastPredict = vals[1].Length - 1;

                        fund = 0;
                        AccumOil = 0;
                        if (area.AllWellList != null && area.AllWellList.Length > 0)
                        {
                            for (int wInd = 0; wInd < area.AllWellList.Length; wInd++)
                            {
                                w = (Well)area.AllWellList[wInd];
                                if (!w.MerLoaded) continue;
                                find = false;
                                for (i = 0; i < w.mer.Count; i++)
                                {
                                    if (w.mer.Items[i].Date > startDate) break;
                                    if (areaPlastCodes.IndexOf(w.mer.Items[i].PlastId) > -1)
                                    {
                                        if (!find)
                                        {
                                            fund++;
                                            find = true;
                                        }
                                        AccumOil += w.mer.Items[i].Oil;
                                    }
                                }
                            }
                        }
                        vals[10][0] = AccumOil;
                        for (int wInd = 0; wInd < area.WellList.Length; wInd++)
                        {
                            w = (Well)area.WellList[wInd];
                            if (!w.MerLoaded) continue;

                            comp = new MerComp(w.mer);
                            
                            for (i = 0; i < comp.Count; i++)
                            {
                                if (comp[i].Date < startDate) continue;
                                month = (comp[i].Date.Year - startDate.Year) * 12 - startDate.Month + comp[i].Date.Month;
                                
                                for (ip = 0; ip < comp[i].PlastItems.Count; ip++)
                                {
                                    if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                    vals[9][month] += comp[i].PlastItems[ip].Oil;
                                    vals[11][month] += comp[i].PlastItems[ip].Liq;
                                    vals[12][month] += comp[i].PlastItems[ip].Inj;
                                    vals[15][month] += (comp[i].PlastItems[ip].LiqV - comp[i].PlastItems[ip].OilV) * area.pvt.WaterVolumeFactor;
                                    vals[16][month] += comp[i].PlastItems[ip].OilV * area.pvt.OilVolumeFactor;
                                }
                                
                                // фонд
                                wtProd = comp[i].TimeItems.GetProductionTime(areaPlastCodes);
                                wtInj = comp[i].TimeItems.GetInjectionTime(areaPlastCodes);
                                if (wtInj.WorkTime > 0) vals[5][month]++;
                                if (wtProd.WorkTime > 0) vals[6][month]++;
                                vals[13][month] += wtProd.WorkTime + wtProd.CollTime;
                                vals[14][month] += wtInj.WorkTime + wtInj.CollTime;
                            }
                        }
                        if (area.AllWellList != null && area.AllWellList.Length > 0)
                        {
                            Pressure = MonitoringReporter.GetPressureByWells(new List<Well>(area.AllWellList), dictStratum, area.PlastCode, startDate, vals[0].Length, 0);
                            for (i = 0; i < vals[0].Length - countPredict; i++)
                            {
                                if (Pressure[i][2] > 0) vals[0][i] = Pressure[i][1] / Pressure[i][2];
                            }
                            Pressure = MonitoringReporter.GetZabPressureByWells(new List<Well>(area.AllWellList), dictStratum, area.PlastCode, startDate, vals[0].Length);
                            for (i = 0; i < vals[7].Length - countPredict; i++)
                            {
                                if (Pressure[i][2] > 0) vals[7][i] = Pressure[i][1] / Pressure[i][2];
                            }
                        }

                        // Запасы
                        Square = 0;
                        Volume = 0;
                        AverageThickness = 0;
                        NBZ = 0;
                        if (area.Params.InitOilReserv == 0)
                        {
                            // ищем сетки
                            find = false;
                            gridPlastCodes.Clear();
                            for (j = 0; j < areaPlastCodes.Count; j++)
                            {
                                grids.Clear();
                                for (i = 0; i < of.Grids.Count; i++)
                                {
                                    if (of.Grids[i].GridType == 2 && of.Grids[i].StratumCode == areaPlastCodes[j])
                                    {
                                        find = false;
                                        stratumNode = dictStratum.GetStratumTreeNode(areaPlastCodes[j]);
                                        for (k = 0; k < areaPlastCodes.Count; k++)
                                        {
                                            if (k != j && stratumNode.GetParentLevelByCode(areaPlastCodes[k]) > -1)
                                            {
                                                find = true;
                                            }
                                        }
                                        if (!find)
                                        {
                                            grids.Add(of.Grids[i]);
                                            if (gridPlastCodes.IndexOf(of.Grids[i].StratumCode) == -1)
                                            {
                                                gridPlastCodes.Add(of.Grids[i].StratumCode);
                                            }
                                        }
                                    }
                                }
                                if (grids.Count > 0)
                                {
                                    NBZ = 0;
                                    for (i = 0; i < grids.Count; i++)
                                    {
                                        if (!grids[i].DataLoaded)
                                        {
                                            try
                                            {
                                                of.LoadGridDataFromCacheByIndex(worker, e, grids[i].Index, false);
                                                loadedGrids.Add(grids[i]);
                                            }
                                            catch(OutOfMemoryException ex)
                                            {
                                                for (int g = 0; g < loadedGrids.Count; g++)
                                                {
                                                    loadedGrids[g].FreeDataMemory();
                                                }
                                                GC.GetTotalMemory(true);
                                                of.LoadGridDataFromCacheByIndex(worker, e, grids[i].Index, false);
                                                loadedGrids.Add(grids[i]);
                                            }
                                        }
                                        grids[i].CalcSumValues(worker, e, area.contour, out Square, out Volume);
                                        if (Square > 0) AverageThickness = Volume / Square;
                                        NBZ = area.pvt.OilDensity * area.pvt.Porosity * area.pvt.OilInitialSaturation * Volume / area.pvt.OilVolumeFactor;
                                        if (NBZ > 0) break;
                                    }
                                }
                                else
                                {
                                    NBZ = area.Params.InitOilReserv;
                                }
                                if (NBZ > 0) break;
                            }
                        }
                        else
                        {
                            NBZ = area.Params.InitOilReserv;
                        }
                        double maxKprod = double.MinValue, minKprod = double.MaxValue;
                        for (i = 0; i < vals[0].Length - countPredict; i++)
                        {
                            vals[1][i] = (vals[14][i] > 0) ? vals[12][i] * 24 / vals[14][i] : 0;
                            vals[2][i] = (vals[13][i] > 0) ? vals[9][i] * 24 / vals[13][i] : 0;
                            vals[3][i] = (vals[13][i] > 0) ? vals[11][i] * 24 / vals[13][i] : 0;
                            vals[4][i] = (vals[11][i] > 0) ? (vals[11][i] - vals[9][i]) * 100.0 / vals[11][i] : 0;
                            vals[8][i] = NBZ * area.pvt.KIN;
                            vals[17][i] = vals[15][i] + vals[16][i] > 0 ? vals[12][i] * area.pvt.WaterVolumeFactor * 100 / (vals[15][i] + vals[16][i]) : 0;
                            if (i > 0) vals[10][i] = vals[10][i - 1] + vals[9][i];
                            if (vals[0][i] - vals[7][i] != 0)
                            {
                                vals[18][i] = vals[3][i] / (vals[0][i] - vals[7][i]);
                                if (maxKprod < vals[18][i]) maxKprod = vals[18][i];
                                if (minKprod > vals[18][i]) minKprod = vals[18][i];
                            }
                        }

                        #region rang
                        for (i = 0; i < rang.Length; i++)
                        {
                            for (j = 0; j < rang[i].Length; j++)
                            {
                                rang[i][j] = 0;
                            }
                        }
                        j = vals[0].Length - countPredict - rang[0].Length;
                        sortPress.Clear();
                        sortLiq.Clear();
                        sortComp.Clear();
                        for (i = 0; i < rang[0].Length; i++)
                        {
                            sortPress.Add(vals[0][i + j]);
                            sortLiq.Add(vals[3][i + j]);
                            sortComp.Add(vals[17][i + j]);
                        }
                        sortPress.Sort();
                        sortLiq.Sort();
                        sortComp.Sort();
                        for (i = 0; i < rang[0].Length; i++)
                        {
                            rang[0][i] = sortPress.IndexOf(vals[0][i + j]) + 1;

                            if (vals[17][i + j] > 0)
                            {
                                rang[1][i] = sortComp.IndexOf(vals[17][i + j]) + 1;
                            }
                            else
                            {
                                rang[1][i] = sortLiq.IndexOf(vals[3][i + j]) + 1;
                            }
                            
                        }
                        for (i = 0; i < rang[0].Length - 2; i++)
                        {
                            rang[2][i] = Math.Pow(rang[0][i + 2] - rang[1][i], 2);
                            rang[3][i] = (i > 0) ? rang[2][i] + rang[3][i - 1] : rang[2][i];
                        }
                        CoefCorrelation = 1 - 6 * rang[3][rang[3].Length - 3] / (rang[0].Length * (rang[0].Length * rang[0].Length - 1));
                        #endregion

                        #region Прогноз
                        // расчет коэф. тренда среднего давления k, b
                        dt = project.maxMerDate.AddMonths(6);
                        int days, lastPredictDays = (dt.AddMonths(1) - dt).Days;
                        double K, B, R2;
                        int counter;
                        PointD[] points = new PointD[24];

                        double sumYear = 0;

                        for (ip = 0; ip < countPredict; ip++)
                        {
                            j = last + ip + 1;
                            dt = project.maxMerDate.AddMonths(ip + 1);
                            days = (dt.AddMonths(1) - dt).Days;

                            vals[1][j] = vals[1][j - 1];
                            vals[5][j] = vals[5][j - 1];
                            vals[6][j] = vals[6][j - 1];
                            vals[8][j] = vals[8][j - 1];
                            vals[17][j] = vals[17][j - 1];
                            vals[4][j] = vals[4][j - 1];

                            counter = 0;
                            for (i = 0; i < points.Length; i++)
                            {
                                if (vals[17][last - 25 + i] > 0) counter++;
                                if (counter > 2) break;
                            }

                            if (vals[17][j - 2] > 0 && counter > 2)
                            {
                                for (i = 0; i < points.Length; i++)
                                {
                                    points[i].Y = vals[0][last - 23 + i] - vals[0][last - 24 + i];
                                    points[i].X = vals[17][last - 25 + i];
                                }
                                Geometry.GetLinearTrend(points, out K, out B, out R2);
                                if (K > 0)
                                {
                                    vals[0][j] = K * vals[17][j - 2] + B + vals[0][j - 1];
                                }
                                else
                                {
                                    sum = 0;
                                    count = 0;
                                    for (i = 0; i < 3; i++)
                                    {
                                        if (vals[0][j - 1 - i] > 0)
                                        {
                                            sum += vals[0][j - 1 - i];
                                            count++;
                                        }
                                    }
                                    if (count > 0) vals[0][j] = sum / count;
                                }
                            }
                            else
                            {
                                for (i = 0; i < points.Length; i++)
                                {
                                    points[i].Y = vals[0][last - 23 + i] - vals[0][last - 24 + i];
                                    points[i].X = vals[3][last - 25 + i];
                                }
                                Geometry.GetLinearTrend(points, out K, out B, out R2);
                                if(vals[3][j - 2] > 0)
                                {
                                    vals[0][j] = (K * vals[3][j - 2] + B) / 100 + vals[0][j - 1];
                                }
                                else
                                {
                                    vals[0][j] = vals[0][j - 1];
                                }
                            }

                            // обводненность на прогноз протягивается
                            //if (vals[8][j - 1] == 0)
                            //{
                            //    sum = 0;
                            //    count = 0;
                            //    for (i = 0; i < 3; i++)
                            //    {
                            //        if (vals[4][j - 1 - i] > 0)
                            //        {
                            //            sum += vals[4][j - 1 - i];
                            //            count++;
                            //        }
                            //    }
                            //    if (count > 0) vals[4][j] = sum / count;
                            //}
                            //else
                            //{
                            //    vals[4][j] = vals[4][j - 1] + vals[2][j - 1] * vals[6][j - 1] * days / vals[8][j] * 100;
                            //}

                            if ((vals[0][j] - vals[7][j - 1]) != 0) vals[3][j] = vals[3][j - 1] + vals[3][j - 1] * (vals[0][j] - vals[0][j - 1]) / (vals[0][j] - vals[7][j - 1]);
                            vals[2][j] = vals[3][j] * (1 - vals[4][j] / 100);
                            if(vals[0][j - 1] != vals[0][j])
                            {
                                vals[7][j] = 0;
                                if (vals[0][j - 1] - vals[7][j - 1] != 0 && vals[3][j - 1] != 0)
                                {
                                    vals[7][j] = -(vals[3][j] / (vals[3][j - 1] / (vals[0][j - 1] - vals[7][j - 1])) - vals[0][j]);
                                }
                            }
                            else
                            {
                                vals[7][j] = vals[7][j - 1];
                            }
                            
                            vals[10][j] = vals[10][j - 1] + vals[2][j] * days * vals[6][j];
                            vals[9][j] = vals[2][j] * vals[6][j] * days;
                            ///if (vals[8][j] > 0) vals[14][j] = vals[9][j] * 100 / vals[8][j];   // темп отбора от низ в 14
                        }
                        // div
                        if ((vals[0][lastPredict] - vals[7][lastPredict]) > 0)
                        {
                            div[0][0] = vals[3][lastPredict] / (vals[0][lastPredict] - vals[7][lastPredict]) * (vals[0][lastPredict] - vals[0][last]) * (1 - vals[4][lastPredict] / 100);
                            if ((vals[0][last] - vals[7][last]) > 0)
                            {
                                div[0][1] = (vals[0][last] - vals[7][last]) * (vals[3][lastPredict] / (vals[0][lastPredict] - vals[7][lastPredict]) - vals[3][last] / (vals[0][last] - vals[7][last])) * (1 - vals[4][lastPredict] / 100);
                            }
                            div[0][2] = -vals[3][lastPredict] / (vals[0][lastPredict] - vals[7][lastPredict]) * (vals[7][lastPredict] - vals[7][last]) * (1 - vals[4][lastPredict] / 100);
                        }


                        div[0][3] = (vals[3][lastPredict] - vals[3][last]) * (1 - vals[4][lastPredict] / 100);
                        div[0][4] = -(vals[3][last] * (vals[4][lastPredict] - vals[4][last]) / 100);
                        div[0][5] = ((vals[3][lastPredict] - vals[3][last]) * (1 - vals[4][lastPredict] / 100)) - (vals[3][last] * (vals[4][lastPredict] - vals[4][last]) / 100);

                        if ((vals[0][lastPredict] - vals[7][lastPredict]) > 0)
                        {
                            div[1][0] = vals[3][lastPredict] * vals[6][lastPredict] / (vals[0][lastPredict] - vals[7][lastPredict]) * (vals[0][lastPredict] - vals[0][last]) * (1 - vals[4][lastPredict] / 100);
                            if(vals[0][last] - vals[7][last] > 0)
                            {
                                div[1][1] = (vals[0][last] - vals[7][last]) * (vals[3][lastPredict] * vals[6][lastPredict] / (vals[0][lastPredict] - vals[7][lastPredict]) - vals[3][last] * vals[6][lastPredict] / (vals[0][last] - vals[7][last])) * (1 - vals[4][lastPredict] / 100);
                            }
                            div[1][2] = -vals[3][lastPredict] * vals[6][lastPredict] / (vals[0][lastPredict] - vals[7][lastPredict]) * (vals[7][lastPredict] - vals[7][last]) * (1 - vals[4][lastPredict] / 100);
                        }
                        div[1][3] = (vals[3][lastPredict] * vals[6][lastPredict] - vals[3][last] * vals[6][lastPredict]) * (1 - vals[4][lastPredict] / 100);
                        div[1][4] = -(vals[3][last] * vals[6][lastPredict] * (vals[4][lastPredict] - vals[4][last]) / 100);
                        div[1][5] = ((vals[3][lastPredict] * vals[6][lastPredict] - vals[3][last] * vals[6][lastPredict]) * (1 - vals[4][lastPredict] / 100)) - (vals[3][last] * vals[6][lastPredict] * (vals[4][lastPredict] - vals[4][last]) / 100);

                        sumYear = 0;
                        count = 0;
                        for (i = vals[9].Length - project.maxMerDate.Month - countPredict; i < vals[9].Length && count < 12; i++)
                        {
                            sumYear += vals[9][i];
                        }
                        #endregion

                        #region SatPressure + Невскрытые скважины
                        double SatPressure = 0;
                        string SealedWells = string.Empty, SealedWells2 = string.Empty;
                        double SealedOilZone = 0;
                        int ind;
                        if (of.PVTList != null && of.PVTList.Count > 0)
                        {
                            PVTParamsItem item;
                            item = of.PVTList.GetItemByPlastCode(area.OilFieldAreaCode, area.PlastCode);
                            if (item.IsEmpty) item = of.PVTList.GetItemByPlastHierarchy(area.OilFieldAreaCode, area.PlastCode, dictStratum);
                            if (item.IsEmpty) item = of.PVTList.GetItemByPlastCode(-1, area.PlastCode);
                            if (!item.IsEmpty) SatPressure = item.SaturationPressure;
                            
                        }
                        if (area.AllWellList != null)
                        {
                            List<Well> allWells = new List<Well>(area.AllWellList);
                            List<Well> workWells = new List<Well>(area.WellList);
                            for (i = 0; i < allWells.Count; i++)
                            {
                                if (workWells.IndexOf(allWells[i]) > -1)
                                {
                                    allWells.RemoveAt(i);
                                    i--;
                                }
                            }
                            StratumTreeNode mainNode = dictStratum.GetStratumTreeNode(area.PlastCode);
                            StratumTreeNode node = null;
                            double sumWellOilZone;
                            if (allWells.Count > 0)
                            {
                                List<int> initSat = new List<int>(new int[] {1, 3, 5, 8, 11 });
                                wells = new List<Well>();
                                for (i = 0; i < allWells.Count; i++)
                                {
                                    w = allWells[i];
                                    sumWellOilZone = 0;
                                    //if (!w.GisLoaded) of.LoadGisFromCache(w.Index);
                                    //if (!w.PerfLoaded) of.LoadPerfFromCache(w.Index);
                                    find = true;
                                    if (w.MerLoaded)
                                    {
                                        comp = new MerComp(w.mer);
                                        ind = comp.GetIndexByDate(project.maxMerDate);
                                        if (ind > -1 && comp[ind].MethodId != 8)
                                        {
                                            find = false;
                                            wtProd = comp[ind].TimeItems.GetAllProductionTime();

                                            find = comp[ind].SumLiquid == 0 && comp[ind].SumOil == 0 && comp[ind].SumInjection == 0 && comp[ind].SumInjectionGas == 0;
                                            find = find || ((comp[ind].SumLiquid > 0) && ((comp[ind].SumLiquid - comp[ind].SumOil) / comp[ind].SumLiquid > 0.98));
                                            find = find || ((wtProd.WorkTime + wtProd.CollTime > 0) && (comp[ind].SumOil * 24 / (wtProd.WorkTime + wtProd.CollTime) < 0.5));
                                        }
                                    }
                                    if (find && w.GisLoaded && w.PerfLoaded)
                                    {
                                        node = null;
                                        for (j = 0; j < w.gis.Count; j++)
                                        {
                                            if(!w.gis.Items[j].Collector || initSat.IndexOf(w.gis.Items[j].Sat0) == -1) continue;

                                            if(node == null || node.StratumCode != w.gis.Items[j].PlastId)
                                            {
                                                node = dictStratum.GetStratumTreeNode(w.gis.Items[j].PlastId);
                                            }
                                            if (dictStratum.EqualsStratum(mainNode, node))
                                            {
                                                find = false;
                                                for (t = 0; t < w.perf.Count; t++)
                                                {
                                                    find = (w.gis.Items[j].H > w.perf.Items[t].Bottom) || (w.gis.Items[j].H + w.gis.Items[j].L < w.perf.Items[t].Top);
                                                    if (!find) break;
                                                }
                                                if (find)
                                                {
                                                    if (wells.IndexOf(w) == -1) wells.Add(w);
                                                    SealedOilZone += w.gis.Items[j].L;
                                                    sumWellOilZone += w.gis.Items[j].L;
                                                }
                                            }
                                        }
                                    }
                                    if (sumWellOilZone > 0)
                                    {
                                        if (SealedWells2.Length > 0) SealedWells2 += ", ";
                                        SealedWells2 += string.Format("{0} ({1:0.#} м)", w.Name, sumWellOilZone);
                                    }
                                }
                                for (i = 0; i < wells.Count; i++)
                                {
                                    if (i > 0) SealedWells += ", ";
                                    SealedWells += wells[i].Name;
                                }
                            }
                        }
                        #endregion

                        #region Параметры ГТМ за 2011 - н.в.
                        int countGtm = 0;
                        
                        gtmVals = new double[70];
                        wells = new List<Well>(area.WellList);
                        for (k = 0; k < GtmCodes.Count; k++)
                        {
                            GtmWellNames[k] = string.Empty;
                        }
                        for (k = 0; k < GtmCodes.Count; k++)
                        {
                            gtmWells[0] = MonitoringReporter.GetWellsWithGTM(wells, GtmCodes[k], areaPlastCodes, new List<int>(new int[] { startYear, startYear + 1, startYear + 2 }), out countGtm);
                            for (i = 0; i < gtmWells[0].Count; i++)
                            {
                                if (GtmWellNames[k].Length > 0) GtmWellNames[k] += ", ";
                                GtmWellNames[k] += gtmWells[0][i].Name;
                            }
                            if (gtmWells[0].Count > 0)
                            {
                                values = MonitoringReporter.GetGtmWellsParams(gtmWells[0], GtmCodes[k], areaPlastCodes, new List<int>(new int[] { startYear, startYear + 1, startYear + 2 }));
                                gtmVals[0 + k * 6] = values[0];
                                gtmVals[1 + k * 6] = values[1];
                                gtmVals[2 + k * 6] = values[3];
                                gtmVals[3 + k * 6] = values[2];
                                gtmVals[4 + k * 6] = gtmWells[0].Count;
                                gtmVals[5 + k * 6] = countGtm;
                            }
                            else if (k == 0)
                            {
                                break;
                            }
                        }
                        #endregion

                        #region Выгрузка в отчет
                        Square = area.contour._points.GetSquare();
                        for (ip = 0; ip < paramNames.Length; ip++)
                        {
                            row = new object[11 + vals[ip].Length];
                            row[0] = of.ParamsDict.NGDUName;
                            row[1] = of.Name;
                            if (of.Name == "АРЛАНСКОЕ")
                            {
                                row[1] = string.Format("{0}({1})", of.Name, dictArea.GetShortNameByCode(area.OilFieldAreaCode));
                            }
                            row[2] = dictArea.GetShortNameByCode(area.OilFieldAreaCode);
                            row[3] = area.Name; 
                            stratumNode = dictStratum.GetStratumTreeNode(area.PlastCode);
                            row[4] = stratumNode == null ? string.Empty : stratumNode.Name;
                            str = string.Empty;
                            for (i = 0; i < areaPlastCodes.Count; i++)
                            {
                                if (str.Length > 0) str += ", ";
                                str += dictStratum.GetShortNameByCode(areaPlastCodes[i]);
                            }
                            row[5] = str;
                            row[6] = (string)row[1] + area.Name + paramNames[ip];
                            row[7] = Square;
                            row[8] = fund;
                            row[9] = area.pvt.SaturationPressure;
                            row[10] = paramNames[ip];
                            for (j = 0; j < vals[ip].Length - countPredict; j++)
                            {
                                row[11 + j] = (ip != 11) ? vals[ip][j] : vals[17][j];
                            }
                            out_list.Add(row);
                        }

                        int col = 0;
                        row = new object[65];
                        row[col++] = rowCounter++;
                        row[col++] = of.ParamsDict.NGDUName;
                        row[col] = of.Name;
                        if (of.Name == "АРЛАНСКОЕ")
                        {
                            row[col] = string.Format("{0}({1})", of.Name, dictArea.GetShortNameByCode(area.OilFieldAreaCode));
                        }
                        col++;
                        row[col++] = dictArea.GetShortNameByCode(area.OilFieldAreaCode);
                        row[col++] = area.Name;
                        stratumNode = dictStratum.GetStratumTreeNode(area.PlastCode);
                        row[col++] = stratumNode == null ? string.Empty : stratumNode.Name;
                        str = string.Empty;
                        for (i = 0; i < areaPlastCodes.Count; i++)
                        {
                            if (str.Length > 0) str += ", ";
                            str += dictStratum.GetShortNameByCode(areaPlastCodes[i]);
                        }
                        row[col++] = str;
                        row[col++] = area.pvt.InitialPressure;
                        row[col++] = (area.pvt.SaturationPressure == 0) ? SatPressure : area.pvt.SaturationPressure;
                        row[col++] = vals[15][last] + vals[16][last] > 0 ? vals[12][last] * area.pvt.WaterVolumeFactor * 100 / (vals[15][last] + vals[16][last]) : 0;
                        row[col++] = CoefCorrelation;
                        row[col++] = (vals[8][last] - vals[10][last]) / 1000;
                        row[col++] = (vals[8][last] > 0) ? vals[10][last] / vals[8][last] * 100 : 0;
                        row[col++] = (vals[8][last] > 0 && vals[10][last] > 0) ? vals[4][last] / (vals[10][last] / vals[8][last] * 100) : 0;
                        row[col++] = (vals[8][last] > 0) ? vals[9][last] * 100 / vals[8][last] : vals[8][last];
                        row[col++] = (vals[9][last] > 0) ? (vals[8][last] - vals[10][last]) / vals[9][last] / 12 : 0;
                        row[col++] = vals[6][last];
                        row[col++] = (vals[6][last] > 0) ? (vals[8][last] - vals[10][last]) / 1000 / vals[6][last] : (vals[8][last] - vals[10][last]) / 1000;
                        row[col++] = (vals[6][last] > 0) ? Square / 1000000 / vals[6][last] * 100 : 0;
                        row[col++] = ((vals[5][last] + vals[6][last]) > 0) ? Square / 1000000 / (vals[5][last] + vals[6][last]) * 100 : 0;
                        row[col++] = (fund > 0) ? Square / 1000000 / fund * 100 : 0;
                        row[col++] = sumYear;
                        row[col++] = SealedWells;                                   // Скважины невскрытые на объект
                        row[col++] = Math.Round(SealedOilZone, 2);                  // Невскрытая толщина
                        row[col++] = (minKprod == double.MaxValue) ? 0 : minKprod;
                        row[col++] = (maxKprod == double.MinValue) ? 0 : maxKprod;
                        row[col++] = vals[18][last];

                        out_selaedWells.Add(new object[] { SealedWells2 });
                        
                        // 27
                        row[col++] = vals[0][last];
                        row[col++] = vals[7][last];
                        row[col++] = vals[2][last];
                        row[col++] = vals[3][last];
                        row[col++] = vals[4][last];
                        row[col++] = vals[0][lastPredict];
                        row[col++] = vals[7][lastPredict];
                        row[col++] = vals[2][lastPredict];
                        row[col++] = vals[3][lastPredict];
                        row[col++] = vals[4][lastPredict];
                        row[col++] = div[0][0];
                        row[col++] = div[0][1];
                        row[col++] = div[0][2];
                        row[col++] = div[0][3];
                        row[col++] = div[0][4];
                        row[col++] = div[0][5];
                        if (vals[2][last] > 0)
                        {
                            row[col++] = div[0][3] / vals[2][last] / 12 * 100;
                            row[col++] = div[0][4] / vals[2][last] / 12 * 100;
                            row[col++] = div[0][5] / vals[2][last] / 12 * 100;
                        }
                        else
                        {
                            col += 3;
                        }
                        // 46
                        row[col++] = vals[0][last];
                        row[col++] = vals[7][last];
                        row[col++] = vals[2][last] * vals[6][last];
                        row[col++] = vals[3][last] * vals[6][last];
                        row[col++] = vals[4][last];
                        row[col++] = vals[0][lastPredict];
                        row[col++] = vals[7][lastPredict];
                        row[col++] = vals[2][lastPredict] * vals[6][lastPredict];
                        row[col++] = vals[3][lastPredict] * vals[6][lastPredict];
                        row[col++] = vals[4][lastPredict];
                        row[col++] = div[1][0];
                        row[col++] = div[1][1];
                        row[col++] = div[1][2];
                        row[col++] = div[1][3];
                        row[col++] = div[1][4];
                        row[col++] = div[1][5];
                        if (vals[9][last] > 0)
                        {
                            row[col++] = div[1][3] / vals[9][last] / 12 * 100;
                            row[col++] = div[1][4] / vals[9][last] / 12 * 100;
                            row[col++] = div[1][5] / vals[9][last] / 12 * 100;
                        }
                        out_list2.Add(row);

                        // данные по ГТМ
                        row = new object[70];
                        for (i = 0; i < GtmCodes.Count; i++)
                        {
                            row[0 + i * 7] = (vals[5][last] + vals[6][last] > 0) ? gtmVals[5 + i * 6] / (vals[5][last] + vals[6][last]) : 0;
                            row[1 + i * 7] = gtmVals[5 + i * 6];
                            row[2 + i * 7] = gtmVals[4 + i * 6];
                            row[3 + i * 7] = GtmWellNames[i];
                            row[4 + i * 7] = gtmVals[1 + i * 6] - gtmVals[0 + i * 6];
                            row[5 + i * 7] = (gtmVals[0 + i * 6] > 0) ? gtmVals[1 + i * 6] / gtmVals[0 + i * 6] : 0;
                            row[6 + i * 7] = gtmVals[3 + i * 6];
                        }
                        out_list2_gtm.Add(row);
                        #endregion
                    }
                    for (i = 0; i < loadedGrids.Count; i++)
                    {
                        if (loadedGrids[i].DataLoaded) loadedGrids[i].FreeDataMemory();
                    }
                    loadedGrids.Clear();
                    of.PVTList = null;
                    of.ClearWellResearchData(false);
                    of.ClearGTMData(false);
                    of.ClearGisData(false);
                    of.ClearPerfData(false);
                    of.ClearMerData(true);
                    worker.ReportProgress(100, userState);
                }
                userState.WorkCurrentTitle = "Выгрузка данных в Microsoft Excel";
                worker.ReportProgress(0, userState);

                if (out_list.Count > 0 && excel != null)
                {
                    int templateBook = excel.GetWorkbooksCount() - 1;
                    excel.SetActiveSheet(InitDataSheet);
                    excel.SetRowsArray(worker, e, 1, 0, out_list);

                    dt = startDate;
                    row = new object[26];
                    for (int h = 0; h < 26; h++)
                    {
                        row[h] = dt;
                        dt = dt.AddMonths(1);
                    }
                    out_list.Clear();
                    out_list.Add(row);
                    excel.SetRowsArray(worker, e, 0, 11, out_list);
 
                    excel.SetActiveSheet(SvodSheet);
                    excel.SetRowsArray(worker, e, 5, 0, out_list2);
                    excel.SetValue(1, 21, string.Format("Добыча нефти за {0}г, т", project.maxMerDate.Year));
                    excel.SetValue(1, 27, string.Format("текущее фактическое значение ({0:MM.yyyy})", project.maxMerDate));
                    excel.SetValue(1, 32, string.Format("расчет на конец периода ({0:MM.yyyy})", project.maxMerDate.AddMonths(6)));
                    excel.SetValue(1, 46, string.Format("текущее фактическое значение ({0:MM.yyyy})", project.maxMerDate));
                    excel.SetValue(1, 51, string.Format("расчет на конец периода ({0:MM.yyyy})", project.maxMerDate.AddMonths(6)));
                    excel.SetValue(1, 79, string.Format("Средний прирост от проведенных РИР за {0}-{1}гг, т/сут", project.maxMerDate.Year - 2, project.maxMerDate.Year));
                    excel.SetValue(1, 91, string.Format("Средний прирост от проведенных ИДН за {0}-{1}гг, т/сут", project.maxMerDate.Year - 2, project.maxMerDate.Year));
                    excel.SetValue(1, 107, string.Format("Средний прирост от проведенных ГРП за {0}-{1}гг, т/сут", project.maxMerDate.Year - 2, project.maxMerDate.Year));
                    excel.SetValue(1, 108, string.Format("Средний прирост от проведенных ОПЗ за {0}-{1}гг, т/сут", project.maxMerDate.Year - 2, project.maxMerDate.Year));
                    excel.SetValue(1, 109, string.Format("Средний прирост от проведенных Реперф. за {0}-{1}гг, т/сут", project.maxMerDate.Year - 2, project.maxMerDate.Year));
                    excel.SetValue(1, 115, string.Format("Средний прирост от проведенных ПВЛГ за {0}-{1}гг, т/сут", project.maxMerDate.Year - 2, project.maxMerDate.Year));
                    excel.SetValue(1, 122, string.Format("Средний прирост от проведенных ГРП за {0}-{1}гг, т/сут", project.maxMerDate.Year - 2, project.maxMerDate.Year));

                    // гтм
                    excel.SetRowsArray(worker, e, 5, 126, out_list2_gtm);
                    excel.RangeRowAutoFill(5, 65, 5 + rowCounter, 124);

                    excel.SetRowsArray(worker, e, 5, 113, out_selaedWells);

                    if (fileNames.Length > 1 && File.Exists(fileNames[1]))
                    {
                        userState.WorkCurrentTitle = "Копирование данных Техрежима скважин..";
                        worker.ReportProgress(0, userState);

                        int InitTRsheet = -1;
                        int InitTRbook = -1;
                        excel.OpenWorkbook(fileNames[1]);
                        InitTRbook = excel.GetWorkbooksCount() - 1;
                        sheetCount = excel.GetWorkSheetsCount();
                        for (i = 0; i < sheetCount; i++)
                        {
                            excel.SetActiveSheet(i);
                            if (excel.GetActiveSheetName() == "АНК Башнефть")
                            {
                                InitTRsheet = i;
                            }
                            if (InitTRsheet != -1) break;
                        }

                        if (InitTRbook != -1 && InitTRsheet != -1 && TRsheet != -1)
                        {
                            int length = 0;
                            excel.SetActiveWorkbook(InitTRbook);
                            string trDate = excel.GetValue(1, 6).ToString();
                            int lastRow = excel.GetLastUsedRow();
                            // Расчет последней строки
                            object[,] cells = excel.GetRangeValues(7, 0, lastRow, 0);
                            if (cells != null && cells.Length > 0)
                            {
                                length = cells.GetLength(0);
                                for (i = 0; i < length; i++)
                                {
                                    if (cells[i + 1, 1] == null)
                                    {
                                        lastRow = i + 6;
                                        break;
                                    }
                                }
                            }

                            // Заполняем столбец Ячейка Заводнения
                            int ofIndex, ofAreaCode, wellIndex;
                            string ofName, ofAreaName, wellName;
                            cells = excel.GetRangeValues(7, 1, lastRow, 7);
                            
                            if (cells != null && cells.Length > 0)
                            {
                                length = cells.GetLength(0);
                                object[] areaName;
                                List<object[]> areaNames = new List<object[]>();
                                for (i = 0; i < length; i++)
                                {
                                    if (cells[i + 1, 3] == null)
                                    {
                                        lastRow = i;
                                        break;
                                    }

                                    ofName = cells[i + 1, 3].ToString().Trim();
                                    ofAreaName = cells[i + 1, 4].ToString().Trim();
                                    if (ofName.StartsWith("им В_С_"))
                                    {
                                        ofName = "Им.В.С.Афанасьева";
                                        cells[i + 1, 3] = ofName;
                                    }
                                    else if (ofAreaName.StartsWith("Юсу"))
                                    {
                                        ofName = "ЮСУПОВСКАЯ ПЛОЩАДЬ";
                                        cells[i + 1, 3] = ofName;
                                    }
                                    
                                    wellName = cells[i + 1, 7].ToString().Trim();
                                    ofIndex = project.GetOFIndex(ofName);
                                    areaName = new object[1];
                                    wellIndex = -1;
                                    if (ofIndex > -1)
                                    {
                                        ofName = project.OilFields[ofIndex].Name;
                                        cells[i + 1, 3] = ofName;
                                        cells[i + 1, 1] = project.OilFields[ofIndex].ParamsDict.NGDUName;
                                        ofAreaCode = dictArea.GetCodeByShortName(ofAreaName);
                                        if (ofAreaCode > -1) wellIndex = project.OilFields[ofIndex].GetWellIndex(wellName, ofAreaCode);
                                        if (wellIndex == -1) wellIndex = project.OilFields[ofIndex].GetWellIndex(wellName);
                                        if (wellIndex > -1)
                                        {
                                            Well wx = project.OilFields[ofIndex].Wells[wellIndex];
                                            List<int> plastCodes = new List<int>();
                                            for (j = 0; j < wx.icons.Count;j++)
                                            {
                                                if (wx.icons[j].StratumCode > 0)
                                                {
                                                    plastCodes.Add(wx.icons[j].StratumCode);
                                                }
                                            }
                                            if (wx.BubbleIcons != null)
                                            {
                                                for (j = 0; j < wx.BubbleIcons.Count; j++)
                                                {
                                                    if (wx.BubbleIcons[j].StratumCode > 0)
                                                    {
                                                        plastCodes.Add(wx.BubbleIcons[j].StratumCode);
                                                    }
                                                }
                                            }
                                            find = false;
                                            
                                            if (plastCodes.Count > 0)
                                            {
                                                for (j = 0; j < project.OilFields[ofIndex].Areas.Count; j++)
                                                {
                                                    if (project.OilFields[ofIndex].Areas[j].WellList == null) continue;
                                                    for (k = 0; k < project.OilFields[ofIndex].Areas[j].WellList.Length; k++)
                                                    {
                                                        if (project.OilFields[ofIndex].Areas[j].WellList[k] == wx && plastCodes.IndexOf(project.OilFields[ofIndex].Areas[j].PlastCode) > -1)
                                                        {
                                                            for (int h = 0; h < plastCodes.Count; h++)
                                                            {
                                                                stratumNode = dictStratum.GetStratumTreeNode(plastCodes[h]);
                                                                if (stratumNode != null)
                                                                {
                                                                    if (stratumNode.StratumCode == project.OilFields[ofIndex].Areas[j].PlastCode ||
                                                                        stratumNode.GetParentLevelByCode(project.OilFields[ofIndex].Areas[j].PlastCode) > -1)
                                                                    {
                                                                        areaName[0] = project.OilFields[ofIndex].Areas[j].Name;
                                                                        find = true;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (find) break;
                                                    }
                                                    if (find) break;
                                                }
                                            }
                                        }
                                    }
                                    if (ofName.StartsWith("АРЛ"))
                                    {
                                        ofName += "(" + ofAreaName + ")";
                                        cells[i + 1, 3] = ofName;
                                    }
                                    
                                    areaNames.Add(areaName);
                                }
                                excel.SetRowsArray(7, 11, areaNames);
                                excel.SetRange(7, 1, lastRow, 7, cells);
                            }

                            for (int h = 0; h < project.OilFields.Count / 2; h++) worker.ReportProgress(100, userState);

                            // Сортируем по НГДУ, месторождению, Ячейке заводнения
                            MSOffice.Excel.SortField[] sortFields = new MSOffice.Excel.SortField[3];
                            sortFields[0].Init(1, MSOffice.Excel.XLSortOn.SortOnValues, MSOffice.Excel.XLSortOrder.xlAscending, MSOffice.Excel.XLSortDataOption.xlSortNormal);
                            sortFields[1].Init(3, MSOffice.Excel.XLSortOn.SortOnValues, MSOffice.Excel.XLSortOrder.xlAscending, MSOffice.Excel.XLSortDataOption.xlSortNormal);
                            sortFields[2].Init(11, MSOffice.Excel.XLSortOn.SortOnValues, MSOffice.Excel.XLSortOrder.xlAscending, MSOffice.Excel.XLSortDataOption.xlSortNormal);
                            excel.RangeSort(7, 0, lastRow, 126, sortFields);

                            int lastCol = excel.GetLastUsedColumn();

                            // Для системного десятичного разделителя ','
                            if (excel.GetDecimalSeparator() == ",")
                            {
                                string formula = excel.GetFormula(5, 76);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 76, formula);
                                }
                                formula = excel.GetFormula(5, 87);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 87, formula);
                                }
                                formula = excel.GetFormula(5, 88);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 88, formula);
                                }
                                formula = excel.GetFormula(5, 100);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 100, formula);
                                }
                                formula = excel.GetFormula(5, 101);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 101, formula);
                                }
                                formula = excel.GetFormula(5, 103);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 103, formula);
                                }
                                formula = excel.GetFormula(5, 104);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 104, formula);
                                }
                                formula = excel.GetFormula(5, 118);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 118, formula);
                                }
                                formula = excel.GetFormula(5, 119);
                                if (formula.Length > 0)
                                {
                                    formula = formula.Replace('.', ',');
                                    excel.SetFormula(5, 119, formula);
                                }
                            }

                            excel.RangeCopy(7, 0, lastRow, 10);
                            excel.SetActiveWorkbook(templateBook);
                            excel.SetActiveSheet(TRsheet);
                            excel.SetValue(1, 6, trDate);
                            excel.RangePaste(7, 0, lastRow, 10, MSOffice.Excel.XLPasteType.xlPasteValues, MSOffice.Excel.XLPasteSpecialOperation.None, false, false);
                            excel.RangeRowAutoFill(7, 11, lastRow, 11, MSOffice.Excel.XLAutoFillType.xlFillDefault);

                            excel.SetActiveWorkbook(InitTRbook);
                            excel.RangeCopy(7, 11, lastRow, 69);
                            excel.SetActiveWorkbook(templateBook);
                            excel.SetActiveSheet(TRsheet);
                            excel.RangePaste(7, 12, lastRow, 70, MSOffice.Excel.XLPasteType.xlPasteValues, MSOffice.Excel.XLPasteSpecialOperation.None, false, false);
                            excel.RangeRowAutoFill(7, 71, lastRow, 71, MSOffice.Excel.XLAutoFillType.xlFillDefault);

                            excel.SetActiveWorkbook(InitTRbook);
                            excel.RangeCopy(7, 70, lastRow, 74);
                            excel.SetActiveWorkbook(templateBook);
                            excel.SetActiveSheet(TRsheet);
                            excel.RangePaste(7, 72, lastRow, 76, MSOffice.Excel.XLPasteType.xlPasteValues, MSOffice.Excel.XLPasteSpecialOperation.None, false, false);
                            excel.RangeRowAutoFill(7, 77, lastRow, 83, MSOffice.Excel.XLAutoFillType.xlFillDefault);

                            excel.SetActiveWorkbook(InitTRbook);
                            excel.RangeCopy(7, 75, lastRow, 104);
                            excel.SetActiveWorkbook(templateBook);
                            excel.SetActiveSheet(TRsheet);
                            excel.RangePaste(7, 84, lastRow, 113, MSOffice.Excel.XLPasteType.xlPasteValues, MSOffice.Excel.XLPasteSpecialOperation.None, false, false);
                            excel.RangeRowAutoFill(7, 114, lastRow, 114, MSOffice.Excel.XLAutoFillType.xlFillDefault);

                            excel.SetActiveWorkbook(InitTRbook);
                            excel.RangeCopy(7, 105, lastRow, 126);
                            excel.SetActiveWorkbook(templateBook);
                            excel.SetActiveSheet(TRsheet);
                            excel.RangePaste(7, 115, lastRow, 136, MSOffice.Excel.XLPasteType.xlPasteValues, MSOffice.Excel.XLPasteSpecialOperation.None, false, false);
                            excel.SetActiveSheet(InitDataSheet);
                            
                            // Выполняем загрузку данных по кнопкам
                            excel.SetActiveSheet(TRsheet);
                            object[,] svodOf, svodArea, svod88, svod101, svod104, svod77, svod119;
                            object[,] oilfieldCol, areaCol, wellCol, col28, col30, col70, col76, col82, col115;
                            object[,] outTR, outGRP, outGRP2, outOPZ, outWs;
                            Dictionary<string, int> ofStartRows= new Dictionary<string,int>();
                            List<int[]> ofStartRowsData = new List<int[]>();

                            excel.SetActiveSheet(SvodSheet);
                            int svodLastRow = excel.GetLastUsedRow();
                            svodOf = excel.GetRangeValues(5, 2, svodLastRow, 2);
                            svodArea = excel.GetRangeValues(5, 4, svodLastRow, 4);
                            svod88 = excel.GetRangeValues(5, 87, svodLastRow, 87);
                            svod77 = excel.GetRangeValues(5, 76, svodLastRow, 76);
                            svod101 = excel.GetRangeValues(5, 100, svodLastRow, 100);
                            svod104 = excel.GetRangeValues(5, 103, svodLastRow, 103);
                            svod119 = excel.GetRangeValues(5, 118, svodLastRow, 118);

                            outTR = excel.GetRangeValues(5, 89, svodLastRow, 89);
                            outGRP = excel.GetRangeValues(5, 102, svodLastRow, 102);
                            outGRP2 = excel.GetRangeValues(5, 120, svodLastRow, 120);
                            outOPZ = excel.GetRangeValues(5, 105, svodLastRow, 105);
                            outWs = excel.GetRangeValues(5, 77, svodLastRow, 77);
                            
                            excel.SetActiveSheet(TRsheet);
                            oilfieldCol = excel.GetRangeValues(7, 3, lastRow, 3);
                            areaCol = excel.GetRangeValues(7, 12, lastRow, 12);
                            wellCol = excel.GetRangeValues(7, 7, lastRow, 7);
                            col28 = excel.GetRangeValues(7, 27, lastRow, 27);
                            col30 = excel.GetRangeValues(7, 29, lastRow, 29);
                            col70 = excel.GetRangeValues(7, 69, lastRow, 69);
                            col76 = excel.GetRangeValues(7, 75, lastRow, 75);
                            col82 = excel.GetRangeValues(7, 81, lastRow, 81);
                            col115 = excel.GetRangeValues(7, 114, lastRow, 114);

                            int key;
                            string lastOfName = string.Empty;
                            length = oilfieldCol.GetLength(0);
                            for(i = 1; i < length;i++)
                            {
                                ofName = oilfieldCol[i, 1].ToString();
                                if(ofName != lastOfName && !ofStartRows.TryGetValue(ofName, out key))
                                {
                                    ofStartRows.Add(ofName, ofStartRowsData.Count);
                                    if (ofStartRowsData.Count > 0) ofStartRowsData[ofStartRowsData.Count - 1][1] = i;
                                    ofStartRowsData.Add(new int[] { i, 0 });
                                    lastOfName = ofName;
                                }
                            }
                            string tr = string.Empty, grp = string.Empty, grp2 = string.Empty, opz = string.Empty, ws = string.Empty;
                            i = 1;
                            string str2, str3;
                            double value, value2, value3;
                            float countTR, countGRP, countGRP2, countOPZ, countWs;
                            while (i <= svodLastRow && svodOf[i, 1] != null)
                            {
                                ofName = svodOf[i, 1].ToString();
                                ofAreaName = svodArea[i, 1].ToString();
                                find = false;
                                tr = string.Empty; 
                                grp = string.Empty;
                                grp2 = string.Empty;
                                opz = string.Empty; 
                                ws = string.Empty;
                                countTR = (svod88[i, 1] != null && svod88[i, 1].ToString().Length > 0) ? Convert.ToSingle(svod88[i, 1].ToString()) : 0;
                                countGRP = (svod101[i, 1] != null && svod101[i, 1].ToString().Length > 0) ? Convert.ToSingle(svod101[i, 1].ToString()) : 0;
                                countGRP2 = (svod119[i, 1] != null && svod119[i, 1].ToString().Length > 0) ? Convert.ToSingle(svod119[i, 1].ToString()) : 0;
                                countOPZ = (svod104[i, 1] != null && svod104[i, 1].ToString().Length > 0) ? Convert.ToSingle(svod104[i, 1].ToString()) : 0;
                                countWs = (svod77[i, 1] != null && svod77[i, 1].ToString().Length > 0) ? Convert.ToSingle(svod77[i, 1].ToString()) : 0;

                                if (ofName.Length > 0 && ofAreaName.Length > 0 && (countTR > 0.5 || countGRP > 0.5 || countGRP2 > 0.5 || countOPZ > 0.5 || countWs > 0.5))
                                {
                                    if (ofStartRows.TryGetValue(ofName, out key))
                                    {
                                        for (j = ofStartRowsData[key][0]; j < ofStartRowsData[key][1]; j++)
                                        {
                                            wellName = wellCol[j, 1].ToString();
                                            if (areaCol[j, 1] != null && ofAreaName == areaCol[j, 1].ToString())
                                            {
                                                // TR
                                                if(countTR > 0.5)
                                                {
                                                    str = (col70[j, 1] != null) ? col70[j, 1].ToString() : "0";
                                                    if (str.Length > 0 && Char.IsDigit(str, 0))
                                                    {
                                                        value = Convert.ToDouble(str);
                                                        if (value > 2.5)
                                                        {
                                                            if (tr.Length > 0) tr += ", ";
                                                            tr += string.Format("{0} ({1:0.#} т/сут)", wellName, value);
                                                        }
                                                    }
                                                }
                                                // GRP
                                                if(countGRP > 0.5)
                                                {
                                                    str = (col30[j, 1] != null) ? col30[j, 1].ToString() : "0";
                                                    str2 = (col76[j, 1] != null) ? col76[j, 1].ToString() : "0";
                                                    str3 = (col115[j, 1] != null) ? col115[j, 1].ToString() : "0";

                                                    if (str.Length > 0 && str2.Length > 0 && str3.Length > 0 &&
                                                        Char.IsDigit(str, 0) && Char.IsDigit(str2, 0) && Char.IsDigit(str3, 0))
                                                    {
                                                        value = Convert.ToDouble(str);
                                                        value2 = Convert.ToDouble(str2);
                                                        value3 = Convert.ToDouble(str3);
                                                        if (value < 50 && value2 > 10 && value3 < 0.5)
                                                        {
                                                            if (grp.Length > 0) grp += ", ";
                                                            grp += string.Format("{0} ({1:0.#} т/сут)", wellName, value2);
                                                        }
                                                    }
                                                }
                                                // GRP 2
                                                if (countGRP2 > 0.5)
                                                {
                                                    str = (col30[j, 1] != null) ? col30[j, 1].ToString() : "0";
                                                    str2 = (col76[j, 1] != null) ? col76[j, 1].ToString() : "0";
                                                    str3 = (col115[j, 1] != null) ? col115[j, 1].ToString() : "0";

                                                    if (str.Length > 0 && str2.Length > 0 && str3.Length > 0 &&
                                                        Char.IsDigit(str, 0) && Char.IsDigit(str2, 0) && Char.IsDigit(str3, 0))
                                                    {
                                                        value = Convert.ToDouble(str);
                                                        value2 = Convert.ToDouble(str2);
                                                        value3 = Convert.ToDouble(str3);
                                                        if (value < 50 && value2 > 10 && value3 < 0.5)
                                                        {
                                                            if (grp2.Length > 0) grp2 += ", ";
                                                            grp2 += string.Format("{0} ({1:0.#} т/сут)", wellName, value2);
                                                        }
                                                    }
                                                }
                                                // OPZ
                                                if (countOPZ > 0.5)
                                                {
                                                    str = (col30[j, 1] != null) ? col30[j, 1].ToString() : "0";
                                                    str2 = (col82[j, 1] != null) ? col82[j, 1].ToString() : "0";

                                                    if (str.Length > 0 && str2.Length > 0 && Char.IsDigit(str, 0) && Char.IsDigit(str2, 0))
                                                    {
                                                        value = Convert.ToDouble(str);
                                                        value2 = Convert.ToDouble(str2);
                                                        if (value < 70 && value2 > 2.5)
                                                        {
                                                            if (opz.Length > 0) opz += ", ";
                                                            opz += string.Format("{0} ({1:0.#} т/сут)", wellName, value2);
                                                        }
                                                    }
                                                }
                                                // Ws
                                                if (countWs > 0.5)
                                                {
                                                    str = (col30[j, 1] != null) ? col30[j, 1].ToString() : "0";
                                                    str2 = (col28[j, 1] != null) ? col28[j, 1].ToString() : "0";

                                                    if (str.Length > 0 && str2.Length > 0 && Char.IsDigit(str, 0) && Char.IsDigit(str2, 0))
                                                    {
                                                        value = Convert.ToDouble(str);
                                                        value2 = Convert.ToDouble(str2);
                                                        if (value > 80 && value2 < 0.5)
                                                        {
                                                            if (ws.Length > 0) ws += ", ";
                                                            ws += string.Format("{0} ({1:0.#} %)", wellName, value);
                                                        }
                                                    }
                                                }
                                                find = true;
                                            }
                                            else if (find)
                                            {
                                                break;
                                            }
                                        }
                                        if (find)
                                        {
                                            outTR[i, 1] = tr;
                                            outGRP[i, 1] = grp;
                                            outGRP2[i, 1] = grp2;
                                            outOPZ[i, 1] = opz;
                                            outWs[i, 1] = ws;
                                        }
                                    }
                                }
                                i++;
                            }
                            excel.SetActiveSheet(SvodSheet);
                            excel.SetRange(5, 77, svodLastRow, 77, outWs);
                            excel.SetRange(5, 89, svodLastRow, 89, outTR);
                            excel.SetRange(5, 102, svodLastRow, 102, outGRP);
                            excel.SetRange(5, 105, svodLastRow, 105, outOPZ);
                            excel.SetRange(5, 120, svodLastRow, 120, outGRP2);

                            for (int h = 0; h < project.OilFields.Count / 2; h++) worker.ReportProgress(100, userState);
                            if (ByAreas > -1)
                            {
                                excel.SetActiveSheet(ByAreas);
                                int merMonth = project.maxMerDate.Month;
                                out_list.Clear();
                                dt = startDate;
                                row = new object[32];
                                for (int h = 0; h < 32; h++)
                                {
                                    row[h] = dt;
                                    dt = dt.AddMonths(1);
                                }
                                out_list.Add(row);
                                excel.SetRowsArray(0, 2, out_list);

                                //if (merMonth > 4)
                                //{
                                //    string range1 = string.Format("R9C{0}:R9C{1}", 4 + (merMonth - 4), 28 + (merMonth - 4));
                                //    string range2 = string.Format("R10C{0}:R10C{1}", 4 + (merMonth - 4), 28 + (merMonth - 4));
                                //    string range3 = string.Format("R5C{0}:R5C{1}", 4 + (merMonth - 4), 28 + (merMonth - 4));
                                //    string formula = string.Format("=ЕСЛИ(И(R[7]C[-2]>0;СЧЁТЕСЛИМН({0};\">0\")>2);ЕСЛИ((СЧЁТ({1})*СУММПРОИЗВ({0};{1})-СУММ({1})*СУММ({0}))/(СЧЁТ({1})*СУММКВ({0})-СУММ({0})*СУММ({0}))>0;RC[-1]+(СЧЁТ({1})*СУММПРОИЗВ({0};{1})-СУММ({1})*СУММ({0}))/(СЧЁТ({1})*СУММКВ({0})-СУММ({0})*СУММ({0}))*R[7]C[-2]+((СУММ({1})*СУММКВ({0})-СУММПРОИЗВ({0};{1})*СУММ({0})))/(СЧЁТ({1})*СУММКВ({0})-СУММ({0})*СУММ({0}));СРЗНАЧ(RC[-3]:RC[-1]));ЕСЛИ(R[3]C[-2]>0;RC[-1]+((СЧЁТ({1})*СУММПРОИЗВ({2};{1})-СУММ({1})*СУММ({2}))/(СЧЁТ({1})*СУММКВ({2})-СУММ({2})*СУММ({2}))*R[3]C[-2]+((СУММ({1})*СУММКВ({2})-СУММПРОИЗВ({2};{1})*СУММ({2})))/(СЧЁТ({1})*СУММКВ({2})-СУММ({2})*СУММ({2})))/100;RC[-1]))", range1, range2, range3);
                                //    excel.RangeColumnAutoFill(0, 29, 0, 29 + (merMonth - 4), MSOffice.Excel.XLAutoFillType.xlFillFormats);
                                //    excel.RangeColumnAutoFill(1, 29, 20, 29 + (merMonth - 4), MSOffice.Excel.XLAutoFillType.xlFillDefault);
                                //    excel.SetFormula(1, 30 + (merMonth - 4), formula);
                                //    excel.RangeColumnAutoFill(1, 30 + (merMonth - 4), 1, 37, MSOffice.Excel.XLAutoFillType.xlFillDefault);

                                //    excel.SetFormula(22, 2, string.Format("=РАНГ(R[-21]C;R2C3:R2C{0};100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(23, 2, string.Format("=ЕСЛИ(СЧЁТЕСЛИМН(R9C3:R9C{0};\">0\")>2;РАНГ(R[-15]C;R9C3:R9C{0};100);РАНГ(R[-19]C;R5C3:R5C{0};100))", 30 + (merMonth - 4)));
                                //    excel.RangeColumnAutoFill(22, 2, 23, 29 + (merMonth - 4));
                                //    excel.RangeColumnAutoFill(24, 2, 25, 27 + (merMonth - 4));
                                //    excel.SetFormula(26, 1, string.Format("=1-6*R26C{0}/(СЧЁТ(R23C3:R23C{1})*(СЧЁТ(R23C3:R23C{1})^2-1))", 28 + (merMonth - 4), 30 + (merMonth - 4)));

                                //    excel.SetFormula(28, 1, string.Format("=R5C38/(R2C38-R11C38)*(R2C38-R2C{0})*(1-R6C38/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(29, 1, string.Format("=(R2C{0}-R11C{0})*(R5C38/(R2C38-R11C38)-R5C{0}/(R2C{0}-R11C{0}))*(1-R6C38/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(30, 1, string.Format("=-R5C38/(R2C38-R11C38)*(R11C38-R11C{0})*(1-R6C38/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(31, 1, string.Format("=(R5C38-R5C{0})*(1-R6C38/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(32, 1, string.Format("=-(R5C{0}*(R6C38-R6C{0})/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(33, 1, string.Format("=((R5C38-R5C{0})*(1-R6C38/100))-(R5C{0}*(R6C38-R6C{0})/100)", 30 + (merMonth - 4)));
                                    
                                //    excel.SetFormula(28, 7, string.Format("=R5C38*R8C38/(R2C38-R11C38)*(R2C38-R2C{0})*(1-R6C38/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(29, 7, string.Format("=(R2C{0}-R11C{0})*(R5C38*R8C38/(R2C38-R11C38)-R5C{0}*R8C38/(R2C{0}-R11C{0}))*(1-R6C38/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(30, 7, string.Format("=-R5C38*R8C38/(R2C38-R11C38)*(R11C38-R11C{0})*(1-R6C38/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(31, 7, string.Format("=(R5C38*R8C38-R5C{0}*R8C38)*(1-R6C38/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(32, 7, string.Format("=-(R5C{0}*R8C38*(R6C38-R6C{0})/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(33, 7, string.Format("=((R5C38*R8C38-R5C{0}*R8C38)*(1-R6C38/100))-(R5C{0}*R8C38*(R6C38-R6C{0})/100)", 30 + (merMonth - 4)));
                                //    excel.SetFormula(30, 11, string.Format("=1-6*R30C{0}/(СЧЁТ(R29C12:R29C{0})*(СЧЁТ(R29C12:R29C{0})^2-1))", 39 + (merMonth - 4)));
                                    
                                //    range1 = string.Format("R1C3:R1C{0}", 30 + (merMonth - 4));
                                //    range2 = string.Format("R15C3:R15C{0}", 30 + (merMonth - 4));
                                //    excel.SetFormula(31, 11, string.Format("=(СЧЁТ({1})*СУММПРОИЗВ({0};{1})-СУММ({1})*СУММ({0}))/(СЧЁТ({1})*СУММКВ({0})-СУММ({0})*СУММ({0}))", range1, range2));

                                //    excel.SetFormula(28, 11, string.Format("=(РАНГ(R[-14]C[-9];R15C3:R15C{0};100)-РАНГ(R[-28]C[-9];R1C3:R1C{0};100))^2", 30 + (merMonth - 4)));
                                //    excel.RangeColumnAutoFill(28, 11, 29, 38 + (merMonth - 4), MSOffice.Excel.XLAutoFillType.xlFillDefault);
                                //}
                            }
                            for (int h = 0; h < project.OilFields.Count / 3; h++) worker.ReportProgress(100, userState);
                        }
                        excel.SetActiveWorkbook(InitTRbook);
                        excel.CloseWorkbook(false);
                    }
                    if (excel != null)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                        excel.Disconnect();
                    }
                }
            }
        }
        
        // Отчет по ячейкам заводнения суммарно за 2010 - н.в.
        public static void ReportAreasSumGTM(BackgroundWorker worker, DoWorkEventArgs e, Project project)
        {
            char[] parser = new char[] { ';' };
            string[] fileNames = project.tempStr.Split(parser);
            OilField of = null;
            Area area;
            Well w;
            int i, j, k, ind, year;
            SmartPlus.MSOffice.Excel excel;
            StratumTreeNode stratumNode;
            object[] row;
            string str;
            List<Well> wells;
            MerComp comp;
            MerWorkTimeItem wtProd, wtInj;
            double sumLiq, sumOil;
            List<object[]>[] out_list = new List<object[]>[3];
            out_list[0] = new List<object[]>();
            out_list[1] = new List<object[]>();
            out_list[2] = new List<object[]>();
            List<Well>[] gtmWells = new List<Well>[3];
            List<Well>[] withoutGtmWells = new List<Well>[3];
            double[][] vals;
            double[][] Pressure;
            string[] GtmNames = new string[] { "Всего", "ВНС", "ЗБС", "ГРП", "ПВЛГ", "ВБД", "ИДН", "ОПЗ", "Реперфорация", "РИР" };
            string[][] GtmWellNames = new string[3][];

            int startYear = 2010;

            for (i = 0; i < GtmWellNames.Length; i++)
            {
                GtmWellNames[i] = new string[10];
            }
            List<int> UsedGtmCodes = new List<int>(new int[] { 1000, 1002, 1003, 2000, 2001, 2002, 2003, 3000, 3001, 3002, 3003, 3004, 3005, 5000, 5001, 5002, 6000, 6001, 6002, 6003, 7000, 8000, 8001, 8002, 8003, 8004, 8005, 8006, 9000, 9001, 9002, 9003, 9004, 10000, 10001, 10002, 10004, 11000, 11001, 11002, 11003, 11004 });
            /// GTM codes
            /// 100x - ВНС из бурения
            /// 200x - ВНС из др.фонда
            /// 300х - ЗБС
            /// 500х - ГРП
            /// 600х - ПВЛГ
            /// 7000 - ВБД
            /// 800х - Оптимизация ПО
            /// 900х - РИР
            /// 1000х - ОПЗ
            /// 1100х - Реперфорация

            if (fileNames.Length == 1)
            {
                var dictArea = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                var dictStratum = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                vals = new double[3][];
                for (i = 0; i < vals.Length; i++)
                {
                    vals[i] = new double[100];
                }
                int indRow, month, gtmIndex, wellGtmIndex;

                List<List<int>> GtmCodes = new List<List<int>>();
                GtmCodes.Add(UsedGtmCodes);
                GtmCodes.Add(new List<int>(new int[] { 1000, 1002, 1003, 2000, 2001, 2002, 2003 })); // ВНС
                GtmCodes.Add(new List<int>(new int[] { 3000, 3001, 3002, 3003, 3004, 3005 }));       // ЗБС
                GtmCodes.Add(new List<int>(new int[] { 5000, 5001, 5002 }));                         // ГРП
                GtmCodes.Add(new List<int>(new int[] { 6000, 6001, 6002, 6003 }));                   // ПВЛГ
                GtmCodes.Add(new List<int>(new int[] { 7000 }));                                     // ВБД
                GtmCodes.Add(new List<int>(new int[] { 8000, 8001, 8002, 8003, 8004, 8005, 8006 })); // ИДН
                GtmCodes.Add(new List<int>(new int[] { 10000, 10001, 10002, 10004 }));               // ОПЗ
                GtmCodes.Add(new List<int>(new int[] { 11000, 11001, 11002, 11003, 11004 }));        // Реперфорация
                GtmCodes.Add(new List<int>(new int[] { 9000, 9001, 9002, 9003, 9004 }));             // РИР
                int countGtm;
                double Square, Volume, NBZ, AverageThickness;
                List<Grid> grids = new List<Grid>();
                List<Grid> loadedGrids = new List<Grid>();
                double[] values;

                List<int> AllPlastCodes = new List<int>();
                List<PVTParamsItem> allPVT = new List<PVTParamsItem>();
                List<List<Well>> allWellList = new List<List<Well>>();
                List<double[][]> AllParams = new List<double[][]>();
                double[][] plastParams = null, allPlastParams = null;
                allPlastParams = new double[3][];
                for (i = 0; i < allPlastParams.Length; i++)
                {
                    allPlastParams[i] = new double[104];
                }
                List<int> oilfieldAreaObjects = new List<int>();
                List<int> areaPlastCodes = new List<int>();
                List<int> gridPlastCodes = new List<int>();
                for (int ofInd = 1; ofInd < project.OilFields.Count; ofInd++)
                {
                    of = project.OilFields[ofInd];
                    //if (of.Name != "АБДУЛОВСКОЕ") continue;
                    //if (of.Name != "ИЛЬИНСКОЕ") continue;
                    //if (ofInd > 2) break;

                    of.LoadMerFromCache(worker, e, false);
                    of.LoadGTMFromCache(worker, e);
                    of.LoadWellResearchFromCache(worker, e);
                    of.LoadPVTParamsFromCache(worker, e);

                    AllPlastCodes.Clear();
                    AllParams.Clear();
                    allPVT.Clear();
                    allWellList.Clear();
                    AllPlastCodes.Add(-1);
                    oilfieldAreaObjects.Clear();
                    for (i = 0; i < allPlastParams.Length; i++)
                    {
                        for (j = 0; j < allPlastParams[i].Length; j++)
                        {
                            allPlastParams[i][j] = 0;
                        }
                    }
                    AllParams.Add(allPlastParams);
                    allPVT.Add(PVTParamsItem.Empty);
                    allWellList.Add(new List<Well>());
                    for (int aInd = 0; aInd < of.Areas.Count; aInd++)
                    {
                        if (oilfieldAreaObjects.IndexOf(of.Areas[aInd].PlastCode) == -1)
                        {
                            oilfieldAreaObjects.Add(of.Areas[aInd].PlastCode);
                        }
                    }
                    for (int aInd = 0; aInd < of.Areas.Count; aInd++)
                    {
                        area = (Area)of.Areas[aInd];
                        if (int.TryParse(area.Name, out i)) continue;

                        for (i = 0; i < vals.Length; i++)
                        {
                            for (j = 0; j < vals[i].Length; j++)
                            {
                                vals[i][j] = 0;
                            }
                        }
                        areaPlastCodes.Clear();
                        for (i = 0; i < of.MerStratumCodes.Count; i++)
                        {
                            stratumNode = dictStratum.GetStratumTreeNode(of.MerStratumCodes[i]);

                            if (area.PlastCode == of.MerStratumCodes[i])
                            {
                                areaPlastCodes.Add(of.MerStratumCodes[i]);
                            }
                            else if (stratumNode.GetParentLevelByCode(area.PlastCode) > -1 && oilfieldAreaObjects.IndexOf(of.MerStratumCodes[i]) == -1)
                            {
                                areaPlastCodes.Add(of.MerStratumCodes[i]);
                            }
                        }
                        /// Параметры массива
                        /// 0 - Добыча нефти за год, т
                        /// 1 - Добыча жидкости за год, т
                        /// 2 - Среднедейств фонд доб
                        /// 3 - Среднедейств фонд наг
                        /// 4 - Среднее пластовое давление на начало года - allParams - НБЗ
                        /// 5 - Среднее пластовое давление на конец года - allParams - ОИЗ
                        /// 6 - Среднее забойное давление на начало года - allParams - Square
                        /// 7 - Среднее забойное давление на конец года - allParams - Volume
                        /// 8 - Среднее забойное давление на конец года по базе - allParams - WellCount
                        /// 9 - % падения дебита нефти из БП
                        /// 10 - Сумм нефть на начало года
                        /// 11 - Сумм жидкость на начало года
                        /// 12 - Сумм время работы на начало года
                        /// 13 - Средн дебит нефти на начало года
                        /// 14 - Средн дебит жидкости на начало года
                        /// 
                        /// 15 - Сумм нефть на конец года
                        /// 16 - Сумм жидкость на конец года
                        /// 17 - Сумм время работы на конец года
                        /// 18 - Средн дебит нефти на конец года
                        /// 19 - Средн дебит жидкости на конец года
                        /// 
                        /// 20 - Сумм нефть по базе на конец года
                        /// 21 - Сумм жидкость по базе на конец года
                        /// 22 - Средн дебит нефти по базе на конец года
                        /// 23 - Средн дебит жидкости по базе на конец года
                        /// 24 - Добыча жидкости за год, м3
                        /// 25 - Закачка на начало года
                        /// 26 - Закачка на конец года
                        /// 27 - Закачка за год
                        /// 28 - Нефть м3 на начало года
                        /// 29 - Вода м3 на начало года
                        /// 30 - Нефть м3 на конец года
                        /// 31 - Вода м3 на конец года
                        /// 32 - Нефть м3 за год
                        /// 33 - Вода м3 за год
                        /// 34 - Накопленная нефть на конец года
                        /// 35 - Накопленная вода на конец года
                        /// 36 - ДДН от ГТМ
                        /// 37 - 97 данные ГТМ

                        plastParams = null;
                        ind = AllPlastCodes.IndexOf(area.PlastCode);
                        if (ind == -1)
                        {
                            ind = AllPlastCodes.Count;
                            AllPlastCodes.Add(area.PlastCode);
                            plastParams = new double[3][];
                            for (i = 0; i < plastParams.Length; i++)
                            {
                                plastParams[i] = new double[104];
                            }
                            AllParams.Add(plastParams);
                            allPVT.Add(area.pvt);
                            allWellList.Add(new List<Well>());
                        }
                        allWellList[ind].AddRange(area.WellList);
                        plastParams = AllParams[ind];

                        WellPlastPart plastPart;
                        bool find = false;
                        for (int wInd = 0; wInd < area.WellList.Length; wInd++)
                        {
                            w = (Well)area.WellList[wInd];
                            if (!w.MerLoaded) continue;
                            if (allWellList[0].IndexOf(w) == -1) allWellList[0].Add(w);

                            comp = new MerComp(w.mer);
                            find = false;
                            for (i = 0; i < comp.Count; i++)
                            {
                                year = comp[i].Date.Year - startYear;
                                if (year < 3)
                                {
                                    for (int ip = 0; ip < comp[i].PlastItems.Count; ip++)
                                    {
                                        if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                        vals[0][34] += comp[i].PlastItems[ip].Oil;
                                        vals[0][35] += comp[i].PlastItems[ip].Liq - comp[i].PlastItems[ip].Oil;
                                    }
                                }
                                if(!find)
                                {
                                    if(comp[i].Date.Year >= startYear && comp[i].Date.Year <= startYear + 2)
                                    {
                                        wtProd = comp[i].TimeItems.GetProductionTime(areaPlastCodes);
                                        find = wtProd.GetWorkHours() > 0; 
                                    }
                                }
                                // На конец года
                              
                                if ((comp[i].Date.Year == 2012) && (comp[i].Date.Month > 9 && comp[i].Date.Month <= 12))
                                {
                                    for (int ip = 0; ip < comp[i].PlastItems.Count; ip++)
                                    {
                                        if (areaPlastCodes.IndexOf(comp[i].PlastItems[ip].PlastCode) == -1) continue;
                                        vals[0][15] += comp[i].PlastItems[ip].Oil;
                                    }
                                }
                            }
                            if(find) vals[0][2]++;
                        }
                        // Запасы
                        //Square = 0;
                        //Volume = 0;
                        //AverageThickness = 0;
                        //NBZ = 0;

                        //// ищем сетки
                        //find = false;
                        //gridPlastCodes.Clear();
                        //for (j = 0; j < areaPlastCodes.Count; j++)
                        //{
                        //    grids.Clear();
                        //    for (i = 0; i < of.Grids.Count; i++)
                        //    {
                        //        if (of.Grids[i].GridType == 2 && of.Grids[i].StratumCode == areaPlastCodes[j])
                        //        {
                        //            find = false;
                        //            stratumNode = dictStratum.GetStratumTreeNode(areaPlastCodes[j]);
                        //            for (k = 0; k < areaPlastCodes.Count; k++)
                        //            {
                        //                if (k != j && stratumNode.GetParentLevelByCode(areaPlastCodes[k]) > -1)
                        //                {
                        //                    find = true;
                        //                }
                        //            }
                        //            if (!find)
                        //            {
                        //                grids.Add(of.Grids[i]);
                        //                if (gridPlastCodes.IndexOf(of.Grids[i].StratumCode) == -1)
                        //                {
                        //                    gridPlastCodes.Add(of.Grids[i].StratumCode);
                        //                }
                        //            }
                        //        }
                        //    }
                        //    if (grids.Count > 0)
                        //    {
                        //        NBZ = 0;
                        //        for (i = 0; i < grids.Count; i++)
                        //        {
                        //            if (!grids[i].DataLoaded)
                        //            {
                        //                of.LoadGridDataFromCacheByIndex(worker, e, grids[i].Index);
                        //                loadedGrids.Add(grids[i]);
                        //            }
                        //            grids[i].CalcSumValues(worker, e, area.contour, out Square, out Volume);
                        //            if (Square > 0) AverageThickness = Volume / Square;
                        //            NBZ = area.pvt.OilDensity * area.pvt.Porosity * area.pvt.OilInitialSaturation * Volume / area.pvt.OilVolumeFactor;
                        //            if (NBZ > 0) break;
                        //        }
                        //        //for (year = 0; year < 3; year++)
                        //        //{
                        //            year = 0;
                        //            plastParams[year][98] += NBZ;
                        //            plastParams[year][99] += NBZ * area.pvt.KIN;
                        //            plastParams[year][100] += NBZ * area.pvt.KIN - vals[year][34];
                        //            plastParams[year][101] += Square;
                        //            plastParams[year][102] += Volume;
                        //            allPlastParams[year][98] += NBZ;
                        //            allPlastParams[year][99] += NBZ * area.pvt.KIN;
                        //            allPlastParams[year][100] += NBZ * area.pvt.KIN - vals[year][34];
                        //            allPlastParams[year][101] += Square;
                        //            allPlastParams[year][102] += Volume;
                        //        //}
                        //    }
                        //}
                        //Square = area.contour._points.GetSquare();

                        // параметры гтм
                        wells = new List<Well>(area.WellList);
                        year = 0;
                        for (k = 0; k < GtmCodes.Count; k++)
                        {
                            GtmWellNames[year][k] = string.Empty;
                        }
                        for (k = 0; k < GtmCodes.Count; k++)
                        {
                            gtmWells[0] = MonitoringReporter.GetWellsWithGTM(wells, GtmCodes[k], areaPlastCodes, new List<int>(new int[] { startYear, startYear + 1, startYear + 2 }), out countGtm);
                            for (i = 0; i < gtmWells[0].Count; i++)
                            {
                                if (GtmWellNames[year][k].Length > 0) GtmWellNames[year][k] += ", ";
                                GtmWellNames[year][k] += gtmWells[0][i].Name;
                            }
                            if (gtmWells[0].Count > 0)
                            {
                                values = MonitoringReporter.GetGtmWellsParams(gtmWells[0], GtmCodes[k], areaPlastCodes, new List<int>(new int[] { startYear, startYear + 1, startYear + 2 }));
                                vals[year][37 + k * 6] = values[0];
                                vals[year][38 + k * 6] = values[1];
                                vals[year][39 + k * 6] = values[3];
                                vals[year][40 + k * 6] = values[2];
                                vals[year][41 + k * 6] = gtmWells[0].Count;
                                vals[year][42 + k * 6] = countGtm;
                            }
                            else if (k == 0)
                            {
                                break;
                            }
                        }

                        #region Выгрузка в отчет
                        year = 0;
                        // заполняем выходную инфу
                        row = new object[89];
                        row[0] = of.ParamsDict.NGDUName;
                        row[1] = of.Name;
                        row[2] = dictArea.GetShortNameByCode(area.OilFieldAreaCode);
                        row[3] = area.Name;
                        stratumNode = dictStratum.GetStratumTreeNode(area.PlastCode);
                        row[4] = stratumNode == null ? string.Empty : stratumNode.Name;
                        str = string.Empty;
                        for (i = 0; i < areaPlastCodes.Count; i++)
                        {
                            if (str.Length > 0) str += ", ";
                            str += dictStratum.GetShortNameByCode(areaPlastCodes[i]);
                        }
                        row[5] = str;

                        values = vals[year];
                        row[6] = (values[34] > 0) ? values[35] / values[34] : 0;
                        //row[7] = (Volume / Square) * (NBZ * area.pvt.KIN - values[34]) / (NBZ * area.pvt.KIN);
                        //if (NBZ == 0)
                        //{
                        //    row[8] = 0;
                        //}
                        //else
                        //{
                        //    row[8] = (values[15] == 0) ? 0 : (NBZ * area.pvt.KIN - values[34]) / (values[15] * 4);
                        //}
                        row[9] = of.Name + dictArea.GetShortNameByCode(area.OilFieldAreaCode) + area.Name;

                        for (i = 0; i < GtmNames.Length; i++)
                        {
                            row[10 + i * 7] = (values[2] != 0) ? values[42 + i * 6] / values[2] : 0;
                            row[11 + i * 7] = values[42 + i * 6];
                            row[12 + i * 7] = values[41 + i * 6];
                            row[13 + i * 7] = GtmWellNames[0][i];
                            row[14 + i * 7] = values[38 + i * 6] - values[37 + i * 6];
                            row[15 + i * 7] = (values[37 + i * 6] != 0) ? values[38 + i * 6] / values[37 + i * 6] : 0;
                            row[16 + i * 7] = values[40 + i * 6];
                        }
                        out_list[year].Add(row);
                        #endregion
                    }
                    for (i = 0; i < loadedGrids.Count; i++)
                    {
                        if (loadedGrids[i].DataLoaded) loadedGrids[i].FreeDataMemory();
                    }
                    loadedGrids.Clear();
                    of.PVTList = null;
                    of.ClearWellResearchData(false);
                    of.ClearGTMData(false);
                    of.ClearMerData(true);
                }
                //userState.WorkCurrentTitle = "Выгрузка данных в Microsoft Excel";
                if (out_list[0].Count > 0)
                {
                    int r, endRow = -1;
                    excel = new SmartPlus.MSOffice.Excel();
                    e.Result = excel;
                    excel.EnableEvents = false;
                    excel.NewWorkbook();
                    year = 0;
                    excel.SetActiveSheet(year);
                    excel.SetActiveSheetName((startYear + year).ToString());
                    object[,] varRow = (object[,])Array.CreateInstance(typeof(object), new int[2] { 50, out_list[year][0].Length }, new int[2] { 0, 0 });

                    for (i = 0; i < out_list[year].Count; i += 50)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            excel.Quit();
                            return;
                        }
                        for (r = 0; r < 50; r++)
                        {
                            if ((i + r >= out_list[year].Count) || (out_list[year][i + r] == null))
                            {
                                endRow = i + r - 1;
                                break;
                            }
                            else
                            {
                                for (j = 0; j < out_list[year][i].Length; j++)
                                {
                                    varRow[r, j] = out_list[year][i + r][j];
                                }
                                endRow = i + 50 - 1;
                            }
                        }
                        excel.SetRange(i + 2, 0, endRow + 2, out_list[year][0].Length - 1, varRow);
                    }
                    if (excel != null)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                        excel.Disconnect();
                    }
                }
            }
        }
    }
#endif
}
