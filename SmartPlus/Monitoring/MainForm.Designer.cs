namespace SmartPlus
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane1 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedLeft, new System.Guid("324e7356-ba51-470d-b29c-5d738541b0da"));
            Infragistics.Win.UltraWinDock.DockableGroupPane dockableGroupPane1 = new Infragistics.Win.UltraWinDock.DockableGroupPane(new System.Guid("d9f1b223-fb6b-4431-9325-c9ea41abff31"), new System.Guid("110000cd-1db0-49c4-a249-0c92a26988b5"), -1, new System.Guid("324e7356-ba51-470d-b29c-5d738541b0da"), -1);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane1 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("9f5f3e9e-3326-493a-8ba0-aa474a43c2eb"), new System.Guid("52e65834-a306-49c5-8035-2deccf6eb073"), -1, new System.Guid("d9f1b223-fb6b-4431-9325-c9ea41abff31"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane2 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("062c8751-8f0c-4f4b-9a9b-6739657f5245"), new System.Guid("1c24d395-937f-4654-b4a6-af7dc5ae2061"), -1, new System.Guid("d9f1b223-fb6b-4431-9325-c9ea41abff31"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane3 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("2699a6da-0b6f-4893-8c1a-c7982aed032b"), new System.Guid("110000cd-1db0-49c4-a249-0c92a26988b5"), -1, new System.Guid("d9f1b223-fb6b-4431-9325-c9ea41abff31"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane4 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("0b302c5f-7a60-411f-84bd-454b7235bdb9"), new System.Guid("4e226684-033d-4224-af0f-7933184d53a1"), 0, new System.Guid("324e7356-ba51-470d-b29c-5d738541b0da"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane5 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("05406fbe-cbc1-4ee4-a1cf-082289c6ca72"), new System.Guid("b3496778-0799-4e13-9b7f-363b98b85dce"), 0, new System.Guid("324e7356-ba51-470d-b29c-5d738541b0da"), 0);
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane2 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("52e65834-a306-49c5-8035-2deccf6eb073"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane3 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("4e226684-033d-4224-af0f-7933184d53a1"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane4 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("1c24d395-937f-4654-b4a6-af7dc5ae2061"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane5 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("718ef331-9959-4dae-90b0-0938cb664dce"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane6 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedRight, new System.Guid("1051a24c-248d-4d4e-9a7a-babe9f79aefa"));
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane6 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("93f4258e-3721-4901-9055-3ca35104ad96"), new System.Guid("718ef331-9959-4dae-90b0-0938cb664dce"), 0, new System.Guid("1051a24c-248d-4d4e-9a7a-babe9f79aefa"), 0);
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane7 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("71d3936f-2edb-45db-9bf1-29df42031519"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane8 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("b3496778-0799-4e13-9b7f-363b98b85dce"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane9 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedBottom, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"));
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane7 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("469bc3e7-85f3-4292-8620-d55c660f801d"), new System.Guid("71d3936f-2edb-45db-9bf1-29df42031519"), 0, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 1);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane8 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("36bbaf1c-7b1c-4024-be26-b08ee30dc7e4"), new System.Guid("73c7342e-086d-486c-b3d2-0283db9d7290"), -1, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane9 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("219b6558-adc6-4d67-a264-bc2d5a8c83dc"), new System.Guid("21650b0f-0f54-4043-8af3-688a6862ab09"), 0, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane10 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("4001fa0d-8c02-4a8f-a5a2-5469d4de3857"), new System.Guid("1dd0c5b2-9c55-4a78-ac25-c951b4917886"), 0, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane11 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("9c7a6557-2ef2-45e2-9fc5-b5f60ad55954"), new System.Guid("14dafc95-c901-47bd-8e7c-4c7d79f0d3f0"), -1, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane12 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("72c05d61-96c3-46d0-92e2-87dc83ac0eb8"), new System.Guid("6208a103-ef10-4c37-a824-a1615156f398"), -1, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane13 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("b7f5bf3c-1b35-49e8-8131-4f37a371408e"), new System.Guid("958ffdea-9317-45bd-adca-a308c9faebc0"), 0, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane14 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("c89491c3-bb1f-4b8a-831b-e89dab00f5b3"), new System.Guid("4c654b48-1c37-440b-b70d-f06068ad9dc0"), 0, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane15 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("68dc33af-2dbd-4fb0-aa2e-f504ad4b788f"), new System.Guid("9a5e7bae-d31d-4f13-a990-c2f280cec272"), 0, new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f"), 0);
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane10 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("73c7342e-086d-486c-b3d2-0283db9d7290"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane11 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("14dafc95-c901-47bd-8e7c-4c7d79f0d3f0"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane12 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("21650b0f-0f54-4043-8af3-688a6862ab09"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane13 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("1dd0c5b2-9c55-4a78-ac25-c951b4917886"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane14 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("958ffdea-9317-45bd-adca-a308c9faebc0"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane15 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("189644d2-7469-4cea-be72-effa01e7fb2d"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane16 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedRight, new System.Guid("3099c5aa-cb8d-47be-a79a-a17a28ba3579"));
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane16 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("12a3c6f6-a845-45ac-8861-16e68a0ce4d0"), new System.Guid("189644d2-7469-4cea-be72-effa01e7fb2d"), 1, new System.Guid("3099c5aa-cb8d-47be-a79a-a17a28ba3579"), 0);
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane17 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("3c7e5ab8-9ea1-4b4c-a800-a69de23078a2"), new System.Guid("e4a139e0-1452-4ffa-9aa5-285bf965b677"), 0, new System.Guid("3099c5aa-cb8d-47be-a79a-a17a28ba3579"), 1);
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane17 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("9a5e7bae-d31d-4f13-a990-c2f280cec272"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane18 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("4c654b48-1c37-440b-b70d-f06068ad9dc0"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane19 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("53848605-5ca2-4bd4-a418-496ef2398626"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane20 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedRight, new System.Guid("f456507c-4bcc-4d54-a284-9d25722c8594"));
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane18 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("86256c5e-3c38-492e-89fa-b78e31dcacce"), new System.Guid("53848605-5ca2-4bd4-a418-496ef2398626"), 0, new System.Guid("f456507c-4bcc-4d54-a284-9d25722c8594"), 0);
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane21 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("c8d25a7a-e08b-4aea-a540-80d95bbfd5c1"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane22 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedRight, new System.Guid("18528aa3-5517-420a-ab0f-84721c6adcac"));
            Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane19 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("cb2a32c4-9812-4da9-8aba-0c56d2f60440"), new System.Guid("c8d25a7a-e08b-4aea-a540-80d95bbfd5c1"), 0, new System.Guid("18528aa3-5517-420a-ab0f-84721c6adcac"), 0);
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane23 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("e4a139e0-1452-4ffa-9aa5-285bf965b677"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane24 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("110000cd-1db0-49c4-a249-0c92a26988b5"));
            Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane25 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("6208a103-ef10-4c37-a824-a1615156f398"));
            this.pInspector = new System.Windows.Forms.Panel();
            this.pActiveOilField = new System.Windows.Forms.Panel();
            this.pFileServer = new System.Windows.Forms.Panel();
            this.pWork = new System.Windows.Forms.Panel();
            this.tbWork = new System.Windows.Forms.ToolStrip();
            this.tsbSaveWork = new System.Windows.Forms.ToolStripButton();
            this.pFilterOilObj = new System.Windows.Forms.Panel();
            this.tsPlaneWellContainer = new System.Windows.Forms.ToolStripContainer();
            this.tbPlane = new System.Windows.Forms.ToolStrip();
            this.tsbPlaneShowHead = new System.Windows.Forms.ToolStripButton();
            this.tsbPlane_DepthAbsMd = new System.Windows.Forms.ToolStripButton();
            this.tsbPlane_ShowDuplicates = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbPlaneBindWidth = new System.Windows.Forms.ToolStripButton();
            this.PLN_SnapShoot = new System.Windows.Forms.ToolStripSplitButton();
            this.PLN_PartSnapShoot = new System.Windows.Forms.ToolStripMenuItem();
            this.PLN_AllSnapShoot = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbPlaneWellSettings = new System.Windows.Forms.ToolStripButton();
            this.tsbPlaneShowMarker = new System.Windows.Forms.ToolStripButton();
            this.pGraphics = new System.Windows.Forms.Panel();
            this.scGraphics = new System.Windows.Forms.SplitContainer();
            this.tsGraphicsContainer = new System.Windows.Forms.ToolStripContainer();
            this.tbGraphics = new System.Windows.Forms.ToolStrip();
            this.tscbScheme = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbInterval = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsbIntervalYear = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbIntervalMonth = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbIntervalDay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbShowProjParams = new System.Windows.Forms.ToolStripButton();
            this.tsbShowBPParams = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbShowInfo = new System.Windows.Forms.ToolStripButton();
            this.tsbShowLegend = new System.Windows.Forms.ToolStripButton();
            this.tsbGraphicsSettings = new System.Windows.Forms.ToolStripButton();
            this.GR_SnapShoot = new System.Windows.Forms.ToolStripSplitButton();
            this.GR_SnapShootPart = new System.Windows.Forms.ToolStripMenuItem();
            this.GR_SnapShootAll = new System.Windows.Forms.ToolStripMenuItem();
            this.GR_ShowData = new System.Windows.Forms.ToolStripDropDownButton();
            this.GR_ShowGTM = new System.Windows.Forms.ToolStripMenuItem();
            this.GR_ShowWellAction = new System.Windows.Forms.ToolStripMenuItem();
            this.GR_ShowWellLeak = new System.Windows.Forms.ToolStripMenuItem();
            this.GR_ShowWellSuspend = new System.Windows.Forms.ToolStripMenuItem();
            this.GR_ShowWellStop = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbTable = new System.Windows.Forms.ToolStripButton();
            this.tsbPISeriesShow = new System.Windows.Forms.ToolStripButton();
            this.tsCorSchContainer = new System.Windows.Forms.ToolStripContainer();
            this.tbCorScheme = new System.Windows.Forms.ToolStrip();
            this.tsbCorSchShowHead = new System.Windows.Forms.ToolStripButton();
            this.tsbCS_DepthAbsMd = new System.Windows.Forms.ToolStripButton();
            this.tsbShowDepth = new System.Windows.Forms.ToolStripButton();
            this.tsbShowGis = new System.Windows.Forms.ToolStripButton();
            this.tsbShowMarker = new System.Windows.Forms.ToolStripButton();
            this.tsbShowOilObjName = new System.Windows.Forms.ToolStripButton();
            this.tsbCS_ShowDuplicate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbMarkerEditMode = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbCorSchSettings = new System.Windows.Forms.ToolStripButton();
            this.COR_SCH_SnapShoot = new System.Windows.Forms.ToolStripSplitButton();
            this.COR_SCH_PartSnapShoot = new System.Windows.Forms.ToolStripMenuItem();
            this.COR_SCH_AllSnapShoot = new System.Windows.Forms.ToolStripMenuItem();
            this.pGisView = new System.Windows.Forms.Panel();
            this.pMerView = new System.Windows.Forms.Panel();
            this.pChessView = new System.Windows.Forms.Panel();
            this.pDiscussion = new System.Windows.Forms.Panel();
            this.SplitDiscussion = new System.Windows.Forms.SplitContainer();
            this.pAreasView = new System.Windows.Forms.Panel();
            this.tbAreas = new System.Windows.Forms.ToolStrip();
            this.tsbLoadAreasParamsFromCache = new System.Windows.Forms.ToolStripButton();
            this.tsbCalcAreasParams = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbDrawAreasComp = new System.Windows.Forms.ToolStripButton();
            this.tsbDrawAreasAccComp = new System.Windows.Forms.ToolStripButton();
            this.tslComment = new System.Windows.Forms.ToolStripLabel();
            this.pCore = new System.Windows.Forms.Panel();
            this.pCoreTest = new System.Windows.Forms.Panel();
            this.pProperties = new System.Windows.Forms.Panel();
            this.pgProperties = new System.Windows.Forms.PropertyGrid();
            this.pReLog = new System.Windows.Forms.Panel();
            this.reLog = new System.Windows.Forms.RichTextBox();
            this.pWellListSettings = new System.Windows.Forms.Panel();
            this.tsMenuWellListSettings = new System.Windows.Forms.ToolStrip();
            this.tsbAdd = new System.Windows.Forms.ToolStripButton();
            this.tsbAddWellListByFilter = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.WL_AddWell = new System.Windows.Forms.ToolStripSplitButton();
            this.WL_AddSelectWell = new System.Windows.Forms.ToolStripMenuItem();
            this.WL_AddContourWell = new System.Windows.Forms.ToolStripMenuItem();
            this.WL_AddClipBoardWell = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbEditIcons = new System.Windows.Forms.ToolStripButton();
            this.tsbWelllistSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.WL_Graphics = new System.Windows.Forms.ToolStripSplitButton();
            this.WL_GraphicsByOneDate = new System.Windows.Forms.ToolStripMenuItem();
            this.WL_GraphicsByAllDate = new System.Windows.Forms.ToolStripMenuItem();
            this.tsLabel = new System.Windows.Forms.ToolStripLabel();
            this.pReportBuilder = new System.Windows.Forms.Panel();
            this.tbReportBuilder = new System.Windows.Forms.ToolStrip();
            this.rb_AddObject = new System.Windows.Forms.ToolStripDropDownButton();
            this.rb_AddOilfield = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_AddArea = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_AddWell = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.rb_AddSumParams = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_AddMer = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_AddGis = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_AddPerf = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_AddGtm = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_AddRepair = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_AddResearch = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_DeleteSelected = new System.Windows.Forms.ToolStripButton();
            this.uDockManager = new Infragistics.Win.UltraWinDock.UltraDockManager(this.components);
            this.tsbCancel = new System.Windows.Forms.ToolStripDropDownButton();
            this.mMenu = new System.Windows.Forms.MenuStrip();
            this.mMenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuGlobalProjShow = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuProjOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuOpenImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuPrintMap = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuExport = new System.Windows.Forms.ToolStripMenuItem();
            this.mmExportAreasContours = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuClose = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.mMenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuFlooding = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuLoadCompensation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbDrawAreasOverlay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbAreasParamsByDate = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuLoading = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuLoadContoursAreasFromWF = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuLoadTable81FromFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuLoadBizPlanFromFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuCoreLoading = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuCoreLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuCoreTestLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuLoadAreasPVTFromFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuLoadDepositsFromFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuLoadPressure = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuSevice = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuSaveCoef = new System.Windows.Forms.ToolStripMenuItem();
            this.mmFindSkv = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.mMenuAppSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuReports = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuReportChess = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuReportFloodingAnalysis = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuReportAreaPredict = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuReportDepositOIZ = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuReportCore = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuReportMerProduction = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.mMenuCreateGtmList = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuAutoMatchGTM = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuProj = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWindows = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinGis = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinGraphics = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinInspector = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinCore = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinCoreTest = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuReportBuilder = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinCorScheme = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinMest = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinMer = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinDiscussion = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinPlane = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinWorkFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinLog = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinWellList = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinFileServer = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinChess = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWinOilFieldAreas = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mMenuResetWindows = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuCheckUpdates = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuWatsNew = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenuSendSupport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stStrip = new System.Windows.Forms.StatusStrip();
            this.tsbUpdates = new System.Windows.Forms.ToolStripDropDownButton();
            this.stBar1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stBar2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.pb = new System.Windows.Forms.ToolStripProgressBar();
            this._fMainUnpinnedTabAreaLeft = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._fMainUnpinnedTabAreaRight = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._fMainUnpinnedTabAreaTop = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._fMainUnpinnedTabAreaBottom = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._fMainAutoHideControl = new Infragistics.Win.UltraWinDock.AutoHideControl();
            this.dockableWindow3 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow6 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow8 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow9 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow5 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow2 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow4 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow1 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow15 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow10 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow14 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow12 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow7 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow11 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow13 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.tbCanvas = new System.Windows.Forms.ToolStrip();
            this.tsbAddContour = new System.Windows.Forms.ToolStripButton();
            this.tsbAddProfileByLine = new System.Windows.Forms.ToolStripButton();
            this.tsbAddProfileByWell = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbLoadWellsTrajectory = new System.Windows.Forms.ToolStripButton();
            this.tsbCalcBubble = new System.Windows.Forms.ToolStripButton();
            this.tsbCalcAccumBubble = new System.Windows.Forms.ToolStripButton();
            this.tsbCalcHistoryBubble = new System.Windows.Forms.ToolStripButton();
            this.tsbBubbleSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbVisibleAreas = new System.Windows.Forms.ToolStripButton();
            this.tsbVisibleWellPad = new System.Windows.Forms.ToolStripButton();
            this.tsbContoursColor = new System.Windows.Forms.ToolStripButton();
            this.tsbGrayIcons = new System.Windows.Forms.ToolStripButton();
            this.tsbMapSnapShoot = new System.Windows.Forms.ToolStripSplitButton();
            this.tsbPartMapSnapShot = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbAllMapSnapShoot = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbShowBizPlanOnMap = new System.Windows.Forms.ToolStripButton();
            this.tsbCreateVoronoiMap = new System.Windows.Forms.ToolStripButton();
            this.tsbShowDiscussLetterBox = new System.Windows.Forms.ToolStripButton();
            this.tsCanvContainer = new System.Windows.Forms.ToolStripContainer();
            this.windowDockingArea6 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.dockableWindow18 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow17 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow16 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.dockableWindow19 = new Infragistics.Win.UltraWinDock.DockableWindow();
            this.windowDockingArea14 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea13 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea17 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea24 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea25 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea4 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea12 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea18 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea19 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea3 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea8 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea5 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea1 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea9 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea7 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea2 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea10 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea15 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea11 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea20 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea21 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea16 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea23 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.windowDockingArea26 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
            this.pWork.SuspendLayout();
            this.tbWork.SuspendLayout();
            this.tsPlaneWellContainer.TopToolStripPanel.SuspendLayout();
            this.tsPlaneWellContainer.SuspendLayout();
            this.tbPlane.SuspendLayout();
            this.pGraphics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scGraphics)).BeginInit();
            this.scGraphics.Panel1.SuspendLayout();
            this.scGraphics.SuspendLayout();
            this.tsGraphicsContainer.TopToolStripPanel.SuspendLayout();
            this.tsGraphicsContainer.SuspendLayout();
            this.tbGraphics.SuspendLayout();
            this.tsCorSchContainer.LeftToolStripPanel.SuspendLayout();
            this.tsCorSchContainer.SuspendLayout();
            this.tbCorScheme.SuspendLayout();
            this.pDiscussion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitDiscussion)).BeginInit();
            this.SplitDiscussion.SuspendLayout();
            this.pAreasView.SuspendLayout();
            this.tbAreas.SuspendLayout();
            this.pProperties.SuspendLayout();
            this.pReLog.SuspendLayout();
            this.pWellListSettings.SuspendLayout();
            this.tsMenuWellListSettings.SuspendLayout();
            this.pReportBuilder.SuspendLayout();
            this.tbReportBuilder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDockManager)).BeginInit();
            this.mMenu.SuspendLayout();
            this.stStrip.SuspendLayout();
            this.dockableWindow3.SuspendLayout();
            this.dockableWindow6.SuspendLayout();
            this.dockableWindow8.SuspendLayout();
            this.dockableWindow9.SuspendLayout();
            this.dockableWindow5.SuspendLayout();
            this.dockableWindow2.SuspendLayout();
            this.dockableWindow4.SuspendLayout();
            this.dockableWindow1.SuspendLayout();
            this.dockableWindow15.SuspendLayout();
            this.dockableWindow10.SuspendLayout();
            this.dockableWindow14.SuspendLayout();
            this.dockableWindow12.SuspendLayout();
            this.dockableWindow7.SuspendLayout();
            this.dockableWindow11.SuspendLayout();
            this.dockableWindow13.SuspendLayout();
            this.tbCanvas.SuspendLayout();
            this.tsCanvContainer.LeftToolStripPanel.SuspendLayout();
            this.tsCanvContainer.SuspendLayout();
            this.windowDockingArea6.SuspendLayout();
            this.dockableWindow18.SuspendLayout();
            this.dockableWindow17.SuspendLayout();
            this.dockableWindow16.SuspendLayout();
            this.dockableWindow19.SuspendLayout();
            this.windowDockingArea25.SuspendLayout();
            this.windowDockingArea18.SuspendLayout();
            this.windowDockingArea7.SuspendLayout();
            this.windowDockingArea11.SuspendLayout();
            this.windowDockingArea21.SuspendLayout();
            this.SuspendLayout();
            // 
            // pInspector
            // 
            this.pInspector.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pInspector.Location = new System.Drawing.Point(0, 21);
            this.pInspector.Name = "pInspector";
            this.pInspector.Size = new System.Drawing.Size(251, 183);
            this.pInspector.TabIndex = 0;
            // 
            // pActiveOilField
            // 
            this.pActiveOilField.Location = new System.Drawing.Point(0, 21);
            this.pActiveOilField.Name = "pActiveOilField";
            this.pActiveOilField.Size = new System.Drawing.Size(251, 185);
            this.pActiveOilField.TabIndex = 0;
            // 
            // pFileServer
            // 
            this.pFileServer.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pFileServer.Location = new System.Drawing.Point(0, 21);
            this.pFileServer.Name = "pFileServer";
            this.pFileServer.Size = new System.Drawing.Size(251, 185);
            this.pFileServer.TabIndex = 0;
            // 
            // pWork
            // 
            this.pWork.Controls.Add(this.tbWork);
            this.pWork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pWork.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pWork.Location = new System.Drawing.Point(0, 21);
            this.pWork.Name = "pWork";
            this.pWork.Size = new System.Drawing.Size(251, 179);
            this.pWork.TabIndex = 0;
            // 
            // tbWork
            // 
            this.tbWork.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSaveWork});
            this.tbWork.Location = new System.Drawing.Point(0, 0);
            this.tbWork.Name = "tbWork";
            this.tbWork.Size = new System.Drawing.Size(251, 25);
            this.tbWork.TabIndex = 0;
            // 
            // tsbSaveWork
            // 
            this.tsbSaveWork.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSaveWork.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveWork.Image")));
            this.tsbSaveWork.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSaveWork.Name = "tsbSaveWork";
            this.tsbSaveWork.Size = new System.Drawing.Size(23, 22);
            this.tsbSaveWork.Text = "��������� ������ ������� �����";
            this.tsbSaveWork.Click += new System.EventHandler(this.tsbSaveWork_Click);
            // 
            // pFilterOilObj
            // 
            this.pFilterOilObj.Location = new System.Drawing.Point(0, 21);
            this.pFilterOilObj.Name = "pFilterOilObj";
            this.pFilterOilObj.Size = new System.Drawing.Size(251, 235);
            this.pFilterOilObj.TabIndex = 0;
            // 
            // tsPlaneWellContainer
            // 
            this.tsPlaneWellContainer.BottomToolStripPanelVisible = false;
            // 
            // tsPlaneWellContainer.ContentPanel
            // 
            this.tsPlaneWellContainer.ContentPanel.Size = new System.Drawing.Size(233, 644);
            this.tsPlaneWellContainer.LeftToolStripPanelVisible = false;
            this.tsPlaneWellContainer.Location = new System.Drawing.Point(0, 21);
            this.tsPlaneWellContainer.Name = "tsPlaneWellContainer";
            this.tsPlaneWellContainer.RightToolStripPanelVisible = false;
            this.tsPlaneWellContainer.Size = new System.Drawing.Size(233, 669);
            this.tsPlaneWellContainer.TabIndex = 0;
            this.tsPlaneWellContainer.Text = "�������";
            // 
            // tsPlaneWellContainer.TopToolStripPanel
            // 
            this.tsPlaneWellContainer.TopToolStripPanel.Controls.Add(this.tbPlane);
            // 
            // tbPlane
            // 
            this.tbPlane.Dock = System.Windows.Forms.DockStyle.None;
            this.tbPlane.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbPlaneShowHead,
            this.tsbPlane_DepthAbsMd,
            this.tsbPlane_ShowDuplicates,
            this.toolStripSeparator13,
            this.tsbPlaneBindWidth,
            this.PLN_SnapShoot,
            this.tsbPlaneWellSettings,
            this.tsbPlaneShowMarker});
            this.tbPlane.Location = new System.Drawing.Point(3, 0);
            this.tbPlane.Name = "tbPlane";
            this.tbPlane.Size = new System.Drawing.Size(188, 25);
            this.tbPlane.TabIndex = 0;
            // 
            // tsbPlaneShowHead
            // 
            this.tsbPlaneShowHead.Checked = true;
            this.tsbPlaneShowHead.CheckOnClick = true;
            this.tsbPlaneShowHead.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbPlaneShowHead.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlaneShowHead.Image = global::SmartPlus.Properties.Resources.cs_drawHead;
            this.tsbPlaneShowHead.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlaneShowHead.Name = "tsbPlaneShowHead";
            this.tsbPlaneShowHead.Size = new System.Drawing.Size(23, 22);
            this.tsbPlaneShowHead.Text = "��������/������ ����� ��������";
            this.tsbPlaneShowHead.Click += new System.EventHandler(this.tsbPlaneShowHead_Click);
            // 
            // tsbPlane_DepthAbsMd
            // 
            this.tsbPlane_DepthAbsMd.Checked = true;
            this.tsbPlane_DepthAbsMd.CheckOnClick = true;
            this.tsbPlane_DepthAbsMd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbPlane_DepthAbsMd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlane_DepthAbsMd.Image = global::SmartPlus.Properties.Resources.cs_depthAbsMD;
            this.tsbPlane_DepthAbsMd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlane_DepthAbsMd.Name = "tsbPlane_DepthAbsMd";
            this.tsbPlane_DepthAbsMd.Size = new System.Drawing.Size(23, 22);
            this.tsbPlane_DepthAbsMd.Text = "���������� �������� ����������/���������� ������";
            this.tsbPlane_DepthAbsMd.Click += new System.EventHandler(this.tsbPlane_DepthAbsMd_Click);
            // 
            // tsbPlane_ShowDuplicates
            // 
            this.tsbPlane_ShowDuplicates.CheckOnClick = true;
            this.tsbPlane_ShowDuplicates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlane_ShowDuplicates.Image = global::SmartPlus.Properties.Resources.cs_ShowDuplicates;
            this.tsbPlane_ShowDuplicates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlane_ShowDuplicates.Name = "tsbPlane_ShowDuplicates";
            this.tsbPlane_ShowDuplicates.Size = new System.Drawing.Size(23, 22);
            this.tsbPlane_ShowDuplicates.Text = "���������� ��������� ��������";
            this.tsbPlane_ShowDuplicates.Click += new System.EventHandler(this.tsbPlane_ShowDuplicates_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbPlaneBindWidth
            // 
            this.tsbPlaneBindWidth.Checked = true;
            this.tsbPlaneBindWidth.CheckOnClick = true;
            this.tsbPlaneBindWidth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbPlaneBindWidth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlaneBindWidth.Image = global::SmartPlus.Properties.Resources.cs_BindWidth;
            this.tsbPlaneBindWidth.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlaneBindWidth.Name = "tsbPlaneBindWidth";
            this.tsbPlaneBindWidth.Size = new System.Drawing.Size(23, 22);
            this.tsbPlaneBindWidth.Text = "��������� ������ �������� �� ������ ����";
            this.tsbPlaneBindWidth.Click += new System.EventHandler(this.tsbPlaneBindWidth_Click);
            // 
            // PLN_SnapShoot
            // 
            this.PLN_SnapShoot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PLN_SnapShoot.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PLN_PartSnapShoot,
            this.PLN_AllSnapShoot});
            this.PLN_SnapShoot.Image = ((System.Drawing.Image)(resources.GetObject("PLN_SnapShoot.Image")));
            this.PLN_SnapShoot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PLN_SnapShoot.Name = "PLN_SnapShoot";
            this.PLN_SnapShoot.Size = new System.Drawing.Size(32, 22);
            this.PLN_SnapShoot.Text = "toolStripSplitButton1";
            // 
            // PLN_PartSnapShoot
            // 
            this.PLN_PartSnapShoot.Image = global::SmartPlus.Properties.Resources.SnapShootCross;
            this.PLN_PartSnapShoot.Name = "PLN_PartSnapShoot";
            this.PLN_PartSnapShoot.Size = new System.Drawing.Size(342, 22);
            this.PLN_PartSnapShoot.Text = "����������� ������� �������� � ����� ������";
            this.PLN_PartSnapShoot.Click += new System.EventHandler(this.PLN_PartSnapShoot_Click);
            // 
            // PLN_AllSnapShoot
            // 
            this.PLN_AllSnapShoot.Image = global::SmartPlus.Properties.Resources.SnapShoot;
            this.PLN_AllSnapShoot.Name = "PLN_AllSnapShoot";
            this.PLN_AllSnapShoot.Size = new System.Drawing.Size(342, 22);
            this.PLN_AllSnapShoot.Text = "����������� ���� ������� � ����� ������";
            this.PLN_AllSnapShoot.Click += new System.EventHandler(this.PLN_AllSnapShoot_Click);
            // 
            // tsbPlaneWellSettings
            // 
            this.tsbPlaneWellSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlaneWellSettings.Image = global::SmartPlus.Properties.Resources.Settings16;
            this.tsbPlaneWellSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlaneWellSettings.Name = "tsbPlaneWellSettings";
            this.tsbPlaneWellSettings.Size = new System.Drawing.Size(23, 22);
            this.tsbPlaneWellSettings.Text = "��������� �������� ��������";
            this.tsbPlaneWellSettings.Click += new System.EventHandler(this.tsbPlaneWellSettings_Click);
            // 
            // tsbPlaneShowMarker
            // 
            this.tsbPlaneShowMarker.CheckOnClick = true;
            this.tsbPlaneShowMarker.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlaneShowMarker.Image = global::SmartPlus.Properties.Resources.cs_MarkerMode;
            this.tsbPlaneShowMarker.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlaneShowMarker.Name = "tsbPlaneShowMarker";
            this.tsbPlaneShowMarker.Size = new System.Drawing.Size(23, 22);
            this.tsbPlaneShowMarker.Text = "��������/������ �������";
            this.tsbPlaneShowMarker.Click += new System.EventHandler(this.tsbPlaneShowMarker_Click);
            // 
            // pGraphics
            // 
            this.pGraphics.Controls.Add(this.scGraphics);
            this.pGraphics.Location = new System.Drawing.Point(0, 21);
            this.pGraphics.Margin = new System.Windows.Forms.Padding(1);
            this.pGraphics.Name = "pGraphics";
            this.pGraphics.Size = new System.Drawing.Size(864, 208);
            this.pGraphics.TabIndex = 0;
            // 
            // scGraphics
            // 
            this.scGraphics.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scGraphics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scGraphics.Location = new System.Drawing.Point(0, 0);
            this.scGraphics.Name = "scGraphics";
            // 
            // scGraphics.Panel1
            // 
            this.scGraphics.Panel1.Controls.Add(this.tsGraphicsContainer);
            this.scGraphics.Size = new System.Drawing.Size(864, 208);
            this.scGraphics.SplitterDistance = 652;
            this.scGraphics.SplitterWidth = 2;
            this.scGraphics.TabIndex = 0;
            // 
            // tsGraphicsContainer
            // 
            // 
            // tsGraphicsContainer.ContentPanel
            // 
            this.tsGraphicsContainer.ContentPanel.Size = new System.Drawing.Size(650, 181);
            this.tsGraphicsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsGraphicsContainer.Location = new System.Drawing.Point(0, 0);
            this.tsGraphicsContainer.Name = "tsGraphicsContainer";
            this.tsGraphicsContainer.Size = new System.Drawing.Size(650, 206);
            this.tsGraphicsContainer.TabIndex = 2;
            this.tsGraphicsContainer.Text = "toolStripContainer1";
            // 
            // tsGraphicsContainer.TopToolStripPanel
            // 
            this.tsGraphicsContainer.TopToolStripPanel.Controls.Add(this.tbGraphics);
            // 
            // tbGraphics
            // 
            this.tbGraphics.Dock = System.Windows.Forms.DockStyle.None;
            this.tbGraphics.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tscbScheme,
            this.toolStripSeparator1,
            this.tsbInterval,
            this.tsbShowProjParams,
            this.tsbShowBPParams,
            this.toolStripSeparator3,
            this.tsbShowInfo,
            this.tsbShowLegend,
            this.tsbGraphicsSettings,
            this.GR_SnapShoot,
            this.GR_ShowData,
            this.tsbTable,
            this.tsbPISeriesShow});
            this.tbGraphics.Location = new System.Drawing.Point(0, 0);
            this.tbGraphics.Name = "tbGraphics";
            this.tbGraphics.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tbGraphics.Size = new System.Drawing.Size(650, 25);
            this.tbGraphics.Stretch = true;
            this.tbGraphics.TabIndex = 0;
            // 
            // tscbScheme
            // 
            this.tscbScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbScheme.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tscbScheme.Items.AddRange(new object[] {
            "������",
            "������",
            "���������� �����"});
            this.tscbScheme.Name = "tscbScheme";
            this.tscbScheme.Size = new System.Drawing.Size(250, 25);
            this.tscbScheme.ToolTipText = "����� ����� ������������� ������";
            this.tscbScheme.SelectedIndexChanged += new System.EventHandler(this.tscbScheme_SelectedIndexChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbInterval
            // 
            this.tsbInterval.AutoSize = false;
            this.tsbInterval.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbInterval.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbIntervalYear,
            this.tsbIntervalMonth,
            this.tsbIntervalDay});
            this.tsbInterval.Image = ((System.Drawing.Image)(resources.GetObject("tsbInterval.Image")));
            this.tsbInterval.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInterval.Name = "tsbInterval";
            this.tsbInterval.Size = new System.Drawing.Size(88, 22);
            this.tsbInterval.Text = "�� �������";
            this.tsbInterval.ToolTipText = "����� ��������� ����������� ������";
            // 
            // tsbIntervalYear
            // 
            this.tsbIntervalYear.Name = "tsbIntervalYear";
            this.tsbIntervalYear.Size = new System.Drawing.Size(142, 22);
            this.tsbIntervalYear.Text = "�� �����";
            this.tsbIntervalYear.Click += new System.EventHandler(this.tsbIntervalYear_Click);
            // 
            // tsbIntervalMonth
            // 
            this.tsbIntervalMonth.Name = "tsbIntervalMonth";
            this.tsbIntervalMonth.Size = new System.Drawing.Size(142, 22);
            this.tsbIntervalMonth.Text = "�� �������";
            this.tsbIntervalMonth.Click += new System.EventHandler(this.tsbIntervalMonth_Click);
            // 
            // tsbIntervalDay
            // 
            this.tsbIntervalDay.Name = "tsbIntervalDay";
            this.tsbIntervalDay.Size = new System.Drawing.Size(142, 22);
            this.tsbIntervalDay.Text = "���������";
            this.tsbIntervalDay.Click += new System.EventHandler(this.tsbIntervalDay_Click);
            // 
            // tsbShowProjParams
            // 
            this.tsbShowProjParams.CheckOnClick = true;
            this.tsbShowProjParams.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowProjParams.Image = global::SmartPlus.Properties.Resources.ProjParams;
            this.tsbShowProjParams.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowProjParams.Name = "tsbShowProjParams";
            this.tsbShowProjParams.Size = new System.Drawing.Size(23, 22);
            this.tsbShowProjParams.ToolTipText = "���������� ��������� ����������";
            this.tsbShowProjParams.Click += new System.EventHandler(this.tsbShowProjParams_Click);
            // 
            // tsbShowBPParams
            // 
            this.tsbShowBPParams.CheckOnClick = true;
            this.tsbShowBPParams.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowBPParams.Image = global::SmartPlus.Properties.Resources.BPParams;
            this.tsbShowBPParams.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowBPParams.Name = "tsbShowBPParams";
            this.tsbShowBPParams.Size = new System.Drawing.Size(23, 22);
            this.tsbShowBPParams.ToolTipText = "�������� ���������� ������-�����";
            this.tsbShowBPParams.Click += new System.EventHandler(this.tsbShowBPParams_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbShowInfo
            // 
            this.tsbShowInfo.Checked = true;
            this.tsbShowInfo.CheckOnClick = true;
            this.tsbShowInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbShowInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowInfo.Image = global::SmartPlus.Properties.Resources.Info16;
            this.tsbShowInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowInfo.Name = "tsbShowInfo";
            this.tsbShowInfo.Size = new System.Drawing.Size(23, 22);
            this.tsbShowInfo.ToolTipText = "��������/������ �������������� ������";
            this.tsbShowInfo.Click += new System.EventHandler(this.tsbShowInfo_Click);
            // 
            // tsbShowLegend
            // 
            this.tsbShowLegend.Checked = true;
            this.tsbShowLegend.CheckOnClick = true;
            this.tsbShowLegend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbShowLegend.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowLegend.Image = global::SmartPlus.Properties.Resources.legend;
            this.tsbShowLegend.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowLegend.Name = "tsbShowLegend";
            this.tsbShowLegend.Size = new System.Drawing.Size(23, 22);
            this.tsbShowLegend.Text = "toolStripButton1";
            this.tsbShowLegend.ToolTipText = "��������/������ �������";
            this.tsbShowLegend.Click += new System.EventHandler(this.tsbShowLegend_Click);
            // 
            // tsbGraphicsSettings
            // 
            this.tsbGraphicsSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGraphicsSettings.Image = global::SmartPlus.Properties.Resources.Settings16;
            this.tsbGraphicsSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGraphicsSettings.Name = "tsbGraphicsSettings";
            this.tsbGraphicsSettings.Size = new System.Drawing.Size(23, 22);
            this.tsbGraphicsSettings.Text = "��������� �������";
            this.tsbGraphicsSettings.Click += new System.EventHandler(this.tsbGraphicsSettings_Click);
            // 
            // GR_SnapShoot
            // 
            this.GR_SnapShoot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.GR_SnapShoot.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GR_SnapShootPart,
            this.GR_SnapShootAll});
            this.GR_SnapShoot.Image = ((System.Drawing.Image)(resources.GetObject("GR_SnapShoot.Image")));
            this.GR_SnapShoot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.GR_SnapShoot.Name = "GR_SnapShoot";
            this.GR_SnapShoot.Size = new System.Drawing.Size(32, 22);
            this.GR_SnapShoot.Text = "toolStripSplitButton1";
            // 
            // GR_SnapShootPart
            // 
            this.GR_SnapShootPart.Image = global::SmartPlus.Properties.Resources.SnapShootCross;
            this.GR_SnapShootPart.Name = "GR_SnapShootPart";
            this.GR_SnapShootPart.Size = new System.Drawing.Size(333, 22);
            this.GR_SnapShootPart.Text = "����������� ������� ������� � ����� ������";
            this.GR_SnapShootPart.Click += new System.EventHandler(this.GR_SnapShootPart_Click);
            // 
            // GR_SnapShootAll
            // 
            this.GR_SnapShootAll.Image = global::SmartPlus.Properties.Resources.SnapShoot;
            this.GR_SnapShootAll.Name = "GR_SnapShootAll";
            this.GR_SnapShootAll.Size = new System.Drawing.Size(333, 22);
            this.GR_SnapShootAll.Text = "����������� ���� ������ � ����� ������";
            this.GR_SnapShootAll.Click += new System.EventHandler(this.GR_SnapShootAll_Click);
            // 
            // GR_ShowData
            // 
            this.GR_ShowData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.GR_ShowData.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GR_ShowGTM,
            this.GR_ShowWellAction,
            this.GR_ShowWellLeak,
            this.GR_ShowWellSuspend,
            this.GR_ShowWellStop});
            this.GR_ShowData.Image = global::SmartPlus.Properties.Resources.apply;
            this.GR_ShowData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.GR_ShowData.Name = "GR_ShowData";
            this.GR_ShowData.Size = new System.Drawing.Size(29, 22);
            this.GR_ShowData.Text = "���/���� ����������� ������";
            // 
            // GR_ShowGTM
            // 
            this.GR_ShowGTM.Checked = true;
            this.GR_ShowGTM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GR_ShowGTM.Name = "GR_ShowGTM";
            this.GR_ShowGTM.Size = new System.Drawing.Size(218, 22);
            this.GR_ShowGTM.Text = "���������� ���";
            this.GR_ShowGTM.Click += new System.EventHandler(this.GR_ShowGTM_Click);
            // 
            // GR_ShowWellAction
            // 
            this.GR_ShowWellAction.Name = "GR_ShowWellAction";
            this.GR_ShowWellAction.Size = new System.Drawing.Size(218, 22);
            this.GR_ShowWellAction.Text = "���������� �����������";
            this.GR_ShowWellAction.Click += new System.EventHandler(this.GR_ShowWellAction_Click);
            // 
            // GR_ShowWellLeak
            // 
            this.GR_ShowWellLeak.Checked = true;
            this.GR_ShowWellLeak.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GR_ShowWellLeak.Name = "GR_ShowWellLeak";
            this.GR_ShowWellLeak.Size = new System.Drawing.Size(218, 22);
            this.GR_ShowWellLeak.Text = "���������� ���";
            this.GR_ShowWellLeak.Click += new System.EventHandler(this.GR_ShowWellLeak_Click);
            // 
            // GR_ShowWellSuspend
            // 
            this.GR_ShowWellSuspend.Checked = true;
            this.GR_ShowWellSuspend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GR_ShowWellSuspend.Name = "GR_ShowWellSuspend";
            this.GR_ShowWellSuspend.Size = new System.Drawing.Size(218, 22);
            this.GR_ShowWellSuspend.Text = "���������� ���";
            this.GR_ShowWellSuspend.Click += new System.EventHandler(this.GR_ShowWellSuspend_Click);
            // 
            // GR_ShowWellStop
            // 
            this.GR_ShowWellStop.Checked = true;
            this.GR_ShowWellStop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GR_ShowWellStop.Name = "GR_ShowWellStop";
            this.GR_ShowWellStop.Size = new System.Drawing.Size(218, 22);
            this.GR_ShowWellStop.Text = "���������� ���������";
            this.GR_ShowWellStop.Click += new System.EventHandler(this.GR_ShowWellStop_Click);
            // 
            // tsbTable
            // 
            this.tsbTable.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbTable.Image = ((System.Drawing.Image)(resources.GetObject("tsbTable.Image")));
            this.tsbTable.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTable.Name = "tsbTable";
            this.tsbTable.Size = new System.Drawing.Size(74, 22);
            this.tsbTable.Text = "�������";
            this.tsbTable.ToolTipText = "��������� ������������� ������";
            this.tsbTable.Click += new System.EventHandler(this.tsbTable_Click);
            // 
            // tsbPISeriesShow
            // 
            this.tsbPISeriesShow.CheckOnClick = true;
            this.tsbPISeriesShow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbPISeriesShow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tsbPISeriesShow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPISeriesShow.Name = "tsbPISeriesShow";
            this.tsbPISeriesShow.Size = new System.Drawing.Size(25, 22);
            this.tsbPISeriesShow.Text = "PI";
            this.tsbPISeriesShow.Visible = false;
            this.tsbPISeriesShow.Click += new System.EventHandler(this.tsbPISeriesShow_Click);
            // 
            // tsCorSchContainer
            // 
            this.tsCorSchContainer.BottomToolStripPanelVisible = false;
            // 
            // tsCorSchContainer.ContentPanel
            // 
            this.tsCorSchContainer.ContentPanel.Size = new System.Drawing.Size(785, 198);
            // 
            // tsCorSchContainer.LeftToolStripPanel
            // 
            this.tsCorSchContainer.LeftToolStripPanel.Controls.Add(this.tbCorScheme);
            this.tsCorSchContainer.Location = new System.Drawing.Point(0, 21);
            this.tsCorSchContainer.Name = "tsCorSchContainer";
            this.tsCorSchContainer.RightToolStripPanelVisible = false;
            this.tsCorSchContainer.Size = new System.Drawing.Size(817, 223);
            this.tsCorSchContainer.TabIndex = 0;
            this.tsCorSchContainer.Text = "���.�����";
            // 
            // tbCorScheme
            // 
            this.tbCorScheme.Dock = System.Windows.Forms.DockStyle.None;
            this.tbCorScheme.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbCorSchShowHead,
            this.tsbCS_DepthAbsMd,
            this.tsbShowDepth,
            this.tsbShowGis,
            this.tsbShowMarker,
            this.tsbShowOilObjName,
            this.tsbCS_ShowDuplicate,
            this.toolStripSeparator9,
            this.tsbMarkerEditMode,
            this.toolStripSeparator10,
            this.tsbCorSchSettings,
            this.COR_SCH_SnapShoot});
            this.tbCorScheme.Location = new System.Drawing.Point(0, 3);
            this.tbCorScheme.Name = "tbCorScheme";
            this.tbCorScheme.Size = new System.Drawing.Size(32, 195);
            this.tbCorScheme.TabIndex = 0;
            // 
            // tsbCorSchShowHead
            // 
            this.tsbCorSchShowHead.Checked = true;
            this.tsbCorSchShowHead.CheckOnClick = true;
            this.tsbCorSchShowHead.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbCorSchShowHead.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCorSchShowHead.Image = global::SmartPlus.Properties.Resources.cs_drawHead;
            this.tsbCorSchShowHead.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCorSchShowHead.Name = "tsbCorSchShowHead";
            this.tsbCorSchShowHead.Size = new System.Drawing.Size(30, 20);
            this.tsbCorSchShowHead.Text = "��������/������ ����� ���.�����";
            this.tsbCorSchShowHead.Click += new System.EventHandler(this.tsbDrawHead_Click);
            // 
            // tsbCS_DepthAbsMd
            // 
            this.tsbCS_DepthAbsMd.Checked = true;
            this.tsbCS_DepthAbsMd.CheckOnClick = true;
            this.tsbCS_DepthAbsMd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbCS_DepthAbsMd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCS_DepthAbsMd.Image = global::SmartPlus.Properties.Resources.cs_depthAbsMD;
            this.tsbCS_DepthAbsMd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCS_DepthAbsMd.Name = "tsbCS_DepthAbsMd";
            this.tsbCS_DepthAbsMd.Size = new System.Drawing.Size(30, 20);
            this.tsbCS_DepthAbsMd.Text = "���������� �������� ����������/���������� ������";
            this.tsbCS_DepthAbsMd.Click += new System.EventHandler(this.tsbCS_DepthAbsMd_Click);
            // 
            // tsbShowDepth
            // 
            this.tsbShowDepth.Checked = true;
            this.tsbShowDepth.CheckOnClick = true;
            this.tsbShowDepth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbShowDepth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowDepth.Image = global::SmartPlus.Properties.Resources.cs_depth;
            this.tsbShowDepth.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowDepth.Name = "tsbShowDepth";
            this.tsbShowDepth.Size = new System.Drawing.Size(30, 20);
            this.tsbShowDepth.Text = "��������/������ ������� ������";
            this.tsbShowDepth.Click += new System.EventHandler(this.tsbShowDepth_Click);
            // 
            // tsbShowGis
            // 
            this.tsbShowGis.Checked = true;
            this.tsbShowGis.CheckOnClick = true;
            this.tsbShowGis.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbShowGis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowGis.Image = global::SmartPlus.Properties.Resources.cs_gis;
            this.tsbShowGis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowGis.Name = "tsbShowGis";
            this.tsbShowGis.Size = new System.Drawing.Size(30, 20);
            this.tsbShowGis.Text = "��������/������ ������� ���";
            this.tsbShowGis.Click += new System.EventHandler(this.tsbShowGis_Click);
            // 
            // tsbShowMarker
            // 
            this.tsbShowMarker.Checked = true;
            this.tsbShowMarker.CheckOnClick = true;
            this.tsbShowMarker.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbShowMarker.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowMarker.Image = global::SmartPlus.Properties.Resources.cs_MarkerMode;
            this.tsbShowMarker.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowMarker.Name = "tsbShowMarker";
            this.tsbShowMarker.Size = new System.Drawing.Size(30, 20);
            this.tsbShowMarker.Text = "��������/������ �������";
            this.tsbShowMarker.Click += new System.EventHandler(this.tsbShowMarker_Click);
            // 
            // tsbShowOilObjName
            // 
            this.tsbShowOilObjName.Checked = true;
            this.tsbShowOilObjName.CheckOnClick = true;
            this.tsbShowOilObjName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbShowOilObjName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowOilObjName.Image = global::SmartPlus.Properties.Resources.cs_OilObject;
            this.tsbShowOilObjName.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowOilObjName.Name = "tsbShowOilObjName";
            this.tsbShowOilObjName.Size = new System.Drawing.Size(30, 20);
            this.tsbShowOilObjName.Text = "��������/������ �������� �������� ���";
            this.tsbShowOilObjName.Click += new System.EventHandler(this.tsbGisMer_Click);
            // 
            // tsbCS_ShowDuplicate
            // 
            this.tsbCS_ShowDuplicate.CheckOnClick = true;
            this.tsbCS_ShowDuplicate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCS_ShowDuplicate.Image = global::SmartPlus.Properties.Resources.cs_ShowDuplicates;
            this.tsbCS_ShowDuplicate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCS_ShowDuplicate.Name = "tsbCS_ShowDuplicate";
            this.tsbCS_ShowDuplicate.Size = new System.Drawing.Size(30, 20);
            this.tsbCS_ShowDuplicate.Text = "���������� ��������� ��������";
            this.tsbCS_ShowDuplicate.Click += new System.EventHandler(this.tsbCS_ShowDuplicate_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(30, 6);
            // 
            // tsbMarkerEditMode
            // 
            this.tsbMarkerEditMode.CheckOnClick = true;
            this.tsbMarkerEditMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMarkerEditMode.Image = global::SmartPlus.Properties.Resources.cs_MarkerEditMode;
            this.tsbMarkerEditMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMarkerEditMode.Name = "tsbMarkerEditMode";
            this.tsbMarkerEditMode.Size = new System.Drawing.Size(30, 20);
            this.tsbMarkerEditMode.Text = "����� �������������� ��������";
            this.tsbMarkerEditMode.Click += new System.EventHandler(this.tsbMarkerEditMode_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbCorSchSettings
            // 
            this.tsbCorSchSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCorSchSettings.Image = global::SmartPlus.Properties.Resources.Settings16;
            this.tsbCorSchSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCorSchSettings.Name = "tsbCorSchSettings";
            this.tsbCorSchSettings.Size = new System.Drawing.Size(23, 22);
            this.tsbCorSchSettings.Text = "��������� ���.�����";
            this.tsbCorSchSettings.Click += new System.EventHandler(this.tsbCorSchSettings_Click);
            // 
            // COR_SCH_SnapShoot
            // 
            this.COR_SCH_SnapShoot.AutoSize = false;
            this.COR_SCH_SnapShoot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.COR_SCH_SnapShoot.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.COR_SCH_PartSnapShoot,
            this.COR_SCH_AllSnapShoot});
            this.COR_SCH_SnapShoot.Image = ((System.Drawing.Image)(resources.GetObject("COR_SCH_SnapShoot.Image")));
            this.COR_SCH_SnapShoot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.COR_SCH_SnapShoot.Name = "COR_SCH_SnapShoot";
            this.COR_SCH_SnapShoot.Size = new System.Drawing.Size(31, 20);
            this.COR_SCH_SnapShoot.Text = "toolStripSplitButton1";
            // 
            // COR_SCH_PartSnapShoot
            // 
            this.COR_SCH_PartSnapShoot.Image = global::SmartPlus.Properties.Resources.SnapShootCross;
            this.COR_SCH_PartSnapShoot.Name = "COR_SCH_PartSnapShoot";
            this.COR_SCH_PartSnapShoot.Size = new System.Drawing.Size(342, 22);
            this.COR_SCH_PartSnapShoot.Text = "����������� ������� �������� � ����� ������";
            this.COR_SCH_PartSnapShoot.Click += new System.EventHandler(this.COR_SCH_PartSnapShoot_Click);
            // 
            // COR_SCH_AllSnapShoot
            // 
            this.COR_SCH_AllSnapShoot.Image = global::SmartPlus.Properties.Resources.SnapShoot;
            this.COR_SCH_AllSnapShoot.Name = "COR_SCH_AllSnapShoot";
            this.COR_SCH_AllSnapShoot.Size = new System.Drawing.Size(342, 22);
            this.COR_SCH_AllSnapShoot.Text = "����������� ��� �������� � ����� ������";
            this.COR_SCH_AllSnapShoot.Click += new System.EventHandler(this.COR_SCH_AllSnapShoot_Click);
            // 
            // pGisView
            // 
            this.pGisView.Location = new System.Drawing.Point(0, 21);
            this.pGisView.Name = "pGisView";
            this.pGisView.Size = new System.Drawing.Size(899, 299);
            this.pGisView.TabIndex = 0;
            // 
            // pMerView
            // 
            this.pMerView.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pMerView.Location = new System.Drawing.Point(0, 21);
            this.pMerView.Name = "pMerView";
            this.pMerView.Size = new System.Drawing.Size(899, 299);
            this.pMerView.TabIndex = 0;
            // 
            // pChessView
            // 
            this.pChessView.Location = new System.Drawing.Point(0, 21);
            this.pChessView.Name = "pChessView";
            this.pChessView.Size = new System.Drawing.Size(899, 299);
            this.pChessView.TabIndex = 0;
            // 
            // pDiscussion
            // 
            this.pDiscussion.Controls.Add(this.SplitDiscussion);
            this.pDiscussion.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pDiscussion.Location = new System.Drawing.Point(0, 21);
            this.pDiscussion.Name = "pDiscussion";
            this.pDiscussion.Size = new System.Drawing.Size(876, 208);
            this.pDiscussion.TabIndex = 0;
            // 
            // SplitDiscussion
            // 
            this.SplitDiscussion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitDiscussion.Location = new System.Drawing.Point(0, 0);
            this.SplitDiscussion.Name = "SplitDiscussion";
            // 
            // SplitDiscussion.Panel1
            // 
            this.SplitDiscussion.Panel1.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            // 
            // SplitDiscussion.Panel2
            // 
            this.SplitDiscussion.Panel2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SplitDiscussion.Size = new System.Drawing.Size(876, 208);
            this.SplitDiscussion.SplitterDistance = 694;
            this.SplitDiscussion.TabIndex = 0;
            // 
            // pAreasView
            // 
            this.pAreasView.Controls.Add(this.tbAreas);
            this.pAreasView.Location = new System.Drawing.Point(0, 21);
            this.pAreasView.Name = "pAreasView";
            this.pAreasView.Size = new System.Drawing.Size(876, 208);
            this.pAreasView.TabIndex = 0;
            // 
            // tbAreas
            // 
            this.tbAreas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbLoadAreasParamsFromCache,
            this.tsbCalcAreasParams,
            this.toolStripSeparator4,
            this.tsbDrawAreasComp,
            this.tsbDrawAreasAccComp,
            this.tslComment});
            this.tbAreas.Location = new System.Drawing.Point(0, 0);
            this.tbAreas.Name = "tbAreas";
            this.tbAreas.Size = new System.Drawing.Size(876, 25);
            this.tbAreas.TabIndex = 0;
            // 
            // tsbLoadAreasParamsFromCache
            // 
            this.tsbLoadAreasParamsFromCache.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbLoadAreasParamsFromCache.Image = global::SmartPlus.Properties.Resources.load_table_folder;
            this.tsbLoadAreasParamsFromCache.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLoadAreasParamsFromCache.Name = "tsbLoadAreasParamsFromCache";
            this.tsbLoadAreasParamsFromCache.Size = new System.Drawing.Size(23, 22);
            this.tsbLoadAreasParamsFromCache.Text = "��������� ��������� ����� ���������� �� ����";
            this.tsbLoadAreasParamsFromCache.Click += new System.EventHandler(this.tsbLoadAreasParamsFromCache_Click);
            // 
            // tsbCalcAreasParams
            // 
            this.tsbCalcAreasParams.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCalcAreasParams.Image = global::SmartPlus.Properties.Resources.load_table;
            this.tsbCalcAreasParams.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCalcAreasParams.Name = "tsbCalcAreasParams";
            this.tsbCalcAreasParams.Size = new System.Drawing.Size(23, 22);
            this.tsbCalcAreasParams.Text = "���������� ��������� ����� ����������";
            this.tsbCalcAreasParams.Click += new System.EventHandler(this.tsbLoadAreasParams_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbDrawAreasComp
            // 
            this.tsbDrawAreasComp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDrawAreasComp.Image = global::SmartPlus.Properties.Resources.DrawAreasComp;
            this.tsbDrawAreasComp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDrawAreasComp.Name = "tsbDrawAreasComp";
            this.tsbDrawAreasComp.Size = new System.Drawing.Size(23, 22);
            this.tsbDrawAreasComp.Text = "��������� ������ ���������� �� ������� �����������";
            this.tsbDrawAreasComp.Click += new System.EventHandler(this.tsbDrawAreasComp_Click);
            // 
            // tsbDrawAreasAccComp
            // 
            this.tsbDrawAreasAccComp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDrawAreasAccComp.Image = global::SmartPlus.Properties.Resources.DrawAreaAccComp;
            this.tsbDrawAreasAccComp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDrawAreasAccComp.Name = "tsbDrawAreasAccComp";
            this.tsbDrawAreasAccComp.Size = new System.Drawing.Size(23, 22);
            this.tsbDrawAreasAccComp.Text = "��������� ������ ���������� �� ����������� �����������";
            this.tsbDrawAreasAccComp.Click += new System.EventHandler(this.tsbDrawAreasAccComp_Click);
            // 
            // tslComment
            // 
            this.tslComment.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslComment.Name = "tslComment";
            this.tslComment.Size = new System.Drawing.Size(0, 22);
            // 
            // pCore
            // 
            this.pCore.Location = new System.Drawing.Point(0, 21);
            this.pCore.Name = "pCore";
            this.pCore.Size = new System.Drawing.Size(233, 673);
            this.pCore.TabIndex = 0;
            // 
            // pCoreTest
            // 
            this.pCoreTest.Location = new System.Drawing.Point(0, 21);
            this.pCoreTest.Name = "pCoreTest";
            this.pCoreTest.Size = new System.Drawing.Size(251, 235);
            this.pCoreTest.TabIndex = 0;
            // 
            // pProperties
            // 
            this.pProperties.Controls.Add(this.pgProperties);
            this.pProperties.Location = new System.Drawing.Point(0, 21);
            this.pProperties.Name = "pProperties";
            this.pProperties.Size = new System.Drawing.Size(251, 181);
            this.pProperties.TabIndex = 0;
            // 
            // pgProperties
            // 
            this.pgProperties.CommandsVisibleIfAvailable = false;
            this.pgProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgProperties.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgProperties.Location = new System.Drawing.Point(0, 0);
            this.pgProperties.Name = "pgProperties";
            this.pgProperties.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.pgProperties.Size = new System.Drawing.Size(251, 181);
            this.pgProperties.TabIndex = 1;
            // 
            // pReLog
            // 
            this.pReLog.Controls.Add(this.reLog);
            this.pReLog.Location = new System.Drawing.Point(0, 21);
            this.pReLog.Name = "pReLog";
            this.pReLog.Size = new System.Drawing.Size(251, 187);
            this.pReLog.TabIndex = 0;
            // 
            // reLog
            // 
            this.reLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reLog.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.reLog.Location = new System.Drawing.Point(0, 0);
            this.reLog.Name = "reLog";
            this.reLog.Size = new System.Drawing.Size(251, 187);
            this.reLog.TabIndex = 5;
            this.reLog.Text = "";
            // 
            // pWellListSettings
            // 
            this.pWellListSettings.Controls.Add(this.tsMenuWellListSettings);
            this.pWellListSettings.Location = new System.Drawing.Point(0, 21);
            this.pWellListSettings.Name = "pWellListSettings";
            this.pWellListSettings.Size = new System.Drawing.Size(251, 187);
            this.pWellListSettings.TabIndex = 0;
            // 
            // tsMenuWellListSettings
            // 
            this.tsMenuWellListSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdd,
            this.tsbAddWellListByFilter,
            this.toolStripSeparator6,
            this.WL_AddWell,
            this.tsbDelete,
            this.toolStripSeparator5,
            this.tsbEditIcons,
            this.tsbWelllistSettings,
            this.toolStripSeparator14,
            this.WL_Graphics,
            this.tsLabel});
            this.tsMenuWellListSettings.Location = new System.Drawing.Point(0, 0);
            this.tsMenuWellListSettings.Name = "tsMenuWellListSettings";
            this.tsMenuWellListSettings.Size = new System.Drawing.Size(251, 25);
            this.tsMenuWellListSettings.TabIndex = 2;
            // 
            // tsbAdd
            // 
            this.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAdd.Image = global::SmartPlus.Properties.Resources.AddWellListLayer;
            this.tsbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdd.Name = "tsbAdd";
            this.tsbAdd.Size = new System.Drawing.Size(23, 22);
            this.tsbAdd.Text = "������� ����� ������ �������";
            this.tsbAdd.Click += new System.EventHandler(this.tsbAdd_Click);
            // 
            // tsbAddWellListByFilter
            // 
            this.tsbAddWellListByFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddWellListByFilter.Image = global::SmartPlus.Properties.Resources.AddWellListByFilter;
            this.tsbAddWellListByFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddWellListByFilter.Name = "tsbAddWellListByFilter";
            this.tsbAddWellListByFilter.Size = new System.Drawing.Size(23, 22);
            this.tsbAddWellListByFilter.Text = "������� ������ ������� �� �������";
            this.tsbAddWellListByFilter.Click += new System.EventHandler(this.tsbAddWellListByFilter_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // WL_AddWell
            // 
            this.WL_AddWell.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.WL_AddWell.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WL_AddSelectWell,
            this.WL_AddContourWell,
            this.WL_AddClipBoardWell});
            this.WL_AddWell.Image = ((System.Drawing.Image)(resources.GetObject("WL_AddWell.Image")));
            this.WL_AddWell.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.WL_AddWell.Name = "WL_AddWell";
            this.WL_AddWell.Size = new System.Drawing.Size(32, 22);
            this.WL_AddWell.Text = "toolStripSplitButton1";
            // 
            // WL_AddSelectWell
            // 
            this.WL_AddSelectWell.Image = global::SmartPlus.Properties.Resources.Add_Sel_Skv16;
            this.WL_AddSelectWell.Name = "WL_AddSelectWell";
            this.WL_AddSelectWell.Size = new System.Drawing.Size(257, 22);
            this.WL_AddSelectWell.Text = "�������� ���������� ��������";
            this.WL_AddSelectWell.ToolTipText = "�������� ���������� �� ����� ��������";
            this.WL_AddSelectWell.Click += new System.EventHandler(this.WL_AddSelectWell_Click);
            // 
            // WL_AddContourWell
            // 
            this.WL_AddContourWell.Image = global::SmartPlus.Properties.Resources.Add_Cntr_Skv16;
            this.WL_AddContourWell.Name = "WL_AddContourWell";
            this.WL_AddContourWell.Size = new System.Drawing.Size(257, 22);
            this.WL_AddContourWell.Text = "�������� �� �������";
            this.WL_AddContourWell.ToolTipText = "�������� �������� ������������ ��������";
            this.WL_AddContourWell.Click += new System.EventHandler(this.WL_AddContourWell_Click);
            // 
            // WL_AddClipBoardWell
            // 
            this.WL_AddClipBoardWell.Image = global::SmartPlus.Properties.Resources.Add_ClipBoard_Skv16;
            this.WL_AddClipBoardWell.Name = "WL_AddClipBoardWell";
            this.WL_AddClipBoardWell.Size = new System.Drawing.Size(257, 22);
            this.WL_AddClipBoardWell.Text = "�������� �� ������ ������";
            this.WL_AddClipBoardWell.ToolTipText = "�������� �������� �� ������ ������";
            this.WL_AddClipBoardWell.Click += new System.EventHandler(this.WL_AddClipBoardWell_Click);
            // 
            // tsbDelete
            // 
            this.tsbDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDelete.Image = global::SmartPlus.Properties.Resources.Delete16;
            this.tsbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDelete.Name = "tsbDelete";
            this.tsbDelete.Size = new System.Drawing.Size(23, 22);
            this.tsbDelete.Text = "������� ��������� ��������";
            this.tsbDelete.Click += new System.EventHandler(this.tsbDelete_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbEditIcons
            // 
            this.tsbEditIcons.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEditIcons.Image = global::SmartPlus.Properties.Resources.EditIcon16;
            this.tsbEditIcons.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditIcons.Name = "tsbEditIcons";
            this.tsbEditIcons.Size = new System.Drawing.Size(23, 22);
            this.tsbEditIcons.Text = "�������������� ������� ���������� �������";
            this.tsbEditIcons.Click += new System.EventHandler(this.tsbEditIcons_Click);
            // 
            // tsbWelllistSettings
            // 
            this.tsbWelllistSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbWelllistSettings.Image = global::SmartPlus.Properties.Resources.Settings16;
            this.tsbWelllistSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbWelllistSettings.Name = "tsbWelllistSettings";
            this.tsbWelllistSettings.Size = new System.Drawing.Size(23, 22);
            this.tsbWelllistSettings.Text = "��������� ������ �������...";
            this.tsbWelllistSettings.Click += new System.EventHandler(this.tsbWelllistSettings_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 25);
            // 
            // WL_Graphics
            // 
            this.WL_Graphics.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.WL_Graphics.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WL_GraphicsByOneDate,
            this.WL_GraphicsByAllDate});
            this.WL_Graphics.Image = ((System.Drawing.Image)(resources.GetObject("WL_Graphics.Image")));
            this.WL_Graphics.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.WL_Graphics.Name = "WL_Graphics";
            this.WL_Graphics.Size = new System.Drawing.Size(32, 22);
            this.WL_Graphics.Text = "toolStripSplitButton1";
            // 
            // WL_GraphicsByOneDate
            // 
            this.WL_GraphicsByOneDate.Image = global::SmartPlus.Properties.Resources.wl_OneDateGraphics;
            this.WL_GraphicsByOneDate.Name = "WL_GraphicsByOneDate";
            this.WL_GraphicsByOneDate.Size = new System.Drawing.Size(243, 22);
            this.WL_GraphicsByOneDate.Text = "������ �� ������ ����";
            this.WL_GraphicsByOneDate.ToolTipText = "�������� ������ ������ �� ������ ����";
            this.WL_GraphicsByOneDate.Click += new System.EventHandler(this.WL_GraphicsByOneDate_Click);
            // 
            // WL_GraphicsByAllDate
            // 
            this.WL_GraphicsByAllDate.Image = global::SmartPlus.Properties.Resources.wl_AllDateGraphics;
            this.WL_GraphicsByAllDate.Name = "WL_GraphicsByAllDate";
            this.WL_GraphicsByAllDate.Size = new System.Drawing.Size(243, 22);
            this.WL_GraphicsByAllDate.Text = "��������� ������ �� ������";
            this.WL_GraphicsByAllDate.ToolTipText = "�������� ��������� ������ �� ������ �������";
            this.WL_GraphicsByAllDate.Click += new System.EventHandler(this.WL_GraphicsByAllDate_Click);
            // 
            // tsLabel
            // 
            this.tsLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsLabel.Name = "tsLabel";
            this.tsLabel.Size = new System.Drawing.Size(40, 22);
            this.tsLabel.Text = "N ���:";
            // 
            // pReportBuilder
            // 
            this.pReportBuilder.Controls.Add(this.tbReportBuilder);
            this.pReportBuilder.Location = new System.Drawing.Point(0, 21);
            this.pReportBuilder.Name = "pReportBuilder";
            this.pReportBuilder.Size = new System.Drawing.Size(251, 187);
            this.pReportBuilder.TabIndex = 0;
            // 
            // tbReportBuilder
            // 
            this.tbReportBuilder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rb_AddObject,
            this.rb_DeleteSelected});
            this.tbReportBuilder.Location = new System.Drawing.Point(0, 0);
            this.tbReportBuilder.Name = "tbReportBuilder";
            this.tbReportBuilder.Size = new System.Drawing.Size(251, 25);
            this.tbReportBuilder.TabIndex = 0;
            this.tbReportBuilder.Text = "toolStrip2";
            // 
            // rb_AddObject
            // 
            this.rb_AddObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rb_AddObject.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rb_AddOilfield,
            this.rb_AddArea,
            this.rb_AddWell,
            this.toolStripSeparator7,
            this.rb_AddSumParams,
            this.rb_AddMer,
            this.rb_AddGis,
            this.rb_AddPerf,
            this.rb_AddGtm,
            this.rb_AddRepair,
            this.rb_AddResearch});
            this.rb_AddObject.Image = global::SmartPlus.Properties.Resources.Add_Green16;
            this.rb_AddObject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rb_AddObject.Name = "rb_AddObject";
            this.rb_AddObject.Size = new System.Drawing.Size(29, 22);
            this.rb_AddObject.Text = "�������� ����� ������";
            // 
            // rb_AddOilfield
            // 
            this.rb_AddOilfield.Name = "rb_AddOilfield";
            this.rb_AddOilfield.Size = new System.Drawing.Size(206, 22);
            this.rb_AddOilfield.Text = "�������������";
            this.rb_AddOilfield.ToolTipText = "�������� �������������";
            this.rb_AddOilfield.Click += new System.EventHandler(this.rb_AddOilfield_Click);
            // 
            // rb_AddArea
            // 
            this.rb_AddArea.Name = "rb_AddArea";
            this.rb_AddArea.Size = new System.Drawing.Size(206, 22);
            this.rb_AddArea.Text = "������ ����������";
            this.rb_AddArea.Click += new System.EventHandler(this.rb_AddArea_Click);
            // 
            // rb_AddWell
            // 
            this.rb_AddWell.Name = "rb_AddWell";
            this.rb_AddWell.Size = new System.Drawing.Size(206, 22);
            this.rb_AddWell.Text = "��������";
            this.rb_AddWell.ToolTipText = "�������� ��������";
            this.rb_AddWell.Click += new System.EventHandler(this.rb_AddWell_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(203, 6);
            // 
            // rb_AddSumParams
            // 
            this.rb_AddSumParams.Name = "rb_AddSumParams";
            this.rb_AddSumParams.Size = new System.Drawing.Size(206, 22);
            this.rb_AddSumParams.Text = "��������� ����������";
            this.rb_AddSumParams.ToolTipText = "�������� ��������� ����������";
            this.rb_AddSumParams.Click += new System.EventHandler(this.rb_AddSumParams_Click);
            // 
            // rb_AddMer
            // 
            this.rb_AddMer.Name = "rb_AddMer";
            this.rb_AddMer.Size = new System.Drawing.Size(206, 22);
            this.rb_AddMer.Text = "���";
            this.rb_AddMer.ToolTipText = "�������� ���";
            this.rb_AddMer.Click += new System.EventHandler(this.rb_AddMer_Click);
            // 
            // rb_AddGis
            // 
            this.rb_AddGis.Name = "rb_AddGis";
            this.rb_AddGis.Size = new System.Drawing.Size(206, 22);
            this.rb_AddGis.Text = "���";
            this.rb_AddGis.ToolTipText = "�������� ���";
            this.rb_AddGis.Click += new System.EventHandler(this.rb_AddGis_Click);
            // 
            // rb_AddPerf
            // 
            this.rb_AddPerf.Name = "rb_AddPerf";
            this.rb_AddPerf.Size = new System.Drawing.Size(206, 22);
            this.rb_AddPerf.Text = "����������";
            this.rb_AddPerf.Click += new System.EventHandler(this.rb_AddPerf_Click);
            // 
            // rb_AddGtm
            // 
            this.rb_AddGtm.Name = "rb_AddGtm";
            this.rb_AddGtm.Size = new System.Drawing.Size(206, 22);
            this.rb_AddGtm.Text = "���";
            this.rb_AddGtm.Click += new System.EventHandler(this.rb_AddGtm_Click);
            // 
            // rb_AddRepair
            // 
            this.rb_AddRepair.Name = "rb_AddRepair";
            this.rb_AddRepair.Size = new System.Drawing.Size(206, 22);
            this.rb_AddRepair.Text = "������� �������";
            this.rb_AddRepair.Click += new System.EventHandler(this.rb_AddRepair_Click);
            // 
            // rb_AddResearch
            // 
            this.rb_AddResearch.Name = "rb_AddResearch";
            this.rb_AddResearch.Size = new System.Drawing.Size(206, 22);
            this.rb_AddResearch.Text = "������ ��������";
            this.rb_AddResearch.Click += new System.EventHandler(this.rb_AddResearch_Click);
            // 
            // rb_DeleteSelected
            // 
            this.rb_DeleteSelected.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rb_DeleteSelected.Image = global::SmartPlus.Properties.Resources.Delete16;
            this.rb_DeleteSelected.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rb_DeleteSelected.Name = "rb_DeleteSelected";
            this.rb_DeleteSelected.Size = new System.Drawing.Size(23, 22);
            this.rb_DeleteSelected.Text = "������� ��������� ������";
            this.rb_DeleteSelected.Click += new System.EventHandler(this.rb_DeleteSelected_Click);
            // 
            // uDockManager
            // 
            this.uDockManager.AllowDrop = false;
            this.uDockManager.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Disabled;
            this.uDockManager.AnimationEnabled = false;
            this.uDockManager.AnimationSpeed = Infragistics.Win.UltraWinDock.AnimationSpeed.StandardSpeedPlus2;
            this.uDockManager.CaptionButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button;
            this.uDockManager.CaptionStyle = Infragistics.Win.UltraWinDock.CaptionStyle.VisualStudio2008;
            this.uDockManager.CompressUnpinnedTabs = false;
            this.uDockManager.DefaultGroupSettings.TabSizing = Infragistics.Win.UltraWinTabs.TabSizing.AutoSize;
            this.uDockManager.DefaultGroupSettings.TabStyle = Infragistics.Win.UltraWinTabs.TabStyle.Flat;
            appearance1.FontData.Name = "Tahoma";
            appearance1.FontData.SizeInPoints = 8.25F;
            this.uDockManager.DefaultPaneSettings.ActiveCaptionAppearance = appearance1;
            appearance5.FontData.Name = "Calibri";
            appearance5.FontData.SizeInPoints = 8.25F;
            this.uDockManager.DefaultPaneSettings.ActiveTabAppearance = appearance5;
            appearance2.FontData.Name = "Tahoma";
            appearance2.FontData.SizeInPoints = 8.25F;
            this.uDockManager.DefaultPaneSettings.CaptionAppearance = appearance2;
            appearance6.FontData.Name = "Calibri";
            appearance6.FontData.SizeInPoints = 8.25F;
            this.uDockManager.DefaultPaneSettings.SelectedTabAppearance = appearance6;
            appearance4.FontData.Name = "Calibri";
            appearance4.FontData.SizeInPoints = 8.25F;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Bottom";
            this.uDockManager.DefaultPaneSettings.TabAppearance = appearance4;
            this.uDockManager.DefaultPaneSettings.TabWidth = 90;
            dockAreaPane1.DockedBefore = new System.Guid("52e65834-a306-49c5-8035-2deccf6eb073");
            dockAreaPane1.FloatingLocation = new System.Drawing.Point(197, 414);
            dockableGroupPane1.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
            dockableControlPane1.Control = this.pInspector;
            dockableControlPane1.Key = "2";
            dockableControlPane1.OriginalControlBounds = new System.Drawing.Rectangle(218, 13, 261, 44);
            dockableControlPane1.Size = new System.Drawing.Size(100, 16);
            dockableControlPane1.Text = "���������";
            dockableControlPane2.Control = this.pActiveOilField;
            dockableControlPane2.Key = "7";
            dockableControlPane2.OriginalControlBounds = new System.Drawing.Rectangle(29, 35, 230, 217);
            dockableControlPane2.Size = new System.Drawing.Size(100, 100);
            dockableControlPane2.Text = "�������������";
            dockableControlPane3.Control = this.pFileServer;
            dockableControlPane3.Key = "15";
            dockableControlPane3.OriginalControlBounds = new System.Drawing.Rectangle(228, 117, 412, 438);
            dockableControlPane3.Size = new System.Drawing.Size(100, 100);
            dockableControlPane3.Text = "�����";
            dockableGroupPane1.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane1,
            dockableControlPane2,
            dockableControlPane3});
            dockableGroupPane1.Size = new System.Drawing.Size(100, 16);
            dockableControlPane4.Control = this.pWork;
            dockableControlPane4.Key = "11";
            dockableControlPane4.OriginalControlBounds = new System.Drawing.Rectangle(84, 192, 295, 300);
            dockableControlPane4.Size = new System.Drawing.Size(18, 14);
            dockableControlPane4.Text = "������� �����";
            dockableControlPane5.Control = this.pFilterOilObj;
            dockableControlPane5.Key = "16";
            dockableControlPane5.OriginalControlBounds = new System.Drawing.Rectangle(48, 96, 305, 248);
            dockableControlPane5.Size = new System.Drawing.Size(155, 18);
            dockableControlPane5.Text = "������ ��������";
            dockAreaPane1.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableGroupPane1,
            dockableControlPane4,
            dockableControlPane5});
            dockAreaPane1.Size = new System.Drawing.Size(251, 690);
            dockAreaPane2.DockedBefore = new System.Guid("4e226684-033d-4224-af0f-7933184d53a1");
            dockAreaPane2.FloatingLocation = new System.Drawing.Point(51, 473);
            dockAreaPane2.Size = new System.Drawing.Size(164, 670);
            dockAreaPane3.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
            dockAreaPane3.DockedBefore = new System.Guid("1c24d395-937f-4654-b4a6-af7dc5ae2061");
            dockAreaPane3.FloatingLocation = new System.Drawing.Point(1481, 997);
            dockAreaPane3.Size = new System.Drawing.Size(208, 266);
            dockAreaPane4.DockedBefore = new System.Guid("718ef331-9959-4dae-90b0-0938cb664dce");
            dockAreaPane4.FloatingLocation = new System.Drawing.Point(1548, 650);
            dockAreaPane4.Size = new System.Drawing.Size(98, 471);
            dockAreaPane5.DockedBefore = new System.Guid("1051a24c-248d-4d4e-9a7a-babe9f79aefa");
            dockAreaPane5.FloatingLocation = new System.Drawing.Point(2380, 392);
            dockAreaPane5.Size = new System.Drawing.Size(98, 415);
            dockAreaPane6.DockedBefore = new System.Guid("71d3936f-2edb-45db-9bf1-29df42031519");
            dockAreaPane6.FloatingLocation = new System.Drawing.Point(2380, 392);
            dockableControlPane6.Control = this.tsPlaneWellContainer;
            dockableControlPane6.Key = "10";
            dockableControlPane6.OriginalControlBounds = new System.Drawing.Rectangle(111, 75, 344, 261);
            dockableControlPane6.Size = new System.Drawing.Size(100, 100);
            dockableControlPane6.Text = "�������";
            dockAreaPane6.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane6});
            dockAreaPane6.Size = new System.Drawing.Size(233, 690);
            dockAreaPane7.DockedBefore = new System.Guid("b3496778-0799-4e13-9b7f-363b98b85dce");
            dockAreaPane7.FloatingLocation = new System.Drawing.Point(1412, 564);
            dockAreaPane7.Size = new System.Drawing.Size(741, 360);
            dockAreaPane8.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.VerticalSplit;
            dockAreaPane8.DockedBefore = new System.Guid("6e8f6e27-633e-4de0-bd08-43b83e74253f");
            dockAreaPane8.FloatingLocation = new System.Drawing.Point(1513, 639);
            dockAreaPane8.Size = new System.Drawing.Size(130, 320);
            dockAreaPane9.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
            dockAreaPane9.DockedBefore = new System.Guid("73c7342e-086d-486c-b3d2-0283db9d7290");
            dockAreaPane9.FloatingLocation = new System.Drawing.Point(1412, 564);
            dockableControlPane7.Control = this.pGraphics;
            dockableControlPane7.Key = "1";
            dockableControlPane7.OriginalControlBounds = new System.Drawing.Rectangle(145, 86, 296, 182);
            dockableControlPane7.Size = new System.Drawing.Size(729, 100);
            dockableControlPane7.Text = "�������";
            dockableControlPane7.TextTab = "�������";
            dockableControlPane8.Control = this.tsCorSchContainer;
            dockableControlPane8.FlyoutSize = new System.Drawing.Size(98, -1);
            dockableControlPane8.Key = "6";
            dockableControlPane8.OriginalControlBounds = new System.Drawing.Rectangle(214, 119, 51, 125);
            dockableControlPane8.Settings.AllowMaximize = Infragistics.Win.DefaultableBoolean.True;
            dockableControlPane8.Settings.AllowMinimize = Infragistics.Win.DefaultableBoolean.True;
            dockableControlPane8.Size = new System.Drawing.Size(100, 100);
            dockableControlPane8.Text = "���.�����";
            dockableControlPane9.Control = this.pGisView;
            dockableControlPane9.Key = "0";
            dockableControlPane9.OriginalControlBounds = new System.Drawing.Rectangle(177, 46, 332, 289);
            dockableControlPane9.Size = new System.Drawing.Size(100, 100);
            dockableControlPane9.Text = "���";
            dockableControlPane10.Control = this.pMerView;
            dockableControlPane10.FlyoutSize = new System.Drawing.Size(244, -1);
            dockableControlPane10.Key = "8";
            dockableControlPane10.OriginalControlBounds = new System.Drawing.Rectangle(180, 114, 211, 335);
            dockableControlPane10.Size = new System.Drawing.Size(99, 100);
            dockableControlPane10.Text = "���";
            dockableControlPane11.Control = this.pChessView;
            dockableControlPane11.Key = "17";
            dockableControlPane11.OriginalControlBounds = new System.Drawing.Rectangle(63, 52, 274, 272);
            dockableControlPane11.Size = new System.Drawing.Size(100, 98);
            dockableControlPane11.Text = "��������";
            dockableControlPane12.Control = this.pDiscussion;
            dockableControlPane12.Key = "9";
            dockableControlPane12.OriginalControlBounds = new System.Drawing.Rectangle(100, 138, 372, 410);
            dockableControlPane12.Size = new System.Drawing.Size(100, 100);
            dockableControlPane12.Text = "����������";
            dockableControlPane13.Control = this.pAreasView;
            dockableControlPane13.Key = "18";
            dockableControlPane13.OriginalControlBounds = new System.Drawing.Rectangle(47, 86, 371, 242);
            dockableControlPane13.Size = new System.Drawing.Size(100, 100);
            dockableControlPane13.Text = "������ ����������";
            dockableControlPane14.Control = this.pCore;
            dockableControlPane14.Key = "3";
            dockableControlPane14.OriginalControlBounds = new System.Drawing.Rectangle(50, 87, 361, 225);
            dockableControlPane14.Size = new System.Drawing.Size(100, 100);
            dockableControlPane14.Text = "����: �������";
            dockableControlPane15.Control = this.pCoreTest;
            dockableControlPane15.Key = "4";
            dockableControlPane15.OriginalControlBounds = new System.Drawing.Rectangle(135, 83, 309, 171);
            dockableControlPane15.Size = new System.Drawing.Size(100, 100);
            dockableControlPane15.Text = "����: ������������";
            dockAreaPane9.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane7,
            dockableControlPane8,
            dockableControlPane9,
            dockableControlPane10,
            dockableControlPane11,
            dockableControlPane12,
            dockableControlPane13,
            dockableControlPane14,
            dockableControlPane15});
            dockAreaPane9.Size = new System.Drawing.Size(864, 253);
            dockAreaPane10.DockedBefore = new System.Guid("14dafc95-c901-47bd-8e7c-4c7d79f0d3f0");
            dockAreaPane10.FloatingLocation = new System.Drawing.Point(1973, 701);
            dockAreaPane10.Size = new System.Drawing.Size(277, 696);
            dockAreaPane11.DockedBefore = new System.Guid("21650b0f-0f54-4043-8af3-688a6862ab09");
            dockAreaPane11.FloatingLocation = new System.Drawing.Point(2027, 415);
            dockAreaPane11.Size = new System.Drawing.Size(98, 471);
            dockAreaPane12.DockedBefore = new System.Guid("1dd0c5b2-9c55-4a78-ac25-c951b4917886");
            dockAreaPane12.FloatingLocation = new System.Drawing.Point(2042, 565);
            dockAreaPane12.Size = new System.Drawing.Size(98, 415);
            dockAreaPane13.DockedBefore = new System.Guid("958ffdea-9317-45bd-adca-a308c9faebc0");
            dockAreaPane13.FloatingLocation = new System.Drawing.Point(1918, 647);
            dockAreaPane13.Size = new System.Drawing.Size(494, 87);
            dockAreaPane14.DockedBefore = new System.Guid("189644d2-7469-4cea-be72-effa01e7fb2d");
            dockAreaPane14.FloatingLocation = new System.Drawing.Point(1750, 660);
            dockAreaPane14.Size = new System.Drawing.Size(954, 505);
            dockAreaPane15.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
            dockAreaPane15.DockedBefore = new System.Guid("3099c5aa-cb8d-47be-a79a-a17a28ba3579");
            dockAreaPane15.FloatingLocation = new System.Drawing.Point(2070, 384);
            dockAreaPane15.Size = new System.Drawing.Size(189, 674);
            dockAreaPane16.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
            dockAreaPane16.DockedBefore = new System.Guid("9a5e7bae-d31d-4f13-a990-c2f280cec272");
            dockAreaPane16.FloatingLocation = new System.Drawing.Point(2070, 384);
            dockableControlPane16.Closed = true;
            dockableControlPane16.Control = this.pProperties;
            dockableControlPane16.Key = "12";
            dockableControlPane16.OriginalControlBounds = new System.Drawing.Rectangle(85, 102, 243, 371);
            dockableControlPane16.Size = new System.Drawing.Size(100, 51);
            dockableControlPane16.Text = "��������";
            dockableControlPane17.Closed = true;
            dockableControlPane17.Control = this.pReLog;
            dockableControlPane17.FlyoutSize = new System.Drawing.Size(88, -1);
            dockableControlPane17.Key = "13";
            dockableControlPane17.OriginalControlBounds = new System.Drawing.Rectangle(426, 136, 229, 402);
            dockableControlPane17.Size = new System.Drawing.Size(100, 100);
            dockableControlPane17.Text = "���������";
            dockAreaPane16.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane16,
            dockableControlPane17});
            dockAreaPane16.SelectedTabIndex = 1;
            dockAreaPane16.Size = new System.Drawing.Size(194, 348);
            dockAreaPane17.DockedBefore = new System.Guid("4c654b48-1c37-440b-b70d-f06068ad9dc0");
            dockAreaPane17.FloatingLocation = new System.Drawing.Point(2049, 740);
            dockAreaPane17.Size = new System.Drawing.Size(98, 346);
            dockAreaPane18.DockedBefore = new System.Guid("53848605-5ca2-4bd4-a418-496ef2398626");
            dockAreaPane18.FloatingLocation = new System.Drawing.Point(2174, 715);
            dockAreaPane18.Size = new System.Drawing.Size(98, 415);
            dockAreaPane19.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.VerticalSplit;
            dockAreaPane19.DockedBefore = new System.Guid("f456507c-4bcc-4d54-a284-9d25722c8594");
            dockAreaPane19.FloatingLocation = new System.Drawing.Point(2010, 357);
            dockAreaPane19.Size = new System.Drawing.Size(351, 696);
            dockAreaPane20.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.VerticalSplit;
            dockAreaPane20.DockedBefore = new System.Guid("c8d25a7a-e08b-4aea-a540-80d95bbfd5c1");
            dockAreaPane20.FloatingLocation = new System.Drawing.Point(2010, 357);
            dockableControlPane18.Closed = true;
            dockableControlPane18.Control = this.pWellListSettings;
            dockableControlPane18.Key = "14";
            dockableControlPane18.OriginalControlBounds = new System.Drawing.Rectangle(19, 29, 407, 335);
            dockableControlPane18.Size = new System.Drawing.Size(261, 100);
            dockableControlPane18.Text = "������ �������";
            dockAreaPane20.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane18});
            dockAreaPane20.Size = new System.Drawing.Size(402, 349);
            dockAreaPane21.DockedBefore = new System.Guid("18528aa3-5517-420a-ab0f-84721c6adcac");
            dockAreaPane21.FloatingLocation = new System.Drawing.Point(2284, 335);
            dockAreaPane21.Size = new System.Drawing.Size(97, 348);
            dockAreaPane22.DockedBefore = new System.Guid("e4a139e0-1452-4ffa-9aa5-285bf965b677");
            dockAreaPane22.FloatingLocation = new System.Drawing.Point(2284, 335);
            dockableControlPane19.Closed = true;
            dockableControlPane19.Control = this.pReportBuilder;
            dockableControlPane19.Key = "5";
            dockableControlPane19.OriginalControlBounds = new System.Drawing.Rectangle(116, 112, 366, 408);
            dockableControlPane19.Size = new System.Drawing.Size(100, 100);
            dockableControlPane19.Text = "����������� �������";
            dockAreaPane22.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane19});
            dockAreaPane22.Size = new System.Drawing.Size(281, 375);
            dockAreaPane23.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
            dockAreaPane23.DockedBefore = new System.Guid("110000cd-1db0-49c4-a249-0c92a26988b5");
            dockAreaPane23.FloatingLocation = new System.Drawing.Point(2140, 302);
            dockAreaPane23.Size = new System.Drawing.Size(194, 348);
            dockAreaPane24.DockedBefore = new System.Guid("6208a103-ef10-4c37-a824-a1615156f398");
            dockAreaPane24.FloatingLocation = new System.Drawing.Point(2309, 301);
            dockAreaPane24.Size = new System.Drawing.Size(97, 393);
            dockAreaPane25.FloatingLocation = new System.Drawing.Point(2786, 619);
            dockAreaPane25.Size = new System.Drawing.Size(97, 393);
            this.uDockManager.DockAreas.AddRange(new Infragistics.Win.UltraWinDock.DockAreaPane[] {
            dockAreaPane1,
            dockAreaPane2,
            dockAreaPane3,
            dockAreaPane4,
            dockAreaPane5,
            dockAreaPane6,
            dockAreaPane7,
            dockAreaPane8,
            dockAreaPane9,
            dockAreaPane10,
            dockAreaPane11,
            dockAreaPane12,
            dockAreaPane13,
            dockAreaPane14,
            dockAreaPane15,
            dockAreaPane16,
            dockAreaPane17,
            dockAreaPane18,
            dockAreaPane19,
            dockAreaPane20,
            dockAreaPane21,
            dockAreaPane22,
            dockAreaPane23,
            dockAreaPane24,
            dockAreaPane25});
            this.uDockManager.DragIndicatorStyle = Infragistics.Win.UltraWinDock.DragIndicatorStyle.VisualStudio2005;
            this.uDockManager.DragWindowStyle = Infragistics.Win.UltraWinDock.DragWindowStyle.LayeredWindowWithIndicators;
            this.uDockManager.HostControl = this;
            this.uDockManager.HotTracking = false;
            this.uDockManager.SaveSettings = true;
            this.uDockManager.ScaleImages = Infragistics.Win.ScaleImage.Never;
            this.uDockManager.SettingsKey = "fMain.uDockManager";
            this.uDockManager.SplitterBarWidth = 3;
            this.uDockManager.TextRenderingMode = Infragistics.Win.TextRenderingMode.GDIPlus;
            this.uDockManager.UnpinnedTabHoverAction = Infragistics.Win.UltraWinDock.UnpinnedTabHoverAction.DisplayToolTip;
            this.uDockManager.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uDockManager.WindowStyle = Infragistics.Win.UltraWinDock.WindowStyle.VSNet;
            // 
            // tsbCancel
            // 
            this.tsbCancel.AutoSize = false;
            this.tsbCancel.AutoToolTip = false;
            this.tsbCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tsbCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbCancel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCancel.Name = "tsbCancel";
            this.tsbCancel.ShowDropDownArrow = false;
            this.tsbCancel.Size = new System.Drawing.Size(58, 20);
            this.tsbCancel.Text = "������";
            this.tsbCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.tsbCancel.Visible = false;
            this.tsbCancel.Click += new System.EventHandler(this.tsbCancel_Click);
            // 
            // mMenu
            // 
            this.mMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuFile,
            this.mMenuFlooding,
            this.mMenuLoading,
            this.mMenuSevice,
            this.mMenuReports,
            this.mMenuProj,
            this.mMenuWindows,
            this.mMenuHelp,
            this.toolStripMenuItem1});
            this.mMenu.Location = new System.Drawing.Point(0, 0);
            this.mMenu.Name = "mMenu";
            this.mMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.mMenu.Size = new System.Drawing.Size(1354, 24);
            this.mMenu.TabIndex = 12;
            this.mMenu.Text = "Menu";
            // 
            // mMenuFile
            // 
            this.mMenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuGlobalProjShow,
            this.mMenuProjOpen,
            this.mMenuOpenImage,
            this.mMenuPrintMap,
            this.mMenuExport,
            this.mMenuClose,
            this.mMenuSeparator,
            this.mMenuExit});
            this.mMenuFile.Name = "mMenuFile";
            this.mMenuFile.Size = new System.Drawing.Size(48, 20);
            this.mMenuFile.Text = "����";
            // 
            // mMenuGlobalProjShow
            // 
            this.mMenuGlobalProjShow.Name = "mMenuGlobalProjShow";
            this.mMenuGlobalProjShow.Size = new System.Drawing.Size(205, 22);
            this.mMenuGlobalProjShow.Text = "������ ��������...";
            this.mMenuGlobalProjShow.Click += new System.EventHandler(this.mMenuGlobalProjShow_Click);
            // 
            // mMenuProjOpen
            // 
            this.mMenuProjOpen.Name = "mMenuProjOpen";
            this.mMenuProjOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.mMenuProjOpen.Size = new System.Drawing.Size(205, 22);
            this.mMenuProjOpen.Text = "������� ...";
            this.mMenuProjOpen.Click += new System.EventHandler(this.mMenuProjOpen_Click);
            // 
            // mMenuOpenImage
            // 
            this.mMenuOpenImage.Name = "mMenuOpenImage";
            this.mMenuOpenImage.Size = new System.Drawing.Size(205, 22);
            this.mMenuOpenImage.Text = "��������� �����������";
            this.mMenuOpenImage.Click += new System.EventHandler(this.mMenuOpenImage_Click);
            // 
            // mMenuPrintMap
            // 
            this.mMenuPrintMap.Name = "mMenuPrintMap";
            this.mMenuPrintMap.Size = new System.Drawing.Size(205, 22);
            this.mMenuPrintMap.Text = "������ �����...";
            // 
            // mMenuExport
            // 
            this.mMenuExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mmExportAreasContours});
            this.mMenuExport.Name = "mMenuExport";
            this.mMenuExport.Size = new System.Drawing.Size(205, 22);
            this.mMenuExport.Text = "�������";
            // 
            // mmExportAreasContours
            // 
            this.mmExportAreasContours.Name = "mmExportAreasContours";
            this.mmExportAreasContours.Size = new System.Drawing.Size(156, 22);
            this.mmExportAreasContours.Text = "������� �����";
            this.mmExportAreasContours.Click += new System.EventHandler(this.mmExportAreasContours_Click);
            // 
            // mMenuClose
            // 
            this.mMenuClose.Enabled = false;
            this.mMenuClose.Name = "mMenuClose";
            this.mMenuClose.Size = new System.Drawing.Size(205, 22);
            this.mMenuClose.Text = "�������";
            this.mMenuClose.Click += new System.EventHandler(this.mMenuClose_Click);
            // 
            // mMenuSeparator
            // 
            this.mMenuSeparator.Name = "mMenuSeparator";
            this.mMenuSeparator.Size = new System.Drawing.Size(202, 6);
            // 
            // mMenuExit
            // 
            this.mMenuExit.Name = "mMenuExit";
            this.mMenuExit.Size = new System.Drawing.Size(205, 22);
            this.mMenuExit.Text = "�����";
            this.mMenuExit.Click += new System.EventHandler(this.mMenuExit_Click);
            // 
            // mMenuFlooding
            // 
            this.mMenuFlooding.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuLoadCompensation,
            this.tsbDrawAreasOverlay,
            this.tsbAreasParamsByDate});
            this.mMenuFlooding.Name = "mMenuFlooding";
            this.mMenuFlooding.Size = new System.Drawing.Size(84, 20);
            this.mMenuFlooding.Text = "����������";
            // 
            // mMenuLoadCompensation
            // 
            this.mMenuLoadCompensation.Name = "mMenuLoadCompensation";
            this.mMenuLoadCompensation.Size = new System.Drawing.Size(275, 22);
            this.mMenuLoadCompensation.Text = "��������� ����������� �� �����";
            this.mMenuLoadCompensation.Click += new System.EventHandler(this.mMenuLoadCompensation_Click);
            // 
            // tsbDrawAreasOverlay
            // 
            this.tsbDrawAreasOverlay.CheckOnClick = true;
            this.tsbDrawAreasOverlay.Name = "tsbDrawAreasOverlay";
            this.tsbDrawAreasOverlay.Size = new System.Drawing.Size(275, 22);
            this.tsbDrawAreasOverlay.Text = "���������� ������ ������ ��������";
            this.tsbDrawAreasOverlay.Click += new System.EventHandler(this.tsbDrawAreasOverlay_Click);
            // 
            // tsbAreasParamsByDate
            // 
            this.tsbAreasParamsByDate.Name = "tsbAreasParamsByDate";
            this.tsbAreasParamsByDate.Size = new System.Drawing.Size(275, 22);
            this.tsbAreasParamsByDate.Text = "��������� ����� �� �����";
            this.tsbAreasParamsByDate.Click += new System.EventHandler(this.tsbAreasParamsByDate_Click);
            // 
            // mMenuLoading
            // 
            this.mMenuLoading.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuLoadContoursAreasFromWF,
            this.mMenuLoadTable81FromFile,
            this.mMenuLoadBizPlanFromFile,
            this.mMenuCoreLoading,
            this.mMenuLoadAreasPVTFromFile,
            this.mMenuLoadDepositsFromFolder,
            this.mMenuLoadPressure});
            this.mMenuLoading.Name = "mMenuLoading";
            this.mMenuLoading.Size = new System.Drawing.Size(110, 20);
            this.mMenuLoading.Text = "�������� ������";
            // 
            // mMenuLoadContoursAreasFromWF
            // 
            this.mMenuLoadContoursAreasFromWF.Name = "mMenuLoadContoursAreasFromWF";
            this.mMenuLoadContoursAreasFromWF.Size = new System.Drawing.Size(285, 22);
            this.mMenuLoadContoursAreasFromWF.Text = "��������� ������� � ������";
            this.mMenuLoadContoursAreasFromWF.Click += new System.EventHandler(this.mMenuLoadContoursAreasFromWF_Click);
            // 
            // mMenuLoadTable81FromFile
            // 
            this.mMenuLoadTable81FromFile.Name = "mMenuLoadTable81FromFile";
            this.mMenuLoadTable81FromFile.Size = new System.Drawing.Size(285, 22);
            this.mMenuLoadTable81FromFile.Text = "��������� ��������� ����������";
            this.mMenuLoadTable81FromFile.Click += new System.EventHandler(this.mMenuLoadTable81FromFile_Click);
            // 
            // mMenuLoadBizPlanFromFile
            // 
            this.mMenuLoadBizPlanFromFile.AccessibleName = "";
            this.mMenuLoadBizPlanFromFile.Name = "mMenuLoadBizPlanFromFile";
            this.mMenuLoadBizPlanFromFile.Size = new System.Drawing.Size(285, 22);
            this.mMenuLoadBizPlanFromFile.Text = "��������� ������-���� ������";
            this.mMenuLoadBizPlanFromFile.Click += new System.EventHandler(this.mMenuLoadBizPlanFromFile_Click);
            // 
            // mMenuCoreLoading
            // 
            this.mMenuCoreLoading.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuCoreLoad,
            this.mMenuCoreTestLoad});
            this.mMenuCoreLoading.Name = "mMenuCoreLoading";
            this.mMenuCoreLoading.Size = new System.Drawing.Size(285, 22);
            this.mMenuCoreLoading.Text = "����";
            // 
            // mMenuCoreLoad
            // 
            this.mMenuCoreLoad.Name = "mMenuCoreLoad";
            this.mMenuCoreLoad.Size = new System.Drawing.Size(297, 22);
            this.mMenuCoreLoad.Text = "��������� ������ �����...";
            this.mMenuCoreLoad.Click += new System.EventHandler(this.mMenuCoreLoad_Click);
            // 
            // mMenuCoreTestLoad
            // 
            this.mMenuCoreTestLoad.Name = "mMenuCoreTestLoad";
            this.mMenuCoreTestLoad.Size = new System.Drawing.Size(297, 22);
            this.mMenuCoreTestLoad.Text = "��������� ������ ������������ �����...";
            this.mMenuCoreTestLoad.Click += new System.EventHandler(this.mMenuCoreTestLoad_Click);
            // 
            // mMenuLoadAreasPVTFromFile
            // 
            this.mMenuLoadAreasPVTFromFile.Name = "mMenuLoadAreasPVTFromFile";
            this.mMenuLoadAreasPVTFromFile.Size = new System.Drawing.Size(285, 22);
            this.mMenuLoadAreasPVTFromFile.Text = "��������� PVT �� ������� ����������";
            // 
            // mMenuLoadDepositsFromFolder
            // 
            this.mMenuLoadDepositsFromFolder.Name = "mMenuLoadDepositsFromFolder";
            this.mMenuLoadDepositsFromFolder.Size = new System.Drawing.Size(285, 22);
            this.mMenuLoadDepositsFromFolder.Text = "��������� ������";
            // 
            // mMenuLoadPressure
            // 
            this.mMenuLoadPressure.Name = "mMenuLoadPressure";
            this.mMenuLoadPressure.Size = new System.Drawing.Size(285, 22);
            this.mMenuLoadPressure.Text = "��������� �������� �� �����";
            // 
            // mMenuSevice
            // 
            this.mMenuSevice.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuSaveCoef,
            this.mmFindSkv,
            this.toolStripSeparator11,
            this.mMenuAppSettings});
            this.mMenuSevice.Name = "mMenuSevice";
            this.mMenuSevice.Size = new System.Drawing.Size(59, 20);
            this.mMenuSevice.Text = "������";
            // 
            // mMenuSaveCoef
            // 
            this.mMenuSaveCoef.Name = "mMenuSaveCoef";
            this.mMenuSaveCoef.Size = new System.Drawing.Size(229, 22);
            this.mMenuSaveCoef.Text = "��������� ����������� ��";
            this.mMenuSaveCoef.Click += new System.EventHandler(this.mMenuSaveCoef_Click);
            // 
            // mmFindSkv
            // 
            this.mmFindSkv.Name = "mmFindSkv";
            this.mmFindSkv.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.mmFindSkv.Size = new System.Drawing.Size(229, 22);
            this.mmFindSkv.Text = "����� ��������...";
            this.mmFindSkv.Click += new System.EventHandler(this.mmFindSkv_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(226, 6);
            // 
            // mMenuAppSettings
            // 
            this.mMenuAppSettings.Name = "mMenuAppSettings";
            this.mMenuAppSettings.Size = new System.Drawing.Size(229, 22);
            this.mMenuAppSettings.Text = "���������...";
            this.mMenuAppSettings.Click += new System.EventHandler(this.mMenuAppSettings_Click);
            // 
            // mMenuReports
            // 
            this.mMenuReports.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuReportChess,
            this.mMenuReportFloodingAnalysis,
            this.mMenuReportAreaPredict,
            this.mMenuReportDepositOIZ,
            this.mMenuReportCore,
            this.mMenuReportMerProduction,
            this.toolStripMenuItem3,
            this.mMenuCreateGtmList,
            this.mMenuAutoMatchGTM});
            this.mMenuReports.Name = "mMenuReports";
            this.mMenuReports.Size = new System.Drawing.Size(60, 20);
            this.mMenuReports.Text = "������";
            // 
            // mMenuReportChess
            // 
            this.mMenuReportChess.Name = "mMenuReportChess";
            this.mMenuReportChess.Size = new System.Drawing.Size(304, 22);
            this.mMenuReportChess.Text = "����� �� ��������";
            this.mMenuReportChess.Click += new System.EventHandler(this.mMenuReportChess_Click);
            // 
            // mMenuReportFloodingAnalysis
            // 
            this.mMenuReportFloodingAnalysis.Name = "mMenuReportFloodingAnalysis";
            this.mMenuReportFloodingAnalysis.Size = new System.Drawing.Size(304, 22);
            this.mMenuReportFloodingAnalysis.Text = "����� \'������ ����������\'";
            this.mMenuReportFloodingAnalysis.Click += new System.EventHandler(this.mMenuReportFloodingAnalysis_Click);
            // 
            // mMenuReportAreaPredict
            // 
            this.mMenuReportAreaPredict.Name = "mMenuReportAreaPredict";
            this.mMenuReportAreaPredict.Size = new System.Drawing.Size(304, 22);
            this.mMenuReportAreaPredict.Text = "����� \'������� �� ������� ����������\'...";
            this.mMenuReportAreaPredict.Click += new System.EventHandler(this.mMenuReportAreaPredict_Click);
            // 
            // mMenuReportDepositOIZ
            // 
            this.mMenuReportDepositOIZ.Name = "mMenuReportDepositOIZ";
            this.mMenuReportDepositOIZ.Size = new System.Drawing.Size(304, 22);
            this.mMenuReportDepositOIZ.Text = "����� \'��������� �� �������\'";
            this.mMenuReportDepositOIZ.Click += new System.EventHandler(this.mMenuReportDepositOIZ_Click);
            // 
            // mMenuReportCore
            // 
            this.mMenuReportCore.Name = "mMenuReportCore";
            this.mMenuReportCore.Size = new System.Drawing.Size(304, 22);
            this.mMenuReportCore.Text = "����� �� �����";
            this.mMenuReportCore.Click += new System.EventHandler(this.mMenuReportCore_Click);
            // 
            // mMenuReportMerProduction
            // 
            this.mMenuReportMerProduction.Name = "mMenuReportMerProduction";
            this.mMenuReportMerProduction.Size = new System.Drawing.Size(304, 22);
            this.mMenuReportMerProduction.Text = "����� �� ������ ���";
            this.mMenuReportMerProduction.Click += new System.EventHandler(this.mMenuReportMerProduction_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(301, 6);
            // 
            // mMenuCreateGtmList
            // 
            this.mMenuCreateGtmList.Name = "mMenuCreateGtmList";
            this.mMenuCreateGtmList.Size = new System.Drawing.Size(304, 22);
            this.mMenuCreateGtmList.Text = "������ ���";
            this.mMenuCreateGtmList.Click += new System.EventHandler(this.mMenuCreateGtmList_Click);
            // 
            // mMenuAutoMatchGTM
            // 
            this.mMenuAutoMatchGTM.Name = "mMenuAutoMatchGTM";
            this.mMenuAutoMatchGTM.Size = new System.Drawing.Size(304, 22);
            this.mMenuAutoMatchGTM.Text = "����������� ���";
            this.mMenuAutoMatchGTM.Click += new System.EventHandler(this.mMenuAutoMatchGTM_Click);
            // 
            // mMenuProj
            // 
            this.mMenuProj.Name = "mMenuProj";
            this.mMenuProj.Size = new System.Drawing.Size(68, 20);
            this.mMenuProj.Text = "�������";
            // 
            // mMenuWindows
            // 
            this.mMenuWindows.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuWinGis,
            this.mMenuWinGraphics,
            this.mMenuWinInspector,
            this.mMenuWinCore,
            this.mMenuWinCoreTest,
            this.mMenuReportBuilder,
            this.mMenuWinCorScheme,
            this.mMenuWinMest,
            this.mMenuWinMer,
            this.mMenuWinDiscussion,
            this.mMenuWinPlane,
            this.mMenuWinWorkFolder,
            this.mMenuWinProperties,
            this.mMenuWinLog,
            this.mMenuWinWellList,
            this.mMenuWinFileServer,
            this.mMenuWinFilter,
            this.mMenuWinChess,
            this.mMenuWinOilFieldAreas,
            this.toolStripMenuItem2,
            this.mMenuResetWindows});
            this.mMenuWindows.Name = "mMenuWindows";
            this.mMenuWindows.Size = new System.Drawing.Size(48, 20);
            this.mMenuWindows.Text = "����";
            // 
            // mMenuWinGis
            // 
            this.mMenuWinGis.Name = "mMenuWinGis";
            this.mMenuWinGis.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinGis.Text = "���";
            this.mMenuWinGis.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinGraphics
            // 
            this.mMenuWinGraphics.Name = "mMenuWinGraphics";
            this.mMenuWinGraphics.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinGraphics.Text = "�������";
            this.mMenuWinGraphics.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinInspector
            // 
            this.mMenuWinInspector.Name = "mMenuWinInspector";
            this.mMenuWinInspector.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinInspector.Text = "���������";
            this.mMenuWinInspector.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinCore
            // 
            this.mMenuWinCore.Name = "mMenuWinCore";
            this.mMenuWinCore.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinCore.Text = "����: �������";
            this.mMenuWinCore.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinCoreTest
            // 
            this.mMenuWinCoreTest.Name = "mMenuWinCoreTest";
            this.mMenuWinCoreTest.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinCoreTest.Text = "����: ������������";
            this.mMenuWinCoreTest.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuReportBuilder
            // 
            this.mMenuReportBuilder.Name = "mMenuReportBuilder";
            this.mMenuReportBuilder.Size = new System.Drawing.Size(242, 22);
            this.mMenuReportBuilder.Text = "����������� �������";
            this.mMenuReportBuilder.Visible = false;
            this.mMenuReportBuilder.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinCorScheme
            // 
            this.mMenuWinCorScheme.Name = "mMenuWinCorScheme";
            this.mMenuWinCorScheme.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinCorScheme.Text = "��������";
            this.mMenuWinCorScheme.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinMest
            // 
            this.mMenuWinMest.Name = "mMenuWinMest";
            this.mMenuWinMest.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinMest.Text = "�������������";
            this.mMenuWinMest.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinMer
            // 
            this.mMenuWinMer.Name = "mMenuWinMer";
            this.mMenuWinMer.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinMer.Text = "���";
            this.mMenuWinMer.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinDiscussion
            // 
            this.mMenuWinDiscussion.Name = "mMenuWinDiscussion";
            this.mMenuWinDiscussion.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinDiscussion.Text = "����������";
            this.mMenuWinDiscussion.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinPlane
            // 
            this.mMenuWinPlane.Name = "mMenuWinPlane";
            this.mMenuWinPlane.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinPlane.Text = "�������";
            this.mMenuWinPlane.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinWorkFolder
            // 
            this.mMenuWinWorkFolder.Name = "mMenuWinWorkFolder";
            this.mMenuWinWorkFolder.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinWorkFolder.Text = "������� �����";
            this.mMenuWinWorkFolder.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinProperties
            // 
            this.mMenuWinProperties.Name = "mMenuWinProperties";
            this.mMenuWinProperties.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinProperties.Text = "��������";
            this.mMenuWinProperties.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinLog
            // 
            this.mMenuWinLog.Name = "mMenuWinLog";
            this.mMenuWinLog.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinLog.Text = "���������";
            this.mMenuWinLog.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinWellList
            // 
            this.mMenuWinWellList.Name = "mMenuWinWellList";
            this.mMenuWinWellList.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinWellList.Text = "������ �������";
            this.mMenuWinWellList.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinFileServer
            // 
            this.mMenuWinFileServer.Name = "mMenuWinFileServer";
            this.mMenuWinFileServer.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinFileServer.Text = "�����";
            this.mMenuWinFileServer.Visible = false;
            this.mMenuWinFileServer.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinFilter
            // 
            this.mMenuWinFilter.Name = "mMenuWinFilter";
            this.mMenuWinFilter.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinFilter.Text = "������ ��������";
            this.mMenuWinFilter.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinChess
            // 
            this.mMenuWinChess.Name = "mMenuWinChess";
            this.mMenuWinChess.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinChess.Text = "��������";
            this.mMenuWinChess.Click += new System.EventHandler(this.WindowClick);
            // 
            // mMenuWinOilFieldAreas
            // 
            this.mMenuWinOilFieldAreas.Name = "mMenuWinOilFieldAreas";
            this.mMenuWinOilFieldAreas.Size = new System.Drawing.Size(242, 22);
            this.mMenuWinOilFieldAreas.Text = "������ ����������";
            this.mMenuWinOilFieldAreas.Click += new System.EventHandler(this.WindowClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(239, 6);
            // 
            // mMenuResetWindows
            // 
            this.mMenuResetWindows.Name = "mMenuResetWindows";
            this.mMenuResetWindows.Size = new System.Drawing.Size(242, 22);
            this.mMenuResetWindows.Text = "�������� ������������ ����";
            this.mMenuResetWindows.Click += new System.EventHandler(this.mMenuResetWindows_Click);
            // 
            // mMenuHelp
            // 
            this.mMenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMenuAbout,
            this.mMenuCheckUpdates,
            this.mMenuWatsNew,
            this.mMenuSendSupport});
            this.mMenuHelp.Name = "mMenuHelp";
            this.mMenuHelp.Size = new System.Drawing.Size(65, 20);
            this.mMenuHelp.Text = "�������";
            // 
            // mMenuAbout
            // 
            this.mMenuAbout.Name = "mMenuAbout";
            this.mMenuAbout.Size = new System.Drawing.Size(208, 22);
            this.mMenuAbout.Text = "� ���������...";
            this.mMenuAbout.Click += new System.EventHandler(this.mMenuAbout_Click);
            // 
            // mMenuCheckUpdates
            // 
            this.mMenuCheckUpdates.Name = "mMenuCheckUpdates";
            this.mMenuCheckUpdates.Size = new System.Drawing.Size(208, 22);
            this.mMenuCheckUpdates.Text = "��������� ����������";
            this.mMenuCheckUpdates.Click += new System.EventHandler(this.mMenuCheckUpdates_Click);
            // 
            // mMenuWatsNew
            // 
            this.mMenuWatsNew.Name = "mMenuWatsNew";
            this.mMenuWatsNew.Size = new System.Drawing.Size(208, 22);
            this.mMenuWatsNew.Text = "������ ���������";
            this.mMenuWatsNew.Click += new System.EventHandler(this.mMenuWatsNew_Click);
            // 
            // mMenuSendSupport
            // 
            this.mMenuSendSupport.Image = global::SmartPlus.Properties.Resources.Send_Email16;
            this.mMenuSendSupport.Name = "mMenuSendSupport";
            this.mMenuSendSupport.Size = new System.Drawing.Size(208, 22);
            this.mMenuSendSupport.Text = "������ �������������";
            this.mMenuSendSupport.Click += new System.EventHandler(this.mMenuSendSupport_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(60, 20);
            this.toolStripMenuItem1.Text = "�����: ";
            // 
            // stStrip
            // 
            this.stStrip.AutoSize = false;
            this.stStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbUpdates,
            this.stBar1,
            this.stBar2,
            this.pb,
            this.tsbCancel});
            this.stStrip.Location = new System.Drawing.Point(0, 714);
            this.stStrip.Name = "stStrip";
            this.stStrip.Size = new System.Drawing.Size(1354, 22);
            this.stStrip.TabIndex = 14;
            this.stStrip.Text = "statusStrip1";
            // 
            // tsbUpdates
            // 
            this.tsbUpdates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbUpdates.Image = global::SmartPlus.Properties.Resources.refresh16;
            this.tsbUpdates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbUpdates.Name = "tsbUpdates";
            this.tsbUpdates.ShowDropDownArrow = false;
            this.tsbUpdates.Size = new System.Drawing.Size(20, 20);
            this.tsbUpdates.Text = "Upd";
            this.tsbUpdates.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.tsbUpdates.ToolTipText = "�������� ������ ��� ���������� �� �������";
            this.tsbUpdates.Click += new System.EventHandler(this.tsbUpdates_Click);
            // 
            // stBar1
            // 
            this.stBar1.AutoToolTip = true;
            this.stBar1.Name = "stBar1";
            this.stBar1.Size = new System.Drawing.Size(1319, 17);
            this.stBar1.Spring = true;
            this.stBar1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stBar2
            // 
            this.stBar2.AutoSize = false;
            this.stBar2.AutoToolTip = true;
            this.stBar2.Name = "stBar2";
            this.stBar2.Size = new System.Drawing.Size(500, 17);
            this.stBar2.Visible = false;
            // 
            // pb
            // 
            this.pb.ForeColor = System.Drawing.Color.LimeGreen;
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(150, 16);
            this.pb.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pb.Visible = false;
            this.pb.Click += new System.EventHandler(this.pb_Click);
            // 
            // _fMainUnpinnedTabAreaLeft
            // 
            this._fMainUnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this._fMainUnpinnedTabAreaLeft.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._fMainUnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 24);
            this._fMainUnpinnedTabAreaLeft.Name = "_fMainUnpinnedTabAreaLeft";
            this._fMainUnpinnedTabAreaLeft.Owner = this.uDockManager;
            this._fMainUnpinnedTabAreaLeft.Size = new System.Drawing.Size(0, 690);
            this._fMainUnpinnedTabAreaLeft.TabIndex = 23;
            // 
            // _fMainUnpinnedTabAreaRight
            // 
            this._fMainUnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right;
            this._fMainUnpinnedTabAreaRight.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._fMainUnpinnedTabAreaRight.Location = new System.Drawing.Point(1354, 24);
            this._fMainUnpinnedTabAreaRight.Name = "_fMainUnpinnedTabAreaRight";
            this._fMainUnpinnedTabAreaRight.Owner = this.uDockManager;
            this._fMainUnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 690);
            this._fMainUnpinnedTabAreaRight.TabIndex = 24;
            // 
            // _fMainUnpinnedTabAreaTop
            // 
            this._fMainUnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top;
            this._fMainUnpinnedTabAreaTop.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._fMainUnpinnedTabAreaTop.Location = new System.Drawing.Point(0, 24);
            this._fMainUnpinnedTabAreaTop.Name = "_fMainUnpinnedTabAreaTop";
            this._fMainUnpinnedTabAreaTop.Owner = this.uDockManager;
            this._fMainUnpinnedTabAreaTop.Size = new System.Drawing.Size(1354, 0);
            this._fMainUnpinnedTabAreaTop.TabIndex = 25;
            // 
            // _fMainUnpinnedTabAreaBottom
            // 
            this._fMainUnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._fMainUnpinnedTabAreaBottom.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._fMainUnpinnedTabAreaBottom.Location = new System.Drawing.Point(0, 714);
            this._fMainUnpinnedTabAreaBottom.Name = "_fMainUnpinnedTabAreaBottom";
            this._fMainUnpinnedTabAreaBottom.Owner = this.uDockManager;
            this._fMainUnpinnedTabAreaBottom.Size = new System.Drawing.Size(1354, 0);
            this._fMainUnpinnedTabAreaBottom.TabIndex = 26;
            // 
            // _fMainAutoHideControl
            // 
            this._fMainAutoHideControl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._fMainAutoHideControl.Location = new System.Drawing.Point(1053, 24);
            this._fMainAutoHideControl.Name = "_fMainAutoHideControl";
            this._fMainAutoHideControl.Owner = this.uDockManager;
            this._fMainAutoHideControl.Size = new System.Drawing.Size(100, 696);
            this._fMainAutoHideControl.TabIndex = 27;
            // 
            // dockableWindow3
            // 
            this.dockableWindow3.Controls.Add(this.pInspector);
            this.dockableWindow3.Location = new System.Drawing.Point(0, 0);
            this.dockableWindow3.Name = "dockableWindow3";
            this.dockableWindow3.Owner = this.uDockManager;
            this.dockableWindow3.Size = new System.Drawing.Size(251, 206);
            this.dockableWindow3.TabIndex = 57;
            // 
            // dockableWindow6
            // 
            this.dockableWindow6.Controls.Add(this.pActiveOilField);
            this.dockableWindow6.Location = new System.Drawing.Point(-10000, 0);
            this.dockableWindow6.Name = "dockableWindow6";
            this.dockableWindow6.Owner = this.uDockManager;
            this.dockableWindow6.Size = new System.Drawing.Size(251, 208);
            this.dockableWindow6.TabIndex = 58;
            // 
            // dockableWindow8
            // 
            this.dockableWindow8.Controls.Add(this.pFileServer);
            this.dockableWindow8.Location = new System.Drawing.Point(-10000, 0);
            this.dockableWindow8.Name = "dockableWindow8";
            this.dockableWindow8.Owner = this.uDockManager;
            this.dockableWindow8.Size = new System.Drawing.Size(251, 208);
            this.dockableWindow8.TabIndex = 59;
            // 
            // dockableWindow9
            // 
            this.dockableWindow9.Controls.Add(this.pWork);
            this.dockableWindow9.Location = new System.Drawing.Point(0, 231);
            this.dockableWindow9.Name = "dockableWindow9";
            this.dockableWindow9.Owner = this.uDockManager;
            this.dockableWindow9.Size = new System.Drawing.Size(251, 200);
            this.dockableWindow9.TabIndex = 60;
            // 
            // dockableWindow5
            // 
            this.dockableWindow5.Controls.Add(this.pFilterOilObj);
            this.dockableWindow5.Location = new System.Drawing.Point(0, 434);
            this.dockableWindow5.Name = "dockableWindow5";
            this.dockableWindow5.Owner = this.uDockManager;
            this.dockableWindow5.Size = new System.Drawing.Size(251, 256);
            this.dockableWindow5.TabIndex = 61;
            // 
            // dockableWindow2
            // 
            this.dockableWindow2.Controls.Add(this.tsPlaneWellContainer);
            this.dockableWindow2.Location = new System.Drawing.Point(3, 0);
            this.dockableWindow2.Name = "dockableWindow2";
            this.dockableWindow2.Owner = this.uDockManager;
            this.dockableWindow2.Size = new System.Drawing.Size(233, 690);
            this.dockableWindow2.TabIndex = 62;
            // 
            // dockableWindow4
            // 
            this.dockableWindow4.Controls.Add(this.pGraphics);
            this.dockableWindow4.Location = new System.Drawing.Point(0, 3);
            this.dockableWindow4.Name = "dockableWindow4";
            this.dockableWindow4.Owner = this.uDockManager;
            this.dockableWindow4.Size = new System.Drawing.Size(864, 231);
            this.dockableWindow4.TabIndex = 63;
            // 
            // dockableWindow1
            // 
            this.dockableWindow1.Controls.Add(this.tsCorSchContainer);
            this.dockableWindow1.Location = new System.Drawing.Point(-10000, 3);
            this.dockableWindow1.Name = "dockableWindow1";
            this.dockableWindow1.Owner = this.uDockManager;
            this.dockableWindow1.Size = new System.Drawing.Size(817, 246);
            this.dockableWindow1.TabIndex = 64;
            // 
            // dockableWindow15
            // 
            this.dockableWindow15.Controls.Add(this.pGisView);
            this.dockableWindow15.Location = new System.Drawing.Point(-10000, 3);
            this.dockableWindow15.Name = "dockableWindow15";
            this.dockableWindow15.Owner = this.uDockManager;
            this.dockableWindow15.Size = new System.Drawing.Size(899, 322);
            this.dockableWindow15.TabIndex = 65;
            // 
            // dockableWindow10
            // 
            this.dockableWindow10.Controls.Add(this.pMerView);
            this.dockableWindow10.Location = new System.Drawing.Point(-10000, 3);
            this.dockableWindow10.Name = "dockableWindow10";
            this.dockableWindow10.Owner = this.uDockManager;
            this.dockableWindow10.Size = new System.Drawing.Size(899, 322);
            this.dockableWindow10.TabIndex = 66;
            // 
            // dockableWindow14
            // 
            this.dockableWindow14.Controls.Add(this.pChessView);
            this.dockableWindow14.Location = new System.Drawing.Point(-10000, 3);
            this.dockableWindow14.Name = "dockableWindow14";
            this.dockableWindow14.Owner = this.uDockManager;
            this.dockableWindow14.Size = new System.Drawing.Size(899, 322);
            this.dockableWindow14.TabIndex = 67;
            // 
            // dockableWindow12
            // 
            this.dockableWindow12.Controls.Add(this.pDiscussion);
            this.dockableWindow12.Location = new System.Drawing.Point(-10000, 3);
            this.dockableWindow12.Name = "dockableWindow12";
            this.dockableWindow12.Owner = this.uDockManager;
            this.dockableWindow12.Size = new System.Drawing.Size(876, 231);
            this.dockableWindow12.TabIndex = 68;
            // 
            // dockableWindow7
            // 
            this.dockableWindow7.Controls.Add(this.pAreasView);
            this.dockableWindow7.Location = new System.Drawing.Point(-10000, 3);
            this.dockableWindow7.Name = "dockableWindow7";
            this.dockableWindow7.Owner = this.uDockManager;
            this.dockableWindow7.Size = new System.Drawing.Size(876, 231);
            this.dockableWindow7.TabIndex = 69;
            // 
            // dockableWindow11
            // 
            this.dockableWindow11.Controls.Add(this.pCore);
            this.dockableWindow11.Location = new System.Drawing.Point(-10000, 0);
            this.dockableWindow11.Name = "dockableWindow11";
            this.dockableWindow11.Owner = this.uDockManager;
            this.dockableWindow11.Size = new System.Drawing.Size(233, 696);
            this.dockableWindow11.TabIndex = 70;
            // 
            // dockableWindow13
            // 
            this.dockableWindow13.Controls.Add(this.pCoreTest);
            this.dockableWindow13.Location = new System.Drawing.Point(-10000, 438);
            this.dockableWindow13.Name = "dockableWindow13";
            this.dockableWindow13.Owner = this.uDockManager;
            this.dockableWindow13.Size = new System.Drawing.Size(251, 258);
            this.dockableWindow13.TabIndex = 71;
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "documents.png");
            this.imgList.Images.SetKeyName(1, "edit16.png");
            this.imgList.Images.SetKeyName(2, "well_mouth.png");
            this.imgList.Images.SetKeyName(3, "large icons.png");
            this.imgList.Images.SetKeyName(4, "Folder.png");
            this.imgList.Images.SetKeyName(5, "apply.png");
            // 
            // tbCanvas
            // 
            this.tbCanvas.Dock = System.Windows.Forms.DockStyle.None;
            this.tbCanvas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAddContour,
            this.tsbAddProfileByLine,
            this.tsbAddProfileByWell,
            this.toolStripSeparator2,
            this.tsbLoadWellsTrajectory,
            this.tsbCalcBubble,
            this.tsbCalcAccumBubble,
            this.tsbCalcHistoryBubble,
            this.tsbBubbleSettings,
            this.toolStripSeparator8,
            this.tsbVisibleAreas,
            this.tsbVisibleWellPad,
            this.tsbContoursColor,
            this.tsbGrayIcons,
            this.tsbMapSnapShoot,
            this.tsbShowBizPlanOnMap,
            this.tsbCreateVoronoiMap,
            this.tsbShowDiscussLetterBox});
            this.tbCanvas.Location = new System.Drawing.Point(0, 3);
            this.tbCanvas.Name = "tbCanvas";
            this.tbCanvas.Size = new System.Drawing.Size(32, 391);
            this.tbCanvas.TabIndex = 29;
            this.tbCanvas.Text = "toolStrip1";
            // 
            // tsbAddContour
            // 
            this.tsbAddContour.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddContour.Image = global::SmartPlus.Properties.Resources.AddContour;
            this.tsbAddContour.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddContour.Name = "tsbAddContour";
            this.tsbAddContour.Size = new System.Drawing.Size(30, 20);
            this.tsbAddContour.Text = "�������� ������";
            this.tsbAddContour.Click += new System.EventHandler(this.tsbAddContour_Click);
            // 
            // tsbAddProfileByLine
            // 
            this.tsbAddProfileByLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddProfileByLine.Image = global::SmartPlus.Properties.Resources.SetProfile;
            this.tsbAddProfileByLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddProfileByLine.Name = "tsbAddProfileByLine";
            this.tsbAddProfileByLine.Size = new System.Drawing.Size(30, 20);
            this.tsbAddProfileByLine.Text = "������ ������� �� �������";
            this.tsbAddProfileByLine.Click += new System.EventHandler(this.tsbAddProfileByLine_Click);
            // 
            // tsbAddProfileByWell
            // 
            this.tsbAddProfileByWell.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddProfileByWell.Image = global::SmartPlus.Properties.Resources.SetProfileByWell;
            this.tsbAddProfileByWell.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddProfileByWell.Name = "tsbAddProfileByWell";
            this.tsbAddProfileByWell.Size = new System.Drawing.Size(30, 20);
            this.tsbAddProfileByWell.Text = "������ ������� �� ��������� ��������� � ������ �������";
            this.tsbAddProfileByWell.Click += new System.EventHandler(this.tsbAddProfileByWell_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(30, 6);
            // 
            // tsbLoadWellsTrajectory
            // 
            this.tsbLoadWellsTrajectory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbLoadWellsTrajectory.Image = global::SmartPlus.Properties.Resources.trajectory;
            this.tsbLoadWellsTrajectory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLoadWellsTrajectory.Name = "tsbLoadWellsTrajectory";
            this.tsbLoadWellsTrajectory.Size = new System.Drawing.Size(30, 20);
            this.tsbLoadWellsTrajectory.Text = "��������/������ ���������� �������";
            this.tsbLoadWellsTrajectory.Click += new System.EventHandler(this.tsbLoadWellsTrajectory_Click);
            // 
            // tsbCalcBubble
            // 
            this.tsbCalcBubble.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCalcBubble.Image = global::SmartPlus.Properties.Resources.bubble2;
            this.tsbCalcBubble.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCalcBubble.Name = "tsbCalcBubble";
            this.tsbCalcBubble.Size = new System.Drawing.Size(30, 20);
            this.tsbCalcBubble.Text = "����� ������� �������";
            this.tsbCalcBubble.Click += new System.EventHandler(this.tsbCalcBubble_Click);
            // 
            // tsbCalcAccumBubble
            // 
            this.tsbCalcAccumBubble.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCalcAccumBubble.Image = global::SmartPlus.Properties.Resources.multi_bubble2;
            this.tsbCalcAccumBubble.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCalcAccumBubble.Name = "tsbCalcAccumBubble";
            this.tsbCalcAccumBubble.Size = new System.Drawing.Size(30, 20);
            this.tsbCalcAccumBubble.Text = "����� ����������� �������";
            this.tsbCalcAccumBubble.Click += new System.EventHandler(this.tsbCalcAccumBubble_Click);
            // 
            // tsbCalcHistoryBubble
            // 
            this.tsbCalcHistoryBubble.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCalcHistoryBubble.Image = global::SmartPlus.Properties.Resources.bubble_hystory2;
            this.tsbCalcHistoryBubble.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCalcHistoryBubble.Name = "tsbCalcHistoryBubble";
            this.tsbCalcHistoryBubble.Size = new System.Drawing.Size(30, 20);
            this.tsbCalcHistoryBubble.Text = "����� ��������� �������";
            this.tsbCalcHistoryBubble.Click += new System.EventHandler(this.tsbCalcHistoryBubble_Click);
            // 
            // tsbBubbleSettings
            // 
            this.tsbBubbleSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBubbleSettings.Image = global::SmartPlus.Properties.Resources.Settings16;
            this.tsbBubbleSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBubbleSettings.Name = "tsbBubbleSettings";
            this.tsbBubbleSettings.Size = new System.Drawing.Size(30, 20);
            this.tsbBubbleSettings.Text = "��������� ���� �������";
            this.tsbBubbleSettings.Click += new System.EventHandler(this.tsbBubbleSettings_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(30, 6);
            // 
            // tsbVisibleAreas
            // 
            this.tsbVisibleAreas.CheckOnClick = true;
            this.tsbVisibleAreas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVisibleAreas.Image = global::SmartPlus.Properties.Resources.Areas;
            this.tsbVisibleAreas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVisibleAreas.Name = "tsbVisibleAreas";
            this.tsbVisibleAreas.Size = new System.Drawing.Size(30, 20);
            this.tsbVisibleAreas.Text = "���/���� ����� ����������";
            this.tsbVisibleAreas.Click += new System.EventHandler(this.tsbVisibleAreas_Click);
            // 
            // tsbVisibleWellPad
            // 
            this.tsbVisibleWellPad.CheckOnClick = true;
            this.tsbVisibleWellPad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVisibleWellPad.Image = global::SmartPlus.Properties.Resources.well_pad;
            this.tsbVisibleWellPad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVisibleWellPad.Name = "tsbVisibleWellPad";
            this.tsbVisibleWellPad.Size = new System.Drawing.Size(30, 20);
            this.tsbVisibleWellPad.Text = "��������/������ ����� �������";
            this.tsbVisibleWellPad.Click += new System.EventHandler(this.tsbVisibleWellPad_Click);
            // 
            // tsbContoursColor
            // 
            this.tsbContoursColor.Checked = true;
            this.tsbContoursColor.CheckOnClick = true;
            this.tsbContoursColor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbContoursColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbContoursColor.Image = global::SmartPlus.Properties.Resources.CntrColor;
            this.tsbContoursColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbContoursColor.Name = "tsbContoursColor";
            this.tsbContoursColor.Size = new System.Drawing.Size(30, 20);
            this.tsbContoursColor.Text = "����� �������� �� �����/�� ��������";
            this.tsbContoursColor.Click += new System.EventHandler(this.tsbContoursColor_Click);
            // 
            // tsbGrayIcons
            // 
            this.tsbGrayIcons.Checked = true;
            this.tsbGrayIcons.CheckOnClick = true;
            this.tsbGrayIcons.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbGrayIcons.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGrayIcons.Image = global::SmartPlus.Properties.Resources.canv_GrayIcons;
            this.tsbGrayIcons.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGrayIcons.Name = "tsbGrayIcons";
            this.tsbGrayIcons.Size = new System.Drawing.Size(30, 20);
            this.tsbGrayIcons.Text = "���/���� ����� ����� ������� �������";
            this.tsbGrayIcons.Click += new System.EventHandler(this.tsbGrayIcons_Click);
            // 
            // tsbMapSnapShoot
            // 
            this.tsbMapSnapShoot.AutoSize = false;
            this.tsbMapSnapShoot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMapSnapShoot.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbPartMapSnapShot,
            this.tsbAllMapSnapShoot});
            this.tsbMapSnapShoot.Image = ((System.Drawing.Image)(resources.GetObject("tsbMapSnapShoot.Image")));
            this.tsbMapSnapShoot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMapSnapShoot.Name = "tsbMapSnapShoot";
            this.tsbMapSnapShoot.Size = new System.Drawing.Size(31, 20);
            this.tsbMapSnapShoot.Text = "toolStripSplitButton1";
            // 
            // tsbPartMapSnapShot
            // 
            this.tsbPartMapSnapShot.Image = global::SmartPlus.Properties.Resources.SnapShootCross;
            this.tsbPartMapSnapShot.Name = "tsbPartMapSnapShot";
            this.tsbPartMapSnapShot.Size = new System.Drawing.Size(320, 22);
            this.tsbPartMapSnapShot.Text = "����������� ������� ����� � ����� ������";
            this.tsbPartMapSnapShot.Click += new System.EventHandler(this.tsbPartMapSnapShoot_Click);
            // 
            // tsbAllMapSnapShoot
            // 
            this.tsbAllMapSnapShoot.Image = global::SmartPlus.Properties.Resources.SnapShoot;
            this.tsbAllMapSnapShoot.Name = "tsbAllMapSnapShoot";
            this.tsbAllMapSnapShoot.Size = new System.Drawing.Size(320, 22);
            this.tsbAllMapSnapShoot.Text = "����������� ���� ����� � ����� ������";
            this.tsbAllMapSnapShoot.Click += new System.EventHandler(this.tsbAllMapSnapShot_Click);
            // 
            // tsbShowBizPlanOnMap
            // 
            this.tsbShowBizPlanOnMap.CheckOnClick = true;
            this.tsbShowBizPlanOnMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowBizPlanOnMap.Image = global::SmartPlus.Properties.Resources.BPParamsMap;
            this.tsbShowBizPlanOnMap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowBizPlanOnMap.Name = "tsbShowBizPlanOnMap";
            this.tsbShowBizPlanOnMap.Size = new System.Drawing.Size(30, 20);
            this.tsbShowBizPlanOnMap.Text = "��������/������ ���������� ������-�����";
            this.tsbShowBizPlanOnMap.Click += new System.EventHandler(this.tsbShowBizPlan_Click);
            // 
            // tsbCreateVoronoiMap
            // 
            this.tsbCreateVoronoiMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCreateVoronoiMap.Image = global::SmartPlus.Properties.Resources.CreateVoronoiMap;
            this.tsbCreateVoronoiMap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCreateVoronoiMap.Name = "tsbCreateVoronoiMap";
            this.tsbCreateVoronoiMap.Size = new System.Drawing.Size(30, 20);
            this.tsbCreateVoronoiMap.Text = "��������� ����� �������� �������� �� �������";
            this.tsbCreateVoronoiMap.Click += new System.EventHandler(this.tsbCreateVoronoiMap_Click);
            // 
            // tsbShowDiscussLetterBox
            // 
            this.tsbShowDiscussLetterBox.CheckOnClick = true;
            this.tsbShowDiscussLetterBox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbShowDiscussLetterBox.Image = global::SmartPlus.Properties.Resources.Send_Email16;
            this.tsbShowDiscussLetterBox.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowDiscussLetterBox.Name = "tsbShowDiscussLetterBox";
            this.tsbShowDiscussLetterBox.Size = new System.Drawing.Size(30, 20);
            this.tsbShowDiscussLetterBox.Text = "��������/������ ���������� ���� ����������";
            this.tsbShowDiscussLetterBox.Click += new System.EventHandler(this.tsbShowDiscussLetterBox_Click);
            // 
            // tsCanvContainer
            // 
            // 
            // tsCanvContainer.ContentPanel
            // 
            this.tsCanvContainer.ContentPanel.Size = new System.Drawing.Size(832, 409);
            this.tsCanvContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // tsCanvContainer.LeftToolStripPanel
            // 
            this.tsCanvContainer.LeftToolStripPanel.Controls.Add(this.tbCanvas);
            this.tsCanvContainer.Location = new System.Drawing.Point(254, 24);
            this.tsCanvContainer.Name = "tsCanvContainer";
            this.tsCanvContainer.Size = new System.Drawing.Size(864, 434);
            this.tsCanvContainer.TabIndex = 30;
            this.tsCanvContainer.Text = "toolStripContainer1";
            // 
            // windowDockingArea6
            // 
            this.windowDockingArea6.Controls.Add(this.dockableWindow3);
            this.windowDockingArea6.Controls.Add(this.dockableWindow6);
            this.windowDockingArea6.Controls.Add(this.dockableWindow8);
            this.windowDockingArea6.Controls.Add(this.dockableWindow9);
            this.windowDockingArea6.Controls.Add(this.dockableWindow5);
            this.windowDockingArea6.Dock = System.Windows.Forms.DockStyle.Left;
            this.windowDockingArea6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea6.Location = new System.Drawing.Point(0, 24);
            this.windowDockingArea6.Name = "windowDockingArea6";
            this.windowDockingArea6.Owner = this.uDockManager;
            this.windowDockingArea6.Size = new System.Drawing.Size(254, 690);
            this.windowDockingArea6.TabIndex = 0;
            // 
            // dockableWindow18
            // 
            this.dockableWindow18.Controls.Add(this.pProperties);
            this.dockableWindow18.Location = new System.Drawing.Point(0, 0);
            this.dockableWindow18.Name = "dockableWindow18";
            this.dockableWindow18.Owner = this.uDockManager;
            this.dockableWindow18.Size = new System.Drawing.Size(251, 208);
            this.dockableWindow18.TabIndex = 72;
            // 
            // dockableWindow17
            // 
            this.dockableWindow17.Controls.Add(this.pReLog);
            this.dockableWindow17.Location = new System.Drawing.Point(-10000, 0);
            this.dockableWindow17.Name = "dockableWindow17";
            this.dockableWindow17.Owner = this.uDockManager;
            this.dockableWindow17.Size = new System.Drawing.Size(251, 208);
            this.dockableWindow17.TabIndex = 73;
            // 
            // dockableWindow16
            // 
            this.dockableWindow16.Controls.Add(this.pWellListSettings);
            this.dockableWindow16.Location = new System.Drawing.Point(-10000, 0);
            this.dockableWindow16.Name = "dockableWindow16";
            this.dockableWindow16.Owner = this.uDockManager;
            this.dockableWindow16.Size = new System.Drawing.Size(251, 208);
            this.dockableWindow16.TabIndex = 74;
            // 
            // dockableWindow19
            // 
            this.dockableWindow19.Controls.Add(this.pReportBuilder);
            this.dockableWindow19.Location = new System.Drawing.Point(0, 233);
            this.dockableWindow19.Name = "dockableWindow19";
            this.dockableWindow19.Owner = this.uDockManager;
            this.dockableWindow19.Size = new System.Drawing.Size(251, 202);
            this.dockableWindow19.TabIndex = 75;
            // 
            // windowDockingArea14
            // 
            this.windowDockingArea14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea14.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea14.Name = "windowDockingArea14";
            this.windowDockingArea14.Owner = this.uDockManager;
            this.windowDockingArea14.Size = new System.Drawing.Size(164, 670);
            this.windowDockingArea14.TabIndex = 0;
            // 
            // windowDockingArea13
            // 
            this.windowDockingArea13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea13.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea13.Name = "windowDockingArea13";
            this.windowDockingArea13.Owner = this.uDockManager;
            this.windowDockingArea13.Size = new System.Drawing.Size(208, 266);
            this.windowDockingArea13.TabIndex = 0;
            // 
            // windowDockingArea17
            // 
            this.windowDockingArea17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea17.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea17.Name = "windowDockingArea17";
            this.windowDockingArea17.Owner = this.uDockManager;
            this.windowDockingArea17.Size = new System.Drawing.Size(98, 471);
            this.windowDockingArea17.TabIndex = 0;
            // 
            // windowDockingArea24
            // 
            this.windowDockingArea24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea24.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea24.Name = "windowDockingArea24";
            this.windowDockingArea24.Owner = this.uDockManager;
            this.windowDockingArea24.Size = new System.Drawing.Size(98, 415);
            this.windowDockingArea24.TabIndex = 0;
            // 
            // windowDockingArea25
            // 
            this.windowDockingArea25.Controls.Add(this.dockableWindow2);
            this.windowDockingArea25.Dock = System.Windows.Forms.DockStyle.Right;
            this.windowDockingArea25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea25.Location = new System.Drawing.Point(1118, 24);
            this.windowDockingArea25.Name = "windowDockingArea25";
            this.windowDockingArea25.Owner = this.uDockManager;
            this.windowDockingArea25.Size = new System.Drawing.Size(236, 690);
            this.windowDockingArea25.TabIndex = 50;
            // 
            // windowDockingArea4
            // 
            this.windowDockingArea4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea4.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea4.Name = "windowDockingArea4";
            this.windowDockingArea4.Owner = this.uDockManager;
            this.windowDockingArea4.Size = new System.Drawing.Size(741, 360);
            this.windowDockingArea4.TabIndex = 0;
            // 
            // windowDockingArea12
            // 
            this.windowDockingArea12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea12.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea12.Name = "windowDockingArea12";
            this.windowDockingArea12.Owner = this.uDockManager;
            this.windowDockingArea12.Size = new System.Drawing.Size(130, 320);
            this.windowDockingArea12.TabIndex = 0;
            // 
            // windowDockingArea18
            // 
            this.windowDockingArea18.Controls.Add(this.dockableWindow7);
            this.windowDockingArea18.Controls.Add(this.dockableWindow12);
            this.windowDockingArea18.Controls.Add(this.dockableWindow14);
            this.windowDockingArea18.Controls.Add(this.dockableWindow10);
            this.windowDockingArea18.Controls.Add(this.dockableWindow15);
            this.windowDockingArea18.Controls.Add(this.dockableWindow1);
            this.windowDockingArea18.Controls.Add(this.dockableWindow4);
            this.windowDockingArea18.Controls.Add(this.dockableWindow11);
            this.windowDockingArea18.Controls.Add(this.dockableWindow13);
            this.windowDockingArea18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.windowDockingArea18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea18.Location = new System.Drawing.Point(254, 458);
            this.windowDockingArea18.Name = "windowDockingArea18";
            this.windowDockingArea18.Owner = this.uDockManager;
            this.windowDockingArea18.Size = new System.Drawing.Size(864, 256);
            this.windowDockingArea18.TabIndex = 0;
            // 
            // windowDockingArea19
            // 
            this.windowDockingArea19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea19.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea19.Name = "windowDockingArea19";
            this.windowDockingArea19.Owner = this.uDockManager;
            this.windowDockingArea19.Size = new System.Drawing.Size(277, 696);
            this.windowDockingArea19.TabIndex = 0;
            // 
            // windowDockingArea3
            // 
            this.windowDockingArea3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea3.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea3.Name = "windowDockingArea3";
            this.windowDockingArea3.Owner = this.uDockManager;
            this.windowDockingArea3.Size = new System.Drawing.Size(98, 471);
            this.windowDockingArea3.TabIndex = 0;
            // 
            // windowDockingArea8
            // 
            this.windowDockingArea8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea8.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea8.Name = "windowDockingArea8";
            this.windowDockingArea8.Owner = this.uDockManager;
            this.windowDockingArea8.Size = new System.Drawing.Size(98, 415);
            this.windowDockingArea8.TabIndex = 0;
            // 
            // windowDockingArea5
            // 
            this.windowDockingArea5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea5.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea5.Name = "windowDockingArea5";
            this.windowDockingArea5.Owner = this.uDockManager;
            this.windowDockingArea5.Size = new System.Drawing.Size(494, 87);
            this.windowDockingArea5.TabIndex = 0;
            // 
            // windowDockingArea1
            // 
            this.windowDockingArea1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea1.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea1.Name = "windowDockingArea1";
            this.windowDockingArea1.Owner = this.uDockManager;
            this.windowDockingArea1.Size = new System.Drawing.Size(954, 505);
            this.windowDockingArea1.TabIndex = 0;
            // 
            // windowDockingArea9
            // 
            this.windowDockingArea9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea9.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea9.Name = "windowDockingArea9";
            this.windowDockingArea9.Owner = this.uDockManager;
            this.windowDockingArea9.Size = new System.Drawing.Size(189, 674);
            this.windowDockingArea9.TabIndex = 0;
            // 
            // windowDockingArea7
            // 
            this.windowDockingArea7.Controls.Add(this.dockableWindow18);
            this.windowDockingArea7.Controls.Add(this.dockableWindow17);
            this.windowDockingArea7.Dock = System.Windows.Forms.DockStyle.Right;
            this.windowDockingArea7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea7.Location = new System.Drawing.Point(913, 25);
            this.windowDockingArea7.Name = "windowDockingArea7";
            this.windowDockingArea7.Owner = this.uDockManager;
            this.windowDockingArea7.Size = new System.Drawing.Size(197, 348);
            this.windowDockingArea7.TabIndex = 53;
            // 
            // windowDockingArea2
            // 
            this.windowDockingArea2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea2.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea2.Name = "windowDockingArea2";
            this.windowDockingArea2.Owner = this.uDockManager;
            this.windowDockingArea2.Size = new System.Drawing.Size(98, 346);
            this.windowDockingArea2.TabIndex = 0;
            // 
            // windowDockingArea10
            // 
            this.windowDockingArea10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea10.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea10.Name = "windowDockingArea10";
            this.windowDockingArea10.Owner = this.uDockManager;
            this.windowDockingArea10.Size = new System.Drawing.Size(98, 415);
            this.windowDockingArea10.TabIndex = 0;
            // 
            // windowDockingArea15
            // 
            this.windowDockingArea15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea15.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea15.Name = "windowDockingArea15";
            this.windowDockingArea15.Owner = this.uDockManager;
            this.windowDockingArea15.Size = new System.Drawing.Size(351, 696);
            this.windowDockingArea15.TabIndex = 0;
            // 
            // windowDockingArea11
            // 
            this.windowDockingArea11.Controls.Add(this.dockableWindow16);
            this.windowDockingArea11.Dock = System.Windows.Forms.DockStyle.Right;
            this.windowDockingArea11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea11.Location = new System.Drawing.Point(668, 24);
            this.windowDockingArea11.Name = "windowDockingArea11";
            this.windowDockingArea11.Owner = this.uDockManager;
            this.windowDockingArea11.Size = new System.Drawing.Size(405, 349);
            this.windowDockingArea11.TabIndex = 54;
            // 
            // windowDockingArea20
            // 
            this.windowDockingArea20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea20.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea20.Name = "windowDockingArea20";
            this.windowDockingArea20.Owner = this.uDockManager;
            this.windowDockingArea20.Size = new System.Drawing.Size(97, 348);
            this.windowDockingArea20.TabIndex = 0;
            // 
            // windowDockingArea21
            // 
            this.windowDockingArea21.Controls.Add(this.dockableWindow19);
            this.windowDockingArea21.Dock = System.Windows.Forms.DockStyle.Right;
            this.windowDockingArea21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea21.Location = new System.Drawing.Point(710, 24);
            this.windowDockingArea21.Name = "windowDockingArea21";
            this.windowDockingArea21.Owner = this.uDockManager;
            this.windowDockingArea21.Size = new System.Drawing.Size(284, 375);
            this.windowDockingArea21.TabIndex = 56;
            // 
            // windowDockingArea16
            // 
            this.windowDockingArea16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea16.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea16.Name = "windowDockingArea16";
            this.windowDockingArea16.Owner = this.uDockManager;
            this.windowDockingArea16.Size = new System.Drawing.Size(194, 348);
            this.windowDockingArea16.TabIndex = 0;
            // 
            // windowDockingArea23
            // 
            this.windowDockingArea23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea23.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea23.Name = "windowDockingArea23";
            this.windowDockingArea23.Owner = this.uDockManager;
            this.windowDockingArea23.Size = new System.Drawing.Size(97, 393);
            this.windowDockingArea23.TabIndex = 0;
            // 
            // windowDockingArea26
            // 
            this.windowDockingArea26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowDockingArea26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowDockingArea26.Location = new System.Drawing.Point(8, 8);
            this.windowDockingArea26.Name = "windowDockingArea26";
            this.windowDockingArea26.Owner = this.uDockManager;
            this.windowDockingArea26.Size = new System.Drawing.Size(97, 393);
            this.windowDockingArea26.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 736);
            this.Controls.Add(this._fMainAutoHideControl);
            this.Controls.Add(this.tsCanvContainer);
            this.Controls.Add(this.windowDockingArea7);
            this.Controls.Add(this.windowDockingArea11);
            this.Controls.Add(this.windowDockingArea21);
            this.Controls.Add(this.windowDockingArea18);
            this.Controls.Add(this.windowDockingArea25);
            this.Controls.Add(this.windowDockingArea6);
            this.Controls.Add(this._fMainUnpinnedTabAreaTop);
            this.Controls.Add(this._fMainUnpinnedTabAreaBottom);
            this.Controls.Add(this._fMainUnpinnedTabAreaLeft);
            this.Controls.Add(this._fMainUnpinnedTabAreaRight);
            this.Controls.Add(this.mMenu);
            this.Controls.Add(this.stStrip);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mMenu;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainForm";
            this.Text = "MiR";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fMain_FormClosing);
            this.pWork.ResumeLayout(false);
            this.pWork.PerformLayout();
            this.tbWork.ResumeLayout(false);
            this.tbWork.PerformLayout();
            this.tsPlaneWellContainer.TopToolStripPanel.ResumeLayout(false);
            this.tsPlaneWellContainer.TopToolStripPanel.PerformLayout();
            this.tsPlaneWellContainer.ResumeLayout(false);
            this.tsPlaneWellContainer.PerformLayout();
            this.tbPlane.ResumeLayout(false);
            this.tbPlane.PerformLayout();
            this.pGraphics.ResumeLayout(false);
            this.scGraphics.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scGraphics)).EndInit();
            this.scGraphics.ResumeLayout(false);
            this.tsGraphicsContainer.TopToolStripPanel.ResumeLayout(false);
            this.tsGraphicsContainer.TopToolStripPanel.PerformLayout();
            this.tsGraphicsContainer.ResumeLayout(false);
            this.tsGraphicsContainer.PerformLayout();
            this.tbGraphics.ResumeLayout(false);
            this.tbGraphics.PerformLayout();
            this.tsCorSchContainer.LeftToolStripPanel.ResumeLayout(false);
            this.tsCorSchContainer.LeftToolStripPanel.PerformLayout();
            this.tsCorSchContainer.ResumeLayout(false);
            this.tsCorSchContainer.PerformLayout();
            this.tbCorScheme.ResumeLayout(false);
            this.tbCorScheme.PerformLayout();
            this.pDiscussion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitDiscussion)).EndInit();
            this.SplitDiscussion.ResumeLayout(false);
            this.pAreasView.ResumeLayout(false);
            this.pAreasView.PerformLayout();
            this.tbAreas.ResumeLayout(false);
            this.tbAreas.PerformLayout();
            this.pProperties.ResumeLayout(false);
            this.pReLog.ResumeLayout(false);
            this.pWellListSettings.ResumeLayout(false);
            this.pWellListSettings.PerformLayout();
            this.tsMenuWellListSettings.ResumeLayout(false);
            this.tsMenuWellListSettings.PerformLayout();
            this.pReportBuilder.ResumeLayout(false);
            this.pReportBuilder.PerformLayout();
            this.tbReportBuilder.ResumeLayout(false);
            this.tbReportBuilder.PerformLayout();
            ((System.Configuration.IPersistComponentSettings)(this.uDockManager)).LoadComponentSettings();
            ((System.ComponentModel.ISupportInitialize)(this.uDockManager)).EndInit();
            this.mMenu.ResumeLayout(false);
            this.mMenu.PerformLayout();
            this.stStrip.ResumeLayout(false);
            this.stStrip.PerformLayout();
            this.dockableWindow3.ResumeLayout(false);
            this.dockableWindow6.ResumeLayout(false);
            this.dockableWindow8.ResumeLayout(false);
            this.dockableWindow9.ResumeLayout(false);
            this.dockableWindow5.ResumeLayout(false);
            this.dockableWindow2.ResumeLayout(false);
            this.dockableWindow4.ResumeLayout(false);
            this.dockableWindow1.ResumeLayout(false);
            this.dockableWindow15.ResumeLayout(false);
            this.dockableWindow10.ResumeLayout(false);
            this.dockableWindow14.ResumeLayout(false);
            this.dockableWindow12.ResumeLayout(false);
            this.dockableWindow7.ResumeLayout(false);
            this.dockableWindow11.ResumeLayout(false);
            this.dockableWindow13.ResumeLayout(false);
            this.tbCanvas.ResumeLayout(false);
            this.tbCanvas.PerformLayout();
            this.tsCanvContainer.LeftToolStripPanel.ResumeLayout(false);
            this.tsCanvContainer.LeftToolStripPanel.PerformLayout();
            this.tsCanvContainer.ResumeLayout(false);
            this.tsCanvContainer.PerformLayout();
            this.windowDockingArea6.ResumeLayout(false);
            this.dockableWindow18.ResumeLayout(false);
            this.dockableWindow17.ResumeLayout(false);
            this.dockableWindow16.ResumeLayout(false);
            this.dockableWindow19.ResumeLayout(false);
            this.windowDockingArea25.ResumeLayout(false);
            this.windowDockingArea18.ResumeLayout(false);
            this.windowDockingArea7.ResumeLayout(false);
            this.windowDockingArea11.ResumeLayout(false);
            this.windowDockingArea21.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mMenu;
        private System.Windows.Forms.ToolStripMenuItem mMenuFile;
        private System.Windows.Forms.ToolStripSeparator mMenuSeparator;
        private System.Windows.Forms.ToolStripMenuItem mMenuExit;
        private System.Windows.Forms.ToolStripMenuItem mMenuProj;
        private System.Windows.Forms.ToolStripMenuItem mMenuProjOpen;
        private System.Windows.Forms.ToolStripMenuItem mMenuClose;
        private System.Windows.Forms.Panel pProperties;
        public System.Windows.Forms.PropertyGrid pgProperties;
        private System.Windows.Forms.Panel pReLog;
        private System.Windows.Forms.ToolStripMenuItem mMenuOpenImage;
        private System.Windows.Forms.ToolStripMenuItem mMenuSevice;
        public System.Windows.Forms.StatusStrip stStrip;
        public System.Windows.Forms.Panel pInspector;
        public System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.ToolStripMenuItem mMenuSaveCoef;
        public System.Windows.Forms.ToolStripProgressBar pb;
        public System.Windows.Forms.ToolStripStatusLabel stBar1;
        private System.Windows.Forms.ToolStripMenuItem mMenuWindows;
        public System.Windows.Forms.ToolStripDropDownButton tsbCancel;
        private Infragistics.Win.UltraWinDock.AutoHideControl _fMainAutoHideControl;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow6;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow3;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _fMainUnpinnedTabAreaTop;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _fMainUnpinnedTabAreaBottom;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _fMainUnpinnedTabAreaLeft;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _fMainUnpinnedTabAreaRight;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow1;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow2;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow4;
        private System.Windows.Forms.ToolStripMenuItem mmFindSkv;
        private System.Windows.Forms.ToolStripMenuItem mMenuGlobalProjShow;
        private System.Windows.Forms.ToolStrip tbCanvas;
        private System.Windows.Forms.ToolStripButton tsbAddContour;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow5;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea6;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow8;
        public System.Windows.Forms.RichTextBox reLog;
        private System.Windows.Forms.ToolStrip tbWork;
        private System.Windows.Forms.ToolStripButton tsbSaveWork;
        public System.Windows.Forms.Panel pWork;
        public System.Windows.Forms.Panel pFilterOilObj;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow9;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea14;
        private System.Windows.Forms.ToolStripMenuItem mMenuHelp;
        private System.Windows.Forms.ToolStripMenuItem mMenuAbout;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea13;
        public System.Windows.Forms.Panel pGraphics;
        public System.Windows.Forms.Panel pMerView;
        public System.Windows.Forms.ToolStripContainer tsCanvContainer;
        public System.Windows.Forms.ToolStripDropDownButton tsbUpdates;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea17;
        public System.Windows.Forms.Panel pActiveOilField;
        private System.Windows.Forms.ToolStripMenuItem mMenuFlooding;
        private System.Windows.Forms.ToolStripMenuItem mMenuLoadCompensation;
        public System.Windows.Forms.Panel pChessView;
        public System.Windows.Forms.Panel pAreasView;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow11;
        public System.Windows.Forms.ToolStripContainer tsGraphicsContainer;
        private System.Windows.Forms.ToolStripButton tsbShowProjParams;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton tsbInterval;
        private System.Windows.Forms.ToolStripMenuItem tsbIntervalMonth;
        private System.Windows.Forms.ToolStripMenuItem tsbIntervalYear;
        private System.Windows.Forms.ToolStripMenuItem tsbIntervalDay;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbTable;
        public System.Windows.Forms.SplitContainer scGraphics;
        private System.Windows.Forms.ToolStripComboBox tscbScheme;
        public System.Windows.Forms.ToolStrip tbGraphics;
        private System.Windows.Forms.ToolStripButton tsbShowInfo;
        private System.Windows.Forms.ToolStripButton tsbCalcBubble;
        private System.Windows.Forms.ToolStripButton tsbCalcAccumBubble;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbCalcHistoryBubble;
        private System.Windows.Forms.ToolStripButton tsbBubbleSettings;
        private System.Windows.Forms.ToolStrip tbAreas;
        private System.Windows.Forms.ToolStripButton tsbCalcAreasParams;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbDrawAreasComp;
        private System.Windows.Forms.ToolStripButton tsbDrawAreasAccComp;
        private System.Windows.Forms.ToolStripMenuItem mMenuCheckUpdates;
        private System.Windows.Forms.ToolStripButton tsbLoadAreasParamsFromCache;
        private System.Windows.Forms.ToolStripButton tsbAdd;
        private System.Windows.Forms.ToolStripButton tsbDelete;
        private System.Windows.Forms.ToolStripLabel tsLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tsbEditIcons;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow7;
        public System.Windows.Forms.Panel pWellListSettings;
        public System.Windows.Forms.ToolStrip tsMenuWellListSettings;
        private System.Windows.Forms.ToolStripButton tsbWelllistSettings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem mMenuAppSettings;
        private System.Windows.Forms.ToolStripButton tsbShowLegend;
        private System.Windows.Forms.ToolStripMenuItem mMenuExport;
        private System.Windows.Forms.ToolStripMenuItem mmExportAreasContours;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton tsbVisibleAreas;
        private System.Windows.Forms.ToolStripButton tsbContoursColor;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow12;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow10;
        public System.Windows.Forms.Panel pGisView;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow13;
        public System.Windows.Forms.Panel pCore;
        private System.Windows.Forms.ToolStripButton tsbAddProfileByLine;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow14;
        public System.Windows.Forms.ToolStripContainer tsCorSchContainer;
        private System.Windows.Forms.ToolStrip tbCorScheme;
        private System.Windows.Forms.ToolStripButton tsbShowOilObjName;
        private System.Windows.Forms.ToolStripButton tsbCorSchSettings;
        private System.Windows.Forms.ToolStripButton tsbCorSchShowHead;
        private System.Windows.Forms.ToolStripButton tsbShowDepth;
        private System.Windows.Forms.ToolStripButton tsbShowGis;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton tsbShowMarker;
        private System.Windows.Forms.ToolStripButton tsbMarkerEditMode;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton tsbAddProfileByWell;
        public System.Windows.Forms.ToolStripContainer tsPlaneWellContainer;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea25;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow15;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea24;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea4;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea12;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea18;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea19;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea3;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea8;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea5;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea1;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea7;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea9;
        private System.Windows.Forms.ToolStrip tbPlane;
        private System.Windows.Forms.ToolStripButton tsbPlaneShowHead;
        private System.Windows.Forms.ToolStripButton tsbPlaneBindWidth;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinGis;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinGraphics;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinInspector;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinCorScheme;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinMest;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinMer;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinPlane;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinWorkFolder;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinProperties;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinLog;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinWellList;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinFilter;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinChess;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinOilFieldAreas;
        private System.Windows.Forms.ToolStripLabel tslComment;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow16;
        private System.Windows.Forms.ToolStripButton tsbCS_DepthAbsMd;
        private System.Windows.Forms.ToolStripButton tsbPlane_DepthAbsMd;
        private System.Windows.Forms.ToolStripButton tsbCS_ShowDuplicate;
        private System.Windows.Forms.ToolStripButton tsbPlane_ShowDuplicates;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        public System.Windows.Forms.Panel pCoreTest;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinCore;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinCoreTest;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea11;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea2;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea10;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea15;
        private System.Windows.Forms.ToolStripButton tsbGrayIcons;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton tsbGraphicsSettings;
        private System.Windows.Forms.ToolStripMenuItem tsbDrawAreasOverlay;
        private System.Windows.Forms.ToolStripButton tsbVisibleWellPad;
        private System.Windows.Forms.ToolStripSplitButton WL_AddWell;
        private System.Windows.Forms.ToolStripMenuItem WL_AddSelectWell;
        private System.Windows.Forms.ToolStripMenuItem WL_AddContourWell;
        private System.Windows.Forms.ToolStripMenuItem WL_AddClipBoardWell;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripSplitButton WL_Graphics;
        private System.Windows.Forms.ToolStripMenuItem WL_GraphicsByOneDate;
        private System.Windows.Forms.ToolStripMenuItem WL_GraphicsByAllDate;
        private System.Windows.Forms.ToolStripSplitButton tsbMapSnapShoot;
        private System.Windows.Forms.ToolStripMenuItem tsbPartMapSnapShot;
        private System.Windows.Forms.ToolStripMenuItem tsbAllMapSnapShoot;
        private System.Windows.Forms.ToolStripSplitButton GR_SnapShoot;
        private System.Windows.Forms.ToolStripMenuItem GR_SnapShootPart;
        private System.Windows.Forms.ToolStripMenuItem GR_SnapShootAll;
        private System.Windows.Forms.ToolStripButton tsbAddWellListByFilter;
        private System.Windows.Forms.ToolStripDropDownButton GR_ShowData;
        private System.Windows.Forms.ToolStripMenuItem GR_ShowGTM;
        private System.Windows.Forms.ToolStripMenuItem GR_ShowWellAction;
        private System.Windows.Forms.ToolStripSplitButton COR_SCH_SnapShoot;
        private System.Windows.Forms.ToolStripMenuItem COR_SCH_PartSnapShoot;
        private System.Windows.Forms.ToolStripMenuItem COR_SCH_AllSnapShoot;
        private System.Windows.Forms.ToolStripSplitButton PLN_SnapShoot;
        private System.Windows.Forms.ToolStripMenuItem PLN_PartSnapShoot;
        private System.Windows.Forms.ToolStripMenuItem PLN_AllSnapShoot;
        private Infragistics.Win.UltraWinDock.UltraDockManager uDockManager;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea21;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow17;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea20;
        public System.Windows.Forms.Panel pReportBuilder;
        private System.Windows.Forms.ToolStripMenuItem mMenuReportBuilder;
        private System.Windows.Forms.ToolStrip tbReportBuilder;
        private System.Windows.Forms.ToolStripDropDownButton rb_AddObject;
        private System.Windows.Forms.ToolStripMenuItem rb_AddOilfield;
        private System.Windows.Forms.ToolStripMenuItem rb_AddWell;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem rb_AddSumParams;
        private System.Windows.Forms.ToolStripMenuItem rb_AddMer;
        private System.Windows.Forms.ToolStripMenuItem rb_AddGis;
        private System.Windows.Forms.ToolStripMenuItem rb_AddGtm;
        private System.Windows.Forms.ToolStripButton rb_DeleteSelected;
        private System.Windows.Forms.ToolStripMenuItem rb_AddPerf;
        private System.Windows.Forms.ToolStripMenuItem rb_AddRepair;
        private System.Windows.Forms.ToolStripButton tsbPlaneWellSettings;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea16;
        private System.Windows.Forms.ToolStripButton tsbPlaneShowMarker;
        private System.Windows.Forms.ToolStripMenuItem mMenuReports;
        private System.Windows.Forms.ToolStripMenuItem mMenuReportChess;
        private System.Windows.Forms.ToolStripMenuItem tsbAreasParamsByDate;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mMenuResetWindows;
        private System.Windows.Forms.ToolStripMenuItem rb_AddResearch;
        private System.Windows.Forms.ToolStripMenuItem mMenuReportFloodingAnalysis;
        private System.Windows.Forms.ToolStripMenuItem GR_ShowWellLeak;
        private System.Windows.Forms.ToolStripMenuItem GR_ShowWellSuspend;
        private System.Windows.Forms.ToolStripMenuItem mMenuLoading;
        private System.Windows.Forms.ToolStripMenuItem mMenuLoadContoursAreasFromWF;
        private System.Windows.Forms.ToolStripMenuItem mMenuLoadTable81FromFile;
        private System.Windows.Forms.ToolStripMenuItem mMenuLoadBizPlanFromFile;
        private System.Windows.Forms.ToolStripButton tsbShowBizPlanOnMap;
        private System.Windows.Forms.ToolStripMenuItem rb_AddArea;
        private System.Windows.Forms.ToolStripButton tsbCreateVoronoiMap;
        private System.Windows.Forms.ToolStripMenuItem mMenuSendSupport;
        private System.Windows.Forms.ToolStripMenuItem GR_ShowWellStop;
        private System.Windows.Forms.ToolStripMenuItem mMenuCoreLoading;
        private System.Windows.Forms.ToolStripMenuItem mMenuCoreLoad;
        private System.Windows.Forms.ToolStripMenuItem mMenuCoreTestLoad;
        private System.Windows.Forms.ToolStripMenuItem mMenuLoadAreasPVTFromFile;
        public System.Windows.Forms.Panel pFileServer;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow18;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea23;
        private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow19;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinDiscussion;
        private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea26;
        public System.Windows.Forms.Panel pDiscussion;
        private System.Windows.Forms.ToolStripMenuItem mMenuPrintMap;
        private System.Windows.Forms.ToolStripMenuItem mMenuLoadDepositsFromFolder;
        private System.Windows.Forms.ToolStripMenuItem mMenuReportAreaPredict;
        private System.Windows.Forms.ToolStripButton tsbShowBPParams;
        private System.Windows.Forms.ToolStripButton tsbShowDiscussLetterBox;
        public System.Windows.Forms.ToolStripStatusLabel stBar2;
        public System.Windows.Forms.SplitContainer SplitDiscussion;
        private System.Windows.Forms.ToolStripMenuItem mMenuWinFileServer;
        private System.Windows.Forms.ToolStripMenuItem mMenuLoadPressure;
        private System.Windows.Forms.ToolStripMenuItem mMenuReportDepositOIZ;
        private System.Windows.Forms.ToolStripButton tsbLoadWellsTrajectory;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mMenuCreateGtmList;
        private System.Windows.Forms.ToolStripButton tsbPISeriesShow;
        private System.Windows.Forms.ToolStripMenuItem mMenuReportCore;
        private System.Windows.Forms.ToolStripMenuItem mMenuAutoMatchGTM;
        private System.Windows.Forms.ToolStripMenuItem mMenuWatsNew;
        private System.Windows.Forms.ToolStripMenuItem mMenuReportMerProduction;


    }
}

