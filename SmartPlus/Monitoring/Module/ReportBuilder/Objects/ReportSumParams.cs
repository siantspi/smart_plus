﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using SmartPlus.ReportBuilder.UI;
using System.ComponentModel;

namespace SmartPlus.ReportBuilder.Objects
{
    public class ReportSumParams : ReportObject
    {
        public ReportSumParams(int Index) : base(Index)
        {
            this.Name = "Суммарные показатели";
            Type = ObjectType.SumParameters;
            DataObjects.Add(new DataObject(DataObjectTypes.SumParams_Date, DataTypes.Date));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_PlastCode, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_Liq, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_Oil, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_Fprod, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_Finj, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_Inj, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_InjGas, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_LiqV, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_OilV, DataTypes.Number));

            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_AccumLiq, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_AccumOil, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_AccumWat, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_AccumInj, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_AccumInjGas, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_AccumLiqV, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_AccumOilV, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Sum_Params_AccumWatV, DataTypes.Number, "Накопленные показатели"));
            Width = 150;
            Height = 100;
        }
        public bool UsePlastConditions()
        {
            List<ConditionObject> plastCond = GetConditionsByType(DataObjectTypes.Sum_Params_PlastCode);
            List<DataObject> plastObj = GetReportObjectsByType(DataObjectTypes.Sum_Params_PlastCode);
            return (plastCond.Count > 0 || plastObj.Count > 0);
        }

        #region Connection
        public override bool TestConnection(int Index)
        {
            bool result = false;
            if ((Index > -1) && (Index < Connections.Count))
            {
                Connection conn = Connections[Index];
                if (conn.MainData.DataType == DataTypes.ProjectObject)
                {
                    SumParameters sumParams = (SumParameters)this.CurrentObject;
                    if(conn.TargetData.DataType == DataTypes.ProjectObject)
                    {
                        switch (conn.TargetObject.Type)
                        {
                            case ObjectType.OilField:
                                result = sumParams == ((OilField)this.ParentObject).sumParams;
                                break;
                            default:
                                throw new NotSupportedException("Тип связи не поддерживается");
                        }
                    }
                }
            }
            return result;
        }
        #endregion

        #region Conditions
        public List<SumParamItem> TestConditions(SumObjParameters SumParams)
        {
            List<SumParamItem> items = new List<SumParamItem>();

            for (int i = 0; i < SumParams.MonthlyItems.Length; i++)
            {
                if (TestConditions(SumParams.MonthlyItems[i], SumParams.OilObjCode))
                {
                    items.Add(SumParams.MonthlyItems[i]);
                }
            }
            return items;
        }
        bool TestConditions(SumParamItem item, int PlastCode)
        {
            bool result = true;
            for (int i = 0; i < Conditions.Count; i++)
            {
                result = result && TestCondition(item, Conditions[i], PlastCode);
                if(!result) break;
            }
            return result;
        }
        bool TestCondition(SumParamItem item, ConditionObject Condition, int PlastCode)
        {
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.Sum_Params_PlastCode:
                    return Condition.TestValue(PlastCode);
                case DataObjectTypes.SumParams_Date:
                    return Condition.TestValue(item.Date);
                case DataObjectTypes.Sum_Params_Liq:
                    return Condition.TestValue(item.Liq);
                case DataObjectTypes.Sum_Params_Oil:
                    return Condition.TestValue(item.Oil);
                case DataObjectTypes.Sum_Params_Inj:
                    return Condition.TestValue(item.Inj);
                case DataObjectTypes.Sum_Params_InjGas:
                    return Condition.TestValue(item.InjGas);
            }
            return false;
        }
        #endregion

        #region Fill Table
        public void FillAccumParams(List<SumParamItem> Items, List<ReportFormCell[]> Table, ReportFormCell[] InitRow, int ColumnIndex, int PlastCode)
        {
            ReportFormCell[] row;
            int col = ColumnIndex;
            double[] Acc = new double[8];

            for (int i = 0; i < Items.Count; i++)
            {
                Acc[0] += Items[i].Liq;
                Acc[1] += Items[i].Oil;
                Acc[2] += Items[i].Liq - Items[i].Oil;
                Acc[3] += Items[i].Inj;
                if (Items[i].InjGas > 0) Acc[4] += Items[i].InjGas;
                Acc[5] += Items[i].LiqV;
                Acc[6] += Items[i].OilV;
                Acc[7] += Items[i].LiqV - Items[i].OilV;
            }
            row = CloneRow(InitRow, ColumnIndex);
            for (int j = 0; j < ReportDataObjects.Count; j++)
            {
                switch (ReportDataObjects[j].Type)
                {
                    case DataObjectTypes.Sum_Params_PlastCode:
                        row[col++].Value = ReportDataObjects[j].GetDictionaryValue(PlastCode);
                        break;
                    case DataObjectTypes.Sum_Params_AccumLiq:
                        row[col++].Value = Acc[0];
                        break;
                    case DataObjectTypes.Sum_Params_AccumOil:
                        row[col++].Value = Acc[1];
                        break;
                    case DataObjectTypes.Sum_Params_AccumWat:
                        row[col++].Value = Acc[2];
                        break;
                    case DataObjectTypes.Sum_Params_AccumInj:
                        row[col++].Value = Acc[3];
                        break;
                    case DataObjectTypes.Sum_Params_AccumInjGas:
                        row[col++].Value = Acc[4];
                        break;
                    case DataObjectTypes.Sum_Params_AccumLiqV:
                        row[col++].Value = Acc[5];
                        break;
                    case DataObjectTypes.Sum_Params_AccumOilV:
                        row[col++].Value = Acc[6];
                        break;
                    case DataObjectTypes.Sum_Params_AccumWatV:
                        row[col++].Value = Acc[7];
                        break;

                    case DataObjectTypes.SumParams_Date:
                        goto case DataObjectTypes.Sum_Params_OilV;

                    case DataObjectTypes.Sum_Params_Fprod:
                        goto case DataObjectTypes.Sum_Params_OilV;

                    case DataObjectTypes.Sum_Params_Finj:
                        goto case DataObjectTypes.Sum_Params_OilV;

                    case DataObjectTypes.Sum_Params_Inj:
                        goto case DataObjectTypes.Sum_Params_OilV;

                    case DataObjectTypes.Sum_Params_InjGas:
                        goto case DataObjectTypes.Sum_Params_OilV;

                    case DataObjectTypes.Sum_Params_Liq:
                        goto case DataObjectTypes.Sum_Params_OilV;

                    case DataObjectTypes.Sum_Params_LiqV:
                        goto case DataObjectTypes.Sum_Params_OilV;

                    case DataObjectTypes.Sum_Params_Oil:
                        goto case DataObjectTypes.Sum_Params_OilV;

                    case DataObjectTypes.Sum_Params_OilV:
                        col++;
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип данных");
                }
            }
            Table.Add(row);
        }
        public void FillRow(object Object, int PlastCode, ReportFormCell[] Row, int ColumnIndex)
        {
            SumParamItem item = (SumParamItem)Object;
            int col = ColumnIndex;
            for (int j = 0; j < ReportDataObjects.Count; j++)
            {
                switch (ReportDataObjects[j].Type)
                {
                    case DataObjectTypes.Sum_Params_PlastCode:
                        Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(PlastCode);
                        break;
                    case DataObjectTypes.SumParams_Date:
                        Row[col++].Value = item.Date.Date;
                        break;
                    case DataObjectTypes.Sum_Params_Liq:
                        Row[col++].Value = item.Liq;
                        break;
                    case DataObjectTypes.Sum_Params_Oil:
                        Row[col++].Value = item.Oil;
                        break;
                    case DataObjectTypes.Sum_Params_Inj:
                        Row[col++].Value = item.Inj;
                        break;
                    case DataObjectTypes.Sum_Params_InjGas:
                        if (item.InjGas < 0) item.InjGas = 0;
                        Row[col++].Value = item.InjGas;
                        break;
                    case DataObjectTypes.Sum_Params_LiqV:
                        Row[col++].Value = item.LiqV;
                        break;
                    case DataObjectTypes.Sum_Params_OilV:
                        Row[col++].Value = item.OilV;
                        break;
                    case DataObjectTypes.Sum_Params_Fprod:
                        Row[col++].Value = item.Fprod;
                        break;
                    case DataObjectTypes.Sum_Params_Finj:
                        Row[col++].Value = item.Finj;
                        break;
                }
            }            
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            if (Object != null)
            {
                SumParameters SumParams = (SumParameters)Object;
                SumObjParameters SumObjParams;
                ReportFormCell[] row;
                bool UsePlasts = UsePlastConditions();
                int min = UsePlasts ? 1 : 0;
                int max = UsePlasts ? SumParams.Count : 1;
                for (int k = min; k < max; k++)
                {
                    SumObjParams = SumParams[k];
                    List<SumParamItem> items = TestConditions(SumObjParams);
                    if (TestAccumReportDataObject())
                    {
                        FillAccumParams(items, Table, InitRow, ColumnIndex, SumObjParams.OilObjCode);
                    }
                    else
                    {
                        for (int i = 0; i < items.Count; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            row = CloneRow(InitRow, ColumnIndex);
                            FillRow(items[i], SumObjParams.OilObjCode, row, ColumnIndex);
                            Table.Add(row);
                        }
                    }
                }
            }
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            return 0;
        }
        #endregion
    }
}

