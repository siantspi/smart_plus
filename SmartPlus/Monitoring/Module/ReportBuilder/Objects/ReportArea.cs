﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using SmartPlus.ReportBuilder.UI;
using System.ComponentModel;
using RDF.Objects;

namespace SmartPlus.ReportBuilder.Objects
{
    class ReportArea : ReportObject
    {
        public ReportArea(int Index) : base(Index)
        {
            this.Name = "Ячейка заводнения";
            Type = ObjectType.Area;
            DataObjects.Add(new DataObject(DataObjectTypes.Oilfield_Area, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Area_Name, DataTypes.String));
            DataObjects.Add(new DataObject(DataObjectTypes.Area_PlastCode, DataTypes.Dictionary));
            Width = 100;
            Height = 100;
        }

        #region Conditions
        public bool TestConditions(Area area)
        {
            bool result = true;
            for (int i = 0; i < Conditions.Count; i++)
            {
                result = result && TestCondition(area, Conditions[i]);
            }
            return result;
        }
        bool TestCondition(Area area, ConditionObject Condition)
        {
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.Oilfield_Area:
                    return Condition.TestValue(area.OilFieldAreaCode);
                case DataObjectTypes.Area_Name:
                    return Condition.TestValue(area.Name);
                case DataObjectTypes.Area_PlastCode:
                    return Condition.TestValue(area.PlastCode);
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных");
            }
        }
        #endregion

        #region Fill Table
        public override void FillRow(object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            Area area = (Area)Object;
            OilField of = (OilField)ParentObject;
            int col = ColumnIndex;
            for (int i = 0; i < ReportDataObjects.Count; i++)
            {
                switch (ReportDataObjects[i].Type)
                {
                    case DataObjectTypes.Oilfield_Area:
                        InitRow[col++].Value = ReportDataObjects[i].GetDictionaryValue(area.OilFieldAreaCode);
                        break;
                    case DataObjectTypes.Area_Name:
                        InitRow[col++].Value = area.Name;
                        break;
                    case DataObjectTypes.Area_PlastCode:
                        InitRow[col++].Value = ReportDataObjects[i].GetDictionaryValue(area.PlastCode);
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип данных");
                }
            }
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            if (TestConditions((Area)Object))
            {
                ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
                FillRow(Object, row, ColumnIndex);
                Table.Add(row);
                worker.ReportProgress(1);
            }
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            int maxColumnsCount = 0, count;
            Area area = (Area)Object;
            Well w;
            OilField of;
            if (TestConditions(area))
            {
                int column = ColumnIndex;
                ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
                FillRow(Object, row, ColumnIndex);
                columnsCount += this.ReportDataObjects.Count;
                this.CurrentObject = Object;

                for (int i = 0; i < ConnectedObjects.Count; i++)
                {
                    ConnectedObjects[i].ParentObject = this.ParentObject;
                    column += columnsCount;
                    maxColumnsCount = 0;
                    if (area.WellList != null)
                    {
                        for (int j = 0; j < area.WellList.Length; j++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return 0;
                            }
                            w = area.WellList[j];
                            if (w.Missing) continue;
                            count = ConnectedObjects[i].FillTable(worker, e, Table, w, row, ColumnIndex + columnsCount);
                            if (maxColumnsCount < count) maxColumnsCount = count;
                            worker.ReportProgress(j);
                        }
                    }
                    columnsCount += maxColumnsCount;
                }
            }
            return columnsCount;
        }
        #endregion
    }
}