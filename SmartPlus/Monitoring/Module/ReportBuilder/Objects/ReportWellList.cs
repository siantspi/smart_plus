﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SmartPlus.ReportBuilder.Base;
using System.ComponentModel;
using SmartPlus.ReportBuilder.UI;

namespace SmartPlus.ReportBuilder.Objects
{
    public class ReportWellList : ReportObject
    {
        string FullPath = string.Empty;
        public ReportWellList(int Index, BaseObj ProjectObject) : base(Index)
        {
            this.CanAddCondition = false;
            this.Name = ProjectObject.Name;
            C2DLayer layer = (C2DLayer)ProjectObject;
            this.CurrentObject = layer;
            if (layer.node != null && layer.node.TreeView != null) FullPath = layer.node.FullPath;
            Type = ObjectType.WellList;
            DataObjects.Add(new DataObject(DataObjectTypes.WellList_Name, DataTypes.String));
            Width = 100;
            Height = 100;
        }
        C2DLayer CreateSortedWellList(C2DLayer layer)
        {
            C2DLayer newLayer = new C2DLayer(layer.ObjTypeID, layer.oilFieldIndex);
            newLayer.Name = layer.Name;
            PhantomWell pw;
            List<PhantomWell> wells = new List<PhantomWell>();
            int index = -1;
            for (int i = 0; i < layer.ObjectsList.Count; i++)
            {
                pw = (PhantomWell)layer.ObjectsList[i];
                index = -1;
                for (int j = 0; j < wells.Count; j++)
                {
                    if (wells[j].srcWell.OilFieldIndex > pw.srcWell.OilFieldIndex)
                    {
                        index = j;
                        break;
                    }
                }
                if (index > -1)
                {
                    
                    wells.Insert(index, pw);
                }
                else
                {
                    wells.Add(pw);
                }
            }
            newLayer.ObjectsList.AddRange(wells.ToArray());
            return newLayer;
        }

        #region Conditions
        public bool TestConditions(C2DLayer wl)
        {
            bool result = true;
            for (int i = 0; i < Conditions.Count; i++)
            {
                result = result && TestCondition(wl, Conditions[i]);
            }
            return result;
        }
        bool TestCondition(C2DLayer wl, ConditionObject Condition)
        {
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.WellList_Name:
                    return Condition.TestValue(wl.Name);
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных");
            }
        }
        #endregion

        #region Fill Table
        public override void FillRow(object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            C2DLayer wl = (C2DLayer)Object;
            int col = ColumnIndex;
            for (int i = 0; i < ReportDataObjects.Count; i++)
            {
                switch (ReportDataObjects[i].Type)
                {
                    case DataObjectTypes.WellList_Name:
                        InitRow[col++].Value = wl.Name;
                        break;
                }
            }
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
            FillRow(this.CurrentObject, row, ColumnIndex);
            Table.Add(row);
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            int maxColumnsCount = 0, count;
            int column = ColumnIndex;
            ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
            FillRow(this.CurrentObject, row, ColumnIndex);
            columnsCount += this.ReportDataObjects.Count;
            this.ParentObject = Object;

            Well w;
            OilField of = null;
            Project proj = (Project)Object;
            this.ParentObject = proj;
            
            this.CurrentObject = CreateSortedWellList((C2DLayer)this.CurrentObject);
            C2DLayer wellList = (C2DLayer)this.CurrentObject;

            int lastOFindex = -1, wellCount;
            bool OFdataLoaded = false;
            PhantomWell pw = null;
            for (int i = 0; i < ConnectedObjects.Count; i++)
            {
                column += columnsCount;
                maxColumnsCount = 0;
                wellCount = 0;
                
                for (int j = 0; j < wellList.ObjectsList.Count; j++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return 0;
                    }
                    pw = (PhantomWell)wellList.ObjectsList[j];
                    w = pw.srcWell;
                    DateTime savedStartDate = w.StartDate;
                    try
                    {
                        if (w.Missing) continue;
                        if (lastOFindex != w.OilFieldIndex)
                        {
                            if ((OFdataLoaded) && (of != null)) ClearOilFieldData(of);
                            OFdataLoaded = false;
                            of = (OilField)proj.OilFields[w.OilFieldIndex];
                            ConnectedObjects[i].ParentObject = of;
                            if (wellCount > of.Wells.Count * 0.5)
                            {
                                LoadOilFieldData(worker, e, of);
                                OFdataLoaded = true;
                            }
                            wellCount = 0;
                            lastOFindex = w.OilFieldIndex;
                        }
                        if(!OFdataLoaded) LoadWellData(w);
                        if (pw.Caption != null && pw.Caption.Date != DateTime.MinValue)
                        {
                            w.StartDate = pw.Caption.Date;
                        }
                        count = ConnectedObjects[i].FillTable(worker, e, Table, w, row, ColumnIndex + columnsCount);
                        
                        if (maxColumnsCount < count) maxColumnsCount = count;
                        worker.ReportProgress(j);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Ошибка обработки скважины " + w.Name, ex);
                    }
                    finally
                    {
                        w.StartDate = savedStartDate;
                        if (!OFdataLoaded) ClearWellData(w);
                    }
                    wellCount++;
                }
                columnsCount += maxColumnsCount;
            }
            return columnsCount;
        }
        #endregion

        #region LOADING DATA
        public void LoadOilFieldData(ReportBackgroundWorker worker, DoWorkEventArgs e, OilField of)
        {
            if (TestConnectedObj(ObjectType.Mer))
            {
                of.LoadMerFromCache(worker, e, false, true);
            }
            if (TestConnectedObj(ObjectType.SumParameters))
            {
                of.LoadSumParamsFromCache(worker, e, true);
            }
            if (TestConnectedObj(ObjectType.Gis))
            {
                of.LoaвGisFromCache(worker, e, false, true);
            }
            if (TestConnectedObj(ObjectType.GTM))
            {
                of.LoadGTMFromCache(worker, e, true);
            }
            if (TestConnectedObj(ObjectType.RepairAction))
            {
                of.LoadWellActionFromCache(worker, e, true);
            }
            if (TestConnectedObj(ObjectType.Perforation))
            {
                of.LoadPerfFromCache(worker, e, false, true);
            }
            if (TestConnectedObj(ObjectType.WellResearch))
            {
                of.LoadWellResearchFromCache(worker, e, true);
            }
        }
        public void ClearOilFieldData(OilField of)
        {
            of.ClearMerData(false);
            if (of.GisLoaded) of.ClearGisData(false);
            if (of.ChessLoaded) of.ClearChessData(false);
            if (of.PerfLoaded) of.ClearPerfData(false);
            if (of.sumParams != null) of.ClearSumParams(false);
            if (of.LogsLoaded) of.ClearLogsData(false);
            if (of.LogsBaseLoaded) of.ClearLogsBaseData(false);
            if (of.WellGtmLoaded) of.ClearGTMData(false);
            if (of.WellRepairActionLoaded) of.ClearWellActionData(false);
            if (of.WellResearchLoaded) of.ClearWellResearchData(false);
        }
        public void LoadWellData(Well w)
        {
            Project project = (Project)this.ParentObject;
            OilField of = (OilField)project.OilFields[w.OilFieldIndex];
            if (TestConnectedObj(ObjectType.Mer))
            {
                of.LoadMerFromCache(w.Index);
            }
            if (TestConnectedObj(ObjectType.Gis))
            {
                of.LoadGisFromCache(w.Index);
            }
            if (TestConnectedObj(ObjectType.GTM))
            {
                of.LoadGTMFromCache(w.Index);
            }
            if (TestConnectedObj(ObjectType.RepairAction))
            {
                of.LoadWellActionFromCache(w.Index);
            }
            if (TestConnectedObj(ObjectType.Perforation))
            {
                of.LoadPerfFromCache(w.Index);
            }
            if (TestConnectedObj(ObjectType.WellResearch))
            {
                of.LoadWellResearchFromCache(w.Index);
            }
        }
        public void ClearWellData(Well w)
        {
            if (TestConnectedObj(ObjectType.Mer))
            {
                if (w.mer != null) w.mer.SetItems(null, null);
                w.mer = null;
                w.merEx = null;
            }
            if (TestConnectedObj(ObjectType.Gis))
            {
                w.gis = null;
            }
            if (TestConnectedObj(ObjectType.GTM))
            {
                if (w.gtm != null)
                {
                    w.gtm.Clear();
                    w.gtm = null;
                }
            }
            if (TestConnectedObj(ObjectType.RepairAction))
            {
                if (w.action != null)
                {
                    w.action.Clear();
                    w.action = null;
                }
            }
            if (TestConnectedObj(ObjectType.Perforation))
            {
                w.perf = null;
            }
            if (TestConnectedObj(ObjectType.WellResearch))
            {
                if (w.research != null)
                {
                    w.research.Clear();
                    w.research = null;
                }
            }
        }
        #endregion
    }
}