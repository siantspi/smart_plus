﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using RDF.Objects;
using SmartPlus.ReportBuilder.UI;
using System.ComponentModel;

namespace SmartPlus.ReportBuilder.Objects
{
    class ReportPerforation : ReportObject
    {
        public ReportPerforation(int Index) : base(Index)
        {
            this.Name = "Перфорация";
            Type = ObjectType.Perforation;
            DataObjects.Add(new DataObject(DataObjectTypes.Perf_Date, DataTypes.Date));
            DataObjects.Add(new DataObject(DataObjectTypes.Perf_Type, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Perf_IntervalMD, DataTypes.NumberInterval));
            Width = 100;
            Height = 100;
        }

        #region Connection
        public bool TestConnections(SkvPerfItem item)
        {
            bool res = true;
            ConditionObject cond;
            Connection conn;
            object val = null, testVal = null;
            for (int i = 0; i < Connections.Count; i++)
            {
                conn = Connections[i];
                switch (conn.TargetObject.Type)
                {
                    case ObjectType.Well:
                        ReportWell well = (ReportWell)conn.TargetObject;
                        res = true;
                        break;
                    case ObjectType.Gis:
                        ReportGis gis = (ReportGis)conn.TargetObject;
                        double[] vals;
                        switch (conn.TargetData.Type)
                        {
                            case DataObjectTypes.Gis_IntervalMD:
                                vals = new double[2];
                                vals[0] = item.Top;
                                vals[1] = item.Bottom;
                                testVal = vals;
                                 
                                vals = new double[2];
                                vals[0] = ((SkvGisItem)gis.CurrentObject).H;
                                vals[1] = ((SkvGisItem)gis.CurrentObject).H + ((SkvGisItem)gis.CurrentObject).L;
                                val = vals;
                                break;
                            case DataObjectTypes.Gis_IntervalAbs:
                                vals = new double[2];
                                vals[0] = item.Top;
                                vals[1] = item.Bottom;
                                testVal = vals;

                                vals = new double[2];
                                vals[0] = ((SkvGisItem)gis.CurrentObject).Habs;
                                vals[1] = ((SkvGisItem)gis.CurrentObject).Habs + ((SkvGisItem)gis.CurrentObject).Labs;
                                val = vals;
                                break;
                            default:
                                throw new ArgumentException("Не поддерживаемый тип данных объекта!");
                        }
                        break;
                    case ObjectType.Mer:
                        ReportMer mer = (ReportMer)conn.TargetObject;
                        switch(conn.TargetData.Type)
                        {
                            case DataObjectTypes.Mer_Date:
                                testVal = item.Date;
                                val = (mer.ReportByPlast) ? ((MerCompPlastItem)mer.CurrentObject).ParentItem.Date : ((MerCompItem)mer.CurrentObject).Date;
                                break;
                        }
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип объекта!");
                }
                if (val != null && testVal != null)
                {
                    cond = new ConditionObject(conn, val);
                    res = cond.TestValue(testVal);
                }
                if (!res) break;
            }
            return res;
        }
        #endregion

        #region Conditions
        public List<SkvPerfItem> TestConditions(SkvPerf Perf)
        {
            List<SkvPerfItem> items = new List<SkvPerfItem>();
            for (int i = 0; i < Perf.Count; i++)
            {
                if (TestConditions(Perf.Items[i]))
                {
                    items.Add(Perf.Items[i]);
                }
            }
            return items;
        }
        bool TestConditions(SkvPerfItem item)
        {
            bool res = true;
            if (ConditionDictRepeats != null)
            {
                List<int> indexes;
                bool res2 = false;
                for (int i = 0; i < ConditionDictRepeats.Count; i++)
                {
                    indexes = ConditionDictRepeats.GetConditionIndexes(i);
                    res2 = false;
                    for (int j = 0; (j < indexes.Count) && (!res2); j++)
                    {
                        res2 = TestCondition(item, Conditions[indexes[j]]) || res2;
                    }
                }
                res = res && res2;
            }
            for (int i = 0; i < Conditions.Count; i++)
            {
                if (!res) break;
                if (Conditions[i].Object.DataType != DataTypes.Dictionary)
                {
                    res = res && TestCondition(item, Conditions[i]);
                }
            }
            return res;
        }
        bool TestCondition(SkvPerfItem item, ConditionObject Condition)
        {
            double[] values;
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.Perf_Date:
                    return Condition.TestValue(item.Date);
                case DataObjectTypes.Perf_Type:
                    return Condition.TestValue((int)item.PerfTypeId);
                case DataObjectTypes.Perf_IntervalMD:
                    values = new double[2];
                    values[0] = item.Top;
                    values[1] = item.Bottom;
                    return Condition.TestValue(values);
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных.");
            }
        }
        #endregion

        #region Fill Table
        public override void FillRow(object Object, ReportFormCell[] Row, int ColumnIndex)
        {
            SkvPerfItem item = (SkvPerfItem)Object;
            int col = ColumnIndex;
            for (int j = 0; j < ReportDataObjects.Count; j++)
            {
                switch (ReportDataObjects[j].Type)
                {
                    case DataObjectTypes.Perf_Date:
                        Row[col++].Value = item.Date.Date;
                        break;
                    case DataObjectTypes.Perf_IntervalMD:
                        Row[col++].Value = string.Format("'{0};{1}", item.Top, item.Bottom);
                        break;
                    case DataObjectTypes.Perf_Type:
                        Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.PerfTypeId);
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип данных");
                }
            }
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            if (Object != null)
            {
                ReportFormCell[] row;
                List<SkvPerfItem> items = TestConditions((SkvPerf)Object);
                for (int i = 0; i < items.Count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    if (TestConnections(items[i]))
                    {
                        row = CloneRow(InitRow, ColumnIndex);
                        FillRow(items[i], row, ColumnIndex);
                        Table.Add(row);
                    }
                }
            }
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnCount = 0, column;
            if (Object != null)
            {
                ReportFormCell[] row;
                List<SkvPerfItem> items = TestConditions((SkvPerf)Object);
                columnCount += this.ReportDataObjects.Count;
                column = ColumnIndex + columnCount;
                for (int i = 0; i < items.Count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return 0;
                    }
                    if (TestConditions(items[i]) && TestConnections(items[i]))
                    {
                        row = CloneRow(InitRow, ColumnIndex);
                        FillRow(items[i], row, ColumnIndex);
                        this.CurrentObject = items[i];
                        columnCount += FillConnectedObjects(worker, e, Table, row, column);
                    }
                }
            }
            return columnCount;
        }
        int FillConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            for (int i = 0; i < ConnectedObjects.Count; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return 0;
                }
                Well w = (Well)this.ParentObject;
                switch (ConnectedObjects[i].Type)
                {
                    case ObjectType.Gis:
                        columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.gis, InitRow, ColumnIndex + columnsCount);
                        break;
                    case ObjectType.Mer:
                        if (w.MerLoaded)
                        {
                            MerComp merComp = new MerComp(w.mer);
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, merComp, InitRow, ColumnIndex + columnsCount);
                            merComp.Clear();
                        }
                        break;
                }
            }
            return columnsCount;
        }
        #endregion
    }
}
