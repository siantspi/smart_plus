﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using SmartPlus.ReportBuilder.UI;
using System.ComponentModel;
using RDF.Objects;

namespace SmartPlus.ReportBuilder.Objects
{
    class ReportWell : ReportObject
    {
        public ReportWell(int Index) : base(Index)
        {
            this.Name = "Скважина";
            Type = ObjectType.Well;
            DataObjects.Add(new DataObject(DataObjectTypes.NGDU_Name, DataTypes.String));
            DataObjects.Add(new DataObject(DataObjectTypes.OilField_Name, DataTypes.String));
            DataObjects.Add(new DataObject(DataObjectTypes.Oilfield_Area, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Well_Name, DataTypes.String));
            Width = 100;
            Height = 100;
        }

        #region Conditions
        public bool TestConditions(Well w)
        {
            bool result = true;
            for (int i = 0; i < Conditions.Count; i++)
            {
                result = result && TestCondition(w, Conditions[i]);
            }
            return result;
        }
        bool TestCondition(Well w, ConditionObject Condition)
        {
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.Well_Name:
                    return Condition.TestValue(w.Name);
                case DataObjectTypes.Oilfield_Area:
                    return Condition.TestValue(w.OilFieldAreaCode);
                case DataObjectTypes.OilField_Name:
                    return Condition.TestValue(((OilField)this.ParentObject).Name);
                case DataObjectTypes.NGDU_Name:
                    return Condition.TestValue(((OilField)this.ParentObject).ParamsDict.NGDUName);
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных");
            }
        }
        #endregion

        #region Fill Table
        public override void FillRow(object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            Well w = (Well)Object;
            OilField of = (OilField)ParentObject;
            int col = ColumnIndex;
            for (int i = 0; i < ReportDataObjects.Count; i++)
            {
                switch (ReportDataObjects[i].Type)
                {
                    case DataObjectTypes.Well_Name:
                        InitRow[col++].Value = w.Name;
                        break;
                    case DataObjectTypes.Oilfield_Area:
                        InitRow[col++].Value = ReportDataObjects[i].GetDictionaryValue(w.OilFieldAreaCode);
                        break;
                    case DataObjectTypes.OilField_Name:
                        InitRow[col++].Value = of.Name;
                        break;
                    case DataObjectTypes.NGDU_Name:
                        InitRow[col++].Value = of.ParamsDict.NGDUName;
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип данных");
                }
            }
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            if (TestConditions((Well)Object))
            {
                ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
                FillRow(Object, row, ColumnIndex);
                Table.Add(row);
                worker.ReportProgress(1);
            }
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            if (TestConditions((Well)Object))
            {
                int column = ColumnIndex;
                ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
                FillRow(Object, row, ColumnIndex);
                columnsCount += this.ReportDataObjects.Count;
                this.CurrentObject = Object;
                for (int i = 0; i < ConnectedObjects.Count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return 0;
                    }
                    ConnectedObjects[i].ParentObject = Object;
                    column += columnsCount;
                    Well w = (Well)Object;
                    switch (ConnectedObjects[i].Type)
                    {
                        case ObjectType.Mer:
                            if (w.MerLoaded)
                            {
                                MerComp merComp = new MerComp(w.mer);
                                columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, merComp, row, ColumnIndex + columnsCount);
                                merComp.Clear();
                            }
                            break;
                        case ObjectType.Gis:
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.gis, row, ColumnIndex + columnsCount);
                            break;
                        case ObjectType.Coord:
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.coord, row, ColumnIndex + columnsCount);
                            break;
                        case ObjectType.Chess:
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.chess, row, ColumnIndex + columnsCount);
                            break;
                        case ObjectType.Logs:
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.logs, row, ColumnIndex + columnsCount);
                            break;
                        case ObjectType.Perforation:
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.perf, row, ColumnIndex + columnsCount);
                            break;
                        case ObjectType.GTM:
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.gtm, row, ColumnIndex + columnsCount);
                            break;
                        case ObjectType.RepairAction:
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.action, row, ColumnIndex + columnsCount);
                            break;
                        case ObjectType.WellResearch:
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.research, row, ColumnIndex + columnsCount);
                            break;
                        default:
                            throw new ArgumentException("Не поддерживаемый тип данных.");
                    }
                }
                worker.ReportProgress(1);
            }
            return columnsCount;
        }
        #endregion
    }
}
