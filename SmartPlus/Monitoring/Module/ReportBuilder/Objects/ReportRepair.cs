﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using RDF.Objects;
using SmartPlus.ReportBuilder.UI;
using System.ComponentModel;

namespace SmartPlus.ReportBuilder.Objects
{
    class ReportRepair : ReportObject
    {
        public ReportRepair(int Index) : base(Index)
        {
            this.Name = "Ремонты";
            Type = ObjectType.RepairAction;
            DataObjects.Add(new DataObject(DataObjectTypes.Repair_Date, DataTypes.Date));
            DataObjects.Add(new DataObject(DataObjectTypes.Repair_Type, DataTypes.Dictionary));
            Width = 100;
            Height = 100;
        }
        #region Connections
        public bool TestConnections(WellActionItem item)
        {
            bool res = true;
            ConditionObject cond;
            Connection conn;
            object val = null, testVal = null;
            for (int i = 0; i < Connections.Count; i++)
            {
                conn = Connections[i];
                switch (conn.TargetObject.Type)
                {
                    case ObjectType.Well:
                        ReportWell well = (ReportWell)conn.TargetObject;
                        res = true;
                        break;
                    case ObjectType.Mer:
                        ReportMer mer = (ReportMer)conn.TargetObject;
                        switch (conn.TargetData.Type)
                        {
                            case DataObjectTypes.Mer_Date:
                                testVal = new DateTime(item.Date.Year, item.Date.Month, 1);
                                if (mer.ReportByPlast)
                                {
                                    val = ((MerCompPlastItem)mer.CurrentObject).ParentItem.Date;
                                }
                                else
                                {
                                    val = ((MerCompItem)mer.CurrentObject).Date;
                                }
                                break;
                            default:
                                throw new ArgumentException("Не поддерживаемый тип данных объекта!");
                        }
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип объекта!");
                }
                if (val != null && testVal != null)
                {
                    cond = new ConditionObject(conn, val);
                    res = cond.TestValue(testVal);
                }
                if (!res) break;
            }
            return res;
        }
        #endregion

        #region Conditions
        public List<WellActionItem> TestConditions(WellAction Repair)
        {
            List<WellActionItem> items = new List<WellActionItem>();
            for (int i = 0; i < Repair.Count; i++)
            {
                if (TestConditions(Repair[i]))
                {
                    items.Add(Repair[i]);
                }
            }
            return items;
        }
        bool TestConditions(WellActionItem item)
        {
            bool res = true;
            if (ConditionDictRepeats != null)
            {
                List<int> indexes;
                bool res2 = false;
                for (int i = 0; i < ConditionDictRepeats.Count; i++)
                {
                    indexes = ConditionDictRepeats.GetConditionIndexes(i);
                    res2 = false;
                    for (int j = 0; (j < indexes.Count) && (!res2); j++)
                    {
                        res2 = TestCondition(item, Conditions[indexes[j]]) || res2;
                    }
                }
                res = res && res2;
            }
            for (int i = 0; i < Conditions.Count; i++)
            {
                if (!res) break;
                if (Conditions[i].Object.DataType != DataTypes.Dictionary)
                {
                    res = res && TestCondition(item, Conditions[i]);
                }
            }
            return res;
        }
        bool TestCondition(WellActionItem item, ConditionObject Condition)
        {
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.Repair_Date:
                    return Condition.TestValue(item.Date);
                case DataObjectTypes.Repair_Type:
                    return Condition.TestValue((int)item.WellActionCode);
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных.");
            }
        }
        #endregion

        #region Fill Table
        public override void FillRow(object Object, ReportFormCell[] Row, int ColumnIndex)
        {
            WellActionItem item = (WellActionItem)Object;
            int col = ColumnIndex;
            for (int j = 0; j < ReportDataObjects.Count; j++)
            {
                switch (ReportDataObjects[j].Type)
                {
                    case DataObjectTypes.Repair_Date:
                        Row[col++].Value = item.Date.Date;
                        break;
                    case DataObjectTypes.Repair_Type:
                        Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.WellActionCode);
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип данных");
                }
            }
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            if (Object != null)
            {
                ReportFormCell[] row;
                List<WellActionItem> items = TestConditions((WellAction)Object);
                for (int i = 0; i < items.Count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    if (TestConnections(items[i]))
                    {
                        row = CloneRow(InitRow, ColumnIndex);
                        FillRow(items[i], row, ColumnIndex);
                        Table.Add(row);
                    }
                }
            }
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnCount = 0, column;
            if (Object != null)
            {
                ReportFormCell[] row;
                List<WellActionItem> items = TestConditions((WellAction)Object);
                columnCount += this.ReportDataObjects.Count;
                column = ColumnIndex + columnCount;
                for (int i = 0; i < items.Count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return 0;
                    }
                    if (TestConditions(items[i]) && TestConnections(items[i]))
                    {
                        row = CloneRow(InitRow, ColumnIndex);
                        FillRow(items[i], row, ColumnIndex);
                        this.CurrentObject = items[i];
                        columnCount += FillConnectedObjects(worker, e, Table, row, column);
                    }
                }
            }
            return columnCount;
        }
        int FillConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            for (int i = 0; i < ConnectedObjects.Count; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return 0;
                }
                Well w = (Well)this.ParentObject;
                switch (ConnectedObjects[i].Type)
                {
                    case ObjectType.Mer:
                        if (w.MerLoaded)
                        {
                            MerComp merComp = new MerComp(w.mer);
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, merComp, InitRow, ColumnIndex + columnsCount);
                            merComp.Clear();
                        }
                        break;
                }
            }
            return columnsCount;
        }
        #endregion
    }
}