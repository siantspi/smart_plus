﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SmartPlus.ReportBuilder.Base;
using System.ComponentModel;
using SmartPlus.ReportBuilder.UI;

namespace SmartPlus.ReportBuilder.Objects
{
    public class ReportOilField : ReportObject
    {
        public ReportOilField(int Index) : base(Index)
        {
            this.Name = "Месторождение";
            Type = ObjectType.OilField;
            DataObjects.Add(new DataObject(DataObjectTypes.NGDU_Name, DataTypes.String));
            DataObjects.Add(new DataObject(DataObjectTypes.OilField_Name, DataTypes.String));
            DataObjects.Add(new DataObject(DataObjectTypes.OilField_WellCount, DataTypes.Number));
            Width = 100;
            Height = 100;
        }

        #region Conditions
        public bool TestConditions(OilField of)
        {
            bool result = true;
            for (int i = 0; i < Conditions.Count; i++)
            {
                result = result && TestCondition(of, Conditions[i]);
            }
            return result;
        }
        bool TestCondition(OilField of, ConditionObject Condition)
        {
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.OilField_Name:
                    return Condition.TestValue(of.Name);
                case DataObjectTypes.OilField_WellCount:
                    return Condition.TestValue((double)of.Wells.Count);
                case DataObjectTypes.NGDU_Name:
                    return Condition.TestValue(of.ParamsDict.NGDUName);
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных");
            }
        }
        #endregion

        #region Fill Table
        public override void FillRow(object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            OilField of = (OilField)Object;
            int col = ColumnIndex;
            for (int i = 0; i < ReportDataObjects.Count; i++)
            {
                switch (ReportDataObjects[i].Type)
                {
                    case DataObjectTypes.OilField_Name:
                        InitRow[col++].Value = of.Name;
                        break;
                    case DataObjectTypes.NGDU_Name:
                        InitRow[col++].Value = of.ParamsDict.NGDUName;
                        break;
                    case DataObjectTypes.OilField_WellCount:
                        InitRow[col++].Value = of.Wells.Count.ToString();
                        break;
                }
            }
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            if (TestConditions((OilField)Object))
            {
                ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
                FillRow(Object, row, ColumnIndex);
                Table.Add(row);
                worker.ReportProgress(1);
            }
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            if (TestConditions((OilField)Object))
            {
                int maxColumnsCount = 0, count;
                int column = ColumnIndex;
                ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
                FillRow(Object, row, ColumnIndex);
                columnsCount += this.ReportDataObjects.Count;
                this.CurrentObject = Object;
                try
                {
                    LoadData(worker, e);
                    for (int i = 0; i < ConnectedObjects.Count; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return 0;
                        }
                        ConnectedObjects[i].ParentObject = Object;
                        column += columnsCount;
                        OilField of = (OilField)this.ParentObject;
                        Well w;
                        switch (ConnectedObjects[i].Type)
                        {
                            case ObjectType.Area:
                                maxColumnsCount = 0;
                                for (int j = 0; j < of.Areas.Count; j++)
                                {
                                    count = ConnectedObjects[i].FillTable(worker, e, Table, of.Areas[j], row, ColumnIndex + columnsCount);
                                    if (maxColumnsCount < count) maxColumnsCount = count;
                                }
                                columnsCount += maxColumnsCount;
                                break;
                            case ObjectType.Well:
                                maxColumnsCount = 0;
                                for (int j = 0; j < of.Wells.Count; j++)
                                {
                                    w = of.Wells[j];
                                    if (w.Missing) continue;
                                    count = ConnectedObjects[i].FillTable(worker, e, Table, of.Wells[j], row, ColumnIndex + columnsCount);
                                    if (maxColumnsCount < count) maxColumnsCount = count;
                                }
                                columnsCount += maxColumnsCount;
                                break;
                            case ObjectType.SumParameters:
                                if (of.sumParams == null) of.LoadSumParamsFromCache(worker, e);
                                columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, of.sumParams, row, ColumnIndex + columnsCount);
                                break;
                        }
                    }
                }
                finally
                {
                    ClearData();
                }
                worker.ReportProgress(1);
            }
            return columnsCount;
        }
        #endregion

        #region LOADING DATA
        public void LoadData(ReportBackgroundWorker worker, DoWorkEventArgs e)
        {
            OilField of = (OilField)this.CurrentObject;

            if (TestConnectedObj(ObjectType.Mer))
            {
                of.LoadMerFromCache(worker, e, false, true);
            }
            if (TestConnectedObj(ObjectType.SumParameters))
            {
                of.LoadSumParamsFromCache(worker, e, true);
            }
            if (TestConnectedObj(ObjectType.Gis))
            {
                of.LoaвGisFromCache(worker, e, false, true);
            }
            if (TestConnectedObj(ObjectType.GTM))
            {
                of.LoadGTMFromCache(worker, e, true);
            }
            if (TestConnectedObj(ObjectType.RepairAction))
            {
                of.LoadWellActionFromCache(worker, e, true);
            }
            if (TestConnectedObj(ObjectType.Perforation))
            {
                of.LoadPerfFromCache(worker, e, false, true);
            }
            if (TestConnectedObj(ObjectType.WellResearch))
            {
                of.LoadWellResearchFromCache(worker, e, true);
            }
        }
        public void ClearData()
        {
            OilField of = (OilField)this.CurrentObject;
            of.ClearMerData(false);
            if (of.GisLoaded) of.ClearGisData(false);
            if (of.ChessLoaded) of.ClearChessData(false);
            if (of.PerfLoaded) of.ClearPerfData(false);
            if (of.sumParams != null) of.ClearSumParams(false);
            if (of.LogsLoaded) of.ClearLogsData(false);
            if (of.LogsBaseLoaded) of.ClearLogsBaseData(false);
            if (of.WellGtmLoaded) of.ClearGTMData(false);
            if (of.WellRepairActionLoaded) of.ClearWellActionData(false);
            if (of.WellResearchLoaded) of.ClearWellResearchData(false);
            GC.GetTotalMemory(true);
        }
        #endregion
    }
}
