﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using RDF.Objects;
using SmartPlus.ReportBuilder.UI;
using System.ComponentModel;

namespace SmartPlus.ReportBuilder.Objects
{
    class ReportMer : ReportObject
    {
        public bool ReportByPlast;
        ConditionSpecialDate SpecialDate;
        List<object> SavedValueConditions;

        public ReportMer(int Index) : base(Index)
        {
            this.Name = "Данные МЭР";
            Type = ObjectType.Mer;
            SpecialDate = ConditionSpecialDate.None;
            SavedValueConditions = null;
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Date, DataTypes.Date));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_PlastCode, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Charwork, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Method, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_State, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_WorkTime, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Liq, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Oil, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Watering, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Inj, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_LiqV, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_OilV, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Gas, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.None, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Qliq, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Qoil, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_Winj, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_QliqV, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_QoilV, DataTypes.Number));

            DataObjects.Add(new DataObject(DataObjectTypes.Mer_AccumLiquid, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_AccumOil, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_AccumWater, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_AccumInjection, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_AccumInjectionGas, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_AccumLiquidV, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_AccumOilV, DataTypes.Number, "Накопленные показатели"));
            DataObjects.Add(new DataObject(DataObjectTypes.Mer_AccumWaterV, DataTypes.Number, "Накопленные показатели"));

            Width = 100;
            Height = 100;
            this.OnFieldsChange += new OnFiledChangeDelegate(ReportMer_OnConditionsChange);
        }

        void ReportMer_OnConditionsChange()
        {
            ReportByPlast = UsePlastConditions();
        }
        public bool UsePlastConditions()
        {
            List<ConditionObject> plastCond = GetConditionsByType(DataObjectTypes.Mer_PlastCode);
            List<DataObject> plastObj = GetReportObjectsByType(DataObjectTypes.Mer_PlastCode);
            return (plastCond.Count > 0 || plastObj.Count > 0);
        }

        #region Connections
        public bool TestConnections(MerCompPlastItem item)
        {
            bool res = true;
            ConditionObject cond;
            Connection conn;
            object val = null, testVal = null;
            for (int i = 0; i < Connections.Count; i++)
            {
                conn = Connections[i];
                switch (conn.TargetObject.Type)
                {
                    case ObjectType.Well:
                        ReportWell well = (ReportWell)conn.TargetObject;
                        res = true;
                        break;
                    case ObjectType.Gis:
                        ReportGis gis = (ReportGis)conn.TargetObject;
                        switch (conn.TargetData.Type)
                        {
                            case DataObjectTypes.Gis_PlastCode:
                                testVal = (int)item.PlastCode;
                                ConditionObjectDict dict = new ConditionObjectDict();
                                dict.DictCode = ((SkvGisItem)gis.CurrentObject).PlastId;
                                dict.StratumDict = (SmartPlus.DictionaryObjects.StratumDictionary)DictList.GetDictionary(SmartPlus.DictionaryObjects.DICTIONARY_ITEM_TYPE.STRATUM);
                                val = dict;
                                break;
                            default:
                                throw new ArgumentException("Не поддерживаемый тип данных связи объекта!");
                        }
                        break;
                    case ObjectType.WellResearch:
                        ReportResearch research = (ReportResearch)conn.TargetObject;
                        if (conn.TargetData.Type == DataObjectTypes.Research_Date)
                        {
                            testVal = (DateTime)item.ParentItem.Date;
                            DateTime dt = ((WellResearchItem)research.CurrentObject).Date;
                            val = new DateTime(dt.Year, dt.Month, 1);
                        }
                        else
                        {
                            res = false;
                        }
                        break;
                    case ObjectType.Perforation:
                        var perf = (ReportPerforation)conn.TargetObject;
                        if (conn.TargetData.Type == DataObjectTypes.Perf_Date)
                        {
                            testVal = (DateTime)item.ParentItem.Date;
                            DateTime dt = ((SkvPerfItem)perf.CurrentObject).Date;
                            val = new DateTime(dt.Year, dt.Month, 1);
                        }
                        else
                        {
                            res = false;
                        }
                        break;
                    case ObjectType.GTM:
                        ReportGTM gtm = (ReportGTM)conn.TargetObject;
                        if (conn.TargetData.Type == DataObjectTypes.GTM_Date)
                        {
                            testVal = (DateTime)item.ParentItem.Date;
                            DateTime dt = ((WellGTMItem)gtm.CurrentObject).Date;
                            val = new DateTime(dt.Year, dt.Month, 1);
                        }
                        else
                        {
                            res = false;
                        }
                        break;
                    case ObjectType.RepairAction:
                        ReportRepair repair = (ReportRepair)conn.TargetObject;
                        if (conn.TargetData.Type == DataObjectTypes.Repair_Date)
                        {
                            testVal = (DateTime)item.ParentItem.Date;
                            DateTime dt = ((WellActionItem)repair.CurrentObject).Date;
                            val = new DateTime(dt.Year, dt.Month, 1);
                        }
                        else
                        {
                            res = false;
                        }
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип объекта!");
                }
                if (val != null && testVal != null)
                {
                    cond = new ConditionObject(conn, val);
                    res = cond.TestValue(testVal);
                }
                if (!res) break;
            }
            return res;
        }
        public bool TestConnections(MerCompItem item)
        {
            bool res = true;
            ConditionObject cond;
            Connection conn;
            object val = null, testVal = null;
            for (int i = 0; i < Connections.Count; i++)
            {
                conn = Connections[i];
                switch (conn.TargetObject.Type)
                {
                    case ObjectType.Well:
                        ReportWell well = (ReportWell)conn.TargetObject;
                        res = true;
                        break;
                    case ObjectType.GTM:
                        ReportGTM gtm = (ReportGTM)conn.TargetObject;
                        if (conn.TargetData.Type == DataObjectTypes.GTM_Date)
                        {
                            testVal = (DateTime)item.Date;
                            DateTime dt = ((WellGTMItem)gtm.CurrentObject).Date;
                            val = new DateTime(dt.Year, dt.Month, 1);
                        }
                        else
                        {
                            res = false;
                        }
                        break;
                    case ObjectType.RepairAction:
                        ReportRepair repair = (ReportRepair)conn.TargetObject;
                        if (conn.TargetData.Type == DataObjectTypes.Repair_Date)
                        {
                            testVal = (DateTime)item.Date;
                            DateTime dt = ((WellActionItem)repair.CurrentObject).Date;
                            val = new DateTime(dt.Year, dt.Month, 1);
                        }
                        else
                        {
                            res = false;
                        }
                        break;
                    case ObjectType.WellResearch:
                        ReportResearch research = (ReportResearch)conn.TargetObject;
                        if (conn.TargetData.Type == DataObjectTypes.Research_Date)
                        {
                            testVal = (DateTime)item.Date;
                            DateTime dt = ((WellResearchItem)research.CurrentObject).Date;
                            val = new DateTime(dt.Year, dt.Month, 1);
                        }
                        else
                        {
                            res = false;
                        }
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип объекта!");
                }
                if (val != null && testVal != null)
                {
                    cond = new ConditionObject(conn, val);
                    res = cond.TestValue(testVal);
                }
                if (!res) break;
            }
            return res;
        }
        #endregion

        #region Special Date
        public void TestSpecialDate(MerComp MerEx)
        {
            int i;
            Well w = null;
            if (this.ParentObject != null) w = (Well)this.ParentObject;
            List<ConditionObject> dateCond = GetConditionsByType(DataObjectTypes.Mer_Date);
            SpecialDate = ConditionSpecialDate.None;
            DateTime specDate = new DateTime(1900, 1, 1);
            if (dateCond.Count > 0)
            {
                DateTime MerStartDate = new DateTime(1900, 1, 1);
                DateTime WellListDate = new DateTime(1900, 1, 2);
                for (i = 0; i < dateCond.Count; i++)
                {
                    if (((DateTime)dateCond[i].Value).Date == MerStartDate)
                    {
                        SpecialDate = ConditionSpecialDate.MerStartWork;
                        specDate = (DateTime)dateCond[i].Value;
                        break;
                    }
                    if (((DateTime)dateCond[i].Value).Date == WellListDate && w != null && w.StartDate.Year >= 1900)
                    {
                        SpecialDate = ConditionSpecialDate.WellListDate;
                        specDate = (DateTime)dateCond[i].Value;
                        break;
                    }
                }
            }
            DateTime dt;
            if (SavedValueConditions == null) SavedValueConditions = new List<object>();
            if (SpecialDate != ConditionSpecialDate.None)
            {
                DateTime findDate = new DateTime(1900, 1, 1);
                switch (SpecialDate)
                {
                    case ConditionSpecialDate.MerStartWork:
                        for (i = 0; i < MerEx.Count; i++)
                        {
                            if (MerEx[i].SumLiquid > 0 || MerEx[i].SumInjection > 0 || MerEx[i].SumInjectionGas > 0 || MerEx[i].SumNaturalGas > 0 || MerEx[i].SumGasCondensate > 0)
                            {
                                findDate = MerEx[i].Date;
                                break;
                            }
                        }
                        break;
                    case ConditionSpecialDate.WellListDate:
                        findDate = w.StartDate;
                        break;
                }
                SavedValueConditions.Clear();
                for (i = 0; i < dateCond.Count; i++)
                {
                    SavedValueConditions.Add(dateCond[i].Value);
                    dt = (DateTime)dateCond[i].Value;
                    if (dt.Date == specDate.Date)
                    {
                        if (dt.Year >= 1900 && dt.Millisecond > 0 && dt.Millisecond < 500)
                        {
                            int k = (dt.Second > 0) ? -1 : 1;
                            dateCond[i].SetValue(findDate.AddMonths(k * dt.Millisecond));
                        }
                        else
                        {
                            dateCond[i].SetValue(findDate);
                        }
                    }
                }
            }
            else
            {
                SavedValueConditions.Clear();
                for (i = 0; i < dateCond.Count; i++)
                {
                    SavedValueConditions.Add(dateCond[i].Value);
                    dt = (DateTime)dateCond[i].Value;
                    if (dt.Year >= 1900 && dt.Millisecond > 0 && dt.Millisecond < 500)
                    {
                        int k = (dt.Second > 0) ? -1 : 1;
                        dt = dt.AddMonths(k * dt.Millisecond);
                        dateCond[i].SetValue(dt);
                    }
                }
            }
        }
        public void ClearSpecialDate()
        {
            if (SavedValueConditions != null)
            {
                List<ConditionObject> dateCond = GetConditionsByType(DataObjectTypes.Mer_Date);
                for (int i = 0; (i < dateCond.Count) && (i < SavedValueConditions.Count); i++)
                {
                    dateCond[i].SetValue(SavedValueConditions[i]);
                }
                SavedValueConditions.Clear();
            }
        }
        #endregion

        #region Conditions
        public List<MerCompItem> TestConditions(MerComp MerEx)
        {
            List<MerCompItem> items = new List<MerCompItem>();
            TestSpecialDate(MerEx);
            for (int i = 0; i < MerEx.Count; i++)
            {
                if (TestConditions(MerEx[i]))
                {
                    items.Add(MerEx[i]);
                }
            }
            ClearSpecialDate();
            return items;
        }
        public List<MerCompPlastItem> TestConditionsByPlast(MerComp MerEx)
        {
            List<MerCompPlastItem> items = new List<MerCompPlastItem>();
            List<MerCompPlastItem> items2 = new List<MerCompPlastItem>();
            TestSpecialDate(MerEx);
            for (int i = 0; i < MerEx.Count; i++)
            {
                items2 = GetItemsByConditions(MerEx[i]);
                for (int j = 0; j < items2.Count; j++)
                {
                    items.Add(items2[j]);
                }
            }
            ClearSpecialDate();
            return items;
        }
        List<MerCompPlastItem> GetItemsByConditions(MerCompItem item)
        {
            List<MerCompPlastItem> result = new List<MerCompPlastItem>();
            if (TestConditions(item))
            {
                for (int i = 0; i < item.PlastItems.Count; i++)
                {
                    if (TestConditions(item.PlastItems[i]))
                    {
                        result.Add(item.PlastItems[i]);
                    }
                }
            }
            return result;
        }

        bool TestConditions(MerCompItem item)
        {
            bool res = true;
            if (ConditionDictRepeats != null)
            {
                List<int> indexes;
                bool res2 = false;
                for (int i = 0; i < ConditionDictRepeats.Count; i++)
                {
                    indexes = ConditionDictRepeats.GetConditionIndexes(i);
                    res2 = false;
                    for (int j = 0; (j < indexes.Count) && (!res2); j++)
                    {
                        res2 = TestCondition(item, Conditions[indexes[j]]) || res2;
                    }
                }
                res = res && res2;
            }
            for (int i = 0; i < Conditions.Count; i++)
            {
                if (!res) break;
                if (Conditions[i].Object.DataType != DataTypes.Dictionary)
                {
                    res = res && TestCondition(item, Conditions[i]);
                }
            }
            return res;
        }
        bool TestConditions(MerCompPlastItem item)
        {
            bool res = true;
            if (ConditionDictRepeats != null)
            {
                List<int> indexes;
                bool res2 = false;
                for (int i = 0; i < ConditionDictRepeats.Count; i++)
                {
                    indexes = ConditionDictRepeats.GetConditionIndexes(i);
                    res2 = false;
                    for (int j = 0; (j < indexes.Count) && (!res2); j++)
                    {
                        res2 = TestCondition(item, Conditions[indexes[j]]) || res2;
                    }
                }
                res = res && res2;
            }
            for (int i = 0; i < Conditions.Count; i++)
            {
                if (!res) break;
                if (Conditions[i].Object.DataType != DataTypes.Dictionary)
                {
                    res = res && TestCondition(item, Conditions[i]);
                }
            }
            return res;
        }
        bool TestCondition(MerCompItem item, ConditionObject Condition)
        {
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.Mer_Date:
                    return Condition.TestValue(item.Date);
                case DataObjectTypes.Mer_PlastCode:
                    break;
                case DataObjectTypes.Mer_Charwork:
                    bool res = false;
                    for (int i = 0; i < item.CharWorkIds.Count; i++)
                    {
                        res = Condition.TestValue(item.CharWorkIds[i]);
                        if (res) break;
                    }
                    return res;
                case DataObjectTypes.Mer_Method:
                    return Condition.TestValue(item.MethodId);
                case DataObjectTypes.Mer_State:
                    return Condition.TestValue(item.StateId);
                case DataObjectTypes.Mer_WorkTime:
                    if (!ReportByPlast)
                    {
                        MerWorkTimeItem wtItem = item.TimeItems.GetAllTime();
                        return Condition.TestValue(wtItem.WorkTime + wtItem.CollTime);
                    }
                    break;
                case DataObjectTypes.Mer_Liq:
                    if (!ReportByPlast)
                    {
                        return Condition.TestValue(item.SumLiquid);
                    }
                    break;
                case DataObjectTypes.Mer_Qliq:
                    if (!ReportByPlast)
                    {
                        double qliq = 0;
                        MerWorkTimeItem wtItem = item.TimeItems.GetAllTime(20, true);
                        if (wtItem.WorkTime + wtItem.CollTime > 0) qliq = item.SumLiquid * 24 / wtItem.GetWorkHours();
                        return Condition.TestValue(qliq);
                    }
                    break;
                case DataObjectTypes.Mer_Oil:
                    if (!ReportByPlast)
                    {
                        return Condition.TestValue(item.SumOil);
                    }
                    break;
                case DataObjectTypes.Mer_Qoil:
                    if (!ReportByPlast)
                    {
                        double qoil = 0;
                        MerWorkTimeItem wtItem = item.TimeItems.GetAllTime(20, true);
                        if (wtItem.WorkTime + wtItem.CollTime > 0) qoil = item.SumOil * 24 / wtItem.GetWorkHours();
                        return Condition.TestValue(qoil);
                    }
                    break;
                case DataObjectTypes.Mer_LiqV:
                    if (!ReportByPlast)
                    {
                        return Condition.TestValue(item.SumLiquidV);
                    }
                    break;
                case DataObjectTypes.Mer_Gas:
                    if (!ReportByPlast)
                    {
                        return Condition.TestValue(item.SumGas);
                    }
                    break;
                case DataObjectTypes.Mer_QliqV:
                    if (!ReportByPlast)
                    {
                        double qliq = 0;
                        MerWorkTimeItem wtItem = item.TimeItems.GetAllTime(20, true);
                        if (wtItem.WorkTime + wtItem.CollTime > 0) qliq = item.SumLiquidV * 24 / wtItem.GetWorkHours();
                        return Condition.TestValue(qliq);
                    }
                    break;
                case DataObjectTypes.Mer_OilV:
                    if (!ReportByPlast)
                    {
                        return Condition.TestValue(item.SumOilV);
                    }
                    break;
                case DataObjectTypes.Mer_QoilV:
                    if (!ReportByPlast)
                    {
                        double qoil = 0;
                        MerWorkTimeItem wtItem = item.TimeItems.GetAllTime(20, true);
                        if (wtItem.WorkTime + wtItem.CollTime > 0) qoil = item.SumOilV * 24 / wtItem.GetWorkHours();
                        return Condition.TestValue(qoil);
                    }
                    break;
                case DataObjectTypes.Mer_Inj:
                    if (!ReportByPlast)
                    {
                        return Condition.TestValue(item.SumInjection);
                    }
                    break;
                case DataObjectTypes.Mer_Winj:
                    if (!ReportByPlast)
                    {
                        double winj = 0;
                        MerWorkTimeItem wtItem = item.TimeItems.GetAllTime(20, false);
                        if (wtItem.WorkTime + wtItem.CollTime > 0) winj = item.SumInjection * 24 / wtItem.GetWorkHours();
                        return Condition.TestValue(winj);
                    }
                    break;
                case DataObjectTypes.Mer_Watering:
                    if (!ReportByPlast)
                    {
                        double water = 0;
                        if (item.SumLiquid > 0) water = (item.SumLiquid - item.SumOil) * 100 / item.SumLiquid;
                        return Condition.TestValue(water);
                    }
                    break;
                    
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных.");
            }
            return true;
        }
        bool TestCondition(MerCompPlastItem item, ConditionObject Condition)
        {
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.Mer_PlastCode:
                    return Condition.TestValue(item.PlastCode);
                case DataObjectTypes.Mer_Date:
                    return Condition.TestValue(item.ParentItem.Date);
                case DataObjectTypes.Mer_Charwork:
                    bool res = false;
                    for (int i = 0; i < item.ParentItem.CharWorkIds.Count; i++)
                    {
                        res = Condition.TestValue(item.ParentItem.CharWorkIds[i]);
                        if (res) break;
                    }
                    return res;
                case DataObjectTypes.Mer_Method:
                    return Condition.TestValue(item.ParentItem.MethodId);
                case DataObjectTypes.Mer_State:
                    return Condition.TestValue(item.ParentItem.StateId);
                case DataObjectTypes.Mer_Liq:
                    return Condition.TestValue(item.Liq);
                case DataObjectTypes.Mer_Qliq:
                    double qliq = 0;
                    MerWorkTimeItem wtItem = item.ParentItem.TimeItems.GetProductionTime(item.PlastCode);
                    if (wtItem.WorkTime + wtItem.CollTime > 0) qliq = item.Liq * 24 / wtItem.GetWorkHours();
                    return Condition.TestValue(qliq);
                case DataObjectTypes.Mer_Oil:
                    return Condition.TestValue(item.Oil);
                case DataObjectTypes.Mer_Qoil:
                    double qoil = 0;
                    wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, true);
                    if (wtItem.WorkTime + wtItem.CollTime > 0) qoil = item.Oil * 24 / wtItem.GetWorkHours();
                    return Condition.TestValue(qoil);
                case DataObjectTypes.Mer_LiqV:
                    return Condition.TestValue(item.LiqV);
                case DataObjectTypes.Mer_Gas:
                    return Condition.TestValue(item.Gas);
                case DataObjectTypes.Mer_QliqV:
                    qliq = 0;
                    wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, true);
                    if (wtItem.WorkTime + wtItem.CollTime > 0) qliq = item.LiqV * 24 / wtItem.GetWorkHours();
                    return Condition.TestValue(qliq);
                case DataObjectTypes.Mer_OilV:
                    return Condition.TestValue(item.OilV);
                case DataObjectTypes.Mer_QoilV:
                    qoil = 0;
                    wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, true);
                    if (wtItem.WorkTime + wtItem.CollTime > 0) qoil = item.OilV * 24 / wtItem.GetWorkHours();
                    return Condition.TestValue(qoil);
                case DataObjectTypes.Mer_Inj:
                    return Condition.TestValue(item.Inj);
                case DataObjectTypes.Mer_Winj:
                    double winj = 0;
                    wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, false);
                    if (wtItem.WorkTime + wtItem.CollTime > 0) winj = item.Inj * 24 / wtItem.GetWorkHours();
                    return Condition.TestValue(winj);
                case DataObjectTypes.Mer_WorkTime:
                    wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, true);
                    MerWorkTimeItem wtItem2 = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, false);
                    return Condition.TestValue(wtItem.WorkTime + wtItem.CollTime + wtItem2.WorkTime + wtItem2.CollTime);
                case DataObjectTypes.Mer_Watering:
                    double water = 0;
                    if (item.Liq > 0) water = (item.Liq - item.Oil) * 100 / item.Liq;
                    return Condition.TestValue(water);
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных.");
            }
        }
        #endregion

        #region Fill Table
        public void FillAccumParams(List<MerCompPlastItem> Items, List<ReportFormCell[]> Table, ReportFormCell[] InitRow, int ColumnIndex)
        {
            ReportFormCell[] row;
            int col = ColumnIndex, ind;
            List<int> PlastCodes = new List<int>();
            List<double[]> Acc = new List<double[]>();

            for(int i = 0; i < Items.Count;i++)
            {
                ind = PlastCodes.IndexOf(Items[i].PlastCode);
                if(ind == -1)
                {
                    ind = PlastCodes.Count;
                    PlastCodes.Add(Items[i].PlastCode);
                    Acc.Add(new double[6]);
                }
                Acc[ind][0] += Items[i].Liq; 
                Acc[ind][1] += Items[i].Oil;
                Acc[ind][2] += Items[i].LiqV;
                Acc[ind][3] += Items[i].OilV;
                Acc[ind][4] += Items[i].Inj;
                Acc[ind][5] += Items[i].InjGas;
            }
            for(int i = 0; i < PlastCodes.Count;i++)
            {
                col = ColumnIndex;
                row = CloneRow(InitRow, ColumnIndex);
                for (int j = 0; j < ReportDataObjects.Count; j++)
                {
                    switch (ReportDataObjects[j].Type)
                    {
                        case DataObjectTypes.Mer_PlastCode:
                            row[col++].Value = ReportDataObjects[j].GetDictionaryValue(PlastCodes[i]);
                            break;
                        case DataObjectTypes.Mer_AccumLiquid:
                            row[col++].Value = Acc[i][0];
                            break;
                        case DataObjectTypes.Mer_AccumOil:
                            row[col++].Value = Acc[i][1];
                            break;
                        case DataObjectTypes.Mer_AccumWater:
                            row[col++].Value = Acc[i][0] - Acc[i][1];
                            break;
                        case DataObjectTypes.Mer_AccumLiquidV:
                            row[col++].Value = Acc[i][2];
                            break;
                        case DataObjectTypes.Mer_AccumOilV:
                            row[col++].Value = Acc[i][3];
                            break;
                        case DataObjectTypes.Mer_AccumWaterV:
                            row[col++].Value = Acc[i][2] - Acc[i][3];
                            break;
                        case DataObjectTypes.Mer_AccumInjection:
                            row[col++].Value = Acc[i][4];
                            break;
                        case DataObjectTypes.Mer_AccumInjectionGas:
                            row[col++].Value = Acc[i][5];
                            break;
                        case DataObjectTypes.Mer_Charwork:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Method:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_State:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Date:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Liq:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Oil:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Gas:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_LiqV:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_OilV:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Inj:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_WorkTime:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Qliq:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Qoil:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_QliqV:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_QoilV:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Winj:
                            goto case DataObjectTypes.Mer_Watering;
                        case DataObjectTypes.Mer_Watering:
                            col++;
                            break;
                        default:
                            throw new ArgumentException("Не поддерживаемый тип данных");
                    }
                }
                Table.Add(row);
            }
        }
        public void FillAccumParams(List<MerCompItem> Items, List<ReportFormCell[]> Table, ReportFormCell[] InitRow, int ColumnIndex)
        {
            ReportFormCell[] row;
            int col = ColumnIndex;
            double[] Acc = new double[6];

            for (int i = 0; i < Items.Count; i++)
            {
                Acc[0] += Items[i].SumLiquid;
                Acc[1] += Items[i].SumOil;
                Acc[2] += Items[i].SumLiquidV;
                Acc[3] += Items[i].SumOilV;
                Acc[4] += Items[i].SumInjection;
                Acc[5] += Items[i].SumInjectionGas;
            }
            row = CloneRow(InitRow, ColumnIndex);
            for (int j = 0; j < ReportDataObjects.Count; j++)
            {
                switch (ReportDataObjects[j].Type)
                {
                    case DataObjectTypes.Mer_AccumLiquid:
                        row[col++].Value = Acc[0];
                        break;
                    case DataObjectTypes.Mer_AccumOil:
                        row[col++].Value = Acc[1];
                        break;
                    case DataObjectTypes.Mer_AccumWater:
                        row[col++].Value = Acc[0] - Acc[1];
                        break;
                    case DataObjectTypes.Mer_AccumLiquidV:
                        row[col++].Value = Acc[2];
                        break;
                    case DataObjectTypes.Mer_AccumOilV:
                        row[col++].Value = Acc[3];
                        break;
                    case DataObjectTypes.Mer_AccumWaterV:
                        row[col++].Value = Acc[2] - Acc[3];
                        break;
                    case DataObjectTypes.Mer_AccumInjection:
                        row[col++].Value = Acc[4];
                        break;
                    case DataObjectTypes.Mer_AccumInjectionGas:
                        row[col++].Value = Acc[5];
                        break;
                    case DataObjectTypes.Mer_Charwork:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Method:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_State:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Date:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Liq:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Oil:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Gas:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_LiqV:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_OilV:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Inj:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_WorkTime:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Qliq:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Qoil:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_QliqV:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_QoilV:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Winj:
                        goto case DataObjectTypes.Mer_Watering;
                    case DataObjectTypes.Mer_Watering:
                        col++;
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип данных");

                }
            }
            Table.Add(row);
        }
        public override void FillRow(object Object, ReportFormCell[] Row, int ColumnIndex)
        {
            if (ReportByPlast)
            {
                MerCompPlastItem item = (MerCompPlastItem)Object;
                int col = ColumnIndex;
                for (int j = 0; j < ReportDataObjects.Count; j++)
                {
                    switch (ReportDataObjects[j].Type)
                    {
                        case DataObjectTypes.Mer_PlastCode:
                            Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.PlastCode);
                            break;
                        case DataObjectTypes.Mer_Charwork:
                            int charWork = 0;
                            if (item.ParentItem.CharWorkIds.Count > 0)
                            {
                                charWork = item.ParentItem.CharWorkIds[item.ParentItem.CharWorkIds.Count - 1];
                            }
                            Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(charWork);
                            break;
                        case DataObjectTypes.Mer_Method:
                            Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.ParentItem.MethodId);
                            break;
                        case DataObjectTypes.Mer_State:
                            Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.ParentItem.StateId);
                            break;
                        case DataObjectTypes.Mer_Date:
                            Row[col++].Value = item.ParentItem.Date.Date;
                            break;
                        case DataObjectTypes.Mer_Liq:
                            Row[col++].Value = item.Liq;
                            break;
                        case DataObjectTypes.Mer_Oil:
                            Row[col++].Value = item.Oil;
                            break;
                        case DataObjectTypes.Mer_Gas:
                            Row[col++].Value = item.Gas;
                            break;
                        case DataObjectTypes.Mer_LiqV:
                            Row[col++].Value = item.LiqV;
                            break;
                        case DataObjectTypes.Mer_OilV:
                            Row[col++].Value = item.OilV;
                            break;
                        case DataObjectTypes.Mer_Inj:
                            Row[col++].Value = item.Inj;
                            break;
                        case DataObjectTypes.Mer_WorkTime:
                            MerWorkTimeItem wtItem = item.ParentItem.TimeItems.GetAllTime();
                            Row[col++].Value = wtItem.GetWorkHours();
                            break;
                        case DataObjectTypes.Mer_Qliq:
                            wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, true);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.Liq * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_Qoil:
                            wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, true);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.Oil * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_QliqV:
                            wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, true);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.LiqV * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_QoilV:
                            wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, true);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.OilV * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_Winj:
                            wtItem = item.ParentItem.TimeItems.GetTime(item.PlastCode, 20, false);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.Inj * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_Watering:
                            if (item.Liq > 0)
                            {
                                Row[col++].Value = (item.Liq - item.Oil) / item.Liq;
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        default:
                            throw new ArgumentException("Не поддерживаемый тип данных");
                    }
                }
            }
            else
            {
                MerCompItem item = (MerCompItem)Object;
                int col = ColumnIndex;
                for (int j = 0; j < ReportDataObjects.Count; j++)
                {
                    switch (ReportDataObjects[j].Type)
                    {
                        case DataObjectTypes.Mer_Charwork:
                            int charWork = 0;
                            if (item.CharWorkIds.Count > 0)
                            {
                                charWork = item.CharWorkIds[item.CharWorkIds.Count - 1];
                            }
                            Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(charWork);
                            break;
                        case DataObjectTypes.Mer_Method:
                            Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.MethodId);
                            break;
                        case DataObjectTypes.Mer_State:
                            Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.StateId);
                            break;
                        case DataObjectTypes.Mer_Date:
                            Row[col++].Value = item.Date.Date;
                            break;
                        case DataObjectTypes.Mer_Liq:
                            Row[col++].Value = item.SumLiquid;
                            break;
                        case DataObjectTypes.Mer_Gas:
                            Row[col++].Value = item.SumGas;
                            break;
                        case DataObjectTypes.Mer_Oil:
                            Row[col++].Value = item.SumOil;
                            break;
                        case DataObjectTypes.Mer_LiqV:
                            Row[col++].Value = item.SumLiquidV;
                            break;
                        case DataObjectTypes.Mer_OilV:
                            Row[col++].Value = item.SumOilV;
                            break;
                        case DataObjectTypes.Mer_Inj:
                            Row[col++].Value = item.SumInjection;
                            break;
                        case DataObjectTypes.Mer_WorkTime:
                            MerWorkTimeItem wtItem = item.TimeItems.GetAllTime();
                            Row[col++].Value = wtItem.GetWorkHours();
                            break;
                        case DataObjectTypes.Mer_Qliq:
                            wtItem = item.TimeItems.GetAllTime(20, true);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.SumLiquid * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_Qoil:
                            wtItem = item.TimeItems.GetAllTime(20, true);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.SumOil * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_QliqV:
                            wtItem = item.TimeItems.GetAllTime(20, true);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.SumLiquidV * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_QoilV:
                            wtItem = item.TimeItems.GetAllTime(20, true);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.SumOilV * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_Winj:
                            wtItem = item.TimeItems.GetAllTime(20, false);
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                Row[col++].Value = item.SumInjection * 24 / wtItem.GetWorkHours();
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        case DataObjectTypes.Mer_Watering:
                            if (item.SumLiquid > 0)
                            {
                                Row[col++].Value = (item.SumLiquid - item.SumOil) / item.SumLiquid;
                            }
                            else
                            {
                                Row[col++].Value = 0;
                            }
                            break;
                        default:
                            throw new ArgumentException("Не поддерживаемый тип данных");
                    }
                } 
            }
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            if (Object != null)
            {
                ReportFormCell[] row;
                if (ReportByPlast)
                {
                    List<MerCompPlastItem> items = TestConditionsByPlast((MerComp)Object);
                    if (TestAccumReportDataObject())
                    {
                        FillAccumParams(items, Table, InitRow, ColumnIndex);
                    }
                    else
                    {
                        for (int i = 0; i < items.Count; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            if (TestConnections(items[i]))
                            {
                                row = CloneRow(InitRow, ColumnIndex);
                                FillRow(items[i], row, ColumnIndex);
                                Table.Add(row);
                            }
                        }
                    }
                }
                else
                {
                    List<MerCompItem> items = TestConditions((MerComp)Object);
                    if (TestAccumReportDataObject())
                    {
                        FillAccumParams(items, Table, InitRow, ColumnIndex);
                    }
                    else
                    {
                        for (int i = 0; i < items.Count; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            if (TestConnections(items[i]))
                            {
                                row = CloneRow(InitRow, ColumnIndex);
                                FillRow(items[i], row, ColumnIndex);
                                Table.Add(row);
                            }
                        }
                    }
                }
            }
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnCount = 0, column;
            if (Object != null)
            {
                ReportFormCell[] row;
                if (ReportByPlast)
                {
                    List<MerCompPlastItem> items = TestConditionsByPlast((MerComp)Object);
                    columnCount += this.ReportDataObjects.Count;
                    column = ColumnIndex + columnCount;
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return 0;
                        }
                        if (TestConditions(items[i]))
                        {
                            row = CloneRow(InitRow, ColumnIndex);
                            FillRow(items[i], row, ColumnIndex);
                            this.CurrentObject = items[i];
                            columnCount += FillConnectedObjects(worker, e, Table, row, column);
                        }
                    }
                }
                else
                {
                    List<MerCompItem> items = TestConditions((MerComp)Object);
                    columnCount += this.ReportDataObjects.Count;
                    column = ColumnIndex + columnCount;
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return 0;
                        }
                        if (GetItemsByConditions(items[i]).Count > 0)
                        {
                            row = CloneRow(InitRow, ColumnIndex);
                            FillRow(items[i], row, ColumnIndex);
                            this.CurrentObject = items[i];
                            columnCount += FillConnectedObjects(worker, e, Table, row, column);
                        }
                    }
                }
            }
            return columnCount;
        }
        int FillConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            for (int i = 0; i < ConnectedObjects.Count; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return 0;
                }
                Well w = (Well)this.ParentObject;
                switch (ConnectedObjects[i].Type)
                {
                    case ObjectType.Perforation:
                        columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.perf, InitRow, ColumnIndex + columnsCount);
                        break;
                    case ObjectType.Gis:
                        columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.gis, InitRow, ColumnIndex + columnsCount);
                        break;
                    case ObjectType.GTM:
                        columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.gtm, InitRow, ColumnIndex + columnsCount);
                        break;
                    case ObjectType.RepairAction:
                        columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.action, InitRow, ColumnIndex + columnsCount);
                        break;
                    case ObjectType.WellResearch:
                        columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.research, InitRow, ColumnIndex + columnsCount);
                        break;
                }
            }
            return columnsCount; 
        }
        #endregion
    }
}