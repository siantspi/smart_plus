﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using RDF.Objects;
using SmartPlus.ReportBuilder.UI;
using System.ComponentModel;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.ReportBuilder.Objects
{
    class ReportGis : ReportObject
    {
        public ReportGis(int Index) : base(Index)
        {
            this.Name = "Данные ГИС";
            Type = ObjectType.Gis;
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_PlastCode, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_IntervalMD, DataTypes.NumberInterval));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_IntervalAbs, DataTypes.NumberInterval));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_Collector, DataTypes.Bool));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_Lit, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_Sat, DataTypes.Dictionary));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_Porosity, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_Permeability, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_OilSat, DataTypes.Number));
            DataObjects.Add(new DataObject(DataObjectTypes.Gis_GasSat, DataTypes.Number));

            Width = 100;
            Height = 100;
        }
        
        #region Connection
        public bool TestConnections(SkvGisItem item)
        {
            bool res = true;
            ConditionObject cond;
            Connection conn;
            object val = null, testVal = null;
            for (int i = 0; i < Connections.Count; i++)
            {
                conn = Connections[i];
                switch (conn.TargetObject.Type)
                {
                    case ObjectType.Well:
                        ReportWell well = (ReportWell)conn.TargetObject;
                        res = true;
                        break;
                    case ObjectType.Mer:
                        ReportMer mer = (ReportMer)conn.TargetObject;
                        switch (conn.TargetData.Type)
                        {
                            case DataObjectTypes.Mer_PlastCode:
                                if (mer.ReportByPlast)
                                {
                                    testVal = (int)item.PlastId;
                                    ConditionObjectDict dict = new ConditionObjectDict();
                                    dict.DictCode = ((MerCompPlastItem)mer.CurrentObject).PlastCode;
                                    dict.StratumDict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                                    val = dict;
                                }
                                break;
                            default:
                                throw new ArgumentException("Не поддерживаемый тип данных объекта!");
                        }
                        break;
                    case ObjectType.Perforation:
                        ReportPerforation perf = (ReportPerforation)conn.TargetObject;
                        double[] vals;
                        switch (conn.TargetData.Type)
                        {
                            case DataObjectTypes.Perf_IntervalMD:
                                vals = new double[2];
                                switch (conn.MainData.Type)
                                {
                                    case DataObjectTypes.Gis_IntervalMD:
                                        vals[0] = item.H;
                                        vals[1] = item.H + item.L;
                                        break;
                                    case DataObjectTypes.Gis_IntervalAbs:
                                        vals[0] = item.Habs;
                                        vals[1] = item.Habs + item.Labs;
                                        break;
                                }
                                testVal = vals;

                                vals = new double[2];
                                vals[0] = ((SkvPerfItem)perf.CurrentObject).Top;
                                vals[1] = ((SkvPerfItem)perf.CurrentObject).Bottom;
                                val = vals;
                                break;
                            default:
                                throw new ArgumentException("Не поддерживаемый тип данных объекта!");
                        }
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип объекта!");
                }
                if (val != null && testVal != null)
                {
                    cond = new ConditionObject(conn, val);
                    res = cond.TestValue(testVal);
                }
                if (!res) break;
            }
            return res;
        }
        #endregion

        #region Conditions
        public List<SkvGisItem> TestConditions(SkvGIS Gis)
        {
            List<SkvGisItem> items = new List<SkvGisItem>();
            for (int i = 0; i < Gis.Count; i++)
            {
                if (TestConditions(Gis.Items[i]))
                {
                    items.Add(Gis.Items[i]);
                }
            }
            return items;
        }
        bool TestConditions(SkvGisItem item)
        {
            bool res = true;
            if (ConditionDictRepeats != null)
            {
                List<int> indexes;
                bool res2 = false;
                for (int i = 0; i < ConditionDictRepeats.Count; i++)
                {
                    indexes = ConditionDictRepeats.GetConditionIndexes(i);
                    res2 = false;
                    for (int j = 0; (j < indexes.Count) && (!res2); j++)
                    {
                        res2 = TestCondition(item, Conditions[indexes[j]]) || res2;
                    }
                }
                res = res && res2;
            }
            for (int i = 0; i < Conditions.Count; i++)
            {
                if (!res) break;
                if (Conditions[i].Object.DataType != DataTypes.Dictionary)
                {
                    res = res && TestCondition(item, Conditions[i]);
                }
            }
            return res;
        }
        bool TestCondition(SkvGisItem item, ConditionObject Condition)
        {
            double[] values;
            switch (Condition.Object.Type)
            {
                case DataObjectTypes.Gis_Collector:
                    return Condition.TestValue(item.Collector);
                case DataObjectTypes.Gis_Lit:
                    return Condition.TestValue((int)item.LitId);
                case DataObjectTypes.Gis_PlastCode:
                    return Condition.TestValue((int)item.PlastId);
                case DataObjectTypes.Gis_Sat:
                    return Condition.TestValue((int)item.Sat0);
                case DataObjectTypes.Gis_Porosity:
                    return Condition.TestValue(item.Kpor);
                case DataObjectTypes.Gis_Permeability:
                    return Condition.TestValue(item.Kpr);
                case DataObjectTypes.Gis_OilSat:
                    return Condition.TestValue(item.Kn);
                case DataObjectTypes.Gis_GasSat:
                    return Condition.TestValue(item.Kgas);
                case DataObjectTypes.Gis_IntervalMD:

                    values = new double[2];
                    values[0] = item.H;
                    values[1] = item.H + item.L;
                    return Condition.TestValue(values);
                case DataObjectTypes.Gis_IntervalAbs:
                    values = new double[2];
                    values[0] = item.Habs;
                    values[1] = item.Habs + item.Labs;
                    return Condition.TestValue(values);
                default:
                    throw new ArgumentException("Не поддерживаемый тип данных.");
            }
        }
        #endregion

        #region Fill Table
        public override void FillRow(object Object, ReportFormCell[] Row, int ColumnIndex)
        {
            SkvGisItem item = (SkvGisItem)Object;
            int col = ColumnIndex;
            for (int j = 0; j < ReportDataObjects.Count; j++)
            {
                switch (ReportDataObjects[j].Type)
                {
                    case DataObjectTypes.Gis_Collector:
                        Row[col++].Value = item.Collector ? "Да" : "Нет";
                        break;
                    case DataObjectTypes.Gis_IntervalMD:
                        Row[col++].Value = string.Format("'{0};{1}", item.H, item.H + item.L);
                        break;
                    case DataObjectTypes.Gis_IntervalAbs:
                        Row[col++].Value = string.Format("'{0};{1}", item.Habs, item.Habs + item.Labs);
                        break;
                    case DataObjectTypes.Gis_PlastCode:
                        Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.PlastId);
                        break;
                    case DataObjectTypes.Gis_Lit:
                        Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.LitId);
                        break;
                    case DataObjectTypes.Gis_Sat:
                        Row[col++].Value = ReportDataObjects[j].GetDictionaryValue(item.Sat0);
                        break;
                    case DataObjectTypes.Gis_Porosity:
                        Row[col++].Value = item.Kpor;
                        break;
                    case DataObjectTypes.Gis_Permeability:
                        Row[col++].Value = item.Kpr;
                        break;
                    case DataObjectTypes.Gis_OilSat:
                        Row[col++].Value = item.Kn;
                        break;
                    case DataObjectTypes.Gis_GasSat:
                        Row[col++].Value = item.Kgas;
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип данных");
                }
            }
        }
        public override void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            if (Object != null)
            {
                ReportFormCell[] row;
                List<SkvGisItem> items = TestConditions((SkvGIS)Object);
                for (int i = 0; i < items.Count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    if (TestConnections(items[i]))
                    {
                        row = CloneRow(InitRow, ColumnIndex);
                        FillRow(items[i], row, ColumnIndex);
                        Table.Add(row);
                    }
                }
            }
        }
        public override int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnCount = 0, column;
            if (Object != null)
            {
                ReportFormCell[] row;
                List<SkvGisItem> items = TestConditions((SkvGIS)Object);
                columnCount += this.ReportDataObjects.Count;
                column = ColumnIndex + columnCount;
                for (int i = 0; i < items.Count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return 0;
                    }
                    if (TestConditions(items[i]) && TestConnections(items[i]))
                    {
                        row = CloneRow(InitRow, ColumnIndex);
                        FillRow(items[i], row, ColumnIndex);
                        this.CurrentObject = items[i];
                        columnCount += FillConnectedObjects(worker, e, Table, row, column);
                    }
                }
            }
            return columnCount;
        }
        int FillConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            for (int i = 0; i < ConnectedObjects.Count; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return 0;
                }
                Well w = (Well)this.ParentObject;
                switch (ConnectedObjects[i].Type)
                {
                    case ObjectType.Mer:
                        if (w.MerLoaded)
                        {
                            MerComp merComp = new MerComp(w.mer);
                            columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, merComp, InitRow, ColumnIndex + columnsCount);
                            merComp.Clear();
                        }
                        break;
                    case ObjectType.Perforation:
                        columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.perf, InitRow, ColumnIndex + columnsCount);
                        break;
                    case ObjectType.WellResearch:
                        columnsCount += ConnectedObjects[i].FillTable(worker, e, Table, w.research, InitRow, ColumnIndex + columnsCount);
                        break;
                }
            }
            return columnsCount;
        }
        #endregion
    }
}
