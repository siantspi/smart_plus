﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using SmartPlus.ReportBuilder.Objects;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.ReportBuilder.Base
{
    public sealed class ConditionObject
    {
        public bool Selected = false;
        public object Value;
        public DataObject Object;
        Condition Condition;
        public string Text;
        DataTypes ValueType;
        ConditionObjectDict dictObj = null;

        double NumberValue;
        double NumberValue2;
        string StringValue;
        bool BoolValue;
        
        public ConditionObject(DataObject Object, Condition Condition, DataTypes ValueType, object Value)
        {
            this.Object = Object;
            this.Condition = Condition;
            this.ValueType = ValueType;
            SetValue(Value);
        }
        public ConditionObject(Connection connection, object Value) : 
            this(connection.TargetData, connection.condition, connection.TargetData.DataType, Value)
        {
        }
        
        public void SetValue(object Value)
        {
            string valueText = string.Empty;
            this.Value = Value;

            if (Value != null)
            {
                switch (ValueType)
                {
                    case DataTypes.Bool:
                        BoolValue = (bool)Value;
                        valueText = (BoolValue ? "Да" : "Нет");
                        break;
                    case DataTypes.Date:
                        DateTime dt = (DateTime)Value;
                        NumberValue = dt.ToOADate();
                        if (dt.Year > 1900)
                        {
                            valueText = dt.ToShortDateString();
                        }
                        else
                        {
                            if (dt.Day == 1)
                            {
                                valueText = "Дата запуска";
                            }
                            else if (dt.Day == 2)
                            {
                                valueText = "Дата списка скважин";
                            }
                        }
                        if (dt.Year >= 1900 && dt.Millisecond > 0 && dt.Millisecond < 500)
                        {
                            valueText += string.Format(" ({0}{1}мес)", (dt.Second == 1) ? "-" : "+", dt.Millisecond);
                        }
                        break;
                    case DataTypes.Dictionary:
                        dictObj = (ConditionObjectDict)Value;
                        valueText = dictObj.Name;
                        break;
                    case DataTypes.Number:
                        NumberValue = (double)Value;
                        valueText = NumberValue.ToString();
                        break;
                    case DataTypes.NumberInterval:
                        double[] values = (double[])Value;
                        NumberValue = values[0];
                        NumberValue2 = values[1];
                        valueText = NumberValue.ToString() + ";" + NumberValue2.ToString();
                        break;
                    case DataTypes.String:
                        StringValue = (string)Value;
                        valueText = StringValue;
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип объекта!");
                }
                Text = string.Format("{0} - {1} - {2}", Object.Text, Condition.Text, valueText);
            }
        }
        
        public bool TestValue(object Value)
        {
            switch (Object.DataType)
            {
                case DataTypes.Bool:
                    return TestValue(Convert.ToBoolean(Value));
                case DataTypes.Date:
                    return TestValue(((DateTime)Value).Date.ToOADate());
                case DataTypes.Dictionary:
                    return TestDictVal(Convert.ToInt32(Value));
                case DataTypes.Number:
                    return TestValue(Convert.ToDouble(Value));
                case DataTypes.String:
                    return TestValue(Convert.ToString(Value));
                case DataTypes.NumberInterval:
                    double[] values = (double[])Value;
                    return TestInterval(values[0], values[1]);
                default:
                    throw new ArgumentException("Не поддерживаемый тип объекта!");
            }
        }
        bool TestValue(bool Value)
        {
            bool res = false;
            switch (Condition.Type)
            {
                case ConditionType.Equal:
                    res = (Value == (bool)BoolValue);
                    break;
                default:
                    throw new ArgumentException("Не поддерживаемый тип условия!");
            }
            return res; 
        }
        bool TestValue(double Value)
        {
            bool res = false;
            switch (Condition.Type)
            {
                case ConditionType.Equal:
                    res = (Value == NumberValue);
                    break;
                case ConditionType.NotEqual:
                    res = (Value != NumberValue);
                    break;
                case ConditionType.Greater:
                    res = (Value > NumberValue);
                    break;
                case ConditionType.GreatOrEqual:
                    res = (Value >= NumberValue);
                    break;
                case ConditionType.Less:
                    res = (Value < NumberValue);
                    break;
                case ConditionType.LessOrEqual:
                    res = (Value <= NumberValue);
                    break;
                default:
                    throw new ArgumentException("Не поддерживаемый тип условия!");
            }
            return res;
        }
        bool TestValue(string Value)
        {
            bool res = false;
            switch (Condition.Type)
            {
                case ConditionType.Equal:
                    res = (Value.Length == StringValue.Length) && (Value.ToUpper() == StringValue.ToUpper());
                    break;
                case ConditionType.NotEqual:
                    res = (Value.Length != StringValue.Length) || (Value.ToUpper() != StringValue.ToUpper());
                    break;
                case ConditionType.StartWith:
                    res = (Value.IndexOf(StringValue, StringComparison.OrdinalIgnoreCase) == 0);
                    break;
                case ConditionType.EndWith:
                    res = ((Value.Length >= StringValue.Length) ? Value.IndexOf(StringValue, Value.Length - StringValue.Length, StringComparison.OrdinalIgnoreCase) != -1 : false);
                    break;
                case ConditionType.Contain:
                    res = (Value.IndexOf(StringValue, StringComparison.OrdinalIgnoreCase) > -1);
                    break;
                default:
                    throw new ArgumentException("Не поддерживаемый тип условия!");
            }
            return res;
        }
        bool TestDictVal(int DictCode)
        {
             bool res = false;
             switch (Condition.Type)
             {
                 case ConditionType.Equal:
                     res = (DictCode == dictObj.DictCode);
                     break;
                 case ConditionType.NotEqual:
                     res = (DictCode != dictObj.DictCode);
                     break;
                 case ConditionType.EnterInto:
                     if ((dictObj != null) && (dictObj.StratumDict != null))
                     {
                         StratumTreeNode plastNode = dictObj.StratumDict.GetStratumTreeNode(DictCode);
                         if (plastNode != null)
                         {
                             res = (plastNode.GetParentLevelByCode(dictObj.DictCode) != -1);
                         }
                     }
                     break;
                 case ConditionType.NotEnterInto:
                     if ((dictObj != null) && (dictObj.StratumDict != null))
                     {
                         StratumTreeNode plastNode = dictObj.StratumDict.GetStratumTreeNode(DictCode);
                         if (plastNode != null)
                         {
                             res = (plastNode.GetParentLevelByCode(dictObj.DictCode) == -1);
                         }
                     }
                     break;
                 case ConditionType.Contain:
                     if ((dictObj != null) && (dictObj.StratumDict != null))
                     {
                         StratumTreeNode plastNode = dictObj.StratumDict.GetStratumTreeNode(dictObj.DictCode);
                         if (plastNode != null)
                         {
                             res = (plastNode.GetParentLevelByCode(DictCode) != -1);
                         }
                     }
                     break;
                 case ConditionType.NotContain:
                     if ((dictObj != null) && (dictObj.StratumDict != null))
                     {
                         StratumTreeNode plastNode = dictObj.StratumDict.GetStratumTreeNode(dictObj.DictCode);
                         if (plastNode != null)
                         {
                             res = (plastNode.GetParentLevelByCode(DictCode) == -1);
                         }
                     }
                     break;
                 default:
                     throw new ArgumentException("Не поддерживаемый тип условия!");
             }
             return res;
        }
        bool TestInterval(double Value1, double Value2)
        {
            bool res = false;
            switch (Condition.Type)
            {
                case ConditionType.Intersect:
                    return ((NumberValue <= Value2) && (Value1 <= NumberValue2));
                default:
                    throw new ArgumentException("Не поддерживаемый тип условия!");
            }
        }
    }

    public class ConditionObjectDict
    {
        public int DictCode;
        public bool ByFullName;
        public DictionaryBase Dict;
        public StratumDictionary StratumDict;

        public string Name
        {
            get
            {
                string result = string.Empty;
                if (Dict != null)
                {
                    result = ByFullName ? Dict.GetFullNameByCode(DictCode) : Dict.GetShortNameByCode(DictCode);
                }
                else if (StratumDict != null)
                {
                    StratumTreeNode node = StratumDict.GetStratumTreeNode(DictCode);
                    if (node != null) result = node.Name;
                }
                return result;
            }
        }
    }

    public class ConditionsRepeatDict
    {
        internal class RepeatDict
        {
            public DICTIONARY_ITEM_TYPE DictType;
            public List<int> Indexes;
            public RepeatDict(DICTIONARY_ITEM_TYPE DictType)
            {
                this.DictType = DictType;
                Indexes = new List<int>();
            }
            public int Count { get { return Indexes.Count; } }
            public int this[int index]
            {
                get
                {
                    return Indexes[index];
                }
                set
                {
                    Indexes[index] = value;
                }
            }
            public void Add(int Index) { Indexes.Add(Index); }
            public void RemoveAt(int index) { Indexes.RemoveAt(index); }
        }
        List<RepeatDict> Items;
        public int Count { get { return Items.Count; } }
        public ConditionsRepeatDict()
        {
            Items = new List<RepeatDict>();
        }
        public void AddIndex(DICTIONARY_ITEM_TYPE DictType, int ConditionIndex)
        {
            int index = GetIndex(DictType);
            if (index == -1)
            {
                RepeatDict rep = new RepeatDict(DictType);
                index = Items.Count;
                Items.Add(rep);
            }
            Items[index].Add(ConditionIndex);
        }
        public List<int> GetConditionIndexes(int index)
        {
            return Items[index].Indexes;;
        }
        public int GetIndex(DICTIONARY_ITEM_TYPE DictType)
        {
            int index = -1;
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i] != null) && (Items[i].DictType == DictType))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }
        public void RemoveCondition(int index)
        {
            int j;
            for (int i = 0; i < Items.Count; i++)
            {
                j = 0;
                while (j < Items[i].Count)
                {
                    if (index == Items[i][j])
                    {
                        Items[i].RemoveAt(j);
                        j--;
                    }
                    else if (index < Items[i][j])
                    {
                        Items[i][j]--;
                    }
                    j++;
                }
            } 
        }
    }
}
