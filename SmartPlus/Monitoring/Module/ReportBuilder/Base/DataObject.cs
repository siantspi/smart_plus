﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.ReportBuilder.Base
{
    public class DataObject
    {
        public string GroupName;
        public bool Selected = false;
        public DataObjectTypes Type;
        public DataTypes DataType;
        public string Text;

        public DICTIONARY_ITEM_TYPE DictType;
        public DictionaryBase Dictionary;
        public bool DictByFullName
        {
            get
            {
                switch (DictType)
                {
                    case DICTIONARY_ITEM_TYPE.STRATUM:
                        return false;
                    case DICTIONARY_ITEM_TYPE.OILFIELD_AREA:
                        return false;
                    case DICTIONARY_ITEM_TYPE.CHARWORK:
                        return false;
                    case DICTIONARY_ITEM_TYPE.METHOD:
                        return false;
                    case DICTIONARY_ITEM_TYPE.STATE:
                        return false;
                }
                return true;
            }
        }
        

        public DataObject(DataObjectTypes Type, DataTypes DataType) : this(Type, DataType, string.Empty) { }
        public DataObject(DataObjectTypes Type, DataTypes DataType, string GroupName)
        {
            this.GroupName = GroupName;
            this.Type = Type;
            this.DataType = DataType;
            this.Dictionary = null;
            DictType = DICTIONARY_ITEM_TYPE.NONE;
            switch (Type)
            {
                case DataObjectTypes.None:
                    Text = "-";
                    break;
                case DataObjectTypes.Region_Name:
                    Text = "Имя Региона";
                    break;
                case DataObjectTypes.NGDU_Name:
                    Text = "Имя НГДУ";
                    break;
                case DataObjectTypes.OilField_Name:
                    Text = "Имя месторождения";
                    break;
                case DataObjectTypes.Oilfield_Area:
                    DictType = DICTIONARY_ITEM_TYPE.OILFIELD_AREA;
                    Text = "Площадь";
                    break;
                case DataObjectTypes.Area_Name:
                    Text = "Номер ячейки заводнения";
                    break;
                case DataObjectTypes.Area_PlastCode:
                    DictType = DICTIONARY_ITEM_TYPE.STRATUM;
                    Text = "Пласт";
                    break;
                case DataObjectTypes.Well_Name:
                    Text = "Номер скважины";
                    break;
                case DataObjectTypes.Mer_Date:
                    Text = "Дата";
                    break;
                case DataObjectTypes.Perf_Date:
                    Text = "Дата";
                    break;
                case DataObjectTypes.GTM_Date:
                    Text = "Дата";
                    break;
                case DataObjectTypes.Repair_Date:
                    Text = "Дата";
                    break;
                case DataObjectTypes.Gis_Lit:
                    DictType = DICTIONARY_ITEM_TYPE.LITOLOGY;
                    Text = "Литология";
                    break;
                case DataObjectTypes.Gis_PlastCode:
                    DictType = DICTIONARY_ITEM_TYPE.STRATUM;
                    Text = "Пласт";
                    break;
                case DataObjectTypes.Gis_Collector:
                    Text = "Коллектор";
                    break;
                case DataObjectTypes.Gis_Sat:
                    DictType = DICTIONARY_ITEM_TYPE.SATURATION;
                    Text = "Насыщенность";
                    break;
                case DataObjectTypes.Gis_Porosity:
                    Text = "Пористость";
                    break;
                case DataObjectTypes.Gis_Permeability:
                    Text = "Проницаемость";
                    break;
                case DataObjectTypes.Gis_OilSat:
                    Text = "Нефтенасыщенность";
                    break;
                case DataObjectTypes.Gis_GasSat:
                    Text = "Газонасыщенность";
                    break;
                case DataObjectTypes.Perf_Type:
                    DictType = DICTIONARY_ITEM_TYPE.PERF_TYPE;
                    Text = "Тип перфорации";
                    break;
                case DataObjectTypes.GTM_Type:
                    DictType = DICTIONARY_ITEM_TYPE.GTM_TYPE;
                    Text = "Тип ГТМ";
                    break;
                case DataObjectTypes.Repair_Type:
                    DictType = DICTIONARY_ITEM_TYPE.WELL_ACTION_TYPE;
                    Text = "Тип ремонта";
                    break;
                case DataObjectTypes.OilField_WellCount:
                    Text = "Число скважин";
                    break;
                case DataObjectTypes.Mer_PlastCode:
                    DictType = DICTIONARY_ITEM_TYPE.STRATUM;
                    Text = "Пласт";
                    break;
                case DataObjectTypes.Mer_Charwork:
                    DictType = DICTIONARY_ITEM_TYPE.CHARWORK;
                    Text = "Характер работы";
                    break;
                case DataObjectTypes.Mer_Method:
                    DictType = DICTIONARY_ITEM_TYPE.METHOD;
                    Text = "Метод работы";
                    break;
                case DataObjectTypes.Mer_State:
                    DictType = DICTIONARY_ITEM_TYPE.STATE;
                    Text = "Состояние";
                    break;
                case DataObjectTypes.Mer_Liq:
                    Text = "Добыча жидкости, т";
                    break;
                case DataObjectTypes.Mer_Oil:
                    Text = "Добыча нефти, т";
                    break;
                case DataObjectTypes.Mer_LiqV:
                    Text = "Добыча жидкости, м3";
                    break;
                case DataObjectTypes.Mer_OilV:
                    Text = "Добыча нефти, м3";
                    break;
                case DataObjectTypes.Mer_Watering:
                    Text = "Обводненность вес, %";
                    break;
                case DataObjectTypes.Mer_Inj:
                    Text = "Закачка, м3";
                    break;
                case DataObjectTypes.Mer_WorkTime:
                    Text = "Время работы, ч";
                    break;
                case DataObjectTypes.Mer_Qliq:
                    Text = "Дебит жидкости, т/сут";
                    break;
                case DataObjectTypes.Mer_Qoil:
                    Text = "Дебит нефти, т/сут";
                    break;
                case DataObjectTypes.Mer_QliqV:
                    Text = "Дебит жидкости, м3/сут";
                    break;
                case DataObjectTypes.Mer_QoilV:
                    Text = "Дебит нефти, м3/сут";
                    break;
                case DataObjectTypes.Mer_Winj:
                    Text = "Приемистость, м3/сут";
                    break;
                case DataObjectTypes.Mer_Gas:
                    Text = "Добыча попутного газа, м3";
                    break;
                case DataObjectTypes.Gis_IntervalMD:
                    Text = "Интервал MD, м";
                    break;
                case DataObjectTypes.Gis_IntervalAbs:
                    Text = "Интервал Abs, м";
                    break;
                case DataObjectTypes.Perf_IntervalMD:
                    Text = "Интервал MD, м";
                    break;
                case DataObjectTypes.SumParams_Date:
                    Text = "Дата";
                    break;
                case DataObjectTypes.Sum_Params_PlastCode:
                    DictType = DICTIONARY_ITEM_TYPE.STRATUM;
                    Text = "Пласт";
                    break;
                case DataObjectTypes.Sum_Params_Liq:
                    Text = "Добыча жидкости, т";
                    break;
                case DataObjectTypes.Sum_Params_Oil:
                    Text = "Добыча нефти, т";
                    break;
                case DataObjectTypes.Sum_Params_Inj:
                    Text = "Закачка, м3";
                    break;
                case DataObjectTypes.Sum_Params_InjGas:
                    Text = "Закачка газа, м3";
                    break;
                case DataObjectTypes.Sum_Params_LiqV:
                    Text = "Добыча жидкости, м3";
                    break;
                case DataObjectTypes.Sum_Params_OilV:
                    Text = "Добыча нефти, м3";
                    break;
                case DataObjectTypes.Research_Date:
                    Text = "Дата";
                    break;
                case DataObjectTypes.Research_Code:
                    DictType = DICTIONARY_ITEM_TYPE.WELL_RESEARCH_TYPE;
                    Text = "Тип замера";
                    break;
                case DataObjectTypes.Research_PlastCode:
                    DictType = DICTIONARY_ITEM_TYPE.STRATUM;
                    Text = "Пласт";
                    break;
                case DataObjectTypes.Research_TimeDelay:
                    Text = "Время выдержки, ч";
                    break;
                case DataObjectTypes.Research_PressZatr:
                    Text = "Давление затрубное, атм";
                    break;
                case DataObjectTypes.Research_PressWaterSurface:
                    Text = "Давление на зеркало воды, атм";
                    break;
                case DataObjectTypes.Research_Press_Perforation:
                    Text = "Давление на верх.дыры перфорации, атм";
                    break;
                case DataObjectTypes.WellList_Name:
                    Text = "Имя списка скважин";
                    break;
                case DataObjectTypes.Mer_AccumLiquid:
                    Text = "Накопленная жидкость, т";
                    break;
                case DataObjectTypes.Mer_AccumOil:
                    Text = "Накопленная нефть, т";
                    break;
                case DataObjectTypes.Mer_AccumWater:
                    Text = "Накопленная вода, т";
                    break;
                case DataObjectTypes.Mer_AccumLiquidV:
                    Text = "Накопленная жидкость, м3";
                    break;
                case DataObjectTypes.Mer_AccumOilV:
                    Text = "Накопленная нефть, м3";
                    break;
                case DataObjectTypes.Mer_AccumWaterV:
                    Text = "Накопленная вода, м3";
                    break;
                case DataObjectTypes.Mer_AccumInjection:
                    Text = "Накопленная закачка, м3";
                    break;
                case DataObjectTypes.Mer_AccumInjectionGas:
                    Text = "Накопленная закачка газа, м3";
                    break;
                case DataObjectTypes.Sum_Params_AccumLiq:
                    Text = "Накопленная жидкость, т";
                    break;
                case DataObjectTypes.Sum_Params_AccumOil:
                    Text = "Накопленная нефть, т";
                    break;
                case DataObjectTypes.Sum_Params_Fprod:
                    Text = "Фонд добывающих скважин, шт";
                    break;
                case DataObjectTypes.Sum_Params_Finj:
                    Text = "Фонд нагнетательных скважин, шт";
                    break;
                case DataObjectTypes.Sum_Params_AccumWat:
                    Text = "Накопленная вода, т";
                    break;
                case DataObjectTypes.Sum_Params_AccumInj:
                    Text = "Накопленная закачка, м3";
                    break;
                case DataObjectTypes.Sum_Params_AccumInjGas:
                    Text = "Накопленная закачка газа, м3";
                    break;
                case DataObjectTypes.Sum_Params_AccumLiqV:
                    Text = "Накопленная жидкость, м3";
                    break;
                case DataObjectTypes.Sum_Params_AccumOilV:
                    Text = "Накопленная нефть, м3";
                    break;
                case DataObjectTypes.Sum_Params_AccumWatV:
                    Text = "Накопленная вода, м3";
                    break;
                default:
                    throw new ArgumentException("Не поддерживаемый тип условия");
            }
        }

        public bool IsConnectionPossible(DataObject Object)
        {
            if ((this.DataType == Object.DataType) && (Object.DataType != DataTypes.Number))
            {
                if ((this.DataType != DataTypes.Dictionary) || (this.DictType == Object.DictType))
                {
                    return true;
                }
            }
            return false;
        }
        public List<Condition> GetConditions()
        {
            List<Condition> result = new List<Condition>();
            switch (DataType)
            {
                case DataTypes.Bool:
                    result.Add(new Condition(ConditionType.Equal));
                    break;
                case DataTypes.Date:
                    result.Add(new Condition(ConditionType.Equal));
                    result.Add(new Condition(ConditionType.Greater));
                    result.Add(new Condition(ConditionType.GreatOrEqual));
                    result.Add(new Condition(ConditionType.Less));
                    result.Add(new Condition(ConditionType.LessOrEqual));
                    break;
                case DataTypes.String:
                    result.Add(new Condition(ConditionType.Equal));
                    result.Add(new Condition(ConditionType.NotEqual));
                    result.Add(new Condition(ConditionType.StartWith));
                    result.Add(new Condition(ConditionType.EndWith));
                    result.Add(new Condition(ConditionType.Contain));
                    break;
                case DataTypes.Number:
                    result.Add(new Condition(ConditionType.Equal));
                    result.Add(new Condition(ConditionType.NotEqual));
                    result.Add(new Condition(ConditionType.Greater));
                    result.Add(new Condition(ConditionType.GreatOrEqual));
                    result.Add(new Condition(ConditionType.Less));
                    result.Add(new Condition(ConditionType.LessOrEqual));
                    break;
                case DataTypes.NumberInterval:
                    result.Add(new Condition(ConditionType.Intersect));
                    break;
                case DataTypes.Dictionary:
                    result.Add(new Condition(ConditionType.Equal));
                    result.Add(new Condition(ConditionType.NotEqual));
                    if (DictType == DICTIONARY_ITEM_TYPE.STRATUM)
                    {
                        result.Add(new Condition(ConditionType.EnterInto));
                        result.Add(new Condition(ConditionType.NotEnterInto));
                        result.Add(new Condition(ConditionType.Contain));
                        result.Add(new Condition(ConditionType.NotContain));
                    }
                    break;
                case DataTypes.ProjectObject:
                    result.Add(new Condition(ConditionType.EnterInto));
                    result.Add(new Condition(ConditionType.NotEnterInto));
                    break;
            }
            return result;
        }
        public override string ToString()
        {
            return Text;
        }

        #region Dictionary
        public string GetDictionaryValue(int Code)
        {
            if (DictType != DICTIONARY_ITEM_TYPE.NONE)
            {
                return (this.DictByFullName) ? Dictionary.GetFullNameByCode(Code) : Dictionary.GetShortNameByCode(Code);
            }
            return string.Empty;
        }
        #endregion
    }

    public enum DataTypes : byte
    {
        Date,
        Bool,
        String,
        Number,
        NumberInterval,
        Dictionary,
        ProjectObject
    }

    public enum DataObjectTypes : byte
    {
        None,
        Region_Name,
        NGDU_Name,
        OilField_Name,
        OilField_WellCount,
        Well_Name,
        Oilfield_Area,
        Mer_Date,
        Mer_PlastCode,
        Mer_State,
        Mer_Charwork,
        Mer_Method,
        Mer_Liq,
        Mer_Oil,
        Mer_Watering,
        Mer_Inj,
        Mer_WorkTime,
        Mer_Qliq,
        Mer_Qoil,
        Mer_Winj,
        Gis_PlastCode,
        Gis_IntervalMD,
        Gis_IntervalAbs,
        Gis_Lit,
        Gis_Collector,
        Gis_Sat,
        Gis_Porosity,
        Gis_Permeability,
        Gis_OilSat,
        Gis_GasSat,
        Perf_IntervalMD,
        Perf_Date,
        Perf_Type,
        GTM_Type,
        GTM_Date,
        Repair_Type,
        Repair_Date,
        SumParams_Date,
        Sum_Params_PlastCode,
        Sum_Params_Liq,
        Sum_Params_Oil,
        Sum_Params_Inj,
        Sum_Params_InjGas,
        Sum_Params_LiqV,
        Sum_Params_OilV,
        Sum_Params_Fprod,
        Sum_Params_Finj,
        Sum_Params_AccumLiq,
        Sum_Params_AccumOil,
        Sum_Params_AccumWat,
        Sum_Params_AccumInj,
        Sum_Params_AccumInjGas,
        Sum_Params_AccumLiqV,
        Sum_Params_AccumOilV,
        Sum_Params_AccumWatV,
        Mer_LiqV,
        Mer_OilV,
        Mer_QliqV,
        Mer_QoilV,
        Research_Date,
        Research_PlastCode,
        Research_Code,
        Research_PressZatr,
        Research_PressWaterSurface,
        Research_Press_Perforation,
        Research_TimeDelay,
        WellList_Name,
        Mer_AccumLiquid,
        Mer_AccumOil,
        Mer_AccumWater,
        Mer_AccumLiquidV,
        Mer_AccumOilV,
        Mer_AccumWaterV,
        Mer_AccumInjection,
        Mer_AccumInjectionGas,
        Mer_Gas,
        Area_Name,
        Area_PlastCode
    }

    public enum ConditionSpecialDate : byte
    {
        None,
        MerStartWork,
        WellListDate
    }
}
