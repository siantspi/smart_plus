﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus.ReportBuilder.Base
{
    public class Condition
    {
        public string Text;
        ConditionType type;
        public ConditionType Type
        {
            get { return type; }
            set
            {
                switch (value)
                {
                    case ConditionType.Equal:
                        Text = "Равно";
                        break;
                    case ConditionType.NotEqual:
                        Text = "Не равно";
                        break;
                    case ConditionType.Greater:
                        Text = "Больше";
                        break;
                    case ConditionType.GreatOrEqual:
                        Text = "Больше или равно";
                        break;
                    case ConditionType.Less:
                        Text = "Меньше";
                        break;
                    case ConditionType.LessOrEqual:
                        Text = "Меньше или равно";
                        break;
                    case ConditionType.StartWith:
                        Text = "Начинается с...";
                        break;
                    case ConditionType.EndWith:
                        Text = "Заканчивается на...";
                        break;
                    case ConditionType.Contain:
                        Text = "Содержит";
                        break;
                    case ConditionType.NotContain:
                        Text = "Не содержит";
                        break;
                    case ConditionType.EnterInto:
                        Text = "Входит в...";
                        break;
                    case ConditionType.NotEnterInto:
                        Text = "Не входит в...";
                        break;
                    case ConditionType.Intersect:
                        Text = "Пересекается с...";
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип условия");
                }
                type = value;
            }
        }

        public Condition(ConditionType Type)
        {
            this.Type = Type;
        }
        public override string ToString()
        {
            return Text;
        }
    }

    public enum ConditionType : byte
    {
        Equal,
        NotEqual,
        Greater,
        GreatOrEqual,
        Less,
        LessOrEqual,
        StartWith,
        EndWith,
        Contain,
        NotContain,
        EnterInto,
        NotEnterInto,
        Intersect
    }
}
