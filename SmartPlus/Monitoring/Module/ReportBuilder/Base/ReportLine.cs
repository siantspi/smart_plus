﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SmartPlus.ReportBuilder.Base
{
    public class ReportLine : Base
    {
        Connection Connection;
        PointF[] Points;
        public void SetPoints(PointF Point1, PointF Point2)
        {
            Points = new PointF[2];
            Points[0].X = RealX(Point1.X);
            Points[0].Y = RealY(Point1.Y);
            Points[1].X = RealX(Point2.X);
            Points[1].Y = RealY(Point2.Y);
        }

        public ReportLine(Connection connection, PointF Point1, PointF Point2)
        {
            this.Connection = connection;
            connection.OnObjectBoundsChange += new OnObjectBoundsChangeDelegate(connection_OnObjectBoundsChange);
            SetPoints(Point1, Point2);
        }

        void connection_OnObjectBoundsChange(ReportObject sender)
        {
            int index = 0, index2 = Points.Length -1;
            ReportObject obj, obj2;
            
            if (Connection.MainObject == sender)
            {
                obj = Connection.MainObject;
                obj2 = Connection.TargetObject;
            }
            else
            {
                index = index2;
                index2 = 0;
                obj2 = Connection.MainObject;
                obj = Connection.TargetObject;
            }
            PointF pt = new PointF();
            pt.X = ScreenX(Points[index2].X);
            pt.Y = ScreenY(Points[index2].Y);
            Points[index] = obj.GetBoundPoint(pt);
            Points[index].X = RealX(Points[index].X);
            Points[index].Y = RealY(Points[index].Y);
            pt = new PointF();
            pt.X = ScreenX(Points[index].X);
            pt.Y = ScreenY(Points[index].Y);
            Points[index2] = obj2.GetBoundPoint(pt);
            Points[index2].X = RealX(Points[index2].X);
            Points[index2].Y = RealY(Points[index2].Y);
        }

        public bool ContainConnection(Connection conn)
        {
            return Connection == conn;
        }

        public override bool PointInBounds(Point Point)
        {
            bool result = false;
            if(Points != null)
            {
                for(int i = 0; i < Points.Length - 1; i++)
                {
                    result = Geometry.IsPointInLine(Points[i], Points[i + 1], Point);
                    if (result) return result;
                }
            }
            return result;
        }
        public override void Draw(Graphics grfx)
        {
            float x = 0, y = 0, x2, y2;
            for (int i = 0; i < Points.Length - 1; i++)
            {
                if (i == 0)
                {
                    x = ScreenX(Points[i].X);
                    y = ScreenY(Points[i].Y);
                }
                x2 = ScreenX(Points[i + 1].X);
                y2 = ScreenY(Points[i + 1].Y);
                grfx.DrawLine(PenBounds, x, y, x2, y2);
                x = x2; y = y2;
            }
        }
    }
}