﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;
using SmartPlus.ReportBuilder.UI;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.ReportBuilder.Base
{
    public abstract class Base
    {
        public DictionaryCollection DictList = null;
        public bool Selected = false;
        public static Rectangle ClientRectangle;
        public static PointF HotSpot;
        public static float FontSize;
        public static float DPI;
        public static Pen PenBounds;
        public static Brush BrushBack;
        public static Font FontName;
        public static Font FontCondition, FontConditionBold;
        public static Font FontReportData, FontReportDataBold;

        public static float ScreenX(float RealX) { return -HotSpot.X + RealX / DPI; }
        public static float ScreenY(float RealY) { return -HotSpot.Y + RealY / DPI; }
        public static float RealX(float ScreenX) { return (HotSpot.X + ScreenX) * DPI; }
        public static float RealY(float ScreenY) { return (HotSpot.Y + ScreenY) * DPI; }
        public abstract void Draw(Graphics grfx);
        public abstract bool PointInBounds(Point Point);
        static Base()
        {
            HotSpot = PointF.Empty;
            DPI = 1;
            FontSize = 10;
            FontName = new Font("Tahoma", 10F, FontStyle.Bold);
            FontCondition = new Font("Calibri", 8F);
            FontReportData = new Font("Calibri", 8F);
            FontConditionBold = new Font("Calibri", 8F, FontStyle.Bold);
            FontReportDataBold = new Font("Calibri", 8F, FontStyle.Bold);
            BrushBack = new SolidBrush(Color.FromArgb(240, 240, 240));
            PenBounds = new Pen(Color.FromArgb(40, 40, 40), 2);
            PenBounds.LineJoin = LineJoin.Round;
        }
    }

    public delegate void OnBoundsChangeDelegate();
    public delegate void OnNeedPaintDelegate(int IndexObject);
    public delegate void OnFiledChangeDelegate();

    public class ReportObject : Base
    {
        public int Index;
        public object ParentObject;
        public object CurrentObject;
        public int NameIndex;
        public ObjectType Type;
        public string Name;
        
        int MarkerSelectedIndex;
        int BoundsArcRadius = 14;
        public RectangleF Bounds;
        public float X
        {
            get { return Bounds.X; }
            set { Bounds.X = value; }
        }
        public float Y
        {
            get { return Bounds.Y; }
            set { Bounds.Y = value; }
        }
        public float Width
        {
            get { return Bounds.Width; }
            set { Bounds.Width = value; }
        }
        public float Height
        {
            get { return Bounds.Height; }
            set { Bounds.Height = value; }
        }
        float YCondition, YReport;
        float CondMaxHeight, CondMinHeight;
        float ReportMaxHeight, ReportMinHeight;
        public bool BuildConnectionMode, BuildConnectionModeStart;
        public PointF ConnectionPoint;
        public bool ExpandedCondition, ExpandedReport;
        Bitmap markerCircle, markerRect, ConditionButton, ReportButton, CondPlusButton, ReportPlusButton, LinkButton;
        public ConditionsRepeatDict ConditionDictRepeats = null;

        public bool IsCondBtnOver, IsReportBtnOver;
        private bool canAddCondition; 
        public bool CanAddCondition
        {
            get { return canAddCondition; }
            set 
            {
                canAddCondition = value;
                CondPlusButton = (value) ? Properties.Resources.Add_Green16 : Properties.Resources.Add_Gray16;
            }
        }
        public bool IsCondPlusBtnOver, IsReportPlusBtnOver, IsCondLinkBtnOver;
        
        public event OnBoundsChangeDelegate OnBoundsChange;
        public event OnFiledChangeDelegate OnFieldsChange;
        public List<DataObject> DataObjects;
        
        public List<Connection> Connections;
        public List<ConditionObject> Conditions;
        public List<DataObject> ReportDataObjects;

        public List<ReportObject> ConnectedObjects;
        public event OnNeedPaintDelegate OnNeedPaint;
        
        public ReportObject(int Index)
        {
            this.Index = Index;
            NameIndex = 0;
            markerCircle = SmartPlus.Properties.Resources.MarkerCircle16;
            markerRect = SmartPlus.Properties.Resources.MarkerRect16;
            ConditionButton = SmartPlus.Properties.Resources.up;
            ReportButton = SmartPlus.Properties.Resources.up;
            CondPlusButton = SmartPlus.Properties.Resources.Add_Green16;
            ReportPlusButton = SmartPlus.Properties.Resources.Add_Green16;
            LinkButton = SmartPlus.Properties.Resources.Link24;
            BuildConnectionModeStart = false;
            BuildConnectionMode = false;
            IsCondBtnOver = false;
            IsReportBtnOver = false;
            CanAddCondition = true;
            DataObjects = new List<DataObject>();
            Conditions = new List<ConditionObject>();
            Connections = new List<Connection>();
            ReportDataObjects = new List<DataObject>();
            ConnectedObjects = new List<ReportObject>();
            MarkerSelectedIndex = -1;
            ExpandedCondition = true;
            ExpandedReport = true;
            ConnectionPoint.X = this.X;
            ConnectionPoint.Y = this.Y;
        }

        public void ReportBuilder_MouseUp(object sender, MouseEventArgs e)
        {
            MarkerSelectedIndex = -1;
        }
        public void ReportBuilder_MouseDown(object sender, MouseEventArgs e)
        {
            if (Selected)
            {
                SelectMarker(e.Location);
            }
            TestConditionButtonClick(e.Location);
            TestReportButtonClick();
        }
        public void ReportObject_MouseMove(object sender, MouseEventArgs e)
        {
            TestConditionButtonsOver(e.Location);
            TestReportButtonsOver(e.Location);
            SetConnectionPoint(e.Location);
        }

        #region FILL TABLE
        public ReportFormCell[] CloneRow(ReportFormCell[] InitRow, int ColumnIndex)
        {
            ReportFormCell[] row = new ReportFormCell[InitRow.Length];
            for (int i = 0; i < InitRow.Length; i++)
            {
                if (i < ColumnIndex)
                {
                    row[i] = InitRow[i];
                }
                else
                {
                    row[i] = new ReportFormCell();
                }
            }
            return row;
        }
        public int FillTable(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            int columnsCount = 0;
            if (ConnectedObjects.Count == 0)
            {
                int rowCount = Table.Count;
                FillTableByObject(worker, e, Table, Object, InitRow, ColumnIndex);
                
                if (rowCount == Table.Count)
                {
                    bool allProjObj = true;
                    for (int i = 0; i < Connections.Count; i++)
                    {
                        allProjObj = allProjObj && Connections[i].TargetObject.IsProjectObject;
                    }
                    if (!allProjObj)
                    {
                        ReportFormCell[] row = CloneRow(InitRow, ColumnIndex);
                        Table.Add(row);
                    }
                }
                columnsCount += this.ReportDataObjects.Count;
            }
            else
            {
                columnsCount += FillTableByConnectedObjects(worker, e, Table, Object, InitRow, ColumnIndex);
            }
            return columnsCount;
        }
        public virtual void FillRow(object Object, ReportFormCell[] Row, int ColumnIndex)
        {
            throw new NotImplementedException("Функция заполнения строки не реализована");
        }
        public virtual void FillTableByObject(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            throw new NotImplementedException("Функция заполнения таблицы не реализована");
        }
        public virtual int FillTableByConnectedObjects(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportFormCell[]> Table, object Object, ReportFormCell[] InitRow, int ColumnIndex)
        {
            throw new NotImplementedException("Функция заполнения таблицы зависимых объектов не реализована");
        }
        #endregion

        #region Connections
        public bool IsCompatibilityObject(ReportObject Object)
        {
            bool result = false;
            switch (this.Type)
            {
                case ObjectType.Region:
                    goto case ObjectType.NGDU;
                case ObjectType.NGDU:
                    result = Object.Type == ObjectType.OilField;
                    break;
                case ObjectType.OilField:
                    result = (Object.Type == ObjectType.Well) ||
                             (Object.Type == ObjectType.Area) ||
                             (Object.Type == ObjectType.SumParameters);
                    break;
                case ObjectType.Area:
                    result = Object.Type == ObjectType.Well;
                    break;
                case ObjectType.WellList:
                    result = Object.Type == ObjectType.Well;
                    break;
                case ObjectType.Contour:
                    result = Object.Type == ObjectType.Well;
                    break;
                case ObjectType.Well:
                    result = (Object.Type == ObjectType.Chess) ||
                             (Object.Type == ObjectType.Coord) ||
                             (Object.Type == ObjectType.Gis) ||
                             (Object.Type == ObjectType.Mer) ||
                             (Object.Type == ObjectType.Perforation) ||
                             (Object.Type == ObjectType.GTM) ||
                             (Object.Type == ObjectType.RepairAction) ||
                             (Object.Type == ObjectType.WellResearch) ||
                             (Object.Type == ObjectType.Logs);
                    break;
                case ObjectType.Mer:
                    result = (Object.Type == ObjectType.Chess) ||
                             (Object.Type == ObjectType.Gis) ||
                             (Object.Type == ObjectType.Perforation) ||
                             (Object.Type == ObjectType.WellResearch) ||
                             (Object.Type == ObjectType.RepairAction) ||
                             (Object.Type == ObjectType.GTM);
                    break;
                case ObjectType.Gis:
                    result = (Object.Type == ObjectType.Perforation) ||
                             (Object.Type == ObjectType.WellResearch) ||
                             (Object.Type == ObjectType.Mer);
                    break;
                case ObjectType.Perforation:
                    result = (Object.Type == ObjectType.Gis) || 
                             (Object.Type == ObjectType.Mer);
                    break;
                case ObjectType.SumParameters:
                    result = false;
                    break;
                case ObjectType.GTM:
                    result = (Object.Type == ObjectType.Mer);
                    break;
                case ObjectType.RepairAction:
                    result = (Object.Type == ObjectType.Mer);
                    break;
                case ObjectType.WellResearch:
                    result = (Object.Type == ObjectType.Mer);
                    break;
                default:
                    throw new ArgumentException("Неизвестный тип данных объекта!");
            }
            return result;
        }
        public virtual bool TestConnection(int Index)
        {
            throw new NotImplementedException("Функция проверки связи объектов не реализована");
        }
        public void RemoveConnection(Connection item)
        {
            if (item != null)
            {
                Connections.Remove(item);
                OnBoundsChange();
                OnNeedPaint(Index);
            }
        }
        public bool TestConnectedObj(ObjectType Type)
        {
            bool result = (this.Type == Type);
            if (result) return result;
            for (int i = 0; i < ConnectedObjects.Count; i++)
            {
                result = ConnectedObjects[i].TestConnectedObj(Type);
                if (result) return result;
            }
            return false;
        }
        #endregion

        #region Conditions
        public List<ConditionObject> GetConditionsByType(DataObjectTypes Type)
        {
            List<ConditionObject> list = new List<ConditionObject>();
            for (int i = 0; i < Conditions.Count; i++)
            {
                if (Conditions[i].Object.Type == Type)
                {
                    list.Add(Conditions[i]);
                }
            }
            return list;
        }
        public List<DataObject> GetReportObjectsByType(DataObjectTypes Type)
        {
            List<DataObject> list = new List<DataObject>();
            for (int i = 0; i < ReportDataObjects.Count; i++)
            {
                if (ReportDataObjects[i].Type == Type)
                {
                    list.Add(ReportDataObjects[i]);
                }
            }
            return list;
        }
        public void AddCondition(ConditionObject item)
        {
            if(item.Object.DataType == DataTypes.Dictionary)
            {
                if (ConditionDictRepeats == null)
                {
                    ConditionDictRepeats = new ConditionsRepeatDict();
                }
                ConditionDictRepeats.AddIndex(item.Object.DictType, Conditions.Count);
            }
            this.Conditions.Add(item);
            if (OnFieldsChange != null) OnFieldsChange();
        }
        public void RemoveCondition(ConditionObject item)
        {
            if (item != null)
            {
                int index = Conditions.IndexOf(item);
                if (ConditionDictRepeats != null)
                {
                    ConditionDictRepeats.RemoveCondition(index);
                }
                Conditions.RemoveAt(index);
                if (OnFieldsChange != null) OnFieldsChange();
                if (OnBoundsChange != null) OnBoundsChange();
                if (Conditions.Count == 0) ConditionDictRepeats = null;
            }
        }
        #endregion

        #region ReportDataObject
        public void AddReportDataObjects(DataObject item)
        {
            if (item != null)
            {
                ReportDataObjects.Add(item);
                if (OnFieldsChange != null) OnFieldsChange();
            }
        }
        public void RemoveReportDataObject(DataObject item)
        {
            if (item != null)
            {
                ReportDataObjects.Remove(item);
                if (OnBoundsChange != null) OnBoundsChange();
                if (OnNeedPaint != null) OnNeedPaint(Index);
                if (OnFieldsChange != null) OnFieldsChange();
            }
        }
        public bool TestAccumReportDataObject()
        {
            for (int i = 0; i < ReportDataObjects.Count; i++)
            {
                if (ReportDataObjects[i].GroupName.Length > 0) return true;
            }
            return false;
        }
        #endregion

        #region Common
        public override bool PointInBounds(Point Point)
        {
            float x = Base.ScreenX(X) - 3, y = Base.ScreenY(Y) - 3, width = Width / DPI + 10, height = Height / DPI + 10;
            if ((x <= Point.X) && (Point.X <= x + width) && (y < Point.Y) && (Point.Y < y + height))
            {
                return true;
            }
            return false;
        }
        public virtual void ResetGraphicsSizes(Graphics grfx)
        {
            SizeF size = grfx.MeasureString(Name, FontName, PointF.Empty, StringFormat.GenericDefault);
            SizeF sizeCond = grfx.MeasureString(Name, FontCondition, PointF.Empty, StringFormat.GenericDefault);
            SizeF sizeReport = grfx.MeasureString(Name, FontReportData, PointF.Empty, StringFormat.GenericDefault);
            float MaxWidth = this.Width / DPI;
            if (MaxWidth < size.Width) MaxWidth = size.Width + BoundsArcRadius;
            if (this.Height / DPI < size.Height) this.Height = size.Height;
            
            CondMinHeight = size.Height + 6;

            for (int i = 0; i < Connections.Count; i++)
            {
                sizeCond = grfx.MeasureString(Connections[i].Text, FontCondition, PointF.Empty, StringFormat.GenericDefault);
                if (MaxWidth < sizeCond.Width + BoundsArcRadius) MaxWidth = sizeCond.Width + BoundsArcRadius;
            }
            for (int i = 0; i < Conditions.Count; i++)
            {
                sizeCond = grfx.MeasureString(Conditions[i].Text, FontCondition, PointF.Empty, StringFormat.GenericDefault);
                if (MaxWidth < sizeCond.Width + BoundsArcRadius) MaxWidth = sizeCond.Width + BoundsArcRadius;
            }
            CondMaxHeight = Connections.Count * sizeCond.Height + Conditions.Count * sizeCond.Height + 20;
            ReportMinHeight = size.Height + 8;
            ReportMaxHeight = ReportDataObjects.Count * sizeReport.Height + 20;

            YCondition = size.Height + BoundsArcRadius / 2;
            for (int i = 0; i < ReportDataObjects.Count; i++)
            {
                sizeCond = grfx.MeasureString(ReportDataObjects[i].Text, FontReportData, PointF.Empty, StringFormat.GenericDefault);
                if (MaxWidth < sizeCond.Width + BoundsArcRadius) MaxWidth = sizeCond.Width + BoundsArcRadius;
            }
            ResetSizes();
            if (this.Width < MaxWidth) this.Width = MaxWidth;
            if(OnBoundsChange != null) OnBoundsChange();
        }
        void ResetSizes()
        {
            YReport = YCondition + CondMinHeight;
            if (ExpandedCondition) YReport += CondMaxHeight;

            this.Height = YReport + CondMinHeight + BoundsArcRadius / 2;
            if (ExpandedReport) this.Height += ReportMaxHeight;
        }
        #endregion

        #region Markers
        private void SelectMarker(Point Point)
        {
            float x = Base.ScreenX(X) - 3, y = Base.ScreenY(Y) - 3, width = Width / DPI + 10, height = Height / DPI + 10;
        }
        #endregion

        #region ConnectionMode
        public PointF GetBoundPoint(PointF CursorPosition)
        {
            PointF point = new PointF();
            float width = Width / DPI, height = Height / DPI;
            float x1 = ScreenX(X), y1 = ScreenY(Y), x2 = x1 + width, y2 = y1 + height;
            float y3;
            int AreaIndex;
            y3 = (y1 - y2) * (CursorPosition.X - x2) / (x1 - x2) + y2;
            AreaIndex = (CursorPosition.Y < y3) ? 0 : 2;
            y3 = (y2 - y1) * (CursorPosition.X - x2) / (x1 - x2) + y1;
            AreaIndex = (CursorPosition.Y < y3) ? AreaIndex : AreaIndex + 1;
            switch (AreaIndex)
            {
                case 0:
                    point.X = x1 + width / 2;
                    point.Y = y1;
                    break;
                case 1:
                    point.X = x2;
                    point.Y = y1 + height / 2;
                    break;
                case 2:
                    point.X = x1;
                    point.Y = y1 + height / 2;
                    break;
                case 3:
                    point.X = x1 + width / 2;
                    point.Y = y2;
                    break;
            }
            return point;
        }
        void SetConnectionPoint(Point CursorPosition)
        {
            if (BuildConnectionMode)
            {
                ConnectionPoint = GetBoundPoint(CursorPosition);
            }
        }
        #endregion

        #region Conditions Region
        void TestConditionButtonClick(Point CursorLocation)
        {
            BuildConnectionModeStart = false;
            if (IsCondBtnOver)
            {
                ConditionButton = (ExpandedCondition ? SmartPlus.Properties.Resources.down : SmartPlus.Properties.Resources.up);
                ExpandedCondition = !ExpandedCondition;
                ResetSizes();
                OnNeedPaint(Index);
                if(OnBoundsChange != null) OnBoundsChange();
            }
            else if ((IsCondLinkBtnOver) && (ExpandedCondition))
            {
                BuildConnectionModeStart = true;
            }
            else
            {
                TestSelectCondition(CursorLocation);
                TestSelectReportObject(CursorLocation);
            }
        }
        void TestConditionButtonsOver(Point CursorLocation)
        {
            float x = ScreenX(X), y = ScreenY(Y), width = Width / DPI, height = Height / DPI;
            Rectangle Rect = new Rectangle((int)(x + width - 24), (int)(y + YReport - 22), 20, 20);
            if (Rect.Contains(CursorLocation) && !IsCondBtnOver)
            {
                ConditionButton = (ExpandedCondition ? SmartPlus.Properties.Resources.up_over : SmartPlus.Properties.Resources.down_over);
                IsCondBtnOver = true;
                OnNeedPaint(Index);
            }
            else if (!Rect.Contains(CursorLocation) && IsCondBtnOver)
            {
                ConditionButton = (ExpandedCondition ? SmartPlus.Properties.Resources.up : SmartPlus.Properties.Resources.down);
                IsCondBtnOver = false;
                OnNeedPaint(Index);
            }
            if (ExpandedCondition)
            {
                Rect = new Rectangle((int)(x + BoundsArcRadius / 2), (int)(y + YReport - 18), 16, 16);
                if (CanAddCondition)
                {
                    if (Rect.Contains(CursorLocation) && !IsCondPlusBtnOver)
                    {
                        CondPlusButton = SmartPlus.Properties.Resources.Add_Blue16;
                        IsCondPlusBtnOver = true;
                        OnNeedPaint(Index);
                    }
                    else if (!Rect.Contains(CursorLocation) && IsCondPlusBtnOver)
                    {
                        CondPlusButton = SmartPlus.Properties.Resources.Add_Green16;
                        IsCondPlusBtnOver = false;
                        OnNeedPaint(Index);
                    }
                }
                else
                {
                    CondPlusButton = SmartPlus.Properties.Resources.Add_Gray16;
                }
                Rectangle Rect2 = new Rectangle((int)(x + 20 + BoundsArcRadius / 2), (int)(y + YReport - 22), 20, 20);
                if (Rect2.Contains(CursorLocation) && !IsCondLinkBtnOver)
                {
                    LinkButton = SmartPlus.Properties.Resources.LinkBlue24;
                    IsCondLinkBtnOver = true;
                    OnNeedPaint(Index);
                }
                else if (!Rect2.Contains(CursorLocation) && IsCondLinkBtnOver)
                {
                    LinkButton = SmartPlus.Properties.Resources.Link24;
                    IsCondLinkBtnOver = false;
                    OnNeedPaint(Index);
                }
            }
        }
        void TestSelectCondition(Point CursorLocation)
        {
            if ((this.Selected) && (ExpandedCondition))
            {
                int ind = GetConnectionByPoint(CursorLocation);
                if (ind != -1) Connections[ind].Selected = true;
                ind = GetConditionByPoint(CursorLocation);
                if (ind != -1) Conditions[ind].Selected = true;
                OnNeedPaint(Index);
            }
        }
        int GetConnectionByPoint(Point CursorLocation)
        {
            int result = -1;
            if (Connections.Count > 0)
            {
                float x = ScreenX(X), y = ScreenY(Y), width = Width / DPI, height = Height / DPI;

                float rowHeight = (this.CondMaxHeight - 20) / (Connections.Count + Conditions.Count);
                if (rowHeight > 0)
                {
                    RectangleF rect = new RectangleF(x, y + YCondition + rowHeight, width, rowHeight);
                    for (int i = 0; i < Connections.Count; i++)
                    {
                        Connections[i].Selected = false;
                        if (rect.Contains(CursorLocation))
                        {
                            result = i;
                        }
                        rect.Y += rowHeight;
                    }
                }
            }
            return result;
        }
        int GetConditionByPoint(Point CursorLocation)
        {
            int result = -1;
            if (Conditions.Count > 0)
            {
                float x = ScreenX(X), y = ScreenY(Y), width = Width / DPI, height = Height / DPI;

                float rowHeight = (this.CondMaxHeight - 20) / (Connections.Count + Conditions.Count);
                if (rowHeight > 0)
                {
                    RectangleF rect = new RectangleF(x, y + YCondition + rowHeight * (Connections.Count + 1), width, rowHeight);
                    for (int i = 0; i < Conditions.Count; i++)
                    {
                        Conditions[i].Selected = false;
                        if (rect.Contains(CursorLocation))
                        {
                            result = i;
                        }
                        rect.Y += rowHeight;
                    }
                }
            }
            return result;
        }
        private void DrawConditions(Graphics grfx, float x, float y, float width, float height)
        {
            string Name = "Условия";
            if (Conditions.Count + Connections.Count > 0) Name += string.Format(" [{0}]", Conditions.Count + Connections.Count);
            grfx.DrawString(Name, FontName, Brushes.Black, x + BoundsArcRadius / 2, y + YCondition + 2, StringFormat.GenericDefault);
            grfx.DrawImageUnscaled(ConditionButton, (int)(x + width - 24 - 2), (int)(y + YReport - 22));
            SizeF size = grfx.MeasureString("ЙЩ", FontConditionBold, PointF.Empty, StringFormat.GenericDefault);
            SizeF size1, size2;
            if (ExpandedCondition)
            {
                Font font;
                float x2 = x + BoundsArcRadius / 2;
                float y2 = y + YCondition + 2 + size.Height;
                float dx;
                for (int i = 0; i < Connections.Count; i++)
                {
                    font = Connections[i].Selected ? FontConditionBold : FontCondition;
                    dx = 0;
                    if (Connections[i].Selected)
                    {
                        size1 = grfx.MeasureString(Connections[i].Text, FontConditionBold, PointF.Empty, StringFormat.GenericDefault);
                        size2 = grfx.MeasureString(Connections[i].Text, FontCondition, PointF.Empty, StringFormat.GenericDefault);
                        dx = (size2.Width - size1.Width) / 2;
                        grfx.FillRectangle(Brushes.LightGray, x, y2, width, size.Height);
                    }
                    grfx.DrawString(Connections[i].Text, font, Brushes.Black, x2 + dx, y2, StringFormat.GenericDefault);
                    y2 += size.Height;
                }
                for (int i = 0; i < Conditions.Count; i++)
                {
                    font = Conditions[i].Selected ? FontConditionBold : FontCondition;
                    dx = 0;
                    if (Conditions[i].Selected)
                    {
                        size1 = grfx.MeasureString(Conditions[i].Text, FontConditionBold, PointF.Empty, StringFormat.GenericDefault);
                        size2 = grfx.MeasureString(Conditions[i].Text, FontCondition, PointF.Empty, StringFormat.GenericDefault);
                        dx = (size2.Width - size1.Width) / 2;
                        grfx.FillRectangle(Brushes.LightGray, x, y2, width, size.Height);
                    }
                    grfx.DrawString(Conditions[i].Text, font, Brushes.Black, x2 + dx, y2, StringFormat.GenericDefault);
                    y2 += size.Height;
                }
                grfx.DrawImageUnscaled(CondPlusButton, (int)(x + BoundsArcRadius / 2), (int)(y + YReport - 18));
                grfx.DrawImageUnscaled(LinkButton, (int)(x + 20 + BoundsArcRadius / 2), (int)(y + YReport - 22));
            }
            grfx.DrawLine(PenBounds, x, y + YReport, x + width, y + YReport);
        }
        #endregion

        #region Report Region
        void TestReportButtonClick()
        {
            if (IsReportBtnOver)
            {
                ReportButton = (ExpandedReport ? SmartPlus.Properties.Resources.down : SmartPlus.Properties.Resources.up);
                ExpandedReport = !ExpandedReport;
                ResetSizes();
                OnNeedPaint(Index);
                if (OnBoundsChange != null) OnBoundsChange();
            }
        }
        int GetReportObjectByPoint(Point CursorLocation)
        {
            int result = -1;
            if (ReportDataObjects.Count > 0)
            {
                float x = ScreenX(X), y = ScreenY(Y), width = Width / DPI, height = Height / DPI;

                float rowHeight = (this.ReportMaxHeight - 20) / ReportDataObjects.Count;
                if (rowHeight > 0)
                {
                    RectangleF rect = new RectangleF(x, y + YReport + rowHeight, width, rowHeight);
                    for (int i = 0; i < ReportDataObjects.Count; i++)
                    {
                        ReportDataObjects[i].Selected = false;
                        if (rect.Contains(CursorLocation))
                        {
                            result = i;
                        }
                        rect.Y += rowHeight;
                    }
                }
            }
            return result;
        }
        void TestReportButtonsOver(Point CursorLocation)
        {
            float x = ScreenX(X), y = ScreenY(Y), width = Width / DPI, height = Height / DPI;
            float y2 = (ExpandedReport ? (y + height - 24) : (y + YReport + 2));
            Rectangle Rect = new Rectangle((int)(x + width - 24), (int)y2, 20, 20);
            bool NeedPaint = false;
            if (Rect.Contains(CursorLocation) && !IsReportBtnOver)
            {
                ReportButton = (ExpandedReport ? SmartPlus.Properties.Resources.up_over : SmartPlus.Properties.Resources.down_over);
                IsReportBtnOver = true;
                NeedPaint = true;
            }
            else if (!Rect.Contains(CursorLocation) && IsReportBtnOver)
            {
                ReportButton = (ExpandedReport ? SmartPlus.Properties.Resources.up : SmartPlus.Properties.Resources.down);
                IsReportBtnOver = false;
                NeedPaint = true;
            }
            if (ExpandedReport)
            {
                y2 = (ExpandedReport ? (y + height - BoundsArcRadius / 2 - 18) : (y + YReport + 2));
                Rect = new Rectangle((int)(x + BoundsArcRadius / 2), (int)y2, 16, 16);
                if (Rect.Contains(CursorLocation) && !IsReportPlusBtnOver)
                {
                    ReportPlusButton = SmartPlus.Properties.Resources.Add_Blue16;
                    IsReportPlusBtnOver = true;
                    NeedPaint = true;
                }
                else if (!Rect.Contains(CursorLocation) && IsReportPlusBtnOver)
                {
                    ReportPlusButton = SmartPlus.Properties.Resources.Add_Green16;
                    IsReportPlusBtnOver = false;
                    NeedPaint = true;
                }
            }
            if (NeedPaint) OnNeedPaint(Index);
        }
        void TestSelectReportObject(Point CursorLocation)
        {
            if ((this.Selected) && (ExpandedCondition))
            {
                int ind = GetReportObjectByPoint(CursorLocation);
                if (ind != -1) ReportDataObjects[ind].Selected = true;
                OnNeedPaint(Index);
            }
        }
        private void DrawReportObjects(Graphics grfx, float x, float y, float width, float height)
        {
            string Name = "Вывод";
            if (ReportDataObjects.Count > 0) Name += string.Format(" [{0}]", ReportDataObjects.Count);
            grfx.DrawString(Name, FontName, Brushes.DarkGray, x + BoundsArcRadius / 2, y + YReport + 2, StringFormat.GenericDefault);
            SizeF size = grfx.MeasureString("ЙЩ", FontCondition, PointF.Empty, StringFormat.GenericDefault);
            SizeF size1, size2;
            if (ExpandedReport)
            {
                Font font;
                float x2 = x + BoundsArcRadius / 2;
                float y2 = y + YReport + 2 + size.Height;
                float dx;
                for (int i = 0; i < ReportDataObjects.Count; i++)
                {
                    font = ReportDataObjects[i].Selected ? FontReportDataBold : FontReportData;
                    dx = 0;
                    if (ReportDataObjects[i].Selected)
                    {
                        size1 = grfx.MeasureString(ReportDataObjects[i].Text, FontReportDataBold, PointF.Empty, StringFormat.GenericDefault);
                        size2 = grfx.MeasureString(ReportDataObjects[i].Text, FontReportData, PointF.Empty, StringFormat.GenericDefault);
                        dx = (size2.Width - size1.Width) / 2;
                        grfx.FillRectangle(Brushes.LightGray, x, y2, width, size.Height);
                        grfx.DrawString(ReportDataObjects[i].Text, font, Brushes.Black, x2, y2, StringFormat.GenericDefault);
                    }
                    else
                    {
                        grfx.DrawString(ReportDataObjects[i].Text, font, Brushes.DarkGray, x2, y2, StringFormat.GenericDefault);
                    }
                    y2 += size.Height;
                }
                grfx.DrawImageUnscaled(ReportPlusButton, (int)(x + BoundsArcRadius / 2), (int)(y + height - BoundsArcRadius / 2 - 18));
            }
            y = (ExpandedReport ? (y + height - BoundsArcRadius / 2 - 18) : (y + YReport + 2));
            grfx.DrawImageUnscaled(ReportButton, (int)(x + width - 24 - 2), (int)y);
        }
        #endregion

        public void Translate(float dx, float dy)
        {
            X += dx * DPI;
            Y += dy * DPI;
            if (OnBoundsChange != null) OnBoundsChange();
        }
        private void DrawMarkers(Graphics grfx)
        {
            if (Selected)
            {
                int x = (int)ScreenX(X) - 3, y = (int)ScreenY(Y) - 3, width = (int)(Width / DPI) - 1, height = (int)(Height / DPI) - 1;

                grfx.DrawImageUnscaled(markerCircle, x, y);
                grfx.DrawImageUnscaled(markerCircle, x + width, y);
                grfx.DrawImageUnscaled(markerCircle, x, y + height);
                grfx.DrawImageUnscaled(markerCircle, x + width, y + height);

                x += 1; y += 1;
                grfx.DrawImageUnscaled(markerRect, x + width / 2, y);
                grfx.DrawImageUnscaled(markerRect, x + width, y + height / 2);
                grfx.DrawImageUnscaled(markerRect, x + width / 2, y + height);
                grfx.DrawImageUnscaled(markerRect, x, y + height / 2);
            }
        }
        public override void Draw(Graphics grfx)
        {
            float x = ScreenX(X), y = ScreenY(Y), width = Width / DPI, height = Height / DPI;
            Rectangle rect = new Rectangle((int)x, (int)y, (int)width, (int)height);
            if (Base.ClientRectangle.IntersectsWith(rect))
            {
                GraphicsPath gp = new GraphicsPath();
                gp.AddLine(x + BoundsArcRadius, y, x + width - BoundsArcRadius * 2, y);
                gp.AddArc(x + width - BoundsArcRadius * 2, y, BoundsArcRadius * 2, BoundsArcRadius * 2, 270, 90);
                gp.AddLine(x + width, y + BoundsArcRadius, x + width, y + height - BoundsArcRadius * 2);
                gp.AddArc(x + width - BoundsArcRadius * 2, y + height - BoundsArcRadius * 2, BoundsArcRadius * 2, BoundsArcRadius * 2, 0, 90);
                gp.AddLine(x + width - BoundsArcRadius * 2, y + height, x + BoundsArcRadius, y + height);
                gp.AddArc(x, y + height - BoundsArcRadius * 2, BoundsArcRadius * 2, BoundsArcRadius * 2, 90, 90);
                gp.AddLine(x, y + height - BoundsArcRadius * 2, x, y + BoundsArcRadius);
                gp.AddArc(x, y, BoundsArcRadius * 2, BoundsArcRadius * 2, 180, 90);

                grfx.SmoothingMode = SmoothingMode.None;
                //grfx.SetClip(Base.ClientRectangle);
                grfx.FillPath(BrushBack, gp);
                //grfx.ResetClip();

                SizeF sizeName = grfx.MeasureString(this.Name, FontName, PointF.Empty, StringFormat.GenericDefault);
                grfx.SmoothingMode = SmoothingMode.AntiAlias;
                grfx.DrawLine(PenBounds, x, y + sizeName.Height + BoundsArcRadius / 2, x + width, y + sizeName.Height + BoundsArcRadius / 2);
                DrawConditions(grfx, x, y, width, height);
                DrawReportObjects(grfx, x, y, width, height);
                ////grfx.SetClip(gp);
                grfx.DrawString(this.Name, FontName, Brushes.Black, x + BoundsArcRadius / 2, y + BoundsArcRadius / 2);
                //grfx.ResetClip();
                grfx.DrawPath(PenBounds, gp);
                gp.Dispose();
                DrawMarkers(grfx);
            }
        }

        #region Data Processing
        public bool IsProjectObject
        {
            get
            {
                bool result = false;
                switch (this.Type)
                {
                    case ObjectType.Region:
                        result = true;
                        break;
                    case ObjectType.NGDU:
                        result = true;
                        break;
                    case ObjectType.OilField:
                        result = true;
                        break;
                    case ObjectType.Area:
                        result = true;
                        break;
                    case ObjectType.WellList:
                        result = true;
                        break;
                    case ObjectType.Contour:
                        result = true;
                        break;
                    case ObjectType.Well:
                        result = true;
                        break;
                    case ObjectType.Mer:
                        result = false;
                        break;
                    case ObjectType.Gis:
                        result = false;
                        break;
                    case ObjectType.Perforation:
                        result = false;
                        break;
                    case ObjectType.SumParameters:
                        result = false;
                        break;
                    case ObjectType.GTM:
                        result = false;
                        break;
                    case ObjectType.RepairAction:
                        result = false;
                        break;
                    case ObjectType.WellResearch:
                        result = false;
                        break;
                    default:
                        throw new ArgumentException("Неизвестный тип данных объекта!");
                }
                return result;
            }
        }
        #endregion
    }

    public enum ObjectType : byte
    {
        None,
        Region,
        NGDU,
        OilField,
        Area,
        Well,
        WellList,
        Contour,
        SumParameters,
        Mer,
        Coord,
        Gis,
        Perforation,
        Chess,
        Logs,
        GTM,
        RepairAction,
        WellResearch
    }
}


