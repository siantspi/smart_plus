﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus.ReportBuilder.Base
{
    public sealed class ReportDateTime
    {
        string[] ReportDateTimeName = { "Значение", "Дата запуска" };
        string[] ReportDateTimeOperationName = { "", "+", "-" };
        string[] ReportDateTimeOpUnitName = { "", "мес.", "лет" };

        DateTime value;
        public ReportDateTimeType Type;
        public ReportDateTimeOperation Operation;
        public ReportDateTimeOpUnit OperationUnit;
        public int OperationValue;
        public DateTime Value
        {
            get
            {
                switch (Operation)
                {
                    case ReportDateTimeOperation.None:
                        return value;
                    case ReportDateTimeOperation.Addition:
                        goto case ReportDateTimeOperation.Subtraction;
                    case ReportDateTimeOperation.Subtraction:
                        return GetValueByOperation(value);
                    default:
                        throw new Exception("Неизвестный тип операции даты");
                }
            }
        }
        public DateTime GetValueByOperation(DateTime Date)
        {
            DateTime res = Date;
            if (this.Operation != ReportDateTimeOperation.None)
            {
                int k = (Operation == ReportDateTimeOperation.Addition) ? 1 : -1;
                switch (OperationUnit)
                {
                    case ReportDateTimeOpUnit.Month:
                        res = value.AddMonths(k * OperationValue);
                        break;
                    case ReportDateTimeOpUnit.Years:
                        res = value.AddYears(k * OperationValue);
                        break;
                    default:
                        throw new Exception("Неизвестная еденица измерения операции даты");
                }
            }
            return res;
        }
        public string ToString()
        {
            string result = value.ToShortDateString();
            if (this.Type != ReportDateTimeType.Value)
            {
                result = GetName(this.Type);
            }
            if (this.Operation != ReportDateTimeOperation.None)
            {
                result += string.Format("({1} {2} {3})", value, GetOpName(this.Operation), GetOpUnitName(this.OperationUnit));
            }
            return result;
        }
        public ReportDateTime(ReportDateTimeType Type, DateTime Value, ReportDateTimeOperation Operation, int OpValue, ReportDateTimeOpUnit OpUnit)
        {
            this.Type = Type;
            this.value = Value;
            this.Operation = Operation;
            this.OperationValue = OpValue;
            this.OperationUnit = OpUnit;
        }
        public string GetName(ReportDateTimeType Type)
        {
            return ReportDateTimeName[(int)Type];
        }
        public string GetOpName(ReportDateTimeOperation Op)
        {
            return ReportDateTimeOperationName[(int)Op];
        }
        public string GetOpUnitName(ReportDateTimeOpUnit Unit)
        {
            return ReportDateTimeOpUnitName[(int)Unit];
        }
    }
    public enum ReportDateTimeType : byte
    {
        Value,
        StartMerWork
    }
    public enum ReportDateTimeOperation : byte 
    {
        None,
        Addition,
        Subtraction
    }
    public enum ReportDateTimeOpUnit : byte
    {
        None,
        Month,
        Years
    }
}
