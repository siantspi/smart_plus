﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using SmartPlus.ReportBuilder.Objects;

namespace SmartPlus.ReportBuilder.Base
{
    public delegate void OnObjectBoundsChangeDelegate(ReportObject sender);
    public sealed class Connection
    {
        public bool Selected = false;
        public string Text;
        public ReportObject MainObject, TargetObject;
        public DataObject MainData, TargetData;
        public Condition condition;
        public event OnObjectBoundsChangeDelegate OnObjectBoundsChange;

        public Connection(ReportObject Main, ReportObject Target)
        {
            Main.OnBoundsChange += new OnBoundsChangeDelegate(Main_OnBoundsChange);
            Target.OnBoundsChange += new OnBoundsChangeDelegate(Target_OnBoundsChange);
            MainObject = Main;
            TargetObject = Target;
            MainData = null;
            TargetData = null;
            condition = new Condition(ConditionType.EnterInto);
            Text = string.Format("'{0}' - {1} - '{2}'", MainObject.Name, condition.Text, TargetObject.Name);
        }
        public Connection(ReportObject Main, ReportObject Target, DataObject MainData, DataObject TargetData, Condition condition)
        {
            Main.OnBoundsChange += new OnBoundsChangeDelegate(Main_OnBoundsChange);
            Target.OnBoundsChange += new OnBoundsChangeDelegate(Target_OnBoundsChange);
            this.MainObject = Main;
            this.TargetObject = Target;
            this.MainData = MainData;
            this.TargetData = TargetData;
            this.condition = condition;
            Text = string.Format("'{0}.{1}' - {2} - '{3}.{4}'", MainObject.Name, MainData.Text, condition.Text, TargetObject.Name, TargetData.Text);
        }
        void Main_OnBoundsChange()
        {
            OnObjectBoundsChange(MainObject);
        }
        void Target_OnBoundsChange()
        {
            OnObjectBoundsChange(TargetObject);
        }
    }
}
