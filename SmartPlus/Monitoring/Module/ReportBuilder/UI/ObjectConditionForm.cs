﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartPlus.ReportBuilder.Base;
using SmartPlus.ReportBuilder.Objects;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.ReportBuilder.UI
{
    public partial class ObjectConditionForm : Form
    {
        MainForm mainForm;
        ReportObject Object;
        public DictionaryBase currDict;
        public object Value;
        int startDictIndex;
        bool DictByFullName;

        bool ShowedSettings
        {
            get { return SettingsGroup.Visible; }
            set
            {
                if (value)
                {
                    this.Height = 279;
                    SettingsGroup.Visible = true;
                }
                else
                {
                    this.Height = 172;
                    SettingsGroup.Visible = false;
                }
            }
        }


        public ObjectConditionForm(ReportObject Object)
        {
            InitializeComponent();
            startDictIndex = 0;
            DictByFullName = false;
            this.Object = Object;
            ShowedSettings = false;
            Point pt = ShiftDateUpDown.Location;
            pt.X = cbShiftDate.Right + 18;
            ShiftDateUpDown.Location = pt;
            pt = label1.Location;
            pt.X = ShiftDateUpDown.Right + 18;
            label1.Location = pt;
            for(int i = 0; i < Object.DataObjects.Count;i++)
            {
                this.cbObject.Items.Add(Object.DataObjects[i]);
            }
            this.FormClosing += new FormClosingEventHandler(ObjectConditionForm_FormClosing);
        }

        void ObjectConditionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                if ((cbObject.SelectedItem != null) && (cbCondition.SelectedItem != null))
                {
                    try
                    {
                        switch (((SmartPlus.ReportBuilder.Base.DataObject)cbObject.SelectedItem).DataType)
                        {
                            case DataTypes.Bool:
                                Value = (cbValues.SelectedIndex == 0) ? true : false;
                                break;
                            case DataTypes.Date:
                                if (cbUseDateStart.Checked)
                                {
                                    Value = new DateTime(1900, 1, 1);
                                }
                                else if (cbUseWellListDate.Checked)
                                {
                                    Value = new DateTime(1900, 1, 2);
                                }
                                else
                                {
                                    Value = dtPicker.Value.Date;
                                }
                                if (cbShiftDate.Checked && ShiftDateUpDown.Value != 0)
                                {
                                    var shiftDate = (DateTime)Value;
                                    if (ShiftDateUpDown.Value < 0) shiftDate = shiftDate.AddSeconds(1);
                                    shiftDate = shiftDate.AddMilliseconds(Convert.ToDouble(Math.Abs(ShiftDateUpDown.Value)));
                                    Value = shiftDate;
                                }
                                break;
                            case DataTypes.Dictionary:
                                ConditionObjectDict dictObj = new ConditionObjectDict();
                                dictObj.Dict = currDict;
                                dictObj.StratumDict = (StratumDictionary)mainForm.GetLastProject().DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                                dictObj.DictCode = currDict.GetCodeByShortName((string)cbValues.Items[cbValues.SelectedIndex]);
                                dictObj.ByFullName = this.DictByFullName;
                                Value = dictObj;
                                break;
                            case DataTypes.Number:
                                Value = Convert.ToDouble(tbValue.Text);
                                break;
                            case DataTypes.NumberInterval:
                                double[] vals = new double[2];
                                vals[0] = Convert.ToDouble(tbInt1.Text);
                                vals[1] = Convert.ToDouble(tbInt2.Text);
                                Value = vals;
                                break;
                            case DataTypes.String:
                                Value = tbValue.Text;
                                break;
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Проверьте правильность введенных данных.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Cancel = true;
                    }
                }
                else
                {
                    MessageBox.Show("Заполните все поля.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }
        }

        public static ConditionObject Show(MainForm MainForm, ReportObject Object)
        {
            ObjectConditionForm form = new ObjectConditionForm(Object);
            form.mainForm = MainForm;
            form.Owner = MainForm;
            form.StartPosition = FormStartPosition.CenterParent;
            form.lObject.Text = string.Format("Объект: {0}", Object.Name);
            form.lObjectFields.Text = string.Format("Свойства объекта: {0}", Object.Name);
            ConditionObject cond = null;
            form.dtPicker.Value = new DateTime(DateTime.Now.Year, 1, 1);
            if (form.ShowDialog() == DialogResult.OK)
            {
                SmartPlus.ReportBuilder.Base.DataObject obj = (SmartPlus.ReportBuilder.Base.DataObject)form.cbObject.SelectedItem;
                obj.Dictionary = form.currDict;
                cond = new ConditionObject(obj, (Condition)form.cbCondition.SelectedItem, obj.DataType, form.Value);
            }
            form.Dispose();
            return cond;
        }

        void UpdateBySelObject(SmartPlus.ReportBuilder.Base.DataObject SelObject)
        {
            cbValues.SelectedIndex = -1;
            cbValues.Items.Clear();
            tbValue.Text = string.Empty;
            cbValues.Visible = false;
            dtPicker.Visible = false;
            tbValue.Visible = false;
            tbInt1.Visible = false;
            tbInt2.Visible = false;
            lInt.Visible = false;
            ShowedSettings = false;
            switch (SelObject.DataType)
            {
                case DataTypes.Bool:
                    cbValues.Visible = true;
                    cbValues.Items.Clear();
                    cbValues.Items.Add("Да");
                    cbValues.Items.Add("Нет");
                    cbValues.SelectedIndex = 0;
                    break;
                case DataTypes.Date:
                    dtPicker.Visible = true;
                    ShowedSettings = true;
                    break;
                case DataTypes.Dictionary:
                    cbValues.Visible = true;
                    break;
                case DataTypes.Number:
                    tbValue.Visible = true;
                    break;
                case DataTypes.NumberInterval:
                    tbInt1.Visible = true;
                    tbInt2.Visible = true;
                    lInt.Visible = true;
                    break;
                case DataTypes.String:
                    tbValue.Visible = true;
                    break;
            }
            List<Condition> list = SelObject.GetConditions();
            cbCondition.Items.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                cbCondition.Items.Add(list[i]);
            }
            if (cbCondition.Items.Count > 0) cbCondition.SelectedIndex = 0;
            FillDictionaryItems();
        }
        void FillDictionaryItems()
        {
            SmartPlus.ReportBuilder.Base.DataObject obj = (SmartPlus.ReportBuilder.Base.DataObject)cbObject.SelectedItem;
            if ((cbObject.SelectedItem != null) && (obj.DataType == DataTypes.Dictionary) && (obj.DictType != DICTIONARY_ITEM_TYPE.NONE))
            {
                currDict = (DictionaryBase)mainForm.GetLastProject().DictList.GetDictionary(obj.DictType);
                cbValues.Items.Clear();
                DictByFullName = obj.DictByFullName;
                if(currDict != null)
                {
                    switch (currDict.Type)
                    {
                        case DICTIONARY_ITEM_TYPE.OILFIELD_AREA:
                            var dictArea = (OilFieldAreaDictionary)currDict;
                            startDictIndex = (dictArea[0].Name.Length == 0) ? 1 : 0;
                            for (int i = startDictIndex; i < dictArea.Count; i++)
                            {
                                cbValues.Items.Add(dictArea[i].Name);
                            }
                            break;
                        case DICTIONARY_ITEM_TYPE.STRATUM:
                            var dictStratum = (StratumDictionary)currDict;
                            startDictIndex = (dictStratum[0].ShortName.Length == 0) ? 1 : 0;
                            for (int i = startDictIndex; i < dictStratum.Count; i++)
                            {
                                cbValues.Items.Add(DictByFullName ? dictStratum[i].FullName : dictStratum[i].ShortName);
                            }
                            break;
                         default:
                            var dictData = (DataDictionary)currDict;
                            startDictIndex = (dictData[0].ShortName.Length == 0) ? 1 : 0;

                            for (int i = startDictIndex; i < dictData.Count; i++)
                            {
                                cbValues.Items.Add(DictByFullName ? dictData[i].FullName : dictData[i].ShortName);
                            }
                            break;
                    }

                }
            }
        }
        private void cbObject_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateBySelObject((SmartPlus.ReportBuilder.Base.DataObject)cbObject.SelectedItem);
        }

        private void cbDateStart_CheckedChanged(object sender, EventArgs e)
        {
            if (cbUseDateStart.Checked) cbUseWellListDate.Checked = false;
            dtPicker.Enabled = !cbUseDateStart.Checked;
        }

        private void cbUseWellListDate_CheckedChanged(object sender, EventArgs e)
        {
            if (cbUseWellListDate.Checked) cbUseDateStart.Checked = false;
            dtPicker.Enabled = !cbUseWellListDate.Checked;
        }

        private void cbShiftDate_CheckedChanged(object sender, EventArgs e)
        {
            ShiftDateUpDown.Enabled = cbShiftDate.Checked;
            label1.Enabled = cbShiftDate.Checked;
        }
    }
}

