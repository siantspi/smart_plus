﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartPlus.ReportBuilder.Base;

namespace SmartPlus.ReportBuilder.UI
{
    public partial class ObjectConnectionForm : Form
    {
        ReportObject MainObject, TargetObject;
        Condition ConnCondition;

        public ObjectConnectionForm(ReportObject Main, ReportObject Target)
        {
            InitializeComponent();
            this.MainObject = Main;
            this.TargetObject = Target;
            for(int i = 0; i < MainObject.DataObjects.Count;i++)
            {
                for (int j = 0; j < TargetObject.DataObjects.Count; j++)
                {
                    if (this.MainObject.DataObjects[i].IsConnectionPossible(TargetObject.DataObjects[j]))
                    {
                        this.cbObjA.Items.Add(MainObject.DataObjects[i]);
                        break;
                    }
                }
            }
            if (this.cbObjA.Items.Count > 0) this.cbObjA.SelectedIndex = 0;
        }

        public static Connection Show(Form MainForm, ReportObject Main, ReportObject Target)
        {
            ObjectConnectionForm form = new ObjectConnectionForm(Main, Target);
            form.Owner = MainForm;
            form.StartPosition = FormStartPosition.CenterParent;
            form.lCond.Text = string.Format("Связь: {0} - {1}", Main.Name, Target.Name);
            form.lObjA.Text = string.Format("Свойства объекта: {0}", Main.Name);
            form.lObjB.Text = string.Format("Свойства объекта: {0}", Target.Name);
            Connection conn = null;
            if (form.ShowDialog() == DialogResult.OK)
            {
                SmartPlus.ReportBuilder.Base.DataObject objA = (SmartPlus.ReportBuilder.Base.DataObject)form.cbObjA.SelectedItem;
                SmartPlus.ReportBuilder.Base.DataObject objB = (SmartPlus.ReportBuilder.Base.DataObject)form.cbObjB.SelectedItem;
                conn = new Connection(form.MainObject, form.TargetObject, objA, objB, (Condition)form.cbCondition.SelectedItem);
            }
            form.Dispose();
            return conn;
        }
        void UpdateBySelObject(SmartPlus.ReportBuilder.Base.DataObject SelObject)
        {
            cbObjB.Items.Clear();
            for (int i = 0; i < TargetObject.DataObjects.Count; i++)
            {
                if (SelObject.IsConnectionPossible(TargetObject.DataObjects[i]))
                {
                    cbObjB.Items.Add(TargetObject.DataObjects[i]);
                }
            }
            if (this.cbObjB.Items.Count > 0) this.cbObjB.SelectedIndex = 0;

            List<Condition> list = SelObject.GetConditions();
            cbCondition.Items.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                cbCondition.Items.Add(list[i]);
            }
            if (this.cbCondition.Items.Count > 0) this.cbCondition.SelectedIndex = 0;
        }

        private void cbObjA_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateBySelObject((SmartPlus.ReportBuilder.Base.DataObject)cbObjA.SelectedItem);
        }
    }
}
