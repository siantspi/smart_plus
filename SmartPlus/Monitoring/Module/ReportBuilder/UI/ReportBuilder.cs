﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SmartPlus.ReportBuilder.Base;
using SmartPlus.ReportBuilder.Objects;

namespace SmartPlus.ReportBuilder.UI
{
    sealed class ReportBuilder : PictureBox
    {
        MainForm MainForm;
        Bitmap bmp;
        Graphics bmp_grfx;
        Brush BrushConnMode;
        Pen PenConnMode;
        Point shiftPoint;
        ReportObjListMenu ReportObjMenu;
        List<ReportObject> objects;
        List<ReportObject> ConnectObjects;
        List<ReportLine> lines;
        ReportObject FlyAddedObject = null;

        public List<ReportObject> Objects
        {
            get { return objects; }
        }
        public List<ReportLine> Lines
        {
            get { return lines; }
        }
        int SelectedObjectIndex, SelectedObjectIndex1, SelectedObjectIndex2;
        public bool UserDialogShowing
        {
            get 
            {
                return ReportObjMenu.Visible;
            }
            set
            {
                ReportObjMenu.Visible = value;
            }
        }
        bool MouseLeftPressed;

        Point PointDown;
        ContextMenuStrip contextMenu;
        Timer tmrDraw;

        void InitContextMenu()
        {
            contextMenu = new ContextMenuStrip();
            ToolStripItem item = contextMenu.Items.Add("Запустить обработку...");
            item.Image = SmartPlus.Properties.Resources.Start;
            item.Click += new EventHandler(StartItem_Сlick);
            this.ContextMenuStrip = contextMenu;
        }
        void StartItem_Сlick(object sender, EventArgs e)
        {
            ReportForm report = new ReportForm(MainForm);
            if (MainForm.canv.MapProject == null)
            {
                MessageBox.Show("Для выгрузки отчета необходимо загрузить проект.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            report.ShowTable(this.objects, MainForm.canv.MapProject);
        }
        public ReportBuilder(MainForm MainForm)
        {
            InitContextMenu();
            this.Parent = MainForm.pReportBuilder;
            MainForm.pReportBuilder.AllowDrop = true;
            this.Dock = DockStyle.None;
            this.Anchor = (AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top);
            this.Left = this.Parent.ClientRectangle.X;
            this.Top = this.ClientRectangle.Y + 26;
            this.Width = Parent.Width;
            this.Height = Parent.Height - 26;
            this.MainForm = MainForm;
            bmp = new Bitmap(App.MaxMonitorWidth, App.MaxMonitorHeight);
            bmp_grfx = Graphics.FromImage(bmp);
            //bmp_grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            Base.Base.ClientRectangle = this.ClientRectangle;
            shiftPoint = this.ClientRectangle.Location;
            objects = new List<ReportObject>();
            ConnectObjects = new List<ReportObject>();
            lines = new List<ReportLine>();
            
            SelectedObjectIndex = -1;
            SelectedObjectIndex1 = -1;
            SelectedObjectIndex2 = -1;

            BrushConnMode = new SolidBrush(Color.FromArgb(200, Color.White));
            PenConnMode = new Pen(Color.Black, 2f);
            tmrDraw = new Timer();
            tmrDraw.Enabled = false;
            tmrDraw.Interval = 25;
            tmrDraw.Tick += new EventHandler(tmrDraw_Tick);

            #region Data Obj List
            ReportObjMenu = new ReportObjListMenu(MainForm);
            ReportObjMenu.ReportObjItemSelected += new ReportObjListItemSelectDelegate(ReportObjMenu_ReportObjItemSelected);
            #endregion

            this.SizeChanged += new EventHandler(ReportBuilder_SizeChanged);
            this.MouseWheel += new MouseEventHandler(ReportBuilder_MouseWheel);
            this.MouseDown += new MouseEventHandler(ReportBuilder_MouseDown);
            this.MouseUp += new MouseEventHandler(ReportBuilder_MouseUp);
            this.MouseMove += new MouseEventHandler(ReportBuilder_MouseMove);
            this.Paint += new PaintEventHandler(ReportBuilder_Paint);
            this.PreviewKeyDown += new PreviewKeyDownEventHandler(ReportBuilder_PreviewKeyDown);

            MainForm.pReportBuilder.DragEnter += new DragEventHandler(ReportBuilder_DragEnter);
            MainForm.pReportBuilder.DragDrop += new DragEventHandler(ReportBuilder_DragDrop);
        }

        #region Drag Drop Objects
        void ReportBuilder_DragEnter(object sender, DragEventArgs e)
        {
            TreeNode dragNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

            if ((dragNode != null) && (dragNode.Tag != null) &&
                (((BaseObj)dragNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        void ReportBuilder_DragDrop(object sender, DragEventArgs e)
        {
            TreeNode dragNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
            if ((dragNode != null) && (dragNode.Tag != null) &&
                (((BaseObj)dragNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
            {
                Point pt = PointToClient(Cursor.Position);
                AddFlyObject(ObjectType.WellList, (C2DLayer)dragNode.Tag);
                if (FlyAddedObject != null)
                {
                    AddObject(FlyAddedObject, Base.Base.RealX(pt.X), Base.Base.RealY(pt.Y));
                    DrawBuilder();
                    FlyAddedObject = null;
                }
            }
        }
        #endregion

        void tmrDraw_Tick(object sender, EventArgs e)
        {
            DrawBuilder();
        }

        void ReportBuilder_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (!DeleteObjectData())
                {
                    RemoveSelected();
                }
            }
            if (e.KeyCode == Keys.Escape)
            {
                if (FlyAddedObject != null)
                {
                    FlyAddedObject = null;
                    this.Invalidate();
                }
            }
        }

        public void AddFlyObject(Base.ObjectType ObjectType)
        {
            AddFlyObject(ObjectType, null);
        }
        public void AddFlyObject(Base.ObjectType ObjectType, SmartPlus.BaseObj ProjectObject)
        {
            if (MainForm.GetLastProject() == null) return;
            FlyAddedObject = null;
            switch (ObjectType)
            {
                case ObjectType.OilField:
                    FlyAddedObject = new ReportOilField(objects.Count);
                    break;
                case ObjectType.Area:
                    FlyAddedObject = new ReportArea(objects.Count);
                    break;
                case ObjectType.Well:
                    FlyAddedObject = new ReportWell(objects.Count);
                    break;
                case ObjectType.SumParameters:
                    FlyAddedObject = new ReportSumParams(objects.Count);
                    break;
                case ObjectType.Mer:
                    FlyAddedObject = new ReportMer(objects.Count);
                    break;
                case ObjectType.Gis:
                    FlyAddedObject = new ReportGis(objects.Count);
                    break;
                case ObjectType.GTM:
                    FlyAddedObject = new ReportGTM(objects.Count);
                    break;
                case ObjectType.Perforation:
                    FlyAddedObject = new ReportPerforation(objects.Count);
                    break;
                case ObjectType.RepairAction:
                    FlyAddedObject = new ReportRepair(objects.Count);
                    break;
                case ObjectType.WellResearch:
                    FlyAddedObject = new ReportResearch(objects.Count);
                    break;
                case ObjectType.WellList:
                    if (ProjectObject != null)
                    {
                        FlyAddedObject = new ReportWellList(objects.Count, ProjectObject);
                    }
                    break;
                default:
                    throw new ArgumentException("Не поддерживаемый тип объекта!");
            }
            if (FlyAddedObject != null)
            {
                int maxNameIndex = -1;
                for (int i = 0; i < objects.Count; i++)
                {
                    if ((objects[i].Name.StartsWith(FlyAddedObject.Name)) && (maxNameIndex < objects[i].NameIndex))
                    {
                        maxNameIndex = objects[i].NameIndex;
                    }
                }
                if (maxNameIndex > -1)
                {
                    maxNameIndex++;
                    FlyAddedObject.Name = string.Format("{0}_{1}", FlyAddedObject.Name, maxNameIndex);
                    FlyAddedObject.NameIndex = maxNameIndex;
                }
                FlyAddedObject.DictList = MainForm.GetLastProject().DictList;
                FlyAddedObject.ResetGraphicsSizes(bmp_grfx);
            }
        }
        public void AddObject(ReportObject Object, float X, float Y)
        {
            int addedIndex = objects.Count;
            objects.Add(Object);
            int maxNameIndex = -1;
            for (int i = 0; i < objects.Count; i++)
            {
                if ((addedIndex != i) && (objects[i].Name.StartsWith(objects[addedIndex].Name)) && (maxNameIndex < objects[i].NameIndex))
                {
                    maxNameIndex = objects[i].NameIndex;
                }
            }
            if (maxNameIndex > -1)
            {
                maxNameIndex++;
                objects[addedIndex].Name = string.Format("{0}_{1}", objects[addedIndex].Name, maxNameIndex);
                objects[addedIndex].NameIndex = maxNameIndex;
            }
            objects[addedIndex].DictList = MainForm.GetLastProject().DictList;

            objects[objects.Count - 1].X = X;
            objects[objects.Count - 1].Y = Y;
            objects[objects.Count - 1].OnNeedPaint += new OnNeedPaintDelegate(RePaintReportObject);
            this.MouseUp += new MouseEventHandler(objects[objects.Count - 1].ReportBuilder_MouseUp);
            this.MouseDown += new MouseEventHandler(objects[objects.Count - 1].ReportBuilder_MouseDown);
            this.MouseMove += new MouseEventHandler(objects[objects.Count - 1].ReportObject_MouseMove);
            objects[objects.Count - 1].ResetGraphicsSizes(bmp_grfx);
        }

        public void RemoveAtObject(int Index)
        {
            if ((Index > -1) && (Index < objects.Count))
            {
                this.MouseUp -= new MouseEventHandler(objects[Index].ReportBuilder_MouseUp);
                this.MouseDown -= new MouseEventHandler(objects[Index].ReportBuilder_MouseDown);
                this.MouseMove -= new MouseEventHandler(objects[Index].ReportObject_MouseMove);
                objects.RemoveAt(Index);
                for (int i = Index; i < objects.Count; i++)
                {
                    objects[i].Index = i; 
                }
            }
        }
        
        void RePaintReportObject(int IndexObject)
        {
            DrawBuilder();
        }
        void ReportBuilder_MouseWheel(object sender, MouseEventArgs e)
        {
            int count = e.Delta / 120;
            PointF oldPoint = new PointF(Base.Base.RealX(e.X), Base.Base.RealY(e.Y));
            if (count < 0)
            {
                Base.Base.DPI /= Math.Abs(count) * 1.3f;
            }
            else
            {
                Base.Base.DPI *= Math.Abs(count) * 1.3f;
                if (Base.Base.DPI > 1f) Base.Base.DPI = 1f;
            }
            PointF newPoint = new PointF(Base.Base.ScreenX(oldPoint.X), Base.Base.ScreenY(oldPoint.Y));
            Base.Base.HotSpot.X += newPoint.X - e.X;
            Base.Base.HotSpot.Y += newPoint.Y - e.Y;
            DrawBuilder();
        }
        void ReportBuilder_MouseDown(object sender, MouseEventArgs e)
        {
            if (FlyAddedObject != null)
            {
                if (e.Button == MouseButtons.Left)
                {
                    AddObject(FlyAddedObject, Base.Base.RealX(e.X), Base.Base.RealY(e.Y));
                }
                FlyAddedObject = null;
            }
            else
            {
                MouseLeftPressed = e.Button == MouseButtons.Left;
                SelectedObjectIndex1 = -1;
                if ((SelectedObjectIndex != -1) && (SelectedObjectIndex1 == -1) &&
                    (objects[SelectedObjectIndex].PointInBounds(e.Location)))
                {
                    SelectedObjectIndex1 = SelectedObjectIndex;
                }
                if (SelectedObjectIndex1 == -1)
                {
                    SelectedObjectIndex = -1;
                    bool find = false;
                    for (int i = objects.Count - 1; i >= 0; i--)
                    {
                        objects[i].Selected = false;
                        if (!find && (objects[i].PointInBounds(e.Location)))
                        {
                            objects[i].Selected = true;
                            SelectedObjectIndex = i;
                            if (objects[i].IsCondPlusBtnOver || objects[i].IsReportPlusBtnOver ||
                                objects[i].IsCondBtnOver || objects[i].IsReportBtnOver)
                            {
                                MouseLeftPressed = false;
                            }
                            find = true;
                        }
                    }
                }
                if ((SelectedObjectIndex != -1) && (objects[SelectedObjectIndex].IsCondLinkBtnOver))
                {
                    ConnectObjects.Clear();
                    for (int i = 0; i < objects.Count; i++)
                    {
                        if ((i != SelectedObjectIndex) && (objects[i].IsCompatibilityObject(objects[SelectedObjectIndex])) &&
                            (objects[i].ConnectedObjects.IndexOf(objects[SelectedObjectIndex]) == -1) &&
                            (objects[SelectedObjectIndex].ConnectedObjects.IndexOf(objects[i]) == -1))
                        {
                            ConnectObjects.Add(objects[i]);
                        }
                    }
                }
                PointDown = e.Location;
            }
            DrawBuilder();
        }
        void ReportBuilder_MouseUp(object sender, MouseEventArgs e)
        {
            bool tryPlusClick = true;
            tmrDraw.Stop();
            ConnectObjects.Clear();
            if (SelectedObjectIndex1 != -1)
            {
                objects[SelectedObjectIndex1].BuildConnectionMode = false;
                if (SelectedObjectIndex2 != -1)
                {
                    CreateConnection();
                    tryPlusClick = false;
                }
            }
            if (tryPlusClick)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (objects[i].Selected)
                    {
                        if (objects[i].IsCondPlusBtnOver && objects[i].CanAddCondition)
                        {
                            CreateCondition(i);
                            break;
                        }
                        if (objects[i].IsReportPlusBtnOver)
                        {
                            Point pt = PointToScreen(e.Location);
                            pt.Offset(-MainForm.Location.X, -MainForm.Location.Y);
                            CreateReportObject(i, pt);
                            break;
                        }
                    }
                }
            }
            MouseLeftPressed = false;
            DrawBuilder();
        }
        void ReportBuilder_MouseMove(object sender, MouseEventArgs e)
        {
            if (!MainForm.UserDialogShowing) this.Focus();
            if (MouseLeftPressed)
            {
                int dx = e.X - PointDown.X, dy = e.Y - PointDown.Y;
                if (SelectedObjectIndex1 != -1)
                {
                    if ((!objects[SelectedObjectIndex1].IsCondLinkBtnOver) &&
                        (!objects[SelectedObjectIndex1].IsCondPlusBtnOver) &&
                        (!objects[SelectedObjectIndex1].IsReportPlusBtnOver) && 
                        (!objects[SelectedObjectIndex1].BuildConnectionMode))
                    {
                        objects[SelectedObjectIndex1].Translate(dx, dy);
                        if (!tmrDraw.Enabled) tmrDraw.Start();
                        
                    }
                    else if (objects[SelectedObjectIndex1].BuildConnectionModeStart)
                    {
                        objects[SelectedObjectIndex1].BuildConnectionMode = true;
                        this.Invalidate();
                    }
                }
                else
                {
                    bool find = false;
                    if ((SelectedObjectIndex != -1) && 
                        objects[SelectedObjectIndex].Selected && 
                        objects[SelectedObjectIndex].BuildConnectionModeStart)
                    {
                        SelectedObjectIndex1 = SelectedObjectIndex;
                    }
                    if (!find)
                    {
                        if (SelectedObjectIndex1 == -1)
                        {
                            if (SelectedObjectIndex != -1)
                            {
                                objects[SelectedObjectIndex].Selected = false;
                                SelectedObjectIndex = -1;
                            }
                            Base.Base.HotSpot.X -= dx;
                            Base.Base.HotSpot.Y -= dy;
                            if (!tmrDraw.Enabled) tmrDraw.Start();
                        }
                        else
                        {
                            objects[SelectedObjectIndex1].BuildConnectionMode = true;
                            this.Invalidate();
                        }
                    }
                }
                PointDown = e.Location;
            }
            else if (FlyAddedObject != null)
            {
                this.Invalidate();
            }
        }
        void ReportBuilder_SizeChanged(object sender, EventArgs e)
        {
            Base.Base.ClientRectangle = this.ClientRectangle;
            DrawBuilder();
        }

        #region CREATE CONNECTION CONDITION REPORT
        public void CreateConnection()
        {
            Connection conn = null;
            if (objects[SelectedObjectIndex2].IsProjectObject)
            {
                conn = new Connection(objects[SelectedObjectIndex1], objects[SelectedObjectIndex2]);
            }
            else
            {
                conn = ObjectConnectionForm.Show(MainForm, objects[SelectedObjectIndex1], objects[SelectedObjectIndex2]);
            }
            if (conn != null)
            {
                PointF pt1, pt2;
                pt1 = objects[SelectedObjectIndex1].ConnectionPoint;
                pt2 = objects[SelectedObjectIndex2].GetBoundPoint(pt1);
                ReportLine line = new ReportLine(conn, pt1, pt2);
                lines.Add(line);
                objects[SelectedObjectIndex1].Connections.Add(conn);
                objects[SelectedObjectIndex2].ConnectedObjects.Add(objects[SelectedObjectIndex]);
                objects[SelectedObjectIndex1].ResetGraphicsSizes(bmp_grfx);
            }
            SelectedObjectIndex1 = -1;
            SelectedObjectIndex2 = -1;
        }
        public void CreateCondition(int Index)
        {
            ConditionObject cond = ObjectConditionForm.Show(MainForm, objects[Index]);
            if (cond != null)
            {
                objects[Index].AddCondition(cond);
                objects[Index].ResetGraphicsSizes(bmp_grfx);
            }
        }
        public void CreateReportObject(int Index, Point Location)
        {
            ReportObjMenu.Show(objects[Index], Index, Location);
        }
        void ReportObjMenu_ReportObjItemSelected()
        {
            if (!ReportObjMenu.Visible && ReportObjMenu.SelObject != null)
            {
                objects[ReportObjMenu.OwnerIndex].AddReportDataObjects(ReportObjMenu.SelObject);
                objects[ReportObjMenu.OwnerIndex].ResetGraphicsSizes(bmp_grfx);
                DrawBuilder();
            }
        }
        #endregion

        #region REMOVE OBJECT
        public bool DeleteObjectData()
        {
            bool result = false;
            if (SelectedObjectIndex != -1)
            {
                ReportObject SelObj = objects[SelectedObjectIndex];
                Connection conn = null;
                ConditionObject cond = null;
                SmartPlus.ReportBuilder.Base.DataObject obj = null;
                string name = string.Empty;

                for (int i = 0; i < SelObj.Connections.Count; i++)
                {
                    if (SelObj.Connections[i].Selected)
                    {
                        conn = SelObj.Connections[i];
                        name = conn.Text;
                        break;
                    }
                }
                if (conn == null)
                {
                    for (int i = 0; i < SelObj.Conditions.Count; i++)
                    {
                        if (SelObj.Conditions[i].Selected)
                        {
                            cond = SelObj.Conditions[i];
                            name = cond.Text;
                            break;
                        }
                    }
                }
                if (conn == null && cond == null)
                {
                    for (int i = 0; i < SelObj.ReportDataObjects.Count; i++)
                    {
                        if (SelObj.ReportDataObjects[i].Selected)
                        {
                            obj = SelObj.ReportDataObjects[i];
                            name = obj.Text;
                            break;
                        }
                    }
                }
                string objName = string.Empty;
                if (conn != null) objName = "связь";
                if (cond != null) objName = "условие";
                if (obj != null) objName = "данные вывода";

                if (conn != null || cond != null)
                {
                    name = string.Format("Удалить {0} '{1}'?", objName, name);
                    if (MessageBox.Show(name, "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        RemoveConnection(SelObj, conn);
                        SelObj.RemoveCondition(cond);
                        SelObj.RemoveReportDataObject(obj);
                        SelObj.ResetGraphicsSizes(bmp_grfx);
                        DrawBuilder();
                    }
                    result = true;
                }
                if (obj != null)
                {
                    SelObj.RemoveReportDataObject(obj);
                    SelObj.ResetGraphicsSizes(bmp_grfx);
                    DrawBuilder();
                    result = true;
                }
            }
            return result;
        }
        public void RemoveSelected()
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].Selected)
                {
                    if (MessageBox.Show(string.Format("Удалить объект '{0}'", objects[i].Name), "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        for (int j = objects[i].Connections.Count - 1; j >= 0; j--)
                        {
                            RemoveConnection(objects[i], objects[i].Connections[j]);
                        }
                        for (int j = objects[i].ConnectedObjects.Count - 1; j >= 0; j--)
                        {
                            for (int k = 0; k < objects[i].ConnectedObjects[j].Connections.Count; k++)
                            {
                                if (objects[i] == objects[i].ConnectedObjects[j].Connections[k].TargetObject)
                                {
                                    RemoveConnection(objects[i].ConnectedObjects[j], objects[i].ConnectedObjects[j].Connections[k]);
                                    break;
                                }
                            }
                        }
                        RemoveAtObject(i);
                        SelectedObjectIndex = -1;
                        SelectedObjectIndex1 = -1;
                        DrawBuilder();
                    }
                }
            }
        }
        public void RemoveConnection(ReportObject SelObj, Connection conn)
        {
            if (conn != null)
            {
                for (int i = 0; i < Lines.Count; i++)
                {
                    if (Lines[i].ContainConnection(conn))
                    {
                        Lines.RemoveAt(i);
                        break;
                    }
                }
                conn.TargetObject.ConnectedObjects.Remove(conn.MainObject);
                SelObj.RemoveConnection(conn);
            }
        }
        #endregion

        // DRAWING
        public void DrawBuilder()
        {
            bmp_grfx.Clear(Color.White);
            for (int i = 0; i < lines.Count; i++)
            {
                lines[i].Draw(bmp_grfx);
            }
            for (int i = 0; i < objects.Count; i++)
            {
                objects[i].Draw(bmp_grfx);
            }
            this.Invalidate();
        }
        void DrawBuildConnectionMode(Graphics grfx)
        {
            if ((SelectedObjectIndex1 != -1) && (objects[SelectedObjectIndex1].BuildConnectionMode))
            {
                grfx.FillRectangle(BrushConnMode, this.ClientRectangle);
                objects[SelectedObjectIndex1].Draw(grfx);
                float x = objects[SelectedObjectIndex1].ConnectionPoint.X;
                float y = objects[SelectedObjectIndex1].ConnectionPoint.Y;
                Point pt = PointToClient(Cursor.Position);
                SelectedObjectIndex2 = -1;
                for (int i = 0; i < objects.Count; i++)
                {
                    if (i != SelectedObjectIndex1)
                    {
                        if ((objects[i].PointInBounds(pt)) && 
                            (objects[i].IsCompatibilityObject(objects[SelectedObjectIndex1])) &&
                            (objects[i].ConnectedObjects.IndexOf(objects[SelectedObjectIndex1]) == -1) &&
                            (objects[SelectedObjectIndex1].ConnectedObjects.IndexOf(objects[i]) == -1))
                        {
                            SelectedObjectIndex2 = i;
                            break;
                        }
                    }
                }
                //if (SelectedObjectIndex2 != -1)
                //{
                //    objects[SelectedObjectIndex2].Draw(grfx);
                //}
                for (int i = 0; i < ConnectObjects.Count; i++)
                {
                    ConnectObjects[i].Draw(grfx);
                }
                grfx.DrawLine(PenConnMode, x, y, pt.X, pt.Y);
                grfx.DrawImageUnscaled(SmartPlus.Properties.Resources.Link24, pt.X - 12, pt.Y - 12);
            }
        }
        void DrawFlyAddedObject(Graphics grfx)
        {
            if (FlyAddedObject != null)
            {
                Point pt = PointToClient(Cursor.Position);
                FlyAddedObject.X = SmartPlus.ReportBuilder.Base.Base.RealX(pt.X);
                FlyAddedObject.Y = SmartPlus.ReportBuilder.Base.Base.RealY(pt.Y);
                FlyAddedObject.Draw(grfx);
            }
        }
        void ReportBuilder_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            grfx.DrawImageUnscaledAndClipped(bmp, this.ClientRectangle);
            grfx.DrawString(Base.Base.DPI.ToString(), Base.Base.FontCondition, Brushes.Black, 0, this.ClientRectangle.Bottom - 15);
            DrawBuildConnectionMode(grfx);
            DrawFlyAddedObject(grfx);
        }
    }
}
