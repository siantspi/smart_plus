﻿namespace SmartPlus.ReportBuilder.UI
{
    partial class ObjectConnectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bOK = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.lCond = new System.Windows.Forms.Label();
            this.lObjA = new System.Windows.Forms.Label();
            this.lObjB = new System.Windows.Forms.Label();
            this.cbObjA = new System.Windows.Forms.ComboBox();
            this.cbObjB = new System.Windows.Forms.ComboBox();
            this.cbCondition = new System.Windows.Forms.ComboBox();
            this.lConnection = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOK.Location = new System.Drawing.Point(664, 106);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(95, 30);
            this.bOK.TabIndex = 0;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(765, 106);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(95, 30);
            this.bCancel.TabIndex = 1;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // lCond
            // 
            this.lCond.AutoSize = true;
            this.lCond.Location = new System.Drawing.Point(300, 39);
            this.lCond.Name = "lCond";
            this.lCond.Size = new System.Drawing.Size(52, 15);
            this.lCond.TabIndex = 7;
            this.lCond.Text = "Условие";
            // 
            // lObjA
            // 
            this.lObjA.AutoSize = true;
            this.lObjA.Location = new System.Drawing.Point(1, 39);
            this.lObjA.Name = "lObjA";
            this.lObjA.Size = new System.Drawing.Size(57, 15);
            this.lObjA.TabIndex = 3;
            this.lObjA.Text = "Объект 1";
            // 
            // lObjB
            // 
            this.lObjB.AutoSize = true;
            this.lObjB.Location = new System.Drawing.Point(593, 39);
            this.lObjB.Name = "lObjB";
            this.lObjB.Size = new System.Drawing.Size(57, 15);
            this.lObjB.TabIndex = 4;
            this.lObjB.Text = "Объект 2";
            // 
            // cbObjA
            // 
            this.cbObjA.FormattingEnabled = true;
            this.cbObjA.Location = new System.Drawing.Point(4, 57);
            this.cbObjA.Name = "cbObjA";
            this.cbObjA.Size = new System.Drawing.Size(244, 23);
            this.cbObjA.TabIndex = 5;
            this.cbObjA.SelectedIndexChanged += new System.EventHandler(this.cbObjA_SelectedIndexChanged);
            // 
            // cbObjB
            // 
            this.cbObjB.FormattingEnabled = true;
            this.cbObjB.Location = new System.Drawing.Point(596, 57);
            this.cbObjB.Name = "cbObjB";
            this.cbObjB.Size = new System.Drawing.Size(267, 23);
            this.cbObjB.TabIndex = 6;
            // 
            // cbCondition
            // 
            this.cbCondition.FormattingEnabled = true;
            this.cbCondition.Location = new System.Drawing.Point(303, 57);
            this.cbCondition.Name = "cbCondition";
            this.cbCondition.Size = new System.Drawing.Size(267, 23);
            this.cbCondition.TabIndex = 8;
            // 
            // lConnection
            // 
            this.lConnection.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lConnection.Location = new System.Drawing.Point(0, 0);
            this.lConnection.Name = "lConnection";
            this.lConnection.Size = new System.Drawing.Size(872, 27);
            this.lConnection.TabIndex = 2;
            this.lConnection.Text = "Связь:";
            this.lConnection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ObjectConnectionForm
            // 
            this.AcceptButton = this.bOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(872, 148);
            this.Controls.Add(this.cbCondition);
            this.Controls.Add(this.lCond);
            this.Controls.Add(this.cbObjB);
            this.Controls.Add(this.cbObjA);
            this.Controls.Add(this.lObjB);
            this.Controls.Add(this.lObjA);
            this.Controls.Add(this.lConnection);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ObjectConnectionForm";
            this.Text = "Создание связи объектов";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label lConnection;
        private System.Windows.Forms.Label lObjA;
        private System.Windows.Forms.Label lObjB;
        private System.Windows.Forms.ComboBox cbObjA;
        private System.Windows.Forms.ComboBox cbObjB;
        private System.Windows.Forms.ComboBox cbCondition;
        private System.Windows.Forms.Label lCond;
    }
}