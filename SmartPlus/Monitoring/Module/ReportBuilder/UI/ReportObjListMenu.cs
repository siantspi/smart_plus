﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SmartPlus.ReportBuilder.Base;
using System.Drawing;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.ReportBuilder.UI
{
    public delegate void ReportObjListItemSelectDelegate();

    class ReportObjListMenu : ContextMenuStrip
    {
        MainForm MainForm;
        public int OwnerIndex;
        public SmartPlus.ReportBuilder.Base.DataObject SelObject;
        public event ReportObjListItemSelectDelegate ReportObjItemSelected;
        List<ToolStripMenuItem> GroupItems;

        public ReportObjListMenu(MainForm MainForm)
        {
            this.MainForm = MainForm;
            SelObject = null;
            this.ShowImageMargin = false;
            this.VScroll = true;
            GroupItems = new List<ToolStripMenuItem>();
        }

        public void Show(ReportObject Object, int ObjectIndex, Point Location)
        {
            SelObject = null;
            this.OwnerIndex = ObjectIndex;
            this.Location = Location;
            this.Items.Clear();
            this.GroupItems.Clear();
            if (Object.DataObjects.Count != Object.ReportDataObjects.Count)
            {
                for (int i = 0; i < Object.DataObjects.Count; i++)
                {
                    if ((Object.DataObjects[i].Type != DataObjectTypes.None) && (Object.ReportDataObjects.IndexOf(Object.DataObjects[i]) == -1))
                    {
                        if (Object.DataObjects[i].GroupName.Length == 0)
                        {
                            bool skip = false;
                            if (Object.Type == ObjectType.Well &&
                                (Object.DataObjects[i].Type == DataObjectTypes.OilField_Name || Object.DataObjects[i].Type == DataObjectTypes.NGDU_Name))
                            {
                                for (int j = 0; j < Object.Connections.Count; j++)
                                {
                                    if (Object.Connections[j].TargetObject.Type == ObjectType.OilField)
                                    {
                                        skip = true;
                                    }
                                }
                            }
                            if (!skip)
                            {
                                this.Items.Add(Object.DataObjects[i].ToString());
                                this.Items[this.Items.Count - 1].Click += new EventHandler(item_Click);
                                this.Items[this.Items.Count - 1].Tag = Object.DataObjects[i];
                            }
                        }
                        else 
                        {
                            int ind = -1;
                            for(int j = 0; j < GroupItems.Count;j++) 
                            {
                                if(GroupItems[j].Text.StartsWith(Object.DataObjects[i].GroupName))
                                {
                                    ind = j;
                                    break;
                                }
                            }
                            if(ind == -1)
                            {
                                ind = GroupItems.Count;
                                GroupItems.Add((ToolStripMenuItem)this.Items.Add(Object.DataObjects[i].GroupName));
                                ((ToolStripDropDownMenu)(GroupItems[ind].DropDown)).ShowImageMargin = false;
                            }
                            GroupItems[ind].DropDownItems.Add(Object.DataObjects[i].ToString());
                            GroupItems[ind].DropDownItems[GroupItems[ind].DropDownItems.Count - 1].Click += new EventHandler(item_Click);
                            GroupItems[ind].DropDownItems[GroupItems[ind].DropDownItems.Count - 1].Tag = Object.DataObjects[i];
                        }
                    }
                    else if ((this.Items.Count > 0) && (Object.DataObjects[i].Type == DataObjectTypes.None))
                    {
                        this.Items.Add("-");
                    }
                }
                Location.Y -= 16;
                this.Show(MainForm, Location);
            }
        }

        void item_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            SelObject = null;
            if (item.Tag != null) SelObject = (SmartPlus.ReportBuilder.Base.DataObject)item.Tag;
            if (SelObject.DictType != DICTIONARY_ITEM_TYPE.NONE)
            {
                SelObject.Dictionary = (DictionaryBase)MainForm.GetLastProject().DictList.GetDictionary(SelObject.DictType);
            }
            if (ReportObjItemSelected != null) ReportObjItemSelected();
        }
    }
}
