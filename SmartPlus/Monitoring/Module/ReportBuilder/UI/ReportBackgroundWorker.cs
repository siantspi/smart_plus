﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace SmartPlus.ReportBuilder.UI
{
    public class ReportBackgroundWorker : BackgroundWorker
    {
        public int AccumProgress;
        public WorkerProgressObject ProgressObject;
        public ReportBackgroundWorker()
        {
            this.WorkerSupportsCancellation = true;
            this.WorkerReportsProgress = true;
        }
    }

    public enum WorkerProgressObject
    {
        OilField,
        Area,
        Well,
        Area_Well
    }
}
