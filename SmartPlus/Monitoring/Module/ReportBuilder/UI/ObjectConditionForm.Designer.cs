﻿namespace SmartPlus.ReportBuilder.UI
{
    partial class ObjectConditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCondition = new System.Windows.Forms.ComboBox();
            this.lCond = new System.Windows.Forms.Label();
            this.cbValues = new System.Windows.Forms.ComboBox();
            this.cbObject = new System.Windows.Forms.ComboBox();
            this.lValue = new System.Windows.Forms.Label();
            this.lObjectFields = new System.Windows.Forms.Label();
            this.lObject = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.tbValue = new System.Windows.Forms.TextBox();
            this.dtPicker = new System.Windows.Forms.DateTimePicker();
            this.tbInt1 = new System.Windows.Forms.TextBox();
            this.tbInt2 = new System.Windows.Forms.TextBox();
            this.lInt = new System.Windows.Forms.Label();
            this.SettingsGroup = new System.Windows.Forms.GroupBox();
            this.cbUseWellListDate = new System.Windows.Forms.CheckBox();
            this.cbUseDateStart = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ShiftDateUpDown = new System.Windows.Forms.NumericUpDown();
            this.cbShiftDate = new System.Windows.Forms.CheckBox();
            this.SettingsGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftDateUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // cbCondition
            // 
            this.cbCondition.FormattingEnabled = true;
            this.cbCondition.Location = new System.Drawing.Point(254, 58);
            this.cbCondition.Name = "cbCondition";
            this.cbCondition.Size = new System.Drawing.Size(137, 27);
            this.cbCondition.TabIndex = 17;
            // 
            // lCond
            // 
            this.lCond.AutoSize = true;
            this.lCond.Location = new System.Drawing.Point(251, 40);
            this.lCond.Name = "lCond";
            this.lCond.Size = new System.Drawing.Size(68, 21);
            this.lCond.TabIndex = 16;
            this.lCond.Text = "Условие";
            // 
            // cbValues
            // 
            this.cbValues.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbValues.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbValues.DropDownHeight = 200;
            this.cbValues.DropDownWidth = 300;
            this.cbValues.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbValues.FormattingEnabled = true;
            this.cbValues.IntegralHeight = false;
            this.cbValues.Location = new System.Drawing.Point(399, 58);
            this.cbValues.Name = "cbValues";
            this.cbValues.Size = new System.Drawing.Size(234, 27);
            this.cbValues.TabIndex = 15;
            // 
            // cbObject
            // 
            this.cbObject.FormattingEnabled = true;
            this.cbObject.Location = new System.Drawing.Point(12, 58);
            this.cbObject.Name = "cbObject";
            this.cbObject.Size = new System.Drawing.Size(236, 27);
            this.cbObject.TabIndex = 14;
            this.cbObject.SelectedIndexChanged += new System.EventHandler(this.cbObject_SelectedIndexChanged);
            // 
            // lValue
            // 
            this.lValue.AutoSize = true;
            this.lValue.Location = new System.Drawing.Point(396, 40);
            this.lValue.Name = "lValue";
            this.lValue.Size = new System.Drawing.Size(82, 21);
            this.lValue.TabIndex = 13;
            this.lValue.Text = "Значение:";
            // 
            // lObjectFields
            // 
            this.lObjectFields.AutoSize = true;
            this.lObjectFields.Location = new System.Drawing.Point(9, 40);
            this.lObjectFields.Name = "lObjectFields";
            this.lObjectFields.Size = new System.Drawing.Size(75, 21);
            this.lObjectFields.TabIndex = 12;
            this.lObjectFields.Text = "Объект 1";
            // 
            // lObject
            // 
            this.lObject.Dock = System.Windows.Forms.DockStyle.Top;
            this.lObject.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lObject.Location = new System.Drawing.Point(0, 0);
            this.lObject.Name = "lObject";
            this.lObject.Size = new System.Drawing.Size(644, 27);
            this.lObject.TabIndex = 11;
            this.lObject.Text = "Объект:";
            this.lObject.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(542, 198);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(95, 30);
            this.bCancel.TabIndex = 10;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOK.Location = new System.Drawing.Point(441, 198);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(95, 30);
            this.bOK.TabIndex = 9;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            // 
            // tbValue
            // 
            this.tbValue.Location = new System.Drawing.Point(397, 58);
            this.tbValue.Name = "tbValue";
            this.tbValue.Size = new System.Drawing.Size(234, 27);
            this.tbValue.TabIndex = 20;
            this.tbValue.Visible = false;
            // 
            // dtPicker
            // 
            this.dtPicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPicker.Location = new System.Drawing.Point(397, 58);
            this.dtPicker.Name = "dtPicker";
            this.dtPicker.Size = new System.Drawing.Size(234, 27);
            this.dtPicker.TabIndex = 22;
            this.dtPicker.Visible = false;
            // 
            // tbInt1
            // 
            this.tbInt1.Location = new System.Drawing.Point(397, 58);
            this.tbInt1.Name = "tbInt1";
            this.tbInt1.Size = new System.Drawing.Size(107, 27);
            this.tbInt1.TabIndex = 23;
            this.tbInt1.Visible = false;
            // 
            // tbInt2
            // 
            this.tbInt2.Location = new System.Drawing.Point(527, 58);
            this.tbInt2.Name = "tbInt2";
            this.tbInt2.Size = new System.Drawing.Size(106, 27);
            this.tbInt2.TabIndex = 24;
            this.tbInt2.Visible = false;
            // 
            // lInt
            // 
            this.lInt.AutoSize = true;
            this.lInt.Location = new System.Drawing.Point(510, 61);
            this.lInt.Name = "lInt";
            this.lInt.Size = new System.Drawing.Size(15, 21);
            this.lInt.TabIndex = 25;
            this.lInt.Text = "-";
            this.lInt.Visible = false;
            // 
            // SettingsGroup
            // 
            this.SettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsGroup.Controls.Add(this.cbUseWellListDate);
            this.SettingsGroup.Controls.Add(this.cbUseDateStart);
            this.SettingsGroup.Controls.Add(this.label1);
            this.SettingsGroup.Controls.Add(this.ShiftDateUpDown);
            this.SettingsGroup.Controls.Add(this.cbShiftDate);
            this.SettingsGroup.Location = new System.Drawing.Point(13, 88);
            this.SettingsGroup.Name = "SettingsGroup";
            this.SettingsGroup.Size = new System.Drawing.Size(619, 105);
            this.SettingsGroup.TabIndex = 28;
            this.SettingsGroup.TabStop = false;
            this.SettingsGroup.Text = " Дополнительные настройки ";
            // 
            // cbUseWellListDate
            // 
            this.cbUseWellListDate.AutoSize = true;
            this.cbUseWellListDate.Location = new System.Drawing.Point(6, 47);
            this.cbUseWellListDate.Name = "cbUseWellListDate";
            this.cbUseWellListDate.Size = new System.Drawing.Size(305, 25);
            this.cbUseWellListDate.TabIndex = 33;
            this.cbUseWellListDate.Text = "Использовать дату из списка скважин";
            this.cbUseWellListDate.UseVisualStyleBackColor = true;
            this.cbUseWellListDate.CheckedChanged += new System.EventHandler(this.cbUseWellListDate_CheckedChanged);
            // 
            // cbUseDateStart
            // 
            this.cbUseDateStart.AutoSize = true;
            this.cbUseDateStart.Location = new System.Drawing.Point(6, 22);
            this.cbUseDateStart.Name = "cbUseDateStart";
            this.cbUseDateStart.Size = new System.Drawing.Size(227, 25);
            this.cbUseDateStart.TabIndex = 32;
            this.cbUseDateStart.Text = "Использовать дату запуска";
            this.cbUseDateStart.UseVisualStyleBackColor = true;
            this.cbUseDateStart.CheckedChanged += new System.EventHandler(this.cbDateStart_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(305, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 21);
            this.label1.TabIndex = 31;
            this.label1.Text = "мес.";
            // 
            // ShiftDateUpDown
            // 
            this.ShiftDateUpDown.Enabled = false;
            this.ShiftDateUpDown.Location = new System.Drawing.Point(170, 72);
            this.ShiftDateUpDown.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.ShiftDateUpDown.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.ShiftDateUpDown.Name = "ShiftDateUpDown";
            this.ShiftDateUpDown.Size = new System.Drawing.Size(120, 27);
            this.ShiftDateUpDown.TabIndex = 30;
            // 
            // cbShiftDate
            // 
            this.cbShiftDate.AutoSize = true;
            this.cbShiftDate.Location = new System.Drawing.Point(6, 73);
            this.cbShiftDate.Name = "cbShiftDate";
            this.cbShiftDate.Size = new System.Drawing.Size(187, 25);
            this.cbShiftDate.TabIndex = 29;
            this.cbShiftDate.Text = "Смещение по дате на";
            this.cbShiftDate.UseVisualStyleBackColor = true;
            this.cbShiftDate.CheckedChanged += new System.EventHandler(this.cbShiftDate_CheckedChanged);
            // 
            // ObjectConditionForm
            // 
            this.AcceptButton = this.bOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(644, 240);
            this.Controls.Add(this.lInt);
            this.Controls.Add(this.tbInt2);
            this.Controls.Add(this.tbInt1);
            this.Controls.Add(this.tbValue);
            this.Controls.Add(this.cbCondition);
            this.Controls.Add(this.cbValues);
            this.Controls.Add(this.cbObject);
            this.Controls.Add(this.lObject);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.dtPicker);
            this.Controls.Add(this.SettingsGroup);
            this.Controls.Add(this.lValue);
            this.Controls.Add(this.lCond);
            this.Controls.Add(this.lObjectFields);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ObjectConditionForm";
            this.Text = "Установка условия объекта";
            this.SettingsGroup.ResumeLayout(false);
            this.SettingsGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftDateUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCondition;
        private System.Windows.Forms.Label lCond;
        private System.Windows.Forms.ComboBox cbValues;
        private System.Windows.Forms.ComboBox cbObject;
        private System.Windows.Forms.Label lValue;
        private System.Windows.Forms.Label lObjectFields;
        private System.Windows.Forms.Label lObject;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.TextBox tbValue;
        private System.Windows.Forms.DateTimePicker dtPicker;
        private System.Windows.Forms.TextBox tbInt1;
        private System.Windows.Forms.TextBox tbInt2;
        private System.Windows.Forms.Label lInt;
        private System.Windows.Forms.GroupBox SettingsGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ShiftDateUpDown;
        private System.Windows.Forms.CheckBox cbShiftDate;
        private System.Windows.Forms.CheckBox cbUseWellListDate;
        private System.Windows.Forms.CheckBox cbUseDateStart;
    }
}