﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartPlus.ReportBuilder.Base;
using SmartPlus.ReportBuilder.Objects;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using SmartPlus.MSOffice;

namespace SmartPlus.ReportBuilder.UI
{
    class ReportForm : Form
    {
        //todo: Добавить сохранение/загрузку схемы Конструктора отчетов

        MainForm MainForm;
        private Button bExcel;
        private Button bCancel;
        private DataGridViewVirtual dgvReport;
        ReportBackgroundWorker worker;
        Project CurrentProject;
        private int ProgressMax;

        ReportFormCell[][] Table;
        int RowsCount;

        List<ReportObject> CurrentObjects;
        List<ReportObject> ColumnsObjects;
        List<DataGridViewColumn> FillColumns;

        private ProgressBar pBar;
        int WorkerMode;
        bool DrawWaitIconMode;
        string DrawWaitText;
        Font DrawWaitFont;
        int wait_angle;
        Timer tmrWait;

        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvReport = new SmartPlus.DataGridViewVirtual();
            this.bExcel = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.pBar = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvReport
            // 
            this.dgvReport.AllowUserToAddRows = false;
            this.dgvReport.AllowUserToDeleteRows = false;
            this.dgvReport.AllowUserToResizeRows = false;
            this.dgvReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReport.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dgvReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReport.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvReport.EnableHeadersVisualStyles = false;
            this.dgvReport.Location = new System.Drawing.Point(0, 0);
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.RowHeadersVisible = false;
            this.dgvReport.Size = new System.Drawing.Size(794, 400);
            this.dgvReport.TabIndex = 0;
            this.dgvReport.VirtualMode = true;
            // 
            // bExcel
            // 
            this.bExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bExcel.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bExcel.Location = new System.Drawing.Point(586, 406);
            this.bExcel.Name = "bExcel";
            this.bExcel.Size = new System.Drawing.Size(95, 30);
            this.bExcel.TabIndex = 1;
            this.bExcel.Text = "В Excel";
            this.bExcel.UseVisualStyleBackColor = true;
            this.bExcel.Click += new System.EventHandler(this.bExcel_Click);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bCancel.Location = new System.Drawing.Point(687, 406);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(95, 30);
            this.bCancel.TabIndex = 2;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // pBar
            // 
            this.pBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pBar.Location = new System.Drawing.Point(8, 411);
            this.pBar.Name = "pBar";
            this.pBar.Size = new System.Drawing.Size(568, 23);
            this.pBar.TabIndex = 3;
            // 
            // ReportForm
            // 
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(794, 448);
            this.Controls.Add(this.pBar);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bExcel);
            this.Controls.Add(this.dgvReport);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "ReportForm";
            this.Text = "Отчет для выгрузки в Excel";
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.ResumeLayout(false);
        }

        public ReportForm(MainForm Parent)
        {
            InitializeComponent();
            this.Owner = Parent;
            this.MainForm = Parent;
            this.CurrentObjects = null;
            this.CurrentProject = null;
            FillColumns = new List<DataGridViewColumn>();
            Table = null;
            ColumnsObjects = null;

            // DATAGRIDVIEW
            dgvReport.RowTemplate.Height = 16;
            dgvReport.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvReport.EnableHeadersVisualStyles = false;
            dgvReport.AllowUserToResizeRows = false;
            dgvReport.AllowUserToAddRows = false;
            dgvReport.AllowUserToDeleteRows = false;
            dgvReport.RowHeadersVisible = false;
            dgvReport.ReadOnly = true;
            DrawWaitFont = new Font("Tahoma", 10, FontStyle.Bold);
            DrawWaitText = string.Empty;
            tmrWait = new Timer();
            tmrWait.Enabled = false;
            tmrWait.Interval = 100;
            tmrWait.Tick +=new EventHandler(tmrWait_Tick);

            worker = new ReportBackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            this.FormClosing += new FormClosingEventHandler(ReportForm_FormClosing);

            // GRID VIRTUAL MODE
            dgvReport.CellValueNeeded += new DataGridViewCellValueEventHandler(dgvReport_CellValueNeeded);
            dgvReport.Paint += new PaintEventHandler(dgvReport_Paint);
            dgvReport.OnStartCopyToClipboard += new OnStartCopyToClipboardDelegate(dgvReport_OnStartCopyToClipboard);
            dgvReport.OnEndCopyToClipboard += new OnEndCopyToClipboardDelegate(dgvReport_OnEndCopyToClipboard);
        }

        #region DGV EVENTS
        void dgvReport_OnStartCopyToClipboard()
        {
            DrawWaitText = "Подождите...идет копирование в буфер обмена";
            StartDrawWaitIcon();
        }
        void dgvReport_OnEndCopyToClipboard()
        {
            StopDrawWaitIcon();
            dgvReport.Invalidate();
        }
        #endregion

        void ClearData()
        {
            if (ColumnsObjects != null) ColumnsObjects.Clear();
            if (FillColumns != null) FillColumns.Clear();
            if(Table != null)
            {
                for (int i = 0; i < Table.Length; i++)
                {
                    Table[i] = null;
                }
                Table = null;
            }
            GC.GetTotalMemory(true);
        }
        #region EVENTS
        void ReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.Cancel)
            {
                if (worker.IsBusy)
                {
                    worker.CancelAsync();
                    bCancel.Enabled = false;
                    bExcel.Enabled = false;
                    e.Cancel = true;
                }
                ClearData();
            }
        }
        #endregion

        #region WORKER
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (this.CurrentObjects != null && this.CurrentProject != null)
            {
                switch(WorkerMode)
                {
                    case 0:
                        CreateColumns(CurrentObjects, CurrentProject);
                        break;
                    case 1:
                        SetProgressBarMax(worker, e, CurrentObjects, CurrentProject);
                        break;
                    case 2:
                        FillTable(worker, e, CurrentObjects, CurrentProject);
                        break;
                    case 3:
                        worker.AccumProgress = 0;
                        ExportTableToExcel(worker, e);
                        break;
                }
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState == null)
            {
                string str = (WorkerMode == 3) ? "Идет выгрузка в Excel" : "Идет обработка данных";
                if(worker.AccumProgress < pBar.Maximum) worker.AccumProgress += e.ProgressPercentage;
                if (worker.AccumProgress > pBar.Maximum) worker.AccumProgress = pBar.Maximum;
                this.Text = string.Format("Отчет для выгрузки в Excel (Идет обработка данных {0:0} %)", Math.Round(worker.AccumProgress * 100d / pBar.Maximum, 0));
                DrawWaitText = string.Format("Подождите...{0} {1:0} %", str, Math.Round(worker.AccumProgress * 100d / pBar.Maximum, 0));
                this.pBar.Value = worker.AccumProgress;
            }
            if ((WorkerMode == 3) && (e.UserState != null))
            {
                DrawWaitText = ((WorkerState)e.UserState).WorkMainTitle;
                dgvReport.Invalidate();
            }
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Text = "Отчет для выгрузки в Excel";
            pBar.Value = pBar.Maximum;
            bExcel.Enabled = true;
            StopDrawWaitIcon();
            if (e.Error != null)
            {
                Exception ex = e.Error;
                string InnerMessage = ex.Message;
                while (ex.InnerException != null)
                {
                    InnerMessage += "\n" + ex.InnerException.Message;
                    ex = ex.InnerException;
                }
                MessageBox.Show(this, "Выгрузка отчета завершилась с ошибкой!\n" + InnerMessage, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (e.Cancelled)
            {
                FillColumns.Clear();
                ClearProjectData();
                this.Close();
            }
            else
            {
                if (WorkerMode == 0)
                {
                    dgvReport.Columns.Clear();
                    for (int i = 0; i < FillColumns.Count; i++)
                    {
                        dgvReport.Columns.Add(FillColumns[i]);
                    }
                    WorkerMode = 1;
                    StartDrawWaitIcon();
                    worker.RunWorkerAsync();
                }
                else if (WorkerMode == 1)
                {
                    if (ProgressMax < 0) ProgressMax = 100;
                    pBar.Maximum = ProgressMax;
                    WorkerMode = 2;
                    StartDrawWaitIcon();
                    worker.RunWorkerAsync();
                }
                else if (WorkerMode == 2)
                {
                    dgvReport.RowCount = RowsCount;
                    if (ColumnsObjects != null)
                    {
                        ColumnsObjects.Clear();
                        ColumnsObjects = null;
                    }
                }
            }
            GC.GetTotalMemory(true);
        }
        #endregion

        public void ShowTable(List<ReportObject> Objects, Project Project)
        {
            CurrentObjects = Objects;
            CurrentProject = Project;
            dgvReport.RowCount = 0;
            Table = null;
            WorkerMode = 0;
            pBar.Value = 0;
            bExcel.Enabled = false;
            worker.RunWorkerAsync();
            StartDrawWaitIcon();
            this.ShowDialog();
        }

        #region VIRTUAL MODE
        void dgvReport_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if ((Table != null) && (e.RowIndex < RowsCount) && (Table[e.RowIndex] != null))
            {
                if (e.ColumnIndex < Table[e.RowIndex].Length)
                {
                    e.Value = Table[e.RowIndex][e.ColumnIndex].Value;
                }
            }
        }
        #endregion

        #region PROGRESS BAR
        void SetProgressBarMax(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportObject> Objects, Project Project)
        {
            ProgressMax = -1;
            if ((Project != null) && (ColumnsObjects.Count > 0) && (FillColumns.Count > 0))
            {
                ReportOilField reportOilfield;
                ReportWell reportWell;
                ReportArea reportArea;
                if (ColumnsObjects[0].Type == ObjectType.OilField)
                {
                    reportOilfield = (ReportOilField)ColumnsObjects[0];
                    OilField of;
                    for (int i = 1; i < Project.OilFields.Count; i++)
                    {
                        of = (OilField)Project.OilFields[i];
                        if (reportOilfield.TestConditions(of))
                        {
                            if(ProgressMax < 0) ProgressMax = 0;
                            ProgressMax++;
                            for (int j = 0; j < reportOilfield.ConnectedObjects.Count; j++)
                            {
                                if (reportOilfield.ConnectedObjects[j].IsProjectObject)
                                {
                                    switch (reportOilfield.ConnectedObjects[j].Type)
                                    {
                                        case ObjectType.Well:
                                            reportWell = (ReportWell)reportOilfield.ConnectedObjects[j];
                                            for (int k = 0; k < of.Wells.Count; k++)
                                            {
                                                if (reportWell.TestConditions(of.Wells[k]))
                                                {
                                                    ProgressMax++;
                                                }
                                            }
                                            break;
                                        case ObjectType.Area:
                                            Area area;
                                            reportArea = (ReportArea)reportOilfield.ConnectedObjects[j];
                                            for (int k = 0; k < of.Areas.Count; k++)
                                            {
                                                area = (Area)of.Areas[k];
                                                if (reportArea.TestConditions(area))
                                                {
                                                    for (int p = 0; p < reportArea.ConnectedObjects.Count; p++)
                                                    {
                                                        if (reportArea.ConnectedObjects[p].IsProjectObject)
                                                        {
                                                            switch (reportArea.ConnectedObjects[p].Type)
                                                            {
                                                                case ObjectType.Well:
                                                                    reportWell = (ReportWell)reportArea.ConnectedObjects[p];
                                                                    if (area.WellList != null)
                                                                    {
                                                                        for (int r = 0; r < area.WellList.Length; r++)
                                                                        {
                                                                            if (reportWell.TestConditions((Well)area.WellList[r]))
                                                                            {
                                                                                ProgressMax++;
                                                                            }
                                                                        }
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (ColumnsObjects[0].Type == ObjectType.WellList)
                {
                    ReportWellList reportWellList = (ReportWellList)ColumnsObjects[0];
                    C2DLayer layer = (C2DLayer)ColumnsObjects[0].CurrentObject;
                    if(reportWellList.ConnectedObjects.Count > 0)
                    {
                        if(reportWellList.ConnectedObjects[0].Type == ObjectType.Well)
                        {
                            reportWell = (ReportWell)reportWellList.ConnectedObjects[0];
                            ProgressMax = 0;
                            for(int i = 0; i < layer.ObjectsList.Count;i++)
                            {
                                if (reportWell.TestConditions(((PhantomWell)layer.ObjectsList[i]).srcWell))
                                {
                                    ProgressMax++;
                                }
                            }
                        }
                    }
                }
            }
        }
        void SetProgressBarMax0()
        {
            pBar.Maximum = 100;
            if((CurrentObjects != null) && (CurrentObjects.Count > 0) && (CurrentProject != null))
            {
                bool OilFieldUsed = CurrentObjects[0].Type == ObjectType.OilField;
                bool WellUsed = (CurrentObjects[0].Type == ObjectType.Well || CurrentObjects[0].TestConnectedObj(ObjectType.Well));
                bool AreaUsed = (CurrentObjects[0].Type == ObjectType.Area || CurrentObjects[0].TestConnectedObj(ObjectType.Area));

                int max = 0;
                if (CurrentObjects[0].Type == ObjectType.WellList)
                {
                    if (WellUsed)
                    {
                        max += ((C2DLayer)CurrentObjects[0].CurrentObject).ObjectsList.Count;
                        worker.ProgressObject = WorkerProgressObject.Well;
                    }
                }
                else
                {
                    if (WellUsed)
                    {
                        for (int i = 1; i < CurrentProject.OilFields.Count; i++)
                        {
                            max += ((OilField)CurrentProject.OilFields[i]).Wells.Count;
                        }
                        worker.ProgressObject = WorkerProgressObject.Well;
                    }
                    if (AreaUsed)
                    {
                        for (int i = 1; i < CurrentProject.OilFields.Count; i++)
                        {
                            max += ((OilField)CurrentProject.OilFields[i]).Areas.Count;
                        }
                        worker.ProgressObject = WorkerProgressObject.Area;
                    }
                    if (WellUsed && AreaUsed) worker.ProgressObject = WorkerProgressObject.Area_Well;
                    if (max == 0)
                    {
                        max = CurrentProject.OilFields.Count;
                        worker.ProgressObject = WorkerProgressObject.OilField;
                    }
                }
                pBar.Maximum = max;
            }
        }
        #endregion

        #region CREATE COLUMNS
        void FillConnectedObjects(ReportObject Object, ref List<ReportObject> ObjList)
        {
            for (int i = 0; i < Object.ConnectedObjects.Count; i++)
            {
                ObjList.Add(Object.ConnectedObjects[i]);
                FillConnectedObjects(Object.ConnectedObjects[i], ref ObjList);
            }
        }
        public List<ReportObject> CreateReportColumns(List<ReportObject> Objects)
        {
            ReportObject mainObj = null;
            int i, j, k;
            bool find = false;
            List<ReportObject> objList = new List<ReportObject>();
            for (i = 0; i < Objects.Count; i++)
            {
                if (Objects[i].IsProjectObject)
                {
                    mainObj = Objects[i];
                    if(Objects[i].ConnectedObjects.Count > 0)
                    {
                        find = false;
                        for (j = 0; j < Objects.Count; j++)
                        {
                            if (Objects[j].ConnectedObjects.IndexOf(mainObj) != -1)
                            {
                                find = true;
                                break;
                            }
                        }
                        if (!find)
                        {
                            mainObj = Objects[i];
                            break;
                        }
                    }
                }
            }
            if (mainObj != null)
            {
                objList.Add(mainObj);
                FillConnectedObjects(mainObj, ref objList);
                if (objList.Count > 0)
                {
                    this.dgvReport.Columns.Clear();
                    DataGridViewColumn column;
                    DataGridViewCell cellTemplate = new DataGridViewTextBoxCell();
                    for (j = 0; j < objList.Count; j++)
                    {
                        for (k = 0; k < objList[j].ReportDataObjects.Count; k++)
                        {
                            column = new DataGridViewColumn(cellTemplate);
                            column.HeaderText = objList[j].ReportDataObjects[k].Text;
                            FillColumns.Add(column);
                        }
                    }
                }
            }
            return objList;
        }
        void CreateColumns(List<ReportObject> Objects, Project Project)
        {
            ColumnsObjects = CreateReportColumns(Objects);
        }
        #endregion

        #region FILL TABLE
        ReportFormCell[] CreateTableRow()
        {
            ReportFormCell[] row = new ReportFormCell[dgvReport.Columns.Count];
            for (int i = 0; i < dgvReport.Columns.Count; i++)
            {
                row[i] = new ReportFormCell();
            }
            return row;
        }
        void FillTable(ReportBackgroundWorker worker, DoWorkEventArgs e, List<ReportObject> Objects, Project Project)
        {
            if ((Project != null) && (ColumnsObjects.Count > 0) && (FillColumns.Count > 0))
            {
                List<ReportFormCell[]> table = new List<ReportFormCell[]>();
                if (ColumnsObjects[0].Type == ObjectType.OilField)
                {
                    ReportFormCell[] InitRow = CreateTableRow();
                    for (int i = 1; i < Project.OilFields.Count; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }

                        ColumnsObjects[0].ParentObject = Project.OilFields[i];
                        ColumnsObjects[0].FillTable(worker, e, table, (OilField)Project.OilFields[i], InitRow, 0);
                        worker.ReportProgress(i);
                        //switch (worker.ProgressObject)
                        //{
                        //    case WorkerProgressObject.Well:
                        //        worker.AccumProgres += ((OilField)Project.OilFields[i]).WellList.Length;
                        //        break;
                        //    case WorkerProgressObject.Area:
                        //        worker.AccumProgres += ((OilField)Project.OilFields[i]).AreaList.Count;
                        //        break;
                        //    case WorkerProgressObject.Area_Well:
                        //        worker.AccumProgres += ((OilField)Project.OilFields[i]).WellList.Length;
                        //        worker.AccumProgres += ((OilField)Project.OilFields[i]).AreaList.Count;
                        //        break;
                        //}
                    }
                }
                else if (ColumnsObjects[0].Type == ObjectType.WellList)
                {
                    ReportFormCell[] InitRow = CreateTableRow();
                    ColumnsObjects[0].FillTable(worker, e, table, Project, InitRow, 0);
                    //switch (worker.ProgressObject)
                    //{
                    //    case WorkerProgressObject.Well:
                    //        worker.AccumProgres += ((C2DLayer)ColumnsObjects[0].CurrentObject)._ObjectsList.Count;
                    //        break;
                    //}
                }
                if (table.Count > 0)
                {
                    Table = new ReportFormCell[table.Count][];
                    for (int i = 0; i < table.Count; i++)
                    {
                        Table[i] = table[i];
                    }
                    RowsCount = Table.Length;
                }
            }
        }
        #endregion

        #region CLEAR PROJECT DATA
        void ClearProjectData()
        {
            if (CurrentProject != null)
            {
                for (int i = 0; i < CurrentProject.OilFields.Count; i++)
                {
                    ((OilField)CurrentProject.OilFields[i]).ClearMerData(false);
                    ((OilField)CurrentProject.OilFields[i]).ClearGisData(false);
                    ((OilField)CurrentProject.OilFields[i]).ClearPerfData(false);
                    ((OilField)CurrentProject.OilFields[i]).ClearSumParams(false);
                    ((OilField)CurrentProject.OilFields[i]).ClearSumArea(false);
                    ((OilField)CurrentProject.OilFields[i]).ClearGTMData(false);
                    ((OilField)CurrentProject.OilFields[i]).ClearWellActionData(false);
                    ((OilField)CurrentProject.OilFields[i]).ClearWellResearchData(false);
                }
            }
        }
        #endregion

        #region DRAW WAIT ICON
        public void StartDrawWaitIcon()
        {
            DrawWaitIconMode = true;
            dgvReport.Enabled = false;
            tmrWait.Start();
            this.Invalidate();
        }
        public void StopDrawWaitIcon()
        {
            DrawWaitIconMode = false;
            dgvReport.Enabled = true;
            DrawWaitText = string.Empty;
            tmrWait.Stop();
        }
        void tmrWait_Tick(object sender, EventArgs e)
        {
            if (DrawWaitIconMode)
            {
                wait_angle++;
                if (wait_angle > 11) wait_angle = 0;
                dgvReport.Invalidate();
            }
            else
            {
                tmrWait.Stop();
            }
        }
        public void DrawWaitIcon(Graphics gr)
        {
            if (this.DrawWaitIconMode)
            {
                gr.Clear(Color.White);
                PointF center = new PointF((float)(dgvReport.ClientRectangle.Right / 2), (float)(dgvReport.ClientRectangle.Bottom / 2));
                gr.DrawImage(MainForm.bmpWait[wait_angle], center.X - 16, center.Y - 16, 32, 32);
                
                SizeF size = gr.MeasureString(DrawWaitText, DrawWaitFont, PointF.Empty, StringFormat.GenericTypographic);
                gr.DrawString(DrawWaitText, DrawWaitFont, Brushes.Black, center.X - size.Width / 2, center.Y + 16);
            }
        }
        void dgvReport_Paint(object sender, PaintEventArgs e)
        {
            DrawWaitIcon(e.Graphics);
        }
        #endregion

        #region Excel
        private void bExcel_Click(object sender, EventArgs e)
        {
            bool work = true;
            if ((Table.Length > Excel.Max_Rows + 1) && (MessageBox.Show(string.Format("В таблице более {0:#,###} строк для вывода.\nMicrosoft Excel позволяет выгрузить только {1:#,###} строк на лист.\nПродолжить выгрузку?", Excel.Max_Rows + 1, Excel.Max_Rows + 1), "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel))
            {
                work = false;
            }
            if (work)
            {
                WorkerMode = 3;
                pBar.Maximum = 100;
                StartDrawWaitIcon();
                worker.RunWorkerAsync();
            }
        }
        void ExportTableToExcel(BackgroundWorker worker, DoWorkEventArgs e)
        {
            Excel excel  = null;
            try
            {
                excel = new Excel();
                excel.NewWorkbook();
                excel.SetActiveSheet(0);
                excel.EnableEvents = false;

                int i, j, k;
                worker.ReportProgress(0);
                
                if (FillColumns != null)
                {
                    for (i = 0; i < FillColumns.Count; i++)
                    {
                        excel.SetValue(0, i, FillColumns[i].HeaderText);
                    }
                }
                int progress = 1;
                int endRow = -1;
                bool exit = false;
                worker.ReportProgress(1);
                object[,] varRow = (object[,])Array.CreateInstance(typeof(object), new int[2] { 50, FillColumns.Count }, new int[2] { 0, 0 });
                int maxRows = Table.Length;
                if (maxRows > Excel.Max_Rows) maxRows = Excel.Max_Rows;
                for (i = 0; i < maxRows; i += 50)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        excel.Quit();
                        return;
                    }
                    for (k = 0; k < 50; k++)
                    {
                        if ((i + k >= maxRows) || (Table[i + k] == null))
                        {
                            endRow = i + k - 1;
                            break;
                        }
                        else
                        {
                            for (j = 0; j < Table[i + k].Length; j++)
                            {
                                varRow[k, j] = Table[i + k][j].Value;
                            }
                            endRow = i + k;
                        }
                    }
                    excel.SetRange(i + 1, 0, endRow + 1, FillColumns.Count - 1, varRow);
                    progress = (int)((i + 1) * 100.0 / (maxRows + 1));
                    worker.ReportProgress(progress);
                    if (exit) break;
                }
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Запуск Microsoft Excel...";
                worker.ReportProgress(100, userState);
                excel.Visible = true;
            }
            catch (Exception ex)
            {
                excel.Quit();
                throw new Exception("Ошибка экспорта в Microsoft Excel", ex);
            }
            finally
            {
                if (excel != null) excel.Disconnect();
            }
        }
        #endregion
    }

    public class ReportFormCell
    {
        public object Value = null;
    }
}
