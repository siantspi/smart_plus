﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class ChartSettings : Form
    {
        bool CancelClose;

        public double K1, K2;
        public int AveMonth;
        public bool SeriesVisibleFixed;

        public ChartSettings()
        {
            K1 = 0.1;
            K2 = 0.1;
            AveMonth = 1;
            SeriesVisibleFixed = false;
            InitializeComponent();
        }
        public DialogResult ShowDialogSaved(MainForm mainForm)
        {
            this.Owner = mainForm;
            this.StartPosition = FormStartPosition.CenterParent;
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";
            tbK1.Text = K1.ToString(nfi);
            tbK2.Text = K2.ToString(nfi);
            tbAve.Text = AveMonth.ToString();
            cbFixedVisibleSeries.Checked = SeriesVisibleFixed;
            return ShowDialog();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (CancelClose)
            {
                e.Cancel = true;
                CancelClose = false;
            }
            else
            {
                base.OnClosing(e);
            }
        }
        private void bOk_Click(object sender, EventArgs e)
        {
            CancelClose = false;
            try
            {
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";
                double k1 = Convert.ToDouble(tbK1.Text, nfi);
                double k2 = Convert.ToDouble(tbK2.Text, nfi);
                AveMonth = Convert.ToInt32(tbAve.Text);
                SeriesVisibleFixed = cbFixedVisibleSeries.Checked;

                if ((k1 < 0) || (k1 > 1) || (k2 < 0) || (k2 > 1))
                {
                    CancelClose = true;
                    MessageBox.Show("Значение K должно быть от 0 до 1!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    K1 = k1; K2 = k2;
                }
            }
            catch (Exception)
            {
                CancelClose = true;
                MessageBox.Show("Проверьте заполненные поля!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
