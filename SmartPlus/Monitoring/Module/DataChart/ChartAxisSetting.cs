﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class ChartAxisSetting : Form
    {
        DataChart chart;
        bool CancelClose;
        public DateTime MinValue;
        public DateTime MaxValue;

        public ChartAxisSetting()
        {
            InitializeComponent();
            chart = null;
            CancelClose = false;
            MinValue = DateTime.MinValue;
            MaxValue = DateTime.MaxValue;

            tbMinValueFixed.EnabledChanged += new EventHandler(tbMinValueFixed_EnabledChanged);
            tbMaxValueFixed.EnabledChanged += new EventHandler(tbMaxValueFixed_EnabledChanged);
        }

        public void SetChart(DataChart dChart)
        {
            this.chart = dChart;
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (CancelClose)
            {
                e.Cancel = true;
                CancelClose = false;
            }
            else
            {
                base.OnClosing(e);
            }
        }

        private void bMinValueFromGraph_Click(object sender, EventArgs e)
        {
            //if (chart != null)
            //{
            //    ChartAreaItem item;
            //    for (int i = 0; i < chart.ChartAreasList.Count; i++)
            //    {
            //        item = (ChartAreaItem)chart.ChartAreasList[i];
            //        if (item.ChartAreaData.Visible)
            //        {
            //            tbMinValueFixed.Text = DateTime.FromOADate(item.ChartAreaData.AxisX.ScaleView.ViewMinimum).ToShortDateString();
            //            break;
            //        }

            //    }

            //}
            //rbMinValueFixed.Checked = true;
        }
        private void bMaxValueFromGraph_Click(object sender, EventArgs e)
        {
            //if (chart != null)
            //{
            //    ChartAreaItem item;
            //    for (int i = 0; i < chart.ChartAreasList.Count; i++)
            //    {
            //        item = (ChartAreaItem)chart.ChartAreasList[i];
            //        if (item.ChartAreaData.Visible)
            //        {
            //            tbMaxValueFixed.Text = DateTime.FromOADate(item.ChartAreaData.AxisX.ScaleView.ViewMaximum).ToShortDateString();
            //            break;
            //        }

            //    }

            //}
            //rbMaxValueFixed.Checked = true;
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            CancelClose = false;
            try
            {
                MinValue = DateTime.MinValue;
                MaxValue = DateTime.MaxValue;
                if(rbMinValueFixed.Checked) MinValue = Convert.ToDateTime(tbMinValueFixed.Text);
                if(rbMaxValueFixed.Checked) MaxValue = Convert.ToDateTime(tbMaxValueFixed.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте заполненные поля!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CancelClose = true;
            }
        }
        private void bCancel_Click(object sender, EventArgs e)
        {
            CancelClose = false;
            if (MinValue == DateTime.MinValue)
            {
                rbMinValueAuto.Checked = true;
            }
            else
            {
                rbMinValueFixed.Checked = true;
                tbMinValueFixed.Text = MinValue.ToShortDateString();
            }
            if (MaxValue == DateTime.MaxValue)
            {
                rbMaxValueAuto.Checked = true;
            }
            else
            {
                rbMaxValueFixed.Checked = true;
                tbMaxValueFixed.Text = MaxValue.ToShortDateString();
            }
        }

        private void rbMinValueFixed_CheckedChanged(object sender, EventArgs e)
        {
            tbMinValueFixed.Enabled = rbMinValueFixed.Checked;
        }
        private void rbMaxValueFixed_CheckedChanged(object sender, EventArgs e)
        {
            tbMaxValueFixed.Enabled = rbMaxValueFixed.Checked;
        }
        void tbMaxValueFixed_EnabledChanged(object sender, EventArgs e)
        {
            if (!tbMaxValueFixed.Enabled) tbMaxValueFixed.Text = "";
        }
        void tbMinValueFixed_EnabledChanged(object sender, EventArgs e)
        {
            if (!tbMinValueFixed.Enabled) tbMinValueFixed.Text = "";
        }
    }
}
