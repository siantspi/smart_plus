﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using System.ComponentModel;
using RDF.Objects;
using SmartPlus.DataChartEx;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.IO;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public sealed class SeriesTyped
    {
        public Constant.Chart.SERIES_TYPE SeriesType;
        public Constant.Chart.DATA_SOURCE_TYPE SourceType;
        public DateTimeIntervalType IntervalType;
        public bool xAxisPrimary, yAxisPrimary;
        public int yAxisIndex;
        public int PlastCode;

        public int ChartAreaIndex;
        public ChartSeries DefaultSeries;

        public SeriesTyped(Constant.Chart.SERIES_TYPE Type)
        {
            this.SeriesType = Type;
            this.DefaultSeries = new ChartSeries();
            xAxisPrimary = true;
            yAxisPrimary = true;
            yAxisIndex = 0;
            PlastCode = 0;
            ChartSeries ser = this.DefaultSeries;

            #region SeriesTypeSwitch
            switch (Type)
            {
                case Constant.Chart.SERIES_TYPE.LIQUID_M:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча жидкости[МЭР]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.DarkGreen;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.NATGAS:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча газа[МЭР]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.FromArgb(220, 220, 0);
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.GASCONDENSAT:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча конденсата[МЭР]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.Black;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.LIQUID_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча жидкости[МЭР]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.DarkGreen;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.LIQUID_CHESS_M:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча жидкости[Шахматка]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Black;
                    break;
                case Constant.Chart.SERIES_TYPE.LIQUID_CHESS_T:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча жидкости[Шахматка]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Black;
                    break;
                case Constant.Chart.SERIES_TYPE.LIQUID_PROJ_M:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча жидкости[Проектн.]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.DarkGreen;
                    break;
                case Constant.Chart.SERIES_TYPE.LIQUID_PROJ_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча жидкости[Проектн.]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.DarkGreen;
                    break;
                case Constant.Chart.SERIES_TYPE.LIQUID_BP_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча жидкости[Бизнес-план]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "#,##0";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.FromArgb(0, 150, 0);
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQUID_M:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости[МЭР]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.DarkGreen;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQUID_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости[МЭР]";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.DarkGreen;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_NATGAS:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит газа[МЭР]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.FromArgb(220, 220, 0);
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит конденсата[МЭР]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.Black;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_M:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости[Шахматка]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Black;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_T:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости[Шахматка]";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Black;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_SUM:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости сумм.[Шахматка]";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Black;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_AVERAGE:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости средний[Шахматка]";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Black;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_SUM_M:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости сумм.[Шахматка]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Black;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_AVERAGE_M:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости средний[Шахматка]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Black;
                    break;
                case Constant.Chart.SERIES_TYPE.OIL_M:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча нефти[МЭР]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.SaddleBrown;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.OIL_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча нефти[МЭР]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.SaddleBrown;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.OIL_CHESS_M:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча нефти[Шахматка]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Maroon;
                    break;
                case Constant.Chart.SERIES_TYPE.OIL_CHESS_T:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча нефти[Шахматка]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Maroon;
                    break;
                case Constant.Chart.SERIES_TYPE.OIL_PROJ_M:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча нефти[Проект.]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.SaddleBrown;
                    break;
                case Constant.Chart.SERIES_TYPE.OIL_PROJ_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча нефти[Проект.]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.SaddleBrown;
                    break;
                case Constant.Chart.SERIES_TYPE.OIL_BP_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча нефти[Бизнес-план]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "#,##0";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.Red;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_OIL_M:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит нефти[МЭР]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.SaddleBrown;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_OIL_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит нефти[МЭР]";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.SaddleBrown;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_M:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит нефти[Шахматка]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Maroon;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_T:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит нефти[Шахматка]";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Maroon;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_SUM:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит нефти сумм.[Шахматка]";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Maroon;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_AVERAGE:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит нефти средний[Шахматка]";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.Maroon;
                    break;
                case Constant.Chart.SERIES_TYPE.FLOWING_LEVEL:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Динамический уровень";
                    ser.Name = "Динамический уровень[Шахматка]";
                    ser.Unit = "м";
                    ser.ToolTipFormat = "0";
                    ser.ChartType = SeriesChartType.Column;
                    ser.ColumnWidth = 2;
                    ser.Color = Color.DarkGray;
                    ser.BorderWidth = 0;
                    break;
                case Constant.Chart.SERIES_TYPE.WATER:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча воды[МЭР]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.Color = Color.FromArgb(0, 78, 155);
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.WATER_CHESS:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Добыча";
                    ser.Name = "Добыча воды[Шахматка]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.DarkBlue;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_WATER:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит воды[МЭР]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.Color = Color.FromArgb(0, 78, 155);
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_WATER_CHESS:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит воды[Шахматка]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.DarkBlue;
                    break;
                case Constant.Chart.SERIES_TYPE.WATERING:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Обводненность";
                    ser.Name = "Обводненность вес.[МЭР]";
                    ser.Unit = "%";
                    ser.ToolTipFormat = "0.##";
                    ser.Color = Color.FromArgb(0, 128, 255);
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.WATERING_M:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Обводненность";
                    ser.Name = "Обводненность объемн.[МЭР]";
                    ser.Unit = "%";
                    ser.ToolTipFormat = "0.##";
                    ser.Color = Color.FromArgb(0, 128, 255);
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.WATERING_CHESS:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Обводненность";
                    ser.Name = "Обводненность вес[Шахм.]";
                    ser.Unit = "%";
                    ser.ToolTipFormat = "0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.FromArgb(0, 128, 255);
                    break;
                case Constant.Chart.SERIES_TYPE.WATERING_CHESS_M:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Обводненность";
                    ser.Name = "Обводненность объемн[Шахм.]";
                    ser.Unit = "%";
                    ser.ToolTipFormat = "0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.FromArgb(0, 128, 255);
                    break;
                case Constant.Chart.SERIES_TYPE.INJECTION:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Закачка";
                    ser.Name = "Закачка[МЭР]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.Color = Color.DarkBlue;
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.INJ_GAS:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Закачка газа";
                    ser.Name = "Закачка газа[МЭР]";
                    ser.Unit = "тыс.м3";
                    ser.ToolTipFormat = "#,##0";
                    ser.Color = Color.Goldenrod;
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.INJECTION_CHESS:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Закачка";
                    ser.Name = "Закачка[Шахматка]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.DarkBlue;
                    break;
                case Constant.Chart.SERIES_TYPE.INJECTION_PROJ:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Закачка";
                    ser.Name = "Закачка[Проектн.]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.Color = Color.DarkBlue;
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    break;
                case Constant.Chart.SERIES_TYPE.INJECTION_BP:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Закачка";
                    ser.Name = "Закачка[Бизнес-план]";
                    ser.Unit = "м3";
                    ser.ToolTipFormat = "#,##0";
                    ser.Color = Color.FromArgb(0, 0, 185);
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    break;
                case Constant.Chart.SERIES_TYPE.W_INJ:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Приемистость";
                    ser.Name = "Приемистость[МЭР]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.Color = Color.DarkBlue;
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.W_INJ_GAS:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Приемистость газа";
                    ser.Name = "Приемистость газа[МЭР]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.Color = Color.Goldenrod;
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderWidth = 2;
                    break;
                case Constant.Chart.SERIES_TYPE.W_INJ_CHESS:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Приемистость";
                    ser.Name = "Приемистость[Шахматка]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.DarkBlue;
                    break;
                case Constant.Chart.SERIES_TYPE.W_INJ_CHESS_SUM:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Приемистость";
                    ser.Name = "Приемистость сумм.[Шахматка]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.DarkBlue;
                    break;
                case Constant.Chart.SERIES_TYPE.W_INJ_CHESS_AVERAGE:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Приемистость";
                    ser.Name = "Приемистость средняя[Шахматка]";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 3;
                    ser.Marker.Color = Color.DarkBlue;
                    break;
                case Constant.Chart.SERIES_TYPE.PROD_FUND:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Фонд";
                    ser.Name = "Фонд добывающих скважин[МЭР]";
                    ser.Unit = "шт";
                    ser.ToolTipFormat = "0";
                    ser.Color = Color.Silver;
                    ser.ChartType = SeriesChartType.Column;
                    ser.BorderWidth = 0;
                    break;
                case Constant.Chart.SERIES_TYPE.INJ_FUND:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Фонд";
                    ser.Name = "Фонд нагнетательных скважин[МЭР]";
                    ser.Unit = "шт";
                    ser.ToolTipFormat = "0";
                    ser.Color = Color.SkyBlue;
                    ser.ChartType = SeriesChartType.Column;
                    ser.BorderWidth = 0;
                    break;
                case Constant.Chart.SERIES_TYPE.PROD_FUND_CHESS:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Фонд";
                    ser.Name = "Фонд добывающих скважин[Шахматка]";
                    ser.Unit = "шт";
                    ser.ToolTipFormat = "0";
                    ser.Color = Color.LightSlateGray;
                    ser.ChartType = SeriesChartType.Column;
                    ser.BorderWidth = 0;
                    break;
                case Constant.Chart.SERIES_TYPE.INJ_FUND_CHESS:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.CHESS;
                    ser.SmallName = "Фонд";
                    ser.Name = "Фонд нагнетательных скважин[Шахматка]";
                    ser.Unit = "шт";
                    ser.ToolTipFormat = "0";
                    ser.Color = Color.SkyBlue;
                    ser.ChartType = SeriesChartType.Column;
                    ser.BorderWidth = 0;
                    break;
                case Constant.Chart.SERIES_TYPE.PROD_FUND_PROJ:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Фонд";
                    ser.Name = "Фонд добывающих скважин на конец года[Проектн.]";
                    ser.Unit = "шт";
                    ser.ToolTipFormat = "0";
                    ser.Color = Color.Silver;
                    ser.ChartType = SeriesChartType.Column;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.BorderColor = Color.FromArgb(70, 70, 70);
                    break;
                case Constant.Chart.SERIES_TYPE.INJ_FUND_PROJ:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Фонд";
                    ser.Name = "Фонд нагнетательных скважин на конец года[Проектн.]";
                    ser.Unit = "шт";
                    ser.ToolTipFormat = "0";
                    ser.Color = Color.SkyBlue;
                    ser.ChartType = SeriesChartType.Column;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.BorderColor = Color.FromArgb(70, 70, 70);
                    break;
                case Constant.Chart.SERIES_TYPE.LEAD_PROD_ALL:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Фонд";
                    ser.Name = "Ввод новых добывающих скважин[Проектн.]";
                    ser.Unit = "шт";
                    ser.ToolTipFormat = "0";
                    ser.Color = Color.Salmon;
                    ser.ChartType = SeriesChartType.Column;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.BorderColor = Color.FromArgb(70, 70, 70);
                    break;
                case Constant.Chart.SERIES_TYPE.LEAD_PROD_OPERATION:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS;
                    ser.SmallName = "Фонд";
                    ser.Name = "Ввод новых доб.скважин из экспл.бурения[Проектн.]";
                    ser.Unit = "шт";
                    ser.ToolTipFormat = "0";
                    ser.Color = Color.Red;
                    ser.ChartType = SeriesChartType.Column;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.BorderColor = Color.FromArgb(70, 70, 70);
                    break;
                case Constant.Chart.SERIES_TYPE.PROJECT_EDGE_POINTS:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Пр.точки";
                    ser.Name = "Проектные краевые точки[Проектн.]";
                    ser.Unit = "т";
                    ser.ToolTipFormat = "0";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Circle;
                    ser.Marker.Size = 10;
                    ser.Marker.Color = Color.Red;
                    ser.Marker.BorderEnabled = true;
                    break;
                case Constant.Chart.SERIES_TYPE.FORCED_DATE:
                    this.IntervalType = DateTimeIntervalType.Number;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Приведенная дата списка";
                    ser.Name = "Приведенная дата списка скважин";
                    ser.Unit = "";
                    ser.ToolTipFormat = "";
                    ser.ChartType = SeriesChartType.Column;
                    ser.ColumnWidth = 2;
                    ser.Color = Color.Red;
                    ser.BorderWidth = 0;
                    break;
                case Constant.Chart.SERIES_TYPE.PLAST_PRESSURE:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.WELL_RESEARCH;
                    ser.SmallName = "Давление пластовое";
                    ser.Name = "Замер пластового давления[Pпл]";
                    ser.Unit = "атм";
                    ser.ToolTipFormat = "0.#";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Circle;
                    ser.Marker.Size = 8;
                    ser.Marker.Color = Color.Red;
                    ser.Marker.BorderEnabled = true;
                    break;
                case Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_HSTAT:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.WELL_RESEARCH;
                    ser.SmallName = "Давление пластовое";
                    ser.Name = "Замер пластового давления[Нст]";
                    ser.Unit = "атм";
                    ser.ToolTipFormat = "0.#";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Circle;
                    ser.Marker.Size = 5;
                    ser.Marker.Color = Color.DarkMagenta;
                    ser.Marker.BorderEnabled = true;
                    break;
                case Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_BUFFPL:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.WELL_RESEARCH;
                    ser.SmallName = "Давление пластовое";
                    ser.Name = "Замер буферного пластового давления[Pбуфпл]";
                    ser.Unit = "атм";
                    ser.ToolTipFormat = "0.#";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Circle;
                    ser.Marker.Size = 8;
                    ser.Marker.Color = Color.Red;
                    ser.Marker.BorderEnabled = true;
                    break;
                case Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.WELL_RESEARCH;
                    ser.SmallName = "Давление пластовое";
                    ser.Name = "Среднеарифметическое значение пластового давления";
                    ser.Unit = "атм";
                    ser.ToolTipFormat = "0.#";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 6;
                    ser.Marker.Color = Color.Red;
                    ser.Marker.BorderEnabled = true;
                    break;
                case Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.WELL_RESEARCH;
                    ser.SmallName = "Давление забойное";
                    ser.Name = "Среднее значение забойного давления";
                    ser.Unit = "атм";
                    ser.ToolTipFormat = "0.#";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 6;
                    ser.Marker.Color = Color.Lime;
                    ser.Marker.BorderEnabled = true;
                    break;
                case Constant.Chart.SERIES_TYPE.PRESSURE_ZAB_HDYN:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.WELL_RESEARCH;
                    ser.SmallName = "Давление забойное";
                    ser.Name = "Замер забойного давления[Hдин]";
                    ser.Unit = "атм";
                    ser.ToolTipFormat = "0.#";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Square;
                    ser.Marker.Size = 4;
                    ser.Marker.Color = Color.Lime;
                    ser.Marker.BorderEnabled = true;
                    break;
                case Constant.Chart.SERIES_TYPE.PRESSURE_ZAB:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.WELL_RESEARCH;
                    ser.SmallName = "Давление забойное";
                    ser.Name = "Замер забойного давления[Pзаб]";
                    ser.Unit = "атм";
                    ser.ToolTipFormat = "0.#";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Circle;
                    ser.Marker.Size = 8;
                    ser.Marker.Color = Color.Green;
                    ser.Marker.BorderEnabled = true;
                    break;
                case Constant.Chart.SERIES_TYPE.SUSPEND_SOLID:
                    this.IntervalType = DateTimeIntervalType.Days;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.WELL_SUSPEND;
                    ser.SmallName = string.Empty;
                    ser.Name = "Проба КВЧ";
                    ser.Unit = string.Empty;
                    ser.ToolTipFormat = "0";
                    ser.ChartType = SeriesChartType.Point;
                    ser.Marker.Style = MarkerStyle.Circle;
                    ser.Marker.Size = 8;
                    ser.Marker.Color = Color.Black;
                    ser.Marker.BorderEnabled = false;
                    break;
                case Constant.Chart.SERIES_TYPE.COMPENSATION:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Компенсация";
                    ser.Name = "Компенсация текущая";
                    ser.Unit = "%";
                    ser.ToolTipFormat = "#,##0";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.DarkMagenta;
                    ser.BorderWidth = 1;
                    break;
                case Constant.Chart.SERIES_TYPE.COMPENSATION_ACCUM:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Компенсация";
                    ser.Name = "Компенсация накопленная";
                    ser.Unit = "%";
                    ser.ToolTipFormat = "#,##0";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.FromArgb(40, 40, 40);
                    ser.BorderWidth = 1;
                    break;
                case Constant.Chart.SERIES_TYPE.PI_M:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Кпрод";
                    ser.Name = "Кпрод";
                    ser.Unit = "м3/сут/атм";
                    ser.ToolTipFormat = "#,##0.###";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.DarkMagenta;
                    ser.BorderWidth = 2;
                    ser.BorderDashStyle = DashStyle.Dot;
                    break;
                case Constant.Chart.SERIES_TYPE.PI_T:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Кпрод";
                    ser.Name = "Кпрод";
                    ser.Unit = "т/сут/атм";
                    ser.ToolTipFormat = "#,##0.###";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.Color = Color.DarkMagenta;
                    ser.BorderWidth = 2;
                    ser.BorderDashStyle = DashStyle.Dot;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQUID_M_POTENTIAL:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости потенциал";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.DarkGreen;
                    ser.BorderWidth = 1;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_LIQUID_T_POTENTIAL:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит жидкости потенциал";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.DarkGreen;
                    ser.BorderWidth = 1;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_OIL_M_POTENTIAL:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит нефти потенциал";
                    ser.Unit = "м3/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.SaddleBrown;
                    ser.BorderWidth = 1;
                    break;
                case Constant.Chart.SERIES_TYPE.Q_OIL_T_POTENTIAL:
                    this.IntervalType = DateTimeIntervalType.Months;
                    this.SourceType = Constant.Chart.DATA_SOURCE_TYPE.MER;
                    ser.SmallName = "Дебит";
                    ser.Name = "Дебит нефти потенциал";
                    ser.Unit = "т/сут";
                    ser.ToolTipFormat = "#,##0.##";
                    ser.ChartType = SeriesChartType.StepLine;
                    ser.BorderDashStyle = DashStyle.Dash;
                    ser.Color = Color.SaddleBrown;
                    ser.BorderWidth = 1;
                    break;
            }
            #endregion
        }
    }

    public sealed class Scheme
    {
        public string Name;

        public ChartAxis AxisX, AxisX2;
        public ChartAxis[] AxisY, AxisY2;
        public List<SeriesTyped> Series;
        public short DefaultIntervalType;
        public short LastIntervalType;
        public bool[] DateTimeIntervalTypeEnabled;
        public SeriesTyped PlastModeSeries;
        public bool ByPlastMode;

        public Scheme(string Name)
        {
            this.Name = Name;
            ByPlastMode = false;
            PlastModeSeries = null;
            AxisX = new ChartAxis();
            AxisX.Enabled = false;
            AxisX.Position = ChartPosition.Bottom;
            AxisX.MinorTickmark.Enabled = true;
            AxisX.MajorGrid.Enabled = true;
            AxisX.MajorTickMark.Enabled = true;
            AxisX.LabelStyle.Enabled = true;

            AxisX.ValueType = ChartValueType.DateTime;
            AxisX.LabelStyle.Format = "yyyy";

            AxisX2 = new ChartAxis();
            AxisX2.Enabled = false;
            AxisX2.Position = ChartPosition.Top;
            AxisY = new ChartAxis[1];
            AxisY[0] = new ChartAxis();
            AxisY[0].Position = ChartPosition.Left;
            AxisY[0].MajorGrid.Enabled = true;
            AxisY[0].MajorGrid.FixedCount = 10;
            AxisY[0].MajorTickMark.Enabled = true;
            AxisY[0].MajorTickMark.FixedCount = 10;
            AxisY[0].LabelStyle.Enabled = true;
            AxisY[0].LabelStyle.Enabled = true;
            AxisY[0].LabelStyle.FixedCount = 10;

            AxisY2 = new ChartAxis[1];
            AxisY2[0] = new ChartAxis();
            AxisY2[0].Position = ChartPosition.Right;
            AxisY2[0].MajorGrid.Enabled = true;
            AxisY2[0].MajorGrid.FixedCount = 10;
            AxisY2[0].MajorTickMark.Enabled = true;
            AxisY2[0].MajorTickMark.FixedCount = 10;
            AxisY2[0].LabelStyle.Enabled = true;
            AxisY2[0].LabelStyle.FixedCount = 10;
            Series = new List<SeriesTyped>();
            DateTimeIntervalTypeEnabled = new bool[3];
            DateTimeIntervalTypeEnabled[0] = true;
            DateTimeIntervalTypeEnabled[1] = true;
            DateTimeIntervalTypeEnabled[2] = true;
        }

        public void AddAxisY()
        {
            ChartAxis[] newAxisY = new ChartAxis[AxisY.Length + 1];
            for (int i = 0; i < AxisY.Length; i++)
            {
                newAxisY[i] = AxisY[i];
            }
            newAxisY[AxisY.Length] = new ChartAxis();
            newAxisY[AxisY.Length].Enabled = false;
            newAxisY[AxisY.Length].Position = ChartPosition.Left;
            newAxisY[AxisY.Length].MajorGrid.Enabled = true;
            newAxisY[AxisY.Length].MajorGrid.FixedCount = 10;
            newAxisY[AxisY.Length].MajorTickMark.Enabled = true;
            newAxisY[AxisY.Length].MajorTickMark.FixedCount = 10;
            newAxisY[AxisY.Length].LabelStyle.Enabled = true;
            newAxisY[AxisY.Length].LabelStyle.FixedCount = 10;
            AxisY = newAxisY;
        }
        public void AddAxisY2()
        {
            ChartAxis[] newAxisY2 = new ChartAxis[AxisY2.Length + 1];
            for (int i = 0; i < AxisY2.Length; i++)
            {
                newAxisY2[i] = AxisY2[i];
            }
            newAxisY2[AxisY2.Length] = new ChartAxis();
            newAxisY2[AxisY2.Length].Enabled = false;
            newAxisY2[AxisY2.Length].Position = ChartPosition.Right;
            newAxisY2[AxisY2.Length].MajorGrid.Enabled = true;
            newAxisY2[AxisY2.Length].MajorGrid.FixedCount = 10;
            newAxisY2[AxisY2.Length].MajorTickMark.Enabled = true;
            newAxisY2[AxisY2.Length].MajorTickMark.FixedCount = 10;
            newAxisY2[AxisY2.Length].LabelStyle.Enabled = true;
            newAxisY2[AxisY2.Length].LabelStyle.FixedCount = 10;
            AxisY2 = newAxisY2;
        }

        public void SetPlastModeSeries(Constant.Chart.SERIES_TYPE SeriesType, bool xAxisPrimary, bool yAxisPrimary, int yAxisIndex, int GroupIndex)
        {
            PlastModeSeries = new SeriesTyped(SeriesType);
            PlastModeSeries.xAxisPrimary = xAxisPrimary;
            PlastModeSeries.yAxisPrimary = yAxisPrimary;
            PlastModeSeries.yAxisIndex = yAxisIndex;
            PlastModeSeries.DefaultSeries.GroupIndex = GroupIndex;
        }
        public SeriesTyped AddSeries(Constant.Chart.SERIES_TYPE SeriesType, bool xAxisPrimary, bool yAxisPrimary, int yAxisIndex)
        {
            return AddSeries(SeriesType, xAxisPrimary, yAxisPrimary, yAxisIndex, -1);
        }
        public SeriesTyped AddSeries(Constant.Chart.SERIES_TYPE SeriesType, bool xAxisPrimary, bool yAxisPrimary, int yAxisIndex, int GroupIndex)
        {
            SeriesTyped newSeries = new SeriesTyped(SeriesType);
            newSeries.xAxisPrimary = xAxisPrimary;
            newSeries.yAxisPrimary = yAxisPrimary;
            newSeries.yAxisIndex = yAxisIndex;
            Series.Add(newSeries);
            newSeries.DefaultSeries.GroupIndex = GroupIndex;
            return newSeries;
        }
        public int GetSeriesIndexByType(Constant.Chart.SERIES_TYPE SeriesType)
        {
            for (int i = 0; i < Series.Count; i++)
            {
                if (Series[i].SeriesType == SeriesType)
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetSeriesIndexByPlastCode(int PlastCode)
        {
            for (int i = 0; i < Series.Count; i++)
            {
                if (Series[i].PlastCode == PlastCode)
                {
                    return i;
                }
            }
            return -1;
        }
        public bool IsUsedSource(Constant.Chart.DATA_SOURCE_TYPE SourceType)
        {
            for (int i = 0; i < Series.Count; i++)
            {
                if (Series[i].SourceType == SourceType) return true;
            }
            return false;
        }
    }

    public sealed class DataChart
    {
        #region DATA
        DataChartEx.DataChartEx chart;
        class AccumParams
        {
            public int OilObjCode;
            public double AccumLiq;
            public double AccumOil;
            public double AccumWat;
            public double AccumInj;
            public double AccumInjGas;
            public DateTime BPDate;
            public double dOilYear;
            public double dOilLastMonth;
            public double dOilYearPercent;
            public double dOilLastMonthPercent;
        }
        class AccumParamList
        {
            public string Comment;
            ArrayList AccList;
            public AccumParamList()
            {
                AccList = new ArrayList();
                Add(-1, -1, -1, -1, -1, -1);
                SetBPParams(DateTime.MinValue, 0, 0, 0, 0);
                Comment = string.Empty;
            }
            public void Clear()
            {
                while (AccList.Count > 1) AccList.RemoveAt(AccList.Count - 1);
                AccumParams acc = (AccumParams)AccList[0];
                acc.AccumLiq = -1;
                acc.AccumOil = -1;
                acc.AccumInj = -1;
                acc.AccumInjGas = -1;
                acc.AccumWat = -1;
                SetBPParams(DateTime.MinValue, 0, 0, 0, 0);
                Comment = string.Empty;
            }
            public AccumParams Add(int OilObjCode, double AccLiq, double AccOil, double AccWat, double AccInj, double AccInjGas)
            {
                AccumParams acc = new AccumParams();
                acc.OilObjCode = OilObjCode;
                acc.AccumLiq = AccLiq;
                acc.AccumOil = AccOil;
                acc.AccumInj = AccInj;
                acc.AccumInjGas = AccInjGas;
                acc.AccumWat = AccWat;
                AccList.Add(acc);
                return acc;
            }
            public void SumAllAccum()
            {
                AccumParams acc, acc0 = (AccumParams)AccList[0];
                if (AccList.Count > 1)
                {
                    acc0.AccumLiq = 0;
                    acc0.AccumOil = 0;
                    acc0.AccumInj = 0;
                    acc0.AccumInjGas = 0;
                    acc0.AccumWat = 0;
                    for (int i = 1; i < AccList.Count; i++)
                    {
                        acc = (AccumParams)AccList[i];
                        acc0.AccumLiq += acc.AccumLiq;
                        acc0.AccumOil += acc.AccumOil;
                        acc0.AccumInj += acc.AccumInj;
                        acc0.AccumInjGas += acc.AccumInjGas;
                        acc0.AccumWat += acc.AccumWat;
                    }
                    if (acc0.AccumLiq == 0)
                    {
                        acc0.AccumLiq = -1;
                        acc0.AccumOil = -1;
                        acc0.AccumWat = -1;
                    }
                    if (acc0.AccumInj == 0) acc0.AccumInj = -1;
                    if (acc0.AccumInjGas == 0) acc0.AccumInjGas = -1;
                }
            }
            public void SetBPParams(DateTime Date, double dOilYear, double dOilLastMonth, double dOilYearPercent, double dOilLastMonthPercent)
            {
                if (AccList != null && AccList.Count > 0)
                {
                    AccumParams acc = (AccumParams)AccList[0];
                    acc.BPDate = Date;
                    acc.dOilYear = dOilYear;
                    acc.dOilLastMonth = dOilLastMonth;
                    acc.dOilYearPercent = dOilYearPercent;
                    acc.dOilLastMonthPercent = dOilLastMonthPercent;
                }
            }
            public void SumAccumByPlastCodes(List<int> PlastCodes)
            {
                AccumParams acc, acc0 = (AccumParams)AccList[0];
                if (AccList.Count > 1)
                {
                    acc0.AccumLiq = 0;
                    acc0.AccumOil = 0;
                    acc0.AccumInj = 0;
                    acc0.AccumInjGas = 0;
                    acc0.AccumWat = 0;
                    for (int i = 1; i < AccList.Count; i++)
                    {
                        acc = (AccumParams)AccList[i];
                        if (PlastCodes.IndexOf(acc.OilObjCode) != -1)
                        {
                            acc0.AccumLiq += acc.AccumLiq;
                            acc0.AccumOil += acc.AccumOil;
                            acc0.AccumInj += acc.AccumInj;
                            acc0.AccumInjGas += acc.AccumInjGas;
                            acc0.AccumWat += acc.AccumWat;
                        }
                    }
                    if (acc0.AccumLiq == 0)
                    {
                        acc0.AccumLiq = -1;
                        acc0.AccumOil = -1;
                        acc0.AccumWat = -1;
                    }
                    if (acc0.AccumInj == 0) acc0.AccumInj = -1;
                    if (acc0.AccumInjGas == 0) acc0.AccumInjGas = -1;
                }
            }
            public int GetItemIndexByOilObjCode(int OilObjCode)
            {
                for (int i = 0; i < AccList.Count; i++)
                {
                    if (OilObjCode == ((AccumParams)AccList[i]).OilObjCode)
                    {
                        return i;
                    }
                }
                return -1;
            }
            public AccumParams GetItemByOilObjCode(int OilObjCode)
            {
                for (int i = 0; i < AccList.Count; i++)
                {
                    if (OilObjCode == ((AccumParams)AccList[i]).OilObjCode)
                    {
                        return (AccumParams)AccList[i];
                    }
                }
                AccumParams acc = new AccumParams();
                acc.OilObjCode = OilObjCode;
                acc.AccumLiq = 0;
                acc.AccumOil = 0;
                acc.AccumInj = 0;
                acc.AccumInjGas = 0;
                acc.AccumWat = 0;
                AccList.Add(acc);
                return (AccumParams)AccList[AccList.Count - 1];
            }
            public AccumParams this[int index]
            {
                get 
                { 
                    if(index < AccList.Count) return ((AccumParams)AccList[index]);
                    return null;
                }
            }
        }

        MainForm mainForm;
        Project project;
        Infragistics.Win.UltraWinDock.DockableControlPane dcp;
        public BaseObj SourceObj;
        public string Title;
        FilterOilObjects filterOilObj;
        DataGridGraphics tableGraphics;
        ToolStrip ToolBar;
        ChartSettings settings;
        ArrayList OFIndexes;
        ArrayList OFChessIndexes;
        ArrayList AreaIndexes;
        ArrayList AreaChessIndexes;

        public Scheme[] SchemeList;
        public int ActiveScheme;
        public short IntervalType;

        public bool ShowProjParams = false;
        public bool ShowBPParams = false;
        public bool AllOilObjChecked = false;
        public bool TableVisible = false;
        public bool ShowInfo = true;
        public bool ShowGTM;
        public bool ShowWellAction;
        public bool ShowWellLeak;
        public bool ShowWellSuspend;
        public bool ShowWellStop;
        public bool ShowPIseries;

        bool LegendVis = true;
        public bool LegendVisible
        {
            get { return LegendVis; }
            set
            {
                LegendVis = value;
                chart.ChartAreas[0].Legend.Enabled = value;
                chart.ChartAreas[0].ReCalcChartAreaSizes();
            }
        }

        // OIL OBJ MODE
        AccumParamList AccList;
        public bool ByPlastMode
        {
            get
            {
                if ((ActiveScheme != -1) && (ActiveScheme < SchemeList.Length))
                {
                    return SchemeList[ActiveScheme].ByPlastMode;
                }
                return false;
            }
        }
        
        public string ProjParamsName;
        public double ProjParamsXValue;
        
        Font grFont;
        bool DrawWaitIconMode;
        int wait_angle;
        Timer tmrWait;
        #endregion

        public DataChart(MainForm MainForm)
        {
            this.mainForm = MainForm;
            this.ToolBar = mainForm.tbGraphics;
            dcp = mainForm.chartPane;
            filterOilObj = mainForm.filterOilObj;
            settings = new ChartSettings();
            settings.Owner = mainForm;

            chart = new SmartPlus.DataChartEx.DataChartEx(MainForm.MaxMonitorWidth * 2, MainForm.MaxMonitorHeight);
            chart.Parent = mainForm.tsGraphicsContainer.ContentPanel;
            chart.Dock = DockStyle.Fill;

            AccList = new AccumParamList();

            tableGraphics = new DataGridGraphics(this, mainForm);

            DrawWaitIconMode = false;
            tmrWait = new Timer();
            tmrWait.Enabled = false;
            tmrWait.Interval = 100;
            tmrWait.Tick += new EventHandler(tmrWait_Tick);

            this.Title = "";
            grFont = new Font("Calibri", 8, FontStyle.Bold);
            
            OFIndexes = new ArrayList();
            OFChessIndexes = new ArrayList();
            AreaIndexes = new ArrayList();
            AreaChessIndexes = new ArrayList();

            this.SourceObj = null;
            this.ShowProjParams = true;
            this.ShowGTM = true;
            this.ShowWellAction = false;
            this.ShowWellLeak = true;
            this.ShowWellSuspend = true;
            this.ShowWellStop = true;

            ProjParamsName = "";
            ProjParamsXValue = -1;
            
            InitializeSchemes();
            chart.Paint += new PaintEventHandler(chart_Paint);
            chart.MouseDown += new MouseEventHandler(chart_MouseDown);
            chart.MouseWheel += new MouseEventHandler(chart_MouseWheel);
            chart.MouseMove += new MouseEventHandler(chart_MouseMove);
            chart.MouseUp += new MouseEventHandler(chart_MouseUp);
        }

        public void InitializeSchemes() 
        {
            int i;
            SchemeList = new Scheme[7];
            SchemeList[0] = new Scheme("Добыча");
            Scheme scheme = SchemeList[0];
            scheme.AddAxisY();
            scheme.AddAxisY();
            scheme.AddAxisY();
            scheme.AddAxisY2();

            scheme.AxisX.CustomLabels.Enabled = true;
            scheme.AxisY[0].Title = "Добыча/закачка";
            scheme.AxisY[0].MinFixed = 0;
            scheme.AxisY[1].Title = "Добыча/Закачка газа";
            scheme.AxisY[1].MinFixed = 0;
            scheme.AxisY[2].Title = "Фонд";
            scheme.AxisY[2].MinFixed = 0;
            scheme.AxisY[2].MajorGrid.Enabled = false;
            scheme.AxisY[3].Title = "Замеры давления";
            scheme.AxisY[3].MinFixed = 0;
            scheme.AxisY[3].MajorGrid.Enabled = false;
            
            scheme.AxisY2[0].Title = "Обводненность";
            scheme.AxisY2[0].MinFixed = 0;
            scheme.AxisY2[0].MaxFixed = 100;

            scheme.AxisY2[1].Title = "Компенсация";
            scheme.AxisY2[1].MinFixed = 0;

            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PROD_FUND_PROJ, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PROD_FUND, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJ_FUND_PROJ, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJ_FUND, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJ_GAS, true, true, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.COMPENSATION, true, false, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.COMPENSATION_ACCUM, true, false, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.LIQUID_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.LIQUID_M, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.NATGAS, true, true, 1);
            //scheme.AddSeries(Constant.Chart.SERIES_TYPE.GASCONDENSAT, true, true, 1);

            scheme.AddSeries(Constant.Chart.SERIES_TYPE.OIL_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING_M, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJECTION, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.LIQUID_PROJ_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.LIQUID_BP_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.OIL_PROJ_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.OIL_BP_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJECTION_PROJ, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJECTION_BP, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PROJECT_EDGE_POINTS, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.FORCED_DATE, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_HSTAT, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_BUFFPL, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB_HDYN, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, true, true, 3);

            scheme.DefaultIntervalType = 1;
            scheme.LastIntervalType = 1;
            scheme.DateTimeIntervalTypeEnabled[2] = false;

            SchemeList[1] = new Scheme("Дебиты");
            scheme = SchemeList[1];
            scheme.AxisX.CustomLabels.Enabled = true;
            scheme.AddAxisY();
            scheme.AddAxisY();
            scheme.AddAxisY();
            scheme.AddAxisY2();
            scheme.AddAxisY2();

            scheme.AxisX.CustomLabels.Enabled = true;
            scheme.AxisY[0].Title = "Дебит/Приемистость";
            scheme.AxisY[0].MinFixed = 0;

            scheme.AxisY[1].Title = "Дебит/Приемистость газа";
            scheme.AxisY[1].MinFixed = 0;

            scheme.AxisY[2].Title = "Фонд";
            scheme.AxisY[2].MinFixed = 0;
            scheme.AxisY[2].MajorGrid.Enabled = false;

            scheme.AxisY[3].Title = "Замеры давления";
            scheme.AxisY[3].MinFixed = 0;
            scheme.AxisY[3].MajorGrid.Enabled = false;

            scheme.AxisY2[0].Title = "Обводненность";
            scheme.AxisY2[0].MinFixed = 0;
            scheme.AxisY2[0].MaxFixed = 100;

            scheme.AxisY2[1].Title = "Компенсация";
            scheme.AxisY2[1].MinFixed = 0;

            scheme.AxisY2[2].Title = "Кпрод";
            scheme.AxisY2[2].MinFixed = 0;

            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PROD_FUND, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJ_FUND, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.W_INJ_GAS, true, true, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING_M, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.COMPENSATION, true, false, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.COMPENSATION_ACCUM, true, false, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_M, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_NATGAS, true, true, 1);
            //scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.W_INJ, true, true, 0);
            
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.FORCED_DATE, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_HSTAT, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_BUFFPL, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB_HDYN, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PI_T, true, false, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PI_M, true, false, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_M_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_T_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_M_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_T_POTENTIAL, true, true, 0);

            scheme.DefaultIntervalType = 1;
            scheme.LastIntervalType = 1;
            scheme.DateTimeIntervalTypeEnabled[2] = false;

            SchemeList[2] = new Scheme("Шахматка [сумма]");
            scheme = SchemeList[2];
            scheme.AxisX.CustomLabels.Enabled = true;
            scheme.AddAxisY();
            scheme.AddAxisY();
            scheme.AddAxisY();
            scheme.AddAxisY2();

            scheme.AxisY[0].Title = "Дебит/Приемистость";
            scheme.AxisY[0].MinFixed = 0;

            scheme.AxisY[1].Title = "Динамический уровень";
            scheme.AxisY[1].IsReversed = true;
            scheme.AxisY[1].MajorGrid.Enabled = false;

            scheme.AxisY[2].Title = "Фонд";
            scheme.AxisY[2].MinFixed = 0;
            scheme.AxisY[2].MajorGrid.Enabled = false;

            scheme.AxisY[3].Title = "Замеры давления";
            scheme.AxisY[3].MinFixed = 0;
            scheme.AxisY[3].MajorGrid.Enabled = false;

            scheme.AxisY2[0].Title = "Обводненность";
            scheme.AxisY2[0].MinFixed = 0;
            scheme.AxisY2[0].MaxFixed = 100;

            scheme.AxisY2[1].Title = "Кпрод";
            scheme.AxisY2[1].MinFixed = 0;

            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PROD_FUND, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PROD_FUND_CHESS, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJ_FUND_CHESS, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.FLOWING_LEVEL, true, true, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.W_INJ, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_M, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING_M, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.W_INJ_CHESS, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.W_INJ_CHESS_SUM, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_M, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_SUM, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_SUM_M, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_SUM, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING_CHESS, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING_CHESS_M, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.FORCED_DATE, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_HSTAT, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_BUFFPL, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB_HDYN, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.SUSPEND_SOLID, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PI_T, true, false, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PI_M, true, false, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_M_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_T_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_M_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_T_POTENTIAL, true, true, 0);

            scheme.DefaultIntervalType = 2;
            scheme.LastIntervalType = 2;
            scheme.DateTimeIntervalTypeEnabled[0] = false;
            scheme.DateTimeIntervalTypeEnabled[1] = false;

            SchemeList[3] = new Scheme("Шахматка [средняя]");
            scheme = SchemeList[3];
            scheme.AxisX.CustomLabels.Enabled = true;
            scheme.AddAxisY();
            scheme.AddAxisY();
            scheme.AddAxisY();
            scheme.AddAxisY2();

            scheme.AxisY[0].Title = "Дебит/Приемистость";
            scheme.AxisY[0].MinFixed = 0;

            scheme.AxisY[1].Title = "Динамический уровень";
            scheme.AxisY[1].IsReversed = true;
            scheme.AxisY[1].MajorGrid.Enabled = false;

            scheme.AxisY[2].Title = "Фонд";
            scheme.AxisY[2].MinFixed = 0;
            scheme.AxisY[2].MajorGrid.Enabled = false;

            scheme.AxisY[3].Title = "Замеры давления";
            scheme.AxisY[3].MinFixed = 0;
            scheme.AxisY[3].MajorGrid.Enabled = false;

            scheme.AxisY2[0].Title = "Обводненность";
            scheme.AxisY2[0].MinFixed = 0;
            scheme.AxisY2[0].MaxFixed = 100;

            scheme.AxisY2[1].Title = "Кпрод";
            scheme.AxisY2[1].MinFixed = 0;

            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PROD_FUND, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PROD_FUND_CHESS, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.INJ_FUND_CHESS, true, true, 2);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.FLOWING_LEVEL, true, true, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.W_INJ, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_M, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING_M, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.W_INJ_CHESS, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.W_INJ_CHESS_AVERAGE, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_M, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_AVERAGE, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_AVERAGE_M, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_T, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_AVERAGE, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING_CHESS, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.WATERING_CHESS_M, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.FORCED_DATE, true, false, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_HSTAT, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_BUFFPL, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB_HDYN, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, true, true, 3);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.SUSPEND_SOLID, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PI_T, true, false, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.PI_M, true, false, 1);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_M_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_LIQUID_T_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_M_POTENTIAL, true, true, 0);
            scheme.AddSeries(Constant.Chart.SERIES_TYPE.Q_OIL_T_POTENTIAL, true, true, 0);

            scheme.DefaultIntervalType = 2;
            scheme.LastIntervalType = 2;
            scheme.DateTimeIntervalTypeEnabled[0] = false;
            scheme.DateTimeIntervalTypeEnabled[1] = false;

            SchemeList[4] = new Scheme("Добыча жидкости [По объектам]");
            scheme = SchemeList[4];
            scheme.ByPlastMode = true;
            scheme.AxisX.CustomLabels.Enabled = false;
            scheme.AxisY[0].Title = "Добыча жидкости, т";
            scheme.AxisY[0].MinFixed = 0;

            scheme.SetPlastModeSeries(Constant.Chart.SERIES_TYPE.LIQUID_T, true, true, 0, 0);

            scheme.DefaultIntervalType = 1;
            scheme.LastIntervalType = 1;
            scheme.DateTimeIntervalTypeEnabled[2] = false;

            SchemeList[5] = new Scheme("Добыча нефти [По объектам]");
            scheme = SchemeList[5];
            scheme.ByPlastMode = true;
            scheme.AxisX.CustomLabels.Enabled = false;
            scheme.AxisY[0].Title = "Добыча нефти, т";
            scheme.AxisY[0].MinFixed = 0;

            scheme.SetPlastModeSeries(Constant.Chart.SERIES_TYPE.OIL_T, true, true, 0, 0);

            scheme.DefaultIntervalType = 1;
            scheme.LastIntervalType = 1;
            scheme.DateTimeIntervalTypeEnabled[2] = false;

            SchemeList[6] = new Scheme("Закачка [По объектам]");
            scheme = SchemeList[6];
            scheme.ByPlastMode = true;
            scheme.AxisX.CustomLabels.Enabled = false;
            scheme.AxisY[0].Title = "Закачка, м3";
            scheme.AxisY[0].MinFixed = 0;

            scheme.SetPlastModeSeries(Constant.Chart.SERIES_TYPE.INJECTION, true, true, 0, 0);

            scheme.DefaultIntervalType = 1;
            scheme.LastIntervalType = 1;
            scheme.DateTimeIntervalTypeEnabled[2] = false;

            // Common
            ((ToolStripComboBox)ToolBar.Items[0]).Items.Clear();
            for (i = 0; i < SchemeList.Length; i++)
            {
                ((ToolStripComboBox)ToolBar.Items[0]).Items.Add(SchemeList[i].Name);
            }
            ((ToolStripComboBox)ToolBar.Items[0]).SelectedIndex = 0;
            SchemeActivate(0);
            chart.ChartAreas[0].ChartScaleChange += new ChartAreaScaleChangeDelegate(DataChart_ChartScaleChange);
            chart.ChartAreas[0].ChartLegendItemChange += new ChartLegendItemChangeDelegate(DataChart_ChartLegendItemChange);
            chart.OnChartSnapShoot += new OnChartSnapShootDelegate(DataChart_OnChartSnapShoot);
        }

        public void SchemeActivate(int index)
        {
            if(chart.ChartAreas.Count == 1)
            {
                int i;
                ChartArea area = chart.ChartAreas[0];
                Scheme scheme = SchemeList[index];
                ActiveScheme = index;
                for (i = 0; i < 3; i++)
                {
                    mainForm.SetIntervalEnabled(i, scheme.DateTimeIntervalTypeEnabled[i]);
                }
                this.IntervalType = scheme.LastIntervalType;
                mainForm.SetInervalChecked(scheme.LastIntervalType);
                area.Legend.Enabled = this.LegendVisible;
                area.Legend.Clear();
                area.Legend.Position = ChartPosition.Bottom;
                area.AxisX.GetStyle(scheme.AxisX);
                area.AxisX2.GetStyle(scheme.AxisX2);
                while (area.AxisY.Length > scheme.AxisY.Length) area.RemoveAxisY();
                while (area.AxisY2.Length > scheme.AxisY2.Length) area.RemoveAxisY2();
                while (area.AxisY.Length < scheme.AxisY.Length) area.AddAxisY();
                while (area.AxisY2.Length < scheme.AxisY2.Length) area.AddAxisY2();

                for (i = 0; i < area.AxisY.Length; i++) area.AxisY[i].GetStyle(scheme.AxisY[i]);
                for (i = 0; i < area.AxisY2.Length; i++) area.AxisY2[i].GetStyle(scheme.AxisY2[i]);
                
                area.Series.Clear();
                area.Groups.Clear();
                int groupIndex = 0;
                for (i = 0; i < scheme.Series.Count; i++)
                {
                    groupIndex = scheme.Series[i].DefaultSeries.GroupIndex;
                    area.AddSeries(scheme.Series[i].DefaultSeries.Name,
                                   scheme.Series[i].DefaultSeries.Unit,
                                   scheme.Series[i].DefaultSeries.ChartType,
                                   scheme.Series[i].xAxisPrimary,
                                   scheme.Series[i].yAxisPrimary,
                                   scheme.Series[i].yAxisIndex,
                                   groupIndex);
                    area.Series[i].GetStyle(scheme.Series[i].DefaultSeries);
                }
                ReLoadByActiveScheme();
            }
        }
        void SetToolbarItemsEnable(bool Enabled)
        {
            if (DrawWaitIconMode == Enabled)
            {
                if ((this.ToolBar != null) && (this.ToolBar.Items.Count > 0))
                {
                    for (int i = 0; i < this.ToolBar.Items.Count; i++)
                    {
                        this.ToolBar.Items[i].Enabled = Enabled;
                    }
                }
            }
        }

        public void SetProject(Project Project)
        {
            project = Project;
            tableGraphics.SetProject(project);
            CreateSeriesInPlastModeSchemes();
        }
        public void ClearProject() 
        { 
            this.project = null; 
            this.Clear();
            this.tableGraphics.SetProject(null);
            this.tableGraphics.ClearTable();
        }
        public void Clear()
        {
            SourceObj = null;
            AccList.Clear();
            ProjParamsXValue = -1;
            chart.ChartAreas.Clear();
            chart.AddChartArea();
            chart.ChartAreas[chart.ChartAreas.Count - 1].ChartScaleChange +=new ChartAreaScaleChangeDelegate(DataChart_ChartScaleChange);
            chart.ChartAreas[chart.ChartAreas.Count - 1].ChartLegendItemChange += new ChartLegendItemChangeDelegate(DataChart_ChartLegendItemChange);
            chart.ClearBitMap();
            SchemeActivate(ActiveScheme);
        }

        public void Update()
        {
            chart.Update();
        }

        // SETTINGS FORM
        public void ShowSettings()
        {
            if (settings != null)
            {
                if (settings.ShowDialogSaved(mainForm) == DialogResult.OK)
                {
                    ReLoadByActiveScheme();
                }
            }
        }

        public ChartArea GetChartArea(int index)
        {
            return chart.ChartAreas[index];
        }
        public bool SetSourceObject(BaseObj DataSourceObject)
        {
            bool res = false;
            if (this.SourceObj != null)
            {
                res = this.SourceObj.TypeID == DataSourceObject.TypeID;
            }
            this.SourceObj = DataSourceObject;
            return res;
        }

        public void ReLoadByActiveScheme()
        {
            if (project != null)
            {
                chart.ChartAreas[0].Enabled = false;
                chart.ChartAreas[0].Series.SeriesVisibleFixed = settings.SeriesVisibleFixed;
                chart.DrawChartAreas();
                StartDrawWaitIcon();
                this.AllOilObjChecked = filterOilObj.AllCurrentSelected;
                int countObjects = filterOilObj.CurrentObjects.Count;
                if ((SourceObj != null) &&
                    ((SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.OILFIELD) ||
                     (SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT)))
                {
                    this.AllOilObjChecked = filterOilObj.AllCurrentOilFieldSelected;
                    countObjects = filterOilObj.CurrentOilFieldObjects.Count;
                }
                ReLoadByActiveScheme(this.IntervalType);
                mainForm.SetInervalChecked(this.IntervalType);
                StopDrawWaitIcon();
                chart.ChartAreas[0].Enabled = true;
                chart.DrawChartAreas();
                if ((this.IntervalType == 2) && (!this.AllOilObjChecked) && (countObjects > 0))
                {
                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Графики", "Для отображения шахматки включите все текущие объекты в Фильтре объектов!");
                }
            }
        }
        public void ReLoadByActiveScheme(short NewIntervalType)
        {
            if ((SourceObj != null) && (SchemeList.Length > 0))
            {
                //mainForm.StatUsage.AddMessage(CollectorStatId.UPDATE_GRAPHICS);
                if (NewIntervalType != -1)
                {
                    this.IntervalType = NewIntervalType;
                    SchemeList[ActiveScheme].LastIntervalType = NewIntervalType;
                }
                else if (SchemeList.Length > 0)
                {
                    this.IntervalType = SchemeList[ActiveScheme].DefaultIntervalType;
                }
                else
                    this.IntervalType = 1;

                ChartArea area = chart.ChartAreas[0];
                if (mainForm.canv.IsGraphicsByOneDate)
                {
                    area.AxisX.ValueType = ChartValueType.Double;
                    area.AxisX2.ValueType = ChartValueType.Double;
                    area.AxisX.IntervalType = DateTimeIntervalType.Number;
                    area.AxisX2.IntervalType = DateTimeIntervalType.Number;
                }
                else
                {
                    area.AxisX.ValueType = ChartValueType.DateTime;
                    area.AxisX2.ValueType = ChartValueType.DateTime;
                    switch (this.IntervalType)
                    {
                        case 0:
                            area.AxisX.IntervalType = DateTimeIntervalType.Years;
                            area.AxisX2.IntervalType = DateTimeIntervalType.Years;
                            break;
                        case 1:
                            area.AxisX.IntervalType = DateTimeIntervalType.Months;
                            area.AxisX2.IntervalType = DateTimeIntervalType.Months;
                            break;
                        case 2:
                            area.AxisX.IntervalType = DateTimeIntervalType.Days;
                            area.AxisX2.IntervalType = DateTimeIntervalType.Days;
                            break;
                    } 
                }
                bool VisibleShowProj, VisibleShowBP, VisibleShowPI = false;
                VisibleShowProj = (this.IntervalType == 0) && (ActiveScheme == 0) && AllOilObjChecked;
                VisibleShowBP = (this.IntervalType == 1) && (ActiveScheme == 0) && AllOilObjChecked;

                if ((SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.WELL_LIST) && (!ByPlastMode) && (!mainForm.canv.IsGraphicsByOneDate) &&
                    (mainForm.canv.selWellList.Count == 1))
                {
                    VisibleShowBP = false;
                    VisibleShowProj = false;
                    VisibleShowPI = false;
                    if (((BaseObj)mainForm.canv.selWellList[0]).TypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                    {
                        Well w = ((PhantomWell)mainForm.canv.selWellList[0]).srcWell;
                        ShowData(w.OilFieldIndex, w.Index);
                    }
                    else
                    {
                        Well w = (Well)mainForm.canv.selWellList[0];
                        ShowData(w.OilFieldIndex, w.Index);
                    }
                }
                else
                {
                    switch (SourceObj.TypeID)
                    {
                        case Constant.BASE_OBJ_TYPES_ID.WELL:
                            this.ShowProjParams = false;
                            this.ShowBPParams = false;
                            VisibleShowBP = false;
                            VisibleShowProj = false;
                            VisibleShowPI = (ActiveScheme == 1 || ActiveScheme == 2 || ActiveScheme == 3);
                            LoadDataFromWell((Well)SourceObj);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                            this.ShowProjParams = VisibleShowProj && ((ToolStripButton)ToolBar.Items[3]).Checked;
                            this.ShowBPParams = VisibleShowBP && ((ToolStripButton)ToolBar.Items[4]).Checked;
                            AddSumParams(mainForm.canv);
                            VisibleShowPI = false;
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.WELL_LIST:
                            this.ShowProjParams = false;
                            this.ShowBPParams = false;
                            VisibleShowBP = false;
                            VisibleShowProj = false;
                            VisibleShowPI = false;
                            AddSumParams(mainForm.canv);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.AREA:
                            this.ShowProjParams = false;
                            this.ShowBPParams = false;
                            VisibleShowBP = false;
                            VisibleShowProj = false;
                            VisibleShowPI = false;
                            AddSumParams(mainForm.canv);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                            this.ShowProjParams = VisibleShowProj && ((ToolStripButton)ToolBar.Items[3]).Checked;
                            this.ShowBPParams = VisibleShowBP && ((ToolStripButton)ToolBar.Items[4]).Checked;
                            VisibleShowPI = false;
                            AddSumParams(mainForm.canv);
                            break;
                    }
                }

                chart.DrawChartAreas();
                ((ToolStripButton)ToolBar.Items[3]).Visible = VisibleShowProj;
                if (((ToolStripButton)ToolBar.Items[12]).Tag != null && (bool)((ToolStripButton)ToolBar.Items[12]).Tag)
                {
                    ((ToolStripButton)ToolBar.Items[12]).Visible = VisibleShowPI;
                }

                if (((ToolStripButton)ToolBar.Items[4]).Tag != null && (bool)((ToolStripButton)ToolBar.Items[4]).Tag)
                {
                    ((ToolStripButton)ToolBar.Items[4]).Visible = VisibleShowBP;
                }
                
                if (TableVisible) this.tableGraphics.ShowData();
            }
        }

        #region SHOW/HIDE DATA
        public void SetChartLabelsEnabled(int DataType, bool Enabled)
        {
            if (chart.ChartAreas.Count > 0)
            {
                switch (DataType)
                {
                    case 0:
                        this.ShowGTM = Enabled;
                        break;
                    case 1:
                        this.ShowWellAction = Enabled;
                        break;
                }
                chart.ChartAreas[0].AxisX.ChartLabels.SetLabelsEnabled(DataType, Enabled);
                chart.DrawChartAreas();
            }
        }
        public void SetAxisLabelsEnabled(int DataType, bool Enabled)
        {
            if (chart.ChartAreas.Count > 0)
            {
                switch (DataType)
                {
                    case 0:
                        this.ShowWellLeak = Enabled;
                        break;
                    case 1:
                        this.ShowWellSuspend = Enabled;
                        break;
                }
                chart.ChartAreas[0].AxisX.AxisLabels.SetLabelsEnabled(DataType, Enabled);
                chart.DrawChartAreas();
            }
        }
        public void SetAxisColorZonesEnabled(int DataType, bool Enabled)
        {
            if (chart.ChartAreas.Count > 0)
            {
                switch (DataType)
                {
                    case 0:
                        this.ShowWellStop = Enabled;
                        break;
                }
                chart.ChartAreas[0].AxisX.ChartColorZones.SetColorZonesEnabled(DataType, Enabled);
                chart.DrawChartAreas();
            }
        }

        #endregion

        #region Points
        public void AddPoint(Constant.Chart.SERIES_TYPE Type, double XValue, double YValue)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Type);
            if (ind > -1)
            {
                chart.ChartAreas[0].Series[ind].AddXY(XValue, YValue);
            }
        }
        public void AddPoint(Constant.Chart.SERIES_TYPE Type, double XValue, double YValue, string Comment)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Type);
            if (ind > -1)
            {
                chart.ChartAreas[0].Series[ind].AddXY(XValue, YValue, Comment);
            }
        }
        public void AddPoint(Constant.Chart.SERIES_TYPE Type, double XValue, double YValue1, double YValue2, SeriesOperation Operation)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Type);
            if (ind > -1)
            {
                chart.ChartAreas[0].Series[ind].AddArithmeticXY(XValue, YValue1, YValue2, Operation);
            }
        }
        public void InsertPoint(int index, Constant.Chart.SERIES_TYPE Type, double XValue, double YValue)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Type);
            if ((ind > -1) && ((index > -1) && (index < chart.ChartAreas[0].Series[ind].Points.Count)))
            {
                chart.ChartAreas[0].Series[ind].Insert(index, XValue, YValue);
            }
            else if (ind > -1)
            {
                chart.ChartAreas[0].Series[ind].AddXY(XValue, YValue);
            }
        }
        public void InsertPoint(int index, Constant.Chart.SERIES_TYPE Type, double XValue, double YValue1, double YValue2, SeriesOperation Operation)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Type);
            if (ind > -1)
            {
                if ((ind > -1) && ((index > -1) && (index < chart.ChartAreas[0].Series[ind].Points.Count)))
                {
                    chart.ChartAreas[0].Series[ind].InsertArithmetic(index, XValue, YValue1, YValue2, Operation);
                }
                else if (ind > -1)
                {
                    AddPoint(Type, XValue, YValue1, YValue2, Operation);
                }
            }
        }
        public void SumPoint(int index, Constant.Chart.SERIES_TYPE Type, double XValue, double YValue)
        {
            DataPoint dp;
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Type);
            if (ind > -1)
            {
                if ((index > -1) && (index < chart.ChartAreas[0].Series[ind].Points.Count))
                {
                    dp = chart.ChartAreas[0].Series[ind].Points[index];
                    dp.YValue += YValue;
                    if(chart.ChartAreas[0].Series[ind].MaxY < dp.YValue)
                    {
                        chart.ChartAreas[0].Series[ind].MaxY = dp.YValue;
                    }
                    chart.ChartAreas[0].Series[ind].Points[index] = dp;
                }
                else
                {
                    InsertPoint(index, Type, XValue, YValue);
                }
            }
        }
        public void SumPoint(int index, Constant.Chart.SERIES_TYPE Type, double XValue, double YValue1, double YValue2, SeriesOperation Operation)
        {
            DataPoint dp;
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Type);
            if (ind > -1)
            {
                if ((index > -1) && (index < chart.ChartAreas[0].Series[ind].Points.Count))
                {
                    dp = chart.ChartAreas[0].Series[ind].Points[index];
                    dp.YValues[0] += YValue1;
                    dp.YValues[1] += YValue2;
                    switch (Operation)
                    {
                        case SeriesOperation.Division:
                            dp.YValue = 0;
                            if (dp.YValues[1] != 0)
                            {
                                dp.YValue = dp.YValues[0] / dp.YValues[1];
                            }
                            break;
                    }
                    if (chart.ChartAreas[0].Series[ind].MaxY < dp.YValue)
                    {
                        chart.ChartAreas[0].Series[ind].MaxY = dp.YValue;
                    }
                    chart.ChartAreas[0].Series[ind].Points[index] = dp;
                }
                else
                {
                    InsertPoint(index, Type, XValue, YValue1, YValue2, Operation);
                }
            }
        }

        public void AddPoint(int PlastCode, double XValue, double YValue)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByPlastCode(PlastCode);
            if (ind > -1)
            {
                chart.ChartAreas[0].Series[ind].AddXY(XValue, YValue);
            }
        }
        public void AddPoint(int PlastCode, double XValue, double YValue, string Comment)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByPlastCode(PlastCode);
            if (ind > -1)
            {
                chart.ChartAreas[0].Series[ind].AddXY(XValue, YValue, Comment);
            }
        }
        public void AddPoint(int PlastCode, double XValue, double YValue1, double YValue2, SeriesOperation Operation)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByPlastCode(PlastCode);
            if (ind > -1)
            {
                chart.ChartAreas[0].Series[ind].AddArithmeticXY(XValue, YValue1, YValue2, Operation);
            }
        }
        public void InsertPoint(int index, int PlastCode, double XValue, double YValue)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByPlastCode(PlastCode);
            if ((ind > -1) && ((index > -1) && (index < chart.ChartAreas[0].Series[ind].Points.Count)))
            {
                chart.ChartAreas[0].Series[ind].Insert(index, XValue, YValue);
            }
            else if (ind > -1)
            {
                chart.ChartAreas[0].Series[ind].AddXY(XValue, YValue);
            }
        }
        public void InsertPoint(int index, int PlastCode, double XValue, double YValue1, double YValue2, SeriesOperation Operation)
        {
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByPlastCode(PlastCode);
            if (ind > -1)
            {
                if ((ind > -1) && ((index > -1) && (index < chart.ChartAreas[0].Series[ind].Points.Count)))
                {
                    chart.ChartAreas[0].Series[ind].InsertArithmetic(index, XValue, YValue1, YValue2, Operation);
                }
                else if (ind > -1)
                {
                    AddPoint(PlastCode, XValue, YValue1, YValue2, Operation);
                }
            }
        }
        public void SumPoint(int index, int PlastCode, double XValue, double YValue)
        {
            DataPoint dp;
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByPlastCode(PlastCode);
            if (ind > -1)
            {
                if ((index > -1) && (index < chart.ChartAreas[0].Series[ind].Points.Count))
                {
                    dp = chart.ChartAreas[0].Series[ind].Points[index];
                    if (XValue != dp.XValue)
                    {
                        MessageBox.Show(DateTime.FromOADate(XValue).ToShortDateString() + " - " + DateTime.FromOADate(dp.XValue).ToShortDateString());
                    }
                    dp.YValue += YValue;
                    if (chart.ChartAreas[0].Series[ind].MaxY < dp.YValue)
                    {
                        chart.ChartAreas[0].Series[ind].MaxY = dp.YValue;
                    }
                    chart.ChartAreas[0].Series[ind].Points[index] = dp;
                }
                else
                {
                    InsertPoint(index, PlastCode, XValue, YValue);
                }
            }
        }
        public void SumPoint(int index, int PlastCode, double XValue, double YValue1, double YValue2, SeriesOperation Operation)
        {
            DataPoint dp;
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByPlastCode(PlastCode);
            if (ind > -1)
            {
                if ((index > -1) && (index < chart.ChartAreas[0].Series[ind].Points.Count))
                {
                    dp = chart.ChartAreas[0].Series[ind].Points[index];
                    dp.YValues[0] += YValue1;
                    dp.YValues[1] += YValue2;
                    switch (Operation)
                    {
                        case SeriesOperation.Division:
                            dp.YValue = 0;
                            if (dp.YValues[1] != 0)
                            {
                                dp.YValue = dp.YValues[0] / dp.YValues[1];
                            }
                            break;
                    }
                    chart.ChartAreas[0].Series[ind].Points[index] = dp;
                }
                else
                {
                    InsertPoint(index, PlastCode, XValue, YValue1, YValue2, Operation);
                }
            }
        }
        public void ClearPoints()
        {
            AccList.Clear();
            ProjParamsXValue = -1;
            chart.ChartAreas[0].ClearPoints();
        }
        #endregion

        #region X Axis
        public void SetAxesXInterval()
        {
            if ((chart != null) && (chart.ChartAreas.Count > 0))
            {
                ChartArea area = chart.ChartAreas[0];
                if ((area.AxisX != null) && (area.AxisX.Enabled) &&
                    (area.AxisX.ScaleView.Minimum != Constant.DOUBLE_NAN) &&
                    (area.AxisX.ScaleView.Maximum != Constant.DOUBLE_NAN))
                {
                    switch (area.AxisX.ValueType)
                    {
                        case ChartValueType.DateTime:
                            DateTime dt1 = DateTime.FromOADate(area.AxisX.ScaleView.Minimum), dt2 = DateTime.FromOADate(area.AxisX.ScaleView.Maximum);
                            area.AxisX.MajorGrid.Interval = 1;
                            area.AxisX.MajorTickMark.Interval = 1;
                            area.AxisX.LabelStyle.Interval = 1;
                            if ((dt2 - dt1).Days > 3650)
                            {
                                area.AxisX.MinorGrid.Enabled = false;
                                area.AxisX.MinorTickmark.Enabled = false;
                                area.AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Years;
                                area.AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Years;
                                area.AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Years;
                                area.AxisX.LabelStyle.IntervalOffset = 6;
                                area.AxisX.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Months;
                                area.AxisX.LabelStyle.Format = "yyyy";
                            }
                            else if ((dt2 - dt1).Days > 1095)
                            {
                                area.AxisX.MinorGrid.Enabled = false;
                                area.AxisX.MinorTickmark.Enabled = true;
                                area.AxisX.MinorTickmark.IntervalType = DateTimeIntervalType.Months;
                                area.AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Years;
                                area.AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Years;
                                area.AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Years;
                                area.AxisX.LabelStyle.IntervalOffset = 6;
                                area.AxisX.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Months;
                                area.AxisX.LabelStyle.Format = "yyyy";
                            }
                            else
                            {
                                area.AxisX.MinorGrid.Enabled = true;
                                area.AxisX.MinorGrid.IntervalType = DateTimeIntervalType.Months;
                                area.AxisX.MinorTickmark.Enabled = true;
                                area.AxisX.MinorTickmark.IntervalType = DateTimeIntervalType.Months;
                                area.AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Years;
                                area.AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Years;
                                area.AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Months;
                                area.AxisX.LabelStyle.IntervalOffset = 0;
                                area.AxisX.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Months;
                                area.AxisX.LabelStyle.Format = "MM.yyyy";
                            }
                            break;
                        case ChartValueType.Double:
                            if (area.AxisX.ScaleView.Maximum - area.AxisX.ScaleView.Minimum > 300)
                            {
                                area.AxisX.MinorGrid.Enabled = false;
                                area.AxisX.MinorTickmark.Enabled = false;
                                area.AxisX.MajorGrid.Interval = 100;
                                area.AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.MajorTickMark.Interval = 100;
                                area.AxisX.LabelStyle.Interval = 100;
                                area.AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.LabelStyle.IntervalOffset = -1;
                                area.AxisX.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Number;
                                area.AxisX.LabelStyle.Format = "0";
                            }
                            else if (area.AxisX.ScaleView.Maximum - area.AxisX.ScaleView.Minimum > 30)
                            {
                                area.AxisX.MinorGrid.Enabled = false;
                                area.AxisX.MinorTickmark.Enabled = false;
                                area.AxisX.MajorGrid.Interval = 10;
                                area.AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.MajorTickMark.Interval = 10;
                                area.AxisX.LabelStyle.Interval = 10;
                                area.AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.LabelStyle.IntervalOffset = -1;
                                area.AxisX.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Number;
                                area.AxisX.LabelStyle.Format = "0";
                            }
                            else
                            {
                                area.AxisX.MinorGrid.Enabled = false;
                                area.AxisX.MinorTickmark.Enabled = false;
                                area.AxisX.MajorGrid.Interval = 1;
                                area.AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.MajorTickMark.Interval = 1;
                                area.AxisX.LabelStyle.Interval = 1;
                                area.AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Number;
                                area.AxisX.LabelStyle.IntervalOffset = -1;
                                area.AxisX.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Number;
                                area.AxisX.LabelStyle.Format = "0";
                            }
                            if (area.AxisX.ScaleView.Minimum <= 0)
                            {
                                int offset = (-(int)area.AxisX.Minimum) % (int)area.AxisX.MajorGrid.Interval;
                                area.AxisX.MajorGrid.IntervalOffset = offset;
                                area.AxisX.MajorTickMark.IntervalOffset = offset;
                                area.AxisX.LabelStyle.IntervalOffset = offset;
                            }
                            break;
                        case ChartValueType.Int32:
                            break;
                    }
                }
            }
        }
        #endregion

        #region TABLE
        public void ShowDataTable()
        {
            tableGraphics.ShowData();
        }
        public void SetLastRow()
        {
            if ((tableGraphics.Rows.Count > 0) && (tableGraphics.ClientRectangle.Height > 15))
            {
                tableGraphics.FirstDisplayedScrollingRowIndex = tableGraphics.Rows.Count - 1;
            }
        }
        #endregion

        #region BY PLAST INDEX MODE
        void CreateSeriesInPlastModeSchemes()
        {
            for (int i = 0; i < SchemeList.Length; i++)
            {
                if ((SchemeList[i].ByPlastMode) && (SchemeList[i].PlastModeSeries != null) && (mainForm.canv.MapProject != null))
                {
                    var PlastDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                    StratumTreeNode PlastNode;
                    SeriesTyped series, src = SchemeList[i].PlastModeSeries;
                    SchemeList[i].Series.Clear();
                    int plastCode, argb;
                    for (int j = 0; j < mainForm.canv.MapProject.MerStratumCodes.Count; j++)
                    {
                        plastCode = mainForm.canv.MapProject.MerStratumCodes[j];
                        PlastNode = PlastDict.GetStratumTreeNode(plastCode);
                        if (PlastNode != null)
                        {
                            series = SchemeList[i].AddSeries(src.SeriesType, src.xAxisPrimary, src.yAxisPrimary, src.yAxisIndex, src.DefaultSeries.GroupIndex);
                            series.DefaultSeries.ChartType = SeriesChartType.StackedArea;
                            series.DefaultSeries.Enabled = false;

                            if (PlastNode.ARGB == -1)
                            {
                                argb = PlastNode.GetParentStratumColor();
                                if (argb != -1)
                                {
                                    series.DefaultSeries.Color = Color.FromArgb(argb);
                                }
                                else
                                {
                                    series.DefaultSeries.Color = Color.LightGray;
                                }
                            }
                            else
                            {
                                series.DefaultSeries.Color = Color.FromArgb(PlastNode.ARGB);
                            }
                            series.DefaultSeries.BorderWidth = 1;
                            series.DefaultSeries.Name = PlastNode.Name;
                            series.PlastCode = plastCode;
                        }
                    }
                }
            }
        }
        #endregion

        #region Plast Colors
        public void UpdateOilObjColors()
        {
            int i, j;
            var dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            int color;
            for (i = 0; i < SchemeList.Length; i++)
            {
                for(j = 0; j < SchemeList[i].Series.Count;j++)
                {
                    if (SchemeList[i].Series[j].PlastCode > 0)
                    {
                        color = dict.GetStratumColorByStratumTree(SchemeList[i].Series[j].PlastCode);
                        SchemeList[i].Series[j].DefaultSeries.Color = Color.FromArgb(color);
                    }
                }
            }
            if (ByPlastMode)
            {
                Scheme scheme = SchemeList[ActiveScheme];
                ChartArea area = chart.ChartAreas[0];
                for (i = 0; i < scheme.Series.Count; i++)
                {
                    area.Series[i].Color = scheme.Series[i].DefaultSeries.Color;
                }
            }
            chart.DrawChartAreas();
        }
        #endregion

        // LOADING DATA
        #region LOAD WELL
        public void ShowData(int oilFieldIndex, int wellIndex)
        {
            if (oilFieldIndex >= 0)
            {
                OilField of = project.OilFields[oilFieldIndex];
                var dictArea = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                Well w = of.Wells[wellIndex];
                SetSourceObject(w);
                string str = "";
                if ((w.OilFieldAreaCode > 0) && (dictArea != null))
                {
                    str = ", " + dictArea.GetShortNameByCode(w.OilFieldAreaCode) + " площадь";
                }
                mainForm.dChartSetWindowText(String.Format("Графики - Скв: {0} [{1}, {2}{3}]", w.UpperCaseName, of.ParamsDict.NGDUName, of.ParamsDict.OilFieldName, str));
                ReLoadByActiveScheme();
            }
            else
            {
                Clear();
                mainForm.dChartSetWindowText("Графики");
            }
        }
        internal class MerPlastData
        {
            public int PlastCode;
            public double Liq;
            public double LiqV;
            public double Oil;
            public double NatGas;
            public double GasCond;
            public double Injection;
            public double InjectionGas;
        }
        internal class MerPlastDataCollection
        {
            public List<MerPlastData> Items;

            public MerPlastDataCollection()
            {
                Items = new List<MerPlastData>();
                MerPlastData item = new MerPlastData();
                item.PlastCode = -1;
                Items.Add(item);
            }
            public MerPlastData Add(int PlastCode)
            {
                MerPlastData item = new MerPlastData();
                item.PlastCode = PlastCode;
                Items.Add(item);
                return item;
            }
            public void AddProductionData(int PlastCode, double Liq, double Oil)
            {
                MerPlastData item = GetItem(PlastCode);
                if (item != null)
                {
                    item.Liq += Liq;
                    item.Oil += Oil;
                }
            }
            public void AddProductionNatGas(int PlastCode, double NatGas)
            {
                MerPlastData item = GetItem(PlastCode);
                if (item != null)
                {
                    item.NatGas += NatGas;
                }
            }
            public void AddProductionGasCond(int PlastCode, double GasCond)
            {
                MerPlastData item = GetItem(PlastCode);
                if (item != null)
                {
                    item.GasCond += GasCond;
                }
            }
            public void AddInjectionData(int PlastCode, double Inj)
            {
                MerPlastData item = GetItem(PlastCode);
                if (item != null)
                {
                    item.Injection += Inj;
                }
            }
            public void AddInjectionGasData(int PlastCode, double InjGas)
            {
                MerPlastData item = GetItem(PlastCode);
                if (item != null)
                {
                    item.InjectionGas += InjGas;
                }
            }

            public void ClearValues()
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    Items[i].Injection = 0;
                    Items[i].InjectionGas = 0;
                    Items[i].Liq = 0;
                    Items[i].LiqV = 0;
                    Items[i].Oil = 0;
                }
            }
            public MerPlastData GetItem(int PlastCode)
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    if (Items[i].PlastCode == PlastCode)
                    {
                        return Items[i];
                    }
                }
                return Add(PlastCode);
            }
        }
        void LoadDataFromWell(Well w)
        {
            ChartArea area = chart.ChartAreas[0];
            if (area.Series.Count == 0) return;
            Scheme scheme = SchemeList[ActiveScheme];
            AxisCustomLabels labels = area.AxisX.CustomLabels;
            ChartAreaLabels chartLabels = area.AxisX.ChartLabels;
            ChartAreaColorZones chartColorZones = area.AxisX.ChartColorZones;
            ChartAxisLabels axisLabels = area.AxisX.AxisLabels;
            chartLabels.BackColor = Color.SkyBlue;

            ClearPoints();
            if (labels != null) labels.Clear();

            ProjParamsXValue = -1;

            var dictPlast = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            var dictGTM = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.GTM_TYPE);
            var dictWellAction = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.WELL_ACTION_TYPE);
            var dictWellResearch = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.WELL_RESEARCH_TYPE);
            var dictStayReason = (DataDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STAY_REASON);


            int i, j, ind;
            double MinChessDt = DateTime.MinValue.ToOADate();
            double sumLiq, sumWat, sumOil, sumInj, sumInjGas, sumNatGas, sumGasCond;
            double sumLiqV, sumOilV;
            double QLiq, QLiqV, QOil, QOilV, WInj, WInjGas, QNatGas, QGasCond;
            double dtVal;
            DateTime predDT;
            bool bMer = scheme.IsUsedSource(Constant.Chart.DATA_SOURCE_TYPE.MER);
            bool bChess = scheme.IsUsedSource(Constant.Chart.DATA_SOURCE_TYPE.CHESS);
            bool bWellResearch = scheme.IsUsedSource(Constant.Chart.DATA_SOURCE_TYPE.WELL_RESEARCH);
            bool ShowInCube = (mainForm.AppSettings != null) ? mainForm.AppSettings.ShowDataLiquidInCube : false;

            if ((filterOilObj == null) || (dictPlast == null)) return;
            if (!w.GTMLoaded)
            {
                (project.OilFields[w.OilFieldIndex]).LoadGTMFromCache(w.Index);
            }
            if (!w.ActionLoaded)
            {
                (project.OilFields[w.OilFieldIndex]).LoadWellActionFromCache(w.Index);
            }
            if (!w.SuspendLoaded)
            {
                (project.OilFields[w.OilFieldIndex]).LoadWellSuspendFromCache(w.Index);
            }
            if (!w.LeakLoaded)
            {
                (project.OilFields[w.OilFieldIndex]).LoadWellLeakFromCache(w.Index);
            }
            if (bWellResearch && !w.ResearchLoaded)
            {
                (project.OilFields[w.OilFieldIndex]).LoadWellResearchFromCache(w.Index);
            }

            #region CHESS

            if ((bChess) && ((!w.ChessLoaded) && (!w.ChessInjLoaded)) &&
                ((filterOilObj.AllCurrentSelected) || (filterOilObj.CurrentObjects.Count == 0)))
            {
                (project.OilFields[w.OilFieldIndex]).LoadChessFromCache(w.Index);
            }
            double StartStay = -1;
            int StartStayReason = -1;

            DateTime lastDate = DateTime.Now.Date.AddDays(-1);
            //lastDate = new DateTime(lastDate.Year, lastDate.Month, lastDate.Day);

            if ((bChess) && (w.ChessLoaded) && ((filterOilObj.AllCurrentSelected) || (filterOilObj.SelectedCurrentObjects.Count == 0)))
            {
                if(w.ChessInjLoaded && w.chess[w.chess.Count - 1].Date < w.chessInj[0].Date) 
                {
                    lastDate = w.chessInj[0].Date;
                }
                ChessItem dayItem;
                double sumChess = -1;
                i = 0;
                if(w.chess.Count > 0) 
                {
                    predDT = w.chess[0].Date;
                    MinChessDt = new DateTime(predDT.Year, predDT.Month, 1).ToOADate();
                    QLiq = 0;
                    QOil = 0;
                    QLiqV = 0;
                    QOilV = 0;
                    while (i < w.chess.Count)
                    {
                        dayItem = w.chess[i];
                        dtVal = dayItem.Date.ToOADate();

                        if (dayItem.StayTime == 24)
                        {
                            if (StartStay == -1) StartStay = dayItem.Date.ToOADate();
                            if (StartStayReason == -1)
                            {
                                StartStayReason = dayItem.StayReason;
                                if (StartStayReason == 0 && i > 0 && w.chess[i - 1].StayTime > 0 && w.chess[i - 1].StayReason > 0)
                                {
                                    StartStayReason = w.chess[i - 1].StayReason;
                                }
                            }
                        }
                        if (StartStay != -1 && (dayItem.StayTime != 24 || (dayItem.StayReason > 0 && StartStayReason != dayItem.StayReason)))
                        {
                            chartColorZones.AddZone(this.ShowWellStop, 0, StartStay, dayItem.Date.ToOADate(), Color.FromArgb(50, Color.Red), dictStayReason.GetShortNameByCode(StartStayReason));
                            StartStay = -1;
                            StartStayReason = -1;
                            if (dayItem.StayTime == 24 && dayItem.StayReason > 0 && StartStayReason != dayItem.StayReason)
                            {
                                StartStay = dayItem.Date.ToOADate();
                                StartStayReason = dayItem.StayReason;
                            }
                        }
                        if (predDT == dayItem.Date)
                        {
                            if (/*(dayItem.StayTime != 24) || */(sumChess != dayItem.Qliq + dayItem.Qoil))
                            {
                                QLiq = dayItem.Qliq; 
                                QOil = dayItem.Qoil;
                                QLiqV = dayItem.QliqV;
                                QOilV = dayItem.QoilV;
                            }
                        }
                        else
                        {
                            predDT = dayItem.Date;
                        }
                        if ((sumChess != QLiq + QOil) || (i == w.chess.Count - 1))
                        {
                            if (!ShowInCube)
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_T, dtVal, QLiq);
                                if (QLiq > 0) AddPoint(Constant.Chart.SERIES_TYPE.WATERING_CHESS, dtVal, (QLiq - QOil) * 100, QLiq, SeriesOperation.Division);
                            }
                            else
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_M, dtVal, QLiqV);
                                if (QLiqV > 0) AddPoint(Constant.Chart.SERIES_TYPE.WATERING_CHESS_M, dtVal, (QLiqV - QOilV) * 100, QLiqV, SeriesOperation.Division);
                            }
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_T, dtVal, QOil);
                            
                            sumChess = dayItem.Qliq + dayItem.Qoil;
                        }
                        if (i == w.chess.Count - 1 && QLiq + QOil == 0 && StartStay != -1 && w.chess[i].Date < lastDate)
                        {
                            if (!ShowInCube)
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_T, lastDate.ToOADate(), QLiq);
                            }
                            else
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_CHESS_M, lastDate.ToOADate(), QLiqV);
                            }
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_T, lastDate.ToOADate(), QOil);
                        }
                        if (dayItem.Hdyn > 0)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.FLOWING_LEVEL, dtVal, dayItem.Hdyn);
                        }
                        i++;
                        predDT = predDT.AddDays(1);
                    }
                    if (StartStay != -1)
                    {
                        if (w.ChessInjLoaded && w.chessInj[0].Date < w.chess[0].Date && w.chess[w.chess.Count - 1].Date < w.chessInj[w.chessInj.Count - 1].Date)
                        {
                            DateTime dt = DateTime.FromOADate(StartStay);
                            for (j = w.chessInj.Count - 1; w.chessInj[j].Date > dt; j--)
                            {
                                lastDate = w.chessInj[j].Date;
                            }
                        }
                        if (DateTime.FromOADate(StartStay) < lastDate)
                        {
                            chartColorZones.AddZone(this.ShowWellStop, 0, StartStay, lastDate.AddDays(1).ToOADate(), Color.FromArgb(50, Color.Red), dictStayReason.GetShortNameByCode(StartStayReason));
                            StartStay = -1;
                            StartStayReason = -1;
                        }
                    }
                }
            }
            if ((bChess) && (w.ChessInjLoaded) && ((filterOilObj.AllCurrentSelected) || (filterOilObj.SelectedCurrentObjects.Count == 0)))
            {
                if (w.ChessLoaded && w.chessInj[w.chessInj.Count - 1].Date < w.chess[0].Date)
                {
                    lastDate = w.chess[0].Date;
                }
                ChessInjItem dayItem;
                double sumChess = -1;
                if (w.chessInj.Count > 0)
                {
                    predDT = w.chessInj[0].Date;
                    if (MinChessDt == DateTime.MinValue.ToOADate() || (MinChessDt > predDT.ToOADate()))
                    {
                        MinChessDt = new DateTime(predDT.Year, predDT.Month, 1).ToOADate();
                    }
                }
                StartStay = -1;
                StartStayReason = -1;
                for (i = 0; i < w.chessInj.Count; i++)
                {
                    dayItem = w.chessInj[i];
                    dtVal = dayItem.Date.ToOADate();
                    if (dayItem.StayTime == 24)
                    {
                        if (StartStay == -1) StartStay = dayItem.Date.ToOADate();
                        if (StartStayReason == -1)
                        {
                            StartStayReason = dayItem.StayReason;
                            if (StartStayReason == 0 && i > 0 && w.chessInj[i - 1].StayTime > 0 && w.chessInj[i - 1].StayReason > 0)
                            {
                                StartStayReason = w.chessInj[i - 1].StayReason;
                            }
                        }
                    }
                    if (StartStay != -1 && (dayItem.StayTime != 24 || (dayItem.StayReason > 0 && StartStayReason != dayItem.StayReason)))
                    {
                        chartColorZones.AddZone(this.ShowWellStop, 0, StartStay, dayItem.Date.ToOADate(), Color.FromArgb(50, Color.Red), dictStayReason.GetShortNameByCode(StartStayReason));
                        StartStay = -1;
                        StartStayReason = -1;
                    }
                    sumWat = (/*(dayItem.StayTime != 24) ||*/ (sumChess != dayItem.Wwat)) ? dayItem.Wwat : 0;

                    if ((sumChess != sumWat) || (i == w.chessInj.Count - 1))
                    {
                        AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_CHESS, dtVal, sumWat);
                        sumChess = dayItem.Wwat;
                    }
                    if (i == w.chessInj.Count - 1 && sumWat == 0 && StartStay != -1 && w.chessInj[i].Date < lastDate)
                    {
                        AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_CHESS, lastDate.ToOADate(), sumWat);
                    }
                }
                if (StartStay != -1)
                {
                    if (w.ChessLoaded && w.chess[0].Date < w.chessInj[0].Date && w.chessInj[w.chessInj.Count - 1].Date < w.chess[w.chess.Count - 1].Date)
                    {
                        DateTime dt = DateTime.FromOADate(StartStay);
                        for (j = w.chess.Count - 1; w.chess[j].Date > dt; j--)
                        {
                            lastDate = w.chess[j].Date;
                        }
                    }
                    if (DateTime.FromOADate(StartStay) < lastDate)
                    {
                        chartColorZones.AddZone(this.ShowWellStop, 0, StartStay, lastDate.AddDays(1).ToOADate(), Color.FromArgb(50, Color.Red), dictStayReason.GetShortNameByCode(StartStayReason));
                        StartStay = -1;
                        StartStayReason = -1;
                    }
                }
            }
            if (chartColorZones.Count > 0)
            {
                for (i = 0; i < chartColorZones.Count; i++)
                {
                    for (j = 0; j < chartColorZones.Count; j++)
                    {
                        if (i != j && chartColorZones[i].Start < chartColorZones[j].Start && chartColorZones[j].Start < chartColorZones[i].End)
                        {
                            ChartAreaColorZoneItem item = chartColorZones[i];
                            item.End = chartColorZones[j].Start;
                            chartColorZones[i] = item;
                        }
                    }

                }
            }
            if (bChess)
            {
                if (MinChessDt == DateTime.MinValue.ToOADate())
                {
                    MinChessDt = new DateTime(2008, 01, 01).ToOADate();
                }
            }            
            
            #endregion

            #region MER
            if ((bMer) && (!w.MerLoaded))
            {
                (project.OilFields[w.OilFieldIndex]).LoadMerFromCache(w.Index);
            }

            if ((w.MerLoaded) && (bMer))
            {
                int hours, predYear, sumPlast = 0, predSumPlast = -1, days;
                double Wat;
                bool bProd, bInj, bNatGas, bGasCond;
                string plasts = string.Empty;
                MerComp merComp = new MerComp(w.mer);
                List<int> plast_ids = new List<int>();
                MerWorkTimeItem wtItemProd, wtItemInj, wtItemScoop;
                double wtProd, wtInj, wtScoop;
                MerCompPlastItem plastItem;
                MerPlastDataCollection MerPlastData = new MerPlastDataCollection();
                AccumParams acc;
                DateTime dt;
                i = 0;
                sumPlast = 0;
                while (i < merComp.Count)
                {
                    if (this.IntervalType == 0)
                    {
                        dtVal = new DateTime(merComp[i].Date.Year, 1, 1).ToOADate();
                    }
                    else
                    {
                        dtVal = merComp[i].Date.ToOADate();
                    }
                    predYear = merComp[i].Date.Year;

                    sumWat = 0;
                    sumOil = 0;
                    sumInj = 0;
                    sumNatGas = 0;
                    sumGasCond = 0;
                    sumInjGas = 0;
                    sumLiq = 0;
                    sumLiqV = 0;
                    sumOilV = 0;
                    Wat = 0;
                    wtProd = 0; wtInj = 0; wtScoop = 0;
                    bProd = false;
                    bInj = false;
                    bNatGas = false;
                    bGasCond = false;
                    MerPlastData.ClearValues();
                    wtItemProd.WorkTime = 0;
                    wtItemInj.WorkTime = 0;
                    wtItemScoop.WorkTime = 0;
                    plast_ids.Clear();
                    sumPlast = 0;
                    plasts = string.Empty;

                    // Сумма по месяцу или году
                    while ((((i < merComp.Count) &&
                           (((this.IntervalType == 1) && (dtVal == merComp[i].Date.ToOADate())) ||
                            ((this.IntervalType == 2) && (dtVal == merComp[i].Date.ToOADate())) ||
                            ((this.IntervalType == 0) && (predYear == merComp[i].Date.Year))))))
                    {
                        for (j = 0; j < merComp[i].PlastItems.Count; j++)
                        {
                            plastItem = merComp[i].PlastItems[j];
                            if (filterOilObj.SelectedObjects.IndexOf(dictPlast.GetIndexByCode(plastItem.PlastCode)) == -1)
                            {
                                continue;
                            }
                            wtItemProd = merComp[i].TimeItems.GetProductionTime(plastItem.PlastCode);
                            wtItemInj = merComp[i].TimeItems.GetInjectionTime(plastItem.PlastCode);
                            wtItemScoop = merComp[i].TimeItems.GetScoopTime(plastItem.PlastCode);
                            wtItemProd.WorkTime = wtItemProd.GetWorkHours();
                            wtItemInj.WorkTime = wtItemInj.GetWorkHours();
                            wtItemScoop.WorkTime = wtItemScoop.GetWorkHours();

                            sumLiq += plastItem.Liq;
                            sumOil += plastItem.Oil;
                            sumInj += plastItem.Inj;
                            sumLiqV += plastItem.LiqV;
                            sumOilV += plastItem.OilV;
                            sumNatGas += plastItem.NatGas;
                            sumGasCond += plastItem.Condensate;
                            sumInjGas += plastItem.InjGas;
                            for (int k = 0; k < merComp[i].CharWorkIds.Count; k++)
                            {
                                switch (merComp[i].CharWorkIds[k])
                                {
                                    case 11:
                                        MerPlastData.AddProductionData(plastItem.PlastCode, (!ShowInCube) ? plastItem.Liq : plastItem.LiqV, plastItem.Oil);
                                        bProd = true;
                                        break;
                                    case 12:
                                        MerPlastData.AddProductionNatGas(plastItem.PlastCode, plastItem.NatGas);
                                        bNatGas = true;
                                        break;
                                    case 15:
                                        MerPlastData.AddProductionGasCond(plastItem.PlastCode, plastItem.Condensate);
                                        bGasCond = true;
                                        break;
                                    case 20:
                                        MerPlastData.AddInjectionData(plastItem.PlastCode, plastItem.Inj);
                                        MerPlastData.AddInjectionGasData(plastItem.PlastCode, plastItem.InjGas);
                                        bInj = true;
                                        break;
                                }
                            }
                            #region ACCUM
                            acc = AccList.GetItemByOilObjCode(plastItem.PlastCode);
                            if (plastItem.Liq > 0)
                            {
                                if (acc.AccumLiq == -1)
                                {
                                    acc.AccumLiq = 0; acc.AccumOil = 0; acc.AccumWat = 0;
                                }
                                acc.AccumLiq += (!ShowInCube) ? plastItem.Liq : plastItem.LiqV;
                                acc.AccumOil += plastItem.Oil;
                                acc.AccumWat += plastItem.Liq - plastItem.Oil;
                            }
                            if (plastItem.Inj > 0)
                            {
                                if (acc.AccumInj == -1) acc.AccumInj = 0;
                                acc.AccumInj += plastItem.Inj;
                            }
                            if ((plastItem.InjGas > 0))
                            {
                                if (acc.AccumInjGas == -1) acc.AccumInjGas = 0;
                                acc.AccumInjGas += plastItem.InjGas;
                            }
                            #endregion

                            if (plast_ids.IndexOf(plastItem.PlastCode) == -1)
                            {
                                plast_ids.Add(plastItem.PlastCode);
                                sumPlast += plastItem.PlastCode;
                            }
                        }
                        wtItemProd = merComp[i].TimeItems.GetAllProductionTime();
                        wtItemInj = merComp[i].TimeItems.GetAllInjectionTime();
                        wtItemScoop = merComp[i].TimeItems.GetAllScoopTime();
                        hours = (merComp[i].Date.AddMonths(1) - merComp[i].Date).Days * 24;
                        wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                        wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                        wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;
                        wtProd += wtItemProd.WorkTime;
                        wtInj += wtItemInj.WorkTime;
                        wtScoop += wtItemScoop.WorkTime;
                        i++;
                    }

                    if ((bChess) && (dtVal < MinChessDt)) continue;

                    Wat = 0;
                    if (sumLiq > 0 && !ShowInCube)
                    {
                        Wat = (sumLiq - sumOil) * 100f / sumLiq;
                    }
                    else if (sumLiqV > 0 && ShowInCube)
                    {
                        Wat = (sumLiqV - sumOilV) * 100f / sumLiqV;
                    }

                    if ((labels != null) && ((predSumPlast == -1) || (sumPlast != predSumPlast)))
                    {
                        if (plast_ids.Count > 0)
                        {
                            plasts = string.Empty;
                            if (dictPlast != null)
                            {
                                for (j = 0; j < plast_ids.Count;j++) 
                                {
                                    if (j > 0) plasts += ",";
                                    plasts += dictPlast.GetShortNameByCode(plast_ids[j]);
                                }
                            }
                            labels.AddLabel(dtVal, plasts);
                        }
                        predSumPlast = sumPlast;
                    }
                    QLiq = 0; QOil = 0; QNatGas = 0; QGasCond = 0;
                    
                    if (wtProd > 0)
                    {
                        QLiq = (((ShowInCube) ? sumLiqV : sumLiq) * 24) / wtProd;
                        QOil = (sumOil * 24) / wtProd;
                        QNatGas = (sumNatGas * 24) / wtProd;
                        QGasCond = (sumGasCond * 24) / wtProd;
                    }
                    WInj = 0; WInjGas = 0;
                    if (wtInj > 0)
                    {
                        WInj = (sumInj * 24) / wtInj;
                        WInjGas = (sumInjGas * 24) / wtInj;
                    }

                    if (this.IntervalType == 2)
                    {
                        if (i < merComp.Count)
                        {
                            days = (merComp[i].Date - DateTime.FromOADate(dtVal)).Days;
                        }
                        else
                        {
                            days = (DateTime.FromOADate(dtVal).AddMonths(1) - DateTime.FromOADate(dtVal)).Days + 1;
                        }
                        dt = DateTime.FromOADate(dtVal);

                        for (j = 0; j < days; j++)
                        {
                            if (bProd)
                            {
                                AddPoint((!ShowInCube) ? Constant.Chart.SERIES_TYPE.Q_LIQUID_T : Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dtVal, QLiq);
                                AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T, dtVal, QOil);
                                AddPoint((!ShowInCube) ? Constant.Chart.SERIES_TYPE.WATERING : Constant.Chart.SERIES_TYPE.WATERING_M, dtVal, Wat);
                            }
                            if (bNatGas) AddPoint(Constant.Chart.SERIES_TYPE.Q_NATGAS, dtVal, QNatGas);
                            if (bGasCond) AddPoint(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dtVal, QGasCond);
                            if (bInj)
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.W_INJ, dtVal, WInj);
                                if (merComp.UseInjGas) AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_GAS, dtVal, WInjGas);
                            }
                            dt = dt.AddDays(1);
                            dtVal = dt.ToOADate();
                        }
                    }
                    else if (ByPlastMode)
                    {
                        for (j = 0; j < MerPlastData.Items.Count; j++)
                        {
                            if (MerPlastData.Items[j].PlastCode != -1)
                            {
                                switch (scheme.PlastModeSeries.SeriesType)
                                {
                                    case Constant.Chart.SERIES_TYPE.LIQUID_M:
                                        AddPoint(MerPlastData.Items[j].PlastCode, dtVal, MerPlastData.Items[j].LiqV);
                                        break;
                                    case Constant.Chart.SERIES_TYPE.LIQUID_T:
                                        AddPoint(MerPlastData.Items[j].PlastCode, dtVal, MerPlastData.Items[j].Liq);
                                        break;
                                    case Constant.Chart.SERIES_TYPE.OIL_T:
                                        AddPoint(MerPlastData.Items[j].PlastCode, dtVal, MerPlastData.Items[j].Oil);
                                        break;
                                    case Constant.Chart.SERIES_TYPE.INJECTION:
                                        if (MerPlastData.Items[j].Injection > -1)
                                        {
                                            AddPoint(MerPlastData.Items[j].PlastCode, dtVal, MerPlastData.Items[j].Injection);
                                        }
                                        break;
                                    case Constant.Chart.SERIES_TYPE.INJ_GAS:
                                        if (MerPlastData.Items[j].InjectionGas > -1)
                                        {
                                            AddPoint(MerPlastData.Items[j].PlastCode, dtVal, MerPlastData.Items[j].InjectionGas);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (bProd)
                        {
                            if (!ShowInCube)
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.LIQUID_T, dtVal, sumLiq);
                            }
                            else
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.LIQUID_M, dtVal, sumLiqV);
                            }
                            AddPoint(Constant.Chart.SERIES_TYPE.OIL_T, dtVal, sumOil);
                            AddPoint((!ShowInCube) ? Constant.Chart.SERIES_TYPE.WATERING : Constant.Chart.SERIES_TYPE.WATERING_M, dtVal, Wat);
                            AddPoint((!ShowInCube) ? Constant.Chart.SERIES_TYPE.Q_LIQUID_T : Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dtVal, QLiq);
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T, dtVal, QOil);
                        }
                        if (bNatGas)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.NATGAS, dtVal, sumNatGas);
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_NATGAS, dtVal, QNatGas);
                        }
                        if (bGasCond)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.GASCONDENSAT, dtVal, sumGasCond);
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dtVal, QGasCond);
                        }
                        if (bInj)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.INJECTION, dtVal, sumInj);
                            AddPoint(Constant.Chart.SERIES_TYPE.W_INJ, dtVal, WInj);
                            if (merComp.UseInjGas)
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.INJ_GAS, dtVal, sumInjGas / 1000);
                                AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_GAS, dtVal, WInjGas);
                            }
                        }
                    }
                }
            }

            #region old mer calc
            //if ((false) &&(w.MerLoaded) && (bMer))
            //{
            //    ArrayList plast_ids = new ArrayList();
            //    ArrayList customlabels = new ArrayList();
            //    MerItem merItem;
            //    MerWorkTimeItem wtItem;
            //    int sumPlast = 0, predSumPlast = -1;
            //    int days;
            //    string plasts = "";
            //    double Wat;
            //    bool bProd, bInj, bNatGas, bGasCond;
            //    DateTime dt;
            //    MerPlastDataCollection MerPlastData = new MerPlastDataCollection();
            //    int predYear;
            //    i = 0;
            //    merItem = w.mer.Items[0];
            //    MerWorkTimeCollection ProdWorkTime = new MerWorkTimeCollection();
            //    MerWorkTimeCollection InjWorkTime = new MerWorkTimeCollection();
            //    AccumParams acc;

            //    sumWat = 0; sumOil = 0; sumInj = 0; sumInjGas = 0; sumLiq = 0; Wat = 0; sumNatGas = 0; sumGasCond = 0;
            //    bProd = false; bInj = false; bNatGas = false; bGasCond = false;
            //    predDT = DateTime.MinValue.ToOADate();
            //    while (i < w.mer.Count)
            //    {
            //        merItem = w.mer.Items[i];

            //        if (filterOilObj.SelectedObjects.IndexOf(dictPlast.GetIndexByCode(merItem.PlastId)) == -1)
            //        {
            //            i++;
            //            continue;
            //        }
            //        if (this.IntervalType == 0)
            //        {
            //            predDT = new DateTime(merItem.Date.Year, 1, 1).ToOADate();
            //        }
            //        else
            //        {
            //            predDT = merItem.Date.ToOADate();
            //        }
            //        predYear = merItem.Date.Year;

            //        sumWat = 0;
            //        sumOil = 0;
            //        sumInj = 0;
            //        sumNatGas = 0;
            //        sumGasCond = 0;
            //        sumInjGas = 0;
            //        sumLiq = 0;
            //        Wat = 0;
            //        ProdWorkTime.Clear();
            //        InjWorkTime.Clear();
            //        bProd = false;
            //        bInj = false;
            //        bNatGas = false;
            //        bGasCond = false;
            //        MerPlastData.ClearValues();

            //        // Сумма по месяцу или году
            //        while ((((i < w.mer.Count) &&
            //               (((this.IntervalType == 1) && (predDT == w.mer.Items[i].Date.ToOADate())) ||
            //                ((this.IntervalType == 2) && (predDT == w.mer.Items[i].Date.ToOADate())) ||
            //                ((this.IntervalType == 0) && (predYear == w.mer.Items[i].Date.Year))))))
            //        {
            //            merItem = w.mer.Items[i];
            //            if (filterOilObj.SelectedObjects.IndexOf(dictPlast.GetIndexByCode(merItem.PlastId)) == -1)
            //            {
            //                i++;
            //                continue;
            //            }
            //            if (merItem.CharWorkId == 11)
            //            {
            //                sumOil += merItem.Oil;
            //                sumWat += merItem.Wat;
            //                MerPlastData.AddProductionData(merItem.PlastId, merItem.Oil + merItem.Wat, merItem.Oil);
            //                bProd = true;
            //                ProdWorkTime.Add(merItem);
            //            }
            //            if (merItem.CharWorkId == 12)
            //            {
            //                sumNatGas += w.mer.GetItemEx(i).NaturalGas;
            //                MerPlastData.AddProductionNatGas(merItem.PlastId, w.mer.GetItemEx(i).NaturalGas);
            //                bNatGas = true;
            //                ProdWorkTime.Add(merItem);
            //            }
            //            if (merItem.CharWorkId == 15)
            //            {
            //                sumGasCond += w.mer.GetItemEx(i).GasCondensate;
            //                MerPlastData.AddProductionGasCond(merItem.PlastId, w.mer.GetItemEx(i).GasCondensate);
            //                bGasCond = true;
            //                ProdWorkTime.Add(merItem);
            //            }
            //            else if (merItem.CharWorkId == 20)
            //            {
            //                int agent = w.mer.GetItemEx(i).AgentInjId;
            //                if ((agent > 80) && (agent < 90))
            //                {
            //                    sumInjGas += w.mer.GetItemEx(i).Injection;
            //                    MerPlastData.AddInjectionGasData(merItem.PlastId, w.mer.GetItemEx(i).Injection);
            //                }
            //                else
            //                {
            //                    sumInj += w.mer.GetItemEx(i).Injection;
            //                    MerPlastData.AddInjectionData(merItem.PlastId, w.mer.GetItemEx(i).Injection);
            //                }
            //                InjWorkTime.Add(merItem);
            //                bInj = true;
            //            }

            //            if (plast_ids.IndexOf(merItem.PlastId) == -1)
            //            {
            //                plast_ids.Add(merItem.PlastId);
            //                sumPlast += merItem.PlastId;
            //                if (dictPlast != null)
            //                {
            //                    item = dictPlast.GetItemByCode(merItem.PlastId);
            //                    if (plast_ids.Count > 1) plasts += ",";
            //                    plasts += item.ShortName;
            //                }
            //            }
            //            i++;
            //        }

            //        sumLiq = sumOil + sumWat;

            //        #region ACCUM
            //        for (j = 0; j < MerPlastData.Items.Count; j++)
            //        {
            //            acc = AccList.GetItemByOilObjCode(MerPlastData.Items[j].PlastCode);
            //            if ((acc.AccumLiq == -1) && (MerPlastData.Items[j].Liq > 0))
            //            {
            //                acc.AccumLiq = 0;
            //                acc.AccumOil = 0;
            //            }
            //            if ((acc.AccumInj == -1) && (MerPlastData.Items[j].Injection > 0))
            //            {
            //                acc.AccumInj = 0;
            //            }
            //            if ((acc.AccumInjGas == -1) && (MerPlastData.Items[j].InjectionGas > 0))
            //            {
            //                acc.AccumInjGas = 0;
            //            }
            //            if (MerPlastData.Items[j].Liq > 0)
            //            {
            //                acc.AccumLiq += MerPlastData.Items[j].Liq;
            //                acc.AccumOil += MerPlastData.Items[j].Oil;
            //            }
            //            if (MerPlastData.Items[j].Injection > 0)
            //            {
            //                acc.AccumInj += MerPlastData.Items[j].Injection;
            //            }
            //            if (MerPlastData.Items[j].InjectionGas > 0)
            //            {
            //                acc.AccumInjGas += MerPlastData.Items[j].InjectionGas;
            //            }
            //        }
            //        #endregion

            //        Wat = 0;
            //        if (sumLiq > 0) Wat = sumWat * 100f / sumLiq;

            //        if ((bChess) && (predDT < MinChessDt)) continue;

            //        if ((labels != null) && ((predSumPlast == -1) || (sumPlast != predSumPlast)))
            //        {
            //            if (plast_ids.Count > 0) labels.AddLabel(predDT, plasts);
            //            predSumPlast = sumPlast;
            //            sumPlast = 0;
            //            plast_ids.Clear();
            //            plasts = "";
            //        }
            //        QLiq = 0; QOil = 0; QNatGas = 0; QGasCond = 0;
            //        wtItem = ProdWorkTime.GetTime();
            //        int workTime = wtItem.WorkTime + wtItem.CollTime;
            //        if ((this.IntervalType == 1) && (workTime > 744))
            //        {
            //            workTime = 744;
            //        }
            //        else if ((this.IntervalType == 0) && (workTime > 744 * 365))
            //        {
            //            workTime = 744 * 365;
            //        }
            //        if (workTime > 0)
            //        {
            //            QLiq = (sumLiq * 24) / workTime;
            //            QOil = (sumOil * 24) / workTime;
            //            QNatGas = (sumNatGas * 24) / workTime;
            //            QGasCond = (sumGasCond * 24) / workTime;
            //        }
            //        WInj = 0; WInjGas = 0;
            //        wtItem = InjWorkTime.GetTime();
            //        workTime = wtItem.WorkTime + wtItem.CollTime;
            //        if ((this.IntervalType == 1) && (workTime > 744))
            //        {
            //            workTime = 744;
            //        }
            //        else if ((this.IntervalType == 0) && (workTime > 744 * 365))
            //        {
            //            workTime = 744 * 365;
            //        }
            //        if (workTime > 0)
            //        {
            //            WInj = (sumInj * 24) / workTime;
            //            WInjGas = (sumInjGas * 24) / workTime;
            //        }
                    
            //        if (this.IntervalType == 2)
            //        {
            //            if (i < w.mer.Count)
            //            {
            //                days = (w.mer.Items[i].Date - DateTime.FromOADate(predDT)).Days;
            //            }
            //            else
            //            {
            //                days = (DateTime.FromOADate(predDT).AddMonths(1) - DateTime.FromOADate(predDT)).Days + 1;
            //            }
            //            dt = DateTime.FromOADate(predDT);

            //            for (j = 0; j < days; j++)
            //            {
            //                if (bProd)
            //                {
            //                    AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_T, predDT, QLiq);
            //                    AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T, predDT, QOil);
            //                    AddPoint(Constant.Chart.SERIES_TYPE.WATERING, predDT, Wat);
            //                }
            //                if (bNatGas) AddPoint(Constant.Chart.SERIES_TYPE.Q_NATGAS, predDT, QNatGas);
            //                if (bGasCond) AddPoint(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, predDT, QGasCond);
            //                if (bInj)
            //                {
            //                    AddPoint(Constant.Chart.SERIES_TYPE.W_INJ, predDT, WInj);
            //                    AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_GAS, predDT, WInjGas / 1000);
            //                }
            //                dt = dt.AddDays(1);
            //                predDT = dt.ToOADate();
            //            }
            //        }
            //        else if (ByPlastMode)
            //        {
            //            for (j = 0; j < MerPlastData.Items.Count; j++)
            //            {
            //                if (MerPlastData.Items[j].PlastCode != -1)
            //                {
            //                    switch (scheme.PlastModeSeries.SeriesType)
            //                    {
            //                        case Constant.Chart.SERIES_TYPE.LIQUID_T:
            //                            AddPoint(MerPlastData.Items[j].PlastCode, predDT, MerPlastData.Items[j].Liq);
            //                            break;
            //                        case Constant.Chart.SERIES_TYPE.OIL_T:
            //                            AddPoint(MerPlastData.Items[j].PlastCode, predDT, MerPlastData.Items[j].Oil);
            //                            break;
            //                        case Constant.Chart.SERIES_TYPE.INJECTION:
            //                            if (MerPlastData.Items[j].Injection > -1)
            //                            {
            //                                AddPoint(MerPlastData.Items[j].PlastCode, predDT, MerPlastData.Items[j].Injection);
            //                            }
            //                            break;
            //                        case Constant.Chart.SERIES_TYPE.INJ_GAS:
            //                            if (MerPlastData.Items[j].InjectionGas > -1)
            //                            {
            //                                AddPoint(MerPlastData.Items[j].PlastCode, predDT, MerPlastData.Items[j].InjectionGas);
            //                            }
            //                            break;
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (bProd)
            //            {
            //                AddPoint(Constant.Chart.SERIES_TYPE.LIQUID_T, predDT, sumLiq);
            //                AddPoint(Constant.Chart.SERIES_TYPE.OIL_T, predDT, sumOil);
            //                AddPoint(Constant.Chart.SERIES_TYPE.WATERING, predDT, Wat);
            //                AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_T, predDT, QLiq);
            //                AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T, predDT, QOil);
            //            }
            //            if (bNatGas)
            //            {
            //                AddPoint(Constant.Chart.SERIES_TYPE.NATGAS, predDT, sumNatGas);
            //                AddPoint(Constant.Chart.SERIES_TYPE.Q_NATGAS, predDT, QNatGas);
            //            }
            //            if (bGasCond)
            //            {
            //                AddPoint(Constant.Chart.SERIES_TYPE.GASCONDENSAT, predDT, sumGasCond);
            //                AddPoint(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, predDT, QGasCond);
            //            }
            //            if (bInj)
            //            {
            //                AddPoint(Constant.Chart.SERIES_TYPE.INJECTION, predDT, sumInj);
            //                AddPoint(Constant.Chart.SERIES_TYPE.INJ_GAS, predDT, sumInjGas / 1000);
            //                AddPoint(Constant.Chart.SERIES_TYPE.W_INJ, predDT, WInj);
            //                AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_GAS, predDT, WInjGas / 1000);
            //            }
            //        }
            //    }
            //}
            #endregion

            #endregion

            #region WELL RESEARCH
            if (bWellResearch && w.ResearchLoaded && !ByPlastMode)
            {
                string comment;
                for (i = 0; i < w.research.Count; i++)
                {
                    if ((w.research[i].PressPerforation > 0) && (w.research[i].Date.ToOADate() > MinChessDt))
                    {
                        if ((filterOilObj.AllCurrentSelected) || (filterOilObj.SelectedCurrentObjects.IndexOf(dictPlast.GetIndexByCode(w.research[i].PlastCode)) != -1))
                        {
                            if (w.research[i].ResearchCode == 2) // по просьбе Литвиненко && w.research[i].TimeDelay > 0)
                            {
                                comment = w.research[i].TimeDelay.ToString() + " ч";
                                ind = dictPlast.GetIndexByCode(w.research[i].PlastCode);
                                if (ind != -1) comment += " - " + dictPlast[ind].ShortName;
                                AddPoint(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_HSTAT, w.research[i].Date.ToOADate(), w.research[i].PressPerforation, comment);
                            }
                            else if (w.research[i].ResearchCode == 3)
                            {
                                comment = null;
                                ind = dictPlast.GetIndexByCode(w.research[i].PlastCode);
                                if (ind != -1) comment += " - " + dictPlast[ind].ShortName;
                                AddPoint(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB, w.research[i].Date.ToOADate(), w.research[i].PressPerforation, comment);
                            }
                            else if (w.research[i].ResearchCode == 4)
                            {
                                comment = null;
                                ind = dictPlast.GetIndexByCode(w.research[i].PlastCode);
                                if (ind != -1) comment = dictPlast[ind].ShortName;
                                AddPoint(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE, w.research[i].Date.ToOADate(), w.research[i].PressPerforation, comment);
                            }
                            else if (w.research[i].ResearchCode == 6)
                            {
                                comment = null;
                                ind = dictPlast.GetIndexByCode(w.research[i].PlastCode);
                                if (ind != -1) comment = dictPlast[ind].ShortName;
                                AddPoint(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_BUFFPL, w.research[i].Date.ToOADate(), w.research[i].PressPerforation, comment);
                            }
                            else if (w.research[i].ResearchCode == 1)
                            {
                                comment = null;
                                ind = dictPlast.GetIndexByCode(w.research[i].PlastCode);
                                if (ind != -1) comment = dictPlast[ind].ShortName;
                                AddPoint(Constant.Chart.SERIES_TYPE.PRESSURE_ZAB_HDYN, w.research[i].Date.ToOADate(), w.research[i].PressPerforation, comment);
                            }
                        }
                    }
                }
            }
            #endregion

            #region Well Action, GTM, WellState, Suspend
            if (w.ActionLoaded && !ByPlastMode)
            {
                bool find = false;
                for (i = 0; i < w.action.Count; i++)
                {
                    ind = dictWellAction.GetIndexByCode(w.action[i].WellActionCode);
                    if (ind > 0)
                    {
                        find = false;
                        if(w.GTMLoaded)
                        {
                            for (j = 0; j < w.gtm.Count; j++)
                            {
                                if (Math.Abs((w.gtm[j].Date - w.action[i].Date).TotalDays) < 4)
                                {
                                    find = true;
                                    break;
                                }
                            }
                        }
                        if (!find && w.action[i].Date.ToOADate() >= MinChessDt)
                        {
                            chartLabels.AddLabel(this.ShowWellAction, 1, w.action[i].Date.ToOADate(), dictWellAction[ind].ShortName, dictWellAction[ind].FullName, Color.Peru);
                        }
                    }
                }
            }
            if (w.GTMLoaded && !ByPlastMode)
            {
                for (i = 0; i < w.gtm.Count; i++)
                {
                    ind = dictGTM.GetIndexByCode(w.gtm[i].GtmCode);
                    if (ind > 0 && w.gtm[i].Date.ToOADate() >= MinChessDt)
                    {
                        chartLabels.AddLabel(this.ShowGTM, 0, w.gtm[i].Date.ToOADate(), dictGTM[ind].ShortName, dictGTM[ind].FullName, Color.SkyBlue);
                    }
                }
            }
            if (w.LeakLoaded && !ByPlastMode)
            {
                string str;
                string comm;
                bool skip = false;
                for (i = 0; i < w.leak.Count; i++)
                {
                    str = string.Empty;
                    comm = string.Empty;
                    skip = false;
                    if (w.leak[i].LeakType == 0)
                    {
                        str = "ЗКЦ";
                        comm = "ЗКЦ:";
                        if ((i + 1 < w.leak.Count) && (w.leak[i + 1].LeakType == 1) && (w.leak[i + 1].Date == w.leak[i].Date))
                        {
                            str += "+Негермет";
                            comm += string.Format(" [{0}]", WellLeak.GetOperationName(w.leak[i].OperationCode));
                            comm += "\nНегерметичность:";

                            if (w.leak[i + 1].LeakHoleType > 0) comm += string.Format(" {0}", WellLeak.GetLeakHoleName(w.leak[i + 1].LeakHoleType));
                            comm += string.Format(" [{0}]", WellLeak.GetOperationName(w.leak[i + 1].OperationCode));
                            skip = true;
                        }
                        else
                        {
                            comm += string.Format("\n{0}", WellLeak.GetOperationName(w.leak[i].OperationCode));
                        }
                    }
                    else if (w.leak[i].LeakType == 1)
                    {
                        str = "Негерметичность:";
                        comm = str;
                        if (w.leak[i].LeakHoleType > 0) comm += string.Format(" {0}", WellLeak.GetLeakHoleName(w.leak[i].LeakHoleType));
                        comm += string.Format("\n{0}", WellLeak.GetOperationName(w.leak[i].OperationCode));
                    }
                    else
                    {
                        str = "Исследование";
                        comm = str;
                    }
                    if (comm.Length > 0) comm += "\n";
                    comm += w.leak[i].Comment;
                    axisLabels.AddLabel(this.ShowWellLeak, 0, MarkerStyle.Triangle, w.leak[i].Date.ToOADate(), str, comm, Color.Lime);
                    if (skip) i++;
                }
            }
            if (w.SuspendLoaded && !ByPlastMode)
            {
                string comment;
                for (i = 0; i < w.suspend.Count; i++)
                {
                    comment = string.Format("КВЧ: {0}, мг/л", w.suspend[i].SuspendSolid);
                    axisLabels.AddLabel(this.ShowWellSuspend, 1, MarkerStyle.Square, w.suspend[i].Date.ToOADate(), string.Empty, comment, Color.Gray);
                }
            }
            #endregion

            if (ShowPIseries) CreateWellPISeries();

            area.ReCalcStackedSeriesValues();
            AccList.SumAllAccum();
            chart.ChartAreas[0].ReCalcChartAreaSizes();
            SetAxesXInterval();
        }
        public void CreateWellPISeries()
        {
            if (SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
            {
                ChartArea area = chart.ChartAreas[0];
                if (area.Series.Count == 0) return;
                Scheme scheme = SchemeList[ActiveScheme];
                int ind = scheme.GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.PI_M);
                if (ind == -1 || area.Series[ind].Points.Count == 0) ind = scheme.GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.PI_T);
                if (ind > -1 && area.Series[ind].Points.Count > 0) return;

                int liqInd = scheme.GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_LIQUID_T);
                if (liqInd != -1 && area.Series[liqInd].Points.Count == 0) liqInd = scheme.GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_LIQUID_M);
                if (liqInd == -1) return;

                double maxLiq = 0;
                
                var w = (Well)SourceObj;
                
                int i, j;

                for (i = 0; i < area.Series[liqInd].Points.Count; i++)
                {
                    if (maxLiq < area.Series[liqInd].Points[i].YValue)
                    {
                        maxLiq = area.Series[liqInd].Points[i].YValue;
                    }
                }

                double satPress;
                Dictionary<int, double> SatPressure = new Dictionary<int,double>();
                for (i = 0; i < project.OilFields[w.OilFieldIndex].Areas.Count; i++)
                {
                    if (project.OilFields[w.OilFieldIndex].Areas[i].WellList != null)
                    {
                        for (j = 0; j < project.OilFields[w.OilFieldIndex].Areas[i].WellList.Length; j++)
                        {
                            if (project.OilFields[w.OilFieldIndex].Areas[i].WellList[j] == w)
                            {
                                if (SatPressure.ContainsKey(project.OilFields[w.OilFieldIndex].Areas[i].PlastCode))
                                {
                                    SatPressure.TryGetValue(project.OilFields[w.OilFieldIndex].Areas[i].PlastCode, out satPress);
                                    if (satPress < project.OilFields[w.OilFieldIndex].Areas[i].pvt.SaturationPressure)
                                    {
                                        SatPressure.Remove(project.OilFields[w.OilFieldIndex].Areas[i].PlastCode);
                                        SatPressure.Add(project.OilFields[w.OilFieldIndex].Areas[i].PlastCode, project.OilFields[w.OilFieldIndex].Areas[i].pvt.SaturationPressure);
                                    }
                                }
                                else
                                {
                                    SatPressure.Add(project.OilFields[w.OilFieldIndex].Areas[i].PlastCode, project.OilFields[w.OilFieldIndex].Areas[i].pvt.SaturationPressure);
                                }
                            }
                        }
                    }
                }


                if (!w.ResearchLoaded)
                {
                    (project.OilFields[w.OilFieldIndex]).LoadWellResearchFromCache(w.Index);
                }
                if (!w.MerLoaded)
                {
                    (project.OilFields[w.OilFieldIndex]).LoadMerFromCache(w.Index);
                }

                if (w.ResearchLoaded && w.MerLoaded && SatPressure.Count > 0 && liqInd > -1 && area.Series[liqInd].Points.Count > 0)
                {

                    bool ShowInCube = (mainForm.AppSettings != null) ? mainForm.AppSettings.ShowDataLiquidInCube : false;

                    var dictPlast = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);

                    double MinDate = area.Series[liqInd].Points[0].XValue;

                    double dtVal;
                    double sumLiq, sumLiqV, sumOil, sumOilV;
                    double QLiq, Wat;
                    double satPressMax, PI, QliqPot, QoilPot;
                    double averagePlastPress, averageZabPress;
                    int hours, predYear, sumPlast = 0, predSumPlast = -1, days, counter;
                    bool bProd;
                    string plasts = string.Empty;
                    MerComp merComp = new MerComp(w.mer);
                    List<int> plast_ids = new List<int>();
                    MerWorkTimeItem wtItemProd, wtItemInj, wtItemScoop;
                    double wtProd;
                    MerCompPlastItem plastItem;
                    MerPlastDataCollection MerPlastData = new MerPlastDataCollection();
                    DateTime dt, merDate;
                    i = 0;
                    sumPlast = 0;
                    
                    string str = string.Empty;
                    foreach(int key in SatPressure.Keys)
                    {
                        if (str.Length > 0) str += ",";
                        str += string.Format("{0}({1})", SatPressure[key], dictPlast.GetShortNameByCode(key));
                    }
                    if (str.Length > 0) AccList.Comment = string.Format("Pнас : {0} атм", str);

                    // заполняем давления пластовые
                    int[] pInd = null, zInd = null;
                    List<int> presPlastCodes = new List<int>();
                    List<List<WellResearchItem>> plastPressure = new List<List<WellResearchItem>>();
                    List<List<WellResearchItem>> zabPressure = new List<List<WellResearchItem>>();
                    bool added = false;
                    for (i = 0; i < w.research.Count; i++)
                    {
                        if (w.research[i].PressPerforation > 0 && (w.research[i].ResearchCode != 2 || w.research[i].TimeDelay > 0))
                        {
                            added = false;
                            ind = presPlastCodes.IndexOf(w.research[i].PlastCode);
                            if (ind == -1)
                            {
                                ind = presPlastCodes.Count;
                                presPlastCodes.Add(w.research[i].PlastCode);
                                plastPressure.Add(new List<WellResearchItem>());
                                zabPressure.Add(new List<WellResearchItem>());
                                added = true;
                            }
                            if (w.research[i].ResearchCode == 2 || w.research[i].ResearchCode == 4 || w.research[i].ResearchCode == 6)
                            {
                                plastPressure[ind].Add(w.research[i]);
                            }
                            else if (w.research[i].ResearchCode == 1 || w.research[i].ResearchCode == 3 || w.research[i].ResearchCode == 5)
                            {
                                zabPressure[ind].Add(w.research[i]);
                            }
                            else if (added)
                            {
                                presPlastCodes.RemoveAt(ind);
                            }
                        }
                    }
                    if (presPlastCodes.Count == 0)
                    {
                        ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Для построения графика Кпрод не хватает данных замеров давлений");
                        return;
                    }

                    pInd = new int[presPlastCodes.Count];
                    zInd = new int[presPlastCodes.Count];
                    for (i = 0; i < presPlastCodes.Count; i++)
                    {
                        pInd[i] = -1;
                        zInd[i] = -1;
                    }
                    
                    i = 0;
                    satPressMax = -1;
                    int[][] errorCounters = new int[5][];
                    for (i = 0; i < errorCounters.Length; i++)
                    {
                        errorCounters[i] = new int[2];
                    }

                    while (i < merComp.Count)
                    {
                        if (this.IntervalType == 0)
                        {
                            dtVal = new DateTime(merComp[i].Date.Year, 1, 1).ToOADate();
                        }
                        else
                        {
                            dtVal = merComp[i].Date.ToOADate();
                        }
                        predYear = merComp[i].Date.Year;
                        merDate = merComp[i].Date;

                        sumLiq = 0;
                        sumLiqV = 0;
                        sumOil = 0;
                        sumOilV = 0;
                        wtProd = 0;
                        bProd = false;
                        MerPlastData.ClearValues();
                        wtItemProd.WorkTime = 0;
                        wtItemInj.WorkTime = 0;
                        wtItemScoop.WorkTime = 0;
                        plast_ids.Clear();
                        sumPlast = 0;
                        plasts = string.Empty;

                        // Сумма по месяцу или году
                        while ((((i < merComp.Count) &&
                               (((this.IntervalType == 1) && (dtVal == merComp[i].Date.ToOADate())) ||
                                ((this.IntervalType == 2) && (dtVal == merComp[i].Date.ToOADate())) ||
                                ((this.IntervalType == 0) && (predYear == merComp[i].Date.Year))))))
                        {
                            for (j = 0; j < merComp[i].PlastItems.Count; j++)
                            {
                                plastItem = merComp[i].PlastItems[j];
                                if (filterOilObj.SelectedObjects.IndexOf(dictPlast.GetIndexByCode(plastItem.PlastCode)) == -1)
                                {
                                    continue;
                                }
                                wtItemProd = merComp[i].TimeItems.GetProductionTime(plastItem.PlastCode);
                                wtItemProd.WorkTime = wtItemProd.GetWorkHours();


                                sumLiq += plastItem.Liq;
                                sumLiqV += plastItem.LiqV;
                                sumOil += plastItem.Oil;
                                sumOilV += plastItem.OilV;

                                for (int k = 0; k < merComp[i].CharWorkIds.Count; k++)
                                {
                                    switch (merComp[i].CharWorkIds[k])
                                    {
                                        case 11:
                                            MerPlastData.AddProductionData(plastItem.PlastCode, (!ShowInCube) ? plastItem.Liq : plastItem.LiqV, plastItem.Oil);
                                            bProd = true;
                                            break;
                                    }
                                }
                                if (plast_ids.IndexOf(plastItem.PlastCode) == -1)
                                {
                                    plast_ids.Add(plastItem.PlastCode);
                                    sumPlast += plastItem.PlastCode;
                                }
                            }
                            wtItemProd = merComp[i].TimeItems.GetAllProductionTime();
                            hours = (merComp[i].Date.AddMonths(1) - merComp[i].Date).Days * 24;
                            wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                            wtProd += wtItemProd.WorkTime;
                            i++;
                        }

                        if (dtVal < MinDate) continue;

                        averagePlastPress = 0;
                        averageZabPress = 0;
                        counter = 0;
                        for (j = 0; j < presPlastCodes.Count; j++)
                        {
                            while (pInd[j] + 1 < plastPressure[j].Count && plastPressure[j][pInd[j] + 1].Date < merDate) pInd[j]++;
                            while (zInd[j] + 1 < zabPressure[j].Count && zabPressure[j][zInd[j] + 1].Date < merDate) zInd[j]++;

                            if (plast_ids.IndexOf(presPlastCodes[j]) > -1)
                            {
                                counter++;
                                if (averagePlastPress > -1 && pInd[j] > -1) averagePlastPress += plastPressure[j][pInd[j]].PressPerforation; else averagePlastPress = -1;
                                if (averageZabPress > -1 && zInd[j] > -1) averageZabPress += zabPressure[j][zInd[j]].PressPerforation; else averageZabPress = -1;
                            }
                        }
                        if (averagePlastPress == 0 && averageZabPress == 0) continue;

                        if (averagePlastPress > -1) averagePlastPress /= counter;
                        if (averageZabPress > -1) averageZabPress /= counter;

                        errorCounters[0][0]++;
                        errorCounters[1][0]++;

                        if (averagePlastPress == -1 || averageZabPress == -1)
                        {
                            if (averagePlastPress == -1) errorCounters[0][1]++;
                            if (averageZabPress == -1) errorCounters[1][1]++;
                            continue;
                        }

                        Wat = 0;
                        if (sumLiq > 0)
                        {
                            Wat = (sumLiq - sumOil) / sumLiq;
                        }

                        if ((predSumPlast == -1) || (sumPlast != predSumPlast))
                        {
                            predSumPlast = sumPlast;
                            for (j = 0; j < plast_ids.Count; j++)
                            {
                                SatPressure.TryGetValue(plast_ids[j], out satPress);
                                if (satPress > 0)
                                {
                                    if (satPress > satPressMax) satPressMax = satPress;
                                }
                                else
                                {
                                    StratumTreeNode node = dictPlast.GetStratumTreeNode(plast_ids[j]);
                                    if(node != null)
                                    {
                                        foreach (int code in SatPressure.Keys)
                                        {
                                            if (node.GetParentLevelByCode(code) > -1)
                                            {
                                                SatPressure.TryGetValue(code, out satPress);
                                                if (satPress > 0)
                                                {
                                                    if (satPress > satPressMax) satPressMax = satPress;
                                                }
                                                else
                                                {
                                                    satPressMax = -1;
                                                    predSumPlast = -1;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    
                                }
                            }
                        }
                        errorCounters[2][0]++;
                        errorCounters[3][0]++;

                        if (satPressMax > 0 && averagePlastPress > satPressMax)
                        {
                            QLiq = 0;

                            if (wtProd > 0)
                            {
                                QLiq = (((ShowInCube) ? sumLiqV : sumLiq) * 24) / wtProd;
                            }

                            if (averageZabPress > satPressMax)
                            {
                                PI = QLiq / (averagePlastPress - averageZabPress);
                            }
                            else
                            {
                                double x = averageZabPress / satPressMax;
                                PI = QLiq / (averagePlastPress - satPressMax + satPressMax * (1 - 0.2 * x - 0.8 * x * x) / 1.8);
                            }
                            if (PI <= 0) continue;

                            QliqPot = PI * (averagePlastPress - 0.5 * satPressMax);
                            QoilPot = QliqPot * (1 - Wat);
                            if (QliqPot > maxLiq * 30) continue;

                            if (this.IntervalType == 2)
                            {
                                if (i < merComp.Count)
                                {
                                    days = (merComp[i].Date - DateTime.FromOADate(dtVal)).Days;
                                }
                                else
                                {
                                    days = (DateTime.FromOADate(dtVal).AddMonths(1) - DateTime.FromOADate(dtVal)).Days + 1;
                                }
                                dt = DateTime.FromOADate(dtVal);

                                for (j = 0; j < days; j++)
                                {
                                    if (bProd)
                                    {
                                        AddPoint((!ShowInCube) ? Constant.Chart.SERIES_TYPE.Q_LIQUID_T_POTENTIAL : Constant.Chart.SERIES_TYPE.Q_LIQUID_M_POTENTIAL, dtVal, QliqPot);
                                        AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T_POTENTIAL, dtVal, QoilPot);
                                        AddPoint((!ShowInCube) ? Constant.Chart.SERIES_TYPE.PI_T : Constant.Chart.SERIES_TYPE.PI_M, dtVal, PI);
                                    }
                                    dt = dt.AddDays(1);
                                    dtVal = dt.ToOADate();
                                }
                            }
                            else if (!ByPlastMode)
                            {
                                if (bProd)
                                {
                                    AddPoint((!ShowInCube) ? Constant.Chart.SERIES_TYPE.Q_LIQUID_T_POTENTIAL : Constant.Chart.SERIES_TYPE.Q_LIQUID_M_POTENTIAL, dtVal, QliqPot);
                                    AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T_POTENTIAL, dtVal, QoilPot);
                                    AddPoint((!ShowInCube) ? Constant.Chart.SERIES_TYPE.PI_T : Constant.Chart.SERIES_TYPE.PI_M, dtVal, PI);
                                }
                            }

                        }
                        else
                        {
                            if (satPressMax <= 0) errorCounters[2][1]++;
                            else if (averagePlastPress <= satPressMax) errorCounters[3][1]++;
                        }
                    }
                    if ((errorCounters[0][0] > 0 && errorCounters[0][0] == errorCounters[0][1]) || (errorCounters[1][0] > 0 && errorCounters[1][0] == errorCounters[1][1]))
                    {
                        ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Недостаточно данных по замерам давлений для постороения графика Кпрод");
                    }

                    if (errorCounters[2][0] > 0 && errorCounters[2][0] == errorCounters[2][1])
                    {
                        ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Недостаточно данных по давлению насыщения для постороения графика Кпрод");
                    }
                    if (errorCounters[3][0] > 0 && errorCounters[3][0] == errorCounters[3][1])
                    {
                        ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Средние пластовые давления ниже давления насыщения при постороении графика Кпрод");
                    }
                }
            }
        }
        #endregion

        #region LOAD SUM DATA
        public void AddSumParams(CanvasMap canv)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Пересчет суммарных показателей";
            var dictPlast = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);

            bool firstSum = true;

            AxisCustomLabels labels = null;
            ChartArea area = chart.ChartAreas[0];
            if (area.Series.Count == 0) return;

            if (area.AxisX.CustomLabels.Enabled) labels = area.AxisX.CustomLabels;
            else if (area.AxisX2.CustomLabels.Enabled) labels = area.AxisX2.CustomLabels;

            AccList.Clear();
            ClearPoints();
            if (labels != null) labels.Clear();

            bool bMer = SchemeList[ActiveScheme].IsUsedSource(Constant.Chart.DATA_SOURCE_TYPE.MER);
            bool bChess = SchemeList[ActiveScheme].IsUsedSource(Constant.Chart.DATA_SOURCE_TYPE.CHESS);

            OFIndexes.Clear();

            if ((canv != null) && (filterOilObj != null) && (dictPlast != null))
            {
                if ((canv.sumParams == null) || (canv.sumParams.Count == 0)) return;
                userState.Element = string.Empty;

                int i, objIndex, ind, plastCode;
                DateTime minDT = DateTime.MinValue;
                
                bool AllSelected = filterOilObj.AllCurrentOilFieldSelected;
                ArrayList SelectedList = filterOilObj.SelectedOilFieldObjects;

                int countObjects = filterOilObj.CurrentOilFieldObjects.Count;

                if ((SourceObj != null) && 
                    ((SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.OILFIELD) ||
                     (SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT)))
                {
                    AllSelected = filterOilObj.AllCurrentOilFieldSelected;
                    SelectedList = filterOilObj.SelectedOilFieldObjects;
                    countObjects = filterOilObj.CurrentOilFieldObjects.Count;
                }
                if (canv.IsGraphicsByOneDate) AddPoint(Constant.Chart.SERIES_TYPE.FORCED_DATE, 0, 100);

                if ((AllSelected || (countObjects == 0)) && (!ByPlastMode))
                {
                    if ((bChess) && (AllSelected || (countObjects == 0)) && (canv.sumChessParams != null) && (canv.sumChessParams.Count > 0))
                    {
                        AddSumChessPoints(canv.sumChessParams[0].MonthlyItems, firstSum, canv.sumChessParams[0].ShiftMonthIndex);
                        if (canv.sumChessParams[0].MonthlyItems != null)
                        {
                            if (!canv.IsGraphicsByOneDate)
                            {
                                minDT = canv.sumChessParams[0].MonthlyItems[0].Date;
                                minDT = new DateTime(minDT.Year, minDT.Month, 1);
                            }
                            else
                            {
                                minDT = DateTime.FromOADate(canv.sumChessParams[0].ShiftMonthIndex);
                            }
                        }
                    }
                    else if (bChess)
                    {
                        minDT = new DateTime(2008, 1, 1);
 
                    }

                    if (this.IntervalType != 0)
                    {
                        AddSumPoints(canv.sumParams[0].MonthlyItems, minDT, firstSum, canv.sumParams[0].ShiftMonthIndex);
                        AddAccumParams(-1, canv.sumParams[0].MonthlyItems);
                        if (this.IntervalType == 1)
                        {
                            if ((this.ShowBPParams) && (canv.sumBP != null) && (canv.sumBP.Count > 0))
                            {
                                AddBPParams(canv.sumBP[0].MonthlyItems, minDT);
                                //double dOilYear, dOilMonth, dYearPercent, dMonthPercent;
                                //DateTime LastDate = DateTime.MinValue;
                                //GetDeltaOilByBP(canv.sumParams[0].MonthlyItems, canv.sumBP[0].MonthlyItems, out LastDate, out dOilYear, out dOilMonth, out dYearPercent, out dMonthPercent);
                                if (canv.BPParams != null && canv.BPParams[0] > 0)
                                {
                                    AccList.SetBPParams(DateTime.FromOADate(canv.BPParams[0]), canv.BPParams[1], canv.BPParams[2], canv.BPParams[3], canv.BPParams[4]);
                                }
                            }
                        }
                    }
                    else
                    {
                        AddSumPoints(canv.sumParams[0].YearlyItems, minDT, firstSum, canv.sumParams[0].ShiftYearIndex);
                        AddAccumParams(-1, canv.sumParams[0].YearlyItems);
                        if ((this.ShowProjParams) && (canv.sumTable81 != null) && (canv.sumTable81.Count > 0))
                        {
                            AddTable81Params(canv.sumTable81[0].YearlyItems, minDT);
                            if(canv.sumTable81[0].YearlyItems.Length > 0)
                            {
                                double dt = canv.sumTable81[0].YearlyItems[0].Date.ToOADate();
                                if (this.ProjParamsXValue < dt) this.ProjParamsXValue = dt;
                            }
                        }
                    }
                }
                else
                {
                    for (i = 0; i < SelectedList.Count; i++)
                    {
                        if (ByPlastMode) firstSum = true;
                        ind = (int)SelectedList[i];
                        plastCode = dictPlast[ind].Code;
                        objIndex = canv.sumParams.GetIndexByObjCode(plastCode);
                        if (objIndex == -1) continue;

                        if (this.IntervalType != 0)
                        {
                            if (!ByPlastMode)
                            {
                                AddSumPoints(canv.sumParams[objIndex].MonthlyItems, minDT, firstSum, canv.sumParams[objIndex].ShiftMonthIndex);
                            }
                            else
                            {
                                AddSumPoints(plastCode, canv.sumParams[objIndex].MonthlyItems, minDT, firstSum, canv.sumParams[objIndex].ShiftMonthIndex);
                            }
                            AddAccumParams(plastCode, canv.sumParams[objIndex].MonthlyItems);
                        }
                        else
                        {
                            if (!ByPlastMode)
                            {
                                AddSumPoints(canv.sumParams[objIndex].YearlyItems, minDT, firstSum, canv.sumParams[objIndex].ShiftYearIndex);
                            }
                            else
                            {
                                AddSumPoints(plastCode, canv.sumParams[objIndex].YearlyItems, minDT, firstSum, canv.sumParams[objIndex].ShiftYearIndex);
                            }
                            AddAccumParams(plastCode, canv.sumParams[objIndex].YearlyItems);
                        }

                        if (firstSum) firstSum = false;
                        if (i < SelectedList.Count)
                        {
                            userState.Element = dictPlast[ind].ShortName;
                        }
                    }
                    area.ReCalcStackedSeriesValues();
                }
                AccList.SumAllAccum();
                chart.ChartAreas[0].ReCalcChartAreaSizes();
                if (canv.IsGraphicsByOneDate)
                {
                   chart.ChartAreas[0].SetAxisXMinimum(0);
                }
                else
                {
                    chart.ChartAreas[0].SetNonZeroMinimum();
                }
                
                SetAxesXInterval();
            }
        }

        #region SUM MER
        private void AddSumPoints(SumParamItem[] SumParams, DateTime minDT, bool FirstSum, int ShiftOneDate)
        {
            if (FirstSum)
            {
                ShowSumParams(SumParams, minDT, ShiftOneDate);
            }
            else
            {
                AddSumParams(SumParams, minDT, ShiftOneDate);
            }
        }
        private void ShowSumParams(SumParamItem[] SumParams, DateTime minDT, int ShiftOneDate)
        {
            if (SumParams == null) return;
            int i, j;
            SumParamItem item;
            if (SumParams.Length > 0)
            {
                bool ShowInCube = mainForm.AppSettings.ShowDataLiquidInCube;
                int sumLength = SumParams.Length;
                bool startCompensation = false;
                bool startPlastPressure = false, startZabPressure = false;
                double dt = 0;
                if (this.ActiveScheme == 3)
                {
                    int days, daysFond;
                    DateTime nextDt, date;
                    if (ShiftOneDate == -1)
                    {
                        for (i = 0; i < sumLength; i++)
                        {
                            item = SumParams[i];
                            if (item.Date < minDT) continue;
                            nextDt = item.Date.AddMonths(1);
                            days = (nextDt - item.Date).Days;
                            date = item.Date;
                            if (item.PressureCounter > 0 || startPlastPressure)
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, date.ToOADate(), item.Pressure, item.PressureCounter, SeriesOperation.Division);
                                startPlastPressure = true;
                            }
                            if (mainForm.AppSettings.CalcZabPressureAverage && (item.ZabPressureCounter > 0 || startZabPressure))
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, date.ToOADate(), item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                                startZabPressure = true;
                            }
                            for (j = 0; j < days; j++)
                            {
                                dt = date.ToOADate();
                                daysFond = days;
                                if(item.Fprod > 0) daysFond = days * item.Fprod;
                                if (!ShowInCube)
                                {
                                    AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_T , dt, item.Liq / daysFond);
                                    AddPoint(Constant.Chart.SERIES_TYPE.WATERING, dt, (item.Liq - item.Oil) * 100, item.Liq, SeriesOperation.Division);
                                }
                                else 
                                {
                                    AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, item.LiqV / daysFond);
                                    AddPoint(Constant.Chart.SERIES_TYPE.WATERING_M, dt, (item.LiqV - item.OilV) * 100, item.LiqV, SeriesOperation.Division);
                                }
                                
                                AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, item.Oil / daysFond);
                                if(item.NatGas > -1) AddPoint(Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, item.NatGas / daysFond);
                                if (item.GasCondensate > -1) AddPoint(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, item.GasCondensate / daysFond);

                                
                                daysFond = days;
                                if (item.Finj > 0) daysFond = days * item.Finj;
                                AddPoint(Constant.Chart.SERIES_TYPE.W_INJ, dt, item.Inj / daysFond);
                                if (item.InjGas > -1) AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, item.InjGas / daysFond);
                                AddPoint(Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                                AddPoint(Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                                date = date.AddDays(1);
                            }
                        }
                    }
                }
                else if (this.ActiveScheme == 2)
                {
                    int days;
                    DateTime nextDt, date;
                    if (ShiftOneDate == -1)
                    {
                        for (i = 0; i < sumLength; i++)
                        {
                            item = SumParams[i];
                            if (item.Date < minDT) continue;
                            nextDt = item.Date.AddMonths(1);
                            days = (nextDt - item.Date).Days;
                            date = item.Date;
                            if (item.PressureCounter > 0 || startPlastPressure)
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, date.ToOADate(), item.Pressure, item.PressureCounter, SeriesOperation.Division);
                                startPlastPressure = true;
                            }
                            if (mainForm.AppSettings.CalcZabPressureAverage && (item.ZabPressureCounter > 0 || startZabPressure))
                            {
                                AddPoint(Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, date.ToOADate(), item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                                startZabPressure = true;
                            }

                            for (j = 0; j < days; j++)
                            {
                                dt = date.ToOADate();
                                if (!ShowInCube)
                                {
                                    AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_T, dt, item.Liq / days);
                                    AddPoint(Constant.Chart.SERIES_TYPE.WATERING, dt, (item.Liq - item.Oil) * 100, item.Liq, SeriesOperation.Division);
                                }
                                else
                                {
                                    AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, item.LiqV / days);
                                    AddPoint(Constant.Chart.SERIES_TYPE.WATERING_M, dt, (item.LiqV - item.OilV) * 100, item.LiqV, SeriesOperation.Division);
                                }
                                
                                AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, item.Oil / days);

                                if (item.NatGas > -1) AddPoint(Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, item.NatGas / days);
                                if (item.GasCondensate > -1) AddPoint(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, item.GasCondensate / days);

                                AddPoint(Constant.Chart.SERIES_TYPE.W_INJ, dt, item.Inj / days);
                                if (item.InjGas > -1) AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, item.InjGas / days);
                                AddPoint(Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                                AddPoint(Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                                date = date.AddDays(1);
                            }
                        }
                    }
                }
                else
                {
                    startCompensation = false;
                    startPlastPressure = false;
                    startZabPressure = false;

                    for (i = 0; i < sumLength; i++)
                    {
                        item = SumParams[i];
                        if (item.Date < minDT) continue;

                        if (ShiftOneDate != -1)
                        {
                            dt = i - ShiftOneDate;
                        }
                        else
                        {
                            dt = item.Date.ToOADate();
                        }
                        if (!ShowInCube)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.LIQUID_T, dt, item.Liq);
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_T , dt, item.Liq * 24, item.WorkTimeProd, SeriesOperation.Division);
                            AddPoint(Constant.Chart.SERIES_TYPE.WATERING, dt, (item.Liq - item.Oil) * 100, item.Liq, SeriesOperation.Division);
                        }
                        else
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.LIQUID_M, dt, item.LiqV);
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, item.LiqV * 24, item.WorkTimeProd, SeriesOperation.Division);
                            AddPoint(Constant.Chart.SERIES_TYPE.WATERING_M, dt, (item.LiqV - item.OilV) * 100, item.LiqV, SeriesOperation.Division);
                        }

                        if (item.NatGas > -1)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.NATGAS, dt, item.NatGas);
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, item.NatGas * 24, item.WorkTimeProd, SeriesOperation.Division);
                        }
                        if (item.GasCondensate > -1)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.GASCONDENSAT, dt, item.GasCondensate);
                            AddPoint(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, item.GasCondensate * 24, item.WorkTimeProd, SeriesOperation.Division);
                        }

                        AddPoint(Constant.Chart.SERIES_TYPE.OIL_T, dt, item.Oil);
                        AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, item.Oil * 24, item.WorkTimeProd, SeriesOperation.Division);
                        AddPoint(Constant.Chart.SERIES_TYPE.INJECTION, dt, item.Inj);
                        AddPoint(Constant.Chart.SERIES_TYPE.W_INJ, dt, item.Inj * 24, item.WorkTimeInj, SeriesOperation.Division);
                        if (item.InjGas > -1)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.INJ_GAS, dt, item.InjGas / 1000);
                            AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, item.InjGas * 24, item.WorkTimeInjGas, SeriesOperation.Division);
                        }

                        AddPoint(Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                        AddPoint(Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                        if (item.PressureCounter > 0 || startPlastPressure)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, dt, item.Pressure, item.PressureCounter, SeriesOperation.Division);
                            startPlastPressure = true;
                        }
                        if (mainForm.AppSettings.CalcZabPressureAverage && (item.ZabPressureCounter > 0 || startZabPressure))
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, dt, item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                            startZabPressure = true;
                        }
                        if (!startCompensation) startCompensation = item.InjVb > 0 && item.LiqVb > 0;
                        if (startCompensation)
                        {
                            AddPoint(Constant.Chart.SERIES_TYPE.COMPENSATION, dt, item.InjVb * 100, item.LiqVb, SeriesOperation.Division);
                            AddPoint(Constant.Chart.SERIES_TYPE.COMPENSATION_ACCUM, dt, item.AccumInjVb * 100, item.AccumLiqVb, SeriesOperation.Division);
                        }
                    }
                }
            }
        }
        private void AddSumParams(SumParamItem[] SumParams, DateTime minDT, int ShiftOneDate)
        {
            if (SumParams == null) return;
            int i, j, ind, k, k2, kzab, kComp, ind2, ind3, kInjGas, kNatGas, kGasCondensate;
            SumParamItem item;
            ChartSeries ser, ser2, serComp, serZabAverage, serInjGas = null, serNatGas = null, serGasCondensate = null;
            bool startPlastPressure = false, startZabPressure = false;

            if (SumParams.Length > 0)
            {
                int sumLength = SumParams.Length;
                double dt = 0;
                bool startCompensation = false;
                bool ShowInCube = mainForm.AppSettings.ShowDataLiquidInCube;
                if ((this.ActiveScheme == 3) && (!mainForm.canv.IsGraphicsByOneDate))
                {
                    int days, daysFond;
                    DateTime nextDt, date;
                    double serMinDate = DateTime.MaxValue.ToOADate();
                    if (!ShowInCube)
                    {
                        ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.LIQUID_T);
                        if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_LIQUID_T);
                    }
                    else
                    {
                        ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.LIQUID_M);
                        if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_LIQUID_M);
                    }
                    if (ind > -1)
                    {
                        ser = chart.ChartAreas[0].Series[ind];
                        if (!ser.IsEmpty) serMinDate = ser.Points[0].XValue;
                        k = 0;
                        k2 = 0;
                        kzab = 0;
                        kComp = 0;
                        ind2 = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE);
                        ser2 = chart.ChartAreas[0].Series[ind2];
                        double serMinDate2 = DateTime.MaxValue.ToOADate();
                        if (!ser2.IsEmpty) serMinDate2 = ser2.Points[0].XValue;

                        for (i = 0; i < sumLength; i++)
                        {
                            item = SumParams[i];
                            if (item.Date < minDT) continue;

                            nextDt = item.Date.AddMonths(1);
                            days = (nextDt - item.Date).Days;
                            date = item.Date;
                            dt = date.ToOADate();
                            if (item.PressureCounter > 0 || startPlastPressure)
                            {
                                if (dt < serMinDate2)
                                {
                                    InsertPoint(k2, Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, dt, item.Pressure, item.PressureCounter, SeriesOperation.Division);
                                }
                                else
                                {
                                    SumPoint(k2, Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, dt, item.Pressure, item.PressureCounter, SeriesOperation.Division);
                                }
                                k2++;
                                startPlastPressure = true;
                            }
                            if (mainForm.AppSettings.CalcZabPressureAverage && (item.ZabPressureCounter > 0 || startZabPressure))
                            {
                                if (dt < serMinDate2)
                                {
                                    InsertPoint(kzab, Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, dt, item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                                }
                                else
                                {
                                    SumPoint(kzab, Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, dt, item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                                }
                                kzab++;
                                startZabPressure = true;
                            }
                            for (j = 0; j < days; j++)
                            {
                                dt = date.ToOADate();
                                while ((ser.Points[k].XValue < dt) && (k < ser.Points.Count)) k++;
                                if (dt < serMinDate)
                                {
                                    daysFond = days;
                                    if (item.Fprod > 0) daysFond = days * item.Fprod;
                                    if (!ShowInCube)
                                    {
                                        InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_T, dt, item.Liq / daysFond);
                                        InsertPoint(k, Constant.Chart.SERIES_TYPE.WATERING, dt, (item.Liq - item.Oil) * 100, item.Liq, SeriesOperation.Division);
                                    }
                                    else
                                    {
                                        InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, item.LiqV / daysFond);
                                        InsertPoint(k, Constant.Chart.SERIES_TYPE.WATERING_M, dt, (item.LiqV - item.OilV) * 100, item.LiqV, SeriesOperation.Division);
                                    }
                                    
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, item.Oil / daysFond);

                                    if (item.NatGas > -1) InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, item.NatGas / daysFond);
                                    if (item.GasCondensate > -1) InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, item.GasCondensate / daysFond);

                                    daysFond = days;
                                    if (item.Finj > 0) daysFond = days * item.Finj;
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.W_INJ, dt, item.Inj / daysFond);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, item.InjGas / daysFond);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                                }
                                else
                                {
                                    daysFond = days;
                                    if (item.Fprod > 0) daysFond = days * item.Fprod;
                                    if (!ShowInCube)
                                    {
                                        SumPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_T, dt, item.Liq / daysFond);
                                        SumPoint(k, Constant.Chart.SERIES_TYPE.WATERING, dt, (item.Liq - item.Oil) * 100, item.Liq, SeriesOperation.Division);
                                    }
                                    else
                                    {
                                        SumPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, item.LiqV / daysFond);
                                        SumPoint(k, Constant.Chart.SERIES_TYPE.WATERING_M, dt, (item.LiqV - item.OilV) * 100, item.LiqV, SeriesOperation.Division);
                                    }
                                    
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, item.Oil / daysFond);
                                    if (item.NatGas > -1) SumPoint(k, Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, item.NatGas / daysFond);
                                    if (item.GasCondensate > -1) SumPoint(k, Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, item.GasCondensate / daysFond);
                                    daysFond = days;
                                    if (item.Finj > 0) daysFond = days * item.Finj;
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.W_INJ, dt, item.Inj / daysFond);
                                    if (item.InjGas > 0) SumPoint(k, Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, item.InjGas / daysFond);
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                                }

                                date = date.AddDays(1);
                                k++;
                            }
                        }
                    }
                }
                else if ((this.ActiveScheme == 2) && (!mainForm.canv.IsGraphicsByOneDate))
                {
                    int days;
                    DateTime nextDt, date;
                    double serMinDate = DateTime.MaxValue.ToOADate();
                    ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.INJ_GAS);
                    if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.W_INJ_GAS);
                    if (ind > -1) serInjGas = chart.ChartAreas[0].Series[ind];

                    ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.NATGAS);
                    if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_NATGAS);
                    if (ind > -1) serNatGas = chart.ChartAreas[0].Series[ind];

                    ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.GASCONDENSAT);
                    if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT);
                    if (ind > -1) serGasCondensate = chart.ChartAreas[0].Series[ind];

                    if (!ShowInCube)
                    {
                        ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.LIQUID_T);
                        if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_LIQUID_T);
                    }
                    else
                    {
                        ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.LIQUID_M);
                        if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_LIQUID_M);
                    }
                    if (ind > -1)
                    {
                        ser = chart.ChartAreas[0].Series[ind];
                        if (!ser.IsEmpty) serMinDate = ser.Points[0].XValue;
                        k = 0;
                        k2 = 0;
                        kzab = 0;
                        kInjGas = 0;
                        kNatGas = 0;
                        kGasCondensate = 0;
                        ind2 = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE);
                        ser2 = chart.ChartAreas[0].Series[ind2];
                        double serMinDate2 = DateTime.MaxValue.ToOADate();
                        if (!ser2.IsEmpty) serMinDate2 = ser2.Points[0].XValue;
                        startPlastPressure = false;
                        startZabPressure = false;
                        for (i = 0; i < sumLength; i++)
                        {
                            item = SumParams[i];
                            if (item.Date < minDT) continue;

                            nextDt = item.Date.AddMonths(1);
                            days = (nextDt - item.Date).Days;
                            date = item.Date;
                            dt = date.ToOADate();
                            if (item.PressureCounter > 0 || startPlastPressure)
                            {
                                if (dt < serMinDate2)
                                {
                                    InsertPoint(k2, Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, dt, item.Pressure, item.PressureCounter, SeriesOperation.Division);
                                }
                                else
                                {
                                    SumPoint(k2, Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, dt, item.Pressure, item.PressureCounter, SeriesOperation.Division);
                                }
                                k2++;
                                startPlastPressure = true;
                            }
                            if (mainForm.AppSettings.CalcZabPressureAverage && (item.ZabPressureCounter > 0 || startZabPressure))
                            {
                                if (dt < serMinDate2)
                                {
                                    InsertPoint(kzab, Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, dt, item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                                }
                                else
                                {
                                    SumPoint(kzab, Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, dt, item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                                }
                                kzab++;
                                startZabPressure = true;
                            }
                            for (j = 0; j < days; j++)
                            {
                                dt = date.ToOADate();
                                while ((k < ser.Points.Count) && (ser.Points[k].XValue < dt)) k++;
                                if (serInjGas != null)
                                {
                                    while ((kInjGas < serInjGas.Points.Count) && (serInjGas.Points[kInjGas].XValue < dt)) kInjGas++;
                                }
                                if (serNatGas != null)
                                {
                                    while ((kNatGas < serNatGas.Points.Count) && (serNatGas.Points[kNatGas].XValue < dt)) kNatGas++;
                                }
                                if (serGasCondensate != null)
                                {
                                    while ((kGasCondensate < serGasCondensate.Points.Count) && (serGasCondensate.Points[kGasCondensate].XValue < dt)) kGasCondensate++;
                                }

                                if (dt < serMinDate)
                                {
                                    if (!ShowInCube)
                                    {
                                        InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_T, dt, item.Liq / days);
                                        InsertPoint(k, Constant.Chart.SERIES_TYPE.WATERING, dt, (item.Liq - item.Oil) * 100, item.Liq, SeriesOperation.Division);
                                    }
                                    else
                                    {
                                        InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, item.LiqV / days);
                                        InsertPoint(k, Constant.Chart.SERIES_TYPE.WATERING_M, dt, (item.LiqV - item.OilV) * 100, item.LiqV, SeriesOperation.Division);
                                    }
                                    
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, item.Oil / days);
                                    if(item.NatGas > -1) InsertPoint(kNatGas, Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, item.NatGas / days);
                                    if (item.GasCondensate > -1) InsertPoint(kGasCondensate, Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, item.GasCondensate / days);

                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.W_INJ, dt, item.Inj / days);
                                    if (item.InjGas > 0) InsertPoint(kInjGas, Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, item.InjGas / days);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                                }
                                else
                                {
                                    if (!ShowInCube)
                                    {
                                        SumPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_T, dt, item.Liq / days);
                                        SumPoint(k, Constant.Chart.SERIES_TYPE.WATERING, dt, (item.Liq - item.Oil) * 100, item.Liq, SeriesOperation.Division);
                                    }
                                    else
                                    {
                                        SumPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, item.LiqV / days);
                                        SumPoint(k, Constant.Chart.SERIES_TYPE.WATERING_M, dt, (item.LiqV - item.OilV) * 100, item.LiqV, SeriesOperation.Division);
                                    }
                                    
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, item.Oil / days);
                                    if(item.NatGas > -1) SumPoint(kNatGas, Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, item.NatGas / days);
                                    if(item.GasCondensate > -1) SumPoint(kGasCondensate, Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, item.GasCondensate / days);

                                    SumPoint(k, Constant.Chart.SERIES_TYPE.W_INJ, dt, item.Inj / days);
                                    if (item.InjGas > 0) SumPoint(kInjGas, Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, item.InjGas / days);
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                                }

                                date = date.AddDays(1);
                                k++;
                            }
                        }
                    }
                }
                else
                {
                    double serMinDate = DateTime.MaxValue.ToOADate();
                    
                    ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.INJ_GAS);
                    if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.W_INJ_GAS);
                    if (ind > -1) serInjGas = chart.ChartAreas[0].Series[ind];

                    ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.NATGAS);
                    if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_NATGAS);
                    if (ind > -1) serNatGas = chart.ChartAreas[0].Series[ind];

                    ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.GASCONDENSAT);
                    if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT);
                    if (ind > -1) serGasCondensate = chart.ChartAreas[0].Series[ind];

                    if (!ShowInCube)
                    {
                        ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.LIQUID_T);
                        if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_LIQUID_T);
                    }
                    else
                    {
                        ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.LIQUID_M);
                        if (ind == -1) ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.Q_LIQUID_M);
                    }
                    if (ind > -1)
                    {
                        ser = chart.ChartAreas[0].Series[ind];
                        if (!ser.IsEmpty) serMinDate = ser.Points[0].XValue;
                        k = 0;
                        k2 = 0;
                        kzab = 0;
                        kInjGas = 0;
                        kNatGas = 0;
                        kGasCondensate = 0;
                        kComp = 0;

                        ind2 = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE);
                        ser2 = chart.ChartAreas[0].Series[ind2];
                        ind2 = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE);
                        serZabAverage = chart.ChartAreas[0].Series[ind2];
                        double serMinDate2 = DateTime.MaxValue.ToOADate();
                        if (!ser2.IsEmpty) serMinDate2 = ser2.Points[0].XValue;

                        ind3 = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.COMPENSATION);
                        serComp = chart.ChartAreas[0].Series[ind3];
                        double serMinDate3 = DateTime.MaxValue.ToOADate();
                        if (!serComp.IsEmpty) serMinDate3 = serComp.Points[0].XValue;

                        startCompensation = false;
                        startPlastPressure = false;
                        startZabPressure = false;
                        for (i = 0; i < sumLength; i++)
                        {
                            item = SumParams[i];
                            if (item.Date < minDT) continue;

                            if (ShiftOneDate != -1)
                            {
                                dt = i - ShiftOneDate;
                            }
                            else
                            {
                                dt = item.Date.ToOADate();
                            }
                            while ((k < ser.Points.Count) && (ser.Points[k].XValue < dt)) k++;
                            while ((k2 < ser2.Points.Count) && (ser2.Points[k2].XValue < dt)) k2++;
                            while ((kzab < serZabAverage.Points.Count) && (serZabAverage.Points[kzab].XValue < dt)) kzab++;
                            if (serInjGas != null)
                            {
                                while ((kInjGas < serInjGas.Points.Count) && (serInjGas.Points[kInjGas].XValue < dt)) kInjGas++;
                            }
                            if (serNatGas != null)
                            {
                                while ((kNatGas < serNatGas.Points.Count) && (serNatGas.Points[kNatGas].XValue < dt)) kNatGas++;
                            }
                            if (serGasCondensate != null)
                            {
                                while ((kGasCondensate < serGasCondensate.Points.Count) && (serGasCondensate.Points[kGasCondensate].XValue < dt)) kGasCondensate++;
                            }
                            if (serComp != null)
                            {
                                while ((kComp < serComp.Points.Count) && (serComp.Points[kComp].XValue < dt)) kComp++;
                            }

                            if (item.PressureCounter > 0 || startPlastPressure)
                            {
                                if (dt < serMinDate2)
                                {
                                    InsertPoint(k2, Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, dt, item.Pressure, item.PressureCounter, SeriesOperation.Division);
                                }
                                else
                                {
                                    SumPoint(k2, Constant.Chart.SERIES_TYPE.PLAST_PRESSURE_AVERAGE, dt, item.Pressure, item.PressureCounter, SeriesOperation.Division);
                                }
                                startPlastPressure = true;
                            }
                            if (mainForm.AppSettings.CalcZabPressureAverage && (item.ZabPressureCounter > 0 || startZabPressure))
                            {
                                if (dt < serMinDate2)
                                {
                                    InsertPoint(kzab, Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, dt, item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                                }
                                else
                                {
                                    SumPoint(kzab, Constant.Chart.SERIES_TYPE.ZAB_PRESSURE_AVERAGE, dt, item.ZabPressure, item.ZabPressureCounter, SeriesOperation.Division);
                                }
                                startZabPressure = true;
                            }

                            if (dt < serMinDate)
                            {
                                if (!ShowInCube)
                                {
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.LIQUID_T, dt, item.Liq);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.WATERING, dt, ((item.Liq - item.Oil) * 100), item.Liq, SeriesOperation.Division);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_T, dt, (item.Liq * 24), item.WorkTimeProd, SeriesOperation.Division);
                                }
                                else
                                {
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.LIQUID_M, dt, item.LiqV);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.WATERING_M, dt, ((item.LiqV - item.OilV) * 100), item.LiqV, SeriesOperation.Division);
                                    InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, (item.LiqV * 24), item.WorkTimeProd, SeriesOperation.Division);
                                }
                                InsertPoint(k, Constant.Chart.SERIES_TYPE.OIL_T, dt, item.Oil);

                                if (item.NatGas > -1)
                                {
                                    InsertPoint(kNatGas, Constant.Chart.SERIES_TYPE.NATGAS, dt, item.NatGas);
                                    InsertPoint(kNatGas, Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, (item.NatGas * 24), item.WorkTimeProd, SeriesOperation.Division);
                                }

                                if (item.GasCondensate > -1)
                                {
                                    InsertPoint(kGasCondensate, Constant.Chart.SERIES_TYPE.GASCONDENSAT, dt, item.GasCondensate);
                                    InsertPoint(kGasCondensate, Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, (item.GasCondensate * 24), item.WorkTimeProd, SeriesOperation.Division);
                                }
                                InsertPoint(k, Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, (item.Oil * 24), item.WorkTimeProd, SeriesOperation.Division);

                                InsertPoint(k, Constant.Chart.SERIES_TYPE.INJECTION, dt, item.Inj);
                                InsertPoint(k, Constant.Chart.SERIES_TYPE.W_INJ, dt, (item.Inj * 24), item.WorkTimeInj, SeriesOperation.Division);

                                if (item.InjGas > 0)
                                {
                                    InsertPoint(kInjGas, Constant.Chart.SERIES_TYPE.INJ_GAS, dt, item.InjGas / 1000);
                                    InsertPoint(kInjGas, Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, (item.InjGas * 24), item.WorkTimeInjGas, SeriesOperation.Division);
                                }
                                
                                InsertPoint(k, Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                                InsertPoint(k, Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                                if (!startCompensation) startCompensation = item.InjVb > 0 && item.LiqVb > 0;
                                if (startCompensation)
                                {
                                    InsertPoint(kComp, Constant.Chart.SERIES_TYPE.COMPENSATION, dt, item.InjVb * 100, item.LiqVb, SeriesOperation.Division);
                                    InsertPoint(kComp, Constant.Chart.SERIES_TYPE.COMPENSATION_ACCUM, dt, item.AccumInjVb * 100, item.AccumLiqVb, SeriesOperation.Division);
                                }
                            }
                            else
                            {
                                if (!ShowInCube)
                                {
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.LIQUID_T , dt, item.Liq);
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.WATERING, dt, ((item.Liq - item.Oil) * 100), item.Liq, SeriesOperation.Division);
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_T, dt, (item.Liq * 24), item.WorkTimeProd, SeriesOperation.Division);
                                }
                                else
                                {
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.LIQUID_M, dt, item.LiqV);
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.WATERING_M, dt, ((item.LiqV - item.OilV) * 100), item.LiqV, SeriesOperation.Division);
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.Q_LIQUID_M, dt, (item.LiqV * 24), item.WorkTimeProd, SeriesOperation.Division);
                                }
                                
                                SumPoint(k, Constant.Chart.SERIES_TYPE.OIL_T, dt, item.Oil);

                                if(item.NatGas > -1) SumPoint(kNatGas, Constant.Chart.SERIES_TYPE.NATGAS, dt, item.NatGas);
                                if(item.GasCondensate > -1) SumPoint(kGasCondensate, Constant.Chart.SERIES_TYPE.GASCONDENSAT, dt, item.GasCondensate);

                                if (item.WorkTimeProd > 0)
                                {
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.Q_OIL_T, dt, (item.Oil * 24), item.WorkTimeProd, SeriesOperation.Division);
                                    if (item.NatGas > -1)
                                    {
                                        SumPoint(kNatGas, Constant.Chart.SERIES_TYPE.Q_NATGAS, dt, (item.NatGas * 24), item.WorkTimeProd, SeriesOperation.Division);
                                    }
                                    if (item.GasCondensate > -1)
                                    {
                                        SumPoint(kGasCondensate, Constant.Chart.SERIES_TYPE.Q_GASCONDENSAT, dt, (item.GasCondensate * 24), item.WorkTimeProd, SeriesOperation.Division);
                                    }
                                }
                                SumPoint(k, Constant.Chart.SERIES_TYPE.INJECTION, dt, item.Inj);
                                if (item.InjGas > 0)
                                {
                                    SumPoint(kInjGas, Constant.Chart.SERIES_TYPE.INJ_GAS, dt, item.InjGas / 1000);
                                }
                                if (item.WorkTimeInj > 0)
                                {
                                    SumPoint(k, Constant.Chart.SERIES_TYPE.W_INJ, dt, item.Inj * 24, item.WorkTimeInj, SeriesOperation.Division);
                                    if (item.InjGas > 0) SumPoint(kInjGas, Constant.Chart.SERIES_TYPE.W_INJ_GAS, dt, item.InjGas * 24, item.WorkTimeInjGas, SeriesOperation.Division);
                                }
                               
                                SumPoint(k, Constant.Chart.SERIES_TYPE.PROD_FUND, dt, item.Fprod);
                                SumPoint(k, Constant.Chart.SERIES_TYPE.INJ_FUND, dt, item.Finj);
                                if (!startCompensation) startCompensation = item.InjVb > 0 && item.LiqVb > 0;
                                if (startCompensation)
                                {
                                    SumPoint(kComp, Constant.Chart.SERIES_TYPE.COMPENSATION, dt, item.InjVb * 100, item.LiqVb, SeriesOperation.Division);
                                    SumPoint(kComp, Constant.Chart.SERIES_TYPE.COMPENSATION_ACCUM, dt, item.AccumInjVb * 100, item.AccumLiqVb, SeriesOperation.Division);
                                }
                            }
                        }
                    }
                }
            }
        }
        private void AddTable81Params(SumParamItem[] SumParams, DateTime minDT)
        {
            if (SumParams == null) return;
            int i, j, ind, k;
            SumParamItem item;
            ChartSeries ser;
            if (SumParams.Length > 0)
            {
                int sumLength = SumParams.Length;
                double dt = 0;
                if (this.IntervalType == 0)
                {
                    ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.LIQUID_PROJ_T);
                    if (ind > -1)
                    {
                        ser = chart.ChartAreas[0].Series[ind];
                        k = 0;

                        for (i = 0; i < sumLength; i++)
                        {
                            item = SumParams[i];
                            if (item.Date < minDT) continue;

                            dt = item.Date.ToOADate();
                            while ((k < ser.Points.Count) && (ser.Points[k].XValue < dt)) k++;

                            InsertPoint(k, Constant.Chart.SERIES_TYPE.LIQUID_PROJ_T, dt, item.Liq);
                            InsertPoint(k, Constant.Chart.SERIES_TYPE.OIL_PROJ_T, dt, item.Oil);

                            InsertPoint(k, Constant.Chart.SERIES_TYPE.INJECTION_PROJ, dt, item.Inj);

                            InsertPoint(k, Constant.Chart.SERIES_TYPE.PROD_FUND_PROJ, dt, item.Fprod);
                            InsertPoint(k, Constant.Chart.SERIES_TYPE.INJ_FUND_PROJ, dt, item.Finj);

                            InsertPoint(k, Constant.Chart.SERIES_TYPE.LEAD_PROD_ALL, dt, item.LeadInAll);
                            InsertPoint(k, Constant.Chart.SERIES_TYPE.LEAD_PROD_OPERATION, dt, item.LeadInProd);
                        }
                    }
                }
            }
        }
        private void AddBPParams(SumParamItem[] SumParams, DateTime minDT)
        {
            if (SumParams == null) return;
            int i, j, ind, k;
            SumParamItem item;
            ChartSeries ser;
            if (SumParams.Length > 0)
            {
                int sumLength = SumParams.Length;
                double dt = 0;
                if (this.IntervalType == 1)
                {
                    ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.OIL_BP_T);
                    if (ind > -1)
                    {
                        ser = chart.ChartAreas[0].Series[ind];
                        k = 0;

                        for (i = 0; i < sumLength; i++)
                        {
                            item = SumParams[i];
                            if (item.Date < minDT) continue;

                            dt = item.Date.ToOADate();
                            while ((k < ser.Points.Count) && (ser.Points[k].XValue < dt)) k++;
                            InsertPoint(k, Constant.Chart.SERIES_TYPE.LIQUID_BP_T, dt, item.Liq * 1000);
                            InsertPoint(k, Constant.Chart.SERIES_TYPE.OIL_BP_T, dt, item.Oil * 1000);
                            InsertPoint(k, Constant.Chart.SERIES_TYPE.INJECTION_BP, dt, item.Inj * 1000);
                        }
                    }
                }
            }
        }
        private void GetDeltaOilByBP(SumParamItem[] FactParams, SumParamItem[] BPParams, out DateTime LastDate, out double dOilYear, out double dOilLastMonth, out double dOilYearPercent, out double dOilLastMonthPercent)
        {
            LastDate = DateTime.MinValue;
            dOilYear = 0;
            dOilLastMonth = 0;
            dOilLastMonthPercent = 0;
            dOilYearPercent = 0;
            if (FactParams != null && BPParams != null && FactParams.Length > 0 && BPParams.Length > 0 && 
                FactParams[FactParams.Length - 1].Date >= BPParams[0].Date)
            {
                LastDate = FactParams[FactParams.Length - 1].Date;

                int ind = (FactParams[FactParams.Length - 1].Date.Year - BPParams[0].Date.Year) * 12 + FactParams[FactParams.Length - 1].Date.Month - BPParams[0].Date.Month;
                double sumFact = 0, sumBP = 0;

                dOilLastMonth = FactParams[FactParams.Length - 1].Oil - BPParams[ind].Oil * 1000;
                if (BPParams[ind].Oil > 0)
                {
                    dOilLastMonthPercent = (dOilLastMonth * 100) / (BPParams[ind].Oil * 1000);
                }
                int i = FactParams.Length - 1;
                while (ind >= 0 && i >= 0)
                {
                    sumFact += FactParams[i].Oil;
                    sumBP += BPParams[ind].Oil * 1000;
                    i--;
                    ind--;
                }
                if (sumFact > 0 || sumBP > 0)
                {
                    dOilYear = sumFact - sumBP;
                    if (sumBP > 0)
                    {
                        dOilYearPercent = (dOilYear * 100) / sumBP;
                    }
                }
            }
        }
        private void AddSumPoints(int PlastCode, SumParamItem[] SumParams, DateTime minDT, bool FirstSum, int ShiftOneDate)
        {
            if (FirstSum)
            {
                ShowSumParams(PlastCode, SumParams, minDT, ShiftOneDate);
            }
            else
            {
                AddSumParams(PlastCode, SumParams, minDT, ShiftOneDate);
            }
        }
        private void ShowSumParams(int PlastCode, SumParamItem[] SumParams, DateTime minDT, int ShiftOneDate)
        {
            if (SumParams == null) return;
            int i, j;
            SumParamItem item;
            if (SumParams.Length > 0)
            {
                double dt = 0;
                if ((this.ActiveScheme != -1) && (SchemeList[ActiveScheme].ByPlastMode) && (SchemeList[ActiveScheme].PlastModeSeries != null))
                {
                    for (i = 0; i < SumParams.Length; i++)
                    {
                        item = SumParams[i];
                        if (item.Date < minDT) continue;

                        if (ShiftOneDate != -1)
                        {
                            dt = i - ShiftOneDate;
                        }
                        else
                        {
                            dt = item.Date.ToOADate();
                        }
                        switch (SchemeList[ActiveScheme].PlastModeSeries.SeriesType)
                        {
                            case Constant.Chart.SERIES_TYPE.LIQUID_T:
                                AddPoint(PlastCode, dt, item.Liq);
                                break;
                            case Constant.Chart.SERIES_TYPE.OIL_T:
                                AddPoint(PlastCode, dt, item.Oil);
                                break;
                            case Constant.Chart.SERIES_TYPE.INJECTION:
                                AddPoint(PlastCode, dt, item.Inj);
                                break;
                        }
                    }
                }
            }
        }
        private void AddSumParams(int PlastCode, SumParamItem[] SumParams, DateTime minDT, int ShiftOneDate)
        {
            if (SumParams == null) return;
            int i, j, ind, k;
            SumParamItem item;
            ChartSeries ser;
            if (SumParams.Length > 0)
            {
                int sumLength = SumParams.Length;
                double dt = 0;
                if ((this.ActiveScheme != -1) && (SchemeList[ActiveScheme].ByPlastMode) && (SchemeList[ActiveScheme].PlastModeSeries != null))
                {
                    double serMinDate = DateTime.MaxValue.ToOADate();
                    ind = SchemeList[ActiveScheme].GetSeriesIndexByPlastCode(PlastCode);
                    ser = chart.ChartAreas[0].Series[ind];
                    if ((ind > -1) && (ser.Points.Count > 0))
                    {
                        if (!ser.IsEmpty) serMinDate = ser.Points[0].XValue;
                        k = 0;

                        for (i = 0; i < sumLength; i++)
                        {
                            item = SumParams[i];
                            if (item.Date < minDT) continue;

                            if (ShiftOneDate != -1)
                            {
                                dt = i - ShiftOneDate;
                            }
                            else
                            {
                                dt = item.Date.ToOADate();
                            }
                            while ((k < ser.Points.Count) && (ser.Points[k].XValue < dt)) k++;

                            if (dt < serMinDate)
                            {
                                switch (SchemeList[ActiveScheme].PlastModeSeries.SeriesType)
                                {
                                    case Constant.Chart.SERIES_TYPE.LIQUID_T:
                                        InsertPoint(k, PlastCode, dt, item.Liq);
                                        break;
                                    case Constant.Chart.SERIES_TYPE.OIL_T:
                                        InsertPoint(k, PlastCode, dt, item.Oil);
                                        break;
                                    case Constant.Chart.SERIES_TYPE.INJECTION:
                                        InsertPoint(k, PlastCode, dt, item.Inj);
                                        break;
                                }
                            }
                            else
                            {
                                switch (SchemeList[ActiveScheme].PlastModeSeries.SeriesType)
                                {
                                    case Constant.Chart.SERIES_TYPE.LIQUID_T:
                                        SumPoint(k, PlastCode, dt, item.Liq);
                                        break;
                                    case Constant.Chart.SERIES_TYPE.OIL_T:
                                        SumPoint(k, PlastCode, dt, item.Oil);
                                        break;
                                    case Constant.Chart.SERIES_TYPE.INJECTION:
                                        SumPoint(k, PlastCode, dt, item.Inj);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void AddAccumParams(int OilObjCode, SumParamItem[] SumParams)
        {
            AccumParams acc;
            acc = AccList.GetItemByOilObjCode(OilObjCode);
            bool ShowInCube = mainForm.AppSettings.ShowDataLiquidInCube;
            if (acc != null)
            {
                acc.AccumLiq = 0;
                acc.AccumInj = 0;
                acc.AccumInjGas = 0;
                acc.AccumOil = 0;
                acc.AccumWat = 0;
                for (int i = 0; i < SumParams.Length; i++)
                {
                    acc.AccumLiq += (ShowInCube) ? SumParams[i].LiqV : SumParams[i].Liq;
                    acc.AccumOil += SumParams[i].Oil;
                    acc.AccumInj += SumParams[i].Inj;
                    acc.AccumWat += SumParams[i].Liq - SumParams[i].Oil;
                    if (SumParams[i].InjGas > -1) acc.AccumInjGas += SumParams[i].InjGas;
                }
                if (acc.AccumLiq == 0)
                {
                    acc.AccumLiq = -1;
                    acc.AccumOil = -1;
                    acc.AccumWat = -1;
                }
                if (acc.AccumInj == 0) acc.AccumInj = -1;
                if (acc.AccumInjGas == 0) acc.AccumInjGas = -1;
            }
        }
        private void CalcProjectEdgePoints(double K1, double K2, int aveMonth)
        {
            ChartArea area = chart.ChartAreas[0];
            int ind = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.OIL_T);
            int ind2 = SchemeList[ActiveScheme].GetSeriesIndexByType(Constant.Chart.SERIES_TYPE.PROJECT_EDGE_POINTS);

            if ((ind != -1) && (ind < area.Series.Count))
            {
                ChartSeries ser = area.Series[ind];
                ChartSeries ser2 = area.Series[ind2];
                double max = 0, sum;
                DataPoint pt = DataPoint.Empty;
                for (int i = 0; i < ser.Points.Count; i++)
                {
                    sum = 0;
                    for (int j = i; (j - i + 1 <= aveMonth) && (j < ser.Points.Count); j++)
                    {
                        sum += ser.Points[j].YValue;
                    }
                    sum = sum / aveMonth;
                    if (max < sum) max = sum;
                }
                if (max > 0)
                {
                    for (int i = 0; i < ser.Points.Count; i++)
                    {
                        if (ser2.Points.Count == 0)
                        {
                            if (ser.Points[i].YValue > 0)
                            {
                                ser2.AddXY(ser.Points[i].XValue, ser.Points[i].YValue);
                            }
                        }
                        else
                        {
                            if (ser2.Points.Count == 1)
                            {
                                sum = 0;
                                for (int j = i; (j - i + 1 <= aveMonth) && (j < ser.Points.Count); j++)
                                {
                                    sum += ser.Points[j].YValue;
                                }
                                sum = sum / aveMonth;
                                if (sum > K1 * max)
                                {
                                    ser2.AddXY(ser.Points[i].XValue, ser.Points[i].YValue);
                                }
                            }
                            else if (ser2.Points.Count == 2)
                            {
                                sum = 0;
                                for (int j = i; (j - i + 1 <= aveMonth) && (j < ser.Points.Count); j++)
                                {
                                    sum += ser.Points[j].YValue;
                                }
                                sum = sum / aveMonth;
                                if (sum == max)
                                {
                                    ser2.AddXY(ser.Points[i].XValue, ser.Points[i].YValue);
                                    break;
                                }
 
                            }
                        }
                    }
                    if(ser2.Points.Count == 3)
                    {
                        for (int i = ser.Points.Count - aveMonth; i >= 0; i--)
                        {
                            if (ser.Points[i].XValue < ser2.Points[2].XValue) break;
                            sum = 0;
                            for (int j = i; (j - i + 1 <= aveMonth) && (j < ser.Points.Count); j++)
                            {
                                sum += ser.Points[j].YValue;
                            }
                            sum = sum / aveMonth;
                            if ((sum > 0) && (sum < K2 * max))
                            {
                                pt.XValue = ser.Points[i].XValue;
                                pt.YValue = ser.Points[i].YValue;
                            }
                        }
                        if (!pt.IsEmpty) ser2.AddXY(pt.XValue, pt.YValue);
                    }
                }
            }
        }
        #endregion

        #region SUM CHESS
        private void AddSumChessPoints(SumParamItem[] SumParams, bool FirstSum, int ShiftOneDate)
        {
            if (FirstSum)
            {
                ShowChessSumParams(SumParams, ShiftOneDate);
            }
            else
            {
                AddChessSumParams(SumParams, ShiftOneDate);
            }
        }
        private void ShowChessSumParams(SumParamItem[] SumParams, int ShiftOneDate)
        {
            if (SumParams == null) return;
            int i;
            SumParamItem item;
            bool ShowInCube = mainForm.AppSettings.ShowDataLiquidInCube;
            if (SumParams.Length > 0)
            {
                int sumLength = SumParams.Length;
                double dt = 0;
                for (i = 0; i < sumLength; i++)
                {
                    item = SumParams[i];
                    if (ShiftOneDate != -1)
                    {
                        dt = i - ShiftOneDate;
                    }
                    else
                    {
                        dt = item.Date.ToOADate();
                    }

                    if (!ShowInCube)
                    {
                        AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_SUM, dt, item.Liq);
                        if (item.Fprod > 0) AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_AVERAGE, dt, item.Liq / item.Fprod);
                        if (item.Liq > 0) AddPoint(Constant.Chart.SERIES_TYPE.WATERING_CHESS, dt, ((item.Liq - item.Oil) * 100) / item.Liq);
                    }
                    else
                    {
                        AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_SUM_M, dt, item.LiqV);
                        if (item.Fprod > 0) AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_AVERAGE_M, dt, item.LiqV / item.Fprod);
                        if (item.Liq > 0) AddPoint(Constant.Chart.SERIES_TYPE.WATERING_CHESS_M, dt, ((item.LiqV - item.OilV) * 100) / item.LiqV);
                    }
                    
                    AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_SUM, dt, item.Oil);
                    if (item.Fprod > 0) AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_AVERAGE, dt, item.Oil / item.Fprod);
                    
                    AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_CHESS_SUM, dt, item.Inj);
                    if (item.Finj > 0) AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_CHESS_AVERAGE, dt, item.Inj / item.Finj);
                    AddPoint(Constant.Chart.SERIES_TYPE.PROD_FUND_CHESS, dt, item.Fprod);
                    AddPoint(Constant.Chart.SERIES_TYPE.INJ_FUND_CHESS, dt, item.Finj);
                }
            }
        }
        private void AddChessSumParams(SumParamItem[] SumParams, int ShiftOneDate)
        {
            if (SumParams == null) return;
            int i;
            SumParamItem item;
            bool ShowInCube = mainForm.AppSettings.ShowDataLiquidInCube;
            if (SumParams.Length > 0)
            {
                int sumLength = SumParams.Length;
                double dt = 0;
                for (i = 0; i < sumLength; i++)
                {
                    item = SumParams[i];
                    if (ShiftOneDate != -1)
                    {
                        dt = i - ShiftOneDate;
                    }
                    else
                    {
                        dt = item.Date.ToOADate();
                    }

                    if (!ShowInCube)
                    {
                        AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_SUM, dt, item.Liq);
                        if (item.Fprod > 0) AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_AVERAGE, dt, item.Liq / item.Fprod);
                        if (item.Liq > 0) AddPoint(Constant.Chart.SERIES_TYPE.WATERING_CHESS, dt, ((item.Liq - item.Oil) * 100) / item.Liq);
                    }
                    else
                    {
                        AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_SUM_M, dt, item.LiqV);
                        if (item.Fprod > 0) AddPoint(Constant.Chart.SERIES_TYPE.Q_LIQ_CHESS_AVERAGE_M, dt, item.LiqV / item.Fprod);
                        if (item.Liq > 0) AddPoint(Constant.Chart.SERIES_TYPE.WATERING_CHESS_M, dt, ((item.LiqV - item.OilV) * 100) / item.LiqV);
                    }

                    AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_SUM, dt, item.Oil);
                    if (item.Fprod > 0) AddPoint(Constant.Chart.SERIES_TYPE.Q_OIL_CHESS_AVERAGE, dt, item.Oil / item.Fprod);

                    AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_CHESS_SUM, dt, item.Inj);
                    if (item.Finj > 0) AddPoint(Constant.Chart.SERIES_TYPE.W_INJ_CHESS_AVERAGE, dt, item.Inj / item.Finj);
                    AddPoint(Constant.Chart.SERIES_TYPE.PROD_FUND_CHESS, dt, item.Fprod);
                    AddPoint(Constant.Chart.SERIES_TYPE.INJ_FUND_CHESS, dt, item.Finj);
                }
            }
        }
        #endregion
        #endregion

        #region CHART EVENTS
        void chart_MouseMove(object sender, MouseEventArgs e)
        {
            mainForm.SetStatus(chart.GetTooltipTitle());
            if (!mainForm.UserDialogShowing) chart.Focus();
        }
        void chart_MouseWheel(object sender, MouseEventArgs e)
        {
            //mainForm.StatUsage.AddMessage(CollectorStatId.GRAPHICS_ZOOM);
        }
        void chart_MouseDown(object sender, MouseEventArgs e)
        {
            //mainForm.StatUsage.AddMessage(CollectorStatId.GRAPHICS_MOUSE_DOWN);
        }
        void chart_MouseUp(object sender, MouseEventArgs e)
        {
        }
        void DataChart_ChartScaleChange()
        {
            SetAxesXInterval();
        }
        void DataChart_ChartLegendItemChange()
        {
            if (TableVisible) this.tableGraphics.ShowData();
            if (ByPlastMode)
            {
                ReCalcAccumParams();
            }
        }
        void DataChart_OnChartSnapShoot(Rectangle SnapRect)
        {
            if (SnapRect.Width + SnapRect.Height > 0)
            {
                if(SnapRectToClipboard(SnapRect))
                {
                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Участок графика скопирован в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".");
                }
            }
        }
        #endregion

        // DRAWING

        #region DRAW WAIT ICON
        public void StartDrawWaitIcon()
        {
            SetToolbarItemsEnable(false);
            DrawWaitIconMode = true;
            chart.Enabled = false;
            chart.ClearBitMap();
            chart.EnableToolTip = false;
            tmrWait.Start();
        }
        public void StopDrawWaitIcon()
        {
            SetToolbarItemsEnable(true);
            DrawWaitIconMode = false;
            chart.Enabled = true;
            chart.EnableToolTip = true;
            tmrWait.Stop();
        }
        void tmrWait_Tick(object sender, EventArgs e)
        {
            if (DrawWaitIconMode)
            {
                wait_angle++;
                if (wait_angle > 11) wait_angle = 0;
                chart.Invalidate();
            }
            else
            {
                tmrWait.Stop();
            }
        }
        public void DrawWaitIcon(Graphics gr)
        {
            if (this.DrawWaitIconMode)
            {
                PointF center = new PointF((float)(chart.ClientRectangle.Right / 2), (float)(chart.ClientRectangle.Bottom / 2));
                gr.DrawImage(mainForm.bmpWait[wait_angle], center.X - 16, center.Y - 16, 32, 32);
            }
        }
        #endregion

        #region SNAPSHOOT
        public void SnapToClipboard(bool SnapAll)
        {
            if (SnapAll)
            {
                if (SnapRectToClipboard(chart.ClientRectangle))
                {
                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "График скопирован в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".");
                }
            }
            else
            {
                chart.SnapShootMode = true;
            }
        }
        public bool SnapRectToClipboard(Rectangle rect)
        {
            bool result = false;
            ClipboardImageHelper help = new ClipboardImageHelper(mainForm, mainForm.AppSettings.CopyClipboardFormat, new Rectangle(0, 0, rect.Width, rect.Height));
            Graphics grfx = help.GetGraphics();
            if (grfx != null)
            {
                grfx.TranslateTransform(chart.ClientRectangle.X - rect.X, chart.ClientRectangle.Y - rect.Y);
                chart.DrawChartAreas(grfx);
                DrawAccumInfo(grfx);
                DrawProjInfo(grfx);
                chart.DrawToolTip(grfx);
                grfx.Dispose();
                result = help.PutImageOnClipboard();
            }
            help.Dispose();
            return result;
        }
        #endregion

        #region ACCUM PARAMS
        public void ReCalcAccumParams()
        {
            if ((AccList != null) && (ActiveScheme != -1))
            {
                if (ByPlastMode)
                {
                    Scheme scheme = SchemeList[ActiveScheme];
                    SeriesTyped series;
                    List<int> plastCodes = new List<int>();
                    for (int i = 0; i < scheme.Series.Count; i++)
                    {
                        series = scheme.Series[i];
                        if ((i < chart.ChartAreas[0].Series.Count) && (chart.ChartAreas[0].Series[i].Enabled && chart.ChartAreas[0].Series[i].Visible))
                        {
                            plastCodes.Add(series.PlastCode);
                        }
                    }
                    AccList.SumAccumByPlastCodes(plastCodes);
                    plastCodes.Clear();
                }
                else
                {
                    AccList.SumAllAccum();
                }
            }
        }
        #endregion
        
        public void ReDrawChartAreas()
        {
            chart.DrawChartAreas();
        }
        public void DrawAccumInfo(Graphics gr)
        {
            AccumParams acc = AccList[0];
            float X, Y;
            X = chart.ChartAreas[0].InnerPlotPosition.Left + 2;
            Y = chart.ChartAreas[0].InnerPlotPosition.Top;
            string liqUnit = (mainForm.AppSettings != null && mainForm.AppSettings.ShowDataLiquidInCube) ? "м3" : "т";
            if ((this.ShowInfo) && (acc != null) && ((acc.AccumLiq > -1) || (acc.AccumInj > -1) || (acc.AccumInjGas > -1)) &&
                (chart.ChartAreas[0].InnerPlotPosition != RectangleF.Empty) &&
                (chart.ChartAreas[0].Enabled))
            {
                string strLiq, strOil, strWat, strInj, strInjGas, strBPYear, strBPMonth, strBPYearParams, strBPMonthParams;
                bool plusBPYear = acc.dOilYear > 0;
                bool plusBPMonth = acc.dOilLastMonth > 0;

                strBPYear = string.Format("dQн({0}): ", acc.BPDate.Year);
                strBPYearParams = string.Format("{0}{1:0.#} ({2}{3:0.#} %)", plusBPYear ? "+" : "", acc.dOilYear, plusBPYear ? "+" : "", acc.dOilYearPercent);
                strBPMonth = string.Format("dQн({0:MM.yyyy}): ",acc.BPDate);
                strBPMonthParams = string.Format("{0}{1:0.#} ({2}{3:0.#} %)", plusBPMonth ? "+" : "", acc.dOilLastMonth, plusBPMonth ? "+" : "", acc.dOilLastMonthPercent);
                if (acc.AccumLiq > 999999)
                {
                    strLiq = String.Format("Нак.жидкость: {0:#,#} тыс.{1}", acc.AccumLiq / 1000, liqUnit);
                    if (acc.AccumOil > 999999)
                    {
                        strOil = String.Format("Нак.нефть: {0:#,#} тыс.т", acc.AccumOil / 1000);
                    }
                    else
                    {
                        strOil = String.Format("Нак.нефть: {0:#,#.###} тыс.т", acc.AccumOil / 1000);
                    }
                    if (acc.AccumWat > 999999)
                    {
                        strWat = String.Format("Нак.вода: {0:#,#} тыс.т", (acc.AccumWat) / 1000);
                    }
                    else
                    {
                        strWat = String.Format("Нак.вода: {0:#,#.###} тыс.т", (acc.AccumWat) / 1000);
                    }
                }
                else
                {
                    strLiq = String.Format("Нак.жидкость: {0:#,#} {1}", acc.AccumLiq, liqUnit);
                    strOil = String.Format("Нак.нефть: {0:#,#} т", acc.AccumOil);
                    strWat = String.Format("Нак.вода: {0:#,#} т", (acc.AccumWat));
                }
                if (acc.AccumInj > 999999)
                {
                    strInj = String.Format("Нак.закачка: {0:#,#} тыс.м3", acc.AccumInj / 1000);
                }
                else
                {
                    strInj = String.Format("Нак.закачка: {0:#,#} м3", acc.AccumInj);
                }
                if (acc.AccumInjGas > 999999999)
                {
                    strInjGas = String.Format("Нак.закачка газа: {0:#,#} млн.м3", acc.AccumInjGas / 1000000);
                }
                else
                {
                    strInjGas = String.Format("Нак.закачка газа: {0:#,#} тыс.м3", acc.AccumInjGas / 1000);
                }
                SizeF size = gr.MeasureString(strLiq, grFont, PointF.Empty, StringFormat.GenericTypographic);

                if (acc.AccumLiq > -1)
                {
                    gr.DrawString(strLiq, grFont, Brushes.White, X - 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strLiq, grFont, Brushes.White, X - 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strLiq, grFont, Brushes.White, X + 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strLiq, grFont, Brushes.White, X + 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strLiq, grFont, Brushes.DarkGreen, X, Y, StringFormat.GenericTypographic);

                    Y += size.Height;
                    gr.DrawString(strOil, grFont, Brushes.White, X - 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strOil, grFont, Brushes.White, X - 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strOil, grFont, Brushes.White, X + 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strOil, grFont, Brushes.White, X + 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strOil, grFont, Brushes.SaddleBrown, X, Y, StringFormat.GenericTypographic);

                    Y += size.Height;
                    gr.DrawString(strWat, grFont, Brushes.White, X - 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strWat, grFont, Brushes.White, X - 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strWat, grFont, Brushes.White, X + 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strWat, grFont, Brushes.White, X + 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strWat, grFont, Brushes.DodgerBlue, X, Y, StringFormat.GenericTypographic);
                    Y += size.Height;
                }
                if (acc.AccumInj > -1)
                {
                    gr.DrawString(strInj, grFont, Brushes.White, X - 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strInj, grFont, Brushes.White, X - 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strInj, grFont, Brushes.White, X + 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strInj, grFont, Brushes.White, X + 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strInj, grFont, Brushes.DarkBlue, X, Y, StringFormat.GenericTypographic);
                    Y += size.Height;
                }
                if (acc.AccumInjGas > -1)
                {
                    gr.DrawString(strInjGas, grFont, Brushes.White, X - 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strInjGas, grFont, Brushes.White, X - 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strInjGas, grFont, Brushes.White, X + 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strInjGas, grFont, Brushes.White, X + 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strInjGas, grFont, Brushes.Goldenrod, X, Y, StringFormat.GenericTypographic);
                    Y += size.Height;
                }
                if (acc.BPDate != DateTime.MinValue)
                {
                    gr.DrawString(strBPYear, grFont, Brushes.White, X - 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYear, grFont, Brushes.White, X - 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYear, grFont, Brushes.White, X + 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYear, grFont, Brushes.White, X + 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYear, grFont, Brushes.Black, X, Y, StringFormat.GenericTypographic);
                    SizeF s = gr.MeasureString(strBPYear, grFont, PointF.Empty, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYearParams, grFont, Brushes.White, X + s.Width, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYearParams, grFont, Brushes.White, X + s.Width, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYearParams, grFont, Brushes.White, X + 2 + s.Width, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYearParams, grFont, Brushes.White, X + 2 + s.Width, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPYearParams, grFont, plusBPYear ? Brushes.Green : Brushes.Red, X + 1 + s.Width, Y, StringFormat.GenericTypographic);
                    Y += size.Height;
                    gr.DrawString(strBPMonth, grFont, Brushes.White, X - 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonth, grFont, Brushes.White, X - 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonth, grFont, Brushes.White, X + 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonth, grFont, Brushes.White, X + 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonth, grFont, Brushes.Black, X, Y, StringFormat.GenericTypographic);
                    s = gr.MeasureString(strBPMonth, grFont, PointF.Empty, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonthParams, grFont, Brushes.White, X + s.Width, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonthParams, grFont, Brushes.White, X + s.Width, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonthParams, grFont, Brushes.White, X + 2 + s.Width, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonthParams, grFont, Brushes.White, X + 2 + s.Width, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(strBPMonthParams, grFont, plusBPMonth ? Brushes.Green : Brushes.Red, X + 1 + s.Width, Y, StringFormat.GenericTypographic);

                    Y += size.Height;
                }
                if (AccList.Comment.Length > 0)
                {
                    gr.DrawString(AccList.Comment, grFont, Brushes.White, X - 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(AccList.Comment, grFont, Brushes.White, X - 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(AccList.Comment, grFont, Brushes.White, X + 1, Y - 1, StringFormat.GenericTypographic);
                    gr.DrawString(AccList.Comment, grFont, Brushes.White, X + 1, Y + 1, StringFormat.GenericTypographic);
                    gr.DrawString(AccList.Comment, grFont, Brushes.Black, X, Y, StringFormat.GenericTypographic);
                }
            }
        }
        public void DrawProjInfo(Graphics gr)
        {
            if ((this.ShowInfo) && (this.ShowProjParams) && (ProjParamsXValue > -1) && (chart.ChartAreas[0].Enabled))
            {
                SizeF size = gr.MeasureString(ProjParamsName, grFont);
                float X, Y;
                X = chart.ChartAreas[0].AxisX.ScreenPosByValue(ProjParamsXValue);
                Y = chart.ChartAreas[0].InnerPlotPosition.Top + 2;
                if (chart.ChartAreas[0].InnerPlotPosition.Contains(X, Y))
                {
                    gr.DrawLine(Pens.Red, X, Y + size.Height, X, Y + chart.ChartAreas[0].InnerPlotPosition.Height - 2);
                    if (X + size.Width > chart.ChartAreas[0].InnerPlotPosition.X + chart.ChartAreas[0].InnerPlotPosition.Width)
                    {
                        X = chart.ChartAreas[0].InnerPlotPosition.X + chart.ChartAreas[0].InnerPlotPosition.Width - size.Width - 2;
                    }
                    gr.DrawString(ProjParamsName, grFont, Brushes.White, X - 1, Y - 1);
                    gr.DrawString(ProjParamsName, grFont, Brushes.White, X - 1, Y + 1);
                    gr.DrawString(ProjParamsName, grFont, Brushes.White, X + 1, Y - 1);
                    gr.DrawString(ProjParamsName, grFont, Brushes.White, X + 1, Y + 1);

                    gr.DrawString(ProjParamsName, grFont, Brushes.Red, X, Y);
                }
            }
            if (ShowInfo && ShowBPParams && chart.ChartAreas[0].Enabled && AccList != null)
            {
                AccumParams acc = AccList[0];
                if (acc.BPDate > DateTime.MinValue)
                {
                    string bpName = string.Format("Бизнес-план [Утв. {0:dd.MM.yyyy}]", acc.BPDate);
                    SizeF size = gr.MeasureString(bpName, grFont);
                    float X, Y;
                    X = chart.ChartAreas[0].AxisX.ScreenPosByValue(acc.BPDate.ToOADate());
                    Y = chart.ChartAreas[0].InnerPlotPosition.Top + 2;
                    if (chart.ChartAreas[0].InnerPlotPosition.Contains(X, Y))
                    {
                        gr.DrawLine(Pens.Red, X, Y + size.Height, X, Y + chart.ChartAreas[0].InnerPlotPosition.Height - 2);
                        if (X + size.Width > chart.ChartAreas[0].InnerPlotPosition.X + chart.ChartAreas[0].InnerPlotPosition.Width)
                        {
                            X = chart.ChartAreas[0].InnerPlotPosition.X + chart.ChartAreas[0].InnerPlotPosition.Width - size.Width - 2;
                        }
                        gr.DrawString(bpName, grFont, Brushes.White, X - 1, Y - 1);
                        gr.DrawString(bpName, grFont, Brushes.White, X - 1, Y + 1);
                        gr.DrawString(bpName, grFont, Brushes.White, X + 1, Y - 1);
                        gr.DrawString(bpName, grFont, Brushes.White, X + 1, Y + 1);
                        gr.DrawString(bpName, grFont, Brushes.Red, X, Y);
                    }
                }
            }

        }
        void chart_Paint(object sender, PaintEventArgs e)
        {
            if (!DrawWaitIconMode)
            {
                DrawAccumInfo(e.Graphics);
                DrawProjInfo(e.Graphics);
                chart.DrawToolTip(e.Graphics);
            }
            DrawWaitIcon(e.Graphics);
        }
    }

    sealed class DataGridGraphics : DataGridView
    {
        internal struct ColumnWidthItem
        {
            public int Type;
            public int Width;
        }
        Project _proj;
        DataChart ParentChart;
        MainForm mainForm;
        DataSet _dsGraph;
        public DataTable table;
        Font _font, _font_bold;
        DataGridViewCellStyle TableCellStyleAll;
        DataGridViewCellStyle TableCellStyleFloat;
        DataGridViewCellStyle TableCellStyleDateMER;
        ArrayList ColumnWidth;
        DataGridViewCell lastSelectCell;

        public DataGridGraphics(DataChart parentChart, MainForm MainForm)
        {
            InitContextMenu();

            ColumnWidth = new ArrayList();
            SaveColumnWidth(-1, 100);
            DataGridViewCellStyle HeaderCellStyle = new DataGridViewCellStyle();
            TableCellStyleAll = new DataGridViewCellStyle();
            TableCellStyleFloat = new DataGridViewCellStyle();
            TableCellStyleDateMER = new DataGridViewCellStyle();
            


            _font = new Font("Calibri", 8.25f);
            _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);

            this.ParentChart = parentChart;

            this.mainForm = MainForm;
            this.Parent = mainForm.scGraphics.Panel2;
            this.Dock = DockStyle.Fill;

            _dsGraph = new DataSet("dsGraphics");
            table = new DataTable("GRAPH");

            table.Columns.Add("Индекс", Type.GetType("System.Int32"));      // 0
            table.Columns.Add("Дата", Type.GetType("System.DateTime"));     // 1

            TableCellStyleAll.Font = _font;
            TableCellStyleFloat.Font = _font;
            TableCellStyleDateMER.Font = _font;
            TableCellStyleFloat.Format = "0.##";
            TableCellStyleDateMER.Format = "MM.yyyy";
            
            _dsGraph.Tables.Add(table);
            this.DataSource = _dsGraph.Tables[0];

            // Настройка таблицы графика
            this.RowTemplate.Height = 13;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = true;
            this.DoubleBuffered = true;

            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
            HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
            HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
            this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;
            this.BackgroundColor = Color.White;
            this.ColumnWidthChanged += new DataGridViewColumnEventHandler(DataGridGraphics_ColumnWidthChanged);
            this.KeyDown += new KeyEventHandler(DataGridGraphics_KeyDown);
            this.MouseDown += new MouseEventHandler(DataGridGraphics_MouseDown);
        }

        #region Context Menu
        ContextMenuStrip cMenu;
        void InitContextMenu()
        {
            cMenu = new ContextMenuStrip();
            ToolStripMenuItem cmCopyCells = (ToolStripMenuItem)cMenu.Items.Add("Копировать в буфер обмена", Properties.Resources.Copy16);
            cmCopyCells.Click += new EventHandler(cmCopyCells_Click);
        }
        void cmCopyCells_Click(object sender, EventArgs e)
        {
            copySelectedRowsToClipboard((DataGridView)this);
        }
        #endregion

        void DataGridGraphics_MouseDown(object sender, MouseEventArgs e)
        {
            lastSelectCell = null;
            ContextMenuStrip = null;
            HitTestInfo hit = HitTest(e.X, e.Y);
            if ((e.Button == MouseButtons.Right) && (hit.ColumnIndex != -1) && (hit.RowIndex != -1))
            {
                lastSelectCell = Rows[hit.RowIndex].Cells[hit.ColumnIndex];
                if (lastSelectCell.Selected) ContextMenuStrip = cMenu;
            }
        }

        void DataGridGraphics_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            int index = e.Column.Index;
            if (index == 0)
            {
                SaveColumnWidth(-1, e.Column.Width);
            }
            else if (index > 0)
            {
                Scheme scheme = null;
                if ((ParentChart.SchemeList.Length > 0) && (ParentChart.ActiveScheme < ParentChart.SchemeList.Length))
                {
                    scheme = ParentChart.SchemeList[ParentChart.ActiveScheme];
                }
                index = scheme.Series.Count - index;
                if ((index > -1) && (index < scheme.Series.Count))
                {
                    SaveColumnWidth((int)scheme.Series[index].SeriesType, e.Column.Width);
                }
            }
        }

        void DataGridGraphics_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
            {
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    copySelectedRowsToClipboard((DataGridView)sender);
                }
            }
        }
        private void copySelectedRowsToClipboard(DataGridView dgv)
        {
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            Clipboard.Clear();
            if (dgv.GetClipboardContent() != null)
            {
                Clipboard.SetDataObject(dgv.GetClipboardContent());
                Clipboard.GetData(DataFormats.Text);
                IDataObject dt = Clipboard.GetDataObject();
                if (dt.GetDataPresent(typeof(string)))
                {
                    string tb = (string)(dt.GetData(typeof(string)));
                    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                    byte[] dataStr = encoding.GetBytes(tb);
                    Clipboard.SetDataObject(encoding.GetString(dataStr));
                }
            }
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
        }

        void SaveColumnWidth(int Type, int width)
        {
            bool find = false;
            ColumnWidthItem item;
            for (int i = 0; i < ColumnWidth.Count; i++)
            {
                item = (ColumnWidthItem)ColumnWidth[i];
                if (item.Type == Type)
                {
                    item.Width = width;
                    ColumnWidth[i] = item;
                    find = true;
                }
            }
            if (!find)
            {
                item.Type = Type;
                item.Width = width;
                ColumnWidth.Add(item);
            }
        }
        int LoadColumnWidth(int Type)
        {
            ColumnWidthItem item;
            for (int i = 0; i < ColumnWidth.Count; i++)
            {
                item = (ColumnWidthItem)ColumnWidth[i];
                if (item.Type == Type)
                {
                    return item.Width;
                }
            }
            return -1;
        }

        public void SetProject(Project aProject)
        {
            _proj = aProject;
        }
        public void ClearTable()
        {
            table.Clear();
            while (table.Columns.Count > 2) table.Columns.RemoveAt(2);
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();
        }
        void AddNewDataRows(DateTime minDT, DateTime maxDT)
        {
            DateTime dt;
            DataRow row;
            if (table.Rows.Count == 0)
            {
                row = table.NewRow();
                row[0] = 0;
                row[1] = minDT;
                for (int i = 2; i < table.Columns.Count; i++) row[i] = 0;
                table.Rows.Add(row);
            }
            if (!mainForm.canv.IsGraphicsByOneDate)
            {
                dt = (DateTime)table.Rows[0].ItemArray[1];
                if (ParentChart.IntervalType == 0)
                {
                    dt = new DateTime(dt.Year, 1, 1);
                }
                else if (ParentChart.IntervalType == 1)
                {
                    dt = new DateTime(dt.Year, dt.Month, 1);
                }
                else
                {
                    dt = new DateTime(dt.Year, dt.Month, dt.Day);
                }
                while (dt > minDT)
                {
                    if (ParentChart.IntervalType == 0)
                        dt = dt.AddYears(-1);
                    if (ParentChart.IntervalType == 1)
                        dt = dt.AddMonths(-1);
                    if (ParentChart.IntervalType == 2)
                        dt = dt.AddDays(-1);

                    row = table.NewRow();
                    row[0] = table.Rows.Count - 1;
                    row[1] = dt;
                    for (int i = 2; i < table.Columns.Count; i++) row[i] = 0;
                    table.Rows.InsertAt(row, 0);
                }
                dt = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[1];
                if (ParentChart.IntervalType == 0)
                {
                    dt = new DateTime(dt.Year, 1, 1);
                }
                else if (ParentChart.IntervalType == 1)
                {
                    dt = new DateTime(dt.Year, dt.Month, 1);
                }
                else
                {
                    dt = new DateTime(dt.Year, dt.Month, dt.Day);
                }
                while (dt < maxDT)
                {
                    if (ParentChart.IntervalType == 0)
                    {
                        dt = dt.AddYears(1);
                    }
                    if (ParentChart.IntervalType == 1)
                    {
                        dt = dt.AddMonths(1);
                    }
                    if (ParentChart.IntervalType == 2)
                    {
                        dt = dt.AddDays(1);
                    }
                    row = table.NewRow();
                    row[0] = table.Rows.Count - 1;
                    row[1] = dt;
                    for (int i = 2; i < table.Columns.Count; i++) row[i] = 0;
                    table.Rows.Add(row);
                }
            }
            else
            {
                dt = (DateTime)table.Rows[0].ItemArray[1];
                while (dt > minDT)
                {
                    dt = dt.AddDays(-1);
                    row = table.NewRow();
                    row[0] = table.Rows.Count - 1;
                    row[1] = dt;
                    for (int i = 2; i < table.Columns.Count; i++) row[i] = 0;
                    table.Rows.InsertAt(row, 0);
                }
                dt = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[1];
                while (dt < maxDT)
                {
                    dt = dt.AddDays(1);
                    row = table.NewRow();
                    row[0] = table.Rows.Count - 1;
                    row[1] = dt;
                    for (int i = 2; i < table.Columns.Count; i++) row[i] = 0;
                    table.Rows.Add(row);
                }
            }
        }

        public void ShowData()
        {
            int i, j, ind, x, indMax;
            this.DataSource = null;
            if (_proj == null) return;
            Scheme scheme = null;
            if ((ParentChart.SchemeList.Length > 0) && (ParentChart.ActiveScheme < ParentChart.SchemeList.Length))
            {
                scheme = ParentChart.SchemeList[ParentChart.ActiveScheme];
            }
            ChartSeries series;
            DataColumn col;
            string colName;
            DataRow row;
            DateTime minDT, dt, maxDT, currDt;
            this.ClearTable();
            double lastValue;
            int Xvalue;
            if (scheme != null)
            {
                ChartArea area = ParentChart.GetChartArea(0);
                for (i = scheme.Series.Count - 1; i > -1; i--)
                {
                    series = area.Series[i];
                    if ((!series.IsEmpty) && (series.Visible))
                    {
                        colName = series.Name;
                        if (series.Unit.Length > 0)
                            colName = string.Format("{0}, {1}", colName, series.Unit);
                        col = table.Columns.Add(colName, Type.GetType("System.Double"));
                    }
                }
                x = 2;
                maxDT = DateTime.MinValue.Date;
                for (i = scheme.Series.Count - 1; i > -1; i--)
                {
                    series = area.Series[i];
                    if ((!series.IsEmpty) && (series.Visible))
                    {
                        if (mainForm.canv.IsGraphicsByOneDate)
                        {
                            dt = DateTime.FromOADate((int)series.Points[0].XValue);
                            Xvalue = (int)series.Points[series.Points.Count - 1].XValue - (int)series.Points[0].XValue;
                            //if (ParentChart.IntervalType == 0)
                            //{
                            //    maxDT = dt.AddYears(Xvalue - 1);
                            //}
                            //else if (ParentChart.IntervalType == 1)
                            //{
                            //    maxDT = dt.AddMonths(Xvalue - 1);
                            //}
                            //else
                            //{
                            //    maxDT = dt.AddDays(Xvalue);
                            //}
                            maxDT = dt.AddDays(Xvalue);
                        }
                        else
                        {
                            dt = DateTime.FromOADate((int)series.Points[0].XValue);
                            if ((scheme.Series[i].SourceType == Constant.Chart.DATA_SOURCE_TYPE.PROJ_PARAMS) ||
                                (scheme.Series[i].SourceType == Constant.Chart.DATA_SOURCE_TYPE.CHESS))
                            {
                                maxDT = DateTime.FromOADate((int)series.Points[series.Points.Count - 1].XValue);
                            }
                            else
                            {
                                maxDT = DateTime.FromOADate((int)series.Points[series.Points.Count - 1].XValue);
                                for (j = series.Points.Count - 1; j > 0; j--)
                                {
                                    if (!series.Points[j].IsEmpty)
                                    {
                                        maxDT = DateTime.FromOADate((int)series.Points[j].XValue);
                                        break;
                                    }
                                }
                            }
                            switch (ParentChart.IntervalType)
                            {
                                case 0:
                                    dt = new DateTime(dt.Year, 1, 1);
                                    maxDT = new DateTime(maxDT.Year, 1, 1);
                                    break;
                                case 1:
                                    dt = new DateTime(dt.Year, dt.Month, 1);
                                    maxDT = new DateTime(maxDT.Year, maxDT.Month, 1);
                                    break;
                            }
                        }
                        AddNewDataRows(dt, maxDT);
                    }
                }
                if (table.Rows.Count > 0) maxDT = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[1];
                for (i = scheme.Series.Count - 1; i > -1; i--)
                {
                    series = area.Series[i];
                    lastValue = -1;
                    if ((!series.IsEmpty) && (series.Visible))
                    {
                        //if (mainForm.canv.IsGraphicsByOneDate)
                        //{
                        //    dt = DateTime.FromOADate(0.0);
                        //}
                        //else
                        //{
                            dt = DateTime.FromOADate((int)series.Points[0].XValue);
                        //}
                        minDT = ((DateTime)table.Rows[0].ItemArray[1]).Date;
                        if (mainForm.canv.IsGraphicsByOneDate)
                        {
                            ind = (dt - minDT).Days;
                            indMax = (maxDT - minDT).Days + 1;
                        }
                        else
                        {
                            if (ParentChart.IntervalType == 0)
                            {
                                ind = dt.Year - minDT.Year;
                                indMax = maxDT.Year - minDT.Year + 1;
                            }
                            else if (ParentChart.IntervalType == 1)
                            {
                                ind = (dt.Year - minDT.Year) * 12 - minDT.Month + dt.Month;
                                indMax = (maxDT.Year - minDT.Year) * 12 - minDT.Month + maxDT.Month + 1;
                            }
                            else
                            {
                                ind = (dt - minDT).Days;
                                indMax = (maxDT - minDT).Days + 1;
                            }
                        }
                        for (j = 0; (j < series.Points.Count) && (ind < table.Rows.Count); j++)
                        {
                            currDt = DateTime.FromOADate((int)series.Points[j].XValue);
                            if (!mainForm.canv.IsGraphicsByOneDate)
                            {
                                switch (ParentChart.IntervalType)
                                {
                                    case 0:
                                        currDt = new DateTime(currDt.Year, 1, 1);
                                        break;
                                    case 1:
                                        currDt = new DateTime(currDt.Year, currDt.Month, 1);
                                        break;
                                }
                            }
                            Xvalue = (int)currDt.ToOADate();
                            while ((ind < table.Rows.Count) && (((DateTime)table.Rows[ind].ItemArray[1]).Date < currDt))
                            {
                                if (lastValue > -1 && series.ChartType == SeriesChartType.Point)
                                {
                                    table.Rows[ind][x] = lastValue;
                                }
                                else
                                {
                                    lastValue = -1;
                                }
                                table.Rows[ind][0] = Xvalue;
                                ind++;
                                Xvalue++;
                            }
                            if ((ind < table.Rows.Count) && (((DateTime)table.Rows[ind].ItemArray[1]).Date == currDt))
                            {
                                row = table.Rows[ind];
                                if (!series.Points[j].IsEmpty)
                                {
                                    lastValue = series.Points[j].YValue;
                                    if ((series.IsStackedSeries) && (series.Points[j].YValues != null))
                                    {
                                        lastValue = series.Points[j].YValues[0];
                                    }
                                    row[x] = lastValue;
                                    row[0] = (int)series.Points[j].XValue;
                                }
                                else if (lastValue != -1)
                                {
                                    row[x] = lastValue;
                                    row[0] = (int)series.Points[j].XValue;
                                }
                                ind++;
                            }
                        }
                        x++;
                    }
                }
            }
            this.DataSource = _dsGraph.Tables[0];
            if ((this.Rows.Count > 0) & (this.ClientRectangle.Height > 15))
            {
                this.FirstDisplayedScrollingRowIndex = this.Rows.Count - 1;
            }
            SetStyle();
             
        }
        public void SetStyle()
        {
            if (this.Columns.Count > 1)
            {
                Scheme scheme = null;
                if ((ParentChart.SchemeList.Length > 0) && (ParentChart.ActiveScheme < ParentChart.SchemeList.Length))
                {
                    scheme = ParentChart.SchemeList[ParentChart.ActiveScheme];
                }

                this.Columns[0].DefaultCellStyle = TableCellStyleAll;
                if (scheme.DateTimeIntervalTypeEnabled[2] == false)
                    this.Columns[1].DefaultCellStyle = TableCellStyleDateMER;
                else
                    this.Columns[1].DefaultCellStyle = TableCellStyleAll;
                int width = LoadColumnWidth(-1), index;
                if (width == -1)
                {
                    width = 90;
                    SaveColumnWidth(-1, 90);
                }
                this.Columns[1].Width = width;
                for (int i = 2; i < this.Columns.Count; i++)
                {
                    this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    index = scheme.Series.Count - i + 1;
                    width = LoadColumnWidth((int)scheme.Series[index].SeriesType);
                    if (width == -1)
                    {
                        width = 60;
                        SaveColumnWidth((int)scheme.Series[index].SeriesType, 60);
                    }
                    this.Columns[i].Width = width;
                    this.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
                this.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                this.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                if (mainForm.canv.IsGraphicsByOneDate)
                {
                    this.Columns[0].Visible = true;
                    this.Columns[1].Visible = false;
                }
                else
                {
                    this.Columns[0].Visible = false;
                    this.Columns[1].Visible = true;
                }
            }
        }
    }    
}
