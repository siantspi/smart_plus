﻿namespace SmartPlus
{
    partial class ChartAxisSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbMinValueFixed = new System.Windows.Forms.TextBox();
            this.bMinValueFromGraph = new System.Windows.Forms.Button();
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.bMaxValueFromGraph = new System.Windows.Forms.Button();
            this.tbMaxValueFixed = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pMinValue = new System.Windows.Forms.Panel();
            this.rbMinValueFixed = new System.Windows.Forms.RadioButton();
            this.rbMinValueAuto = new System.Windows.Forms.RadioButton();
            this.pMaxValue = new System.Windows.Forms.Panel();
            this.rbMaxValueFixed = new System.Windows.Forms.RadioButton();
            this.rbMaxValueAuto = new System.Windows.Forms.RadioButton();
            this.pMinValue.SuspendLayout();
            this.pMaxValue.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Минимальное значение:";
            // 
            // tbMinValueFixed
            // 
            this.tbMinValueFixed.Enabled = false;
            this.tbMinValueFixed.Location = new System.Drawing.Point(345, 14);
            this.tbMinValueFixed.Name = "tbMinValueFixed";
            this.tbMinValueFixed.Size = new System.Drawing.Size(112, 23);
            this.tbMinValueFixed.TabIndex = 3;
            // 
            // bMinValueFromGraph
            // 
            this.bMinValueFromGraph.Location = new System.Drawing.Point(463, 10);
            this.bMinValueFromGraph.Name = "bMinValueFromGraph";
            this.bMinValueFromGraph.Size = new System.Drawing.Size(80, 28);
            this.bMinValueFromGraph.TabIndex = 4;
            this.bMinValueFromGraph.Text = "С графика";
            this.bMinValueFromGraph.UseVisualStyleBackColor = true;
            this.bMinValueFromGraph.Click += new System.EventHandler(this.bMinValueFromGraph_Click);
            // 
            // bOk
            // 
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(357, 418);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 5;
            this.bOk.Text = "OK";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // bCancel
            // 
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(453, 418);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 6;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bMaxValueFromGraph
            // 
            this.bMaxValueFromGraph.Location = new System.Drawing.Point(463, 44);
            this.bMaxValueFromGraph.Name = "bMaxValueFromGraph";
            this.bMaxValueFromGraph.Size = new System.Drawing.Size(80, 28);
            this.bMaxValueFromGraph.TabIndex = 11;
            this.bMaxValueFromGraph.Text = "С графика";
            this.bMaxValueFromGraph.UseVisualStyleBackColor = true;
            this.bMaxValueFromGraph.Click += new System.EventHandler(this.bMaxValueFromGraph_Click);
            // 
            // tbMaxValueFixed
            // 
            this.tbMaxValueFixed.Enabled = false;
            this.tbMaxValueFixed.Location = new System.Drawing.Point(345, 46);
            this.tbMaxValueFixed.Name = "tbMaxValueFixed";
            this.tbMaxValueFixed.Size = new System.Drawing.Size(112, 23);
            this.tbMaxValueFixed.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Максимальное значение:";
            // 
            // pMinValue
            // 
            this.pMinValue.Controls.Add(this.rbMinValueFixed);
            this.pMinValue.Controls.Add(this.rbMinValueAuto);
            this.pMinValue.Location = new System.Drawing.Point(164, 12);
            this.pMinValue.Name = "pMinValue";
            this.pMinValue.Size = new System.Drawing.Size(181, 24);
            this.pMinValue.TabIndex = 12;
            // 
            // rbMinValueFixed
            // 
            this.rbMinValueFixed.AutoSize = true;
            this.rbMinValueFixed.Location = new System.Drawing.Point(59, 3);
            this.rbMinValueFixed.Name = "rbMinValueFixed";
            this.rbMinValueFixed.Size = new System.Drawing.Size(116, 19);
            this.rbMinValueFixed.TabIndex = 4;
            this.rbMinValueFixed.Text = "фиксированный";
            this.rbMinValueFixed.UseVisualStyleBackColor = true;
            this.rbMinValueFixed.CheckedChanged += new System.EventHandler(this.rbMinValueFixed_CheckedChanged);
            // 
            // rbMinValueAuto
            // 
            this.rbMinValueAuto.AutoSize = true;
            this.rbMinValueAuto.Checked = true;
            this.rbMinValueAuto.Location = new System.Drawing.Point(3, 3);
            this.rbMinValueAuto.Name = "rbMinValueAuto";
            this.rbMinValueAuto.Size = new System.Drawing.Size(50, 19);
            this.rbMinValueAuto.TabIndex = 3;
            this.rbMinValueAuto.TabStop = true;
            this.rbMinValueAuto.Text = "авто";
            this.rbMinValueAuto.UseVisualStyleBackColor = true;
            // 
            // pMaxValue
            // 
            this.pMaxValue.Controls.Add(this.rbMaxValueFixed);
            this.pMaxValue.Controls.Add(this.rbMaxValueAuto);
            this.pMaxValue.Location = new System.Drawing.Point(164, 44);
            this.pMaxValue.Name = "pMaxValue";
            this.pMaxValue.Size = new System.Drawing.Size(181, 24);
            this.pMaxValue.TabIndex = 13;
            // 
            // rbMaxValueFixed
            // 
            this.rbMaxValueFixed.AutoSize = true;
            this.rbMaxValueFixed.Location = new System.Drawing.Point(59, 3);
            this.rbMaxValueFixed.Name = "rbMaxValueFixed";
            this.rbMaxValueFixed.Size = new System.Drawing.Size(116, 19);
            this.rbMaxValueFixed.TabIndex = 4;
            this.rbMaxValueFixed.Text = "фиксированный";
            this.rbMaxValueFixed.UseVisualStyleBackColor = true;
            this.rbMaxValueFixed.CheckedChanged += new System.EventHandler(this.rbMaxValueFixed_CheckedChanged);
            // 
            // rbMaxValueAuto
            // 
            this.rbMaxValueAuto.AutoSize = true;
            this.rbMaxValueAuto.Checked = true;
            this.rbMaxValueAuto.Location = new System.Drawing.Point(3, 3);
            this.rbMaxValueAuto.Name = "rbMaxValueAuto";
            this.rbMaxValueAuto.Size = new System.Drawing.Size(50, 19);
            this.rbMaxValueAuto.TabIndex = 3;
            this.rbMaxValueAuto.TabStop = true;
            this.rbMaxValueAuto.Text = "авто";
            this.rbMaxValueAuto.UseVisualStyleBackColor = true;
            // 
            // ChartAxisSetting
            // 
            this.AcceptButton = this.bOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(551, 460);
            this.ControlBox = false;
            this.Controls.Add(this.pMaxValue);
            this.Controls.Add(this.pMinValue);
            this.Controls.Add(this.bMaxValueFromGraph);
            this.Controls.Add(this.tbMaxValueFixed);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.bMinValueFromGraph);
            this.Controls.Add(this.tbMinValueFixed);
            this.Controls.Add(this.label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChartAxisSetting";
            this.ShowInTaskbar = false;
            this.Text = "Настройка оси X";
            this.pMinValue.ResumeLayout(false);
            this.pMinValue.PerformLayout();
            this.pMaxValue.ResumeLayout(false);
            this.pMaxValue.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbMinValueFixed;
        private System.Windows.Forms.Button bMinValueFromGraph;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bMaxValueFromGraph;
        private System.Windows.Forms.TextBox tbMaxValueFixed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pMinValue;
        private System.Windows.Forms.RadioButton rbMinValueFixed;
        private System.Windows.Forms.RadioButton rbMinValueAuto;
        private System.Windows.Forms.Panel pMaxValue;
        private System.Windows.Forms.RadioButton rbMaxValueFixed;
        private System.Windows.Forms.RadioButton rbMaxValueAuto;
    }
}