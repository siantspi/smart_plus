﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using SmartPlus;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.IO;

namespace SmartPlus.DataChartEx
{
    public delegate void OnChartSnapShootDelegate(Rectangle SnapRect);

    public sealed class DataChartEx : PictureBox
    {
        Bitmap bmp; 
        public ChartAreaCollection ChartAreas;
        Graphics bmp_grfx;
        int ChartAreaOver;
        Point MDown;
        int ChartAreaDown;
        bool LeftBtnPressed;
        public event OnChartSnapShootDelegate OnChartSnapShoot;

        public bool EnableToolTip { get; set; }

        #region Timer
        Point TmrPt;
        Timer tmrDraw;
        #endregion

        #region TOOLTIP
        ChartToolTip ToolTip, ToolTipLabels;
        Font FontToolTip, FontToolTipAxis;
        public string GetTooltipTitle()
        {
            string str = "";
            if (ToolTip != null)
            {
                switch (ToolTip.Type)
                {
                    case 0:
                        str = ToolTip.series.Name;
                        if (ToolTip.series.Unit != "") str += ", " + ToolTip.series.Unit;
                        break;
                    case 1:
                        str = ToolTip.Text;
                        break;
                }
            }
            return str;
        }
        #endregion

        #region ZoomMode
        int ZoomChartArea;
        int ZoomAll;
        Point Zoom1, Zoom2;
        Brush brZoom, brZoomAll;

        public void ZoomChartAreaAxes()
        {
            if (ZoomChartArea > -1)
            {
                float minX, minY, maxX, maxY;
                if (Zoom2.X > Zoom1.X)
                {
                    minX = Zoom1.X;
                    maxX = Zoom2.X;
                }
                else
                {
                    minX = Zoom2.X;
                    maxX = Zoom1.X;
                }
                if (Zoom2.Y > Zoom1.Y)
                {
                    minY = Zoom1.Y;
                    maxY = Zoom2.Y;
                }
                else
                {
                    minY = Zoom2.Y;
                    maxY = Zoom1.Y;
                }
                ChartArea area = ChartAreas[ZoomChartArea];
                if ((maxX - minX < 10) && (maxY - minY < 10)) return;

                if (minX < area.InnerPlotPosition.Left) minX = area.InnerPlotPosition.Left;
                if (maxX > area.InnerPlotPosition.Right) maxX = area.InnerPlotPosition.Right;
                if (minY < area.InnerPlotPosition.Top) minY = area.InnerPlotPosition.Top;
                if (maxY > area.InnerPlotPosition.Bottom) maxY = area.InnerPlotPosition.Bottom;

                if (ZoomAll == 0)
                {
                    minY = area.InnerPlotPosition.Top;
                    maxY = area.InnerPlotPosition.Bottom;
                }
                else if (ZoomAll == 1)
                {
                    minX = area.InnerPlotPosition.Left;
                    maxX = area.InnerPlotPosition.Right;
                }
                area.Zoom(minX, minY, maxX, maxY);
            }
        }
        public void ZoomReset(int numberOfViews)
        {
            ChartAreas[ChartAreaOver].ZoomReset(numberOfViews);
            DrawChartArea(ChartAreaOver);
        }
        #endregion
        
        #region SnapShoot Mode
        Point Snap1, Snap2;
        bool snapShootMode = false;
        public bool SnapShootMode
        {
            get { return snapShootMode; }
            set
            {
                Snap1 = Point.Empty;
                snapShootMode = value;
                if (value) this.Cursor = Cursors.Cross; else this.Cursor = Cursors.Default;
            }
        }
        public void SnapToClipboard()
        {
            if ((snapShootMode) && (!Snap1.IsEmpty))
            {
                int minX, minY, maxX, maxY;
                if (Snap2.X > Snap1.X)
                {
                    minX = Snap1.X;
                    maxX = (int)Snap2.X;
                }
                else
                {
                    minX = (int)Snap2.X;
                    maxX = Snap1.X;
                }
                if (Snap2.Y > Snap1.Y)
                {
                    minY = Snap1.Y;
                    maxY = (int)Snap2.Y;
                }
                else
                {
                    minY = (int)Snap2.Y;
                    maxY = Snap1.Y;
                }

                if (minX < this.ClientRectangle.Left) minX = this.ClientRectangle.Left;
                if (maxX > this.ClientRectangle.Right) maxX = this.ClientRectangle.Right;
                if (minY < this.ClientRectangle.Top) minY = this.ClientRectangle.Top;
                if (maxY > this.ClientRectangle.Bottom) maxY = this.ClientRectangle.Bottom;
                Rectangle rect = new Rectangle(minX, minY, (maxX - minX), (maxY - minY));
                if ((rect.Width < 20) || (rect.Height < 20))
                {
                    rect = this.ClientRectangle;
                    rect.X++;
                    rect.Y++;
                    rect.Width -= 2;
                    rect.Height -= 2;
                }
                if (OnChartSnapShoot != null) OnChartSnapShoot(rect);
                //Bitmap snap = new Bitmap(rect.Width, rect.Height);
                //Graphics grfx = Graphics.FromImage(snap);
                //grfx.DrawImage(bmp, 0, 0, rect, GraphicsUnit.Pixel);
                //Clipboard.SetImage(snap);
                //grfx.Dispose();
                //snap.Dispose();
            }
            SnapShootMode = false;
            this.Invalidate();
        }
        #endregion

        public DataChartEx(int MaxWidth, int MaxHeight)
        {
            ChartAreas = new ChartAreaCollection(2);
            bmp = new Bitmap(MaxWidth, MaxHeight);
            bmp_grfx = Graphics.FromImage(bmp);
            ChartAreaOver = -1;
            MDown = Point.Empty;
            ChartAreaDown = -1;
            LeftBtnPressed = false;


            #region TIMER
            TmrPt = Point.Empty;
            tmrDraw = new Timer();
            tmrDraw.Enabled = false;
            tmrDraw.Interval = 50;
            tmrDraw.Tick += new EventHandler(tmrDraw_Tick);
            #endregion

            #region TOOLTIP
            EnableToolTip = true;
            this.ToolTip = null;
            this.FontToolTip = new Font("Tahoma", 8.25f, FontStyle.Bold);
            this.FontToolTipAxis = new Font("Calibri", 8.25f);
            #endregion

            #region ZOOM
            ZoomChartArea = -1;
            ZoomAll = -1;
            Zoom1 = Point.Empty;
            Zoom2 = Point.Empty;
            brZoom = new SolidBrush(Color.FromArgb(75, Color.Gray));
            brZoomAll = new SolidBrush(Color.FromArgb(50, Color.Gray));
            #endregion

            this.AddChartArea();

            this.MouseDown += new MouseEventHandler(DataChartEx_MouseDown);
            this.MouseUp += new MouseEventHandler(DataChartEx_MouseUp);
            this.MouseWheel += new MouseEventHandler(DataChartEx_MouseWheel);
            this.MouseMove += new MouseEventHandler(DataChartEx_MouseMove);
            this.MouseLeave += new EventHandler(DataChartEx_MouseLeave);
            this.Resize += new EventHandler(DataChartEx_Resize);
            this.Paint += new PaintEventHandler(DataChartEx_Paint);
            this.Paint += new PaintEventHandler(ToolTip_Paint);
        }

        #region CHART AREAS
        public int AddChartArea()
        {
            int ind = ChartAreas.Add();
            ChartAreas[ind].SetGraphics(bmp_grfx);
            SetChartTiles();
            return ind;
        }

        public ChartArea this[int index]
        {
            get
            {
                if ((index > -1) && (index < ChartAreas.Count))
                {
                    return ChartAreas[index];
                }
                else
                    return null;
            }
        }
        public int GetAreaByCursor()
        {
            Point pt = this.PointToClient(Cursor.Position);
            for (int i = 0; i < ChartAreas.Count; i++)
            {
                if(ChartAreas[i].Position.Contains(pt.X, pt.Y))
                {
                    return i;
                }
            }
            return -1;
        }
        public void SetChartTiles()
        {
            if (ChartAreas.Count > 0)
            {
                ChartArea chart;
                RectangleF pos = RectangleF.Empty;
                pos.Width = this.ClientRectangle.Width;
                pos.Height = this.ClientRectangle.Height;
                pos.Width--;
                pos.Height--;
                int nx = 1, ny = 1;
                float width, height;
                if (ChartAreas.Count == 1)
                {
                    chart = this[0];
                    chart.Position = pos;
                }
                else
                {
                    if (ChartAreas.Count < 5)
                    {
                        nx = 2;
                        if (ChartAreas.Count > 2) ny = 2;
                    }
                    else
                    {
                        nx = 3;
                        ny = 2;
                    }
                    height = pos.Height / ny;
                    width = pos.Width / nx;
                    pos.Width = width;
                    pos.Height = height;
                    for (int i = 0, j = 0; i < ChartAreas.Count; i++, j++)
                    {
                        if (j >= nx) j = 0;
                        pos.X = j * width;
                        pos.Y = ((int)(i / nx)) * height;
                        chart = this[i];
                        chart.Position = pos;
                    }
                }
            }
        }
        #endregion

        #region EVENTS
        void DataChartEx_MouseDown(object sender, MouseEventArgs e)
        {
            if (snapShootMode)
            {
                Snap1 = e.Location;
                Snap2 = Snap1;
                if (e.Button == MouseButtons.Left) LeftBtnPressed = true;
            }
            else if (ChartAreaOver > -1)
            {
                ChartLegend legend = ChartAreas[ChartAreaOver].Legend;
                if ((legend != null) && (legend.Enabled) && (legend.Location.Contains(e.Location)))
                {
                    int ind = legend.GetItemIndex(e.Location);
                    if (ind > -1) 
                    {
                        if ((legend[ind].Location.X + 2 < e.X) && (e.X < legend[ind].Location.X + legend[ind].Location.Width))
                        {
                            ChartAreas[ChartAreaOver].ChangeLegendItem(ind);
                            DrawChartArea(ChartAreaOver);
                        }
                    }
                }
                else
                {
                    if ((e.Button == MouseButtons.Right) && (ChartAreas[ChartAreaOver].Position.Contains(e.Location)))
                    {
                        ZoomReset(1);
                    }
                    else if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
                    {
                        ZoomChartArea = ChartAreaOver;
                        Zoom1 = e.Location;
                        Zoom2 = e.Location;
                    }
                    else if (ChartAreas[ChartAreaOver].InnerPlotPosition.Contains(e.Location))
                    {
                        TmrPt = e.Location;
                        tmrDraw.Enabled = true;
                    }
                    ChartAreaDown = ChartAreaOver;
                    MDown = e.Location;
                    if (e.Button == MouseButtons.Left) LeftBtnPressed = true;
                }
            }
        }
        void DataChartEx_MouseUp(object sender, MouseEventArgs e)
        {
            ToolTip = null;
            if (snapShootMode)
            {
                SnapToClipboard();
            }
            if (ZoomChartArea > -1)
            {
                ZoomChartAreaAxes();
                DrawChartArea(ZoomChartArea);
                ZoomChartArea = -1;
                ZoomAll = -1;
            }
            if (LeftBtnPressed) tmrDraw.Enabled = false;
            ChartAreaDown = -1;
            LeftBtnPressed = false;
        }
        void DataChartEx_MouseWheel(object sender, MouseEventArgs e)
        {
            if ((ChartAreaOver > -1) && (ChartAreas[ChartAreaOver].InnerPlotPosition.Contains(e.Location)))
            {
                int deltaK = (int)Math.Abs(e.Delta / 120);
                bool res, reDraw = false;
                if (deltaK > 3) deltaK = 3;
                bool ShiftPressed = (ModifierKeys == Keys.Shift);
                while (deltaK > 0)
                {
                    if (e.Delta > 0)
                    {
                        res = ChartAreas[ChartAreaOver].ScaleAxis(1.3F, e.X, e.Y, ShiftPressed);
                        reDraw = true;
                    }
                    else
                    {
                        res = ChartAreas[ChartAreaOver].ScaleAxis(1.0F / 1.3F, e.X, e.Y, ShiftPressed);
                        reDraw = true;
                    }
                    if (!res) break;
                    deltaK--;
                }
                if (reDraw)
                {
                    ToolTip = null;
                    DrawChartArea(ChartAreaOver);
                    this.Invalidate();
                }
            }
        }
        void DataChartEx_MouseLeave(object sender, EventArgs e)
        {
            tmrDraw.Enabled = false;
            ChartAreaOver = -1;
            ToolTip = null;
            this.Cursor = Cursors.Default;
            ChartAreas[0].Legend.SetSelectedSeries(null);
            this.DrawChartAreaLegends();
            this.Invalidate();
        }
        void DataChartEx_MouseMove(object sender, MouseEventArgs e)
        {
            bool reDraw = false;
            bool reDrawLegend = false;
            int ind = GetAreaByCursor();
            if (ChartAreaOver != ind)
            {
                ChartAreaOver = ind;
                reDraw = true;
            }
            if (snapShootMode)
            {
                if (this.Cursor == Cursors.Default) this.Cursor = Cursors.Cross;
                if (LeftBtnPressed)
                {
                    Snap2 = e.Location;
                    reDraw = true;
                }
            }
            if ((ZoomChartArea > -1) && ((Control.ModifierKeys & Keys.Control) == Keys.Control))
            {
                Zoom2 = e.Location;
                ZoomAll = -1;
                if (Math.Abs(Zoom2.Y - Zoom1.Y) < 15)
                {
                    ZoomAll = 0;
                }
                else if (Math.Abs(Zoom2.X - Zoom1.X) < 15)
                {
                    ZoomAll = 1;
                }
                reDraw = true;
            }
            else if (e.Button == MouseButtons.Left)
            {
                if ((ChartAreaOver > -1) && (ChartAreas[ChartAreaOver].InnerPlotPosition.Contains(e.Location)))
                {
                    TmrPt = e.Location;
                }
            }
            else
            {
                if (ToolTip != null)
                {
                    if (ToolTip.series != null)
                    {
                        ChartAreas[0].Legend.SetSelectedSeries(null);
                        reDrawLegend = true;
                    }
                    reDraw = true;
                }

                ToolTip = null;
                
                if (ind != -1)
                {
                    ToolTip = ChartAreas[ind].GetSeriesToolTip(e.X, e.Y);
                    ToolTipLabels = ChartAreas[ind].GetChartLabelsToolTip(e.X, e.Y);
                    if (ToolTipLabels == null) ToolTipLabels = ChartAreas[ind].GetColorZonesToolTip(e.X, e.Y);

                    if ((ToolTipLabels != null) && (((Control.ModifierKeys & Keys.Shift) == Keys.Shift) || (ToolTip == null)))
                    {
                        ToolTip = ToolTipLabels;
                    }
                    if (ToolTip == null) ToolTip = ChartAreas[ind].GetStackedAreaNameToolTip(e.X, e.Y);
                }
                if (ToolTip != null)
                {
                    if (ToolTip.series != null)
                    {
                        ChartAreas[0].Legend.SetSelectedSeries(ToolTip.series);
                        reDrawLegend = true;
                    }
                    reDraw = true;
                }
            }
            if (reDrawLegend) this.DrawChartAreaLegends();
            if (reDraw) this.Invalidate();
        }
        
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Control) this.Cursor = Cursors.Cross;
            if (e.KeyCode == Keys.Escape) SnapShootMode = false;
            base.OnKeyDown(e);
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (ZoomChartArea > -1) this.Invalidate();
            ZoomChartArea = -1;
            Cursor = Cursors.Default;
            base.OnKeyUp(e);
        }

        void DataChartEx_Resize(object sender, EventArgs e)
        {
            SetChartTiles();
            DrawChartAreas();
        }
        #endregion

        #region DRAWING
        void tmrDraw_Tick(object sender, EventArgs e)
        {
            if ((LeftBtnPressed) && (ChartAreaDown == ChartAreaOver) && ((Math.Abs(MDown.X - TmrPt.X) > 2) || (Math.Abs(MDown.Y - TmrPt.Y) > 2)))
            {
                ToolTip = null;
                if (ChartAreas[ChartAreaOver].InnerPlotPosition.Contains(TmrPt))
                {
                    ChartAreas[ChartAreaOver].ScroolAxis(MDown, TmrPt);
                    DrawChartArea(ChartAreaOver);
                    MDown = TmrPt;
                }
            }
        }
        public void ClearBitMap()
        {
            bmp_grfx.Clear(Color.White);
            this.Invalidate();
        }
        public void DrawChartArea(int ChartAreaInd)
        {
            if (ChartAreaInd > -1)
            {
                bmp_grfx.FillRectangle(Brushes.White, ChartAreas[ChartAreaInd].Position);
                ChartAreas[ChartAreaInd].DrawObject(bmp_grfx);
                this.Invalidate();
            }
        }
        public void DrawChartAreas()
        {
            bmp_grfx.Clear(Color.White);
            for (int i = 0; i < ChartAreas.Count; i++)
            {
                Rectangle rect = new Rectangle((int)this[i].InnerPlotPosition.X, (int)this[i].InnerPlotPosition.Y, (int)this[i].InnerPlotPosition.Width, (int)this[i].InnerPlotPosition.Height);
                if (this.ClientRectangle.IntersectsWith(rect))
                {
                    this[i].DrawObject(bmp_grfx);
                }
            }
            this.Invalidate();
        }
        public void DrawChartAreaLegends()
        {
            for (int i = 0; i < ChartAreas.Count; i++)
            {
                if (this[i].Enabled)
                {
                    this[i].Legend.DrawObject(bmp_grfx);
                }
            }
        }
        public void DrawChartAreas(Graphics grfx)
        {
            int i;
            grfx.Clear(Color.White);
            for (i = 0; i < ChartAreas.Count; i++)
            {
                Rectangle rect = new Rectangle((int)this[i].InnerPlotPosition.X, (int)this[i].InnerPlotPosition.Y, (int)this[i].InnerPlotPosition.Width, (int)this[i].InnerPlotPosition.Height);
                if (this.ClientRectangle.IntersectsWith(rect))
                {
                    this[i].DrawObject(grfx);
                }
            }
        }
        public void DrawToolTip(Graphics grfx)
        {
            if ((EnableToolTip) && (ToolTip != null))
            {
                string str = "";
                Color clr = Color.FromArgb(ToolTip.Color);
                str = ToolTip.Text;
                switch (ToolTip.Type)
                {
                    case 0:
                        if (ToolTip.series.ChartType == SeriesChartType.Point) clr = ToolTip.series.Marker.Color;
                        break;
                    case 1:
                        str = ToolTip.Text;
                        int lastPos = str.LastIndexOf('\n');
                        if (str.Length - lastPos > 80)
                        {
                            int pos = str.IndexOf(' ', lastPos + 60);
                            while (pos > 0)
                            {
                                str = str.Remove(pos, str.Length - pos) + "\n" + str.Remove(0, pos + 1);
                                if (pos + 60 >= str.Length) break;
                                pos = str.IndexOf(' ', pos + 60);
                            }
                        }
                        break;
                }
                SizeF sz = grfx.MeasureString(str, FontToolTip, PointF.Empty, StringFormat.GenericTypographic);
                using (Brush brush = new SolidBrush(clr))
                {
                    float x = ToolTip.Point.X - sz.Width / 2;
                    float y = ToolTip.Point.Y - sz.Height - 11f;

                    if (x < 0) x = 0; else if (x + sz.Width > this.ClientRectangle.Right) x = this.ClientRectangle.Right - sz.Width;
                    if (y < 0) y = 0; else if (y + sz.Height > this.ClientRectangle.Bottom) y = this.ClientRectangle.Bottom - sz.Height;

                    if (ToolTip.Type != 2)
                    {
                        grfx.FillEllipse(brush, ToolTip.Point.X - 4, ToolTip.Point.Y - 4, 8, 8);
                        grfx.DrawEllipse(Pens.Black, ToolTip.Point.X - 4, ToolTip.Point.Y - 4, 8, 8);
                    }

                    grfx.DrawString(str, FontToolTip, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                    grfx.DrawString(str, FontToolTip, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                    grfx.DrawString(str, FontToolTip, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                    grfx.DrawString(str, FontToolTip, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);

                    grfx.DrawString(str, FontToolTip, Brushes.Black, x, y, StringFormat.GenericTypographic);

                    float scrX, scrY, dy = ToolTip.AxisTip.MajorTickMark.OutsideSize;
                    if (dy < ToolTip.AxisTip.MinorTickmark.OutsideSize) dy = ToolTip.AxisTip.MinorTickmark.OutsideSize;

                    if (ToolTip.Type != 2)
                    {
                        #region FORMAT
                        string format = "";
                        switch (ToolTip.AxisTip.ValueType)
                        {
                            case ChartValueType.DateTime:
                                switch (ToolTip.AxisTip.IntervalType)
                                {
                                    case DateTimeIntervalType.Auto:
                                        format = "dd.MM.yyyy";
                                        break;
                                    case DateTimeIntervalType.Years:
                                        format = "yyyy";
                                        break;
                                    case DateTimeIntervalType.Months:
                                        format = "MM.yyyy";
                                        break;
                                    case DateTimeIntervalType.Days:
                                        format = "dd.MM.yyyy";
                                        break;
                                    case DateTimeIntervalType.Hours:
                                        format = "HH";
                                        break;
                                    case DateTimeIntervalType.Minutes:
                                        format = "HH:mm";
                                        break;
                                    case DateTimeIntervalType.Seconds:
                                        format = "HH:mm:ss";
                                        break;
                                }
                                str = DateTime.FromOADate(ToolTip.XValue).ToString(format);
                                break;
                            case ChartValueType.Double:
                                format = "0";
                                str = ToolTip.XValue.ToString(format);
                                break;
                            case ChartValueType.Int32:
                                format = "0";
                                str = ToolTip.XValue.ToString(format);
                                break;
                        }
                        #endregion

                        sz = grfx.MeasureString(str, FontToolTipAxis, PointF.Empty, StringFormat.GenericTypographic);
                        sz.Width += 2;

                        scrX = ToolTip.Point.X - sz.Width / 2;
                        scrY = ToolTip.AxisTip.Start.Y;
                        switch (ToolTip.AxisTip.Position)
                        {
                            case ChartPosition.Top:
                                scrY = ToolTip.AxisTip.Start.Y - dy - sz.Height;
                                break;
                            case ChartPosition.Bottom:
                                scrY = ToolTip.AxisTip.Start.Y + dy;
                                break;
                        }
                        grfx.FillRectangle(Brushes.White, scrX, scrY, sz.Width, sz.Height - 2);
                        grfx.DrawRectangle(Pens.Black, scrX, scrY, sz.Width, sz.Height - 2);
                        grfx.DrawString(str, FontToolTipAxis, Brushes.Black, scrX + 1, scrY, StringFormat.GenericTypographic);
                    }
                }
            }
        }
        void DataChartEx_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            grfx.DrawImageUnscaledAndClipped(bmp, this.ClientRectangle);
            DrawChartAreaRectangle(grfx);
            if (ModifierKeys == Keys.Shift)
            {
                for (int i = 0; i < ChartAreas.Count; i++)
                {
                    this[i].DrawChartAreaLabels(grfx);
                    this[i].DrawChartAxisLabels(grfx);
                }
            }
            DrawToolTip(grfx);
            DrawZoomMode(grfx);
            DrawSnapShootMode(grfx);
        }
        void ToolTip_Paint(object sender, PaintEventArgs e)
        {
            DrawToolTip(e.Graphics);
        }
        void DrawChartAreaRectangle(Graphics grfx)
        {
            if ((ChartAreaOver != -1) && (ChartAreas.Count > 1))
            {
                ChartArea area = ChartAreas[ChartAreaOver];
                grfx.DrawRectangle(Pens.Red, area.Position.X, area.Position.Y, area.Position.Width, area.Position.Height);
            }
        }
        void DrawZoomMode(Graphics grfx)
        {
            if ((ZoomChartArea > -1) && (!Zoom1.IsEmpty) && (!Zoom2.IsEmpty))
            {
                float minX, minY, maxX, maxY;
                if (Zoom2.X > Zoom1.X)
                {
                    minX = Zoom1.X;
                    maxX = Zoom2.X;
                }
                else
                {
                    minX = Zoom2.X;
                    maxX = Zoom1.X;
                }
                if (Zoom2.Y > Zoom1.Y)
                {
                    minY = Zoom1.Y;
                    maxY = Zoom2.Y;
                }
                else
                {
                    minY = Zoom2.Y;
                    maxY = Zoom1.Y;
                }
                ChartArea area = ChartAreas[ZoomChartArea];

                if (minX < area.InnerPlotPosition.Left) minX = area.InnerPlotPosition.Left;
                if (maxX > area.InnerPlotPosition.Right) maxX = area.InnerPlotPosition.Right;
                if (minY < area.InnerPlotPosition.Top) minY = area.InnerPlotPosition.Top;
                if (maxY > area.InnerPlotPosition.Bottom) maxY = area.InnerPlotPosition.Bottom;

                if (ZoomAll == 0)
                {
                    grfx.FillRectangle(brZoom, minX, area.InnerPlotPosition.Top, maxX - minX, area.InnerPlotPosition.Height);
                    grfx.DrawRectangle(Pens.Black, minX, area.InnerPlotPosition.Top, maxX - minX, area.InnerPlotPosition.Height);
                }
                else if (ZoomAll == 1)
                {
                    grfx.FillRectangle(brZoom, area.InnerPlotPosition.Left, minY, area.InnerPlotPosition.Width, maxY - minY);
                    grfx.DrawRectangle(Pens.Black, area.InnerPlotPosition.Left, minY, area.InnerPlotPosition.Width, maxY - minY);
                }
                else
                {
                    grfx.FillRectangle(brZoom, minX, minY, maxX - minX, maxY - minY);
                    grfx.DrawRectangle(Pens.Black, minX, minY, maxX - minX, maxY - minY);
                }
            }
        }
        void DrawSnapShootMode(Graphics grfx)
        {
            if ((!Snap1.IsEmpty) && (!Snap2.IsEmpty))
            {
                float minX, minY, maxX, maxY;
                if (Snap2.X > Snap1.X)
                {
                    minX = Snap1.X;
                    maxX = Snap2.X;
                }
                else
                {
                    minX = Snap2.X;
                    maxX = Snap1.X;
                }
                if (Snap2.Y > Snap1.Y)
                {
                    minY = Snap1.Y;
                    maxY = Snap2.Y;
                }
                else
                {
                    minY = Snap2.Y;
                    maxY = Snap1.Y;
                }

                if (minX < this.ClientRectangle.Left) minX = this.ClientRectangle.Left;
                if (maxX > this.ClientRectangle.Right) maxX = this.ClientRectangle.Right;
                if (minY < this.ClientRectangle.Top) minY = this.ClientRectangle.Top;
                if (maxY > this.ClientRectangle.Bottom) maxY = this.ClientRectangle.Bottom;

                grfx.FillRectangle(brZoom, minX, minY, maxX - minX, maxY - minY);
                grfx.DrawRectangle(Pens.Black, minX, minY, maxX - minX, maxY - minY);
            }
        }
        #endregion
    }

    #region основные классы

    public delegate void ChartAreaScaleChangeDelegate();
    public delegate void ChartLegendItemChangeDelegate();

    public sealed class ChartArea
    {
        public ChartAxis[] AxisY, AxisY2;
        public ChartAxis AxisX, AxisX2;
        public ChartSeriesCollection Series;
        public List<SeriesGroup> Groups;
        public ChartLegend Legend;
        Graphics grfx;
        double AxisXMinimum;

        public bool Enabled;
        public int Index;
        public Color BackColor;
        public Color BorderColor;
        public DashStyle BorderDashStyle;
        public int BorderWidth;
        RectangleF pos;
        public RectangleF Position
        {
            get { return pos; }
            set
            {
                pos = value;
                SetInnerPlotSize();
            }
        }
        public RectangleF InnerPlotPosition;
        public bool Visible;

        #region MY EVENTS
        public event ChartAreaScaleChangeDelegate ChartScaleChange;
        public event ChartLegendItemChangeDelegate ChartLegendItemChange;
      
        #endregion

        public ChartArea()
        {
            this.Enabled = true;

            #region LEGEND
            Legend = new ChartLegend();
            #endregion

            #region Axes
            this.AxisXMinimum = Constant.DOUBLE_NAN;
            this.AxisX = new ChartAxis("");
            this.AxisX.Position = ChartPosition.Bottom;
            this.AxisX2 = new ChartAxis("");
            this.AxisX2.Position = ChartPosition.Top;
            
            this.AddAxisY();
            this.AddAxisY2();
            #endregion

            #region SERIES
            Series = new ChartSeriesCollection();
            Groups = new List<SeriesGroup>();
            #endregion

        }

        void SetInnerPlotSize()
        {
            int i;
            float left = 0, right = 0, top = 0, bottom = 0;
            if ((AxisX != null) && (AxisX.Enabled)) bottom = AxisX.GetOutsideSize() + AxisX.TitleSize.Height;
            if ((AxisX2 != null) && (AxisX2.Enabled)) top = AxisX2.GetOutsideSize() + AxisX2.TitleSize.Height;
            PointF pt = PointF.Empty;

            if (Legend.Enabled)
            {
                Legend.SetLegendTiles(Position.Width, Position.Height);
                switch (Legend.Position)
                {
                    case ChartPosition.Top:
                        if (top == 0) top += 2;
                        top += Legend.Location.Height;
                        break;
                    case ChartPosition.Bottom:
                        if (bottom == 0) bottom += 2;
                        bottom += Legend.Location.Height;
                        break;
                    case ChartPosition.Left:
                        left += Legend.Location.Width + 2;
                        break;
                    case ChartPosition.Right:
                        right += Legend.Location.Width + 2;
                        break;
                }
            }
            float lastInside = 0;
            if (AxisY != null)
            {
                for (i = AxisY.Length - 1; i >= 0; i--)
                {
                    if (AxisY[i].Enabled)
                    {
                        left += lastInside + AxisY[i].GetOutsideSize() + AxisY[i].TitleSize.Height;
                        pt.X = pos.Left + left;
                        pt.Y = pos.Top + top;
                        AxisY[i].End = pt;
                        pt.X = pos.Left + left;
                        pt.Y = pos.Bottom - bottom;
                        AxisY[i].Start = pt;
                        if (AxisY[i].IsReversed)
                        {
                            AxisY[i].Start = AxisY[i].End;
                            AxisY[i].End = pt;
                        }

                        if (AxisY[i].Enabled) lastInside = AxisY[i].GetInsideSize();
                    }
                }
            }
            lastInside = 0;
            if (AxisY2 != null)
            {
                for (i = AxisY2.Length - 1; i >= 0; i--)
                {
                    if (AxisY2[i].Enabled)
                    {
                        right += lastInside + AxisY2[i].GetOutsideSize() +  AxisY2[i].TitleSize.Height;
                        pt.X = pos.Left + pos.Width - right;
                        pt.Y = pos.Top + top;
                        AxisY2[i].End = pt;
                        pt.X = AxisY2[i].End.X;
                        pt.Y = pos.Bottom - bottom;
                        AxisY2[i].Start = pt;
                        if (AxisY2[i].IsReversed)
                        {
                            AxisY2[i].Start = AxisY2[i].End;
                            AxisY2[i].End = pt;
                        }
                        if (AxisY2[i].Enabled) lastInside = AxisY2[i].GetInsideSize();
                    }
                }
            }
            if ((AxisX != null) && (AxisX.Enabled))
            {
                pt.X = pos.Left + left;
                pt.Y = pos.Bottom - bottom;
                AxisX.Start = pt;
                pt.X = pos.Right - right;
                pt.Y = pos.Bottom - bottom;
                AxisX.End = pt;
                if (AxisX.IsReversed)
                {
                    AxisX.End = AxisX.Start;
                    AxisX.Start = pt;
                }
            }
            if ((AxisX2 != null) && (AxisX2.Enabled))
            {
                pt.X = pos.Left + left;
                pt.Y = pos.Top + top;
                AxisX2.Start = pt;
                pt.X = pos.Right - right;
                pt.Y = pos.Top + top;
                AxisX2.End = pt;
                if (AxisX2.IsReversed)
                {
                    AxisX2.End = AxisX2.Start;
                    AxisX2.Start = pt;
                }
            }
            if (Legend.Enabled)
            {
                switch (Legend.Position)
                {
                    case ChartPosition.Top:
                        Legend.SetHotSpot(pos.Left + pos.Width / 2 - Legend.Location.Width / 2, pos.Top);
                        break;
                    case ChartPosition.Bottom:
                        Legend.SetHotSpot(pos.Left + pos.Width / 2 - Legend.Location.Width / 2, pos.Bottom - Legend.Location.Height);
                        break;
                    case ChartPosition.Left:
                        Legend.SetHotSpot(pos.Left, pos.Top + pos.Height / 2 - Legend.Location.Height / 2);
                        break;
                    case ChartPosition.Right:
                        Legend.SetHotSpot(pos.Right - Legend.Location.Width, pos.Top + pos.Height / 2 - Legend.Location.Height / 2);
                        break;
                }
            }

            RectangleF inner = RectangleF.Empty;
            inner.X = pos.Left + left;
            inner.Y = pos.Top + top;
            inner.Width = pos.Width - left - right;
            inner.Height = pos.Height - top - bottom;
            InnerPlotPosition = inner;
        }
        public void ReCalcChartAreaSizes()
        {
            for (int i = 0; i < Series.Count; i++)
            {
                Series[i].SetAxisMinMax();
            }
            ResetAxisLabelSizes();
            SetInnerPlotSize();
        }

        #region LEGEND
        public void ChangeLegendItem(int index)
        {
            ChartLegendItem item = Legend[index];
            item.Visible = !item.Visible;
            Legend[index] = item;
            ReCalcStackedSeriesValues();
            UpdateAxesMinMax();
            if (ChartLegendItemChange != null) ChartLegendItemChange();
        }
        #endregion

        #region AXIS FUNC
        public void AddAxisY()
        {
            if (AxisY == null)
            {
                AxisY = new ChartAxis[1];
                AxisY[0] = new ChartAxis("");
                AxisY[0].Position = ChartPosition.Left;
                AxisY[0].Index = 0;
                AxisY[0].MinorGrid.FixedCount = 10;
                AxisY[0].MajorGrid.FixedCount = 10;
                AxisY[0].MinorTickmark.FixedCount = 10;
                AxisY[0].MajorTickMark.FixedCount = 10;
                AxisY[0].LabelStyle.FixedCount = 10;
            }
            else
            {
                ChartAxis[] NewAxisY = new ChartAxis[AxisY.Length + 1];
                for (int i = 0; i < AxisY.Length; i++)
                {
                    NewAxisY[i] = AxisY[i];
                    NewAxisY[i].Index = i;
                }
                NewAxisY[AxisY.Length] = new ChartAxis("");
                NewAxisY[AxisY.Length].SetGraphics(grfx);
                NewAxisY[AxisY.Length].Position = ChartPosition.Left;
                NewAxisY[AxisY.Length].Index = AxisY.Length;
                NewAxisY[AxisY.Length].MinorGrid.FixedCount = 10;
                NewAxisY[AxisY.Length].MajorGrid.FixedCount = 10;
                NewAxisY[AxisY.Length].MinorTickmark.FixedCount = 10;
                NewAxisY[AxisY.Length].MajorTickMark.FixedCount = 10;
                NewAxisY[AxisY.Length].LabelStyle.FixedCount = 10;
                AxisY = NewAxisY;
            }
            SetInnerPlotSize();
        }
        public void AddAxisY2()
        {
            if (AxisY2 == null)
            {
                AxisY2 = new ChartAxis[1];
                AxisY2[0] = new ChartAxis("");
                AxisY2[0].Position = ChartPosition.Right;
                AxisY2[0].Index = 0;
                AxisY2[0].MinorGrid.FixedCount = 10;
                AxisY2[0].MajorGrid.FixedCount = 10;
                AxisY2[0].MinorTickmark.FixedCount = 10;
                AxisY2[0].MajorTickMark.FixedCount = 10;
                AxisY2[0].LabelStyle.FixedCount = 10;
            }
            else
            {
                ChartAxis[] NewAxisY2 = new ChartAxis[AxisY2.Length + 1];
                for (int i = 0; i < AxisY2.Length; i++)
                {
                    NewAxisY2[i] = AxisY2[i];
                    NewAxisY2[i].Index = i;
                }
                NewAxisY2[AxisY2.Length] = new ChartAxis("");
                NewAxisY2[AxisY2.Length].SetGraphics(grfx);
                NewAxisY2[AxisY2.Length].Position = ChartPosition.Right;
                NewAxisY2[AxisY2.Length].Index = AxisY2.Length;
                NewAxisY2[AxisY2.Length].MinorGrid.FixedCount = 10;
                NewAxisY2[AxisY2.Length].MajorGrid.FixedCount = 10;
                NewAxisY2[AxisY2.Length].MinorTickmark.FixedCount = 10;
                NewAxisY2[AxisY2.Length].MajorTickMark.FixedCount = 10;
                NewAxisY2[AxisY2.Length].LabelStyle.FixedCount = 10;
                AxisY2 = NewAxisY2;
            }
            SetInnerPlotSize();
        }
        public void RemoveAxisY()
        {
            if (AxisY.Length > 1)
            {
                ChartAxis[] NewAxisY = new ChartAxis[AxisY.Length - 1];
                for (int i = 0; i < AxisY.Length - 1; i++)
                {
                    NewAxisY[i] = AxisY[i];
                }
                AxisY = NewAxisY;
            }
        }
        public void RemoveAxisY2()
        {
            if (AxisY2.Length > 1)
            {
                ChartAxis[] NewAxisY2 = new ChartAxis[AxisY2.Length - 1];
                for (int i = 0; i < AxisY2.Length - 1; i++)
                {
                    NewAxisY2[i] = AxisY2[i];
                }
                AxisY2 = NewAxisY2;
            }
        }
        public void UpdateAxesMinMax()
        {
            this.ZoomReset();
            this.ScaleReset();
            if (AxisX != null)
            {
                AxisX.Enabled = false;
                if (AxisX.MinFixed == Constant.DOUBLE_NAN) AxisX.Minimum = Constant.DOUBLE_NAN;
                if (AxisX.MaxFixed == Constant.DOUBLE_NAN) AxisX.Maximum = Constant.DOUBLE_NAN;
            }
            if (AxisX2 != null)
            {
                AxisX2.Enabled = false;
                if (AxisX2.MinFixed == Constant.DOUBLE_NAN) AxisX2.Minimum = Constant.DOUBLE_NAN;
                if (AxisX2.MaxFixed == Constant.DOUBLE_NAN) AxisX2.Maximum = Constant.DOUBLE_NAN;
            }
            if (AxisY != null)
            {
                for (int i = 0; i < AxisY.Length; i++)
                {
                    AxisY[i].Enabled = false;
                    if (AxisY[i].MinFixed == Constant.DOUBLE_NAN) AxisY[i].Minimum = Constant.DOUBLE_NAN;
                    if (AxisY[i].MaxFixed == Constant.DOUBLE_NAN) AxisY[i].Maximum = Constant.DOUBLE_NAN;
                }
            }
            if (AxisY2 != null)
            {
                for (int i = 0; i < AxisY2.Length; i++)
                {
                    AxisY2[i].Enabled = false;
                    if (AxisY2[i].MinFixed == Constant.DOUBLE_NAN) AxisY2[i].Minimum = Constant.DOUBLE_NAN;
                    if (AxisY2[i].MaxFixed == Constant.DOUBLE_NAN) AxisY2[i].Maximum = Constant.DOUBLE_NAN;
                }
            }
            for (int i = 0; i < Series.Count; i++)
            {
                Series[i].SetAxisMinMax();
            }
            ReCalcChartAreaSizes();
            if (AxisXMinimum != Constant.DOUBLE_NAN) SetAxisXMinimum(AxisXMinimum);
        }
        public void Zoom(float Left, float Top, float Right, float Bottom)
        {
            bool res = true;
            if (AxisX != null) res = AxisX.TestZoom(Left, Right);
            if ((res) && (AxisX2 != null)) res = AxisX2.TestZoom(Left, Right);

            if (AxisY != null)
            {
                for (int i = 0; i < AxisY.Length; i++)
                {
                    if (res) res = AxisY[i].TestZoom(Bottom, Top);
                }
            }
            if (AxisY2 != null)
            {
                for (int i = 0; i < AxisY2.Length; i++)
                {
                    if (res) res = AxisY2[i].TestZoom(Bottom, Top);
                }
            }
            if (res)
            {
                if (AxisX != null) AxisX.Zoom(Left, Right);
                if (AxisX2 != null) AxisX2.Zoom(Left, Right);

                if (AxisY != null)
                {
                    for (int i = 0; i < AxisY.Length; i++)
                    {
                        AxisY[i].Zoom(Bottom, Top);
                    }
                }
                if (AxisY2 != null)
                {
                    for (int i = 0; i < AxisY2.Length; i++)
                    {
                        AxisY2[i].Zoom(Bottom, Top);
                    }
                }
                if (ChartScaleChange != null) ChartScaleChange();
            }
        }
        public void ZoomReset()
        {
            if (AxisX != null) AxisX.ZoomReset();
            if (AxisX2 != null) AxisX2.ZoomReset();

            if (AxisY != null)
            {
                for (int i = 0; i < AxisY.Length; i++)
                {
                    AxisY[i].ZoomReset();
                }
            }
            if (AxisY2 != null)
            {
                for (int i = 0; i < AxisY2.Length; i++)
                {
                    AxisY2[i].ZoomReset();
                }
            }
            if (ChartScaleChange != null) ChartScaleChange();
        }
        public void ZoomReset(int numberOfViews)
        {
            if (AxisX != null) AxisX.ZoomReset(numberOfViews);
            if (AxisX2 != null) AxisX2.ZoomReset(numberOfViews);

            if (AxisY != null)
            {
                for (int i = 0; i < AxisY.Length; i++)
                {
                    AxisY[i].ZoomReset(numberOfViews);
                }
            }
            if (AxisY2 != null)
            {
                for (int i = 0; i < AxisY2.Length; i++)
                {
                    AxisY2[i].ZoomReset(numberOfViews);
                }
            }
            if (ChartScaleChange != null) ChartScaleChange();
        }
        public void ScaleReset()
        {
            if (AxisX != null) AxisX.ScaleReset();
            if (AxisX2 != null) AxisX2.ScaleReset();

            if (AxisY != null)
            {
                for (int i = 0; i < AxisY.Length; i++)
                {
                    AxisY[i].ScaleReset();
                }
            }
            if (AxisY2 != null)
            {
                for (int i = 0; i < AxisY2.Length; i++)
                {
                    AxisY2[i].ScaleReset();
                }
            }
            if (ChartScaleChange != null) ChartScaleChange();
        }
        void ResetAxisLabelSizes()
        {
            int i;
            if (AxisX != null) AxisX.SetLabelsSize();
            if (AxisX2 != null) AxisX2.SetLabelsSize();
            if (AxisY != null)
            {
                for (i = AxisY.Length - 1; i >= 0; i--)
                {
                    AxisY[i].SetLabelsSize();
                }
            }
            if (AxisY2 != null)
            {
                for (i = AxisY2.Length - 1; i >= 0; i--)
                {
                    AxisY2[i].SetLabelsSize();
                }
            }
        }
        public bool ScaleAxis(float dScale, int CursorX, int CursorY, bool OnlyAxesX)
        {
            int i;
            bool res = true;
            if (AxisX != null) res = AxisX.TestScale(CursorX, AxisX.ScaleView.Size * dScale);
            if ((res) && (AxisX2 != null)) res = AxisX2.TestScale(CursorX, AxisX2.ScaleView.Size * dScale);

            if (!OnlyAxesX)
            {
                if ((res) && (AxisY != null))
                {
                    for (i = 0; i < AxisY.Length; i++)
                    {
                        if (res) res = AxisY[i].TestScale(CursorY, AxisY[i].ScaleView.Size * dScale);
                    }
                }
                if ((res) && (AxisY2 != null))
                {
                    for (i = 0; i < AxisY2.Length; i++)
                    {
                        if (res) res = AxisY2[i].TestScale(CursorY, AxisY2[i].ScaleView.Size * dScale);
                    }
                }
            }
            if (res)
            {
                if (AxisX != null) res = AxisX.Scale(CursorX, AxisX.ScaleView.Size * dScale);
                if ((res) && (AxisX2 != null)) res = AxisX2.Scale(CursorX, AxisX2.ScaleView.Size * dScale);

                if (!OnlyAxesX)
                {
                    if ((res) && (AxisY != null))
                    {
                        for (i = 0; i < AxisY.Length; i++)
                        {
                            if (res) res = AxisY[i].Scale(CursorY, AxisY[i].ScaleView.Size * dScale);
                        }
                    }
                    if ((res) && (AxisY2 != null))
                    {
                        for (i = 0; i < AxisY2.Length; i++)
                        {
                            if (res) res = AxisY2[i].Scale(CursorY, AxisY2[i].ScaleView.Size * dScale);
                        }
                    }
                }
                if (ChartScaleChange != null) ChartScaleChange();
            }
            return res;
        }
        public void ScroolAxis(Point Point1, Point Point2)
        {
            if (AxisX != null) AxisX.Scrool(Point1.X, Point2.X);
            if (AxisX2 != null) AxisX2.Scrool(Point1.X, Point2.X);
            if (AxisY != null)
            {
                for (int i = 0; i < AxisY.Length; i++)
                {
                    AxisY[i].Scrool(Point1.Y, Point2.Y);
                }
            }
            if (AxisY2 != null)
            {
                for (int i = 0; i < AxisY2.Length; i++)
                {
                    AxisY2[i].Scrool(Point1.Y, Point2.Y);
                }
            }
        }
        #endregion

        #region TOOLTIP
        public ChartToolTip GetSeriesToolTip(int X, int Y)
        {
            ChartToolTip ToolTip = null;
            double ScreenDistance = 20;
            double minDist = -1, dist, val;
            float scrX, scrY;
            int ptIndex;
            DataPoint dp;
            int i, j, j1, j2;
            RectangleF outInnerPlotPosition = InnerPlotPosition;
            outInnerPlotPosition.Offset(-10, -10);
            outInnerPlotPosition.Inflate(20, 20);
            if (outInnerPlotPosition.Contains(X, Y))
            {
                for (i = 0; i < Series.Count; i++)
                {
                    if ((Series[i].Enabled) && (Series[i].Visible))
                    {
                        ptIndex = Series[i].GetNearestPointIndex(X);
                        if (ptIndex != -1)
                        {
                            j1 = ptIndex - 5;
                            j2 = ptIndex + 5;
                            if (j1 < 0) j1 = 0;
                            if (j2 >= Series[i].Points.Count) j2 = Series[i].Points.Count;
                            for (j = j1; j < j2; j++)
                            {
                                dp = Series[i].Points[j];
                                val = dp.XValue;
                                if (Series[i].ChartType == SeriesChartType.StepLine)
                                {
                                    val = (val + Series[i].AxisX.GetNextValue(dp.XValue)) / 2;
                                }
                                if (!Series[i].AxisX.ScaleView.ContainsValue(val)) continue;
                                scrX = Series[i].AxisX.ScreenPosByValue(val);
                                scrY = Series[i].AxisY.ScreenPosByValue(dp.YValue);
                                dist = Math.Sqrt(Math.Pow(scrX - X, 2) + Math.Pow(scrY - Y, 2));
                                if ((dist < ScreenDistance) && ((minDist == -1) || (dist < minDist)))
                                {
                                    if (ToolTip == null) ToolTip = new ChartToolTip();
                                    ToolTip.Type = 0;
                                    ToolTip.AxisTip = Series[i].AxisX;
                                    ToolTip.series = Series[i];
                                    ToolTip.Color = Series[i].Color.ToArgb();
                                    ToolTip.Point.X = scrX;
                                    ToolTip.Point.Y = scrY;
                                    if (dp.YValue > 0 || dp.Comment == null)
                                    {
                                        ToolTip.Text = dp.YValue.ToString(Series[i].ToolTipFormat);
                                        if (Series[i].Unit != "") ToolTip.Text += ", " + Series[i].Unit;
                                        if (dp.Comment != null) ToolTip.Text += "(" + dp.Comment + ")";
                                        if ((Series[i].ChartType == SeriesChartType.StackedArea) && (dp.YValues != null))
                                        {
                                            ToolTip.Text += string.Format("({0}={1})", Series[i].Name, dp.YValues[0].ToString(Series[i].ToolTipFormat));
                                        }
                                    }
                                    else if (dp.Comment != null)
                                    {
                                        ToolTip.Text = dp.Comment;
                                    }
                                    ToolTip.XValue = dp.XValue;
                                    minDist = dist;
                                }
                            }
                        }
                    }
                }
            }
            return ToolTip;
        }
        public ChartToolTip GetChartLabelsToolTip(int X, int Y)
        {
            ChartToolTip ToolTip = null;
            double ScreenDistance = 20;
            double minDist = -1, dist;
            float scrX, scrY;
            int i;
            RectangleF outInnerPlotPosition = InnerPlotPosition;
            outInnerPlotPosition.Offset(-10, -10);
            outInnerPlotPosition.Inflate(20, 20);

            if (InnerPlotPosition.Contains(X, Y))
            {
                for (i = 0; i < AxisX.ChartLabels.Count; i++)
                {
                    if (AxisX.ChartLabels[i].Enabled)
                    {
                        scrX = AxisX.ScreenPosByValue(AxisX.ChartLabels[i].Position);
                        dist = Math.Abs(scrX - X);
                        if ((dist < ScreenDistance) && ((minDist == -1) || (dist < minDist)))
                        {
                            if (ToolTip == null) ToolTip = new ChartToolTip();
                            ToolTip.Type = 1;
                            if (AxisX.ChartLabels[i].BackColor != -1)
                            {
                                ToolTip.Color = AxisX.ChartLabels[i].BackColor;
                            }
                            else
                            {
                                ToolTip.Color = AxisX.ChartLabels.BackColor.ToArgb();
                            }
                            ToolTip.AxisTip = AxisX;
                            ToolTip.Point.X = scrX;
                            ToolTip.Point.Y = Y - 3;
                            ToolTip.Text = AxisX.ChartLabels[i].Text;
                            ToolTip.XValue = AxisX.ChartLabels[i].Position;
                            minDist = dist;
                        }
                    }
                }
            }
            if (outInnerPlotPosition.Contains(X, Y))
            {
                for (i = 0; i < AxisX.AxisLabels.Count; i++)
                {
                    if (AxisX.AxisLabels[i].Enabled)
                    {
                        scrX = AxisX.ScreenPosByValue(AxisX.AxisLabels[i].Position);
                        scrY = AxisX.Start.Y;
                        dist = Math.Sqrt(Math.Pow(scrX - X, 2) + Math.Pow(scrY - Y, 2));
                        if ((dist < ScreenDistance) && ((minDist == -1) || (dist < minDist)))
                        {
                            if (ToolTip == null) ToolTip = new ChartToolTip();
                            ToolTip.Type = 1;
                            ToolTip.Color = AxisX.AxisLabels[i].Marker.Color.ToArgb();
                            ToolTip.AxisTip = AxisX;
                            ToolTip.Point.X = scrX;
                            ToolTip.Point.Y = scrY;
                            ToolTip.Text = AxisX.AxisLabels[i].Text;
                            ToolTip.XValue = AxisX.AxisLabels[i].Position;
                            minDist = dist;
                        }
                    }
                }
            }
            return ToolTip;
        }
        public ChartToolTip GetStackedAreaNameToolTip(int X, int Y)
        {
            ChartToolTip ToolTip = null;
            float scrY, scrY2;
            int ptIndex;
            DataPoint dp;
            int i;
            if (InnerPlotPosition.Contains(X, Y))
            {
                for (i = 0; i < Series.Count; i++)
                {
                    if (Series[i].Enabled && Series[i].Visible && Series[i].ChartType == SeriesChartType.StackedArea)
                    {
                        ptIndex = Series[i].GetNearestPointIndex(X);
                        if (ptIndex != -1)
                        {
                            dp = Series[i].Points[ptIndex];
                            scrY2 = Series[i].AxisY.ScreenPosByValue(dp.YValue);
                            scrY = scrY2;
                            if(dp.YValues != null) scrY = Series[i].AxisY.ScreenPosByValue(dp.YValues[1]);
                            if((scrY2 < Y) && (Y < scrY))
                            {
                                if (ToolTip == null) ToolTip = new ChartToolTip();
                                ToolTip.Type = 2;
                                ToolTip.AxisTip = Series[i].AxisX;
                                ToolTip.series = Series[i];
                                ToolTip.Color = Series[i].Color.ToArgb();
                                ToolTip.Point.X = X;
                                ToolTip.Point.Y = Y + 11f;
                                ToolTip.Text = ToolTip.series.Name;
                                ToolTip.XValue = dp.XValue;
                                break;
                            }
                        }
                    }
                }
            }
            return ToolTip;
        }
        public ChartToolTip GetColorZonesToolTip(int X, int Y)
        {
            ChartToolTip ToolTip = null;
            float scrX1, scrX2;
            if (InnerPlotPosition.Contains(X, Y))
            {
                for (int i = 0; i < AxisX.ChartColorZones.Count; i++)
                {
                    if (AxisX.ChartColorZones[i].Enabled && AxisX.ScaleView.ContainsInterval(AxisX.ChartColorZones[i].Start, AxisX.ChartColorZones[i].End))
                    {
                        scrX1 = AxisX.ScreenPosByValue(AxisX.ChartColorZones[i].Start);
                        scrX2 = AxisX.ScreenPosByValue(AxisX.ChartColorZones[i].End);
                        if (scrX1 - 10 < X && X < scrX2 + 10)
                        {
                            if (ToolTip == null) ToolTip = new ChartToolTip();
                            ToolTip.Type = 2;
                            ToolTip.AxisTip = AxisX;
                            ToolTip.Point.X = X;
                            ToolTip.Point.Y = Y + 11f;
                            ToolTip.Text = "Причина остановки:\n" + AxisX.ChartColorZones[i].Text;
                            ToolTip.XValue = AxisX.ValueBySreenPos(X);
                            break;
                        }
                    }
                }
            }
            return ToolTip;
        }
        #endregion

        #region SERIES FUNC
        public void AddSeries(string Name, string Unit, SeriesChartType Type, bool XAxisPrimary, bool YAxisPrimary, int YAxisIndex, int GroupIndex)
        {
            ChartSeries ser;
            ChartAxis axisX = null, axisY = null;
            if (AxisX != null)
            {
                axisX = (XAxisPrimary) ? AxisX : AxisX2;
                if (YAxisPrimary)
                {
                    if ((AxisY != null) && (YAxisIndex < AxisY.Length))
                    {
                        axisY = AxisY[YAxisIndex];
                    }
                }
                else
                {
                    if ((AxisY2 != null) && (YAxisIndex < AxisY2.Length))
                    {
                        axisY = AxisY2[YAxisIndex];
                    }
                }

                

                if (GroupIndex > -1)
                {
                    while (GroupIndex >= Groups.Count)
                    {
                        Groups.Add(null);
                    }
                    if (Groups[GroupIndex] == null)
                    {
                        Groups[GroupIndex] = new SeriesGroup(GroupIndex, axisX);
                    }
                    ser = new ChartSeries();
                    ser.ChartType = Type;
                    if (axisX != null)
                    {
                        ser.SetAxis(axisX, axisY);
                    }
                    Groups[GroupIndex].AddSeries(ser);
                }
                else
                {
                    ser = new ChartSeries();
                    ser.ChartType = Type;
                    if (axisX != null)
                    {
                        ser.SetAxis(axisX, axisY);
                    }
                }
                ser.Name = Name;
                ser.Unit = Unit;
                if (this.AxisX.AddLastInterval) this.AxisX2.AddLastInterval = true;
                if (this.AxisX2.AddLastInterval) this.AxisX.AddLastInterval = true;
                Series.Add(ser);
                Legend.AddItem(ser);            
            }
        }
        public void SetNonZeroMinimum()
        {
            if (Series.Count > 0)
            {
                double min = AxisX.ScaleView.Minimum;
                AxisX.ScaleView.Minimum = AxisX.ScaleView.Maximum;
                for (int i = 0; i < Series.Count; i++)
                {
                    if (Series[i].Enabled && Series[i].Visible)
                    {
                        Series[i].SetNotZeroMinimum();
                    }
                }
                AxisXMinimum = AxisX.ScaleView.Minimum;
                if (AxisX.ScaleView.Minimum == AxisX.ScaleView.Maximum)
                {
                    AxisX.ScaleView.Minimum = min;
                    AxisXMinimum = Constant.DOUBLE_NAN;
                }
            }
        }
        public void SetAxisXMinimum(double Minimum)
        {
            if ((Series.Count > 0))
            {
                AxisXMinimum = Minimum;
                if ((AxisX.Enabled) && (AxisX.Minimum <= Minimum) && (Minimum <= AxisX.Maximum))
                {
                    AxisX.ScaleView.Minimum = Minimum;
                    AxisX.SetCurrentSize();
                }
                if ((AxisX2.Enabled) && (AxisX2.Minimum <= Minimum) && (Minimum <= AxisX2.Maximum))
                {
                    AxisX2.ScaleView.Minimum = Minimum;
                    AxisX2.SetCurrentSize();
                }                
            }
        }
        public void ReCalcStackedSeriesValues()
        {
            SeriesGroup group;
            ChartSeries ser;
            int g, i, j, ind;
            double minDate = DateTime.MaxValue.ToOADate();
            DateTime dt, minDt = DateTime.MinValue;
            int count = 0, max = 0;
            for (g = 0; g < Groups.Count; g++)
            {
                group = Groups[g];
                if (group.AxisX.ValueType == ChartValueType.DateTime)
                {
                    if ((group.AxisX.IntervalType != DateTimeIntervalType.Years) &&
                        (group.AxisX.IntervalType != DateTimeIntervalType.Months) &&
                        (group.AxisX.IntervalType != DateTimeIntervalType.Days))
                    {
                        throw new ArgumentException("Не поддерживаемый интервал данных.");
                    }
                }

                List<ChartSeries> seriesList = new List<ChartSeries>();
                for (i = 0; i < group.SeriesList.Count; i++)
                {
                    if ((group.SeriesList[i].Points.Count > 0) && group.SeriesList[i].Enabled && group.SeriesList[i].Visible)
                    {
                        if (minDate > group.SeriesList[i].Points[0].XValue)
                        {
                            minDate = group.SeriesList[i].Points[0].XValue;
                        }
                        seriesList.Add(group.SeriesList[i]);
                    }
                }
                if (minDate > 0)
                {
                    minDt = DateTime.FromOADate(minDate);
                    for (i = 0; i < seriesList.Count; i++)
                    {
                        dt = DateTime.FromOADate(seriesList[i].Points[seriesList[i].Points.Count - 1].XValue);
                        switch (group.AxisX.IntervalType)
                        {
                            case DateTimeIntervalType.Years: // по годам
                                count = dt.Year - minDt.Year + 1;
                                break;
                            case DateTimeIntervalType.Months: // по месяцам
                                count = (dt.Year - minDt.Year) * 12 + dt.Month - minDt.Month + 1;
                                break;
                            case DateTimeIntervalType.Days: // по дням
                                count = (dt - minDt).Days;
                                break;
                        }
                        if (max < count) max = count;
                    }

                    if (max > 0)
                    {
                        double[] SumValues = new double[count];
                        ind = 0;
                        for (i = 0; i < seriesList.Count; i++)
                        {
                            dt = DateTime.FromOADate(seriesList[i].Points[0].XValue);
                            switch (group.AxisX.IntervalType)
                            {
                                case DateTimeIntervalType.Years: // по годам
                                    ind = dt.Year - minDt.Year;
                                    break;
                                case DateTimeIntervalType.Months: // по месяцам
                                    ind = (dt.Year - minDt.Year) * 12 + dt.Month - minDt.Month;
                                    break;
                                case DateTimeIntervalType.Days: // по дням
                                    ind = (dt - minDt).Days;
                                    break;
                            }

                            if (ind >= 0)
                            {
                                seriesList[i].MinY = Constant.DOUBLE_NAN;
                                seriesList[i].MaxY = Constant.DOUBLE_NAN;
                                for (j = 0; (j < seriesList[i].Points.Count) && (ind < SumValues.Length); j++, ind++)
                                {
                                    seriesList[i].SetStackedValue(j, SumValues[ind]);
                                    SumValues[ind] += seriesList[i].Points[j].YValues[0];
                                }
                            }
                        }
                    }
                }
            }
        }
        public void ClearPoints()
        {
            for (int i = 0; i < Series.Count; i++)
            {
                Series[i].Enabled = true;
                Series[i].Points.Clear();
                Series[i].MinX = Constant.DOUBLE_NAN;
                Series[i].MinY = Constant.DOUBLE_NAN;
                Series[i].MaxX = Constant.DOUBLE_NAN;
                Series[i].MaxY = Constant.DOUBLE_NAN;
                Series[i].IsXValueIndexed = true;
            }

            AxisXMinimum = Constant.DOUBLE_NAN;
            if (AxisX != null)
            {
                AxisX.Clear();
                AxisX.CustomLabels.Clear();
            }
            if (AxisX2 != null)
            {
                AxisX2.Clear();
                AxisX2.CustomLabels.Clear();
            }
            if (AxisY != null) for (int i = 0; i < AxisY.Length; i++) AxisY[i].Clear();
            if (AxisY2 != null) for (int i = 0; i < AxisY2.Length; i++) AxisY2[i].Clear();
        }
        #endregion

        #region DRAWING
        public void SetGraphics(Graphics grfx)
        {
            this.grfx = grfx;
            Legend.SetGraphics(grfx);
            AxisX.SetGraphics(grfx);
            AxisX2.SetGraphics(grfx);
            for (int i = 0; i < AxisY.Length; i++)
            {
                AxisY[i].SetGraphics(grfx);
            }
            for (int i = 0; i < AxisY2.Length; i++)
            {
                AxisY2[i].SetGraphics(grfx);
            }
        }
        public void DrawObject(Graphics grfx)
        {
            if ((this.Enabled) && (InnerPlotPosition.Height > 0) && (InnerPlotPosition.Width > 0))
            {
                grfx.Clip = new Region(InnerPlotPosition);

                #region DRAW NAME

                //Font f = new Font("Tahoma", 16);
                //string str = "ChartArea " + Index.ToString();
                //SizeF sz = grfx.MeasureString(str, f, PointF.Empty, StringFormat.GenericTypographic);
                //if ((sz.Width < InnerPlotPosition.Width) && (sz.Height < InnerPlotPosition.Height))
                //{
                //    float x, y;
                //    x = InnerPlotPosition.X + InnerPlotPosition.Width / 2 - sz.Width / 2;
                //    y = InnerPlotPosition.Top + InnerPlotPosition.Height / 2 - sz.Height / 2;
                //    grfx.DrawString(str, f, Brushes.Gray, x, y, StringFormat.GenericTypographic);
                //}
                #endregion

                DrawChartAreaColorZones(grfx);
                DrawGridLines(grfx);
                DrawChartAreaLabels(grfx);
                bool draw = false;
                for (int i = 0; i < Series.Count; i++)
                {
                    Series[i].DrawBackObject(grfx);
                }
                for (int i = 0; i < Series.Count; i++)
                {
                    Series[i].DrawObject(grfx);
                    if ((!draw) && (!Series[i].IsEmpty)) draw = true;
                }
                grfx.ResetClip();

                if (AxisY != null)
                {
                    for (int i = AxisY.Length - 1; i >= 0; i--)
                    {
                        AxisY[i].DrawObject(grfx);
                    }
                }
                if (AxisY2 != null)
                {
                    for (int i = AxisY2.Length - 1; i >= 0; i--)
                    {
                        AxisY2[i].DrawObject(grfx);
                    }
                }
                if (AxisX != null) AxisX.DrawObject(grfx);
                if (AxisX2 != null) AxisX2.DrawObject(grfx);
                DrawChartAxisLabels(grfx);
                Legend.DrawObject(grfx);
                //if(draw) grfx.DrawRectangle(Pens.Black, InnerPlotPosition.X, InnerPlotPosition.Y, InnerPlotPosition.Width, InnerPlotPosition.Height);
            }
        }
        public void DrawGridLines(Graphics grfx)
        {
            int i;
            if (AxisX.Enabled) AxisX.DrawGridLines(grfx, InnerPlotPosition.Height);
            if (AxisX2.Enabled) AxisX2.DrawGridLines(grfx, InnerPlotPosition.Height);
            if (AxisY != null)
            {
                for (i = AxisY.Length - 1; i >= 0; i--)
                {
                    AxisY[i].DrawGridLines(grfx, InnerPlotPosition.Width);
                }
            }
            if (AxisY2 != null)
            {
                for (i = AxisY2.Length - 1; i >= 0; i--)
                {
                    AxisY2[i].DrawGridLines(grfx, InnerPlotPosition.Width);
                }
            }
        }
        public void DrawChartAreaLabels(Graphics grfx)
        {
            int i;
            if (AxisX.Enabled) AxisX.DrawChartAreaLabels(grfx, InnerPlotPosition.Height);
            if (AxisX2.Enabled) AxisX2.DrawChartAreaLabels(grfx, InnerPlotPosition.Height);
            if (AxisY != null)
            {
                for (i = AxisY.Length - 1; i >= 0; i--)
                {
                    AxisY[i].DrawChartAreaLabels(grfx, InnerPlotPosition.Width);
                }
            }
            if (AxisY2 != null)
            {
                for (i = AxisY2.Length - 1; i >= 0; i--)
                {
                    AxisY2[i].DrawChartAreaLabels(grfx, InnerPlotPosition.Width);
                }
            }
        }
        public void DrawChartAreaColorZones(Graphics grfx)
        {
            int i;
            if (AxisX.Enabled) AxisX.DrawChartAreaColorZones(grfx, InnerPlotPosition.Height);
            if (AxisX2.Enabled) AxisX2.DrawChartAreaColorZones(grfx, InnerPlotPosition.Height);
            if (AxisY != null)
            {
                for (i = AxisY.Length - 1; i >= 0; i--)
                {
                    AxisY[i].DrawChartAreaColorZones(grfx, InnerPlotPosition.Width);
                }
            }
            if (AxisY2 != null)
            {
                for (i = AxisY2.Length - 1; i >= 0; i--)
                {
                    AxisY2[i].DrawChartAreaColorZones(grfx, InnerPlotPosition.Width);
                }
            }
        }
        public void DrawChartAxisLabels(Graphics grfx)
        {
            int i;
            if (AxisX.Enabled) AxisX.DrawChartAxisLabels(grfx);
            if (AxisX2.Enabled) AxisX2.DrawChartAxisLabels(grfx);
            if (AxisY != null)
            {
                for (i = AxisY.Length - 1; i >= 0; i--)
                {
                    AxisY[i].DrawChartAxisLabels(grfx);
                }
            }
            if (AxisY2 != null)
            {
                for (i = AxisY2.Length - 1; i >= 0; i--)
                {
                    AxisY2[i].DrawChartAxisLabels(grfx);
                }
            }
        }
        #endregion
    }

    public sealed class ChartLegend
    {
        public bool Enabled;
        Graphics grfx;
        ChartLegendItem[] Items;
        public RectangleF Location;

        public ChartPosition Position;
        public Font Font;
        int Rows, Columns;
        float[] MaxColumnsWidth;
        ImageList imgList;

        public ChartLegend()
        {
            Enabled = false;
            Items = null;
            MaxColumnsWidth = null;
            grfx = null;
            Location = RectangleF.Empty;
            Position = ChartPosition.Bottom;
            Font = new Font("Calibri", 8.25f);
            Columns = 1;
            Rows = 1;
            imgList = new ImageList();
            imgList.Images.Add(Properties.Resources.unchecked16);
            imgList.Images.Add(Properties.Resources.checked16);
        }

        public ChartLegendItem this[int index]
        {
            get 
            {
                if ((index > -1) && (Items != null) && (index < Items.Length))
                {
                    return Items[index];
                }
                else
                    return ChartLegendItem.Empty;
            }
            set
            {
                Items[index] = value;
            }
        }
        public void AddItem(ChartSeries series)
        {
            if (Items == null)
            {
                Items = new ChartLegendItem[1];
                Items[0].Series = series;
                Items[0].Index = 0;
                SetItemSize(0);
            }
            else
            {
                ChartLegendItem[] newItems = new ChartLegendItem[Items.Length + 1];
                for (int i = 0; i < Items.Length; i++)
                {
                    newItems[i] = Items[i];
                }
                newItems[Items.Length].Series = series;
                newItems[Items.Length].Index = Items.Length;
                newItems[Items.Length].Visible = true;
                Items = newItems;
                SetItemSize(Items.Length - 1);
            }
        }
        void SetItemSize(int index)
        {
            SizeF size = SizeF.Empty;
            if ((index > -1) && (index < Items.Length) && (grfx != null))
            {
                ChartLegendItem item = Items[index];
                string text = item.Series.Name;
                if (item.Series.Unit != "") text += ", " + item.Series.Unit;
                SizeF txtSz = grfx.MeasureString(text, Font, PointF.Empty, StringFormat.GenericTypographic);
                size.Width = Convert.ToSingle(Math.Round(38 + txtSz.Width));
                size.Height = txtSz.Height;
            }
            if (size.Width < 38) size.Width = 38;
            if (size.Height < 20) size.Height = 20;
            Items[index].Location.Size = size;
            
        }
        public void Clear()
        {
            Items = null;
            MaxColumnsWidth = null;
        }

        public int[] GetNotEmptyIndexes()
        {
            int[] NE_Indexes = null;
            if (Items != null)
            {
                int count = 0;
                for(int i = 0; i < Items.Length;i++)
                {
                    if (!Items[i].Series.IsEmpty) count++;
                }
                if (count > 0)
                {
                    NE_Indexes = new int[count];
                    int j = 0;
                    for (int i = 0; i < Items.Length; i++)
                    {
                        if (!Items[i].Series.IsEmpty)
                        {
                            NE_Indexes[j++] = i;
                        }
                    }
                }
            }
            return NE_Indexes;
        }

        public void SetSelectedSeries(ChartSeries series)
        {
            if (Items != null)
            {
                for (int i = 0; i < Items.Length; i++)
                {
                    Items[i].Selected = false;
                    if (Items[i].Series == series)
                    {
                        Items[i].Selected = true;
                    }
                }
            }
        }
        public void SetGraphics(Graphics grfx)
        {
            this.grfx = grfx;
        }
        public void SetLegendTiles(float MaxWidth, float MaxHeight)
        {
            bool exit;
            if ((grfx != null) && (Items != null))
            {
                int i, j, r;
                float sz, maxW, maxH;
                int[] indexes = GetNotEmptyIndexes();

                if (indexes != null)
                {
                    switch (Position)
                    {
                        case ChartPosition.Top:
                            goto case ChartPosition.Bottom;
                        case ChartPosition.Bottom:
                            exit = true;
                            Rows = 1;
                            Columns = indexes.Length;
                            while ((exit) && (Columns > 1))
                            {
                                exit = false;
                                for (j = 0; j < Rows; j++)
                                {
                                    sz = 0;
                                    for (i = 0; (i < Columns) && (j * Columns + i < indexes.Length); i++)
                                    {
                                        maxW = 0;
                                        for (r = 0; (r < Rows) && (r * Columns + i < indexes.Length); r++)
                                        {
                                            if (maxW < Items[indexes[r * Columns + i]].Location.Width)
                                            {
                                                maxW = Items[indexes[r * Columns + i]].Location.Width;
                                            }
                                        }
                                        sz += maxW;
                                        if (sz > MaxWidth)
                                        {
                                            Columns--;
                                            if (Columns == 0) Columns++;
                                            Rows = (indexes.Length / Columns) + ((indexes.Length % Columns > 0) ? 1 : 0);
                                            exit = true;
                                            break;
                                        }
                                    }
                                    if (exit) break;
                                }
                            }
                            if (Columns == 0)
                            {
                                Rows = indexes.Length;
                                Columns = 1;
                            }
                            break;
                        case ChartPosition.Left:
                            goto case ChartPosition.Right;
                        case ChartPosition.Right:
                            exit = true;
                            Rows = indexes.Length;
                            Columns = 1;
                            while ((exit) && (Rows > 1))
                            {
                                exit = false;
                                for (j = 0; j < Columns; j++)
                                {
                                    sz = 0;
                                    for (i = 0; (i < Rows) && (j * Columns + i < indexes.Length); i++)
                                    {
                                        sz += Items[indexes[j * Columns + i]].Location.Height;
                                        if (sz > MaxHeight)
                                        {
                                            Rows--;
                                            if (Rows == 0) Rows++;
                                            Columns = (indexes.Length / Rows) + ((indexes.Length % Rows > 0) ? 1 : 0);
                                            exit = true;
                                            break;
                                        }
                                    }
                                    if (exit) break;
                                }
                            }
                            if (Rows == 0)
                            {
                                Columns = indexes.Length;
                                Rows = 1;
                            }
                            break;
                    }
                    maxH = 0;
                    MaxColumnsWidth = new float[Columns];
                    for (j = 0; j < Rows; j++)
                    {
                        exit = true;
                        for (i = 0; (i < Columns) && (j * Columns + i < indexes.Length); i++)
                        {
                            if (Items[indexes[j * Columns + i]].Location.Width > MaxColumnsWidth[i])
                            {
                                MaxColumnsWidth[i] = Items[indexes[j * Columns + i]].Location.Width;
                            }
                            if (exit)
                            {
                                maxH += Items[indexes[j * Columns + i]].Location.Height;
                                exit = false;
                            }
                        }
                    }
                    maxW = 0;
                    for (i = 0; i < Columns; i++) maxW += MaxColumnsWidth[i];
                    this.Location.Width = maxW;
                    this.Location.Height = maxH;
                }
            }
        }
        public void SetHotSpot(float X, float Y)
        {
            if (Items != null)
            {
                int i, j;
                float sz = 0;
                int[] indexes = GetNotEmptyIndexes();
                if (indexes != null)
                {
                    bool res;
                    for (j = 0; j < Rows; j++)
                    {
                        res = true;
                        for (i = 0; (i < Columns) && (j * Columns + i < indexes.Length); i++)
                        {
                            Items[indexes[j * Columns + i]].Location.Y = Y + sz;
                        }
                        if (j * Columns < indexes.Length) sz += Items[indexes[j * Columns]].Location.Height;
                    }
                    for (j = 0; j < Rows; j++)
                    {
                        sz = 0;
                        for (i = 0; (i < Columns) && (j * Columns + i < indexes.Length); i++)
                        {
                            Items[indexes[j * Columns + i]].Location.X = X + sz;
                            sz += MaxColumnsWidth[i];
                        }
                    }
                }
            }
            this.Location.X = X;
            this.Location.Y = Y;
        }
        public int GetItemIndex(Point ScreenPoint)
        {
            for (int i = 0; i < Items.Length; i++)
            {
                if ((!Items[i].Series.IsEmpty) && (Items[i].Location.Contains(ScreenPoint)))
                {
                    return i;
                }
            }
            return -1;
        }
        public bool DrawItem(Graphics grfx, int ItemIndex)
        {
            bool result = false;
            if ((grfx != null) && (ItemIndex > -1) && (ItemIndex < Items.Length))
            {
                ChartLegendItem item = Items[ItemIndex];
                Brush br;
                Pen pen;
                if (!item.Series.IsEmpty)
                {
                    result = true;
                    string str = item.Series.Name;
                    if (item.Series.Unit != "") str += ", " + item.Series.Unit;
                    float x = item.Location.X, y = item.Location.Y;
                    if (item.Selected)
                    {
                        grfx.FillRectangle(Brushes.LightGray, item.Location);
                    }
                    grfx.DrawImageUnscaled(imgList.Images[(item.Visible ? 1 : 0)], (int)(x + 1), (int)(y + 3));

                    switch (item.Series.ChartType)
                    {
                        case SeriesChartType.StackedArea:
                            goto case SeriesChartType.Column;
                        case SeriesChartType.Column:
                            br = new SolidBrush(item.Series.Color);
                            pen = new Pen(item.Series.BorderColor, item.Series.BorderWidth);
                            pen.DashStyle = item.Series.BorderDashStyle;
                            grfx.FillRectangle(br, x + 16, y + 2, 16, 16);
                            grfx.DrawRectangle(pen, x + 16, y + 2, 16, 16);
                            br.Dispose();
                            pen.Dispose();
                            break;
                        case SeriesChartType.Line:
                            goto case SeriesChartType.StepLine;
                        case SeriesChartType.StepLine:
                            float width = item.Series.BorderWidth;
                            if (width > 14) width = 10;
                            pen = new Pen(item.Series.Color, width);
                            pen.DashStyle = item.Series.BorderDashStyle;
                            grfx.DrawLine(pen, x + 16, y + item.Location.Height / 2 - width / 2, x + 32, y + item.Location.Height / 2 - width / 2);
                            pen.Dispose();
                            break;
                        case SeriesChartType.Point:
                            br = new SolidBrush(item.Series.Marker.Color);
                            pen = new Pen(item.Series.Marker.BorderColor, item.Series.Marker.BorderWidth);
                            item.Series.Marker.Draw(grfx, br, pen, x + 24, y + item.Location.Height / 2 - item.Series.Marker.BorderWidth / 2);
                            br.Dispose();
                            pen.Dispose();
                            break;
                    }


                    SizeF sz = grfx.MeasureString(str, Font, PointF.Empty, StringFormat.GenericTypographic);
                    grfx.DrawString(str, Font, Brushes.Black, (int)(x + 34), y + item.Location.Height / 2 - sz.Height / 2, StringFormat.GenericTypographic);

                    //grfx.DrawRectangle(Pens.Red, item.Location.X, item.Location.Y, item.Location.Width, item.Location.Height);
                }
            }
            return result;
        }
        public void DrawObject(Graphics grfx)
        {
            if ((Enabled) && (Items != null))
            {
                int count = 0;
                grfx.FillRectangle(Brushes.White, Location);
                for (int i = 0; i < Items.Length; i++)
                {
                    if (DrawItem(grfx, i)) count++;
                }
                if(count > 0) grfx.DrawRectangle(Pens.Black, Location.X, Location.Y, Location.Width, Location.Height);
            }
        }
    }

    public sealed class ChartToolTip
    {
        public byte Type;
        public PointF Point;
        public ChartSeries series;
        public ChartAxis AxisTip;
        public string Text;
        public double XValue;
        public int Color;
    }

    public sealed class ChartAxis
    {
        #region Drawing
        Graphics grfx;
        Pen line_pen;
        Color titleForeColor;
        Brush title_brush;
        #endregion

        #region AXIS DATA
        public AxisArrowStyle ArrowStyle;
        public int Index;
        public bool Enabled;
        public double Interval;
        public double IntervalOffset;
        DateTimeIntervalType intervalType;
        public DateTimeIntervalType IntervalType
        {
            get { return intervalType; }
            set 
            {
                intervalType = value;
                MajorGrid.IntervalType = value;
                MajorGrid.IntervalOffsetType = value;
                MinorGrid.IntervalType = value;
                MinorGrid.IntervalOffsetType = value;
                MajorTickMark.IntervalType = value;
                MajorTickMark.IntervalOffsetType = value;
                MinorTickmark.IntervalType = value;
                MinorTickmark.IntervalOffsetType = value;
                LabelStyle.IntervalType = value;
                LabelStyle.IntervalOffsetType = value;
                if (ScaleView.Minimum != Constant.DOUBLE_NAN)
                {
                    if (intervalType == DateTimeIntervalType.Auto || intervalType == DateTimeIntervalType.NotSet || intervalType == DateTimeIntervalType.Number)
                    {
                        ScaleView.MinInterval = (GetNextValueByIntervalType(ScaleView.Minimum, intervalType, ScaleView.MinSize) - ScaleView.Minimum) / 1000;
                    }
                    else
                    {
                        ScaleView.MinInterval = GetNextValueByIntervalType(ScaleView.Minimum, intervalType, ScaleView.MinSize) - ScaleView.Minimum;
                    }
                }
            }
        }
        public DateTimeIntervalType IntervalOffsetType;
        public bool IsLogarithmic;
        public bool IsReversed;
        public bool AddLastInterval;
        public LabelStyle LabelStyle;
        public Color LineColor
        {
            get { return line_pen.Color; }
            set
            {
                line_pen = new Pen(value);
            }
        }
        public DashStyle LineDashStyle;
        public int LineWidth;
        public double LogarithmBase;
        public Grid MinorGrid, MajorGrid;
        public TickMark MinorTickmark, MajorTickMark;
        double min, max;
        public double Minimum
        {
            get { return min; }
            set 
            { 
                min = value;
                ScaleView.ZoomReset();
                ScaleView.Minimum = min;
                SetCurrentSize();
            }
        }
        public double Maximum
        {
            set 
            { 
                max = value;
                ScaleView.ZoomReset();
                double result = max;
                if ((AddLastInterval) && (max != Constant.DOUBLE_NAN))
                {
                    DateTime dt = DateTime.FromOADate(max);
                    switch (intervalType)
                    {
                        case DateTimeIntervalType.Months:
                            dt = new DateTime(dt.Year, dt.Month, 1);
                            break;
                        case DateTimeIntervalType.Years:
                            dt = new DateTime(dt.Year, 1, 1);
                            break;
                    }
                    result = GetNextValue(dt.ToOADate());
                }
                ScaleView.Maximum = result;
                SetCurrentSize();
            }
            get 
            {
                double result = max;
                if ((AddLastInterval) && (max != Constant.DOUBLE_NAN))
                {
                    DateTime dt = DateTime.FromOADate(max);
                    switch (intervalType)
                    {
                        case DateTimeIntervalType.Months:
                            dt = new DateTime(dt.Year, dt.Month, 1);
                            break;
                        case DateTimeIntervalType.Years:
                            dt = new DateTime(dt.Year, 1, 1);
                            break;
                    }
                    result = GetNextValue(max);
                }
                return result;
            }
        }
        public double MinFixed;
        public double MaxFixed;
        public AxisScaleView ScaleView;
        public TextOrientation TextOrientation;
        string title, unit;
        public string Title
        {
            get { return title; }
            set 
            {
                title = value;
                TitleSize = SizeF.Empty;
                if ((grfx != null) && (value != null) && (value != ""))
                {
                    TitleSize = grfx.MeasureString(title, TitleFont, PointF.Empty, StringFormat.GenericTypographic);
                    switch(Position)
                    {
                        case ChartPosition.Top:
                            goto case ChartPosition.Bottom;
                        case ChartPosition.Bottom:
                            TitleSize.Height -= 2;
                            break;
                    }
                }
            }
        }
        public string Unit
        {
            get { return unit; }
            set
            {
                unit = value;
                string str = title;
                if (unit != "") str += ", " + unit;
                TitleSize = SizeF.Empty;
                if ((grfx != null) && (str != null) && (str != ""))
                {
                    TitleSize = grfx.MeasureString(str, TitleFont, PointF.Empty, StringFormat.GenericTypographic);
                    switch (Position)
                    {
                        case ChartPosition.Top:
                            goto case ChartPosition.Bottom;
                        case ChartPosition.Bottom:
                            TitleSize.Height -= 2;
                            break;
                    }
                }
            }
        }
        public StringAlignment TitleAlignment;
        public Font TitleFont;
        public Color TitleForeColor
        {
            get { return titleForeColor; }
            set
            {
                titleForeColor = value;
                title_brush = new SolidBrush(value);
            }
        }
        public SizeF TitleSize;
        public SizeF LabelsSize;
        public ChartPosition Position;
        PointF start, end;
        public PointF Start
        {
            get { return start; }
            set
            {
                start = value;
                SetCurrentSize();
            }
        }
        public PointF End
        {
            get { return end; }
            set
            {
                end = value;
                SetCurrentSize();
            }
        }
        public float Size
        {
            get
            {
                float res = 0;
                if ((!Start.IsEmpty) && (!End.IsEmpty))
                {
                    int k = (IsReversed) ? -1 : 1;
                    switch (this.Position)
                    {
                        case ChartPosition.Top:
                            goto case ChartPosition.Bottom;
                        case ChartPosition.Bottom:
                            res = k * (End.X - Start.X);
                            break;
                        case ChartPosition.Left:
                            goto case ChartPosition.Right;
                        case ChartPosition.Right:
                            res = k * (Start.Y - End.Y);
                            break;
                    }
                }
                return res;
            }
        }
        public double CurrentSize;
        ChartValueType valueType;
        public ChartValueType ValueType
        {
            get { return valueType; }
            set 
            {
                switch (value)
                {
                    case ChartValueType.DateTime:
                        LabelStyle.Format = "dd.MM.yyyy";
                        break;
                    case ChartValueType.Double:
                        if (Maximum > 10)
                        {
                            LabelStyle.Format = "0";
                        }
                        else if (Maximum > 1)
                        {
                            LabelStyle.Format = "0.0";
                        }
                        else if (Maximum > 0.1)
                        {
                            LabelStyle.Format = "0.00";
                        }
                        else if (Maximum > 0.01)
                        {
                            LabelStyle.Format = "0.000";
                        }
                        else if (Maximum > 0.001)
                        {
                            LabelStyle.Format = "0.0000";
                        }
                        break;
                    default:
                        LabelStyle.Format = "0";
                        break;
                }
                valueType = value;
            }
        }
        public bool Horizontal
        {
            get 
            {
                switch (this.Position)
                {
                    case ChartPosition.Top:
                        goto case ChartPosition.Bottom;
                    case ChartPosition.Bottom:
                        return true;
                    case ChartPosition.Left:
                        goto case ChartPosition.Right;
                    case ChartPosition.Right:
                        return false;
                }
                return true;
            }
        }
        public AxisCustomLabels CustomLabels;
        public ChartAreaLabels ChartLabels;
        public ChartAreaColorZones ChartColorZones;
        public ChartAxisLabels AxisLabels;

        public bool IsScaled
        {
            get
            {
                return ((ScaleView.Minimum != this.Minimum) || (ScaleView.Maximum != this.Maximum));
            }
        }
        #endregion

        public ChartAxis()
        {
            this.grfx = null;
            this.ScaleView = new AxisScaleView();

            #region MAJOR/MINOR GRID
            this.MajorGrid = new Grid();
            this.MajorGrid.Interval = 1;

            this.MinorGrid = new Grid();
            this.MinorGrid.Interval = 1;
            #endregion

            #region TickMarks
            this.MajorTickMark = new TickMark();
            MajorTickMark.Size = 8F;
            MajorTickMark.Interval = 1;

            this.MinorTickmark = new TickMark();
            MinorTickmark.Size = 3F;
            MinorTickmark.Interval = 1;
            MinorTickmark.TickMarkStyle = TickMarkStyle.InsideArea;
            #endregion

            #region LabelStyle
            this.LabelStyle = new LabelStyle();
            this.LabelStyle.Enabled = false;
            this.LabelStyle.Interval = 1;
            #endregion

            this.ArrowStyle = AxisArrowStyle.None;
            this.Enabled = false;
            this.Interval = 1.0;
            this.IntervalType = DateTimeIntervalType.Auto;
            this.IntervalOffset = 0;
            this.IntervalOffsetType = DateTimeIntervalType.Auto;
            this.IsLogarithmic = false;
            this.IsReversed = false;
            this.AddLastInterval = false;
            this.Index = 0;
            this.LineColor = Color.Black;
            this.LineDashStyle = DashStyle.Solid;
            this.LineWidth = 1;
            this.Start = PointF.Empty;
            this.End = PointF.Empty;
            this.Minimum = Constant.DOUBLE_NAN;
            this.Maximum = Constant.DOUBLE_NAN;
            this.MinFixed = Constant.DOUBLE_NAN;
            this.MaxFixed = Constant.DOUBLE_NAN;
            this.LogarithmBase = 10.0;
            this.Position = ChartPosition.Left;
            this.TextOrientation = TextOrientation.Horizontal;
            this.unit = "";
            this.title = "";
            this.TitleAlignment = StringAlignment.Center;
            this.TitleFont = new Font("Calibri", 8.25f);
            this.TitleForeColor = Color.Black;
            this.TitleSize = SizeF.Empty;
            this.ValueType = ChartValueType.Double;
            this.CustomLabels = new AxisCustomLabels();
            this.ChartLabels = new ChartAreaLabels(this);
            this.ChartColorZones = new ChartAreaColorZones();
            this.AxisLabels = new ChartAxisLabels();
            this.CurrentSize = 0;
        }
        public ChartAxis(string Title) : this()
        {
            this.Title = Title;
        }

        public void GetStyle(ChartAxis Axis)
        {
            MinorGrid.GetStyle(Axis.MinorGrid);
            MajorGrid.GetStyle(Axis.MajorGrid);

            MinorTickmark.GetStyle(Axis.MinorTickmark);
            MajorTickMark.GetStyle(Axis.MajorTickMark);

            LabelStyle.GetStyle(Axis.LabelStyle);
            this.ScaleView.MinInterval = Axis.ScaleView.MinInterval;

            this.ArrowStyle = Axis.ArrowStyle;
            this.Enabled = Axis.Enabled;
            this.Interval = Axis.Interval;
            this.IntervalType = Axis.IntervalType;
            this.IntervalOffset = Axis.IntervalOffset;
            this.IntervalOffsetType = Axis.IntervalOffsetType;
            this.IsLogarithmic = Axis.IsLogarithmic;
            this.IsReversed = Axis.IsReversed;
            this.AddLastInterval = Axis.AddLastInterval;

            this.LineColor = Axis.LineColor;
            this.LineDashStyle = Axis.LineDashStyle;
            this.LineWidth = Axis.LineWidth;

            this.LogarithmBase = Axis.LogarithmBase;
            this.Position = Axis.Position;
            this.TextOrientation = Axis.TextOrientation;
            this.unit = Axis.Unit;
            this.title = Axis.Title;
            this.TitleAlignment = Axis.TitleAlignment;
            this.TitleFont = Axis.TitleFont;
            this.TitleForeColor = Axis.TitleForeColor;
            this.CustomLabels.Enabled = Axis.CustomLabels.Enabled;
            this.ChartLabels.BackColor = Axis.ChartLabels.BackColor;
            
            this.MinFixed = Axis.MinFixed;
            this.MaxFixed = Axis.MaxFixed;
            this.Minimum = Axis.MinFixed;
            this.Maximum = Axis.MaxFixed;
            this.ValueType = Axis.ValueType;
        }

        public float GetOutsideSize()
        {
            float res = 0;
            float dTM = 0;
            if (dTM < MinorTickmark.OutsideSize) dTM = MinorTickmark.OutsideSize;
            if (dTM < MajorTickMark.OutsideSize) dTM = MajorTickMark.OutsideSize;
            res += dTM;
            switch (this.Position)
            {
                case ChartPosition.Top:
                    goto case ChartPosition.Bottom;
                case ChartPosition.Bottom:
                    res += LabelsSize.Height;
                    break;
                case ChartPosition.Left:
                    goto case ChartPosition.Right;
                case ChartPosition.Right:
                    res += LabelsSize.Width;
                    break;
            }
            res += CustomLabels.GetGraphicsSize(grfx).Height;
            return res;
        }
        public float GetInsideSize()
        {
            float dTM = 0;
            if (dTM < MinorTickmark.InsideSize) dTM = MinorTickmark.InsideSize;
            if (dTM < MajorTickMark.InsideSize) dTM = MajorTickMark.InsideSize;
            return dTM;
        }

        public void Clear()
        {
            this.Enabled = false;
            ChartLabels.Clear();
            ChartColorZones.Clear();
            AxisLabels.Clear();
            ScaleView.ZoomReset();
            this.Minimum = this.MinFixed;
            this.Maximum = this.MaxFixed;
            ScaleView.Minimum = this.Minimum;
            ScaleView.Maximum = this.Maximum;
            this.CurrentSize = 0;
            this.Unit = string.Empty;
        }
        public void SetGraphics(Graphics grfx)
        {
            this.grfx = grfx;
            if ((TitleSize.Height == 0) && (grfx != null) && (title != ""))
            {
                Title = title;
            }
            SetLabelsSize();
        }
        public void SetLabelsSize()
        {
            if ((grfx != null) && (LabelStyle != null) && (LabelStyle.Enabled) && (this.Maximum != Constant.DOUBLE_NAN))
            {
                string str;
                switch (this.ValueType)
                {
                    case ChartValueType.DateTime:
                        str = DateTime.FromOADate(this.Maximum).ToString(LabelStyle.Format);
                        break;
                    default:
                        str = this.Maximum.ToString(LabelStyle.Format);
                        break;
                }
                
                LabelsSize = grfx.MeasureString(str, LabelStyle.Font, PointF.Empty, StringFormat.GenericTypographic);
                switch (this.Position)
                {
                    case ChartPosition.Top:
                        goto case ChartPosition.Bottom;
                    case ChartPosition.Bottom:
                        LabelsSize.Height -= 2;
                        break;
                    case ChartPosition.Left:
                        goto case ChartPosition.Right;
                    case ChartPosition.Right:
                        LabelsSize.Width += 2;
                        break;
                }
            }
        }
        public void SetMinMax(double Min, double Max)
        {
            if (this.MinFixed != Constant.DOUBLE_NAN && Min < this.MinFixed) return;
            if (this.MaxFixed != Constant.DOUBLE_NAN && Max > this.MaxFixed) return;

            if ((this.min == Constant.DOUBLE_NAN) || (this.min > Min)) this.Minimum = Min;
            if ((this.max == Constant.DOUBLE_NAN) || (this.max < Max)) this.Maximum = Max;

            this.ValueType = this.valueType;
        }
        public void ZoomReset()
        {
            ScaleView.ZoomReset();
            ScaleView.Minimum = this.Minimum;
            ScaleView.Maximum = this.Maximum;
            SetCurrentSize();
        }
        public void ZoomReset(int numberOfViews)
        {
            ScaleView.ZoomReset(numberOfViews);
            SetCurrentSize();
        }
        public bool TestZoom(float ScrStart, float ScrEnd)
        {
            if ((this.Enabled) && (ScaleView.Minimum != Constant.DOUBLE_NAN) && (ScaleView.Maximum != Constant.DOUBLE_NAN))
            {
                double valStart = ValueBySreenPos(ScrStart);
                double valEnd = ValueBySreenPos(ScrEnd);

                if ((valStart >= Math.Round(Minimum, 3)) && (valEnd <= Math.Round(Maximum, 3)) && (Math.Abs(valEnd - valStart) > ScaleView.MinInterval))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        public void Zoom(float ScrStart, float ScrEnd)
        {
            if ((this.Enabled) && (ScaleView.Minimum != Constant.DOUBLE_NAN) && (ScaleView.Maximum != Constant.DOUBLE_NAN))
            {
                double valStart = ValueBySreenPos(ScrStart);
                double valEnd = ValueBySreenPos(ScrEnd);

                if ((valStart >= Minimum) && (valEnd <= Math.Round(Maximum, 3)) && (Math.Abs(valEnd - valStart) > ScaleView.MinInterval))
                {
                    if (IsReversed)
                    {
                        ScaleView.Zoom(valEnd, valStart);
                    }
                    else
                    {
                        ScaleView.Zoom(valStart, valEnd);
                    }
                    SetCurrentSize();
                }
            }
        }
        public bool TestScale(float Cursor, double NewSize)
        {
            if (this.Enabled)
            {
                if ((this.Maximum - this.Minimum - NewSize >= (ScaleView.MinInterval / 100.0)) && (NewSize < this.ScaleView.MinInterval))
                {
                    return false;
                }
            }
            return true;
        }
        public void ScaleReset()
        {
            ScaleView.Minimum = this.Minimum;
            ScaleView.Maximum = this.Maximum;
            SetCurrentSize();
        }
        public bool Scale(float Cursor, double NewSize)
        {
            bool result = true;
            if (this.Enabled)
            {
                double AxisSize = this.Maximum - this.Minimum;
                if (AxisSize - NewSize < (ScaleView.MinInterval / 100.0))
                {
                    ScaleView.Minimum = this.Minimum;
                    ScaleView.Maximum = this.Maximum;
                    SetCurrentSize();
                }
                else if (NewSize > this.ScaleView.MinInterval)
                {
                    double val1, val2;
                    double min, max, predMin, predMax;
                    val1 = ValueBySreenPos(Cursor);
                    predMin = ScaleView.Minimum;
                    predMax = ScaleView.Maximum;
                    ScaleView.Maximum = ScaleView.Minimum + NewSize;
                    SetCurrentSize();
                    val2 = ValueBySreenPos(Cursor);
                    min = ScaleView.Minimum - (val2 - val1);
                    max = min + NewSize;
                    if (min < Minimum)
                    {
                        min = Minimum;
                        max = min + NewSize;
                    }
                    else if (max > Maximum)
                    {
                        min = Maximum - NewSize;
                        max = Maximum;
                    }
                    ScaleView.Minimum = min;
                    ScaleView.Maximum = max;
                    //ScaleView.Minimum = predMin;
                    //ScaleView.Maximum = predMax;
                    //ScaleView.Zoom(min, max);
                    SetCurrentSize();
                }
                else
                    result = false;
            }
            return result;
        }
        public void Scrool(float Coord1, float Coord2)
        {
            if (this.Enabled)
            {
                double dx, size;
                dx = ValueBySreenPos(Coord1);
                dx = dx - ValueBySreenPos(Coord2);
                ScaleView.Minimum += dx;
                ScaleView.Maximum += dx;
                if (ScaleView.Minimum < Minimum)
                {
                    size = ScaleView.Size;
                    ScaleView.Minimum = Minimum;
                    ScaleView.Maximum = Minimum + size;
                }
                else if (ScaleView.Maximum > Maximum)
                {
                    size = ScaleView.Size;
                    ScaleView.Maximum = Maximum;
                    ScaleView.Minimum = Maximum - size;
                }
            }
        }
        public void SetCurrentSize()
        {
            CurrentSize = 0;
            if ((Minimum < Maximum) && (ScaleView.Minimum != Constant.DOUBLE_NAN) && (ScaleView.Maximum != Constant.DOUBLE_NAN))
            {
                int k = (IsReversed) ? -1 : 1;
                if (Horizontal)
                {
                    CurrentSize = k * (end.X - start.X) / ScaleView.Size;
                }
                else
                {
                    CurrentSize = k * (start.Y - end.Y) / ScaleView.Size;
                }
                if (intervalType == DateTimeIntervalType.Auto || intervalType == DateTimeIntervalType.NotSet || intervalType == DateTimeIntervalType.Number)
                {
                    ScaleView.MinInterval = (GetNextValueByIntervalType(ScaleView.Minimum, intervalType, ScaleView.MinSize) - ScaleView.Minimum) / 1000;
                }
                else
                {
                    ScaleView.MinInterval = GetNextValueByIntervalType(ScaleView.Minimum, intervalType, ScaleView.MinSize) - ScaleView.Minimum;
                }
            }
        }

        public float ScreenPosByValue(double Value)
        {
            double result = -1;
            int k = (IsReversed) ? -1 : 1;
            //if (CurrentSize > 0)
            //{
                if (Horizontal)
                {
                    result = Start.X + k * (Value - ScaleView.Minimum) * CurrentSize; // (End.X - Start.X) / (Maximum - Minimum);
                }
                else
                {
                    result = Start.Y - k * (Value - ScaleView.Minimum) * CurrentSize; // (Start.Y - End.Y) / (Maximum - Minimum);
                }
            //}
            //else
            //{
            //    if (Horizontal)
            //    {
            //        if (Value > ScaleView.Maximum) result = End.X; else if (Value < ScaleView.Minimum) result = Start.X;
            //    }
            //    else
            //    {
            //        if (Value > ScaleView.Maximum) result = Start.Y; else if (Value < ScaleView.Minimum) result = End.Y;
            //    } 
            //}
            return Convert.ToSingle(result);
        }
        public double ValueBySreenPos(float Screen) // Проверить Double.NAN
        {
            double result = Constant.DOUBLE_NAN;
            if (CurrentSize > 0)
            {
                double k = (IsReversed) ? -1.0 : 1.0;
                if (Horizontal)
                {
                    result = Math.Round(ScaleView.Minimum + k * (Screen - Start.X) / CurrentSize, 3);
                }
                else
                {
                    result = Math.Round(ScaleView.Minimum + k * (Start.Y - Screen) / CurrentSize, 3);
                }
            }
            return result;
        }
        public double GetNextValue(double Value)
        {
            double next = Value;
            switch (this.ValueType)
            {
                case ChartValueType.Double:
                    next = Value + this.Interval;
                    break;
                case ChartValueType.DateTime:
                    switch (this.IntervalType)
                    {
                        case DateTimeIntervalType.Years:
                            next = (DateTime.FromOADate(Value).AddYears((int)this.Interval)).ToOADate();
                            break;
                        case DateTimeIntervalType.Months:
                            next = (DateTime.FromOADate(Value).AddMonths((int)this.Interval)).ToOADate();
                            break;
                        case DateTimeIntervalType.Days:
                            next = (DateTime.FromOADate(Value).AddDays((int)this.Interval)).ToOADate();
                            break;
                        case DateTimeIntervalType.Hours:
                            next = (DateTime.FromOADate(Value).AddHours((int)this.Interval)).ToOADate();
                            break;
                        case DateTimeIntervalType.Minutes:
                            next = (DateTime.FromOADate(Value).AddMinutes((int)this.Interval)).ToOADate();
                            break;
                        case DateTimeIntervalType.Seconds:
                            next = (DateTime.FromOADate(Value).AddSeconds((int)this.Interval)).ToOADate();
                            break;
                        case DateTimeIntervalType.Milliseconds:
                            next = (DateTime.FromOADate(Value).AddMilliseconds((int)this.Interval)).ToOADate();
                            break;
                        case DateTimeIntervalType.Auto:
                            next = Value + 1;
                            break;
                    }
                    break;
                case ChartValueType.Int32:
                    next = Value + (int)this.Interval;
                    break;
            }
            return next;
        }
        public double GetNextValueByIntervalType(double Value, DateTimeIntervalType Type, double Interval)
        {
            double next = Value;
            if (Interval > 0)
            {
                switch (this.ValueType)
                {
                    case ChartValueType.DateTime:
                        switch (Type)
                        {
                            case DateTimeIntervalType.Years:
                                next = (DateTime.FromOADate(Value).AddYears((int)Interval)).ToOADate();
                                break;
                            case DateTimeIntervalType.Months:
                                next = (DateTime.FromOADate(Value).AddMonths((int)Interval)).ToOADate();
                                break;
                            case DateTimeIntervalType.Days:
                                next = (DateTime.FromOADate(Value).AddDays((int)Interval)).ToOADate();
                                break;
                            case DateTimeIntervalType.Hours:
                                next = (DateTime.FromOADate(Value).AddHours((int)Interval)).ToOADate();
                                break;
                            case DateTimeIntervalType.Minutes:
                                next = (DateTime.FromOADate(Value).AddMinutes((int)Interval)).ToOADate();
                                break;
                            case DateTimeIntervalType.Seconds:
                                next = (DateTime.FromOADate(Value).AddSeconds((int)Interval)).ToOADate();
                                break;
                            case DateTimeIntervalType.Milliseconds:
                                next = (DateTime.FromOADate(Value).AddMilliseconds((int)Interval)).ToOADate();
                                break;
                            case DateTimeIntervalType.Auto:
                                next = Value + Interval;
                                break;
                        }
                        break;
                    default:
                        next = Value + Interval;
                        break;
                }
            }
            return next;
        }
        public double GetFirstValueByIntervaltype(double Value, DateTimeIntervalType Type)
        {
            double first = Value;
            switch (this.ValueType)
            {
                case ChartValueType.Double:
                    first = Value;
                    break;
                case ChartValueType.DateTime:
                    DateTime dt = DateTime.FromOADate(Value);
                    switch (Type)
                    {
                        case DateTimeIntervalType.Years:
                            first = (new DateTime(dt.Year, 1, 1)).ToOADate();
                            break;
                        case DateTimeIntervalType.Months:
                            first = (new DateTime(dt.Year, dt.Month, 1)).ToOADate();
                            break;
                        case DateTimeIntervalType.Days:
                            first = (new DateTime(dt.Year, dt.Month, dt.Day)).ToOADate();
                            break;
                        case DateTimeIntervalType.Hours:
                            first = (new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0)).ToOADate();
                            break;
                        case DateTimeIntervalType.Minutes:
                            first = (new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0)).ToOADate();
                            break;
                        case DateTimeIntervalType.Seconds:
                            first = (new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0)).ToOADate();
                            break;
                        case DateTimeIntervalType.Milliseconds:
                            first = (new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Millisecond)).ToOADate();
                            break;
                        case DateTimeIntervalType.Auto:
                            first = Value;
                            break;
                    }
                    break;
                default:
                    first = Value;
                    break;
            }
            return first;
        }

        public void DrawObject(Graphics grfx)
        {
            if ((this.Enabled) && (Size > 0) && (this.Minimum != Constant.DOUBLE_NAN) && (this.Maximum != Constant.DOUBLE_NAN))
            {
                DrawTitle(grfx);
                DrawTickMark(grfx, MinorTickmark);
                DrawTickMark(grfx, MajorTickMark);
                DrawLabels(grfx);
                DrawCustomLabels(grfx);
                grfx.DrawLine(line_pen, Start.X, Start.Y, End.X, End.Y);
            }
        }
        void DrawTitle(Graphics grfx)
        {
            float x = Start.X, y = Start.Y;
            float dTM = 0;
            if (dTM < MinorTickmark.OutsideSize) dTM = MinorTickmark.OutsideSize;
            if (dTM < MajorTickMark.OutsideSize) dTM = MajorTickMark.OutsideSize;
            string str = title;
            if (unit != "") str += ", " + unit;
            int k = (IsReversed) ? -1 : 1;
            SizeF custSize = CustomLabels.GetGraphicsSize(grfx);
            switch (this.Position)
            {
                case ChartPosition.Top:
                    y = Start.Y - TitleSize.Height - dTM - LabelsSize.Height - custSize.Height;
                    break;
                case ChartPosition.Bottom:
                    y = Start.Y + dTM + LabelsSize.Height - 2 + custSize.Height;
                    break;
                case ChartPosition.Left:
                    x = Start.X - TitleSize.Height - dTM - LabelsSize.Width - custSize.Height;
                    break;
                case ChartPosition.Right:
                    x = Start.X + dTM + LabelsSize.Width + custSize.Height;
                    break;
            }

            if (this.Horizontal)
            {
                #region Draw Title
                x = Start.X;
                switch (this.TitleAlignment)
                {
                    case StringAlignment.Center:
                        x = Start.X + k * Size / 2 - TitleSize.Width / 2;
                        break;
                    case StringAlignment.Far:
                        x = End.X - k * TitleSize.Width / 2;
                        break;
                }
                grfx.DrawString(str, TitleFont, title_brush, x, y, StringFormat.GenericTypographic);
                #endregion
            }
            else
            {
                #region Draw Title
                y = Start.Y;
                switch (this.TitleAlignment)
                {
                    case StringAlignment.Center:
                        y = Start.Y - k * Size / 2 + (TitleSize.Width / 2);
                        break;
                    case StringAlignment.Far:
                        y = End.Y + k * TitleSize.Width / 2;
                        break;
                }
                Matrix savedTrasform = grfx.Transform;
                grfx.TranslateTransform(x, y);
                grfx.RotateTransform(-90);
                grfx.DrawString(str, TitleFont, title_brush, PointF.Empty, StringFormat.GenericTypographic);
                grfx.Transform = savedTrasform;
                #endregion
            }
        }
        void DrawTickMark(Graphics grfx, TickMark tickMark)
        {
            #region Init Position
            float x = 0, y = 0;
            switch (this.Position)
            {
                case ChartPosition.Top:
                    switch (tickMark.TickMarkStyle)
                    {
                        case TickMarkStyle.InsideArea:
                            y = End.Y;
                            break;
                        case TickMarkStyle.OutsideArea:
                            y = End.Y - tickMark.Size;
                            break;
                        case TickMarkStyle.AcrossAxis:
                            y = End.Y - (tickMark.Size / 2);
                            break;
                    }
                    break;
                case ChartPosition.Bottom:
                    switch (tickMark.TickMarkStyle)
                    {
                        case TickMarkStyle.OutsideArea:
                            y = Start.Y;
                            break;
                        case TickMarkStyle.InsideArea:
                            y = Start.Y - tickMark.Size;
                            break;
                        case TickMarkStyle.AcrossAxis:
                            y = Start.Y - (tickMark.Size / 2);
                            break;
                    }
                    break;
                case ChartPosition.Left:
                    switch (tickMark.TickMarkStyle)
                    {
                        case TickMarkStyle.InsideArea:
                            x = Start.X;
                            break;
                        case TickMarkStyle.OutsideArea:
                            x = Start.X - tickMark.Size;
                            break;
                        case TickMarkStyle.AcrossAxis:
                            x = Start.X - (tickMark.Size / 2);
                            break;
                    }
                    break;
                case ChartPosition.Right:
                    switch (tickMark.TickMarkStyle)
                    {
                        case TickMarkStyle.OutsideArea:
                            x = End.X;
                            break;
                        case TickMarkStyle.InsideArea:
                            x = End.X - tickMark.Size;
                            break;
                        case TickMarkStyle.AcrossAxis:
                            x = End.X - (tickMark.Size / 2);
                            break;
                    }
                    break;
            }
            #endregion

            if ((tickMark.TickMarkStyle != TickMarkStyle.None) && (tickMark.Enabled))
            {
                float scr;
                bool hor = this.Horizontal;
                bool DrawFirst = false;
                if ((tickMark.FixedCount > -1) && (ScaleView.Size > 0))
                {
                    float min = ScreenPosByValue(ScaleView.Minimum);
                    float max = ScreenPosByValue(ScaleView.Maximum);
                    float h = (max - min) / tickMark.FixedCount;
                    scr = min;
                    for (int i = 0; i <= tickMark.FixedCount; i++)
                    {
                        if (hor)
                        {
                            grfx.DrawLine(Pens.Black, scr, (int)y, scr, (int)y + tickMark.Size);
                        }
                        else
                        {
                            grfx.DrawLine(Pens.Black, (int)x, scr, (int)x + tickMark.Size, scr);
                        }
                        scr += h;
                    }
                }
                else
                {
                    double first = GetFirstValueByIntervaltype(this.Minimum, tickMark.IntervalType);
                    first = GetNextValueByIntervalType(first, tickMark.IntervalOffsetType, tickMark.IntervalOffset);
                    double val = first, maxVal = this.Maximum;
                    while (val <= this.Maximum)
                    {
                        if (ScaleView.ContainsValue(val))
                        {
                            DrawFirst = true;
                            scr = ScreenPosByValue(val);
                            if (scr > -1)
                            {
                                if (hor)
                                {
                                    grfx.DrawLine(Pens.Black, scr, y, scr, y + tickMark.Size);
                                }
                                else
                                {
                                    grfx.DrawLine(Pens.Black, x, scr, x + tickMark.Size, scr);
                                }
                            }
                        }
                        else if (DrawFirst)
                        {
                            break;
                        }
                        val = GetNextValueByIntervalType(val, tickMark.IntervalType, tickMark.Interval);
                    }
                }
            }
        }
        void DrawLabels(Graphics grfx)
        {
            float dTM = 0;
            if (dTM < MinorTickmark.OutsideSize) dTM = MinorTickmark.OutsideSize;
            if (dTM < MajorTickMark.OutsideSize) dTM = MajorTickMark.OutsideSize;
            float scr;
            bool hor = this.Horizontal;
            SizeF sz = SizeF.Empty;
            float x = 0, y = 0;
            string str = "";
            float lastBound = (Horizontal) ? Start.X : Start.Y;
            bool checkBound;
            int k = (IsReversed) ? -1 : 1;
            if ((LabelStyle.Enabled) && (LabelStyle.Interval > 0))
            {
                if (LabelStyle.FixedCount > -1)
                {
                    double val;
                    float min = ScreenPosByValue(ScaleView.Minimum);
                    float max = ScreenPosByValue(ScaleView.Maximum);
                    float h = (max - min) / LabelStyle.FixedCount;
                    scr = min;
                    for (int i = 0; i <= LabelStyle.FixedCount; i++)
                    {

                        if (i < LabelStyle.FixedCount)
                        {
                            val = ValueBySreenPos(scr);
                        }
                        else
                        {
                            val = ScaleView.Maximum;
                        }
                        
                        if (val != Constant.DOUBLE_NAN)
                        {
                            switch (this.ValueType)
                            {
                                case ChartValueType.DateTime:
                                    str = DateTime.FromOADate(val).ToString(LabelStyle.Format);
                                    break;
                                default:
                                    str = val.ToString(LabelStyle.Format);
                                    break;
                            }
                            sz = grfx.MeasureString(str, LabelStyle.Font, PointF.Empty, StringFormat.GenericTypographic);
                            switch (Position)
                            {
                                case ChartPosition.Top:
                                    x = scr - sz.Width / 2;
                                    y = Start.Y - dTM - sz.Height + 1;
                                    break;
                                case ChartPosition.Bottom:
                                    x = scr - sz.Width / 2;
                                    y = Start.Y + dTM - 1;
                                    break;
                                case ChartPosition.Left:
                                    y = scr - sz.Height / 2;
                                    x = Start.X - dTM - sz.Width - 2;
                                    break;
                                case ChartPosition.Right:
                                    y = scr - sz.Height / 2;
                                    x = Start.X + dTM + 1;
                                    break;
                            }
                            if (y < 0) y = 0;
                            checkBound = true;
                            if (hor)
                            {
                                checkBound = (IsReversed) ? (x < lastBound - 2) : (x > lastBound + 2);
                            }
                            else
                            {
                                checkBound = (IsReversed) ? (y - sz.Height / 2 > lastBound) : (y + sz.Height / 2 < lastBound);
                            }
                            if (checkBound)
                            {
                                grfx.DrawString(str, LabelStyle.Font, Brushes.Black, x, y, StringFormat.GenericTypographic);
                                lastBound = (hor) ? scr + k * sz.Width / 2 : scr - k * sz.Height / 2;
                            }
                        }
                        scr += h;
                    }
                }
                else
                {
                    double first = GetFirstValueByIntervaltype(this.Minimum, LabelStyle.IntervalType);
                    first = GetNextValueByIntervalType(first, LabelStyle.IntervalOffsetType, LabelStyle.IntervalOffset);
                    double val = first, maxVal = this.Maximum;

                    bool DrawFirst = false;
                    while (val <= maxVal)
                    {
                        if (ScaleView.ContainsValue(val))
                        {
                            DrawFirst = true;
                            scr = ScreenPosByValue(val);
                            switch (this.ValueType)
                            {
                                case ChartValueType.DateTime:
                                    str = DateTime.FromOADate(val).ToString(LabelStyle.Format);
                                    break;
                                default:
                                    str = val.ToString(LabelStyle.Format);
                                    break;
                            }
                            sz = grfx.MeasureString(str, LabelStyle.Font, PointF.Empty, StringFormat.GenericTypographic);
                            switch (Position)
                            {
                                case ChartPosition.Top:
                                    x = scr - sz.Width / 2;
                                    y = Start.Y - dTM - sz.Height + 1;
                                    break;
                                case ChartPosition.Bottom:
                                    x = scr - sz.Width / 2;
                                    y = Start.Y + dTM - 1;
                                    break;
                                case ChartPosition.Left:
                                    y = scr - sz.Height / 2;
                                    x = Start.X - dTM - sz.Width - 2;
                                    break;
                                case ChartPosition.Right:
                                    y = scr - sz.Height / 2;
                                    x = Start.X + dTM + 1;
                                    break;
                            }
                            if (y < 0) y = 0;
                            checkBound = true;
                            if (hor)
                            {
                                checkBound = (IsReversed) ? ((scr + sz.Width / 2) < lastBound - 2) : ((scr - sz.Width / 2) > lastBound + 2);
                            }
                            else
                            {
                                checkBound = (IsReversed) ? (y - sz.Height / 2 > lastBound) : (y + sz.Height / 2 < lastBound);
                            }
                            if (checkBound)
                            {
                                grfx.DrawString(str, LabelStyle.Font, Brushes.Black, x, y, StringFormat.GenericTypographic);
                                lastBound = (hor) ? scr + k * sz.Width / 2 : scr - k * sz.Height / 2;
                            }
                        }
                        else if (DrawFirst)
                        {
                            break;
                        }
                        val = GetNextValueByIntervalType(val, LabelStyle.IntervalType, LabelStyle.Interval);
                    }
                }
            }
        }
        void DrawCustomLabels(Graphics grfx)
        {
            if ((CustomLabels.Enabled) && (CustomLabels.Count > 0))
            {
                float x = Start.X, y = Start.Y, scrX, scrY, scrNext;
                double next;

                float delta = 0;
                if (delta < MinorTickmark.OutsideSize) delta = MinorTickmark.OutsideSize;
                if (delta < MajorTickMark.OutsideSize) delta = MajorTickMark.OutsideSize;
                SizeF size = grfx.MeasureString("Custom", CustomLabels.FontLabels, PointF.Empty, StringFormat.GenericTypographic);
                int k = 1;
                bool hor = this.Horizontal;

                switch (this.Position)
                {
                    case ChartPosition.Top:
                        y = Start.Y - delta - LabelsSize.Height - size.Height /2;
                        k = 1;
                        break;
                    case ChartPosition.Bottom:
                        y = Start.Y + delta + LabelsSize.Height - 2 + size.Height /2;
                        k = -1;
                        break;
                    case ChartPosition.Left:
                        x = Start.X - delta - LabelsSize.Width - size.Height / 2;
                        k = 1;
                        break;
                    case ChartPosition.Right:
                        x = Start.X + delta + LabelsSize.Width + size.Height /2;
                        k = -1;
                        break;
                }
                Point BoundPt = CustomLabels.GetBoundIndex(ScaleView);

                for (int i = BoundPt.X; i < BoundPt.Y; i++)
                {
                    if (!CustomLabels[i].IsEmpty)
                    {
                        if (i < CustomLabels.Count - 1) next = CustomLabels[i + 1].Start; else next = this.Maximum;
                        scrNext = ScreenPosByValue(next);

                        size = grfx.MeasureString(CustomLabels[i].Text, CustomLabels.FontLabels, PointF.Empty, StringFormat.GenericTypographic);
                        if (hor)
                        {
                            scrX = ScreenPosByValue(CustomLabels[i].Start);

                            if (scrX < Start.X) scrX = Start.X;
                            if (scrNext > End.X) scrNext = End.X;

                            scrY = y;

                            if (ScaleView.ContainsValue(CustomLabels[i].Start))
                            {
                                grfx.DrawLine(Pens.Black, scrX, scrY, scrX, scrY + k * size.Height / 2 - k);
                            }
                            if ((scrNext - scrX) > size.Width)
                            {
                                grfx.DrawLine(Pens.Black, scrX, scrY, (scrNext + scrX) / 2 - size.Width / 2 - 2f, scrY);
                                grfx.DrawLine(Pens.Black, (scrNext + scrX) / 2 + size.Width / 2 + 2f, scrY, scrNext, scrY);
                                grfx.DrawString(CustomLabels[i].Text, TitleFont, title_brush, (scrNext + scrX) / 2 - size.Width / 2, y - size.Height / 2, StringFormat.GenericTypographic);
                            }
                            else if (scrNext > scrX)
                            {
                                grfx.DrawLine(Pens.Black, scrX, scrY, scrNext, scrY);
                            }
                            if ((i == CustomLabels.Count - 1) && (ScaleView.ContainsValue(max)))
                            {
                                grfx.DrawLine(Pens.Black, scrNext, scrY, scrNext, scrY + k * size.Height / 2 - k);
                            }
                        }
                        else
                        {
                            scrX = x;
                            scrY = ScreenPosByValue(CustomLabels[i].Start);
                            if (scrY > Start.Y) scrY = Start.Y;
                            if (scrNext < End.Y) scrNext = End.Y;

                            if (ScaleView.ContainsValue(CustomLabels[i].Start))
                            {
                                grfx.DrawLine(Pens.Black, scrX, scrY, scrX + k * size.Height / 2 - k, scrY);
                            }
                            if ((scrY - scrNext) > size.Width)
                            {
                                grfx.DrawLine(Pens.Black, scrX, scrY, scrX, (scrY + scrNext) / 2 + size.Width / 2 + 2f);
                                grfx.DrawLine(Pens.Black, scrX, (scrY + scrNext) / 2 - size.Width / 2 - 2f, scrX, scrNext);
                                grfx.TranslateTransform(x - size.Height / 2, (scrY + scrNext) / 2 + size.Width / 2);
                                grfx.RotateTransform(-90);
                                grfx.DrawString(CustomLabels[i].Text, CustomLabels.FontLabels, Brushes.Black, PointF.Empty, StringFormat.GenericTypographic);
                                grfx.ResetTransform();
                            }
                            else if (scrNext < scrY)
                            {
                                grfx.DrawLine(Pens.Black, scrX, scrY, scrX, scrNext);
                            }
                            if ((i == CustomLabels.Count - 1) && (ScaleView.ContainsValue(max)))
                            {
                                grfx.DrawLine(Pens.Black, scrX, scrNext, scrX + k * size.Height / 2 - k, scrNext);
                            }
                        }
                    }
                }
            }
        }
        public void DrawGridLines(Graphics grfx, float Size)
        {
            if ((Size > 0) && (grfx != null) && (this.Enabled) && (this.Size > 0) && (this.Minimum != Constant.DOUBLE_NAN) && (this.Maximum != Constant.DOUBLE_NAN))
            {
                DrawGrid(grfx, MinorGrid, Size);
                DrawGrid(grfx, MajorGrid, Size);
            }
        }
        public void DrawGrid(Graphics grfx, Grid grid, float Size)
        {
            if (grid.Enabled)
            {
                double val = this.Minimum, maxVal = this.Maximum;
                float scr, k = 1;
                float scrX = 0, scrY = 0, scrX2 = 0, scrY2 = 0;
                switch (Position)
                {
                    case ChartPosition.Top:
                        scrY = End.Y;
                        scrY2 = End.Y + Size;
                        break;
                    case ChartPosition.Bottom:
                        scrY = Start.Y - Size;
                        scrY2 = Start.Y;
                        break;
                    case ChartPosition.Left:
                        scrX = Start.X;
                        scrX2 = Start.X + Size;
                        break;
                    case ChartPosition.Right:
                        scrX = Start.X - Size;
                        scrX2 = Start.X;
                        break;
                }
                bool hor = Horizontal;
                Pen pen = new Pen(grid.LineColor, grid.LineWidth);
                pen.DashStyle = grid.LineDashStyle;

                if (grid.FixedCount > -1)
                {
                    float min = ScreenPosByValue(ScaleView.Minimum);
                    float max = ScreenPosByValue(ScaleView.Maximum);
                    float h = (max - min) / grid.FixedCount;
                    scr = min + h;
                    for (int i = 0; i < grid.FixedCount; i++)
                    {
                        if (hor) scrX2 = scrX = scr; else scrY2 = scrY = scr;
                        grfx.DrawLine(pen, scrX, scrY, scrX2, scrY2);
                        scr += h;
                    }
                }
                else
                {
                    bool DrawFirst = false;
                    double first = GetFirstValueByIntervaltype(this.Minimum, grid.IntervalType);
                    first = GetNextValueByIntervalType(first, grid.IntervalOffsetType, grid.IntervalOffset);
                    val = GetNextValueByIntervalType(first, grid.IntervalType, grid.Interval);
                    while (val < maxVal)
                    {
                        if (ScaleView.ContainsValue(val))
                        {
                            DrawFirst = true;
                            scr = ScreenPosByValue(val);
                            if (hor) scrX2 = scrX = scr; else scrY2 = scrY = scr;
                            grfx.DrawLine(pen, scrX, scrY, scrX2, scrY2);
                        }
                        else if (DrawFirst)
                        {
                            break;
                        }
                        val = GetNextValueByIntervalType(val, grid.IntervalType, grid.Interval);
                    }
                }
                pen.Dispose();
            }
        }
        public void DrawChartAreaLabels(Graphics grfx, float Size)
        {
            if ((this.Enabled) && (this.Size > 0) && (this.Minimum != Constant.DOUBLE_NAN) && (this.Maximum != Constant.DOUBLE_NAN))
            {
                float k = 1;

                if ((Size > 0) && (grfx != null))
                {
                    k = ((Position == ChartPosition.Left) || (Position == ChartPosition.Top)) ? 1 : -1;
                    if (ChartLabels.Count > 0)
                    {
                        float scrX, scrY, width;
                        SizeF sizeStr;
                        Brush brush = null;

                        bool hor = this.Horizontal;
                        int lastColor = -1;
                        for (int i = 0; i < ChartLabels.Count; i++)
                        {
                            if ((ChartLabels[i].Enabled) && (ScaleView.ContainsValue(ChartLabels[i].Position)))
                            {
                                if (ChartLabels[i].BackColor != -1)
                                {
                                    if(lastColor != ChartLabels[i].BackColor)
                                    {
                                        if (brush != null) brush.Dispose();
                                        brush = new SolidBrush(Color.FromArgb(ChartLabels[i].BackColor));
                                        lastColor = ChartLabels[i].BackColor;
                                    }
                                }
                                else
                                {
                                    if (brush != null) brush.Dispose();
                                    brush = new SolidBrush(ChartLabels.BackColor);
                                }
                                sizeStr = grfx.MeasureString(ChartLabels[i].Title, ChartLabels.FontLabels, PointF.Empty, StringFormat.GenericTypographic);
                                if (sizeStr.Width < Size)
                                {
                                    if (hor)
                                    {
                                        scrX = ScreenPosByValue(ChartLabels[i].Position) - sizeStr.Height / 2;

                                        width = (Size - sizeStr.Width - 4F) / 2;

                                        scrY = Start.Y;
                                        if (k == -1) scrY -= Size;
                                        grfx.FillRectangle(brush, scrX + sizeStr.Height / 4, scrY, sizeStr.Height / 2, width);

                                        grfx.FillRectangle(Brushes.White, scrX + sizeStr.Height / 4, scrY + width, sizeStr.Height / 2, sizeStr.Width + 4F);

                                        scrY += width + sizeStr.Width + 4F;
                                        grfx.FillRectangle(brush, scrX + sizeStr.Height / 4, scrY, sizeStr.Height / 2, width);

                                        scrY -= 2F;
                                        grfx.TranslateTransform(scrX, scrY);
                                        grfx.RotateTransform(-90);
                                        grfx.DrawString(ChartLabels[i].Title, ChartLabels.FontLabels, Brushes.Black, PointF.Empty, StringFormat.GenericTypographic);
                                        grfx.ResetTransform();
                                    }
                                    else
                                    {
                                        scrY = ScreenPosByValue(ChartLabels[i].Position) - sizeStr.Height / 2;

                                        width = (Size - sizeStr.Width - 4F) / 2;

                                        scrX = Start.X;
                                        if (k == -1) scrX -= Size;

                                        grfx.FillRectangle(brush, scrX, scrY, width, sizeStr.Height);

                                        grfx.FillRectangle(Brushes.White, scrX + width, scrY, sizeStr.Width + 4F, sizeStr.Height);

                                        scrX += width + sizeStr.Width + 4F;
                                        grfx.FillRectangle(brush, scrX, scrY, width, sizeStr.Height);

                                        scrX -= (sizeStr.Width + 2F);
                                        grfx.DrawString(ChartLabels[i].Title, ChartLabels.FontLabels, Brushes.Black, scrX, scrY, StringFormat.GenericTypographic);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public void DrawChartAreaColorZones(Graphics grfx, float Size)
        {
            if ((this.Enabled) && (this.Size > 0) && (this.Minimum != Constant.DOUBLE_NAN) && (this.Maximum != Constant.DOUBLE_NAN))
            {
                float k = 1;

                if ((Size > 0) && (grfx != null))
                {
                    k = ((Position == ChartPosition.Left) || (Position == ChartPosition.Top)) ? 1 : -1;
                    if (ChartColorZones.Count > 0)
                    {
                        float scrX1, scrX2, scrY1, scrY2;
                        Brush brush = null;

                        bool hor = this.Horizontal;
                        int lastColor = -1;
                        for (int i = 0; i < ChartColorZones.Count; i++)
                        {
                            if (ChartColorZones[i].Enabled && ScaleView.ContainsInterval(ChartColorZones[i].Start, ChartColorZones[i].End))
                            {
                                if (ChartColorZones[i].BackColor != -1)
                                {
                                    if (lastColor != ChartColorZones[i].BackColor)
                                    {
                                        if (brush != null) brush.Dispose();
                                        brush = new SolidBrush(Color.FromArgb(ChartColorZones[i].BackColor));
                                        lastColor = ChartColorZones[i].BackColor;
                                    }
                                }
                                else
                                {
                                    brush = new SolidBrush(Color.Gray);
                                }

                                if (hor)
                                {
                                    scrX1 = ScreenPosByValue(ChartColorZones[i].Start);
                                    scrX2 = ScreenPosByValue(ChartColorZones[i].End);
                                    if (scrX2 - scrX1 < 1) scrX2 = scrX1 + 1;
                                    scrY1 = Start.Y;
                                    if (k == -1) scrY1 -= Size;
                                    grfx.FillRectangle(brush, scrX1, scrY1, scrX2 - scrX1, Size);
                                }
                                else
                                {
                                    scrY1 = ScreenPosByValue(ChartColorZones[i].Start);
                                    scrY2 = ScreenPosByValue(ChartColorZones[i].End);
                                    if (scrY2 - scrY1 < 1) scrY2 = scrY1 + 1;
                                    scrX1 = Start.X;
                                    if (k == -1) scrX1 -= Size;

                                    grfx.FillRectangle(brush, scrX1, scrY1, Size, scrY2 - scrY1);
                                }
                            }
                        }
                    }
                }
            }
        }
        public void DrawChartAxisLabels(Graphics grfx)
        {
            if ((this.Enabled) && (this.Size > 0) && (this.Minimum != Constant.DOUBLE_NAN) && (this.Maximum != Constant.DOUBLE_NAN))
            {
                float k = 1;

                if ((Size > 0) && (grfx != null))
                {
                    k = ((Position == ChartPosition.Left) || (Position == ChartPosition.Top)) ? 1 : -1;

                    if (AxisLabels.Count > 0)
                    {
                        float scrX, scrY;
                        Brush brush = null;
                        Pen pen = null;

                        bool hor = this.Horizontal;
                        int lastBorderWidth = 0;
                        Color lastColor = Color.White, lastBorderColor = Color.White;

                        for (int i = 0; i < AxisLabels.Count; i++)
                        {
                            if ((AxisLabels[i].Enabled) && (ScaleView.ContainsValue(AxisLabels[i].Position)))
                            {
                                if ((lastColor != AxisLabels[i].Marker.Color) || (brush == null))
                                {
                                    if (brush != null) brush.Dispose();
                                    brush = new SolidBrush(AxisLabels[i].Marker.Color);
                                    lastColor = AxisLabels[i].Marker.Color;
                                }
                                if ((lastBorderColor != AxisLabels[i].Marker.BorderColor) || (lastBorderWidth != AxisLabels[i].Marker.BorderWidth) || (pen == null))
                                {
                                    if (pen != null) pen.Dispose();
                                    pen = new Pen(AxisLabels[i].Marker.BorderColor, AxisLabels[i].Marker.BorderWidth);
                                    lastBorderColor = AxisLabels[i].Marker.BorderColor;
                                    lastBorderWidth = AxisLabels[i].Marker.BorderWidth;
                                }
                                if(hor)
                                {
                                    scrX = ScreenPosByValue(AxisLabels[i].Position);
                                    scrY = Start.Y;
                                }
                                else
                                {
                                    scrX = Start.X;
                                    scrY = this.ScreenPosByValue(AxisLabels[i].Position);
                                }
                                AxisLabels[i].Marker.Draw(grfx, brush, pen, scrX, scrY);
                            }
                        }
                    }
                }
            }
        }
    }

    public sealed class AxisCustomLabels
    {
        List<CustomLabelItem> Items;
        public bool Enabled;
        public int Count { get { return Items.Count; } }
        public Font FontLabels;

        public AxisCustomLabels()
        {
            this.Enabled = false;
            Items = new List<CustomLabelItem>(5);
            FontLabels = new Font("Calibri", 8.25f);
        }

        public Point GetBoundIndex(AxisScaleView ScaleView)
        {
            Point pt = new Point(-1, -1);
            for (int i = 0; i < Items.Count; i++)
            {
                if (ScaleView.ContainsValue(this[i].Start))
                {
                    if (pt.X == -1) pt.X = i - 1;
                }
                else if (pt.X > -1)
                {
                    pt.Y = i + 1;
                }
            }
            if(pt.X < 0) pt.X = 0;
            if((pt.Y == -1) || (pt.Y > Count)) pt.Y = Count;
            return pt;
        }
        public SizeF GetGraphicsSize(Graphics grfx)
        {
            SizeF size = SizeF.Empty;
            if ((this.Enabled) && (grfx != null) && (Count > 0))
            {
                for(int i = 0; i < Count; i ++)
                {
                    if (this[i].Text != "")
                    {
                        size = grfx.MeasureString(this[i].Text, FontLabels, PointF.Empty, StringFormat.GenericTypographic);
                        size.Height -= 2;
                        break;
                    }
                }
            }
            return size;
        }
        public CustomLabelItem this[int index]
        {
            set
            {
                if ((index > -1) && (index < Items.Count))
                {
                    Items[index] = value;
                }
            }
            get
            {
                if ((index > -1) && (index < Items.Count))
                {
                    return Items[index];
                }
                return CustomLabelItem.Empty;
            }
        }
        public void AddLabel(double AxisValue, string Text)
        {
            if (!Enabled) Enabled = true;
            CustomLabelItem item;
            item.Start = AxisValue;
            item.Text = Text;
            Items.Add(item);
        }
        public void Clear()
        {
            Enabled = false;
            Items.Clear();
        }
    }

    public sealed class ChartAreaLabels
    {
        ChartAxis Axis;
        List<ChartAreaLabelItem> Items;
        public int Count { get { return Items.Count; } }
        public Color BackColor;
        public Font FontLabels;

        public ChartAreaLabels(ChartAxis Axis)
        {
            this.Axis = Axis;
            Items = new List<ChartAreaLabelItem>(5);
            FontLabels = new Font("Calibri", 10f);
            BackColor = Color.FromArgb(50, 150, 150, 150);
        }

        public ChartAreaLabelItem this[int index]
        {
            set
            {
                if ((index > -1) && (index < Items.Count))
                {
                    Items[index] = value;
                }
            }
            get
            {
                if ((index > -1) && (index < Items.Count))
                {
                    return Items[index];
                }
                return ChartAreaLabelItem.Empty;
            }
        }
        public void AddLabel(bool Enabled, int DataType, double AxisValue, string Title, string Text, Color clr)
        {
            ChartAreaLabelItem item;
            item.Enabled = Enabled;
            item.DataType = DataType;
            item.Position = AxisValue;
            item.Title = Title;
            item.Text = Text;
            item.BackColor = clr.ToArgb();
            Axis.SetMinMax(AxisValue, AxisValue);
            if (Items.Count > 0 && DateTime.FromOADate(Items[Items.Count -1].Position) == DateTime.FromOADate(AxisValue))
            {
                item.Position = DateTime.FromOADate(AxisValue).AddHours(6).ToOADate();
            }
            Items.Add(item);
        }
        public void SetLabelsEnabled(int DataType, bool Enabled)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Items[i].DataType == DataType)
                {
                    ChartAreaLabelItem item = Items[i];
                    item.Enabled = Enabled;
                    Items[i] = item;
                }
            }
        }
        public void Clear()
        {
            Items.Clear();
        }
    }

    public sealed class ChartAreaColorZones
    {
        List<ChartAreaColorZoneItem> Items;
        public int Count { get { return Items.Count; } }

        public ChartAreaColorZones()
        {
            Items = new List<ChartAreaColorZoneItem>(5);
        }

        public ChartAreaColorZoneItem this[int index]
        {
            set
            {
                if ((index > -1) && (index < Items.Count))
                {
                    Items[index] = value;
                }
            }
            get
            {
                if ((index > -1) && (index < Items.Count))
                {
                    return Items[index];
                }
                return ChartAreaColorZoneItem.Empty;
            }
        }
        public void AddZone(bool Enabled, int DataType, double StartAxisValue, double EndAxisValue, Color color, string Text)
        {
            ChartAreaColorZoneItem item;
            item.Enabled = Enabled;
            item.DataType = DataType;
            item.Text = Text;
            item.Start = StartAxisValue;
            item.End = EndAxisValue;
            item.BackColor = color.ToArgb();
            Items.Add(item);
        }
        public void SetColorZonesEnabled(int DataType, bool Enabled)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Items[i].DataType == DataType)
                {
                    ChartAreaColorZoneItem item = Items[i];
                    item.Enabled = Enabled;
                    Items[i] = item;
                }
            }
        }
        public void Clear()
        {
            Items.Clear();
        }
    }

    public sealed class ChartAxisLabels
    {
        List<ChartAxisLabelItem> Items;
        public bool Enabled;
        public int Count { get { return Items.Count; } }
        public Font FontLabels;

        public ChartAxisLabels()
        {
            this.Enabled = false;
            Items = new List<ChartAxisLabelItem>(5);
            FontLabels = new Font("Calibri", 10f);
        }

        public ChartAxisLabelItem this[int index]
        {
            set
            {
                if ((index > -1) && (index < Items.Count))
                {
                    Items[index] = value;
                }
            }
            get
            {
                if ((index > -1) && (index < Items.Count))
                {
                    return Items[index];
                }
                return ChartAxisLabelItem.Empty;
            }
        }
        public void AddLabel(bool Enabled, int DataType, MarkerStyle Marker, double AxisValue, string Title, string Text, Color clr)
        {
            if (!this.Enabled) this.Enabled = true;
            ChartAxisLabelItem item;
            item.Enabled = Enabled;
            item.DataType = DataType;
            item.Position = AxisValue;
            item.Title = Title;
            item.Text = Text;
            item.Marker = new ChartMarker();
            item.Marker.Style = Marker;
            item.Marker.Color = clr;
            item.Marker.BorderEnabled = true;
            item.Marker.Size = 8;
            Items.Add(item);
        }
        public void SetLabelsEnabled(int DataType, bool Enabled)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Items[i].DataType == DataType)
                {
                    ChartAxisLabelItem item = Items[i];
                    item.Enabled = Enabled;
                    Items[i] = item;
                }
            }
        }
        public void Clear()
        {
            Enabled = false;
            Items.Clear();
        }
    }

    public sealed class ChartSeries
    {
        public ChartAxis AxisX, AxisY;
        public ChartMarker Marker;

        public Color Color;
        public Color BorderColor;
        public DashStyle BorderDashStyle;
        public int BorderWidth;
        float colWidth;
        public float ColumnWidth
        {
            get { return colWidth; }
            set
            {
                colWidth = value;
                if (colWidth < 0.05f) colWidth = 0.05f;
            }
        }
        public SeriesChartType ChartType;
        public bool Enabled;
        public bool Visible;
        public bool IsStackedSeries
        {
            get
            {
                switch (this.ChartType)
                {
                    case SeriesChartType.Area:
                        return false;
                    case SeriesChartType.Column:
                        return false;
                    case SeriesChartType.Line:
                        return false;
                    case SeriesChartType.Point:
                        return false;
                    case SeriesChartType.StackedArea:
                        return true;
                    case SeriesChartType.StepLine:
                        return false;
                    default:
                        throw new ArgumentException("Неподдерживаемый тип графика.");
                }
            }
        }
        public bool IsXValueIndexed;
        public double MinX, MaxX;
        public double MinY, MaxY;
        public string SmallName, Name, Unit;
        public string ToolTipFormat = string.Empty;
        public List<DataPoint> Points;
        public bool IsEmpty
        {
            get { return (Points.Count == 0); }
        }
        public int GroupIndex;

        public ChartSeries()
        {
            this.AxisX = null;
            this.AxisY = null;

            this.Marker = new ChartMarker();

            this.Color = Color.Gray;
            this.BorderColor = Color.Black;
            this.BorderWidth = 1;
            this.BorderDashStyle = DashStyle.Solid;

            this.ColumnWidth = 0.6f;

            this.ChartType = SeriesChartType.Point;
            this.Enabled = true;
            this.Visible = true;
            this.IsXValueIndexed = true;

            this.SmallName = "";
            this.Name = "";
            this.Unit = "";
            this.Points = new List<DataPoint>();
            this.MinX = Constant.DOUBLE_NAN;
            this.MinY = Constant.DOUBLE_NAN;
            this.MaxX = Constant.DOUBLE_NAN;
            this.MaxY = Constant.DOUBLE_NAN;
            this.GroupIndex = -1;
        }

        public void GetStyle(ChartSeries series)
        {
            if (series != null)
            {
                this.ChartType = series.ChartType;
                this.Enabled = series.Enabled;
                this.Visible = series.Visible;
                this.Marker.GetStyle(series.Marker);
                this.Color = series.Color;
                this.BorderColor = series.BorderColor;
                this.BorderDashStyle = series.BorderDashStyle;
                this.BorderWidth = series.BorderWidth;
                this.ColumnWidth = series.ColumnWidth;
                this.IsXValueIndexed = series.IsXValueIndexed;
                this.SmallName = series.SmallName;
                this.Name = series.Name;
                this.Unit = series.Unit;
                this.ToolTipFormat = series.ToolTipFormat;
            }
        }

        public void SetAxis(ChartAxis AxisX, ChartAxis AxisY)
        {
            if ((AxisX != null) && (AxisY != null))
            {
                this.AxisX = AxisX;
                if (this.ChartType == SeriesChartType.StepLine) this.AxisX.AddLastInterval = true;
                this.AxisY = AxisY;
            }
            else
            {
                MessageBox.Show("Ошибка создания графика.");
            }
        }
        public void SetAxisMinMax()
        {
            if ((this.Enabled) && (this.Visible) && (MinX != Constant.DOUBLE_NAN) && (MinY != Constant.DOUBLE_NAN))
            {
                if (AxisX != null)
                {
                    AxisX.Enabled = true;
                    AxisX.SetMinMax(MinX, MaxX);
                }
                if (AxisY != null)
                {
                    AxisY.Enabled = true;
                    if (MinY != MaxY)
                    {
                        AxisY.SetMinMax(MinY, MaxY);
                    }
                    else if ((AxisY.Minimum == Constant.DOUBLE_NAN) || (AxisY.Maximum == Constant.DOUBLE_NAN))
                    {
                        AxisY.SetMinMax(MinY - MinY * 0.1, MinY + MinY * 0.1);
                    }
                    if (AxisY.Unit != "")
                    {
                        if (AxisY.Unit.IndexOf(this.Unit) == -1)
                        {
                            AxisY.Unit += "/" + this.Unit;
                        }
                    }
                    else
                    {
                        this.AxisY.Unit = this.Unit;
                    }

                }
            }
        }
        
        public void Insert(int index, double XValue, double YValue)
        {
            DataPoint dp;
            dp.XValue = XValue;
            dp.YValue = YValue;
            dp.YValues = null;
            dp.Operation = SeriesOperation.None;
            dp.Comment = null;
            Points.Insert(index, dp);
            if ((MinX == Constant.DOUBLE_NAN) || (MinX > XValue)) MinX = XValue;
            if ((MaxX == Constant.DOUBLE_NAN) || (MaxX < XValue)) MaxX = XValue;
            if ((MinY == Constant.DOUBLE_NAN) || (MinY > YValue)) MinY = YValue;
            if ((MaxY == Constant.DOUBLE_NAN) || (MaxY < YValue)) MaxY = YValue;

            if ((AxisX != null) && (Points.Count > 1) && (IsXValueIndexed) && (index + 1 < Points.Count))
            {
                double next = AxisX.GetNextValue(XValue);
                if (Math.Abs(next - Points[index + 1].XValue) > 1)
                {
                    IsXValueIndexed = false;
                }
            }
        }
        public void InsertArithmetic(int index, double XValue, double YValue1, double YValue2, SeriesOperation Operation)
        {
            DataPoint dp;
            dp.XValue = XValue;
            dp.YValues = new double[2];
            dp.YValues[0] = YValue1;
            dp.YValues[1] = YValue2;
            dp.YValue = dp.YValues[0];
            switch (Operation)
            {
                case SeriesOperation.Addition:
                    break;
                case SeriesOperation.Subtraction:
                    break;
                case SeriesOperation.Division:
                    if (dp.YValues[1] != 0)
                    {
                        dp.YValue = dp.YValues[0] / dp.YValues[1];
                    }
                    break;
                case SeriesOperation.Multiplication:
                    break;
            }
            dp.Operation = Operation;
            dp.Comment = null;
            Points.Insert(index, dp);
            if ((MinX == Constant.DOUBLE_NAN) || (MinX > XValue)) MinX = XValue;
            if ((MaxX == Constant.DOUBLE_NAN) || (MaxX < XValue)) MaxX = XValue;
            if ((MinY == Constant.DOUBLE_NAN) || (MinY > dp.YValue)) MinY = dp.YValue;
            if ((MaxY == Constant.DOUBLE_NAN) || (MaxY < dp.YValue)) MaxY = dp.YValue;

            if ((AxisX != null) && (Points.Count > 1) && (IsXValueIndexed) && (index + 1 < Points.Count))
            {
                double next = AxisX.GetNextValue(XValue);
                if (Math.Abs(next - Points[index + 1].XValue) > 1)
                {
                    IsXValueIndexed = false;
                }
            }
        }
        public void AddXY(double XValue, double YValue)
        {
            AddXY(XValue, YValue, null);
        }
        public void AddXY(double XValue, double YValue, string Comment)
        {
            DataPoint dp;
            dp.XValue = XValue;
            dp.YValue = YValue;
            dp.YValues = null;
            if (this.IsStackedSeries)
            {
                dp.YValues = new double[2];
                dp.YValues[0] = YValue;
            }
            dp.Operation = SeriesOperation.None;
            dp.Comment = Comment;
            Points.Add(dp);
            if ((MinX == Constant.DOUBLE_NAN) || (MinX > XValue)) MinX = XValue;
            if ((MaxX == Constant.DOUBLE_NAN) || (MaxX < XValue)) MaxX = XValue;
            if ((MinY == Constant.DOUBLE_NAN) || (MinY > YValue)) MinY = YValue;
            if ((MaxY == Constant.DOUBLE_NAN) || (MaxY < YValue)) MaxY = YValue;
            if ((AxisX != null) && (Points.Count > 1) && (IsXValueIndexed))
            {
                double next = AxisX.GetNextValue(Points[Points.Count - 2].XValue);
                if (Math.Abs(next - Points[Points.Count - 1].XValue) > AxisX.Interval)
                {
                    IsXValueIndexed = false;
                }
            }
        }
        public void AddArithmeticXY(double XValue, double YValue1, double YValue2, SeriesOperation Operation)
        {
            DataPoint dp;
            dp.XValue = XValue;
            dp.YValues = new double[2];
            dp.YValues[0] = YValue1;
            dp.YValues[1] = YValue2;
            dp.YValue = dp.YValues[0];
            switch (Operation)
            {
                case SeriesOperation.Addition:
                    break;
                case SeriesOperation.Subtraction:
                    break;
                case SeriesOperation.Division:
                    if (dp.YValues[1] != 0)
                    {
                        dp.YValue = dp.YValues[0] / dp.YValues[1];
                    }
                    break;
                case SeriesOperation.Multiplication:
                    break;
            }
            dp.Operation = Operation;
            dp.Comment = null;
            Points.Add(dp);
            if ((MinX == Constant.DOUBLE_NAN) || (MinX > XValue)) MinX = XValue;
            if ((MaxX == Constant.DOUBLE_NAN) || (MaxX < XValue)) MaxX = XValue;
            if ((MinY == Constant.DOUBLE_NAN) || (MinY > dp.YValue)) MinY = dp.YValue;
            if ((MaxY == Constant.DOUBLE_NAN) || (MaxY < dp.YValue)) MaxY = dp.YValue;
            if ((AxisX != null) && (Points.Count > 1) && (IsXValueIndexed))
            {
                double next = AxisX.GetNextValue(Points[Points.Count - 2].XValue);
                if (Math.Abs(next - Points[Points.Count - 1].XValue) > AxisX.Interval)
                {
                    IsXValueIndexed = false;
                }
            }
        }
        public void SetStackedValue(int index, double StackedValue)
        {
            if (index < Points.Count)
            {
                DataPoint dp = Points[index];
                if (dp.YValues == null)
                {
                    dp.YValues = new double[2];
                    dp.YValues[0] = dp.YValue;
                }
                dp.YValues[1] = StackedValue;
                dp.YValue = StackedValue + dp.YValues[0];
                if ((MinY == Constant.DOUBLE_NAN) || (MinY > dp.YValue)) MinY = dp.YValue;
                if ((MaxY == Constant.DOUBLE_NAN) || (MaxY < dp.YValue)) MaxY = dp.YValue;
                Points[index] = dp;
            }
        }
        public int GetNearestPointIndex(int screenX)
        {
            if (Points.Count > 0)
            {
                double valX = AxisX.ValueBySreenPos(screenX);
                if ((valX != Constant.DOUBLE_NAN) && (AxisX.ScaleView.Size > 0))
                {
                    if (IsXValueIndexed)
                    {
                        int i = (int)Math.Round(Points.Count * ((valX - MinX) / (MaxX - MinX)), 0);
                        if (i < 0) i = 0;
                        else if (i >= Points.Count) i = Points.Count - 1;
                        return i;
                    }
                    else
                    {
                        int i = (int)Math.Round(Points.Count * ((valX - MinX) / (MaxX - MinX)), 0);
                        if (i < 0) i = 0;
                        else if (i >= Points.Count) i = Points.Count - 1;
                        int k = (Points[i].XValue < valX ? 1 : -1);
                        int h = 32, j = 0;
                        j = i;
                        while (h >= 1)
                        {
                            while ((j > -1) && (j < Points.Count) && (k * Points[j].XValue < k * valX))
                            {
                                j += k * h;
                            }
                            if (j < 0) j = 0;
                            else if (j >= Points.Count) j = Points.Count - 1;

                            k = -k;
                            h = h / 2;
                        }
                        return j;
                    }
                }
            }
            return -1;
        }
        public Point GetAxisXBoundIndex()
        {
            Point pt = Point.Empty;
            int start = -1, end = -1;
            if (AxisX != null)
            {
                if (AxisX.ScaleView.Size > (AxisX.Maximum - AxisX.Minimum) / 2)
                {
                    for (int i = 0; i < Points.Count; i++)
                    {
                        if (AxisX.ScaleView.ContainsValue(Points[i].XValue))
                        {
                            if (start == -1)
                            {
                                start = i - 1;
                                if (start < 0) start = 0;
                            }
                        }
                        else if (start > -1)
                        {
                            end = i;
                            break;
                        }
                    }
                    if ((end == -1) || (end > Points.Count)) end = Points.Count; 
                }
                else
                {
                    for (int i = 0; i < Points.Count; i++)
                    {
                        if (Points[i].XValue >= AxisX.ScaleView.Minimum)
                        {
                            if (start == -1)
                            {
                                start = i - 1;
                                if (start < 0) start = 0;
                                break;
                            }
                        }
                    }
                    for (int i = Points.Count - 1; i >= 0; i--)
                    {
                        if (Points[i].XValue <= AxisX.ScaleView.Maximum)
                        {
                            if (end == -1)
                            {
                                end = i + 1;
                                break;
                            }
                        }
                    }
                }
            }
            if (start < 0) start = 0;
            if ((end == -1) || (end > Points.Count)) end = Points.Count; 
            pt.X = start;
            pt.Y = end;
            return pt;
        }
        public void SetNotZeroMinimum()
        {
            if ((AxisX != null) && (Points.Count > 0) && (AxisX.ScaleView.Minimum != Constant.DOUBLE_NAN))
            {
                for (int i = 0; i < Points.Count; i++)
                {
                    if (Points[i].YValue > 0)
                    {
                        if (Points[i].XValue < AxisX.ScaleView.Minimum)
                        {
                            AxisX.ScaleView.Minimum = Points[i].XValue;
                            AxisX.SetCurrentSize();
                        }
                        break;
                    }
                }
            }
        }

        #region DRAWING

        void DrawMarkers(Graphics grfx)
        {
            if (this.Marker.Style != MarkerStyle.None)
            {
                Brush brush = new SolidBrush(this.Marker.Color);
                Pen pen = new Pen(this.Marker.BorderColor, this.Marker.BorderWidth);
                float scrX, scrY;
                double val;
                Point BoundPt = GetAxisXBoundIndex();

                for (int i = BoundPt.X; i < BoundPt.Y; i++)
                {
                    val = Points[i].XValue;
                    if (this.ChartType == SeriesChartType.StepLine)
                    {
                        val =(val + AxisX.GetNextValue(Points[i].XValue)) / 2;
                    }
                    scrX = AxisX.ScreenPosByValue(val);
                    scrY = AxisY.ScreenPosByValue(Points[i].YValue);
                    this.Marker.Draw(grfx, brush, pen, scrX, scrY);
                }
                brush.Dispose();
            }
        }
        
        public void DrawBackObject(Graphics grfx)
        {
            if ((this.Enabled) && (this.Visible) && (Points.Count > 0) &&
                            (AxisX != null) && (AxisY != null) &&
                            (AxisX.ScaleView.Size > 0) && (AxisY.ScaleView.Size > 0))
            {
                switch (this.ChartType)
                {
                    case SeriesChartType.Column:
                        DrawColumnsBack(grfx);
                        break;
                    case SeriesChartType.StackedArea:
                        DrawStackedAreasBack(grfx);
                        break;
                    case SeriesChartType.Area:
                        DrawAreasBack(grfx);
                        break;
                }
            }
        }
        public void DrawObject(Graphics grfx)
        {
            if ((this.Enabled) && (this.Visible) && (Points.Count > 0) && 
                (AxisX != null) && (AxisY != null) && 
                (AxisX.ScaleView.Size > 0) && (AxisY.ScaleView.Size > 0))
            {
                switch (this.ChartType)
                {
                    case SeriesChartType.StepLine:
                        DrawStepLines(grfx);
                        break;
                    case SeriesChartType.Line:
                        DrawLines(grfx);
                        break;
                    case SeriesChartType.Column:
                        DrawColumns(grfx);
                        break;
                    case SeriesChartType.StackedArea:
                        DrawStackedAreas(grfx);
                        break;
                    case SeriesChartType.Area:
                        DrawAreas(grfx);
                        break;
                    case SeriesChartType.Point:
                        break;
                }
                DrawMarkers(grfx);
            }
        }
        
        // Отрисовка различных типов графиков
        void DrawStepLines(Graphics grfx)
        {
            int dx = (BorderWidth > 1) ? 1 : 0;
            float scrX, scrY, scrNext = 0, predY = 0, predX = 0;
            double next = Constant.DOUBLE_NAN;
            bool DrawConn = false;

            Pen pen = new Pen(this.Color, this.BorderWidth);
            pen.DashStyle = this.BorderDashStyle;
            Point BoundPt = GetAxisXBoundIndex();
            try
            {
                for (int i = BoundPt.X; i < BoundPt.Y; i++)
                {
                    if ((i > BoundPt.X) && DrawConn)
                    {
                        scrX = scrNext;
                    }
                    else
                    {
                        scrX = AxisX.ScreenPosByValue(Points[i].XValue);
                    }

                    scrY = AxisY.ScreenPosByValue(Points[i].YValue);
                    if (DrawConn)
                    {
                        grfx.DrawLine(pen, scrX, predY, scrX, scrY);
                    }
                    if (i < Points.Count - 1)
                    {
                        if (AxisX.ValueType == ChartValueType.DateTime)
                        {
                            next = AxisX.GetNextValue(Points[i].XValue);
                            if (Math.Abs(next - Points[i + 1].XValue) < 1) DrawConn = true; else DrawConn = false;
                        }
                        else
                        {
                            next = Points[i + 1].XValue;
                            DrawConn = true;
                        }
                    }
                    else
                    {
                        next = AxisX.GetNextValue(Points[i].XValue);
                        DrawConn = false;
                    }
                    scrNext = AxisX.ScreenPosByValue(next);
                    if (scrNext - scrX > 5 || predY != scrY)
                    {
                        grfx.DrawLine(pen, scrX - dx, scrY, scrNext + dx, scrY);
                    }
                    else
                    {
                        scrNext = scrX; 
                    }
                    predY = scrY;   
                }
            }
            catch
            {
                //MessageBox.Show("AV");
            }
            pen.Dispose();
        }
        void DrawLines(Graphics grfx)
        {
            float scrX = 0, scrY = 0, scrX2, scrY2;
            Pen pen = new Pen(this.Color, this.BorderWidth);
            pen.DashStyle = this.BorderDashStyle;
            Point BoundPt = GetAxisXBoundIndex();
            if (BoundPt.Y == Points.Count) BoundPt.Y = Points.Count - 1;

            for (int i = BoundPt.X; i < BoundPt.Y; i++)
            {
                if (i == BoundPt.X)
                {
                    scrX = AxisX.ScreenPosByValue(Points[i].XValue);
                    scrY = AxisY.ScreenPosByValue(Points[i].YValue);
                }
                scrX2 = AxisX.ScreenPosByValue(Points[i + 1].XValue);
                scrY2 = AxisY.ScreenPosByValue(Points[i + 1].YValue);
                grfx.DrawLine(pen, scrX, scrY, scrX2, scrY2);
                scrX = scrX2;
                scrY = scrY2;
            }
            pen.Dispose();
        }
        void DrawColumns(Graphics grfx)
        {
            float scrX, scrY, scrX2, scrY2;
            Pen pen = new Pen(this.BorderColor, this.BorderWidth);
            pen.DashStyle = this.BorderDashStyle;

            float AxisYmin = AxisY.ScreenPosByValue(AxisY.Minimum);
            float Height = 0, width1 = -1, width2 = 0;
            Point BoundPt = GetAxisXBoundIndex();
            if (BoundPt.Y < Points.Count - 1) BoundPt.Y++;
            bool diffColWidth = (ColumnWidth <= 1);
            if (!diffColWidth) width2 = ColumnWidth / 2f;

            for (int i = BoundPt.X; i < BoundPt.Y; i++)
            {
                scrX = AxisX.ScreenPosByValue(Points[i].XValue);
                scrX2 = AxisX.ScreenPosByValue(AxisX.GetNextValue(Points[i].XValue));
                if (diffColWidth)
                {
                    width2 = ((scrX2 - scrX) * ColumnWidth / 2f);
                }

                if (width1 == -1) width1 = width2;
                scrY = AxisYmin;
                scrY2 = AxisY.ScreenPosByValue(Points[i].YValue);
                
                if (AxisY.IsReversed)
                {
                    Height = scrY2 - AxisYmin;
                }
                else
                {
                    Height = AxisYmin - scrY2;
                    scrY = scrY2;
                }

                if ((BorderWidth > 0) && (BorderWidth < width1 + width2) && (width1 + width2 > 1))
                {
                    grfx.DrawRectangle(pen, scrX - width1, scrY2, width1 + width2, Height);
                }
                width1 = width2;
            }
            pen.Dispose();
        }
        void DrawColumnsBack(Graphics grfx)
        {
            float scrX, scrY, scrX2, scrY2;
            Brush brush = new SolidBrush(this.Color);
            Pen penBack = new Pen(this.Color);

            float AxisYmin = AxisY.ScreenPosByValue(AxisY.Minimum);
            float Height = 0, width1 = -1, width2 = 0;
            Point BoundPt = GetAxisXBoundIndex();
            if (BoundPt.Y < Points.Count - 1) BoundPt.Y++;
            bool diffColWidth = (ColumnWidth <= 1);
            if (!diffColWidth) width2 = ColumnWidth / 2f;

            for (int i = BoundPt.X; i < BoundPt.Y; i++)
            {
                scrX = AxisX.ScreenPosByValue(Points[i].XValue);
                scrX2 = AxisX.ScreenPosByValue(AxisX.GetNextValue(Points[i].XValue));
                if (diffColWidth)
                {
                    width2 = ((scrX2 - scrX) * ColumnWidth / 2f);
                }

                if (width1 == -1) width1 = width2;
                scrY = AxisYmin;
                scrY2 = AxisY.ScreenPosByValue(Points[i].YValue);

                if (AxisY.IsReversed)
                {
                    Height = scrY2 - AxisYmin;
                }
                else
                {
                    Height = AxisYmin - scrY2;
                    scrY = scrY2;
                }

                if (width1 + width2 > 1)
                {
                    grfx.FillRectangle(brush, scrX - width1, scrY, width1 + width2, Height);
                }
                else
                {
                    grfx.DrawLine(penBack, scrX, scrY, scrX, scrY + Height);
                }
                width1 = width2;
            }
            brush.Dispose();
            penBack.Dispose();
        }
        void DrawStackedAreas(Graphics grfx)
        {
            float scrX = 0, scrY = 0;
            Pen pen = new Pen(this.BorderColor, this.BorderWidth);
            pen.DashStyle = this.BorderDashStyle;
            ArrayList list = new ArrayList();
            PointF[] scrPoints = null;

            Point BoundPt = GetAxisXBoundIndex();

            if (BoundPt.Y < Points.Count) BoundPt.Y++;
            
            if (BoundPt.Y - BoundPt.X > 0)
            {
                int j = 0;
                scrPoints = new PointF[BoundPt.Y - BoundPt.X];
                for (int i = BoundPt.X; i < BoundPt.Y; i++)
                {
                    scrX = AxisX.ScreenPosByValue(Points[i].XValue);
                    scrY = AxisY.ScreenPosByValue(Points[i].YValue);

                    scrPoints[i - BoundPt.X].X = scrX;
                    scrPoints[i - BoundPt.X].Y = scrY;

                    j++;
                }
                if(scrPoints.Length > 1) grfx.DrawLines(pen, scrPoints);
            }
            pen.Dispose();
        }
        void DrawStackedAreasBack(Graphics grfx)
        {
            Brush brush = new SolidBrush(this.Color);
            PointF[] scrPoints = null;
            Point BoundPt = GetAxisXBoundIndex();

            if (BoundPt.Y < Points.Count) BoundPt.Y++;

            if (BoundPt.Y - BoundPt.X > 0)
            {
                int j = 0;
                scrPoints = new PointF[(BoundPt.Y - BoundPt.X) * 2];
                for (int i = BoundPt.X; i < BoundPt.Y; i++)
                {
                    scrPoints[j].X = AxisX.ScreenPosByValue(Points[i].XValue);
                    scrPoints[j].Y = AxisY.ScreenPosByValue(Points[i].YValue);
                    j++;
                }
                for (int i = BoundPt.Y - 1; i >= BoundPt.X; i--)
                {
                    if (Points[i].YValues != null)
                    {
                        scrPoints[j].X = AxisX.ScreenPosByValue(Points[i].XValue);
                        scrPoints[j].Y = AxisY.ScreenPosByValue(Points[i].YValues[1]);
                    }
                    j++;
                }
                grfx.FillPolygon(brush, scrPoints);
            }
            brush.Dispose();
        }
        void DrawAreas(Graphics grfx)
        {
            float scrX = 0, scrY = 0;
            Pen pen = new Pen(this.BorderColor, this.BorderWidth);
            pen.DashStyle = this.BorderDashStyle;
            ArrayList list = new ArrayList();
            PointF[] scrPoints = null;

            Point BoundPt = GetAxisXBoundIndex();
            if (BoundPt.Y < Points.Count) BoundPt.Y++;

            if (BoundPt.Y - BoundPt.X > 0)
            {
                int j = 0;
                scrPoints = new PointF[BoundPt.Y - BoundPt.X + 2];
                for (int i = BoundPt.X; i < BoundPt.Y; i++)
                {
                    scrX = AxisX.ScreenPosByValue(Points[i].XValue);
                    scrY = AxisY.ScreenPosByValue(Points[i].YValue);
                    if (j == 0)
                    {
                        scrPoints[j].X = scrX;
                        scrPoints[j].Y = AxisY.Start.Y;
                        j++;
                    }
                    scrPoints[j].X = scrX;
                    scrPoints[j].Y = scrY;
                    j++;
                    if (j == scrPoints.Length - 1)
                    {
                        scrPoints[j].X = scrX;
                        scrPoints[j].Y = AxisY.Start.Y;
                        break;
                    }
                }
                grfx.DrawPolygon(pen, scrPoints);
            }
            pen.Dispose();
        }
        void DrawAreasBack(Graphics grfx)
        {
            float scrX = 0, scrY = 0;
            Brush brush = new SolidBrush(this.Color);
            ArrayList list = new ArrayList();
            PointF[] scrPoints = null;

            Point BoundPt = GetAxisXBoundIndex();
            if (BoundPt.Y < Points.Count) BoundPt.Y++;

            if (BoundPt.Y - BoundPt.X > 0)
            {
                int j = 0;
                scrPoints = new PointF[BoundPt.Y - BoundPt.X + 2];
                for (int i = BoundPt.X; i < BoundPt.Y; i++)
                {
                    scrX = AxisX.ScreenPosByValue(Points[i].XValue);
                    scrY = AxisY.ScreenPosByValue(Points[i].YValue);
                    if (j == 0)
                    {
                        scrPoints[j].X = scrX;
                        scrPoints[j].Y = AxisY.Start.Y;
                        j++;
                    }
                    scrPoints[j].X = scrX;
                    scrPoints[j].Y = scrY;
                    j++;
                    if (j == scrPoints.Length - 1)
                    {
                        scrPoints[j].X = scrX;
                        scrPoints[j].Y = AxisY.Start.Y;
                        break;
                    }
                }
                grfx.FillPolygon(brush, scrPoints);
            }
            brush.Dispose();
        }
        #endregion
    }

    public sealed class ChartMarker
    {
        public MarkerStyle Style;
        public int Size;
        public Color Color;
        public bool BorderEnabled;
        public Color BorderColor;
        public int BorderWidth;

        public ChartMarker()
        {
            Style = MarkerStyle.None;
            Size = 6;
            Color = Color.Yellow;
            BorderEnabled = false;
            BorderColor = Color.Black;
            BorderWidth = 1;
        }

        public void GetStyle(ChartMarker marker)
        {
            this.Style = marker.Style;
            this.Size = marker.Size;
            this.Color = marker.Color;
            this.BorderEnabled = marker.BorderEnabled;
            this.BorderColor = marker.BorderColor;
            this.BorderWidth = marker.BorderWidth;
        }
        public void Draw(Graphics grfx, Brush brush, Pen borderPen, float X, float Y)
        {
            PointF[] points = null;
            float size = this.Size;
            switch (this.Style)
            {
                case MarkerStyle.Triangle:
                    size++;
                    points = new PointF[3];
                    float dy = (float)(Math.Sqrt(3) * size / 6);
                    points[0].X = X - size / 2F;
                    points[0].Y = Y + dy;
                    points[1].X = X + size / 2F;
                    points[1].Y = Y + dy;
                    points[2].X = X;
                    points[2].Y = Y - (float)(Math.Sqrt(3) * size / 3);
                    break;
                case MarkerStyle.Square:
                    grfx.FillRectangle(brush, X - size / 2, Y - size / 2, size, size);
                    if (BorderEnabled) grfx.DrawRectangle(borderPen, X - size / 2, Y - size / 2, size, size);
                    break;
                case MarkerStyle.Circle:
                    grfx.FillEllipse(brush, X - size / 2, Y - size / 2, size, size);
                    if (BorderEnabled) grfx.DrawEllipse(borderPen, X - size / 2, Y - size / 2, size, size);
                    break;
            }

            if (points != null)
            {
                grfx.FillPolygon(brush, points);
                if (BorderEnabled) grfx.DrawPolygon(borderPen, points);
            }
        }
    }

    public sealed class SeriesGroup
    {
        public int Index;
        public List<ChartSeries> SeriesList;
        public ChartAxis AxisX;

        public SeriesGroup(int Index, ChartAxis AxisX)
        {
            this.Index = Index;
            this.AxisX = AxisX;
            SeriesList = new List<ChartSeries>();
        }
        public void AddSeries(ChartSeries series)
        {
            if (series.AxisX == this.AxisX)
            {
                series.GroupIndex = this.Index;
                if (SeriesList.IndexOf(series) == -1)
                {
                    SeriesList.Add(series);
                }
            }
            else
            {
                throw new ArgumentException("Все графики группы должны быть на одной оси X.");
            }
        }
        public ChartSeries GetSeriesBefore(ChartSeries CurrentSeries)
        {
            for (int i = SeriesList.Count - 1; i >= 0; i--)
            {
                if ((CurrentSeries == SeriesList[i]) && (i > 0))
                {
                    return SeriesList[i - 1];
                }
            }
            return null;
        }
    }
    #endregion

    #region вспомогательные классы
    public struct DataPoint
    {
        public double XValue;
        public double YValue;
        public double[] YValues;
        public SeriesOperation Operation;
        public string Comment;
        public bool IsEmpty
        {
            get
            {
                if ((this.XValue == Constant.DOUBLE_NAN) || (this.YValue == Constant.DOUBLE_NAN)) 
                {
                    return true;
                }
                return false;
            }
        }
        public static DataPoint Empty
        {
            get
            {
                DataPoint item;
                item.XValue = Constant.DOUBLE_NAN;
                item.YValue = Constant.DOUBLE_NAN;
                item.YValues = null;
                item.Comment = null;
                item.Operation = SeriesOperation.None;
                return item;
            }
        }
    }

    public sealed class ChartAreaCollection : ArrayList
    {
        public ChartAreaCollection(int Count) : base(Count) { }
        public ChartArea this[int index]
        {
            get
            {
                if ((index > -1) && (index < base.Count))
                {
                    return (ChartArea)base[index];
                }
                return null;
            }
            set
            {
                base[index] = value;
            }
        }
        public int Add()
        {
            ChartArea chart = new ChartArea();
            chart.Index = base.Count;
            base.Add(chart);
            return (base.Count - 1); 
        }
    }

    public sealed class ChartSeriesCollection : ArrayList
    {
        public bool SeriesVisibleFixed;
        public ChartSeries this[int index]
        {
            get 
            {
                if ((index > -1) && (index < base.Count))
                {
                    return (ChartSeries)base[index];
                }
                return null;
            }
            set { base[index] = value; }
        }

        public ChartSeriesCollection()
        {
            SeriesVisibleFixed = false;
        }
    }

    public class Grid
    {
        public bool Enabled;
        public double Interval;
        public double IntervalOffset;
        public DateTimeIntervalType IntervalOffsetType;
        public DateTimeIntervalType IntervalType;
        public Color LineColor;
        public DashStyle LineDashStyle;
        public int LineWidth;
        public int FixedCount;
        public Grid()
        {
            Enabled = false;
            Interval = 1;
            IntervalOffset = 0;
            LineColor = Color.LightGray;
            LineDashStyle = DashStyle.Dash;
            LineWidth = 1;
            FixedCount = -1;
        }

        public void GetStyle(Grid grid)
        {
            this.Enabled = grid.Enabled;
            this.Interval = grid.Interval;
            this.IntervalOffset = grid.IntervalOffset;
            this.IntervalOffsetType = grid.IntervalOffsetType;
            this.IntervalType = grid.IntervalType;
            this.LineColor = grid.LineColor;
            this.LineDashStyle = grid.LineDashStyle;
            this.LineWidth = grid.LineWidth;
            this.FixedCount = grid.FixedCount;
        }
    }

    public sealed class TickMark : Grid
    {
        public float Size;
        public float InsideSize
        {
            get
            {
                float res = 0;
                if (this.Enabled)
                {
                    switch (this.TickMarkStyle)
                    {
                        case TickMarkStyle.AcrossAxis:
                            res = this.Size / 2;
                            break;
                        case TickMarkStyle.InsideArea:
                            res = this.Size;
                            break;
                    }
                }
                return res;
            }
        }
        public float OutsideSize
        {
            get
            {
                float res = 0;
                if (this.Enabled)
                {
                    switch (this.TickMarkStyle)
                    {
                        case TickMarkStyle.AcrossAxis:
                            res = this.Size / 2;
                            break;
                        case TickMarkStyle.OutsideArea:
                            res = this.Size;
                            break;
                    }
                }
                return res;
            }
        }
        public TickMarkStyle TickMarkStyle;
        public TickMark() : base()
        {
            Enabled = false;
            Size = 1F;
            TickMarkStyle = TickMarkStyle.AcrossAxis;
            FixedCount = -1;
        }

        public void GetStyle(TickMark tickMark)
        {
            this.Enabled = tickMark.Enabled;
            this.Interval = tickMark.Interval;
            this.IntervalOffset = tickMark.IntervalOffset;
            this.IntervalOffsetType = tickMark.IntervalOffsetType;
            this.IntervalType = tickMark.IntervalType;
            this.LineColor = tickMark.LineColor;
            this.LineDashStyle = tickMark.LineDashStyle;
            this.LineWidth = tickMark.LineWidth;
            this.FixedCount = tickMark.FixedCount;
            this.Size = tickMark.Size;
            this.TickMarkStyle = tickMark.TickMarkStyle;
            this.FixedCount = tickMark.FixedCount;
        }
    }

    public sealed class LabelStyle
    {
        public int Angle;
        public bool Enabled;
        public Font Font;
        public Color ForeColor;
        public string Format;
        public double Interval;
        public double IntervalOffset;
        public DateTimeIntervalType IntervalOffsetType;
        public DateTimeIntervalType IntervalType;
        public bool IsEndLabelVisible;
        public bool IsStaggered;
        public bool TruncatedLabels;
        public int FixedCount;
        public LabelStyle()
        {
            this.Angle = 0;
            this.Enabled = false;
            this.Font = new Font("Calibri", 8.25F);
            this.ForeColor = Color.Black;
            this.Format = "";
            this.Interval = 0;
            this.IntervalOffset = 0;
            this.IntervalOffsetType = DateTimeIntervalType.NotSet;
            this.IntervalType = DateTimeIntervalType.NotSet;
            this.IsEndLabelVisible = false;
            this.IsStaggered = false;
            this.TruncatedLabels = false;
            this.FixedCount = -1;
        }
        public void GetStyle(LabelStyle style)
        {
            this.Angle = style.Angle;
            this.Enabled = style.Enabled;
            this.Font = style.Font;
            this.ForeColor = style.ForeColor;
            this.Interval = style.Interval;
            this.IntervalOffset = style.IntervalOffset;
            this.IntervalOffsetType = style.IntervalOffsetType;
            this.IntervalType = style.IntervalType;
            this.IsEndLabelVisible = style.IsEndLabelVisible;
            this.IsStaggered = style.IsStaggered;
            this.TruncatedLabels = style.TruncatedLabels;
            this.FixedCount = style.FixedCount; 
        }
    }

    public sealed class AxisScaleView
    {
        public bool IsZoomed;
        public double Minimum;
        public double Maximum;
        public double MinSize;
        public double MinInterval;
        public double Size
        {
            get { return Math.Round(Maximum - Minimum, 3); }
        }
        ArrayList Items;

        public AxisScaleView()
        {
            Items = new ArrayList(3);
            IsZoomed = false;
            MinSize = 1;
            MinInterval = 0;
            Minimum = Constant.DOUBLE_NAN;
            Maximum = Constant.DOUBLE_NAN;
        }

        public bool ContainsValue(double Value)
        {
            if ((Value >= Minimum) && (Value <= Maximum))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ContainsInterval(double Start, double End)
        {
            return !((End < Minimum) || (Start > Maximum));
        }
        public void Zoom(double viewStart, double viewEnd)
        {
            DataPoint dp;
            dp.XValue = Minimum;
            dp.YValue = Maximum;
            dp.YValues = null;
            dp.Operation = SeriesOperation.None;
            dp.Comment = null;
            Items.Add(dp);
            this.Minimum = viewStart;
            this.Maximum = viewEnd;
            IsZoomed = true;
        }
        public void ZoomReset()
        {
            if (IsZoomed)
            {
                DataPoint dp = (DataPoint)Items[0];
                Minimum = dp.XValue;
                Maximum = dp.YValue;
                Items.Clear();
                IsZoomed = false;
            }
        }
        public void ZoomReset(int numberOfViews)
        {
            if ((IsZoomed) && (numberOfViews <= Items.Count))
            {
                DataPoint dp = (DataPoint)Items[Items.Count - numberOfViews];
                int i = Items.Count - 1;
                while (numberOfViews > 0)
                {
                    Items.RemoveAt(i);
                    i--;
                    numberOfViews--;
                }
                if (Items.Count == 0) IsZoomed = false;
                Minimum = dp.XValue;
                Maximum = dp.YValue;
            }
        }
    }

    public struct CustomLabelItem
    {
        public double Start;
        public string Text;

        public bool IsEmpty
        {
            get 
            {
                if (this.Text == null) return true;
                return false;
            }
        }
        public static CustomLabelItem Empty
        {
            get
            {
                CustomLabelItem item;
                item.Start = 0;
                item.Text = null;
                return item;
            }
        }
    }
    public struct ChartAreaLabelItem
    {
        public bool Enabled;
        public int DataType;
        public double Position;
        public string Title;
        public string Text;
        public int BackColor;
        public bool IsEmpty
        {
            get
            {
                return ((this.Text == null) || (this.Title == null));
            }
        }
        public static ChartAreaLabelItem Empty
        {
            get
            {
                ChartAreaLabelItem item;
                item.DataType = -1;
                item.Enabled = false;
                item.Position = 0;
                item.Title = null;
                item.Text = null;
                item.BackColor = -1;
                return item;
            }
        }
    }
    public struct ChartAreaColorZoneItem
    {
        public bool Enabled;
        public int DataType;
        public string Text;
        public double Start;
        public double End;
        public int BackColor;
        public static ChartAreaColorZoneItem Empty
        {
            get
            {
                ChartAreaColorZoneItem item;
                item.DataType = -1;
                item.Text = string.Empty;
                item.Enabled = false;
                item.Start = 0;
                item.End = 0;
                item.BackColor = -1;
                return item;
            }
        }
    }
    public struct ChartLegendItem
    {
        public bool Selected;
        public ChartSeries Series;
        public bool Visible
        {
            get { return Series.Visible; }
            set { Series.Visible = value; }
        }
        public int Index;
        public RectangleF Location;

        public static ChartLegendItem Empty
        {
            get
            {
                ChartLegendItem item;
                item.Selected = false;
                item.Index = 0;
                item.Series = null;
                item.Location = RectangleF.Empty;
                return item;
            }
        }
    }
    public struct ChartAxisLabelItem
    {
        public bool Enabled;
        public int DataType;
        public double Position;
        public string Title;
        public string Text;
        public ChartMarker Marker;
        public bool IsEmpty
        {
            get
            {
                return (Marker == null);
            }
        }
        public static ChartAxisLabelItem Empty
        {
            get
            {
                ChartAxisLabelItem item;
                item.Enabled = false;
                item.DataType = -1;
                item.Position = 0;
                item.Title = null;
                item.Text = null;
                item.Marker = null;
                return item;
            }
        }
    }
    #endregion

    #region Constants
        public enum ChartPosition : byte 
        {
            Left,
            Right,
            Top,
            Bottom
        }

        public enum TextOrientation : byte
        {
            Horizontal,
            Rotated90,
            Rotated270
        }

        public enum SeriesChartType : byte
        {
            Point = 0,
            //FastPoint = 1,
            //Bubble = 2,
            Line = 3,
            //Spline = 4,
            StepLine = 5,
            //FastLine = 6,
            //Bar = 7,
            //StackedBar = 8,
            //StackedBar100 = 9,
            Column = 10,
            //StackedColumn = 11,
            //StackedColumn100 = 12,
            Area = 13,
            //SplineArea = 14,
            StackedArea = 15,
            //StackedArea100 = 16,
            //Pie = 17,
            //Doughnut = 18,
            //Stock = 19,
            //Candlestick = 20,
            //Range = 21,
            //SplineRange = 22,
            //RangeBar = 23,
            //RangeColumn = 24,
            //Radar = 25,
            //Polar = 26,
            //ErrorBar = 27,
            //BoxPlot = 28,
            //Renko = 29,
            //ThreeLineBreak = 30,
            //Kagi = 31,
            //PointAndFigure = 32,
            //Funnel = 33,
            //Pyramid = 34,
        }

        public enum ChartValueType : byte
        {
            //Auto = 0,
            Double = 1,
            //Single = 2,
            Int32 = 3,
            //Int64 = 4,
            //UInt32 = 5,
            //UInt64 = 6,
            //String = 7,
            DateTime = 8,
            //Date = 9,
            //Time = 10,
            //DateTimeOffset = 11,
        }

        public enum TickMarkStyle : byte
        {
            None = 0,
            OutsideArea = 1,
            InsideArea = 2,
            AcrossAxis = 3
        }

        public enum DateTimeIntervalType : byte
        {
            Auto = 0,
            Number = 1,
            Years = 2,
            Months = 3,
            //Weeks = 4,
            Days = 5,
            Hours = 6,
            Minutes = 7,
            Seconds = 8,
            Milliseconds = 9,
            NotSet = 10
        }

        public enum AxisArrowStyle : byte
        {
            None = 0,
            Triangle = 1,
            SharpTriangle = 2,
            Lines = 3,
        }

        public enum MarkerStyle : byte
        {
            None = 0,
            Square = 1,
            Circle = 2,
            //Diamond = 3,
            Triangle = 4,
            //Cross = 5,
            //Star4 = 6,
            //Star5 = 7,
            //Star6 = 8,
            //Star10 = 9,
        }

        public enum SeriesOperation : byte
        {
            None,
            Addition,
            Subtraction,
            Multiplication,
            Division
        }

        public enum ChartToolTipType
        {
            Default,
            ChartAreaLabel,
            Stacked
        }
    #endregion
}