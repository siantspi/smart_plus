﻿namespace SmartPlus
{
    partial class ChartSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbK1 = new System.Windows.Forms.TextBox();
            this.tbK2 = new System.Windows.Forms.TextBox();
            this.tbAve = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbFixedVisibleSeries = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Коэф. 2 точки:";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Осреднять за ";
            this.label2.Visible = false;
            // 
            // bOk
            // 
            this.bOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(234, 275);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 6;
            this.bOk.Text = "OK";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(330, 275);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 7;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Коэф. 4 точки:";
            this.label3.Visible = false;
            // 
            // tbK1
            // 
            this.tbK1.Location = new System.Drawing.Point(93, 204);
            this.tbK1.Name = "tbK1";
            this.tbK1.Size = new System.Drawing.Size(100, 23);
            this.tbK1.TabIndex = 9;
            this.tbK1.Text = "0.1";
            this.tbK1.Visible = false;
            // 
            // tbK2
            // 
            this.tbK2.Location = new System.Drawing.Point(297, 204);
            this.tbK2.Name = "tbK2";
            this.tbK2.Size = new System.Drawing.Size(100, 23);
            this.tbK2.TabIndex = 10;
            this.tbK2.Text = "0.1";
            this.tbK2.Visible = false;
            // 
            // tbAve
            // 
            this.tbAve.Location = new System.Drawing.Point(93, 245);
            this.tbAve.Name = "tbAve";
            this.tbAve.Size = new System.Drawing.Size(100, 23);
            this.tbAve.TabIndex = 11;
            this.tbAve.Text = "1";
            this.tbAve.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbAve.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(199, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "месяцев";
            this.label4.Visible = false;
            // 
            // cbFixedVisibleSeries
            // 
            this.cbFixedVisibleSeries.AutoSize = true;
            this.cbFixedVisibleSeries.Location = new System.Drawing.Point(12, 12);
            this.cbFixedVisibleSeries.Name = "cbFixedVisibleSeries";
            this.cbFixedVisibleSeries.Size = new System.Drawing.Size(203, 19);
            this.cbFixedVisibleSeries.TabIndex = 13;
            this.cbFixedVisibleSeries.Text = "Сохранять видимость графиков";
            this.cbFixedVisibleSeries.UseVisualStyleBackColor = true;
            this.cbFixedVisibleSeries.Visible = false;
            // 
            // ChartSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 317);
            this.Controls.Add(this.cbFixedVisibleSeries);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbAve);
            this.Controls.Add(this.tbK2);
            this.Controls.Add(this.tbK1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChartSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки графиков";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbK1;
        private System.Windows.Forms.TextBox tbK2;
        private System.Windows.Forms.TextBox tbAve;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbFixedVisibleSeries;
    }
}