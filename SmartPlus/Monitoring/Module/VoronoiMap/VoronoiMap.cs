﻿using System;
using System.Collections.Generic;
using FortuneVoronoi;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public struct MapGridParams
    {
        public int OilfieldIndex;
        public int Index;
        public string Name;
        public DateTime Date;
        public string Hash;
    }
    public sealed class VoronoiMap : C2DObject
    {
        public new bool Selected
        {
            get { return base.Selected; }
            set
            {
                base.Selected = value;
                if (!value)
                {
                    for (int i = 0; (Cells != null) && (i < Cells.Length); i++)
                    {
                        Cells[i].Selected = false;
                    }
                }
            }
        }
        public DateTime Date;
        public bool PaletteByNBZ
        {
            get { return paletteByNBZ; }
            set
            {
                if (paletteByNBZ != value)
                {
                    paletteByNBZ = value;
                    for (int i = 0; i < Cells.Length; i++)
                    {
                        Cells[i].PaletteByNBZ = paletteByNBZ;
                    }
                    CreatePalette();
                }
            }
        }
        public bool FilteredByProduction;
        public List<int> PlastCodes;
        Font CellCaptionFont;
        public MapGridParams[] GridParams;

        public VoronoiCell[] Cells;
        double MinX, MinY, MaxX, MaxY;
        Palette pal;
        bool paletteByNBZ;
        
        VoronoiCreator MapCreator;
        public VoronoiMap()
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP;
            Visible = true;
            Selected = false;
            Cells = new VoronoiCell[0];
            MinX = 0; MaxX = 0; MinY = 0; MaxY = 0;
            pal = new Palette();
            PaletteByNBZ = false;
            FilteredByProduction = true;
            GridParams = new MapGridParams[0];
            CellCaptionFont = new Font("Calibri", 8);
        }

        
        /// <summary>
        /// Вычисление карты областей Вороного
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="e"></param>
        /// <param name="Date">Дата для установки значков скважин</param>
        /// <param name="Project">Проект</param>
        /// <param name="Wells">Список скважин для областей Вороного</param>
        /// <param name="PlastCodes">Список выбранных пластов</param>
        /// <param name="CutOffRadius">Максимальный радиус области Вороного</param>
        /// <param name="FilterByProduction">Учитывать работу скважин на объект</param>
        /// <param name="SelectedGrid">Вручную заданная сетка толщин для расчета</param>
        public void ComputeVoronoiMap(BackgroundWorker worker, DoWorkEventArgs e, DateTime Date, Project Project, List<Well> Wells, List<int> PlastCodes, int CutOffRadius, bool FilterByProduction, Grid SelectedGrid)
        {
            this.Date = Date;
            this.PlastCodes = PlastCodes;
            this.FilteredByProduction = FilterByProduction;
            Well w;
            double minX = Constant.DOUBLE_NAN, minY = Constant.DOUBLE_NAN, maxX = Constant.DOUBLE_NAN, maxY = Constant.DOUBLE_NAN;
            List<int> ofIndexes = new List<int>();
            for (int i = 0; i < Wells.Count; i++)
            {
                w = (Well)Wells[i];
                if (ofIndexes.IndexOf(w.OilFieldIndex) == -1)
                {
                    ofIndexes.Add(w.OilFieldIndex);
                }
                if (minX == Constant.DOUBLE_NAN || minX > w.X) minX = w.X;
                if (maxX == Constant.DOUBLE_NAN || maxX < w.X) maxX = w.X;
                if (minY == Constant.DOUBLE_NAN || minY > w.Y) minY = w.Y;
                if (maxY == Constant.DOUBLE_NAN || maxY < w.Y) maxY = w.Y;
            }
            if (minX == Constant.DOUBLE_NAN || minY == Constant.DOUBLE_NAN) return;
            minX -= CutOffRadius * 2; minY -= CutOffRadius * 2;
            maxX += CutOffRadius * 2; maxY += CutOffRadius * 2;
            OilField of;
            List<Well> AroundWells = new List<Well>();
            for (int j = 0; j < ofIndexes.Count; j++)
            {
                of = Project.OilFields[ofIndexes[j]];
                for (int i = 0; i < of.Wells.Count; i++)
                {
                    w = of.Wells[i];
                    if (w.X < minX || w.X > maxX) continue;
                    if (w.Y < minY || w.Y > maxY) continue;
                    AroundWells.Add(w);
                }
            }
            List<double[]> AroundParams = null;

            AroundParams = GetWellsProduction(worker, e, Project, AroundWells, Wells, PlastCodes, FilterByProduction); 
            if (Wells.Count == 0)
            {
                throw new Exception("Нет скважин работающих на выбранный пласт для расчета!\nКарта областей Вороного не создана.");
            }
            FortuneVoronoi.PointD[] points = GetPoints(AroundWells, CutOffRadius);
            if (points.Length > 0)
            {
                MapCreator = new VoronoiCreator();
                MapCreator.ComputeVoronoiMap(points, MinX, MaxX, MinY, MaxY, 0.1);
                List<Edge> edges = MapCreator.GetEdgeList();
                ComputeVoronoiCells(Project, edges, Wells);
                if (CutOffRadius > 0) CutOffCells(worker, e, CutOffRadius);
                SetCellParams(AroundParams, AroundWells);
                ComputeReserves(worker, e, Project, SelectedGrid);
                CreatePalette();
                StratumDictionary stratumDict = (StratumDictionary)Project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                StratumTreeNode stratum = stratumDict.GetStratumTreeNode(PlastCodes[0]);
                string ofName = ofIndexes.Count > 0 ? Project.OilFields[ofIndexes[0]].Name : string.Empty;
                Name = string.Format("Карта Вороного [{0} - {1}]", ofName, stratum != null ? stratum.Name : string.Empty);
            }
        }
        List<double[]> GetWellsProduction(BackgroundWorker worker, DoWorkEventArgs e, Project Project, List<Well> AroundWells, List<Well> ShowWells, List<int> PlastCodes, bool FilterProductWells)
        {
            int i, ind;
            List<double[]> outParams = new List<double[]>();
            List<Well> outWells = new List<Well>();
            List<List<int>> wellIndexes = new List<List<int>>();
            List<int> ofIndexes = new List<int>();
            for (i = 0; i < AroundWells.Count; i++)
            {
                ind = ofIndexes.IndexOf(AroundWells[i].OilFieldIndex);
                if (ind == -1)
                {
                    ind = wellIndexes.Count;
                    wellIndexes.Add(new List<int>());
                    ofIndexes.Add(AroundWells[i].OilFieldIndex);
                }
                wellIndexes[ind].Add(AroundWells[i].Index);
            }
            // загрузка данных МЭР
            Well w;
            OilField of;
            double accumOil, accumInj;
            for (i = 0; i < ofIndexes.Count; i++)
            {
                of = Project.OilFields[ofIndexes[i]];
                if (of.MerLoaded || of.LoadMerFromCache(worker, e, wellIndexes[i]))
                {
                    for (int j = 0; j < wellIndexes[i].Count; j++)
                    {
                        w = of.Wells[wellIndexes[i][j]];
                        accumOil = 0;
                        accumInj = 0;
                        if (w.MerLoaded && w.mer.Count > 0)
                        {
                            for (int k = 0; k < w.mer.Count; k++)
                            {
                                if (w.mer.Items[k].Date.Year < Date.Year || (w.mer.Items[k].Date.Year == Date.Year && w.mer.Items[k].Date.Month <= Date.Month))
                                {
                                    if (w.mer.Items[k].WorkTime > 0 && PlastCodes.IndexOf(w.mer.Items[k].PlastId) > -1)
                                    {
                                        accumOil += w.mer.Items[k].Oil;
                                        accumInj += w.mer.GetItemEx(k).Injection;
                                    }
                                }
                            }
                        }
                        if (!FilterProductWells || accumOil > 0 || accumInj > 0)
                        {
                            outWells.Add(w);
                            outParams.Add(new double[] { accumOil, accumInj });
                        }
                    }
                }
                //of.ClearMerData(false);
            }
            AroundWells.Clear();
            AroundWells.AddRange(outWells);
            i = 0;
            while (i < ShowWells.Count)
            {
                if (AroundWells.IndexOf(ShowWells[i]) == -1)
                {
                    ShowWells.RemoveAt(i);
                    i--;
                }
                i++;
            }
            return outParams;
        }
        void SetCellParams(List<double[]> AroundParams, List<Well> AroundWells)
        {
            if (AroundParams != null)
            {
                int ind;
                for (int i = 0; i < Cells.Length; i++)
                {
                    ind = AroundWells.IndexOf(Cells[i].Well.srcWell);
                    if (ind > -1)
                    {
                        Cells[i].AccumOil = AroundParams[ind][0];
                    }
                }
            }
        }
        void ComputeReserves(BackgroundWorker worker, DoWorkEventArgs e, Project Project, Grid SelectedGrid)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание карты областей Вороного...";
            userState.WorkCurrentTitle = "Расчет запасов по областям Вороного...";
            userState.Element = this.Name;

            Area area;
            StratumTreeNode node;
            PVTParamsItem pvt = PVTParamsItem.Empty;
            OilField of = null;
            List<Grid> grids = new List<Grid>();
            List<MapGridParams> gridParams = new List<MapGridParams>();
            List<Grid> loadedGrids = new List<Grid>();
            var stratumDict = (StratumDictionary)Project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            for (int i = 0; i < Cells.Length; i++)
            {
                userState.Element = Cells[i].Well.srcWell.Name;

                if (of == null || of.Index != Cells[i].Well.srcWell.OilFieldIndex)
                {
                    of = Project.OilFields[Cells[i].Well.srcWell.OilFieldIndex];
                    grids.Clear();
                    if (SelectedGrid == null)
                    {
                        for (int j = 0; j < of.Grids.Count; j++)
                        {
                            if (of.Grids[j].GridType == 2 && (PlastCodes[0] == of.Grids[j].StratumCode))
                            {
                                grids.Add((Grid)of.Grids[j]);
                            }
                        }
                        if (grids.Count == 0) // сетки не найдена пробуем найти вышележащий пласт
                        {
                            node = stratumDict.GetStratumTreeNode(PlastCodes[0]);
                            if (node != null)
                            {
                                for (int j = 0; j < of.Grids.Count; j++)
                                {
                                    if (((Grid)of.Grids[j]).GridType == 2 &&
                                        (node.GetParentLevelByCode(((Grid)of.Grids[j]).StratumCode) > -1) &&
                                        (of.MerStratumCodes.IndexOf(((Grid)of.Grids[j]).StratumCode) == -1))
                                    {
                                        grids.Add((Grid)of.Grids[j]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        grids.Add(SelectedGrid);
                    }
                    MapGridParams param;
                    for (int j = 0; j < grids.Count; j++)
                    {
                        param.Date = grids[j].Date;
                        param.Hash = string.Empty;
                        param.Index = grids[j].Index;
                        param.Name = string.Format("{0} [{1:MM.yyyy}]", stratumDict.GetShortNameByCode(grids[j].StratumCode), grids[j].Date);
                        param.OilfieldIndex = (SelectedGrid == null) ? of.Index : Project.GetOFIndex(SelectedGrid.OilFieldCode);
                        if (gridParams.IndexOf(param) == -1) gridParams.Add(param);

                        if (!grids[j].DataLoaded)
                        {
                            of.LoadGridDataFromCacheByIndex(worker, e, grids[j].Index, false);
                            loadedGrids.Add(grids[j]);
                        }
                    }
                }

                
                if (grids.Count == 0)
                {
                    node = stratumDict.GetStratumTreeNode(PlastCodes[0]);
                    throw new Exception(string.Format("Не найдена сетка толщин по пласту {0} [{1}].\nКарта областей Вороного не построена.", (node != null) ? node.Name : string.Empty, of.Name));
                }
                else
                {
                    for (int j = 0; j < of.Areas.Count; j++)
                    {
                        area = (Area)of.Areas[j];
                        if (area.PlastCode == PlastCodes[0])
                        {
                            if (FilteredByProduction)
                            {
                                for (int k = 0; k < area.WellList.Length; k++)
                                {
                                    if (area.WellList[k] == Cells[i].Well.srcWell)
                                    {
                                        pvt = area.pvt;
                                        break;
                                    }
                                }
                            }
                            else if (area.contour.PointBoundsEntry(Cells[i].Well.srcWell.X, Cells[i].Well.srcWell.Y))
                            {
                                pvt = area.pvt;
                            }
                        }
                        if (!pvt.IsEmpty) break;
                    }
                    if (pvt.IsEmpty)
                    {
                        node = stratumDict.GetStratumTreeNode(PlastCodes[0]);
                        if (node != null)
                        {
                            for (int j = 0; j < of.Areas.Count; j++)
                            {
                                area = (Area)of.Areas[j];
                                if ((node.GetParentLevelByCode(area.PlastCode) > -1) && (of.MerStratumCodes.IndexOf(area.PlastCode) == -1))
                                {
                                    if (FilteredByProduction)
                                    {
                                        for (int k = 0; k < area.WellList.Length; k++)
                                        {
                                            if (area.WellList[k] == Cells[i].Well.srcWell)
                                            {
                                                pvt = area.pvt;
                                                break;
                                            }
                                        }
                                    }
                                    else if (area.contour.PointBoundsEntry(Cells[i].Well.srcWell.X, Cells[i].Well.srcWell.Y))
                                    {
                                        pvt = area.pvt;
                                    }
                                }
                                if (!pvt.IsEmpty) break;
                            }
                        }
                    }
                    if (!pvt.IsEmpty)
                    {
                        Cells[i].Pvt = pvt;
                        double x = pvt.OilDensity * pvt.Porosity * pvt.OilInitialSaturation / pvt.OilVolumeFactor;
                        for (int j = 0; j < grids.Count; j++)
                        {
                            if (grids[j].DataLoaded)
                            {
                                double Square, Value;
                                grids[j].CalcSumValues(worker, e, Cells[i].Cntr, out Square, out Value);
                                if (Square > 0)
                                {
                                    Cells[i].NBZ = x * Value;
                                    Cells[i].NIZ = x * Value * pvt.KIN;
                                }
                            }
                        }
                    }
                }
                if (worker != null) worker.ReportProgress((int)(i * 100f / Cells.Length), userState);
            }
            GridParams = gridParams.ToArray();
            for (int i = 0; i < loadedGrids.Count;i++)
            {
                loadedGrids[i].FreeDataMemory();
            }
        }
        FortuneVoronoi.PointD[] GetPoints(List<Well> Wells, int CutOffRadius)
        {
            List<FortuneVoronoi.PointD> result = new List<FortuneVoronoi.PointD>(Wells.Count);
            FortuneVoronoi.PointD pt;
            MinX = Constant.DOUBLE_NAN;
            MaxX = Constant.DOUBLE_NAN;
            MinY = Constant.DOUBLE_NAN;
            MaxY = Constant.DOUBLE_NAN;
            int index;
            List<double> sum = new List<double>();
            for (int i = 0; i < Wells.Count; i++)
            {
                if (Wells[i].X != Constant.DOUBLE_NAN && Wells[i].Y != Constant.DOUBLE_NAN)
                {
                    index = sum.IndexOf(Wells[i].X + Wells[i].Y);
                    if ((index > -1) && (result[index].X == Wells[i].X) && (result[index].Y == Wells[i].Y))
                    {
                        continue;
                    }
                    pt.X = Wells[i].X;
                    pt.Y = Wells[i].Y;


                    if (MinX == Constant.DOUBLE_NAN || MinX > Wells[i].X) MinX = Wells[i].X;
                    if (MaxX == Constant.DOUBLE_NAN || MaxX < Wells[i].X) MaxX = Wells[i].X;
                    if (MinY == Constant.DOUBLE_NAN || MinY > Wells[i].Y) MinY = Wells[i].Y;
                    if (MaxY == Constant.DOUBLE_NAN || MaxY < Wells[i].Y) MaxY = Wells[i].Y;
                    result.Add(pt);
                    sum.Add(pt.X + pt.Y);
                }
            }
            if (MinX == Constant.DOUBLE_NAN || MinY == Constant.DOUBLE_NAN) return new FortuneVoronoi.PointD[0];
            MinX -= CutOffRadius; MinY -= CutOffRadius;
            MaxX += CutOffRadius; MaxY += CutOffRadius;
            sum.Clear();
            return result.ToArray();
        }

        // Compute Cells
        //
        void ComputeVoronoiCells(Project Project, List<Edge> Edges, List<Well> ShowWells)
        {
            this.Cells = new VoronoiCell[ShowWells.Count];
            for (int i = 0; i < ShowWells.Count; i++)
            {
                List<Edge> cellEdges = GetEdgesByPoint(Edges, ShowWells[i].X, ShowWells[i].Y);
                if (cellEdges.Count > 0)
                {
                    this.Cells[i] = new VoronoiCell(Project, Date, ShowWells[i], CreateClosedContour(cellEdges, ShowWells[i].X, ShowWells[i].Y));
                }
                else
                {
                    this.Cells[i] = new VoronoiCell(Project, Date, ShowWells[i]);
                }
            }
        }
        List<Edge> GetEdgesByPoint(List<Edge> Edges, double X, double Y)
        {
            List<Edge> newEdges = new List<Edge>();
            bool minDist;
            Edge edge;
            for (int i = 0; i < Edges.Count; i++)
            {
                edge = Edges[i];
                minDist = (Math.Abs(edge.reg[0].coord.X - X) < 1) && (Math.Abs(edge.reg[0].coord.Y - Y) < 1);
                if (!minDist) minDist = (Math.Abs(edge.reg[1].coord.X - X) < 1) && (Math.Abs(edge.reg[1].coord.Y - Y) < 1);
                if (minDist)
                {
                    if ((Math.Abs(edge.ep[0].coord.X - edge.ep[1].coord.X) > 0.1) ||
                        (Math.Abs(edge.ep[0].coord.Y - edge.ep[1].coord.Y) > 0.1))
                    {
                        newEdges.Add(edge);
                    }
                }
            }
            if (newEdges.Count == 2)
            {
                if (((Math.Abs(newEdges[0].ep[0].coord.X - newEdges[1].ep[0].coord.X) < 0.1) && (Math.Abs(newEdges[0].ep[0].coord.Y - newEdges[1].ep[0].coord.Y) < 0.1) &&
                     (Math.Abs(newEdges[0].ep[1].coord.X - newEdges[1].ep[1].coord.X) < 0.1) && (Math.Abs(newEdges[0].ep[1].coord.Y - newEdges[1].ep[1].coord.Y) < 0.1)) ||
                    ((Math.Abs(newEdges[0].ep[0].coord.X - newEdges[1].ep[1].coord.X) < 0.1) && (Math.Abs(newEdges[0].ep[0].coord.Y - newEdges[1].ep[1].coord.Y) < 0.1) &&
                     (Math.Abs(newEdges[0].ep[1].coord.X - newEdges[1].ep[0].coord.X) < 0.1) && (Math.Abs(newEdges[0].ep[1].coord.Y - newEdges[1].ep[0].coord.Y) < 0.1)))
                {
                    newEdges.RemoveAt(1);
                }
            }
            return newEdges;
        }
        Contour CloseContourByBounds2(Contour cntr, double X, double Y)
        {
            PointD StartPt, EndPt;
            StartPt.X = cntr._points[0].X;
            StartPt.Y = cntr._points[0].Y;
            EndPt.X = cntr._points[cntr._points.Count - 1].X;
            EndPt.Y = cntr._points[cntr._points.Count - 1].Y;
            int i;

            if ((Math.Abs(MinX - StartPt.X) < 0.1 || Math.Abs(MaxX - StartPt.X) < 0.1 ||
                 Math.Abs(MinY - StartPt.Y) < 0.1 || Math.Abs(MaxY - StartPt.Y) < 0.1) &&

                (Math.Abs(MinX - EndPt.X) < 0.1 || Math.Abs(MaxX - EndPt.X) < 0.1 ||
                 Math.Abs(MinY - EndPt.Y) < 0.1 || Math.Abs(MaxY - EndPt.Y) < 0.1))
            {
                i = 0;
                Contour newCntr = new Contour(cntr);
                PointD LastPt;
                LastPt.X = StartPt.X;
                LastPt.Y = StartPt.Y;
                while (Math.Abs(LastPt.X - EndPt.X) > 0.1 && Math.Abs(LastPt.Y - EndPt.Y) > 0.1)
                {
                    if (Math.Abs(MinX - LastPt.X) < 0.1)
                    {
                        if (Math.Abs(MinY - LastPt.Y) < 0.1) LastPt.X = MaxX;
                        LastPt.Y = MinY;
                    }
                    else if (Math.Abs(MaxX - LastPt.X) < 0.1)
                    {
                        if (Math.Abs(MaxY - LastPt.Y) < 0.1) LastPt.X = MinX;
                        LastPt.Y = MaxY;
                    }
                    else if (Math.Abs(MinY - LastPt.Y) < 0.1)
                    {
                        if (Math.Abs(MaxX - LastPt.X) < 0.1) LastPt.Y = MaxY;
                        LastPt.X = MaxX;
                    }
                    else if (Math.Abs(MaxY - LastPt.Y) < 0.1)
                    {
                        if (Math.Abs(MinX - LastPt.X) < 0.1) LastPt.Y = MinY;
                        LastPt.X = MinX;
                    }
                    newCntr.Insert(0, LastPt.X, LastPt.Y);
                    i++;
                    if (i > 4) break;
                }
                if (newCntr.PointBodyEntry(X, Y)) return newCntr;

                newCntr = new Contour(cntr);
                //newCntr.Add(StartPt.X, StartPt.Y);
                LastPt.X = StartPt.X;
                LastPt.Y = StartPt.Y;
                i = 0;
                while (Math.Abs(LastPt.X - EndPt.X) > 0.1 && Math.Abs(LastPt.Y - EndPt.Y) > 0.1)
                {
                    if (Math.Abs(MinX - LastPt.X) < 0.1)
                    {
                        if (Math.Abs(MaxY - LastPt.Y) < 0.1) LastPt.X = MaxX;
                        LastPt.Y = MaxY;
                    }
                    else if (Math.Abs(MaxX - LastPt.X) < 0.1)
                    {
                        if (Math.Abs(MinY - LastPt.Y) < 0.1) LastPt.X = MinX;
                        LastPt.Y = MinY;
                    }
                    else if (Math.Abs(MinY - LastPt.Y) < 0.1)
                    {
                        if (Math.Abs(MinX - LastPt.X) < 0.1) LastPt.Y = MaxY;
                        LastPt.X = MinX;
                    }
                    else if (Math.Abs(MaxY - LastPt.Y) < 0.1)
                    {
                        if (Math.Abs(MaxX - LastPt.X) < 0.1) LastPt.Y = MinY;
                        LastPt.X = MaxX;
                    }
                    newCntr.Insert(0, LastPt.X, LastPt.Y);
                    i++;
                    if (i > 4) break;
                }
                newCntr.Invert();
                if (newCntr.PointBodyEntry(X, Y)) return newCntr;
            }
            return cntr;
        }
        Contour CreateClosedContour2(List<Edge> Edges, double X, double Y)
        {
            Contour cntr = new Contour(Edges.Count + 1);
            List<int> edgeInd = new List<int>();
            List<Edge> otherEdges = new List<Edge>();
            bool findHead = false;
            edgeInd.Add(0);
            PointD LastPt;
            cntr.Add(Edges[0].ep[0].coord.X, Edges[0].ep[0].coord.Y);
            cntr.Add(Edges[0].ep[1].coord.X, Edges[0].ep[1].coord.Y);
            LastPt.X = Edges[0].ep[1].coord.X;
            LastPt.Y = Edges[0].ep[1].coord.Y;
            int counter;
            while (edgeInd.Count < Edges.Count)
            {
                counter = 0;
                for (int i = 1; i < Edges.Count; i++)
                {
                    counter++;
                    if (edgeInd.IndexOf(i) > 0) continue;
                    if (Math.Abs(LastPt.X - Edges[i].ep[0].coord.X) < 0.1 && Math.Abs(LastPt.Y - Edges[i].ep[0].coord.Y) < 0.1)
                    {
                        edgeInd.Add(i);
                        if (!findHead)
                        {
                            cntr.Add(Edges[i].ep[1].coord.X, Edges[i].ep[1].coord.Y);
                        }
                        else
                        {
                            cntr.Insert(0, Edges[i].ep[1].coord.X, Edges[i].ep[1].coord.Y);
                        }

                        LastPt.X = Edges[i].ep[1].coord.X;
                        LastPt.Y = Edges[i].ep[1].coord.Y;
                        counter = 0;
                        break;
                    }
                    else if (Math.Abs(LastPt.X - Edges[i].ep[1].coord.X) < 0.1 && Math.Abs(LastPt.Y - Edges[i].ep[1].coord.Y) < 0.1)
                    {
                        edgeInd.Add(i);
                        if (!findHead)
                        {
                            cntr.Add(Edges[i].ep[0].coord.X, Edges[i].ep[0].coord.Y);
                        }
                        else
                        {
                            cntr.Insert(0, Edges[i].ep[0].coord.X, Edges[i].ep[0].coord.Y);
                        }
                        LastPt.X = Edges[i].ep[0].coord.X;
                        LastPt.Y = Edges[i].ep[0].coord.Y;
                        counter = 0;
                        break;
                    }
                }
                if (counter == Edges.Count - 1)
                {
                    if (!findHead)
                    {
                        findHead = true;
                        LastPt.X = Edges[0].ep[0].coord.X;
                        LastPt.Y = Edges[0].ep[0].coord.Y;
                    }
                    else
                        break;
                }
            }
            if (edgeInd.Count < Edges.Count)
            {
                for (int i = 0; i < Edges.Count; i++)
                {
                    if (edgeInd.IndexOf(i) == -1)
                    {
                        otherEdges.Add(Edges[i]);
                    }
                }
            }
            cntr._Closed = ((Math.Abs(cntr._points[0].X - cntr._points[cntr._points.Count - 1].X) < 0.001) &&
                            (Math.Abs(cntr._points[0].Y - cntr._points[cntr._points.Count - 1].Y) < 0.001));

            if (!cntr._Closed || otherEdges.Count > 0)
            {
                cntr = CloseContourByBounds2(cntr, X, Y);
            }
            if ((Math.Abs(cntr._points[0].X - cntr._points[cntr._points.Count - 1].X) < 0.001) &&
                             (Math.Abs(cntr._points[0].Y - cntr._points[cntr._points.Count - 1].Y) < 0.001))
            {
                cntr._points.RemoveAt(cntr._points.Count - 1);
            }
            cntr._Closed = true;
            if (!cntr.GetClockWiseByX(true)) cntr.Invert();
            return cntr;
        }
        Contour CreateClosedContour(List<Edge> Edges, double X, double Y)
        {
            Contour cntr = new Contour(Edges.Count + 1);
            List<int> edgeInd = new List<int>();
            bool findHead = false;
            List<Contour> cntrs = new List<Contour>();
            cntrs.Add(cntr);

            edgeInd.Add(0);
            PointD LastPt;
            cntr.Add(Edges[0].ep[0].coord.X, Edges[0].ep[0].coord.Y);
            cntr.Add(Edges[0].ep[1].coord.X, Edges[0].ep[1].coord.Y);
            LastPt.X = Edges[0].ep[1].coord.X;
            LastPt.Y = Edges[0].ep[1].coord.Y;
            int counter, ind;
            while (edgeInd.Count < Edges.Count)
            {
                counter = 0;
                for (int i = 1; i < Edges.Count; i++)
                {
                    counter++;
                    if (edgeInd.IndexOf(i) > 0) continue;
                    if (Math.Abs(LastPt.X - Edges[i].ep[0].coord.X) < 0.1 && Math.Abs(LastPt.Y - Edges[i].ep[0].coord.Y) < 0.1)
                    {
                        edgeInd.Add(i);
                        if (!findHead)
                        {
                            cntr.Add(Edges[i].ep[1].coord.X, Edges[i].ep[1].coord.Y);
                        }
                        else
                        {
                            cntr.Insert(0, Edges[i].ep[1].coord.X, Edges[i].ep[1].coord.Y);
                        }

                        LastPt.X = Edges[i].ep[1].coord.X;
                        LastPt.Y = Edges[i].ep[1].coord.Y;
                        counter = 0;
                        break;
                    }
                    else if (Math.Abs(LastPt.X - Edges[i].ep[1].coord.X) < 0.1 && Math.Abs(LastPt.Y - Edges[i].ep[1].coord.Y) < 0.1)
                    {
                        edgeInd.Add(i);
                        if (!findHead)
                        {
                            cntr.Add(Edges[i].ep[0].coord.X, Edges[i].ep[0].coord.Y);
                        }
                        else
                        {
                            cntr.Insert(0, Edges[i].ep[0].coord.X, Edges[i].ep[0].coord.Y);
                        }
                        LastPt.X = Edges[i].ep[0].coord.X;
                        LastPt.Y = Edges[i].ep[0].coord.Y;
                        counter = 0;
                        break;
                    }
                }
                if (counter == Edges.Count - 1)
                {
                    if (!findHead)
                    {
                        findHead = true;
                        LastPt.X = Edges[0].ep[0].coord.X;
                        LastPt.Y = Edges[0].ep[0].coord.Y;
                    }
                    else
                    {
                        cntr = new Contour(Edges.Count + 1);
                        ind = -1;
                        for (int i = 1; i < Edges.Count; i++)
                        {
                            if (edgeInd.IndexOf(i) == -1)
                            {
                                ind = i;
                                break;
                            }
                        }
                        if (ind != -1)
                        {
                            edgeInd.Add(ind);
                            cntr.Add(Edges[ind].ep[0].coord.X, Edges[ind].ep[0].coord.Y);
                            cntr.Add(Edges[ind].ep[1].coord.X, Edges[ind].ep[1].coord.Y);
                            LastPt.X = Edges[ind].ep[1].coord.X;
                            LastPt.Y = Edges[ind].ep[1].coord.Y;
                            cntrs.Add(cntr);
                        }
                        else
                        {
                            throw new Exception("Невозможно замкнуть контур ячейки Вороного.\nКарта областей Вороного не построена!");
                        }
                    }
                }
            }
            cntr._Closed = (cntrs.Count == 1 && (Math.Abs(cntr._points[0].X - cntr._points[cntr._points.Count - 1].X) < 0.001) &&
                            (Math.Abs(cntr._points[0].Y - cntr._points[cntr._points.Count - 1].Y) < 0.001));

            if (!cntr._Closed || cntrs.Count > 1)
            {
                cntr = CloseContourByBounds(cntrs, X, Y);
            }
            if ((Math.Abs(cntr._points[0].X - cntr._points[cntr._points.Count - 1].X) < 0.001) &&
                             (Math.Abs(cntr._points[0].Y - cntr._points[cntr._points.Count - 1].Y) < 0.001))
            {
                cntr._points.RemoveAt(cntr._points.Count - 1);
            }
            cntr._Closed = true;
            if (!cntr.GetClockWiseByX(true)) cntr.Invert();
            return cntr;
        }
        int GetCntrIndex(List<Contour> cntrs, List<int> usedCntrs, double TestValue, bool IsTestX)
        {
            int ind = 0;
            for (int j = 1; j < cntrs.Count; j++)
            {
                if (usedCntrs.IndexOf(j) != -1) continue;
                if (IsTestX)
                {
                    if (Math.Abs(TestValue - cntrs[j]._points[0].X) < 0.1)
                    {
                        ind = j;
                        break;
                    }
                    else if (Math.Abs(TestValue - cntrs[j]._points[cntrs[j]._points.Count - 1].X) < 0.1)
                    {
                        ind = -j;
                        break;
                    }
                }
                else
                {
                    if (Math.Abs(TestValue - cntrs[j]._points[0].Y) < 0.1)
                    {
                        ind = j;
                        break;
                    }
                    else if (Math.Abs(TestValue - cntrs[j]._points[cntrs[j]._points.Count - 1].Y) < 0.1)
                    {
                        ind = -j;
                        break;
                    }
                }
            }
            return ind;
        }
        PointD InsertCntr(List<Contour> cntrs, List<int> usedCntrs, Contour newCntr, int ind)
        {
            PointD LastPt;
            if (ind < 0)
            {
                ind = -ind;
                cntrs[ind].Invert();
            }
            usedCntrs.Add(ind);
            for (int j = 0; j < cntrs[ind]._points.Count; j++)
            {
                newCntr.Insert(0, cntrs[ind]._points[j].X, cntrs[ind]._points[j].Y);
            }
            LastPt.X = cntrs[ind]._points[cntrs[ind]._points.Count - 1].X;
            LastPt.Y = cntrs[ind]._points[cntrs[ind]._points.Count - 1].Y;
            return LastPt;
        }
        Contour CloseContourByBounds(List<Contour> cntrs, double X, double Y)
        {
            List<int> usedCntrs = new List<int>();
            usedCntrs.Add(0);

            PointD StartPt, EndPt;
            StartPt.X = cntrs[0]._points[0].X;
            StartPt.Y = cntrs[0]._points[0].Y;
            EndPt.X = cntrs[0]._points[cntrs[0]._points.Count - 1].X;
            EndPt.Y = cntrs[0]._points[cntrs[0]._points.Count - 1].Y;
            int i, j, ind;

            if ((Math.Abs(MinX - StartPt.X) < 0.1 || Math.Abs(MaxX - StartPt.X) < 0.1 ||
                 Math.Abs(MinY - StartPt.Y) < 0.1 || Math.Abs(MaxY - StartPt.Y) < 0.1) &&

                (Math.Abs(MinX - EndPt.X) < 0.1 || Math.Abs(MaxX - EndPt.X) < 0.1 ||
                 Math.Abs(MinY - EndPt.Y) < 0.1 || Math.Abs(MaxY - EndPt.Y) < 0.1))
            {
                i = 0;
                Contour newCntr = new Contour(cntrs[0]);
                PointD LastPt;
                LastPt.X = StartPt.X;
                LastPt.Y = StartPt.Y;
                while (Math.Abs(LastPt.X - EndPt.X) > 0.1 && Math.Abs(LastPt.Y - EndPt.Y) > 0.1)
                {
                    if (Math.Abs(MinX - LastPt.X) < 0.1)
                    {
                        ind = GetCntrIndex(cntrs, usedCntrs, MinX, true);
                        if (ind != 0)
                        {
                            LastPt = InsertCntr(cntrs, usedCntrs, newCntr, ind);
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(MinY - LastPt.Y) < 0.1) LastPt.X = MaxX;
                            LastPt.Y = MinY;
                        }
                    }
                    else if (Math.Abs(MaxX - LastPt.X) < 0.1)
                    {
                        ind = GetCntrIndex(cntrs, usedCntrs, MaxX, true);
                        if (ind != 0)
                        {
                            LastPt = InsertCntr(cntrs, usedCntrs, newCntr, ind);
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(MaxY - LastPt.Y) < 0.1) LastPt.X = MinX;
                            LastPt.Y = MaxY;
                        }
                    }
                    else if (Math.Abs(MinY - LastPt.Y) < 0.1)
                    {
                        ind = GetCntrIndex(cntrs, usedCntrs, MinY, false);
                        if (ind != 0)
                        {
                            LastPt = InsertCntr(cntrs, usedCntrs, newCntr, ind);
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(MaxX - LastPt.X) < 0.1) LastPt.Y = MaxY;
                            LastPt.X = MaxX;
                        }
                    }
                    else if (Math.Abs(MaxY - LastPt.Y) < 0.1)
                    {
                        ind = GetCntrIndex(cntrs, usedCntrs, MaxY, false);
                        if (ind != 0)
                        {
                            LastPt = InsertCntr(cntrs, usedCntrs, newCntr, ind);
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(MinX - LastPt.X) < 0.1) LastPt.Y = MinY;
                            LastPt.X = MinX;
                        }
                    }
                    newCntr.Insert(0, LastPt.X, LastPt.Y);
                    i++;
                    if (i > 4) break;
                }
                if (newCntr.PointBodyEntry(X, Y)) return newCntr;

                newCntr = new Contour(cntrs[0]);
                //newCntr.Add(StartPt.X, StartPt.Y);
                LastPt.X = StartPt.X;
                LastPt.Y = StartPt.Y;
                i = 0;
                while (Math.Abs(LastPt.X - EndPt.X) > 0.1 && Math.Abs(LastPt.Y - EndPt.Y) > 0.1)
                {
                    if (Math.Abs(MinX - LastPt.X) < 0.1)
                    {
                        ind = GetCntrIndex(cntrs, usedCntrs, MinX, true);
                        if (ind != 0)
                        {
                            LastPt = InsertCntr(cntrs, usedCntrs, newCntr, ind);
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(MaxY - LastPt.Y) < 0.1) LastPt.X = MaxX;
                            LastPt.Y = MaxY;
                        }
                    }
                    else if (Math.Abs(MaxX - LastPt.X) < 0.1)
                    {
                        ind = GetCntrIndex(cntrs, usedCntrs, MaxX, true);
                        if (ind != 0)
                        {
                            LastPt = InsertCntr(cntrs, usedCntrs, newCntr, ind);
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(MinY - LastPt.Y) < 0.1) LastPt.X = MinX;
                            LastPt.Y = MinY;
                        }
                    }
                    else if (Math.Abs(MinY - LastPt.Y) < 0.1)
                    {
                        ind = GetCntrIndex(cntrs, usedCntrs, MinY, false);
                        if (ind != 0)
                        {
                            LastPt = InsertCntr(cntrs, usedCntrs, newCntr, ind);
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(MinX - LastPt.X) < 0.1) LastPt.Y = MaxY;
                            LastPt.X = MinX;
                        }
                    }
                    else if (Math.Abs(MaxY - LastPt.Y) < 0.1)
                    {
                        ind = GetCntrIndex(cntrs, usedCntrs, MaxY, false);
                        if (ind != 0)
                        {
                            LastPt = InsertCntr(cntrs, usedCntrs, newCntr, ind);
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(MaxX - LastPt.X) < 0.1) LastPt.Y = MinY;
                            LastPt.X = MaxX;
                        }
                    }
                    newCntr.Insert(0, LastPt.X, LastPt.Y);
                    i++;
                    if (i > 4) break;
                }
                newCntr.Invert();
                if (newCntr.PointBodyEntry(X, Y)) return newCntr;
            }
            return cntrs[0];
        }

        void CutOffCells(BackgroundWorker worker, DoWorkEventArgs e, int Radius)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание карты областей Вороного...";
            userState.WorkCurrentTitle = "Бланкование областей Вороного ...";
            List<Contour> cntrsOut;
            Contour cntr;
            Polygon poly = new Polygon();
            for (int i = 0; i < Cells.Length; i++)
            {
                if (Cells[i].Well != null)
                {
                    cntr = Geometry.CreateCircleContour(Cells[i].Well.X, Cells[i].Well.Y, Radius, Radius / 6);
                    if (Cells[i].Cntr._points.Count == 0)
                    {
                        Cells[i].Cntr = cntr;
                        Cells[i].Cntr.SetMinMax();
                        continue;
                    }
                    
                    poly.Clear();
                    poly.AddContour(Cells[i].Cntr);
                    poly.AddContour(cntr);
                    poly.OperateByContours(ContoursOperation.Crossing);
                    cntrsOut = poly.GetContours();
                    if (cntrsOut.Count > 0)
                    {
                        Cells[i].Cntr = cntrsOut[0];
                        Cells[i].Cntr.SetMinMax();
                    }
                }
            }
        }
        void CutOffCellsByContour(BackgroundWorker worker, DoWorkEventArgs e,  Project Project, List<int> OfIndexes)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание карты областей Вороного...";
            userState.WorkCurrentTitle = "Бланкование по контурам ВНК...";

            OilField of;
            Contour cntr;
            List<int> ofCntrLoaded = new List<int>();
            Polygon poly = new Polygon();
            List<int> cntrsSum;
            int progress = 0, sumCntrs = 0;
            for (int i = 0; i < OfIndexes.Count; i++)
            {
                of = Project.OilFields[OfIndexes[i]];
                sumCntrs += of.Contours.Count;
            }

            for (int i = 0; i < OfIndexes.Count; i++)
            {
                of = Project.OilFields[OfIndexes[i]];
                if(worker != null && worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                bool added = false;
                poly.Clear();

                userState.Element = of.Name;
                if(!of.ContoursDataLoaded && of.LoadContoursDataFromCache(worker, e))
                {
                    ofCntrLoaded.Add(OfIndexes[i]);
                    added = false;
                    for (int j = 0; j < of.Contours.Count; j++)
                    {
                        cntr = (Contour)of.Contours[j];
                        if (cntr._ContourType == 2 && PlastCodes[0] == cntr.StratumCode)
                        {
                            cntr = cntr.GetSimplyContour(100);
                            poly.AddContour(cntr);
                            poly.OperateByContours(ContoursOperation.Union);
                            added = true;
                        }
                        progress++;
                        if (worker != null) worker.ReportProgress((int)(progress * 100f / sumCntrs), userState);
                    }

                    if(added) 
                    {
                        List<Contour> cntrs = poly.GetContours();
                        List<Contour> outCntrs;
                        for(int j = 0; j < Cells.Length;j++)
                        {
                            poly.Clear();
                            poly.AddContour(Cells[i].Cntr);
                            for (int k = 0; k < cntrs.Count; k++)
                            {
                                poly.AddContour(cntrs[k]);
                                poly.OperateByContours(ContoursOperation.Crossing);
                            }
                            outCntrs = poly.GetContours();
                            if (outCntrs.Count > 0) Cells[i].Cntr = outCntrs[0];
                        }
                    }
                }
            }
            for (int i = 0; i < ofCntrLoaded.Count; i++)
            {
                of = Project.OilFields[ofCntrLoaded[i]];
                of.FreeContoursData();
            }
        }

        // Palette
        //
        void CreatePalette()
        {
            double min = Constant.DOUBLE_NAN, max = Constant.DOUBLE_NAN;

            if (PaletteByNBZ)
            {
                for (int i = 0; i < Cells.Length; i++)
                {
                    if (min == Constant.DOUBLE_NAN || min > Cells[i].NBZ) min = Cells[i].NBZ;
                    if (max == Constant.DOUBLE_NAN || max < Cells[i].NBZ) max = Cells[i].NBZ;
                }
                pal = Palette.DefaultPalette(min, max);
            }
            else
            {
                for (int i = 0; i < Cells.Length; i++)
                {
                    if (min == Constant.DOUBLE_NAN || min > (Cells[i].NIZ - Cells[i].AccumOil)) min = Cells[i].NIZ - Cells[i].AccumOil;
                    if (max == Constant.DOUBLE_NAN || max < (Cells[i].NIZ - Cells[i].AccumOil)) max = Cells[i].NIZ - Cells[i].AccumOil;
                }
                double center;
                pal = new Palette();
                center = (min < 0) ? 0 : (max + min) / 2;
                if (min < 0)
                {
                    pal.Add(min, Color.Red);
                    pal.Add(center, Color.OrangeRed);
                    pal.Add((center + max) / 2, Color.Yellow);
                    pal.Add(max, Color.Green);
                }
                else
                {
                    pal.Add(0, Color.OrangeRed);
                    pal.Add(min, Color.Yellow);
                    pal.Add(max, Color.Green);
                }
            }
            
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i].pal = pal;
            }
        }
        
        // Selected
        //

        // Selecting
        //
        public List<int> GetSelectedCells()
        {
            List<int> result = new List<int>();
            for (int i = 0; i < Cells.Length; i++)
            {
                if(Cells[i].Selected) result.Add(i);
            }
            return result;
        }

        public void SelectCell(int X, int Y, bool Shift, bool MouseRight)
        {
            bool testSelect = false;
            if (!Shift && !MouseRight)
            {
                for (int i = 0; i < Cells.Length; i++)
                {
                    Cells[i].Selected = false;
                }
            }
            bool initSel;
            for (int i = 0; i < Cells.Length; i++)
            {
                initSel = Cells[i].Selected;
                testSelect = Cells[i].HitTest(X, Y);
                if (MouseRight && testSelect && (initSel != testSelect))
                {
                    for (int j = 0; j < Cells.Length; j++)
                    {
                        Cells[j].Selected = false;
                    }
                }
                if (testSelect) Cells[i].Selected = true;
                if (testSelect) break;
            }
            if (testSelect) Selected = true;
        }

        // Icons
        //
        public void SetIconsFonts(Font[] fonts, ExFontList exFontList)
        {
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i].SetIconsFonts(fonts, exFontList);
                Cells[i].font = CellCaptionFont;
            }
        }
        
        // ToolTip
        //

        // Load / Write
        //
        public int HitTest(int CurscorX, int CursorY)
        {
            for (int i = 0; i < Cells.Length; i++)
            {
                if (Cells[i].HitTest(CurscorX, CursorY)) return i;
            }
            return -1;
        }

        public void LoadFromCache(Project Project, BinaryReader br, int version)
        {
            Date = DateTime.FromOADate(br.ReadDouble());
            this.Name = br.ReadString();
            paletteByNBZ = br.ReadBoolean();
            if (version > 0) FilteredByProduction = br.ReadBoolean();
           
            int count = br.ReadInt32();
            PlastCodes = new List<int>();
            for (int i = 0; i < count; i++)
            {
                PlastCodes.Add(br.ReadInt32());
            }
            count = br.ReadInt32();
            GridParams = new MapGridParams[count];

            for (int i = 0; i < count; i++)
            {
                GridParams[i].OilfieldIndex = br.ReadInt32();
                GridParams[i].Index = br.ReadInt32();
                GridParams[i].Date = DateTime.FromOADate(br.ReadDouble());
                GridParams[i].Name = br.ReadString();
                GridParams[i].Hash = br.ReadString();
            }
            MinX = br.ReadDouble();
            MinY = br.ReadDouble();
            MaxX = br.ReadDouble();
            MaxY = br.ReadDouble();
            pal.ReadFromCache(br);
            count = br.ReadInt32();
            Cells = new VoronoiCell[count];
            for (int i = 0; i < count; i++)
            {
                Cells[i] = new VoronoiCell();
                Cells[i].LoadFromCache(Project, br);
                Cells[i].PaletteByNBZ = paletteByNBZ;
                Cells[i].pal = pal;
            }
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(Date.ToOADate());
            bw.Write(this.Name);
            bw.Write(paletteByNBZ);
            bw.Write(FilteredByProduction);
            bw.Write(PlastCodes.Count);
            for (int i = 0; i < PlastCodes.Count; i++)
            {
                bw.Write(PlastCodes[i]);
            }
            bw.Write(GridParams.Length);
            for (int i = 0; i < GridParams.Length; i++)
            {
                bw.Write(GridParams[i].OilfieldIndex);
                bw.Write(GridParams[i].Index);
                bw.Write(GridParams[i].Date.ToOADate());
                bw.Write(GridParams[i].Name);
                bw.Write(GridParams[i].Hash);
            }
            bw.Write(MinX);
            bw.Write(MinY);
            bw.Write(MaxX);
            bw.Write(MaxY);
            pal.WriteToCache(bw);
            bw.Write(Cells.Length);
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i].WriteToCache(bw);
            }
        }
        
        // Drawing
        //
        public void DrawFrontMap(System.Drawing.Graphics grfx)
        {
            if (st_VisbleLevel <= st_OilfieldXUnits)
            {
                for (int i = 0; i < Cells.Length; i++)
                {
                    Cells[i].DrawCaption(grfx);
                }
                for (int i = 0; i < Cells.Length; i++)
                {
                    Cells[i].DrawSelectedPoints(grfx);
                }
            }

        }
        public override void DrawObject(System.Drawing.Graphics grfx)
        {
            if (st_VisbleLevel <= st_OilfieldXUnits)
            {
                for (int i = 0; i < Cells.Length; i++)
                {
                    Cells[i].DrawObject(grfx);
                }
            }
        }
    }
}
