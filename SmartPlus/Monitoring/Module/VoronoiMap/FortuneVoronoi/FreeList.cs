﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortuneVoronoi
{
    public class FreeNode
    {
        public FreeNode NextFree;
        public bool Deleted;

        public FreeNode()
        {
            NextFree = null;
            Deleted = false;
        }
        public virtual void Init() { }
    }

    public class FreeList
    {
        public FreeNode Head;
        int sqrtNsites;

        public FreeList(int SqrtNSites)
        {
            Head = null;
            this.sqrtNsites = SqrtNSites;
            #region c++
            //void freeinit(Freelist * fl, int size)
            //    {
            //    fl->head = (Freenode *)NULL ;
            //    fl->nodesize = size ;
            //    }
            #endregion
        }
        public T Pop<T>() where T : FreeNode, new()
        {
            T t = new T();
            if (Head == null)
            {
                
                for (int i = 0; i < sqrtNsites; i++)
                {
                    t = new T();
                    t.Init();
                    Push(t);
                }
            }
            t = (T)Head;
            Head = Head.NextFree;
            return t;
            #region c++
            //char * //getfree(Freelist * fl)
            //    {
            //    int i ;
            //    Freenode * t ;
            //    if (fl->head == (Freenode *)NULL)
            //        {
            //        t =  (Freenode *) myalloc(sqrt_nsites * fl->nodesize) ;
            //        for(i = 0 ; i < sqrt_nsites ; i++)
            //            {
            //            makefree((Freenode *)((char *)t+i*fl->nodesize), fl) ;
            //            }
            //        }
            //    t = fl->head ;
            //    fl->head = (fl->head)->nextfree ;
            //    return ((char *)t) ;
            //    }
            #endregion
        }
        public void Push(FreeNode curr)
        {
            curr.NextFree = Head;
            Head = curr;
        }
    }
}
