﻿using System;
using System.Collections.Generic;

namespace FortuneVoronoi
{
    public class VoronoiCreator
    {
        const int le = 0;
        const int re = 1;
        HalfEdge[] ELHash;
        FreeList hfl;
        HalfEdge ELLeftEnd, ELRightEnd;
        int ELHashSize;
        double Xmin, Ymin, Xmax, Ymax, deltaX, deltaY;

        Site[] sites;
        int nEdges, nVertices, nSites, siteIdx;
        int sqrtNsites
        {
            get { return (int)Math.Sqrt(nSites + 4); }
        }
        FreeList sfl;
        Site bottomsite;
        FreeList efl;

        int PQmin, PQcount, PQhashsize;
        HalfEdge[] PQhash;
        int ntry, totalSearch;
        double pxmin, pxmax, pymin, pymax;
        double borderMinX, borderMaxX, borderMinY, borderMaxY;

        List<Edge> resultEdges;

        double minDistanceBetweenSites;

        public VoronoiCreator()
        {
            siteIdx = 0;
            sites = new Site[0];
            resultEdges = new List<Edge>();
            minDistanceBetweenSites = 0;
        }
        public bool ComputeVoronoiMap(PointD[] points, double MinX, double MaxX, double MinY, double MaxY, double MinDist)
        {
            int i;
            resultEdges.Clear();
            sites = new Site[points.Length];
            for (i = 0; i < points.Length; i++)
            {
                sites[i] = new Site();
                sites[i].coord.X = points[i].X;
                sites[i].coord.Y = points[i].Y;
            }
            nSites = sites.Length;
            minDistanceBetweenSites = MinDist;

            sfl = new FreeList(this.sqrtNsites);

            Ymin = sites[0].coord.Y;
            Ymax = sites[0].coord.Y;
            Xmin = sites[0].coord.X;
            Xmax = sites[0].coord.X;
            for (i = 0; i < sites.Length; i++)
            {
                sites[i].Sitenbr = i;
                sites[i].refcnt = 0;
                if (sites[i].coord.X < Xmin)
                {
                    Xmin = sites[i].coord.X;
                }
                else if (sites[i].coord.X > Xmax)
                {
                    Xmax = sites[i].coord.X;
                }
                if (sites[i].coord.Y < Ymin)
                {
                    Ymin = sites[i].coord.Y;
                }
                else if (sites[i].coord.Y > Ymax)
                {
                    Ymax = sites[i].coord.Y;
                }
            }

            QuickSort(sites);
            siteIdx = 0;
            GeomInit(); 
            double temp = 0;
            if (MinX > MaxX)
            {
                temp = MinX;
                MinX = MaxX;
                MaxX = temp;
            }
            if (MinY > MaxY)
            {
                temp = MinY;
                MinY = MaxY;
                MaxY = temp;
            }
            borderMinX = MinX;
            borderMinY = MinY;
            borderMaxX = MaxX;
            borderMaxY = MaxY;

            siteIdx = 0;
            CreateVoronoi();
            return true;
        }
        public List<Edge> GetEdgeList()
        {
            return resultEdges;
        }

        // EdgeList
        //
        void ELInitialize()
        {
            hfl = new FreeList(this.sqrtNsites);
            ELHashSize = 2 * sqrtNsites;
            ELHash = new HalfEdge[ELHashSize];
            for (int i = 0; i < ELHashSize; i++)
            {
                ELHash[i] = null;
            }
            ELLeftEnd = HECreate(null, 0);
            ELRightEnd = HECreate(null, 0);
            ELLeftEnd.ELleft = null;

            ELLeftEnd.ELright = ELRightEnd;
            ELRightEnd.ELleft = ELLeftEnd;
            ELRightEnd.ELright = null;
            ELHash[0] = ELLeftEnd;
            ELHash[ELHashSize - 1] = ELRightEnd;
            #region c++
            //int i;
            //freeinit(&hfl, sizeof **ELhash);
            //ELhashsize = 2 * sqrt_nsites;
            //ELhash = (struct Halfedge **) myalloc ( sizeof *ELhash * ELhashsize);

            //if(ELhash == 0)
            //    return false;

            //for(i=0; i<ELhashsize; i +=1) ELhash[i] = (struct Halfedge *)NULL;
            //ELleftend = HEcreate( (struct Edge *)NULL, 0);
            //ELrightend = HEcreate( (struct Edge *)NULL, 0);
            //ELleftend -> ELleft = (struct Halfedge *)NULL;
            //ELleftend -> ELright = ELrightend;
            //ELrightend -> ELleft = ELleftend;
            //ELrightend -> ELright = (struct Halfedge *)NULL;
            //ELhash[0] = ELleftend;
            //ELhash[ELhashsize-1] = ELrightend;

            //return true;
            #endregion
        }
        HalfEdge HECreate(Edge e, int pm)
        {
            HalfEdge answer;
            answer = hfl.Pop<HalfEdge>();
            answer.DeletedEdge = false;
            answer.ELedge = e;
            answer.ELpm = pm;     
            answer.PQnext = null;
            answer.vertex = null;
            answer.ELrefcnt = 0;
            return answer;
            #region c++
            //struct Halfedge *answer;
            //answer = (struct Halfedge *) getfree(&hfl);
            //answer -> ELedge = e;
            //answer -> ELpm = pm;
            //answer -> PQnext = (struct Halfedge *) NULL;
            //answer -> vertex = (struct Site *) NULL;
            //answer -> ELrefcnt = 0;
            //return(answer);
            #endregion
        }
        void ELInsert(HalfEdge lb, HalfEdge newHe)
        {
            newHe.ELleft = lb;
            newHe.ELright = lb.ELright;
            lb.ELright.ELleft = newHe;
            lb.ELright = newHe;
            #region c++
            //newHe->ELleft = lb;
            //newHe->ELright = lb->ELright;
            //(lb->ELright)->ELleft = newHe;
            //lb->ELright = newHe;
            #endregion
        }
        HalfEdge ELGetHash(int b)
        {
            HalfEdge he;
            if ((b < 0) || (b >= ELHashSize))
            {
                return null;
            }
            he = ELHash[b];
            if ((he == null) || !he.DeletedEdge)
            {
                return he;
            }
            ELHash[b] = null;
            if ((he.ELrefcnt -= 1) == 0)
            {
                hfl.Push(he);
            }
            return null;
            #region c++
            //struct Halfedge *he;
	
            //if(b<0 || b>=ELhashsize) 
            //    return((struct Halfedge *) NULL);
            //he = ELhash[b]; 
            //if (he == (struct Halfedge *) NULL || he->ELedge != (struct Edge *) DELETED ) 
            //    return (he);
	
            ///* Hash table points to deleted half edge.  Patch as necessary. */
            //ELhash[b] = (struct Halfedge *) NULL;
            //if ((he -> ELrefcnt -= 1) == 0) 
            //    makefree((Freenode*)he, &hfl);
            //return ((struct Halfedge *) NULL);
            #endregion
        }
        HalfEdge ELLeftBnd(PointD p)
        {
            int i, bucket;
            HalfEdge he;
            bucket = (int)((p.X - Xmin) / deltaX * ELHashSize);
            if (bucket < 0) bucket = 0;
            if (bucket >= ELHashSize) bucket = ELHashSize - 1;
            he = ELGetHash(bucket);
            if (he == null)
            {
                for (i = 1; true; i++)
                {
                    if ((he = ELGetHash(bucket - i)) != null)
                    {
                        break;
                    }
                    if ((he = ELGetHash(bucket + i)) != null)
                    {
                        break;
                    }
                }
                totalSearch += i;
            }
            ntry++;
            if (he == ELLeftEnd || (he != ELRightEnd && RightOf(he, p)))
            {
                do
                {
                    he = he.ELright;
                }
                while (he != ELRightEnd && RightOf(he, p));
                he = he.ELleft;
            }
            else
            {
                do
                {
                    he = he.ELleft;
                }
                while (he != ELLeftEnd && !RightOf(he, p));
            }
            if ((bucket > 0) && (bucket < ELHashSize - 1))
            {
                if (ELHash[bucket] != null)
                {
                    ELHash[bucket].ELrefcnt--;
                }
                ELHash[bucket] = he;
                ELHash[bucket].ELrefcnt++;
            }
            return he;
            #region c++
            //int i, bucket;
            //    struct Halfedge *he;
	
            //    /* Use hash table to get close to desired halfedge */
            //    bucket = (int)((p->x - xmin)/deltax * ELhashsize);	//use the hash function to find the place in the hash map that this HalfEdge should be

            //    if(bucket<0) bucket =0;					//make sure that the bucket position in within the range of the hash array
            //    if(bucket>=ELhashsize) bucket = ELhashsize - 1;

            //    he = ELgethash(bucket);
            //    if(he == (struct Halfedge *) NULL)			//if the HE isn't found, search backwards and forwards in the hash map for the first non-null entry
            //    {   
            //        for(i=1; 1 ; i += 1)
            //        {	
            //            if ((he=ELgethash(bucket-i)) != (struct Halfedge *) NULL) 
            //                break;
            //            if ((he=ELgethash(bucket+i)) != (struct Halfedge *) NULL) 
            //                break;
            //        };
            //        totalsearch += i;
            //    };
            //    ntry += 1;
            //    /* Now search linear list of halfedges for the correct one */
            //    if (he==ELleftend  || (he != ELrightend && right_of(he,p)))
            //    {
            //        do 
            //        {
            //            he = he -> ELright;
            //        } while (he!=ELrightend && right_of(he,p));	//keep going right on the list until either the end is reached, or you find the 1st edge which the point
            //        he = he -> ELleft;				//isn't to the right of
            //    }
            //    else 							//if the point is to the left of the HalfEdge, then search left for the HE just to the left of the point
            //        do 
            //        {
            //            he = he -> ELleft;
            //        } while (he!=ELleftend && !right_of(he,p));
		
            //    /* Update hash table and reference counts */
            //    if(bucket > 0 && bucket <ELhashsize-1)
            //    {	
            //        if(ELhash[bucket] != (struct Halfedge *) NULL) 
            //        {
            //            ELhash[bucket] -> ELrefcnt -= 1;
            //        }
            //        ELhash[bucket] = he;
            //        ELhash[bucket] -> ELrefcnt += 1;
            //    };
            //    return (he);
            #endregion
        }

        void ELDelete(HalfEdge he)
        {
            he.ELleft.ELright = he.ELright;
            he.ELright.ELleft = he.ELleft;
            he.DeletedEdge = true;
            #region c++
            //(he -> ELleft) -> ELright = he -> ELright;
            //(he -> ELright) -> ELleft = he -> ELleft;
            //he -> ELedge = (struct Edge *)DELETED;
            #endregion
        }
        HalfEdge Elright(HalfEdge he)
        {
            return he.ELright;
        }
        HalfEdge Elleft(HalfEdge he)
        {
            return he.ELleft;
        }
        Site LeftReg(HalfEdge he)
        {
            if (he.ELedge == null)
            {
                return bottomsite;
            }
            return (he.ELpm == le ? he.ELedge.reg[le] : he.ELedge.reg[re]);
            #region c++
            //if(he -> ELedge == (struct Edge *)NULL) 
            //    return(bottomsite);
            //return( he -> ELpm == le ? 
            //    he -> ELedge -> reg[le] : he -> ELedge -> reg[re]);        
            #endregion
        }
        Site RightReg(HalfEdge he)
        {
            if (he.ELedge == null)
            {
                return bottomsite;
            }
            return (he.ELpm == le) ? he.ELedge.reg[re] : he.ELedge.reg[le];
            #region c++
            //if(he -> ELedge == (struct Edge *)NULL) //if this halfedge has no edge, return the bottom site (whatever that is)
            //    return(bottomsite);

            ////if the ELpm field is zero, return the site 0 that this edge bisects, otherwise return site number 1
            //return( he -> ELpm == le ? he -> ELedge -> reg[re] : he -> ELedge -> reg[le]); 
            #endregion
        }

        // Geometry
        //
        void GeomInit()
        {
            efl = new FreeList(this.sqrtNsites);
            nVertices = 0;
            nEdges = 0;
            deltaY = Ymax - Ymin;
            deltaX = Xmax - Xmin;
            #region c++
            //float sn;
            //freeinit(&efl, sizeof(Edge));
            //nvertices = 0;
            //nedges = 0;
            //sn = (float)nsites + 4;
            //sqrt_nsites = (int)sqrt(sn);
            //deltay = ymax - ymin;
            //deltax = xmax - xmin;
            #endregion
        }
        Edge Bisect(Site s1, Site s2)
        {
            double dx, dy, adx, ady;
            Edge newEdge;
            newEdge = efl.Pop<Edge>();
            newEdge.reg[0] = s1;
            newEdge.reg[1] = s2;
            Ref(s1);
            Ref(s2);
            newEdge.ep[0] = null;
            newEdge.ep[1] = null;
            dx = s2.coord.X - s1.coord.X;
            dy = s2.coord.Y - s1.coord.Y;
            adx = dx > 0 ? dx : -dx;
            ady = dy > 0 ? dy : -dy;
            newEdge.c = s1.coord.X * dx + s1.coord.Y * dy + (dx * dx + dy * dy) * 0.5;
            if (adx > ady)
            {
                newEdge.a = 1.0;
                newEdge.b = dy / dx;
                newEdge.c /= dx;
            }
            else
            {
                newEdge.b = 1.0;
                newEdge.a = dx / dy;
                newEdge.c /= dy;
            }
            newEdge.Edgenbr = nEdges;

            nEdges++;
            return newEdge;
            #region c++
            //float dx,dy,adx,ady;
            //    struct Edge *newedge;	

            //    newedge = (struct Edge *) getfree(&efl);
	
            //    newedge -> reg[0] = s1; //store the sites that this edge is bisecting
            //    newedge -> reg[1] = s2;
            //    ref(s1); 
            //    ref(s2);
            //    newedge -> ep[0] = (struct Site *) NULL; //to begin with, there are no endpoints on the bisector - it goes to infinity
            //    newedge -> ep[1] = (struct Site *) NULL;
	
            //    dx = s2->coord.x - s1->coord.x;			//get the difference in x dist between the sites
            //    dy = s2->coord.y - s1->coord.y;
            //    adx = dx>0 ? dx : -dx;					//make sure that the difference in positive
            //    ady = dy>0 ? dy : -dy;
            //    newedge -> c = (float)(s1->coord.x * dx + s1->coord.y * dy + (dx*dx + dy*dy)*0.5);//get the slope of the line

            //    if (adx>ady)
            //    {	
            //        newedge -> a = 1.0; 
            //        newedge -> b = dy/dx; 
            //        newedge -> c /= dx;  //set formula of line, with x fixed to 1
            //    }
            //    else
            //    {	
            //        newedge -> b = 1.0; 
            //        newedge -> a = dx/dy; 
            //        newedge -> c /= dy;  //set formula of line, with y fixed to 1
            //    };
	
            //    newedge -> edgenbr = nedges;

            //    //printf("\nbisect(%d) ((%f,%f) and (%f,%f)",nedges,s1->coord.x,s1->coord.y,s2->coord.x,s2->coord.y);
	
            //    nedges += 1;
            //    return(newedge);
            #endregion
        }
        Site Intersect(HalfEdge el1, HalfEdge el2)
        {
            Edge e1, e2, e;
            HalfEdge el;
            double d, xint, yint;
            bool right_of_site;
            Site v;
            e1 = el1.ELedge;
            e2 = el2.ELedge;
            if (e1 == null || e2 == null)
            {
                return null;
            }
            if (e1.reg[1] == e2.reg[1])
            {
                return null;
            }
            d = e1.a * e2.b - e1.b * e2.a;
            if ((-1.0e-10 < d) && (d < 1.0e-10))
            {
                return null;
            }
            xint = (e1.c * e2.b - e2.c * e1.b) / d;
            yint = (e2.c * e1.a - e1.c * e2.a) / d;
            if ((e1.reg[1].coord.Y < e2.reg[1].coord.Y) ||
                (e1.reg[1].coord.Y == e2.reg[1].coord.Y &&
                 e1.reg[1].coord.X < e2.reg[1].coord.X))
            {
                el = el1;
                e = e1;
            }
            else
            {
                el = el2;
                e = e2;
            }
            right_of_site = (xint >= e.reg[1].coord.X);
            if ((right_of_site && (el.ELpm == le)) || (!right_of_site && (el.ELpm == re)))
            {
                return null;
            }
            v = sfl.Pop<Site>();
            v.refcnt = 0;
            v.coord.X = xint;
            v.coord.Y = yint;
            return v;
            #region c++
            //        struct	Edge *e1,*e2, *e;
            //struct  Halfedge *el;
            //float d, xint, yint;
            //int right_of_site;
            //struct Site *v;
	
            //e1 = el1 -> ELedge;
            //e2 = el2 -> ELedge;
            //if(e1 == (struct Edge*)NULL || e2 == (struct Edge*)NULL) 
            //    return ((struct Site *) NULL);

            ////if the two edges bisect the same parent, return null
            //if (e1->reg[1] == e2->reg[1]) 
            //    return ((struct Site *) NULL);
	
            //d = e1->a * e2->b - e1->b * e2->a;
            //if (-1.0e-10 < d && d < 1.0e-10) 
            //    return ((struct Site *) NULL);
	
            //xint = (e1->c*e2->b - e2->c*e1->b)/d;
            //yint = (e2->c*e1->a - e1->c*e2->a)/d;
	
            //if( (e1->reg[1]->coord.y < e2->reg[1]->coord.y) ||
            //    (e1->reg[1]->coord.y == e2->reg[1]->coord.y &&
            //    e1->reg[1]->coord.x < e2->reg[1]->coord.x) )
            //{	
            //    el = el1; 
            //    e = e1;
            //}
            //else
            //{	
            //    el = el2; 
            //    e = e2;
            //};
	
            //right_of_site = xint >= e -> reg[1] -> coord.x;
            //if ((right_of_site && el -> ELpm == le) || (!right_of_site && el -> ELpm == re)) 
            //    return ((struct Site *) NULL);
	
            ////create a new site at the point of intersection - this is a new vector event waiting to happen
            //v = (struct Site *) getfree(&sfl);
            //v -> refcnt = 0;
            //v -> coord.x = xint;
            //v -> coord.y = yint;
            //return(v);
            #endregion
        }
        bool RightOf(HalfEdge el, PointD p)
        {
            Edge e;
            Site topSite;
            bool right_of_site, above, fast;
            double dxp, dyp, dxs, t1, t2, t3, yl;

            e = el.ELedge;
            topSite = e.reg[1];
            right_of_site = p.X > topSite.coord.X;
            if (right_of_site && (el.ELpm == le))
            {
                return true;
            }
            if (!right_of_site && (el.ELpm == re))
            {
                return false;
            }
            if (e.a == 1.0)
            {
                dyp = p.Y - topSite.coord.Y;
                dxp = p.X - topSite.coord.X;
                fast = false;
                if ((!right_of_site & (e.b < 0.0)) | (right_of_site & (e.b >= 0.0)))
                {
                    above = (dyp >= e.b * dxp);
                    fast = above;
                }
                else
                {
                    above = ((p.X + p.Y * e.b) > e.c);
                    if (e.b < 0.0) above = !above;
                    if (!above) fast = true;
                }
                if (!fast)
                {
                    dxs = topSite.coord.X - e.reg[0].coord.X;
                    above = ((e.b * (dxp * dxp - dyp * dyp)) < (dxs * dyp * (1.0 + 2.0 * dxp / dxs + e.b * e.b)));
                    if (e.b < 0.0) above = !above;
                }
            }
            else /*** e->b == 1.0 ***/
            {
                yl = e.c - e.a * p.X;
                t1 = p.Y - yl;
                t2 = p.X - topSite.coord.X;
                t3 = yl - topSite.coord.Y;
                above = ((t1 * t1) > ((t2 * t2) + (t3 * t3)));
            }
            return (el.ELpm == le ? above : !above);
            #region c++
            //        struct Edge *e;
            //struct Site *topsite;
            //int right_of_site, above, fast;
            //float dxp, dyp, dxs, t1, t2, t3, yl;
	
            //e = el -> ELedge;
            //topsite = e -> reg[1];
            //right_of_site = p -> x > topsite -> coord.x;
            //if(right_of_site && el -> ELpm == le) return(1);
            //if(!right_of_site && el -> ELpm == re) return (0);
	
            //if (e->a == 1.0)
            //{	dyp = p->y - topsite->coord.y;
            //dxp = p->x - topsite->coord.x;
            //fast = 0;
            //if ((!right_of_site & (e->b<0.0)) | (right_of_site & (e->b>=0.0)) )
            //{	above = dyp>= e->b*dxp;	
            //fast = above;
            //}
            //else 
            //{	above = p->x + p->y*e->b > e-> c;
            //if(e->b<0.0) above = !above;
            //if (!above) fast = 1;
            //};
            //if (!fast)
            //{	dxs = topsite->coord.x - (e->reg[0])->coord.x;
            //above = e->b * (dxp*dxp - dyp*dyp) <
            //    dxs*dyp*(1.0+2.0*dxp/dxs + e->b*e->b);
            //if(e->b<0.0) above = !above;
            //};
            //}
            //else  /*e->b==1.0 */
            //{	yl = e->c - e->a*p->x;
            //t1 = p->y - yl;
            //t2 = p->x - topsite->coord.x;
            //t3 = yl - topsite->coord.y;
            //above = t1*t1 > t2*t2 + t3*t3;
            //};
            //return (el->ELpm==le ? above : !above);
            #endregion
        }
        void EndPoint(Edge e, int lr, Site s)
        {
            e.ep[lr] = s;
            Ref(s);
            if (e.ep[re - lr] == null) return;
            SaveEdge(e);
            DeRef(e.reg[le]);
            DeRef(e.reg[re]);
            efl.Push(e);
            #region c++
            //e -> ep[lr] = s;
            //ref(s);
            //if(e -> ep[re-lr]== (struct Site *) NULL) 
            //    return;

            //clip_line(e);

            //deref(e->reg[le]);
            //deref(e->reg[re]);
            //makefree((Freenode*)e, &efl);
            #endregion
        }
        void SaveEdge(Edge e)
        {
            Edge edge = e.Clone();
            if (ClipEdge(edge))
            {
                resultEdges.Add(edge);
            }
        }
        bool ClipEdge(Edge e)
        {
            Site s1, s2;
            double x1 = 0, x2 = 0, y1 = 0, y2 = 0;

            x1 = e.reg[0].coord.X;
            x2 = e.reg[1].coord.X;
            y1 = e.reg[0].coord.Y;
            y2 = e.reg[1].coord.Y;

            if (Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) < minDistanceBetweenSites)
            {
                return false;
            }
            pxmin = borderMinX;
            pxmax = borderMaxX;
            pymin = borderMinY;
            pymax = borderMaxY;

            if (e.a == 1.0 && e.b >= 0.0)
            {
                s1 = e.ep[1];
                s2 = e.ep[0];
            }
            else
            {
                s1 = e.ep[0];
                s2 = e.ep[1];
            }
            if (e.a == 1.0)
            {
                y1 = pymin;
                if (s1 != null && s1.coord.Y > pymin)
                {
                    y1 = s1.coord.Y;
                }
                if (y1 > pymax)
                {
                    y1 = pymax;
                }
                x1 = e.c - e.b * y1;
                y2 = pymax;
                if (s2 != null && s2.coord.Y < pymax)
                {
                    y2 = s2.coord.Y;
                }
                if (y2 < pymin)
                {
                    y2 = pymin;
                }
                x2 = e.c - e.b * y2;
                if (((x1 > pxmax) & (x2 > pxmax)) | ((x1 < pxmin) & (x2 < pxmin)))
                {
                    return false;
                }
                if (x1 > pxmax)
                {
                    x1 = pxmax;
                    y1 = (e.c - x1) / e.b;
                }
                if (x1 < pxmin)
                {
                    x1 = pxmin;
                    y1 = (e.c - x1) / e.b;
                }
                if (x2 > pxmax)
                {
                    x2 = pxmax;
                    y2 = (e.c - x2) / e.b;
                }
                if (x2 < pxmin)
                {
                    x2 = pxmin;
                    y2 = (e.c - x2) / e.b;
                }
            }
            else
            {
                x1 = pxmin;
                if (s1 != null && s1.coord.X > pxmin)
                {
                    x1 = s1.coord.X;
                }
                if (x1 > pxmax)
                {
                    x1 = pxmax;
                }
                y1 = e.c - e.a * x1;
                x2 = pxmax;
                if (s2 != null && s2.coord.X < pxmax)
                {
                    x2 = s2.coord.X;
                }
                if (x2 < pxmin) x2 = pxmin;

                y2 = e.c - e.a * x2;
                if (((y1 > pymax) & (y2 > pymax)) | ((y1 < pymin) & (y2 < pymin)))
                {
                    return false;
                }
                if (y1 > pymax)
                {
                    y1 = pymax;
                    x1 = (e.c - y1) / e.a;
                }
                if (y1 < pymin)
                {
                    y1 = pymin;
                    x1 = (e.c - y1) / e.a;
                }
                if (y2 > pymax)
                {
                    y2 = pymax;
                    x2 = (e.c - y2) / e.a;
                }
                if (y2 < pymin)
                {
                    y2 = pymin;
                    x2 = (e.c - y2) / e.a;
                }
            }
            if (e.ep[0] == null) e.ep[0] = new Site();
            if (e.ep[1] == null) e.ep[1] = new Site();
            e.ep[0].coord.X = x1;
            e.ep[0].coord.Y = y1;
            e.ep[1].coord.X = x2;
            e.ep[1].coord.Y = y2;
            return true;
        }

        double Dist(Site s, Site t)
        {
            double dx, dy;
            dx = s.coord.X - t.coord.X;
            dy = s.coord.Y - t.coord.Y;
            return Math.Sqrt(dx * dx + dy * dy);
            #region c++
            //float dx, dy;
            //dx = s->coord.x - t->coord.x;
            //dy = s->coord.y - t->coord.y;
            //return (float)(sqrt(dx * dx + dy * dy));
            #endregion
        }
        void MakeVertex(Site v)
        {
            v.Sitenbr = nVertices;
            nVertices++;
            #region c++
            //v->sitenbr = nvertices;
            //nvertices += 1;
            //out_vertex(v);
            #endregion
        }
        void DeRef(Site v)
        {
            v.refcnt--;
            if (v.refcnt == 0)
            {
                sfl.Push(v);
            }
            #region c++
            //v->refcnt -= 1;
            //if (v->refcnt == 0)
            //    makefree((Freenode*)v, &sfl);
            #endregion
        }
        void Ref(Site v)
        {
            ++v.refcnt;
        }

        // Heap
        //
        void PQinsert(HalfEdge he, Site v, double offset)
        {
            HalfEdge last, next;
            he.vertex = v;
            Ref(v);
            he.ystar = v.coord.Y + offset;
            last = PQhash[PQbucket(he)];
            
            while (((next = last.PQnext) != null) &&
                (he.ystar > next.ystar ||
                (he.ystar == next.ystar &&
                 v.coord.X > next.vertex.coord.X)))
            {
                last = next;
            }
            he.PQnext = last.PQnext;
            last.PQnext = he;
            PQcount++;
            #region c++
            //struct Halfedge *last, *next;
            //    he -> vertex = v;
            //    ref(v);
            //    he -> ystar = (float)(v -> coord.y + offset);
            //    last = &PQhash[PQbucket(he)];
            //    while ((next = last -> PQnext) != (struct Halfedge *) NULL &&
            //        (he -> ystar  > next -> ystar  ||
            //        (he -> ystar == next -> ystar && v -> coord.x > next->vertex->coord.x)))
            //    {	
            //        last = next;
            //    };
            //    he -> PQnext = last -> PQnext; 
            //    last -> PQnext = he;
            //    PQcount += 1;
            #endregion
        }
        void PQdelete(HalfEdge he)
        {
            HalfEdge last;
            if (he.vertex != null)
            {
                last = PQhash[PQbucket(he)];
                while (last.PQnext != he)
                {
                    last = last.PQnext;
                }
                last.PQnext = he.PQnext;
                PQcount--;
                DeRef(he.vertex);
                he.vertex = null;
            }
            #region c++
            //struct Halfedge *last;
            //if(he -> vertex != (struct Site *) NULL)
            //{	
            //    last = &PQhash[PQbucket(he)];
            //    while (last -> PQnext != he) 
            //        last = last -> PQnext;

            //    last -> PQnext = he -> PQnext;
            //    PQcount -= 1;
            //    deref(he -> vertex);
            //    he -> vertex = (struct Site *) NULL;
            //};
            #endregion
        }
        int PQbucket(HalfEdge he)
        {
            int bucket;
            bucket = (int)((he.ystar - Ymin) / deltaY * PQhashsize);
            if (bucket < 0) bucket = 0;
            if (bucket >= PQhashsize) bucket = PQhashsize - 1;
            if (bucket < PQmin) PQmin = bucket;
            return bucket;
            #region c++
            //int bucket;
            //bucket = (int)((he->ystar - ymin) / deltay * PQhashsize);
            //if (bucket < 0) bucket = 0;
            //if (bucket >= PQhashsize) bucket = PQhashsize - 1;
            //if (bucket < PQmin) PQmin = bucket;
            //return (bucket);
            #endregion
        }
        bool PQempty
        {
            get { return PQcount == 0; }
        }
        PointD PQ_min()
        {
            PointD answer;
            while (PQhash[PQmin].PQnext == null)
            {
                PQmin++;
            }
            answer.X = PQhash[PQmin].PQnext.vertex.coord.X;
            answer.Y = PQhash[PQmin].PQnext.ystar;
            return answer;
            #region c++
            //struct Point answer;
            //while(PQhash[PQmin].PQnext == (struct Halfedge *)NULL) {PQmin += 1;};
            //answer.x = PQhash[PQmin].PQnext -> vertex -> coord.x;
            //answer.y = PQhash[PQmin].PQnext -> ystar;
            //return (answer);
            #endregion
        }
        HalfEdge PQextractmin()
        {
            HalfEdge curr = PQhash[PQmin].PQnext;
            PQhash[PQmin].PQnext = curr.PQnext;
            PQcount--;
            return curr;
            #region c++
            //struct Halfedge *curr;
            //curr = PQhash[PQmin].PQnext;
            //PQhash[PQmin].PQnext = curr -> PQnext;
            //PQcount -= 1;
            //return(curr);
            #endregion
        }
        void PQinitialize()
        {
            PQcount = 0;
            PQmin = 0;
            PQhashsize = 4 * sqrtNsites;
            PQhash = new HalfEdge[PQhashsize];
            for (int i = 0; i < PQhashsize; i++)
            {
                PQhash[i] = new HalfEdge();
                PQhash[i].Init();
                PQhash[i].PQnext = null;
            }
            #region c++
            //int i; 
	
            //PQcount = 0;
            //PQmin = 0;
            //PQhashsize = 4 * sqrt_nsites;
            //PQhash = (struct Halfedge *) myalloc(PQhashsize * sizeof *PQhash);

            //if(PQhash == 0)
            //    return false;

            //for(i=0; i<PQhashsize; i+=1) PQhash[i].PQnext = (struct Halfedge *)NULL;

            //return true;
            #endregion
        }
        
        void CreateVoronoi()
        {
            Site newSite, bot, top, temp, p, v;
            PointD newIntStar = new PointD();
            int pm;
            HalfEdge lbnd, rbnd, llbnd, rrbnd, bisector;
            Edge e;

            PQinitialize();
            bottomsite = NextOne();
            ELInitialize();
            newSite = NextOne();
            while (true)
            {
                if (!PQempty) newIntStar = PQ_min();
                
                if (newSite != null && (PQempty || newSite.coord.Y < newIntStar.Y ||
                   newSite.coord.Y == newIntStar.Y && newSite.coord.X < newIntStar.Y))
                {
                    lbnd = ELLeftBnd(newSite.coord);
                    rbnd = Elright(lbnd);
                    bot = RightReg(lbnd);
                    e = Bisect(bot, newSite);
                    bisector = HECreate(e, le);
                    ELInsert(lbnd, bisector);
                    if ((p = Intersect(lbnd, bisector)) != null)
                    {
                        PQdelete(lbnd);
                        PQinsert(lbnd, p, Dist(p, newSite));
                    }
                    lbnd = bisector;
                    bisector = HECreate(e, re);
                    ELInsert(lbnd, bisector);
                    if ((p = Intersect(bisector, rbnd)) != null)
                    {
                        PQinsert(bisector, p, Dist(p, newSite));
                    }
                    newSite = NextOne();
                }
                else if (!PQempty)
                {
                    lbnd = PQextractmin();
                    llbnd = Elleft(lbnd);
                    rbnd = Elright(lbnd);
                    rrbnd = Elright(rbnd);
                    bot = LeftReg(lbnd);
                    top = RightReg(rbnd);

                    v = lbnd.vertex;
                    MakeVertex(v);
                    EndPoint(lbnd.ELedge, lbnd.ELpm, v);
                    EndPoint(rbnd.ELedge, rbnd.ELpm, v);
                    ELDelete(lbnd);
                    PQdelete(rbnd);
                    ELDelete(rbnd);
                    pm = le;
                    if (bot.coord.Y > top.coord.Y)
                    {
                        temp = bot;
                        bot = top;
                        top = temp;
                        pm = re;
                    }
                    e = Bisect(bot, top);
                    bisector = HECreate(e, pm);
                    ELInsert(llbnd, bisector);
                    EndPoint(e, re - pm, v);
                    DeRef(v);
                    
                    if ((p = Intersect(llbnd, bisector))!= null)
                    {
                        PQdelete(llbnd);
                        PQinsert(llbnd, p, Dist(p, bot));
                    }
                    p = Intersect(bisector, rrbnd);
                    if (p != null)
                    {
                        PQinsert(bisector, p, Dist(p, bot));
                    }
                }
                else
                {
                    break;
                }
            }
            for (lbnd = Elright(ELLeftEnd); lbnd != ELRightEnd; lbnd = Elright(lbnd))
            {
                e = lbnd.ELedge;
                SaveEdge(e);
            }
            #region c++
            //        struct Site *newsite, *bot, *top, *temp, *p;
            //struct Site *v;
            //struct Point newintstar;
            //int pm;
            //struct Halfedge *lbnd, *rbnd, *llbnd, *rrbnd, *bisector;
            //struct Edge *e;
	
            //PQinitialize();
            //bottomsite = nextone();
            //out_site(bottomsite);
            //bool retval = ELinitialize();

            //if(!retval)
            //    return false;
	
            //newsite = nextone();
            //while(1)
            //{

            //    if(!PQempty()) 
            //        newintstar = PQ_min();
		
            //    //if the lowest site has a smaller y value than the lowest vector intersection, process the site
            //    //otherwise process the vector intersection		

            //    if (newsite != (struct Site *)NULL 	&& (PQempty() || newsite -> coord.y < newintstar.y
            //        || (newsite->coord.y == newintstar.y && newsite->coord.x < newintstar.x)))
            //    {/* new site is smallest - this is a site event*/
            //        out_site(newsite);						//output the site
            //        lbnd = ELleftbnd(&(newsite->coord));				//get the first HalfEdge to the LEFT of the new site
            //        rbnd = ELright(lbnd);						//get the first HalfEdge to the RIGHT of the new site
            //        bot = rightreg(lbnd);						//if this halfedge has no edge, , bot = bottom site (whatever that is)
            //        e = bisect(bot, newsite);					//create a new edge that bisects 
            //        bisector = HEcreate(e, le);					//create a new HalfEdge, setting its ELpm field to 0			
            //        ELinsert(lbnd, bisector);					//insert this new bisector edge between the left and right vectors in a linked list	

            //        if ((p = intersect(lbnd, bisector)) != (struct Site *) NULL) 	//if the new bisector intersects with the left edge, remove the left edge's vertex, and put in the new one
            //        {	
            //            PQdelete(lbnd);
            //            PQinsert(lbnd, p, dist(p,newsite));
            //        };
            //        lbnd = bisector;						
            //        bisector = HEcreate(e, re);					//create a new HalfEdge, setting its ELpm field to 1
            //        ELinsert(lbnd, bisector);					//insert the new HE to the right of the original bisector earlier in the IF stmt

            //        if ((p = intersect(bisector, rbnd)) != (struct Site *) NULL)	//if this new bisector intersects with the
            //        {	
            //            PQinsert(bisector, p, dist(p,newsite));			//push the HE into the ordered linked list of vertices
            //        };
            //        newsite = nextone();	
            //    }
            //    else if (!PQempty()) /* intersection is smallest - this is a vector event */			
            //    {	
            //        lbnd = PQextractmin();						//pop the HalfEdge with the lowest vector off the ordered list of vectors				
            //        llbnd = ELleft(lbnd);						//get the HalfEdge to the left of the above HE
            //        rbnd = ELright(lbnd);						//get the HalfEdge to the right of the above HE
            //        rrbnd = ELright(rbnd);						//get the HalfEdge to the right of the HE to the right of the lowest HE 
            //        bot = leftreg(lbnd);						//get the Site to the left of the left HE which it bisects
            //        top = rightreg(rbnd);						//get the Site to the right of the right HE which it bisects

            //        out_triple(bot, top, rightreg(lbnd));		//output the triple of sites, stating that a circle goes through them

            //        v = lbnd->vertex;						//get the vertex that caused this event
            //        makevertex(v);							//set the vertex number - couldn't do this earlier since we didn't know when it would be processed
            //        endpoint(lbnd->ELedge,lbnd->ELpm,v);	//set the endpoint of the left HalfEdge to be this vector
            //        endpoint(rbnd->ELedge,rbnd->ELpm,v);	//set the endpoint of the right HalfEdge to be this vector
            //        ELdelete(lbnd);							//mark the lowest HE for deletion - can't delete yet because there might be pointers to it in Hash Map	
            //        PQdelete(rbnd);							//remove all vertex events to do with the  right HE
            //        ELdelete(rbnd);							//mark the right HE for deletion - can't delete yet because there might be pointers to it in Hash Map	
            //        pm = le;								//set the pm variable to zero
			
            //        if (bot->coord.y > top->coord.y)		//if the site to the left of the event is higher than the Site
            //        {										//to the right of it, then swap them and set the 'pm' variable to 1
            //            temp = bot; 
            //            bot = top; 
            //            top = temp; 
            //            pm = re;
            //        }
            //        e = bisect(bot, top);					//create an Edge (or line) that is between the two Sites. This creates
            //                                                //the formula of the line, and assigns a line number to it
            //        bisector = HEcreate(e, pm);				//create a HE from the Edge 'e', and make it point to that edge with its ELedge field
            //        ELinsert(llbnd, bisector);				//insert the new bisector to the right of the left HE
            //        endpoint(e, re-pm, v);					//set one endpoint to the new edge to be the vector point 'v'.
            //                                                //If the site to the left of this bisector is higher than the right
            //                                                //Site, then this endpoint is put in position 0; otherwise in pos 1
            //        deref(v);								//delete the vector 'v'

            //        //if left HE and the new bisector don't intersect, then delete the left HE, and reinsert it 
            //        if((p = intersect(llbnd, bisector)) != (struct Site *) NULL)
            //        {	
            //            PQdelete(llbnd);
            //            PQinsert(llbnd, p, dist(p,bot));
            //        };

            //        //if right HE and the new bisector don't intersect, then reinsert it 
            //        if ((p = intersect(bisector, rrbnd)) != (struct Site *) NULL)
            //        {	
            //            PQinsert(bisector, p, dist(p,bot));
            //        };
            //    }
            //    else break;
            //};
            //for(lbnd=ELright(ELleftend); lbnd != ELrightend; lbnd=ELright(lbnd))
            //{	
            //    e = lbnd -> ELedge;

            //    clip_line(e);
            //};

            //cleanup();

            //return true;
            #endregion
        }
        Site NextOne()
        {
            Site s;
            if (siteIdx < nSites)
            {
                s = sites[siteIdx];
                siteIdx++;
                return s;
            }
            else
            {
                return null;
            }
            #region c++
            //struct Site *s;
            //if(siteidx < nsites)
            //{	
            //    s = &sites[siteidx];
            //    siteidx += 1;
            //    return(s);
            //}
            //else	
            //    return( (struct Site *)NULL);
            #endregion
        }

        #region Qsort
        static void QuickSort(Site[] m, int a, int b)
        {
            int A = a;
            int B = b;
            Site mid;
            if (b > a)
            {
                mid = m[(a + b) / 2];
                while (A <= B)
                {
                    while ((A < b) && (m[A].coord.Y < mid.coord.Y)) ++A;
                    while ((B > a) && (m[B].coord.Y > mid.coord.Y)) --B;
                    if (A <= B)
                    {
                        Site t = m[A];
                        m[A] = m[B];
                        m[B] = t;
                        ++A;
                        --B;
                    }
                }
                if (a < B) QuickSort(m, a, B);
                if (A < b) QuickSort(m, A, b);
            }
        }
        static void QuickSort(Site[] m)
        {
            QuickSort(m, 0, m.Length - 1);
        }
        #endregion
    }
}