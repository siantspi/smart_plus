﻿namespace FortuneVoronoi
{
    public struct PointD
    {
        public double X;
        public double Y;
    }
    public class Site : FreeNode
    {
        public PointD coord;
        public int Sitenbr;
        public int refcnt;
        public Site() { }
        public Site Clone()
        {
            Site s = new Site();
            s.coord = coord;
            return s;
        }
    }
    public class Edge : FreeNode
    {
        public double a, b, c;
        public Site[] ep;
        public Site[] reg;
        public int Edgenbr;
        public Edge()
        {
            ep = new Site[2];
            ep[0] = new Site(); ep[1] = new Site();
            reg = new Site[2];
            reg[0] = new Site(); reg[1] = new Site();
        }
        public Edge Clone()
        {
            Edge newEdge = new Edge();
            newEdge.a = a;
            newEdge.b = b;
            newEdge.c = c;
            if (ep[0] != null) newEdge.ep[0] = ep[0].Clone(); else newEdge.ep[0] = null;
            if (ep[1] != null) newEdge.ep[1] = ep[1].Clone(); else newEdge.ep[1] = null;
            newEdge.reg[0] = reg[0].Clone();
            newEdge.reg[1] = reg[1].Clone();
            newEdge.Edgenbr = Edgenbr;
            return newEdge;
        }
    }
    class HalfEdge : FreeNode
    {
        public HalfEdge ELleft;
        public HalfEdge ELright;
        public Edge ELedge;
        public bool DeletedEdge;
        public int ELrefcnt;
        public int ELpm;
        public Site vertex;
        public double ystar;
        public HalfEdge PQnext;

        public HalfEdge()
        {
            ELedge = new Edge();
            vertex = new Site();
            DeletedEdge = false;
        }
        public override void Init()
        {
            ELleft = new HalfEdge();
            ELright = new HalfEdge();
            PQnext = new HalfEdge();
        }
    }
}
