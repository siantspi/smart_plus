﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace SmartPlus
{
    public sealed class VoronoiCell : C2DObject
    {
        public PhantomWell Well;
        public Contour Cntr;
        public PVTParamsItem Pvt;
        public Palette pal;
        public Font font;
        public bool PaletteByNBZ;
        public double AccumOil;
        public double NBZ, NIZ;
        public bool ShiftY;

        public VoronoiCell() { }
        public VoronoiCell(Project Project, DateTime Date, Well Well)
        {
            this.Well = new PhantomWell(Well.OilFieldCode, Well);
            this.Well.icons = this.Well.srcWell.GetIcons(Project.OilFields[Well.OilFieldIndex].CoefDict, Date);
            this.Well.icons.SetActiveAllIcon();
            ShiftY = Well.icons.CheckIconCode(new ushort[] { 35, 36, 37, 39, 44, 47, 50 });
            this.Well.CaptionVisible = false;
            this.Well.VisibleName = false;
            this.Well.X = Well.X;
            this.Well.Y = Well.Y;
            Cntr = new Contour(0);
            pal = null;
            font = null;
        }
        public bool HitTest(float X, float Y)
        {
            double rx = C2DObject.RealXfromScreenX(X);
            double ry = C2DObject.RealYfromScreenY(Y);
            return Cntr.PointBodyEntry(rx, ry);
        }
        public VoronoiCell(Project Project, DateTime Date, Well Well, Contour Contour) : this(Project, Date, Well)
        {
            Contour.SetMinMax();
            this.Cntr = Contour;
        }
        public void SetIconsFonts(Font[] fonts, ExFontList exFontList)
        {
            if (Well != null) Well.SetIconsFonts(fonts, exFontList);
        }

        // Load Save
        public bool LoadFromCache(Project Project, BinaryReader br)
        {
            int ofIndex = br.ReadInt32();
            if (ofIndex >= Project.OilFields.Count) return false;
            OilField of = Project.OilFields[ofIndex];

            int wellIndex = br.ReadInt32();
            if (wellIndex > of.Wells.Count) return false;
            Well w = of.Wells[wellIndex];
            
            string name = br.ReadString();
            if (w.Name != name) return false;

            this.Well = new PhantomWell(w.OilFieldCode, w);
            this.Well.VisibleName = false;
            this.Well.CaptionVisible = false;
            this.Well.X = br.ReadDouble();
            this.Well.Y = br.ReadDouble();

            int count = br.ReadInt32(), plastCode;

            WellIcon wIcon;
            Well.icons = new WellsIconList(count);
            for (int j = 0; j < count; j++)
            {
                plastCode = br.ReadInt32();
                wIcon = new WellIcon(Well.X, Well.Y, plastCode);
                wIcon.Read(br);
                Well.icons.AddIcon(wIcon);
            }
            Well.icons.SetActiveAllIcon();
            ShiftY = Well.icons.CheckIconCode(new ushort[] { 35, 36, 37, 39, 44, 47, 50 });
            Pvt = PVTParamsItem.Empty;
            Pvt.ReadFromBin(br);

            count = br.ReadInt32();
            Cntr = new Contour(count);
            double x, y;
            for (int i = 0; i < count; i++)
            {
                x = br.ReadDouble();
                y = br.ReadDouble();
                Cntr.Add(x, y);
            }
            Cntr.Xmin = br.ReadDouble();
            Cntr.Ymin = br.ReadDouble();
            Cntr.Xmax = br.ReadDouble();
            Cntr.Ymax = br.ReadDouble();
            Cntr._Closed = true;
            NBZ = br.ReadDouble();
            NIZ = br.ReadDouble();
            AccumOil = br.ReadDouble();
            return true;
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(Well.srcWell.OilFieldIndex);
            bw.Write(Well.srcWell.Index);
            bw.Write(Well.srcWell.Name);
            bw.Write(Well.X);
            bw.Write(Well.Y);

            if (Well.icons != null)
            {
                WellIcon wIcon;
                bw.Write(Well.icons.Count);
                for (int j = 0; j < Well.icons.Count; j++)
                {
                    wIcon = Well.icons[j];
                    bw.Write(wIcon.StratumCode);
                    wIcon.Write(bw);
                }
            }
            Pvt.WriteToBin(bw);
            bw.Write(Cntr._points.Count);
            for (int i = 0; i < Cntr._points.Count; i++)
            {
                bw.Write(Cntr._points[i].X);
                bw.Write(Cntr._points[i].Y);
            }
            bw.Write(Cntr.Xmin);
            bw.Write(Cntr.Ymin);
            bw.Write(Cntr.Xmax);
            bw.Write(Cntr.Ymax);
            bw.Write(NBZ);
            bw.Write(NIZ);
            bw.Write(AccumOil);
        }

        // Drawing
        public override void DrawObject(Graphics grfx)
        {
            if (Cntr != null && pal != null)
            {
                PointF[] points = new PointF[Cntr._points.Count + 1];
                for (int i = 0; i < Cntr._points.Count; i++)
                {
                    points[i].X = (float)ScreenXfromRealX(Cntr._points[i].X);
                    points[i].Y = (float)ScreenYfromRealY(Cntr._points[i].Y);
                }
                points[points.Length - 1].X = points[0].X;
                points[points.Length - 1].Y = points[0].Y;
                using (Brush br = new SolidBrush(Color.FromArgb(200, pal.GetColorByX(PaletteByNBZ ? NBZ : this.NIZ - this.AccumOil))))
                {
                    grfx.FillPolygon(br, points);
                    if (Selected) grfx.FillPolygon(SmartPlusGraphics.VoronoiMap.Brushes.CrossLine, points);
                    grfx.DrawPolygon(this.Selected ? SmartPlusGraphics.Contour.Pens.Black3 : Pens.Black, points);
                }
            }
            if (Well != null)
            {
                Well.DrawObject(grfx);
            }
        }
        public void DrawCaption(Graphics grfx)
        {
            if (Well != null && font != null && st_VisbleLevel < 4)
            {
                float x = ScreenXfromRealX(Well.X);
                float y = ScreenYfromRealY(Well.Y);
                string str = string.Format("НИЗ: {0:0.###} тыс.т\nОИЗ: {1:0.###} тыс.т", NIZ / 1000, (NIZ - AccumOil) / 1000);
                SizeF size = grfx.MeasureString(str, font, PointF.Empty, StringFormat.GenericTypographic);
                x -= size.Width / 2;
                y += st_radiusY;
                if (ShiftY) y += st_radiusY * 0.5f;
                grfx.DrawString(str, font, Brushes.Black, x, y, StringFormat.GenericTypographic);
            }
            Well.DrawCaption(grfx);
        }
        public void DrawSelectedPoints(Graphics grfx)
        {
            if (Selected)
            {
                PointF[] points = new PointF[Cntr._points.Count + 1];
                for (int i = 0; i < Cntr._points.Count; i++)
                {
                    points[i].X = (float)ScreenXfromRealX(Cntr._points[i].X);
                    points[i].Y = (float)ScreenYfromRealY(Cntr._points[i].Y);
                }
                points[points.Length - 1].X = points[0].X;
                points[points.Length - 1].Y = points[0].Y;
                for (int i = 0; i < Cntr._points.Count; i++)
                {
                    grfx.FillEllipse(Brushes.Yellow, points[i].X - 3.5f, points[i].Y - 3.5f, 7, 7);
                    grfx.DrawEllipse(Pens.Black, points[i].X - 4, points[i].Y - 4, 8, 8);
                }
            }
        }
    }
}
