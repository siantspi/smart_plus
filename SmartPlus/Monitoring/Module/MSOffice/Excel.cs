﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;
using System.ComponentModel;

namespace SmartPlus.MSOffice
{
    sealed class Excel
    {
        #region Constants
        public const int xlCellTypeLastCell = 11;
        public const int Max_Rows = 1048575;

        public enum XlCalculation
        {
            Manual = -4135,
            Automatic = -4105,
            Semiautomatic = 2,
        }
        public enum XLAutoFillType
        {
            xlFillDefault = 0,
            xlFillCopy = 1,
            xlFillSeries = 2,
            xlFillFormats = 3,
            xlFillValues = 4,
            xlFillDays = 5,
            xlFillWeekdays = 6,
            xlFillMonths = 7,
            xlFillYears = 8,
            xlLinearTrend = 9,
            xlGrowthTrend = 10
        }
        public enum XLPasteType
        {
            xlPasteAll = -4104,
            xlPasteAllExceptBorders = 7,
            xlPasteColumnWidths = 8,
            xlPasteComments = -4144,
            xlPasteFormats = -4122,
            xlPasteFormulas = -4123,
            xlPasteFormulasAndNumberFormats = 11,
            xlPasteValidation = 6,
            xlPasteValues = -4163,
            xlPasteValuesAndNumberFormats = 12
        }
        public enum XLPasteSpecialOperation
        {
            None = -4142,
            Add = 2,
            Subtract = 3,
            Multiply = 4,
            Divide = 5
        }

        public enum XLSortType
        {
            xlSortValues = 1,
            xlSortLabels = 2
        }
        public enum XLSortOrder
        {
            xlAscending = 1,
            xlDescending = 2
        }
        public enum XLSortMethod
        {
            xlPinYin = 1,
            xlStroke = 2
        }
        public enum XLSortOrientation
        {
            xlSortColumns = 1,
            xlSortRows = 2
        }
        public enum XLYesNoGuess
        {
            xlGuess = 0,
            xlYes = 1,
            xlNo = 2
        }
        public enum XLSortOn
        {
            SortOnValues = 0,
            SortOnCellColor = 1,
            SortOnFontColor = 2,
            SortOnIcon = 3
        }
        public enum XLSortDataOption
        {
            xlSortNormal = 0,
            xlSortTextAsNumbers = 1
        }
        public enum XLConst
        {
            xlBottom = -4107,
            xlLeft = -4131,
            xlRight = -4152,
            xlTop = -4160
        }
        #endregion
        public struct SortField
        {
            public int ColumnIndex;
            public XLSortOn SortOn;
            public XLSortOrder SortOrder;
            public XLSortDataOption DataOption;

            public void Init(int index, XLSortOn sortOn, XLSortOrder sortOrder, XLSortDataOption dataOption)
            {
                ColumnIndex = index;
                SortOn = sortOn;
                SortOrder = sortOrder;
                DataOption = dataOption;
            }
        }

        const string AppName = "Excel.Application";
        object app;
        object WorkBooks, WorkBook, WorkSheets, WorkSheet, Range;

        public Excel()
        {
            app = null;
            try
            {
                Type excelType = Type.GetTypeFromProgID(AppName);
                app = Activator.CreateInstance(excelType);
            }
            catch
            {
                app = null;
            }
            if (app == null) throw new Exception("Ошибка подключения к Microsoft Excel!");
        }

        /// <summary>
        /// Видимость Excel - свойство
        /// </summary>
        public bool Visible
        {
            set
            {
                if (app == null) return;
                if (value) EnableEvents = true;
                app.GetType().InvokeMember("Visible", BindingFlags.SetProperty, null, app, new object[] { value });
            }
        }
        /// <summary>
        /// Включение/отключение обработки events, обновления экрана и расчетов
        /// </summary>
        public bool EnableEvents
        {
            set
            {
                if (app != null) return;
                app.GetType().InvokeMember("EnableEvents", BindingFlags.SetProperty, null, app, new object[] { value });
                app.GetType().InvokeMember("ScreenUpdating", BindingFlags.SetProperty, null, app, new object[] { value });
                app.GetType().InvokeMember("Calculation", BindingFlags.SetProperty, null, app, new object[] { (value) ? XlCalculation.Automatic : XlCalculation.Manual });
            }
        }
        /// <summary>
        /// Закрыть Excel и освободить ресурсы
        /// </summary>
        public void Quit()
        {
            if (app == null) return;
            CloseWorkbook(false);
            app.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, app, null);
            Disconnect();
        }
        /// <summary>
        /// Отключиться от Excel не трогая его
        /// </summary>
        public void Disconnect()
        {
            if (Range != null) Marshal.ReleaseComObject(Range);
            if (WorkSheet != null) Marshal.ReleaseComObject(WorkSheet);
            if (WorkSheets != null) Marshal.ReleaseComObject(WorkSheets);
            if (WorkBook != null) Marshal.ReleaseComObject( WorkBook);
            if (WorkBooks != null) Marshal.ReleaseComObject(WorkBooks);
            if(app != null) Marshal.ReleaseComObject(app);
            Range = null; WorkSheet = null; WorkSheets = null;
            WorkBook = null; WorkBooks = null; app = null;
            GC.GetTotalMemory(true);
        }
        /// <summary>
        /// Сброс копирования данных
        /// </summary>
        public void CancelCopy()
        {
            if (app == null) return;
            app.GetType().InvokeMember("CutCopyMode", BindingFlags.SetProperty, null, app, new object[] { false });
        }

        public string GetDecimalSeparator()
        {
            if (app == null) return "";
            return Convert.ToString(app.GetType().InvokeMember("DecimalSeparator", BindingFlags.GetProperty, null, app, null));
        }

        #region Cells
        /// <summary>
        /// Переводит порядковый номер столбца в его буквенный эквивалент.
        /// </summary>
        string ParseColNum(int ColNum)
        {
            StringBuilder sb = new StringBuilder();
            if (ColNum <= 0) return "A";
            //ColNum++;
            int alfa, rem;
            alfa = (int)(ColNum / 26);
            rem = ColNum - (alfa * 26) + 1;
            if (alfa > 0) sb.Append((char)(alfa + 64));
            if (rem > 0) sb.Append((char)(rem + 64));
            return sb.ToString();
        }
        string GetCellName(int Row, int Column) { return string.Format("{0}{1}", ParseColNum(Column), Row + 1); }
        string GetColumnName(int Column) { return string.Format("{0}:{0}", ParseColNum(Column)); }
        string GetRowName(int Row) { return string.Format("{0}:{0}", Row + 1); }
        string GetRegionName(int Row1, int Column1, int Row2, int Column2)
        {
            return string.Format("{0}:{1}", GetCellName(Row1, Column1), GetCellName(Row2, Column2));
        }
        string GetRCName(int Row, int Column) { return string.Format("R{0}C{0}", Row + 1, Column + 1); }
        #endregion

        #region WorkBook
        /// <summary>
        /// Открыть документ
        /// </summary>
        public void OpenWorkbook(string name)
        {
            if (app == null) return;
            
            WorkBooks = app.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, app, null);
            bool exit = false;
            // Открываем по готовности Excel
            while (!exit)
            {
                exit = true;
                try
                {
                    WorkBook = WorkBooks.GetType().InvokeMember("Open", BindingFlags.InvokeMethod, null, WorkBooks, new object[] { name, true });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.Message.IndexOf("0x800AC472") != -1)
                    {
                        System.Threading.Thread.Sleep(50);
                        exit = false;
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            WorkSheets = WorkBook.GetType().InvokeMember("Worksheets", BindingFlags.GetProperty, null, WorkBook, null);
            WorkSheet = WorkSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkSheets, new object[] { 1 });
        }
        
        /// <summary>
        /// Новый документ
        /// </summary>
        public void NewWorkbook()
        {
            WorkBooks = app.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, app, null);
            WorkBook = WorkBooks.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, WorkBooks, null);
            WorkSheets = WorkBook.GetType().InvokeMember("Worksheets", BindingFlags.GetProperty, null, WorkBook, null);
            WorkSheet = WorkSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkSheets, new object[] { 1 });
        }

        /// <summary>
        /// Закрыть документ
        /// </summary>
        public void CloseWorkbook(bool SaveChanges)
        {
            if (WorkBook == null) return;
            bool exit = false;
            // Закрываем по готовности Excel
            while (!exit)
            {
                exit = true;
                try
                {
                    CancelCopy();
                    WorkBook.GetType().InvokeMember("Close", BindingFlags.InvokeMethod, null, WorkBook, new object[] { SaveChanges });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.Message.IndexOf("0x800AC472") != -1)
                    {
                        System.Threading.Thread.Sleep(50);
                        exit = false;
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            
            if (Range != null) Marshal.ReleaseComObject(Range);
            if (WorkSheet != null) Marshal.ReleaseComObject(WorkSheet);
            if (WorkSheets != null) Marshal.ReleaseComObject(WorkSheets);
            if (WorkBook != null) Marshal.ReleaseComObject(WorkBook);
            if (WorkBooks != null) Marshal.ReleaseComObject(WorkBooks);
            Range = null; WorkSheet = null; WorkSheets = null;
            WorkBook = null; WorkBooks = null;
        }

        /// <summary>
        /// Сохранить документ
        /// </summary>
        public void SaveWorkbook(string name)
        {
            if (File.Exists(name))
                WorkBook.GetType().InvokeMember("Save", BindingFlags.InvokeMethod, null, WorkBook, null);
            else
                WorkBook.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, WorkBook, new object[] { name });
        }

        /// <summary>
        /// Количество документов
        /// </summary>
        /// <returns></returns>
        public int GetWorkbooksCount()
        {
            if (app == null) return 0;
            WorkBooks = app.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, app, null);
            return Convert.ToInt32(WorkBooks.GetType().InvokeMember("Count", BindingFlags.GetProperty, null, WorkBooks, null));
        }

        /// <summary>
        /// Установка текущей книги
        /// </summary>
        /// <returns></returns>
        public void SetActiveWorkbook(int WorkbookIndex)
        {
            if (app == null) return;
            WorkBooks = app.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, app, null);
            WorkBook = WorkBooks.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkBooks, new object[] { WorkbookIndex  + 1 });
            WorkSheets = WorkBook.GetType().InvokeMember("Worksheets", BindingFlags.GetProperty, null, WorkBook, null);
            WorkSheet = WorkSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkSheets, new object[] { 1 });
        }
        #endregion

        #region WorkSheet
        public void AddNewWorkSheet()
        {
            if (WorkBook == null) return;
            WorkSheets = WorkBook.GetType().InvokeMember("WorkSheets", BindingFlags.GetProperty, null, WorkBook, null);
            WorkSheet = WorkSheets.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, WorkSheets, null);
        }
        public int GetWorkSheetsCount()
        {
            if (WorkBook == null) return 0;
            WorkSheets = WorkBook.GetType().InvokeMember("WorkSheets", BindingFlags.GetProperty, null, WorkBook, null);
            return Convert.ToInt32(WorkSheets.GetType().InvokeMember("Count", BindingFlags.GetProperty, null, WorkSheets, null));
        }
        public int GetLastUsedRow()
        {
            if (WorkSheet == null) return -1;
            object Cells = WorkSheet.GetType().InvokeMember("Cells", BindingFlags.GetProperty, null, WorkSheet, null);
            object LastCell = Cells.GetType().InvokeMember("SpecialCells", BindingFlags.GetProperty, null, Cells, new object[] { xlCellTypeLastCell });
            return Convert.ToInt32(LastCell.GetType().InvokeMember("Row", BindingFlags.GetProperty, null, LastCell, null));
        }
        public int GetLastUsedColumn()
        {
            if (WorkSheet == null) return -1;
            object Cells = WorkSheet.GetType().InvokeMember("Cells", BindingFlags.GetProperty, null, WorkSheet, null);
            object LastCell = Cells.GetType().InvokeMember("SpecialCells", BindingFlags.GetProperty, null, Cells, new object[] { xlCellTypeLastCell });
            return Convert.ToInt32(LastCell.GetType().InvokeMember("Column", BindingFlags.GetProperty, null, LastCell, null));
        }
        public void SetActiveSheet(int WorkSheetIndex)
        {
            if (WorkBook == null) return;
            WorkSheet = WorkSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkSheets, new object[] { WorkSheetIndex + 1 });
        }
        public void SetActiveSheetName(string Name)
        {
            if (WorkSheet == null) return;
            WorkSheet.GetType().InvokeMember("Name", BindingFlags.SetProperty, null, WorkSheet, new object[] { Name }); 
        }
        public string GetActiveSheetName()
        {
            if (WorkSheet == null) return string.Empty;
            return Convert.ToString(WorkSheet.GetType().InvokeMember("Name", BindingFlags.GetProperty, null, WorkSheet, null));
        }

        //УСТАНОВИТЬ ОРИЕНТАЦИЮ СТРАНИЦЫ 
        //1 - КНИЖНЫЙ
        //2 - АЛЬБОМНЫЙ
        public void SetOrientation(int Orientation)
        {
            //Range.Interior.ColorIndex
            object PageSetup = WorkSheet.GetType().InvokeMember("PageSetup", BindingFlags.GetProperty,
                null, WorkSheet, null);

            PageSetup.GetType().InvokeMember("Orientation", BindingFlags.SetProperty,
                null, PageSetup, new object[] { Orientation });
        }

        
        #endregion

        #region Cells
        /// <summary>
        /// Select ячейки
        /// </summary>
        /// <param name="Row">Строка (с 0)</param>
        /// <param name="Column">Столбец (с 0)</param>
        public void SelectCell(int Row, int Column)
        {
            if (WorkSheet == null) return;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetCellName(Row, Column) });
            Range.GetType().InvokeMember("Select", BindingFlags.InvokeMethod, null, Range, null);
        }
        /// <summary>
        /// Запись значения в ячейку
        /// </summary>
        public void SetValue(string range, object value)
        {
            if (WorkSheet == null) return;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { range });
            Range.GetType().InvokeMember("Value", BindingFlags.SetProperty, null, Range, new object[] { value });
        }
        public void SetValue(int Row, int Column, object value)
        {
            SetValue(GetCellName(Row, Column), value);
        }
        /// <summary>
        /// Запись значения в ячейку
        /// </summary>
        public void SetFormula(string range, string formula)
        {
            if (WorkSheet == null) return;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { range });
            Range.GetType().InvokeMember("FormulaR1C1", BindingFlags.SetProperty, null, Range, new object[] { formula });
        }
        public void SetFormula(int Row, int Column, string formula)
        {
            SetFormula(GetCellName(Row, Column), formula);
        }
        public string GetFormula(int Row, int Column)
        {
            if (WorkSheet == null) return string.Empty;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetCellName(Row, Column) });
            return Convert.ToString(Range.GetType().InvokeMember("FormulaR1C1", BindingFlags.GetProperty, null, Range, null));
        }

        /// <summary>
        /// Чтение данных из ячейки
        /// </summary>
        public object GetValue(string range)
        {
            if (WorkSheet == null) return string.Empty;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,  null, WorkSheet, new object[] { range });
            object value = Range.GetType().InvokeMember("Value", BindingFlags.GetProperty, null, Range, null);
            return (value == null ? string.Empty : value.ToString());
        }
        /// <summary>
        /// Чтение данных из ячейки (нумерация с 0)
        /// </summary>
        public object GetValue(int Row, int Column)
        {
            return GetValue(GetCellName(Row, Column));
        }

        #region Настройки отображения ячеек
        /// <summary>
        /// Объединение и выравнивание в ячейках
        /// </summary>
        public void SetMerge(string range, int Alignment)
        {
            if (WorkSheet == null) return;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { range });
            object[] args = new object[] { Alignment };
            Range.GetType().InvokeMember("MergeCells", BindingFlags.SetProperty, null, Range, new object[] { true });
            Range.GetType().InvokeMember("HorizontalAlignment", BindingFlags.SetProperty, null, Range, args);
        }

        //УСТАНОВИТЬ ШИРИНУ СТОЛБЦОВ
        public void SetColumnWidth(string range, double Width)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { range });
            object[] args = new object[] { Width };
            Range.GetType().InvokeMember("ColumnWidth", BindingFlags.SetProperty, null, Range, args);
        }

        //УСТАНОВИТЬ ВЫСОТУ СТРОК
        public void SetRowHeight(string range, double Height)
        {
            if (WorkSheet == null) return;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { range });
            object[] args = new object[] { Height };
            Range.GetType().InvokeMember("RowHeight", BindingFlags.SetProperty, null, Range, args);
        }

        //УСТАНОВИТЬ ВИД РАМКИ ВОКРУГ ЯЧЕЙКИ
        public void SetBorderStyle(string range, int Style)
        {
            if (WorkSheet == null) return;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { range });
            object[] args = new object[] { 1 };
            object[] args1 = new object[] { 1 };
            object Borders = Range.GetType().InvokeMember("Borders", BindingFlags.GetProperty, null, Range, null);
            Borders = Range.GetType().InvokeMember("LineStyle", BindingFlags.SetProperty, null, Borders, args);
        }

        //УСТАНОВИТЬ ВЫРАВНИВАНИЕ В ЯЧЕЙКЕ ПО ВЕРТИКАЛИ
        public void SetVerticalAlignment(int Row1, int Column1, int Row2, int Column2, XLConst Alignment)
        {
            if (WorkSheet == null || Row1 > Max_Rows) return;
            if (Row2 > Max_Rows) Row2 = Max_Rows;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Range.GetType().InvokeMember("VerticalAlignment", BindingFlags.SetProperty, null, Range, new object[] { Alignment });
        }

        //УСТАНОВИТЬ ВЫРАВНИВАНИЕ В ЯЧЕЙКЕ ПО ГОРИЗОНТАЛИ
        public void SetHorisontalAlignment(int Row1, int Column1, int Row2, int Column2, XLConst Alignment)
        {
            if (WorkSheet == null || Row1 > Max_Rows) return;
            if (Row2 > Max_Rows) Row2 = Max_Rows;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Range.GetType().InvokeMember("HorizontalAlignment", BindingFlags.SetProperty, null, Range, new object[] { Alignment });
        }
        #endregion
        #endregion

        #region Range

        // Get-Set Value Arrays
        //
        public object[,] GetRangeValues(int Row1, int Column1, int Row2, int Column2)
        {
            if (WorkSheet == null || Row1 > Max_Rows) return new object[0, 0];
            if (Row2 > Max_Rows) Row2 = Max_Rows;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            return (object[,])Range.GetType().InvokeMember("Value", BindingFlags.GetProperty, null, Range, null);
        }
        public void SetRange(int Row1, int Column1, int Row2, int Column2, object[,] RangeValues)
        {
            if (WorkSheet == null || Row1 > Max_Rows) return;
            if (Row2 > Max_Rows) Row2 = Max_Rows;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Range.GetType().InvokeMember("Value", BindingFlags.SetProperty, null, Range, new object[] { RangeValues });
        }
        public void SetRowsArray(int StartRow, int StartCol, List<object[]> Array)
        {
            SetRowsArray(null, null, StartRow, StartCol, Array);
        }
        public void SetRowsArray(BackgroundWorker worker, DoWorkEventArgs e, int StartRow, int StartCol, List<object[]> Array)
        {
            if (Array.Count > 0)
            {
                int i, j, r, endRow = -1;
                object[,] varRow = (object[,])System.Array.CreateInstance(typeof(object), new int[2] { 50, Array[0].Length }, new int[2] { 0, 0 });
                for (i = 0; i < Array.Count; i += 50)
                {
                    if (worker != null && worker.CancellationPending)
                    {
                        e.Cancel = true;
                        this.Quit();
                        return;
                    }
                    for (r = 0; r < 50; r++)
                    {
                        if ((i + r >= Array.Count) || (Array[i + r] == null))
                        {
                            endRow = i + r - 1;
                            break;
                        }
                        else
                        {
                            for (j = 0; j < Array[i].Length; j++)
                            {
                                varRow[r, j] = Array[i + r][j];
                            }
                            endRow = i + 50 - 1;
                        }
                    }
                    this.SetRange(StartRow + i, StartCol, StartRow + endRow, StartCol + Array[0].Length - 1, varRow);
                    if (worker != null) worker.ReportProgress(i);
                }
            }
        }

        // Auto Fill
        //
        public void RangeRowAutoFill(int Row1, int Column1, int Row2, int Column2)
        {
            RangeRowAutoFill(Row1, Column1, Row2, Column2, XLAutoFillType.xlFillDefault);
        }
        public void RangeRowAutoFill(int Row1, int Column1, int Row2, int Column2, XLAutoFillType FillType)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row1, Column2) });
            object range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Range.GetType().InvokeMember("AutoFill", BindingFlags.InvokeMethod, null, Range, new object[] { range, FillType });
        }
        public void RangeColumnAutoFill(int Row1, int Column1, int Row2, int Column2)
        {
            RangeColumnAutoFill(Row1, Column1, Row2, Column2, XLAutoFillType.xlFillDefault);
        }
        public void RangeColumnAutoFill(int Row1, int Column1, int Row2, int Column2, XLAutoFillType FillType)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column1) });
            object range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Range.GetType().InvokeMember("AutoFill", BindingFlags.InvokeMethod, null, Range, new object[] { range, FillType });
        }

        // Copy-Paste
        //
        public void RangeCopy(int Row1, int Column1, int Row2, int Column2)
        {
            if (WorkSheet == null || Row1 > Max_Rows) return;
            if (Row2 > Max_Rows) Row2 = Max_Rows;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Range.GetType().InvokeMember("Copy", BindingFlags.InvokeMethod, null, Range, null);
        }
        public void RangePaste(int Row1, int Column1, int Row2, int Column2, XLPasteType PasteType, XLPasteSpecialOperation Operation, bool SkipBlanks, bool Transpose)
        {
            if (WorkSheet == null || Row1 > Max_Rows) return;
            if (Row2 > Max_Rows) Row2 = Max_Rows;
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Range.GetType().InvokeMember("PasteSpecial", BindingFlags.InvokeMethod, null, Range, new object[] { PasteType, Operation, SkipBlanks, Transpose });
        }
        public void RangeClear(int Row1, int Column1, int Row2, int Column2)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Range.GetType().InvokeMember("ClearContents", BindingFlags.InvokeMethod, null, Range, null);
        }

        // Сортировка
        //
        public void RangeSort(int Row1, int Column1, int Row2, int Column2, SortField[] SortParams)
        {
            if (WorkSheet == null || Row1 > Max_Rows || SortParams.Length == 0) return;
            if (Row2 > Max_Rows) Row2 = Max_Rows;
            object Sort, SortFields, range;
            Sort = WorkSheet.GetType().InvokeMember("Sort", BindingFlags.GetProperty, null, WorkSheet, null);
            SortFields = Sort.GetType().InvokeMember("SortFields", BindingFlags.GetProperty, null, Sort, null);
            if (Sort == null || SortFields == null) return;
            SortFields.GetType().InvokeMember("Clear", BindingFlags.InvokeMethod, null, SortFields, null);
            for (int i = 0; i < SortParams.Length; i++)
            {
                range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1 + SortParams[i].ColumnIndex, Row2, Column1 + SortParams[i].ColumnIndex) });
                if (range != null)
                {
                    SortFields.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, SortFields, new object[] { range, SortParams[i].SortOn, SortParams[i].SortOrder, null, SortParams[i].DataOption });
                }
            }
            range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[] { GetRegionName(Row1, Column1, Row2, Column2) });
            Sort.GetType().InvokeMember("SetRange", BindingFlags.InvokeMethod, null, Sort, new object[] { range });
            Sort.GetType().InvokeMember("MatchCase", BindingFlags.SetProperty, null, Sort, new object[] { false });
            Sort.GetType().InvokeMember("Orientation", BindingFlags.SetProperty, null, Sort, new object[] { XLSortOrientation.xlSortColumns });
            Sort.GetType().InvokeMember("SortMethod", BindingFlags.SetProperty, null, Sort, new object[] { XLSortMethod.xlPinYin });
            Sort.GetType().InvokeMember("Apply", BindingFlags.InvokeMethod, null, Sort, null);    
        }
        #endregion
    }
}
