﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    enum ColumnDataType : byte
    {
        NONE,
        WELL_NAME,
        OILFIELD_NAME,
        OILFIELD_AREA_NAME,
        TEXT,
        DATATIME,
        DOUBLE,
        COMMENT
    };

    class ProblemWellList : DataGridView
    {
        internal enum WORKER_WORKTYPE
        {
            FILL_TABLE,
            FILL_OILFIELD_COLUMN,
            UPDATE_COLUMN_STYLE,
            TEST_NEED_AREA_COLUMN
        }

        #region DATA TYPE NAMES
        static string[] DataTypeNames = 
        {
            "Не определено", 
            "Номер скважины",
            "Месторождение",
            "Площадь",
            "Текст",
            "Дата",
            "Число"
        };
        #endregion

        internal enum ProblemCellColor
        {
            WHITE,
            PINK,
            GREEN,
            GRAY,
            YELLOW,
        }
        internal struct ProblemColumn
        {
            public ColumnDataType DataType;
            public string Name { get { return DataTypeNames[(int)DataType]; } }
            public bool CorrectColumn;
            public ProblemCellColor ColorIndex;
        }
        internal class ProblemCell
        {
            public bool CorrectValue;
            public int ObjIndex;
            public ProblemCellColor ColorIndex;
            public string Value;
            public string Error;
            public ProblemCell()
            {
                CorrectValue = false;
                ObjIndex = -1;
                ColorIndex = 0;
                Value = "";
                Error = "";
            }
        }
        internal class WorkerArgument
        {
            public List<string> ClipboardRows;
            public ColumnDataType[] ColDataTypes;
        }

        ColumnDataType[] DataTypes;
        ProblemColumn[] ProblemColumns;
        ProblemCell[][] ProblemTable;
        int RowsCount;

        Label label, lNum;
        Button bUp;
        Panel pClose;
        Project SourceProject;
        WellListSettings wellListSettings;
        
        ProgressBar progress;
        bool WaitMode;
        public bool DrawWaitMode
        {
            get { return WaitMode; }
            set
            {
                progress.Visible = value;
                this.Height = value ? this.Parent.ClientRectangle.Height - 25 - 18 : this.Parent.ClientRectangle.Height - 25;
                this.bUp.Enabled = !value;
                this.ContextMenuStrip = (value) ? null : contextMenu;
                WaitMode = value;
                this.Invalidate();
            }
        }
        OilFieldAreaDictionary areaDict;
        ContextMenuStrip contextMenu;
        DataGridViewCell MouseDownCell = null;
        private ComboBox ComboBoxValues = null;
        private struct ComboBoxValue
        {
            public int ObjIndex;
            public string ObjName;
        }
        List<ComboBoxValue> ComboBoxValueCollection;
        int countMouseDown = 0;
        int DropDownMouseOver = -1;
        int WellColumn = -1, OilFieldColumn = -1, OilFieldAreaColumn = -1;
        DataGridViewCell[] SavedSelectedCells = null;
        List<PhantomWell> addedWells;
        BackgroundWorker worker;
        WORKER_WORKTYPE WorkType;

        public ProblemWellList(WellListSettings WLSettings, Control Panel)
        {
            wellListSettings = WLSettings;
            this.Parent = Panel;
            this.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.Dock = DockStyle.Fill;
            this.Font = new Font("Calibri", 8.25f);
            this.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;

            DataTypes = null;
            ProblemTable = null;
            ProblemColumns = null;
            RowsCount = 0;
            this.EnableHeadersVisualStyles = false;
            contextMenu = new ContextMenuStrip();
            DropDownMouseOver = -1;
            addedWells = new List<PhantomWell>();

            progress = new ProgressBar();
            Panel.Controls.Add(progress);
            progress.Visible = true;
            Point pt = Point.Empty;
            progress.Dock = DockStyle.Bottom;
            progress.Height = 18;
           

            #region COMBOBOX
            ComboBoxValues = new ComboBox();
            ComboBoxValues.BackColor = Color.FromArgb(175, 232, 255);
            ComboBoxValues.DropDownStyle = ComboBoxStyle.DropDownList;
            ComboBoxValues.DroppedDown = true;
            ComboBoxValues.Visible = false;
            this.Controls.Add(ComboBoxValues);
            ComboBoxValueCollection = new List<ComboBoxValue>();
            #endregion

            #region Close Button
            pClose = new Panel();
            Panel.Controls.Add(pClose);
            pClose.BackgroundImage = Properties.Resources.delete;
            pClose.BackgroundImageLayout = ImageLayout.Center;
            pClose.BorderStyle = BorderStyle.None;
            pClose.Size = new Size(16, 16);
            pClose.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            pClose.Location = new Point(Panel.Width - 17, 4);
            #endregion

            this.VirtualMode = true;
            this.Dock = DockStyle.None;
            this.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

            #region Labels
            label = new Label();
            label.AutoSize = true;
            label.Font = new Font("Segoe UI", 9.0f);
            label.Text = "Окно редактирования списка скважин";
            label.Location = new Point(0, 4);
            Panel.Controls.Add(label);

            lNum = new Label();
            lNum.AutoSize = true;
            lNum.Font = new Font("Segoe UI", 9.0f);
            lNum.Text = "N строк:";
            Panel.Controls.Add(lNum);
            #endregion

            this.Location = new Point(0, 25);
            this.Height = Panel.ClientRectangle.Height - 25;
            this.Width = Panel.ClientRectangle.Width;

            #region Up Button
            bUp = new Button();
            Panel.Controls.Add(bUp);
            bUp.TextImageRelation = TextImageRelation.ImageBeforeText;
            bUp.Image = Properties.Resources.up2;
            bUp.Text = "Принять";
            bUp.AutoSize = true;
            Size sz = Size.Empty;
            sz.Height = 21;
            sz.Width = 87;
            bUp.Size = sz;
            bUp.MinimumSize = sz;
            pt = new Point();
            pt.Y = -1;
            pt.X = label.Width;
            bUp.Location = pt;
            bUp.Click += new EventHandler(bUp_Click);
            #endregion

            this.RowTemplate.Height = 16;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = false;
            this.DoubleBuffered = true;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            #region WORKER
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            #endregion

            // EVENTS
            // COMBOBOX EVENTS
            ComboBoxValues.SelectedIndexChanged += new EventHandler(ComboBoxValues_SelectedIndexChanged);
            ComboBoxValues.KeyDown += new KeyEventHandler(ComboBoxValues_KeyDown);
            // PCLODE EVENTS
            pClose.Click += new EventHandler(pClose_Click);
            pClose.MouseEnter += new EventHandler(pClose_MouseEnter);
            pClose.MouseLeave += new EventHandler(pClose_MouseLeave);
            pClose.MouseDown += new MouseEventHandler(pClose_MouseDown);

            // CONTEXT MENU
            contextMenu.Opening += new System.ComponentModel.CancelEventHandler(contextMenu_Opening);
            DrawWaitMode = false;

            // GRID VIRTUAL MODE
            this.CellToolTipTextNeeded += new DataGridViewCellToolTipTextNeededEventHandler(grid_CellToolTipTextNeeded);
            this.CellValueNeeded += new DataGridViewCellValueEventHandler(grid_CellValueNeeded);
            this.CellValuePushed += new DataGridViewCellValueEventHandler(grid_CellValuePushed);
            this.CellPainting += new DataGridViewCellPaintingEventHandler(grid_CellPainting);
            // GRID EVENTS
            this.ColumnWidthChanged += new DataGridViewColumnEventHandler(grid_ColumnWidthChanged);
            this.MouseWheel += new MouseEventHandler(grid_MouseWheel);
            this.MouseMove += new MouseEventHandler(grid_MouseMove);
            this.MouseUp += new MouseEventHandler(grid_MouseUp);
            this.MouseDown += new MouseEventHandler(grid_MouseDown);
            this.CellValueChanged += new DataGridViewCellEventHandler(grid_CellValueChanged);
            this.Paint += new PaintEventHandler(ProblemWellList_Paint);
            this.SizeChanged += new EventHandler(grid_SizeChanged);
            this.KeyDown += new KeyEventHandler(grid_KeyDown);
        }
        public void SetProject(Project MapProject) 
        { 
            this.SourceProject = MapProject;
            if (MapProject != null)
            {
                areaDict = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            }
        }
        
        #region Заполнение из Clipboard
        public void FillTable(List<string> ClipboardRows)
        {
            WorkerArgument arg = new WorkerArgument();
            arg.ClipboardRows = ClipboardRows;
            arg.ColDataTypes = null;
            Clear();
            WorkType = WORKER_WORKTYPE.FILL_TABLE;
            wellListSettings.DrawWaitMode = true;
            this.Enabled = false;
            WorkerRun(arg);
        }
        public void FillTable(List<string> ClipboardRows, ColumnDataType[] ColDataTypes)
        {
            WorkerArgument arg = new WorkerArgument();
            arg.ClipboardRows = ClipboardRows;
            arg.ColDataTypes = ColDataTypes;
            Clear();
            WorkType = WORKER_WORKTYPE.FILL_TABLE;
            wellListSettings.DrawWaitMode = true;
            this.Enabled = false;
            WorkerRun(arg);
        }
        void FillTable(BackgroundWorker worker, DoWorkEventArgs e, List<string> ClipboardRows, ColumnDataType[] ColDataTypes)
        {
            if ((ClipboardRows != null) && (ClipboardRows.Count > 0))
            {
                FillGridByClipboard(worker, e, ClipboardRows, ColDataTypes);
                FillOilFieldGridColumn(worker, e);
                if (worker.CancellationPending) return;
                if ((WellColumn != -1) && (ProblemColumns != null))
                {
                    bool result = false;
                    for (int i = 0; i < this.ProblemColumns.Length; i++)
                    {
                        if (ProblemColumns[i].CorrectColumn)
                        {
                            result = true;
                            break;
                        }
                    }
                    if (result) AddWellToMainTable(worker, e);
                }
            }
        }
        void FillGridByClipboard(BackgroundWorker worker, DoWorkEventArgs e, List<string> ClipboardRows, ColumnDataType[] ColDataTypes)
        {
            int i, j;
            char[] columnSep = { '\t' };
            string[] cols;

            ProblemColumns = null;
            ProblemTable = null;

            WellColumn = -1;
            OilFieldColumn = -1;
            OilFieldAreaColumn = -1;
            worker.ReportProgress(0);
            if (ColDataTypes == null)
            {
                ColumnDataType[] TryingDataTypes = TryClipBoardDataTypes(ClipboardRows);
                if (TryingDataTypes != null) { DataTypes = TryingDataTypes; }
            }
            else
            {
                DataTypes = new ColumnDataType[ColDataTypes.Length];
                for (i = 0; i < ColDataTypes.Length; i++)
                {
                    DataTypes[i] = ColDataTypes[i];
                    switch (ColDataTypes[i])
                    {
                        case ColumnDataType.WELL_NAME:
                            WellColumn = i;
                            break;
                        case ColumnDataType.OILFIELD_NAME:
                            OilFieldColumn = i;
                            break;
                        case ColumnDataType.OILFIELD_AREA_NAME:
                            OilFieldAreaColumn = i;
                            break;
                    }
                }
            }
            worker.ReportProgress(0);
            float prog = 0, max = ClipboardRows.Count - 1;
            for (i = 0; i < ClipboardRows.Count; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                cols = ClipboardRows[i].Split(columnSep, StringSplitOptions.None);
                if (ProblemTable == null)
                {
                    if (cols.Length == 0) continue;
                    if (DataTypes == null) 
                    {
                        DataTypes = new ColumnDataType[cols.Length];
                    }
                    ProblemColumns = new ProblemColumn[DataTypes.Length];

                    for(j = 0; j < DataTypes.Length;j++)
                    {
                        ProblemColumns[j] = new ProblemColumn();
                        ProblemColumns[j].DataType = DataTypes[j];
                    }
                    
                    ProblemTable = new ProblemCell[ClipboardRows.Count][];
                    RowsCount = ClipboardRows.Count;
                }
                ProblemTable[i] = new ProblemCell[ProblemColumns.Length];
                for (j = 0; j < ProblemColumns.Length; j++)
                {
                    ProblemTable[i][j] = new ProblemCell();
                    ProblemTable[i][j].Value = cols[j].Trim();
                }
                prog = (float)i * 100f / max;
                worker.ReportProgress((int)prog);
            }
            worker.ReportProgress(0);
            for (i = 0; i < ProblemColumns.Length; i++)
            {
                SetColumnCellsStyle(worker, e, i);
            }
        }
        #endregion

        #region Предобработка текста Clipboard
        public ColumnDataType[] TryClipBoardDataTypes(List<string> ClipboardRows)
        {
            ColumnDataType[] result = null;
            char[] columnSep = { '\t' };
            string[] cols = null;
            int sumNone = 0;
            bool res;
            bool restart = false;
            for (int i = 0; i < ClipboardRows.Count; i++)
            {
                cols = ClipboardRows[i].Split(columnSep);
                for (int col = 0; col < cols.Length; col++)
                {
                    cols[col] = cols[col].Trim();
                }
                if ((cols != null) && (cols.Length > 0))
                {
                    if (result == null)
                    {
                        result = new ColumnDataType[cols.Length];
                    }
                    for (int j = 0; j < cols.Length; j++)
                    {
                        if (result[j] == ColumnDataType.NONE)
                        {
                            result[j] = TryClipboardCellType(j, cols, true);
                            if (result[j] != ColumnDataType.NONE)
                            {
                                switch (result[j])
                                {
                                    case ColumnDataType.WELL_NAME:
                                        WellColumn = j;
                                        break;
                                    case ColumnDataType.OILFIELD_NAME:
                                        OilFieldColumn = j;
                                        restart = true;
                                        break;
                                    case ColumnDataType.OILFIELD_AREA_NAME:
                                        OilFieldAreaColumn = j;
                                        break;
                                }
                            }
                        }
                        if (restart) break;
                    }
                    if (restart)
                    {
                        i--;
                        restart = false;
                    }
                }
                if (result != null)
                {
                    sumNone = 0;
                    for (int j = 0; j < result.Length; j++)
                    {
                        if (result[j] == ColumnDataType.NONE) sumNone++;
                    }
                }
                if (sumNone == 0) break;
            }
            worker.ReportProgress(33);
            // автоматическое добавление столбца месторождения
            res = false;
            if (OilFieldColumn == -1)
            {
                ColumnDataType[] newResult = new ColumnDataType[result.Length + 1];
                for (int i = 0; i < result.Length; i++)
                {
                    newResult[i] = result[i];
                }
                newResult[result.Length] = ColumnDataType.OILFIELD_NAME;
                OilFieldColumn = result.Length;
                result = newResult;
                res = FillOilFieldColumn(ref ClipboardRows, ref result);
            }
            worker.ReportProgress(100);
            if (IsNeedOilFieldAreaColumn(ref ClipboardRows))
            {
                ColumnDataType[] newResult = new ColumnDataType[result.Length + 1];
                for (int i = 0; i < result.Length; i++)
                {
                    newResult[i] = result[i];
                }
                newResult[result.Length] = ColumnDataType.OILFIELD_AREA_NAME;
                OilFieldAreaColumn = result.Length;
                result = newResult;
                res = FillOilFieldAreas(ref ClipboardRows, ref result) || res;
            }
            for (int i = 0; i < ClipboardRows.Count; i++)
            {
                cols = ClipboardRows[i].Split(columnSep);
                for (int j = 0; j < cols.Length; j++) cols[j] = cols[j].Trim();
                
                if ((cols != null) && (cols.Length > 0))
                {
                    if (result == null)
                    {
                        result = new ColumnDataType[cols.Length];
                    }
                    for (int j = 0; j < cols.Length; j++)
                    {
                        if (result[j] == ColumnDataType.NONE)
                        {
                            result[j] = TryClipboardCellType(j, cols, false);
                            if (result[j] != ColumnDataType.NONE)
                            {
                                switch (result[j])
                                {
                                    case ColumnDataType.WELL_NAME:
                                        WellColumn = j;
                                        break;
                                    case ColumnDataType.OILFIELD_NAME:
                                        OilFieldColumn = j;
                                        break;
                                    case ColumnDataType.OILFIELD_AREA_NAME:
                                        OilFieldAreaColumn = j;
                                        break;
                                }
                            }
                        }
                    }
                }
                if (result != null)
                {
                    sumNone = 0;
                    for (int j = 0; j < result.Length; j++)
                    {
                        if (result[j] == ColumnDataType.NONE) sumNone++;
                    }
                }
                if (sumNone == 0) break;
            }
            worker.ReportProgress(100);
            return result;
        }
        bool FillOilFieldColumn(ref List<string> ClipboardRows, ref ColumnDataType[] ResultTypes)
        {
            char[] columnSep = { '\t' };
            string[] cols = null;
            string str;
            int indArea;
            OilField ofActive;
            bool result = false;
            if (SourceProject.indActiveOilField > 0)
            {
                ofActive = SourceProject.OilFields[SourceProject.indActiveOilField];
                // если неопределен столбец скважины
                if (WellColumn == -1)
                {
                    // пытаемся найти по текущему месторождению
                    for (int i = 0; i < ClipboardRows.Count; i++)
                    {
                        str = ClipboardRows[i] + "\t" + ofActive.Name;
                        cols = str.Split(columnSep);
                        if ((cols != null) && (cols.Length > 0))
                        {
                            for (int j = 0; j < cols.Length; j++)
                            {
                                if (ResultTypes[j] == ColumnDataType.NONE)
                                {
                                    ResultTypes[j] = TryClipboardCellType(j, cols, true);
                                    if (ResultTypes[j] == ColumnDataType.WELL_NAME)
                                    {
                                        WellColumn = j;
                                        break;
                                    }
                                }
                            }
                        }
                        if (WellColumn != -1) break;
                    }
                    if (WellColumn != -1)
                    {
                        string wellName, ofName;
                        OilField of;
                        int ind, k, countInd, ofCode;
                        for (int i = 0; i < ClipboardRows.Count; i++)
                        {
                            str = ClipboardRows[i] + "\t" + ofActive.Name;
                            cols = str.Split(columnSep);
                            k = -1;
                            countInd = 0;
                            ind = -1;
                            ofName = " ";
                            if ((cols != null) && (cols.Length > 0))
                            {
                                wellName = cols[WellColumn].Trim().ToUpper();
                                // есть ли на текущем
                                if (ofActive != null) ind = ofActive.GetWellIndex(wellName);
                                if (ind != -1)
                                {
                                    ofName = ofActive.Name;
                                    result = true;
                                }
                                else // на текущем не найдено
                                {
                                    if (OilFieldAreaColumn != -1)
                                    {
                                        indArea = areaDict.GetIndexByShortName(cols[OilFieldAreaColumn].Trim());
                                        ofCode = areaDict[indArea].OilFieldCode;

                                        if (indArea != -1)
                                        {
                                            for (int j = 1; j < SourceProject.OilFields.Count; j++)
                                            {
                                                of = SourceProject.OilFields[j];
                                                if (of.ParamsDict.OraFieldCode == ofCode)
                                                {
                                                    if (of.GetWellIndex(wellName, areaDict[indArea].Code) != -1)
                                                    {
                                                        k = j;
                                                        countInd++;
                                                    }
                                                }
                                                if (countInd > 0) break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        for (int j = 1; j < SourceProject.OilFields.Count; j++)
                                        {
                                            of = SourceProject.OilFields[j];
                                            if (of.GetWellIndex(wellName) != -1)
                                            {
                                                k = j;
                                                countInd++;
                                            }
                                            if (countInd > 1) break;
                                        }
                                    }
                                    if (countInd == 1)
                                    {
                                        ofName = (SourceProject.OilFields[k]).Name;
                                        result = true;
                                    }
                                }
                            }
                            str = ClipboardRows[i] + "\t" + ofName;
                            ClipboardRows[i] = str;
                        }
                    }
                }
            }
            if (!result)
            {
                for (int i = 0; i < ClipboardRows.Count; i++)
                {
                    str = ClipboardRows[i] + "\t ";
                    ClipboardRows[i] = str;
                }
            }
            return result;
        }
        bool FillOilFieldAreas(ref List<string> ClipboardRows, ref ColumnDataType[] ResultTypes)
        {
            char[] columnSep = { '\t' };
            string[] cols = null;
            string str;
            bool result = false;
            int ind;
            if ((WellColumn != -1) && (OilFieldColumn != -1))
            {
                string wellName, ofName, ofAreaName = " ";
                OilField of;
                for (int i = 0; i < ClipboardRows.Count; i++)
                {
                    ofAreaName = " ";
                    cols = ClipboardRows[i].Split(columnSep);
                    if ((cols != null) && (cols.Length > 0))
                    {
                        wellName = cols[WellColumn].Trim().ToUpper();
                        ofName = cols[OilFieldColumn].Trim().ToUpper();
                        ind = SourceProject.GetOFIndex(ofName);
                        if (ind != -1)
                        {
                            of = SourceProject.OilFields[ind];
                            ind = of.TestEqualsWell(wellName);
                            if (ind > -1)
                            {
                                ind = areaDict.GetIndexByCode(of.Wells[ind].OilFieldAreaCode);
                                if (ind != -1)
                                {
                                    ofAreaName = areaDict[ind].Name;
                                }
                            }
                        }
                    }
                    str = ClipboardRows[i] + "\t" + ofAreaName;
                    ClipboardRows[i] = str;
                }
            }
            return result;
        }
        bool IsNeedOilFieldAreaColumn(ref List<string> ClipboardRows)
        {
            if ((OilFieldColumn != -1) && (WellColumn != -1) && (OilFieldAreaColumn == -1))
            {
                char[] columnSep = { '\t' };
                string[] cols = null;
                string wellName, ofName;
                OilField of;
                int ind;
                for (int i = 0; i < ClipboardRows.Count; i++)
                {
                    cols = ClipboardRows[i].Split(columnSep);
                    wellName = cols[WellColumn].Trim().ToUpper();
                    ofName = cols[OilFieldColumn].Trim().ToUpper();
                    ind = SourceProject.GetOFIndex(ofName);
                    if (ind != -1)
                    {
                        of = SourceProject.OilFields[ind];
                        ind = of.TestEqualsWell(wellName);
                        if (ind == -2) return true;
                    }
                }
            }
            return false;
        }
        ColumnDataType TryClipboardCellType(int index, string[] ClipColumns, bool FindOilFieldArea)
        {
            ColumnDataType result = ColumnDataType.NONE;
            if ((ClipColumns != null) && (index < ClipColumns.Length) && (ClipColumns[index] != ""))
            {
                char[] digits = new char[10] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                bool findInDigits = false;
                char firstSym = ClipColumns[index][0];
                if (firstSym == '-')
                {
                    if (ClipColumns[index].Length > 1) firstSym = ClipColumns[index][1];
                }
                for (int i = 0; i < digits.Length; i++)
                {
                    if (digits[i] == firstSym)
                    {
                        findInDigits = true;
                        break;
                    }
                }
                if (!findInDigits) // мест площадь или текст
                {
                    int ind = -1;
                    if (OilFieldColumn == -1)
                    {
                        ind = SourceProject.GetOFIndex(ClipColumns[index], true);
                    }
                    if (ind != -1)
                    {
                        result = ColumnDataType.OILFIELD_NAME;
                    }
                    else
                    {
                        ind = -1;
                        if (OilFieldAreaColumn == -1)
                        {
                            ind = areaDict.GetIndexByShortName(ClipColumns[index]);
                        }
                        if (ind != -1)
                        {
                            result = ColumnDataType.OILFIELD_AREA_NAME;
                        }
                    }
                    if ((result == ColumnDataType.NONE) &&
                        (OilFieldColumn != -1) && (OilFieldColumn != index) &&
                        ((!FindOilFieldArea) || ((OilFieldAreaColumn != -1) && (OilFieldAreaColumn != index))))
                    {
                        result = ColumnDataType.TEXT;
                    }
                }
                else // скважина дата или число
                {
                    int ind = ClipColumns[index].IndexOf('.');
                    int lastInd = ClipColumns[index].LastIndexOf('.');
                    if ((ind != -1) && (lastInd != -1) && (lastInd != ind))
                    {
                        try
                        {
                            DateTime dt = Convert.ToDateTime(ClipColumns[index]);
                            result = ColumnDataType.DATATIME;
                        }
                        catch 
                        {
                            result = ColumnDataType.NONE;
                        }
                    }
                    else if ((ind == lastInd) && ((ind > 0) || (WellColumn != -1)))
                    {
                        try
                        {
                            double d = Convert.ToDouble(ClipColumns[index]);
                            result = ColumnDataType.DOUBLE;
                        }
                        catch 
                        {
                            result = ColumnDataType.NONE;
                        }
                    }
                    else if (OilFieldColumn != -1)
                    {
                        ind = SourceProject.GetOFIndex(ClipColumns[OilFieldColumn], true);
                        if (ind != -1)
                        {
                            ind = (SourceProject.OilFields[ind]).GetWellIndex(ClipColumns[index]);
                            if (ind != -1)
                            {
                                result = ColumnDataType.WELL_NAME;
                            }
                        }
                    }
                }
            }
            return result;
        }
        #endregion

        #region Заполнение столбцов
        void FillOilFieldGridColumn(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool result = false;
            if ((WellColumn != -1) && (OilFieldColumn != -1) && (ProblemTable != null) && (RowsCount > 0))
            {
                string wellName, ofName;
                int ofCode;
                OilField of, ofActive = null;
                int countInd = 0, ind = -1, k = 0, indArea;
                if (SourceProject.indActiveOilField != -1)
                {
                    ofActive = SourceProject.OilFields[SourceProject.indActiveOilField];
                }
                for (int i = 0; i < RowsCount; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    k = -1;
                    countInd = 0;
                    ind = -1;
                    indArea = -1;
                    result = false;
                    ofName = "";
                    if (ProblemTable[i][OilFieldColumn].ObjIndex == -1)
                    {
                        wellName = ProblemTable[i][WellColumn].Value.ToUpper();
                        // есть ли на текущем
                        if (ofActive != null) ind = ofActive.GetWellIndex(wellName);
                        if (ind != -1)
                        {
                            ofName = ofActive.Name;
                            result = true;
                        }
                        else if (ProblemTable[i][OilFieldColumn].Value != "") // ищем по псевдониму
                        {
                            for (int j = 1; j < SourceProject.OilFields.Count; j++)
                            {
                                of = SourceProject.OilFields[j];
                                if ((of.AliasLoaded) && (of.TestAlias(ProblemTable[i][OilFieldColumn].Value)))
                                {
                                    k = j;
                                    ofName = of.Name;
                                    result = true;
                                    break;
                                }
                            }
                        }

                        if ((!result) && (ProblemTable[i][OilFieldColumn].Value == "")) // на текущем  и по псевдониму не найдено
                        {
                            if ((OilFieldAreaColumn != -1) && (ProblemTable[i][OilFieldAreaColumn].ObjIndex != -1)) // ищем используя площадь
                            {
                                indArea = ProblemTable[i][OilFieldAreaColumn].ObjIndex;
                                ofCode = areaDict[indArea].OilFieldCode;
                                for (int j = 1; j < SourceProject.OilFields.Count; j++)
                                {
                                    of = SourceProject.OilFields[j];
                                    if (of.ParamsDict.OraFieldCode == ofCode)
                                    {
                                        if (of.GetWellIndex(wellName, areaDict[indArea].Code) != -1)
                                        {
                                            k = j;
                                            countInd++;
                                        }
                                    }
                                    if (countInd > 0) break;
                                }
                            }
                            else
                            {
                                for (int j = 1; j < SourceProject.OilFields.Count; j++)
                                {
                                    of = SourceProject.OilFields[j];
                                    if (of.GetWellIndex(wellName) != -1)
                                    {
                                        k = j;
                                        countInd++;
                                    }
                                    if (countInd > 1) break;
                                }
                            }
                            if (countInd == 1)
                            {
                                ofName = (SourceProject.OilFields[k]).Name;
                                result = true;
                            }
                        }
                        if (ofName != "")
                        {
                            ProblemTable[i][OilFieldColumn].Value = ofName;
                            ProblemTable[i][OilFieldColumn].ObjIndex = k;
                        }
                        ProblemTable[i][OilFieldColumn].CorrectValue = result;
                        ProblemTable[i][OilFieldColumn].ColorIndex = ((result) ? ProblemCellColor.WHITE : ProblemCellColor.PINK);
                    }
                    else if ((ProblemTable[i][WellColumn].ObjIndex == -1) && 
                             (ProblemTable[i][WellColumn].Value != ""))
                    {
                        // скважина не найдена на тек месторождении
                        for (int j = 1; j < SourceProject.OilFields.Count; j++)
                        {
                            of = SourceProject.OilFields[j];
                            if ((of.AliasLoaded) && (of.TestAlias(ProblemTable[i][OilFieldColumn].Value)))
                            {
                                ind = of.GetWellIndex(ProblemTable[i][WellColumn].Value.ToUpper());
                                if (ind != -1)
                                {
                                    ProblemTable[i][OilFieldColumn].Value = of.Name;
                                    ProblemTable[i][OilFieldColumn].ObjIndex = j;
                                    ProblemTable[i][OilFieldColumn].CorrectValue = true;
                                    ProblemTable[i][OilFieldColumn].ColorIndex = ProblemCellColor.WHITE;
                                    break;
                                }
                            }
                        }
 
                    }
                    worker.ReportProgress((int)(i * 100f / RowsCount));
                }
                SetColumnCellsStyle(worker, e, WellColumn);
                UpdateColumnCellsStyle(OilFieldColumn);
                FillOilFieldAreasGridColumn(worker, e);
                TestEqualsMainWells();
            }
        }
        void FillOilFieldAreasGridColumn(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool result = false;
            if ((WellColumn != -1) && (OilFieldColumn != -1) && (OilFieldAreaColumn != -1) && (ProblemTable != null) && (RowsCount > 0))
            {
                string wellName, ofName, areaName;
                OilField of;
                int ind;
                for (int i = 0; i < RowsCount; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    areaName = "";
                    result = false;
                    if ((ProblemTable[i][OilFieldAreaColumn].ObjIndex == -1) && (ProblemTable[i][OilFieldAreaColumn].Value == ""))
                    {
                        wellName = ProblemTable[i][WellColumn].Value.ToUpper();
                        ofName = ProblemTable[i][OilFieldColumn].Value.ToUpper();

                        if (ProblemTable[i][OilFieldColumn].ObjIndex != -1)
                        {
                            of = SourceProject.OilFields[ProblemTable[i][OilFieldColumn].ObjIndex];
                            ind = of.TestEqualsWell(wellName);
                            if (ind > -1)
                            {
                                ind = areaDict.GetIndexByCode((of.Wells[ind]).OilFieldAreaCode);
                                if (ind != -1)
                                {
                                    areaName = areaDict[ind].Name;
                                    ProblemTable[i][OilFieldAreaColumn].Value = areaName;
                                    ProblemTable[i][OilFieldAreaColumn].ObjIndex = ind;
                                    result = true;
                                }
                            }
                        }
                    }
                    ProblemTable[i][OilFieldAreaColumn].CorrectValue = result;
                    ProblemTable[i][OilFieldAreaColumn].ColorIndex = ((result) ? ProblemCellColor.WHITE : ProblemCellColor.PINK);
                }
                SetColumnCellsStyle(worker, e, OilFieldAreaColumn);
            }
        }
        void TestNeedOilFieldAreasColumn(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if ((OilFieldAreaColumn == -1) && (WellColumn != -1) && (OilFieldColumn != -1) && (ProblemTable != null))
            {
                string wellName, ofName;
                int ind;
                bool NeedAreaColumn = false;
                for (int i = 0; i < RowsCount; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    if (!ProblemTable[i][WellColumn].CorrectValue)
                    {
                        wellName = ProblemTable[i][WellColumn].Value.ToUpper();
                        ofName = ProblemTable[i][OilFieldColumn].Value.ToUpper();
                        ind = ProblemTable[i][OilFieldColumn].ObjIndex;
                        if (ind != -1)
                        {
                            ind = (SourceProject.OilFields[ind]).TestEqualsWell(wellName);
                            if (ind == -2) // найдена на мест более 2 раз
                            {
                                NeedAreaColumn = true;
                                break;
                            }
                        }
                    }
                }
                if (NeedAreaColumn)
                {
                    ColumnDataType[] newDataTypes = new ColumnDataType[DataTypes.Length + 1];
                    for (int i = 0; i < DataTypes.Length; i++)
                    {
                        newDataTypes[i] = DataTypes[i];
                    }
                    OilFieldAreaColumn = DataTypes.Length;
                    newDataTypes[DataTypes.Length] = ColumnDataType.OILFIELD_AREA_NAME;
                    DataTypes = newDataTypes;
                    ProblemColumn[] newColumns = new ProblemColumn[newDataTypes.Length];
                    for (int i = 0; i < ProblemColumns.Length; i++)
                    {
                        newColumns[i] = ProblemColumns[i];
                    }
                    newColumns[newColumns.Length - 1].DataType = ColumnDataType.OILFIELD_AREA_NAME;
                    newColumns[newColumns.Length - 1].ColorIndex = ProblemCellColor.PINK;
                    newColumns[newColumns.Length - 1].CorrectColumn = false;
                    ProblemColumns = newColumns;
                    OilFieldAreaColumn = ProblemColumns.Length - 1;
                    
                    ProblemCell[][] newTable = new ProblemCell[RowsCount][];

                    for (int i = 0; i < RowsCount; i++)
                    {
                        newTable[i] = new ProblemCell[ProblemColumns.Length];
                        for (int j = 0; j < ProblemColumns.Length; j++)
                        {
                            if (j < ProblemTable[i].Length)
                            {
                                newTable[i][j] = ProblemTable[i][j];
                            }
                            else
                            {
                                newTable[i][j] = new ProblemCell();
                            }
                        }
                    }
                    ProblemTable = newTable;
                }
                FillOilFieldAreasGridColumn(worker, e);
            }
        }
        #endregion

        #region Проверка значений ячеек
        void UpdateColumnCellsStyle(int ColumnIndex)
        {
            bool result = true;
            if ((ProblemColumns.Length > 0) && (RowsCount > 0) && (ColumnIndex < ProblemColumns.Length))
            {
                for (int i = 0; i < RowsCount; i++)
                {
                    if (!ProblemTable[i][ColumnIndex].CorrectValue)
                    {
                        result = false;
                        break;
                    }
                }
                ProblemColumns[ColumnIndex].ColorIndex = ((result) ? ProblemCellColor.GREEN : ProblemCellColor.PINK);
                ProblemColumns[ColumnIndex].CorrectColumn = result;
            }
        }
        void SetColumnCellsStyle(BackgroundWorker worker, DoWorkEventArgs e, int ColumnIndex)
        {
            bool result = true;
            if ((ProblemColumns.Length > 0) && (RowsCount > 0) && (ColumnIndex < ProblemColumns.Length))
            {
                float start = ColumnIndex * RowsCount;
                float max = (ProblemColumns.Length) * RowsCount;
                int progress = 0, current;
                for (int i = 0; i < RowsCount; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    result = SetCellStyle(i, ColumnIndex, false) && result;
                    current = (int)((start + i) * 100 / max);
                    if (progress != current) worker.ReportProgress(current);
                }
            }
            ProblemColumns[ColumnIndex].ColorIndex = ((result) ? ProblemCellColor.GREEN : ProblemCellColor.PINK);
            ProblemColumns[ColumnIndex].CorrectColumn = result;
        }
        bool SetCellStyle(int RowIndex, int ColumnIndex, bool FillOilFieldArea)
        {
            bool result = true;
            if ((RowIndex < RowsCount) && (ColumnIndex < ProblemColumns.Length))
            {
                char[] digits = new char[10] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                int ind = -1;
                ProblemTable[RowIndex][ColumnIndex].Error = "";
                ProblemCell cell = ProblemTable[RowIndex][ColumnIndex];
                switch (DataTypes[ColumnIndex])
                {
                    case ColumnDataType.NONE:
                        cell.ColorIndex = ProblemCellColor.WHITE;
                        result = false;
                        break;
                    case ColumnDataType.OILFIELD_NAME:
                        ind = SourceProject.GetOFIndex(cell.Value.ToUpper());
                        if (ind == -1)
                        {
                            cell.Error = "Неправильное имя месторождения";
                            result = false;
                        }
                        else
                        {
                            cell.ObjIndex = ind;
                        }
                        break;
                    case ColumnDataType.OILFIELD_AREA_NAME:
                        ind = areaDict.GetIndexByShortName(cell.Value);
                        if (ind == -1)
                        {
                            result = false;
                            bool ofErr = (OilFieldColumn != -1) && ProblemTable[RowIndex][OilFieldColumn].CorrectValue && (WellColumn != -1) && !ProblemTable[RowIndex][WellColumn].CorrectValue;
                            if (!ofErr)
                            {
                                if ((OilFieldColumn == -1) || (!ProblemTable[RowIndex][OilFieldColumn].CorrectValue))
                                {
                                    cell.Error = "Задайте месторождение";
                                    cell.ColorIndex = ProblemCellColor.GRAY;
                                }
                                else if (cell.Value == "")
                                {
                                    cell.Error = "Задайте площадь";
                                    cell.ColorIndex = ProblemCellColor.PINK;
                                }
                                else
                                {
                                    cell.Error = "Неправильное имя площади";
                                    cell.ColorIndex = ProblemCellColor.PINK;
                                }
                                if (WellColumn != -1)
                                {
                                    ProblemTable[RowIndex][WellColumn].Error = cell.Error;
                                }
                            }
                        }
                        if (result)
                        {
                            cell.ColorIndex = ProblemCellColor.WHITE;
                            cell.ObjIndex = ind;
                        }
                        else if ((WellColumn != -1) && (OilFieldColumn != -1))
                        {
                            if ((!ProblemTable[RowIndex][WellColumn].CorrectValue) &&
                                (ProblemTable[RowIndex][WellColumn].ColorIndex == ProblemCellColor.GRAY) &&
                                (ProblemTable[RowIndex][OilFieldColumn].ColorIndex == ProblemCellColor.PINK))
                            {
                                cell.ColorIndex = ProblemCellColor.GRAY;
                            }
                        }
                        break;
                    case ColumnDataType.WELL_NAME:
                        result = false;
                        if (OilFieldColumn != -1)
                        {
                            if (ProblemTable[RowIndex][OilFieldColumn].CorrectValue)
                            {
                                ind = ProblemTable[RowIndex][OilFieldColumn].ObjIndex;
                                if (ind != -1)
                                {
                                    OilField of = SourceProject.OilFields[ind];
                                    List<int> wellList = of.GetWellIndexList(cell.Value);
                                    if (wellList.Count > 0)
                                    {
                                        if (wellList.Count == 1)
                                        {
                                            result = true;
                                            cell.ObjIndex = (int)wellList[0];
                                        }
                                        else if (wellList.Count > 1)
                                        {
                                            if (OilFieldAreaColumn != -1)
                                            {
                                                if (ProblemTable[RowIndex][OilFieldAreaColumn].CorrectValue)
                                                {
                                                    if (ProblemTable[RowIndex][OilFieldAreaColumn].ObjIndex != -1)
                                                    {
                                                        ind = ProblemTable[RowIndex][OilFieldAreaColumn].ObjIndex;
                                                        if (ind != -1)
                                                        {
                                                            for (int j = 0; j < wellList.Count; j++)
                                                            {
                                                                if ((of.Wells[(int)wellList[j]]).OilFieldAreaCode == areaDict[ind].Code)
                                                                {
                                                                    result = true;
                                                                    cell.ObjIndex = (int)wellList[j];
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        if (!result)
                                                        {
                                                            cell.Error = "Скважина не найдена на заданной площади";
                                                            cell.ColorIndex = ProblemCellColor.PINK;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cell.Error = "Задайте площадь месторождения";
                                                        cell.ColorIndex = ProblemCellColor.GRAY;
                                                    }
                                                }
                                                else if (ProblemTable[RowIndex][OilFieldAreaColumn].Value == "")
                                                {
                                                    cell.Error = "Задайте площадь";
                                                    cell.ColorIndex = ProblemCellColor.GRAY;
                                                    ProblemTable[RowIndex][OilFieldAreaColumn].Error = "Задайте площадь";
                                                    ProblemTable[RowIndex][OilFieldAreaColumn].ColorIndex = ProblemCellColor.PINK;
                                                }
                                                else
                                                {
                                                    cell.Error = "Неправильное имя площади";
                                                    cell.ColorIndex = ProblemCellColor.GRAY;
                                                    ProblemTable[RowIndex][OilFieldAreaColumn].Error = "Неправильное имя площади";
                                                    ProblemTable[RowIndex][OilFieldAreaColumn].ColorIndex = ProblemCellColor.PINK;
                                                }
                                            }
                                            else
                                            {
                                                cell.Error = "Задайте столбец 'Площадь'";
                                                cell.ColorIndex = ProblemCellColor.GRAY;
                                            }
                                        }
                                    }
                                    else if (cell.Value == "")
                                    {
                                        cell.Error = "Не задан номер скважины";
                                        cell.ColorIndex = ProblemCellColor.PINK;
                                    }
                                    else
                                    {
                                        cell.Error = "Скважина не найдена на данном месторождении";
                                        cell.ColorIndex = ProblemCellColor.GRAY;
                                        ProblemTable[RowIndex][OilFieldColumn].ColorIndex = ProblemCellColor.PINK;
                                        if (OilFieldAreaColumn != -1)
                                        {
                                            ProblemTable[RowIndex][OilFieldAreaColumn].ColorIndex = ProblemCellColor.GRAY;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cell.Error = "Неправильное имя месторождения";
                                cell.ColorIndex = ProblemCellColor.GRAY;
                            }
                        }
                        else
                        {
                            cell.Error = "Не задан столбец 'Месторождение'";
                            cell.ColorIndex = ProblemCellColor.GRAY;
                        }
                        if (result)
                        {
                            cell.ColorIndex = ProblemCellColor.WHITE;
                            if (OilFieldColumn != -1)
                            {
                                SetCellStyle(RowIndex, OilFieldColumn, FillOilFieldArea);
                            }
                        }

                        break;
                    case ColumnDataType.DATATIME:
                        try
                        {
                            if (cell.Value != "")
                            {
                                DateTime dt = Convert.ToDateTime(cell.Value);
                            }
                        }
                        catch
                        {
                            result = false;
                            cell.Error = "Неправильное значение даты";
                        }
                        break;
                    case ColumnDataType.DOUBLE:
                        try
                        {
                            if (cell.Value != "")
                            {
                                double val = Convert.ToDouble(cell.Value);
                            }
                        }
                        catch
                        {
                            result = false;
                            cell.Error = "Неправильное числовое значение";
                        }
                        break;
                }
                if ((ColumnIndex != WellColumn) && (ColumnIndex != OilFieldAreaColumn))
                {
                    cell.ColorIndex = ((result) ? ProblemCellColor.WHITE : ProblemCellColor.PINK);
                }
                cell.CorrectValue = result;
                UpdateColumnCellsStyle(ColumnIndex);
                if (FillOilFieldArea)
                {
                    if ((WellColumn != -1) &&
                       ((DataTypes[ColumnIndex] == ColumnDataType.OILFIELD_NAME) ||
                        (DataTypes[ColumnIndex] == ColumnDataType.OILFIELD_AREA_NAME)))
                    {
                        SetCellStyle(RowIndex, WellColumn, false);
                        if ((OilFieldAreaColumn != -1) &&
                            (DataTypes[ColumnIndex] == ColumnDataType.OILFIELD_NAME) &&
                            ProblemTable[RowIndex][OilFieldColumn].CorrectValue)
                        {
                            ind = ProblemTable[RowIndex][OilFieldColumn].ObjIndex;
                            if (ind != -1)
                            {
                                OilField of = SourceProject.OilFields[ind];
                                List<int> list = of.GetWellIndexList((string)Rows[RowIndex].Cells[WellColumn].Value);
                                if (list.Count == 1)
                                {
                                    Rows[RowIndex].Cells[OilFieldAreaColumn].Value = areaDict.GetShortNameByCode((of.Wells[(int)list[0]]).OilFieldAreaCode);
                                    SetCellStyle(RowIndex, OilFieldAreaColumn, false);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
        #endregion

        #region Перенос скважин в главный список
        public void TestEqualsMainWells()
        {
            int i, j;
            int countCorrect;
            if (WellColumn != -1)
            {
                for (i = 0; i < RowsCount; i++)
                {
                    if ((!ProblemTable[i][WellColumn].CorrectValue) && 
                        (ProblemTable[i][WellColumn].ColorIndex == ProblemCellColor.YELLOW))
                    {
                        ProblemTable[i][WellColumn].CorrectValue = true;
                        ProblemTable[i][WellColumn].ColorIndex = ProblemCellColor.WHITE;
                        ProblemTable[i][WellColumn].Error = "";
                    }
                    countCorrect = 0;
                    for (j = 0; j < ProblemTable[i].Length; j++)
                    {
                        if (ProblemTable[i][j].CorrectValue) countCorrect++;
                    }
                    if ((countCorrect == ProblemTable[i].Length) && (OilFieldColumn != -1))
                    {
                        if (wellListSettings.TestEqualWell(ProblemTable[i][OilFieldColumn].ObjIndex, ProblemTable[i][WellColumn].ObjIndex))
                        {
                            ProblemTable[i][WellColumn].CorrectValue = false;
                            ProblemTable[i][WellColumn].ColorIndex = ProblemCellColor.YELLOW;
                            ProblemTable[i][WellColumn].Error = "Скважина уже есть в основном списке!";
                        }
                    }
                }
            }
            this.Invalidate();
        }
        bool TestEqualAddedWells(int OilFieldIndex, int WellIndex)
        {
            PhantomWell pw;
            for (int i = 0; i < addedWells.Count; i++)
            {
                pw = (PhantomWell)addedWells[i];
                if ((pw.srcWell.OilFieldIndex == OilFieldIndex) && (pw.srcWell.Index == WellIndex))
                {
                    return true;
                }
            }
            return false;
        }
        void AddWellToMainTable(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (WellColumn != -1)
            {
                int i, j;
                string wellName;
                OilField of;
                Well w;
                int ind;
                PhantomWell pw;
                addedWells.Clear();

                List<int> equalsIndexes = new List<int>();
                int countCorrect;
                for (i = 0; i < RowsCount; i++)
                {
                    if ((worker != null) && (worker.CancellationPending))
                    {
                        e.Cancel = true;
                        return;
                    }
                    countCorrect = 0;
                    for (j = 0; j < ProblemTable[i].Length; j++)
                    {
                        if (ProblemTable[i][j].CorrectValue) countCorrect++;
                    }
                    if (countCorrect == ProblemTable[i].Length)
                    {
                        wellName = ProblemTable[i][WellColumn].Value.ToUpper();
                        if (OilFieldColumn != -1)
                        {
                            ind = ProblemTable[i][OilFieldColumn].ObjIndex;
                            if (ind != -1)
                            {
                                of = SourceProject.OilFields[ind];
                                if (WellColumn != -1)
                                {
                                    ind = ProblemTable[i][WellColumn].ObjIndex;
                                    if (ind != -1)
                                    {
                                        w = of.Wells[ind];
                                        pw = new PhantomWell(w.OilFieldCode, w);
                                        for (j = 0; j < DataTypes.Length; j++)
                                        {
                                            if (ProblemTable[i][j].CorrectValue)
                                            {
                                                if (DataTypes[j] == ColumnDataType.TEXT)
                                                {
                                                    if (ProblemTable[i][j].Value != "")
                                                    {
                                                        if (pw.Caption == null) pw.Caption = new PhantomWellCaption();
                                                        pw.Caption.Text = ProblemTable[i][j].Value.Replace("\n", "");
                                                    }
                                                }
                                                else if (DataTypes[j] == ColumnDataType.DATATIME)
                                                {
                                                    if (ProblemTable[i][j].Value != "")
                                                    {
                                                        if (pw.Caption == null) pw.Caption = new PhantomWellCaption();
                                                        pw.Caption.Date = Convert.ToDateTime(ProblemTable[i][j].Value);
                                                    }
                                                }
                                                else if (DataTypes[j] == ColumnDataType.DOUBLE)
                                                {
                                                    if (ProblemTable[i][j].Value != "")
                                                    {
                                                        if (pw.Caption == null) pw.Caption = new PhantomWellCaption();
                                                        pw.Caption.Value = Convert.ToDouble(ProblemTable[i][j].Value);
                                                    }
                                                }
                                            }
                                        }
                                        
                                        if ((!wellListSettings.TestEqualWell(ProblemTable[i][OilFieldColumn].ObjIndex, ind)) &&
                                            (!TestEqualAddedWells(ProblemTable[i][OilFieldColumn].ObjIndex, ind)))
                                        {
                                            addedWells.Add(pw);
                                            ProblemTable[i] = null;
                                        }
                                        else if (equalsIndexes.IndexOf(i) == -1)
                                        {
                                            equalsIndexes.Add(i);
                                            ProblemTable[i][WellColumn].CorrectValue = false;
                                            ProblemTable[i][WellColumn].ColorIndex = ProblemCellColor.YELLOW;
                                            ProblemTable[i][WellColumn].Error = "Скважина уже есть в основном списке!";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (addedWells.Count > 0)
                {
                    if (worker == null)
                    {
                        wellListSettings.AddPhantomWellList(addedWells);
                        addedWells.Clear();
                    }
                    DefragNullValues();
                    if (worker == null)
                    {
                        UpdateRowsCount();
                        this.Invalidate();
                        if (RowsCount == 0)
                        {
                            this.Clear();
                            wellListSettings.HideProblemList();
                        }
                    }
                }
            }
        }
        #endregion

        #region Работа с ProblemTable
        void AddNewRow()
        {
            if (RowsCount == ProblemTable.Length)
            {
                ProblemCell[][] newTable = new ProblemCell[RowsCount + 20][];
                for (int i = 0; i < RowsCount; i++)
                {
                    newTable[i] = ProblemTable[i];
                }
                ProblemTable = newTable;
            }
            if (RowsCount < ProblemTable.Length)
            {
                ProblemTable[RowsCount] = new ProblemCell[ProblemColumns.Length];
                for (int i = 0; i < ProblemTable[RowsCount].Length; i++)
                {
                    ProblemTable[RowsCount][i] = new ProblemCell();
                }
                RowsCount++;
            }
        }
        void DefragNullValues()
        {
            int i, j;
            for (i = 0; i < RowsCount - 1; i++)
            {
                if (ProblemTable[i] == null)
                {
                    j = i + 1;
                    while ((j < RowsCount) && (ProblemTable[j] == null))
                    {
                        j++;
                    }
                    if (j < RowsCount)
                    {
                        ProblemTable[i] = ProblemTable[j];
                        ProblemTable[j] = null;
                    }
                    else if (j >= RowsCount)
                    {
                        break;
                    }
                }
            }
            int newRow = 0;
            for (i = 0; i < RowsCount; i++)
            {
                if (ProblemTable[i] != null) newRow++;
            }
            RowsCount = newRow;
        }
        void UpdateRowsCount()
        {
            this.RowCount = RowsCount;
            lNum.Text = "N строк: " + this.RowsCount.ToString();
            lNum.Location = new Point(Parent.Width - 17 - lNum.Width, 4);
            UpdateControlsSize();
        }
        void RemoveColumn(int ColumnIndex)
        {
            if ((ColumnIndex > -1) && (ProblemTable != null) && (ProblemTable.Length > 0) && (ColumnIndex < ProblemTable[0].Length))
            {
                #region WELL OF OFAREA COLUMNS
                if (WellColumn != -1)
                {
                    if (WellColumn == ColumnIndex)
                    {
                        WellColumn = -1;
                    }
                    else if (WellColumn > ColumnIndex)
                    {
                        WellColumn--;
                    }
                }
                if (OilFieldColumn != -1)
                {
                    if (OilFieldColumn == ColumnIndex)
                    {
                        OilFieldColumn = -1;
                    }
                    else if (OilFieldColumn > ColumnIndex)
                    {
                        OilFieldColumn--;
                    }
                }
                if (OilFieldAreaColumn != -1)
                {
                    if (OilFieldAreaColumn == ColumnIndex)
                    {
                        OilFieldAreaColumn = -1;
                    }
                    else if (OilFieldAreaColumn > ColumnIndex)
                    {
                        OilFieldAreaColumn--;
                    }
                }
                #endregion

                int i, j, k;
                ProblemCell[] newRow;
                for (i = 0; i < ProblemTable.Length; i++)
                {
                    if (ProblemTable[i] != null)
                    {
                        newRow = new ProblemCell[ProblemTable[i].Length - 1];
                        for (j = 0, k = 0; (j < ProblemTable[i].Length) && (k < newRow.Length); j++)
                        {
                            if (j != ColumnIndex)
                            {
                                newRow[k] = ProblemTable[i][j];
                                k++;
                            }
                        }
                        ProblemTable[i] = newRow;
                    }
                }
                ProblemColumn[] newProblemCols = new ProblemColumn[ProblemColumns.Length - 1];
                for (j = 0, k = 0; (j < ProblemColumns.Length) && (k < newProblemCols.Length); j++)
                {
                    if (j != ColumnIndex)
                    {
                        newProblemCols[k] = ProblemColumns[j];
                        k++;
                    }
                }
                ProblemColumns = newProblemCols;
                ColumnDataType[] newDataTypes = new ColumnDataType[DataTypes.Length - 1];
                for (j = 0, k = 0; (j < DataTypes.Length) && (k < newDataTypes.Length); j++)
                {
                    if (j != ColumnIndex)
                    {
                        newDataTypes[k] = DataTypes[j];
                        k++;
                    }
                }
                DataTypes = newDataTypes;
                this.Columns.RemoveAt(ColumnIndex);
                if (this.Columns.Count == 1)
                {
                    this.Clear();
                    wellListSettings.HideProblemList();
                }
                if (this.SelectedColumns.Count > 0) this.SelectedColumns[0].Selected = false;
                this.Invalidate();
            }
        }
        #endregion

        public void Clear()
        {
            this.addedWells.Clear();
            this.RowsCount = 0;
            this.Columns.Clear();
            ProblemTable = null;
            ProblemColumns = null;
            DataTypes = null;
            WellColumn = -1;
            OilFieldColumn = -1;
            OilFieldAreaColumn = -1;
        }
        public string GetDataTypesString()
        {
            string res = "";
            for (int i = 0; i < DataTypes.Length; i++)
            {
                res += ((byte)DataTypes[i]).ToString() + ";";
            }
            return res;
        }

        #region WORKER
        void WorkerRun()
        {
            if (!worker.IsBusy)
            {
                worker.RunWorkerAsync();
                DrawWaitMode = true;
            }
        }
        void WorkerRun(object Arg)
        {
            if (!worker.IsBusy)
            {
                worker.RunWorkerAsync(Arg);
                DrawWaitMode = true;
            }
        }
        public void SetProgressValue(int Value)
        {
            if (Value < 0) Value = 0;
            if (Value > 100) Value = 100;
            progress.Value = Value;
        }
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (WorkType)
            {
                case WORKER_WORKTYPE.FILL_TABLE:
                    WorkerArgument arg = (WorkerArgument)e.Argument;
                    FillTable(worker, e, arg.ClipboardRows, arg.ColDataTypes);
                    break;
                case WORKER_WORKTYPE.FILL_OILFIELD_COLUMN:
                    FillOilFieldGridColumn(worker, e);
                    break;
                case WORKER_WORKTYPE.UPDATE_COLUMN_STYLE:
                    SetColumnCellsStyle(worker, e, (int)e.Argument);
                    break;
                case WORKER_WORKTYPE.TEST_NEED_AREA_COLUMN:
                    TestNeedOilFieldAreasColumn(worker, e);
                    break;
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            wellListSettings.SetProgressValue(e.ProgressPercentage);
            if (DrawWaitMode) SetProgressValue(e.ProgressPercentage);
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DrawWaitMode = false;
            if (!e.Cancelled)
            {
                switch (WorkType)
                {
                    case WORKER_WORKTYPE.FILL_TABLE:
                        if (addedWells.Count > 0)
                        {
                            wellListSettings.AddPhantomWellList(addedWells);
                            addedWells.Clear();
                        }
                        if (RowsCount == 0)
                        {
                            this.Clear();
                            wellListSettings.HideProblemList();
                        }
                        else if (ProblemTable != null)
                        {
                            DataGridViewColumn column;
                            DataGridViewCell cellTemplate = new DataGridViewTextBoxCell();
                            for (int i = 0; i < ProblemColumns.Length; i++)
                            {
                                column = new DataGridViewColumn(cellTemplate);
                                column.HeaderText = ProblemColumns[i].Name;
                                this.Columns.Add(column);
                            }
                            column = new DataGridViewColumn(cellTemplate);
                            column.Resizable = DataGridViewTriState.False;
                            column.HeaderText = "Комментарий";
                            column.HeaderCell.Style.BackColor = Color.Gray;
                            this.Columns.Add(column);
                            UpdateRowsCount();
                            CreateContextMenu();
                            wellListSettings.splContainer.Panel2Collapsed = false;
                            SetCommentColumnSize();
                        }
                        wellListSettings.DrawWaitMode = false;
                        break;
                    case WORKER_WORKTYPE.FILL_OILFIELD_COLUMN:
                        break;
                    case WORKER_WORKTYPE.UPDATE_COLUMN_STYLE:
                        break;
                    case WORKER_WORKTYPE.TEST_NEED_AREA_COLUMN:
                        if ((ProblemTable != null) && (ProblemColumns != null) && (this.Columns.Count > 0) && (ProblemColumns.Length + 1 > this.Columns.Count))
                        {
                            DataGridViewCell cellTemplate = new DataGridViewTextBoxCell();
                            DataGridViewColumn column = new DataGridViewColumn(cellTemplate);
                            column.HeaderText = ProblemColumns[ProblemColumns.Length - 1].Name;
                            this.Columns.Insert(this.Columns.Count - 1, column);
                        }
                        break;
                }
            }
            else
            {
                this.Clear();
            }
            this.Enabled = true;
            this.Invalidate();
        }
        #endregion

        #region BUP EVENTS
        void bUp_Click(object sender, EventArgs e)
        {
            AddWellToMainTable(null, null);
        }
        #endregion

        #region COMBOBOX FUNCTION
        bool ComboBoxShow(int RowIndex, int ColumnIndex)
        {
            bool result = false;
            ComboBoxValueCollection.Clear();
            ComboBoxValues.Text = "";
            ComboBoxValues.Items.Clear();
            ComboBoxValues.Tag = null;
            if (DrawWaitMode) return false;
            if ((WellColumn != -1) && (SavedSelectedCells != null))
            {
                int ind;
                string wellName;
                ComboBoxValue cmbValue;
                if (OilFieldColumn == ColumnIndex)
                {
                    OilField of;
                    ArrayList list = new ArrayList();
                    for (int k = 0; k < this.SavedSelectedCells.Length; k++)
                    {
                        if (!ProblemTable[this.SavedSelectedCells[k].RowIndex][WellColumn].CorrectValue)
                        {
                            wellName = ProblemTable[this.SavedSelectedCells[k].RowIndex][WellColumn].Value.ToUpper();
                            for (int i = 0; i < SourceProject.OilFields.Count; i++)
                            {
                                of = SourceProject.OilFields[i];
                                ind = -1;
                                for (int j = 0; j < ComboBoxValueCollection.Count; j++)
                                {
                                    if (ComboBoxValueCollection[j].ObjIndex == of.Index)
                                    {
                                        ind = j;
                                        break;
                                    }
                                }
                                if (ind == -1)
                                {
                                    ind = of.GetWellIndex(wellName);
                                    if (ind != -1)
                                    {
                                        cmbValue.ObjIndex = of.Index;
                                        cmbValue.ObjName = of.Name;
                                        if ((ComboBoxValueCollection.Count == 0) || (ComboBoxValueCollection[ComboBoxValueCollection.Count - 1].ObjIndex < of.Index))
                                        {
                                            ComboBoxValueCollection.Add(cmbValue);
                                        }
                                        else if (ComboBoxValueCollection[0].ObjIndex > of.Index)
                                        {
                                            ComboBoxValueCollection.Insert(0, cmbValue);
                                        }
                                        else
                                        {
                                            for (int s = 0; s < ComboBoxValueCollection.Count - 1; s++)
                                            {
                                                if ((ComboBoxValueCollection[s].ObjIndex < of.Index) && (of.Index < ComboBoxValueCollection[s + 1].ObjIndex))
                                                {
                                                    ComboBoxValueCollection.Insert(s + 1, cmbValue);
                                                    break;
                                                }
                                            }
                                        }
                                        result = true;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (((OilFieldColumn != -1) && (OilFieldAreaColumn == ColumnIndex)) && (this.SelectedCells.Count == 1))
                {
                    if (ProblemTable[RowIndex][OilFieldColumn].CorrectValue)
                    {
                        wellName = ProblemTable[RowIndex][WellColumn].Value.ToUpper();
                        List<int> list;
                        OilField of;
                        ind = ProblemTable[RowIndex][OilFieldColumn].ObjIndex;
                        if (ind != -1)
                        {
                            of = SourceProject.OilFields[ind];
                            list = of.GetWellIndexList(wellName);
                            if (list.Count > 0)
                            {
                                for (int i = 0; i < list.Count; i++)
                                {
                                    cmbValue.ObjIndex = (of.Wells[(int)list[i]]).OilFieldAreaCode;
                                    ind = areaDict.GetIndexByCode((of.Wells[(int)list[i]]).OilFieldAreaCode);
                                    if (ind > 0)
                                    {
                                        cmbValue.ObjName = areaDict[ind].Name;
                                        ComboBoxValueCollection.Add(cmbValue);
                                    }
                                }
                                result = true;
                            }
                        }
                    }
                }
            }
            if (result)
            {
                for (int i = 0; i < ComboBoxValueCollection.Count; i++)
                {
                    ComboBoxValues.Items.Add(ComboBoxValueCollection[i].ObjName);
                }
                Rectangle rect = this.GetCellDisplayRectangle(ColumnIndex, RowIndex, true);
                int Y = rect.Y - 3;
                if (Y < 0) Y = 0;
                ComboBoxValues.Location = new Point(rect.Location.X + this.Location.X, Y);
                ComboBoxValues.Size = rect.Size;
                ComboBoxValues.Tag = new Point(ColumnIndex, RowIndex);
                ComboBoxValues.Visible = true;
                ComboBoxValues.DroppedDown = true;
            }
            return result;
        }
        void ComboBoxValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((ComboBoxValues.Tag != null) && (ComboBoxValues.SelectedIndex > -1) &&
                (SavedSelectedCells != null) && (WellColumn != -1))
            {
                Point pt = (Point)ComboBoxValues.Tag;
                ComboBoxValues.Visible = false;

                if (pt.X == OilFieldColumn)
                {
                    int ind = 0;
                    string wellName;
                    OilField of = SourceProject.OilFields[ComboBoxValueCollection[ComboBoxValues.SelectedIndex].ObjIndex];
                    if ((SavedSelectedCells.Length == 1) && (ProblemTable[this.SavedSelectedCells[0].RowIndex][OilFieldColumn].Value != ""))
                    {
                        ind = SourceProject.GetOFIndex(ProblemTable[this.SavedSelectedCells[0].RowIndex][OilFieldColumn].Value.ToUpper());
                        if (ind == -1)
                        {
                            of.AddAlias(ProblemTable[this.SavedSelectedCells[0].RowIndex][OilFieldColumn].Value);
                            wellListSettings.UpdateOilFieldAliases(of);
                            WorkType = WORKER_WORKTYPE.FILL_OILFIELD_COLUMN;
                            WorkerRun();
                        }
                    }
                    if (ind != -1)
                    {
                        for (int i = 0; i < SavedSelectedCells.Length; i++)
                        {
                            if (!ProblemTable[SavedSelectedCells[i].RowIndex][WellColumn].CorrectValue)
                            {
                                wellName = ProblemTable[SavedSelectedCells[i].RowIndex][WellColumn].Value.ToUpper();
                                ind = of.GetWellIndex(wellName);
                                if (ind != -1)
                                {
                                    Rows[SavedSelectedCells[i].RowIndex].Cells[OilFieldColumn].Value = ComboBoxValueCollection[ComboBoxValues.SelectedIndex].ObjName;
                                    SetCellStyle(SavedSelectedCells[i].RowIndex, OilFieldColumn, true);
                                }
                            }
                        }
                        WorkType = WORKER_WORKTYPE.TEST_NEED_AREA_COLUMN;
                        WorkerRun();
                    }
                }
                else
                {
                    Rows[pt.Y].Cells[pt.X].Value = ComboBoxValueCollection[ComboBoxValues.SelectedIndex].ObjName;
                    SetCellStyle(pt.Y, pt.X, false);
                }
                
            }
        }
        void ComboBoxValues_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                ComboBoxValues.DroppedDown = false;
                ComboBoxValues.Hide();
                this.Focus();
            }
        }
        #endregion

        #region DROPDOWNBOX EVENTS
        int TestDropDownBoxMouseMove(Point pt)
        {
            int x = 0, sum = 0;
            if ((pt.Y > 3) && (pt.Y < 15))
            {
                for (int i = 0; i < Columns.Count - 1; i++)
                {
                    x = sum + this.Columns[i].Width - this.HorizontalScrollBar.Value - 14;
                    if ((x < pt.X) && (pt.X < x + 12))
                    {
                        return i;
                    }
                    if (x > this.ClientRectangle.Right) break;
                    sum += this.Columns[i].Width;
                }
            }
            return -1;
        }
        #endregion

        #region CONTEXT MENU FUNC
        void CreateContextMenu()
        {
            contextMenu.Items.Clear();
            ToolStripMenuItem menuItem;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add(""); menuItem.Tag = ColumnDataType.WELL_NAME;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add(""); menuItem.Tag = ColumnDataType.OILFIELD_NAME;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add(""); menuItem.Tag = ColumnDataType.OILFIELD_AREA_NAME;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add(""); menuItem.Tag = ColumnDataType.TEXT;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add(""); menuItem.Tag = ColumnDataType.DATATIME;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add(""); menuItem.Tag = ColumnDataType.DOUBLE;
            contextMenu.Items.Add("-");
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add(""); menuItem.Tag = ColumnDataType.NONE;
            contextMenu.Items.Add("-");
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add("Удалить");
            menuItem.Click +=new EventHandler(RemoveColumn_Click);

            int ind;
            for (int i = 0; i < contextMenu.Items.Count; i++)
            {
                if (contextMenu.Items[i].Tag != null)
                {
                    menuItem = (ToolStripMenuItem)contextMenu.Items[i];
                    ind = (int)(ColumnDataType)menuItem.Tag;
                    menuItem.Text = DataTypeNames[ind];
                    menuItem.Click += new EventHandler(menuItem_Click);
                }
            }
            this.ContextMenuStrip = contextMenu;
        }
        void ContextItemsEnabled(int ColumnIndex)
        {
            ToolStripMenuItem menuItem;
            if (DataTypes != null)
            {
                for (int i = 0; i < contextMenu.Items.Count; i++)
                {
                    if (contextMenu.Items[i].Tag != null)
                    {
                        menuItem = (ToolStripMenuItem)contextMenu.Items[i];
                        ColumnDataType type = (ColumnDataType)menuItem.Tag;
                        menuItem.Enabled = true;
                        for (int j = 0; j < DataTypes.Length; j++)
                        {
                            if ((j != ColumnIndex) && (DataTypes[j] == type) && (type != ColumnDataType.NONE))
                            {
                                menuItem.Enabled = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
        void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.SelectedColumns.Count > 0)
            {
                if ((DataTypes != null) && (contextMenu.Items.Count > 0))
                {
                    ColumnDataType type = DataTypes[this.SelectedColumns[0].Index];
                    ToolStripMenuItem menuItem;
                    for (int i = 0; i < contextMenu.Items.Count; i++)
                    {
                        if (contextMenu.Items[i].Tag != null)
                        {
                            menuItem = (ToolStripMenuItem)contextMenu.Items[i];
                            if ((menuItem.Tag != null) && (((ColumnDataType)menuItem.Tag) == type))
                            {
                                menuItem.Checked = true;
                            }
                            else
                            {
                                menuItem.Checked = false;
                            }
                        }
                    }
                }
            }
        }
        void menuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
            if ((menuItem.Tag != null) && (SelectedColumns.Count > 0))
            {
                int col = SelectedColumns[0].Index;
                ColumnDataType type = (ColumnDataType)menuItem.Tag;
                if (menuItem.Checked)
                {
                    SelectedColumns[0].HeaderText = DataTypeNames[0];
                    DataTypes[col] = ColumnDataType.NONE;
                    Columns[col].HeaderCell.Style.BackColor = Color.Pink;
                    WorkType = WORKER_WORKTYPE.UPDATE_COLUMN_STYLE;
                    this.Enabled = false;
                    WorkerRun(col);
                    switch (type)
                    {
                        case ColumnDataType.WELL_NAME:
                            WellColumn = -1;
                            break;
                        case ColumnDataType.OILFIELD_NAME:
                            OilFieldColumn = -1;
                            break;
                        case ColumnDataType.OILFIELD_AREA_NAME:
                            OilFieldAreaColumn = -1;
                            break;
                    }
                }
                else
                {
                    SelectedColumns[0].HeaderText = DataTypeNames[(int)type];
                    DataTypes[col] = type;
                    ProblemColumns[col].DataType = DataTypes[col];
                    ProblemColumns[col].ColorIndex = ProblemCellColor.PINK;
                    ProblemColumns[col].CorrectColumn = false;
                    if (col == WellColumn) WellColumn = -1;
                    if (col == OilFieldColumn) OilFieldColumn = -1;
                    if (col == OilFieldAreaColumn) OilFieldAreaColumn = -1;
                    switch (type)
                    {
                        case ColumnDataType.WELL_NAME:
                            WellColumn = col;
                            WorkType = WORKER_WORKTYPE.FILL_OILFIELD_COLUMN;
                            WorkerRun();
                            break;
                        case ColumnDataType.OILFIELD_NAME:
                            OilFieldColumn = col;
                            break;
                        case ColumnDataType.OILFIELD_AREA_NAME:
                            OilFieldAreaColumn = col;
                            break;
                    }
                    if (type != ColumnDataType.WELL_NAME)
                    {
                        WorkType = WORKER_WORKTYPE.UPDATE_COLUMN_STYLE;
                        this.Enabled = false;
                        WorkerRun(col);
                    }
                }
            }
            SelectionMode = DataGridViewSelectionMode.CellSelect;
            if (MouseDownCell != null) MouseDownCell.Selected = true;
        }
        void RemoveColumn_Click(object sender, EventArgs e)
        {
            if (this.SelectedColumns.Count > 0)
            {
                int index = this.SelectedColumns[0].Index;
                this.SelectedColumns[0].Selected = false;
                RemoveColumn(index);
                SetCommentColumnSize();
            }
        }

        #endregion

        #region BTN CLOSE EVENTS
        void pClose_Click(object sender, EventArgs e)
        {
            if (worker.IsBusy)
            {
                worker.CancelAsync();
            }
            else
            {
                this.Clear();
            }
            wellListSettings.HideProblemList();
            ((Panel)sender).BackColor = SystemColors.Control;
        }
        void pClose_MouseDown(object sender, MouseEventArgs e)
        {
            ((Panel)sender).BackColor = SystemColors.ControlDarkDark;
        }
        void pClose_MouseLeave(object sender, EventArgs e)
        {
            ((Panel)sender).BackColor = SystemColors.Control;
        }
        void pClose_MouseEnter(object sender, EventArgs e)
        {
            ((Panel)sender).BackColor = SystemColors.ControlDark;
        }
        #endregion

        #region GRID VIRTUAL MODE
        void grid_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            if ((ProblemTable != null) && (e.RowIndex != -1) && (e.RowIndex < RowsCount) && (ProblemTable[e.RowIndex] != null))
            {
                if (e.ColumnIndex < ProblemTable[e.RowIndex].Length)
                {
                    e.ToolTipText = ProblemTable[e.RowIndex][e.ColumnIndex].Error;
                }
            }
        }
        void grid_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if ((ProblemTable != null) && (e.RowIndex < RowsCount) && (ProblemTable[e.RowIndex] != null))
            {
                if (e.ColumnIndex < ProblemTable[e.RowIndex].Length)
                {
                    e.Value = ProblemTable[e.RowIndex][e.ColumnIndex].Value;
                }
                else if (e.ColumnIndex == ProblemTable[e.RowIndex].Length)
                {
                    string str = "";
                    if (OilFieldColumn != -1)
                    {
                        if (WellColumn != -1)
                        {
                            str = ProblemTable[e.RowIndex][WellColumn].Error;
                        }
                        else
                        {
                            str = "Не задан столбец 'Скважина'";
                        }
                    }
                    else
                    {
                        str = "Не задан столбец 'Месторождение'";
                    }

                    for (int i = 0; i < DataTypes.Length; i++)
                    {
                        if ((DataTypes[i] == ColumnDataType.DATATIME) || (DataTypes[i] == ColumnDataType.DOUBLE))
                        {
                            if (!ProblemTable[e.RowIndex][i].CorrectValue)
                            {
                                if (str != "") str += ",";
                                str += ProblemTable[e.RowIndex][i].Error;
                            }
                        }
                        else if (DataTypes[i] == ColumnDataType.NONE)
                        {
                            if (str != "") str += ",";
                            str += "Есть столбцы с неопределенным типом";
                        }
                    }
                    e.Value = str;
                }
            }
        }
        void grid_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
        {
            if ((ProblemTable != null) && (e.RowIndex < RowsCount))
            {
                if (e.ColumnIndex < ProblemTable[e.RowIndex].Length)
                {
                    string val = "";
                    if(e.Value != null) val = (string)e.Value;
                    ProblemTable[e.RowIndex][e.ColumnIndex].Value = val;
                    this.Invalidate();
                }
            }
        }
        void grid_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if ((ProblemTable != null) && (e.RowIndex < RowsCount))
            {
                ProblemCellColor color = ProblemCellColor.WHITE;

                if (e.RowIndex == -1)
                {
                    if (e.ColumnIndex < ProblemColumns.Length)
                    {
                        color = ProblemColumns[e.ColumnIndex].ColorIndex;
                    }
                    else color = ProblemCellColor.GRAY;
                }
                else if ((ProblemTable[e.RowIndex] != null) && (e.ColumnIndex < ProblemTable[e.RowIndex].Length))
                {
                    color = ProblemTable[e.RowIndex][e.ColumnIndex].ColorIndex;
                }
                else if (e.ColumnIndex == ProblemTable[e.RowIndex].Length)
                {
                    color = ProblemCellColor.GRAY;
                }
                switch (color)
                {
                    case ProblemCellColor.WHITE:
                        e.CellStyle.BackColor = Color.White;
                        break;
                    case ProblemCellColor.PINK:
                        e.CellStyle.BackColor = Color.Pink;
                        break;
                    case ProblemCellColor.GREEN:
                        e.CellStyle.BackColor = Color.LightGreen;
                        break;
                    case ProblemCellColor.GRAY:
                        e.CellStyle.BackColor = Color.LightGray;
                        break;
                    case ProblemCellColor.YELLOW:
                        e.CellStyle.BackColor = Color.Yellow;
                        break;
                } 
            }
        }
        #endregion

        #region GRID EVENTS
        void grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (DrawWaitMode) return;
            if ((ComboBoxValues.Visible) && (e.KeyCode == Keys.Escape))
            {
                ComboBoxValues.DroppedDown = false;
                ComboBoxValues.Hide();
                this.Focus();
            }
            if ((e.KeyCode == Keys.Delete) && (this.SelectedCells.Count > 0))
            {
                List<int> selectedColumns = new List<int>();
                for (int i = 0; i < this.SelectedCells.Count; i++)
                {
                    if (selectedColumns.IndexOf(this.SelectedCells[i].ColumnIndex) == -1)
                    {
                        selectedColumns.Add(this.SelectedCells[i].ColumnIndex);
                        if (selectedColumns.Count == ProblemColumns.Length) break;
                    }
               }
                if (selectedColumns.Count >= ProblemColumns.Length)
                {
                    if (this.SelectedCells.Count >= RowsCount * ProblemColumns.Length) // удаляем все
                    {
                        this.Clear();
                        wellListSettings.HideProblemList();
                    }
                    else
                    {
                        // удаляем выделенные строки
                        for (int i = 0; i < this.SelectedCells.Count; i++)
                        {
                            ProblemTable[this.SelectedCells[i].RowIndex] = null;
                        }
                        DefragNullValues();
                        UpdateRowsCount();
                        for (int i = 0; i < selectedColumns.Count; i++)
                        {
                            UpdateColumnCellsStyle(selectedColumns[i]);
                        }
                        while (this.SelectedCells.Count > 0)
                        {
                            this.SelectedCells[0].Selected = false;
                        }
                        if (RowsCount == 0) wellListSettings.HideProblemList();
                    }
                }
                else
                {
                    int RowIndex, ColumnIndex;
                    for (int i = 0; i < this.SelectedCells.Count; i++)
                    {
                        RowIndex = this.SelectedCells[i].RowIndex;
                        ColumnIndex = this.SelectedCells[i].ColumnIndex;
                        ProblemTable[RowIndex][ColumnIndex].Value = "";
                        SetCellStyle(RowIndex, ColumnIndex, false);
                    }
                }
                this.Invalidate();
            }
        }
        void grid_MouseWheel(object sender, MouseEventArgs e)
        {
            if (ComboBoxValues.Visible)
            {
                ComboBoxValues.Hide();
                ComboBoxValues.DroppedDown = false;
            }
        }
        void grid_MouseMove(object sender, MouseEventArgs e)
        {
            int res = TestDropDownBoxMouseMove(e.Location);
            if (DropDownMouseOver != res)
            {
                DropDownMouseOver = res;
                this.Invalidate();
            }
        }
        void grid_MouseUp(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo info = HitTest(e.X, e.Y);
            if (DrawWaitMode) return;
            if ((countMouseDown > 1) && (MouseDownCell != null) && 
                (info.ColumnIndex == MouseDownCell.ColumnIndex) && (info.RowIndex == MouseDownCell.RowIndex))
            {
                bool inSelected = false;
                bool isOneColumn = true;
                int SelectedColumn = -1;
                for (int i = 0; i < this.SelectedCells.Count; i++)
                {
                    if (SelectedColumn == -1) SelectedColumn = this.SelectedCells[i].ColumnIndex;
                    if ((isOneColumn) && (SelectedColumn != this.SelectedCells[i].ColumnIndex))
                    {
                        isOneColumn = false;
                    }
                    if ((this.SelectedCells[i].RowIndex == info.RowIndex) && (this.SelectedCells[i].ColumnIndex == info.ColumnIndex))
                    {
                        inSelected = true;
                    }
                }
                if ((inSelected) && (isOneColumn))
                {
                    bool showCombo = ComboBoxShow(info.RowIndex, info.ColumnIndex);
                    if (!showCombo)
                    {
                        if (info.ColumnIndex != OilFieldColumn)
                        {
                            CurrentCell = Rows[info.RowIndex].Cells[info.ColumnIndex];
                            BeginEdit(true);
                        }
                    }
                    else
                    {
                        if (SavedSelectedCells != null)
                        {
                            for (int i = 0; i < SavedSelectedCells.Length; i++)
                            {
                                SavedSelectedCells[i].Selected = true;
                            }
                        }
                    }
                }
            }
            MouseDownCell = null;
        }
        void grid_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo info = HitTest(e.X, e.Y);
            MouseDownCell = null;
            if (DrawWaitMode) return;
            if (ComboBoxValues.Visible)
            {
                ComboBoxValues.Visible = false;
            }
            if (SelectedColumns.Count > 0)
            {
                SelectedColumns[0].Selected = false;
            }
            SavedSelectedCells = null;
            if ((info.ColumnIndex > -1) && (info.RowIndex > -1))
            {
                if (e.Button == MouseButtons.Right)
                {
                    SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
                    countMouseDown = 0;
                    Columns[info.ColumnIndex].Selected = true;
                    ContextMenuStrip = null;
                    if ((info.ColumnIndex > -1) && (info.ColumnIndex < Columns.Count - 1))
                    {
                        ContextItemsEnabled(info.ColumnIndex);
                        ContextMenuStrip = contextMenu;
                    }
                }
                else
                {
                    bool inSelected = false;
                    bool isOneColumn = true;
                    int SelectedColumn = -1;
                    for (int i = 0; i < this.SelectedCells.Count; i++)
                    {
                        if (SelectedColumn == -1) SelectedColumn = this.SelectedCells[i].ColumnIndex;
                        if ((isOneColumn) && (SelectedColumn != this.SelectedCells[i].ColumnIndex))
                        {
                            isOneColumn = false;
                        }
                        if ((this.SelectedCells[i].RowIndex == info.RowIndex) && (this.SelectedCells[i].ColumnIndex == info.ColumnIndex))
                        {
                            inSelected = true;
                        }
                    }

                    if ((isOneColumn) && (inSelected))
                    {
                        countMouseDown++;
                        if (info.ColumnIndex == OilFieldColumn)
                        {
                            SavedSelectedCells = new DataGridViewCell[this.SelectedCells.Count];
                            for (int i = 0; i < this.SelectedCells.Count; i++)
                            {
                                SavedSelectedCells[i] = this.SelectedCells[i];
                            }
                        }
                        else
                        {
                            SavedSelectedCells = new DataGridViewCell[1];
                            SavedSelectedCells[0] = Rows[info.RowIndex].Cells[info.ColumnIndex];
                        }
                    }
                    else
                    {
                        countMouseDown = 1;
                        SelectionMode = DataGridViewSelectionMode.CellSelect;
                        //MouseDownCell = Rows[info.RowIndex].Cells[info.ColumnIndex];
                    }
                }
                MouseDownCell = Rows[info.RowIndex].Cells[info.ColumnIndex];
            }
            else if ((info.ColumnIndex != -1) && (e.Button != MouseButtons.Right) && (info.ColumnIndex < ProblemColumns.Length))
            {
                countMouseDown = 0;
                SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
                this.Columns[info.ColumnIndex].Selected = true;
                int sumWidth = 0;
                Point screen = Point.Empty;
                for (int i = 0; i < info.ColumnIndex; i++) sumWidth += this.Columns[i].Width;

                if ((sumWidth + this.Columns[info.ColumnIndex].Width - HorizontalScrollBar.Value - 14 < e.X) &&
                    (e.X < sumWidth + HorizontalScrollBar.Value + this.Columns[info.ColumnIndex].Width - 1))
                {
                    screen.X = sumWidth + this.Columns[info.ColumnIndex].Width - HorizontalScrollBar.Value - 14;
                    screen.Y = this.Location.Y + 16;
                    screen = this.Parent.PointToScreen(screen);
                    ContextItemsEnabled(info.ColumnIndex);
                    contextMenu.Show(screen);
                }
            }
            else
            {
                ContextMenu = null;
                countMouseDown = 0;
                if (IsCurrentCellInEditMode) EndEdit();
                while (SelectedCells.Count > 0) SelectedCells[0].Selected = false;
            }
        }
        void grid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex > -1) && (e.RowIndex > -1))
            {
                SetCellStyle(e.RowIndex, e.ColumnIndex, true);
                UpdateColumnCellsStyle(e.ColumnIndex);
                this.Invalidate();
            }
        }
        void SetCommentColumnSize()
        {
            if (Columns.Count > 0)
            {
                int sumWidth = 0;
                for (int i = 0; i < Columns.Count - 1; i++)
                {
                    sumWidth += Columns[i].Width;
                }
                if (this.VerticalScrollBar.Visible) sumWidth += VerticalScrollBar.Width;
                if (sumWidth < this.ClientRectangle.Width)
                {
                    Columns[Columns.Count - 1].Width = this.ClientRectangle.Width - sumWidth - 4;
                }
            }
        }
        void UpdateControlsSize()
        {
            label.Text = "Окно редактирования списка скважин";
            int widthAll = this.ClientRectangle.Width;
            lNum.Location = new Point(Parent.Width - 17 - lNum.Width, 4);
            int sumWidth = label.Size.Width + lNum.Size.Width + pClose.Width;
            if (sumWidth > widthAll)
            {
                lNum.Visible = false;
                sumWidth -= lNum.Size.Width;
            }
            else
            {
                lNum.Visible = true;
            }
            sumWidth = sumWidth + bUp.Width;
            if (sumWidth > widthAll)
            {
                sumWidth = ((lNum.Visible) ? lNum.Width : 0) + pClose.Width + bUp.Width;
                string str = label.Text;
                int remove = 1;
                Graphics grfx = this.CreateGraphics();
                SizeF size = grfx.MeasureString(str.Remove(str.Length - remove, remove) + "...", label.Font);
                while ((remove < str.Length - 4) && (size.Width + sumWidth > widthAll))
                {
                    remove++;
                    size = grfx.MeasureString(str.Remove(str.Length - remove, remove) + "...", label.Font);
                }
                if (size.Width + sumWidth < widthAll)
                {
                    label.Text = str.Remove(str.Length - remove, remove) + "...";
                    bUp.Location = new Point(label.Location.X + label.Width, bUp.Location.Y);
                    bUp.Visible = true;
                }
                else
                {
                    
                    bUp.Visible = false;
                }
            }
            else
            {
                bUp.Visible = true;
            }
            bUp.Location = new Point(label.Location.X + label.Width, bUp.Location.Y);
        }
        void grid_SizeChanged(object sender, EventArgs e)
        {
           UpdateControlsSize();
           SetCommentColumnSize();
        }
        void grid_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            SetCommentColumnSize();
        }
        #endregion

        #region DRAWING
        void DrawingWaitMode(Graphics grfx)
        {
            if (DrawWaitMode)
            {
                Size sz = this.ClientRectangle.Size;
                string str = "Идет обработка. Пожалуйста подождите...";
                SizeF sz2 = grfx.MeasureString(str, this.Font, PointF.Empty, StringFormat.GenericDefault);
                using (Brush br = new SolidBrush(Color.FromArgb(125, Color.Gray)))
                {
                    grfx.FillRectangle(br, this.ClientRectangle);
                }
                grfx.FillRectangle(Brushes.White, sz.Width / 2 - sz2.Width / 2, sz.Height / 2 - sz2.Height / 2, sz2.Width, sz2.Height);
                grfx.DrawRectangle(Pens.Black, sz.Width / 2 - sz2.Width / 2, sz.Height / 2 - sz2.Height / 2, sz2.Width, sz2.Height);
                grfx.DrawString(str, this.Font, Brushes.Black, sz.Width / 2 - sz2.Width / 2, sz.Height / 2 - sz2.Height / 2);
            }
        }
        void DrawDropDownBoxes(Graphics grfx)
        {
            float x, y, sum = 0;
            PointF[] poly = new PointF[3];
            Brush br;
            Color clr;
            for (int i = 0; i < Columns.Count - 1; i++)
            {
                switch (ProblemColumns[i].ColorIndex)
                {
                    case ProblemCellColor.WHITE:
                        clr = Color.White;
                        break;
                    case ProblemCellColor.PINK:
                        clr = Color.Pink;
                        break;
                    case ProblemCellColor.GRAY:
                        clr = Color.LightGray;
                        break;
                    case ProblemCellColor.GREEN:
                        clr = Color.LightGreen;
                        break;
                    case ProblemCellColor.YELLOW:
                        clr = Color.Yellow;
                        break;
                    default :
                        clr = Color.White;
                        break;
                }
                using (br = new SolidBrush(clr))
                {
                    x = sum + this.Columns[i].Width - 14 - this.HorizontalScrollBar.Value;
                    if (x > this.ClientRectangle.Right) break;
                    if (i == DropDownMouseOver)
                    {
                        grfx.FillRectangle(Brushes.LightGray, x, 3, 12, 12);
                    }
                    else
                    {
                        grfx.FillRectangle(br, x, 3, 12, 12);
                    }
                    grfx.DrawRectangle(Pens.Black, x, 3, 12, 12);
                    sum += this.Columns[i].Width;
                }
            }
            sum = 0;
            grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            for (int i = 0; i < Columns.Count - 1; i++)
            {
                x = sum + this.Columns[i].Width - 14 - this.HorizontalScrollBar.Value;
                if (x > this.ClientRectangle.Right) break;
                poly[0].X = x + 2; poly[0].Y = 6;
                poly[1].X = x + 10; poly[1].Y = 6;
                poly[2].X = x + 6; poly[2].Y = 12;
                grfx.FillPolygon(Brushes.Black, poly);
                sum += this.Columns[i].Width;
            }
        }
        void ProblemWellList_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            DrawDropDownBoxes(grfx);
            DrawingWaitMode(grfx);
        }
        #endregion
    }
}