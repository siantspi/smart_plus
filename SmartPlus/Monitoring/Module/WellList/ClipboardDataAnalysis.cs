﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public enum CallerType : byte
    {
        WELLIST
    }
    public enum ClipboardDataType : byte
    {
        NONE,
        WELL_NAME,
        OILFIELD_NAME,
        OILFIELD_AREA_NAME,
        TEXT,
        DATATIME,
        DOUBLE
    };

    public partial class ClipboardDataAnalysis : Form
    {
        Project proj = null;
        ClipboardDataType[] DataTypes;
        OilFieldAreaDictionary dictArea = null;
        ContextMenuStrip contextMenu;
        DataGridViewCell MouseDownCell = null;

        #region DATA TYPE NAMES
        string[] DataTypeNames = 
        { 
            "Не определено", 
            "Номер скважины",
            "Месторождение",
            "Площадь",
            "Текст",
            "Дата",
            "Число"
        };
        #endregion

        public ClipboardDataAnalysis()
        {
            InitializeComponent();
            DataTypes = null;
            grid.EnableHeadersVisualStyles = false;
            contextMenu = new ContextMenuStrip();
            grid.MouseDown += new MouseEventHandler(grid_MouseDown);
            grid.CellDoubleClick += new DataGridViewCellEventHandler(grid_CellDoubleClick);
            grid.CellValueChanged += new DataGridViewCellEventHandler(grid_CellValueChanged);
        }

        public ClipboardDataType[] GetClipboardStructure(string ClipboardText, Project SourceProject, CallerType Caller)
        {
            if ((ClipboardText != null) && (ClipboardText != ""))
            {
                this.proj = SourceProject;
                dictArea = (OilFieldAreaDictionary)this.proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);

                FillGridByClipboard(ClipboardText, Caller);

                if (this.ShowDialog() == DialogResult.OK)
                {
                    return DataTypes;
                }
            }
            return null;
        }

        void FillGridByClipboard(string ClipboardText, CallerType Caller)
        {
            string[] rowSep = { "\r\n" };
            char[] columnSep = { '\t' };
            string[] rows = ClipboardText.Split(rowSep, StringSplitOptions.RemoveEmptyEntries);
            string[] cols;
            grid.Rows.Clear();
            grid.Columns.Clear();
            DataGridViewColumn column;
            bool columnsCreated = false;
            int i, j;
            DataGridViewRow row = null;
            DataGridViewCell cellTemplate = new DataGridViewTextBoxCell();
            if (rows.Length > 0)
            {
                for (i = 0; i < rows.Length; i++)
                {
                    cols = rows[i].Split();
                    if (!columnsCreated)
                    {
                        if (cols.Length == 0) continue;
                        DataTypes = new ClipboardDataType[cols.Length];
                        for (j = 0; j < cols.Length; j++)
                        {
                            column = new DataGridViewColumn();
                            column.SortMode = DataGridViewColumnSortMode.NotSortable;
                            column.HeaderText = "Неопределено";
                            column.CellTemplate = cellTemplate;
                            column.HeaderCell.Style.BackColor = Color.Pink;
                            grid.Columns.Add(column);
                            switch (Caller)
                            {
                                case CallerType.WELLIST:
                                    CreateWellListMenu();
                                    break;
                            }
                        }
                        columnsCreated = true;
                    }
                    row = new DataGridViewRow();
                    row.CreateCells(grid);
                    for (j = 0; j < cols.Length; j++)
                    {
                        row.Cells[j].Value = cols[j].Trim();
                    }
                    grid.Rows.Add(row);
                }
            }
        }

        #region TRY GET STRUCTURE BY CALLER
        public ClipboardDataType[] TryWellListStructure(string ClipboardText)
        {
            if ((ClipboardText != null) && (ClipboardText != ""))
            {
                char[] digits = new char[10] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                if (dictArea == null)
                {
                    dictArea = (OilFieldAreaDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                }
                ClipboardDataType[] localTypes = null;
                string[] rowSep = { "\r\n" };
                char[] columnSep = { '\t' };
                string[] rows = ClipboardText.Split(rowSep, StringSplitOptions.RemoveEmptyEntries);
                string[] cols;
                int i, j, countDigits;
                string str;
                if (rows.Length > 0)
                {
                    for (i = 0; i < rows.Length; i++)
                    {
                        countDigits = 0;
                        cols = rows[i].Split();
                        if (cols.Length > 3) return null;
                        for (j = 0; j < cols.Length; j++)
                        {
                            str = cols[j].Trim();

                        }
                        if(localTypes == null)
                        {

                        }
                        
                    }
                }
            }
            return null;
        }
        #endregion

        #region CONTEXT MENU FUNC
        void CreateWellListMenu()
        {
            contextMenu.Items.Clear();
            ToolStripMenuItem menuItem;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add("");  menuItem.Tag = ClipboardDataType.WELL_NAME;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add("");  menuItem.Tag = ClipboardDataType.OILFIELD_NAME;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add("");  menuItem.Tag = ClipboardDataType.OILFIELD_AREA_NAME;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add("");  menuItem.Tag = ClipboardDataType.TEXT;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add("");  menuItem.Tag = ClipboardDataType.DATATIME;
            menuItem = (ToolStripMenuItem)contextMenu.Items.Add("");  menuItem.Tag = ClipboardDataType.DOUBLE;

            int ind;
            for(int i = 0; i < contextMenu.Items.Count;i++)
            {
                menuItem = (ToolStripMenuItem)contextMenu.Items[i];
                ind = (int)(ClipboardDataType)menuItem.Tag;
                menuItem.Text = DataTypeNames[ind];
                menuItem.Click += new EventHandler(menuItem_Click);
            }
            grid.ContextMenuStrip = contextMenu;
        }

        void menuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
            if (menuItem.Tag != null)
            {
                if (menuItem.Checked)
                {
                    grid.SelectedColumns[0].HeaderText = DataTypeNames[0];
                    DataTypes[grid.SelectedColumns[0].Index] = ClipboardDataType.NONE;
                    grid.Columns[grid.SelectedColumns[0].Index].HeaderCell.Style.BackColor = Color.Pink;
                    SetColumnCellsStyle(grid.SelectedColumns[0].Index);
                }
                else
                {
                    ClipboardDataType type = (ClipboardDataType)menuItem.Tag;
                    grid.SelectedColumns[0].HeaderText = DataTypeNames[(int)type];
                    DataTypes[grid.SelectedColumns[0].Index] = type;
                    if (SetColumnCellsStyle(grid.SelectedColumns[0].Index))
                    {
                        grid.Columns[grid.SelectedColumns[0].Index].HeaderCell.Style.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        grid.Columns[grid.SelectedColumns[0].Index].HeaderCell.Style.BackColor = Color.Pink;
                    }
                }
            }
            grid.SelectionMode = DataGridViewSelectionMode.CellSelect;
            if (MouseDownCell != null) MouseDownCell.Selected = true;
        }
        #endregion

        #region GRID EVENTS
        void grid_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo info = grid.HitTest(e.X, e.Y);
            MouseDownCell = null;
            if ((info.ColumnIndex > -1) && (info.RowIndex > -1))
            {
                grid.SelectionMode = DataGridViewSelectionMode.CellSelect;
                MouseDownCell = grid.Rows[info.RowIndex].Cells[info.ColumnIndex];
                if (e.Button == MouseButtons.Right)
                {
                    grid.SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
                    grid.Columns[info.ColumnIndex].Selected = true;
                    grid.ContextMenuStrip = null;
                    if ((info.ColumnIndex > -1) && (info.ColumnIndex < grid.Columns.Count))
                    {
                        grid.ContextMenuStrip = contextMenu;

                        if ((DataTypes != null) && (contextMenu.Items.Count > 0))
                        {
                            ClipboardDataType type = DataTypes[info.ColumnIndex];
                            ToolStripMenuItem menuItem;
                            for (int i = 0; i < contextMenu.Items.Count; i++)
                            {
                                menuItem = (ToolStripMenuItem)contextMenu.Items[i];
                                if ((menuItem.Tag != null) && (((ClipboardDataType)menuItem.Tag) == type))
                                {
                                    menuItem.Checked = true;
                                }
                                else
                                {
                                    menuItem.Checked = false;
                                }

                            }
                        }
                    }
                }
            }
            else if (grid.SelectedColumns.Count > 0)
            {
                grid.SelectedColumns[0].Selected = false;
            }
        }
        void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex > -1) && (e.RowIndex > -1))
            {
                grid.CurrentCell = grid.Rows[e.RowIndex].Cells[e.ColumnIndex];
                grid.BeginEdit(true);
            }
        }
        void grid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex > -1) && (e.RowIndex > -1))
            {
                if (SetColumnCellsStyle(e.ColumnIndex))
                {
                    grid.Columns[e.ColumnIndex].HeaderCell.Style.BackColor = Color.LightGreen;
                }
                else
                {
                    grid.Columns[e.ColumnIndex].HeaderCell.Style.BackColor = Color.Pink;
                }
            }
        }
        #endregion

        #region GRID CELLS STYLE
        bool SetColumnCellsStyle(int ColumnIndex)
        {
            bool result = true;
            if ((grid.Columns.Count > 0) && (grid.Rows.Count > 0) && (ColumnIndex < grid.Columns.Count))
            {
                for (int i = 0; i < grid.Rows.Count; i++)
                {
                    result = SetCellStyle(i, ColumnIndex) && result;
                }
            }
            return result;
        }
        bool SetCellStyle(int RowIndex, int ColumnIndex)
        {
            bool result = true;
            if ((grid.Columns.Count > 0) && (grid.Rows.Count > 0) && (RowIndex < grid.Rows.Count) && (ColumnIndex < grid.Columns.Count))
            {
                char[] digits = new char[10] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                int ind = -1;
                string cellValue = (string)grid.Rows[RowIndex].Cells[ColumnIndex].Value;
                switch (DataTypes[ColumnIndex])
                {
                    case ClipboardDataType.NONE:
                        grid.Rows[RowIndex].Cells[ColumnIndex].Style.BackColor = Color.White;
                        result = false;
                        break;
                    case ClipboardDataType.OILFIELD_NAME:
                        ind = proj.GetOFIndex(cellValue.ToUpper());
                        if (ind == -1) result = false;
                        break;
                    case ClipboardDataType.OILFIELD_AREA_NAME:
                        ind = dictArea.GetIndexByShortName(cellValue);
                        if (ind == -1) result = false;
                        break;
                    case ClipboardDataType.WELL_NAME:
                        char first = cellValue[0];
                        result = false;
                        for(int i = 0; i < digits.Length;i++)
                        {
                            if (digits[i] == first)
                            {
                                result = true;
                                break;
                            }
                        }
                        break;
                    case ClipboardDataType.DATATIME:
                        break;
                    case ClipboardDataType.DOUBLE:
                        break;
                }
                if (!result)
                {
                    grid.Rows[RowIndex].Cells[ColumnIndex].Style.BackColor = Color.Pink;
                }
                else
                {
                    grid.Rows[RowIndex].Cells[ColumnIndex].Style.BackColor = Color.White;
                }

            }
            return result;
        }
        #endregion
    }
}
