﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RDF.Objects;
using Selecting;
using System.Collections;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    delegate void OnFindWellsCompleteDelegate();
    sealed class DataGridWellList : DataGridView
    {
        public event OnFindWellsCompleteDelegate OnFindWellsComplete;
        public DataTable table;

        Project proj;
        C2DLayer layer;
        WellListSettings wellListSettings;
        internal bool LayerDataChanged = false;
        FontFamily[] fontFamilyList;
        Font[] iconFonts;
        ExFontList exFontList;
        bool EditCellMode = false;
        ArrayList columnWidth;
        private ProgressBar progress;
        bool WaitIconMode;
        string LastFindName;
        int LastFindRowDisplayed = -1;
        public List<int> FindWellIndex;
        public bool DrawWaitIconMode
        {
            get { return WaitIconMode; }
            set 
            {
                progress.Visible = value;
                WaitIconMode = value;
            }
        }
        BackgroundWorker finder;
        DataGridViewCell lastSelectCell;

        public DataGridWellList(WellListSettings WLSettings, Control Panel)
        {
            InitContextMenu();

            columnWidth = new ArrayList(10);
            columnWidth.Add(27);
            columnWidth.Add(24);
            columnWidth.Add(60);
            columnWidth.Add(90);
            columnWidth.Add(80);
            columnWidth.Add(10);
            columnWidth.Add(10);
            columnWidth.Add(60);
            columnWidth.Add(65);
            columnWidth.Add(60);
            table = new DataTable("WL");
            wellListSettings = WLSettings;
            
            this.Parent = Panel;
            this.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.Dock = DockStyle.Fill;
            this.Font = new Font("Calibri", 8.25f);
            this.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            
            progress = new ProgressBar();
            Panel.Controls.Add(progress);
            progress.Visible = true;
            Point pt = Point.Empty;
            progress.Dock = DockStyle.Bottom;
            progress.Height = 18;
            DrawWaitIconMode = false;

            #region ADD COLUMNS TABLE
            table.Columns.Add("NN", Type.GetType("System.Int32"));                  // 0
            table.Columns.Add("ICON", Type.GetType("System.String"));               // 1
            table.Columns.Add("WELLNAME", Type.GetType("System.String"));           // 2
            table.Columns.Add("OILFIELD", Type.GetType("System.String"));           // 3
            table.Columns.Add("OOILFIELDNAME", Type.GetType("System.String"));      // 4
            table.Columns.Add("PHWELL_INDEX", Type.GetType("System.Int32"));        // 5
            table.Columns.Add("PHWELL_OFAREA_LIST", Type.GetType("System.String")); // 6
            table.Columns.Add("STRING_VALUE", Type.GetType("System.String"));       // 7
            table.Columns.Add("DATETIME_VALUE", Type.GetType("System.DateTime"));   // 8
            table.Columns.Add("DOUBLE_VALUE", Type.GetType("System.Double"));       // 9
            #endregion

            #region Поиск скважин по списку
            LastFindName = string.Empty;
            finder = new BackgroundWorker();
            finder.WorkerSupportsCancellation = true;
            finder.DoWork += new DoWorkEventHandler(finder_DoWork);
            finder.RunWorkerCompleted += new RunWorkerCompletedEventHandler(finder_RunWorkerCompleted);
            FindWellIndex = new List<int>();
            #endregion
            
            this.RowTemplate.Height = 16;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = false;
            this.DoubleBuffered = true;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            DataGridViewCellStyle HeaderCellStyle = new DataGridViewCellStyle();
            HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
            HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
            HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
            ColumnHeadersDefaultCellStyle = HeaderCellStyle;
            BackgroundColor = SystemColors.ControlDark;

            this.CellParsing += new DataGridViewCellParsingEventHandler(DataGridWellList_CellParsing);
            this.DataError += new DataGridViewDataErrorEventHandler(DataGridWellList_DataError);
            this.CellValueChanged += new DataGridViewCellEventHandler(DataGridWellList_CellValueChanged);
            this.CellPainting += new DataGridViewCellPaintingEventHandler(DataGridWellList_CellPainting);
            this.ColumnWidthChanged += new DataGridViewColumnEventHandler(DataGridWellList_ColumnWidthChanged);
            this.KeyDown += new KeyEventHandler(DataGridWellList_KeyDown);
            this.CellMouseDown += new DataGridViewCellMouseEventHandler(DataGridWellList_CellMouseDown);
            this.CellDoubleClick += new DataGridViewCellEventHandler(DataGridWellList_CellDoubleClick);
            this.MouseDown += new MouseEventHandler(DataGridWellList_MouseDown);
            this.Paint += new PaintEventHandler(DataGridWellList_Paint);
        }

        #region Context Menu
        ContextMenuStrip cMenu;
        void InitContextMenu()
        {
            cMenu = new ContextMenuStrip();
            cMenu.Items.Add("Копировать в буфер обмена", Properties.Resources.Copy16, cmCopyCells_Click);
            cMenu.Items.Add("Добавить из буфера обмена", Properties.Resources.Add_ClipBoard_Skv16, cmPasteWells_Click);
            cMenu.Items.Add("Редактирование значков...", Properties.Resources.EditIcon16, cmEditIcons_Click);
            cMenu.Items.Add("-");
            cMenu.Items.Add("Удалить скважины из списка", Properties.Resources.Delete16, cmDeleteWells_Click);
            this.ContextMenuStrip = cMenu;
        }

        void cmEditIcons_Click(object sender, EventArgs e)
        {
            EditIconList();
        }
        void cmCopyCells_Click(object sender, EventArgs e)
        {
            copySelectedRowsToClipboard((DataGridView)this);
        }
        void cmPasteWells_Click(object sender, EventArgs e)
        {
            wellListSettings.AddClipboardWellList();
        }
        void cmDeleteWells_Click(object sender, EventArgs e)
        {
            wellListSettings.RemoveSelectedWells(true);
        }
        #endregion

        #region Finder
        public void StartFindWell(string FindName)
        {
            if (LastFindName != FindName)
            {
                FindWellIndex.Clear();
                LastFindRowDisplayed = -1;
                LastFindName = FindName;
                if (finder.IsBusy)
                {
                    
                    finder.CancelAsync();
                }
                else
                {
                    finder.RunWorkerAsync(FindName);
                }
            }
        }
        void finder_DoWork(object sender, DoWorkEventArgs e)
        {
            string name = (string)e.Argument;
            List<int> newFind = new List<int>();
            if (name.Length > 0)
            {
                for (int i = 0; i < this.Rows.Count; i++)
                {
                    if (finder.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    if (((string)this.Rows[i].Cells[2].Value).StartsWith(name))
                    {
                        newFind.Add(i);
                    }
                }
                e.Result = newFind;
            }
        }
        void finder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled && LastFindName.Length > 0)
            {
                finder.RunWorkerAsync(LastFindName);
            }
            else if (e.Error == null && e.Result != null)
            {
                FindWellIndex = (List<int>)e.Result;
                ShowNextFindRow(1);
            }
            if (OnFindWellsComplete != null) OnFindWellsComplete();
            this.Invalidate();
        }
        void ShowNextFindRow(int count)
        {
            if (FindWellIndex.Count > 0)
            {
                LastFindRowDisplayed += count;
                if (LastFindRowDisplayed < 0) LastFindRowDisplayed = FindWellIndex.Count - 1;
                if (LastFindRowDisplayed >= FindWellIndex.Count) LastFindRowDisplayed = 0;
                int ind = FindWellIndex[LastFindRowDisplayed];
                int dispRows = this.DisplayedRowCount(false);
                if (ind < this.FirstDisplayedScrollingRowIndex || ind > this.FirstDisplayedScrollingRowIndex + dispRows)
                {
                    int newInd = ind - (int)(dispRows / 2.0);
                    if (newInd < 0) newInd = 0;
                    this.FirstDisplayedScrollingRowIndex = newInd;
                }
                while (this.SelectedRows.Count > 0) this.SelectedRows[0].Selected = false;
                if (FindWellIndex[LastFindRowDisplayed] > -1 && FindWellIndex[LastFindRowDisplayed] < this.Rows.Count)
                {
                    this.Rows[FindWellIndex[LastFindRowDisplayed]].Selected = true;
                }
            }
        }
        public void KeyPress(Keys key)
        {
            switch (key)
            {
                case Keys.Up:
                    ShowNextFindRow(-1);
                    break;
                case Keys.Down:
                    ShowNextFindRow(1);
                    break;
                case Keys.PageUp:
                    ShowNextFindRow(-15);
                    break;
                case Keys.PageDown:
                    ShowNextFindRow(15);
                    break;
                case Keys.Enter:
                    if (LastFindRowDisplayed > -1 && LastFindRowDisplayed < FindWellIndex.Count)
                    {
                        CenterWellOnMap(FindWellIndex[LastFindRowDisplayed]);
                    }
                    break;
            }
        }
        #endregion

        #region Ручной ввод данных
        void DataGridWellList_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridViewCell cell = this.Rows[e.RowIndex].Cells[e.ColumnIndex];
            switch (e.ColumnIndex)
            {
                case 8:
                    MessageBox.Show("Введена неправильная дата", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                    break;
                case 9:
                    MessageBox.Show("Введено неправильное число", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                    break;
            }
        }
        void DataGridWellList_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (e.Value != null)
            {
                DataGridViewCell cell = this.Rows[e.RowIndex].Cells[e.ColumnIndex];
                if (e.ColumnIndex == 8)
                {
                    string val = ((string)e.Value).Replace(',', '.');
                    e.Value = val;
                    e.ParsingApplied = true;
                }
            }
        }
        void DataGridWellList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex > -1) && (e.ColumnIndex > 6))
            {
                PhantomWell pw;
                int ind = (int)this.Rows[e.RowIndex].Cells[0].Value - 1;
                ind = (int)this.table.Rows[ind].ItemArray[5];
                if ((ind > -1) && (ind < layer.ObjectsList.Count))
                {
                    pw = (PhantomWell)layer.ObjectsList[ind];
                    pw.CaptionVisible = true;
                    if (pw.Caption == null) pw.Caption = new PhantomWellCaption();
                    switch (e.ColumnIndex)
                    {
                        case 7:
                            if (this.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != System.DBNull.Value)
                            {
                                pw.Caption.Text = (string)this.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                            }
                            else
                            {
                                pw.Caption.Text = "";
                            }
                            break;
                        case 8:
                            if (this.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != System.DBNull.Value)
                            {
                                pw.Caption.Date = (DateTime)this.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                            }
                            else
                            {
                                pw.Caption.Date = DateTime.MinValue;
                            }
                            break;
                        case 9:
                            if (this.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != System.DBNull.Value)
                            {
                                pw.Caption.Value = (double)this.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                            }
                            else
                            {
                                pw.Caption.Value = Constant.DOUBLE_NAN;
                            }
                            break;
                    }
                    if (EditCellMode)
                    {
                        wellListSettings.UpdateMap();
                        wellListSettings.SetWorkFolderChanged(true);
                    }
                }
            }
        }
        #endregion

        #region EVENT HANDLER
        void DataGridWellList_MouseDown(object sender, MouseEventArgs e)
        {
            wellListSettings.AddStatUsageMessage(CollectorStatId.WELLLIST_MOUSE_DOWN);
            lastSelectCell = null;
            HitTestInfo hit = HitTest(e.X, e.Y);
            cMenu.Items[0].Visible = false;
            cMenu.Items[2].Visible = false;
            cMenu.Items[3].Visible = false;
            cMenu.Items[4].Visible = false;
            if ((e.Button == MouseButtons.Right) && (hit.ColumnIndex != -1) && (hit.RowIndex != -1))
            {
                lastSelectCell = Rows[hit.RowIndex].Cells[hit.ColumnIndex];
                if (lastSelectCell.Selected)
                {
                    cMenu.Items[0].Visible = true;
                    cMenu.Items[2].Visible = true;
                    cMenu.Items[3].Visible = true;
                    cMenu.Items[4].Visible = true;
                }
            }
        }
        void DataGridWellList_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            if (e.Column.Index < columnWidth.Count)
            {
                columnWidth[e.Column.Index] = e.Column.Width;
            }
        }
        void DataGridWellList_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1) return;
            if (e.ColumnIndex == 1)
            {
                Graphics grfx = e.Graphics;
                Brush LineBrush = new SolidBrush(this.GridColor);
                Brush BackBrush = new SolidBrush(e.CellStyle.BackColor);
                Brush SelectBackBrush = new SolidBrush(e.CellStyle.SelectionBackColor);

                Pen pen = new Pen(LineBrush);

                e.PaintBackground(e.CellBounds, true);

                grfx.DrawLine(pen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);
                grfx.DrawLine(pen, e.CellBounds.Right - 1, e.CellBounds.Top, e.CellBounds.Right - 1, e.CellBounds.Bottom);

                int ind = (int)this.Rows[e.RowIndex].Cells[0].Value - 1;
                if (ind >= table.Rows.Count) return;

                ind = (int)this.table.Rows[ind].ItemArray[5];

                if ((ind > -1) && (ind < layer.ObjectsList.Count))
                {
                    PhantomWell pw = (PhantomWell)layer.ObjectsList[ind];
                    WellIcon icon = null;
                    if (pw.icons != null) icon = pw.icons.GetActiveIcon();
                    WellIconCode iconCode;

                    if ((icon != null) && (icon.Count > 0))
                    {
                        SizeF iconSize = SizeF.Empty;
                        string s;
                        Font f;
                        int i, maxSize = 0;
                        SolidBrush br;
                        for (i = 0; i < icon.Count; i++)
                        {
                            if (maxSize < icon[i].Size) maxSize = icon[i].Size * 2;
                        }
                        if (maxSize == 0) return;

                        int size = maxSize;
                        if (size < 14) size = 14;
                        using (Bitmap bmp = new Bitmap(size, size))
                        {
                            Graphics gr = Graphics.FromImage(bmp);

                            float x = size / 2, y = size / 2;
                            Rectangle rect = Rectangle.Empty;
                            Rectangle destRect = e.CellBounds;

                            destRect.Height = destRect.Height - 2;
                            if (destRect.Height > 16) destRect.Height = 14;
                            destRect.X = destRect.X + (int)((destRect.Width - destRect.Height) / 2);
                            destRect.Y = destRect.Y + 1;
                            destRect.Width = destRect.Height;

                            rect.Width = size;
                            rect.Height = size;
                            gr.Clear(Color.White);

                            for (i = 0; i < icon.Count; i++)
                            {
                                iconCode = icon[i];
                                s = Convert.ToChar(iconCode.CharCode).ToString();
                                f = new Font(fontFamilyList[icon.GetIconFontCode(iconCode.FontCode)], iconCode.Size * 2);

                                br = new SolidBrush(Color.FromArgb(iconCode.IconColor));
                                iconSize = grfx.MeasureString(s, f, PointF.Empty, StringFormat.GenericTypographic);

                                gr.DrawString(s, f, br, x - iconSize.Width / 2, y - iconSize.Height / 2, StringFormat.GenericTypographic);
                                f.Dispose();
                                br.Dispose();
                            }
                            grfx.DrawImage(bmp, destRect, rect, GraphicsUnit.Pixel);
                            gr.Dispose();
                        }
                    }
                }
                BackBrush.Dispose();
                pen.Dispose();
                LineBrush.Dispose();
                e.Handled = true;
            }
            else if (e.ColumnIndex == 2)
            {
                if (FindWellIndex.IndexOf(e.RowIndex) != -1)
                {
                    Graphics grfx = e.Graphics;
                    e.PaintBackground(e.CellBounds, true);
                    Rectangle rect = e.CellBounds;
                    rect.Inflate(-1, -1);
                    string name = (string)this.Rows[e.RowIndex].Cells[2].Value;
                    if (LastFindName.Length != name.Length)
                    {
                        SizeF size = grfx.MeasureString(LastFindName, e.CellStyle.Font, PointF.Empty, StringFormat.GenericDefault);
                        rect.Width = (int)Math.Floor(size.Width);
                    }
                    grfx.FillRectangle(Brushes.Lime, rect);
                    e.PaintContent(e.CellBounds);
                    e.Handled = true;
                }
            }
        }
        void DataGridWellList_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if ((e.RowIndex > -1) && (e.ColumnIndex > -1))
            {
                if (e.RowIndex < layer.ObjectsList.Count)
                {
                    int ind = (int)this.Rows[e.RowIndex].Cells[0].Value - 1;
                    ind = (int)this.table.Rows[e.RowIndex].ItemArray[5];
                    if ((ind > -1 ) && (ind < layer.ObjectsList.Count))
                    {
                        PhantomWell pw = (PhantomWell)layer.ObjectsList[ind];
                    }
                }
            }            
        }
        void DataGridWellList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.SelectedRows.Count == 1)
            {
                if (e.ColumnIndex > 6)
                {
                    if (e.RowIndex != -1)
                    {
                        CurrentCell = this.Rows[e.RowIndex].Cells[e.ColumnIndex];
                        BeginEdit(true);
                        EditCellMode = true;
                    }
                }
                else if (e.ColumnIndex == 1)
                {
                    int ind = (int)this.Rows[e.RowIndex].Cells[0].Value - 1;
                    ind = (int)this.table.Rows[ind].ItemArray[5];
                    if ((ind > -1) && (ind < layer.ObjectsList.Count))
                    {
                        PhantomWell[] pwList = new PhantomWell[1];
                        pwList[0] = (PhantomWell)layer.ObjectsList[ind];
                        if (ShowIconList(pwList)) wellListSettings.UpdateMap();
                    }
                }
                else
                {
                    CenterWellOnMap(e.RowIndex);
                }
            }
        }
        private void copySelectedRowsToClipboard(DataGridView dgv)
        {
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            Clipboard.Clear();
            dgv.Columns[1].Visible = false;
            if (dgv.GetClipboardContent() != null)
            {
                Clipboard.SetDataObject(dgv.GetClipboardContent());
                Clipboard.GetData(DataFormats.Text);
                IDataObject dt = Clipboard.GetDataObject();
                if (dt.GetDataPresent(typeof(string)))
                {
                    string tb = (string)(dt.GetData(typeof(string)));
                    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                    byte[] dataStr = encoding.GetBytes(tb);
                    Clipboard.SetDataObject(encoding.GetString(dataStr));
                }
            }
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            dgv.Columns[1].Visible = true;
        }
        void DataGridWellList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
            {
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    copySelectedRowsToClipboard((DataGridView)sender);
                }
            }
            else if (e.KeyCode == Keys.Delete)
            {
                wellListSettings.RemoveSelectedWells(false);
            }
            else if ((e.Control) && (e.KeyCode == Keys.V))
            {
                wellListSettings.AddClipboardWellList();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                if (this.SelectedRows.Count == 1)
                {
                    CenterWellOnMap(this.SelectedRows[0].Index);
                }
                this.Focus();
                e.Handled = true;
            }
        }
        #endregion

        public void SelectWell(Well w)
        {
            if (layer.ObjectsList.Count > 0)
            {
                PhantomWell pw;
                int index = -1;
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    pw = (PhantomWell)layer.ObjectsList[i];
                    if(pw.srcWell == w)
                    {
                        index = i;
                        break;
                    }
                }
                if (index != -1)
                {
                    int row = -1;
                    for (int i = 0; i < this.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(this.Rows[i].Cells[0].Value) - 1 == index)
                        {
                            row = i;
                            break;
                        }
                    }
                    if (row != -1)
                    {
                        while (this.SelectedRows.Count > 0) this.SelectedRows[0].Selected = false;
                        this.Rows[row].Selected = true;
                        int dispRows = this.DisplayedRowCount(false);
                        if (row < this.FirstDisplayedScrollingRowIndex || row > this.FirstDisplayedScrollingRowIndex + dispRows)
                        {
                            int newInd = row - (int)(dispRows / 2.0);
                            if (newInd < 0) newInd = 0;
                            this.FirstDisplayedScrollingRowIndex = newInd;
                        }
                    }
                }
            }
        }
        public void SetLayer(C2DLayer Layer)
        {
            this.layer = Layer;
            if(Layer == null)
            {
                ClearDataSource(false);
                table.Rows.Clear();
                SetDataSource();
            }
        }
        public void SetProject(Project MapProject)
        {
            this.proj = MapProject;
        }
        public void ClearDataSource(bool Update)
        {
            this.DataSource = null;
            LastFindName = string.Empty;
            LastFindRowDisplayed = -1;
            if(Update) this.Update();
        }
        public void SetDataSource()
        {
            this.DataSource = table;
            SetStyle();
        }
        private void SetStyle() 
        {
            DataGridViewColumn col;
            DataGridViewCellStyle style;
            col = this.Columns[0];
            col.HeaderText = "№";
            col.Width = (int)columnWidth[0];
            style = col.DefaultCellStyle;
            style.ForeColor = Color.Silver;
            col.DefaultCellStyle = style;
            //col.SortMode = DataGridViewColumnSortMode.NotSortable;
            
            col = this.Columns[1];
            col.HeaderText = "Значок";
            col.Width = (int)columnWidth[1];
            style = col.DefaultCellStyle;
            col.DefaultCellStyle = style;
            col.SortMode = DataGridViewColumnSortMode.NotSortable;

            col = this.Columns[2];
            col.HeaderText = "Скважина";
            col.Width = (int)columnWidth[2];
            style = col.DefaultCellStyle;
            style.Font = new Font(this.Font, FontStyle.Bold);
            col.DefaultCellStyle = style;
            //col.SortMode = DataGridViewColumnSortMode.NotSortable;

            col = this.Columns[3];
            col.HeaderText = "Месторождение";
            col.Width = (int)columnWidth[3];
            style = col.DefaultCellStyle;
            col.DefaultCellStyle = style;
            //col.SortMode = DataGridViewColumnSortMode.NotSortable;

            col = this.Columns[4];
            col.HeaderText = "Площадь";
            col.Width = (int)columnWidth[4];
            style = col.DefaultCellStyle;
            col.DefaultCellStyle = style;
            //col.SortMode = DataGridViewColumnSortMode.NotSortable;

            col = this.Columns[5];
            col.HeaderText = "PhWellIndex";
            col.Width = (int)columnWidth[5];
            col.Visible = false;

            col = this.Columns[6];
            col.HeaderText = "PhWellOFAreaIndexes";
            col.Width = (int)columnWidth[6];
            col.Visible = false;

            col = this.Columns[7];
            col.HeaderText = "Текст";
            col.Width = (int)columnWidth[7];
            //col.SortMode = DataGridViewColumnSortMode.NotSortable;

            col = this.Columns[8];
            col.HeaderText = "Дата";
            col.Width = (int)columnWidth[8];
            //col.SortMode = DataGridViewColumnSortMode.NotSortable;

            col = this.Columns[9];
            col.HeaderText = "Число";
            col.Width = (int)columnWidth[9];
            //col.SortMode = DataGridViewColumnSortMode.NotSortable;
        }
        public void SetFontFamilyList(FontFamily[] FFamilyList, Font[] iconFonts, ExFontList exFontList)
        {
            this.fontFamilyList = FFamilyList;
            this.iconFonts = iconFonts;
            this.exFontList = exFontList;
        }
        void CenterWellOnMap(int RowIndex)
        {
            if (RowIndex > -1)
            {
                int ind = (int)this.Rows[RowIndex].Cells[0].Value - 1;
                ind = (int)this.table.Rows[ind].ItemArray[5];
                if ((ind > -1) && (ind < layer.ObjectsList.Count))
                {
                    PhantomWell pw = (PhantomWell)layer.ObjectsList[ind];
                    if (RowIndex < layer.ObjectsList.Count)
                    {
                        wellListSettings.SetCenterPhantomWell(pw.srcWell.OilFieldIndex, pw.srcWell.Index, pw.srcWell.ProjectDest > 0);
                    }
                }
            }
        }

        public void SetProgressValue(int Value)
        {
            if (Value < 0) Value = 0;
            if (Value > 100) Value = 100;
            progress.Value = Value;
        }
        public void RemoveSelectedWells()
        {
            if (this.Rows.Count > 0)
            {
                if (this.SelectedRows.Count > 0)
                {
                    int i, ind, k;
                    DataGridViewRow row;
                    List<int> indexes = new List<int>();
                    bool remove = false;
                    k = 5;
                    for (i = 0; i < this.Rows.Count; i++)
                    {
                        if (this.Rows[i].Selected)
                        {
                            row = this.Rows[i];
                            ind = (int)row.Cells[k].Value;
                            indexes.Add((int)row.Cells[k].Value);
                            layer.ObjectsList[ind] = null;
                            row.Cells[k].Value = -1;
                            remove = true;
                        }
                    }
                    i = 0;
                    while (i < layer.ObjectsList.Count)
                    {
                        if (layer.ObjectsList[i] == null)
                        {
                            layer.ObjectsList.RemoveAt(i);
                        }
                        else
                            i++;
                    }
                    RemoveByPHWellIndex();
                    if (remove) ResetPHWellIndex(indexes);
                }
                else
                {
                    MessageBox.Show("Выберите в таблице удаляемые скважины.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            } 
        }
        public void RemoveByPHWellIndex()
        {
            int i = 0;
            ClearDataSource(false);
            while (i < this.table.Rows.Count)
            {
                if ((int)table.Rows[i].ItemArray[5] == -1)
                {
                    this.table.Rows.RemoveAt(i);
                }
                else
                    i++;
            }
            SetDataSource();
        }
        public void ResetPHWellIndex(List<int> indexes)
        {
            int i = 0, j;
            DataGridViewRow row;
            indexes.Sort();
            int value;
            for (i = 0; i < indexes.Count; i++)
            {
                for (j = 0; j < this.Rows.Count; j++)
                {
                    row = this.Rows[j];
                    value = (int)row.Cells[5].Value;
                    if (value > indexes[i] - i)
                    {
                        value--;
                        row.Cells[5].Value = value;
                        row.Cells[0].Value = value + 1;
                    }
                }
 
            }
        }
        public bool EditIconList()
        {
            bool res = false;
            if (this.SelectedRows.Count > 0)
            {
                PhantomWell[] pwList = new PhantomWell[this.SelectedRows.Count];
                int ind, j = 0;
                for (int i = 0; i < this.Rows.Count; i++)
                {
                    if (this.Rows[i].Selected)
                    {
                        ind = (int)this.Rows[i].Cells[0].Value - 1;
                        ind = (int)this.table.Rows[ind].ItemArray[5];
                        pwList[j] = (PhantomWell)layer.ObjectsList[ind];
                        j++;
                    }
                }
                res = ShowIconList(pwList);
                this.Focus();
                if (res) this.Invalidate();
            }
            else
            {
                MessageBox.Show("Выберите скважины в таблице для редактирования значков.", 
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return res;
        }
        bool ShowIconList(PhantomWell[] pwList)
        {
            ArrayList icon;
            WellIcon AllIcon = null, wellIcon;
            WellIconCode icode;
            PhantomWell pw;
            int i, j, k;
            WellIconCode newCode;
            bool OneIconByAllObj = true;

            for (i = 0; (i < pwList.Length) && (OneIconByAllObj); i++)
            {
                pw = pwList[i];
                if ((pw.icons != null) && (pw.icons.Count > 0))
                {
                    for (j = 0; j < pw.icons.Count; j++)
                    {
                        if (pw.icons[j].StratumCode != -1)
                        {
                            if (AllIcon == null)
                            {
                                AllIcon = new WellIcon(pw.icons[j].X, pw.icons[j].Y, pw.icons[j].StratumCode);
                                for (k = 0; k < pw.icons[j].Count; k++)
                                {
                                    newCode = pw.icons[j][k];
                                    AllIcon.AddIconCode(newCode);
                                }
                            }
                            else if (!AllIcon.EqualsIcon(pw.icons[j]))
                            {
                                OneIconByAllObj = false;
                                break;
                            }
                        }
                    }
                    if (!OneIconByAllObj) break;
                }
            }
            icon = new ArrayList();
            if (OneIconByAllObj)
            {
                if ((AllIcon != null) && (AllIcon.Count > 0))
                {
                    AllIcon.SetIconFonts(iconFonts, exFontList);
                    for (i = 0; i < AllIcon.Count; i++)
                    {
                        icode.CharCode = AllIcon[i].CharCode;
                        icode.FontCode = AllIcon.GetIconFontCode(AllIcon[i].FontCode);
                        icode.IconColor = AllIcon[i].IconColor;
                        icode.IconColorDefault = AllIcon[i].IconColor;
                        icode.Size = AllIcon[i].Size;
                        icon.Add(icode);
                    }
                    icon.Add(true);
                }
            }
            else
            {
                icon.Add(true);
            }
            bool res = false, add;
            //if (icon.Count > 0)
            //{
                using (IconListForm fIconList = new IconListForm(icon, true))
                {
                    if (fIconList.ShowDialog() == DialogResult.OK)
                    {
                        ArrayList list = (ArrayList)fIconList.iconList;
                        if (list.Count > 0)
                        {
                            for (i = 0; i < pwList.Length; i++)
                            {
                                add = false;
                                pw = pwList[i];
                                if (pw.icons != null)
                                {
                                    for (j = 0; j < pw.icons.Count; j++)
                                    {
                                        if (pw.icons[j].StratumCode != -1)
                                        {
                                            wellIcon = pw.icons[j];
                                            wellIcon.ClearIconList();
                                            add = true;
                                            for (k = 0; k < list.Count; k++)
                                                wellIcon.AddIconCode((WellIconCode)list[k]);
                                        }
                                    }
                                    if ((!add) && (pw.icons.Count == 1) && (pw.icons[0].StratumCode == -1))
                                    {
                                        wellIcon = new WellIcon(pw.icons[0].X, pw.icons[0].Y, 0);
                                        wellIcon.SetIconFonts(iconFonts, exFontList);
                                        //wellIcon.SetIconFonts(layer.get
                                        for (k = 0; k < list.Count; k++)
                                            wellIcon.AddIconCode((WellIconCode)list[k]);
                                        pw.icons.AddIcon(wellIcon);
                                    }
                                    pw.icons.SetActiveAllIcon();
                                    pw.SetBrushFromIcon();
                                }
                            }
                            res = true;
                        }
                    }
                }
            //}
            return res;
        }

        void DrawWaitIcon(Graphics grfx)
        {
            if (DrawWaitIconMode)
            {
                Size sz = this.ClientRectangle.Size;
                string str = "Идет загрузка. Пожалуйста подождите...";
                SizeF sz2 = grfx.MeasureString(str, this.Font, PointF.Empty, StringFormat.GenericDefault);
                grfx.FillRectangle(Brushes.White, sz.Width / 2 - sz2.Width / 2, sz.Height / 2 - sz2.Height / 2, sz2.Width, sz2.Height);
                grfx.DrawRectangle(Pens.Black, sz.Width / 2 - sz2.Width / 2, sz.Height / 2 - sz2.Height / 2, sz2.Width, sz2.Height);
                grfx.DrawString(str, this.Font, Brushes.Black, sz.Width / 2 - sz2.Width / 2, sz.Height / 2 - sz2.Height / 2);
            }
        }
        void DataGridWellList_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            DrawWaitIcon(grfx);
        }
    }

    sealed class WellListSettings
    {
        private MainForm mainForm;
        C2DLayer layer;
        bool LayerDataChanged;
        public SplitContainer splContainer;
        DataGridWellList dgvWellList;
        ProblemWellList gridProblem;
        ToolStrip tsButtons;
        public bool DrawWaitMode
        {
            get { return dgvWellList.DrawWaitIconMode; }
            set 
            { 
                dgvWellList.DrawWaitIconMode = value;
                dgvWellList.Invalidate();
            }
        }
        public bool DrawProblemWaitMode
        {
            get { return gridProblem.DrawWaitMode; }
            set
            {
                gridProblem.DrawWaitMode = value;
                gridProblem.Invalidate();
            }
 
        }

        public bool Visible { get { return mainForm.pWellListSettings.Visible; } }
        public bool LayerFixed { get { return (layer != null); } }

        public WellListSettings(MainForm MainForm)
        {
            this.mainForm = MainForm;
            tsButtons = mainForm.tsMenuWellListSettings;
            splContainer = new SplitContainer();
            splContainer.Parent = mainForm.pWellListSettings;
            splContainer.Location = new Point(1, tsButtons.Height + 1);
            splContainer.Orientation = Orientation.Horizontal;
            Size sz = new Size();
            sz.Height = mainForm.pWellListSettings.ClientRectangle.Height - splContainer.Location.Y;
            sz.Width = mainForm.pWellListSettings.ClientRectangle.Width - splContainer.Location.X;
            splContainer.Size = sz;
            splContainer.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            splContainer.Panel2Collapsed = true;
            splContainer.FixedPanel = FixedPanel.Panel2;
            splContainer.SplitterDistance = (int)(sz.Height / 3);
            LayerDataChanged = false;
            mainForm.WellListSetWindowText("Список скважин");

            dgvWellList = new DataGridWellList(this, splContainer.Panel1);
            gridProblem = new ProblemWellList(this, splContainer.Panel2);
            dgvWellList.SetFontFamilyList(mainForm.canv.GetFontFamilyList(), mainForm.canv.GetIconFontList(), mainForm.canv.GetExFontList());
            mainForm.tsMenuWellListSettings.SizeChanged += new EventHandler(tsMenuWellListSettings_SizeChanged);
            dgvWellList.OnFindWellsComplete += new OnFindWellsCompleteDelegate(dgvWellList_OnFindWellsComplete);
        }

        void dgvWellList_OnFindWellsComplete()
        {
            if (dgvWellList.FindWellIndex.Count == 0 && mainForm.WL_FinderEx.TextBox.Text.Length > 0)
            {
                mainForm.WL_FinderEx.TextBox.BackColor = Color.Pink;
            }
            else
            {
                mainForm.WL_FinderEx.TextBox.BackColor = Color.White;
            }
        }

        void tsMenuWellListSettings_SizeChanged(object sender, EventArgs e)
        {
            ToolStrip ts = (ToolStrip)sender;
            if (ts.Items.Count <= 10) return;
            ToolStripItem box = ts.Items[10];
            int sumW = 0, lastW;
            bool hidden = false;
            ts.Items[9].Overflow = ToolStripItemOverflow.AsNeeded;
            
            box.Overflow = ToolStripItemOverflow.AsNeeded;
            for (int i = 0; i < ts.Items.Count - 1; i++)
            {
                if (!ts.Items[i].Visible) hidden = true;
                if (i != 10)
                {
                    sumW += ts.Items[i].Bounds.Width;
                }
            }
            sumW += 12;
            lastW = ts.Items[ts.Items.Count - 1].Bounds.Width;
            if (sumW + 30 < ts.ClientRectangle.Width)
            {
                box.Width = 30;
            }
            if (sumW + lastW + 30 < ts.ClientRectangle.Width)
            {
                box.Width = ts.ClientRectangle.Width - (sumW + lastW);
                ts.Items[9].Overflow = ToolStripItemOverflow.Never;
                box.Overflow = ToolStripItemOverflow.Never;
            }
            else if (sumW < ts.ClientRectangle.Width - 30 - ((hidden) ? 12 : 0))
            {
                ts.Items[9].Overflow = ToolStripItemOverflow.Never;
                box.Width = ts.ClientRectangle.Width - sumW - ((hidden) ? 12 : 0);
                box.Overflow = ToolStripItemOverflow.Never;
            }
            ts.Update();
        }

        #region COPY / PASTE CLIPBOARD
        private string[] ParseClipboardString(string ClipboardText)
        {
            string[] retValue;
            string[] rowSep = { "\r\n" };
            retValue = ClipboardText.Split(rowSep, StringSplitOptions.RemoveEmptyEntries);
            return retValue;
        }
        private void PasteClipboard()
        {
            int i;
            int wellIndex, OFIndex, OFAreaCode, projWellIndex;
            char[] sep = { '\t' };
            string[] parseStr;
            string skvName, OFName, OFAreaName;
            List<PhantomWell> CorrList = new List<PhantomWell>();
            List<string> ProblemList = new List<string>();
            DataGridViewRowCollection _rows = dgvWellList.Rows;

            Project MapProject = mainForm.canv.MapProject;
            string[] strList;
            string copyText = Clipboard.GetText();
            bool CopyMiRWellList = false;

            strList = ParseClipboardString(copyText);
            var dictOFArea = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            
            i = 0;
            OilField of;
            if (strList.Length > 1)
            {
                parseStr = strList[0].Split(sep, StringSplitOptions.RemoveEmptyEntries);
                if ((parseStr.Length > 3) &&
                    (parseStr[0] == "№") &&
                    (parseStr[1] == "Скважина") &&
                    (parseStr[2] == "Месторождение") &&
                    (parseStr[3] == "Площадь"))
                {
                    CopyMiRWellList = true;
                }
            }
            if (CopyMiRWellList)
            {
                for (i = 1; i < strList.Length; i++)
                {
                    parseStr = strList[i].Split(sep);
                    wellIndex = -1;
                    projWellIndex = -1;
                    skvName = parseStr[1].ToUpper();
                    OFName = parseStr[2].ToUpper();
                    OFAreaName = parseStr[3];
                    OFAreaCode = dictOFArea.GetCodeByShortName(OFAreaName);
                    OFIndex = MapProject.GetOFIndex(OFName);
                    of = null;
                    if ((OFIndex != -1) && (OFAreaCode > 0))
                    {
                        of = (OilField)MapProject.OilFields[OFIndex];
                        wellIndex = of.GetWellIndex(skvName, OFAreaCode);
                    }
                    else if ((OFIndex != -1) && (OFAreaName.Length == 0))
                    {
                        of = (OilField)MapProject.OilFields[OFIndex];
                        wellIndex = of.GetWellIndex(skvName);
                        projWellIndex = of.GetProjWellIndex(skvName);
                    }

                    if ((of != null) && (wellIndex != -1 || projWellIndex != -1))
                    {
                        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                        nfi.NumberDecimalSeparator = ".";
                        nfi.NumberGroupSeparator = " ";
                        PhantomWell pw = null;
                        if (wellIndex != -1)
                        {
                            pw = new PhantomWell(of.OilFieldCode, of.Wells[wellIndex]);
                        }
                        else if (projWellIndex != -1)
                        {
                            pw = new PhantomWell(of.OilFieldCode, of.ProjWells[projWellIndex]);
                        }
                        string text = parseStr[4];
                        DateTime dt = DateTime.MinValue;
                        Double number = Constant.DOUBLE_NAN;
                        bool parsingDate = DateTime.TryParse(parseStr[5], out dt);
                        if (parseStr[6].Length > 0) parseStr[6] = parseStr[6].Replace(',', '.');
                        bool parsingDouble = Double.TryParse(parseStr[6], System.Globalization.NumberStyles.Float, nfi, out number);
                        if (text.Length > 0 || parsingDate || parsingDouble)
                        {
                            pw.Caption = new PhantomWellCaption();
                            pw.Caption.Text = text;
                            if(parsingDate) pw.Caption.Date = dt;
                            if(parsingDouble) pw.Caption.Value = number;
                        }
                        CorrList.Add(pw);
                    }
                    else
                    {
                        ProblemList.Add(strList[i]);
                    }
                }
            }
            else
            {
                for (i = 0; i < strList.Length; i++)
                {
                    ProblemList.Add(strList[i]);
                }
            }
            if (CorrList.Count > 0)
            {
                AddWellList(CorrList, ProblemList.Count);
                SetWellCount(dgvWellList.Rows.Count);
            }

            if (ProblemList.Count > 0)
            {
                gridProblem.FillTable(ProblemList);
                if (gridProblem.Rows.Count > 0)
                {
                    splContainer.Panel2Collapsed = false;
                    string text = "Не удалось автоматическое распознавание списка.\nОтредактируйте список вручную.";
                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Информация", text);
                }
            }
            else
            {
                splContainer.Panel2Collapsed = true;
            }
        }
        #endregion

        // Find Wells
        public void FindWells(string WellName)
        {
            dgvWellList.StartFindWell(WellName);
        }
        public void KeyPress(Keys key)
        {
            dgvWellList.KeyPress(key);
        }

        // WellList Progress
        public void SetProgressValue(int Value)
        {
            dgvWellList.SetProgressValue(Value);
        }

        public void SelectWell(Well w)
        {
            if (layer != null && this.Visible && dgvWellList.Rows.Count > 0)
            {
                dgvWellList.SelectWell(w);
            }
        }
        public void AppendLog(string text)
        {
            mainForm.reLog.AppendText(text + Environment.NewLine);
        }
        public void ShowToolTip(string text)
        {
            ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Информация", text); 
        }
        public void SetWorkFolderChanged(bool Changed)
        {
            mainForm.canv.LayerWorkListChanged = Changed;
        }
        void SaveProblemWellList()
        {
            if (this.layer != null)
            {
                int len = gridProblem.Rows.Count;
                if (len > 0)
                {
                    string[] list = new string[len + 1];
                    string str;
                    list[0] = gridProblem.GetDataTypesString();
                    for (int i = 0; i < len; i++)
                    {
                        list[i + 1] = "";
                        for (int j = 0; j < gridProblem.Columns.Count; j++)
                        {
                            str = (string)gridProblem.Rows[i].Cells[j].Value;
                            if (str == "") str = " ";
                            list[i + 1] += str;
                            if (j < gridProblem.Columns.Count - 1) list[i + 1] += "\t";
                        }
                    }
                    this.layer.SetProblemWellList(list);
                }
                else
                    this.layer.SetProblemWellList(null);
            }
        }
        void LoadProblemWellList()
        {
            if (this.layer != null)
            {
                gridProblem.Clear();
                string[] list = this.layer.GetProblemWellList();
                if ((list != null) && (list.Length > 0))
                {
                    char[] sep = new char[] {';'};
                    string[] parse = list[0].Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    if (parse.Length > 0)
                    {
                        ColumnDataType[] DataTypes = new ColumnDataType[parse.Length];
                        for (int i = 0; i < parse.Length; i++)
                        {
                            DataTypes[i] = (ColumnDataType)Convert.ToByte(parse[i]);
                        }
                        List<string> rows = new List<string>(list.Length - 1);
                        for (int i = 1; i < list.Length; i++)
                        {
                            rows.Add(list[i]);
                        }
                        if (rows.Count > 0)
                        {
                            gridProblem.FillTable(rows, DataTypes);
                        }
                    }
                    splContainer.Panel2Collapsed = false;
                }
                else
                {
                    splContainer.Panel2Collapsed = true;
                }
            }
        }
        public void TestOneDateGraphic()
        {
            int count = 0;
            if ((this.layer != null) && (layer.ObjectsList.Count > 0))
            {
                PhantomWell pw;
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    pw = (PhantomWell)layer.ObjectsList[i];
                    if ((pw.Caption != null) && (pw.Caption.Date != DateTime.MinValue))
                    {
                        count++;
                    }
                }
                if(count == 0)
                {
                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Ни по одной скважине списка не загружена дата для отображения графика на единую дату.");
                }
                else if (count < layer.ObjectsList.Count)
                {
                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Часть скважин списка не содержит дату для отображения графика на единую дату.");
                }
            }
        }
        public void SetLayerSource(C2DLayer Layer)
        {
            if (this.layer != Layer)
            {
                if (LayerDataChanged)
                {
                    mainForm.canv.WorkLayerActive = Layer;
                    LayerDataChanged = false;
                    mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER);
                }
                if (this.layer != null)
                {
                    if (((C2DLayer)layer).node != null) mainForm.canv.twWork.SetNodeFontBold(((C2DLayer)layer).node, false);
                }
                this.layer = Layer;
                if ((Layer != null) && (Layer.node != null))
                {
                    mainForm.canv.twWork.SelectNodeByType(Constant.BASE_OBJ_TYPES_ID.WELL_LIST, Layer.node);
                }
                dgvWellList.SetLayer(Layer);
                if (layer == null)
                {
                    ClearDataSource(true);
                }
                else
                {
                    gridProblem.TestEqualsMainWells();
                }
                dgvWellList.SetProject(mainForm.canv.MapProject);
                gridProblem.SetProject(mainForm.canv.MapProject);
                UpdateTitle();
            }
        }
        public void UpdateTitle()
        {
            if (layer != null)
                mainForm.WellListSetWindowText("Список скважин [" + layer.Name + "]");
            else
                mainForm.WellListSetWindowText("Список скважин");
        }
        public void Clear()
        {
            dgvWellList.table.Rows.Clear();
            mainForm.WL_FinderEx.TextBox.Clear();
            HideProblemList();
            this.SetLayerSource(null);
            ClearDataSource(true);
            mainForm.UpdateWellListSettingsToolBar();
        }
        public void Focus()
        {
            dgvWellList.Focus();
        }
        public void ShowWellList()
        {
            if ((layer != null) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL))
            {
                PhantomWell pw;
                int countWell = 0, countProblem = 0;
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    if (((PhantomWell)layer.ObjectsList[i]).srcWell != null)
                    {
                        countWell++;
                    }
                    else
                    {
                        countProblem++;
                    }
                }
                dgvWellList.table.Rows.Clear();
                ClearDataSource(true);
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    pw = (PhantomWell)layer.ObjectsList[i];
                    if (pw.srcWell != null)
                    {
                        AddWellToTable(i, pw, false);
                    }
                    //else
                    //{
                    //    AddProblemWell(pw.Name, "", "");
                    //}
                }
                SetDataSource();
                if (mainForm.WL_FinderEx.TextBox.Text.Length > 0)
                {
                    dgvWellList.StartFindWell(mainForm.WL_FinderEx.TextBox.Text);
                }
                //if(dgvProblemWellList.Rows.Count > 0) splContainer.Panel2Collapsed = false;
            }
            SetWellCount(dgvWellList.Rows.Count);
        }
        public void HideProblemList()
        {
            if (layer != null) layer.SetProblemWellList(null);
            splContainer.Panel2Collapsed = true;
        }
        public void SetWellCount(int WellCount)
        {
            ToolStripLabel label = (ToolStripLabel)tsButtons.Items[tsButtons.Items.Count - 1];
            label.Text = "N скв: " + WellCount.ToString();
        }
        public void SetCenterPhantomWell(int OFIndex, int WellIndex, bool IsProject)
        {
            mainForm.canv.SetCenterSkv(OFIndex, WellIndex, IsProject, true);
        }
        public void ClearDataSource(bool Update)
        {
            dgvWellList.ClearDataSource(Update);
            //dgvProblemWellList.ClearDataSource(Update);
        }
        public void SetDataSource()
        {
            dgvWellList.SetDataSource();

            //dgvProblemWellList.SetDataSource();
        }
        public void AddWellToTable(int index, PhantomWell pw, bool SelectRow)
        {
            var dictArea = (OilFieldAreaDictionary)mainForm.GetLastProject().DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            if (index > -1)
            {
                DataRow row = dgvWellList.table.NewRow();
                row[0] = dgvWellList.table.Rows.Count + 1;
                row[2] = pw.srcWell.UpperCaseName;
                row[3] = mainForm.canv.MapProject.OilFields[pw.srcWell.OilFieldIndex].Name;
                row[4] = dictArea.GetShortNameByCode(pw.srcWell.OilFieldAreaCode);
                row[5] = index;
                if (pw.Caption != null)
                {
                    row[7] = pw.Caption.Text;
                    if (pw.Caption.Date != DateTime.MinValue) row[8] = pw.Caption.Date;
                    if (pw.Caption.Value != Constant.DOUBLE_NAN) row[9] = pw.Caption.Value;
                }
                dgvWellList.table.Rows.Add(row);
            }
        }
        public void AddProblemWell(string skvName, string OilFieldName, string OilFieldAreaName)
        {
            //DataRow row = dgvProblemWellList.table.NewRow();
            //row[0] = dgvProblemWellList.table.Rows.Count;
            //row[2] = skvName;
            //row[3] = OilFieldName;

            //row[4] = "";
            //row[5] = 0;
            //row[6] = OilFieldAreaName;

            //if (OilFieldAreaName != "")
            //{
            //    char[] sep = { ';' };
            //    string[] parseStr = OilFieldAreaName.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            //    int[] ofAreaIndList = null;
            //    for (int i = 0; i < parseStr.Length; i++)
            //    {
            //        if (ofAreaIndList == null) ofAreaIndList = new int[parseStr.Length];
            //        ofAreaIndList[i] = Convert.ToInt32(parseStr[i]);
            //    }
            //    row[4] = "";
            //    row[6] = ofAreaIndList;
            //}
            //else
            //{
            //    row[4] = OilFieldAreaName;
            //    row[6] = null;
            //}
            
            //dgvProblemWellList.table.Rows.Add(row);
        }
        public void UpdateOilFieldAliases(OilField of)
        {
            mainForm.canv.UpdateOilFieldAliases(of);
        }
        
        public void AddStatUsageMessage(CollectorStatId id)
        {
            // mainForm.StatUsage.AddMessage(id);
        }
        void DisplayAddingMessage(int CountAllAdding, int CountAdding, int CountProblemWell)
        {
            if (CountAdding > 0)
            {
                layer.GetObjectsRect();
                layer.ResetFontsPens();
                mainForm.canv.SetLayerFontsRecursive(layer);
                LayerDataChanged = true;
                SetWellCount(dgvWellList.Rows.Count);
                int diffCount = CountAllAdding - CountAdding;
                string s = Ending.GetWellEnding(CountAdding);
                if (s == "") s = "о";
                string text = "В список скважин '" + layer.Name + "' загружен" + s + " " + CountAdding.ToString() + " скважин" + Ending.GetWellEnding(CountAdding) + ".";
                if (diffCount > 0)
                {
                    string s2 = "ет";
                    if (diffCount > 1)
                    {
                        if ((diffCount > 19) && (diffCount % 10 == 1))
                            s2 = "ет";
                        else
                            s2 = "ют";
                    }
                    text += "\n" + diffCount.ToString() + " скважин" + Ending.GetWellEnding(diffCount) + " уже присутву" + s2 + " в списке.";
                }
                if (CountProblemWell > 0)
                {
                    s = Ending.GetWellEnding(CountProblemWell);
                    if (s == "") s = "о";
                    text += "\n" + CountProblemWell.ToString() + " скважин" + Ending.GetWellEnding(CountProblemWell) + " не найден" + s + ".";
                }
                ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Информация", text);
            }
            else if ((CountAllAdding > 0) && (layer.ObjectsList.Count > 0))
            {
                string text = "Добавляемые скважины уже присутсвуют в списке скважин '" + layer.Name + "'";
                ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Информация", text);
            }
        }
        void AddWellList(Well[] wellList, int CountProblemWell)
        {
            int i, j, len, ind, firstAddedIndex = -1;
            PhantomWell pw;
            len = wellList.Length;
            Well w;
            int countAdd = 0;
            bool clearSelected = true;
            ClearDataSource(false);
            for (i = 0; i < len; i++)
            {
                w = wellList[i];
                if (layer.GetIndexByWell(w.OilFieldIndex, w.Index) == -1)
                {
                    pw = new PhantomWell(((OilField)mainForm.canv.MapProject.OilFields[w.OilFieldIndex]).OilFieldCode, w);
                    if (pw.icons != null)
                    {
                        ind = layer.ObjectsList.Count;
                        layer.ObjectsList.Add(pw);
                        if (clearSelected)
                        {
                            clearSelected = false;
                            while (dgvWellList.SelectedRows.Count > 0)
                            {
                                dgvWellList.SelectedRows[0].Selected = false;
                            }
                        }
                        AddWellToTable(ind, pw, true);
                        if (firstAddedIndex == -1) firstAddedIndex = ind;
                        countAdd++;
                    }
                }
            }
            SetDataSource();
            if ((dgvWellList.ClientRectangle.Height > 20) && (firstAddedIndex != -1) && (dgvWellList.Rows.Count > firstAddedIndex))
            {
                dgvWellList.FirstDisplayedScrollingRowIndex = firstAddedIndex;
            }
            DisplayAddingMessage(wellList.Length, countAdd, CountProblemWell);
        }
        void AddWellList(List<PhantomWell> wellList, int CountProblemWell)
        {
            int i, ind, firstAddedIndex = -1;
            int countAdd = 0;
            bool clearSelected = true;
            ClearDataSource(false);
            for (i = 0; i < wellList.Count; i++)
            {
                if (layer.GetIndexByWell(wellList[i].srcWell.OilFieldIndex, wellList[i].srcWell.Index) == -1)
                {
                    if (wellList[i].icons != null)
                    {
                        ind = layer.ObjectsList.Count;
                        layer.ObjectsList.Add(wellList[i]);
                        if (clearSelected)
                        {
                            clearSelected = false;
                            while (dgvWellList.SelectedRows.Count > 0)
                            {
                                dgvWellList.SelectedRows[0].Selected = false;
                            }
                        }
                        AddWellToTable(ind, wellList[i], true);
                        if (firstAddedIndex == -1) firstAddedIndex = ind;
                        countAdd++;
                    }
                }
            }
            SetDataSource();
            if ((dgvWellList.ClientRectangle.Height > 20) && (firstAddedIndex != -1) && (dgvWellList.Rows.Count > firstAddedIndex))
            {
                dgvWellList.FirstDisplayedScrollingRowIndex = firstAddedIndex;
            }
            DisplayAddingMessage(wellList.Count, countAdd, CountProblemWell);
        }
        public void AddPhantomWellList(List<PhantomWell> wellList)
        {
            int count = dgvWellList.Rows.Count;
            int i, len, ind, firstAddedIndex = -1;
            PhantomWell pw;
            len = wellList.Count;
            int countAdd = 0;
            bool clearSelected = true;
            ClearDataSource(false);
            //bool drawCaption = false;
            for (i = 0; i < len; i++)
            {
                pw = wellList[i];
                if (layer.GetIndexByWell(pw.srcWell.OilFieldIndex, pw.srcWell.Index) == -1)
                {
                    ind = layer.ObjectsList.Count;
                    layer.ObjectsList.Add(pw);
                    if (clearSelected)
                    {
                        clearSelected = false;
                        while (dgvWellList.SelectedRows.Count > 0)
                        {
                            dgvWellList.SelectedRows[0].Selected = false;
                        }
                    }
                    AddWellToTable(ind, pw, true);
                    if (firstAddedIndex == -1) firstAddedIndex = ind;
                    countAdd++;
                }
            }
            layer.DrawCaption = layer.DrawCaption;
            SetDataSource();
            if ((dgvWellList.ClientRectangle.Height > 20) && (firstAddedIndex != -1) && (dgvWellList.Rows.Count > firstAddedIndex))
            {
                dgvWellList.FirstDisplayedScrollingRowIndex = firstAddedIndex;
            }
            if (dgvWellList.Rows.Count != count) mainForm.canv.WriteWorkLayer(this.layer);
            DisplayAddingMessage(wellList.Count, countAdd, 0);
        }

        public bool TestEqualWell(int OilFieldIndex, int WellIndex)
        {
            if (layer != null)
            {
                PhantomWell pw = null;
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    pw = (PhantomWell)layer.ObjectsList[i];
                    if ((pw.srcWell.OilFieldIndex == OilFieldIndex) && (pw.srcWell.Index == WellIndex))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public PhantomWell GetEqualPhantomWell(PhantomWell ProblemWell)
        {
            if (layer != null)
            {
                PhantomWell pw = null;
                for (int i = 0; i < layer.ObjectsList.Count;i++)
                {
                    pw = (PhantomWell)layer.ObjectsList[i];
                    if ((pw.srcWell.OilFieldIndex == ProblemWell.srcWell.OilFieldIndex) &&
                        (pw.srcWell.Index == ProblemWell.srcWell.Index))
                    {
                        return pw;
                    }
                }
            }
            return null;
        }
        public void AddSelectedWellList()
        {
            if (mainForm.canv.selWellList.Count > 0)
            {
                int count = dgvWellList.Rows.Count;
                Well[] wellList = new Well[mainForm.canv.selWellList.Count];
                for (int i = 0; i < mainForm.canv.selWellList.Count; i++)
                {
                    wellList[i] = (Well)mainForm.canv.selWellList[i];
                }
                AddWellList(wellList, 0);
                SetWellCount(dgvWellList.Rows.Count);
                if (dgvWellList.Rows.Count != count) mainForm.canv.WriteWorkLayer(this.layer);
            }
            else
            {
                MessageBox.Show("Выберите скважины на карте для добавления в список.", "Внимание!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        public void AddCntrWellList(bool UseProjectWells)
        {
            int count = dgvWellList.Rows.Count;
            Well[] wellList = mainForm.canv.GetWellsByNewContour(UseProjectWells);
            if (wellList != null)
            {
                AddWellList(wellList, 0);
                SetWellCount(dgvWellList.Rows.Count);
            }
            else
            {
                MessageBox.Show("Задайте контур на карте содержащий нужные скважины.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (dgvWellList.Rows.Count != count) mainForm.canv.WriteWorkLayer(this.layer);
        }
        public void AddClipboardWellList()
        {
            int count = dgvWellList.Rows.Count;
            // добавляем скважины из буфера обмена
            if (layer != null)
            {
                PasteClipboard();
            }
            if (dgvWellList.Rows.Count != count) mainForm.canv.WriteWorkLayer(this.layer);
        }
        public void DrawGraphicsWellList(bool IsOneDate)
        {
            mainForm.canv.SetSelectedObj(this.layer);
            if (IsOneDate) TestOneDateGraphic();
            mainForm.canv.SelectWellsByPhantom(IsOneDate);
        }
        public void RemoveSelectedWells(bool ShowMessageBox)
        {
            int count = dgvWellList.Rows.Count;
            if (!ShowMessageBox || (count > 0 && MessageBox.Show("Удалить выбранные скважины из списка?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                dgvWellList.RemoveSelectedWells();
                SetWellCount(dgvWellList.Rows.Count);
                gridProblem.TestEqualsMainWells();
                mainForm.canv.DrawLayerList();
                if (dgvWellList.Rows.Count != count) mainForm.canv.WriteWorkLayer(this.layer);
            }
        }
        public void EditIconList()
        {
            if (dgvWellList.EditIconList())
            {
                mainForm.canv.WriteWorkLayer(this.layer);
                mainForm.canv.DrawLayerList();
            }
        }
        public void UpdateMap()
        {
            mainForm.canv.DrawLayerList();
        }

        public void ShowAdvWellListSettings()
        {
            mainForm.canv.WorkObjectSettingsShow(this.layer);
        }
    }
}
