using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace SmartPlus
{
	public class HttpFileReciever
	{
		static byte[] buff;
		
		static char[] chars = new char[] { ' ' };
		static string[] replaces = new string[] { "%20" };
		HttpWebRequest request = null;
		HttpWebResponse response = null;
		NetworkCredential nwCredential = null;
		
		static HttpFileReciever()
		{
			buff = new byte[1024*1024];
		}

		public HttpFileReciever() 
		{
			// обработчик сертификатов принимающий все сертификаты
			ServicePointManager.ServerCertificateValidationCallback += delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{
				return true;
			};
		}

		public static string CyrillicUrlEncode(string CyrillicUrl)
		{
			//return HttpUtility.UrlEncode(CyrillicUrl, Encoding.UTF8);
			StringBuilder sb = new StringBuilder(CyrillicUrl.Length*3);
			sb.Append(CyrillicUrl);
			for(int i = 0; i < sb.Length; ++i)
			{
				for(int j = 0; j < chars.Length; ++j)
				{
					if(sb[i] == chars[j])
					{
						sb.Remove(i, 1);
						sb.Insert(i, replaces[j]);
						i += 2; 
						break;
					}
				}
			}
			return sb.ToString();
		}

		public DateTime GetLastModifiedDate(string FileUrl, string UserName, string Password)
		{
            DateTime result = DateTime.MinValue;
            Uri newUri = new Uri(FileUrl); //Snk: вроде для escape надо new Uri(Uri.EscapeUriString(URL)); 
			request = (HttpWebRequest)WebRequest.Create(newUri);
            
			// Adding Autentification
            if (UserName.Length > 0 || Password.Length > 0)
            {
                nwCredential = new NetworkCredential(UserName, Password);
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;
                request.PreAuthenticate = true;
                request.Credentials = nwCredential;
            }
			
			try
			{
                response = (HttpWebResponse)request.GetResponse();
                result = response.LastModified;

                
                response.Close();
            }
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

			return result;
		}

		public Stream RecieveData(string FileUrl, string UserName, string Password)
		{
			Stream result = null;

			Uri newUri = new Uri(FileUrl);
			request = (HttpWebRequest)WebRequest.Create(newUri);
			
			// Adding Autentification
			if(!string.IsNullOrEmpty(UserName) || !string.IsNullOrEmpty(Password))
			{
				nwCredential = new NetworkCredential(UserName, Password);
				request.PreAuthenticate = true;
				request.Credentials = nwCredential;
			}
			
			try
			{
                response = (HttpWebResponse)request.GetResponse();
				result = response.GetResponseStream();
			}
			catch (Exception ex)
			{
				 throw new Exception(ex.Message);
			}

			return result;
		}

        public static void CopyStream(Stream Input, Stream Output)
		{
			int sizeBuff = buff.Length;
			int count = Input.Read(buff, 0, sizeBuff);
			while(count > 0)
			{
				Output.Write(buff, 0, count);
				count = Input.Read(buff, 0, sizeBuff);
			}

			Output.Flush();
			Output.Seek(0, SeekOrigin.Begin);
		}

        public bool CheckServerNamePassword(string FileUrl, string UserName, string Password)
        {
            bool result;

            Uri newUri = new Uri(FileUrl);
            request = (HttpWebRequest)WebRequest.Create(newUri);

            // Adding Autentification
            if (!string.IsNullOrEmpty(UserName) || !string.IsNullOrEmpty(Password))
            {
                nwCredential = new NetworkCredential(UserName, Password);
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;
                request.PreAuthenticate = true;
                request.Credentials = nwCredential;
            }
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                
                //result = response.IsMutuallyAuthenticated;
                if (response.GetResponseStream() != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                response.Close();
            }
            catch (Exception ex)
            {
                result = false;
                //throw new Exception(ex.Message);
            }
            
            return result;
        }           
	}
}