﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Collections;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.Network
{
    class Discuss
    {
        MainForm mainForm;
        FileServer fileServer;
        InviteServer inviteServer;

        SplitContainer splitPanel, splitPanel2;
        TabControl tabControl;
        TreeView UsersTreeView;
        ListView ThemesListView;

        bool UserIdInited;
        BaseObj SetObject;
        int LastThemesCount;
        bool HidedTabPages;
        RichTextBox MessagesBox;
        TextBox InputTextBox;
        Graphics InputGrfx = null;
        ImageList onlineImages;
        Timer tmrUpdateOnlineTheme, tmrUpdateMessages;
        Font BoldFont, ValueFont;
        ContextMenuStrip contextMenu, contextUserNode;
        NetClient client;
        int InviteThemeId = -1;
        Panel leftPanel, topPanel, bottomPanel;
        string topTitle, topText, leftTitle, leftText, bottomTitle, bottomText;
        bool InputBoxVisible
        {
            set
            {
                splitPanel2.Panel2Collapsed = !value;
            }
        }

        public Discuss(MainForm mainForm, FileServer fileServer)
        {
            this.mainForm = mainForm;
            this.fileServer = fileServer;
            inviteServer = new InviteServer(mainForm, false, null, null);
            LastThemesCount = 20;

            mainForm.canv.OnChangeSelectedObject += new OnChangeSelectedObjectDelegate(canv_OnChangeSelectedObject);
            HidedTabPages = false;
            UserIdInited = false;
            SetObject = null;
            BoldFont = new Font("Calibri", 8.25f, FontStyle.Bold);
            ValueFont = new Font("Calibri", 8.25f);

            #region panels
            leftPanel = new Panel();
            leftPanel.BackColor = Color.White;
            leftPanel.Paint += new PaintEventHandler(leftPanel_Paint);
            leftPanel.Dock = DockStyle.Fill;
            leftPanel.Visible = false;

            topPanel = new Panel();
            topPanel.BackColor = Color.White;
            topPanel.Paint += new PaintEventHandler(topPanel_Paint);
            topPanel.Dock = DockStyle.Fill;
            topPanel.Visible = false;

            bottomPanel = new Panel();
            bottomPanel.BackColor = Color.White;
            bottomPanel.Paint += new PaintEventHandler(bottomPanel_Paint);
            bottomPanel.Dock = DockStyle.None;
            bottomPanel.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
            bottomPanel.Visible = false;
            #endregion

            splitPanel = new SplitContainer();
            splitPanel.Orientation = Orientation.Vertical;
            splitPanel.Dock = DockStyle.Fill;
            this.mainForm.SplitDiscussion.Panel1.Controls.Add(splitPanel);

            tabControl = new TabControl();
            tabControl.Dock = DockStyle.Fill;
            tabControl.Alignment = TabAlignment.Left;
            tabControl.SizeMode = TabSizeMode.Fixed;
            
            splitPanel.Panel1.Controls.Add(tabControl);
            splitPanel.SplitterWidth = 3;
            splitPanel.MouseDoubleClick += new MouseEventHandler(splitPanel_MouseDoubleClick);
            tabControl.TabPages.Add("", "Пользователи");
            tabControl.TabPages.Add("", "Темы");
            tabControl.ItemSize = new Size(85, 16);
            tabControl.TabPages[0].BorderStyle = BorderStyle.None;
            tabControl.TabPages[1].BorderStyle = BorderStyle.None;
            tabControl.MouseClick += new MouseEventHandler(TabPages_MouseClick);
            tabControl.SelectedIndex = 1;

            UsersTreeView = new TreeView();
            UsersTreeView.ShowLines = false;
            UsersTreeView.ShowPlusMinus = false;
            UsersTreeView.ShowRootLines = false;
            UsersTreeView.FullRowSelect = true;
            UsersTreeView.Dock = DockStyle.Fill;
            tabControl.TabPages[0].BorderStyle = BorderStyle.None;
            tabControl.TabPages[0].Controls.Add(UsersTreeView);

            onlineImages = new ImageList();
            onlineImages.ColorDepth = ColorDepth.Depth24Bit;
            onlineImages.ImageSize = new System.Drawing.Size(12, 12);
            onlineImages.Images.Add(Properties.Resources.Disable);
            onlineImages.Images.Add(Properties.Resources.Enable16);
            UsersTreeView.ImageList = onlineImages;
            UsersTreeView.ShowNodeToolTips = true;
            UsersTreeView.MouseDown += UsersTreeView_MouseDown;
            UsersTreeView.BeforeSelect += UsersTreeView_BeforeSelect;

            splitPanel2 = new SplitContainer();
            splitPanel2.Dock = DockStyle.Fill;
            splitPanel2.Orientation = Orientation.Horizontal;
            splitPanel.Panel2.Controls.Add(splitPanel2);

            MessagesBox = new RichTextBox();
            MessagesBox.BackColor = Color.White;
            MessagesBox.ReadOnly = true;
            MessagesBox.WordWrap = true;
            MessagesBox.Dock = DockStyle.Fill;
            
            InputTextBox = new TextBox();
            InputTextBox.Multiline = true;
            InputTextBox.Dock = DockStyle.Fill;
            InputTextBox.KeyUp += InputTextBox_KeyUp;
            InputGrfx = bottomPanel.CreateGraphics();
            InputTextBox.KeyPress += new KeyPressEventHandler(InputTextBox_KeyPress);
            //InputTextBox

            splitPanel2.Panel1.Controls.Add(topPanel);
            splitPanel2.Panel1.Controls.Add(MessagesBox);

            splitPanel2.Panel2.Controls.Add(bottomPanel);
            splitPanel2.Panel2.Controls.Add(InputTextBox);
            splitPanel2.SplitterDistance = 3 * splitPanel2.Height / 4;

            ImageList ObjectImgList = new ImageList();
            ObjectImgList.ColorDepth = ColorDepth.Depth24Bit;
            ObjectImgList.Images.Add(Properties.Resources.Zoom_In16);
            ObjectImgList.Images.Add(Properties.Resources.Bashneft16);
            ObjectImgList.Images.Add(Properties.Resources.NGDU);
            ObjectImgList.Images.Add(Properties.Resources.Oilfield);
            ObjectImgList.Images.Add(Properties.Resources.OilfieldArea);
            ObjectImgList.Images.Add(Properties.Resources.Well);


            ThemesListView = new ListView();
            ThemesListView.View = View.Details;
            ThemesListView.Columns.Add("Head");
            
            ThemesListView.HeaderStyle = ColumnHeaderStyle.None;
            ThemesListView.LabelWrap = false;
            
            ThemesListView.ShowGroups = false;
            ThemesListView.ShowItemToolTips = true;
            ThemesListView.FullRowSelect = true;
            ThemesListView.HideSelection = false;
            ThemesListView.Dock = DockStyle.Fill;
            ThemesListView.SmallImageList = ObjectImgList;
            ThemesListView.MouseDoubleClick += ThemesListView_MouseDoubleClick;
            ThemesListView.MouseClick += ThemesListView_MouseClick;


            #region Context Menus
            contextMenu = new ContextMenuStrip();
            //contextMenu.Items.Add("Загрузить список тем", null, GetThemeList_Click);
            contextMenu.Items.Add("Создать новую тему", Properties.Resources.Add_Green16, AddTheme_Click);

            contextUserNode = new ContextMenuStrip();
            contextUserNode.Items.Add("Пригласить пользователя в тему", null, SendInvite_Click);
            #endregion

            leftPanel.ContextMenuStrip = contextMenu;

            tabControl.TabPages[1].Controls.Add(leftPanel);
            //tabControl.TabPages[1].Controls.Add(ThemesTreeView);
            tabControl.TabPages[1].Controls.Add(ThemesListView);
            
            splitPanel.SplitterDistance = splitPanel.Width / 5;

            tmrUpdateOnlineTheme = new Timer();
            tmrUpdateOnlineTheme.Enabled = false;
            tmrUpdateOnlineTheme.Interval = 30000;
            tmrUpdateOnlineTheme.Tick += new EventHandler(tmrUpdateOnlineTheme_Tick);

            tmrUpdateMessages = new Timer();
            tmrUpdateMessages.Enabled = false;
            tmrUpdateMessages.Interval = 3000;
            tmrUpdateMessages.Tick += new EventHandler(tmrUpdateMessages_Tick);

            client = new NetClient(mainForm.ServerName, 1992, true, WorkProgressChanged, WorkComplete);

            leftPanel.AllowDrop = true;
            topPanel.AllowDrop = true;
            bottomPanel.AllowDrop = true;
            InputTextBox.AllowDrop = true;
            UsersTreeView.AllowDrop = true;
            ThemesListView.AllowDrop = true;
            MessagesBox.AllowDrop = true;

            leftPanel.DragEnter += FilesTreeView_DragEnter;
            leftPanel.DragDrop += FilesTreeView_DragDrop;

            topPanel.DragEnter += FilesTreeView_DragEnter;
            topPanel.DragDrop += FilesTreeView_DragDrop;

            bottomPanel.DragEnter += FilesTreeView_DragEnter;
            bottomPanel.DragDrop += FilesTreeView_DragDrop;

            InputTextBox.DragEnter += FilesTreeView_DragEnter;
            InputTextBox.DragDrop += FilesTreeView_DragDrop;

            UsersTreeView.DragEnter += FilesTreeView_DragEnter;
            UsersTreeView.DragDrop += FilesTreeView_DragDrop;

            ThemesListView.DragEnter += FilesTreeView_DragEnter;
            ThemesListView.DragDrop += FilesTreeView_DragDrop;

            MessagesBox.DragEnter += FilesTreeView_DragEnter;
            MessagesBox.DragDrop += FilesTreeView_DragDrop;
        }

        void UsersTreeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (UsersTreeView.SelectedNode != null)
            {
                UsersTreeView.SelectedNode.ContextMenuStrip = null;
            }
        }
        void UsersTreeView_MouseDown(object sender, MouseEventArgs e)
        {
            TreeViewHitTestInfo hit = UsersTreeView.HitTest(e.Location);
            if (hit.Node != null)
            {
                UsersTreeView.SelectedNode = hit.Node;
                if (ThemesListView.SelectedItems.Count > 0 && ThemesListView.SelectedItems[0].Tag != null)
                {
                    UsersTreeView.SelectedNode.ContextMenuStrip = contextUserNode;
                }
            }
        }

        #region Drag Drop
        void FilesTreeView_DragEnter(object sender, DragEventArgs e)
        {
            Control cntrl = (Control)sender;
            if (cntrl.Visible)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
                {
                    e.Effect = DragDropEffects.All;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
        }
        void FilesTreeView_DragDrop(object sender, DragEventArgs e)
        {
            if (mainForm.canv.MapProject == null) return;
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                string[] files = null;

                List<string> extentions = new List<string>(new string[] { ".img", ".cntr", ".prfl", ".pvt", ".mrk", ".wl", ".grid", ".vm" });
                List<string> extentionsOther = new List<string>(new string[] { ".doc", ".docx", ".docm", ".pdf", ".ppt", ".pptx", ".pptm", ".xls", ".xlsx", ".xlsb", ".xlsm" });
                files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                string name, ext;

                //for (int i = 0; i < files.Length; i++)
                //{
                name = Path.GetFileName(files[0]);
                ext = Path.GetExtension(files[0]);
                if (extentions.IndexOf(ext) > -1 || extentionsOther.IndexOf(ext) > -1)
                {
                    if (File.Exists(files[0]))
                    {
                        string oilfieldName = string.Empty, wellName = string.Empty;
                        object[] indexes = PinObjectForm.ShowPinFileForm(mainForm, mainForm.canv.LastSelectObject, files[0]);

                        if (indexes != null)
                        {
                            object[] codes = GetProjectObjectCodes(mainForm.canv.LastSelectObject);
                            fileServer.UploadFile(mainForm.canv.LastSelectObject.TypeID, (byte)codes[0], (byte)codes[1], (ushort)codes[2], (int)codes[3], (string)indexes[0]);
                        }

                    }
                }
                //}
            }
        }
        #endregion

        #region Common
        public void InitModules()
        {
            splitPanel.SplitterDistance = splitPanel.Width / 5;
            ShowTopPanel("Пожалуйста, подождите...", "Идет подключение к серверу.");
            client.InitUserId();
        }
        void SetWindowText()
        {
            if (SetObject == null)
            {
                mainForm.dDiscussWindowText("Обсуждения");
            }
            else
            {
                mainForm.dcpDiscuss.Text = "Обсуждения: " + GetObjectName(SetObject);
                if (ThemesListView.SelectedItems.Count > 0 && ThemesListView.SelectedItems[0].Tag != null)
                {
                    var theme = (NetworkThemeItem)ThemesListView.SelectedItems[0].Tag;
                    mainForm.dcpDiscuss.Text += string.Format(" [{0}]", theme.Name);
                }
            }
        }
        public void UpdateData(BaseObj NewObject, bool ForceUpdate) 
        {
            UpdateData(NewObject, -1, ForceUpdate);
        }
        public void UpdateData(BaseObj NewObject, int InviteThemeId, bool ForceUpdate)
        {
            if (mainForm.canv.MapProject != null && (ForceUpdate || mainForm.pDiscussion.Visible))
            {
                if (NewObject == null)
                {
                    ShowTopPanel("Для загрузки обсуждений", "выберите объект на карте.");
                }
                else
                {
                    if (InviteThemeId > -1)
                    {
                        tabControl.SelectedIndex = 1;
                    }
                    object[] codes = GetProjectObjectCodes(NewObject);
                    byte regionCode = (byte)codes[0];
                    byte ngduCode = (byte)codes[1];
                    ushort oilfieldIndex = (ushort)codes[2];
                    int objIndex = (int)codes[3];

                    if (SetObject == null ||
                        SetObject.TypeID != NewObject.TypeID ||
                        SetObject.Code != NewObject.Code ||
                        SetObject.Index != NewObject.Index)
                    {
                        SetObject = NewObject;
                        codes = GetProjectObjectCodes(SetObject);
                        LastThemesCount = 20;
                        if (!UserIdInited)
                        {
                            InitModules();
                        }
                        else
                        {
                            ShowTopPanel("Пожалуйста, подождите...", "Загрузка списка обсуждений.");
                            client.GetThemeList(SetObject.TypeID, codes, LastThemesCount);
                        }
                    }
                    else if (this.InviteThemeId != InviteThemeId)
                    {
                        client.GetThemeList(SetObject.TypeID, codes, LastThemesCount);
                    }
                    this.InviteThemeId = InviteThemeId;
                }
                SetWindowText();
            }
        }
        public void Clear(bool clearObject)
        {
            UserIdInited = false;
            LastThemesCount = 20;
            InviteThemeId = -1;
            UsersTreeView.Nodes.Clear();
            ThemesListView.Items.Clear();
            ThemesListView.SelectedItems.Clear();
            MessagesBox.Clear();
            ThemesListView.ContextMenuStrip = null;
            client.Clear();
            if (clearObject) SetObject = new BaseObj();
            SetWindowText();

        }
        public object[] GetProjectObjectCodes(BaseObj PinObject)
        {
            object[] result = new object[4];
            OilField of;
            byte regionCode = 0, ngduCode = 0;
            ushort oilfieldIndex = 0;
            int objIndex = -1;
            switch (PinObject.TypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                    regionCode = (byte)PinObject.Code;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.NGDU:
                    regionCode = (byte)PinObject.Code;
                    ngduCode = (byte)PinObject.Index;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    of = (OilField)PinObject;
                    regionCode = (byte)of.ParamsDict.EnterpriseCode;
                    ngduCode = (byte)of.ParamsDict.NGDUCode;
                    oilfieldIndex = (ushort)of.Index;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    Area area = (Area)PinObject;
                    of = (OilField)mainForm.canv.MapProject.OilFields[area.OilFieldIndex];
                    regionCode = (byte)of.ParamsDict.EnterpriseCode;
                    ngduCode = (byte)of.ParamsDict.NGDUCode;
                    oilfieldIndex = (ushort)of.Index;
                    objIndex = area.Index;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    Well well = (Well)PinObject;
                    of = (OilField)mainForm.canv.MapProject.OilFields[well.OilFieldIndex];
                    regionCode = (byte)of.ParamsDict.EnterpriseCode;
                    ngduCode = (byte)of.ParamsDict.NGDUCode;
                    oilfieldIndex = (ushort)of.Index;
                    objIndex = well.Index;
                    break;
            }
            result[0] = regionCode;
            result[1] = ngduCode;
            result[2] = oilfieldIndex;
            result[3] = objIndex;
            return result;
        }
        void canv_OnChangeSelectedObject(BaseObj LastSelectedObject)
        {
            UpdateData(LastSelectedObject, false);
        }
        
        string GetObjectName(BaseObj PinObject)
        {
            object[] codes = GetProjectObjectCodes(PinObject);
            return GetObjectName(PinObject.TypeID, (byte)codes[0], (byte)codes[1], (ushort)codes[2], (int)codes[3]);
        }
        string GetObjectName(Constant.BASE_OBJ_TYPES_ID Type, byte RegionCode, byte NGDUCode, ushort OilfieldIndex, int ObjectIndex)
        {
            Project project = mainForm.canv.MapProject;
            string result = string.Empty;
            switch (Type)
            {
                case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                    if (project.OilFields.Count > 0 && project.OilFields[0].Contours.Count > 0)
                    {
                        for (int g = 0; g < project.OilFields[0].Contours.Count; g++)
                        {
                            if (project.OilFields[0].Contours[g]._ContourType == -1 && project.OilFields[0].Contours[g]._EnterpriseCode == RegionCode)
                            {
                                result = "Регион " + project.OilFields[0].Contours[g].Name;
                                break;
                            }
                        }
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.NGDU:
                    if (project.OilFields.Count > 0 && project.OilFields[0].Contours.Count > 0)
                    {
                        for (int g = 0; g < project.OilFields[0].Contours.Count; g++)
                        {
                            if (project.OilFields[0].Contours[g]._ContourType == -2 && project.OilFields[0].Contours[g]._NGDUCode == NGDUCode)
                            {
                                result = project.OilFields[0].Contours[g].Name;
                                break;
                            }
                        }
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    if (OilfieldIndex > 0 && OilfieldIndex < project.OilFields.Count)
                    {
                        result = "Мест. " + project.OilFields[OilfieldIndex].Name;
                        break;
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    if (OilfieldIndex > 0 && OilfieldIndex < project.OilFields.Count)
                    {
                        if (ObjectIndex > -1 && ObjectIndex < project.OilFields[OilfieldIndex].Areas.Count)
                        {
                            result = "ЯЗ " + project.OilFields[OilfieldIndex].Areas[ObjectIndex].Name;
                        }
                        result += string.Format(" [{0}]", project.OilFields[OilfieldIndex].Name);
                        break;
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    if (OilfieldIndex > 0 && OilfieldIndex < project.OilFields.Count)
                    {
                        if (ObjectIndex > -1 && ObjectIndex < project.OilFields[OilfieldIndex].Wells.Count)
                        {
                            result = "Скв. " + project.OilFields[OilfieldIndex].Wells[ObjectIndex].Name;
                        }
                        result += string.Format(" [{0}]", project.OilFields[OilfieldIndex].Name);
                        break;
                    }
                    break;
            }
            return result;
        }
        #endregion

        #region Panels
        void topPanel_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            grfx.Clear(Color.White);
            SizeF s1 = grfx.MeasureString(topTitle, ValueFont, PointF.Empty, StringFormat.GenericTypographic);
            SizeF s2 = grfx.MeasureString(topText, ValueFont, PointF.Empty, StringFormat.GenericTypographic);
            float x = topPanel.ClientRectangle.Width / 2;
            float y = topPanel.ClientRectangle.Height / 2;
            Brush br = (topTitle.StartsWith("Ошибка")) ? Brushes.Red : Brushes.Black; 
            grfx.DrawString(topTitle, ValueFont, br, x - s1.Width / 2, y - s1.Height);
            grfx.DrawString(topText, ValueFont, br, x - s2.Width / 2, y);
        }
        public void ShowTopPanel(string topTitle, string topText)
        {
            this.topTitle = topTitle;
            this.topText = topText;
            topPanel.Visible = true;
            topPanel.Invalidate();
        }
        public void HideTopPanel()
        {
            topPanel.Visible = false;
        }

        void leftPanel_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            grfx.Clear(Color.White);
            SizeF s1 = grfx.MeasureString(leftTitle, ValueFont, PointF.Empty, StringFormat.GenericTypographic);
            SizeF s2 = grfx.MeasureString(leftText, ValueFont, PointF.Empty, StringFormat.GenericTypographic);
            float x = leftPanel.ClientRectangle.Width / 2;
            float y = leftPanel.ClientRectangle.Height / 2;
            //Brush br = (topTitle.StartsWith("Ошибка")) ? Brushes.Red : Brushes.Black;
            grfx.DrawString(leftTitle, ValueFont, Brushes.Black, x - s1.Width / 2, y - s1.Height);
            grfx.DrawString(leftText, ValueFont, Brushes.Black, x - s2.Width / 2, y);
        }
        public void ShowLeftPanel(string leftTitle, string leftText)
        {
            this.leftTitle = leftTitle;
            this.leftText = leftText;
            leftPanel.Visible = true;
            leftPanel.Invalidate();
        }
        public void HideLeftPanel()
        {
            leftPanel.Visible = false;
        }

        void bottomPanel_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            grfx.Clear(Color.White);
            grfx.DrawString(bottomTitle, ValueFont, Brushes.Black, 0, 0);
            grfx.DrawString(bottomText, ValueFont, Brushes.Black, 0, 0);
        }
        public void ShowBottomPanel(string bottomTitle, string bottomText)
        {
            this.bottomTitle = bottomTitle;
            this.bottomText = bottomText;
            if (InputGrfx == null) InputGrfx = bottomPanel.CreateGraphics();
            SizeF size = InputGrfx.MeasureString(bottomTitle, bottomPanel.Font, PointF.Empty, StringFormat.GenericDefault);
            int x = (int)(bottomPanel.Parent.ClientRectangle.Width / 2.0 - size.Width / 2);
            int y = (int)(bottomPanel.Parent.ClientRectangle.Height / 2.0 - size.Height / 2);
            bottomPanel.Location = new Point(x, y);
            bottomPanel.Size = new Size((int)size.Width, (int)(size.Height * 1.2));
            bottomPanel.Visible = true;
            bottomPanel.Invalidate();
        }
        public void HideBottomPanel()
        {
            bottomPanel.Visible = false;
        }
        #endregion

        #region TabPages
        void TabPages_MouseClick(object sender, MouseEventArgs e)
        {
            if (HidedTabPages)
            {
                HidedTabPages = false;
                splitPanel.SplitterDistance = splitPanel.Width / 5;
            }
        }
        void splitPanel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (splitPanel.SplitterRectangle.Contains(e.Location))
            {
                HidedTabPages = !HidedTabPages;
                if (HidedTabPages)
                {
                    splitPanel.SplitterDistance = 30;
                }
                else
                {
                    splitPanel.SplitterDistance = splitPanel.Width / 5;
                }
            }
        }
        #endregion

        #region User List
        void SendInvite_Click(object sender, EventArgs e)
        {
            if (UsersTreeView.SelectedNode == null || UsersTreeView.SelectedNode.Tag == null) return;

            if (ThemesListView.SelectedItems.Count == 0 || ThemesListView.SelectedItems[0].Tag == null)
            {
                MessageBox.Show("Загрузите тему по которой необходимо отправить приглашение.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            var user = (NetworkUserItem)UsersTreeView.SelectedNode.Tag;
            var theme = (NetworkThemeItem)ThemesListView.SelectedItems[0].Tag;

            if (UsersTreeView.SelectedNode.ImageIndex == 0)
            {
                
                string str = string.Format("Пользователь '{0}' не в сети.\nПродублировать приглашение по электронной почте?", user.ADName);
                DialogResult result = MessageBox.Show(mainForm, str, "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
                if(result == DialogResult.Yes)
                {
                    string currentUserName = GetUserName(client.CurrentUserId);
                    string objName = GetObjectName(SetObject);
                    string Body = string.Format("Пользователь ПК SmartPlus(МиР) {0} приглашает Вас принять участие в обсуждении темы:\n'{1}'\n{2}\nВремя приглашения: {3:dd.MM.yyyy HH:mm}", currentUserName, theme.Name, objName, DateTime.Now);
                    Mapi mapi = new Mapi();
                    mapi.AddRecipientTo(user.ADName);
                    mapi.SendMailPopup("ПК SmartPlus(МиР).Приглашение к обсуждению.", Body);
                }
            }
            
            inviteServer.SendInvite(InviteServer.InviteType.InviteInTheme, user.Id, theme.Id);
        }
        public int Compare(TreeNode nodeX, TreeNode nodeY)
        {
            if (nodeX.Tag != null && nodeY.Tag != null)
            {
                if (nodeX.ImageIndex != nodeY.ImageIndex)
                {
                    return nodeY.ImageIndex - nodeX.ImageIndex;
                }
                else
                {
                    NetworkUserItem user1, user2;
                    user1 = (NetworkUserItem)nodeX.Tag;
                    user2 = (NetworkUserItem)nodeY.Tag;
                    return user1.ADName.CompareTo(user2.ADName);
                }
            }
            return 0;
        }
        void SortUsers()
        {
            UsersTreeView.BeginUpdate();
            TreeNode clone =new TreeNode();
            int compare;
            for (int i = 0; i < UsersTreeView.Nodes.Count - 1; i++)
            {
                for (int j = i + 1; j < UsersTreeView.Nodes.Count; j++)
                {
                    compare = Compare(UsersTreeView.Nodes[i], UsersTreeView.Nodes[j]);

                    if (compare > 0)
                    {
                        clone.Text = UsersTreeView.Nodes[j].Text;
                        clone.ImageIndex = UsersTreeView.Nodes[j].ImageIndex;
                        clone.SelectedImageIndex = UsersTreeView.Nodes[j].SelectedImageIndex;
                        clone.Tag = UsersTreeView.Nodes[j].Tag;
                        clone.ToolTipText = UsersTreeView.Nodes[j].ToolTipText;

                        UsersTreeView.Nodes[j].Text = UsersTreeView.Nodes[i].Text;
                        UsersTreeView.Nodes[j].ImageIndex = UsersTreeView.Nodes[i].ImageIndex;
                        UsersTreeView.Nodes[j].SelectedImageIndex = UsersTreeView.Nodes[i].SelectedImageIndex;
                        UsersTreeView.Nodes[j].Tag = UsersTreeView.Nodes[i].Tag;
                        UsersTreeView.Nodes[j].ToolTipText = UsersTreeView.Nodes[i].ToolTipText;

                        UsersTreeView.Nodes[i].Text = clone.Text;
                        UsersTreeView.Nodes[i].ImageIndex = clone.ImageIndex;
                        UsersTreeView.Nodes[i].SelectedImageIndex = clone.SelectedImageIndex;
                        UsersTreeView.Nodes[i].Tag = clone.Tag;
                        UsersTreeView.Nodes[i].ToolTipText = clone.ToolTipText;
                    }
                }
            }
            UsersTreeView.EndUpdate();
        }
        string GetShortUserName(string Name)
        {
            int pos = Name.IndexOf(" ");
            string shortName = Name;
            if (pos > -1)
            {
                shortName = Name.Remove(pos + 2) + ".";
                pos = Name.IndexOf(" ", pos + 1);
                if (pos > -1)
                {
                    shortName += Name.Remove(0, pos + 1).Remove(1) + ".";
                }
            }
            return shortName;
        }
        public string GetUserName(int UserId)
        {
            for (int i = 0; i < UsersTreeView.Nodes.Count; i++)
            {
                if (UsersTreeView.Nodes[i].Tag != null)
                {
                    var user = (NetworkUserItem)UsersTreeView.Nodes[i].Tag;
                    if (user.Id == UserId)
                    {
                        return user.ADName;
                    }
                }
            }
            return string.Empty;
        }
        void tmrUpdateOnlineTheme_Tick(object sender, EventArgs e)
        {
            if (!client.IsInitialized)
            {
                tmrUpdateOnlineTheme.Stop();
            }
            else
            {
                if (tabControl.SelectedIndex == 0)
                {
                    tmrUpdateOnlineTheme.Interval = 10000;
                    client.GetUserOnlineList();
                }
                else
                {
                    tmrUpdateOnlineTheme.Interval = 60000;
                    //client.GetThemeList();
                }
            }
        }
        #endregion

        #region Themes and Message List
        void AddMoreThemesButton()
        {
            ListViewItem item = new ListViewItem(" -- Загрузить еще 20 тем --", 0);
            item.Tag = null;
            ThemesListView.Items.Add(item);
        }
        void tmrUpdateMessages_Tick(object sender, EventArgs e)
        {
            if (ThemesListView.SelectedItems.Count > 0 && client.IsInitialized)
            {
                var theme = (NetworkThemeItem)ThemesListView.SelectedItems[0].Tag;
                client.GetLastMessages(theme.Id, theme.GetLastId() + 1, 20);
            }
        }
        void AddTheme_Click(object sender, EventArgs e)
        {
            string oilfieldName = string.Empty;
            string wellName = string.Empty;
            if (mainForm != null && mainForm.canv != null && mainForm.canv.MapProject != null)
            {
                if (mainForm.canv.twActiveOilField.ActiveOilFieldIndex != -1)
                {
                    oilfieldName = mainForm.canv.MapProject.OilFields[mainForm.canv.twActiveOilField.ActiveOilFieldIndex].Name;
                }
                if (mainForm.canv.selWellList.Count == 1)
                {
                    var w = (Well)mainForm.canv.selWellList[0];
                    oilfieldName = mainForm.canv.MapProject.OilFields[w.OilFieldIndex].Name;
                    wellName = w.Name;
                }
            }
            object[] indexes = PinObjectForm.ShowPinThemeForm(mainForm, SetObject);
            if (indexes != null)
            {
                object[] codes = GetProjectObjectCodes(SetObject);
                client.AddTheme(SetObject.TypeID, (byte)codes[0], (byte)codes[1], (ushort)codes[2], (int)codes[3], (string)indexes[0]);
            }
        }
        void GetThemeList_Click(object sender, EventArgs e)
        {
            object[] codes = GetProjectObjectCodes(SetObject);
            client.GetThemeList(SetObject.TypeID, codes, LastThemesCount);
        }
        void InputTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (ThemesListView.SelectedItems.Count > 0 && ThemesListView.SelectedItems[0].Tag != null)
                {
                    var theme = (NetworkThemeItem)ThemesListView.SelectedItems[0].Tag;
                    int lastId = -1;
                    if (theme.items.Count > 0) lastId = theme.items[theme.items.Count - 1].Id + 1;
                    client.AddMessage(theme.Id, lastId, InputTextBox.Text);
                    InputTextBox.Text = string.Empty;
                }
                else
                {
                    MessageBox.Show(mainForm, "Для отправки сообщения выберите тему.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                }
                e.Handled = true;
            }
        }
        void InputTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TestInputTextBox();
        }
        void TestInputTextBox()
        {
            if (InputTextBox.Text.Length == 0)
            {
                ShowBottomPanel("Введите сюда текст сообщения и нажмите Enter для его отправки", "");
            }
            else
            {
                HideBottomPanel();
            }
        }

        void CenterMapOnObject(Constant.BASE_OBJ_TYPES_ID Type, byte RegionCode, byte NGDUCode, ushort OilfieldIndex, int ObjectIndex)
        {
            OilField of;
            switch (Type)
            {
                case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                    mainForm.canv.SetCenterProject();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.NGDU:
                    mainForm.canv.SetCenterNGDU(NGDUCode);
                    break;
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    mainForm.canv.SetCenterOilField(OilfieldIndex, false);
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    mainForm.canv.SetCenterArea(OilfieldIndex, ObjectIndex, false);
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    of = mainForm.canv.MapProject.OilFields[OilfieldIndex];
                    Well well = of.Wells[ObjectIndex];
                    mainForm.canv.SetCenterSkv(OilfieldIndex, ObjectIndex, well.ProjectDest > 0, false);
                    break;
            }
        }
        void SelectThemeNode(ListViewItem item, bool CenterMap)
        {
            item.Selected = true;
            if (ThemesListView.SelectedItems.Count > 0 && ThemesListView.SelectedItems[0].Tag != null)
            {
                var theme = (NetworkThemeItem)ThemesListView.SelectedItems[0].Tag;

                if (CenterMap)
                {
                    CenterMapOnObject(theme.ObjectType, theme.RegionCode, theme.NGDUCode, theme.OilfieldIndex, theme.ObjectIndex);
                }
                
                MessagesBox.Clear();
                string user;
                for (int i = 0; i < theme.items.Count; i++)
                {
                    user = GetUserName(theme.items[i].UserId);
                    MessagesBox.SelectionFont = BoldFont;
                    MessagesBox.SelectionColor = Color.Gray;
                    MessagesBox.AppendText(string.Format("{0} [{1:dd.MM.yyyy HH:mm}]\n", (user.Length > 0) ? user : theme.items[i].UserId.ToString(), theme.items[i].Date));
                    MessagesBox.SelectionFont = ValueFont;
                    MessagesBox.SelectionColor = Color.Black;
                    MessagesBox.AppendText(theme.items[i].Text + "\n");
                    MessagesBox.SelectionStart = MessagesBox.TextLength;
                    MessagesBox.ScrollToCaret();
                }
                client.GetLastMessages(theme.Id, theme.GetLastId() + 1, 20);
                if (theme.items.Count == 0) ShowTopPanel("Пожалуйста, подождите...", "Загрузка сообщений с сервера");
                SetWindowText();
            }
        }
        void ThemesListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo hit = ThemesListView.HitTest(e.Location);
            if (hit != null && hit.Item.Tag != null)
            {
                SelectThemeNode(hit.Item, true);
            }
        }
        void ThemesListView_MouseClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo hit = ThemesListView.HitTest(e.Location);
            if (hit != null && hit.Item.Tag == null && hit.Item.Text.StartsWith(" -- "))
            {
                LastThemesCount += 20;
                object[] codes = GetProjectObjectCodes(SetObject);
                client.GetThemeList(SetObject.TypeID, codes, LastThemesCount);
            }
        }
        #endregion 

        public void WorkProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        public void WorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            bool cancelHideTopPanel = false, cancelHideLeftPanel = false, cancelHideBottomPanel = false;
            if (e.Error != null)
            {
                if (e.Error.Message.StartsWith("Не удалось установить подключение к серверу"))
                {
                    Clear(true);
                    ShowTopPanel("Внимание!", "Не удалось установить соединение с сервером");
                    cancelHideTopPanel = true;
                }
                else
                {
                    MessageBox.Show(mainForm, e.Error.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                InputBoxVisible = false;
            }
            else if (e.Cancelled)
            {
                InputBoxVisible = false;
            }
            else if (e.Result != null)
            {
                object[] codes;
                Network.NetPacket packet = (Network.NetPacket)e.Result;
                switch (packet.result)
                {
                    case NetPacket.Result.Timeout:
                        Clear(true);
                        ShowTopPanel("Внимание!", "Таймаут соединения с сервером.");
                        cancelHideTopPanel = true;
                        InputBoxVisible = false;
                        break;
                    case NetPacket.Result.Error:
                        MessageBox.Show(mainForm, "Ошибка обработки запроса на сервере!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        InputBoxVisible = false;
                        break;
                    case SmartPlus.Network.NetPacket.Result.Succes:
                        try
                        {
                            TreeNode node;
                            ListViewItem item;
                            string res = string.Empty;

                            if (packet.type == SmartPlus.Network.NetPacket.Type.GetUserId)
                            {
                                UsersTreeView.Nodes.Clear();
                                ShowTopPanel("Пожалуйста, подождите...", "Инициализация списка пользователей");
                                client.GetUserList();
                                cancelHideTopPanel = true;
                                InputBoxVisible = false;
                                UserIdInited = true;
                            }
                            else if (packet.type == NetPacket.Type.SetOnlineStatus)
                            {
                                cancelHideTopPanel = true;
                                cancelHideLeftPanel = true;
                                cancelHideBottomPanel = true;
                            }
                            if (packet.message != null)
                            {
                                int id;
                                bool online;
                                int count;
                                NetworkThemeItem theme;
                                BinaryReader br = new BinaryReader(packet.message, Encoding.UTF8);
                                switch (packet.type)
                                {
                                    case SmartPlus.Network.NetPacket.Type.GetUserList:
                                        ShowTopPanel("Пожалуйста, подождите...", "Инициализация списка тем");
                                        InputBoxVisible = false;
                                        UsersTreeView.BeginUpdate();
                                        count = br.ReadInt32();
                                        UsersTreeView.Nodes.Clear();
                                        List<NetworkUserItem> users = new List<NetworkUserItem>();
                                        for (int i = 0; i < count; i++)
                                        {
                                            var user = new NetworkUserItem();
                                            user.Id = br.ReadInt32();
                                            user.ADName = br.ReadString();
                                            user.ADJob = br.ReadString();
                                            user.ADOffice = br.ReadString();
                                            online = br.ReadBoolean();
                                            users.Add(user);
                                            if (user.Id == client.CurrentUserId && user.ADOffice.IndexOf("БашНИПИ") > -1)
                                            {
                                                mainForm.dChartSetPIButtonTag(true);
                                            }
                                            node = new TreeNode();
                                            node.Text = GetShortUserName(user.ADName);
                                            node.ImageIndex = online ? 1 : 0;
                                            node.SelectedImageIndex = node.ImageIndex;
                                            node.ToolTipText = string.Format("{0}\n{1}\n{2}", user.ADName, user.ADJob, user.ADOffice);
                                            node.Tag = user;
                                            node.ContextMenuStrip = contextUserNode;
                                            UsersTreeView.Nodes.Add(node);
                                        }
                                        inviteServer.Init(client.CurrentUserId, users);

                                        UsersTreeView.EndUpdate();
                                        SortUsers();
                                        if (SetObject != null || mainForm.canv.LastSelectObject != null) 
                                        {
                                            SetObject = mainForm.canv.LastSelectObject;
                                            codes = GetProjectObjectCodes(SetObject);
                                            client.GetThemeList(SetObject.TypeID, codes, LastThemesCount);
                                            SetWindowText();
                                        }
                                        tmrUpdateOnlineTheme.Start();
                                        cancelHideTopPanel = true;
                                        break;
                                    case SmartPlus.Network.NetPacket.Type.GetOnlineUserList:
                                        count = br.ReadInt32();
                                        for (int i = 0; i < count; i++)
                                        {
                                            id = br.ReadInt32();
                                            online = br.ReadBoolean();
                                            for (int j = 0; j < UsersTreeView.Nodes.Count; j++)
                                            {
                                                if (UsersTreeView.Nodes[j].Tag != null)
                                                {
                                                    var user = (NetworkUserItem)UsersTreeView.Nodes[j].Tag;
                                                    if (user.Id == id)
                                                    {
                                                        UsersTreeView.Nodes[j].ImageIndex = online ? 1 : 0;
                                                        UsersTreeView.Nodes[j].SelectedImageIndex = UsersTreeView.Nodes[j].ImageIndex;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        SortUsers();
                                        if (!tmrUpdateOnlineTheme.Enabled)
                                        {
                                            tmrUpdateOnlineTheme.Start();
                                        }
                                        cancelHideTopPanel = true;
                                        cancelHideBottomPanel = true;
                                        break;
                                    case NetPacket.Type.GetThemeList:
                                        ThemesListView.Items.Clear();
                                        ThemesListView.SelectedItems.Clear();
                                        MessagesBox.Clear();
                                        InputTextBox.Clear();
                                        var objectType = (Constant.BASE_OBJ_TYPES_ID)br.ReadInt32();
                                        byte regionCode  = br.ReadByte();
                                        byte ngduCode = br.ReadByte();
                                        ushort oilfieldIndex = br.ReadUInt16();
                                        int objectIndex = br.ReadInt32();
                                        bool IsNeedMoreButton = br.ReadBoolean();
                                        codes = GetProjectObjectCodes(SetObject);
                                        if (objectType == SetObject.TypeID && regionCode == (byte)codes[0] && ngduCode == (byte)codes[1] && oilfieldIndex == (ushort)codes[2] && objectIndex == (int)codes[3])
                                        {
                                            count = br.ReadInt32();
                                            List<NetworkThemeItem> themes = new List<NetworkThemeItem>();
                                            for (int i = 0; i < count; i++)
                                            {
                                                theme = new NetworkThemeItem();
                                                theme.Read(br);
                                                themes.Add(theme);
                                            }

                                            if (count == 0)
                                            {
                                                ShowTopPanel("Для выбранного объекта на сервере", "нет созданных тем.");
                                                ShowLeftPanel("Для создания новой темы ", "нажмите сюда правой кнопкой мыши");
                                                cancelHideTopPanel = true;
                                                cancelHideLeftPanel = true;
                                                InputBoxVisible = false;
                                            }
                                            else
                                            {
                                                int imgIndex = -1;
                                                for (int i = 0; i < themes.Count; i++)
                                                {
                                                    switch(themes[i].ObjectType)
                                                    {
                                                        case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                                                            imgIndex = 1;
                                                            break;
                                                        case Constant.BASE_OBJ_TYPES_ID.NGDU:
                                                            imgIndex = 2;
                                                            break;
                                                        case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                                                            imgIndex = 3;
                                                            break;
                                                        case Constant.BASE_OBJ_TYPES_ID.AREA:
                                                            imgIndex = 4;
                                                            break;
                                                        case Constant.BASE_OBJ_TYPES_ID.WELL:
                                                            imgIndex = 5;
                                                            break;
                                                    }
                                                    string date = (themes[i].LastMessageTime.Year < 1901) ? " -- " : themes[i].LastMessageTime.ToShortDateString();
                                                    item = new ListViewItem(string.Format("[{0}]{1}", date, themes[i].Name), imgIndex);
                                                    item.ToolTipText = item.Text + "\n" + GetObjectName(themes[i].ObjectType, themes[i].RegionCode, themes[i].NGDUCode, themes[i].OilfieldIndex, themes[i].ObjectIndex);
                                                    item.Tag = themes[i];                                                    
                                                    ThemesListView.Items.Add(item);

                                                    if (themes[i].Id == InviteThemeId)
                                                    {
                                                        SelectThemeNode(ThemesListView.Items[i], false);
                                                    }
                                                }
                                                
                                                if (ThemesListView.Items.Count > 0 && ThemesListView.SelectedItems.Count == 0)
                                                {
                                                    SelectThemeNode(ThemesListView.Items[0], false);
                                                }
                                                InputBoxVisible = true;
                                                if (IsNeedMoreButton) AddMoreThemesButton();
                                                if (ThemesListView.Items.Count > 0)
                                                {
                                                    ThemesListView.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                                                }
                                            }
                                        }
                                        if (ThemesListView.ContextMenuStrip == null)
                                        {
                                            ThemesListView.ContextMenuStrip = contextMenu;
                                        }
                                        SetWindowText();
                                        break;
                                    case NetPacket.Type.AddTheme:
                                        theme = new NetworkThemeItem();
                                        theme.Read(br);
                                        codes = GetProjectObjectCodes(SetObject);
                                        if (theme.ObjectType == SetObject.TypeID && theme.RegionCode == (byte)codes[0] && theme.NGDUCode == (byte)codes[1] && theme.OilfieldIndex == (ushort)codes[2] && theme.ObjectIndex == (int)codes[3])
                                        {
                                            int imgIndex = -1;
                                            switch (theme.ObjectType)
                                            {
                                                case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                                                    imgIndex = 1;
                                                    break;
                                                case Constant.BASE_OBJ_TYPES_ID.NGDU:
                                                    imgIndex = 2;
                                                    break;
                                                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                                                    imgIndex = 3;
                                                    break;
                                                case Constant.BASE_OBJ_TYPES_ID.AREA:
                                                    imgIndex = 4;
                                                    break;
                                                case Constant.BASE_OBJ_TYPES_ID.WELL:
                                                    imgIndex = 5;
                                                    break;
                                            }
                                            item = new ListViewItem(theme.Name, imgIndex);
                                            item.Tag = theme;
                                            ThemesListView.Items.Insert(0, item);
                                            MessagesBox.Clear();
                                            InputTextBox.Clear();
                                            ThemesListView.Items[0].Selected = true;
                                            ShowTopPanel("Для выбранной темы нет сообщений.", string.Empty);
                                            cancelHideTopPanel = true;
                                            if (ThemesListView.Items.Count > 0)
                                            {
                                                ThemesListView.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                                            }
                                        }
                                        InputBoxVisible = true;
                                        SetWindowText();
                                        break;
                                    case NetPacket.Type.AddMessage:
                                        if (ThemesListView.SelectedItems.Count > 0 && ThemesListView.SelectedItems[0].Tag != null)
                                        {
                                            theme = (NetworkThemeItem)ThemesListView.SelectedItems[0].Tag;
                                            int themeId = br.ReadInt32();
                                            if (theme.Id == themeId)
                                            {
                                                count = br.ReadInt32();
                                                string user;
                                                for (int i = 0; i < count; i++)
                                                {
                                                    NetworkMessageItem mess = new NetworkMessageItem();
                                                    mess.Read(br);
                                                    theme.items.Add(mess);
                                                    user = GetUserName(mess.UserId);
                                                    MessagesBox.SelectionFont = BoldFont;
                                                    MessagesBox.SelectionColor = Color.Gray;
                                                    MessagesBox.AppendText(string.Format("{0} [{1:dd.MM.yyyy HH:mm}]\n", (user.Length > 0) ? user : mess.UserId.ToString(), mess.Date));
                                                    MessagesBox.SelectionFont = ValueFont;
                                                    MessagesBox.SelectionColor = Color.Black;
                                                    MessagesBox.AppendText(mess.Text + "\n");
                                                    MessagesBox.SelectionStart = MessagesBox.TextLength;
                                                    MessagesBox.ScrollToCaret();
                                                }
                                            }
                                        }
                                        InputBoxVisible = true;
                                        break;
                                    case NetPacket.Type.GetLastMessages:
                                        if (ThemesListView.SelectedItems.Count > 0 && ThemesListView.SelectedItems[0].Tag != null)
                                        {
                                            theme = (NetworkThemeItem)ThemesListView.SelectedItems[0].Tag;
                                            int themeId = br.ReadInt32();
                                            if (theme.Id == themeId)
                                            {
                                                count = br.ReadInt32();
                                                string user;
                                                for (int i = 0; i < count; i++)
                                                {
                                                    NetworkMessageItem mess = new NetworkMessageItem();
                                                    mess.Read(br);
                                                    theme.items.Add(mess);
                                                    user = GetUserName(mess.UserId);
                                                    MessagesBox.SelectionFont = BoldFont;
                                                    MessagesBox.SelectionColor = Color.Gray;
                                                    MessagesBox.AppendText(string.Format("{0} [{1:dd.MM.yyyy HH:mm}]\n", (user.Length > 0) ? user : mess.UserId.ToString(), mess.Date));
                                                    MessagesBox.SelectionFont = ValueFont;
                                                    MessagesBox.SelectionColor = Color.Black;
                                                    MessagesBox.AppendText(mess.Text + "\n");
                                                    MessagesBox.SelectionStart = MessagesBox.TextLength;
                                                    MessagesBox.ScrollToCaret();
                                                }
                                                InputBoxVisible = true;
                                                if (MessagesBox.Text.Length == 0 && count == 0)
                                                {
                                                    ShowTopPanel("Для выбранной темы нет сообщений.", string.Empty);
                                                    cancelHideTopPanel = true;
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                            else if (packet.type == NetPacket.Type.GetLastMessages && MessagesBox.Text.Length == 0)
                            {
                                ShowTopPanel("Для выбранной темы нет сообщений.", string.Empty);
                                cancelHideTopPanel = true;
                                InputBoxVisible = true;
                            }
                        }
                        catch
                        {
                            MessageBox.Show(mainForm, "Ошибка обработки результатов запроса сервера!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            InputBoxVisible = false;
                        }
                        break;
                }
            }
            if (!cancelHideTopPanel) HideTopPanel();
            if (!cancelHideLeftPanel) HideLeftPanel();
            if (!cancelHideBottomPanel) HideBottomPanel();
            TestInputTextBox();
        }
    }
}
