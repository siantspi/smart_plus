﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus.Network
{
    public class NetworkUserItem
    {
        public int Id;
        public string ADName;
        public string ADJob;
        public string ADOffice;
    }
    public class NetworkFileItem
    {
        public int Id;
        public int UserId;
        public string Name;

        public void Read(BinaryReader br)
        {
            Id = br.ReadInt32();
            Name = br.ReadString();
            UserId = br.ReadInt32();
        }
    }

    public class NetworkThemeItem
    {
        public int Id;
        public int UserId;
        public string Name;
        public Constant.BASE_OBJ_TYPES_ID ObjectType;
        public byte RegionCode;
        public byte NGDUCode;
        public ushort OilfieldIndex;
        public int ObjectIndex;
        public int Count;
        public DateTime LastMessageTime;

        public List<NetworkMessageItem> items;

        public NetworkThemeItem()
        {
            items = new List<NetworkMessageItem>();
        }
        public int GetLastId()
        {
            return (items.Count > 0) ? items[items.Count - 1].Id : -1;
        }
        public void Read(BinaryReader br)
        {
            ObjectType = (Constant.BASE_OBJ_TYPES_ID)br.ReadInt32();
            RegionCode = br.ReadByte();
            NGDUCode = br.ReadByte();
            OilfieldIndex = br.ReadUInt16();
            ObjectIndex = br.ReadInt32();
            Id = br.ReadInt32();
            UserId = br.ReadInt32();
            Name = br.ReadString();
            Count = br.ReadInt32();
            LastMessageTime = DateTime.FromOADate(br.ReadDouble());
        }
    }

    public class NetworkMessageItem
    {
        public int Id;
        public int UserId;
        public DateTime Date;
        public string Text;

        public void Read(BinaryReader br)
        {
            Id = br.ReadInt32();
            UserId = br.ReadInt32();
            Date = DateTime.FromOADate(br.ReadDouble());
            Text = br.ReadString();
        }
    }

    public class NetworkInviteItem
    {
        public int InviteType;
        public int UserSenderId;
        public DateTime TimeSend;
        public string Text;
        public Constant.BASE_OBJ_TYPES_ID ObjectType;
        public byte RegionCode;
        public byte NGDUCode;
        public ushort OilfieldIndex;
        public int ObjectIndex;
        public int ThemeId;

        public void Read(BinaryReader br)
        {
            InviteType = br.ReadInt32();
            UserSenderId = br.ReadInt32();
            TimeSend = DateTime.FromOADate(br.ReadDouble());
            Text = br.ReadString();
            ObjectType = (Constant.BASE_OBJ_TYPES_ID)br.ReadInt32();
            RegionCode = br.ReadByte();
            NGDUCode = br.ReadByte();
            OilfieldIndex = br.ReadUInt16();
            ObjectIndex = br.ReadInt32();
            ThemeId = br.ReadInt32();
        }
    }
}
