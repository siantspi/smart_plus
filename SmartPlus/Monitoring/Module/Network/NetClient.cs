﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net.Security;

namespace SmartPlus.Network
{
    public delegate void WorkProgressChangedDelegate(object sender, ProgressChangedEventArgs e);
    public delegate void WorkCompleteDelegate(object sender, RunWorkerCompletedEventArgs e);

    class NetClient
    {
        internal class Task
        {
            public NetPacket.Type type;
            public object Argument;

            public Task(NetPacket.Type type, object argument)
            {
                this.type = type;
                this.Argument = argument;
            }
        }
        public event WorkCompleteDelegate WorkComplete;
        public event WorkProgressChangedDelegate WorkProgressChanged;

        int UserId;
        public bool SendOnlineStatus;
        public bool IsInitialized { get {return UserId != -1; } }
        public bool IsBusy { get { return worker.IsBusy; } }
        public NetPacket.Type CurrentTask;
        List<Task> queue;

        BackgroundWorker worker;
        string serverName;
        int port;
        bool EnableReportProgress;
        Timer onlineTmr;

        public int CurrentUserId { get { return UserId; } }

        public NetClient(string serverName, int port, bool SendOnlineStatus)
        {
            this.serverName = serverName;
            this.port = port;
            this.SendOnlineStatus = SendOnlineStatus;
            UserId = -1;
            EnableReportProgress = true;
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            
            queue = new List<Task>();

            onlineTmr = new Timer();
            onlineTmr.Enabled = SendOnlineStatus;
            onlineTmr.Interval = 10000;
            onlineTmr.Tick += new EventHandler(onlineTmr_Tick);
        }

        public NetClient(string serverName, int port, bool SendOnlineStatus, WorkProgressChangedDelegate WorkProgressChanged, WorkCompleteDelegate WorkComplete) : this(serverName, port, SendOnlineStatus)
        {
            if (WorkProgressChanged != null) this.WorkProgressChanged += new WorkProgressChangedDelegate(WorkProgressChanged);
            if (WorkComplete != null) this.WorkComplete += new WorkCompleteDelegate(WorkComplete);
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] param;
            switch (CurrentTask)
            {
                case NetPacket.Type.GetUserId:
                    InitUserId(worker, e);
                    break;
                case NetPacket.Type.GetUserList:
                    GetUserList(worker, e);
                    break;
                case NetPacket.Type.GetOnlineUserList:
                    GetUserOnlineList(worker, e);
                    break;
                case NetPacket.Type.SetOnlineStatus:
                    SetUserOnlineStatus(worker, e);
                    break;
                case NetPacket.Type.GetFileList:
                    param = (object[])e.Argument;
                    GetFileList(worker, e, (Constant.BASE_OBJ_TYPES_ID)param[0], (byte)param[1], (byte)param[2], (ushort)param[3], (int)param[4]);
                    break;
                case NetPacket.Type.UploadFile:
                    param = (object[])e.Argument;
                    UploadFile(worker, e, (Constant.BASE_OBJ_TYPES_ID)param[0], (byte)param[1], (byte)param[2], (ushort)param[3], (int)param[4], (string)param[5]);
                    break;
                case NetPacket.Type.DownloadFile:
                    param = (object[])e.Argument;
                    DownloadFile(worker, e, (int)param[0]);
                    break;
                case NetPacket.Type.AddTheme:
                    param = (object[])e.Argument;
                    AddTheme(worker, e, (Constant.BASE_OBJ_TYPES_ID)param[0], (byte)param[1], (byte)param[2], (ushort)param[3], (int)param[4], (string)param[5]);
                    break;
                case NetPacket.Type.AddMessage:
                    param = (object[])e.Argument;
                    AddMessage(worker, e, (int)param[0], (int)param[1], (string)param[2]);
                    break;
                case NetPacket.Type.GetThemeList:
                    param = (object[])e.Argument;
                    GetThemeList(worker, e, (Constant.BASE_OBJ_TYPES_ID)param[0], (byte)param[1], (byte)param[2], (ushort)param[3], (int)param[4], (int)param[5]);
                    break;
                case NetPacket.Type.GetLastThemeList:
                    param = (object[])e.Argument;
                    GetLastThemeList(worker, e, (DateTime)param[0]);
                    break;
                case NetPacket.Type.GetLastMessages:
                    param = (object[])e.Argument;
                    GetLastMessages(worker, e, (int)param[0], (int)param[1], (int)param[2]);
                    break;
                case NetPacket.Type.UploadTemplate:
                    param = (object[])e.Argument;
                    UploadTemplate(worker, e, (TemplateServer.TemplateType)param[0], (string)param[1]);
                    break;
                case NetPacket.Type.DownloadTemplate:
                    param = (object[])e.Argument;
                    DownloadTemplate(worker, e, (TemplateServer.TemplateType)param[0]);
                    break;
                case NetPacket.Type.AddInvite:
                    param = (object[])e.Argument;
                    AddInvite(worker, e, (InviteServer.InviteType)param[0], (int)param[1], (int)param[2]);
                    break;
                case NetPacket.Type.GetInviteList:
                    GetInviteList(worker, e);
                    break;
                case NetPacket.Type.RemoveInviteList:
                    param = (object[])e.Argument;
                    RemoveInviteList(worker, e, (DateTime)param[0]);
                    break;
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (EnableReportProgress)
            {
                if (WorkProgressChanged != null) WorkProgressChanged(sender, e);
            }
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            CurrentTask = NetPacket.Type.None;
            if (e.Error != null && IsInitialized)
            {
                UserId = -1;
                onlineTmr.Stop();
            }
            else if (this.SendOnlineStatus && !onlineTmr.Enabled)
            {
                onlineTmr.Start();
            }

            if (WorkComplete != null) WorkComplete(sender, e);
            StartNext();    // обработка очереди сообщений
        }

        void onlineTmr_Tick(object sender, EventArgs e)
        {
            StartTask(NetPacket.Type.SetOnlineStatus, null);
        }

        /// Внешние функции
        public void Clear()
        {
            lock (queue)
            {
                queue.Clear();
                UserId = -1;
            }
        }
        public void InitUserId(int UserId)
        {
            this.UserId = UserId;
        }
        public void InitUserId()
        {
            if (!worker.IsBusy)
            {
                CurrentTask = NetPacket.Type.GetUserId;
                worker.RunWorkerAsync();
            }
        }
        public void GetUserList()
        {
            StartTask(NetPacket.Type.GetUserList, null);
        }
        public void GetUserOnlineList()
        {
            StartTask(NetPacket.Type.GetOnlineUserList, null);
        }

        public void GetFileList(Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex)
        {
            StartTask(NetPacket.Type.GetFileList, new object[] { objectType, regionCode, ngduCode, oilfieldIndex, objectIndex });
        }
        public void UploadFile(Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex, string fileName)
        {
            StartTask(NetPacket.Type.UploadFile, new object[] { objectType, regionCode, ngduCode, oilfieldIndex, objectIndex, fileName });
        }
        public void DownloadFile(int FileId)
        {
            StartTask(NetPacket.Type.DownloadFile, new object[] { FileId });
        }

        public void AddTheme(Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex, string ThemeName)
        {
            StartTask(NetPacket.Type.AddTheme, new object[] { objectType, regionCode, ngduCode, oilfieldIndex, objectIndex, ThemeName });
        }
        public void AddMessage(int ThemeId, int LastMessageId, string Message)
        {
            StartTask(NetPacket.Type.AddMessage, new object[] { ThemeId, LastMessageId, Message });
        }
        public void GetThemeList(Constant.BASE_OBJ_TYPES_ID objectType, object[] codes, int count)
        {
            StartTask(NetPacket.Type.GetThemeList, new object[] { objectType, codes[0], codes[1], codes[2], codes[3], count });
        }
        public void GetThemeList(Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex, int count)
        {
            StartTask(NetPacket.Type.GetThemeList, new object[] { objectType, regionCode, ngduCode, oilfieldIndex, objectIndex, count });
        }
        public void GetLastThemeList(DateTime lastUpdateTime)
        {
            StartTask(NetPacket.Type.GetLastThemeList, new object[] { lastUpdateTime });
        }
        public void GetLastMessages(int ThemeId, int LastId, int MaxCount)
        {
            StartTask(NetPacket.Type.GetLastMessages, new object[] { ThemeId, LastId, MaxCount });
        }
        
        public void UploadTemplate(TemplateServer.TemplateType TemplateId, string fileName)
        {
            StartTask(NetPacket.Type.UploadTemplate, new object[] { TemplateId, fileName });
        }
        public void DownloadTemplate(TemplateServer.TemplateType TemplateId)
        {
            StartTask(NetPacket.Type.DownloadTemplate, new object[] { TemplateId });
        }

        public void AddInvite(InviteServer.InviteType InviteType, int UserRecieverId, int ThemeId)
        {
            StartTask(NetPacket.Type.AddInvite, new object[] { InviteType, UserRecieverId, ThemeId });
        }
        public void GetInviteList()
        {
            StartTask(NetPacket.Type.GetInviteList, null);
        }
        public void RemoveInviteList(DateTime LastInviteTime)
        {
            StartTask(NetPacket.Type.RemoveInviteList, new object[] { LastInviteTime });
        }

        // Очередь сообщений
        void StartTask(NetPacket.Type taskType, object argument)
        {
            if (IsInitialized)
            {
                if (worker.IsBusy)
                {
                    lock (queue)
                    {
                        if (NeedAddTask(taskType))
                        {
                            RemoveSuchTask(taskType);
                            queue.Add(new Task(taskType, argument));
                            TryCancelWorker(taskType);
                        }
                    }
                }
                else
                {
                    CurrentTask = taskType;
                    worker.RunWorkerAsync(argument);
                }
            }
        }
        bool NeedAddTask(NetPacket.Type taskType)
        {
            if (taskType == NetPacket.Type.GetOnlineUserList || 
                taskType == NetPacket.Type.GetThemeList)
            {
                for (int i = 0; i < queue.Count;i++)
                {
                    if (queue[i].type == taskType)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        void RemoveSuchTask(NetPacket.Type taskType)
        {
            if (taskType == NetPacket.Type.GetLastMessages ||
                taskType == NetPacket.Type.SetOnlineStatus)
            {
                int i = 0;
                while (i < queue.Count)
                {
                    if (queue[i].type == taskType)
                    {
                        queue.RemoveAt(i);
                        continue;
                    }
                    i++;
                }
            }
        }
        void TryCancelWorker(NetPacket.Type taskType)
        {
            if (taskType == NetPacket.Type.GetLastMessages)
            {
                if (worker.IsBusy) worker.CancelAsync();
            }
        }
        void StartNext()
        {
            lock(queue)
            {
                if (queue.Count > 0)
                {
                    Task task = queue[0];
                    queue.RemoveAt(0);
                    StartTask(task.type, task.Argument);
                }
            }
        }

        /// Внутренние функции
        void InitUserId(BackgroundWorker worker, DoWorkEventArgs e)
        {
            UserId = -1;
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            string ip = ((System.Net.IPEndPoint)client.Client.LocalEndPoint).Address.ToString();
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write(Environment.UserName);
            bw.Write(Environment.UserDomainName);
            bw.Write(ip);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.GetUserId, UserId, ms);
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            NetPacket answer = ReciveServerAnswer(worker, e, client, 15000);
            if (answer.result == NetPacket.Result.Succes)
            {
                UserId = answer.userId;
            }
            e.Result = answer;
            client.Close();
        }
        void GetUserList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));

            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.GetUserList, UserId, null);
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            NetPacket answer = ReciveServerAnswer(worker, e, client, 30000);
            e.Result = answer;
            client.Close();
        }
        void GetUserOnlineList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));

            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.GetOnlineUserList, UserId, null);
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            NetPacket answer = ReciveServerAnswer(worker, e, client, 30000);
            e.Result = answer;
            client.Close();
        }
        void SetUserOnlineStatus(BackgroundWorker worker, DoWorkEventArgs e)
        {
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));

            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.SetOnlineStatus, UserId, null);
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = new NetPacket(NetPacket.State.Recive, NetPacket.Result.Succes, NetPacket.Type.SetOnlineStatus, UserId, null);
            client.Close();
        }

        void GetFileList(BackgroundWorker worker, DoWorkEventArgs e, Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write((int)objectType);
            bw.Write(regionCode);
            bw.Write(ngduCode);
            bw.Write(oilfieldIndex);
            bw.Write(objectIndex);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.GetFileList, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 30000);
            client.Close();
        }
        void UploadFile(BackgroundWorker worker, DoWorkEventArgs e, Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex, string fileName)
        {
            if (!File.Exists(fileName)) throw new FileNotFoundException("Указанный файл отсутствует!");
            FileStream fs = new FileStream(fileName, FileMode.Open);
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write((int)objectType);
            bw.Write(regionCode);
            bw.Write(ngduCode);
            bw.Write(oilfieldIndex);
            bw.Write(objectIndex);
            bw.Write(Path.GetFileName(fileName));
            CopyStream(fs, ms);
            fs.Close();
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.UploadFile, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 60000);
            client.Close();
        }
        void DownloadFile(BackgroundWorker worker, DoWorkEventArgs e, int FileId)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write(FileId);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.DownloadFile, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 300000);
            client.Close();
        }
        void AddTheme(BackgroundWorker worker, DoWorkEventArgs e, Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex, string ThemeName)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write(ThemeName);
            bw.Write((int)objectType);
            bw.Write(regionCode);
            bw.Write(ngduCode);
            bw.Write(oilfieldIndex);
            bw.Write(objectIndex);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.AddTheme, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 20000);
            client.Close();
        }
        void AddMessage(BackgroundWorker worker, DoWorkEventArgs e, int ThemeId, int LastMessageId, string Message)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write(ThemeId);
            bw.Write(LastMessageId);
            bw.Write(Message);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.AddMessage, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 20000);
            client.Close();
        }
        void GetThemeList(BackgroundWorker worker, DoWorkEventArgs e, Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex, int count)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write((int)objectType);
            bw.Write(regionCode);
            bw.Write(ngduCode);
            bw.Write(oilfieldIndex);
            bw.Write(objectIndex);
            bw.Write(count);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.GetThemeList, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 30000);
            client.Close();
        }
        void GetLastThemeList(BackgroundWorker worker, DoWorkEventArgs e, DateTime lastUpdateTime)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write(lastUpdateTime.ToOADate());
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.GetLastThemeList, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 30000);
            client.Close();
        }
        void GetLastMessages(BackgroundWorker worker, DoWorkEventArgs e, int ThemeId, int LastId, int MaxCount)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write(ThemeId);
            bw.Write(LastId);
            bw.Write(MaxCount);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.GetLastMessages, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 30000);
            client.Close();
        }
        void UploadTemplate(BackgroundWorker worker, DoWorkEventArgs e, TemplateServer.TemplateType TemplateId, string fileName)
        {
            if (!File.Exists(fileName)) throw new FileNotFoundException("Указанный файл отсутствует!");
            FileStream fs = new FileStream(fileName, FileMode.Open);
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write((int)TemplateId);
            bw.Write(Path.GetFileName(fileName));
            CopyStream(fs, ms);
            fs.Close();
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.UploadTemplate, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 60000);
            client.Close();
        }
        void DownloadTemplate(BackgroundWorker worker, DoWorkEventArgs e, TemplateServer.TemplateType TemplateId)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write((int)TemplateId);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.DownloadTemplate, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 60000);
            client.Close();
        }
        
        void AddInvite(BackgroundWorker worker, DoWorkEventArgs e, InviteServer.InviteType inviteType, int userRecieverId, int themeId)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write((int)inviteType);
            bw.Write(userRecieverId);
            bw.Write(themeId);
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.AddInvite, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 20000);
            client.Close();
        }
        void GetInviteList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.GetInviteList, UserId, null);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 60000);
            client.Close();
        }
        void RemoveInviteList(BackgroundWorker worker, DoWorkEventArgs e, DateTime LastInviteTime)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms, Encoding.UTF8);
            bw.Write(LastInviteTime.ToOADate());
            NetPacket packet = new NetPacket(NetPacket.State.Send, NetPacket.Result.None, NetPacket.Type.RemoveInviteList, UserId, ms);
            TcpClient client = NetPacket.GetConnectedClient(serverName, port);
            if (!client.Connected) throw new Exception(string.Format("Не удалось установить подключение к серверу {0}:{1}", serverName, port));
            NetPacket.SendPacket(worker, e, client.GetStream(), packet);
            e.Result = ReciveServerAnswer(worker, e, client, 20000);
            client.Close();
        }

        // Вспомогательные фукнции
        void CopyStream(Stream InStream, Stream OutStream)
        {
            int buffSize = 512 * 1024;
            int count = (int)InStream.Length;
            if (buffSize > count) buffSize = count;
            byte[] buff = new byte[buffSize];
            int sumCount = 0, currCount;
            InStream.Seek(0, SeekOrigin.Begin);
            do
            {
                currCount = InStream.Read(buff, 0, buffSize);
                OutStream.Write(buff, 0, currCount);
                sumCount += currCount;
            }
            while (sumCount < count);
            OutStream.Flush();
            OutStream.Seek(0, SeekOrigin.Begin);
        }
        NetPacket ReciveServerAnswer(BackgroundWorker worker, DoWorkEventArgs e, TcpClient client, int timeout)
        {
            NetPacket packet = new NetPacket();
            EnableReportProgress = false;
            int timeCounter = 0;
            Stream netStream = client.GetStream();
            while (true)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    packet.result = NetPacket.Result.Cancel;
                    break;
                }
                if (timeCounter > timeout)
                {
                    packet.result = NetPacket.Result.Timeout;
                    break;
                }

                if (client.Available > 10 && netStream.CanRead)
                {
                    packet = NetPacket.RecivePacket(worker, e, netStream);
                    break;
                }
                System.Threading.Thread.Sleep(10);
                timeCounter += 10;
            }
            netStream.Close();
            EnableReportProgress = true;
            return packet;
        }
    }
}