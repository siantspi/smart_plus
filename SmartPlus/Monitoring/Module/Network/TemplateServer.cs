﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus.Network
{
    class TemplateServer
    {
        public enum TemplateType
        {
            /// <summary>
            /// Неопределено
            /// </summary>
            None,
            /// <summary>
            /// Шаблон отчета прогноза по ячейкам заводнения
            /// </summary>
            ReportAreasPredictTemplate
        }

        MainForm mainForm;
        NetClient client;
        public string TemplateLocalPath;
        public bool IsBusy
        {
            get
            {
                return (client != null) && client.IsBusy; 
            }
        }

        public TemplateServer(MainForm mainForm, WorkProgressChangedDelegate ExtWorkProgressChanged, WorkCompleteDelegate ExtWorkComplete)
        {
            this.mainForm = mainForm;
            TemplateLocalPath = string.Empty;
            client = new NetClient(mainForm.ServerName, 1992, false, null, WorkComplete);
            if (ExtWorkProgressChanged != null) client.WorkProgressChanged += ExtWorkProgressChanged;
            if (ExtWorkComplete != null) client.WorkComplete += ExtWorkComplete;
            client.InitUserId();
        }
        
        TemplateType GetTemplateType(string TemplateFilePath)
        {
            string fileName = Path.GetFileName(TemplateFilePath);
            switch (fileName)
            {
                case "Прогноз по ЯЗ.xltm":
                    return TemplateType.ReportAreasPredictTemplate;
            }
            return TemplateType.None;
        }

        public void CancelDownload()
        {
            if (client != null && client.IsBusy) client.Clear();
        }

        public void UploadTemplate(string FilePath)
        {
            if (File.Exists(FilePath))
            {
                client.UploadTemplate(GetTemplateType(FilePath), Path.GetFileName(FilePath));
            }
        }
        public void DownloadTemplate(TemplateType templateType)
        {
            client.DownloadTemplate(templateType);
        }

        public void WorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(mainForm, e.Error.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (e.Cancelled)
            {
            }
            else if (e.Result != null)
            {
                Network.NetPacket packet = (Network.NetPacket)e.Result;
                switch (packet.result)
                {
                    case NetPacket.Result.Timeout:
                        MessageBox.Show(mainForm, "Таймаут соединения с сервером!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case NetPacket.Result.Error:
                        MessageBox.Show(mainForm, "Ошибка обработки запроса на сервере!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case SmartPlus.Network.NetPacket.Result.Succes:
                        try
                        {
                            NetworkFileItem file;
                            string res = string.Empty;
                            if (packet.message != null)
                            {
                                BinaryReader br = new BinaryReader(packet.message, Encoding.UTF8);
                                switch (packet.type)
                                {
                                    case NetPacket.Type.UploadFile:
                                        int type = br.ReadUInt16();
                                        int id = br.ReadInt32();
                                        string fileName = br.ReadString();
                                        //MessageBox.Show(mainForm, string.Format("Шаблон '{0}' загружен успешно!", fileName), "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        break;
                                    case NetPacket.Type.DownloadTemplate:
                                        file = new NetworkFileItem();
                                        file.Read(br);
                                        int ver = br.ReadByte();
                                        ushort templateType = br.ReadUInt16();
                                        if (file.Name.Length > 0)
                                        {
                                            MemoryStream ms = new MemoryStream();
                                            RDF.ConvertEx.CopyStream(br.BaseStream, ms, false);
                                            ms.Seek(0, SeekOrigin.Begin);
                                            string ext = Path.GetExtension(file.Name);
                                            // удаление предыдущей версии
                                            string filePath = mainForm.UserTemplatesFolder + "\\" + file.Name.Replace(ext, string.Format("(ver {0}){1}", ver - 1, ext));
                                            if (File.Exists(filePath)) File.Delete(filePath);

                                            filePath = mainForm.UserTemplatesFolder + "\\" + file.Name.Replace(ext, string.Format("(ver {0}){1}", ver, ext));
                                            if (!Directory.Exists(mainForm.UserTemplatesFolder)) Directory.CreateDirectory(mainForm.UserTemplatesFolder);
                                            FileStream fs = new FileStream(filePath, FileMode.Create);
                                            ms.WriteTo(fs);
                                            fs.Flush();
                                            fs.Close();
                                            TemplateLocalPath = filePath;
                                        }
                                        break;
                                }
                            }
                        }
                        catch
                        {
                            MessageBox.Show(mainForm, "Ошибка обработки результатов запроса сервера!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                }
            }
        }
    }
}
