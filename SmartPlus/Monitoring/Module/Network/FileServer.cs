﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;
using System.Drawing;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.Network
{
    class FileServer
    {
        MainForm mainForm;
        TreeView FilesTreeView;
        NetClient client;
        ImageList filesIcons;
        ContextMenuStrip contextMenu;
        List<NetworkUserItem> users;
        Panel waitPanel;
        Font waitFont;
        string waitTitle, waitText;
        bool InitIdStarted;
        BaseObj SetObject;

        public FileServer(MainForm mainForm)
        {
            this.mainForm = mainForm;
            mainForm.canv.OnChangeSelectedObject += new OnChangeSelectedObjectDelegate(canv_OnChangeSelectedObject);
            InitIdStarted = false;
            SetObject = new BaseObj();
            waitFont = new Font("Calibri", 8.25f);
            waitPanel = new Panel();
            waitPanel.BackColor = Color.White;
            waitPanel.Paint += new PaintEventHandler(waitPanel_Paint);
            waitPanel.Dock = DockStyle.Fill;
            mainForm.SplitDiscussion.Panel2.Controls.Add(waitPanel);
            waitPanel.Visible = false;

            FilesTreeView = new TreeView();
            FilesTreeView.AllowDrop = true;
            filesIcons = new ImageList();
            filesIcons.ColorDepth = ColorDepth.Depth24Bit;
            filesIcons.Images.Add(Properties.Resources.Folder);
            filesIcons.Images.Add(Properties.Resources.doc);
            filesIcons.Images.Add(Properties.Resources.pdf);
            filesIcons.Images.Add(Properties.Resources.ppt);
            filesIcons.Images.Add(Properties.Resources.png);
            filesIcons.Images.Add(Properties.Resources.txt);
            filesIcons.Images.Add(Properties.Resources.xls);
            filesIcons.Images.Add(Properties.Resources.def);
            filesIcons.Images.Add(Properties.Resources.delete);
            FilesTreeView.ImageList = filesIcons;
            FilesTreeView.MouseDown += FilesTreeView_MouseDown;
            FilesTreeView.Dock = DockStyle.Fill;
            FilesTreeView.ShowRootLines = false;
            FilesTreeView.FullRowSelect = true;
            mainForm.SplitDiscussion.Panel2.Controls.Add(FilesTreeView);
            FilesTreeView.ShowNodeToolTips = true;

            FilesTreeView.DragOver += FilesTreeView_DragOver;
            FilesTreeView.ItemDrag += FilesTreeView_ItemDrag;
            FilesTreeView.DragEnter += FilesTreeView_DragEnter;
            FilesTreeView.DragDrop += FilesTreeView_DragDrop;
            


            contextMenu = new ContextMenuStrip();
            contextMenu.Items.Add("Обновить список файлов", null, LoadFiles_Click);
            contextMenu.Items.Add("Отправить файл...", null, UpLoadFile_Click);

            client = new NetClient(mainForm.ServerName, 1992, false, WorkProgressChanged, WorkComplete);
            users = new List<NetworkUserItem>();
        }

        void canv_OnChangeSelectedObject(BaseObj LastSelectedObject)
        {
            UpdateData(LastSelectedObject, false);
        }

        #region DragDrop
        DataObject CreateDataObject(NetworkFileItem file, NetClient client)
        {
            DataFormats.Format format = DataFormats.GetFormat("FileServerDragObject");
            DragObject dragObject = new DragObject(file, client);
            return new DataObject(format.Name, dragObject);
        }

        [Serializable]
        public class DragObject : Object
        {
            public NetworkFileItem file;
            public NetClient client;

            public DragObject(NetworkFileItem file, NetClient client)
            {
                this.file = file;
                this.client = client;
            }
        }
        void FilesTreeView_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        void FilesTreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            TreeNode node = (TreeNode)e.Item;
            if (client.IsInitialized && node.Tag != null)
            {
                var file = (NetworkFileItem)node.Tag;
                DataObject dataObject = CreateDataObject(file, client);
                FilesTreeView.DoDragDrop(dataObject, DragDropEffects.Move);
            }
        }
        void FilesTreeView_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        void FilesTreeView_DragDrop(object sender, DragEventArgs e)
        {
            if (mainForm.canv.MapProject == null) return;
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                string[] files = null;

                List<string> extentions = new List<string>(new string[] { ".img", ".cntr", ".prfl", ".pvt", ".mrk", ".wl", ".grid", ".vm" });
                List<string> extentionsOther = new List<string>(new string[] { ".doc", ".docx", ".docm", ".pdf", ".ppt", ".pptx", ".pptm", ".xls", ".xlsx", ".xlsb", ".xlsm" });
                files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                string name, ext;

                //for (int i = 0; i < files.Length; i++)
                //{
                name = Path.GetFileName(files[0]);
                ext = Path.GetExtension(files[0]);
                if (extentions.IndexOf(ext) > -1 || extentionsOther.IndexOf(ext) > -1)
                {
                    if (File.Exists(files[0]))
                    {
                        string oilfieldName = string.Empty, wellName = string.Empty;
                        object[] indexes = PinObjectForm.ShowPinFileForm(mainForm, mainForm.canv.LastSelectObject, files[0]);

                        if (indexes != null)
                        {
                            object[] codes = GetProjectObjectCodes();
                            client.UploadFile(mainForm.canv.LastSelectObject.TypeID, (byte)codes[0], (byte)codes[1], (ushort)codes[2], (int)codes[3], (string)indexes[0]);
                        }

                    }
                }
                //}
            }
        }

        #endregion

        public object[] GetProjectObjectCodes()
        {
            object[] result = new object[4];
            OilField of;
            byte regionCode = 0, ngduCode = 0;
            ushort oilfieldIndex = 0;
            int objIndex = -1;
            switch (SetObject.TypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                    regionCode = (byte)SetObject.Code;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.NGDU:
                    regionCode = (byte)SetObject.Code;
                    ngduCode = (byte)SetObject.Index;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    of = (OilField)SetObject;
                    regionCode = (byte)of.ParamsDict.EnterpriseCode;
                    ngduCode = (byte)of.ParamsDict.NGDUCode;
                    oilfieldIndex = (ushort)of.Index;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    Area area = (Area)SetObject;
                    of = (OilField)mainForm.canv.MapProject.OilFields[area.OilFieldIndex];
                    regionCode = (byte)of.ParamsDict.EnterpriseCode;
                    ngduCode = (byte)of.ParamsDict.NGDUCode;
                    oilfieldIndex = (ushort)of.Index;
                    objIndex = area.Index;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    Well well = (Well)SetObject;
                    of = (OilField)mainForm.canv.MapProject.OilFields[well.OilFieldIndex];
                    regionCode = (byte)of.ParamsDict.EnterpriseCode;
                    ngduCode = (byte)of.ParamsDict.NGDUCode;
                    oilfieldIndex = (ushort)of.Index;
                    objIndex = well.Index;
                    break;
            }
            result[0] = regionCode;
            result[1] = ngduCode;
            result[2] = oilfieldIndex;
            result[3] = objIndex;
            return result;
        }
        string GetObjectName(BaseObj PinObject)
        {
            var project = mainForm.GetLastProject();
            string name = string.Empty;
            switch (PinObject.TypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                    if (PinObject.Code == 0)
                    {
                        name = project.Name;
                    }
                    else if (PinObject.Code == 1) // crutch
                    {
                        name = "ХМАО";
                    }
                    else if (PinObject.Code == 2) // crutch
                    {
                        name = "Оренбургская область";
                    }
                    else // crutch
                    {
                        name = "Башнефть-Полюс";
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.NGDU:
                    for (int i = 0; i < project.OilFields.Count; i++)
                    {
                        if (project.OilFields[i].ParamsDict.NGDUCode == PinObject.Index)
                        {
                            name = "НГДУ " + project.OilFields[i].ParamsDict.NGDUName;
                            break;
                        }
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    name = "Мест: " + project.OilFields[PinObject.Index].Name;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    Area area = (Area)PinObject;
                    var of = project.OilFields[area.OilFieldIndex];
                    name = string.Format("ЯЗ: {0} ({1})", area.Name, of.Name);
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    Well w = (Well)PinObject;
                    of = project.OilFields[w.OilFieldIndex];
                    var dict = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                    string areaName = dict.GetShortNameByCode(w.OilFieldAreaCode);
                    if (areaName.Length > 0)
                    {
                        areaName = ", " + areaName;
                        if (areaName.IndexOf("участок") == -1) areaName += " площадь";
                    }
                    name = string.Format("Скв: {0} ({1}{2})", w.Name, of.Name, areaName);
                    break;
            }
            return name;
        }

        public void Clear(bool clearObject)
        {
            FilesTreeView.Nodes.Clear();
            FilesTreeView.ContextMenuStrip = null;
            client.Clear();
            InitIdStarted = false;
            if (clearObject) SetObject = new BaseObj();
        }
        public void UpdateData(BaseObj NewObject, bool ForceUpdate)
        {
            if (mainForm.canv.MapProject != null && (ForceUpdate || mainForm.pDiscussion.Visible))
            {
                if (NewObject == null)
                {
                    ShowWaitPanel("Для загрузки файлов", "выберите объект на карте.");
                }
                else
                {
                    if (SetObject == null ||
                        SetObject.TypeID != NewObject.TypeID || 
                        SetObject.Code != NewObject.Code ||
                        SetObject.Index != NewObject.Index)
                    {
                        SetObject = NewObject;
                        ShowWaitPanel("Подождите...", "Загрузка списка файлов.");
                        if (!InitIdStarted)
                        {
                            InitFilesTreeView();
                        }
                        else
                        {
                            object[] codes = GetProjectObjectCodes();
                            client.GetFileList(SetObject.TypeID, (byte)codes[0], (byte)codes[1], (ushort)codes[2], (int)codes[3]);
                        }
                    }
                }
            }
        }

        void waitPanel_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            grfx.Clear(Color.White);
            string str1 = waitTitle;
            string str2 = waitText;
            SizeF s1 = grfx.MeasureString(str1, waitFont, PointF.Empty, StringFormat.GenericTypographic);
            SizeF s2 = grfx.MeasureString(waitText, waitFont, PointF.Empty, StringFormat.GenericTypographic);
            float x = waitPanel.ClientRectangle.Width / 2;
            float y = waitPanel.ClientRectangle.Height / 2;
            grfx.DrawString(str1, waitFont, Brushes.Black, x - s1.Width / 2, y - s1.Height);
            grfx.DrawString(waitText, waitFont, Brushes.Black, x - s2.Width / 2, y);
        }
        public void ShowWaitPanel(string Title, string WaitText)
        {
            waitTitle = Title;
            waitText = WaitText;
            waitPanel.Visible = true;
            waitPanel.Invalidate();
        }
        public void HideWaitPanel()
        {
            waitPanel.Visible = false;
            waitPanel.ContextMenuStrip = null;
        }


        void FilesTreeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node != null && e.Node.Tag != null)
            {
                ShowWaitPanel("Подождите...", "Загрузка файла с сервера.");
                NetworkFileItem file = (NetworkFileItem)e.Node.Tag;
                client.DownloadFile(file.Id);
            }
        }
        void FilesTreeView_MouseDown(object sender, MouseEventArgs e)
        {
            TreeViewHitTestInfo info = FilesTreeView.HitTest(e.X, e.Y);
            FilesTreeView.SelectedNode = info.Node;
        }
        int GetFileIconIndex(string Extention)
        {
            switch (Extention)
            {
                case ".doc": return 1;
                case ".docx": return 1;
                case ".docm": return 1;
                case ".pdf": return 2;
                case ".ppt": return 3;
                case ".pptx": return 3;
                case ".pptm": return 3;
                case ".bmp": return 4;
                case ".jpg": return 4;
                case ".jpeg": return 4;
                case ".png": return 4;
                case ".tif": return 4;
                case ".tiff": return 4;
                case ".txt": return 5;
                case ".xls": return 6;
                case ".xlsx": return 6;
                case ".xlsm": return 6;
                case ".xlsb": return 6;
                default: return 7;
            }
        }

        void InitFilesTreeView()
        {
            if (!client.IsInitialized)
            {
                InitIdStarted = true;
                ShowWaitPanel("Подождите...", "Идет подключение к серверу");
                client.InitUserId();
            }
        }
        void SetErrorMessage(string message)
        {
            FilesTreeView.Nodes.Clear();
            TreeNode node = new TreeNode();
            node.ForeColor = System.Drawing.Color.Red;
            node.ImageIndex = 8;
            node.SelectedImageIndex = 8;
            node.Text = message;
            FilesTreeView.Nodes.Add(node);
        }
        void LoadFiles_Click(object sender, EventArgs e)
        {
            object[] codes = GetProjectObjectCodes();
            client.GetFileList(SetObject.TypeID, (byte)codes[0], (byte)codes[1], (ushort)codes[2], (int)codes[3]);
        }
        void UpLoadFile_Click(object sender, EventArgs e)
        {
            string oilfieldName = string.Empty, wellName = string.Empty;
            if (FilesTreeView.SelectedNode != null)
            {
                TreeNode node = FilesTreeView.SelectedNode;
                if (node.Level == 0 && node.Nodes.Count > 0)
                {
                    oilfieldName = node.Text;
                }
                else if (node.Level == 1 && node.Nodes.Count > 0)
                {
                    if (node.Parent != null) oilfieldName = node.Parent.Text;
                    wellName = node.Text;
                }
            }
            object[] indexes = PinObjectForm.ShowPinFileForm(mainForm, mainForm.canv.LastSelectObject, string.Empty);

            if (indexes != null)
            {
                object[] codes = GetProjectObjectCodes();
                client.UploadFile(mainForm.canv.LastSelectObject.TypeID, (byte)codes[0], (byte)codes[1], (ushort)codes[2], (int)codes[3], (string)indexes[0]);
            }
        }
        public void UploadFile(Constant.BASE_OBJ_TYPES_ID objectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int objectIndex, string fileName)
        {
            client.UploadFile(objectType, regionCode, ngduCode, oilfieldIndex, objectIndex, fileName);
        }
        int GetUserIndex(int UserId)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].Id == UserId)
                {
                    return i;
                }
            }
            return -1;
        }

        void AddNode(NetworkFileItem file)
        {
            TreeNode node = null;
            FilesTreeView.BeginUpdate();
            node = new TreeNode();
            node.ImageIndex = GetFileIconIndex(Path.GetExtension(file.Name));
            node.SelectedImageIndex = node.ImageIndex;
            node.Text = file.Name;
            node.Tag = file;
            InsertFileNode(FilesTreeView.Nodes, node);
            if (node != null) FilesTreeView.SelectedNode = node;
            FilesTreeView.EndUpdate();
        }
        void InsertFileNode(TreeNodeCollection nodes, TreeNode node)
        {
            if (node.Tag != null)
            {
                NetworkFileItem item;
                NetworkFileItem file = (NetworkFileItem)node.Tag;
                int index = -1;
                for (int i = 0; i < nodes.Count; i++)
                {
                    item = (NetworkFileItem)nodes[i].Tag;
                    if (file.Name.CompareTo(item.Name) < 0)
                    {
                        index = i;
                        break;
                    }
                }
                if (index != -1)
                {
                    nodes.Insert(index, node);
                }
                else
                {
                    nodes.Add(node);
                }
            }
        }

        void ClearFileCacheFolder()
        {
            string path = mainForm.UserSmartPlusFolder + "\\Файлы";
            if (Directory.Exists(path))
            {
                DirectoryInfo root = new DirectoryInfo(path);
                FileInfo[] files = root.GetFiles();
                for (int i = 0; i < files.Length; i++)
                {
                    try
                    {
                        if (files[i].Exists) files[i].Delete();
                    }
                    catch
                    {
 
                    }
                }
            }
        }

        public void WorkProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        public void WorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            HideWaitPanel();
            if (e.Error != null)
            {
                if (e.Error.Message.StartsWith("Не удалось установить подключение к серверу"))
                {
                    SetErrorMessage("Нет подключения к серверу.");
                    Clear(true);
                    return;
                }
                else
                {
                    MessageBox.Show(mainForm, e.Error.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (e.Cancelled)
            {
            }
            else if (e.Result != null)
            {
                byte regionCode, ngduCode;
                ushort oilfieldIndex;
                int objectIndex;
                Network.NetPacket packet = (Network.NetPacket)e.Result;
                switch (packet.result)
                {
                    case NetPacket.Result.Timeout:
                        SetErrorMessage("Таймаут соединения с сервером.");
                        Clear(true);
                        return;
                    case NetPacket.Result.Error:
                        MessageBox.Show(mainForm, "Ошибка обработки запроса на сервере!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case SmartPlus.Network.NetPacket.Result.Succes:
                        try
                        {
                            TreeNode node;
                            NetworkFileItem file;
                            string res = string.Empty;
                            if (packet.type == SmartPlus.Network.NetPacket.Type.GetUserId)
                            {
                                ShowWaitPanel("Подождите...", "Инициализация списка файлов");
                                ClearFileCacheFolder();
                                client.GetUserList();
                                return;
                            }
                            if (packet.message != null)
                            {
                                int count;
                                object[] codes;
                                TreeNodeCollection nodes;
                                BinaryReader br = new BinaryReader(packet.message, Encoding.UTF8);
                                switch (packet.type)
                                {
                                    case SmartPlus.Network.NetPacket.Type.GetUserList:
                                        count = br.ReadInt32();
                                        users.Clear();
                                        for (int i = 0; i < count; i++)
                                        {
                                            var user = new NetworkUserItem();
                                            user.Id = br.ReadInt32();
                                            user.ADName = br.ReadString();
                                            user.ADJob = br.ReadString();
                                            user.ADOffice = br.ReadString();
                                            br.BaseStream.Seek(1, SeekOrigin.Current); // online - status
                                            users.Add(user);
                                        }
                                        if (SetObject != null)
                                        {
                                            codes = GetProjectObjectCodes();
                                            client.GetFileList(SetObject.TypeID, (byte)codes[0], (byte)codes[1], (ushort)codes[2], (int)codes[3]);
                                        }
                                        FilesTreeView.ContextMenuStrip = contextMenu;
                                        FilesTreeView.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(FilesTreeView_NodeMouseDoubleClick);
                                        break;
                                    case NetPacket.Type.UploadFile:
                                        var objectType = (Constant.BASE_OBJ_TYPES_ID)br.ReadInt32();
                                        regionCode = br.ReadByte();
                                        ngduCode = br.ReadByte();
                                        oilfieldIndex = br.ReadUInt16();
                                        objectIndex = br.ReadInt32();
                                        codes = GetProjectObjectCodes();
                                        if (objectType == SetObject.TypeID && regionCode == (byte)codes[0] && ngduCode == (byte)codes[1] && oilfieldIndex == (ushort)codes[2] && objectIndex == (int)codes[3])
                                        {
                                            file = new NetworkFileItem();
                                            file.Read(br);
                                            AddNode(file);
                                        }
                                        break;
                                    case NetPacket.Type.GetFileList:
                                        objectType = (Constant.BASE_OBJ_TYPES_ID)br.ReadInt32();
                                        regionCode = br.ReadByte();
                                        ngduCode = br.ReadByte();
                                        oilfieldIndex = br.ReadUInt16();
                                        objectIndex = br.ReadInt32();
                                        codes = GetProjectObjectCodes();
                                        count = 0;
                                        if (objectType == SetObject.TypeID && regionCode == (byte)codes[0] && ngduCode == (byte)codes[1] && oilfieldIndex == (ushort)codes[2] && objectIndex == (int)codes[3])
                                        {
                                            nodes = FilesTreeView.Nodes;
                                            if (nodes != null) nodes.Clear();
                                            count = br.ReadInt32();
                                            FilesTreeView.BeginUpdate();
                                            for (int i = 0; i < count; i++)
                                            {
                                                file = new NetworkFileItem();
                                                file.Read(br);

                                                if (nodes != null)
                                                {
                                                    node = new TreeNode();
                                                    if (file.Name.IndexOf(".") > -1)
                                                    {
                                                        node.ImageIndex = GetFileIconIndex(Path.GetExtension(file.Name));
                                                        node.SelectedImageIndex = GetFileIconIndex(Path.GetExtension(file.Name));
                                                    }
                                                    node.Text = file.Name;
                                                    node.ToolTipText = file.Name;
                                                    int index = GetUserIndex(file.UserId);
                                                    if (index > -1)
                                                    {
                                                        node.ToolTipText = string.Format("Загружен: {0}\n\t {1}\n\t {2}", users[index].ADName, users[index].ADJob, users[index].ADOffice);
                                                    }

                                                    node.Tag = file;
                                                    InsertFileNode(nodes, node);
                                                }
                                            }
                                            if (!FilesTreeView.ShowRootLines) FilesTreeView.ShowRootLines = true;
                                            FilesTreeView.EndUpdate();
                                        }
                                        if (count == 0)
                                        {
                                            ShowWaitPanel("Для выбранного объекта", "на сервере нет файлов.");
                                            waitPanel.ContextMenuStrip = contextMenu;
                                            return;
                                        }
                                        break;
                                    case NetPacket.Type.DownloadFile:
                                        ShowWaitPanel("Подождите...", "Файл открывается.");
                                        file = new NetworkFileItem();
                                        file.Read(br);
                                        if (file.Name.Length > 0)
                                        {
                                            MemoryStream ms = new MemoryStream();
                                            RDF.ConvertEx.CopyStream(br.BaseStream, ms, false);
                                            ms.Seek(0, SeekOrigin.Begin);
                                            mainForm.canv.OpenNewDataFile(file.Name, ms);
                                        }
                                        HideWaitPanel();
                                        break;
                                }
                            }
                        }
                        catch
                        {
                            MessageBox.Show(mainForm, "Ошибка обработки результатов запроса сервера!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                }
            }
        }
    }
}
