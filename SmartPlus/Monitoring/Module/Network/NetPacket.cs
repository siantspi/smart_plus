﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;

namespace SmartPlus.Network
{
    public sealed class NetPacket
    {
        public enum State : byte
        {
            /// <summary>
            /// Неопределено
            /// </summary>
            None,
            /// <summary>
            /// Отправка
            /// </summary>
            Send,
            /// <summary>
            /// Получение
            /// </summary>
            Recive
        }
        public enum Result : byte
        {
            /// <summary>
            /// Неопределено
            /// </summary>
            None,
            /// <summary>
            /// Отмена
            /// </summary>
            Cancel,
            /// <summary>
            /// Ошибка
            /// </summary>
            Error,
            /// <summary>
            /// Превышено время выполнения
            /// </summary>
            Timeout,
            /// <summary>
            /// Успешно
            /// </summary>
            Succes
        }
        public enum Type : byte
        {
            /// <summary>
            /// Неопределено
            /// </summary>
            None,
            /// <summary>
            /// Получить(зарегистрировать) Id пользователя
            /// </summary>
            GetUserId,
            /// <summary>
            /// Получить список всех пользователей
            /// </summary>
            GetUserList,
            /// <summary>
            /// Получить статус онлайн для пользователей
            /// </summary>
            GetOnlineUserList,
            /// <summary>
            /// Обновление онлайн статуса
            /// </summary>
            SetOnlineStatus,
            /// <summary>
            /// Загрузить файл в файловое хранилище
            /// </summary>
            UploadFile,
            /// <summary>
            /// Получить файл с файлового хранилища
            /// </summary>
            DownloadFile,
            /// <summary>
            /// Получить список файлов файлового хранилища
            /// </summary>
            GetFileList,
            /// <summary>
            /// Получить список тем службы сообщений
            /// </summary>
            GetThemeList,
            /// <summary>
            /// Получить список актуальных тем службы сообщений
            /// </summary>
            GetLastThemeList,
            /// <summary>
            /// Добавить новую тему службы сообщений
            /// </summary>
            AddTheme,
            /// <summary>
            /// Добавить сообщение
            /// </summary>
            AddMessage,
            /// <summary>
            /// Получить последние сообщения темы
            /// </summary>
            GetLastMessages,
            /// <summary>
            /// Получить сообщения по индексу
            /// </summary>
            GetMessages,
            /// <summary>
            /// Загрузить шаблон документа в файловое хранилище
            /// </summary>
            UploadTemplate,
            /// <summary>
            /// Получить шаблон документа с файлового хранилища
            /// </summary>
            DownloadTemplate,
            /// <summary>
            /// Добавить приглашение для пользователя
            /// </summary>
            AddInvite,
            /// <summary>
            /// Получить список приглашений для пользователя
            /// </summary>
            GetInviteList,
            /// <summary>
            /// Удалить список приглашений для пользователя
            /// </summary>
            RemoveInviteList
        }
        

        public State state;
        public Result result;
        public Type type;
        public int userId;
        public MemoryStream message;

        public NetPacket()
        {
            state = State.None;
            result = Result.None;
            type = Type.None;
            userId = -1;
            message = null;
        }
        public NetPacket(State MessageState, Result MessageResult, Type MessageType, int UserId, MemoryStream Message)
        {
            this.state = MessageState;
            this.result = MessageResult;
            this.type = MessageType;
            this.userId = UserId;
            this.message = Message;
        }
        public static TcpClient GetConnectedClient(string hostName, int port)
        {
            TcpClient client = new TcpClient();
            try
            {
                client.Connect(hostName, port);
            }
            catch
            {
                return new TcpClient();
            }
            return client;
        }

        /// Security
        public static SslStream GetSSLStream(string ServerName, TcpClient client)
        {
            SslStream sslStream = null;
            if (client != null && client.Connected)
            {
                try
                {
                    bool leaveInnerStreamOpen = false;
                    RemoteCertificateValidationCallback validationCallback = new RemoteCertificateValidationCallback(ServerValidationCallback);
                    LocalCertificateSelectionCallback selectionCallback = new LocalCertificateSelectionCallback(ClientSertificateSelectionCallback);
                    //EncryptionPolicy encryptionPolicy = EncryptionPolicy.RequireEncryption;

                    sslStream = new SslStream(client.GetStream(), leaveInnerStreamOpen, validationCallback, selectionCallback);
                    AuthenticateAsClient(ServerName, sslStream);
                }
                catch
                {
                    if (sslStream != null) sslStream.Close();
                    sslStream = null;
                }
            }
            return sslStream;
        }
        private static bool ServerValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            switch (sslPolicyErrors)
            {
                case SslPolicyErrors.RemoteCertificateNameMismatch:
                    return false;
                case SslPolicyErrors.RemoteCertificateNotAvailable:
                    return false;
                case SslPolicyErrors.RemoteCertificateChainErrors:
                    return false;
            }
            return true;
        }
        public static X509Certificate ClientSertificateSelectionCallback(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            return localCertificates[0];
        }
        private static void AuthenticateAsClient(string ServerName, SslStream sslStream)
        {
            X509CertificateCollection clientCertificates = GetClientCertificates("ssl_stream");
            SslProtocols sslProtocols = SslProtocols.Tls;
            bool checkCertificateRevocation = true;
            sslStream.AuthenticateAsClient(ServerName, clientCertificates, sslProtocols, checkCertificateRevocation);
        }
        private static X509CertificateCollection GetClientCertificates(string CertificateName)
        {
            X509CertificateCollection certificateCollection = new X509CertificateCollection();


            return certificateCollection;
        }
        public static NetPacket RecivePacket(BackgroundWorker worker, DoWorkEventArgs e, Stream networkStream)
        {
            NetPacket packet = new NetPacket();
            BinaryReader br = new BinaryReader(networkStream);
            packet.state = (State)br.ReadByte();
            packet.result = (Result)br.ReadByte();
            packet.type = (Type)br.ReadByte();
            packet.userId = br.ReadInt32();
            int count = br.ReadInt32();
            if (count > 0)
            {
                packet.message = new MemoryStream();
                int buffSize = 512 * 1024;
                if (buffSize > count) buffSize = count;

                byte[] buff = new byte[buffSize];
                int sumCount = 0, currCount;
                float progress;
                do
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        packet.result = Result.Cancel;
                        packet.message = null;
                        return packet;
                    }
                    currCount = networkStream.Read(buff, 0, buffSize);
                    packet.message.Write(buff, 0, currCount);
                    sumCount += currCount;
                    progress = sumCount * 100f / count;
                    if (progress > 100) progress = 100;
                    worker.ReportProgress((int)progress);
                }
                while (sumCount < count);
                packet.message.Seek(0, SeekOrigin.Begin);
            }
            return packet;
        }
        public static void SendPacket(BackgroundWorker worker, DoWorkEventArgs e, Stream networkStream, NetPacket packet)
        {
            if (networkStream.CanWrite && packet != null)
            {
                BinaryWriter bw = new BinaryWriter(networkStream);
                bw.Write((byte)packet.state);
                bw.Write((byte)packet.result);
                bw.Write((byte)packet.type);
                bw.Write((int)packet.userId);
                if (packet.message != null)
                {
                    packet.message.Seek(0, SeekOrigin.Begin);
                    int count = (int)packet.message.Length;
                    bw.Write(count);
                    int buffSize = 512 * 1024;
                    byte[] buff = new byte[buffSize];
                    int sumCount = 0, currCount;
                    float progress;
                    do
                    {
                        if(worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        currCount = packet.message.Read(buff, 0, buffSize);
                        networkStream.Write(buff, 0, currCount);
                        sumCount += currCount;
                        progress = sumCount * 100f / count;
                        if(progress > 100) progress = 100;
                        worker.ReportProgress((int)progress);
                    }
                    while (sumCount < count);
                }
                else
                {
                    bw.Write(0);
                }
            }
        }
    }
}
