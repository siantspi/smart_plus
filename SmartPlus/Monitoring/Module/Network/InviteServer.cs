﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus.Network
{
    class InviteServer
    {
        public enum InviteType
        {
            /// <summary>
            /// Неопределено
            /// </summary>
            None,
            /// <summary>
            /// Приглашение пользователя в обсуждение
            /// </summary>
            InviteInTheme
        }

        MainForm mainForm;
        NetClient client;
        List<NetworkUserItem> users;
        DateTime lastInviteTime;
        Timer tmr;

        public bool IsBusy
        {
            get
            {
                return (client != null) && client.IsBusy; 
            }
        }
        int GetUserIndex(int UserId)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].Id == UserId)
                {
                    return i;
                }
            }
            return -1;
        }
        public InviteServer(MainForm mainForm, bool startInitUser, WorkProgressChangedDelegate ExtWorkProgressChanged, WorkCompleteDelegate ExtWorkComplete)
        {
            this.mainForm = mainForm;
            lastInviteTime = DateTime.MinValue;
            tmr = new Timer();
            tmr.Interval = 5000;
            tmr.Enabled = false;
            tmr.Tick += tmr_Tick;
            users = new List<NetworkUserItem>();

            client = new NetClient(mainForm.ServerName, 1992, false, null, WorkComplete);
            if (ExtWorkProgressChanged != null) client.WorkProgressChanged += ExtWorkProgressChanged;
            if (ExtWorkComplete != null) client.WorkComplete += ExtWorkComplete;
            if(startInitUser) client.InitUserId();
            tmr.Start();
        }

        void tmr_Tick(object sender, EventArgs e)
        {
            if (!client.IsBusy && client.IsInitialized && users.Count > 0)
            {
                client.GetInviteList();
            }
        }
        public void Init(int UserId, List<NetworkUserItem> userList)
        {
            client.InitUserId(UserId);
            users.AddRange(userList.ToArray());
        }

        public void SendInvite(InviteType InviteType, int UserRecieverId, int ThemeId)
        {
            client.AddInvite(InviteType, UserRecieverId, ThemeId);
        }

        string GetObjectName(Constant.BASE_OBJ_TYPES_ID ObjectType, byte regionCode, byte ngduCode, ushort oilfieldIndex, int ObjectIndex)
        {
            string result = string.Empty;
            SmartPlus.DictionaryObjects.OilFieldParamsDictionary dict = null;
            switch (ObjectType)
            {
                case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                    if (regionCode == 0)
                    {
                        result = mainForm.canv.MapProject.Name;
                    }
                    else if (regionCode == 1) // crutch
                    {
                        result = "ХМАО";
                    }
                    else if (regionCode == 2) // crutch
                    {
                        result = "Оренбургская область";
                    }
                    else // crutch
                    {
                        result = "Башнефть-Полюс";
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.NGDU:
                    result = "НГДУ ";
                    dict = (SmartPlus.DictionaryObjects.OilFieldParamsDictionary)mainForm.canv.MapProject.DictList.GetDictionary(DictionaryObjects.DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS);
                    for (int i = 0; i < dict.Count; i++)
                    {
                        if (dict[i].NGDUCode == ngduCode)
                        {
                            result += dict[i].NGDUName;
                            break;
                        }
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    result = "Мест. ";
                    if (oilfieldIndex > -1 && oilfieldIndex < mainForm.canv.MapProject.OilFields.Count)
                    {
                        result += mainForm.canv.MapProject.OilFields[oilfieldIndex].Name;
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    result = "ЯЗ: ";
                    if (oilfieldIndex > -1 && oilfieldIndex < mainForm.canv.MapProject.OilFields.Count)
                    {
                        if (ObjectIndex > -1 && ObjectIndex < mainForm.canv.MapProject.OilFields[oilfieldIndex].Areas.Count)
                        {
                            result += mainForm.canv.MapProject.OilFields[oilfieldIndex].Areas[ObjectIndex].Name;
                        }
                        result += " [" + mainForm.canv.MapProject.OilFields[oilfieldIndex].Name + "]";
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    result = "Скв: ";
                    if (oilfieldIndex > -1 && oilfieldIndex < mainForm.canv.MapProject.OilFields.Count)
                    {
                        if (ObjectIndex > -1 && ObjectIndex < mainForm.canv.MapProject.OilFields[oilfieldIndex].Wells.Count)
                        {
                            result += mainForm.canv.MapProject.OilFields[oilfieldIndex].Wells[ObjectIndex].Name;
                        }
                        result += " [" + mainForm.canv.MapProject.OilFields[oilfieldIndex].Name + "]";
                    }
                    break;
            }
            return result;
        }

        public void WorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                tmr.Enabled = false;
            }
            else if (e.Cancelled)
            {
                tmr.Enabled = false;
            }
            else if (e.Result != null)
            {
                int count;
                Network.NetPacket packet = (Network.NetPacket)e.Result;
                switch (packet.result)
                {
                    case NetPacket.Result.Error:
                        MessageBox.Show(mainForm, "Ошибка обработки запроса на сервере!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case SmartPlus.Network.NetPacket.Result.Succes:
                        try
                        {
                            string res = string.Empty;
                            if (packet.type == SmartPlus.Network.NetPacket.Type.GetUserId)
                            {
                                client.GetUserList();
                                return;
                            }
                            if (packet.message != null)
                            {
                                BinaryReader br = new BinaryReader(packet.message, Encoding.UTF8);
                                switch (packet.type)
                                {
                                    case SmartPlus.Network.NetPacket.Type.GetUserList:
                                        count = br.ReadInt32();
                                        users.Clear();
                                        for (int i = 0; i < count; i++)
                                        {
                                            var user = new NetworkUserItem();
                                            user.Id = br.ReadInt32();
                                            user.ADName = br.ReadString();
                                            user.ADJob = br.ReadString();
                                            user.ADOffice = br.ReadString();
                                            br.BaseStream.Seek(1, SeekOrigin.Current); // online - status
                                            users.Add(user);
                                        }
                                        break;
                                    case NetPacket.Type.GetInviteList:
                                        string str;
                                        int ind;
                                        count = br.ReadInt32();
                                        for (int i = 0; i < count; i++)
                                        {
                                            var invite = new NetworkInviteItem();
                                            invite.Read(br);
                                            ind = GetUserIndex(invite.UserSenderId);
                                            string objName = GetObjectName(invite.ObjectType, invite.RegionCode, invite.NGDUCode, invite.OilfieldIndex, invite.ObjectIndex);
                                            str = string.Format("Пользователь {0}\nприглашает обсудить тему\n'{1}'\n{2}\nДата приглашения:  {3:dd.MM.yyyy HH:mm}", ((ind == -1) ? string.Empty : users[ind].ADName), invite.Text, objName, invite.TimeSend, invite.TimeSend);
                                            ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INVITE_IN_THEME, "Внимание!", str, invite);
                                            if (lastInviteTime < invite.TimeSend) lastInviteTime = invite.TimeSend;
                                        }
                                        if (count > 0) client.RemoveInviteList(lastInviteTime);
                                        break;
                                }
                            }
                        }
                        catch
                        {
                            MessageBox.Show(mainForm, "Ошибка обработки результатов запроса сервера!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                }
            }
        }
    }
}
