﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus.Network
{
    class DiscussLetterBox
    {
        MainForm mainForm;
        NetClient client;
        Timer updateTimer;

        DateTime lastUpdateTime;
        public DiscussLetterBox(MainForm mainForm)
        {
            this.mainForm = mainForm;
            client = new NetClient(mainForm.ServerName, 1992, false, WorkProgressChanged, WorkComplete);
            lastUpdateTime = DateTime.MinValue;

            updateTimer = new Timer();
            updateTimer.Interval = 5000;
            updateTimer.Enabled = false;
            updateTimer.Tick += updateTimer_Tick;
        }

        void updateTimer_Tick(object sender, EventArgs e)
        {
            Update();
        }

        public bool IsBusy
        {
            get { return client.IsBusy; }
        }
        public void StartUpdate()
        {
            updateTimer.Start();
        }
        public void StopUpdate()
        {
            client.Clear();
            updateTimer.Stop();
            lastUpdateTime = DateTime.MinValue;
        }

        public void Update()
        {
            if (!updateTimer.Enabled) updateTimer.Start();

            if (!client.IsBusy)
            {
                if (!client.IsInitialized)
                {
                    mainForm.stBar2.Visible = true;
                    mainForm.stBar2.Text = "Идет загрузка актуальных тем сообщений...";
                    client.InitUserId();
                }
                else
                {
                    client.GetLastThemeList(lastUpdateTime);
                }
            }
        }
        public void WorkProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        public void WorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            mainForm.stBar2.Text = string.Empty;
            mainForm.stBar2.Visible = false;
            if (e.Error != null)
            {
                StopUpdate();
                mainForm.SetShowDiscussLetterBoxBtnChecked(false);
                ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.ERROR, "Ошибка!", "Не удается подключиться к серверу\nдля загрузки активных тем обсуждений.");
                mainForm.canv.DrawLayerList();
            }
            else if (e.Cancelled)
            {
                StopUpdate();
                mainForm.SetShowDiscussLetterBoxBtnChecked(false);
            }
            else if (e.Result != null)
            {
                Network.NetPacket packet = (Network.NetPacket)e.Result;
                switch (packet.result)
                {
                    case NetPacket.Result.Timeout:
                        StopUpdate();
                        mainForm.SetShowDiscussLetterBoxBtnChecked(false);
                        ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.ERROR, "Ошибка!", "Таймаут соединения с сервером!");
                        break;
                    case NetPacket.Result.Error:
                        StopUpdate();
                        mainForm.SetShowDiscussLetterBoxBtnChecked(false);
                        ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.ERROR, "Ошибка!", "Ошибка обработки запроса на сервере!");
                        break;
                    case SmartPlus.Network.NetPacket.Result.Succes:
                        try
                        {
                            string res = string.Empty;

                            if (packet.type == SmartPlus.Network.NetPacket.Type.GetUserId)
                            {
                                client.GetLastThemeList(lastUpdateTime);
                            }
                            else if (packet.message != null)
                            {
                                BinaryReader br = new BinaryReader(packet.message, Encoding.UTF8);
                                switch (packet.type)
                                {
                                    case NetPacket.Type.GetLastThemeList:
                                        CanvasMap canv = mainForm.canv;
                                        Constant.BASE_OBJ_TYPES_ID objType;
                                        byte regionCode;
                                        byte ngduCode;
                                        ushort oilfieldIndex;
                                        int ObjIndex;
                                        DateTime lastDate;
                                        int level;
                                        int count = br.ReadInt32();
                                        for (int i = 0; i < count; i++)
                                        {
                                            objType = (Constant.BASE_OBJ_TYPES_ID)br.ReadInt32();
                                            regionCode = br.ReadByte();
                                            ngduCode = br.ReadByte();
                                            oilfieldIndex = br.ReadUInt16();
                                            ObjIndex = br.ReadInt32();
                                            lastDate = DateTime.FromOADate((br.ReadDouble()));
                                            if (lastUpdateTime < lastDate) lastUpdateTime = lastDate;
                                            int days = (DateTime.Now - lastDate).Days;
                                            if (days < 1)
                                            {
                                                level = 2;
                                            }
                                            else if (days < 7)
                                            {
                                                level = 1;
                                            }
                                            else
                                            {
                                                level = 0;
                                            }
                                            OilField of;
                                            Contour cntr;
                                            if (objType == Constant.BASE_OBJ_TYPES_ID.PROJECT)
                                            {
                                                of = mainForm.canv.MapProject.OilFields[0];
                                                for (int j = 0; j < of.Contours.Count; j++)
                                                {
                                                    cntr = of.Contours[j];
                                                    if (cntr._ContourType != -1) continue;
                                                    if (cntr._EnterpriseCode == regionCode)
                                                    {
                                                        cntr.LetterBoxLevel = level;
                                                        break;
                                                    }
                                                }
                                            }
                                            else if (objType == Constant.BASE_OBJ_TYPES_ID.NGDU)
                                            {
                                                of = mainForm.canv.MapProject.OilFields[0];
                                                for (int j = 0; j < of.Contours.Count; j++)
                                                {
                                                    cntr = of.Contours[j];
                                                    if (cntr._ContourType != -2) continue;
                                                    if (cntr._NGDUCode == ngduCode)
                                                    {
                                                        cntr.LetterBoxLevel = level;
                                                        break;
                                                    }
                                                }
                                            }
                                            else if (objType == Constant.BASE_OBJ_TYPES_ID.OILFIELD)
                                            {
                                                if (oilfieldIndex >= canv.MapProject.OilFields.Count) continue;
                                                of = mainForm.canv.MapProject.OilFields[oilfieldIndex];
                                                for (int j = 0; j < of.Contours.Count; j++)
                                                {
                                                    cntr = of.Contours[j];
                                                    if (cntr._ContourType == -3)
                                                    {
                                                        cntr.LetterBoxLevel = level;
                                                        break;
                                                    }
                                                }
                                            }
                                            else if (objType == Constant.BASE_OBJ_TYPES_ID.AREA)
                                            {
                                                if (oilfieldIndex == 0 || oilfieldIndex >= canv.MapProject.OilFields.Count || ObjIndex == -1) continue;
                                                of = mainForm.canv.MapProject.OilFields[oilfieldIndex];
                                                if (ObjIndex < of.Areas.Count)
                                                {
                                                    of.Areas[ObjIndex].contour.LetterBoxLevel = level;
                                                }
                                            }
                                            else if (objType == Constant.BASE_OBJ_TYPES_ID.WELL)
                                            {
                                                if (oilfieldIndex == 0 || oilfieldIndex >= canv.MapProject.OilFields.Count || ObjIndex == -1) continue;
                                                of = mainForm.canv.MapProject.OilFields[oilfieldIndex];
                                                if (ObjIndex < of.Wells.Count)
                                                {
                                                    of.Wells[ObjIndex].LetterBoxLevel = level;
                                                }
                                            }
                                        }
                                        canv.DrawLayerList();
                                        break;
                                }
                            }
                        }
                        catch
                        {
                            StopUpdate();
                            MessageBox.Show(mainForm, "Ошибка обработки результатов запроса сервера!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                }
            }
            mainForm.SetShowDiscussLetterBoxBtnEnable(true);
        }
    }
}
