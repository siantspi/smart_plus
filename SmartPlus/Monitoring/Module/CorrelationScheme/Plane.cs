﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using RDF.Objects;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.IO;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    // ПЛАНШЕТ скважины
    public sealed class Plane
    {
        public bool PlaneMode;
        Plane BaseScheme;
        // Drawing
        float YUnits;
        SizeF size;
        PointF hotSpot, coord;
        Pen penFat;
        public PointF Coord
        {
            get { return coord; }
            set
            {
                coord = value;
                PointF coordPt = value;
                coordPt.Y += WellHeadYOffset;
                for (int i = 0; i < Count; i++)
                {
                    Columns[i].Coord = coordPt;
                    if (Columns[i].Visible) coordPt.X += Columns[i].Width;
                }
            }
        }
        public PointF HotSpot
        {
            get { return hotSpot; }
            set
            {
                hotSpot = value;
                for (int i = 0; i < Count; i++)
                {
                    Columns[i].HotSpot = value;
                }
            }
        }
        public Size WindowSize;
        bool ShowMarkers;
        public bool ShowCloseButton;
        public bool ShowMarkersConnection;
        public bool ShowMarkersInfo;
        public float WellHeadYOffset;
        public float ColumnHeadYOffset
        {
            get 
            {
                if (Count > 0)
                {
                    if (Columns[0].IsHeadDrawing)
                    {
                        return Columns[0].HeadTextYOffset;
                    }
                }
                return 0;
            }
        }
        public float YUnitsInDepthUnit
        {
            get
            {
                return YUnits;
            }
            set
            {
                if (Columns != null)
                {
                    YUnits = value;
                    for (int i = 0; i < Columns.Length; i++)
                    {
                        Columns[i].Height = DataHeight;
                        Columns[i].YUnitsInDepthUnit = value;
                    }
                }
            }
        }
        public float Width 
        {
            get { return size.Width; }
        }
        public float MinWidth
        {
            get
            {
                float min = 0;
                for (int i = 0; i < Count; i++)
                {
                    if (Columns[i].MinWidth == -1)
                    {
                        min = -1;
                        break;
                    }
                    if(Columns[i].Visible) min += Columns[i].MinWidth;
                }
                return min;
            }
        }
        public float MaxWidth
        {
            get
            {
                float max = 0;
                for (int i = 0; i < Count; i++)
                {
                    if (Columns[i].MaxWidth == -1)
                    {
                        max = -1;
                        break;
                    }
                    if (Columns[i].Visible) max += Columns[i].MaxWidth;
                }
                return max;
            }
        }
        public int TopSpace;
        public int BottomSpace;
        public float DataHeight;

        public float ScreenMinY
        {
            set
            {
                for (int i = 0; i < Count; i++)
                {
                    Columns[i].ScreenMinY = value;
                }
            }
        }
        public float ScreenMaxY
        {
            set
            {
                for (int i = 0; i < Count; i++)
                {
                    Columns[i].ScreenMaxY = value;
                }
            }
        }
        Font fontWellName, fontMarkerName, fontMarkerNameBold;
        
        // Data
        public Well w;
        public string Name;
        public int Count
        {
            get
            {
                return Columns.Length;
            }
        }
        public float MinDepth, MaxDepth;
        public float RangeDepth;

        public PlaneMarker[] Markers;
        public List<PlaneMarkerZone> MarkerZones;
        public PlaneColumn[] Columns;

        public bool InitDepthByTrajectory
        {
            get { return depthCalculator.InitFromTrajectory; }
            set { depthCalculator.InitFromTrajectory = value; }
        }
        DepthCalculator depthCalculator;

        // READ/WRITE TO STREAM
        public bool ReadFromStream(BinaryReader br, int Version)
        {
            if (br != null)
            {
                this.WellHeadYOffset = br.ReadSingle();
                this.Name = br.ReadString();
                int colCount = br.ReadInt32();
                Columns = new PlaneColumn[colCount];
                for (int i = 0; i < colCount; i++)
                {
                    Columns[i] = new PlaneColumn();
                    Columns[i].ReadFromStream(br);
                }
                this.TopSpace = br.ReadInt32();
                this.BottomSpace = br.ReadInt32();
                if (Version > 2) this.ShowMarkersConnection = br.ReadBoolean();
                if (Version > 3) this.ShowMarkersInfo = br.ReadBoolean();
                if (Version > 4) this.InitDepthByTrajectory = br.ReadBoolean();
                SetWidth();
                return true;
            }
            return false;
        }
        public void WriteToStream(BinaryWriter bw)
        {
            bw.Write(this.WellHeadYOffset);
            bw.Write(this.Name);
            bw.Write(Columns.Length);
            for (int i = 0; i < Columns.Length; i++)
            {
                Columns[i].WriteToStream(bw);
            }
            bw.Write(this.TopSpace);
            bw.Write(this.BottomSpace);
            bw.Write(this.ShowMarkersConnection);
            bw.Write(this.ShowMarkersInfo);
            bw.Write(this.InitDepthByTrajectory);
        }

        // GET DATA
        public PlaneBaseObject GetObjectByType(Constant.Plane.ObjectType Type)
        {
            PlaneBaseObject obj = null;
            for (int i = 0; i < Count; i++)
            {
                obj = Columns[i].GetObjectByType(Type);
                if (obj != null) return obj;
            }
            return obj;
        }

        // GET TOOLTIP
        public PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y, bool ShiftPressed)
        {
            PlaneToolTip toolTip = null, resToolTip = null;
            if ((this.hotSpot.X + this.Coord.X - 20 < X) && (X < this.hotSpot.X + this.Coord.X + this.Width + 20))
            {
                for (int i = 0; i < Count; i++)
                {
                    toolTip = Columns[i].GetToolTip(DictList, X, Y, ShiftPressed);
                    if ((toolTip != null) && ((resToolTip == null) || (resToolTip.ScreenDist > toolTip.ScreenDist)))
                    {
                        resToolTip = toolTip;
                        resToolTip.Plane = this;
                    }
                }
            }
            return resToolTip;
        }

        // MARKER
        public void SetMarkerDepth(Marker mrk, float Depth)
        {
            if (Markers != null)
            {
                for (int i = 0; i < Markers.Length; i++)
                {
                    if (Markers[i].srcMarker == mrk)
                    {
                        Markers[i].Depth = Depth;
                        break;
                    }
                }
            }
        }
        public void LoadMarkers()
        {
            if (Markers != null)
            {
                for (int i = 0; i < Markers.Length; i++) Markers[i] = null;
            }
            this.Markers = null;
            if ((this.w != null) && (w.MarkerCount > 0))
            {
                Markers = new PlaneMarker[w.MarkerCount];
                Marker mrk;
                for (int i = 0; i < w.MarkerCount; i++)
                {
                    Markers[i] = new PlaneMarker();
                    mrk = w.GetMarker(i);
                    Markers[i].srcMarker = mrk;
                    Markers[i].Depth = mrk.GetDepthByWell(this.w);
                    Markers[i].pen = new Pen(mrk.PenBound.Color, 2f);
                    Markers[i].pen_selected = new Pen(mrk.PenBound.Color, 3f);
                    Markers[i].brush_text = Brushes.Black;
                }
            }
        }
        public void RemoveMarker(Marker mrk)
        {
            if (Markers != null)
            {
                int ind = -1;
                for (int i = 0; i < Markers.Length; i++)
                {
                    if (Markers[i].srcMarker == mrk)
                    {
                        ind = i;
                        break;
                    }
                }
                if (ind > -1)
                {
                    if (Markers.Length > 1)
                    {
                        PlaneMarker[] newMarkers = new PlaneMarker[Markers.Length - 1];
                        for (int i = 0, j = 0; i < Markers.Length; i++)
                        {
                            if (i != ind)
                            {
                                newMarkers[j] = Markers[i];
                                j++;
                            }
                        }
                        Markers = newMarkers;
                    }
                    else
                    {
                        Markers[0] = null;
                        Markers = null;
                    }
                }
            }
        }
        public float GetMarkerDepth(Marker mrk)
        {
            if (Markers != null)
            {
                for (int i = 0; i < Markers.Length; i++)
                {
                    if (Markers[i].srcMarker == mrk)
                    {
                        return Markers[i].Depth;
                    }
                }
            }
            return -1;
        }

        // METHODS
        public Plane(bool PlaneMode)
        {
            this.PlaneMode = PlaneMode;
            this.BaseScheme = null;
            w = null;
            Name = null;
            ShowMarkers = true;
            ShowMarkersConnection = false;
            ShowMarkersInfo = true;
            Markers = null;
            Columns = null;
            MarkerZones = null;
            fontWellName = new Font("Tahoma", 10f, FontStyle.Bold);
            fontMarkerName = new Font("Calibri", 8f);
            fontMarkerNameBold = new Font("Calibri", 8f, FontStyle.Bold);
            ShowCloseButton = false;
            penFat = new Pen(Color.Black, 2);
            this.TopSpace = 50;
            this.BottomSpace = 50;
            depthCalculator = new DepthCalculator();
        }
        public Plane(Plane Scheme, bool PlaneMode) : this(PlaneMode)
        {
            if ((Scheme != null) && (Scheme.Count > 0))
            {
                this.BaseScheme = Scheme;
                WellHeadYOffset = Scheme.WellHeadYOffset;
                ShowMarkers = Scheme.ShowMarkers;
                ShowMarkersConnection = Scheme.ShowMarkersConnection;
                ShowMarkersInfo = Scheme.ShowMarkersInfo;
                Columns = new PlaneColumn[Scheme.Count];
                this.size.Width = 0;
                this.TopSpace = Scheme.TopSpace;
                this.BottomSpace = Scheme.BottomSpace;
                for (int i = 0; i < Count; i++)
                {
                    Columns[i] = new PlaneColumn(Scheme.Columns[i]);
                    if (Columns[i].Visible) this.size.Width += Columns[i].Width;
                }
                InitDepthByTrajectory = BaseScheme.InitDepthByTrajectory;
                SetDepthCalculator(this.depthCalculator);
            }
        }
        public Plane Clone()
        {
            Plane plane = new Plane(this, this.PlaneMode);
            
            plane.YUnits = YUnits;
            plane.size = size;
            plane.hotSpot = hotSpot;
            plane.coord = coord;
            plane.penFat = new Pen(penFat.Brush, penFat.Width);
            plane.WindowSize = WindowSize;
            plane.ShowMarkers = ShowMarkers;
            plane.ShowMarkersConnection = ShowMarkersConnection;
            plane.ShowMarkersInfo = ShowMarkersInfo;
            plane.ShowCloseButton = ShowCloseButton;
            plane.WellHeadYOffset = WellHeadYOffset;
            plane.DataHeight = DataHeight;
            plane.w = w;
            plane.Name = Name;
            plane.TopSpace = this.TopSpace;
            plane.BottomSpace = this.BottomSpace;
            if (Markers != null)
            {
                plane.Markers = new PlaneMarker[Markers.Length];
                for (int i = 0; i < Markers.Length; i++)
                {
                    plane.Markers[i] = Markers[i];
                }
            }
            if(Columns != null)
            {
                plane.Columns = new PlaneColumn[Columns.Length];
                for (int i = 0; i < Columns.Length; i++)
                {
                    plane.Columns[i] = new PlaneColumn(Columns[i]);
                }
            }
            plane.SetDepthCalculator(new DepthCalculator(this.depthCalculator));
            plane.SetWidth();
            return plane;
        }
        public void SetBaseScheme(Plane Scheme) { this.BaseScheme = Scheme; }
        
        // UPDATE PLANE
        public void UpdateSettingsByBaseScheme()
        {
            if (BaseScheme != null)
            {
                float realWidth = 0;
                for (int i = 0; i < Columns.Length; i++) realWidth += Columns[i].RealWidth;
                ShowMarkers = BaseScheme.ShowMarkers;
                ShowMarkersConnection = BaseScheme.ShowMarkersConnection;
                ShowMarkersInfo = BaseScheme.ShowMarkersInfo;
                InitDepthByTrajectory = BaseScheme.InitDepthByTrajectory;
                for (int i = 0; i < Columns.Length; i++)
                {
                    Columns[i].MaxWidth = BaseScheme.Columns[i].MaxWidth;
                    Columns[i].MinWidth = BaseScheme.Columns[i].MinWidth;
                    for (int j = 0; (j < Columns[i].Count) && (j < BaseScheme.Columns[i].Count); j++)
                    {
                        Columns[i][j].UpdateByObject(BaseScheme.Columns[i][j]);
                    }
                    Columns[i].Visible = BaseScheme.Columns[i].Visible;
                }
                this.TopSpace = BaseScheme.TopSpace;
                this.BottomSpace = BaseScheme.BottomSpace;
                SetNewWidth(BaseScheme.Width);
            }
        }

        // SHOW HIDE DATA
        public bool TestContainData(Constant.Plane.ObjectType ObjType)
        {
            if (!depthCalculator.InitFromTrajectory && ObjType == Constant.Plane.ObjectType.GIS)
            {
                return true;
            }
            if (Columns != null)
            {
                bool result = false;
                for (int i = 0; i < Columns.Length; i++)
                {
                    result = Columns[i].TestContainData(ObjType);
                    if (result) return result;
                }
            }
            return false;
        }
        public void ClearSelectedData()
        {
            for (int i = 0; i < Count; i++)
            {
                Columns[i].ClearSelectedData();
            }
        }
        public bool SetSelectedDepth(Constant.Plane.ObjectType Type, float DepthMD)
        {
            bool res = false;
            for (int i = 0; i < Count; i++)
            {
                res = (Columns[i].SetSelectedDepth(Type, DepthMD) || res);
            }
            return res;
        }
        public void SetShowOilObjName(bool ShowOilObjName)
        {
            for (int i = 0; i < Count; i++)
            {
                Columns[i].SetShowOilObjName(ShowOilObjName);
            }
        }
        public void SetVisibleByType(Constant.Plane.ObjectType Type, bool Visible)
        {
            if (Type != Constant.Plane.ObjectType.MARKER)
            {
                this.size.Width = 0;
                for (int i = 0; i < Count; i++)
                {
                    Columns[i].SetVisibleByType(Type, Visible);
                    if (Columns[i].Visible) this.size.Width += Columns[i].Width;
                }
            }
            else
            {
                this.ShowMarkers = Visible;
            }
        }
        public int GetVisibleByType(Constant.Plane.ObjectType Type)
        {
            if (Type != Constant.Plane.ObjectType.MARKER)
            {
                int res = -1;
                for (int i = 0; i < Count; i++)
                {
                    res = Columns[i].GetVisibleByType(Type);
                    if (res != -1) return res;
                }
                return res;
            }
            else
            {
                return (this.ShowMarkers ? 1 : 0);
            }
        }
        public void SetShowColumnHead(bool ShowColumnHead)
        {
            for (int i = 0; i < Count; i++)
            {
                Columns[i].SetShowColumnHead(ShowColumnHead);
            }
            SetPlaneRect();
        }
        public void SetDepthByAbsMd(bool ShowDepthAbs)
        {
            for (int i = 0; i < Count; i++)
            {
                Columns[i].SetDepthByAbsMd(ShowDepthAbs);
            }
        }
        public bool SetShowDuplicates
        {
            set
            {
                for (int i = 0; i < Count; i++)
                {
                    Columns[i].ShowDuplicatesLog(value);
                }
            }
        }

        // DEPTH
        void SetDepthCalculator(DepthCalculator DepthCalc)
        {
            this.depthCalculator = DepthCalc;
            if (Columns != null)
            {
                for (int i = 0; i < Columns.Length; i++)
                {
                    Columns[i].SetDepthCalc(DepthCalc);
                }
            }
        }
        public float DepthFromScreenY(float screenY)
        {
            return this.MinDepth + (screenY - HotSpot.Y - WellHeadYOffset - ColumnHeadYOffset) / YUnitsInDepthUnit;
        }
        public float ScreenYFromDepth(float depth)
        {
            return (depth - this.MinDepth) * YUnitsInDepthUnit + HotSpot.Y + WellHeadYOffset + ColumnHeadYOffset;
        }
        public float GetMDByAbs(float AbsDepth)
        {
            float depth = -1;
            for (int i = 0; i < Count; i++)
            {
                depth = Columns[i].GetMDByAbs(AbsDepth);
                if (depth != -1)
                {
                    return depth;
                }
            }
            return depth;
        }
        public float GetMedianDepth()
        {
            float depth = -1;
            for (int i = 0; i < Count; i++)
            {
                depth = Columns[i].GetMedianDepth();
                if (depth != -1)
                {
                    return depth;
                }
            }
            return -1;
        }
        public float GetLastPerfDepth()
        {
            float depth = -1, d;
            for (int i = 0; i < Count; i++)
            {
                d = Columns[i].GetLastPerfDepth();
                if (d != -1)
                {
                    depth = d;
                    break;
                }
            }
            return depth;
        }

        public void LoadSourceWellData(BackgroundWorker worker, DoWorkEventArgs e, Project proj)
        {
            if (this.w != null)
            {
                depthCalculator.InitDepth(w);

                int progress = 0;
                for (int i = 0; i < Count; i++)
                {
                    if ((worker != null) && (worker.CancellationPending))
                    {
                        if (!PlaneMode)
                        {
                            w.perf = null;
                            w.gis = null;
                        }
                        w.logs = null;
                        w.logsBase = null;
                        
                        e.Cancel = true;
                        return;
                    }
                    Columns[i].LoadData(worker, e, proj, w);
                    progress = (int)((float)i * 100 / (float)Count);
                    if (worker != null) worker.ReportProgress(progress);
                }
                LoadMarkers();
                if (!PlaneMode)
                {
                    // очистка памяти после загрузки данных
                    w.perf = null;
                    w.gis = null;
                }
                if (!proj.OilFields[w.OilFieldIndex].WellTrajectoryLoaded)
                {
                    w.trajectory = null;
                }
                w.logs = null;
                w.logsBase = null;
            }
        }

        public bool TestCloseButton(Graphics grfx, float X, float Y)
        {
            if (!PlaneMode)
            {
                float Right = hotSpot.X + coord.X + Width;
                if ((X > Right - 17) && (X < Right - 2) && (Y > 2) && (Y < 17))
                {
                    grfx.FillRectangle(Brushes.DarkGray, Right - 17, 2, 15, 15);
                    grfx.DrawRectangle(Pens.Black, Right - 17, 2, 15, 15);
                    grfx.DrawImage(Properties.Resources.delete, Right - 17, 2);
                    ShowCloseButton = true;
                    return true;
                }
            }
            ShowCloseButton = false;
            return false;
        }
        public void DrawCloseBtn(Graphics grfx)
        {
            float Right = hotSpot.X + coord.X + Width;
            grfx.FillRectangle(Brushes.LightGray, Right - 17, 2, 15, 15);
            grfx.DrawRectangle(Pens.Black, Right - 17, 2, 15, 15);
            grfx.DrawImage(Properties.Resources.delete, Right - 17, 2);
        }
        public void SetGraphicsSizes(Graphics grfx)
        {
            if ((w != null) && (w.UpperCaseName != ""))
            {
                SizeF size = grfx.MeasureString(w.UpperCaseName, fontWellName, PointF.Empty, StringFormat.GenericTypographic);
                if (size.Height >= WellHeadYOffset)
                {
                    WellHeadYOffset = (int)(size.Height) + 4;
                }
            }
            for (int i = 0; i < Count; i++)
            {
                Columns[i].SetGraphicsSizes(grfx);
            }
        }
        public int GetMouseOverColumn(float X, float Y)
        {
            int res = -1;
            if ((Columns != null) && (Count > 0))
            {
                for (int i = 0; i < Count; i++)
                {
                    if ((hotSpot.X + Columns[i].Coord.X < X) && (X < hotSpot.X + Columns[i].Coord.X + Columns[i].Width))
                    {
                        res = i;
                    }
                }
            }
            return res;
        }
        public void SetPlaneRect()
        {
            RectangleF rect = new RectangleF(coord.X, coord.Y, (int)this.Width, (int)(WellHeadYOffset + DataHeight));
            for (int i = 0; i < Count; i++)
            {
                Columns[i].PlaneRect = rect;
            } 
        }

        // WIDTH
        public void SetWidth()
        {
            int i;
            this.size.Width = 0;
            for (i = 0; i < Count; i++)
            {
                if (Columns[i].Visible)
                {
                    this.size.Width += Columns[i].Width;
                }
            }
        }
        public void ScaleWidth(float dScale)
        {
            SetNewWidth(dScale * this.Width);
        }
        public void SetNewWidth(float newWidth)
        {
            float part, sumPart = 0;
            float dScale = newWidth - this.Width;
            bool limitWidth = false;

            float max = this.MaxWidth, min = this.MinWidth;
            if ((max != -1) && (dScale > 0)) limitWidth = (this.Width >= max);
            if ((min != -1) && (dScale < 0) && (!limitWidth)) limitWidth = (this.Width <= min);
            if (!limitWidth)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (i < Count - 1)
                    {
                        part = Columns[i].RealWidth * dScale / this.Width;
                        sumPart += Columns[i].ScaleWidth(part);
                    }
                    else
                    {
                        part = dScale - sumPart;
                        sumPart += Columns[i].ScaleWidth(part);
                    }
                }
            }
            this.SetWidth();
            this.SetPlaneRect();
        }
        public void SetBindWidth(float newWidth)
        {
            float part, sumPart = 0;

            for (int i = 0; i < Count; i++)
            {
                if (i < Count - 1)
                {
                    part = Columns[i].MaxWidth;
                    Columns[i].SetBindWidth(part);
                    sumPart += part;
                }
                else
                {
                    part = newWidth - sumPart;
                    Columns[i].SetBindWidth(part);
                }
            }
            this.SetWidth();
            this.SetPlaneRect();
        }
        public float GetWidthByColumnIndex(int index)
        {
            float res = 0;
            for (int i = 0; i < Count; i++)
            {
                if (i == index) break;
                if(Columns[i].Visible) res += Columns[i].Width;
            }
            return res;
        }

        // DRAWING
        public void DrawHead(Graphics grfx)
        {
            float X0 = hotSpot.X + coord.X, X;
            if(!w.SelectedByProfile)
                grfx.FillRectangle(Brushes.LightGray, X0 + 1, 0, Width - 1, WellHeadYOffset);
            else
                grfx.FillRectangle(Brushes.Lime, X0 + 1, 0, Width - 1, WellHeadYOffset);

            
            if ((w != null) && (w.UpperCaseName != ""))
            {
                SizeF size = grfx.MeasureString(w.UpperCaseName, fontWellName, PointF.Empty, StringFormat.GenericTypographic);
                if (size.Height < WellHeadYOffset)
                {
                    float Y;
                    X = X0 + Width / 2 - size.Width / 2;
                    if (X < hotSpot.X + coord.X) X = hotSpot.X + coord.X;

                    Y = WellHeadYOffset / 2 - size.Height / 2;
                    grfx.DrawString(w.Name, fontWellName, Brushes.Black, X, Y, StringFormat.GenericTypographic);
                }
                if(!PlaneMode) DrawCloseBtn(grfx);
            }
            grfx.DrawRectangle(Pens.Black, X0, 0, Width, WellHeadYOffset);

            for (int i = 0; i < Count; i++)
            {
                Columns[i].DrawHead(grfx);
            }
        }
        public void DrawMarkers(Graphics grfx, Plane BeforePlane)
        {
            if ((ShowMarkers) && (Markers != null))
            {
                float X = hotSpot.X + coord.X, X2 = X + this.Width;
                float Y0 = hotSpot.Y + coord.Y + WellHeadYOffset + ColumnHeadYOffset, Y;
                PlaneMarker mrk;
                SizeF size;
                Pen pen;
                Font font;
                float dx = 0, dY = 1;
                bool showDepth = GetVisibleByType(Constant.Plane.ObjectType.DEPTH_MARKS) == 1;
                PlaneBaseObject objDepth = GetObjectByType(Constant.Plane.ObjectType.DEPTH_MARKS);
                bool drawConnection = false;
                for (int i = 0; i < Markers.Length; i++)
                {
                    mrk = Markers[i];
                    if (mrk.srcMarker.Visible)
                    {
                        dY = mrk.pen_selected.Width / 2;
                        if (!mrk.srcMarker.Selected)
                        {
                            font = fontMarkerName;
                            pen = mrk.pen;
                        }
                        else
                        {
                            font = fontMarkerNameBold;
                            pen = mrk.pen_selected;
                        }
                        Y = Y0 + (mrk.Depth - MinDepth) * YUnitsInDepthUnit;
                        if (Y < Y0) continue;
                        if (Y > Y0 + this.DataHeight + WellHeadYOffset + ColumnHeadYOffset) break;

                        if (showDepth && (BeforePlane != null) && (objDepth != null))
                        {
                            float bef = BeforePlane.GetMarkerDepth(mrk.srcMarker);
                            if (bef > 0)
                            {
                                float befY = Y0 + (bef - BeforePlane.MinDepth) * YUnitsInDepthUnit;
                                grfx.DrawLine(pen, X, befY, X + objDepth.Width, Y);
                                drawConnection = true;
                            }
                        }
                        dx = drawConnection ? objDepth.Width : 0;
                        grfx.DrawLine(pen, X + dx, Y, X2, Y);
                        if (mrk.srcMarker.Selected)
                        {
                            grfx.DrawLine(Pens.Black, X + dx, Y - dY, X2, Y - dY);
                            grfx.DrawLine(Pens.Black, X + dx, Y + dY, X2, Y + dY);
                        }
                        if (ShowMarkersInfo)
                        {
                            size = grfx.MeasureString(mrk.srcMarker.Name, font, PointF.Empty, StringFormat.GenericTypographic);
                            if (size.Height == 0) size += grfx.MeasureString(mrk.Depth.ToString("0.0"), font, PointF.Empty, StringFormat.GenericTypographic);
                            if (size.Width <= (X2 - X) * 3 / 4)
                            {
                                C2DObject.DrawWhiteGroundString(grfx, mrk.srcMarker.Name, font, mrk.brush_text, X2 - size.Width - 1, Y - size.Height, StringFormat.GenericTypographic);
                                C2DObject.DrawWhiteGroundString(grfx, mrk.Depth.ToString("0.0"), font, mrk.brush_text, X + 7 + dx, Y - size.Height, StringFormat.GenericTypographic);
                            }
                        }
                    }
                }
            }
        }
        public void DrawMarkersZones(Graphics grfx, Plane BeforePlane)
        {
            if ((ShowMarkers) && (MarkerZones != null) && (MarkerZones.Count > 0))
            {
                float X = hotSpot.X + coord.X, X2 = X + this.Width;
                float Y0 = hotSpot.Y + coord.Y + WellHeadYOffset + ColumnHeadYOffset, Y1, Y2, temp;
                PlaneMarkerZone zone;
                float depth;
                bool showDepth = GetVisibleByType(Constant.Plane.ObjectType.DEPTH_MARKS) == 1;
                PlaneBaseObject objDepth = GetObjectByType(Constant.Plane.ObjectType.DEPTH_MARKS);
                bool drawConnection = false;
                for (int i = 0; i < MarkerZones.Count; i++)
                {
                    zone = MarkerZones[i];
                    if (zone.MarkerTop.srcMarker.Visible && zone.MarkerBottom.srcMarker.Visible)
                    {
                        depth = zone.MarkerTop.srcMarker.GetDepthByWell(this.w);
                        if (depth == -1) continue;
                        Y1 = Y0 + (depth - MinDepth) * YUnitsInDepthUnit;
                        if (Y1 > Y0 + this.DataHeight + WellHeadYOffset + ColumnHeadYOffset) break;
                        depth = zone.MarkerBottom.srcMarker.GetDepthByWell(this.w);
                        if (depth == -1) continue;
                        Y2 = Y0 + (depth - MinDepth) * YUnitsInDepthUnit;
                        if (Y2 < Y0) continue;
                        if (Y2 < Y1)
                        {
                            temp = Y2;
                            Y2 = Y1;
                            Y1 = temp;
                        }
                        if (showDepth && (BeforePlane != null) && (objDepth != null))
                        {
                            float befTop = BeforePlane.GetMarkerDepth(zone.MarkerTop.srcMarker);
                            float befBottom = BeforePlane.GetMarkerDepth(zone.MarkerBottom.srcMarker);
                            if ((befTop > 0) && (befBottom > 0))
                            {
                                float befY1 = Y0 + (befTop - BeforePlane.MinDepth) * YUnitsInDepthUnit;
                                float befY2 = Y0 + (befBottom - BeforePlane.MinDepth) * YUnitsInDepthUnit;
                                if (befY2 < befY1)
                                {
                                    temp = befY2;
                                    befY2 = befY1;
                                    befY1 = temp;
                                }
                                PointF[] polygon = new PointF[4];
                                polygon[0] = new PointF(X, befY1);
                                polygon[1] = new PointF(X + objDepth.Width, Y1);
                                polygon[2] = new PointF(X + objDepth.Width, Y2);
                                polygon[3] = new PointF(X, befY2);
                                grfx.FillPolygon(zone.FillBrush, polygon);
                                drawConnection = true;
                            }
                        }
                        float dx = drawConnection ? objDepth.Width : 0;
                        grfx.FillRectangle(zone.FillBrush, X + dx, Y1, this.Width - dx, Y2 - Y1);
                    }
                }
            }
        }
        public void DrawObject(Graphics grfx)
        {
            float X = hotSpot.X + coord.X + this.Width;
            
            for (int i = 0; i < Count; i++)
            {
                Columns[i].DrawObject(grfx);
            }
            for (int i = 0; i < Count; i++)
            {
                Columns[i].DrawFront(grfx);
            }
            //DrawMarkersZones(grfx);
            //DrawMarkers(grfx);
        }
        public void DrawBounds(Graphics grfx)
        {
            float X = hotSpot.X + coord.X + this.Width;

            for (int i = 0; i < Count; i++)
            {
                Columns[i].DrawBounds(grfx);
            }
        }
    }

    // КОЛОНКА с данными планшета
    public sealed class PlaneColumn
    {
        // Drawing
        PointF hotSpot, coord;
        SizeF size;
        public PointF HotSpot
        {
            get { return hotSpot; }
            set
            {
                hotSpot = value;
                PointF pt = value;
                pt.X += coord.X;
                pt.Y = coord.Y + HeadTextYOffset;
                for (int i = 0; i < Count; i++)
                {
                    this[i].HotSpot = value;
                    if (this[i].scale != null) this[i].scale.HotSpot = pt;
                }
            }
        }
        public PointF Coord
        {
            get { return coord; }
            set
            {
                coord = value;
                PointF coordPt = value;
                if(IsHeadDrawing) coordPt.Y += HeadTextYOffset;
                for (int i = 0; i < Count; i++)
                {
                    this[i].Coord = coordPt;
                    if (this[i].scale != null)
                    {
                        this[i].scale.HotSpot.X = coordPt.X + HotSpot.X;
                        this[i].scale.HotSpot.Y = coordPt.Y;
                    }
                }
            }
        }
        public RectangleF PlaneRect 
        { 
            set 
            {
                RectangleF rect = value;
                if(IsHeadDrawing) rect.Height += this.HeadTextYOffset;
                for (int i = 0; i < Count; i++)
                {
                    ObjList[i].PlaneRect = rect;
                }
            }
        }

        public float MinWidth;
        public float MaxWidth; 
        public float RealWidth;
        public float BindWidth;
        public float StartWidth;
        public float Width
        {
            get { return this.size.Width; }
            set
            {
                this.size.Width = value;

                for (int i = 0; i < Count; i++)
                {
                    this[i].Width = value;
                }
            }
        }
        public float Height
        {
            get { return this.size.Height; }
            set { this.size.Height = value; }
        }
        public float HeadTextYOffset;
        public float YUnitsInDepthUnit
        {
            set
            {
                if (ObjList != null)
                {
                    for (int i = 0; i < Count; i++)
                    {
                        this[i].YUnitsInDepthUnit = value;
                    }
                }
            }
        }
        public int ScaleCount;
        public bool Visible;

        public float ScreenMinY
        {
            set
            {
                for (int i = 0; i < Count; i++)
                {
                    if (ObjList[i] != null) ObjList[i].ScreenMinY = value;
                }
            }
        }
        public float ScreenMaxY
        {
            set
            {
                for (int i = 0; i < Count; i++)
                {
                    ObjList[i].ScreenMaxY = value;
                }
            }
        }

        // Data
        public bool IsHeadDrawing = true;
        public string HeadText;
        public Constant.Plane.TextOrientation HeadTextOrientation;
        Font headFont, headFontLower;

        List<PlaneBaseObject> ObjList;
        List<PlaneScale> ScaleList;
        public int Count { get { return ObjList.Count; } }

        // METHODS
        public PlaneColumn()
        {
            HeadText = string.Empty;
            HeadTextOrientation = Constant.Plane.TextOrientation.NORMAL;
            ObjList = new List<PlaneBaseObject>(5);
            ScaleList = new List<PlaneScale>(5);
            ScaleCount = 0;
            Width = 0;
            RealWidth = 0;
            MinWidth = -1;
            MaxWidth = -1;
            headFont = new Font("Segoe UI", 8);
            headFontLower = new Font("Segoe UI", 8);
            Visible = true;
        }
        public PlaneColumn(PlaneColumn col) : this()
        {
            HeadTextYOffset = col.HeadTextYOffset;
            HeadText = col.HeadText;
            HeadTextOrientation = col.HeadTextOrientation;
            this.Visible = col.Visible;

            ScaleCount = 0;
            if (col.Count > 0)
            {
                for (int i = 0; i < col.Count; i++)
                {
                    switch(col[i].TypeID)
                    {
                        case Constant.Plane.ObjectType.DEPTH_MARKS:
                            Add(new PlaneDepths());
                            this.SetDepthByAbsMd(((PlaneDepths)col[i]).ShowDepthByAbs);
                            break;
                        case Constant.Plane.ObjectType.LOG:
                            Add(new PlaneLog((PlaneLog)col[i]));
                            break;
                        case Constant.Plane.ObjectType.GIS:
                            Add(new PlaneGis());
                            break;
                        case Constant.Plane.ObjectType.POROSITY:
                            Add(new PlaneGisPor());
                            break;
                        case Constant.Plane.ObjectType.OIL_SAT:
                            Add(new PlaneGisOilSat());
                            break;
                        case Constant.Plane.ObjectType.OIL_OBJ:
                            Add(new PlaneOilObj());
                            break;
                        case Constant.Plane.ObjectType.PERFORATION:
                            Add(new PlanePerf());
                            break;
                        case Constant.Plane.ObjectType.CORE:
                            Add(new PlaneCore());
                            break;
                        case Constant.Plane.ObjectType.LEAK:
                            Add(new PlaneLeak());
                            break;
                        case Constant.Plane.ObjectType.PERFORATION_HISTORY:
                            Add(new PlanePerfHistory());
                            break;
                    }
                    if(Count > 0) ObjList[i].Visible = col[i].Visible;
                }
            }
            MinWidth = col.MinWidth;
            MaxWidth = col.MaxWidth;
            RealWidth = col.RealWidth;
            StartWidth = col.StartWidth;
            if ((this.RealWidth <= this.MinWidth) && (this.MinWidth != -1))
            {
                this.Width = this.MinWidth;
            }
            else if ((this.RealWidth >= this.MaxWidth) && (this.MaxWidth != -1))
            {
                this.Width = MaxWidth;
            }
            else
            {
                this.Width = this.RealWidth;
            }
            // SCALE LIST
            for (int i = 0; i < col.ScaleList.Count; i++)
            {
                this.AddScale(col.ScaleList[i]);
            }
        } // Add new type

        public float ScaleWidth(float dScale)
        {
            if ((this.RealWidth + dScale > 0) && (this.RealWidth + dScale < 2000))
            {
                this.RealWidth = this.RealWidth + dScale;
            }
            else
                dScale = 0;
            if ((this.RealWidth <= this.MinWidth) && (this.MinWidth != -1))
            {
                this.Width = this.MinWidth;
                dScale = 0;
            }
            else if ((this.RealWidth >= this.MaxWidth) && (this.MaxWidth != -1))
            {
                this.Width = MaxWidth;
                dScale = 0;
            }
            else
            {
                this.Width = this.RealWidth;
            }
            SetWidthScaleList();
            return dScale;
        }
        public float SetBindWidth(float BindWidth)
        {
            this.Width = BindWidth;
            SetWidthScaleList();
            return this.Width;
        }

        // STREAM
        public void ReadFromStream(BinaryReader br)
        {
            this.StartWidth = br.ReadSingle();
            this.Width = this.StartWidth;
            this.RealWidth = this.StartWidth;
            this.MinWidth = br.ReadSingle();
            this.MaxWidth = br.ReadSingle();
            this.HeadTextOrientation = (Constant.Plane.TextOrientation)br.ReadInt32();
            this.HeadText = br.ReadString();
            this.Visible = br.ReadBoolean();
            int count = br.ReadInt32();
            ObjList = new List<PlaneBaseObject>(count);
            Constant.Plane.ObjectType ObjType;
            for (int i = 0; i < count; i++)
            {
                ObjType = (Constant.Plane.ObjectType)br.ReadInt32();
                switch (ObjType)
                {
                    case Constant.Plane.ObjectType.CORE:
                        ObjList.Add(new PlaneCore());
                        ((PlaneCore)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.DEPTH_MARKS:
                        ObjList.Add(new PlaneDepths());
                        ((PlaneDepths)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.GIS:
                        ObjList.Add(new PlaneGis());
                        ((PlaneGis)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.LOG:
                        ObjList.Add(new PlaneLog());
                        ((PlaneLog)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.OIL_OBJ:
                        ObjList.Add(new PlaneOilObj());
                        ((PlaneOilObj)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.OIL_SAT:
                        ObjList.Add(new PlaneGisOilSat());
                        ((PlaneGisOilSat)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.PERFORATION:
                        ObjList.Add(new PlanePerf());
                        ((PlanePerf)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.POROSITY:
                        ObjList.Add(new PlaneGisPor());
                        ((PlaneGisPor)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.LEAK:
                        ObjList.Add(new PlaneLeak());
                        ((PlaneLeak)ObjList[i]).ReadFromStream(br);
                        break;
                    case Constant.Plane.ObjectType.PERFORATION_HISTORY:
                        ObjList.Add(new PlanePerfHistory());
                        ((PlanePerfHistory)ObjList[i]).ReadFromStream(br);
                        break;
                    default:
                        throw new ArgumentException("Не поддерживаемый тип объекта!");
                }
            }
            count = br.ReadInt32();
            int[] objIndexes;
            for (int i = 0; i < count; i++)
            {
                objIndexes = new int[br.ReadInt32()];
                for (int j = 0; j < objIndexes.Length; j++)
                {
                    objIndexes[j] = br.ReadInt32();
                }
                if ((objIndexes.Length > 0) && (ObjList[objIndexes[0]].scale != null))
                {
                    this.ScaleList.Add(ObjList[objIndexes[0]].scale);
                }
             }
        }
        public void WriteToStream(BinaryWriter bw)
        {
            bw.Write(this.StartWidth);
            bw.Write(this.MinWidth);
            bw.Write(this.MaxWidth);
            bw.Write((int)this.HeadTextOrientation);
            bw.Write(this.HeadText);
            bw.Write(this.Visible);
            bw.Write(this.ObjList.Count);
            for (int i = 0; i < ObjList.Count; i++)
            {
                bw.Write((int)ObjList[i].TypeID);
                ObjList[i].WriteToStream(bw);
            }
            bw.Write(this.ScaleList.Count);
            for (int i = 0; i < ScaleList.Count; i++)
            {
                bw.Write(ScaleList[i].ObjIndex.Length);
                for (int j = 0; j < ScaleList[i].ObjIndex.Length; j++)
                {
                    bw.Write(ScaleList[i].ObjIndex[j]);
                }
            }
        }

        // TOOLTIP
        public PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y, bool ShiftPressed)
        {
            PlaneToolTip toolTip = null, resToolTip = null;
            if (this.Visible)
            {
                for (int i = 0; i < Count; i++)
                {
                    toolTip = ObjList[i].GetToolTip(DictList, X, Y);
                    if ((toolTip != null) && ((resToolTip == null) || (resToolTip.ScreenDist > toolTip.ScreenDist)))
                    {
                        if (ShiftPressed && toolTip.Object.TypeID != Constant.Plane.ObjectType.PERFORATION)
                        {
                            resToolTip = toolTip;
                        }
                        else if (!ShiftPressed && (toolTip.Object.TypeID == Constant.Plane.ObjectType.PERFORATION || 
                            toolTip.Object.TypeID == Constant.Plane.ObjectType.LEAK || toolTip.Object.TypeID == Constant.Plane.ObjectType.CORE))
                        {
                            resToolTip = toolTip;
                        }
                    }
                }
            }
            return resToolTip;
        }

        // WORK WITH OBJ
        public void Add(PlaneBaseObject obj) 
        {
            ObjList.Add(obj); 
        }
        public void RemoveAt(int index) 
        {
            if ((index > -1) && (index < Count))
            {
                ObjList.RemoveAt(index);
            }
        }
        public PlaneBaseObject this[int index]
        {
            get
            {
                if ((index > -1) && (index < Count))
                {
                    return ObjList[index];
                }
                else 
                    return null;
            }
        }
        public PlaneBaseObject GetObjectByType(Constant.Plane.ObjectType Type)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ObjList[i].TypeID == Type)
                {
                    return ObjList[i];
                }
            }
            return null;
        }
        public Constant.Plane.ObjectType GetObjType(int index)
        {
            if (index < Count)
            {
                return ObjList[index].TypeID;
            }
            else
                return Constant.Plane.ObjectType.EMPTY;
        }
        
        // SHOW HIDE DATA
        public bool TestContainData(Constant.Plane.ObjectType ObjType)
        {
            if (ObjList != null)
            {
                for (int i = 0; i < ObjList.Count; i++)
                {
                    if (ObjList[i].TypeID == ObjType)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public void ClearSelectedData()
        {
            for (int i = 0; i < Count; i++)
            {
                ObjList[i].Selected = false;
                ObjList[i].SelectedDepthMD = -1;
            }
        }
        public bool SetSelectedDepth(Constant.Plane.ObjectType Type, float DepthMD)
        {
            bool res = false;
            for (int i = 0; i < Count; i++)
            {
                if (ObjList[i].TypeID == Type)
                {
                    if (!res && (ObjList[i].SelectedDepthMD != DepthMD)) res = true;
                    ObjList[i].Selected = true;
                    ObjList[i].SelectedDepthMD = DepthMD;
                }
                else
                {
                    ObjList[i].Selected = false;
                    ObjList[i].SelectedDepthMD = -1;
                }
            }
            return res;
        }
        public void SetDepthByAbsMd(bool ShowDepthAbs)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ObjList[i].TypeID == Constant.Plane.ObjectType.DEPTH_MARKS)
                {
                    ((PlaneDepths)ObjList[i]).ShowDepthByAbs = ShowDepthAbs;
                    break;
                }
            }
        }
        public void SetShowColumnHead(bool ShowColumnHead)
        {
            this.IsHeadDrawing = ShowColumnHead;
        }
        public void SetVisibleByType(Constant.Plane.ObjectType Type, bool Visible)
        {
            this.Visible = false;
            for (int i = 0; i < Count; i++)
            {
                if (ObjList[i].TypeID == Type)
                {
                    ObjList[i].Visible = Visible;
                }
                // Для видимости колонки не учитывается объект Керн
                this.Visible = this.Visible || ((ObjList[i].TypeID != Constant.Plane.ObjectType.CORE) && ObjList[i].Visible);
            }
        }
        public int GetVisibleByType(Constant.Plane.ObjectType Type)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ObjList[i].TypeID == Type)
                {
                    return (ObjList[i].Visible ? 1 : 0);
                }
            }
            return -1;
        }
        public void SetShowOilObjName(bool ShowOilObjName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ObjList[i].TypeID == Constant.Plane.ObjectType.OIL_OBJ)
                {
                    ((PlaneOilObj)ObjList[i]).SetOilObjMode(ShowOilObjName);
                }
            }
        }
        public void ShowDuplicatesLog(bool ShowDuplicates)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ObjList[i].TypeID == Constant.Plane.ObjectType.LOG)
                {
                    ((PlaneLog)ObjList[i]).ShowDuplicates = ShowDuplicates;
                }
            }
        }
        
        // SCALELIST
        public void AddScale(int[] ObjOndexes)
        {
            PlaneScale scale = new PlaneScale();
            scale.ObjIndex = ObjOndexes;
            InitScale(scale);
            scale.HeadLineLevel = ScaleList.Count;
            scale.Width = this.Width;
            this.ScaleList.Add(scale);
        }
        public void AddScale(PlaneScale sourceScale)
        {
            PlaneScale scale = new PlaneScale(sourceScale);
            InitScale(scale);
            scale.HeadLineLevel = ScaleList.Count;
            scale.Width = this.Width;
            this.ScaleList.Add(scale);
        }
        public void InitScaleList()
        {
            for (int i = 0; i < ScaleList.Count; i++)
            {
                InitScale(ScaleList[i]);
            }
        }
        public void TestScaleMinMax()
        {
            for (int i = 0; i < ScaleList.Count; i++)
            {
                InitScale(ScaleList[i]);
            }
        }
        public void InitScale(PlaneScale scale)
        {
            if (scale.ObjIndex != null)
            {
                int j, ind;
                for (j = 0; j < scale.ObjIndex.Length; j++)
                {
                    ind = scale.ObjIndex[j];
                    if ((ind > -1) && (ind < Count))
                    {
                        switch (this[ind].TypeID)
                        {
                            case Constant.Plane.ObjectType.LOG:
                                scale.color = ((PlaneLog)this[scale.ObjIndex[j]]).LineColor;
                                break;
                            case Constant.Plane.ObjectType.POROSITY:
                                scale.color = Color.Green;
                                scale.Text = "Пористость";
                                scale.Unit = "%";
                                break;
                            case Constant.Plane.ObjectType.OIL_SAT:
                                scale.color = Color.Sienna;
                                scale.Text = "Нефтенас-ть";
                                scale.Unit = "%";
                                break;
                        }
                        this[ind].scale = scale;
                    }
                }
            }
        }
        private void SetWidthScaleList()
        {
            for (int i = 0; i < ScaleList.Count; i++)
            {
                (ScaleList[i]).Width = this.Width;
            }
        }

        // OTHER
        public void SetDepthCalc(DepthCalculator DepthCalc)
        {
            if (ObjList != null)
            {
                for (int i = 0; i < ObjList.Count; i++)
                {
                    ObjList[i].SetDepthCalc(DepthCalc);
                }
            }
        }
        public float GetMDByAbs(float AbsDepth)
        {
            float depth = -1;
            for (int i = 0; i < Count; i++)
            {
                depth = ObjList[i].GetMDByAbs(AbsDepth);
                if (depth != -1)
                {
                    return depth;
                }
            }
            return -1;
        }
        public float GetMedianDepth()
        {
            int count = 0;
            float depth = -1, d;
            for (int i = 0; i < Count; i++)
            {
                d = ObjList[i].GetGisMedianDepth();
                if (d != -1)
                {
                    depth += d;
                    count++;
                }
            }
            if (count > 0) depth = depth / (float)count;
            return depth;
        }
        public float GetLastPerfDepth()
        {
            int count = 0;
            float depth = -1, d;
            for (int i = 0; i < Count; i++)
            {
                d = ObjList[i].GetLastPerfDepth();
                if (d != -1)
                {
                    depth += d;
                    count++;
                }
            }
            if (count > 0) depth = depth / (float)count;
            return depth;
        }
        public void SetGraphicsSizes(Graphics gr)
        {
            SizeF sz, szLower = SizeF.Empty;
            float height, maxHeight = 0, h;
            if ((HeadText != null) && (HeadText != ""))
            {
                sz = gr.MeasureString(HeadText, headFont, PointF.Empty, StringFormat.GenericTypographic);
                bool DepthColumn = false, AbsMd = true;
                if (Count == 1)
                {
                    if (ObjList[0].TypeID == Constant.Plane.ObjectType.DEPTH_MARKS)
                    {
                        DepthColumn = true;
                        AbsMd = ((PlaneDepths)ObjList[0]).ShowDepthByAbs;
                        string lowStr = (AbsMd) ? "(абсолютные)" : "(измеренные)";
                        szLower = gr.MeasureString(lowStr, headFontLower, PointF.Empty, StringFormat.GenericTypographic);
                        if (sz.Width < szLower.Width) sz.Width = szLower.Width;
                        if (sz.Height < szLower.Height) sz.Height = szLower.Height;
                    }
                }
                switch (HeadTextOrientation)
                {
                    case Constant.Plane.TextOrientation.NORMAL:
                        goto case Constant.Plane.TextOrientation.HEADLONG;
                    case Constant.Plane.TextOrientation.LEFT:
                        goto case Constant.Plane.TextOrientation.RIGHT;
                    case Constant.Plane.TextOrientation.RIGHT:
                        if (maxHeight < sz.Width) maxHeight = sz.Width;
                        break;
                    case Constant.Plane.TextOrientation.HEADLONG:
                        if (maxHeight < sz.Height) maxHeight = sz.Height;
                        break;
                }
            }
            height = 0; 
            h = 0;
            int level = 0;
            for (int i = 0; i < ScaleList.Count; i++)
            {
                h = (ScaleList[i]).GetHeightByGraphics(gr);
                if (h > 4)
                {
                    (ScaleList[i]).HeadLineLevel = level;
                    level++;
                }
                height += h;
            }
            if (maxHeight < height) maxHeight = height;
            HeadTextYOffset = maxHeight + 4;
        }
        public void LoadData(BackgroundWorker worker, DoWorkEventArgs e, Project proj, Well w)
        {
            for (int i = 0; i < Count; i++)
            {
                if ((worker != null) && (worker.CancellationPending))
                {
                    e.Cancel = true;
                    return;
                }
                switch (this[i].TypeID)
                {
                    case Constant.Plane.ObjectType.DEPTH_MARKS:
                        ((PlaneDepths)ObjList[i]).LoadData(proj, w);
                        break;
                    case Constant.Plane.ObjectType.LOG:
                        ((PlaneLog)ObjList[i]).LoadData(worker, e, proj, w);
                        break;
                    case Constant.Plane.ObjectType.GIS:
                        ((PlaneGis)ObjList[i]).LoadData(proj, w);
                        break;
                    case Constant.Plane.ObjectType.POROSITY:
                        ((PlaneGisPor)ObjList[i]).LoadData(proj, w);
                        break;
                    case Constant.Plane.ObjectType.OIL_SAT:
                        ((PlaneGisOilSat)ObjList[i]).LoadData(proj, w);
                        break;
                    case Constant.Plane.ObjectType.OIL_OBJ:
                        ((PlaneOilObj)ObjList[i]).LoadData(proj, w);
                        break;
                    case Constant.Plane.ObjectType.PERFORATION:
                        ((PlanePerf)ObjList[i]).LoadData(proj, w);
                        break;
                    case Constant.Plane.ObjectType.CORE:
                        ((PlaneCore)ObjList[i]).LoadData(proj, w);
                        break;
                    case Constant.Plane.ObjectType.LEAK:
                        ((PlaneLeak)ObjList[i]).LoadData(proj, w);
                        break;
                    case Constant.Plane.ObjectType.PERFORATION_HISTORY:
                        ((PlanePerfHistory)ObjList[i]).LoadData(proj, w);
                        break;
                }
            }
        }   // Add new type
        public void DrawHead(Graphics grfx)
        {
            if (this.Visible)
            {
                float X, Y;
                X = hotSpot.X + coord.X;
                Y = coord.Y;

                if (IsHeadDrawing)
                {
                    grfx.DrawLine(Pens.Black, X + Width, Y, X + Width, Y + HeadTextYOffset);
                    if ((HeadText != "") && (HeadText != null))
                    {
                        SizeF szStr = grfx.MeasureString(HeadText, headFont, PointF.Empty, StringFormat.GenericTypographic);
                        switch (HeadTextOrientation)
                        {
                            case Constant.Plane.TextOrientation.NORMAL:
                                grfx.DrawString(HeadText, headFont, Brushes.Black,
                                    X + Width / 2 - szStr.Width / 2,
                                    Y + HeadTextYOffset / 2 - szStr.Height / 2,
                                    StringFormat.GenericTypographic);
                                break;
                            case Constant.Plane.TextOrientation.LEFT:
                                bool DepthColumn = false, AbsMd = true;
                                if (Count == 1)
                                {
                                    if (ObjList[0].TypeID == Constant.Plane.ObjectType.DEPTH_MARKS)
                                    {
                                        DepthColumn = true;
                                        AbsMd = ((PlaneDepths)ObjList[0]).ShowDepthByAbs;
                                    }
                                }
                                if (!DepthColumn)
                                {
                                    float dX, dY;
                                    dX = X + Width / 2;
                                    dY = Y + HeadTextYOffset / 2;
                                    if (szStr.Height - 2 < this.Width)
                                    {
                                        Matrix saveTransform = grfx.Transform;
                                        grfx.TranslateTransform(dX, dY);
                                        grfx.RotateTransform(-90);
                                        grfx.DrawString(HeadText, headFont, Brushes.Black, -szStr.Width / 2, -szStr.Height / 2, StringFormat.GenericTypographic);
                                        grfx.Transform = saveTransform;
                                    }
                                }
                                else
                                {
                                    string lowStr = (AbsMd) ? "(абс." + (ObjList[0].InitDepthCalcFromTrajectory ? "по траект.)" : "по ГИС)") : "(измерен.)";
                                    SizeF szLowStr = grfx.MeasureString(lowStr, headFontLower, PointF.Empty, StringFormat.GenericTypographic);
                                    float dX, dY;
                                    dX = X + Width / 2;
                                    dY = Y + HeadTextYOffset / 2;
                                    if (szStr.Height < this.Width)
                                    {
                                        Matrix saveTrans = grfx.Transform;
                                        grfx.TranslateTransform(dX, dY);
                                        grfx.RotateTransform(-90);
                                        if (szStr.Height + szLowStr.Height < this.Width)
                                        {
                                            grfx.DrawString(HeadText, headFont, Brushes.Black, -szStr.Width / 2, -(szStr.Height + szLowStr.Height) / 2, StringFormat.GenericTypographic);
                                            grfx.DrawString(lowStr, headFontLower, Brushes.Black, -szLowStr.Width / 2, (szStr.Height - szLowStr.Height) / 2, StringFormat.GenericTypographic);
                                        }
                                        else
                                        {
                                            grfx.DrawString(HeadText, headFont, Brushes.Black, -szStr.Width / 2, -szStr.Height / 2, StringFormat.GenericTypographic);
                                        }
                                        grfx.Transform = saveTrans;
                                    }
                                }
                                break;
                        }
                    }
                    // отрисовка шкал
                    for (int i = 0; i < ScaleList.Count; i++)
                    {
                        (ScaleList[i]).DrawObject(grfx);
                    }
                }
            }
        }
        public void DrawObject(Graphics grfx)
        {
            if (this.Visible)
            {
                if (ObjList != null)
                {
                    for (int i = 0; i < Count; i++)
                    {
                        this[i].DrawGrid(grfx);
                    }
                    for (int i = 0; i < Count; i++)
                    {
                        this[i].DrawBack(grfx);
                    }
                    for (int i = 0; i < Count; i++)
                    {
                        this[i].DrawObject(grfx);
                    }
                }
            }
        }
        public void DrawFront(Graphics grfx)
        {
            if (ObjList != null && this.Visible)
            {
                for (int i = 0; i < Count; i++)
                {
                    this[i].DrawFront(grfx);
                }
            }
        }
        public void DrawBounds(Graphics grfx)
        {
            if (this.Visible)
            {
                // рисуем границу
                float X = hotSpot.X + coord.X;
                float Y = hotSpot.Y + coord.Y, Y2 = size.Height;
                if (IsHeadDrawing) Y2 += HeadTextYOffset;
                grfx.DrawLine(Pens.Black, X, Y, X + Width, Y);
                grfx.DrawLine(Pens.Black, X, Y, X, Y + Y2);
                grfx.DrawLine(Pens.Black, X + Width, Y, X + Width, Y + Y2);
                grfx.DrawLine(Pens.Black, X, Y + Y2, X + Width, Y + Y2);
            }
        }
    }

    // ШКАЛА объекта данных планшета
    public sealed class PlaneScale
    {
        public Constant.Plane.ScaleType Type;
        public int[] ObjIndex;
        public bool MinMaxAuto;
        public bool MinMaxUseStep;
        public float MinValue, MaxValue, MinMaxStep;
        public PointF HotSpot;
        public float Height;
        public float Width;
        public int HeadLineLevel;
        public string Text, Unit, SmallText;
        public Color color
        {
            get { return pen.Color; }
            set
            {
                pen = new Pen(value);
                brush = new SolidBrush(value);
            }
        }
        public Pen pen;
        public Brush brush;
        Font font, fontValue;

        public PlaneScale()
        {
            Text = string.Empty;
            Unit = string.Empty;
            SmallText = string.Empty;
            ObjIndex = null;
            HeadLineLevel = 0;
            Width = 0;
            MinValue = float.MaxValue;
            MaxValue = float.MinValue;
            MinMaxStep = -1;
            MinMaxAuto = true;
            MinMaxUseStep = false;
            font = new Font("Tahoma", 8, FontStyle.Bold);
            fontValue = new Font("Tahoma", 8);
        }
        public PlaneScale(PlaneScale scale) : this()
        {
            Type = scale.Type;
            Width = scale.Width;
            HeadLineLevel = scale.HeadLineLevel;
            Text = scale.Text;
            Unit = scale.Unit;
            color = scale.color;
            if(scale.ObjIndex != null)
            {
                ObjIndex = new int[scale.ObjIndex.Length];
                for (int i = 0; i < scale.ObjIndex.Length; i++)
                {
                    ObjIndex[i] = scale.ObjIndex[i];
                }
            }
            MinMaxAuto = scale.MinMaxAuto;
            MinMaxStep = scale.MinMaxStep;
            MinMaxUseStep = scale.MinMaxUseStep;
            MinValue = scale.MinValue;
            MaxValue = scale.MaxValue;
        }

        // STREAM
        public void ReadFromStream(BinaryReader br)
        {
            Type = (Constant.Plane.ScaleType)br.ReadInt32();
            MinMaxAuto = br.ReadBoolean();
            MinMaxUseStep = br.ReadBoolean();
            MinValue = br.ReadSingle();
            MaxValue = br.ReadSingle();
            if (MinMaxAuto)
            {
                MinValue = float.MaxValue;
                MaxValue = float.MinValue;
            }
            MinMaxStep = br.ReadSingle();
            HeadLineLevel = br.ReadInt32();
            color = Color.FromArgb(br.ReadInt32());
            int count = br.ReadInt32();
            ObjIndex = new int[count];
            for (int i = 0; i < count; i++)
            {
                ObjIndex[i] = br.ReadInt32();
            }
        }
        public void WriteToStream(BinaryWriter bw)
        {
            bw.Write((int)Type);
            bw.Write(MinMaxAuto);
            bw.Write(MinMaxUseStep);
            bw.Write(MinValue);
            bw.Write(MaxValue);
            bw.Write(MinMaxStep);
            bw.Write(HeadLineLevel);
            bw.Write(color.ToArgb());
            bw.Write(ObjIndex.Length);
            for (int i = 0; i < ObjIndex.Length; i++)
            {
                bw.Write(ObjIndex[i]);
            }
        }

        public float GetHeightByGraphics(Graphics gr)
        {
            SizeF sz = gr.MeasureString(Text, font, PointF.Empty, StringFormat.GenericTypographic);
            SizeF szVal = gr.MeasureString("1234", fontValue, PointF.Empty, StringFormat.GenericTypographic);
            return 2 + 2 + sz.Height;
        }
        public void SetMinMaxAuto(float Min, float Max)
        {
            if (MinMaxAuto)
            {
                if (Min < this.MinValue) this.MinValue = Min;
                if (Max > this.MaxValue) this.MaxValue = Max;
                this.MinMaxStep = (this.MaxValue - this.MinValue) / 10;
            }
        }
        public void TestMinMax()
        {
            if (MinMaxAuto)
            {
                if ((Type == Constant.Plane.ScaleType.LOG10 || Type == Constant.Plane.ScaleType.INVERT_LOG10))
                {
                    //if ((MinValue > 0) && (MinValue != float.MaxValue))
                    //{
                    //    double log = Math.Round(Math.Log10(MinValue), 4);
                    //    log = Math.Floor(log);
                    //    this.MinValue = (float)Math.Pow(10, log);
                    //}
                }
            }
            if (MinValue == float.MaxValue) DefaultMinMax();
        }
        public void DefaultMinMax()
        {
            if (this.Type == Constant.Plane.ScaleType.LINEAR || this.Type == Constant.Plane.ScaleType.INVERT_LINEAR)
            {
                this.MinValue = 0;
                this.MaxValue = 10;
                this.MinMaxStep = 1;
            }
            else
            {
                this.MinValue = 1;
                this.MaxValue = 10;
                this.MinMaxStep = 1;
            }
        }
        public float GetXShift(float value)
        {
            float result = 0;
            if ((MinMaxStep != -1) && (MaxValue - MinValue > 0))
            {
                switch (Type)
                {
                    case Constant.Plane.ScaleType.LINEAR:
                        if (value >= MinValue)
                        {
                            if (value > MaxValue) value = MaxValue;
                            result = Width * (value - MinValue) / (MaxValue - MinValue);
                        }
                        break;
                    case Constant.Plane.ScaleType.INVERT_LINEAR:
                        if (value >= MinValue)
                        {
                            if (value > MaxValue) value = MaxValue;
                            result = Width - Width * (value - MinValue) / (MaxValue - MinValue);
                        }
                        break;
                    case Constant.Plane.ScaleType.LOG10:
                        double logMaxVal = 0, logMinVal = 0, logVal = 0;
                        result = 0;
                        if(value > 0)
                        {
                            if (MaxValue > 0) logMaxVal = Math.Log10(MaxValue);
                            if (MinValue > 0) logMinVal = Math.Log10(MinValue);
                            if (value > 0) logVal = Math.Log10(value);

                            if (value >= MinValue)
                            {
                                if (value > MaxValue) value = MaxValue;
                                result = Width * (float)((logVal - logMinVal) / (logMaxVal - logMinVal));
                            }
                        }
                        break;
                    case Constant.Plane.ScaleType.INVERT_LOG10:
                        result = Width;
                        if (value > 0)
                        {
                            logMaxVal = 0; logMinVal = 0; logVal = 0;
                            if (MaxValue > 0) logMaxVal = Math.Log10(MaxValue);
                            if (MinValue > 0) logMinVal = Math.Log10(MinValue);
                            if (value > 0) logVal = Math.Log10(value);

                            if (value >= MinValue)
                            {
                                if (value > MaxValue) value = MaxValue;
                                result = Width - Width * (float)((logVal - logMinVal) / (logMaxVal - logMinVal));
                            }
                        }
                        break;
                }
                if (result < 0) result = 0;
                if (result > Width) result = Width;
            }
            return result;
        }
        public List<float> GetShifts()
        {
            int i;
            List<float> shifts = new List<float>();
            float val = MinValue, dX;
            if (Type == Constant.Plane.ScaleType.LINEAR || Type == Constant.Plane.ScaleType.INVERT_LINEAR)
            {
                dX = GetXShift(val);
                while ((val <= MaxValue) && (MinMaxStep > 0))
                {
                    val += MinMaxStep;
                    shifts.Add(GetXShift(val));
                }
            }
            else
            {
                val = MinValue;
                float k = Math.Abs(MinValue / 10f);
                if (k == 0) k = 1;
                while (val < MaxValue)
                {
                    for (i = 0; (i < 10) && (val < MaxValue); i++)
                    {
                        shifts.Add(GetXShift(val));
                        val += k;
                    }
                    k *= 10;
                }
            }
            return shifts;
        }
        public void DrawObject(Graphics grfx)
        {
            float X = HotSpot.X;
            float Y = HotSpot.Y - 2;
            float valWidth = 0;
            string strMin, strMax, title, temp;
            SizeF sizeVal, sizeTitle = SizeF.Empty;

            if (Text.Length > 0)
            {
                title = Text;
                //if (Unit.Length == 0)
                //{
                //    title = Text;
                //}
                //else
                //{
                //    title = string.Format("{0}, {1}", Text, Unit);
                //}
                sizeTitle = grfx.MeasureString(title, font, PointF.Empty, StringFormat.GenericTypographic);
                Y -= HeadLineLevel * (sizeTitle.Height + 4);
                
                if (MinMaxStep == -1)
                {
                    MinValue = 0;
                    if (this.Type == Constant.Plane.ScaleType.LOG10 || this.Type == Constant.Plane.ScaleType.INVERT_LOG10)
                    {
                        MinValue = 1;
                    }
                    MaxValue = 10;
                    MinMaxStep = 1;
                }
                grfx.DrawLine(pen, X, Y, X + Width, Y);

                List<float> shifts = GetShifts();
                for (int i = 0; i < shifts.Count; i++)
                {
                    grfx.DrawLine(pen, X + shifts[i], Y, X + shifts[i], Y - 2);
                }

                Y -= 2;

                if (MinValue > 10)
                {
                    strMin = MinValue.ToString("0");
                }
                else if (MinValue > 0.01)
                {
                    strMin = MinValue.ToString("0.##");
                }
                else
                {
                    strMin = MinValue.ToString("0.####");
                }
                
                strMax = MaxValue.ToString("0");

                if (this.Type == Constant.Plane.ScaleType.INVERT_LINEAR || this.Type == Constant.Plane.ScaleType.INVERT_LOG10)
                {
                    temp = strMax;
                    strMax = strMin;
                    strMin = temp;
                }

                sizeVal = grfx.MeasureString(strMin, fontValue, PointF.Empty, StringFormat.GenericTypographic);
                grfx.DrawString(strMin, fontValue, brush, HotSpot.X, Y - sizeVal.Height, StringFormat.GenericTypographic);
                valWidth += sizeVal.Width;
                
                sizeVal = grfx.MeasureString(strMax, fontValue, PointF.Empty, StringFormat.GenericTypographic);
                grfx.DrawString(strMax, fontValue, brush, HotSpot.X + Width - sizeVal.Width, Y - sizeVal.Height, StringFormat.GenericTypographic);
                valWidth += sizeVal.Width;

                if (title.Length > 0)
                {
                    int i = 0;
                    while (i < 3)
                    {
                        if (i > 0)
                        {
                            title = ((i == 1) ? Text : SmallText + "...");
                            sizeTitle = grfx.MeasureString(title, font, PointF.Empty, StringFormat.GenericTypographic);
                        }
                        if ((sizeTitle.Width > 0) && (sizeTitle.Width < this.Width - valWidth))
                        {
                            grfx.DrawString(title, font, brush, HotSpot.X + Width / 2 - sizeTitle.Width / 2, Y - sizeTitle.Height - 2, StringFormat.GenericTypographic);
                            break;
                        }
                        i++;
                    }
                }
            }
        }
    }

    // TOOLTIP Планшета
    public class PlaneToolTip
    {
        public PointF Point;
        public Plane Plane;
        public PlaneBaseObject Object;
        public string Title;
        public string Text;
        public double ScreenDist;
        public float Top;
        public float Bottom;
        public int Color;
    }

    // Расчет глубин
    public class DepthCalculator
    {
        public bool InitFromTrajectory;
        float[][] depths;
        public float gisMinDepth, gisMaxDepth;
        public float allMinDepth, allMaxDepth;

        public DepthCalculator()
        {
            depths = null;
            InitFromTrajectory = false;
            gisMinDepth = -1;
            gisMaxDepth = -1;
            allMinDepth = -1;
            allMaxDepth = -1;
        }
        public DepthCalculator(DepthCalculator depthCalc) : this()
        {
            InitFromTrajectory = depthCalc.InitFromTrajectory;
        }

        public void SetMinMaxDepth(PlaneBaseObject obj)
        {
            if (obj.locMinDepth > 0)
            {
                if (allMinDepth == -1 || allMinDepth > obj.locMinDepth) allMinDepth = obj.locMinDepth;
                if (allMaxDepth == -1 || allMaxDepth < obj.locMinDepth) allMaxDepth = obj.locMinDepth;

                if (obj.TypeID == Constant.Plane.ObjectType.GIS || obj.TypeID == Constant.Plane.ObjectType.OIL_OBJ)
                {
                    if (gisMinDepth == -1 || gisMinDepth > obj.locMinDepth) gisMinDepth = obj.locMinDepth;
                    if (gisMaxDepth == -1 || gisMaxDepth < obj.locMinDepth) gisMaxDepth = obj.locMinDepth;
                }
            }
            if (obj.locMaxDepth > 0)
            {
                if (allMinDepth == -1 || allMinDepth > obj.locMaxDepth) allMinDepth = obj.locMaxDepth;
                if (allMaxDepth == -1 || allMaxDepth < obj.locMaxDepth) allMaxDepth = obj.locMaxDepth;

                if (obj.TypeID == Constant.Plane.ObjectType.GIS || obj.TypeID == Constant.Plane.ObjectType.OIL_OBJ)
                {
                    if (gisMinDepth == -1 || gisMinDepth > obj.locMaxDepth) gisMinDepth = obj.locMaxDepth;
                    if (gisMaxDepth == -1 || gisMaxDepth < obj.locMaxDepth) gisMaxDepth = obj.locMaxDepth;
                }
            }
        }

        public void InitDepth(Well w)
        {
            if (!InitFromTrajectory)
            {
                InitDepth(w.gis);
            }
            else
            {
                InitDepth(w.trajectory);
                if (!w.TrajectoryVisible && w.trajectory != null)
                {
                    w.trajectory.Clear();
                    w.trajectory = null;
                }
            }
        }
        void InitDepth(SkvGIS gis)
        {
            if (gis != null && gis.Count > 0)
            {
                int count = 0;
                for (int i = 0; i < gis.Count; i++)
                {
                    if ((gis.Items[i].H > 0) && (gis.Items[i].Habs > 0))
                    {
                        count++;
                    }
                }
                depths = new float[count][];
                for (int i = 0, j = 0; (i < gis.Count) && (j < depths.Length); i++)
                {
                    if ((gis.Items[i].H > 0) && (gis.Items[i].Habs > 0))
                    {
                        depths[j] = new float[2];
                        depths[j][0] = gis.Items[i].H;
                        depths[j][1] = gis.Items[i].Habs;
                        j++;
                    }
                }
            }
        }
        void InitDepth(WellTrajectory trajectory)
        {
            if (trajectory != null && trajectory.Count > 0)
            {
                depths = new float[trajectory.Count][];
                int k = (trajectory[0].Zabs > trajectory[trajectory.Count - 1].Zabs && trajectory[trajectory.Count - 1].Zabs < 0) ? -1 : 1;
                for (int i = 0; i < trajectory.Count; i++)
                {
                    depths[i] = new float[2];
                    depths[i][0] = trajectory[i].Z;
                    depths[i][1] = k * trajectory[i].Zabs;
                }
            }
        }

        public float GetAbsByMD(float MD)
        {
            if (depths != null && depths.Length > 0)
            {
                int a = 0, b = depths.Length - 1, h;

                if (MD <= depths[a][0]) return (MD - depths[a][0] + depths[a][1]);
                if (MD >= depths[b][0]) return (MD - depths[b][0] + depths[b][1]);

                while (b - a > 1)
                {
                    h = ((b - a) / 2);
                    if(h < 1) h = 1;

                    if (MD == depths[a + h][0])
                    {
                        return depths[a + h][1];
                    }
                    else if (MD < depths[a + h][0])
                    {
                        b = a + h;
                    }
                    else if (MD > depths[a + h][0])
                    {
                        a = a + h;
                    }
                }
                if (b - a == 1)
                {
                    return (depths[a][1] + ((MD - depths[a][0]) * (depths[b][1] - depths[a][1])) / (depths[b][0] - depths[a][0]));
                }
            }
            return -1;
        }
        public float GetMDByAbs(float Abs)
        {
            if ((depths != null) && (depths.Length > 0))
            {
                if (Abs <= depths[0][1]) return (Abs - depths[0][1] + depths[0][0]);
                if (Abs >= depths[depths.Length - 1][1]) return (Abs - depths[depths.Length - 1][1] + depths[depths.Length - 1][0]);

                for (int i = 0; i < depths.Length - 1; i++)
                {
                    if (Abs == depths[i][1])
                    {
                        return depths[i][0];
                    }
                    else if (Abs == depths[i + 1][1])
                    {
                        return depths[i + 1][0];
                    }
                    else if ((depths[i][1] < Abs) && (Abs < depths[i + 1][1]))
                    {
                        return (depths[i][0] + ((Abs - depths[i][1]) * (depths[i + 1][0] - depths[i][0])) / (depths[i + 1][1] - depths[i][1]));
                    }
                }
            }
            return -1;
        }
    }

    #region PLANE OBJECTS

    // МАРКЕР
    public sealed class PlaneMarker
    {
        public Marker srcMarker = null;
        public float Depth;
        public Pen pen;
        public Pen pen_selected;
        public Brush brush_text;
    }

    public sealed class PlaneMarkerZone
    {
        public string Name;
        public PlaneMarker MarkerTop = null;
        public PlaneMarker MarkerBottom = null;
        public SolidBrush FillBrush;
        public int Alfa;
    }

    // БАЗОВЫЙ объект
    public class PlaneBaseObject
    {
        public float ScreenMinY = 0;
        public float ScreenMaxY = 0;

        // Drawing
        public PointF Coord;
        public PointF HotSpot;

        public bool Visible;
        public bool Selected;
        public float SelectedDepthMD;
        public RectangleF PlaneRect;
        float yUnits;
        public virtual float YUnitsInDepthUnit
        {
            get { return yUnits; }
            set { yUnits = value; }
        }
        public float Width;

        // Data
        public Constant.Plane.ObjectType TypeID;
        public PlaneScale scale;

        public bool DrawGridHor;
        public bool DrawGridVert;
        
        public float locMinDepth, locMaxDepth;
        public float MinDepth, MaxDepth;

        DepthCalculator DepthCalc;

        // METHODS
        public PlaneBaseObject()
        {
            TypeID = Constant.Plane.ObjectType.EMPTY;
            scale = null;
            Width = 0;
            locMinDepth = -1;
            locMaxDepth = -1;
            MinDepth = -1;
            MaxDepth = -1;
            SelectedDepthMD = -1;
            Selected = false;
            Visible = true;
        }
        public PlaneBaseObject(PlaneBaseObject obj) : this()
        {
            TypeID = obj.TypeID;
            Width = obj.Width;
        }

        // STREAM
        public virtual void ReadFromStream(BinaryReader br)
        {
            this.Visible = br.ReadBoolean();
            this.DrawGridHor = br.ReadBoolean();
            this.DrawGridVert = br.ReadBoolean();
            if (br.ReadBoolean())
            {
                this.scale = new PlaneScale();
                this.scale.ReadFromStream(br);
            }
        }
        public virtual void WriteToStream(BinaryWriter bw)
        {
            bw.Write(this.Visible);
            bw.Write(this.DrawGridHor);
            bw.Write(this.DrawGridVert);
            bw.Write(this.scale != null);
            if (this.scale != null) scale.WriteToStream(bw);
        }

        public virtual PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y) { return null; }
        public virtual void UpdateByObject(PlaneBaseObject BaseObject)
        {
            this.Visible = BaseObject.Visible;
        }
        public virtual void SetMinMaxScale() { }
        

        public virtual void DrawGrid(Graphics grfx)
        {
            if (this.Visible)
            {
                float X = Coord.X + HotSpot.X;
                if ((DrawGridVert) && (scale != null))
                {
                    List<float> shifts = scale.GetShifts();
                    for (int i = 0; i < shifts.Count; i++)
                    {
                        grfx.DrawLine(Pens.LightGray, X + shifts[i], HotSpot.Y + Coord.Y, X + shifts[i], HotSpot.Y + PlaneRect.Height);
                    }
                }
                if (DrawGridHor)
                {
                    float depthStep = 2;
                    if ((depthStep * YUnitsInDepthUnit < 22) && (depthStep > 0))
                    {
                        while (depthStep * YUnitsInDepthUnit < 22)
                        {
                            depthStep += 2;
                        }
                    }
                    else if (depthStep * YUnitsInDepthUnit > 40)
                    {
                        while ((depthStep * YUnitsInDepthUnit > 40) && (depthStep > 2))
                        {
                            depthStep -= 2;
                        }
                    }
                   
                    float Y, Y0 = this.HotSpot.Y + this.Coord.Y;
                    float dY = depthStep * YUnitsInDepthUnit;
                    float depth = MinDepth;
                    while (depth <= MaxDepth)
                    {
                        Y = Y0 + (depth - MinDepth) * YUnitsInDepthUnit;
                        if (Y + depthStep * YUnitsInDepthUnit > HotSpot.Y + PlaneRect.Height) break;
                        if (Y > 0)
                        {
                            grfx.DrawLine(Pens.LightGray, X, Y, X + Width, Y);
                        }
                        depth += depthStep;
                    }
                }
            }
        }
        public virtual void DrawBack(Graphics grfx) { }
        public virtual void DrawObject(Graphics grfx) { }
        public virtual void DrawFront(Graphics grfx) { }
        public virtual void DrawSelected(Graphics grfx) { }

        // DEPTH
        public bool InitDepthCalcFromTrajectory { get { return DepthCalc.InitFromTrajectory; } }
        public float GetMDByAbs(float Abs)
        {
            if (DepthCalc != null) return DepthCalc.GetMDByAbs(Abs);
            return -1;
        }
        public float GetAbsByMD(float MD) 
        {
            if (DepthCalc != null) return DepthCalc.GetAbsByMD(MD); 
            return -1;
        }
        public bool IsDepthByTrajectory()
        {
            return DepthCalc.InitFromTrajectory;
        }
        public virtual void SetDepthCalc(DepthCalculator DepthCalc) { this.DepthCalc = DepthCalc; }
        public virtual float GetGisMedianDepth()  { return -1.0f; }
        public virtual float GetLastPerfDepth()   { return -1.0f; }

        protected float AbsDepthFromScreenY(float screenY)
        {
            return MinDepth + (screenY - HotSpot.Y - this.Coord.Y) / YUnitsInDepthUnit;
        }
        protected float ScreenYFromAbsDepth(float depth)
        {
            return (depth - MinDepth) * YUnitsInDepthUnit + HotSpot.Y + this.Coord.Y;
        }

        public virtual void LoadData(BackgroundWorker worker, DoWorkEventArgs e, Project proj, Well w, MainForm mf) { }
        public virtual void LoadData(BackgroundWorker worker, DoWorkEventArgs e, Project proj, Well w) { }
        public virtual void LoadData(Project proj, Well w, MainForm mf) { }
        public virtual void LoadData(Project proj, Well w) { }

        public virtual string GetHeadText() { return ""; }
    }

    // ОБЪЕКТ - ГЛУБИНЫ
    public sealed class PlaneDepths : PlaneBaseObject
    {
        string HeadText;
        Font font;
        public bool ShowDepthByAbs;
        // DATA
        float depthStep;
        public override float YUnitsInDepthUnit
        {
            get
            {
                return base.YUnitsInDepthUnit;
            }
            set
            {
                base.YUnitsInDepthUnit = value;
                if ((depthStep * YUnitsInDepthUnit < 22) && (depthStep > 0))
                {
                    while (depthStep * YUnitsInDepthUnit < 22)
                    {
                        depthStep += 4;
                    }
                }
                else if (depthStep * YUnitsInDepthUnit > 40)
                {
                    while ((depthStep * YUnitsInDepthUnit > 40) && (depthStep > 4))
                    {
                        depthStep -= 4;
                    }
                }
            }
        }

        // TOOLTIP
        public override PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y)
        {
            PlaneToolTip toolTip = null;
            float abs = AbsDepthFromScreenY(Y);
            float md = GetMDByAbs(abs);
            return toolTip;
        }
        
        // METHODS
        public PlaneDepths(): base()
        {
            TypeID = Constant.Plane.ObjectType.DEPTH_MARKS;
            HeadText = "Абс.отметки";
            font = new Font("Tahoma", 8);
            depthStep = 4;
            ShowDepthByAbs = true;
        }
        public PlaneDepths(PlaneDepths BaseDepth) : base()
        {
            TypeID = Constant.Plane.ObjectType.DEPTH_MARKS;
            HeadText = BaseDepth.HeadText;
            font = BaseDepth.font;
            depthStep = BaseDepth.depthStep;
            ShowDepthByAbs = BaseDepth.ShowDepthByAbs;
        }

        public override string GetHeadText()
        {
            return HeadText;
        }
        public override void DrawObject(Graphics grfx)
        {
            if (this.Visible)
            {
                float X = this.HotSpot.X + this.Coord.X;

                float dY = depthStep * YUnitsInDepthUnit;
                float depth = MinDepth + depthStep, depthMD = 0;
                float Y, Y0 = this.HotSpot.Y + this.Coord.Y;
                string str;
                SizeF szStr;
                int lineW = 5;
                Brush br;
                Pen pen;
                if (this.ShowDepthByAbs)
                {
                    if (!InitDepthCalcFromTrajectory)
                    {
                        br = Brushes.DarkRed;
                        pen = Pens.DarkRed;
                    }
                    else
                    {
                        br = Brushes.Blue;
                        pen = Pens.Blue;
                    }
                }
                else
                {
                    br = Brushes.Black;
                    pen = Pens.Black;
                }

                while (depth <= MaxDepth)
                {
                    Y = Y0 + (depth - MinDepth) * YUnitsInDepthUnit;
                    if (Y + depthStep * YUnitsInDepthUnit > HotSpot.Y + PlaneRect.Height) break;
                    if (Y > 0)
                    {
                        if (!this.ShowDepthByAbs)
                        {
                            depthMD = GetMDByAbs(depth);
                            str = depthMD.ToString("0");
                        }
                        else
                        {
                            str = depth.ToString("0");
                        }
                        szStr = grfx.MeasureString(str, font, PointF.Empty, StringFormat.GenericTypographic);
                        if (szStr.Width + 10 > this.Width)
                        {
                            while (szStr.Width + 2 * lineW > this.Width && (lineW > 1))
                            {
                                lineW--;
                            }
                        }


                        if (szStr.Width + 2 * lineW <= this.Width)
                        {
                            grfx.DrawString(str, font, br, X + Width / 2 - szStr.Width / 2, Y - szStr.Height / 2, StringFormat.GenericTypographic);
                        }
                        else
                            lineW = 5;
                        grfx.DrawLine(pen, X, Y, X + lineW, Y);
                        grfx.DrawLine(pen, X + Width - lineW, Y, X + Width, Y);
                    }
                    depth += depthStep;
                }
            }
        }
    }

    // ОБЪЕКТ - КАРОТАЖ
    public sealed class PlaneLog : PlaneBaseObject
    {
        // DRAWING
        Pen pen;
        public bool FillRegion = false;
        // DATA
        public Constant.Plane.CurveType CurveType;
        public string MnemonicMask;
        int[] MnemonicInd;
        int DemensionID;
        float[] stepsDepth;
        float[][] locMinMaxDepthMD;
        float[][] Values;
        float NullValue;
        public bool ShowDuplicates;
        List<int> MainLogs;
        public Color LineColor
        {
            get
            {
                if (pen != null) return pen.Color;
                return Color.Black;
            }
            set
            {
                if (pen.Color != value)
                {
                    pen = new Pen(value, pen.Width);
                }
            }
        }
        public int LineWidth
        {
            get
            {
                if (pen != null) return (int)pen.Width;
                return 1;
            }
            set
            {
                if (pen.Width != value)
                {
                    pen = new Pen(pen.Color, value);
                }
            }
        }
        public System.Drawing.Drawing2D.DashStyle LineStyle
        {
            get
            {
                if (pen != null) return pen.DashStyle;
                return System.Drawing.Drawing2D.DashStyle.Solid;
            }
            set
            {
                if (pen == null) pen = new Pen(LineColor, LineWidth);
                if (pen.DashStyle != value)
                {
                    Pen p = new Pen(LineColor, LineWidth);
                    p.DashStyle = value;
                    pen = p;
                }
            }
        }

        public bool BackInverted;
        public int BackTransparent;
        public Color BackColor;

        // METHOD
        public PlaneLog(Constant.Plane.CurveType CurveType, string MnemonicMask) : base()
        {
            TypeID = Constant.Plane.ObjectType.LOG;
            this.ShowDuplicates = false;
            this.MainLogs = null;
            this.MnemonicMask = MnemonicMask;
            this.MnemonicInd = null;
            this.CurveType = CurveType;
            this.Values = null;
            this.LineStyle = DashStyle.Solid;
            this.BackTransparent = 51;
            this.BackColor = Color.Gray;
            this.BackInverted = false;
            this.DrawGridHor = false;
            this.DrawGridVert = false;
            switch (CurveType)
            {
                case Constant.Plane.CurveType.PS:
                    this.pen = new Pen(Color.Red);
                    FillRegion = true;
                    break;
                case Constant.Plane.CurveType.GK:
                    this.pen = new Pen(Color.Black);
                    FillRegion = true;
                    break;
                case Constant.Plane.CurveType.IK:
                    this.pen = new Pen(Color.Blue);
                    FillRegion = false;
                    break;
                case Constant.Plane.CurveType.BK:
                    this.pen = new Pen(Color.Maroon);
                    FillRegion = false;
                    break;
                case Constant.Plane.CurveType.NGK:
                    this.pen = new Pen(Color.DarkGreen);
                    FillRegion = false;
                    break;
                default:
                    this.pen = new Pen(Color.Black);
                    FillRegion = false;
                    break;
            }
        }
        public PlaneLog(PlaneLog SchemeLog) : this(SchemeLog.CurveType, SchemeLog.MnemonicMask) 
        {
            UpdateByObject(SchemeLog);
        }
        public PlaneLog() : this(Constant.Plane.CurveType.BK, "") { }

        // TOOLTIP
        public override PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y)
        {
            PlaneToolTip toolTip = null;
            if ((this.Visible) && (Values != null) && (this.HotSpot.X + this.Coord.X - 20 < X) && (X < this.HotSpot.X + this.Coord.X + this.Width + 20))
            {
                for (int v = 0; v < Values.Length; v++)
                {
                    bool pass = false;
                    if ((!ShowDuplicates) && (MainLogs != null) && (MainLogs.IndexOf(v) == -1)) continue;
                    int i;
                    float md1 = GetMDByAbs(AbsDepthFromScreenY(Y - 5)), md2 = GetMDByAbs(AbsDepthFromScreenY(Y + 5));
                    if (md1 > 0)
                    {
                        float startMD = -1;
                        int ind1 = -1, ind2 = -1;
                        float depthMD = locMinMaxDepthMD[v][0];
                        float X0 = this.HotSpot.X + this.Coord.X, Y0 = HotSpot.Y + this.Coord.Y, X1, Y1;
                        double dist, minDist = -1;
                        for (i = 0; i < Values[v].Length; i++)
                        {
                            if ((ind1 == -1) && (depthMD > md1))
                            {
                                ind1 = i;
                                startMD = depthMD;
                            }
                            if ((ind2 == -1) && (depthMD > md2))
                            {
                                ind2 = i;
                                break;
                            }
                            depthMD += this.stepsDepth[v];
                        }
                        if ((ind1 != -1) && (ind2 == -1)) ind2 = Values[v].Length;
                        if (ind1 != -1)
                        {
                            depthMD = startMD;
                            for (i = ind1; (i < ind2) && (i < Values[v].Length); i++)
                            {
                                if (Values[v][i] != NullValue)
                                {
                                    X1 = X0 + scale.GetXShift(Values[v][i]);
                                    Y1 = ScreenYFromAbsDepth(GetAbsByMD(depthMD));
                                    if (Y1 > this.Coord.Y)
                                    {
                                        dist = Math.Sqrt(Math.Pow(X - X1, 2) + Math.Pow(Y - Y1, 2));
                                        if ((dist < 20) && ((minDist < 0) || (dist < minDist)))
                                        {
                                            minDist = dist;
                                            if (toolTip == null) toolTip = new PlaneToolTip();
                                            toolTip.Color = this.LineColor.ToArgb();
                                            toolTip.Object = this;
                                            toolTip.Point = new PointF(X1, Y1);
                                            toolTip.ScreenDist = minDist;
                                            toolTip.Text = string.Format("{0} - {1:0.##} ({2:0.#})", this.scale.SmallText, Values[v][i], depthMD);
                                        }
                                    }
                                }
                                depthMD += this.stepsDepth[v];
                            }
                        }
                    }
                }
            }
            return toolTip;
        }

        // STREAM
        public override void ReadFromStream(BinaryReader br)
        {
            base.ReadFromStream(br);
            CurveType = (Constant.Plane.CurveType)br.ReadInt32();
            MnemonicMask = br.ReadString();
            DemensionID = br.ReadInt32();
            ShowDuplicates = br.ReadBoolean();
            ShowDuplicates = false;
            Color lineClr = Color.FromArgb(br.ReadInt32());
            int lineWidth = br.ReadInt32();
            pen = new Pen(lineClr, lineWidth);
            LineStyle = (DashStyle)br.ReadByte();
            FillRegion = br.ReadBoolean();
            BackInverted = br.ReadBoolean();
            BackTransparent = br.ReadInt32();
            BackColor = Color.FromArgb(br.ReadInt32());
        }
        public override void WriteToStream(BinaryWriter bw)
        {
            base.WriteToStream(bw);
            bw.Write((int)CurveType);
            bw.Write(MnemonicMask);
            bw.Write(DemensionID);
            bw.Write(ShowDuplicates);
            bw.Write(LineColor.ToArgb());
            bw.Write(LineWidth);
            bw.Write((byte)LineStyle);
            bw.Write(FillRegion);
            bw.Write(BackInverted);
            bw.Write(BackTransparent);
            bw.Write(BackColor.ToArgb());
        }

        public override void UpdateByObject(PlaneBaseObject BaseObject)
        {
            base.UpdateByObject(BaseObject);
            PlaneLog log = (PlaneLog)BaseObject;
            this.FillRegion = log.FillRegion;
            this.pen = log.pen;
            this.BackInverted  = log.BackInverted;
            this.BackTransparent = log.BackTransparent;
            this.BackColor = log.BackColor;
            this.LineStyle = log.LineStyle;
            if (log.scale != null)
            {
                if (scale == null) scale = new PlaneScale(log.scale);
                scale.pen = log.scale.pen;
                scale.brush = log.scale.brush;
                this.scale.MinMaxAuto = log.scale.MinMaxAuto;
                this.scale.Type = log.scale.Type;
                if (!log.scale.MinMaxAuto)
                {
                    float min, max;
                    min = log.scale.MinValue;
                    if (this.scale.MinMaxUseStep)
                    {
                        this.scale.MinMaxStep = log.scale.MinMaxStep;
                        max = log.scale.MinValue + log.scale.MinMaxStep * 10;
                    }
                    else
                    {
                        max = log.scale.MaxValue;
                    }
                    scale.MinValue = min;
                    scale.MaxValue = max;
                }
                else
                {
                    scale.MinValue = float.MaxValue;
                    scale.MaxValue = float.MinValue;
                    SetMinMaxScale();
                    scale.TestMinMax();
                }
            }
            this.DrawGridHor = log.DrawGridHor;
            this.DrawGridVert = log.DrawGridVert;
        }

        public void SetMnemonicIndexByMask(Project proj)
        {
            MnemonicInd = null;
            if (MnemonicMask != "")
            {
                int i, j, k, pos = 0, pos2;
                char[] sep = { '|' };
                char[] del = { '*' };
                string mask;
                string str;
                bool find = false;
                var dict = (WellLogMnemonicDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LOG_MNEMONIC);
                
                ArrayList outList = new ArrayList();
                ArrayList list = new ArrayList();
                string[] parseMask = MnemonicMask.Split(sep);
                string[] parseStr = null;
                for (k = 0; k < parseMask.Length; k++)
                {
                    mask = parseMask[k];
                    parseStr = parseMask[k].Split(del, StringSplitOptions.RemoveEmptyEntries);

                    if (parseStr.Length > 0)
                    {
                        for (i = 1; i < dict.Count; i++)
                        {
                            str = dict[i].ShortName;
                            pos = 0; pos2 = 0;
                            find = true;

                            for (j = 0; j < parseStr.Length; j++)
                            {
                                pos2 = str.IndexOf(parseStr[j], pos);

                                if ((pos2 != -1) && ((j > 0) || ((j == 0) && (pos2 == 0))))
                                {
                                    pos = pos2 + (parseStr[j]).Length;
                                }
                                else
                                {
                                    find = false;
                                    break;
                                }
                            }
                            if (find) outList.Add(i);
                        }
                    }
                }
                if (outList.Count > 0)
                {
                    MnemonicInd = new int[outList.Count];
                    for (i = 0; i < outList.Count; i++)
                    {
                        MnemonicInd[i] = (int)outList[i];
                    }
                }
            }
        }
        bool IsSupportMnemonicInd(int MnemonicIndex)
        {
          for (int i = 0; i < MnemonicInd.Length; i++)
          {
              if (MnemonicInd[i] == MnemonicIndex) return true;
          }
          return false;
        }

        public override void SetMinMaxScale()
        {
            if ((scale != null) && (Values != null) && (locMinMaxDepthMD != null) && (scale.MinMaxAuto))
            {
                float depth, depthAbs;
                float MinVal = float.MaxValue, MaxVal = float.MinValue;
                for (int j = 0; j < Values.Length; j++)
                {
                    depth = locMinMaxDepthMD[j][0];
                    depthAbs = GetAbsByMD(depth);
                    for (int i = 0; i < Values[j].Length; i++)
                    {
                        if ((depthAbs >= MinDepth) && (depthAbs <= MaxDepth) && (Values[j][i] != NullValue))
                        {
                            if (MinVal > Values[j][i]) MinVal = Values[j][i];
                            if (MaxVal < Values[j][i]) MaxVal = Values[j][i];
                        }
                        depth += this.stepsDepth[j];
                        depthAbs = GetAbsByMD(depth);
                        if (depthAbs > MaxDepth) break;
                    }
                }
                if (MinVal != float.MaxValue)
                {
                    scale.SetMinMaxAuto(MinVal, MaxVal);
                }
                else
                {
                    scale.DefaultMinMax();
                }
            }
        }
        internal class GisIndexes
        {
            public List<int> Indexes;
            public int Sum;
            public int Count { get { return Indexes.Count; } }
            public GisIndexes()
            {
                Indexes = new List<int>();
                Sum = 0;
            }
            public void Add(int index)
            {
                Indexes.Add(index);
                Sum += index;
            }
            public bool Equal(GisIndexes GisIndexes)
            {
                if ((GisIndexes.Count == Count) && (GisIndexes.Sum == Sum))
                {
                    return true;
                }
                bool equal = true;
                for (int i = 0; i < GisIndexes.Count; i++)
                {
                    if (Indexes.IndexOf(GisIndexes.Indexes[i]) == -1)
                    {
                        return false;
                    }
                }
                return equal;
            }
        }
        public override void LoadData(BackgroundWorker worker, DoWorkEventArgs e, Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                var dictMnemonic = (WellLogMnemonicDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LOG_MNEMONIC);
                var dictUnit = (DataDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LOG_UNIT);
                if (MnemonicInd == null) SetMnemonicIndexByMask(proj);

                if ((MnemonicInd != null) && (MnemonicInd.Length > 0))
                {
                    if ((!w.LogsBaseLoaded) && (w.OilFieldIndex > -1))
                    {
                        (proj.OilFields[w.OilFieldIndex]).LoaвLogsBaseFromCache(w.Index);
                    }
                    if (w.LogsBaseLoaded)
                    {
                        int i, j, k, ind;
                        this.MainLogs = null;
                        int pos;
                        float GisMinValue = float.MinValue, GisMaxValue = float.MaxValue;
                        if ((w.GisLoaded) && (w.gis.Count > 0))
                        {
                            for (i = 0; i < w.gis.Count; i++)
                            {
                                if (w.gis.Items[i].Habs > 0)
                                {
                                    GisMinValue = w.gis.Items[i].Habs;
                                    break;
                                }
                            }
                            for (i = w.gis.Count - 1; i >= 0; i--)
                            {
                                if (w.gis.Items[i].Habs + w.gis.Items[i].Labs > 0)
                                {
                                    GisMaxValue = w.gis.Items[i].Habs + w.gis.Items[i].Labs;
                                    break;
                                }
                            }
                        }
                        
                        float MinValue = float.MaxValue, MaxValue = float.MinValue;

                        // ищем каротаж с данной мнемоникой
                        locMinDepth = -1;
                        locMaxDepth = -1;
                        List<float[]> valList = new List<float[]>();
                        List<float[]> locMinMaxList = new List<float[]>();
                        List<float> stepsList = new List<float>();
                        string mnemonicName = string.Empty;
                        for (i = 0; i < w.logsBase.Length; i++)
                        {
                            ind = dictMnemonic.GetIndexByCode(w.logsBase[i].MnemonikaId);
                            if ((ind > -1) && (this.IsSupportMnemonicInd(ind)))
                            {
                                stepsList.Add((float)Math.Round(w.logsBase[i].Step, 2));
                                float[] locMinMax = new float[3];
                                locMinMax[0] = w.logsBase[i].StartDepth;
                                locMinMax[1] = w.logsBase[i].EndDepth;
                                locMinMax[2] = w.logsBase[i].EndDepth - w.logsBase[i].StartDepth;

                                if ((locMinDepth == -1) || (locMinDepth > w.logsBase[i].StartDepth)) locMinDepth = w.logsBase[i].StartDepth;
                                if ((locMaxDepth == -1) || (locMaxDepth < w.logsBase[i].EndDepth)) locMaxDepth = w.logsBase[i].EndDepth;

                                locMinMaxList.Add(locMinMax);

                                NullValue = w.logsBase[i].NullValue;
                                if (valList.Count == 0)
                                {
                                    MinValue = w.logsBase[i].MinValue;
                                    MaxValue = w.logsBase[i].MaxValue;
                                }
                                else
                                {
                                    if (MinValue > w.logsBase[i].MinValue) MinValue = w.logsBase[i].MinValue;
                                    if (MaxValue < w.logsBase[i].MaxValue) MaxValue = w.logsBase[i].MaxValue;
                                }
                                valList.Add(w.logsBase[i].Items);
                                w.logsBase[i].SetItems(null);
                                if (scale != null)
                                {
                                    if ((dictMnemonic != null) && (dictUnit != null))
                                    {
                                        if (dictMnemonic[ind].Code > 0)
                                        {
                                            if ((scale.Text.Length > 0) && (scale.ObjIndex != null) && (scale.ObjIndex.Length > 0))
                                            {
                                                if ((scale.Text.Length > 0) && (scale.Text.IndexOf(dictMnemonic[ind].ShortName) == -1))
                                                {
                                                    scale.Text += ",";
                                                    scale.Text += dictMnemonic[ind].ShortName;
                                                }
                                            }
                                            else
                                            {
                                                scale.Text = dictMnemonic[ind].ShortName;
                                            }
                                            if (scale.SmallText.Length == 0)
                                            {
                                                pos = dictMnemonic[ind].ShortName.IndexOf('.');
                                                if (pos > 0)
                                                {
                                                    scale.SmallText = dictMnemonic[ind].ShortName.Remove(pos, dictMnemonic[ind].ShortName.Length - pos);
                                                    scale.Unit = dictMnemonic[ind].ShortName.Remove(0, pos + 1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if ((valList.Count > 0) && (locMinMaxList.Count == valList.Count))
                        {
                            Values = new float[valList.Count][];
                            for (i = 0; i < valList.Count; i++)
                            {
                                Values[i] = valList[i];
                            }
                            if (locMinDepth != -1) locMinDepth = GetAbsByMD(locMinDepth);
                            if (locMaxDepth != -1) locMaxDepth = GetAbsByMD(locMaxDepth);

                            locMinMaxDepthMD = new float[locMinMaxList.Count][];
                            this.stepsDepth = new float[stepsList.Count];
                            for (i = 0; i < locMinMaxList.Count; i++)
                            {
                                locMinMaxDepthMD[i] = locMinMaxList[i];
                                this.stepsDepth[i] = stepsList[i];
                            }

                            if (w.GisLoaded)
                            {
                                List<int> mainLogs = new List<int>();
                                GisIndexes[] LogsGisIndexes = new GisIndexes[locMinMaxList.Count];
                                for (i = 0; i < locMinMaxList.Count; i++)
                                {
                                    LogsGisIndexes[i] = new GisIndexes();
                                    for (j = 0; j < w.gis.Count; j++)
                                    {
                                        if (((locMinMaxList[i][0] <= w.gis.Items[j].H) && (w.gis.Items[j].H <= locMinMaxList[i][1])) ||
                                           ((locMinMaxList[i][0] <= w.gis.Items[j].H + w.gis.Items[j].L) && (w.gis.Items[j].H + w.gis.Items[j].L <= locMinMaxList[i][1])) ||
                                           ((w.gis.Items[j].H <= locMinMaxList[i][0]) && (locMinMaxList[i][1] <= w.gis.Items[j].H + w.gis.Items[j].L)))
                                        {
                                            LogsGisIndexes[i].Add(j);
                                        }
                                    }
                                }
                                k = 0;
                                while (k != -1)
                                {
                                    k = -1;
                                    for (i = 0; i < LogsGisIndexes.Length; i++)
                                    {
                                        if ((LogsGisIndexes[i].Count > 0) && (mainLogs.IndexOf(i) == -1) && ((k == -1) || (!LogsGisIndexes[i].Equal(LogsGisIndexes[k])) || (locMinMaxList[i][2] > locMinMaxList[k][2])))
                                        {
                                            if (mainLogs.Count == 0)
                                            {
                                                k = i;
                                            }
                                            else
                                            {
                                                for (j = 0; j < mainLogs.Count; j++)
                                                {
                                                    if ((!LogsGisIndexes[i].Equal(LogsGisIndexes[mainLogs[j]])) && (!LogsGisIndexes[mainLogs[j]].Equal(LogsGisIndexes[i])))
                                                    {
                                                        k = i;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (k != -1) mainLogs.Add(k);
                                }
                                if (mainLogs.Count > 0) MainLogs = mainLogs;
                            }
                        }
                    }
                }
            }
        }
        public override void DrawBack(Graphics grfx)
        {
            if (this.Visible)
            {
                if ((Values != null) && (FillRegion))
                {
                    for (int v = 0; v < Values.Length; v++)
                    {
                        if ((!ShowDuplicates) && (MainLogs != null) && (MainLogs.IndexOf(v) == -1)) continue;
                        float[] values = Values[v];

                        List<PointF> scrPointsList = new List<PointF>();
                        List<PointF[]> polygonList = new List<PointF[]>();
                        float X = HotSpot.X + Coord.X, predX, shift = 0;

                        bool pass = false;
                        float Y = 0, Y0 = HotSpot.Y + Coord.Y, predY, depthMD, depthAbs;
                        float dY = this.stepsDepth[v];
                        float startX = float.MinValue, startY = float.MinValue, lastX = -1, lastY = -1;
                        int i;
                        bool start = false, hole = false;

                        depthMD = locMinMaxDepthMD[v][0];
                        depthAbs = GetAbsByMD(depthMD);
                        predY = Y0 + (depthAbs - MinDepth) * YUnitsInDepthUnit;
                        predX = X;

                        depthMD += this.stepsDepth[v];
                        for (i = 1; i < values.Length; i++)
                        {
                            hole = false;
                            depthAbs = GetAbsByMD(depthMD);
                            Y = Y0 + (depthAbs - MinDepth) * YUnitsInDepthUnit;
                            if (Y > ScreenMaxY) break;

                            if (Y > HotSpot.Y + PlaneRect.Height) break;
                            if ((Y > ScreenMinY) && (values[i] != NullValue) && (values[i - 1] != NullValue))
                            {
                                if (!start) start = true;

                                if (startX == float.MinValue)
                                {
                                    startX = X + scale.GetXShift(values[i - 1]);
                                    predX = startX;
                                    startY = predY;
                                    hole = true;
                                }
                                shift = scale.GetXShift(values[i]);
                                lastX = X + scale.GetXShift(values[i]);
                                lastY = Y;
                                pass = false;
                                if ((lastY - predY < 0.5F) && (!hole)) pass = true;
                                if (!pass) scrPointsList.Add(new PointF(lastX, lastY));
                            }
                            else if ((start) && (values[i] == NullValue))
                            {
                                if (!BackInverted)
                                {
                                    scrPointsList.Add(new PointF(X, lastY));
                                    scrPointsList.Add(new PointF(X, startY));
                                    scrPointsList.Add(new PointF(startX, startY));
                                }
                                else
                                {
                                    scrPointsList.Add(new PointF(X + Width, lastY));
                                    scrPointsList.Add(new PointF(X + Width, startY));
                                    scrPointsList.Add(new PointF(startX, startY));
                                }
                                scrPointsList.ToArray();
                                polygonList.Add(scrPointsList.ToArray());
                                scrPointsList.Clear();
                                startX = float.MinValue;
                                startY = float.MinValue;
                                start = false;
                                pass = false;
                            }
                            if (!pass)
                            {
                                predY = Y;
                                predX = X + shift;
                            }
                            depthMD += this.stepsDepth[v];
                        }
                        if ((values.Length > 1) && (startX != float.MinValue) && (lastX != -1))
                        {
                            if ((!BackInverted && (scale.Type == Constant.Plane.ScaleType.LINEAR) || (scale.Type == Constant.Plane.ScaleType.LOG10)) ||
                                (BackInverted && (scale.Type == Constant.Plane.ScaleType.INVERT_LINEAR) || (scale.Type == Constant.Plane.ScaleType.INVERT_LOG10)))
                            {
                                scrPointsList.Add(new PointF(X, lastY));
                                scrPointsList.Add(new PointF(X, startY));
                                scrPointsList.Add(new PointF(startX, startY));
                            }
                            else
                            {
                                scrPointsList.Add(new PointF(X + Width, lastY));
                                scrPointsList.Add(new PointF(X + Width, startY));
                                scrPointsList.Add(new PointF(startX, startY));
                            }
                        }
                        if (scrPointsList.Count > 0) polygonList.Add(scrPointsList.ToArray());
                        scrPointsList.Clear();
                        using (Brush br = new SolidBrush(Color.FromArgb(BackTransparent, BackColor)))
                        {
                            for (i = 0; i < polygonList.Count; i++)
                            {
                                grfx.FillPolygon(br, polygonList[i]);
                            }
                        }
                    }
                }
            }
        }
        public override void DrawObject(Graphics grfx)
        {
            if ((this.Visible) && (Values != null))
            {
                for (int v = 0; v < Values.Length; v++)
                {
                    if ((!ShowDuplicates) && (MainLogs != null) && (MainLogs.IndexOf(v) == -1)) continue;

                    float[] values = Values[v];

                    List<PointF> points = new List<PointF>();
                    List<PointF[]> pointsList = new List<PointF[]>();
                    float X = HotSpot.X + Coord.X, predX, shift = 0;

                    bool pass = false;
                    float Y = 0, Y0 = HotSpot.Y + Coord.Y, predY, depthMD, depthAbs;
                    float dY = this.stepsDepth[v];
                    float startX = float.MinValue, startY = float.MinValue, lastX = -1, lastY = -1;
                    int i;
                    bool start = false, hole = false;

                    depthMD = locMinMaxDepthMD[v][0];
                    depthAbs = GetAbsByMD(depthMD);
                    predY = Y0 + (depthAbs - MinDepth) * YUnitsInDepthUnit;

                    depthMD += this.stepsDepth[v];
                    depthAbs = GetAbsByMD(depthMD);
                    predX = X;
                    for (i = 1; i < values.Length; i++)
                    {
                        hole = false;
                        depthAbs = GetAbsByMD(depthMD);
                        Y = Y0 + (depthAbs - MinDepth) * YUnitsInDepthUnit;
                        if (Y > ScreenMaxY) break;
                        if (Y > HotSpot.Y + PlaneRect.Height) break;

                        if ((Y > ScreenMinY) && (values[i] != NullValue) && (values[i - 1] != NullValue))
                        {
                            if (!start) start = true;

                            if (startX == float.MinValue)
                            {
                                startX = X + scale.GetXShift(values[i - 1]);
                                predX = startX;
                                startY = predY;
                                hole = true;
                            }
                            shift = scale.GetXShift(values[i]);
                            lastX = X + scale.GetXShift(values[i]);
                            lastY = Y;
                            pass = false;
                            if ((lastY - predY < 0.5F) && (!hole)) pass = true;
                            if (!pass) points.Add(new PointF(lastX, lastY));
                        }
                        else if ((start) && (values[i] == NullValue))
                        {
                            points.ToArray();
                            pointsList.Add(points.ToArray());
                            points.Clear();
                            startX = float.MinValue;
                            startY = float.MinValue;
                            start = false;
                            pass = false;
                        }
                        if (!pass)
                        {
                            predY = Y;
                            predX = X + shift;
                        }
                        depthMD += this.stepsDepth[v];
                    }
                    if (points.Count > 0) pointsList.Add(points.ToArray());
                    points.Clear();
                    for (i = 0; i < pointsList.Count; i++)
                    {
                        if (pointsList[i].Length > 1) grfx.DrawLines(pen, pointsList[i]);
                    }
                }
            }
        }
    }

    // ОБЪЕКТ - КЕРН
    public sealed class PlaneCore : PlaneBaseObject
    {
        internal class PlaneCoreItem
        {
            public float Top;
            public float Bottom;
            public float TopAbs;
            public float BottomAbs;
        }
        internal class PlaneCoreTestItem
        {
            public float Depth;
            public float DepthAbs;
            public byte SatId;
            public float TopCore;
            public float BottomCore;
        }

        PlaneCoreItem[] Items;
        PlaneCoreTestItem[] ItemsTest;

        Brush br;
        TextureBrush textBrush;
        public PlaneCore() : base()
        {
            TypeID = Constant.Plane.ObjectType.CORE;
            br = new SolidBrush(Color.Yellow);
            textBrush = new TextureBrush(Properties.Resources.line45_16);
            textBrush.WrapMode = WrapMode.Tile;
        }
        public PlaneCore(PlaneCore BaseCore) : base()
        {
            TypeID = Constant.Plane.ObjectType.CORE;
            br = BaseCore.br;
            textBrush = BaseCore.textBrush;
        }

        public override PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y)
        {
            float X0 = this.HotSpot.X + Coord.X;
            float X2 = X0 + this.Width / 2;
            float Y0 = this.HotSpot.Y + Coord.Y, Y1, Y2;

            PlaneToolTip toolTip = null;
            if ((this.Visible) && (X0 < X) && (X < X0 + Width) && (Items != null || ItemsTest != null))
            {
                float abs = AbsDepthFromScreenY(Y);
                //string str;
                float r = this.Width / 4;
                if (r > 10) r = 7;
                if (ItemsTest != null && (X2 - r <= X) && (X <= X2 + r))
                {
                    for (int i = 0; i < ItemsTest.Length; i++)
                    {
                        Y2 = Y0 + (ItemsTest[i].DepthAbs - MinDepth) * YUnitsInDepthUnit;
                        if ((Y2 - r <= Y) && (Y <= Y2 + r))
                        {
                            toolTip = new PlaneToolTip();
                            toolTip.Color = Color.Yellow.ToArgb();
                            toolTip.Object = this;

                            toolTip.Point = new PointF(X2, Y2);
                            toolTip.ScreenDist = 0;
                            toolTip.Top = ItemsTest[i].TopCore;
                            toolTip.Bottom = ItemsTest[i].BottomCore;
                            toolTip.Title = "Керн:исследования";
                            toolTip.Text = string.Format("{0:0.#} - {1:0.#}", ItemsTest[i].TopCore, ItemsTest[i].BottomCore);
                            break;
                        }
                    }
                }
                if(toolTip == null && Items != null)
                {
                    for (int i = 0; i < Items.Length; i++)
                    {
                        if (Items[i].TopAbs <= abs && abs <= Items[i].BottomAbs)
                        {
                            toolTip = new PlaneToolTip();
                            toolTip.Color = Color.Yellow.ToArgb();
                            toolTip.Object = this;

                            toolTip.Point = new PointF(X, Y);
                            toolTip.ScreenDist = 0;
                            toolTip.Top = Items[i].Top;
                            toolTip.Bottom = Items[i].Bottom;
                            toolTip.Title = "Керн:наличие";
                            toolTip.Text = string.Format("{0:0.#} - {1:0.#}", Items[i].Top, Items[i].Bottom);
                            break;
                        }
                    }
                }
            }
            return toolTip;
        }

        // GET DATA
        public float GetDepthMDByCursor(Point curs)
        {
            float res = -1;
            if ((this.Visible) && (ItemsTest != null))
            {
                float X = this.HotSpot.X + Coord.X;
                float X2 = X + this.Width / 2;
                float r = this.Width / 4;

                if ((X2 - r < curs.X) && (curs.X < X2 + r))
                {
                    float Y = this.HotSpot.Y + Coord.Y, Y2;
                    float min = 999, d;
                    PlaneCoreTestItem item;

                    for (int i = 0; i < ItemsTest.Length; i++)
                    {
                        item = this.ItemsTest[i];

                        Y2 = Y + (item.DepthAbs - MinDepth) * YUnitsInDepthUnit;
                        if ((Y2 - r <= curs.Y) && (curs.Y <= Y2 + r))
                        {
                            d = Math.Abs(curs.Y - Y2);
                            if (min > d)
                            {
                                min = d;
                                res = item.Depth;
                            }
                        }
                    }
                }
            }
            return res; 
        }
        public override void LoadData(Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                if (w.GisLoaded)
                {
                    if (!w.CoreLoaded)
                    {
                        (proj.OilFields[w.OilFieldIndex]).LoadCoreFromCache(w.Index);
                    }
                    if (!w.CoreTestLoaded)
                    {
                        (proj.OilFields[w.OilFieldIndex]).LoadCoreTestFromCache(w.Index);
                    }

                    if ((w.CoreLoaded) && (w.core.Count > 0))
                    {
                        SkvCoreItem coreItem;
                        Items = new PlaneCoreItem[w.core.Count];

                        for (int i = 0; i < w.core.Count; i++)
                        {
                            Items[i] = new PlaneCoreItem();
                            coreItem = w.core.Items[i];
                            Items[i].Top = coreItem.Top;
                            Items[i].Bottom = coreItem.Bottom;
                            Items[i].TopAbs = GetAbsByMD(coreItem.Top);
                            Items[i].BottomAbs = GetAbsByMD(coreItem.Bottom);
                        }
                    }
                    if ((w.CoreTestLoaded) && (w.coreTest.Count > 0))
                    {
                        SkvCoreTestItem coreItem;
                        ItemsTest = new PlaneCoreTestItem[w.coreTest.Count];

                        for (int i = 0; i < w.coreTest.Count; i++)
                        {
                            ItemsTest[i] = new PlaneCoreTestItem();
                            coreItem = w.coreTest.Items[i];
                            ItemsTest[i].Depth = coreItem.Top + coreItem.DistanceTop;
                            ItemsTest[i].DepthAbs = GetAbsByMD(this.ItemsTest[i].Depth);
                            ItemsTest[i].SatId = coreItem.SatId;
                            ItemsTest[i].TopCore = coreItem.Top;
                            ItemsTest[i].BottomCore = coreItem.Bottom;
                        }
                    }

                }
            }
        }
        public override void DrawObject(Graphics grfx)
        {
            if (this.Visible)
            {
                float X = this.HotSpot.X + Coord.X;
                float X2 = X + this.Width;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;
                if (Items != null)
                {
                    PlaneCoreItem item;

                    for (int i = 0; i < Items.Length; i++)
                    {
                        item = this.Items[i];

                        Y = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                        Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                        if (Y > HotSpot.Y + PlaneRect.Height) break;
                        if (Y2 < 0) continue;
                        if (X2 > this.HotSpot.X + PlaneRect.Right) continue;
                       
                        grfx.FillRectangle(textBrush, X, Y, this.Width, Y2 - Y);
                        grfx.DrawRectangle(Pens.Black, X, Y, this.Width, Y2 - Y);
                    }
                }
                if (ItemsTest != null)
                {
                    PlaneCoreTestItem item;
                    X2 = X + this.Width / 2;
                    float r = this.Width / 4;
                    if (r > 10) r = 7;
                    float selDepthAbs = -1;
                    for (int i = 0; i < ItemsTest.Length; i++)
                    {
                        item = this.ItemsTest[i];

                        Y = Y0 + (item.DepthAbs - MinDepth) * YUnitsInDepthUnit;
                        if (Y - r > ScreenMaxY) break;
                        if (Y > HotSpot.Y + PlaneRect.Height) break;
                        if (X2 > this.HotSpot.X + PlaneRect.Right) continue;
                        if ((this.Selected) && (item.Depth == SelectedDepthMD))
                        {
                            selDepthAbs = item.DepthAbs;
                        }
                        if (Y + r > ScreenMinY)
                        {
                            grfx.FillEllipse(br, X2 - r, Y - r, 2 * r, 2 * r);
                            grfx.DrawEllipse(Pens.Black, X2 - r, Y - r, 2 * r, 2 * r);
                        }
                    }
                    if ((this.Selected) && (ItemsTest.Length > 0) && (selDepthAbs > -1))
                    {
                        Y = Y0 + (selDepthAbs - MinDepth) * YUnitsInDepthUnit;
                        if (Y < HotSpot.Y + PlaneRect.Height)
                        {
                            grfx.FillEllipse(Brushes.Lime, X2 - r, Y - r, 2 * r, 2 * r);
                            grfx.DrawEllipse(Pens.Black, X2 - r, Y - r, 2 * r, 2 * r);
                        }
                    }
                }
            }
        }
    }

    // ОБЪЕКТ - ГИС
    public sealed class PlaneGis : PlaneBaseObject
    {
        internal class PlaneGisItem
        {
            public float Top;
            public float Bottom;
            public float TopAbs;
            public float BottomAbs;
            public int SaturationInit;
            public int Saturation;
            public int Plast;
            public int BrushIndex;
        }
        PlaneGisItem[] Items;
        SolidBrush[] SatBrushList;
        public PlaneGis() : base()
        {
            TypeID = Constant.Plane.ObjectType.GIS;
            SatBrushList = new SolidBrush[3];
            SatBrushList[0] = new SolidBrush(Color.LightGray);
            SatBrushList[1] = new SolidBrush(Color.FromArgb(200, 122, 85));
            SatBrushList[2] = new SolidBrush(Color.FromArgb(155, 226, 255));
        }
        public PlaneGis(PlaneGis BaseGis) : base()
        {
            TypeID = Constant.Plane.ObjectType.GIS;
            SatBrushList = BaseGis.SatBrushList;
        }

        public override PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y)
        {
            PlaneToolTip toolTip = null;
            if ((this.Visible) && (this.HotSpot.X + this.Coord.X < X) && (X < this.HotSpot.X + this.Coord.X + Width) && (Items != null))
            {
                float abs = AbsDepthFromScreenY(Y);
                int ind;
                string str;
                var dict = (DataDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
                var dictPlast = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                for (int i = 0; i < Items.Length; i++)
                {
                    if ((Items[i].TopAbs <= abs) && (abs <= Items[i].BottomAbs))
                    {
                        toolTip = new PlaneToolTip();
                        toolTip.Color = SatBrushList[Items[i].BrushIndex].Color.ToArgb();
                        toolTip.Object = this;

                        toolTip.Point = new PointF(X, Y + 5);
                        toolTip.ScreenDist = 0;
                        toolTip.Top = Items[i].Top;
                        toolTip.Bottom = Items[i].Bottom;
                        str = "ГИС";

                        if (dictPlast != null)
                        {
                            ind = dictPlast.GetIndexByCode(Items[i].Plast);
                            if (ind != -1) str += string.Format(" ({0})", dictPlast[ind].ShortName);
                        }
                        if (dict != null)
                        {
                            ind = dict.GetIndexByCode(Items[i].SaturationInit);
                            if (str.Length > 0) str += "\n";
                            if (ind != -1) str += dict[ind].FullName;
                        }
                        if (str.Length > 0) str += "\n";
                        str += string.Format("{0:0.#} - {1:0.#}[{2:0.#}]", Items[i].Top, Items[i].Bottom, Items[i].Bottom - Items[i].Top);
                        toolTip.Text = str;
                        break;
                    }
                }
            }
            return toolTip;
        }
        public override float GetGisMedianDepth()
        {
            float median = -1;
            if ((Items != null) && (Items.Length > 0))
            {
                median = 0;
                float sumWidth = 0;
                for (int i = 0; i < Items.Length; i++)
                {
                    median += Items[i].TopAbs * (Items[i].BottomAbs - Items[i].TopAbs);
                    sumWidth += (Items[i].BottomAbs - Items[i].TopAbs);
                }
                median /= sumWidth;
            }
            return median;
        }
        public override void LoadData(Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                if (!w.GisLoaded) (proj.OilFields[w.OilFieldIndex]).LoadGisFromCache(w.Index);
                if (w.GisLoaded)
                {
                    SkvGisItem gisItem;
                    Items = new PlaneGisItem[w.gis.Count];
                    locMinDepth = -1;
                    locMaxDepth = -1;
                    bool isDepthByTrajectory = IsDepthByTrajectory();
                    
                    for (int i = 0; i < w.gis.Count; i++)
                    {
                        this.Items[i] = new PlaneGisItem();
                        gisItem = w.gis.Items[i];
                        if (locMinDepth == -1 && gisItem.Habs > 0) locMinDepth = gisItem.Habs;
                        if (locMaxDepth == -1 && gisItem.Habs + gisItem.Labs > 0) locMaxDepth = gisItem.Habs + gisItem.Labs;

                        this.Items[i].Top = gisItem.H;
                        this.Items[i].Bottom = gisItem.H + gisItem.L;
                        if (!isDepthByTrajectory)
                        {
                            this.Items[i].TopAbs = gisItem.Habs;
                            this.Items[i].BottomAbs = gisItem.Habs + gisItem.Labs;
                        }
                        else
                        {
                            this.Items[i].TopAbs = GetAbsByMD(gisItem.H);
                            this.Items[i].BottomAbs = GetAbsByMD(gisItem.H + gisItem.L);
                        }
                        this.Items[i].SaturationInit = gisItem.Sat0;
                        this.Items[i].Saturation = gisItem.Sat;
                        this.Items[i].Plast = gisItem.PlastId;
                        switch (gisItem.Sat0)
                        {
                            case 1:
                                this.Items[i].BrushIndex = 1;
                                break;
                            case 2:
                                this.Items[i].BrushIndex = 2;
                                break;
                            default:
                                this.Items[i].BrushIndex = 0;
                                break;
                        }
                    }
                }
            }            
        }
        public override void DrawObject(Graphics grfx)
        {
            if ((this.Visible) && (Items != null))
            {
                float X = this.HotSpot.X + Coord.X;
                float X2 = X + this.Width;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;
                Brush br;
                PlaneGisItem item;
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];
                    Y = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                    if (Y > ScreenMaxY) break;
                    Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                    if (Y2 < 0) continue;
                    if (Y2 < ScreenMinY) continue;
                    
                    if (Y > HotSpot.Y + PlaneRect.Height) break;
                    br = SatBrushList[item.BrushIndex];
                    if ((this.Selected) && (item.Top == SelectedDepthMD)) br = Brushes.Lime;
                    if (Y2 - Y > 0.5)
                    {
                        grfx.FillRectangle(br, X, Y, this.Width, Y2 - Y);
                        grfx.DrawRectangle(Pens.Black, X, Y, this.Width, Y2 - Y);
                    }
                }
            }            
        }
    }

    // ОБЪЕКТ - ЗКЦ + Негерметичность
    public sealed class PlaneLeak : PlaneBaseObject
    {
        internal class PlaneLeakItem
        {
            public DateTime Date;
            public ushort PlastCode;
            public float Top;
            public float Bottom;
            public float TopAbs;
            public float BottomAbs;
            public ushort LeakCode;
            public ushort LeakHoleCode;
            public ushort OperationCode;
            public string Comment;
        }

        PlaneLeakItem[] Items;
        TextureBrush textBrush;
        Pen pen1, pen2;

        public PlaneLeak() : base()
        {
            TypeID = Constant.Plane.ObjectType.LEAK;
            textBrush = new TextureBrush(Properties.Resources.line45_16);
            textBrush.WrapMode = WrapMode.Tile;
            pen1 = new Pen(Color.Red, 2);
            pen2 = new Pen(Color.Green, 2);
        }
        public PlaneLeak(PlaneLeak BaseLeak) : base()
        {
            TypeID = Constant.Plane.ObjectType.LEAK;
            pen1 = BaseLeak.pen1;
            pen2 = BaseLeak.pen2;
            textBrush = BaseLeak.textBrush;
        }
        
        // GET DATA
        public override PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y)
        {
            PlaneToolTip toolTip = null;
            if ((this.Visible) && (Items != null))
            {
                float X0 = this.HotSpot.X + Coord.X + 2;
                float X2;
                float Y0 = this.HotSpot.Y + Coord.Y, Y1, Y2;
                float min = 999, d;
                PlaneLeakItem item;
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];
                    X2 = X0 + 4;

                    Y1 = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                    Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                    if ((Y1 - 1 <= Y) && (Y <= Y2 + 1))
                    {
                        if ((X2 - 4 < X) && (X < X2 + 4))
                        {
                            d = Math.Abs(X - X2 - 1);
                            if (min > d)
                            {
                                min = d;
                                if (toolTip == null) toolTip = new PlaneToolTip();
                                toolTip.Color = Color.Black.ToArgb();
                                toolTip.Object = this;
                                toolTip.Point = new PointF(X + 2, Y);
                                toolTip.ScreenDist = 0;
                                toolTip.Title = Items[i].Date.ToString("dd.MM.yyyy");
                                toolTip.Text = string.Empty;
                                switch (Items[i].LeakCode)
                                {
                                    case 0:
                                        toolTip.Title += "\nЗКЦ";
                                        break;
                                    case 1:
                                        toolTip.Title += "\nНегерметичность";
                                        if (Items[i].LeakHoleCode > 0) toolTip.Title += string.Format(" ({0})", WellLeak.GetLeakHoleName(Items[i].LeakHoleCode));
                                        break;
                                }
                                if (Items[i].OperationCode > 0)
                                {
                                    toolTip.Title += string.Format("\n{0}", WellLeak.GetOperationName(Items[i].OperationCode));
                                }
                                else
                                {
                                    toolTip.Title += "\nНе устранена";
                                }
                                if (Items[i].Comment != null && Items[i].Comment.Length > 0) toolTip.Text = Items[i].Comment;
                                toolTip.Top = Items[i].Top;
                                toolTip.Bottom = Items[i].Bottom;
                            }
                        }
                    }
                }
            }
            return toolTip;
        }

        public override void LoadData(Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                if (!w.LeakLoaded)
                {
                    (proj.OilFields[w.OilFieldIndex]).LoadWellLeakFromCache(w.Index);
                }
                if ((w.LeakLoaded) && (w.leak.Count > 0))
                {
                    PlaneLeakItem item;
                    List<PlaneLeakItem> items = new List<PlaneLeakItem>();
                    DateTime lastDate = w.leak[w.leak.Count - 1].Date;
                    for (int i = w.leak.Count - 1; i > -1; i--)
                    {
                        if (w.leak[i].Date != lastDate) break;
                        if (((w.leak[i].LeakType != 2)) && (w.leak[i].Intervals != null))
                        {
                            for (int j = 0; j < w.leak[i].Intervals.Length;j++)
                            {
                                item = new PlaneLeakItem();
                                item.Top = w.leak[i].Intervals[j].Top;
                                item.Bottom = w.leak[i].Intervals[j].Bottom;
                                item.TopAbs = GetAbsByMD(w.leak[i].Intervals[j].Top);
                                item.BottomAbs = GetAbsByMD(w.leak[i].Intervals[j].Bottom);
                                item.LeakCode = w.leak[i].LeakType;
                                item.LeakHoleCode = w.leak[i].LeakHoleType;
                                item.OperationCode = w.leak[i].OperationCode;
                                item.Date = w.leak[i].Date;
                                item.Comment = w.leak[i].Comment;
                                if (item.Comment.Length > 40)
                                {
                                    int pos = item.Comment.IndexOf(' ', 30);
                                    while (pos > 0)
                                    {
                                        item.Comment = item.Comment.Remove(pos, item.Comment.Length - pos) + "\n" + item.Comment.Remove(0, pos + 1);
                                        if (pos + 30 >= item.Comment.Length) break;
                                        pos = item.Comment.IndexOf(' ', pos + 30);
                                    }
                                }
                                items.Add(item);
                            }
                        }
                    }
                    this.Items = items.ToArray();
                }
            }            
        }
        public override void DrawFront(Graphics grfx)
        {
            if ((this.Visible) && (Items != null))
            {
                float X = this.HotSpot.X + Coord.X + 2;
                float X2;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;
                Pen pen;
                PlaneLeakItem item;
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];
                    pen = (item.OperationCode > 0) ? pen2 : pen1;

                    X2 = X + 4;

                    Y = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                    Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                    if (Y2 > HotSpot.Y + PlaneRect.Height) Y2 = HotSpot.Y + PlaneRect.Height;
                    if (Y > HotSpot.Y + PlaneRect.Height) continue;
                    if (Y2 < 0) continue;
                    if (X2 > this.HotSpot.X + PlaneRect.Right) continue;
                    
                    grfx.DrawLine(pen, X, Y, X2, Y);
                    grfx.DrawLine(pen, X2, Y - 1f, X2, Y2 + 1f);
                    grfx.DrawLine(pen, X, Y2, X2, Y2);
                }
            }
        }
    }

    // ОБЪЕКТ - ПЕРФОРАЦИЯ
    public sealed class PlanePerf : PlaneBaseObject
    {
        internal class PlanePerfItem
        {
            public float Top;
            public float Bottom;
            public float TopAbs;
            public float BottomAbs;
            public int PerfTypeId;
            public byte Source;
            public int Level;
            public DateTime Date;
            public bool Selected;
        }

        PlanePerfItem[] Items;
        Pen pen1, pen2, pen3;
        Font font;
        TextureBrush textBrush;
        public new bool Selected
        {
            get { return base.Selected; }
            set
            {
                base.Selected = value;
                if (!value && Items != null)
                {
                    for (int i = 0; i < Items.Length; i++)
                    {
                        Items[i].Selected = false;
                    }
                }
            }
        }
        public PlanePerf() : base()
        {
            TypeID = Constant.Plane.ObjectType.PERFORATION;
            pen1 = new Pen(Color.Black, 2);
            pen2 = new Pen(Color.Black, 4);
            pen3 = new Pen(Color.FromArgb(180, 0 ,0), 1);
            font = new Font("Segoe UI", 10);
            textBrush = new TextureBrush(Properties.Resources.line45_2_16);
            textBrush.WrapMode = WrapMode.Tile;
        }
        public PlanePerf(PlanePerf BasePerf) : base()
        {
            TypeID = Constant.Plane.ObjectType.PERFORATION;
            pen1 = BasePerf.pen1;
            pen2 = BasePerf.pen2;
            pen3 = BasePerf.pen3;
            font = BasePerf.font;
            textBrush = BasePerf.textBrush;
        }
        
        // GET DATA
        public override PlaneToolTip GetToolTip(DictionaryCollection DictList, int X, int Y)
        {
            PlaneToolTip toolTip = null;
            SelectedDepthMD = -1;
            Selected = false;
            if ((this.Visible) && (Items != null))
            {
                float X0 = this.HotSpot.X + Coord.X;
                float X2;
                float Y0 = this.HotSpot.Y + Coord.Y, Y1, Y2;
                float min = 999, d;
                int ind;
                PlanePerfItem item;
                var dict = (DataDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.PERF_TYPE);
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];
                    X2 = X0 + item.Level * 6 + 2;

                    Y1 = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                    Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                    if ((Y1 - 1 <= Y) && (Y <= Y2 + 1))
                    {
                        if ((X2 - 4 < X) && (X < X2 + 6))
                        {
                            d = Math.Abs(X - X2 - 2);
                            if (min > d)
                            {
                                min = d;
                                if (toolTip == null) toolTip = new PlaneToolTip();
                                toolTip.Color = Color.Black.ToArgb();
                                toolTip.Object = this;
                                toolTip.Point = new PointF(X2, Y1);

                                toolTip.ScreenDist = min;
                                toolTip.Title = string.Empty;
                                if((dict != null) && (Items[i].PerfTypeId != 0))
                                {
                                    ind = dict.GetIndexByCode(Items[i].PerfTypeId);
                                    if (ind != -1) toolTip.Title = String.Format(" ({0})", dict[ind].ShortName);
                                }
                                toolTip.Title += string.Format(" - {0}", (Items[i].Source == 1) ? "OIS" : "BNView");
                                toolTip.Top = Items[i].Top;
                                toolTip.Bottom = Items[i].Bottom;
                                toolTip.Text = Items[i].Date.ToString("dd.MM.yyyy");
                                Items[i].Selected = true;
                            }
                        }
                    }
                }
            }
            return toolTip;
        }

        public override float GetLastPerfDepth()
        {
            DateTime dt = DateTime.MinValue;
            float depth = -1F;
            if ((Items != null) && (Items.Length > 0))
            {
                for (int i = 0; i < Items.Length; i++)
                {
                    if (dt < Items[i].Date)
                    {
                        depth = Items[i].TopAbs;
                    }
                }
            }
            return depth;
        }
        
        public override void LoadData(Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                if (!w.PerfLoaded)
                {
                    (proj.OilFields[w.OilFieldIndex]).LoadPerfFromCache(w.Index);
                }
                if ((w.PerfLoaded) && (w.perf.Count > 0))
                {
                    SkvPerfItem perfItem;
                    Items = new PlanePerfItem[w.perf.Count];
                    int level = -1;
                    DateTime predDate = DateTime.MinValue;
                    for (int i = 0; i < w.perf.Count; i++)
                    {
                        this.Items[i] = new PlanePerfItem();
                        perfItem = w.perf.Items[i];
                        if (predDate != perfItem.Date)
                        {
                            level++;
                            predDate = perfItem.Date;
                        }
                        this.Items[i].Top = perfItem.Top;
                        this.Items[i].Bottom = perfItem.Bottom;
                        this.Items[i].TopAbs = GetAbsByMD(perfItem.Top);
                        this.Items[i].BottomAbs = GetAbsByMD(perfItem.Bottom);
                        this.Items[i].PerfTypeId = perfItem.PerfTypeId;
                        this.Items[i].Source = perfItem.Source;
                        this.Items[i].Level = level;
                        this.Items[i].Date = perfItem.Date;

                    }
                }
            }            
        }
        public override void DrawFront(Graphics grfx)
        {
            if ((this.Visible) && (Items != null))
            {
                float X = this.HotSpot.X + Coord.X;
                float X2;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;
                Pen pen;
                PlanePerfItem item;
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];
                    pen = pen1;
                    X2 = X + item.Level * 6 + 2;

                    Y = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                    Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                    if (Y2 > HotSpot.Y + PlaneRect.Height) Y2 = HotSpot.Y + PlaneRect.Height;
                    if (Y > HotSpot.Y + PlaneRect.Height) continue;
                    if (Y2 < 0) continue;
                    if (X2 > this.HotSpot.X + PlaneRect.Right) continue;

                    if (item.PerfTypeId == 10000)
                    {
                        grfx.DrawRectangle(pen3, X2, Y, 4, Y2 - Y);
                        grfx.FillRectangle(textBrush, X2, Y, 4, Y2 - Y);
                    }
                    else
                    {
                        grfx.DrawLine(pen, X2, Y, X2 + 4, Y);
                        grfx.DrawLine(pen, X2 + 4, Y - 1, X2 + 4, Y2 + 1);
                        grfx.DrawLine(pen, X2 + 4, Y2, X2, Y2);
                    }
                }
            }            
        }
        public override void DrawSelected(Graphics grfx)
        {
            if (this.Visible && (Items != null))
            {
                float X = this.HotSpot.X + Coord.X + 2;
                float X2;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;
                Pen pen;
                PlanePerfItem item;
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];
                    if (!item.Selected) continue;
                    pen = pen1;
                    X2 = X + item.Level * 6 + 2;

                    Y = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                    Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                    if (Y2 > HotSpot.Y + PlaneRect.Height) Y2 = HotSpot.Y + PlaneRect.Height;
                    if (Y > HotSpot.Y + PlaneRect.Height) continue;
                    if (Y2 < 0) continue;
                    if (X2 > this.HotSpot.X + PlaneRect.Right) continue;

                    grfx.FillRectangle(Brushes.White, X2 - 2, Y - 2, 8, Y2 - Y + 4);
                    if (item.PerfTypeId == 10000)
                    {
                        grfx.DrawRectangle(pen3, X2, Y, 4, Y2 - Y);
                        grfx.FillRectangle(textBrush, X2, Y, 4, Y2 - Y);
                    }
                    else
                    {
                        grfx.DrawLine(pen, X2, Y, X2 + 4, Y);
                        grfx.DrawLine(pen, X2 + 4, Y - 1, X2 + 4, Y2 + 1);
                        grfx.DrawLine(pen, X2 + 4, Y2, X2, Y2);
                    }
                }
            }
        }
    }

    // ОБЪЕКТ - ИСТОРИЯ ПЕРФОРАЦИИ
    public sealed class PlanePerfHistory : PlaneBaseObject
    {
        internal class PlanePerfHistoryItem
        {
            public float Top;
            public float Bottom;
            public float TopAbs;
            public float BottomAbs;
        }

        PlanePerfHistoryItem[] Items;
        public PlanePerfHistory() : base()
        {
            TypeID = Constant.Plane.ObjectType.PERFORATION_HISTORY;
            Visible = false;
        }
        public PlanePerfHistory(PlanePerfHistory BasePerfHistory) : base()
        {
        }

        // GET DATA
        int GetIndexByInterval(List<PlanePerfHistoryItem> items, float Top, float Bottom)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (!(Bottom < items[i].Top || items[i].Bottom < Top))
                {
                    return i;
                }
            }
            return -1;
        }

        public override void LoadData(Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                if (!w.PerfLoaded)
                {
                    (proj.OilFields[w.OilFieldIndex]).LoadPerfFromCache(w.Index);
                }
                if ((w.PerfLoaded) && (w.GisLoaded) && (w.perf.Count > 0))
                {
                    int index;
                    SkvPerfItem perfItem;
                    List<PlanePerfHistoryItem> items = new List<PlanePerfHistoryItem>();
                    for (int i = 0; i < w.perf.Count; i++)
                    {
                        perfItem = w.perf.Items[i];
                        index = GetIndexByInterval(items, perfItem.Top, perfItem.Bottom);
                        if (index == -1)
                        {
                            index = items.Count;
                            items.Add(new PlanePerfHistoryItem());
                            items[index].Top = perfItem.Top;
                            items[index].Bottom = perfItem.Bottom;
                            items[index].TopAbs = GetAbsByMD(perfItem.Top);
                            items[index].BottomAbs = GetAbsByMD(perfItem.Bottom);
                        }
                        else
                        {
                            if(items[index].Top > perfItem.Top)
                            {
                                items[index].Top = perfItem.Top;
                                items[index].TopAbs = GetAbsByMD(perfItem.Top);
                            }
                            if(items[index].Bottom < perfItem.Bottom)
                            {
                                items[index].Bottom = perfItem.Bottom;
                                items[index].BottomAbs = GetAbsByMD(perfItem.Bottom);
                            }
                        }
                    }
                    Items = items.ToArray();
                }
            }
        }
        public override void DrawFront(Graphics grfx)
        {
            if ((this.Visible) && (Items != null))
            {
                float X = this.HotSpot.X + Coord.X + this.Width - 2;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;
                PlanePerfHistoryItem item;
                Pen pen = SmartPlusGraphics.Pens.Black2;
                int count;
                float dy = 0;
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];

                    Y = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                    Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                    if (Y2 > HotSpot.Y + PlaneRect.Height) Y2 = HotSpot.Y + PlaneRect.Height;
                    if (Y > HotSpot.Y + PlaneRect.Height) continue;
                    if (Y2 < 0) continue;
                    grfx.DrawLine(pen, X - 3, Y, X, Y);
                    grfx.DrawLine(pen, X, Y - 1, X, Y2 + 1);
                    grfx.DrawLine(pen, X - 3, Y2, X, Y2);

                    count = (int)(Math.Ceiling(Y2 - Y - pen.Width) / (pen.Width + 2));
                    if (count > 0)
                    {
                        dy = pen.Width + 2;
                        Y += ((Y2 - Y) - (count - 1) * (pen.Width + 2)) / 2;
                        for (int j = 0; j < count; j++)
                        {
                            grfx.DrawLine(pen, X - 3, Y, X, Y);
                            Y += dy;
                        }
                    }
                }
            }
        }
    }

    // ОБЪЕКТ - ОБЪЕКТ ГИС или РАЗРАБОТКИ
    public sealed class PlaneOilObj : PlaneBaseObject
    {
        internal class PlaneOilObjItem
        {
            public float Top;
            public float Bottom;
            public float TopAbs;
            public float BottomAbs;
            public int OilObjCode;
            public string OilObjName;
        }
        PlaneOilObjItem[] Items;
        PlaneOilObjItem[] ItemsProduct;
        Pen penBounds;
        Brush brBack, brText;
        Font font;
        bool OilObjMode = true;
        public PlaneOilObj() : base()
        {
            TypeID = Constant.Plane.ObjectType.OIL_OBJ;
            brBack = new SolidBrush(Color.FromArgb(51, 245, 245, 245));
            penBounds = Pens.Gray;
            brText = new SolidBrush(Color.FromArgb(50, 50, 50));
            font = new Font("Segoe UI", 8);
        }
        public PlaneOilObj(PlaneOilObj BaseOilObj) : base()
        {
            TypeID = Constant.Plane.ObjectType.OIL_OBJ;
            brBack = BaseOilObj.brBack;
            penBounds = BaseOilObj.penBounds;
            brText = BaseOilObj.brText;
            font = BaseOilObj.font;
        }

        public void SetOilObjMode(bool OilObjMode)
        {
            this.OilObjMode = OilObjMode;
        }
        public override void LoadData(Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                if (!w.GisLoaded) (proj.OilFields[w.OilFieldIndex]).LoadGisFromCache(w.Index);
                StratumTreeNode stratumNode;
                if ((w.GisLoaded) && (w.gis.Count > 0))
                {
                    var dict = (StratumDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                    ArrayList list = new ArrayList();

                    SkvGisItem gisItem;
                    PlaneOilObjItem item = null;
                    int i, predOilObj = -1;
                    float lastBott = -1, lastBottAbs = -1;
                    for (i = 0; i < w.gis.Count; i++)
                    {
                        gisItem = w.gis.Items[i];
                        if (predOilObj != gisItem.PlastId)
                        {
                            if ((item != null) && (lastBott != -1) && (lastBottAbs != -1))
                            {
                                item.Bottom = lastBott;
                                item.BottomAbs = lastBottAbs;
                            }
                            item = new PlaneOilObjItem();
                            list.Add(item);
                            item.OilObjName = dict.GetShortNameByCode(gisItem.PlastId);
                            if (item.OilObjName.Length == 0) item.OilObjName = "Н/Д";
                            item.Top = w.gis.Items[i].H;
                            item.TopAbs = w.gis.Items[i].Habs;

                            predOilObj = gisItem.PlastId;
                        }
                        if (item != null)
                        {
                            lastBott = w.gis.Items[i].H + w.gis.Items[i].L;
                            lastBottAbs = w.gis.Items[i].Habs + w.gis.Items[i].Labs;
                            item.Bottom = lastBott;
                            item.BottomAbs = lastBottAbs;
                        }
                    }
                    if (list.Count > 0)
                    {
                        Items = new PlaneOilObjItem[list.Count];
                        for (i = 0; i < list.Count; i++)
                        {
                            Items[i] = (PlaneOilObjItem)list[i];
                        }
                    }
                    if(w.MerLoaded)
                    {
                        list.Clear();
                        predOilObj = -1;
                        MerItem merItem;
                        List<int> objListMer = new List<int>();
                        for (i = 0; i < w.mer.Count; i++)
                        {
                            merItem = w.mer.Items[i];
                            if (predOilObj != merItem.PlastId)
                            {
                                if (objListMer.IndexOf(merItem.PlastId) == -1)
                                {
                                    objListMer.Add(merItem.PlastId);
                                }
                            }
                        }
                        w.mer = null;
                        if (objListMer.Count > 0)
                        {
                            int ind;
                            int[] parentInd = new int[Items.Length];
                            ArrayList objList2 = new ArrayList();
                            for (i = 0; i < Items.Length; i++)
                            {
                                parentInd[i] = -1;
                                stratumNode = dict.GetStratumTreeNode(Items[i].OilObjCode);
                                if(stratumNode != null)
                                {
                                    for (int j = 0; j < objListMer.Count; j++)
                                    {
                                        if (stratumNode.GetParentLevelByCode(objListMer[j]) > -1)
                                        {
                                            parentInd[i] = j;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (objList2.Count > 0)
                            {
                                ItemsProduct = new PlaneOilObjItem[objList2.Count];
                                for(i = 0; i < objList2.Count;i++)
                                {
                                    ItemsProduct[i] = new PlaneOilObjItem();
                                    ItemsProduct[i].Top = -1;
                                    ItemsProduct[i].TopAbs = -1;
                                    ItemsProduct[i].Bottom = -1;
                                    ItemsProduct[i].BottomAbs = -1;
                                    ItemsProduct[i].OilObjCode = (int)objList2[i];
                                    ItemsProduct[i].OilObjName = dict.GetShortNameByCode(ItemsProduct[i].OilObjCode);
                                }
                                for (i = 0; i < parentInd.Length; i++)
                                {
                                    if (parentInd[i] != -1)
                                    {
                                        ind = objList2.IndexOf((int)objListMer[parentInd[i]]);
                                        if(ind > -1)
                                        {
                                            if ((ItemsProduct[ind].Top == -1) || (ItemsProduct[ind].Top > Items[i].Top)) ItemsProduct[ind].Top = Items[i].Top;
                                            if ((ItemsProduct[ind].TopAbs == -1) || (ItemsProduct[ind].TopAbs > Items[i].TopAbs)) ItemsProduct[ind].TopAbs = Items[i].TopAbs;
                                            if ((ItemsProduct[ind].Bottom == -1) || (ItemsProduct[ind].Bottom < Items[i].Bottom)) ItemsProduct[ind].Bottom = Items[i].Bottom;
                                            if ((ItemsProduct[ind].BottomAbs == -1) || (ItemsProduct[ind].BottomAbs < Items[i].BottomAbs)) ItemsProduct[ind].BottomAbs = Items[i].BottomAbs;
                                        }
                                    }
                                }
                            }

 
                        }
                        objListMer.Clear();
                    }
                    list.Clear();
                    for (i = 0; i < w.gis.Count; i++)
                    {
                        if (w.gis.Items[i].Habs > 0)
                        {
                            locMinDepth = w.gis.Items[i].Habs;
                            break;
                        }
                    }
                    for (i = w.gis.Count - 1; i >= 0; i--)
                    {
                        if (w.gis.Items[i].Habs + w.gis.Items[i].Labs > 0)
                        {
                            locMaxDepth = w.gis.Items[i].Habs + w.gis.Items[i].Labs;
                            break;
                        }
                    }
                }
            }
        }
        public override void DrawBack(Graphics grfx)
        {
            if ((this.Visible) && (Items != null) && (OilObjMode))
            {
                float X = this.HotSpot.X + Coord.X;
                float X2 = X + this.Width;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;

                PlaneOilObjItem item;
                SizeF size;
                PlaneOilObjItem[] _items;
                _items = Items;

                for (int i = 0; i < _items.Length; i++)
                {
                    item = _items[i];
                    Y = Y0 + (item.TopAbs - MinDepth) * YUnitsInDepthUnit;
                    if (Y > ScreenMaxY) break;
                    if (Y > PlaneRect.Height) break;
                    Y2 = Y0 + (item.BottomAbs - MinDepth) * YUnitsInDepthUnit;
                    if (Y2 < ScreenMinY) continue;
                    grfx.FillRectangle(brBack, X + 1, Y, this.Width - 1, Y2 - Y);
                    grfx.DrawLine(penBounds, X, Y, X + this.Width, Y);
                    grfx.DrawLine(penBounds, X, Y2, X + this.Width, Y2);
                    size = grfx.MeasureString(item.OilObjName, font, PointF.Empty, StringFormat.GenericTypographic);
                    if (size.Height - 4 < Y2 - Y)
                    {
                        if (size.Width - 4 < this.Width - 3)
                        {
                            grfx.DrawString(item.OilObjName, font, brText, X + this.Width - size.Width - 5, (Y + Y2) / 2 - size.Height / 2);
                        }
                        else if (size.Width - 4 < this.Width)
                        {
                            grfx.DrawString(item.OilObjName, font, brText, X + this.Width - size.Width, (Y + Y2) / 2 - size.Height / 2);
                        }
                    }
                }

            }
        }
    }

    // ОБЪЕКТ - Пористость
    public sealed class PlaneGisPor : PlaneBaseObject
    {
        internal class PorItem
        {
            public float Top;
            public float Bottom;
            public float TopAbs;
            public float BottomAbs;
            public float Porosity;
        }
        PorItem[] Items;
        Brush brush;

        // METHODS
        public PlaneGisPor() : base()
        {
            TypeID = Constant.Plane.ObjectType.POROSITY;
            brush = new SolidBrush(Color.Green);
        }
        public PlaneGisPor(PlaneGisPor BaseGisPor) : base()
        {
            TypeID = Constant.Plane.ObjectType.POROSITY;
            brush = BaseGisPor.brush;
        }

        public override void LoadData(Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                if (!w.GisLoaded) (proj.OilFields[w.OilFieldIndex]).LoadGisFromCache(w.Index);
                if ((w.GisLoaded) && (w.gis.Count > 0))
                {
                    float MinValue = float.MaxValue, MaxValue = float.MinValue;
                    SkvGisItem gisItem;
                    Items = new PorItem[w.gis.Count];
                    for (int i = 0; i < w.gis.Count; i++)
                    {
                        this.Items[i] = new PorItem();
                        gisItem = w.gis.Items[i];
                        this.Items[i].Top = gisItem.H;
                        this.Items[i].Bottom = gisItem.H + gisItem.L;
                        this.Items[i].TopAbs = gisItem.Habs;
                        this.Items[i].BottomAbs = gisItem.Habs + gisItem.Labs;
                        this.Items[i].Porosity = gisItem.Kpor;
                        if (MinValue > gisItem.Kpor) MinValue = gisItem.Kpor;
                        if (MaxValue < gisItem.Kpor) MaxValue = gisItem.Kpor;
                    }
                    locMinDepth = w.gis.Items[0].Habs;
                    locMaxDepth = w.gis.Items[w.gis.Count - 1].Habs + w.gis.Items[w.gis.Count - 1].Labs;
                    if (MinValue != float.MaxValue)
                    {
                        if (MaxValue < 30) MaxValue = 30;
                        scale.SetMinMaxAuto(MinValue, MaxValue);
                    }
                    else
                    {
                        scale.SetMinMaxAuto(0, 30);
                    }
                }
            }
        }
        public override void DrawObject(Graphics grfx)
        {
            if ((this.Visible) && (Items != null))
            {
                float X = this.HotSpot.X + Coord.X;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;
                float dX;
                PorItem item;
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];
                    Y = Y0 + (item.Top - MinDepth) * YUnitsInDepthUnit;
                    if (Y > ScreenMaxY) break;
                    Y2 = Y0 + (item.Bottom - MinDepth) * YUnitsInDepthUnit;
                    if (Y2 < ScreenMinY) continue;
                    if (Y2 < 0) continue;
                    if (Y > PlaneRect.Height) break;
                    dX = scale.GetXShift(item.Porosity);
                    grfx.FillRectangle(brush, X, Y, dX, Y2 - Y);
                    grfx.DrawRectangle(Pens.Black, X, Y, dX, Y2 - Y);
                }
            }
        }
    }

    // ОБЪЕКТ - Насыщенность
    public sealed class PlaneGisOilSat : PlaneBaseObject
    {
        internal class OilSatItem
        {
            public float Top;
            public float Bottom;
            public float TopAbs;
            public float BottomAbs;
            public float OilSaturation;
        }
        OilSatItem[] Items;
        Brush brush;

        // METHODS
        public PlaneGisOilSat() : base()
        {
            TypeID = Constant.Plane.ObjectType.OIL_SAT;
            brush = new SolidBrush(Color.Sienna);
        }
        public PlaneGisOilSat(PlaneGisOilSat BaseGisOilSat) : base()
        {
            TypeID = Constant.Plane.ObjectType.OIL_SAT;
            brush = BaseGisOilSat.brush;
        }

        public override void LoadData(Project proj, Well w)
        {
            if ((proj != null) && (w != null))
            {
                if (!w.GisLoaded) (proj.OilFields[w.OilFieldIndex]).LoadGisFromCache(w.Index);
                if ((w.GisLoaded) && (w.gis.Count > 0))
                {
                    float MinValue = float.MaxValue, MaxValue = float.MinValue;
                    SkvGisItem gisItem;
                    Items = new OilSatItem[w.gis.Count];
                    for (int i = 0; i < w.gis.Count; i++)
                    {
                        this.Items[i] = new OilSatItem();
                        gisItem = w.gis.Items[i];
                        this.Items[i].Top = gisItem.H;
                        this.Items[i].Bottom = gisItem.H + gisItem.L;
                        this.Items[i].TopAbs = gisItem.Habs;
                        this.Items[i].BottomAbs = gisItem.Habs + gisItem.Labs;
                        this.Items[i].OilSaturation = gisItem.Kn;
                        if (MinValue > gisItem.Kpor) MinValue = gisItem.Kn;
                        if (MaxValue < gisItem.Kpor) MaxValue = gisItem.Kn;
                    }
                    locMinDepth = w.gis.Items[0].Habs;
                    locMaxDepth = w.gis.Items[w.gis.Count - 1].Habs + w.gis.Items[w.gis.Count - 1].Labs;
                    if (MinValue != float.MaxValue)
                    {
                        scale.SetMinMaxAuto(MinValue, 100);
                    }
                }
            }
        }
        public override void DrawObject(Graphics grfx)
        {
            if ((this.Visible) && (Items != null))
            {
                float X = this.HotSpot.X + Coord.X;
                float Y0 = this.HotSpot.Y + Coord.Y, Y, Y2;
                float dX;
                OilSatItem item;
                for (int i = 0; i < Items.Length; i++)
                {
                    item = this.Items[i];
                    Y = Y0 + (item.Top - MinDepth) * YUnitsInDepthUnit;
                    if (Y > ScreenMaxY) break;
                    Y2 = Y0 + (item.Bottom - MinDepth) * YUnitsInDepthUnit;
                    if (Y2 < ScreenMinY) continue;
                    if (Y2 < 0) continue;
                    if (Y > PlaneRect.Height) break;
                    dX = scale.GetXShift(item.OilSaturation);
                    grfx.FillRectangle(brush, X, Y, dX, Y2 - Y);
                    grfx.DrawRectangle(Pens.Black, X, Y, dX, Y2 - Y);
                }
            }
        }
    }
    #endregion
}

