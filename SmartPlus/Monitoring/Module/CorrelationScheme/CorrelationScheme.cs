﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using RDF.Objects;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    // КОР СХЕМА
    public sealed class CorrelationScheme : PictureBox
    {
        bool PlaneMode;
        bool BindWidth;
        bool EnableToolTip;
        MainForm mainForm;
        Project proj;
        ContextMenuStrip cMenuCorScheme;
        ToolStripMenuItem cmAddMarkerItem, cmAutoCreateMarker;
        ToolStripMenuItem cmRangeByMarker, cmRemoveMarkerDepth;
        ToolStripMenuItem cmResetRange;
        ToolStripMenuItem cmCreateMarkerZones;

        // Drawing
        float YUnits;
        Bitmap bmp;
        Cursor curMrkEditMode;

        Font FontToolTip, FontToolTipSmall, FontToolTipAxis;
        PlaneToolTip ToolTip = null;

        Graphics bmp_grfx;
        PointF hotSpot, hotSpotHeadImage, mouseDown, lastDrawHotSpot;
        public PointF HotSpot
        {
            get { return hotSpot; }
            set
            {
                hotSpot = value;
                hotSpotHeadImage.X = value.X;
                for (int i = 0; i < Count; i++)
                {
                    this.PlaneList[i].HotSpot = value;
                }
            }
        }
        public float YUnitsInDepthUnit
        {
            get { return YUnits; }
            set
            {
                YUnits = value;
                DataHeight = (float)((MaxDepth - MinDepth) * YUnits);
                if (PlaneList != null)
                {
                    for (int i = 0; i < PlaneList.Length; i++)
                    {
                        PlaneList[i].DataHeight = DataHeight;
                        PlaneList[i].YUnitsInDepthUnit = value;
                    }
                }
            }
        }
        bool ScroolEventsEnable = true;
        bool DrawWaitIconMode = false;
        float DataHeight;
        public float HeadHeight;
        public float Width;
        public float MinWidth
        {
            get
            {
                float min = 0;
                if (Count > 0)
                {
                    min = PlaneList[0].MinWidth * Count;
                }
                return min;
            }
        }

        Infragistics.Win.UltraWinDock.DockableControlPane dcp;

        #region SnapShoot Mode
        Point Snap1, Snap2;
        bool snapShootMode = false;
        public bool SnapShootMode
        {
            get { return snapShootMode; }
            set
            {
                Snap1 = Point.Empty;
                snapShootMode = value;
                if (value) this.Cursor = Cursors.Cross; else this.Cursor = Cursors.Default;
            }
        }
        Brush brSnap;
        #endregion

        // Data
        public bool DataLoading = false;
        public bool DataLoaded = false;
        bool MouseBtnDown;
        bool DrawCloseButton;
        Plane baseScheme;
        public Plane BaseScheme
        {
            get { return baseScheme; }
            set
            {
                this.baseScheme = value;
                for (int i = 0; i < Count; i++)
                {
                    PlaneList[i].SetBaseScheme(value);
                }
            }
        }
        public C2DObject ObjSrc;
        Plane[] PlaneList;
        List<PlaneMarkerZone> markerZones;
        List<PlaneMarkerZone> MarkerZones
        {
            get { return markerZones; }
            set
            {
                markerZones = value;
                if (PlaneList != null)
                {
                    for (int i = 0; i < PlaneList.Length; i++)
                    {
                        PlaneList[i].MarkerZones = value;
                    }
                }
            }
        }
        public int Count;
        float MinDepth, MaxDepth;
        public float ScreenMinY
        {
            set
            {
                for (int i = 0; i < Count; i++)
                {
                    PlaneList[i].ScreenMinY = value;
                }
            }
        }
        public float ScreenMaxY
        {
            set
            {
                for (int i = 0; i < Count; i++)
                {
                    PlaneList[i].ScreenMaxY = value;
                }
            }
        }
        HScrollBar hScrool;
        VScrollBar vScrool;
        Timer tmrDraw, tmrToolTip;
        public C2DLayer currMrkLayer = null;
        Marker RangeMarker = null;
        bool MarkerEditMode = false;
        bool MarkerDepthEditMode = false;
        bool IsHeadColumnDraw = true;
        bool ShowDuplicates = false;

        // WAIT ICON
        int wait_angle;
        Timer tmrWait;

        // METHODS
        public CorrelationScheme(MainForm MainForm, bool PlaneMode)
        {
            this.PlaneMode = PlaneMode;
            this.BindWidth = false;
            
            if (PlaneMode) this.BindWidth = true;
            // Form Settings
            mainForm = MainForm;
            if (!this.PlaneMode)
            {
                mainForm.tsCorSchContainer.ContentPanel.BorderStyle = BorderStyle.FixedSingle;
                this.Parent = mainForm.tsCorSchContainer.ContentPanel;
                dcp = mainForm.corSchPane;
            }
            else
            {
                this.Parent = mainForm.tsPlaneWellContainer.ContentPanel;
                dcp = mainForm.planeWellPane;
                mainForm.tsPlaneWellContainer.ContentPanel.BorderStyle = BorderStyle.FixedSingle;
            }
            this.Dock = DockStyle.None;

            //TOOLTIP
            EnableToolTip = true;
            this.FontToolTip = new Font("Tahoma", 8.25f, FontStyle.Bold);
            this.FontToolTipSmall = new Font("Tahoma", 7.0f);
            this.FontToolTipAxis = new Font("Calibri", 8.25f);
            ToolTip = null;

            // SCROOLBARS
            hScrool = new HScrollBar();
            vScrool = new VScrollBar();
            if (!this.PlaneMode)
            {
                mainForm.tsCorSchContainer.ContentPanel.Controls.Add(vScrool);
                mainForm.tsCorSchContainer.ContentPanel.Controls.Add(hScrool);
            }
            else
            {
                mainForm.tsPlaneWellContainer.ContentPanel.Controls.Add(vScrool);
                mainForm.tsPlaneWellContainer.ContentPanel.Controls.Add(hScrool);
            }
            vScrool.Visible = true;
            hScrool.Visible = true;
            hScrool.Enabled = false;
            vScrool.Enabled = false;
            ScroolEventsEnable = true;
            DataLoaded = false;

            Point pt = new Point();
            DrawCloseButton = false;
            this.vScrool.Height = mainForm.pCore.ClientRectangle.Height - hScrool.Height;
            pt.X = mainForm.pCore.ClientRectangle.Width - vScrool.Width;
            pt.Y = 0;
            this.vScrool.Location = pt;

            this.hScrool.Width = mainForm.pCore.ClientRectangle.Width - vScrool.Width;
            pt.X = 0;
            pt.Y = mainForm.pCore.ClientRectangle.Height - hScrool.Height;
            this.hScrool.Location = pt;
            this.Location = Point.Empty;
            if (!this.PlaneMode)
            {
                base.Width = mainForm.tsCorSchContainer.ContentPanel.ClientRectangle.Width - vScrool.Width;
                base.Height = mainForm.tsCorSchContainer.ContentPanel.ClientRectangle.Height - hScrool.Height;
            }
            else
            {
                base.Width = mainForm.tsPlaneWellContainer.ContentPanel.ClientRectangle.Width - vScrool.Width;
                base.Height = mainForm.tsPlaneWellContainer.ContentPanel.ClientRectangle.Height - hScrool.Height;
            }
            vScrool.Scroll += new ScrollEventHandler(vScrool_Scroll);
            vScrool.MouseLeave += new EventHandler(vScrool_MouseLeave);
            hScrool.Scroll += new ScrollEventHandler(hScrool_Scroll);
            hScrool.MouseLeave += new EventHandler(hScrool_MouseLeave);

            MarkerZones = new List<PlaneMarkerZone>();

            // CURSORS
            curMrkEditMode = NativeMethods.CreateCursor(Properties.Resources.cs_CursorMarkerEditMode32, 0, 0);

            // WAIT ICON
            DrawWaitIconMode = false;
            tmrWait = new Timer();
            tmrWait.Enabled = false;
            tmrWait.Interval = 100;
            tmrWait.Tick += new EventHandler(tmrWait_Tick);

            // TIMER DRAWING
            tmrDraw = new Timer();
            tmrDraw.Enabled = false;
            tmrDraw.Interval = 30;
            tmrDraw.Tick += new EventHandler(timerDrawing);

            // TIMER TOOLTIP
            tmrToolTip = new Timer();
            tmrToolTip.Enabled = false;
            tmrToolTip.Interval = 30;
            tmrToolTip.Tick += new EventHandler(tmrToolTip_Tick);

            // BITMAP
            if (mainForm.MaxMonitorHeight > 0)
            {
                bmp = new Bitmap(mainForm.MaxMonitorWidth, mainForm.MaxMonitorHeight);
                bmp_grfx = Graphics.FromImage(bmp);
            }
            brSnap = new SolidBrush(Color.FromArgb(75, Color.Gray));

            // TRANSFER
            mouseDown = PointF.Empty;
            MouseBtnDown = false;

            // CONTEXT MENU
            CreateContextMenu();

            // DATA SETTINGS
            Count = 0;
            this.Parent.Resize += new EventHandler(ParentPanelResize);
            this.MouseDown += new MouseEventHandler(CorrelationScheme_MouseDown);
            this.MouseMove += new MouseEventHandler(CorrelationScheme_MouseMove);
            this.MouseUp += new MouseEventHandler(CorrelationScheme_MouseUp);
            this.MouseWheel += new MouseEventHandler(CorrelationScheme_MouseWheel);
            this.MouseLeave += new EventHandler(CorrelationScheme_MouseLeave);
            this.MouseEnter += new EventHandler(CorrelationScheme_MouseEnter);
            this.MouseDoubleClick += new MouseEventHandler(CorrelationScheme_MouseDoubleClick);
            this.Paint += new PaintEventHandler(CorrelationScheme_Paint);
        }

        // BASE SCHEME
        public void UpdatePlanesByBaseScheme()
        {
            if (PlaneList != null)
            {
                for (int i = 0; i < PlaneList.Length; i++)
                {
                    PlaneList[i].UpdateSettingsByBaseScheme();
                }
                SetMinMaxDepth();
                UpdatePlanesCoord();
                SetScrollBar();
                SetWindowSize(0, 0);
                if (this.BindWidth) SetBindWidth(this.BindWidth);
            }
            mainForm.UpdateCorSchemeToolBars();
            DrawData();
        }

        // CONTEXT MENU
        void CreateContextMenu()
        {
            cMenuCorScheme = new ContextMenuStrip();
            
            cmAddMarkerItem = (ToolStripMenuItem)cMenuCorScheme.Items.Add("");
            cmAddMarkerItem.Visible = false;
            cmAddMarkerItem.Text = "Создать новый маркер";
            cmAddMarkerItem.Image = Properties.Resources.cs_AddMarker;
            cmAddMarkerItem.Click += new EventHandler(cmAddMarkerItem_Click);

            cmAutoCreateMarker = (ToolStripMenuItem)cMenuCorScheme.Items.Add("");
            cmAutoCreateMarker.Visible = true;
            cmAutoCreateMarker.Text = "Создать маркеры по ГИС";
            cmAutoCreateMarker.Image = Properties.Resources.cs_AddMarkerByGIS;
            cmAutoCreateMarker.Click += new EventHandler(cmAutoCreateMarker_Click);

            cmRangeByMarker = (ToolStripMenuItem)cMenuCorScheme.Items.Add("");
            cmRangeByMarker.Text = "Выровнять на маркер";
            cmRangeByMarker.Image = Properties.Resources.cs_RangeByMarker;
            cmRangeByMarker.Click += new EventHandler(cmRangeByMarker_Click);

            cmResetRange = (ToolStripMenuItem)cMenuCorScheme.Items.Add("");
            cmResetRange.Text = "Сбросить выравнивание";
            cmResetRange.Visible = false;
            cmResetRange.Click += new EventHandler(cmResetRange_Click);

            cmRemoveMarkerDepth = (ToolStripMenuItem)cMenuCorScheme.Items.Add("");
            cmRemoveMarkerDepth.Text = "Удалить отметку маркера";
            cmRemoveMarkerDepth.Image = Properties.Resources.cs_RemoveMarkerDepth;
            cmRemoveMarkerDepth.Click += new EventHandler(cmRemoveMarkerDepth_Click);

            cmCreateMarkerZones = (ToolStripMenuItem)cMenuCorScheme.Items.Add("");
            cmCreateMarkerZones.Image = Properties.Resources.cs_MarkersZones;
            cmCreateMarkerZones.Text = "Зоны маркеров...";
            cmCreateMarkerZones.Click += new EventHandler(cmCreateMarkerZones_Click);
        }
        void cmResetRange_Click(object sender, EventArgs e)
        {
            RangeMarker = null;
            SetMinMaxDepth();
            SetObjectsCoord();
            SetWindowSize(0, 0);
            SetPlaneRects();
            DrawData();
            UpdateWindowTitle();
        }
        void ShowContextMenu()
        {
            if (DataLoaded)
            {
                cmCreateMarkerZones.Visible = !PlaneMode;
                cmResetRange.Visible = (RangeMarker != null);
                Point pt = Cursor.Position;
                if (pt.X + cMenuCorScheme.Width > mainForm.Location.X + mainForm.Width)
                {
                    pt.X = mainForm.Location.X + mainForm.Width - cMenuCorScheme.Width;
                }
                cMenuCorScheme.Show(pt.X, pt.Y);
            }
        }

        // DEPTH
        public void SetMinMaxDepth()
        {
            int i, j, k;
            MinDepth = -1;
            MaxDepth = -1;
            int MaxTopSpace = -1;
            int MaxBottomSpace = -1;

            if (Count > 0)
            {
                if (RangeMarker == null)
                {
                    PlaneBaseObject obj;
                    for (i = 0; i < this.Count; i++)
                    {
                        bool isGisEmpty = false;
                        for (j = 0; j < PlaneList[i].Count; j++)
                        {
                            for (k = 0; k < PlaneList[i].Columns[j].Count; k++)
                            {
                                obj = (PlaneBaseObject)PlaneList[i].Columns[j][k];
                                if ((obj.TypeID == Constant.Plane.ObjectType.GIS) && (obj.locMinDepth == -1) && (obj.locMaxDepth == -1))
                                {
                                    isGisEmpty = true;
                                    break;
                                }
                            }
                            if (isGisEmpty) break;
                        }
                        for (j = 0; j < PlaneList[i].Count; j++)
                        {
                            for (k = 0; k < PlaneList[i].Columns[j].Count; k++)
                            {
                                obj = (PlaneBaseObject)PlaneList[i].Columns[j][k];

                                if ((obj.TypeID != Constant.Plane.ObjectType.LOG || isGisEmpty) && (obj.locMinDepth != -1) && (obj.locMaxDepth != -1))
                                {
                                    if ((MinDepth == -1) || (obj.locMinDepth < MinDepth)) MinDepth = obj.locMinDepth;
                                    if ((MaxDepth == -1) || (obj.locMaxDepth > MaxDepth)) MaxDepth = obj.locMaxDepth;
                                }
                            }
                        }
                        if ((MaxTopSpace == -1) || (MaxTopSpace < PlaneList[i].TopSpace)) MaxTopSpace = PlaneList[i].TopSpace;
                        if ((MaxBottomSpace == -1) || (MaxBottomSpace < PlaneList[i].BottomSpace)) MaxBottomSpace = PlaneList[i].BottomSpace;
                    }
                    if (MaxTopSpace < 0) MaxTopSpace = 50;
                    if (MaxBottomSpace < 0) MaxBottomSpace = 50;
                    if (MinDepth == -1) MinDepth = 0; else MinDepth = MinDepth - MaxTopSpace;
                    if (MaxDepth == -1) MaxDepth = 0; else MaxDepth = MaxDepth + MaxBottomSpace;

                    for (i = 0; i < PlaneList.Length; i++)
                    {
                        PlaneList[i].MinDepth = MinDepth;
                        PlaneList[i].MaxDepth = MaxDepth;
                        for (j = 0; j < PlaneList[i].Columns.Length; j++)
                        {
                            for (k = 0; k < PlaneList[i].Columns[j].Count; k++)
                            {
                                obj = (PlaneBaseObject)PlaneList[i].Columns[j][k];
                                obj.MaxDepth = MaxDepth;
                                obj.MinDepth = MinDepth;
                                obj.SetMinMaxScale();
                            }
                            PlaneList[i].Columns[j].TestScaleMinMax();
                        }
                    }
                }
                else
                {
                    float mrkDepthBefore = -1, mrkDepthAfter = -1;
                    float mrkDepth;
                    for (i = 0; i < this.Count; i++)
                    {
                        mrkDepth = RangeMarker.GetDepthByWell(PlaneList[i].w);
                        if (mrkDepth > -1)
                        {
                            MinDepth = -1; MaxDepth = -1;
                            for (j = 0; j < PlaneList[i].Count; j++)
                            {
                                for (k = 0; k < PlaneList[i].Columns[j].Count; k++)
                                {
                                    PlaneBaseObject obj = (PlaneBaseObject)PlaneList[i].Columns[j][k];

                                    if ((obj.TypeID != Constant.Plane.ObjectType.LOG) && (obj.locMinDepth != -1) && (obj.locMaxDepth != -1))
                                    {
                                        if ((MinDepth == -1) || (obj.locMinDepth < MinDepth)) MinDepth = obj.locMinDepth;
                                        if ((MaxDepth == -1) || (obj.locMaxDepth > MaxDepth)) MaxDepth = obj.locMaxDepth;
                                    }
                                }
                            }
                            if ((MinDepth != -1) && (MaxDepth != -1))
                            {
                                if ((mrkDepthBefore == -1) || (mrkDepthBefore < (mrkDepth - MinDepth))) mrkDepthBefore = mrkDepth - MinDepth;
                                if ((mrkDepthAfter == -1) || (mrkDepthAfter < (MaxDepth - mrkDepth))) mrkDepthAfter = MaxDepth - mrkDepth;
                                PlaneList[i].MinDepth = MinDepth;
                                PlaneList[i].MaxDepth = MaxDepth;
                            }
                        }
                        if ((MaxTopSpace == -1) || (MaxTopSpace < PlaneList[i].TopSpace)) MaxTopSpace = PlaneList[i].TopSpace;
                        if ((MaxBottomSpace == -1) || (MaxBottomSpace < PlaneList[i].BottomSpace)) MaxBottomSpace = PlaneList[i].BottomSpace;
                    }
                    if (MaxTopSpace < 0) MaxTopSpace = 50;
                    if (MaxBottomSpace < 0) MaxBottomSpace = 50;
                    if (mrkDepthBefore == -1) mrkDepthBefore = 0;
                    if (mrkDepthAfter == -1) mrkDepthAfter = 0;
                    MinDepth = -1; MaxDepth = -1;
                    for (i = 0; i < this.Count; i++)
                    {
                        for (j = 0; j < PlaneList[i].Count; j++)
                        {
                            mrkDepth = RangeMarker.GetDepthByWell(PlaneList[i].w);
                            if (mrkDepth > -1)
                            {
                                if ((PlaneList[i].MinDepth != 0) && (PlaneList[i].MaxDepth != 0))
                                {
                                    PlaneList[i].MinDepth = PlaneList[i].MinDepth - (mrkDepthBefore - (mrkDepth - PlaneList[i].MinDepth) + MaxTopSpace);
                                    PlaneList[i].MaxDepth = PlaneList[i].MaxDepth + (mrkDepthAfter - (PlaneList[i].MaxDepth - mrkDepth) + MaxBottomSpace);
                                    for (k = 0; k < PlaneList[i].Columns[j].Count; k++)
                                    {
                                        PlaneBaseObject obj = (PlaneBaseObject)PlaneList[i].Columns[j][k];
                                        obj.MaxDepth = PlaneList[i].MaxDepth;
                                        obj.MinDepth = PlaneList[i].MinDepth;
                                        obj.SetMinMaxScale();
                                    }
                                    PlaneList[i].Columns[j].TestScaleMinMax();
                                    if ((MinDepth == -1) || (PlaneList[i].MinDepth < MinDepth)) MinDepth = PlaneList[i].MinDepth;
                                    if ((MaxDepth == -1) || (PlaneList[i].MaxDepth > MaxDepth)) MaxDepth = PlaneList[i].MaxDepth;
                                }
                            }
                        }
                    }
                }
                this.YUnitsInDepthUnit = YUnitsInDepthUnit;
                SetPlaneRects();
            }
        }
        public float GetLastPerfDepth()
        {
            int count = 0;
            float depth = -1, d;
            for (int i = 0; i < Count; i++)
            {
                d = PlaneList[i].GetLastPerfDepth();
                if (d != -1)
                {
                    depth += d;
                    count++;
                }
            }
            if (count > 0) depth = depth / (float)count;
            return depth;
        }
        public float GetMedianDepth()
        {
            int count = 0;
            float depth = -1, d;
            for (int i = 0; i < Count; i++)
            {
                d = PlaneList[i].GetMedianDepth();
                if (d != -1)
                {
                    depth += d;
                    count++;
                }
            }
            if (count > 0) depth = depth / (float)count;
            return depth;
        }
        public float GetMedianDepthByMarker(Marker Marker)
        {
            int count = 0;
            float depth = -1, d;
            for (int i = 0; i < Count; i++)
            {
                d = PlaneList[i].GetMarkerDepth(Marker);
                if (d != -1)
                {
                    depth += d;
                    count++;
                }
            }
            if (count > 0) depth = depth / (float)count;
            return depth;
        }
        public void SetMedianDepth(float Depth)
        {
            if ((Depth > -1) && (Depth * YUnitsInDepthUnit + HeadHeight > ClientRectangle.Height / 2))
            {
                PointF hotPt = HotSpot;
                HotSpot = PointF.Empty;
                float dY = (ClientRectangle.Height + HeadHeight) / 2 - ScreenYFromDepth(Depth);
                hotPt.Y = dY;

                if ((DataHeight + HeadHeight > ClientRectangle.Height) && (hotPt.Y < ClientRectangle.Height - DataHeight - HeadHeight))
                {
                    hotPt.Y = ClientRectangle.Height - DataHeight - 1 - HeadHeight;
                }
                else if (hotPt.Y + DataHeight + HeadHeight < ClientRectangle.Height)
                {
                    hotPt.Y = ClientRectangle.Height - DataHeight - HeadHeight;
                }
                else if (DataHeight + HeadHeight < ClientRectangle.Height)
                {
                    hotPt.Y = 0;
                }
                if (hotPt.Y > 0) hotPt.Y = 0;
                HotSpot = hotPt;
                SetScrollBar();
            }
        }
        public void SetMedianDepthByMarker(Marker Marker)
        {
            SetMedianDepth(GetMedianDepthByMarker(Marker));
        }

        // SNAPSHOOT MODE
        public void SetSnapShootMode(int SnapAllMapMode)
        {
            switch (SnapAllMapMode)
            {
                case 0:
                    SnapShootMode = true;
                    break;
                case 1:
                    SnapAllMapToClipboard();
                    break;
                case 2:
                    SnapAllCorSchemeToClipboard();
                    break;
            }
        }
        void SnapToClipboard()
        {
            if ((snapShootMode) && (!Snap1.IsEmpty))
            {
                int minX, minY, maxX, maxY;
                if (Snap2.X > Snap1.X)
                {
                    minX = Snap1.X;
                    maxX = (int)Snap2.X;
                }
                else
                {
                    minX = (int)Snap2.X;
                    maxX = Snap1.X;
                }
                if (Snap2.Y > Snap1.Y)
                {
                    minY = Snap1.Y;
                    maxY = (int)Snap2.Y;
                }
                else
                {
                    minY = (int)Snap2.Y;
                    maxY = Snap1.Y;
                }

                if (minX < this.ClientRectangle.Left) minX = this.ClientRectangle.Left;
                if (maxX > this.ClientRectangle.Right) maxX = this.ClientRectangle.Right;
                if (minY < this.ClientRectangle.Top) minY = this.ClientRectangle.Top;
                if (maxY > this.ClientRectangle.Bottom) maxY = this.ClientRectangle.Bottom;
                Rectangle rect = new Rectangle(minX, minY, (maxX - minX), (maxY - minY));
                if ((rect.Width < 20) || (rect.Height < 20))
                {
                    rect = this.ClientRectangle;
                    rect.X++;
                    rect.Y++;
                    rect.Width -= 2;
                    rect.Height -= 2;
                }
                if (SnapRectToClipboard(rect, false))
                {
                    string str;
                    if (PlaneMode)
                    {
                        str = "Участок планшета скопирован в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".";
                    }
                    else
                    {
                        str = "Участок корсхемы скопирован в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".";
                    }
                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", str);
                }
            }
            SnapShootMode = false;
            this.Invalidate();
        }
        void SnapAllMapToClipboard()
        {
            Rectangle rect = this.ClientRectangle;
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 2;
            if (SnapRectToClipboard(rect, false))
            {
                string str;
                if (PlaneMode)
                {
                    str = "Окно планшета скопировано в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".";
                }
                else
                {
                    str = "Окно корсхемы скопирован в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".";
                }
                ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", str);
            }
            this.Invalidate();
        }
        void SnapAllCorSchemeToClipboard()
        {
            if ((this.DataHeight == 0) || (this.Width == 0)) return;
            PointF savedHotSpot = this.HotSpot;
            this.HotSpot = PointF.Empty;
            //ScreenMinY = HotSpot.Y;
            ScreenMaxY = this.DataHeight + this.HeadHeight;
            Rectangle rect = new Rectangle((int)HotSpot.X, (int)HotSpot.Y, (int)this.Width, (int)(this.DataHeight + this.HeadHeight));
            if (SnapRectToClipboard(rect, true))
            {
                string str;
                if (PlaneMode)
                {
                    str = "Планшет скопирован в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".";
                }
                else
                {
                    str = "Корсхема скопирована в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".";
                }
                ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", str);
            }
            this.HotSpot = savedHotSpot;
            SetWindowSize(0, 0);
            this.Invalidate();
        }
        bool SnapRectToClipboard(Rectangle rect, bool DrawAllPlanes)
        {
            bool result = false;
            ClipboardImageHelper help = new ClipboardImageHelper(mainForm, mainForm.AppSettings.CopyClipboardFormat, new Rectangle(0, 0, rect.Width, rect.Height));
            Graphics grfx = help.GetGraphics();
            if (grfx != null)
            {
                grfx.TranslateTransform(this.ClientRectangle.X - rect.X, this.ClientRectangle.Y - rect.Y);
                DrawData(grfx, DrawAllPlanes);
                grfx.Dispose();
                result = help.PutImageOnClipboard();
            }
            help.Dispose();
            return result;
        }

        // COMMON
        public int[] GetMnemonicIndexList(Project proj, string Mask)
        {
            int[] res = null;
            if (Mask != "")
            {
                int i, j, k, pos = 0, pos2;
                char[] sep = { '|' };
                char[] del = { '*' };
                string mask;
                string str;
                bool find = false;
                var dict = (WellLogMnemonicDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LOG_MNEMONIC);

                ArrayList outList = new ArrayList();
                ArrayList list = new ArrayList();
                string[] parseMask = Mask.Split(sep);
                string[] parseStr = null;
                for (k = 0; k < parseMask.Length; k++)
                {
                    mask = parseMask[k];
                    parseStr = parseMask[k].Split(del, StringSplitOptions.RemoveEmptyEntries);

                    if (parseStr.Length > 0)
                    {
                        for (i = 1; i < dict.Count; i++)
                        {
                            str = dict[i].ShortName;
                            pos = 0; pos2 = 0;
                            find = true;

                            for (j = 0; j < parseStr.Length; j++)
                            {
                                pos2 = str.IndexOf(parseStr[j], pos);
                                if (pos2 != -1)
                                {
                                    pos = pos2 + (parseStr[j]).Length;
                                }
                                else
                                {
                                    find = false;
                                    break;
                                }
                            }
                            if (find) outList.Add(i);
                        }
                    }
                }
                if (outList.Count > 0)
                {
                    res = new int[outList.Count];
                    for (i = 0; i < outList.Count; i++)
                    {
                        res[i] = (int)outList[i];
                    }
                }
            }
            return res;
        }
        void timerDrawing(object sender, EventArgs e)
        {
            this.DrawScroll();
            lastDrawHotSpot = HotSpot;
        }

        // SCROLLBAR METHODS
        void vScrool_MouseLeave(object sender, EventArgs e)
        {
            tmrDraw.Stop();
        }
        void hScrool_MouseLeave(object sender, EventArgs e)
        {
            tmrDraw.Stop();
        }
        void hScrool_Scroll(object sender, ScrollEventArgs e)
        {
            if (ScroolEventsEnable)
            {
                PointF pt = HotSpot;
                pt.X = -e.NewValue;
                HotSpot = pt;
                if (!tmrDraw.Enabled) tmrDraw.Start();
            }
        }
        void vScrool_Scroll(object sender, ScrollEventArgs e)
        {
            if (ScroolEventsEnable)
            {
                PointF pt = HotSpot;
                pt.Y = -e.NewValue;
                HotSpot = pt;
                //DrawData();
                if (!tmrDraw.Enabled) tmrDraw.Start();
            }
        }
        public void SetScrollBar()
        {
            vScrool.Enabled = false;
            hScrool.Enabled = false;
            if (Count > 0)
            {
                if (this.ClientRectangle.Width < Width)
                {
                    hScrool.Enabled = true;
                    hScrool.Maximum = (int)Width;
                    hScrool.LargeChange = ClientRectangle.Width;
                    hScrool.SmallChange = (int)(ClientRectangle.Width / 10);
                    if (hScrool.SmallChange < 0) hScrool.SmallChange = 1;
                }
                if (this.ClientRectangle.Height < DataHeight + HeadHeight)
                {
                    vScrool.Enabled = true;
                    vScrool.Maximum = (int)(DataHeight + HeadHeight);
                    vScrool.LargeChange = ClientRectangle.Height;
                    vScrool.SmallChange = (int)(ClientRectangle.Height / 10);
                    if (vScrool.SmallChange < 0) vScrool.SmallChange = 1;
                }
                if ((hScrool.Enabled) && (HotSpot.X <= 0) && (-(int)HotSpot.X < hScrool.Maximum)) hScrool.Value = -(int)HotSpot.X;
                if ((vScrool.Enabled) && (HotSpot.Y <= 0) && (-(int)HotSpot.Y < vScrool.Maximum)) vScrool.Value = -(int)HotSpot.Y;
            }
        }

        // EVENT METHODS
        float GetWidthByIndex(int index)
        {
            float res = 0;
            for (int i = 0; i < Count; i++)
            {
                if (i == index) break;
                res += PlaneList[i].Width;
            }
            return res;
        }
        void ParentPanelResize(object sender, EventArgs e)
        {
            Panel panel = (Panel)sender;
            Point pt = new Point();
            this.vScrool.Height = panel.ClientRectangle.Height - hScrool.Height;
            pt.X = panel.ClientRectangle.Width - vScrool.Width;
            pt.Y = 0;
            this.vScrool.Location = pt;

            this.hScrool.Width = panel.ClientRectangle.Width - vScrool.Width;
            pt.X = 0;
            pt.Y = panel.ClientRectangle.Height - hScrool.Height;
            this.hScrool.Location = pt;
            this.Location = Point.Empty;
            base.Width = panel.ClientRectangle.Width - vScrool.Width;
            base.Height = panel.ClientRectangle.Height - hScrool.Height;

            if (this.BindWidth) this.SetBindWidth(this.BindWidth);
            SetWindowSize(0, 0);
            SetScrollBar();
            DrawData();
        }
        void CorrelationScheme_MouseDown(object sender, MouseEventArgs e)
        {
            cMenuCorScheme.Hide();
            if (PlaneMode)
            {
                // mainForm.StatUsage.AddMessage(CollectorStatId.PLANE_MOUSE_DOWN);
            }
            else
            {
                // mainForm.StatUsage.AddMessage(CollectorStatId.CORSCHEME_MOUSE_DOWN);
            }
            MouseBtnDown = true;
            mouseDown.X = e.X;
            mouseDown.Y = e.Y;
            lastDrawHotSpot = HotSpot;
            if (DataLoaded)
            {
                this.Focus();
                int index = GetMouseOverPlane(e.X, e.Y);
                cmRemoveMarkerDepth.Visible = false;
                cmRangeByMarker.Visible = false;
                if (snapShootMode)
                {
                    Snap1 = e.Location;
                    Snap2 = Snap1;
                }
                else if (index > -1)
                {
                    if ((e.Y < PlaneList[index].WellHeadYOffset) && (!DrawCloseButton))
                    {
                        mainForm.canv.SelectWell(PlaneList[index].w);
                    }
                    else if ((MarkerEditMode) && (Control.ModifierKeys == Keys.Shift))
                    {
                        //AddMarker(index, e.Y);
                        MarkerDepthEditMode = true;
                        MouseBtnDown = false;
                    }
                    if ((this.MarkerEditMode) && (!this.MarkerDepthEditMode) && (PlaneList[index].Markers != null))
                    {
                        float scrY, D, minD = float.MaxValue;
                        int k = -1;
                        for (int i = 0; i < PlaneList[index].Markers.Length; i++)
                        {
                            if (PlaneList[index].Markers[i].srcMarker.Visible)
                            {
                                scrY = PlaneList[index].ScreenYFromDepth(PlaneList[index].Markers[i].Depth);
                                D = Math.Abs(scrY - e.Y);
                                if ((D < 20) && (minD > D))
                                {
                                    k = i;
                                    minD = D;
                                }
                            }
                        }
                        if (k > -1)
                        {
                            C2DLayer layer = mainForm.canv.GetWorkLayerByObjType(PlaneList[index].Markers[k].srcMarker);
                            if (layer != null) this.SetCurrentMarker(layer);
                            cmRemoveMarkerDepth.Visible = true;
                            cmRangeByMarker.Visible = true;
                        }
                    }
                }
                if (!snapShootMode && e.Button == MouseButtons.Right)
                {
                    ShowContextMenu();
                }
            }
        }
        void CorrelationScheme_MouseMove(object sender, MouseEventArgs e)
        {
            int i;
            mainForm.SetStatus("");
            ClearSelectedData();
            ToolTip = null;
            if (snapShootMode)
            {
                if (this.Cursor == Cursors.Default) this.Cursor = Cursors.Cross;
            }
            if (DataLoaded)
            {
                int ind = this.GetMouseOverPlane(e.X, e.Y);
                if (MouseBtnDown)
                {
                    if (snapShootMode)
                    {
                        Snap2 = e.Location;
                        this.Invalidate();
                    }
                    //else if (MarkerDepthEditMode)
                    //{
                    //    mouseDown.X = e.X;
                    //    mouseDown.Y = e.Y;
                    //    if ((ind != -1) && (currMrkLayer != null))
                    //    {
                    //        Marker mrk = (Marker)currMrkLayer._ObjectsList[0];
                    //        float depth = DepthFromScreenY(e.Y);
                    //        mrk.SetDepthByWell(PlaneList[ind].w, depth);
                    //        PlaneList[ind].SetMarkerDepth(mrk, depth);
                    //    }
                    //}
                    else
                    {
                        float left = hotSpot.X, right = hotSpot.X + this.Width;
                        float dX = e.X - mouseDown.X;
                        if (BindWidth) dX = 0;
                        float headH = (this.IsHeadColumnDraw) ? HeadHeight : 0;
                        PointF hotPt = HotSpot;
                        if (Width < this.ClientRectangle.Width - 1)
                        {
                            if ((((left >= 0) && (left + dX >= 0) && (right <= ClientRectangle.Right - 1) && (right + dX <= ClientRectangle.Right - 1))) ||
                                 ((left < 0) && (dX > 0) && (right <= ClientRectangle.Right - 1) && (right + dX < ClientRectangle.Right - 1)) ||
                                 ((left >= 0) && (left + dX >= 0) && (dX < 0) && (right > ClientRectangle.Right - 1))
                               )
                            {
                                hotPt.X += dX;
                            }
                            else if ((left >= 0) && (left + dX < 0) && (right <= ClientRectangle.Right - 1) && (right + dX <= ClientRectangle.Right - 1))
                            {
                                hotPt.X = 0;
                            }
                            else if (((left >= 0) && (left + dX >= 0) && (right <= ClientRectangle.Right - 1) && (right + dX > ClientRectangle.Right - 1)) ||
                                     ((left >= 0) && (left + dX >= 0) && (right > ClientRectangle.Right - 1) && (right + dX <= ClientRectangle.Right - 1)))
                            {
                                hotPt.X = ClientRectangle.Right - 1 - this.Width;
                            }
                        }
                        else
                        {
                            if (((left < 0) && (dX > 0) && (left + dX < 0)) ||
                                ((right > ClientRectangle.Right - 1) && (dX < 0) && (right + dX > ClientRectangle.Right - 1)))
                            {
                                hotPt.X += dX;
                            }
                            else if ((left < 0) && (dX > 0) && (left + dX >= 0))
                            {
                                hotPt.X = 0;
                            }
                            else if ((right > ClientRectangle.Right - 1) && (dX < 0) && (right + dX <= ClientRectangle.Right - 1))
                            {
                                hotPt.X = ClientRectangle.Right - 1 - this.Width;
                            }
                        }

                        hotPt.Y += e.Y - mouseDown.Y;

                        if (DataHeight + headH < ClientRectangle.Height)
                        {
                            hotPt.Y = 0;
                        }
                        else
                        {
                            if (hotPt.Y + DataHeight + headH < ClientRectangle.Bottom) hotPt.Y = ClientRectangle.Height - DataHeight - headH - 1;
                            if (hotPt.Y > 0) hotPt.Y = 0;
                        }

                        ScroolEventsEnable = false;
                        if ((hScrool.Enabled) && (-(int)hotPt.X >= hScrool.Minimum) && (-(int)hotPt.X <= hScrool.Maximum)) hScrool.Value = -(int)hotPt.X;
                        if ((vScrool.Enabled) && (-(int)hotPt.Y >= vScrool.Minimum) && (-(int)hotPt.Y <= vScrool.Maximum)) vScrool.Value = -(int)hotPt.Y;
                        ScroolEventsEnable = true;

                        HotSpot = hotPt;
                        mouseDown.X = e.X;
                        mouseDown.Y = e.Y;
                    }
                    if (!tmrDraw.Enabled) tmrDraw.Start();
                }
                else
                {
                    if (e.Button == MouseButtons.None)
                    {
                        bool find = false;
                        for (i = 0; i < Count; i++)
                        {
                            if ((PlaneList[i] != null) && (PlaneList[i].TestCloseButton(bmp_grfx, e.X, e.Y)))
                            {
                                this.Invalidate();
                                find = true;
                                DrawCloseButton = true;
                                break;
                            }
                        }
                        if ((!find) && (DrawCloseButton))
                        {
                            DrawCloseButton = false;
                            DrawHead();
                            this.Invalidate();
                        }
                        
                        if (mainForm.canv.SetProfileOverWell(GetMouseOverWell(e.X, e.Y)))
                        {
                            DrawHead();
                            this.Invalidate();
                        }
                        
                        if (!tmrToolTip.Enabled) tmrToolTip.Start();
                        if (ind != -1)
                        {
                            float depth = PlaneList[ind].DepthFromScreenY(e.Y);
                            float md = PlaneList[ind].GetMDByAbs(depth);
                            string str = "";
                            if (md > -1) str = String.Format(", Изм:{0:0.##} м", md);
                            mainForm.SetStatus(String.Format("Абс: {0:0.##} м{1}", depth, str));
                            if (PlaneMode)
                            {
                                // находим керн
                                PlaneCore core = (PlaneCore)PlaneList[ind].GetObjectByType(Constant.Plane.ObjectType.CORE);
                                if (core != null)
                                {
                                    md = core.GetDepthMDByCursor(this.PointToClient(Cursor.Position));
                                    mainForm.dCoreTestSelectRow(md, false);
                                }
                            }
                        }
                    }
                }
                if ((!mainForm.UserDialogShowing) && (mainForm.IsActivated) &&
                    (dcp != null) && (dcp.DockedState == Infragistics.Win.UltraWinDock.DockedState.Docked))
                    this.Focus();
            }
        }
        void CorrelationScheme_MouseLeave(object sender, EventArgs e)
        {
            tmrDraw.Stop();
            mainForm.canv.SetProfileOverWell(null);
            ClearSelectedData();
            this.DrawHead();
            this.Invalidate();
            MarkerDepthEditMode = false;
            this.Cursor = Cursors.Default;
            mainForm.dGisSelectRow(-1);
            mainForm.dCoreTestSelectRow(-1, false);
        }
        void CorrelationScheme_MouseEnter(object sender, EventArgs e)
        {
            if (MarkerEditMode)
            {
                Cursor = this.curMrkEditMode;
            }
            else
            {
                Cursor = Cursors.Default;
            }
        }
        void CorrelationScheme_MouseUp(object sender, MouseEventArgs e)
        {
            if (snapShootMode)
            {
                SnapToClipboard();
            }
            else if (DataLoaded)
            {
                if (DrawCloseButton)
                {
                    for (int i = 0; i < PlaneList.Length; i++)
                    {
                        if (PlaneList[i].ShowCloseButton)
                        {
                            this.RemovePlane(i);
                            if (ObjSrc != null)
                            {
                                Profile prfl = (Profile)((C2DLayer)ObjSrc).ObjectsList[0];
                                prfl.RemoveAt(i);
                                mainForm.corSchSaveProfileLayer();
                                mainForm.canv.DrawLayerList();
                            }
                            DrawData();
                            break;
                        }
                    }
                }
                else
                {
                    if ((MarkerDepthEditMode) && (Math.Sqrt(Math.Pow(mouseDown.X - e.X, 2) + Math.Pow(mouseDown.Y - e.Y, 2)) < 2))
                    {
                        int ind = GetMouseOverPlane(e.X, e.Y);
                        if (ind > -1)
                        {
                            AddMarker(ind, e.Y);
                            mainForm.corSchSaveMarkerLayer();
                            mainForm.PlaneUpdateMarkers(PlaneList[ind].w);
                        }
                        DrawData();
                    }
                    if (e.Button == MouseButtons.Left) DrawData();
                }
            }
            MarkerDepthEditMode = false;
            MouseBtnDown = false;
            SetWindowSize(0, 0);
            tmrDraw.Stop();
        }
        void CorrelationScheme_MouseWheel(object sender, MouseEventArgs e)
        {
            if ((proj != null) && (DataLoaded) && (this.ClientRectangle.Contains(e.X, e.Y)))
            {
                ClearSelectedData();
                if (PlaneMode)
                {
                    // mainForm.StatUsage.AddMessage(CollectorStatId.PLANE_ZOOM);
                }
                else
                {
                    // mainForm.StatUsage.AddMessage(CollectorStatId.CORSCHEME_ZOOM);
                }
                PointF hotPt = HotSpot;
                float delta = (float)e.Delta / 120;
                int k = (delta < 0) ? -1 : 1;
                delta = (k < 0) ? -delta : delta;
                bool reCalcWidth = true;
                float headH = (this.IsHeadColumnDraw) ? HeadHeight : 0;
                bool ScaleProportional = (Control.ModifierKeys == Keys.None);
                while (delta > 0)
                {
                    reCalcWidth = true;
                    if ((!BindWidth) && ((Control.ModifierKeys == Keys.Shift) || (ScaleProportional)))
                    {
                        float dX1 = 0;
                        int index = GetMouseOverPlane(e.X, e.Y);
                        int indexCol = -1;
                        float width1, width2, wCol1 = 0, wCol2 = 0;
                        float startWidth = this.Width;
                        if (index > -1)
                        {
                            indexCol = PlaneList[index].GetMouseOverColumn(e.X, e.Y);
                            if (indexCol > -1)
                            {
                                wCol1 = PlaneList[index].GetWidthByColumnIndex(indexCol);
                                width1 = GetWidthByIndex(index) + wCol1;
                                dX1 = (e.X - (HotSpot.X + width1)) / PlaneList[index].Columns[indexCol].Width;
                            }
                        }
                        if (k < 0)
                            this.ScaleWidth(1.3f);
                        else
                            this.ScaleWidth(1 / 1.3f);

                        reCalcWidth = (startWidth != this.Width);
                        if (indexCol > -1)
                        {
                            wCol2 = PlaneList[index].GetWidthByColumnIndex(indexCol);
                            width2 = GetWidthByIndex(index) + wCol2;
                            hotPt.X = e.X - width2 - dX1 * PlaneList[index].Columns[indexCol].Width;
                        }
                        if ((ScaleProportional) && (!reCalcWidth))
                        {
                            if (hotPt.Y > 0) hotPt.Y = 0;
                            HotSpot = hotPt;
                            break;
                        }
                    }

                    if (((!BindWidth) || (reCalcWidth)) && ((Control.ModifierKeys == Keys.Control) || (Control.ModifierKeys == Keys.None)))
                    {
                        float scrY = e.Y;
                        if (scrY > HotSpot.Y + DataHeight + headH) scrY = HotSpot.Y + DataHeight + headH;
                        float depth = DepthFromScreenY(scrY);
                        float dY;
                        if (k < 0)
                        {
                            if (YUnitsInDepthUnit * 1.3f < 1000)
                            {
                                YUnitsInDepthUnit = YUnitsInDepthUnit * 1.3f;
                                dY = ScreenYFromDepth(depth) - e.Y;
                                hotPt.Y -= dY;
                            }
                            else if (ScaleProportional)
                            {
                                if (hotPt.Y > 0) hotPt.Y = 0;
                                HotSpot = hotPt;
                                break;
                            }
                        }
                        else
                        {
                            if (YUnitsInDepthUnit / 1.3f > 0.00001)
                            {
                                YUnitsInDepthUnit = YUnitsInDepthUnit / 1.3f;
                                dY = ScreenYFromDepth(depth) - e.Y;
                                hotPt.Y -= dY;
                            }
                            else if (ScaleProportional)
                            {
                                if (hotPt.Y > 0) hotPt.Y = 0;
                                HotSpot = hotPt;
                                break;
                            }
                        }

                    }
                    if (hotPt.Y > 0) hotPt.Y = 0;
                    HotSpot = hotPt;
                    delta--;
                }

                SetPlaneRects();
                SetScrollBar();
                DrawData();
            }
        }
        void CorrelationScheme_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (DataLoaded)
            {
                int index = GetMouseOverPlane(e.X, e.Y);
                cmRemoveMarkerDepth.Visible = false;
                if (index > -1)
                {
                    if ((e.Y < PlaneList[index].WellHeadYOffset) && (!DrawCloseButton))
                    {
                        mainForm.canv.SetCenterSkv(PlaneList[index].w.OilFieldIndex, PlaneList[index].w.Index, false, true);
                    }
                    if (PlaneMode)
                    {
                        PlaneCore core = (PlaneCore)PlaneList[index].GetObjectByType(Constant.Plane.ObjectType.CORE);
                        if (core != null)
                        {
                            float md = core.GetDepthMDByCursor(this.PointToClient(Cursor.Position));
                            mainForm.dCoreTestSelectRow(md, true);
                        }
                    }
                }
            }
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) SnapShootMode = false;
            base.OnKeyDown(e);
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
            {
                if (MarkerDepthEditMode)
                {
                    Point pt = Cursor.Position;
                    int ind = GetMouseOverPlane(pt.X, pt.Y);
                    if (ind > -1)
                    {
                        AddMarker(ind, pt.Y);
                        mainForm.corSchSaveMarkerLayer();
                    }
                    MarkerDepthEditMode = false;
                }
            }
        }

        // HOTSPOT RECALC
        public void HotSpotReCalc()
        {
            int i;
            C2DLayer lr;
            Profile prfl;
            List<C2DLayer> list = new List<C2DLayer>();

            for (i = 0; i < mainForm.canv.LayerWorkList.Count; i++)
            {
                lr = (C2DLayer)mainForm.canv.LayerWorkList[i];
                lr.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.PROFILE, list);
            }
            PointF hotSpot;
            for (i = 0; i < list.Count; i++)
            {
                prfl = (Profile)((C2DLayer)list[i]).ObjectsList[0];
                hotSpot = prfl.CorSchHotSpot;
            }
        }

        // LAYERSRC
        public void SelectLayerSrcNode()
        {
            if ((ObjSrc != null) && (ObjSrc.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) && (((C2DLayer)ObjSrc).node != null))
            {
                //mainForm.canv.twWork.SelectedNode = LayerSrc._Node;
                //mainForm.canv.twWork.SetOneSelectedNode(LayerSrc._Node);
                mainForm.canv.twWork.SelectNodeByType(Constant.BASE_OBJ_TYPES_ID.PROFILE, ((C2DLayer)ObjSrc).node);
            }
        }

        // WINDOW TITLE
        public void UpdateWindowTitle()
        {
            if (!PlaneMode)
            {
                string markerText = "", profileText = "";
                if (this.currMrkLayer != null) markerText = String.Format(" Маркер: {0}", currMrkLayer.Name);
                if (this.ObjSrc != null) profileText = string.Format(" [{0} ({1} скв){2}]", ((C2DLayer)ObjSrc).Name, ((Profile)((C2DLayer)ObjSrc).ObjectsList[0]).Count, markerText);
                this.dcp.Text = String.Format("Кор.схема{0}", profileText);
                if (RangeMarker != null) this.dcp.Text += " - Выравнивание на '" + RangeMarker.Name + "'";
            }
            else
            {
                string skvText = "", areaText = "";
                if ((this.ObjSrc != null) && (this.ObjSrc.TypeID == Constant.BASE_OBJ_TYPES_ID.WELL) && (proj != null))
                {
                    OilField of = proj.OilFields[((Well)ObjSrc).OilFieldIndex];
                    var dictArea = (OilFieldAreaDictionary)proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                    areaText = ", " + dictArea.GetShortNameByCode(((Well)ObjSrc).OilFieldAreaCode);
                    skvText = string.Format("{0} [{1}{2}]", ((Well)ObjSrc).UpperCaseName, of.Name, areaText);
                }
                this.dcp.Text = String.Format("Планшет:{0}", skvText);
            }
        }

        // SETTING DATA
        public void SetProject(Project project)
        {
            this.proj = project;
        }
        public void SetBaseScheme(Plane Scheme)
        {
            this.BaseScheme = Scheme;
            this.YUnitsInDepthUnit = 2;
        }
        public bool SetWellSource(Well w)
        {
            bool res = false;
            if (w != null)
            {
                if ((this.ObjSrc != w))
                {
                    this.ObjSrc = w;
                    res = true;
                    UpdateWindowTitle();
                }
            }
            else
            {
                ClearAll();
            }
            return res;
        }
        public bool SetProfileSource(C2DLayer layer)
        {
            bool res = false;
            if ((layer != null) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE) && (layer.ObjectsList.Count > 0))
            {
                if (this.ObjSrc != layer)
                {
                    if (this.ObjSrc != null)
                    {
                        ((Profile)((C2DLayer)ObjSrc).ObjectsList[0]).Selected = false;
                        if (((C2DLayer)ObjSrc).node != null) mainForm.canv.twWork.SetNodeFontBold(((C2DLayer)ObjSrc).node, false);
                    }
                    this.ObjSrc = layer;
                    ((Profile)((C2DLayer)ObjSrc).ObjectsList[0]).Selected = true;
                    if (((C2DLayer)ObjSrc).node != null) mainForm.canv.twWork.SelectNodeByType(Constant.BASE_OBJ_TYPES_ID.PROFILE, ((C2DLayer)ObjSrc).node);
                    res = true;
                    UpdateWindowTitle();
                }
            }
            return res;
        }
        public void CreatePlaneListByObjSrc()
        {
            if ((ObjSrc != null) && (ObjSrc.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                Profile prfl = (Profile)((C2DLayer)ObjSrc).ObjectsList[0];
                PlaneList = new Plane[prfl.Count];
                for (int i = 0; i < prfl.Count; i++)
                {
                    PlaneList[i] = new Plane(BaseScheme, this.PlaneMode);
                    PlaneList[i].w = prfl[i];
                }
                Count = prfl.Count;
            }
            else if ((ObjSrc != null) && (ObjSrc.TypeID == Constant.BASE_OBJ_TYPES_ID.WELL))
            {
                Well w = (Well)this.ObjSrc;
                PlaneList = new Plane[1];
                PlaneList[0] = new Plane(BaseScheme, this.PlaneMode);
                PlaneList[0].w = w;
                Count = 1;
            }
            ShowDuplicatesLog = ShowDuplicates;
        }
        public void LoadProfileData(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (worker.CancellationPending)
            {
                DataLoaded = false;
                e.Cancel = true;
            }
            else
            {
                if (ObjSrc != null)
                {
                    this.ContextMenuStrip = null;
                    DataLoaded = false;
                    DataLoading = true;
                    WorkerState userState = new WorkerState();
                    userState.WorkMainTitle = "Загрузка скважинных данных в корсхему";
                    userState.WorkCurrentTitle = "Загрузка скважинных данных в корсхему";
                    string ofName;
                    bool usedGis = TestContainData(Constant.Plane.ObjectType.GIS) || !PlaneList[0].InitDepthByTrajectory;
                    bool usedPerf = TestContainData(Constant.Plane.ObjectType.PERFORATION);
                    bool usedTrajectory = (PlaneList != null && PlaneList.Length > 0 && PlaneList[0].InitDepthByTrajectory);

                    if (usedGis || usedPerf || usedTrajectory)
                    {
                        int ind;
                        List<int> ofIndexes = new List<int>();
                        List<List<int>> wellIndexes = new List<List<int>>();
                        for (int i = 0; i < Count; i++)
                        {
                            ind = ofIndexes.IndexOf(PlaneList[i].w.OilFieldIndex);
                            if (ind == -1)
                            {
                                ofIndexes.Add(PlaneList[i].w.OilFieldIndex);
                                wellIndexes.Add(new List<int>());
                                ind = wellIndexes.Count - 1;
                            }
                            wellIndexes[ind].Add(PlaneList[i].w.Index);
                        }
                        for (int i = 0; i < ofIndexes.Count; i++)
                        {
                            if (usedGis) proj.OilFields[ofIndexes[i]].LoadGisFromCache(worker, e, wellIndexes[i]);
                            if (usedPerf) proj.OilFields[ofIndexes[i]].LoadPerfFromCache(worker, e, wellIndexes[i]);
                            if (usedTrajectory)
                            {
                                bool trajectoryVisible = proj.OilFields[ofIndexes[i]].WellTrajectoryLoaded;
                                proj.OilFields[ofIndexes[i]].LoadTrajectoryFromCache(worker, e, wellIndexes[i]);
                                if (!trajectoryVisible) proj.OilFields[ofIndexes[i]].SetVisibleWellTrajectory(false);
                            }
                        }
                    }

                    for (int i = 0; i < Count; i++)
                    {
                        if ((worker != null) && (worker.CancellationPending))
                        {
                            e.Cancel = true;
                            return;
                        }
                        PlaneList[i].LoadSourceWellData(worker, e, proj);
                        ofName = "";
                        if (PlaneList != null)
                        {
                            ofName = " [" + proj.OilFields[PlaneList[i].w.OilFieldIndex].Name + "]";
                            userState.Element = PlaneList[i].w.Name + ofName;
                        }
                        if (worker != null) worker.ReportProgress(100, userState);
                    }
                    this.RangeMarker = null;
                    this.MarkerZones = this.markerZones;
                    proj.ClearBufferList(true);
                    SetMinMaxDepth();
                    if (this.BindWidth) this.SetBindWidth(this.BindWidth);
                    HotSpot = PointF.Empty;
                    SetShowColumnHead();
                    SetObjectsCoord();
                    SetWindowSize(0, 0);
                    if (worker == null) SetScrollBar();
                    YUnitsInDepthUnit = YUnitsInDepthUnit;
                    SetPlaneRects();
                    DataLoading = false;
                    DataLoaded = true;
                }
            }
        }
        public bool TestContainData(Constant.Plane.ObjectType ObjType)
        {
            if (PlaneList != null)
            {
                bool result = false;
                for (int i = 0; i < PlaneList.Length; i++)
                {
                    result = PlaneList[i].TestContainData(ObjType);
                    if (result) return result;
                }
            }
            return false;
        }

        // ADD-REMOVE WELL
        public int AddWellToProfile(Well w)
        {
            int index = -1;
            if (ObjSrc != null)
            {
                Profile prfl = (Profile)((C2DLayer)ObjSrc).ObjectsList[0];
                index = prfl.GetWellIndex(w);
                if (index > -1)
                {
                    prfl.RemoveAt(index);
                }
                else
                {
                    index = prfl.AddWellByXY(w);
                }
                ((C2DLayer)ObjSrc).GetObjectsRect();
            }
            return index;

        }
        public int AddWell(BackgroundWorker worker, DoWorkEventArgs e, Well w)
        {
            int index = -1;
            if (ObjSrc != null)
            {
                index = this.GetPlaneIndexByWell(w);
                if (index > -1)
                {
                    RemovePlane(index);
                }
                else
                {
                    Profile prfl = (Profile)((C2DLayer)ObjSrc).ObjectsList[0];
                    index = prfl.GetWellIndex(w);
                    InsertWell(worker, e, index, w);
                }

            }
            return index;
        }
        public void InsertWell(BackgroundWorker worker, DoWorkEventArgs e, int index, Well w)
        {
            if ((PlaneList != null) && (PlaneList.Length > 0))
            {
                Plane[] newPlaneList = new Plane[PlaneList.Length + 1];
                bool find = false;
                int i, j;
                for (i = 0, j = 0; i < PlaneList.Length; i++)
                {
                    if ((!find) && (i == index))
                    {
                        newPlaneList[j] = new Plane(BaseScheme, this.PlaneMode);
                        newPlaneList[j].w = w;
                        newPlaneList[j].SetShowColumnHead(IsHeadColumnDraw);
                        newPlaneList[j].LoadSourceWellData(worker, e, proj);
                        find = true;
                        j++;
                    }
                    newPlaneList[j] = PlaneList[i];
                    j++;
                }
                if (!find)
                {
                    j = PlaneList.Length;
                    newPlaneList[j] = new Plane(BaseScheme, this.PlaneMode);
                    newPlaneList[j].w = w;
                    newPlaneList[j].SetShowColumnHead(IsHeadColumnDraw);
                    newPlaneList[j].LoadSourceWellData(worker, e, proj);
                    newPlaneList[j].MarkerZones = this.MarkerZones;
                }
                PlaneList = newPlaneList;
            }
            if (worker != null) worker.ReportProgress(100);
            Count++;
            HotSpot = hotSpot;
            SetMinMaxDepth();
            SetObjectsCoord();
            SetWindowSize(0, 0);
            SetPlaneRects();
            YUnitsInDepthUnit = YUnits;
        }
        public void RemovePlane(Well w)
        {
            if ((PlaneList != null) && (PlaneList.Length > 0) && (BaseScheme != null))
            {
                for (int i = 0; i < Count; i++)
                {
                    if (PlaneList[i].w == w)
                    {
                        RemovePlane(i);
                        break;
                    }
                }
            }
        }
        public void RemovePlane(int Index)
        {
            if ((PlaneList != null) && (PlaneList.Length > 0) && (BaseScheme != null))
            {
                int i, j = 0;

                if (PlaneList.Length == 1)
                {
                    PlaneList = null;
                }
                else
                {
                    Plane[] newPlaneList = new Plane[PlaneList.Length - 1];
                    for (i = 0; i < Count; i++)
                    {
                        if (i != Index)
                        {
                            newPlaneList[j] = PlaneList[i];
                            j++;
                        }
                    }
                    PlaneList = newPlaneList;
                }
                Count--;

                SetMinMaxDepth();
                SetObjectsCoord();
                if (Width < this.ClientRectangle.Width) hotSpot.X = 0;
                else if (hotSpot.X < ClientRectangle.Width - Width) hotSpot.X = ClientRectangle.Width - Width - 1;
                HotSpot = hotSpot;
                SetWindowSize(0, 0);
                YUnitsInDepthUnit = YUnits;
            }
        }
        public void ClearAll()
        {
            RangeMarker = null;
            if ((mainForm.worker.IsBusy) && (mainForm.worker.GetWorkType() == (int)Constant.BG_WORKER_WORK_TYPE.LOAD_CORSCHEME_BY_PROFILE))
            {
                mainForm.worker.CancelAsync();
            }
            if (bmp_grfx != null) bmp_grfx.Clear(Color.White);
            PlaneList = null;
            Count = 0;
            ObjSrc = null;
            currMrkLayer = null;
            DrawWaitIconMode = false;
            EnableToolTip = true;
            HotSpot = PointF.Empty;
            SetScrollBar();
            DataLoaded = false;
            UpdateWindowTitle();
            this.Invalidate();
        }

        // MARKERS
        void AddMarker(int index, float screenY)
        {
            if (currMrkLayer == null) currMrkLayer = GetCurrentWorkMarker();

            if (currMrkLayer == null)
            {
                if (MessageBox.Show("В рабочей папке нет выбранных маркеров!\nСоздать новый маркер?",
                   "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    currMrkLayer = mainForm.canv.AddNewMarker();
                    if (currMrkLayer.node != null) mainForm.canv.twWork.SetNodeFontBold(currMrkLayer.node, true);
                }
            }
            if (currMrkLayer != null)
            {
                currMrkLayer.Selected = MarkerEditMode;
                Marker mrk = (Marker)currMrkLayer.ObjectsList[0];
                float depth = (float)PlaneList[index].DepthFromScreenY(screenY);
                int ind = PlaneList[index].w.GetMarkerIndex(mrk);
                if (ind == -1)
                {
                    mrk.Add(PlaneList[index].w, depth);
                    currMrkLayer.GetObjectsRect();
                    PlaneList[index].LoadMarkers();
                }
                else
                {
                    mrk.SetDepthByWell(PlaneList[index].w, depth);
                    PlaneList[index].SetMarkerDepth(mrk, depth);
                }
                mainForm.canv.DrawLayerList();
                DrawData();
            }
        }
        public void SetCurrentMarker(C2DLayer mrkLayer)
        {
            if (currMrkLayer != null)
            {
                currMrkLayer.Selected = false;
                if (currMrkLayer.node != null) mainForm.canv.twWork.SetNodeFontBold(currMrkLayer.node, false);
            }
            currMrkLayer = mrkLayer;
            if (currMrkLayer != null)
            {
                currMrkLayer.Selected = true;
                if (currMrkLayer.node != null) mainForm.canv.twWork.SelectNodeByType(Constant.BASE_OBJ_TYPES_ID.MARKER, currMrkLayer.node);
            }
            mainForm.planeUpdateWindow(true);
            UpdateWindowTitle();
        }
        public void SetMarkerEditMode(bool EditMode)
        {
            if (EditMode)
            {
                cmAddMarkerItem.Visible = true;
            }
            else
            {
                cmAddMarkerItem.Visible = false;
            }
            MarkerEditMode = EditMode;
            MarkerDepthEditMode = false;
            if (EditMode)
            {
                Cursor = this.curMrkEditMode;
            }
            else
            {
                Cursor = Cursors.Default;
            }
            if (currMrkLayer != null)
            {
                currMrkLayer.Selected = false;
                if (currMrkLayer.node != null) mainForm.canv.twWork.SetNodeFontBold(currMrkLayer.node, false);
                if (!EditMode) mainForm.corSchSaveMarkerLayer();
            }
            currMrkLayer = null;

            //if (EditMode) currMrkLayer = GetCurrentWorkMarker();
            //if (currMrkLayer != null)
            //{
            //    currMrkLayer.Selected = MarkerEditMode;
            //    SetMedianDepthByMarker((Marker)currMrkLayer._ObjectsList[0]);
            //}
            UpdateWindowTitle();
            DrawData();
        }
        public C2DLayer GetCurrentWorkMarker()
        {
            if ((mainForm != null) && (mainForm.canv.twWork.SelectedNodes.Count > 0))
            {
                C2DLayer lr;
                for (int i = 0; i < mainForm.canv.twWork.SelectedNodes.Count; i++)
                {
                    if (mainForm.canv.twWork.SelectedNodes[i].Tag != null)
                    {
                        lr = (C2DLayer)mainForm.canv.twWork.SelectedNodes[i].Tag;
                        if (lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                        {
                            return lr;
                        }
                    }
                }
            }
            return null;
        }
        public void RemoveMarkerList(ArrayList list)
        {
            if ((list != null) && (Count > 0))
            {
                for (int i = 0; i < Count; i++)
                {
                    for (int j = 0; j < list.Count; j++)
                    {
                        PlaneList[i].RemoveMarker((Marker)list[j]);
                    }
                }
                DrawData();
            }
        }
        public void UpdateMarkers(Well w)
        {
            if ((PlaneList != null) && (PlaneList.Length > 0))
            {
                if (PlaneList[0].w == w)
                {
                    PlaneList[0].LoadMarkers();
                    DrawData();
                }
            }
        }
        public void ReloadMarkers()
        {
            for (int i = 0; i < Count; i++)
            {
                PlaneList[i].LoadMarkers();
            }
            DrawData();
        }
        void cmAddMarkerItem_Click(object sender, EventArgs e)
        {
            if (currMrkLayer != null) currMrkLayer.Selected = false;
            mainForm.corSchSetMarkerEditMode(true);
            currMrkLayer = mainForm.canv.AddNewMarker();
            int ind = GetMouseOverPlane(mouseDown.X, mouseDown.Y);
            if (ind > -1)
            {
                AddMarker(ind, mouseDown.Y);
                mainForm.corSchSaveMarkerLayer();
                mainForm.PlaneReloadMarkers();
            }
        }
        void cmAutoCreateMarker_Click(object sender, EventArgs e)
        {
            MarkerAutoCreateForm.ShowForm(mainForm.canv, mainForm.canv.twActiveOilField.ActiveOilFieldIndex);
        }
        void cmRangeByMarker_Click(object sender, EventArgs e)
        {
            RangeMarker = (Marker)currMrkLayer.ObjectsList[0];
            SetMinMaxDepth();
            SetObjectsCoord();
            SetWindowSize(0, 0);
            SetPlaneRects();
            DrawData();
            UpdateWindowTitle();
        }
        void cmRemoveMarkerDepth_Click(object sender, EventArgs e)
        {
            if (currMrkLayer != null)
            {
                Marker mrk = (Marker)currMrkLayer.ObjectsList[0];
                int ind = GetMouseOverPlane(mouseDown.X, mouseDown.Y);
                if (ind > -1)
                {
                    bool res = mrk.RemoveDepthByWell(PlaneList[ind].w);
                    if (res)
                    {
                        PlaneList[ind].RemoveMarker(mrk);
                        PlaneList[ind].w.RemoveMarker(mrk);
                        DrawData();
                        mainForm.corSchSaveMarkerLayer();
                        mainForm.PlaneReloadMarkers();
                        mainForm.canv.DrawLayerList();
                    }
                }
            }
        }

        // MARKER ZONES
        List<PlaneMarker> GetMarkersList()
        {
            List<PlaneMarker> markers = new List<PlaneMarker>();
            List<string> MarkersName = new List<string>();
            for (int i = 0; i < PlaneList.Length; i++)
            {
                if(PlaneList[i].Markers != null)
                {
                    for (int j = 0; j < PlaneList[i].Markers.Length; j++)
                    {
                        if (MarkersName.IndexOf(PlaneList[i].Markers[j].srcMarker.Name) == -1)
                        {
                            if ((markers.Count == 0) || (markers[markers.Count - 1].Depth < PlaneList[i].Markers[j].Depth))
                            {
                                markers.Add(PlaneList[i].Markers[j]);
                            }
                            else
                            {
                                for (int k = 0; k < markers.Count; k++)
                                {
                                    if (PlaneList[i].Markers[j].Depth < markers[k].Depth)
                                    {
                                        markers.Insert(k, PlaneList[i].Markers[j]);
                                        break;
                                    }
                                }
                            }
                            MarkersName.Add(PlaneList[i].Markers[j].srcMarker.Name);
                        }
                    }
                }
            }
            return markers;
        }
        void cmCreateMarkerZones_Click(object sender, EventArgs e)
        {
            List<PlaneMarker> markers = GetMarkersList();
            if (markers.Count > 1)
            {
                MarkersZonesForm form = new MarkersZonesForm(mainForm, MarkerZones, markers);
                if (form.ShowDialog() == DialogResult.OK)
                {
                    this.MarkerZones = form.Zones;
                    DrawData();
                }
            }
            else
            {
                MessageBox.Show(this, "Для создания зон необходимо создать как минимум 2 маркера!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        // TOOLTIP
        public PlaneToolTip GetToolTip(int X, int Y)
        {
            PlaneToolTip toolTip = null, resToolTip = null;
            if (DataLoaded && !DataLoading && !MarkerEditMode)
            {
                for (int i = 0; i < Count; i++)
                {
                    toolTip = PlaneList[i].GetToolTip(proj.DictList, X, Y, Control.ModifierKeys == Keys.Shift);
                    if ((toolTip != null) && ((resToolTip == null) || (resToolTip.ScreenDist > toolTip.ScreenDist)))
                    {
                        resToolTip = toolTip;
                    }
                }
            }
            return resToolTip;
        }
        void tmrToolTip_Tick(object sender, EventArgs e)
        {
            Point pt = PointToClient(Cursor.Position);
            if (DataLoaded && PlaneMode)
            {
                int ind = this.GetMouseOverPlane(pt.X, pt.Y);
                if (ind != -1)
                {
                    float depth = PlaneList[ind].DepthFromScreenY(pt.Y);
                    mainForm.dGisSelectRow(depth);
                }
            }
            ToolTip = GetToolTip(pt.X, pt.Y);
            this.Invalidate();
            tmrToolTip.Stop();
        }

        // SCREEN - DATA
        public int GetMouseOverPlane(float X, float Y)
        {
            int res = -1;
            if ((!DataLoading) && (PlaneList != null) && (PlaneList.Length > 0))
            {
                for (int i = 0; i < PlaneList.Length; i++)
                {
                    if ((hotSpot.X + PlaneList[i].Coord.X < X) && (X < hotSpot.X + PlaneList[i].Coord.X + PlaneList[i].Width))
                    {
                        res = i;
                    }
                }
            }
            return res;
        }
        public Well GetMouseOverWell(float X, float Y)
        {
            Well res = null;
            int index = GetMouseOverPlane(X, Y);
            if (index > -1)
            {
                if (PlaneList[index] != null) res = PlaneList[index].w;
            }
            return res;
        }
        public int GetPlaneIndexByWell(Well w)
        {
            for (int i = 0; i < Count; i++)
            {
                if (PlaneList[i].w == w) return i;
            }
            return -1;
        }
        public float DepthFromScreenY(float screenY)
        {
            return MinDepth + (screenY - HotSpot.Y - HeadHeight) / YUnitsInDepthUnit;
        }
        public float ScreenYFromDepth(float depth)
        {
            return (depth - MinDepth) * YUnitsInDepthUnit + HotSpot.Y + HeadHeight;
        }

        // SHOW HIDE DATA
        public void ShowPane()
        {
            if (this.dcp != null)
            {
                this.dcp.Closed = false;
                if (this.dcp.Tag != null) ((ToolStripMenuItem)this.dcp.Tag).Checked = !this.dcp.Closed;
                if (!this.dcp.IsActive) this.dcp.Activate();
            }
        }
        void SetShowColumnHead()
        {
            for (int i = 0; i < Count; i++)
            {
                PlaneList[i].SetShowColumnHead(IsHeadColumnDraw);
            }
        }
        public void SetDepthCalcInitSource(bool InitFromTrajectory)
        {
            baseScheme.InitDepthByTrajectory = InitFromTrajectory;
            if (PlaneList != null)
            {
                for (int i = 0; i < Count; i++)
                {
                    PlaneList[i].InitDepthByTrajectory = InitFromTrajectory;
                }
            }
        }
        public void SetShowColumnHead(bool ShowColumnHead)
        {
            this.IsHeadColumnDraw = ShowColumnHead;
            SetShowColumnHead();
            SetObjectsCoord();
            HotSpot = hotSpot;
            DrawData();
            SetScrollBar();
        }
        public void SetVisibleByType(Constant.Plane.ObjectType Type, bool Visible)
        {
            for (int i = 0; i < Count; i++)
            {
                PlaneList[i].SetVisibleByType(Type, Visible);
            }
            BaseScheme.SetVisibleByType(Type, Visible);
            SetObjectsCoord();
            DrawData();
        }
        public void SetAbsMdDepth(bool ShowDepthAbs)
        {
            for (int i = 0; i < Count; i++)
            {
                PlaneList[i].SetDepthByAbsMd(ShowDepthAbs);
            }
            BaseScheme.SetDepthByAbsMd(ShowDepthAbs);
            DrawData();
        }
        public bool ShowDuplicatesLog
        {
            set
            {
                for (int i = 0; i < Count; i++)
                {
                    PlaneList[i].SetShowDuplicates = value;
                }
                this.ShowDuplicates = value;
                DrawData();
            }
        }
        void ClearSelectedData()
        {
            for (int i = 0; i < Count; i++)
            {
                PlaneList[i].ClearSelectedData();
            }
        }
        public void SetSelectedData(Constant.Plane.ObjectType Type, float DepthMD)
        {
            if ((PlaneMode) && (PlaneList != null) && (PlaneList.Length > 0))
            {
                if (PlaneList[0].SetSelectedDepth(Type, DepthMD))
                {
                    DrawData();
                }
            }
        }

        // DRAWING
        public void UpdatePlanesCoord()
        {
            float oldWidth = this.Width;
            this.Width = 0;
            PointF coordPt = PointF.Empty;
            for (int i = 0; i < Count; i++)
            {
                PlaneList[i].Coord = coordPt;
                coordPt.X += PlaneList[i].Width;
                this.Width += PlaneList[i].Width;
            }
        }
        public void SetBindWidth(bool BindWidth)
        {
            this.BindWidth = BindWidth;
            if (this.BindWidth)
            {
                PointF hotPt = HotSpot;
                hotPt.X = 0;
                HotSpot = hotPt;
                int i;

                for (i = 0; i < Count; i++) PlaneList[i].SetBindWidth(Parent.ClientRectangle.Width - vScrool.Width - 1);
                BaseScheme.SetBindWidth(Parent.ClientRectangle.Width - vScrool.Width - 1);

                UpdatePlanesCoord();

                //float oldWidth = this.Width;
                //this.Width = 0;
                //PointF coordPt = PointF.Empty;
                //for (i = 0; i < Count; i++)
                //{
                //    PlaneList[i].Coord = coordPt;
                //    coordPt.X += PlaneList[i].Width;
                //    this.Width += PlaneList[i].Width;
                //}
                DrawData();
            }
            else
            {
                int i;
                for (i = 0; i < Count; i++) PlaneList[i].SetNewWidth(Parent.ClientRectangle.Width - vScrool.Width - 1);
                BaseScheme.SetNewWidth(Parent.ClientRectangle.Width - vScrool.Width - 1);

                UpdatePlanesCoord();

                //float oldWidth = this.Width;
                //this.Width = 0;
                //PointF coordPt = PointF.Empty;
                //for (i = 0; i < Count; i++)
                //{
                //    PlaneList[i].Coord = coordPt;
                //    coordPt.X += PlaneList[i].Width;
                //    this.Width += PlaneList[i].Width;
                //}
                DrawData();
            }
        }
        public void ScaleWidth(float dScale)
        {
            int i;

            for (i = 0; i < Count; i++)
            {
                PlaneList[i].ScaleWidth(dScale);
            }

            BaseScheme.ScaleWidth(dScale);
            UpdatePlanesCoord();
        }
        public void SetNewWidth(float newWidth)
        {
            int i;

            for (i = 0; i < Count; i++)
            {
                PlaneList[i].SetNewWidth(newWidth);
            }

            BaseScheme.SetNewWidth(newWidth);
            UpdatePlanesCoord();
        }
        public void SetGraphicsSizes()
        {
            for (int i = 0; i < Count; i++)
            {
                PlaneList[i].SetGraphicsSizes(bmp_grfx);
            }
        }
        public void SetObjectsCoord()
        {
            // находим размеры шапки
            float hPlaneHead = 0, hColHead = 0;
            int i, j;

            SetGraphicsSizes();

            for (i = 0; i < Count; i++)
            {
                if (hPlaneHead < PlaneList[i].WellHeadYOffset)
                    hPlaneHead = PlaneList[i].WellHeadYOffset;

                for (j = 0; j < PlaneList[i].Count; j++)
                {
                    if (hColHead < PlaneList[i].Columns[j].HeadTextYOffset)
                        hColHead = PlaneList[i].Columns[j].HeadTextYOffset;
                }
            }
            this.Width = 0;
            for (i = 0; i < Count; i++)
            {
                PlaneList[i].WellHeadYOffset = hPlaneHead;
                this.Width += PlaneList[i].Width;

                for (j = 0; j < PlaneList[i].Count; j++)
                {
                    PlaneList[i].Columns[j].HeadTextYOffset = hColHead;
                }
            }
            HeadHeight = hPlaneHead;
            if (IsHeadColumnDraw) HeadHeight += hColHead;
            PointF coordPt = PointF.Empty;
            for (i = 0; i < Count; i++)
            {
                PlaneList[i].Coord = coordPt;
                coordPt.X += PlaneList[i].Width;
            }
            HotSpot = hotSpot;
        }

        public void SetPlaneRects()
        {
            if (PlaneList != null)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (PlaneList[i] != null) PlaneList[i].SetPlaneRect();
                }
            }
        }
        public void DrawHead()
        {
            DrawHead(bmp_grfx, false);
        }
        public void DrawData()
        {
            DrawData(bmp_grfx, false);
        }
        void DrawHead(Graphics grfx, bool DrawAllPlanes)
        {
            if ((PlaneList != null) && (DataLoaded))
            {
                grfx.FillRectangle(Brushes.White, hotSpotHeadImage.X, hotSpotHeadImage.Y, Width, HeadHeight);
                grfx.DrawRectangle(Pens.Black, hotSpotHeadImage.X, hotSpotHeadImage.Y, Width, HeadHeight);
                for (int i = 0; i < PlaneList.Length; i++)
                {
                    if (((hotSpot.X + PlaneList[i].Coord.X < this.ClientRectangle.Right) && (hotSpot.X + PlaneList[i].Coord.X + PlaneList[i].Width > 0)) || DrawAllPlanes)
                    {
                        PlaneList[i].DrawHead(grfx);
                    }
                }
            }
        }
        void DrawPlane(Graphics grfx, int index)
        {
            PlaneList[index].DrawObject(grfx);
            if ((index > 0) && (PlaneList[index].ShowMarkersConnection))
            {
                PlaneList[index].DrawMarkersZones(grfx, PlaneList[index - 1]);
                PlaneList[index].DrawMarkers(grfx, PlaneList[index - 1]);
            }
            else
            {
                PlaneList[index].DrawMarkersZones(grfx, null);
                PlaneList[index].DrawMarkers(grfx, null);
            }
            PlaneList[index].DrawBounds(grfx);
        }
        void DrawData(Graphics grfx, bool DrawAllPlanes)
        {
            if ((DataLoaded) && (!DataLoading))
            {
                grfx.Clear(Color.White);
                if (PlaneList != null)
                {
                    for (int i = 0; i < Count; i++)
                    {
                        if (((hotSpot.X + PlaneList[i].Coord.X < this.ClientRectangle.Right) && (hotSpot.X + PlaneList[i].Coord.X + PlaneList[i].Width > 0)) || DrawAllPlanes)
                        {
                            DrawPlane(grfx, i);
                        }
                    }
                }
                DrawHead(grfx, DrawAllPlanes);
                Invalidate();
           }
        }
        void DrawSnapShootMode(Graphics grfx)
        {
            if ((!Snap1.IsEmpty) && (!Snap2.IsEmpty))
            {
                float minX, minY, maxX, maxY;
                if (Snap2.X > Snap1.X)
                {
                    minX = Snap1.X;
                    maxX = Snap2.X;
                }
                else
                {
                    minX = Snap2.X;
                    maxX = Snap1.X;
                }
                if (Snap2.Y > Snap1.Y)
                {
                    minY = Snap1.Y;
                    maxY = Snap2.Y;
                }
                else
                {
                    minY = Snap2.Y;
                    maxY = Snap1.Y;
                }

                if (minX < this.ClientRectangle.Left) minX = this.ClientRectangle.Left;
                if (maxX > this.ClientRectangle.Right) maxX = this.ClientRectangle.Right - 1;
                if (minY < this.ClientRectangle.Top) minY = this.ClientRectangle.Top;
                if (maxY > this.ClientRectangle.Bottom) maxY = this.ClientRectangle.Bottom - 1;

                grfx.FillRectangle(brSnap, minX, minY, maxX - minX, maxY - minY);
                grfx.DrawRectangle(Pens.Black, minX, minY, maxX - minX, maxY - minY);
            }
        }

        // DRAWING
        void CorrelationScheme_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            grfx.DrawImageUnscaledAndClipped(bmp, this.ClientRectangle);

            DrawWaitIcon(grfx);
            DrawSnapShootMode(grfx);
            DrawTooltip(grfx);
        }

        // WAIT ICON DRAW
        public void StartDrawWaitIcon()
        {
            bmp_grfx.Clear(Color.White);
            DrawWaitIconMode = true;
            EnableToolTip = false;
            DataLoading = true;
            if (!PlaneMode) mainForm.corSchToolBarEnable(false);
            tmrWait.Start();
        }
        public void StopDrawWaitIcon()
        {
            mainForm.corSchToolBarEnable(true);
            DrawWaitIconMode = false;
            EnableToolTip = true;
            tmrWait.Stop();
            this.Invalidate();
        }
        void tmrWait_Tick(object sender, EventArgs e)
        {
            if ((DrawWaitIconMode) && (DataLoading))
            {
                wait_angle++;
                if (wait_angle > 11) wait_angle = 0;
                this.Invalidate();
            }
            else
            {
                tmrWait.Stop();
            }
        }
        public void DrawWaitIcon(Graphics grfx)
        {
            if ((this.DrawWaitIconMode) && (DataLoading))
            {
                PointF center = new PointF((float)(this.ClientRectangle.Right / 2), (float)(this.ClientRectangle.Bottom / 2));
                grfx.DrawImage(mainForm.bmpWait[wait_angle], center.X - 16, center.Y - 16, 32, 32);
            }
        }
        void DrawTooltip(Graphics grfx)
        {
            if ((EnableToolTip) && (ToolTip != null))
            {
                string str = ToolTip.Text;
                Color clr = Color.FromArgb(ToolTip.Color);
                
                using (Brush brush = new SolidBrush(clr))
                {
                    switch (ToolTip.Object.TypeID)
                    {
                        case Constant.Plane.ObjectType.PERFORATION:
                            
                            ToolTip.Object.DrawSelected(grfx);

                            string str1, str2, str3;
                            SizeF sz1, sz2, sz3;
                            str1 = string.Format("Перфорация{0}", ToolTip.Title);
                            str2 = ToolTip.Text;
                            str3 = string.Format("{0:0.#} - {1:0.#}[{2:0.#}]", ToolTip.Top, ToolTip.Bottom, ToolTip.Bottom - ToolTip.Top);
                            sz1 = grfx.MeasureString(str1, FontToolTipSmall, PointF.Empty, StringFormat.GenericTypographic);
                            sz2 = grfx.MeasureString(str2, FontToolTip, PointF.Empty, StringFormat.GenericTypographic);
                            sz3 = grfx.MeasureString(str3, FontToolTipSmall, PointF.Empty, StringFormat.GenericTypographic);
                            float minWidth = sz1.Width, maxWidth = sz1.Width, sumHeight;
                            if (minWidth > sz2.Width) minWidth = sz2.Width;
                            if (minWidth > sz3.Width) minWidth = sz3.Width;
                            if (maxWidth < sz2.Width) maxWidth = sz2.Width;
                            if (maxWidth < sz3.Width) maxWidth = sz3.Width;

                            sumHeight = sz1.Height + sz2.Height + sz3.Height;
                            float x = ToolTip.Point.X - minWidth / 2;
                            float y = ToolTip.Point.Y - sumHeight - 4;
                            if (x < 0) x = 0; else if (x + maxWidth > this.ClientRectangle.Right) x = this.ClientRectangle.Right - maxWidth;
                            if (y < 0) y = 0; else if (y + sumHeight > this.ClientRectangle.Bottom) y = this.ClientRectangle.Bottom - sumHeight;
                            grfx.DrawString(str1, FontToolTipSmall, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTipSmall, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTipSmall, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTipSmall, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTipSmall, Brushes.Black, x, y, StringFormat.GenericTypographic);
                            y += sz1.Height;
                            grfx.DrawString(str2, FontToolTip, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTip, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTip, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTip, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTip, Brushes.Black, x, y, StringFormat.GenericTypographic);
                            y += sz2.Height;
                            grfx.DrawString(str3, FontToolTipSmall, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str3, FontToolTipSmall, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str3, FontToolTipSmall, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str3, FontToolTipSmall, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str3, FontToolTipSmall, Brushes.Black, x, y, StringFormat.GenericTypographic);
                            break;
                        case Constant.Plane.ObjectType.LEAK:
                            str1 = ToolTip.Title;
                            str2 = ToolTip.Text; 
                            if (str2 != null && str2.Length > 0) str2 += "\n";
                            str2 += string.Format("{0:0.#} - {1:0.#}[{2:0.#}]", ToolTip.Top, ToolTip.Bottom, ToolTip.Bottom - ToolTip.Top);
                            sz1 = grfx.MeasureString(str1, FontToolTip, PointF.Empty, StringFormat.GenericTypographic);
                            sz2 = grfx.MeasureString(str2, FontToolTipSmall, PointF.Empty, StringFormat.GenericTypographic);

                            minWidth = sz1.Width;
                            maxWidth = sz1.Width;
                            if (minWidth > sz2.Width) minWidth = sz2.Width;
                            if (maxWidth < sz2.Width) maxWidth = sz2.Width;

                            sumHeight = sz1.Height + sz2.Height;
                            x = ToolTip.Point.X - minWidth / 2;
                            y = ToolTip.Point.Y - sumHeight - 11f;
                            if (x < 0) x = 0; //else if (x + maxWidth > this.ClientRectangle.Right) x = this.ClientRectangle.Right - maxWidth;
                            if (y < 0) y = 0; else if (y + sumHeight > this.ClientRectangle.Bottom) y = this.ClientRectangle.Bottom - sumHeight;
                            grfx.DrawString(str1, FontToolTip, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTip, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTip, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTip, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTip, Brushes.Black, x, y, StringFormat.GenericTypographic);
                            y += sz1.Height;
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.Black, x, y, StringFormat.GenericTypographic);
                            break;
                        case Constant.Plane.ObjectType.CORE:
                            ToolTip.Object.DrawSelected(grfx);
                            str1 = ToolTip.Title;
                            str2 = ToolTip.Text;
                            sz1 = grfx.MeasureString(str1, FontToolTip, PointF.Empty, StringFormat.GenericTypographic);
                            sz2 = grfx.MeasureString(str2, FontToolTipSmall, PointF.Empty, StringFormat.GenericTypographic);

                            minWidth = sz1.Width;
                            maxWidth = sz1.Width;
                            if (minWidth > sz2.Width) minWidth = sz2.Width;
                            if (maxWidth < sz2.Width) maxWidth = sz2.Width;

                            sumHeight = sz1.Height + sz2.Height;
                            x = ToolTip.Point.X - minWidth / 2;
                            y = ToolTip.Point.Y - sumHeight - 11f;
                            if (x < 0) x = 0;
                            if (y < 0) y = 0; else if (y + sumHeight > this.ClientRectangle.Bottom) y = this.ClientRectangle.Bottom - sumHeight;
                            grfx.DrawString(str1, FontToolTip, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTip, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTip, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTip, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str1, FontToolTip, Brushes.Black, x, y, StringFormat.GenericTypographic);
                            y += sz1.Height;
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str2, FontToolTipSmall, Brushes.Black, x, y, StringFormat.GenericTypographic);
                            break;
                        default:
                            SizeF sz = grfx.MeasureString(str, FontToolTipSmall, PointF.Empty, StringFormat.GenericTypographic);
                            x = ToolTip.Point.X - sz.Width / 2;
                            y = ToolTip.Point.Y - sz.Height - 11f;
                            if (x < 0) x = 0; else if (x + sz.Width > this.ClientRectangle.Right) x = this.ClientRectangle.Right - sz.Width;
                            if (y < 0) y = 0; else if (y + sz.Height > this.ClientRectangle.Bottom) y = this.ClientRectangle.Bottom - sz.Height;
                            if (ToolTip.Object.TypeID == Constant.Plane.ObjectType.LOG)
                            {
                                grfx.FillEllipse(brush, ToolTip.Point.X - 4, ToolTip.Point.Y - 4, 8, 8);
                                grfx.DrawEllipse(Pens.Black, ToolTip.Point.X - 4, ToolTip.Point.Y - 4, 8, 8);
                            }

                            grfx.DrawString(str, FontToolTipSmall, Brushes.White, x - 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str, FontToolTipSmall, Brushes.White, x - 1, y + 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str, FontToolTipSmall, Brushes.White, x + 1, y - 1, StringFormat.GenericTypographic);
                            grfx.DrawString(str, FontToolTipSmall, Brushes.White, x + 1, y + 1, StringFormat.GenericTypographic);

                            grfx.DrawString(str, FontToolTipSmall, Brushes.Black, x, y, StringFormat.GenericTypographic);
                            break;
                    }

                }
                // рисование глубины
            }
        }
        public void SetWindowSize(float dx, float dy)
        {
            float min = 0, max = this.ClientRectangle.Size.Height;
            if (dy > 0)
            {
                min = this.HeadHeight - dy - 10;
                if (min < 0) ScreenMinY = 0;
                max = dy + this.HeadHeight + 10;
            }
            else if (dy < 0)
            {
                min = this.ClientRectangle.Size.Height + dy - 30;
                max = this.ClientRectangle.Size.Height + 10;
            }
            ScreenMinY = min;
            ScreenMaxY = max;
        }
        public void DrawScroll()
        {
            if ((DataLoaded) && (!DataLoading) && (PlaneList != null))
            {
                float dx = HotSpot.X - lastDrawHotSpot.X;
                float dy = HotSpot.Y - lastDrawHotSpot.Y;

                if ((!PlaneMode) && (!mainForm.IsOSXP) && (Math.Abs(dx) + Math.Abs(dy) > 0.1))
                {
                    ScrollBitMap(dx, dy);
                    Rectangle outRect = new Rectangle((int)dx, (int)(dy + HeadHeight), ClientRectangle.Width, ClientRectangle.Height - (int)HeadHeight);
                    if (dy > 0) outRect.Y += 2; else outRect.Y -= 2;
                    bmp_grfx.ExcludeClip(outRect);

                    bmp_grfx.Clear(Color.White);
                    float ScreenMinX, ScreenMaxX;
                    ScreenMinX = this.ClientRectangle.Left;
                    ScreenMaxX = this.ClientRectangle.Right;
                    if (dx > 0)
                    {
                        ScreenMaxX = dx;
                    }
                    else
                    {
                        ScreenMinX = ScreenMaxX + dx;
                    }
                    float left, right;
                    List<int> exludeList = new List<int>();
                    for (int i = 0; i < Count; i++)
                    {
                        left = HotSpot.X + PlaneList[i].Coord.X;
                        right = left + PlaneList[i].Width;
                        if ((right > ScreenMinX) && (left < ScreenMaxX))
                        {
                            DrawPlane(bmp_grfx, i);
                            exludeList.Add(i);
                        }
                    }
                    SetWindowSize(dx, dy);
                    for (int i = 0; i < Count; i++)
                    {
                        if (exludeList.IndexOf(i) == -1)
                        {
                            left = HotSpot.X + PlaneList[i].Coord.X;
                            right = left + PlaneList[i].Width;
                            if ((right > this.ClientRectangle.Left) && (left < this.ClientRectangle.Right))
                            {
                                DrawPlane(bmp_grfx, i);
                            }
                        }
                    }
                    bmp_grfx.ResetClip();
                    SetWindowSize(0, 0);
                    DrawHead();
                    this.Invalidate();
                }
                else
                {
                    SetWindowSize(0, 0);
                    DrawData();
                }
            }
        }
        public void ScrollBitMap(float dx, float dy)
        {
            if (bmp_grfx != null)
            {
                bmp_grfx.DrawImageUnscaled(bmp, (int)dx, (int)dy, this.ClientRectangle.Width, this.ClientRectangle.Height);
            }
            this.Invalidate();
        }
    }
}
