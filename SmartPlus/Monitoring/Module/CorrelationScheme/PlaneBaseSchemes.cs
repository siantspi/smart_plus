﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus
{
    // СХЕМА ПО УМОЛЧАНИЮ
    public sealed class PlaneBaseSchemes
    {
        Plane[] SchemesList;

        public Plane this[int index]
        {
            get
            {
                if ((index > -1) && (index < SchemesList.Length))
                {
                    return SchemesList[index];
                }
                return null;
            }
        }
        public void CreateBaseSchemes()
        {
            Plane scheme;
            PlaneColumn col;
            // 2 базовые схемы
            SchemesList = new Plane[2];

            for (int i = 0; i < 2; i++)
            {
                // 1-я базовая схема
                SchemesList[i] = new Plane(false);
                scheme = SchemesList[i];
                scheme.Name = "Схема 1";
                scheme.WellHeadYOffset = 20;
                
                if ((i == 0) && (Environment.UserName.ToUpper() == "ASTASHKINNS")) //ASTASHKINNS
                {
                    scheme.Columns = new PlaneColumn[5];
                }
                else
                {
                    scheme.Columns = new PlaneColumn[4];
                }


                // 1 колонка - отметки глубин
                scheme.Columns[0] = new PlaneColumn();
                col = scheme.Columns[0];
                col.Add(new PlaneDepths());
                col.StartWidth = 40;
                col.MinWidth = 28;
                col.MaxWidth = 40;
                col.RealWidth = col.StartWidth;
                col.HeadTextOrientation = Constant.Plane.TextOrientation.LEFT;
                col.HeadText = "Глубина, м";

                // 2 колонка - ГИС
                scheme.Columns[1] = new PlaneColumn();
                col = scheme.Columns[1];
                col.Add(new PlaneGis());
                col.StartWidth = 16;
                col.MinWidth = 5;
                col.MaxWidth = 25;
                col.RealWidth = col.StartWidth;
                col.HeadTextOrientation = Constant.Plane.TextOrientation.LEFT;
                col.HeadText = "ГИС, Керн, ЗКЦ";
                col.Add(new PlaneCore());
                col.Add(new PlaneLeak());
                col.Add(new PlanePerfHistory());


                // 2 колонка - ГИС
                scheme.Columns[2] = new PlaneColumn();
                col = scheme.Columns[2];
                col.Add(new PlanePerf());
                col.StartWidth = 0;
                col.MinWidth = 0;
                col.MaxWidth = 0;
                col.RealWidth = col.StartWidth;

                // 4 колонка - Имя объекта + каротаж
                scheme.Columns[3] = new PlaneColumn();
                scheme.Columns[3].Add(new PlaneOilObj()); // 0

                // 4 колонка - ПСка
                scheme.Columns[3].Add(new PlaneLog(Constant.Plane.CurveType.PS, "PS*.MV|SP*.MV|ПС*.МВ")); // 1
                scheme.Columns[3].StartWidth = 100;
                scheme.Columns[3].RealWidth = 100;
                scheme.Columns[3].MinWidth = 30;

                // 4 колонка - BK
                scheme.Columns[3].Add(new PlaneLog(Constant.Plane.CurveType.BK, "BK*.OMM|БК*.ОММ")); // 2

                // 4 колонка - ГК
                scheme.Columns[3].Add(new PlaneLog(Constant.Plane.CurveType.GK, "GK*.MKRH|ГК*.МКРЧ")); // 3

                // 4 колонка - ИК
                scheme.Columns[3].Add(new PlaneLog(Constant.Plane.CurveType.IK, "IK*.OMM|ИК*.ОММ")); // 4

                // 5 колонка - ИК
                scheme.Columns[3].Add(new PlaneLog(Constant.Plane.CurveType.NGK, "NGK*.IMIN|НГК*.ИМИН|НК*.ИМИН|NGK*.UE|НГК*.УЕ|НК*.УЕ")); // 5
                scheme.Columns[3].AddScale(new int[] { 1 });
                scheme.Columns[3].AddScale(new int[] { 2 });
                scheme.Columns[3].AddScale(new int[] { 3 });
                scheme.Columns[3].AddScale(new int[] { 4 });
                scheme.Columns[3].AddScale(new int[] { 5 });
                // 6 колонка - ГИС ПОРИСТОСТЬ
                if ((i == 0) && (Environment.UserName.ToUpper() == "ASTASHKINNS")) //ASTASHKINNS
                {
                    scheme.Columns[4] = new PlaneColumn();
                    scheme.Columns[4].StartWidth = 28;
                    scheme.Columns[4].MinWidth = 18;
                    scheme.Columns[4].RealWidth = 28;
                    scheme.Columns[4].MaxWidth = 28;
                }
                scheme.SetWidth();

                //// 6 колонка - ГИС НЕФТЕНАСЫЩЕННОСТЬ
                //scheme.Columns[6] = new PlaneColumn();
                //col = scheme.Columns[6];
                //col.Add(new PlaneGisOilSat());
                //col.Width = 110;
            }
        }
        public void RemoveDoubleObjects()
        {
            for (int i = 0; i < SchemesList.Length; i++)
            {
                for (int j = 0; j < SchemesList[i].Count; j++)
                {
                    for(int k = 0; k < SchemesList[i].Columns[j].Count;k++)
                    {
                        if (SchemesList[i].Columns[j][k].TypeID == Constant.Plane.ObjectType.LOG) continue;

                        for (int t = k + 1; t < SchemesList[i].Columns[j].Count; t++)
                        {
                            if (SchemesList[i].Columns[j][k].TypeID == SchemesList[i].Columns[j][t].TypeID)
                            {
                                SchemesList[i].Columns[j].RemoveAt(t);
                                t--;
                            }
                        }
                    }
 
                }
            }
        }
        public void AddNewObjects(PlaneBaseSchemes DefaultSchemes)
        {
            PlaneColumn col, defCol;
            bool find = false;
            for (int i = 0; (i < SchemesList.Length) && (i < DefaultSchemes.SchemesList.Length); i++)
            {
                for (int j = 0; (j < SchemesList[i].Columns.Length) && (j < DefaultSchemes.SchemesList[i].Columns.Length); j++)
                {
                    col = SchemesList[i].Columns[j];
                    defCol = DefaultSchemes.SchemesList[i].Columns[j];
                    if (col.Count < defCol.Count)
                    {                      
                        for (int k = 0; k < defCol.Count; k++)
                        {
                            find = false;
                            for (int t = 0; t < col.Count; t++)
                            {
                                if (defCol[k].TypeID == col[t].TypeID)
                                {
                                    find = true;
                                    break;
                                }
                            }
                            if (!find) col.Add(defCol[k]);
                        }
                    }
                }
            }
        }

        public void ReadFromAppSettings(BinaryReader br, int Version)
        {
            int count = br.ReadInt32();
            if (count > 0)
            {
                SchemesList = new Plane[count];
                for (int i = 0; i < count; i++)
                {
                    SchemesList[i] = new Plane(br.ReadBoolean());
                    SchemesList[i].ReadFromStream(br, Version);
                }
            }
        }
        public void WriteToAppSettings(BinaryWriter bw)
        {
            bw.Write(SchemesList.Length);
            for (int i = 0; i < SchemesList.Length; i++)
            {
                bw.Write(SchemesList[i].PlaneMode);
                SchemesList[i].WriteToStream(bw);
            }
        }
    }
}
