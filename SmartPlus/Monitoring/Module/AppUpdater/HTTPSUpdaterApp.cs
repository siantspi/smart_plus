﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Threading;
using System.Deployment.Application;
using System.Reflection;
using System.IO.Compression;
using Ionic.Zip;
using zlib;


namespace SmartPlus
{
    public delegate void OnEndCheckAppUpdatesDelegate();
    public delegate void OnEndDownloadAppDelegate();
    public delegate void OnStartDownloadAppDelegate();
    public delegate void OnStartSetupAppDelegate();
    public delegate void OnEndItemCopyAppDelegate(UpdateFileItem item);

    public class HTTPSUpdaterApp
    {
        public event OnEndCheckAppUpdatesDelegate OnCheckAppUpdates;
        public event OnStartDownloadAppDelegate OnStartAppDownload;
        public event OnEndDownloadAppDelegate OnEndAppDownload;
        public event OnEndItemCopyAppDelegate OnEndItemAppCopy;
        public event OnStartSetupAppDelegate OnStartSetupApp;

        private string ServerName;
        private string ServerUser;
        private string ServerPass;
        private static string ServerDirs = "SmartPlus";
        private string AppDir;
        private string TempDir;
        private bool UpdateByHttps;

        private static string temp_mask = "up_temp_";
        
        private string my_filename;
        private string up_filename;
        
        AppUpdaterWork Work;
        BackgroundWorker worker;
        HttpFileReciever http;
        AppCommonSettings AppSettings;
        AppSetting AppSett;

        // Признак, что началось скачивание обновления, требуется ожидание завершения процесса
        private bool is_download;   
        public bool download() 
        { 
            return is_download; 
        }

        // Признак, что обновление не требуется или закончено, можно запускать программу.
        private bool is_skipped;    
        public bool skipped  
        {   
            get { return is_skipped; }
            set { value = is_skipped; }
        }

        // Признак, что обновление доступно и готово к загрузке
        private bool updateAppAvailbe; 
        public bool UpdateAppAvailbe() 
        {
            return updateAppAvailbe;
        }

        public HTTPSUpdaterApp()
        {
            string UserSmartPlusFolder = App.GetRegistryPath();
            if (UserSmartPlusFolder.Length == 0) UserSmartPlusFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            UserSmartPlusFolder += "\\SmartPlus";
            string settingFolder = UserSmartPlusFolder + "\\Настройки";
            TempDir = UserSmartPlusFolder + "\\TEMP";
            if (!Directory.Exists(TempDir))
            {
                try
                {
                    Directory.CreateDirectory(TempDir);
                }
                catch
                {
                    MessageBox.Show("Не удается создать папку: " + TempDir);
                }
            }
            AppSett = new AppSetting(settingFolder);
            AppSett.Load();
            AppSettings = new AppCommonSettings();
            AppSettingSection section = AppSett.GetSectionByType(Constant.APP_SETTINGS_TYPE.COMMON_SETTINGS);
            if (section != null)
            {
                section.Data.Seek(0, SeekOrigin.Begin);
                try
                {
                    BinaryReader br = new BinaryReader(section.Data);
                    if (AppCommonSettings.Version == br.ReadInt32())
                    {
                        AppSettings.Read(br);
                    }
                }
                catch
                {
                }
            }
            ServerName = AppSettings.UpdatebyHttpsServerName;
            ServerUser = AppSettings.UpdatebyHttpsUserName;
            ServerPass = AppSettings.UpdatebyHttpsPassword;
            UpdateByHttps = AppSettings.UpdatebyHttps;
            Initial_Check_Update();
        }

        public HTTPSUpdaterApp(AppCommonSettings appSettings)
        {
            AppSettings = appSettings;
            ServerName = AppSettings.UpdatebyHttpsServerName;
            ServerUser = AppSettings.UpdatebyHttpsUserName;
            ServerPass = AppSettings.UpdatebyHttpsPassword;
            UpdateByHttps = AppSettings.UpdatebyHttps;
            string UserSmartPlusFolder = App.GetRegistryPath();
            if (UserSmartPlusFolder.Length == 0) UserSmartPlusFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            UserSmartPlusFolder += "\\SmartPlus";
            TempDir = UserSmartPlusFolder + "\\TEMP";
            if (!Directory.Exists(TempDir))
            {
                try
                {
                    Directory.CreateDirectory(TempDir);
                }
                catch
                {
                    MessageBox.Show("Не удается создать папку: " +TempDir);
                }
            }
            Initial_Check_Update();
        }

        void Initial_Check_Update()
        {
            is_skipped = true;

            http = new HttpFileReciever();
            AppDir = AppDomain.CurrentDomain.BaseDirectory;
            my_filename = get_exec_filename(); // имя запущенной программы
            up_filename = temp_mask + my_filename; // имя временного файла для новой версии

            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            string[] keys = Environment.GetCommandLineArgs();
            if (UpdateByHttps)
            {
                if (keys.Length < 3)
                {
                    do_check_update();
                }
                else
                {
                    if (keys[1].ToLower() == "/u")
                        do_copy_downloaded_program(keys[2]);
                    do_copy_downloaded_program_files(AppDir, temp_mask);
                    if (keys[1].ToLower() == "/d")
                        do_delete_old_program(keys[2]);
                    try_to_delete_temp_files(AppDir, temp_mask);
                }
            }
            else
            {
                is_skipped = true;
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            switch (Work)
            {
                case AppUpdaterWork.CheckUpdates:
                    do_check_update();
                    break;
                case AppUpdaterWork.HTTPSCopyFromServer:
                    download_AppUpdate();
                    break;
                case AppUpdaterWork.InstallUpdates:
                    install_AppUpdate();
                    break;
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (Work)
            {
                //todo Добавить процесс загрузки файла в форму
                case AppUpdaterWork.InstallUpdates:
                    //if (OnEndItemAppCopy != null) OnEndItemAppCopy((UpdateFileItem)e.UserState);
                    break;
            }
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null) throw new Exception(e.Error.Message);
            if (!e.Cancelled)
            {
                switch (Work)
                {
                    case AppUpdaterWork.HTTPSCopyFromServer:
                        Work = AppUpdaterWork.InstallUpdates;
                        worker.RunWorkerAsync();
                        OnStartSetupApp();
                        break;
                    case AppUpdaterWork.InstallUpdates:
                        Completed();
                        break;
                }
            }
            
        }

        private static string MyVerison()
        {
            string assemblyVersion; 
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                System.Version myVersion;
                myVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                assemblyVersion = string.Format("{0}.{1}.{2}.{3}", myVersion.Major, myVersion.Minor, myVersion.Build, myVersion.Revision);
                return assemblyVersion;
            }
            else
            {
                Assembly assem = Assembly.GetEntryAssembly();
                AssemblyName assemName = assem.GetName();
                Version ver = assemName.Version;
                assemblyVersion = ver.ToString();
                return assemblyVersion;
            }
        }


        private void do_check_update()
        {
            // todo переписать
            string new_version;
            if (GetAvailableServer()) // доступность сети и сервера
                if (http.CheckServerNamePassword("https://" + ServerName + "//" + "SmartPlus.txt", ServerUser, ServerPass))
                {
                    new_version = get_server_version(ServerName, ServerUser, ServerPass); // Получаем номер последней версии
                    string myVersion = MyVerison();
                    string[] myVerArr = myVersion.Split('.');
                    string[] newVerArr = new_version.Split('.');
                    int i = 0;
                    foreach (string ver in newVerArr)
                    {
                        if (Int32.Parse(ver) < Int32.Parse(myVerArr[i]))
                        {
                            new_version = myVersion;
                            break;
                        }
                        i++;
                    }

                    if (myVersion == new_version) // Если обновление не нужно
                    {
                        is_download = false;
                        is_skipped = true; // Пропускаем модуль обновления
                        updateAppAvailbe = false;
                    }
                    else
                    {
                        updateAppAvailbe = true;
                        is_download = true;
                    }
                }
                else
                {
                    is_download = false;
                    is_skipped = true; // Пропускаем модуль обновления
                    updateAppAvailbe = false;
                }
            else
            {
                is_download = false;
                is_skipped = true; // Пропускаем модуль обновления
                updateAppAvailbe = false;
            }
        }

        public void start_updateApp()
        {
            is_skipped = false;
            do_download_update(); // Запускаем скачивание новой версии                
        }

        private void do_download_update()
        {
            is_download = true; // Будем ждать завершения процесса
            is_skipped = false; // Основную форму не нужно запускать

            Work = AppUpdaterWork.HTTPSCopyFromServer;
            worker.RunWorkerAsync(); 
        }

        private void download_AppUpdate()
        {
            try
            {
                CopyFileByHttp("SmartPlusBN.zip");
            }
            catch (Exception ex)
            {
                error(ex.Message + " " + up_filename);
            } 
        }

        private static string[] AppDirFiles()
        {
            string[] allFiles = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.*", SearchOption.AllDirectories);
            return allFiles;
        }

        private void install_AppUpdate()
        {
            DotNetZipExtract(TempDir + "SmartPlusBN.zip", TempDir + "TEMP");
            DirectoryCopyTemp(TempDir + "TEMP", AppDir);
        }

        void CopyFileByHttp(string FileName) 
        {
            if (http == null)
            {
                http = new HttpFileReciever();
            }

            Stream stream = null;
            try
            {
                stream = http.RecieveData("https://" + ServerName + "//" + FileName, ServerUser, ServerPass);
            }
            catch (Exception)
            {
                stream = null;
            }

            if (stream != null)
            {
                string AppFolderFile = TempDir + FileName;
                if (File.Exists(AppFolderFile))
                {
                    if (File.Exists(AppFolderFile))
                    {
                        File.Delete(AppFolderFile);
                    }
                }
               
                FileStream fs = new FileStream(AppFolderFile, FileMode.Create);
                HttpFileReciever.CopyStream(stream, fs);
                stream.Dispose();
                fs.Close();
            }
        }
        
        
        private void Completed()
        {
            // Запускаем второй этап обновления
            run_program(up_filename, "/u \"" + my_filename + "\"");
            OnEndAppDownload();
        }

        private void run_program(string filename, string keys)
        {
            try
            {
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.WorkingDirectory = Application.StartupPath;
                proc.StartInfo.FileName = filename;
                proc.StartInfo.Arguments = keys; // Аргументы командной строки
                proc.Start(); // Запускаем!
            }
            catch (Exception ex)
            {
                error(ex.Message + " " + filename);
            }
        }


        void do_copy_downloaded_program(string filename)
        {
            try_to_delete_file(filename);
            try
            {
                File.Copy(my_filename, filename);
                // Запускаем последний этап обновления
                run_program(filename, "/d \"" + my_filename + "\"");
                is_download = false;
                is_skipped = false;
            }
            catch (Exception ex)
            {
                error(ex.Message + " " + filename);
            }
        }

        void do_delete_old_program(string filename)
        {
            try_to_delete_file(filename);
            is_download = false;
            is_skipped = true;
        }

        private void try_to_delete_file(string filename)
        {
            int loop = 10;
            while (--loop > 0 && File.Exists(filename))
                try 
                {
                     File.Delete(filename);
                } 
                catch 
                {
                    Thread.Sleep(200);
                }
        }

        private string get_server_version(string ServerName, string ServerUser, string ServerPass)
        {
            string server_version;
            HttpFileReciever http = new HttpFileReciever();

            Stream stream = null;
            stream = http.RecieveData("https://" + ServerName + "//" + "SmartPlus.txt", ServerUser, ServerPass);

            if (stream != null)
            {
                string fileName = Path.Combine(TempDir, "SmartPlus.txt");
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
                FileStream fs = new FileStream(fileName, FileMode.Create);

                HttpFileReciever.CopyStream(stream, fs);
                stream.Dispose();
                fs.Close();
                server_version = File.ReadAllText(fileName, System.Text.Encoding.UTF8);
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
            else server_version = null;
            return server_version;
        }

        private void error(string message)
        {
            is_download = false;
            is_skipped = false; // в случае ошибки ничего не запускаем
        }

        private void OpenLink(string sUrl)
        {
            try
            {
                System.Diagnostics.Process.Start(sUrl);
            }
            catch (Exception exc1)
            {
                if (exc1.GetType().ToString() != "System.ComponentModel.Win32Exception")
                {
                    try
                    {
                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo("IExplore.exe", sUrl);
                        System.Diagnostics.Process.Start(startInfo);
                        startInfo = null;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Запустить обозреватель, к сожалению, не удалось.\n\nОткройте страницу ручным способом:\n" + sUrl, "ОШИБКА");
                    }
                }
            }
        }

        private string get_exec_filename()
        {
            string fullname = Application.ExecutablePath; /* переделать велосипеды через path. */
            string[] split = { "\\" };
            string[] parts = fullname.Split(split, StringSplitOptions.None);
            if (parts.Length > 0)
                return parts[parts.Length - 1];
            return "";
        }

        private void DotNetZipExtract(string zipfile, string tempfolder)
        {
            if (Directory.Exists(tempfolder))
            {
                Directory.Delete(tempfolder, true);
                Directory.CreateDirectory(tempfolder);
            }
            
            try
            {
                var options = new ReadOptions { StatusMessageWriter = System.Console.Out }; /* */
                using (ZipFile zip = ZipFile.Read(zipfile, options))
                {
                    zip.ExtractAll(tempfolder);
                }
            }
            catch (System.Exception ex1)
            {
                MessageBox.Show("exception: " + ex1);
            }
        }

        private static void DirectoryCopyTemp(string sourceDirName, string destDirName)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                

                if (File.Exists(temppath))
                {
                    file.CopyTo(Path.Combine(destDirName, temp_mask + file.Name), true);
                }
                else
                {
                    file.CopyTo(temppath, false);
                }
            }

            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopyTemp(subdir.FullName, temppath);
            }
        }

        private void try_to_delete_temp_files(string DirName, string mask)
        {
            DirectoryInfo dir = new DirectoryInfo(DirName);
            DirectoryInfo[] dirs = dir.GetDirectories();
            FileInfo[] files = dir.GetFiles("*"+mask+"*");
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(DirName, file.Name);
                try_to_delete_file(temppath);
                
            }

            foreach (DirectoryInfo subdir in dirs)
            {
                string subpath = Path.Combine(DirName, subdir.Name);
                try_to_delete_temp_files(subpath, mask);
            }
        }

        private void do_copy_downloaded_program_files(string DirName, string mask)
        {
            DirectoryInfo dir = new DirectoryInfo(DirName);
            DirectoryInfo[] dirs = dir.GetDirectories();
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string filename = Path.Combine(DirName, file.Name);
                string tempfile = Path.Combine(DirName, mask + file.Name);

                if (File.Exists(tempfile))
                {
                    try_to_delete_file(filename);
                    try
                    {
                        File.Copy(tempfile, filename);
                    }
                    catch (Exception ex)
                    {
                        error(ex.Message + " " + filename);
                    }
                }
            }
            
            foreach (DirectoryInfo subdir in dirs)
            {
                string subpath = Path.Combine(DirName, subdir.Name);
                do_copy_downloaded_program_files(subpath, mask);
            }
        }

        bool GetAvailableServer()
        {
            bool result = true;
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                try
                {
                    System.Net.NetworkInformation.PingReply pingResult = ping.Send(ServerName, 3000);
                    if (pingResult.Status != System.Net.NetworkInformation.IPStatus.Success)
                    {
                        result = false;
                    }
                }
                catch
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

        bool CheckLoginPassword()
        {
            bool result = true;

            return result;
        }
    }

    public enum AppUpdaterWork
    {
        CheckUpdates,
        HTTPSCopyFromServer,
        InstallUpdates
    }

    public class UpdateAppFielItem
    {
        public bool IsLoaded;
        List<UpdateAppFileItem> UpdateAppItemList;

        public UpdateAppFielItem()
        {
            UpdateAppItemList = new List<UpdateAppFileItem>(10);
            IsLoaded = false;
        }
        public int Count
        {
            get { return UpdateAppItemList.Count; }
        }

        public void Add(UpdateAppFileItem item)
        {
            UpdateAppItemList.Add(item);
        }
        public void Clear()
        {
            UpdateAppItemList.Clear();
        }
        public UpdateAppFileItem this[int index]
        {
            get { return UpdateAppItemList[index]; }
        }
        public UpdateAppFileItem GetItemByFileName(string FileName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].FileName == FileName)
                {
                    return this[i];
                }
            }
            return null;
        }
    }

    public class UpdateAppFileItem
    {
        public string Type;
        public string FileName;
        public DateTime CreationTime;
        public DateTime AppDirCreationTime;
        public string CreationUser;
        public bool IsLoaded;
        public int FileSize;
    }
}
