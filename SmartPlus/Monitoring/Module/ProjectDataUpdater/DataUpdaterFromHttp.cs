﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Threading;
using RDF;

namespace SmartPlus
{
    public sealed partial class DataUpdater
    {
        byte[] buff = null;

        public static List<string> GetProjectListByHttp(string ServerName, string ServerUser, string ServerPass, string TempPath)
        {
	        List<string> projects = new List<string>();
	        HttpFileReciever http = new HttpFileReciever();

	        Stream stream = null;
	        stream = http.RecieveData("https://" + ServerName + "//" + "Projects.txt", ServerUser, ServerPass);

	        if(stream != null)
	        {
                string fileName = Path.Combine(TempPath, "Projects.txt");
		        if(File.Exists(fileName))
		        {
			        File.Delete(fileName);
		        }
		        FileStream fs = new FileStream(fileName, FileMode.Create);

		        HttpFileReciever.CopyStream(stream, fs);
		        stream.Dispose();
		        fs.Close();
		        projects = new List<string>(File.ReadAllLines(fileName, System.Text.Encoding.UTF8));
		        if(File.Exists(fileName))
		        {
			        File.Delete(fileName);
		        }
	        }
            return projects;
        }
        void CopyFromServerFromHttpToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            UpdateSuccesDownloaded = false;
            int progress=0;

            if (!Directory.Exists(Path.Combine(LocalPath, "cache")))
            {
                Directory.CreateDirectory(Path.Combine(LocalPath, "cache"));
            }

            for (int i = 0; i < UpdateOilFieldList.Count; ++i)
            {
               // bool chess_delta = false;
               // bool mer_delta = false;

                UpdateOilFieldItem of = UpdateOilFieldList[i];
                if (!of.IsLoaded)
                {
                    if ((of.OilField != "cache") && (of.OilField != "Dictionaries") && (!Directory.Exists(LocalPath + "\\" + "cache" + "\\" + of.OilField)))
                    {
                        Directory.CreateDirectory(LocalPath + "\\" + "cache" + "\\" + of.OilField);
                    }
                    else if ((of.OilField == "Dictionaries") && (!Directory.Exists(Path.Combine(LocalPath, of.OilField))))
                    {
                        Directory.CreateDirectory(Path.Combine(LocalPath, of.OilField));
                    }

                    for (int j = 0; j < of.Count; ++j)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        if (!of[j].IsLoaded)
                        {
                            if (of.OilField != "cache" && of.OilField != "Dictionaries")
                            {
                                
                                CopyFileByHttp(of[j].FileName, of.OilField, of[j].FileName, of[j].CreationTime);
                                /*
                                if (!chess_delta && of[j].FileName.StartsWith("chess_delta"))
                                {
                                    chess_delta = true;
                                }
                                if (!mer_delta && of[j].FileName.StartsWith("mer_delta"))
                                {
                                    mer_delta = true;
                                }
                                 */
                            }
                            else if (of.OilField == "cache")
                            {
                                CopyFileByHttp(of[j].FileName, "", of[j].FileName, of[j].CreationTime);
                            }
                            else
                            {
                                CopyDictByHttp(of[j].FileName, of[j].CreationTime);
                            }
                            of[j].IsLoaded = true;
                            
                        }
                        progress++;
                        if (of[j] != null) worker.ReportProgress(progress, of[j]);
                    }
                    /*if (chess_delta)
                    {
                        LoadChessFastDeltaUpdates(worker, e, of.OilField, 365 * 2);
                        LoadChessInjFastDeltaUpdates(worker, e, of.OilField, 365 * 2);
                    }
                    if (mer_delta)
                    {
                        LoadMerFastDeltaUpdates(worker, e, of.OilField);
                    }*/
                    of.IsLoaded = true;
                }
             /* if (ServerOilfieldCount > 0 && UpdateOilFieldList.Count <= ServerOilfieldCount)
                {
                    progress = (int)((ServerOilfieldCount - UpdateOilFieldList.Count + i) * 100f / ServerOilfieldCount);
                }
                else
                {
                    progress = (int)(i * 100f / UpdateOilFieldList.Count);
                }
                */
                
            }

            CreateProjectUpdateFile(LocalPath);
            DateTime dtServ = http.GetLastModifiedDate("https://" + ServerName + "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//prjUpd.bin", ServerUser, ServerPass);
            if ((dtServ.Year > 1) && File.Exists(Path.Combine(LocalPath, "prjUpd.bin")))
            {
                File.SetCreationTime(Path.Combine(LocalPath, "prjUpd.bin"), dtServ);
                File.SetLastWriteTime(Path.Combine(LocalPath, "prjUpd.bin"), dtServ);
            }
            DirectoryInfo dir = new DirectoryInfo(LocalPath);
            string projFile = dir.Name + ".txt";
            UpdateSuccesDownloaded = true;
            try
            {
                CopyFileByHttp(HttpFileReciever.CyrillicUrlEncode(projFile), projFile, DateTime.Now);
                if (!File.Exists(Path.Combine(LocalPath, "prjUpd.bin")))
                {
                    CopyFileByHttp(HttpFileReciever.CyrillicUrlEncode("prjUpd.bin"), "prjUpd.bin", dtServ);
                }
                
            }
            catch
            {
                UpdateSuccesDownloaded = false;
            }
        }
        //todo Внимание, повторяющийся код. Исключить!
        void CopyFromServerFromHttpToPreLoad(BackgroundWorker worker, DoWorkEventArgs e)
        {
            UpdateSuccesDownloaded = false;
            int progress = 0;
            string PreLoadPath = Path.Combine(LocalPath,"cache_update");
            if (!Directory.Exists(PreLoadPath)) Directory.CreateDirectory(PreLoadPath);
            if (!Directory.Exists(Path.Combine(PreLoadPath, "cache"))) Directory.CreateDirectory(Path.Combine(PreLoadPath,"cache"));

            for (int i = 0; i < UpdateOilFieldList.Count; ++i)
            {
                UpdateFileItem item;
                UpdateOilFieldItem of = UpdateOilFieldList[i];
                if (!of.IsLoaded && !of.IsPreLoaded)
                {
                    if ((of.OilField != "cache") && (of.OilField != "Dictionaries") && (!Directory.Exists(PreLoadPath + "\\" + "cache" + "\\" + of.OilField)))
                    {
                        Directory.CreateDirectory(PreLoadPath + "\\" + "cache" + "\\" + of.OilField);
                    }
                    else if ((of.OilField == "Dictionaries") && (!Directory.Exists(Path.Combine(PreLoadPath, of.OilField))))
                    {
                        Directory.CreateDirectory(Path.Combine(PreLoadPath, of.OilField));
                    }

                    for (int j = 0; j < of.Count; ++j)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        item = UpdateOilFieldList[i][j];

                        if (!of[j].IsPreLoaded && !item.IsPreLoaded)
                        {
                            if (of.OilField != "cache" && of.OilField != "Dictionaries")
                            {

                                CopyFileByHttpToPreLoad(of[j].FileName, of.OilField, of[j].FileName, of[j].CreationTime);
                            }
                            else if (of.OilField == "cache")
                            {
                                CopyFileByHttpToPreLoad(of[j].FileName, "", of[j].FileName, of[j].CreationTime);
                            }
                            else
                            {
                                CopyDictByHttpToPreLoad(of[j].FileName, of[j].CreationTime);
                            }
                            //of[j].IsPreLoaded = true;
                            item.PreLoadCreationTime = item.CreationTime;
                            item.IsPreLoaded = true;
                            
                        }
                        if (j == UpdateOilFieldList[i].Count - 1)
                        {
                            UpdateOilFieldList[i].IsPreLoaded = true;
                        }
                        progress++;
                        if (of[j] != null) worker.ReportProgress(progress, of[j]);
                    }
                   
                    of.IsPreLoaded = true;
                }
                /* if (ServerOilfieldCount > 0 && UpdateOilFieldList.Count <= ServerOilfieldCount)
                   {
                       progress = (int)((ServerOilfieldCount - UpdateOilFieldList.Count + i) * 100f / ServerOilfieldCount);
                   }
                   else
                   {
                       progress = (int)(i * 100f / UpdateOilFieldList.Count);
                   }
                   */

            }

            CreateProjectUpdateFile(PreLoadPath);
            DateTime dtServ = http.GetLastModifiedDate("https://" + ServerName + "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//prjUpd.bin", ServerUser, ServerPass);
            if ((dtServ.Year > 1) && File.Exists(Path.Combine(PreLoadPath, "prjUpd.bin")))
            {
                File.SetCreationTime(Path.Combine(PreLoadPath, "prjUpd.bin"), dtServ);
                File.SetLastWriteTime(Path.Combine(PreLoadPath, "prjUpd.bin"), dtServ);
            }
            DirectoryInfo dir = new DirectoryInfo(project.path);
            string projFile = dir.Name + ".txt";
            UpdateSuccesDownloaded = true;
            try
            {
                CopyFileByHttpToPreLoad(HttpFileReciever.CyrillicUrlEncode(projFile), projFile, DateTime.Now);
                if (!File.Exists(Path.Combine(PreLoadPath, "prjUpd.bin")))
                {
                    CopyFileByHttpToPreLoad(HttpFileReciever.CyrillicUrlEncode("prjUpd.bin"), "prjUpd.bin", dtServ);
                }

            }
            catch
            {
                UpdateSuccesDownloaded = false;
            }
        }

#region COPY BY HTTP
        void CopyFileByHttp(string FileName, string DestFileName, DateTime CreationTime)
        {
            if (http == null)
            {
                http = new HttpFileReciever();
            }

            Stream stream = null;
            try
            {
                stream = http.RecieveData("https://" + ServerName + "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//" + FileName, ServerUser, ServerPass);
            }
            catch (Exception)
            {
                stream = null;
            }

            if (stream != null)
            {
                try
                {
                    FileStream fs = new FileStream(LocalPath + "\\" +  DestFileName + "_1", FileMode.Create);
                    HttpFileReciever.CopyStream(stream, fs);
                    stream.Dispose();
                    fs.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                string cacheFile = LocalPath + "\\" + DestFileName;
                if (File.Exists(cacheFile + "_1"))
                {
                    if (File.Exists(cacheFile))
                    {
                        File.Delete(cacheFile);
                    }

                    File.Move(cacheFile + "_1", cacheFile);
                    File.SetCreationTime(cacheFile, CreationTime);
                    File.SetLastWriteTime(cacheFile, CreationTime);
                }
            }
        }
        void CopyFileByHttp(string FileName, string OilfieldName, string DestFileName, DateTime CreationTime)
        {
            if (http == null)
            {
                http = new HttpFileReciever();
            }

            Stream stream = null;
            try
            {
                stream = http.RecieveData("https://" + ServerName + "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//cache//" + HttpFileReciever.CyrillicUrlEncode(OilfieldName) + "//" + FileName, ServerUser, ServerPass);
            }
            catch (Exception)
            {
                stream = null;
            }

            if (stream != null)
            {
                string cacheFile = LocalPath + "\\" + "cache" + "\\" + OilfieldName + "\\" + DestFileName;
                FileStream fs = new FileStream(cacheFile, FileMode.Create);
                HttpFileReciever.CopyStream(stream, fs);
                stream.Dispose();

                fs.Close();

                if (File.Exists(cacheFile + "_1"))
                {
                    if (File.Exists(cacheFile))
                    {
                        File.Delete(cacheFile);
                    }

                    File.Move(cacheFile + "_1", cacheFile);
                    File.SetCreationTime(cacheFile, CreationTime);
                    File.SetLastWriteTime(cacheFile, CreationTime);
                }
            }
        }
        void CopyDictByHttp(string FileName, DateTime CreationTime)
        {
            if (http == null)
            {
                http = new HttpFileReciever();
            }

            string[] dirs = FileName.Split('\\');
            string cacheFile = LocalPath + "\\" + "Dictionaries";
            string servDirs = "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//Dictionaries";

            for (int i = 0; i < dirs.Length; ++i)
            {
                cacheFile = Path.Combine(cacheFile, dirs[i]);
                servDirs += "//" + dirs[i];
                if (!Directory.Exists(cacheFile) && (i < dirs.Length - 1)) Directory.CreateDirectory(cacheFile);
            }

            FileStream fs = new FileStream(cacheFile + "_1", FileMode.Create);
            Stream stream = http.RecieveData("https://" + ServerName + servDirs, ServerUser, ServerPass);
            HttpFileReciever.CopyStream(stream, fs);
            stream.Dispose();
            fs.Close();

            if (File.Exists(cacheFile + "_1"))
            {
                if (File.Exists(cacheFile)) File.Delete(cacheFile);
                File.Move(cacheFile + "_1", cacheFile);
                File.SetCreationTime(cacheFile, CreationTime);
                File.SetLastWriteTime(cacheFile, CreationTime);
            }
        }
#endregion
        //todo Внимание повторяющийся код. Исключить! совместить с COPY BY HTTP
#region COPY BY HTTP TO PRELOAD
        void CopyFileByHttpToPreLoad(string FileName, string DestFileName, DateTime CreationTime)
        {
            if (http == null)
            {
                http = new HttpFileReciever();
            }

            Stream stream = null;
            try
            {
                stream = http.RecieveData("https://" + ServerName + "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//" + FileName, ServerUser, ServerPass);
            }
            catch (Exception)
            {
                stream = null;
            }

            if (stream != null)
            {
                try
                {
                    FileStream fs = new FileStream(LocalPath + "\\cache_update\\" + DestFileName + "_1", FileMode.Create);
                    HttpFileReciever.CopyStream(stream, fs);
                    stream.Dispose();
                    fs.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                string cacheFile = LocalPath + "\\cache_update\\" + DestFileName;
                if (File.Exists(cacheFile + "_1"))
                {
                    if (File.Exists(cacheFile))
                    {
                        File.Delete(cacheFile);
                    }

                    File.Move(cacheFile + "_1", cacheFile);
                    File.SetCreationTime(cacheFile, CreationTime);
                    File.SetLastWriteTime(cacheFile, CreationTime);
                }
            }
        }
        void CopyFileByHttpToPreLoad(string FileName, string OilfieldName, string DestFileName, DateTime CreationTime)
        {
            if (http == null)
            {
                http = new HttpFileReciever();
            }

            Stream stream = null;
            try
            {
                stream = http.RecieveData("https://" + ServerName + "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//cache//" + HttpFileReciever.CyrillicUrlEncode(OilfieldName) + "//" + FileName, ServerUser, ServerPass);
            }
            catch (Exception)
            {
                stream = null;
            }

            if (stream != null)
            {
                string cacheFile = LocalPath + "\\" + "cache_update\\cache" + "\\" + OilfieldName + "\\" + DestFileName;
                FileStream fs = new FileStream(cacheFile, FileMode.Create);
                HttpFileReciever.CopyStream(stream, fs);
                stream.Dispose();

                fs.Close();

                if (File.Exists(cacheFile + "_1"))
                {
                    if (File.Exists(cacheFile))
                    {
                        File.Delete(cacheFile);
                    }

                    File.Move(cacheFile + "_1", cacheFile);
                    File.SetCreationTime(cacheFile, CreationTime);
                    File.SetLastWriteTime(cacheFile, CreationTime);
                }
            }
        }
        void CopyDictByHttpToPreLoad(string FileName, DateTime CreationTime)
        {
            if (http == null)
            {
                http = new HttpFileReciever();
            }

            string[] dirs = FileName.Split('\\');
            string cacheFile = LocalPath + "\\" + "cache_update" + "\\" + "Dictionaries";
            string servDirs = "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//Dictionaries";

            for (int i = 0; i < dirs.Length; ++i)
            {
                cacheFile = Path.Combine(cacheFile, dirs[i]);
                servDirs += "//" + dirs[i];
                if (!Directory.Exists(cacheFile) && (i < dirs.Length - 1)) Directory.CreateDirectory(cacheFile);
            }

            FileStream fs = new FileStream(cacheFile + "_1", FileMode.Create);
            Stream stream = http.RecieveData("https://" + ServerName + servDirs, ServerUser, ServerPass);
            HttpFileReciever.CopyStream(stream, fs);
            stream.Dispose();
            fs.Close();

            if (File.Exists(cacheFile + "_1"))
            {
                if (File.Exists(cacheFile)) File.Delete(cacheFile);
                File.Move(cacheFile + "_1", cacheFile);
                File.SetCreationTime(cacheFile, CreationTime);
                File.SetLastWriteTime(cacheFile, CreationTime);
            }
        }
#endregion
        internal struct MerItemEx
        {
            /// <summary>
            /// Код агента закачки
            /// </summary>
            public int AgentInjId;
            /// <summary>
            /// Закачка, м3
            /// </summary>
            public double Injection;
            /// <summary>
            /// Природный газ, м3
            /// </summary>
            public double NaturalGas;
            /// <summary>
            /// Газоконденсат, т
            /// </summary>
            public double GasCondensate;
            /// <summary>
            /// Код агента закачки
            /// </summary>
            public double OtherFluidV;
            /// <summary>
            /// Газ из шапки, м3
            /// </summary>
            public double CapGas;
            /// <summary>
            /// Нулевой item
            /// </summary>
            /// 
            public static MerItemEx Empty
            {
                get { return new MerItemEx(); }
            }
        }

        public void LoadMerFastDeltaUpdates(BackgroundWorker worker, DoWorkEventArgs e, string OilfieldName)
        {
            int i, j, wellIndex;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка delta обновлений МЭР";
            userState.Element = OilfieldName;

            DirectoryInfo dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "\\" + "cache" + "\\" + OilfieldName);
            if (!File.Exists(Path.Combine(dir.FullName, "mer_fast.bin"))) return;
            FileInfo[] deltaFiles = dir.GetFiles("mer_delta_*.bin");
            if (deltaFiles.Length == 0) return;

            int[] deltaCounts = new int[deltaFiles.Length];
            FileStream fs;
            FileStream cache = new FileStream(Path.Combine(dir.FullName, "mer_fast_cache.bin"), FileMode.Create);
            FileStream oldChess = new FileStream(Path.Combine(dir.FullName, "mer_fast.bin"), FileMode.Open);
            BinaryReader[] deltaBr = new BinaryReader[deltaFiles.Length];
            BinaryWriter bw = new BinaryWriter(cache);

            try
            {
                List<int[]> WellIndices = new List<int[]>();
                var br = new BinaryReader(oldChess);
                DateTime minDate, maxDate, dt;
                minDate = RDF.RdfSystem.UnpackDateTimeI(br.ReadInt32());
                maxDate = RDF.RdfSystem.UnpackDateTimeI(br.ReadInt32());

                // read well indices
                int WellsCount = br.ReadInt32();

                for (i = 0; i < WellsCount; ++i)
                {
                    var WellIndexItem = new int[2];
                    WellIndexItem[0] = br.ReadInt32();   // well index
                    WellIndexItem[1] = br.ReadInt32();   // offset
                    WellIndices.Add(WellIndexItem);
                }
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    fs = new FileStream(deltaFiles[di].FullName, FileMode.Open);
                    deltaBr[di] = new BinaryReader(fs);
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (minDate > dt) minDate = dt;
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (maxDate < dt) maxDate = dt;
                    deltaCounts[di] = deltaBr[di].ReadInt32();
                }
                // min - max dates
                byte Month;
                int count, countEx, wellPosition;
                int year, yearsCount, minYear;
                int lastPosition;
                List<int> YearsPosition = new List<int>();
                bw.Write(RDF.RdfSystem.PackDateTime(minDate));
                bw.Write(RDF.RdfSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(-1);                   // well offset
                }
                minDate = DateTime.MaxValue;
                RDF.Objects.MerItem item = new RDF.Objects.MerItem();
                List<List<RDF.Objects.MerItem>> merItems = new List<List<RDF.Objects.MerItem>>();
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    wellPosition = (int)cache.Position;
                    minYear = 9999;
                    yearsCount = 0;
                    YearsPosition.Clear();
                    merItems.Clear();

                    if (WellIndices[wi][1] != -1)
                    {
                        br.BaseStream.Seek(WellIndices[wi][1], SeekOrigin.Begin);
                        yearsCount = br.ReadInt32();
                        minYear = br.ReadInt32();
                        for (i = 0; i < yearsCount; i++)
                        {
                            YearsPosition.Add(br.ReadInt32());
                            merItems.Add(new List<RDF.Objects.MerItem>());
                        }
                        for (i = 0; i < yearsCount; i++)
                        {
                            if (YearsPosition[i] == -1) continue;
                            br.BaseStream.Seek(YearsPosition[i], SeekOrigin.Begin);
                            count = br.ReadInt32();
                            for (j = 0; j < count; j++)
                            {
                                Month = br.ReadByte();
                                item.Date = new DateTime(minYear + i, Month, 1);
                                item.PlastId = br.ReadInt16();
                                item.CharWorkId = br.ReadInt16();
                                item.StateId = br.ReadInt16();
                                item.MethodId = br.ReadInt16();
                                item.WorkTime = br.ReadInt16();
                                item.CollTime = br.ReadInt16();
                                item.Oil = br.ReadSingle();
                                item.Wat = br.ReadSingle();
                                item.Gas = br.ReadSingle();
                                item.StayReasonId = br.ReadInt16();
                                item.StayTime = br.ReadInt16();
                                merItems[i].Add(item);
                            }
                        }
                    }
                    for (int di = 0; di < deltaBr.Length; di++)
                    {
                        wellIndex = deltaBr[di].ReadInt32();
                        if (wellIndex == WellIndices[wi][0])
                        {
                            count = deltaBr[di].ReadInt32();
                            countEx = deltaBr[di].ReadInt32();

                            RDF.Objects.MerItem[] items = new RDF.Objects.MerItem[count];
                            for (i = 0; i < count; i++)
                            {
                                items[i].Date = RDF.RdfSystem.UnpackDateTimeI(deltaBr[di].ReadInt32());
                                items[i].PlastId = deltaBr[di].ReadInt32();
                                items[i].CharWorkId = deltaBr[di].ReadInt32();
                                items[i].StateId = deltaBr[di].ReadInt32();
                                items[i].MethodId = deltaBr[di].ReadInt32();
                                items[i].WorkTime = deltaBr[di].ReadInt32();
                                items[i].CollTime = deltaBr[di].ReadInt32();
                                items[i].Oil = (float)deltaBr[di].ReadDouble();
                                items[i].Wat = (float)deltaBr[di].ReadDouble();
                                deltaBr[di].BaseStream.Seek(16, SeekOrigin.Current);
                                //								items[i].OilV = deltaBr[di].ReadDouble();
                                //								items[i].WatV = deltaBr[di].ReadDouble();
                                items[i].Gas = (float)deltaBr[di].ReadDouble();
                                items[i].StayReasonId = deltaBr[di].ReadInt32();
                                items[i].StayTime = deltaBr[di].ReadInt32();
                                deltaBr[di].BaseStream.Seek(4, SeekOrigin.Current);
                                //items[i].AgentProdId = deltaBr[di].ReadInt32();
                            }
                            MerItemEx[] itemsEx = null;
                            if (countEx > 0)
                            {
                                itemsEx = new MerItemEx[countEx];
                                for (i = 0; i < countEx; i++)
                                {
                                    itemsEx[i] = new MerItemEx();
                                    itemsEx[i].AgentInjId = deltaBr[di].ReadInt32();
                                    itemsEx[i].Injection = deltaBr[di].ReadDouble();
                                    itemsEx[i].NaturalGas = deltaBr[di].ReadDouble();
                                    itemsEx[i].GasCondensate = deltaBr[di].ReadDouble();
                                    itemsEx[i].OtherFluidV = deltaBr[di].ReadDouble();
                                    itemsEx[i].CapGas = deltaBr[di].ReadDouble();
                                }
                            }
                            if (items.Length > 0)
                            {
                                if (minYear == 9999) minYear = items[0].Date.Year;
                                for (i = 0; i < items.Length; i++)
                                {
                                    year = items[i].Date.Year - minYear;
                                    while (year >= merItems.Count)
                                    {
                                        merItems.Add(new List<RDF.Objects.MerItem>());
                                        yearsCount++;
                                    }
                                    if (items[i].CharWorkId == 20 && itemsEx.Length > 0)
                                    {
                                        items[i].Wat = (float)itemsEx[i].Injection;
                                    }
                                    merItems[year].Add(items[i]);
                                }
                            }
                        }
                    }

                    if (yearsCount > 0)
                    {
                        bw.Write(yearsCount);
                        bw.Write(minYear);
                        lastPosition = (int)bw.BaseStream.Position + merItems.Count * sizeof(int);
                        for (i = 0; i < merItems.Count; i++)
                        {
                            bw.Write(lastPosition);
                            lastPosition += sizeof(int) + merItems[i].Count * (sizeof(byte) + 8 * sizeof(short) + 3 * sizeof(float));
                        }
                        for (i = 0; i < merItems.Count; i++)
                        {
                            bw.Write(merItems[i].Count);
                            if (merItems[i].Count > 0 && merItems[i][0].Date < minDate)
                            {
                                minDate = merItems[i][0].Date;
                            }
                            for (j = 0; j < merItems[i].Count; j++)
                            {
                                Month = (byte)merItems[i][j].Date.Month;
                                bw.Write(Month);
                                bw.Write((short)merItems[i][j].PlastId);
                                bw.Write((short)merItems[i][j].CharWorkId);
                                bw.Write((short)merItems[i][j].StateId);
                                bw.Write((short)merItems[i][j].MethodId);
                                bw.Write((short)merItems[i][j].WorkTime);
                                bw.Write((short)merItems[i][j].CollTime);
                                bw.Write((float)merItems[i][j].Oil);
                                bw.Write((float)merItems[i][j].Wat);
                                bw.Write((float)merItems[i][j].Gas);
                                bw.Write((short)merItems[i][j].StayReasonId);
                                bw.Write((short)merItems[i][j].StayTime);
                            }
                        }
                    }
                    lastPosition = (int)cache.Position;
                    WellIndices[wi][1] = (lastPosition != wellPosition) ? wellPosition : -1;
                }
                if (minDate == DateTime.MaxValue) minDate = DateTime.MinValue;
                cache.Seek(0, SeekOrigin.Begin);
                bw.Write(RDF.RdfSystem.PackDateTime(minDate));
                bw.Write(RDF.RdfSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(WellIndices[wi][1]);  // well offset
                }
                oldChess.Close();
                oldChess = null;
                cache.Flush();
                cache.Close();
                cache = null;
                if (File.Exists(Path.Combine(dir.FullName, "mer_fast_cache.bin")))
                {
                    if (File.Exists(Path.Combine(dir.FullName, "mer_fast.bin")))
                    {
                        File.Delete(Path.Combine(dir.FullName, "mer_fast.bin"));
                    }
                    File.Move(Path.Combine(dir.FullName, "mer_fast_cache.bin"), Path.Combine(dir.FullName, "mer_fast.bin"));
                }
            }
            catch
            {
                if (cache != null)
                {
                    cache.Close();
                    cache = null;
                }
            }
            finally
            {
                if (oldChess != null) oldChess.Close();
                if (cache != null) cache.Close();
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    if (deltaBr[di] != null)
                    {
                        deltaBr[di].Close();
                        if (deltaFiles[di].Exists) deltaFiles[di].Delete();
                    }
                }
                string name = Path.Combine(dir.FullName, "mer_fast_cache.bin");
                if (File.Exists(name)) File.Delete(name);
            }
        }
        public void LoadChessFastDeltaUpdates(BackgroundWorker worker, DoWorkEventArgs e, string OilfieldName, int CutOffDays)
        {
            int i, j, wellIndex;
            int chessInjLen = 14;

            DirectoryInfo dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "\\" + "cache" + "\\" + OilfieldName);
            if (!File.Exists(Path.Combine(dir.FullName, "chess_fast.bin"))) return;
            FileInfo[] deltaFiles = dir.GetFiles("chess_delta_*.bin");
            if (deltaFiles.Length == 0) return;

            int[] deltaCounts = new int[deltaFiles.Length];
            FileStream fs;
            FileStream cache = new FileStream(Path.Combine(dir.FullName, "chess_fast_cache.bin"), FileMode.Create);
            FileStream oldChess = new FileStream(Path.Combine(dir.FullName, "chess_fast.bin"), FileMode.Open);
            BinaryReader[] deltaBr = new BinaryReader[deltaFiles.Length];
            BinaryWriter bw = new BinaryWriter(cache);

            try
            {
                WellChess chess = null;
                List<int[]> WellIndices = new List<int[]>();
                var br = new BinaryReader(oldChess);
                DateTime minDate, maxDate, dt;
                minDate = RDF.RdfSystem.UnpackDateTimeI(br.ReadInt32());
                maxDate = RDF.RdfSystem.UnpackDateTimeI(br.ReadInt32());

                // read well indices
                int WellsCount = br.ReadInt32();

                for (i = 0; i < WellsCount; ++i)
                {
                    var WellIndexItem = new int[2];
                    WellIndexItem[0] = br.ReadInt32();   // well index
                    WellIndexItem[1] = br.ReadInt32();   // offset
                    WellIndices.Add(WellIndexItem);
                }
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    fs = new FileStream(deltaFiles[di].FullName, FileMode.Open);
                    deltaBr[di] = new BinaryReader(fs);
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (minDate > dt) minDate = dt;
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (maxDate < dt) maxDate = dt;
                    deltaCounts[di] = deltaBr[di].ReadInt32();
                }
                // min - max dates
                byte Day, Month;
                int count, wellPosition;
                int year, yearsCount, minYear;
                int lastPosition;
                List<int> YearsPosition = new List<int>();
                bw.Write(RDF.RdfSystem.PackDateTime(minDate));
                bw.Write(RDF.RdfSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(-1);                   // well offset
                }
                minDate = DateTime.MaxValue;
                ChessItem item = new ChessItem();
                List<List<ChessItem>> chessItems = new List<List<ChessItem>>();
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    wellPosition = (int)cache.Position;
                    minYear = 9999;
                    yearsCount = 0;
                    YearsPosition.Clear();
                    chessItems.Clear();

                    if (WellIndices[wi][1] != -1)
                    {
                        br.BaseStream.Seek(WellIndices[wi][1], SeekOrigin.Begin);
                        yearsCount = br.ReadInt32();
                        minYear = br.ReadInt32();
                        for (i = 0; i < yearsCount; i++)
                        {
                            YearsPosition.Add(br.ReadInt32());
                            chessItems.Add(new List<ChessItem>());
                        }
                        for (i = 0; i < yearsCount; i++)
                        {
                            if (YearsPosition[i] == -1) continue;
                            br.BaseStream.Seek(YearsPosition[i], SeekOrigin.Begin);
                            count = br.ReadInt32();
                            for (j = 0; j < count; j++)
                            {
                                Month = br.ReadByte();
                                Day = br.ReadByte();
                                item.Date = new DateTime(minYear + i, Month, Day);
                                item.Qliq = br.ReadSingle();
                                item.Qoil = br.ReadSingle();
                                item.QliqV = br.ReadSingle();
                                item.QoilV = br.ReadSingle();
                                item.StayTime = br.ReadByte();
                                item.Hdyn = br.ReadSingle();
                                chessItems[i].Add(item);
                            }
                        }
                    }
                    for (int di = 0; di < deltaBr.Length; di++)
                    {
                        chess = new WellChess();
                        int version = deltaBr[di].ReadInt32();
                        wellIndex = deltaBr[di].ReadInt32();
                        if (wellIndex == WellIndices[wi][0])
                        {
                            if (chess.ReadFromCache(deltaBr[di], version) && chess.Count > 0)
                            {
                                if (minYear == 9999) minYear = chess[0].Date.Year;
                                for (i = 0; i < chess.Count; i++)
                                {
                                    year = chess[i].Date.Year - minYear;
                                    while (year >= chessItems.Count)
                                    {
                                        chessItems.Add(new List<ChessItem>());
                                        yearsCount++;
                                    }
                                    chessItems[year].Add(chess[i]);
                                }
                            }
                            count = deltaBr[di].ReadInt32(); // skip chess inj
                            if (count > 0) deltaBr[di].BaseStream.Seek(count * chessInjLen, SeekOrigin.Current);
                        }
                    }

                    for (i = 0; i < chessItems.Count; i++)
                    {
                        if (chessItems[i].Count > 0 && chessItems[i][chessItems[i].Count - 1].Date < maxDate.AddDays(-CutOffDays))
                        {
                            chessItems[i].Clear();
                        }
                        else
                        {
                            for (j = 0; j < chessItems[i].Count; j++)
                            {
                                if (chessItems[i][j].Date > maxDate.AddDays(-CutOffDays))
                                {
                                    break;
                                }
                                if (chessItems[i][j].Date < maxDate.AddDays(-CutOffDays))
                                {
                                    chessItems[i].RemoveAt(j);
                                    j--;
                                }
                            }
                        }

                        if (i == 0 && chessItems[i].Count == 0)
                        {
                            chessItems.RemoveAt(i);
                            yearsCount--;
                            minYear++;
                            i--;
                        }
                    }

                    if (yearsCount > 0)
                    {
                        bw.Write(yearsCount);
                        bw.Write(minYear);
                        lastPosition = (int)bw.BaseStream.Position + chessItems.Count * sizeof(int);
                        for (i = 0; i < chessItems.Count; i++)
                        {
                            bw.Write(lastPosition);
                            lastPosition += sizeof(int) + chessItems[i].Count * (3 * sizeof(byte) + 5 * sizeof(float));
                        }
                        for (i = 0; i < chessItems.Count; i++)
                        {
                            bw.Write(chessItems[i].Count);
                            if (chessItems[i].Count > 0 && chessItems[i][0].Date < minDate)
                            {
                                minDate = chessItems[i][0].Date;
                            }
                            for (j = 0; j < chessItems[i].Count; j++)
                            {
                                Day = (byte)chessItems[i][j].Date.Day;
                                Month = (byte)chessItems[i][j].Date.Month;
                                bw.Write(Month);
                                bw.Write(Day);
                                bw.Write(chessItems[i][j].Qliq);
                                bw.Write(chessItems[i][j].Qoil);
                                bw.Write(chessItems[i][j].QliqV);
                                bw.Write(chessItems[i][j].QoilV);
                                bw.Write((byte)chessItems[i][j].StayTime);
                                bw.Write(chessItems[i][j].Hdyn);
                            }
                        }
                    }
                    lastPosition = (int)cache.Position;
                    WellIndices[wi][1] = (lastPosition != wellPosition) ? wellPosition : -1;
                }
                if (minDate == DateTime.MaxValue) minDate = maxDate.AddDays(-CutOffDays);
                cache.Seek(0, SeekOrigin.Begin);
                bw.Write(RDF.RdfSystem.PackDateTime(minDate));
                bw.Write(RDF.RdfSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(WellIndices[wi][1]);  // well offset
                }
                oldChess.Close();
                oldChess = null;
                cache.Flush();
                cache.Close();
                cache = null;
                if (File.Exists(Path.Combine(dir.FullName, "chess_fast_cache.bin")))
                {
                    if (File.Exists(Path.Combine(dir.FullName, "chess_fast.bin")))
                    {
                        File.Delete(Path.Combine(dir.FullName, "chess_fast.bin"));
                    }
                    File.Move(Path.Combine(dir.FullName, "chess_fast_cache.bin"), Path.Combine(dir.FullName, "chess_fast.bin"));
                    if (deltaFiles.Length > 0)
                    {
                        DateTime servDate = deltaFiles[deltaFiles.Length - 1].CreationTime;
                        File.SetCreationTime(Path.Combine(dir.FullName, "chess_fast.bin"), servDate);
                        File.SetLastWriteTime(Path.Combine(dir.FullName, "chess_fast.bin"), servDate);
                    }
                }
            }
            catch
            {
                if (cache != null)
                {
                    cache.Close();
                    cache = null;
                }
            }
            finally
            {
                if (oldChess != null) oldChess.Close();
                if (cache != null) cache.Close();
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    if (deltaBr[di] != null)
                    {
                        deltaBr[di].Close();
                        if (deltaFiles[di].Exists) deltaFiles[di].Delete();
                    }
                }
                string name = Path.Combine(dir.FullName, "chess_fast_cache.bin");
                if (File.Exists(name)) File.Delete(name);
            }
        }
        public void LoadChessInjFastDeltaUpdates(BackgroundWorker worker, DoWorkEventArgs e, string OilfieldName, int CutOffDays)
        {
            int i, j, wellIndex;
            int chessLen = 25;

            DirectoryInfo dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "\\" + "cache" + "\\" + OilfieldName);
            if (!File.Exists(Path.Combine(dir.FullName, "chessinj_fast.bin"))) return;
            FileInfo[] deltaFiles = dir.GetFiles("chess_delta_*.bin");
            if (deltaFiles.Length == 0) return;

            int[] deltaCounts = new int[deltaFiles.Length];
            FileStream fs;
            FileStream cache = new FileStream(Path.Combine(dir.FullName, "chessinj_fast_cache.bin"), FileMode.Create);
            FileStream oldChess = new FileStream(Path.Combine(dir.FullName, "chessinj_fast.bin"), FileMode.Open);
            BinaryReader[] deltaBr = new BinaryReader[deltaFiles.Length];
            BinaryWriter bw = new BinaryWriter(cache);

            try
            {
                WellChessInj chessInj = null;
                List<int[]> WellIndices = new List<int[]>();
                var br = new BinaryReader(oldChess);
                DateTime minDate, maxDate, dt;
                minDate = RDF.RdfSystem.UnpackDateTimeI(br.ReadInt32());
                maxDate = RDF.RdfSystem.UnpackDateTimeI(br.ReadInt32());

                // read well indices
                int WellsCount = br.ReadInt32();

                for (i = 0; i < WellsCount; ++i)
                {
                    var WellIndexItem = new int[2];
                    WellIndexItem[0] = br.ReadInt32();   // well index
                    WellIndexItem[1] = br.ReadInt32();   // offset
                    WellIndices.Add(WellIndexItem);
                }
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    fs = new FileStream(deltaFiles[di].FullName, FileMode.Open);
                    deltaBr[di] = new BinaryReader(fs);
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (minDate > dt) minDate = dt;
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (maxDate < dt) maxDate = dt;
                    deltaCounts[di] = deltaBr[di].ReadInt32();
                }
                // min - max dates
                byte Day, Month;
                int count, wellPosition;
                int year, yearsCount, minYear;
                int lastPosition;
                List<int> YearsPosition = new List<int>();
                bw.Write(RDF.RdfSystem.PackDateTime(minDate));
                bw.Write(RDF.RdfSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(-1);                   // well offset
                }
                minDate = DateTime.MaxValue;
                ChessInjItem item = new ChessInjItem();
                List<List<ChessInjItem>> chessItems = new List<List<ChessInjItem>>();
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    wellPosition = (int)cache.Position;
                    minYear = 9999;
                    yearsCount = 0;
                    YearsPosition.Clear();
                    chessItems.Clear();

                    if (WellIndices[wi][1] != -1)
                    {
                        br.BaseStream.Seek(WellIndices[wi][1], SeekOrigin.Begin);
                        yearsCount = br.ReadInt32();
                        minYear = br.ReadInt32();
                        for (i = 0; i < yearsCount; i++)
                        {
                            YearsPosition.Add(br.ReadInt32());
                            chessItems.Add(new List<ChessInjItem>());
                        }
                        for (i = 0; i < yearsCount; i++)
                        {
                            if (YearsPosition[i] == -1) continue;
                            br.BaseStream.Seek(YearsPosition[i], SeekOrigin.Begin);
                            count = br.ReadInt32();
                            for (j = 0; j < count; j++)
                            {
                                Month = br.ReadByte();
                                Day = br.ReadByte();
                                item.Date = new DateTime(minYear + i, Month, Day);
                                item.Wwat = br.ReadSingle();
                                item.Pust = br.ReadSingle();
                                item.CycleTime = br.ReadByte();
                                item.StayTime = br.ReadByte();
                                chessItems[i].Add(item);
                            }
                        }
                    }
                    for (int di = 0; di < deltaBr.Length; di++)
                    {
                        int version = deltaBr[di].ReadInt32();
                        chessInj = new WellChessInj();
                        wellIndex = deltaBr[di].ReadInt32();
                        if (wellIndex == WellIndices[wi][0])
                        {
                            count = deltaBr[di].ReadInt32(); // skip chess inj
                            if (count > 0) deltaBr[di].BaseStream.Seek(count * chessLen, SeekOrigin.Current);
                            if (chessInj.ReadFromCache(deltaBr[di], version) && chessInj.Count > 0)
                            {
                                if (minYear == 9999) minYear = chessInj[0].Date.Year;
                                for (i = 0; i < chessInj.Count; i++)
                                {
                                    year = chessInj[i].Date.Year - minYear;
                                    while (year >= chessItems.Count)
                                    {
                                        chessItems.Add(new List<ChessInjItem>());
                                        yearsCount++;
                                    }
                                    chessItems[year].Add(chessInj[i]);
                                }
                            }
                        }
                    }

                    for (i = 0; i < chessItems.Count; i++)
                    {
                        if (chessItems[i].Count > 0 && chessItems[i][chessItems[i].Count - 1].Date < maxDate.AddDays(-CutOffDays))
                        {
                            chessItems[i].Clear();
                        }
                        else
                        {
                            for (j = 0; j < chessItems[i].Count; j++)
                            {
                                if (chessItems[i][j].Date > maxDate.AddDays(-CutOffDays))
                                {
                                    break;
                                }
                                if (chessItems[i][j].Date < maxDate.AddDays(-CutOffDays))
                                {
                                    chessItems[i].RemoveAt(j);
                                    j--;
                                }
                            }
                        }

                        if (i == 0 && chessItems[i].Count == 0)
                        {
                            chessItems.RemoveAt(i);
                            yearsCount--;
                            minYear++;
                            i--;
                        }
                    }

                    if (yearsCount > 0)
                    {
                        bw.Write(yearsCount);
                        bw.Write(minYear);
                        lastPosition = (int)bw.BaseStream.Position + chessItems.Count * sizeof(int); // + positions 
                        for (i = 0; i < chessItems.Count; i++)
                        {
                            bw.Write(lastPosition);
                            lastPosition += sizeof(int) + chessItems[i].Count * (4 * sizeof(byte) + 2 * sizeof(float));
                        }
                        for (i = 0; i < chessItems.Count; i++)
                        {
                            bw.Write(chessItems[i].Count);
                            if (chessItems[i].Count > 0 && chessItems[i][0].Date < minDate)
                            {
                                minDate = chessItems[i][0].Date;
                            }
                            for (j = 0; j < chessItems[i].Count; j++)
                            {
                                Day = (byte)chessItems[i][j].Date.Day;
                                Month = (byte)chessItems[i][j].Date.Month;
                                bw.Write(Month);
                                bw.Write(Day);
                                bw.Write(chessItems[i][j].Wwat);
                                bw.Write(chessItems[i][j].Pust);
                                bw.Write((byte)chessItems[i][j].CycleTime);
                                bw.Write((byte)chessItems[i][j].StayTime);
                            }
                        }
                    }
                    lastPosition = (int)cache.Position;
                    WellIndices[wi][1] = (lastPosition != wellPosition) ? wellPosition : -1;
                }
                if (minDate == DateTime.MaxValue) minDate = maxDate.AddDays(-CutOffDays);
                cache.Seek(0, SeekOrigin.Begin);
                bw.Write(RDF.RdfSystem.PackDateTime(minDate));
                bw.Write(RDF.RdfSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(WellIndices[wi][1]);  // well offset
                }
                oldChess.Close();
                oldChess = null;
                cache.Flush();
                cache.Close();
                cache = null;
                if (File.Exists(Path.Combine(dir.FullName, "chessinj_fast_cache.bin")))
                {
                    if (File.Exists(Path.Combine(dir.FullName, "chessinj_fast.bin")))
                    {
                        File.Delete(Path.Combine(dir.FullName, "chessinj_fast.bin"));
                    }
                    File.Move(Path.Combine(dir.FullName, "chessinj_fast_cache.bin"), Path.Combine(dir.FullName, "chessinj_fast.bin"));
                    if (deltaFiles.Length > 0)
                    {
                        DateTime servDate = deltaFiles[deltaFiles.Length - 1].CreationTime;
                        File.SetCreationTime(Path.Combine(dir.FullName, "chessinj_fast.bin"), servDate);
                        File.SetLastWriteTime(Path.Combine(dir.FullName, "chessinj_fast.bin"), servDate);
                    }
                }
            }
            catch
            {
                if (cache != null)
                {
                    cache.Close();
                    cache = null;
                }
            }
            finally
            {
                if (oldChess != null) oldChess.Close();
                if (cache != null) cache.Close();
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    if (deltaBr[di] != null)
                    {
                        deltaBr[di].Close();
                        if (deltaFiles[di].Exists) deltaFiles[di].Delete();
                    }
                }
                string name = Path.Combine(dir.FullName, "chessinj_fast_cache.bin");
                if (File.Exists(name)) File.Delete(name);
            }
        }
        void TestDeltaFiles(UpdateOilFieldItem oilfield, UpdateOilFieldItem ofCache, UpdateOilFieldItem ofServ)
        {
            int chessFastIndex = -1, chessInjFastIndex = -1, merFastIndex = -1;
            List<int> chessDeltalist = new List<int>();
            List<int> merDeltalist = new List<int>();

            DateTime servTime, cacheTime;
            for (int i = 0; i < ofServ.Count; i++)
            {
                cacheTime = DateTime.MinValue;

                if (ofServ[i].FileName.StartsWith("chess_fast.bin"))
                {
                    chessFastIndex = i;
                }
                else if (ofServ[i].FileName.StartsWith("chessinj_fast.bin"))
                {
                    chessInjFastIndex = i;
                }
                else if (ofServ[i].FileName.StartsWith("mer_fast.bin"))
                {
                    merFastIndex = i;
                }
                else if (ofServ[i].FileName.StartsWith("chess_delta"))
                {
                    chessDeltalist.Add(i);
                }
                else if (ofServ[i].FileName.StartsWith("mer_delta"))
                {
                    merDeltalist.Add(i);
                }
            }

            //chess_delta
            UpdateFileItem item, cacheItem;
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            for (int i = 0; i < chessDeltalist.Count; i++)
            {
                item = ofServ[chessDeltalist[i]];
                if (chessFastIndex > -1)
                {
                    cacheItem = null;
                    if (ofCache != null) cacheItem = ofCache.GetItemByFileName(ofServ[chessFastIndex].FileName);
                    if (cacheItem != null && GetDateTime(item.CacheCreationTime) <= GetDateTime(cacheItem.CacheCreationTime))
                    {
                        chessDeltalist.RemoveAt(i);
                        i--;
                    }
                }
                if (minDate > item.CacheCreationTime)
                {
                    minDate = item.CacheCreationTime;
                }
                if (maxDate < item.CacheCreationTime)
                {
                    maxDate = item.CacheCreationTime;
                }
            }

            if ((maxDate - minDate).TotalDays <= 14 && chessDeltalist.Count > 0)
            {
                for (int i = 0; i < chessDeltalist.Count; i++)
                {
                    AddUpdateFileItem(oilfield, ofServ[chessDeltalist[i]], ofCache);
                }
            }
            else if (chessDeltalist.Count > 0)
            {
                cacheItem = null;
                servTime = GetDateTime(ofServ[chessFastIndex].CacheCreationTime);
                if (ofCache != null) cacheItem = ofCache.GetItemByFileName(ofServ[chessFastIndex].FileName);
                cacheTime = (cacheItem != null) ? GetDateTime(cacheItem.CacheCreationTime) : DateTime.MinValue;
                if (cacheItem == null || servTime.Subtract(cacheTime).TotalMinutes > 1)
                {
                    AddUpdateFileItem(oilfield, ofServ[chessFastIndex], ofCache);
                    if (chessInjFastIndex != -1) AddUpdateFileItem(oilfield, ofServ[chessInjFastIndex], ofCache);
                }
            }

            //mer_delta
            minDate = DateTime.MaxValue;
            maxDate = DateTime.MinValue;
            for (int i = 0; i < merDeltalist.Count; i++)
            {
                item = ofServ[merDeltalist[i]];
                if (merFastIndex > -1)
                {
                    cacheItem = null;
                    if (ofCache != null) cacheItem = ofCache.GetItemByFileName(ofServ[merFastIndex].FileName);
                    if (cacheItem != null && GetDateTime(item.CacheCreationTime) <= GetDateTime(cacheItem.CacheCreationTime))
                    {
                        merDeltalist.RemoveAt(i);
                        i--;
                    }
                }
                if (minDate > item.CacheCreationTime)
                {
                    minDate = item.CacheCreationTime;
                }
                if (maxDate < item.CacheCreationTime)
                {
                    maxDate = item.CacheCreationTime;
                }
            }

            if ((maxDate - minDate).TotalDays <= 183 && merDeltalist.Count > 0)
            {
                for (int i = 0; i < merDeltalist.Count; i++)
                {
                    AddUpdateFileItem(oilfield, ofServ[merDeltalist[i]], ofCache);
                }
            }
            else if (merDeltalist.Count > 0)
            {
                cacheItem = null;
                servTime = GetDateTime(ofServ[merFastIndex].CacheCreationTime);
                if (ofCache != null) cacheItem = ofCache.GetItemByFileName(ofServ[merFastIndex].FileName);
                cacheTime = (cacheItem != null) ? GetDateTime(cacheItem.CacheCreationTime) : DateTime.MinValue;
                if (cacheItem == null || servTime.Subtract(cacheTime).TotalMinutes > 1)
                {
                    AddUpdateFileItem(oilfield, ofServ[merFastIndex], ofCache);
                }
            }
        }

        DateTime GetDateTime(DateTime Date)
        {
            return new DateTime(Date.Year, Date.Month, Date.Day, Date.Hour, Date.Minute, Date.Second);
        }
        void AddUpdateFileItem(UpdateOilFieldItem oilfield, UpdateFileItem servItem, UpdateOilFieldItem cacheOf)
        {
            string type = GetFileTypeName(servItem.FileName);
            if (type.Length == 0)
            {
                return;
            }
            UpdateFileItem cacheItem = null;
            if (cacheOf != null)
            {
                cacheItem = cacheOf.GetItemByFileName(servItem.FileName);
            }
            UpdateFileItem updateItem = new UpdateFileItem();
            updateItem.Type = type;
            updateItem.IsLoaded = false;
            updateItem.CacheCreationTime = (cacheItem != null) ? cacheItem.CacheCreationTime : DateTime.MinValue;
            updateItem.CreationTime = servItem.CacheCreationTime;
            updateItem.FileName = servItem.FileName;
            updateItem.CreationUser = string.Empty;
            oilfield.Add(updateItem);
        }
     }
}
