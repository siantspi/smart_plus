﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus
{
    public class UpdateFileItem
    {
        public string Type;
        public string FileName;
        public DateTime CreationTime;
        public DateTime CacheCreationTime;
        public string CreationUser;
        public bool IsLoaded;
        public bool IsPreLoaded;
        public DateTime PreLoadCreationTime;
        public string CachePath;
        public string ServerPath;
        public string PreLoadPath;
        public int FileSize;
    }

    public class UpdateOilFieldItem
    {
        public string OilField;
        public bool IsPreLoaded;
        public bool IsLoaded;
        List<UpdateFileItem> UpdateItemList;

        public UpdateOilFieldItem()
        {
            UpdateItemList = new List<UpdateFileItem>(10);
            IsPreLoaded = false;
            IsLoaded = false;
        }
        public int Count
        {
            get { return UpdateItemList.Count; }
        }

        public void Add(UpdateFileItem item)
        {
            UpdateItemList.Add(item);
        }
        public void Clear()
        {
            UpdateItemList.Clear();
        }
        public UpdateFileItem this[int index]
        {
            get { return UpdateItemList[index]; }
        }
        public UpdateFileItem GetItemByFileName(string FileName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].FileName == FileName)
                {
                    return this[i];
                }
            }
            return null;
        }
    }

    public enum DataUpdaterWork
    {
        CheckUpdates,
        CopyFromServerToPreLoad,
        MoveFromPreLoadToCache,
        CopyFromServerToCache,
        CopyFromServerFromHttpToCache,
        CopyFromServerFromHttpToPreLoad
    }

}
