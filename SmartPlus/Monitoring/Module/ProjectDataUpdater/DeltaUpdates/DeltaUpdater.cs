﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace SmartPlus.Update
{
    class DeltaUpdater
    {
        public static DeltaUpdateType GetUpdateType(string FileName)
        {
            string[] DeltaUpdateTypeNames = 
            {
                "contours",
                "grids",
                "areas",
                "areas_param",
                "wellPads",
                "OilObj",
                "PVTParams",
                "bizplan",
                "sum",
                "sum_chess",
                "wellList",
                "coord",
                "chess",
                "chess_fast",
                "chessinj_fast",
                "mer",
                "mer_fast",
                "gis",
                "perf",
                "gtm",
                "action",
                "research",
                "suspend",
                "leak",
                "logs",
                "logs_base",
                "trajectory",
                "core",
                "core_test"
            };
            for(int i = 0; i < DeltaUpdateTypeNames.Length;i++)
            {
                if (FileName.IndexOf(DeltaUpdateTypeNames[i]) > -1)
                {
                    return (DeltaUpdateType)i;
                }
            }
            return DeltaUpdateType.None;
        }
        public static bool TryParseDeltaUpdateParams(string FileName, out int Version, out DeltaUpdateType DeltaType, out DateTime DateUpdate)
        {
            bool result = false;
            Version = -1;
            DeltaType = DeltaUpdateType.None;
            DateUpdate = DateTime.MinValue;
            int pos, length = FileName.Length;
            pos = FileName.IndexOf("_");
            if (pos > -1 && int.TryParse(FileName.Substring(0, pos), out Version))
            {
                FileName = FileName.Substring(pos + 1, length - pos);
                pos = FileName.IndexOf("_");
                DeltaType = GetUpdateType(FileName.Substring(0, pos));
                if (DeltaType != DeltaUpdateType.None)
                {
                    FileName = FileName.Substring(pos + 1, length - pos);
                    pos = FileName.IndexOf("_");
                    if (pos > -1 && DateTime.TryParse(FileName.Substring(0, pos), out DateUpdate))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }
    }

    public enum DeltaUpdateType
    {
        // При добавлении необходимо обновить функцию DeltaUpdater.GetUpdateType
        None = -1,
        Contours = 0,
        Grids,
        AreaList,
        AreaSumParams,
        WellPadList,
        OilfieldStratumList,
        OilfieldPVT,
        Bizplan,
        OilfieldSumMer,
        OifieldSumChess,
        WellList,
        Coord,
        Chess,
        ChessFast,
        ChessInjFast,
        Mer,
        MerFast,
        Gis,
        Perf,
        Gtm,
        Action,
        Research,
        Suspend,
        Leak,
        Logs,
        LogsBase,
        Trajectory,
        Core,
        CoreTest
    }

    public enum DeltaOperationType
    {
        Add,
        Insert,
        Remove,
        Edit
    }


}
