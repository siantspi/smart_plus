﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO;
using RDF;
using RDF.Objects;

namespace SmartPlus.Update
{
    public class DeltaMer : DeltaBase<DeltaRecordMer>
    {
        public static int Version = 0;

        public DeltaMer() : base()
        {
            this.Type = DeltaUpdateType.Mer;
            this.FileNamePart = "mer";
        }
        public void InitDeltaRecords(int wellIndex, Mer oldMer, Mer newMer)
        {
            List<DeltaRecordMer> deltaItems = oldMer.GetDeltaUpdates(newMer);
            if (deltaItems.Count > 0)
            {
                DeltaObjectBase<DeltaRecordMer> newItem = new DeltaObjectBase<DeltaRecordMer>(wellIndex, deltaItems);
                Items.Add(newItem);
            }
        }


        public override void CreateDeltaUpdates(string OldDataFileName, OilField of)
        {
 	         if (File.Exists(OldDataFileName))
             {
                 using (FileStream oldFile = new FileStream(OldDataFileName, FileMode.Open))
                 {
                     int merLen = 80, merExLen = 44;
                     int count, countEx;
                     Well w;
                     BinaryReader oldBr = new BinaryReader(oldFile, Encoding.UTF8);

                 }
             }
        }

        public void Read(string FileName)
        {
            if (File.Exists(FileName))
            {
                using(FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                {
                    BinaryReader br = new BinaryReader(fs, Encoding.UTF8);
                    int version = br.ReadInt32();
                    if (version == DeltaMer.Version) base.Read(br);
                }
            }
        }
        public void Write(string FileName)
        {
            using(FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write))
            {
                BinaryWriter bw = new BinaryWriter(fs, Encoding.UTF8);
                bw.Write(Version);
                base.Write(bw);
            }
        }
    }

    public class DeltaRecordMer : DeltaRecordBase
    {
        public MerItem? Item;
        public MerItemEx? ItemEx;

        public DeltaRecordMer(DeltaOperationType OperationType, int RecordIndex, MerItem? item, MerItemEx? itemEx)
        {
            Type = OperationType;
            Index = RecordIndex;
            Item = item;
            ItemEx = itemEx;
        }
        public override void Read(BinaryReader br)
        {
            Type = (DeltaOperationType)br.ReadInt32();
            Index = br.ReadInt32();
            if (br.ReadBoolean())
            {
                Item = new MerItem();
                ((MerItem)Item).Read(br);
            }
            if (br.ReadBoolean())
            {
                ItemEx = new MerItemEx();
                ((MerItemEx)ItemEx).Read(br);
            }
        }
        public override void Write(BinaryWriter bw)
        {
            bw.Write((int)Type);
            bw.Write(Index);
            bw.Write(Item.HasValue);
            if (Item.HasValue)
            {
                ((MerItem)Item).Write(bw);
            }
            bw.Write(ItemEx.HasValue);
            if (ItemEx.HasValue)
            {
                ((MerItemEx)ItemEx).Write(bw);
            }
        }
    }
}
