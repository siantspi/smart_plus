﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO;
using RDF;

namespace SmartPlus.Update
{
    public class DeltaBase<T> where T : DeltaRecordBase
    {
        public DeltaUpdateType Type = DeltaUpdateType.None;
        public string FileNamePart = string.Empty;
        public int Index;
        public int Count { get { return (Items != null) ? Items.Count : 0; } }

        protected List<DeltaObjectBase<T>> Items = null;
        
        public DeltaBase()
        {
            this.Items = new List<DeltaObjectBase<T>>();
        }

        public bool TestFileName(string FileName)
        {
            return FileName.IndexOf(FileNamePart) > -1;
        }

        public virtual void CreateDeltaUpdates(string OldDataFileName, OilField of) { }

        public void Read(BinaryReader br)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                int unpackSize = br.ReadInt32();
                int packSize = br.ReadInt32();
                ConvertEx.CopyStream(br.BaseStream, ms, packSize);
                MemoryStream unpack = new MemoryStream();
                ConvertEx.UnPackStream(ms, ref unpack);
                unpack.Seek(0, SeekOrigin.Begin);
                BinaryReader locBr = new BinaryReader(unpack, Encoding.UTF8);
                int count = locBr.ReadInt32();
                DeltaObjectBase<T>[] newItems = new DeltaObjectBase<T>[count];
                for (int i = 0; i < Count; i++)
                {
                    Items[i].Read(locBr);
                }
                Items = new List<DeltaObjectBase<T>>(newItems);
                unpack.Dispose();
            }
        }
        protected void Write(BinaryWriter bw)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryWriter lockbw = new BinaryWriter(ms, Encoding.UTF8);
                lockbw.Write(Count);
                for (int i = 0; i < Count; i++)
                {
                    Items[i].Write(lockbw);
                }
                MemoryStream pack = ConvertEx.PackStream(ms);
                bw.Write(ms.Length);
                bw.Write(pack.Length);
                pack.Seek(0, SeekOrigin.Begin);
                pack.WriteTo(bw.BaseStream);
                pack.Dispose();
            }
        }
    }



    public class DeltaObjectBase<T> where T : DeltaRecordBase
    {
        public int IndexObject;
        protected List<T> Items = null;
        public int Count { get { return (Items != null) ? Items.Count : 0; } }

        public DeltaObjectBase(int IndexObject)
        {
            this.IndexObject = IndexObject;
            Items = new List<T>();
        }
        public DeltaObjectBase(int IndexObject, List<T> items)
        {
            this.IndexObject = IndexObject;
            Items = new List<T>(items.ToArray());
        }
        public void Write(BinaryWriter bw)
        {
            if (Items != null)
            {
                bw.Write(Items.Count);
                for (int i = 0; i < Items.Count; i++)
                {
                    Items[i].Write(bw);
                }
            }
            else
            {
                bw.Write(0);
            }
        }
        public void Read(BinaryReader br)
        {
            int count = br.ReadInt32();
            T[] newItems = new T[count];
            for (int i = 0; i < Items.Count; i++)
            {
                Items[i].Read(br);
            }
            Items = new List<T>(newItems);
        }
    }
    public abstract class DeltaRecordBase
    {
        public DeltaOperationType Type;
        public int Index;

        public abstract void Read(BinaryReader br);
        public abstract void Write(BinaryWriter bw);
    }
}
