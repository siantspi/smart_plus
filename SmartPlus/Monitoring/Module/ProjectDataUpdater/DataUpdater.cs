﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Threading;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public delegate void OnEndCheckUpdatesDelegate();
    public delegate void OnEndDownloadDelegate();
    public delegate void OnStartDownloadDelegate();
    public delegate void OnEndItemCopyDelegate(UpdateFileItem item);

    
    public sealed partial class DataUpdater
    {
        public event OnEndCheckUpdatesDelegate OnCheckUpdates;
        public event OnStartDownloadDelegate OnStartDownload;
        public event OnEndDownloadDelegate OnEndDownload;
        public event OnEndItemCopyDelegate OnEndItemCopy;

        public bool IsFirstUpdate;
        public bool UpdatesAvailable, UpdatesPreLoadAvailable, UpdatesInstalled;
        public bool UpdateSuccesDownloaded;
        bool NeedStraightUpdate;
        DataUpdaterWork Work;
        public DataUpdaterWork CurrentWork { get { return Work; } }
        public ThreadPriority Priority;
        public int NeedHaveFreeSpace;
        string ServerName, ServerPath, LocalPath, ServerUser, ServerPass;
        bool LoadColorsScheme;

        int ServerOilfieldCount;

        bool IsLoadByHttp;
        HttpFileReciever http;
        public bool StartDownload;

        Project project;
        BackgroundWorker worker;
        FileStream LockFile;
        public List<UpdateOilFieldItem> UpdateOilFieldList;

        public bool IsBusy { get { return worker.IsBusy; } }

        public DataUpdater(Project project, string ServerName, string ServerPath, string LocalPath)
        {
            this.project = project;
            this.ServerName = ServerName;
            this.ServerPath = ServerPath + "\\" + project.Name;
            this.LocalPath = LocalPath + "\\" + project.Name; 
            NeedHaveFreeSpace = 0;
            NeedStraightUpdate = false;
            UpdatesInstalled = false;
            UpdateOilFieldList = new List<UpdateOilFieldItem>();
            LockFile = null;

            this.IsLoadByHttp = false;
            this.http = null;
            this.StartDownload = false;

            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        public DataUpdater(Project project, string localPath, AppCommonSettings appSettings)
        {
            this.project = project;
            this.ServerName = appSettings.UpdatebyHttpsServerName;
            this.LocalPath = localPath + "\\" + project.Name; 
            NeedHaveFreeSpace = 0;
            NeedStraightUpdate = false;
            UpdatesInstalled = false;
            UpdateOilFieldList = new List<UpdateOilFieldItem>();
            LockFile = null;
            this.ServerUser = appSettings.UpdatebyHttpsUserName;
            this.ServerPass = appSettings.UpdatebyHttpsPassword;
            this.LoadColorsScheme = appSettings.UpdatebyHttpsLoadColorScheme;
            this.IsLoadByHttp = true;
            this.http = null;
            
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        // Worker
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            switch (Work)
            {
                case DataUpdaterWork.CheckUpdates:
                    CheckUpdatesFromServer(ServerPath);
                    if (UpdatesAvailable)
                    {
                        FillUpdateOilfieldList(worker, e);
                        NeedHaveFreeSpace = GetAvailableFreeSpace(worker, e);
                        if(UpdateOilFieldList.Count == 0) 
                        {
                            UpdatesAvailable = false;
                            UpdatesPreLoadAvailable = false;
                        }
                    }
                    break;
                case DataUpdaterWork.CopyFromServerToPreLoad:
                    try
                    {
                        if (GetPermissUpdate()) // доступен сервер + открыт файл LockFile
                        {
                            if (!Directory.Exists(project.path + "\\cache_update"))
                            {
                                Directory.CreateDirectory(project.path + "\\cache_update");
                            }
                            if (UpdatesAvailable)
                            {
                                CopyFromServerToPreLoad(worker, e);
                            }
                        }
                    }
                    catch
                    {
                    }
                    break;
                case DataUpdaterWork.CopyFromServerFromHttpToPreLoad:
                    try
                    {
                        if (GetPermissUpdate()) // доступен сервер + открыт файл LockFile
                        {
                            if (!Directory.Exists(project.path + "\\cache_update"))
                            {
                                Directory.CreateDirectory(project.path + "\\cache_update");
                            }
                            if (UpdatesAvailable)
                            {
                                CopyFromServerFromHttpToPreLoad(worker, e);
                            }
                        }
                    }
                    catch
                    {
                    }
                    break;
                case DataUpdaterWork.CopyFromServerToCache:
                    if (UpdatesAvailable && GetAvaivableServer())
                    {
                        CopyFromServerToCache(worker, e);
                    }
                    break;
                case DataUpdaterWork.MoveFromPreLoadToCache:
                    if (UpdatesAvailable && UpdatesPreLoadAvailable)
                    {
                        MoveFromPreLoadToCache(worker, e);
                    }
                    break;
                case DataUpdaterWork.CopyFromServerFromHttpToCache:
                    if (UpdatesAvailable && GetAvaivableServer())
                    {
                        CopyFromServerFromHttpToCache(worker, e);
                    }
                    break;
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (Work)
            {
                case DataUpdaterWork.CopyFromServerToPreLoad:
                    if (OnEndItemCopy != null) OnEndItemCopy((UpdateFileItem)e.UserState);
                    break;
                case DataUpdaterWork.CopyFromServerFromHttpToPreLoad:
                    if (OnEndItemCopy != null) OnEndItemCopy((UpdateFileItem)e.UserState);
                    break;
                case DataUpdaterWork.CopyFromServerToCache:
                    if (OnEndItemCopy != null) OnEndItemCopy((UpdateFileItem)e.UserState);
                    break;
                case DataUpdaterWork.MoveFromPreLoadToCache:
                    if (OnEndItemCopy != null) OnEndItemCopy((UpdateFileItem)e.UserState);
                    break;
                case DataUpdaterWork.CopyFromServerFromHttpToCache:
                    if (!StartDownload) 
                    { 
                        StartDownload = true; 
                        OnStartDownload(); 
                    }
                    if (OnEndItemCopy != null) OnEndItemCopy((UpdateFileItem)e.UserState);
                    break;
            }
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (LockFile != null)
            {
                LockFile.Close();
                LockFile = null;
                if (File.Exists(project.path + "\\upd.lock")) File.Delete(project.path + "\\upd.lock");
            }
            if (e.Error != null) throw new Exception(e.Error.Message);

            if (!e.Cancelled)
            {
                switch (Work)
                {
                    case DataUpdaterWork.CheckUpdates:
                        if (OnCheckUpdates != null) OnCheckUpdates();
                        if (UpdatesAvailable && (!UpdatesPreLoadAvailable) && NeedHaveFreeSpace == 0) 
                        {
                            if (IsLoadByHttp)
                            {
                                if (IsFirstUpdate)
                                {
                                    Work = DataUpdaterWork.CopyFromServerFromHttpToCache;
                                }
                                else
                                {
                                    Work = DataUpdaterWork.CopyFromServerFromHttpToPreLoad;
                                }
                            }
                            else
                            {
                                Work = DataUpdaterWork.CopyFromServerToPreLoad;
                            }
                            worker.RunWorkerAsync();
                        }
                        break;
                    case DataUpdaterWork.CopyFromServerToPreLoad:
                        UpdatesPreLoadAvailable = true;
                        if (IsFirstUpdate)
                        {
                            Work = DataUpdaterWork.MoveFromPreLoadToCache;
                            worker.RunWorkerAsync();
                        }
                        if (OnEndDownload != null) OnEndDownload();
                        break;
                    case DataUpdaterWork.CopyFromServerFromHttpToPreLoad:
                        UpdatesPreLoadAvailable = true;
                        if (IsFirstUpdate)
                        {
                            Work = DataUpdaterWork.MoveFromPreLoadToCache;
                            worker.RunWorkerAsync();
                        }
                        if (OnEndDownload != null) OnEndDownload();
                        break;
                    case DataUpdaterWork.CopyFromServerToCache:
                        NeedStraightUpdate = false;
                        UpdatesInstalled = true;
                        if (OnEndDownload != null) OnEndDownload();
                        break;
                    case DataUpdaterWork.MoveFromPreLoadToCache:
                        if (UpdateSuccesDownloaded)
                        {
                            UpdatesInstalled = true;
                            if (OnEndDownload != null) OnEndDownload();
                        }
                        else
                        {
                            UpdatesPreLoadAvailable = false;
                            UpdatesInstalled = false;
                            StartStraightUpdate();
                        }
                        break;
                    case DataUpdaterWork.CopyFromServerFromHttpToCache:
                        NeedStraightUpdate = false;
                        UpdatesInstalled = true;
                        if (OnEndDownload != null) OnEndDownload();
                        break;
                }
            }
            else if (NeedStraightUpdate)
            {
                if (IsLoadByHttp)
                {
                    Work = (UpdatesPreLoadAvailable) ? DataUpdaterWork.MoveFromPreLoadToCache : DataUpdaterWork.CopyFromServerFromHttpToCache;
                }
                else
                {
                    Work = (UpdatesPreLoadAvailable) ? DataUpdaterWork.MoveFromPreLoadToCache : DataUpdaterWork.CopyFromServerToCache;
                }
                worker.RunWorkerAsync();
            }
        }

        // Function
        bool GetAvaivableServer()
        {
            bool result = true;
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                try
                {
                    System.Net.NetworkInformation.PingReply pingResult = ping.Send(ServerName, 3000);
                    if (pingResult.Status != System.Net.NetworkInformation.IPStatus.Success)
                    {
                        result = false;
                    }
                }
                catch
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }
        bool GetPermissUpdate()
        {
            LockFile = null;
            if (GetAvaivableServer()) // доступность сети и сервера
            {
                try
                {
                    LockFile = new FileStream(project.path + "\\upd.lock", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                }
                catch
                {
                    LockFile = null;
                }
            }
            return (LockFile != null);
        }
        string GetFileTypeName(string FileName)
        {
            string type = string.Empty;

            string fileName = Path.GetFileName(FileName);

            switch (fileName)
            {
                case "skv.csv":
                    type = "Координаты скважин";
                    break;
                case "coord.bin":
                    type = "Координаты скважин";
                    break;
                case "wellList.bin":
                    type = "Список скважин";
                    break;
                case "contours.bin":
                    type = "Контуры";
                    break;
                case "areas.bin":
                    type = "Ячейки заводнения";
                    break;
                case "areas_param.bin":
                    type = "Параметры ячеек заводнения";
                    break;
                case "bizplan.bin":
                    type = "Данные Бизнес-плана";
                    break;
                case "wellpads.csv":
                    type = "Кусты";
                    break;
                case "wellpads.bin":
                    type = "Кусты";
                    break;
                case "gis.bin":
                    type = "Данные ГИС";
                    break;
                case "perf.bin":
                    type = "Данные Перфорации";
                    break;
                case "logs.bin":
                    type = "Данные каротажа";
                    break;
                case "logs_base.bin":
                    type = "Данные базового каротажа";
                    break;
                case "grids.bin":
                    type = "Сетки";
                    break;
                case "OilObj.bin":
                    type = "Список объектов";
                    break;
                case "mer.bin":
                    type = "Данные МЭР";
                    break;
                case "chess.bin":
                    type = "Данные Шахматки";
                    break;
                case "sum.bin":
                    type = "Суммарные показатели";
                    break;
                case "sum_chess.bin":
                    type = "Суммарные показатели по Шахматке";
                    break;
                case "table81.bin":
                    type = "Проектные показатели";
                    break;
                case "PVTParams.bin":
                    type = "PVT данные";
                    break;
                case "core.bin":
                    type = "Данные наличия керна";
                    break;
                case "core_test.bin":
                    type = "Данные исследований керна";
                    break;
                case "gtm.bin":
                    type = "Данные ГТМ";
                    break;
                case "well_action.bin":
                    type = "Данные мероприятий на скважине";
                    break;
                case "well_research.bin":
                    type = "Данные исследований на скважине";
                    break;
                case "well_leak.bin":
                    type = "Исследования ЭК";
                    break;
                case "well_suspend.bin":
                    type = "Исследования КВЧ";
                    break;
                case "trajectory.bin":
                    type = "Траектории скважин";
                    break;
                case "CharWork.csv":
                    type = "Характер работы";
                    break;
                case "Construction.csv":
                    type = "Объект обустройства";
                    break;
                case "GridType.csv":
                    type = "Тип сетки";
                    break;
                case "Lithology.csv":
                    type = "Литология";
                    break;
                case "LogMnemonic.csv":
                    type = "Мнемоника каротажа";
                    break;
                case "LogUnit.csv":
                    type = "Единица измерения каротажа";
                    break;
                case "OilfieldArea.csv":
                    type = "Площадь месторождения";
                    break;
                case "OilfieldCoefficient.csv":
                    type = "Коэффициенты месторождений";
                    break;
                case "OilfieldParams.csv":
                    type = "Месторождения";
                    break;
                case "PerfType.csv":
                    type = "Тип перфорации";
                    break;
                case "Saturation.csv":
                    type = "Тип насыщенности";
                    break;
                case "WellActionType.csv":
                    type = "Тип мероприятий на скважине";
                    break;
                case "WellMethod.csv":
                    type = "Метод работы";
                    break;
                case "WellResearchType.csv":
                    type = "Тип исследований скважины";
                    break;
                case "WellState.csv":
                    type = "Состояние скважины";
                    break;
                case "Stratum.csv":
                    type = "Пласт";
                    break;
                case "StratumTree.csv":
                    type = "Иерархия пластов";
                    break;
                case "StayReason.csv":
                    type = "StayReason.csv";
                    break;
                case "deposit.bin":
                    type = "Залежи";
                    break;
                case "colors.bin":
                    if (LoadColorsScheme)
                    {
                        type = "Раскраска объектов разработки";
                    }
                    else
                    {
                        type = string.Empty;
                    }
                    break;
                case "GtmType.csv":
                    type = "Тип ГТМ";
                    break;

                // загрузка обновлений для айпад версии
                //case "chessinj_fast.bin":
                //    type = "Данные шахматка fast";
                //    break;
                //case "chess_fast.bin":
                //    type = "Данные Шахматки fast"; 
                //    break;
                //case "mer_fast.bin":
                //    type = "Данные МЭР fast";
                //    break;

                default:
                    //if(FileName.IndexOf("delta") > -1)
					//{
                    //    type = "Файл delta обновления";
                    //    break;
					//}
                    type = string.Empty;
                    break;
            }
            return type;
        }
        string GetPreLoadFileName(string FileName)
        {
            string preLoad = string.Empty;
            int pos = FileName.IndexOf("\\cache\\");
            if (pos > 0)
            {
                preLoad = FileName.Insert(pos, "\\cache_update");
            }
            return preLoad;
        }

        // Check Updates from Server
        public bool CheckUpdatesFromServer(string ServerPath)
        {
            UpdatesAvailable = false;
            UpdatesPreLoadAvailable = false;
            if (ServerPath != "")
            {
                string cacheName = project.path + "\\prjUpd.bin";
                string servName = ServerPath + "\\prjUpd.bin";
                DateTime dtCache = DateTime.MinValue, dtServ = DateTime.MinValue;
                bool cacheExist;
                cacheExist = File.Exists(cacheName);
                if (cacheExist)
                {
                    dtCache = File.GetLastWriteTime(cacheName);
                    dtCache = new DateTime(dtCache.Year, dtCache.Month, dtCache.Day, dtCache.Hour, dtCache.Minute, dtCache.Second);

                    if (IsLoadByHttp)
                    {
                        if (http == null)
                        {
                            http = new HttpFileReciever();
                        }
                        try
                        {
                            dtServ = http.GetLastModifiedDate("https://" + ServerName + "//" + HttpFileReciever.CyrillicUrlEncode(project.Name) + "//prjUpd.bin", ServerUser, ServerPass);
                        }
                        catch (Exception)
                        {
                            dtServ = DateTime.MinValue;
                        }
                    }
                    else 
                    if (File.Exists(servName))
                    {
                        dtServ = File.GetLastWriteTime(servName);
                        dtServ = new DateTime(dtServ.Year, dtServ.Month, dtServ.Day, dtServ.Hour, dtServ.Minute, dtServ.Second);
                    }
                }
                if (!cacheExist)
                {
                    if (Directory.Exists(project.path + "\\cache"))
                    {
                        if (Directory.GetFiles(project.path + "\\cache").Length == 0)
                        {
                            IsFirstUpdate = true;
                        }
                    }
                    else
                    {
                        IsFirstUpdate = true;
                    }
                }
                if (!cacheExist || (dtCache < dtServ)) UpdatesAvailable = true;
            }
            return UpdatesAvailable;
        }

        // Free Space
        int GetAvailableFreeSpace(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int availableSpace = 0, sumSize = 0;
            for (int i = 0; i < UpdateOilFieldList.Count; i++)
            {
                UpdateFileItem item;
                for (int j = 0; j < UpdateOilFieldList[i].Count; j++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return 0;
                    }
                    item = UpdateOilFieldList[i][j];
                    if (!item.IsPreLoaded)
                    {
                        sumSize += item.FileSize;
                    }
                }
            }
            try
            {
                string diskName = LocalPath.Remove(1, LocalPath.Length - 1);
                DriveInfo drive = new DriveInfo(diskName);
                if (drive.TotalFreeSpace < sumSize * 1.3)
                {
                    availableSpace = (int)Math.Round(sumSize * 1.3 / 1024);
                }
            }
            catch
            { }
            return availableSpace;
        }

        // Project Update file
        public bool CreateProjectUpdateFile(string ProjCachePath)
        {
            bool res = false;
            if (project != null && Directory.Exists(ProjCachePath + "\\cache"))
            {
                DirectoryInfo dirInfoCache = new DirectoryInfo(ProjCachePath + "\\cache");
                DirectoryInfo[] dirsCache = dirInfoCache.GetDirectories();
                FileInfo[] files;
                DirectoryInfo dir;
                List<UpdateOilFieldItem> of_list = new List<UpdateOilFieldItem>();
                DateTime cacheTime;
                string CacheFile;

                int i, j, k, len = dirsCache.Length;
                UpdateOilFieldItem oilfield;
                UpdateFileItem updateItem;
                for (i = 0; i <= len; i++)
                {
                    dir = (i < len) ? dirsCache[i] : dirInfoCache;
                    files = dir.GetFiles();

                    oilfield = new UpdateOilFieldItem();
                    oilfield.OilField = dir.Name;
                    k = project.GetOFIndex(oilfield.OilField);
                    if ((k == -1) && (oilfield.OilField != "cache")) continue;

                    foreach (FileInfo file in files)
                    {
                        if (i < len) CacheFile = Path.Combine(ProjCachePath + "\\cache\\" + dir.Name, file.Name);
                        else CacheFile = Path.Combine(ProjCachePath + "\\cache\\", file.Name);

                        cacheTime = File.GetLastWriteTime(CacheFile);
                        cacheTime = new DateTime(cacheTime.Year, cacheTime.Month, cacheTime.Day, cacheTime.Hour, cacheTime.Minute, cacheTime.Second);

                        string type = GetFileTypeName(file.Name);
                        if (type.Length == 0) continue;

                        updateItem = new UpdateFileItem();
                        updateItem.Type = type;
                        updateItem.CacheCreationTime = cacheTime;
                        updateItem.FileName = file.Name;
                        updateItem.CreationUser = string.Empty;
                        updateItem.FileSize = (int)file.Length;
                        oilfield.Add(updateItem);
                    }
                    if (oilfield.Count > 0) of_list.Add(oilfield);
                }
                if (of_list.Count > 0)
                {
                    FileStream fs = new FileStream(ProjCachePath + "\\prjUpd.bin", FileMode.Create);
                    BinaryWriter bfw = new BinaryWriter(fs);
                    bfw.Write(of_list.Count);
                    for (i = 0; i < of_list.Count; i++)
                    {
                        oilfield = of_list[i];
                        bfw.Write(oilfield.OilField);
                        bfw.Write(oilfield.Count);
                        for (j = 0; j < oilfield.Count; j++)
                        {
                            updateItem = oilfield[j];
                            bfw.Write(updateItem.FileName);
                            bfw.Write(updateItem.CacheCreationTime.ToOADate());
                            bfw.Write(updateItem.FileSize);
                        }
                    }
                    bfw.Close();
                    File.SetCreationTime(ProjCachePath + "\\prjUpd.bin", DateTime.Now);
                    File.SetLastWriteTime(ProjCachePath + "\\prjUpd.bin", DateTime.Now);
                }
            }
            res = (Directory.Exists(ProjCachePath) && File.Exists(ProjCachePath + "\\prjUpd.bin"));
            return res;
        }

        // UpdateOilfieldItems
        List<UpdateOilFieldItem> GetUpdOilfieldList(string fileName)
        {
            List<UpdateOilFieldItem> list = new List<UpdateOilFieldItem>();
            UpdateOilFieldItem oilfield;
            UpdateFileItem updateItem;
            try
            {
                if (File.Exists(fileName))
                {
                    int i, j, count, file_count;
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    count = bfr.ReadInt32();
                    for (i = 0; i < count; i++)
                    {
                        oilfield = new UpdateOilFieldItem();
                        oilfield.OilField = bfr.ReadString();
                        file_count = bfr.ReadInt32();
                        for (j = 0; j < file_count; j++)
                        {
                            updateItem = new UpdateFileItem();
                            updateItem.FileName = bfr.ReadString();
                            updateItem.CacheCreationTime = DateTime.FromOADate(bfr.ReadDouble());
                            updateItem.FileSize = bfr.ReadInt32();
                            oilfield.Add(updateItem);
                        }
                        list.Add(oilfield);
                        worker.ReportProgress((int)(i * 100f / count));
                    }
                    bfr.Close();
                }
            }
            catch
            {
 
            }
            return list;
        }
        UpdateOilFieldItem GetUpdOilfield(List<UpdateOilFieldItem> ofUpdList, string OilfieldName)
        {
            for (int i = 0; i < ofUpdList.Count; i++)
            {
                if (ofUpdList[i].OilField == OilfieldName)
                {
                    return ofUpdList[i];
                }
            }
            return null;
        }
        bool FillUpdateOilfieldList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            UpdateOilFieldList.Clear();
            try
            {
                if (Directory.Exists(LocalPath) && ((Directory.Exists(ServerPath) && File.Exists(ServerPath + "\\prjUpd.bin")) || IsLoadByHttp))
                {
                    UpdateOilFieldItem ofCache, ofServ, oilfield;
                    UpdateFileItem CacheFile, ServFile, updateItem;
                    List<UpdateOilFieldItem> cacheList, servList;
                    int i, j, countPreLoaded;
                    DateTime dt;
                    DateTime cacheTime = DateTime.MinValue;
                    bool addItem;

                    cacheList = GetUpdOilfieldList(LocalPath + "\\prjUpd.bin");
                    if (cacheList.Count == 0)
                    {
                        CreateProjectUpdateFile(LocalPath);
                        cacheList = GetUpdOilfieldList(LocalPath + "\\prjUpd.bin");
                    }

                    // копируем файл описание структры проекта с сервера
                    if (IsLoadByHttp)
                    {
                        try
                        {
                            CopyFileByHttp("prjUpd.bin", "serv_prjUpd.bin", DateTime.Now);
                        }
                        catch { }
                    }
                    else
                    {
                        File.Copy(ServerPath + "\\prjUpd.bin", LocalPath + "\\serv_prjUpd.bin", true);
                    }
                    
                    servList = GetUpdOilfieldList(LocalPath + "\\serv_prjUpd.bin");

                    for (i = 0; i < servList.Count; i++)
                    {
                        ofServ = servList[i];
                        ofCache = GetUpdOilfield(cacheList, ofServ.OilField);
                        oilfield = new UpdateOilFieldItem();

                        oilfield.OilField = ofServ.OilField;
                        countPreLoaded = 0;
                        for (j = 0; j < ofServ.Count; j++)
                        {
                            ServFile = ofServ[j];
                            addItem = false;
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                UpdateOilFieldList.Clear();
                                return false;
                            }
                            if (ofCache != null)
                            {
                                CacheFile = ofCache.GetItemByFileName(ServFile.FileName);
                                if (CacheFile != null)
                                {
                                    // todo почему разные милисекунды?
                                    ServFile.CacheCreationTime = new DateTime(ServFile.CacheCreationTime.Year,
                                        ServFile.CacheCreationTime.Month,
                                        ServFile.CacheCreationTime.Day,
                                        ServFile.CacheCreationTime.Hour,
                                        ServFile.CacheCreationTime.Minute,
                                        ServFile.CacheCreationTime.Second);
                                    CacheFile.CacheCreationTime = new DateTime(CacheFile.CacheCreationTime.Year,
                                        CacheFile.CacheCreationTime.Month,
                                        CacheFile.CacheCreationTime.Day,
                                        CacheFile.CacheCreationTime.Hour,
                                        CacheFile.CacheCreationTime.Minute,
                                        CacheFile.CacheCreationTime.Second);
                                    int o = ServFile.CacheCreationTime.CompareTo(CacheFile.CacheCreationTime);
                                    if (ServFile.CacheCreationTime.CompareTo(CacheFile.CacheCreationTime) > 0)
                                    {
                                        addItem = true;
                                        cacheTime = CacheFile.CacheCreationTime;
                                    }
                                }
                                else
                                {
                                    cacheTime = DateTime.MinValue;
                                    addItem = true;
                                }
                            }
                            else
                            {
                                cacheTime = DateTime.MinValue;
                                addItem = true;
                            }
                            if (addItem)
                            {
                                string type = GetFileTypeName(ServFile.FileName);
                                if (type.Length == 0) continue;
                                updateItem = new UpdateFileItem();
                                updateItem.Type = type;
                                updateItem.CacheCreationTime = cacheTime;
                                updateItem.CreationTime = ServFile.CacheCreationTime;
                                updateItem.FileName = ServFile.FileName;
                                updateItem.FileSize = ServFile.FileSize;
                                updateItem.CreationUser = string.Empty;
                                if ((ofServ.OilField != "cache"))
                                {
                                    
                                    updateItem.ServerPath = string.Format("{0}\\cache\\{1}\\{2}", ServerPath, ofServ.OilField, ServFile.FileName);
                                    if (ofServ.OilField == "Dictionaries")
                                    {
                                        updateItem.CachePath = string.Format("{0}\\{1}\\{2}", LocalPath, ofServ.OilField, ServFile.FileName);
                                        updateItem.PreLoadPath = string.Format("{0}\\cache_update\\{1}\\{2}", LocalPath, ofServ.OilField, ServFile.FileName);
                                    }
                                    else
                                    {
                                        updateItem.CachePath = string.Format("{0}\\cache\\{1}\\{2}", LocalPath, ofServ.OilField, ServFile.FileName);
                                        updateItem.PreLoadPath = string.Format("{0}\\cache_update\\cache\\{1}\\{2}", LocalPath, ofServ.OilField, ServFile.FileName);
                                    }
                                }
                                else
                                {
                                    updateItem.CachePath = string.Format("{0}\\cache\\{1}", LocalPath, ServFile.FileName);
                                    updateItem.ServerPath = string.Format("{0}\\cache\\{1}", ServerPath, ServFile.FileName);
                                    updateItem.PreLoadPath = string.Format("{0}\\cache_update\\cache\\{1}", LocalPath, ServFile.FileName);
                                }
                                if (File.Exists(updateItem.PreLoadPath))
                                {
                                    dt = File.GetLastWriteTime(updateItem.PreLoadPath);
                                    dt = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
                                    if (dt == ServFile.CacheCreationTime)
                                    {
                                        updateItem.PreLoadCreationTime = dt;
                                        updateItem.IsPreLoaded = true;
                                        countPreLoaded++;
                                    }
                                }
                                oilfield.Add(updateItem);
                            }
                        }
                        if (countPreLoaded > 0 && countPreLoaded == oilfield.Count) oilfield.IsPreLoaded = true;
                        // if oilfield in UpdateList this AD
                        if (oilfield.Count > 0) UpdateOilFieldList.Add(oilfield);
                    }
                    countPreLoaded = 0;
                    for (i = 0; i < UpdateOilFieldList.Count; i++)
                    {
                        if (UpdateOilFieldList[i].IsPreLoaded) countPreLoaded++;
                    }
                    UpdatesPreLoadAvailable = (countPreLoaded > 0 && countPreLoaded == UpdateOilFieldList.Count);
                    if (File.Exists(LocalPath + "\\serv_prjUpd.bin")) File.Delete(LocalPath + "\\serv_prjUpd.bin");
                    return true;
                }
            }
            catch
            {
                UpdateOilFieldList.Clear();
                UpdatesAvailable = false;
                UpdatesPreLoadAvailable = false;
            }
            return false;
        }
      
        // Copy Files
        void CopyFromServerToPreLoad(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int progress = 0;
            if (!Directory.Exists(project.path + "\\cache_update")) Directory.CreateDirectory(project.path + "\\cache_update");
            if (!Directory.Exists(project.path + "\\cache_update\\cache")) Directory.CreateDirectory(project.path + "\\cache_update\\cache");
            for (int i = 0; i < UpdateOilFieldList.Count; i++)
            {
                if (UpdateOilFieldList.Count > 0 && !Directory.Exists(Path.GetDirectoryName(UpdateOilFieldList[i][0].PreLoadPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(UpdateOilFieldList[i][0].PreLoadPath));
                }
                UpdateFileItem item;
                for (int j = 0; j < UpdateOilFieldList[i].Count; j++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    item = UpdateOilFieldList[i][j];
                    if (!item.IsPreLoaded)
                    {
                        File.Copy(item.ServerPath, item.PreLoadPath, true);
                        File.SetCreationTime(item.PreLoadPath, item.CreationTime);
                        File.SetLastWriteTime(item.PreLoadPath, item.CreationTime);
                        item.PreLoadCreationTime = item.CreationTime;
                        item.IsPreLoaded = true;
                    }
                    if (j == UpdateOilFieldList[i].Count - 1)
                    {
                        UpdateOilFieldList[i].IsPreLoaded = true;
                    }
                    progress++;
                    worker.ReportProgress(progress, item);
                }
            }
            string servFile = ServerPath + "\\" + project.Name + ".txt";
            string cacheFile = project.path + "\\cache_update\\" + project.Name + ".txt";
            if (File.Exists(servFile))
            {
                DateTime dtServ = File.GetLastWriteTime(servFile);
                DateTime dtCache = File.Exists(cacheFile) ? File.GetLastWriteTime(cacheFile) : DateTime.MinValue;
                if(dtCache < dtServ)
                {
                    File.Copy(servFile, cacheFile, true);
                }
            }
        }
        void CopyFromServerToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (!Directory.Exists(project.path + "\\cache")) Directory.CreateDirectory(project.path + "\\cache");
            int progress = 0;
            for (int i = 0; i < UpdateOilFieldList.Count; i++)
            {
                if (UpdateOilFieldList.Count > 0 && !Directory.Exists(Path.GetDirectoryName(UpdateOilFieldList[i][0].CachePath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(UpdateOilFieldList[i][0].CachePath));
                }
                UpdateFileItem item;
                for (int j = 0; j < UpdateOilFieldList[i].Count; j++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    item = UpdateOilFieldList[i][j];
                    if (item.IsPreLoaded)
                    {
                        if (File.Exists(item.CachePath)) File.Delete(item.CachePath);
                        File.Move(item.PreLoadPath, item.CachePath);
                        File.SetCreationTime(item.CachePath, item.CreationTime);
                        File.SetLastWriteTime(item.CachePath, item.CreationTime);
                    }
                    else
                    {
                        File.Copy(item.ServerPath, item.CachePath, true);
                        File.SetCreationTime(item.CachePath, item.CreationTime);
                        File.SetLastWriteTime(item.CachePath, item.CreationTime);
                    }
                    item.IsLoaded = true;
                    if (j == UpdateOilFieldList[i].Count - 1)
                    {
                        UpdateOilFieldList[i].IsLoaded = true;
                    }
                    progress++;
                    worker.ReportProgress(progress, item);
                }
                if (UpdateOilFieldList.Count > 0 && Directory.Exists(Path.GetDirectoryName(UpdateOilFieldList[i][0].PreLoadPath)))
                {
                    DirectoryInfo info = new DirectoryInfo(Path.GetDirectoryName(UpdateOilFieldList[i][0].PreLoadPath));
                    info.Delete(true);
                }
            }
            CreateProjectUpdateFile(project.path);
            string servFile = ServerPath + "\\" + project.Name + ".txt";
            string cacheFile = project.path + "\\" + project.Name + ".txt";
            if (File.Exists(servFile))
            {
                DateTime dtServ = File.GetLastWriteTime(servFile);
                DateTime dtCache = File.Exists(cacheFile) ? File.GetLastWriteTime(cacheFile) : DateTime.MinValue;
                if (dtCache < dtServ)
                {
                    File.Copy(servFile, cacheFile, true);
                }
            }
        }
        void MoveFromPreLoadToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (!Directory.Exists(project.path + "\\cache")) Directory.CreateDirectory(project.path + "\\cache");
            int progress = 0;
            for (int i = 0; i < UpdateOilFieldList.Count; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                if (UpdateOilFieldList.Count > 0 && !Directory.Exists(Path.GetDirectoryName(UpdateOilFieldList[i][0].CachePath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(UpdateOilFieldList[i][0].CachePath));
                }
                UpdateFileItem item;
                for (int j = 0; j < UpdateOilFieldList[i].Count; j++)
                {
                    item = UpdateOilFieldList[i][j];
                    if (item.IsPreLoaded)
                    {
                        if (File.Exists(item.CachePath)) File.Delete(item.CachePath);
                        File.Move(item.PreLoadPath, item.CachePath);
                        File.SetCreationTime(item.CachePath, item.CreationTime);
                        File.SetLastWriteTime(item.CachePath, item.CreationTime);
                        item.IsLoaded = true;
                    }
                    if (j == UpdateOilFieldList[i].Count - 1)
                    {
                        if (UpdateOilFieldList[i].IsPreLoaded)
                            UpdateOilFieldList[i].IsLoaded = true;
                    }
                    progress++;
                    worker.ReportProgress(progress, item);
                }
                if (UpdateOilFieldList.Count > 0 && Directory.Exists(Path.GetDirectoryName(UpdateOilFieldList[i][0].PreLoadPath)))
                {
                    DirectoryInfo info = new DirectoryInfo(Path.GetDirectoryName(UpdateOilFieldList[i][0].PreLoadPath));
                    info.Delete(true);
                }
            }
            CreateProjectUpdateFile(project.path);
            string servFile = project.path + "\\cache_update\\" + project.Name + ".txt";
            string cacheFile = project.path + "\\" + project.Name + ".txt";
            if (File.Exists(servFile))
            {
                DateTime dtServ = File.GetLastWriteTime(servFile);
                DateTime dtCache = File.Exists(cacheFile) ? File.GetLastWriteTime(cacheFile) : DateTime.MinValue;
                if (dtCache < dtServ)
                {
                    if (File.Exists(cacheFile)) File.Delete(cacheFile);
                    File.Copy(servFile, cacheFile, true);
                }
                File.Delete(servFile);
            }
        }

        public void Start()
        {
            Work = DataUpdaterWork.CheckUpdates;
            worker.RunWorkerAsync(); 
        }
        public void StartStraightUpdate()
        {
            if (worker.IsBusy)
            {
                NeedStraightUpdate = true;
                worker.CancelAsync();
            }
            else
            {
                if (IsLoadByHttp)
                {
                    Work = (UpdatesPreLoadAvailable) ? DataUpdaterWork.MoveFromPreLoadToCache : DataUpdaterWork.CopyFromServerFromHttpToCache;
                }
                else
                {
                    Work = (UpdatesPreLoadAvailable) ? DataUpdaterWork.MoveFromPreLoadToCache : DataUpdaterWork.CopyFromServerToCache;
                }
                worker.RunWorkerAsync();
            }
        }
        public void CancelUpdate()
        {
            if (worker.IsBusy) worker.CancelAsync();
        }
    }
}
