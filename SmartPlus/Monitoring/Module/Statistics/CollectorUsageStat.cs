﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;

namespace SmartPlus
{
    public enum CollectorStatId : byte
    {
        BTN_ADD_CONTOUR,
        BTN_ADD_PROFILE_LINE,
        BTN_ADD_PROFILE_NEAR_POINT,
        BTN_CURRENT_BUBBLE,
        BTN_ACCUM_BUBBLE,
        BTN_HYSTORY_BUBBLE,
        BTN_SETTING_BUBBLE,
        FIND,
        CLICK_WELL,
        CLICK_AREA,
        CLICK_OILFIELD,
        CLICK_NGDU,
        CLICK_REGION,
        UPDATE_GRAPHICS,
        BTN_WL_NEW_EMPTY,
        BTN_WL_ADD_SEL_WELL,
        BTN_WL_ADD_WELL_CNTR,
        BTN_WL_ADD_WELL_CLIPBOARD,
        BTN_WL_SETTING_ICON,
        WL_BY_FILTER,
        WF_SET_PROFILE,
        WF_SET_WELLLIST,
        FOO_RESET_OBJ,
        BTN_AP_CALC_PARAMS,
        CANVAS_ZOOM,
        CANVAS_MOUSE_DOWN,
        CANVAS_BTN_SNAPSHOOT,
        PLANE_ZOOM,
        PLANE_MOUSE_DOWN,
        CORSCHEME_ZOOM,
        CORSCHEME_MOUSE_DOWN,
        GRAPHICS_MOUSE_DOWN,
        GRAPHICS_ZOOM,
        INSPECTOR_MOUSE_DOWN,
        ACTIVE_MEST_MOUSE_DOWN,
        WORKFOLDER_MOUSE_DOWN,
        FILTER_OILOBJ_MOUSE_DOWN,
        WELLLIST_MOUSE_DOWN,
        MER_MOUSE_DOWN,
        CHESS_MOUSE_DOWN,
        GIS_MOUSE_DOWN,
        GRAPHICS_CHANGE_SCHEME,
        GRAPHICS_BTN_SNAPSHOOT,
        GRAPHICS_SHOW_TABLE,
        GRAPHICS_CHANGE_DATE_INTERVAL,
        GRAPHICS_BTN_INFO,
        GRAPHICS_BTN_LEGEND,
        AREAS_TABLE_MOUSE_DOWN,
        CORE_TABLE_MOUSE_DOWN,
        CORE_TEST_MOUSE_DOWN
    }

    public class CollectorUsageStat
    {
        internal enum CollectorWorkMode : byte
        {
            SAVE_LOCAL_DATA,
            SEND_SERVER_DATA
        }
        CollectorWorkMode CurrentMode;
        string domain;
        string user;
        string ServerName;
        string LocalFileName, LocalCopyName;
        string ServerFileName, ServerCopyName;
        string workPath;
        BackgroundWorker worker;
        List<int> UsageStats;
        List<int> UsageStatsOld;

        Timer tmr;
        public CollectorUsageStat(string ServerName, string ServerPath, string WorkPath)
        {
            domain = Environment.UserDomainName;
            user = Environment.UserName.ToUpper();
            UsageStatsOld = null;
            UsageStats = new List<int>();
            this.ServerName = ServerName;
            this.CurrentMode = CollectorWorkMode.SEND_SERVER_DATA;

            this.ServerFileName = ServerPath + @"\" + domain + "_" + user + "_stats.csv";
            this.LocalFileName = "local_" + user + "_stats.bin";
            this.LocalCopyName = "local_copy_" + user + "_stats.bin";
            this.ServerCopyName = "server_" + user + "_stats.csv";

            workPath = WorkPath;
            tmr = new Timer();
            tmr.Enabled = true;
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(tmr_Tick);
            worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        void tmr_Tick(object sender, EventArgs e)
        {
            if (!worker.IsBusy) worker.RunWorkerAsync();
        }

        public void WorkerCancel()
        {
            if (worker.IsBusy) worker.CancelAsync();
        }
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {

        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this.CurrentMode == CollectorWorkMode.SEND_SERVER_DATA)
            {
                this.CurrentMode = CollectorWorkMode.SAVE_LOCAL_DATA;
                tmr.Interval = 300000;
            }
        }

        public void AddMessage(CollectorStatId id)
        {
            AddMessage(id, 1);
        }
        public void AddMessage(CollectorStatId id, int value)
        {
            byte ind = (byte)id;
            if (ind >= 0)
            {
                if (UsageStatsOld != null)
                {
                    for (int i = 0; i < UsageStatsOld.Count; i++)
                    {
                        if (i < UsageStats.Count)
                        {
                            UsageStats[i] += UsageStatsOld[i];
                        }
                        else
                        {
                            UsageStats.Add(UsageStatsOld[i]);
                        }
                    }
                    UsageStatsOld = null;
                }
                while (UsageStats.Count <= ind) UsageStats.Add(0);
                if ((ind < UsageStats.Count) && (((long)UsageStats[ind] + (long)value) < (long)Int32.MaxValue)) UsageStats[ind] += value;
            }
        }

        public bool SaveLocalBeforeUpdate()
        {
            bool res = false;
            try
            {
                if ((File.Exists(Application.StartupPath + "\\" + this.LocalFileName)) && (Directory.Exists(workPath)))
                {
                    if (File.Exists(workPath + "\\" + this.LocalFileName)) File.Delete(workPath + "\\" + this.LocalFileName);
                    File.Move(Application.StartupPath + "\\" + this.LocalFileName, workPath + "\\" + this.LocalFileName);
                }
                res = true;
            }
            catch
            {
                res = false;
            }
            return res;
        }

        private bool AvaivableServer()
        {
            bool result = true;
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                try
                {
                    System.Net.NetworkInformation.PingReply pingResult = ping.Send(ServerName, 3000);
                    if (pingResult.Status != System.Net.NetworkInformation.IPStatus.Success)
                    {
                        result = false;
                    }
                }
                catch
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }
        private bool GetFileFromServer()
        {
            bool res = false;
            if (AvaivableServer() && File.Exists(this.ServerFileName))
            {
                try
                {
                    File.Copy(this.ServerFileName, Application.StartupPath + "\\" + this.ServerCopyName);
                    res = true;
                }
                catch
                {
                    res = false;
                }
            }
            return res;
        }
        private void SaveDataToLocalFile(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (File.Exists(Application.StartupPath + "\\" + this.LocalFileName))
            {
                StreamReader sr = null;
                StreamWriter sw = null;
                string[] parseStr;
                char[] ch = new char[] { ';' };
                DateTime dt, now = DateTime.Now;
                try
                {
                    sr = new StreamReader(Application.StartupPath + "\\" + this.LocalFileName, Encoding.GetEncoding(1251));
                    sw = new StreamWriter(Application.StartupPath + "\\" + this.LocalCopyName, false, Encoding.GetEncoding(1251));
                    string str;
                    while (!sr.EndOfStream)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        str = sr.ReadLine();
                        parseStr = str.Split(ch);
                        if (parseStr[0] != "")
                        {
                            dt = Convert.ToDateTime(parseStr[0]);
                            if ((dt.Day == now.Day) && (dt.Month == now.Month) && (dt.Year == now.Year))
                            {
                                str = parseStr[0] + ";";
                                for (int i = 0; i < UsageStats.Count; i++)
                                {
                                    str += UsageStats[i].ToString() + ";";
                                }
                                sw.WriteLine(str);
                                break;
                            }
                            else
                            {
                                sw.WriteLine(str);
                            }
                        }
                    }
                }
                finally
                {
                    if(sr != null) sr.Close();
                    if(sw != null) sw.Close();
                    try
                    {
                        if (File.Exists(Application.StartupPath + "\\" + this.LocalCopyName))
                        {
                            if (File.Exists(Application.StartupPath + "\\" + this.LocalFileName))
                            {
                                File.Delete(Application.StartupPath + "\\" + this.LocalFileName);
                            }
                            File.Move(Application.StartupPath + "\\" + this.LocalCopyName, Application.StartupPath + "\\" + this.LocalFileName);
                        }
                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                StreamWriter sw = null;
                try
                {
                    sw = new StreamWriter(Application.StartupPath + "\\" + this.LocalFileName, true, Encoding.GetEncoding(1251));
                    string str = String.Format("{0:dd.MM.yyyy};", DateTime.Now);
                    for (int i = 0; i < UsageStats.Count; i++)
                    {
                        str += UsageStats[i].ToString() + ";";
                    }
                    sw.WriteLine(str);
                }
                finally
                {
                    if (sw != null) sw.Close();
                }
            }
        }
        private void DeleteLocalFile()
        {
            if (File.Exists(Application.StartupPath + "\\" + this.LocalFileName)) File.Delete(Application.StartupPath + "\\" + this.LocalFileName);
        }
        private bool CopyData(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool res = false;
            if (File.Exists(Application.StartupPath + "\\" + this.LocalFileName))
            {
                StreamReader sr = new StreamReader(Application.StartupPath + "\\" + this.LocalFileName, Encoding.GetEncoding(1251));
                StreamWriter sw = new StreamWriter(Application.StartupPath + "\\" + this.ServerCopyName, true, Encoding.GetEncoding(1251));
                string[] parseStr;
                char[] ch = new char[] { ';' };
                DateTime dt, now = DateTime.Now;
                string nowStr = "";
                int count = 0;
                int value;
                try
                {
                    string str;
                    while (!sr.EndOfStream)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        str = sr.ReadLine();
                        parseStr = str.Split(ch);
                        if (parseStr[0] != "")
                        {
                            dt = Convert.ToDateTime(parseStr[0]);
                            if ((dt.Day != now.Day) || (dt.Month != now.Month) || (dt.Year != now.Year))
                            {
                                sw.WriteLine(str);
                                count++;
                            }
                            else
                            {
                                nowStr = str;
                                break;
                            }
                        }
                    }
                    if ((!e.Cancel) && (count > 0)) res = true;
                }
                finally
                {
                    sr.Close();
                    sw.Close();
                    if (nowStr != "")
                    {
                        parseStr = nowStr.Split(ch);
                        if ((parseStr[0] != "") && (parseStr.Length > 1))
                        {
                            try
                            {
                                List<int> oldStats = new List<int>(parseStr.Length - 1);
                                for (int i = 1; i < parseStr.Length; i++)
                                {
                                    if (parseStr[i] != "")
                                    {
                                        value = Convert.ToByte(parseStr[i]);
                                        oldStats.Add(value);
                                    }
                                }
                                UsageStatsOld = oldStats;
                            }
                            catch
                            {

                            }
                        }
                    }
                }
            }
            return res;
        }
        private bool SendFileToServer()
        {
            if (AvaivableServer() && File.Exists(Application.StartupPath + "\\" + this.ServerCopyName))
            {
                string ticks = DateTime.Now.Ticks.ToString();
                try
                {
                    File.Move(Application.StartupPath + "\\" + this.ServerCopyName, this.ServerFileName + "_" + ticks);
                    if (File.Exists(this.ServerFileName)) File.Delete(this.ServerFileName);
                    File.Move(this.ServerFileName + "_" + ticks.ToString(), this.ServerFileName);
                    return true;
                }
                catch
                {
                    if (File.Exists(this.ServerFileName + "_" + ticks))
                    {
                        File.Delete(this.ServerFileName + "_" + ticks);
                    }
                }
            }
            if (File.Exists(Application.StartupPath + "\\" + this.ServerCopyName)) File.Delete(Application.StartupPath + "\\" + this.ServerCopyName);
            return false;
        }
    }
}
