﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;

namespace SmartPlus
{
    public enum CollectorStatMessageId : byte
    {
        START_APP,
        SHUTDOWN_APP,
        LOAD_PROJECT,
        UPDATE_APP,
        UPDATE_DATA
    }
    
    public class CollectorStat
    {
        internal struct CollectorMess
        {
            public CollectorStatMessageId id;
            public DateTime time;
            public string message;
        }
        internal enum CollectorWorkMode : byte
        {
            ADD_MESSAGE,
            SEND_SERVER_DATA
        }
        CollectorWorkMode CurrentMode;
        string appVer;
        string domain;
        string ip;
        string user;
        string LocalName;
        string ServerName, ServerFileName, ServerCopyName;
        string workPath;

        BackgroundWorker worker;
        List<CollectorMess> LocalMsgs;

        Timer tmr;
        public CollectorStat(string ServerName, string ServerPath, string WorkPath, string AppVer)
        {
            this.appVer = AppVer;
            ip = "";
            LocalMsgs = new List<CollectorMess>();
            domain = Environment.UserDomainName;
            user = Environment.UserName.ToUpper();
            this.ServerName = ServerName;
            this.ServerFileName = ServerPath + @"\" + domain + "_" +  user + ".csv";
            this.LocalName = "local_" + user + ".bin";
            this.ServerCopyName = "server_" + user + ".csv";
            workPath = WorkPath;
            tmr = new Timer();
            tmr.Enabled = false;
            tmr.Interval = 60000;
            tmr.Tick += new EventHandler(tmr_Tick);
            worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        void tmr_Tick(object sender, EventArgs e)
        {
            if (!worker.IsBusy)
            {
                SendServerData();
                tmr.Enabled = false;
            }
        }

        private string GetIpAdress()
        {
            string ip = "";
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                string host = System.Net.Dns.GetHostName();
                System.Net.IPAddress[] ipList = System.Net.Dns.GetHostByName(host).AddressList;
                
                for (int i = 0; i < ipList.Length; i++)
                {
                    if (i > 0) ip += ",";
                    ip += ipList[i].ToString();
                }
            }
            if (ip == "") ip = "-";
            return ip;
        }
        private void SendServerData()
        {
            CurrentMode = CollectorWorkMode.SEND_SERVER_DATA;
            worker.RunWorkerAsync();
        }
        public void WorkerCancel()
        {
           if(worker.IsBusy) worker.CancelAsync();
        }
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
#if !DEBUG
            bool res = false;
            switch (CurrentMode)
            {
                case CollectorWorkMode.ADD_MESSAGE:
                    try
                    {
                        if ((!File.Exists(Application.StartupPath + "\\" + this.LocalName)) && (File.Exists(workPath + "\\" + this.LocalName)))
                        {
                            File.Move(workPath + "\\" + this.LocalName, Application.StartupPath + "\\" + this.LocalName);
                        }
                    }
                    catch
                    {
         
                    }
                    if(ip == "") ip = GetIpAdress();
                    AddLocalMessage((CollectorMess)e.Argument);
                    break;
                case CollectorWorkMode.SEND_SERVER_DATA:
                    System.Threading.Thread.CurrentThread.Priority = System.Threading.ThreadPriority.Lowest;
                    if (AvaivableServer())
                    {
                        res = GetFileFromServer();
                        if ((res) || (!File.Exists(this.ServerFileName)))
                        {
                            res = CopyData(worker, e);
                        }
                        if (res) res = SendFileToServer();
                        if (res) DeleteLocalFile();
                    }
                    break;
            }
#endif
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (LocalMsgs.Count > 0)
            {
                CollectorMess mess = LocalMsgs[0];
                CurrentMode = CollectorWorkMode.ADD_MESSAGE;
                worker.RunWorkerAsync(mess);
                LocalMsgs.RemoveAt(0);
            }
        }

        public bool SaveLocalBeforeUpdate()
        {
            bool res = false;
            try
            {
                if ((File.Exists(Application.StartupPath + "\\" + this.LocalName)) && (Directory.Exists(workPath)))
                {
                    if(File.Exists(workPath + "\\" + this.LocalName)) File.Delete(workPath + "\\" + this.LocalName);
                    File.Move(Application.StartupPath + "\\" + this.LocalName, workPath + "\\" + this.LocalName);
                    
                }
                res = true;
            }
            catch
            {
                res = false;
            }
            return res;
        }
        public void AddMessage(CollectorStatMessageId id, string Message)
        {
            CollectorMess mess;
            DateTime now = DateTime.Now, dt = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
            mess.id = id;
            mess.time = dt;
            mess.message = Message;
            if (!worker.IsBusy)
            {
                CurrentMode = CollectorWorkMode.ADD_MESSAGE;
                worker.RunWorkerAsync(mess);
            }
            else
            {
                LocalMsgs.Add(mess);
            }
        }
        public void AddMessageNotBackGround(CollectorStatMessageId id, string Message)
        {
#if !DEBUG
            CollectorMess mess;
            DateTime now = DateTime.Now, dt = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
            mess.id = id;
            mess.time = dt;
            mess.message = Message;
            AddLocalMessage(mess);
#endif
        }
        public void StartServerTimer()
        {
            tmr.Start();
        }
        private void AddLocalMessage(CollectorMess mess)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(Application.StartupPath + "\\" + this.LocalName, true, Encoding.GetEncoding(1251));
                string str = String.Format("{0};{1};{2:dd.MM.yyyy};{3:HH:mm:ss};{4};{5};", ip, appVer, mess.time, mess.time, (byte)mess.id, mess.message);
                sw.WriteLine(str);
            }
            finally
            {
                if(sw != null) sw.Close();
            }
        }
        private bool AvaivableServer()
        {
            bool result = true;
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                try
                {
                    System.Net.NetworkInformation.PingReply pingResult = ping.Send(ServerName, 3000);
                    if (pingResult.Status != System.Net.NetworkInformation.IPStatus.Success)
                    {
                        result = false;
                    }
                }
                catch
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }
        private bool GetFileFromServer()
        {
            bool res = false;
            if (File.Exists(this.ServerFileName))
            {
                try
                {
                    File.Copy(this.ServerFileName, Application.StartupPath + "\\" + this.ServerCopyName);
                    res = true;
                }
                catch
                {
                    res = false;
                }
            }
            return res;
        }
        private void DeleteLocalFile()
        {
            if (File.Exists(Application.StartupPath + "\\" + this.LocalName)) File.Delete(this.LocalName);
        }
        private bool CopyData(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool res = false;
            if (File.Exists(Application.StartupPath + "\\" + this.LocalName))
            {
                StreamReader sr = new StreamReader(Application.StartupPath + "\\" + this.LocalName, Encoding.GetEncoding(1251));
                StreamWriter sw = new StreamWriter(Application.StartupPath + "\\" + this.ServerCopyName, true, Encoding.GetEncoding(1251));
                try
                {
                    while (!sr.EndOfStream)
                    {
                        if(worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        sw.WriteLine(sr.ReadLine());
                    }
                    if(!e.Cancel) res = true;
                }
                finally
                {
                    sr.Close();
                    sw.Close();
                }
            }
            return res;
        }
        private bool SendFileToServer()
        {
            if (AvaivableServer() && File.Exists(Application.StartupPath + "\\" + this.ServerCopyName))
            {
                string ticks = DateTime.Now.Ticks.ToString();
                try
                {
                    File.Move(Application.StartupPath + "\\" + this.ServerCopyName, this.ServerFileName + "_" + ticks);
                    if (File.Exists(this.ServerFileName)) File.Delete(this.ServerFileName);
                    File.Move(this.ServerFileName + "_" + ticks.ToString(), this.ServerFileName);
                    return true;
                }
                catch
                {
                    if (File.Exists(this.ServerFileName + "_" + ticks))
                    {
                        File.Delete(this.ServerFileName + "_" + ticks);
                    }
                }
            }
            return false;
        }
    }
}
