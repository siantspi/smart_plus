﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class ToolTipMessage : Form
    {
        public Constant.TOOLTIP_TYPE_ID Type;
        public string Title
        {
            get { return lTitle.Text; }
            set { lTitle.Text = value; }
        }
        public string Text
        {
            get { return lText.Text; }
            set { lText.Text = value; }
        }
        MainForm mainForm;
        public bool closeWindow = false;
        Timer tmr;
        int tmrCounter;
        public object PinObject = null;

        public ToolTipMessage(MainForm Parent)
        {
            InitializeComponent();
            mainForm = Parent;
            this.Owner = Parent;
            this.ShowInTaskbar = false;
            closeWindow = false;
            tmrCounter = 0;
            tmr = new Timer();
            tmr.Interval = 3000;
            tmr.Enabled = false;
            tmr.Tick += new EventHandler(tmr_Tick);
            mainForm.LocationChanged += mainForm_LocationChanged;
            this.FormClosing += ToolTipMessage_FormClosing;
        }

        void ToolTipMessage_FormClosing(object sender, FormClosingEventArgs e)
        {
            mainForm.LocationChanged -= mainForm_LocationChanged;
        }

        void tmr_Tick(object sender, EventArgs e)
        {
            if (closeWindow)
            {
                if (this.Opacity > 0) 
                    this.Opacity -= 0.05;
                else
                {
                    tmr.Stop();
                    this.Close();
                }
             }
            else
            {
                if (tmrCounter == 1)
                {
                    closeWindow = true;
                    tmr.Interval = 100;
                }
                tmrCounter++;
            }
        }

        public static void Show(MainForm mainForm, Constant.TOOLTIP_TYPE_ID Type, string Title, string Text)
        {
            Show(mainForm, Type, Title, Text, null);
        }
        public static void Show(MainForm mainForm, Constant.TOOLTIP_TYPE_ID Type, string Title, string Text, object PinObject)
        {
            ToolTipMessage form = new ToolTipMessage(mainForm);
            form.PinObject = PinObject;
            form.Show(Type, Title, Text);
        }

        void mainForm_LocationChanged(object sender, EventArgs e)
        {
            Point pt = Point.Empty;
            int dx = (mainForm.Size.Width - mainForm.ClientSize.Width) / 2;
            pt.X = mainForm.Location.X + dx + 5;
            pt.Y = mainForm.Location.Y + mainForm.Size.Height - this.Size.Height - dx - mainForm.stBar1.Height - 10;
            this.Location = pt;
        }
        void Show(Constant.TOOLTIP_TYPE_ID Type, string Title, string Text)
        {
            Point pt = Point.Empty;
            int dx = (mainForm.Size.Width - mainForm.ClientSize.Width) / 2;
            pt.X = mainForm.Location.X + dx + 5;
            pt.Y = mainForm.Location.Y + mainForm.Size.Height - this.Size.Height - dx - mainForm.stBar1.Height - 10;
            this.Location = pt;
            this.Opacity = 1;
            closeWindow = false;
            tmrCounter = 0;
            tmr.Interval = 3000;
            this.Title = Title;
            this.Text = Text;
            this.Type = Type;
            pClose.Visible = true;
            switch (Type)
            {
                case Constant.TOOLTIP_TYPE_ID.CHECK_APP_UPDATE:
                    this.img.Image = Properties.Resources.UpdateApp;
                    this.panelForm.BackColor = Color.FromArgb(200, 255, 200); 
                    pClose.BackColor = Color.FromArgb(200, 255, 200);
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE:
                    this.img.Image = Properties.Resources.refresh;
                    this.panelForm.BackColor = Color.Cornsilk;
                    pClose.BackColor = Color.Cornsilk;
                    pClose.Visible = false;
                    tmr.Interval = 300000;
                    tmr.Start();
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE_LOADED:
                    this.img.Image = Properties.Resources.refresh_red_24;
                    this.panelForm.BackColor = Color.FromArgb(255, 170, 80);
                    pClose.Visible = false;
                    pClose.BackColor = Color.FromArgb(255, 170, 80);
                    tmr.Interval = 300000;
                    tmr.Start();
                    break;
                case Constant.TOOLTIP_TYPE_ID.INFORMATION:
                    img.Image = System.Drawing.SystemIcons.Information.ToBitmap();
                    this.panelForm.BackColor = Color.Cornsilk;
                    pClose.BackColor = Color.Cornsilk;
                    tmr.Start();
                    break;
                case Constant.TOOLTIP_TYPE_ID.ERROR:
                    img.Image = System.Drawing.SystemIcons.Error.ToBitmap();
                    this.panelForm.BackColor = Color.Cornsilk;
                    pClose.BackColor = Color.Cornsilk;
                    tmr.Start();
                    break;
                case Constant.TOOLTIP_TYPE_ID.INVITE_IN_THEME:
                    img.Image = System.Drawing.SystemIcons.Information.ToBitmap();
                    this.panelForm.BackColor = Color.LightBlue;
                    pClose.BackColor = Color.LightBlue;
                    break;
            }
            this.Show();
        }

        // Click
        private void ClickForm()
        {
            switch (Type)
            {
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE:
                    // mainForm.Stat.AddMessage(CollectorStatMessageId.UPDATE_DATA, "Установка обновлений вручную. Нехватка памяти для фоновой загрузки.");
                    UpdateProjForm.ShowForm(mainForm);
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE_LOADED:
                    // mainForm.Stat.AddMessage(CollectorStatMessageId.UPDATE_DATA, "Установка загруженных фоном обновлений.");
                    UpdateProjForm.ShowForm(mainForm);
                    break;
                case Constant.TOOLTIP_TYPE_ID.INVITE_IN_THEME:
                    if (PinObject != null)
                    {
                        mainForm.OpenInvite((Network.NetworkInviteItem)PinObject);
                    }
                    break;
            }
            Close();
        }
        private void img_Click(object sender, EventArgs e)
        {
            ClickForm();
        }
        private void lTitle_Click(object sender, EventArgs e)
        {
            ClickForm();
        }
        private void lText_Click(object sender, EventArgs e)
        {
            ClickForm();
        }
        private void panelForm_Click(object sender, EventArgs e)
        {
            ClickForm();
        }
        private void pClose_Click(object sender, EventArgs e)
        {
            pClose.BorderStyle = BorderStyle.None;
            switch (Type)
            {
                case Constant.TOOLTIP_TYPE_ID.CHECK_APP_UPDATE:
                    pClose.BackColor = Color.FromArgb(200, 255, 200);
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE_LOADED:
                    pClose.BackColor = Color.FromArgb(255, 170, 80);
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE:
                    pClose.BackColor = Color.Cornsilk;
                    break;
                case Constant.TOOLTIP_TYPE_ID.INFORMATION:
                    pClose.BackColor = Color.Cornsilk;
                    break;
                case Constant.TOOLTIP_TYPE_ID.INVITE_IN_THEME:
                    break;
            }
            Close();
        }

        private void pClose_MouseEnter(object sender, EventArgs e)
        {
            pClose.BorderStyle = BorderStyle.FixedSingle;
            switch (Type)
            {
                case Constant.TOOLTIP_TYPE_ID.CHECK_APP_UPDATE:
                    pClose.BackColor = Color.FromArgb(98, 191, 96);
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE_LOADED:
                    pClose.BackColor = Color.FromArgb(255, 70, 0);
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE:
                    pClose.BackColor = Color.SandyBrown;
                    break;
                case Constant.TOOLTIP_TYPE_ID.INFORMATION:
                    pClose.BackColor = Color.SandyBrown;
                    break;
                case Constant.TOOLTIP_TYPE_ID.INVITE_IN_THEME:
                    pClose.BackColor = Color.DodgerBlue;
                    break;
            }
        }
        private void pClose_MouseLeave(object sender, EventArgs e)
        {
            pClose.BorderStyle = BorderStyle.None;
            switch (Type)
            {
                case Constant.TOOLTIP_TYPE_ID.CHECK_APP_UPDATE:
                    pClose.BackColor = Color.FromArgb(200, 255, 200);
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE_LOADED:
                    pClose.BackColor = Color.FromArgb(255, 170, 80);
                    break;
                case Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE:
                    pClose.BackColor = Color.Cornsilk;
                    break;
                case Constant.TOOLTIP_TYPE_ID.INFORMATION:
                    pClose.BackColor = Color.Cornsilk;
                    break;
                case Constant.TOOLTIP_TYPE_ID.INVITE_IN_THEME:
                    pClose.BackColor = Color.LightBlue;
                    break;
            }
        }
        private void pClose_MouseDown(object sender, MouseEventArgs e)
        {
            pClose.BackColor = Color.SaddleBrown;
        }

        private void panelForm_MouseMove(object sender, MouseEventArgs e)
        {
            tmrCounter = 0;
            closeWindow = false;
            tmr.Interval = 3000;
            this.Opacity = 1;
        }

        private void lText_ClientSizeChanged(object sender, EventArgs e)
        {
            this.Width = lText.Location.X + lText.Width + 14;
            this.Height = lText.Location.Y + lText.Height + 10;
            if (this.Height > lText.Location.Y + 110) this.Height = lText.Location.Y + 110;
            Point pt = pClose.Location;
            pt.X = this.Width - 27;
            pClose.Location = pt;
            if (mainForm != null)
            {
                int dx = (mainForm.Size.Width - mainForm.ClientSize.Width) / 2;
                pt.X = mainForm.Location.X + dx + 5;
                pt.Y = mainForm.Location.Y + mainForm.Size.Height - this.Size.Height - dx - mainForm.stBar1.Height - 10;
                this.Location = pt;
            }
        }
    }
}
