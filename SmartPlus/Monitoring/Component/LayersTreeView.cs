﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public sealed class LayersTreeView : TreeView
    {
        CanvasMap _canv;
        TreeNode dragNode;


        // constructor TreeView
        public LayersTreeView(Control Parent, CanvasMap Canvas)
        {
            this._canv = Canvas;
            this.Parent = Parent;
            this.Dock = DockStyle.Fill;
            this.CheckBoxes = false;
            this.HideSelection = false;
            this.Font = new Font("Calibri", 8.25f);
            this.AllowDrop = true;
            this.MouseDown += new MouseEventHandler(TreeViewLayers_MouseDown);
            this.AfterSelect += new TreeViewEventHandler(TreeViewLayers_AfterSelect);
            this.BeforeExpand += new TreeViewCancelEventHandler(TreeViewLayers_BeforeExpand);
            this.DoubleClick += new EventHandler(TreeViewLayers_DoubleClick);
            InitializeContextMenus();
            this.ContextMenuStrip = cMenuTreeView;

            this.ImageList = new ImageList();
            this.ImageList.ColorDepth = ColorDepth.Depth24Bit;
            this.ImageList.Images.Add(Properties.Resources.def);            //0
            this.ImageList.Images.Add(Properties.Resources.EditIcon16);     //1
            this.ImageList.Images.Add(Properties.Resources.Folder);         //2
            this.ImageList.Images.Add(Properties.Resources.doc);            //3
            this.ImageList.Images.Add(Properties.Resources.pdf);            //4
            this.ImageList.Images.Add(Properties.Resources.ppt);            //5
            this.ImageList.Images.Add(Properties.Resources.png);            //6
            this.ImageList.Images.Add(Properties.Resources.txt);            //7
            this.ImageList.Images.Add(Properties.Resources.xls);            //8
            this.ImageList.Images.Add(Properties.Resources.Oilfield);       //9
            this.ImageList.Images.Add(Properties.Resources.OilfieldArea);   //10
            this.ImageList.Images.Add(Properties.Resources.WellPad);        //11
            this.ImageList.Images.Add(Properties.Resources.Well);           //12
            this.ImageList.Images.Add(Properties.Resources.ProjectWell);    //13
            this.ImageList.Images.Add(Properties.Resources.Contour);        //14
            this.ImageList.Images.Add(Properties.Resources.Profile);        //15
            this.ImageList.Images.Add(Properties.Resources.Marker);         //16
            this.ImageList.Images.Add(Properties.Resources.Grid);           //17
            this.ImageList.Images.Add(Properties.Resources.WellList);       //18
            this.ImageList.Images.Add(Properties.Resources.VoronoiMap);     //19
            this.ImageList.Images.Add(Properties.Resources.PVT);            //20
            this.ImageList.Images.Add(Properties.Resources.Bashneft16);     //21
            this.ImageList.Images.Add(Properties.Resources.NGDU);           //22
            this.ImageList.Images.Add(Properties.Resources.Image);          //23
            this.ImageList.Images.Add(Properties.Resources.Deposit);        //24

            this.DragOver += new DragEventHandler(TreeViewLayers_DragOver);
            this.ItemDrag += new ItemDragEventHandler(TreeViewLayers_ItemDrag);
            this.DragDrop += new DragEventHandler(TreeViewLayers_DragDrop);
        }

        #region ContextMenuStrip

        public ContextMenuStrip cMenuOilField, cMenuTreeView;
        public ContextMenuStrip cMenuSelect;
        public ContextMenuStrip cMenuAlias;

        ToolStripMenuItem cmSK, cmEditMode, cmRotateSK, cmDropCoef, cmDrawWellHead;
        ToolStripMenuItem cmNameEditMode;
        ToolStripMenuItem cmContourLoad, cmChessLoad;
        ToolStripMenuItem cmSort;
        ToolStripMenuItem cmSelectAll, cmDeSelectAll, cmDeSelectOther;
        ToolStripMenuItem cmAliasRemove;

        public ToolStripMenuItem cmSortByNGDU, cmSortByOilField, cmSortByData;

        // constructor context menu
        private void InitializeContextMenus()
        {

            // CmenuUserData - Месторождение
            cMenuOilField = new ContextMenuStrip();
            cmSK = (ToolStripMenuItem)cMenuOilField.Items.Add("");
            cmSK.Text = "Система координат";

            // Режим редактирования
            cmEditMode = (ToolStripMenuItem)cmSK.DropDownItems.Add("");
            cmEditMode.Text = "Режим редактирования";
            cmEditMode.CheckOnClick = true;
            cmEditMode.CheckedChanged += new EventHandler(cmEditMode_CheckedChanged);
            cmEditMode.Click += new EventHandler(cmEditMode_Click);

            // Поворот системы координат
            cmRotateSK = (ToolStripMenuItem)cmSK.DropDownItems.Add("");
            cmRotateSK.Text = "Поворот СК";
            cmRotateSK.CheckOnClick = true;
            cmRotateSK.Enabled = false;
            cmRotateSK.Click += new EventHandler(cmRotateSK_Click);

            // Сбросить систему координат
            cmDropCoef = (ToolStripMenuItem)cmSK.DropDownItems.Add("");
            cmDropCoef.Text = "Сбросить коэффициенты";
            cmDropCoef.Enabled = false;
            cmDropCoef.Click += new EventHandler(cmDropCoef_Click);
            cmSK.Visible = false;

            // CmenuTreeView - Дерево Инспектора
            cMenuTreeView = new ContextMenuStrip();
            cmSort = (ToolStripMenuItem)cMenuTreeView.Items.Add("");
            cmSort.Text = "Сортировка...";
            cmSortByNGDU = (ToolStripMenuItem)cmSort.DropDownItems.Add("");
            cmSortByNGDU.Text = "По НГДУ";
            cmSortByNGDU.Checked = true;
            cmSortByNGDU.Click += new EventHandler(cmSortByNGDU_Click);

            cmSortByOilField = (ToolStripMenuItem)cmSort.DropDownItems.Add("");
            cmSortByOilField.Text = "По месторождениям";
            cmSortByOilField.Click += new EventHandler(cmSortByOilField_Click);

            cmSortByData = (ToolStripMenuItem)cmSort.DropDownItems.Add("");
            cmSortByData.Text = "По типам данных";
            cmSortByData.Click += new EventHandler(cmSortByData_Click);

            cMenuSelect = new ContextMenuStrip();
            // Выбрать все
            cmSelectAll = (ToolStripMenuItem)cMenuSelect.Items.Add("");
            cmSelectAll.Text = "Включить все";
            cmSelectAll.Image = SmartPlus.Properties.Resources.SellAll;
            cmSelectAll.Click += new EventHandler(cmSelectAll_Click);
            // Откл все
            cmDeSelectAll = (ToolStripMenuItem)cMenuSelect.Items.Add("");
            cmDeSelectAll.Text = "Отключить все";
            cmDeSelectAll.Image = SmartPlus.Properties.Resources.DeSellAll;
            cmDeSelectAll.Click += new EventHandler(cmDeSelectAll_Click);
            // Откл остальные
            cmDeSelectOther = (ToolStripMenuItem)cMenuSelect.Items.Add("");
            cmDeSelectOther.Text = "Отключить остальные";
            cmDeSelectOther.Image = SmartPlus.Properties.Resources.DeSellOther;
            cmDeSelectOther.Click += new EventHandler(cmDeSelectOther_Click);

            // CmenuUserData - Месторождение
            cMenuAlias = new ContextMenuStrip();
            cmAliasRemove = (ToolStripMenuItem)cMenuAlias.Items.Add("");
            cmAliasRemove.Text = "Удалить псевдоним";
            cmAliasRemove.Click += new EventHandler(cmAliasRemove_Click);
        }

        // Псевдонимы месторождений
        void cmAliasRemove_Click(object sender, EventArgs e)
        {
            TreeNode tnAlias = this.SelectedNode, tnParent = tnAlias.Parent, ofNode = null;
            if ((tnAlias.Tag == null) && (tnParent != null) && (tnParent.Text.StartsWith("Псевдонимы")))
            {
                OilField of = null;
                if (tnParent.Parent != null) ofNode = tnParent.Parent;
                if ((ofNode != null) && (ofNode.Tag != null))
                {
                    if (((BaseObj)ofNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                    {
                        of = _canv.MapProject.OilFields[((C2DLayer)ofNode.Tag).oilFieldIndex];
                    }
                }
                if (of != null)
                {
                    if (of.Index == _canv.twActiveOilField.ActiveOilFieldIndex)
                    {
                        _canv.RemoveAliasByName(_canv.twActiveOilField.Nodes[0], tnAlias.Text);
                    }
                    of.RemoveAlias(tnAlias.Text);
                    this.BeginUpdate();
                    tnParent.Nodes.Remove(tnAlias);
                    if (tnParent.Nodes.Count == 0) ofNode.Nodes.Remove(tnParent);
                    else
                    {
                        tnParent.Text = "Псевдонимы [" + tnParent.Nodes.Count.ToString() + "]";
                    }
                    this.EndUpdate();

                }
            }
        }

        void SelectNodes(TreeNode node, int SelectType)
        {
            if ((node != null) && (node.Parent != null))
            {
                int index = node.Index;
                TreeNode parent = this.SelectedNode.Parent;
                bool check = (SelectType == 0);
                if (parent.Tag != null)
                {
                    bool callGC = false;
                    for (int i = 0; i < parent.Nodes.Count; i++)
                    {
                        parent.Nodes[i].Checked = check;
                        if (parent.Nodes[i].Tag != null)
                        {
                            ((C2DObject)parent.Nodes[i].Tag).Visible = check;
                            if ((((C2DObject)parent.Nodes[i].Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                (((C2DLayer)parent.Nodes[i].Tag).node != null))
                            {
                                ((C2DLayer)parent.Nodes[i].Tag).node.Checked = check;
                                if ((((C2DLayer)parent.Nodes[i].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID))
                                {
                                    if ((SelectType != 2) || (i != node.Index))
                                    {
                                        Grid grid = (Grid)((C2DLayer)parent.Nodes[i].Tag).ObjectsList[0];
                                        OilField of;
                                        if ((!grid.DataLoaded) && (check))
                                        {
                                            _canv.LoadGridMode = 1;
                                            _canv.LoadGridNode = parent;
                                            for (int j = 0; j < _canv.MapProject.OilFields.Count; j++)
                                            {
                                                of = _canv.MapProject.OilFields[j];
                                                if (of.OilFieldCode == grid.OilFieldCode)
                                                {
                                                    _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE, of, grid.Index);
                                                    break;
                                                }
                                            }
                                        }
                                        else if (!check)
                                        {
                                            grid.FreeDataMemory();
                                            callGC = true;
                                        }
                                    }
                                }
                                else if ((((C2DLayer)parent.Nodes[i].Tag).oilfield != null) && (((C2DLayer)parent.Nodes[i].Tag).Level == -1))
                                {
                                    C2DLayer layer = (C2DLayer)parent.Nodes[i].Tag;
                                    Constant.BASE_OBJ_TYPES_ID type = layer.GetObjTypeIDRecursive();
                                    if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                        (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) &&
                                        (layer.ObjectsList.Count > 0))
                                    {
                                        if (check)
                                        {
                                            _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE, layer);
                                        }
                                        else
                                        {
                                            if ((!_canv.mainForm.worker.IsBusy) || (_canv.mainForm.worker.GetWorkType() != (int)Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE))
                                            {
                                                layer.oilfield.FreeContoursData();
                                                callGC = true;
                                            }
                                            else
                                            {
                                                _canv.mainForm.worker.CancelAsync();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (callGC) GC.GetTotalMemory(true);
                }
                if (SelectType == 2)
                {
                    node.Checked = true;
                    if (node.Tag != null)
                    {
                        ((BaseObj)node.Tag).Visible = true;
                        if ((((BaseObj)node.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                            (((C2DLayer)node.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID))
                        {
                            Grid grid = (Grid)((C2DLayer)node.Tag).ObjectsList[0];
                            OilField of;
                            if (!grid.DataLoaded)
                            {
                                _canv.LoadGridMode = 0;
                                _canv.LoadGridNode = node;
                                for (int j = 0; j < _canv.MapProject.OilFields.Count; j++)
                                {
                                    of = _canv.MapProject.OilFields[j];
                                    if (of.OilFieldCode == grid.OilFieldCode)
                                    {
                                        _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE, of, grid.Index);
                                        break;
                                    }
                                }
                            }
                        }
                        else if ((((C2DLayer)node.Tag).oilfield != null) && (((C2DLayer)node.Tag).Level == -1))
                        {
                            C2DLayer layer = (C2DLayer)node.Tag;
                            Constant.BASE_OBJ_TYPES_ID type = layer.GetObjTypeIDRecursive();
                            if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) &&
                                (layer.ObjectsList.Count > 0))
                            {
                                _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE, layer);
                            }
                        }
                    }
                }
                _canv.DrawLayerList();
            }
        }
        void cmSelectAll_Click(object sender, EventArgs e)
        {
            SelectNodes(this.SelectedNode, 0);
        }
        void cmDeSelectAll_Click(object sender, EventArgs e)
        {
            SelectNodes(this.SelectedNode, 1);
        }
        void cmDeSelectOther_Click(object sender, EventArgs e)
        {
            SelectNodes(this.SelectedNode, 2);
        }

        // установка иконки узла дерева
        private void SetTNImageIndex(TreeNode tn, int ImageIndex)
        {
            tn.ImageIndex = ImageIndex;
            tn.SelectedImageIndex = ImageIndex;
        }

        //CmenuOilField    
        void cmDrawWellHead_Click(object sender, EventArgs e)
        {
            SetDrawWellHead(this.SelectedNode, cmDrawWellHead.Checked);
            _canv.DrawLayerList();
        }
        public void SetDrawWellHead(TreeNode tn, bool DrawWellHead)
        {
            if ((tn.Tag != null) && (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                if (DrawWellHead) SetTNImageIndex(tn, 2);
                else SetTNImageIndex(tn, 0);
                C2DLayer layer = (C2DLayer)tn.Tag;
                layer.DrawWellHead = DrawWellHead;
                _canv.twActiveOilField.UpdateRecursiveTreeView();
            }
        }
        public void SetSKEditMode(TreeNode tn, bool EditMode)
        {
            if (tn != null && (tn.Tag != null) && (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                cmEditMode.Checked = EditMode;
                if (EditMode)
                {
                    SetTNImageIndex(tn, 1);
                    //cmNameEditMode.Checked = false;
                }
                else
                {
                    SetTNImageIndex(tn, 0);
                }
                _canv.Rotate = false;
                if (EditMode)
                    _canv.EditedOilFieldIndex = ((C2DLayer)tn.Tag).oilFieldIndex;
                else
                    _canv.EditedOilFieldIndex = -1;
                _canv.twActiveOilField.UpdateRecursiveTreeView();
                this.Update();
            }
        }
        void cmEditMode_Click(object sender, EventArgs e)
        {
            SetSKEditMode(this.SelectedNode, cmEditMode.Checked);
            _canv.twActiveOilField.SetSKEditCheck(cmEditMode.Checked);
        }
        void cmEditMode_CheckedChanged(object sender, EventArgs e)
        {
            cmDropCoef.Enabled = cmEditMode.Checked;
            cmRotateSK.Enabled = cmEditMode.Checked;
            if (!cmEditMode.Checked) cmRotateSK.Checked = false;
        }

        private void SetEditMode(TreeNode tn, bool EditMode)
        {
            if ((tn.Tag != null) && (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                C2DLayer layer = (C2DLayer)tn.Tag;
                if (layer != null)
                {
                    _canv.Rotate = false;
                    if (EditMode) _canv.EditedLayer = layer;
                    else _canv.EditedLayer = null;
                }
            }
        }

        void cmRotateSK_Click(object sender, EventArgs e)
        {
            _canv.Rotate = cmRotateSK.Checked;
        }
        public void ResetRotate()
        {
            cmRotateSK.Checked = false;
            _canv.ResetRotate();
        }
        public void ResetEditMode()
        {
            //cmEditMode.Checked = false;
            if (cmEditMode != null)
            {
                cmEditMode.PerformClick();
                cmNameEditMode.PerformClick();
            }
        }
        void cmDropCoef_Click(object sender, EventArgs e)
        {
            _canv.ResetSK(((C2DLayer)this.SelectedNode.Tag).Index);
        }

        void cmContourLoad_Click(object sender, EventArgs e)
        {
            //_canv.AddContour(this.SelectedNode.Tag);
        }

        // cMenuTreeView
        private void CheckNodes(TreeNode tnSource, TreeNode tnDest)
        {
            tnDest.Checked = tnSource.Checked;
            if (tnDest.Nodes.Count > 0)
            {
                for (int i = 0; i < tnDest.Nodes.Count; i++)
                {
                    CheckNodes(tnSource.Nodes[i], tnDest.Nodes[i]);
                }
            }
        }

        // Сортировка
        void cmSortByNGDU_Click(object sender, EventArgs e)
        {
            if (!cmSortByNGDU.Checked)
            {
                cmSortByNGDU.Checked = true;
                cmSortByData.Checked = false;
                cmSortByOilField.Checked = false;
                _canv.SortInspectorByNGDU();
            }
        }
        void cmSortByOilField_Click(object sender, EventArgs e)
        {
            if (!cmSortByOilField.Checked)
            {
                cmSortByOilField.Checked = true;
                cmSortByNGDU.Checked = false;
                cmSortByData.Checked = false;
                _canv.SortInspectorByOilField();
            }
        }
        void cmSortByData_Click(object sender, EventArgs e)
        {
            if (!cmSortByData.Checked)
            {
                cmSortByData.Checked = true;
                cmSortByNGDU.Checked = false;
                cmSortByOilField.Checked = false;
                _canv.SortInspectorByDataType();
            }
        }

        // cMenuContour
        void cmNameEditMode_Click(object sender, EventArgs e)
        {
            if (cmNameEditMode.Checked)
            {
                SetTNImageIndex(this.SelectedNode, 1);
                cmEditMode.Checked = false;
            }
            else
            {
                SetTNImageIndex(this.SelectedNode, 0);
                if ((this.SelectedNode.Tag != null) &&
                    (((C2DLayer)this.SelectedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR ||
                     ((C2DLayer)this.SelectedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR))
                {
                    if (((C2DLayer)this.SelectedNode.Tag).oilfield.ContoursEdited)
                        _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_OF_CONTOUR_TO_CACHE, ((C2DLayer)this.SelectedNode.Tag).oilfield);
                }
            }
            SetEditMode(this.SelectedNode, cmNameEditMode.Checked);
            this.Update();
        }
        #endregion

        #region DragDrop
        void TreeViewLayers_DragDrop(object sender, DragEventArgs e)
        {
            Point pointClick = this.PointToClient(Cursor.Position);
            TreeNode targetNode = this.GetNodeAt(pointClick);
        }
        void TreeViewLayers_ItemDrag(object sender, ItemDragEventArgs e)
        {
            dragNode = (TreeNode)e.Item;
            this.SelectedNode = dragNode;
            DoDragDrop(dragNode, DragDropEffects.Move);
        }
        void TreeViewLayers_DragOver(object sender, DragEventArgs e)
        {
            Point pointClick = this.PointToClient(Cursor.Position);
            TreeNode targetNode = this.GetNodeAt(pointClick);
            e.Effect = DragDropEffects.Move;
        }
        #endregion

        public void CheckState(TreeNode CheckNode)
        {
            if (CheckNode != null)
            {
                for (int i = 0; i < CheckNode.Nodes.Count; i++)
                {
                    if (CheckNode.Nodes[i].Nodes.Count > 0)
                        CheckState(CheckNode.Nodes[i]);
                    else
                    {
                        if (CheckNode.Nodes[i].Tag != null)
                            CheckNode.Nodes[i].Checked = ((BaseObj)CheckNode.Nodes[i].Tag).Visible;
                    }
                }
            }
            if (CheckNode.Tag != null) CheckNode.Checked = ((BaseObj)CheckNode.Tag).Visible;
        }

        // Load Data Layers
        public TreeNode GetNGDUNodeByCode(TreeNode ParentNode, int NGDUcode)
        {
            if ((ParentNode != null) && (ParentNode.Nodes.Count > 0))
            {
                int i = 0;
                while (i < ParentNode.Nodes.Count)
                {
                    if (((C2DLayer)ParentNode.Nodes[i].Tag).ngduCode == NGDUcode)
                    {
                        return ParentNode.Nodes[i];
                    }
                    i++;
                }
            }
            return null;
        }
        public TreeNode InsertNGDUByCode(TreeNode ParentNode, string NGDUname, int NGDUcode)
        {
            TreeNode tnNGDU = null;
            if (ParentNode != null)
            {
                if (ParentNode.Nodes.Count == 0)
                {
                    tnNGDU = ParentNode.Nodes.Add("", NGDUname, 22, 22);
                }
                else
                {
                    int j = 0;
                    bool ins = false;
                    while (j < ParentNode.Nodes.Count)
                    {
                        if (((C2DLayer)ParentNode.Nodes[j].Tag).ngduCode > NGDUcode)
                        {
                            tnNGDU = ParentNode.Nodes.Insert(j, "", NGDUname, 22, 22);
                            ins = true;
                            break;
                        }
                        j++;
                    }
                    if (!ins)
                    {
                        tnNGDU = ParentNode.Nodes.Add("", NGDUname, 22, 22);
                        tnNGDU.Checked = true;
                    }
                }
            }
            return tnNGDU;
        }
        public TreeNode GetOFNodeByCode(TreeNode ParentNode, int OilFieldIndex)
        {
            if ((ParentNode != null) && (ParentNode.Nodes.Count > 0))
            {
                int i = 0;
                while (i < ParentNode.Nodes.Count)
                {
                    if (((C2DLayer)ParentNode.Nodes[i].Tag).oilFieldIndex == OilFieldIndex)
                    {
                        return ParentNode.Nodes[i];
                    }
                    i++;
                }
            }
            return null;
        }
        public TreeNode InsertOFByCode(TreeNode ParentNode, string OilFieldName, int OilFieldIndex)
        {
            TreeNode tnOilField = null;
            if (ParentNode != null)
            {
                if (ParentNode.Nodes.Count == 0)
                {
                    tnOilField = ParentNode.Nodes.Add("", OilFieldName, 9, 9);
                }
                else
                {
                    int j = 0;
                    bool ins = false;
                    while (j < ParentNode.Nodes.Count)
                    {
                        if (((C2DLayer)ParentNode.Nodes[j].Tag).oilFieldIndex > OilFieldIndex)
                        {
                            tnOilField = ParentNode.Nodes.Insert(j, "", OilFieldName, 9, 9);
                            ins = true;
                            break;
                        }
                        j++;
                    }
                    if (!ins)
                    {
                        tnOilField = ParentNode.Nodes.Add("", OilFieldName, 9, 9);
                    }
                }
            }
            return tnOilField;
        }
        public TreeNode GetDataNodeByID(TreeNode ParentNode, Constant.BASE_OBJ_TYPES_ID ObjTypeID)
        {
            if ((ParentNode != null) && (ParentNode.Nodes.Count > 0))
            {
                int i = 0;
                while (i < ParentNode.Nodes.Count)
                {
                    if (((C2DLayer)ParentNode.Nodes[i].Tag).GetObjTypeIDRecursive() == ObjTypeID)
                    {
                        return ParentNode.Nodes[i];
                    }
                    i++;
                }
            }
            return null;
        }
        public TreeNode InsertDataNodeByID(TreeNode ParentNode, Constant.BASE_OBJ_TYPES_ID ObjTypeID)
        {
            TreeNode tnDataNode = null;
            string DataName = "";
            int ImgIndex;
            switch (ObjTypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    DataName = "Ячейки";
                    ImgIndex = 10;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                    DataName = "Условные контуры";
                    ImgIndex = 14;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    DataName = "Контуры";
                    ImgIndex = 14;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                    DataName = "Кусты";
                    ImgIndex = 11;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    DataName = "Скважины";
                    ImgIndex = 12;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                    DataName = "Проектные скважины";
                    ImgIndex = 13;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION:
                    DataName = "Обустройство";
                    ImgIndex = 0;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.GRID:
                    DataName = "Сетки";
                    ImgIndex = 17;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                    DataName = "Залежи";
                    ImgIndex = 24;
                    break;
                default:
                    DataName = "Неизвестно";
                    ImgIndex = 0;
                    break;
            }
            if (ParentNode != null)
            {
                if (ParentNode.Nodes.Count == 0)
                {
                    tnDataNode = ParentNode.Nodes.Add("", DataName, ImgIndex, ImgIndex);
                }
                else
                {
                    int j = 0;
                    bool ins = false;
                    while (j < ParentNode.Nodes.Count)
                    {
                        if (((C2DLayer)ParentNode.Nodes[j].Tag).GetObjTypeIDRecursive() > ObjTypeID)
                        {
                            tnDataNode = ParentNode.Nodes.Insert(j, "", DataName, ImgIndex, ImgIndex);
                            ins = true;
                            break;
                        }
                        j++;
                    }
                    if (!ins)
                    {
                        tnDataNode = ParentNode.Nodes.Add("", DataName, ImgIndex, ImgIndex);
                    }
                }
            }
            tnDataNode.ContextMenuStrip = cMenuSelect;
            return tnDataNode;
        }

        // other
        public void StartDataUpdate()
        {
            this.BeginUpdate();
            //_bLoadChildNodes = true;
        }
        public void EndDataUpdate()
        {
            //this._bLoadChildNodes = false;
            this.EndUpdate();
        }
        public void Clear()
        {
            cmSortByNGDU.Checked = true;
            cmSortByData.Checked = false;
            cmSortByOilField.Checked = false;
            this.Nodes.Clear();
        }

        public void UpdateAreasNodeVisible()
        {
            if (this.Nodes.Count > 0 && _canv.mainForm.AppSettings != null && (!_canv.mainForm.AppSettings.ShowAllOilfieldAreas))
            {
                RecursiveUpdateAreasNodes(this.Nodes[0]);
            }
        }
        private void RecursiveUpdateAreasNodes(TreeNode Node)
        {
            if ((Node.Nodes.Count > 0) && (Node.Tag != null))
            {
                if (((BaseObj)Node.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER &&
                    ((C2DLayer)Node.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER && (((C2DLayer)Node.Tag).ObjectsList.Count > 0) &&
                    (((C2DLayer)((C2DLayer)Node.Tag).ObjectsList[0]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA))
                {
                    C2DLayer layer = (C2DLayer)Node.Tag, lr;
                    bool find = false;
                    for (int i = 0; i < layer.ObjectsList.Count; i++)
                    {
                        lr = (C2DLayer)layer.ObjectsList[i];
                        if (lr.Visible)
                        {
                            if (!find)
                            {
                                find = true;
                            }
                            else
                            {
                                lr.Visible = false;
                                if (lr.node != null) lr.node.Checked = false;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < Node.Nodes.Count; i++)
                    {
                        RecursiveUpdateAreasNodes(Node.Nodes[i]);
                    }
                }
            }
        }

        int GetImageIndex(Constant.BASE_OBJ_TYPES_ID Type)
        {
            switch (Type)
            {
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    return 9;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    return 10;
                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                    return 11;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    return 12;
                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                    return 13;
                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                    return 14;
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    return 14;
                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                    return 15;
                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                    return 16;
                case Constant.BASE_OBJ_TYPES_ID.GRID:
                    return 17;
                case Constant.BASE_OBJ_TYPES_ID.WELL_LIST:
                    return 18;
                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                    return 18;
                case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                    return 19;
                case Constant.BASE_OBJ_TYPES_ID.PVT:
                    return 20;
                case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                    return 23;
                case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                    return 24;
                default:
                    return 0;
            }
        }

        // Обработка методов TreeView
        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == 515)
            { /* WM_LBUTTONDBLCLK */
            }
            else
                base.DefWndProc(ref m);
        }
        void TreeViewLayers_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            string text;
            TreeNode tnBase = e.Node, tn, tn2;
            int imgIndex;
            if ((tnBase.Tag != null)
                && (tnBase.Nodes.Count < 2)
                && (((BaseObj)tnBase.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                && ((((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL) ||
                    (((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) ||
                    (((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL_PAD) ||
                    (((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION) ||
                    (((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                && (tnBase.Nodes[0].Text == "Empty"))
            {
                C2DLayer layer = (C2DLayer)tnBase.Tag;
                this.StartDataUpdate();
                tnBase.Nodes.Clear();
                imgIndex = GetImageIndex(layer.ObjTypeID);
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) imgIndex = GetImageIndex(((C2DLayer)layer.ObjectsList[i]).ObjTypeID);
                    tn = tnBase.Nodes.Add("", ((C2DObject)layer.ObjectsList[i]).Name, imgIndex, imgIndex);
                    tn.Tag = (C2DObject)layer.ObjectsList[i];
                    tn.Checked = ((C2DObject)layer.ObjectsList[i]).Visible;
                    tn.ContextMenuStrip = cMenuSelect;

                    if ((((C2DObject)layer.ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                        (((C2DLayer)layer.ObjectsList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                        (((C2DLayer)layer.ObjectsList[i]).ObjectsList.Count > 0))
                        tn.Nodes.Add("Empty");
                    if ((((C2DObject)layer.ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.WELL))
                        tn.Nodes.Add("Empty");
                }
                this.EndDataUpdate();
            }
            else if ((tnBase.Tag != null)
                && (tnBase.Nodes.Count < 2)
                && (((BaseObj)tnBase.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                && (tnBase.Parent != null) && (tnBase.Parent.Tag != null) && (((C2DLayer)tnBase.Parent.Tag).ObjTypeID != Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL)
                && (tnBase.Nodes[0].Text == "Empty"))
            {
                var dictLogs = (WellLogMnemonicDictionary)_canv.MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LOG_MNEMONIC);
                this.StartDataUpdate();
                tnBase.Nodes.Clear();
                Well w = (Well)tnBase.Tag;
                OilField of = _canv.MapProject.OilFields[w.OilFieldIndex];
                if (w.CoordLoaded)
                {
                    tn = tnBase.Nodes.Add("Координаты");
                    tn.Checked = true;
                }
                of.LoadMerFromCache(w.Index, true);
                if (w.MerLoaded)
                {
                    tn = tnBase.Nodes.Add("МЭР");
                    tn.Checked = true;
                }
                of.LoadGisFromCache(w.Index, true);
                if (w.GisLoaded)
                {
                    tn = tnBase.Nodes.Add("ГИС");
                    tn.Checked = true;
                }
                of.LoadChessFromCache(w.Index, true);
                if (w.ChessLoaded)
                {
                    tn = tnBase.Nodes.Add("Шахматка");
                    tn.Checked = true;
                }
                of.LoadCoreFromCache(w.Index, true);
                of.LoadCoreTestFromCache(w.Index, true);
                if ((w.CoreLoaded) || (w.CoreTestLoaded))
                {
                    tn = tnBase.Nodes.Add("Керн");
                    tn.Checked = true;
                    if (w.CoreLoaded)
                    {
                        tn2 = tn.Nodes.Add("Наличие керна");
                        tn2.Checked = true;
                        w.core = null;
                    }
                    if (w.CoreTestLoaded)
                    {
                        tn2 = tn.Nodes.Add("Исследования керна");
                        tn2.Checked = true;
                        w.coreTest = null;
                    }
                }
                of.LoadLogsFromCache(w.Index, false, true);
                of.LoadLogsBaseFromCache(w.Index, false, true);
                if ((w.LogsLoaded) || (w.LogsBaseLoaded))
                {
                    tn = tnBase.Nodes.Add("Каротаж");
                    tn.Checked = true;
                    if (w.LogsBaseLoaded)
                    {
                        for (int i = 0; i < w.logsBase.Length; i++)
                        {
                            text = dictLogs.GetShortNameByCode(w.logsBase[i].MnemonikaId);
                            if (text.Length > 0)
                            {
                                tn2 = tn.Nodes.Add(text + " [Базовый]");
                                tn2.Checked = true;
                            }
                        }
                        w.logsBase = null;
                    }
                    if (w.LogsLoaded)
                    {
                        for (int i = 0; i < w.logs.Length; i++)
                        {
                            text = dictLogs.GetShortNameByCode(w.logs[i].MnemonikaId);
                            if (text.Length > 0)
                            {
                                tn2 = tn.Nodes.Add(text + " [Базовый]");
                                tn2.Checked = true;
                            }
                        }
                        w.logs = null;
                    }
                }
                of.ClearBufferList(false);
                this.EndDataUpdate();
            }
        }
        void TreeViewLayers_MouseDown(object sender, MouseEventArgs e)
        {
            TreeView tw = (TreeView)sender;
            TreeNode clickedNode = tw.GetNodeAt(e.Location);
            C2DLayer layer;
           // _canv.// mainForm.StatUsage.AddMessage(CollectorStatId.INSPECTOR_MOUSE_DOWN);
            if (clickedNode != null)
            {
                if ((e.Location.X >= clickedNode.Bounds.X - 32) && (e.Location.X < clickedNode.Bounds.X - 16))
                {
                    bool check, res;
                    check = !clickedNode.Checked;
                    if (clickedNode.Tag != null) ((BaseObj)clickedNode.Tag).Visible = check;

                    #region Shift + Click
                    //if (((Control.ModifierKeys & Keys.Shift) == Keys.Shift) &&
                    //    (clickedNode.Nodes.Count > 0) &&
                    //    (((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    //{
                    //    callGC = false;
                    //    for (int i = 0; i < clickedNode.Nodes.Count; i++)
                    //    {
                    //        if ((C2DObject)clickedNode.Nodes[i].Tag != null)
                    //        {
                    //            ((C2DObject)clickedNode.Nodes[i].Tag).Visible = check;
                    //            clickedNode.Nodes[i].Checked = check;
                    //            if ((((C2DLayer)clickedNode.Nodes[i].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID))
                    //            {
                    //                GridEx grid = (GridEx)((C2DLayer)clickedNode.Nodes[i].Tag)._ObjectsList[0];
                    //                OilField of;
                    //                if ((!grid.DataLoaded) && (check))
                    //                {
                    //                    _canv.LoadGridMode = 1;
                    //                    _canv.LoadGridNode = clickedNode;
                    //                    for (int j = 0; j < _canv.MapProject.OilFields.Count; j++)
                    //                    {
                    //                        of = _canv.MapProject.OilFields[j];
                    //                        if (of.OilFieldCode == grid.OilFieldCode)
                    //                        {
                    //                            _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE, of, grid.Index);
                    //                            break;
                    //                        }
                    //                    }
                    //                }
                    //                else if (!check)
                    //                {
                    //                    grid.FreeDataMemory();
                    //                    callGC = true;
                    //                }
                    //            }
                    //        }
                    //    }
                    //    if (callGC) GC.GetTotalMemory(true);
                    //}
                    #endregion
                    if ((clickedNode.Tag != null) && ((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                    {
                        layer = (C2DLayer)clickedNode.Tag;
                        if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
                        {
                            if (_canv.mainForm.AppSettings != null && (!_canv.mainForm.AppSettings.ShowAllOilfieldAreas))
                            {
                                TreeNode parent = clickedNode.Parent;
                                if (parent != null && parent.Tag != null && ((BaseObj)parent.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                                {
                                    C2DLayer parentLayer = (C2DLayer)parent.Tag, layer2;
                                    if (parentLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                                    {
                                        for (int j = 0; j < parentLayer.ObjectsList.Count; j++)
                                        {
                                            layer2 = (C2DLayer)parentLayer.ObjectsList[j];
                                            if (layer2 != layer)
                                            {
                                                layer2.Visible = false;
                                                if (layer2.node != null) layer2.node.Checked = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                        {
                            Grid grid = (Grid)layer.ObjectsList[0];
                            OilField of;
                            if ((!grid.DataLoaded) && (check))
                            {
                                _canv.LoadGridMode = 0;
                                _canv.LoadGridNode = clickedNode;
                                for (int j = 0; j < _canv.MapProject.OilFields.Count; j++)
                                {
                                    of = _canv.MapProject.OilFields[j];
                                    if (of.OilFieldCode == grid.OilFieldCode)
                                    {
                                        _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE, of, grid.Index);
                                        break;
                                    }
                                }
                            }
                            else if (!check)
                            {
                                grid.FreeDataMemory();
                                GC.GetTotalMemory(true);
                            }
                        }
                        else if ((layer.oilfield != null) && (layer.Level == -1))
                        {
                            Constant.BASE_OBJ_TYPES_ID type = layer.GetObjTypeIDRecursive();
                            if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) &&
                                (layer.ObjectsList.Count > 0))
                            {
                                if (check)
                                {
                                    _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE, layer);
                                }
                                else
                                {
                                    if ((!_canv.mainForm.worker.IsBusy) || (_canv.mainForm.worker.GetWorkType() != (int)Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE))
                                    {
                                        layer.oilfield.FreeContoursData();
                                        GC.GetTotalMemory(true);
                                    }
                                    else
                                    {
                                        _canv.mainForm.worker.CancelAsync();
                                    }
                                }
                            }
                        }
                    }
                    _canv.DrawLayerList();
                    clickedNode.Checked = check;
                }
                else
                {
                    if (e.Button == MouseButtons.Right)
                    {
                        cmSelectAll.Visible = true;
                        cmDeSelectAll.Visible = true;
                        if (_canv.mainForm.AppSettings != null &&
                            (!_canv.mainForm.AppSettings.ShowAllOilfieldAreas) &&
                            (clickedNode.Tag != null) &&
                            (((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                            (((C2DLayer)clickedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA))
                        {
                            cmSelectAll.Visible = false;
                            cmDeSelectAll.Visible = false;
                        }
                    }
                    tw.SelectedNode = clickedNode;
                    if ((clickedNode.Tag != null) &&
                        (((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    {
                        layer = (C2DLayer)clickedNode.Tag;
                        if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                        {
                            //cmDrawWellHead.Enabled = true;
                            //cmDrawWellHead.Checked = layer.DrawWellHead;
                        }
                    }
                }
                tw.SelectedNode = clickedNode;
                _canv.twActiveOilField.UpdateRecursiveTreeView();
            }
        }
        void TreeViewLayers_AfterSelect(object sender, TreeViewEventArgs e)
        {
            /*
            this.BeginUpdate();
            if ((e.Node.Tag != null) && (((BaseObj)e.Node.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)) {
                for (int i = 0; i < _canv.LayerList.Count; i++) ((C2DLayer)_canv.LayerList[i]).Selected = false;
                ((C2DLayer)e.Node.Tag).Selected = true;
            }
           // _canv.DrawLayerList();
            this.EndUpdate();
             */
        }
        void TreeViewLayers_DoubleClick(object sender, EventArgs e)
        {
        }
    }
}
