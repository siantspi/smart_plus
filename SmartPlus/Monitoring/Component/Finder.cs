﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    class Finder : Form
    {
        CanvasMap canv;
        Font mainFont, boldFont;
        BackgroundWorker worker;
        List<Area>[] areaLists;
        List<Well>[] wellLists;
        List<OilField> oilFieldList;
        string LastFindString;
        int LastActiveOilFieldIndex;
        float heightRow;
        VScrollBar scroll;

        int MaxVisbleRows;
        int mouseRowInd;
        PictureBox box;
        public int Count
        {
            get
            {
                try
                {
                    if (oilFieldList != null)
                    {
                        return oilFieldList.Count;
                    }
                    else if (wellLists != null)
                    {
                        int sum = 0;
                        for (int i = 0; i < 4; i++)
                        {
                            if (wellLists[i] != null) sum += wellLists[i].Count;
                        }
                        return sum;
                    }
                    else if (areaLists != null)
                    {
                        int sum = 0;
                        for (int i = 0; i < 2; i++)
                        {
                            if (areaLists[i] != null) sum += areaLists[i].Count;
                        }
                        return sum;
                    }
                }
                catch
                {
                }
                return 0;
            }
        }
        Rectangle Bounds;
        Timer tmrLeave;
        OilFieldAreaDictionary dictArea = null;
        StratumDictionary stratumDict = null;

        public Finder(CanvasMap canvMap)
        {
            this.StartPosition = FormStartPosition.Manual;
            this.FormBorderStyle = FormBorderStyle.None;
            this.ShowInTaskbar = false;
            this.BackColor = Color.White;
            this.mainFont = new Font("Tahoma", 8.25F);
            this.boldFont = new Font("Tahoma", 8.75F, FontStyle.Bold);
            this.worker = new BackgroundWorker();
            this.areaLists = null;
            this.wellLists = null;
            this.oilFieldList = null;
            this.canv = canvMap;
            this.Visible = false;
            this.LastFindString = string.Empty;
            LastActiveOilFieldIndex = -1;
            this.MaxVisbleRows = 15;
            this.mouseRowInd = -1;
            heightRow = 0;
            this.scroll = new VScrollBar();
            this.Controls.Add(scroll);
            this.scroll.Dock = DockStyle.Right;
            box = new PictureBox();
            box.Width = this.Width - scroll.Width;
            this.Controls.Add(box);

            Graphics grfx = box.CreateGraphics();
            heightRow = (int)Math.Floor(grfx.MeasureString("ЙЩ", boldFont, PointF.Empty, StringFormat.GenericTypographic).Height);
            grfx.Dispose();

            this.Width = 400;
            this.Height = (int)Math.Floor(MaxVisbleRows * heightRow);

            this.worker.WorkerSupportsCancellation = true;
            
            this.worker.DoWork += new DoWorkEventHandler(workerFind);
            this.worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(workerFind_Stop);
            
            this.scroll.ValueChanged += new EventHandler(scroll_ValueChanged);
            this.MouseWheel += new MouseEventHandler(Finder_MouseWheel);
            box.MouseLeave += new EventHandler(box_MouseLeave);
            scroll.MouseLeave += new EventHandler(scroll_MouseLeave);
            box.MouseMove += new MouseEventHandler(box_MouseMove);
            box.MouseDown += new MouseEventHandler(box_MouseDown);
            box.Paint += new PaintEventHandler(box_Paint);

            //TIMER
            tmrLeave = new Timer();
            tmrLeave.Enabled = false;
            tmrLeave.Interval = 500;
            tmrLeave.Tick += new EventHandler(tmrLeave_Tick);
        }

        public void Clear()
        {
            tmrLeave.Stop();
            if (areaLists != null)
            {
                for (int i = 0; i < 2; i++) areaLists[i] = null;
                areaLists = null;
            }
            if (wellLists != null)
            {
                for (int i = 0; i < 4; i++) wellLists[i] = null;
                wellLists = null;
            }
            oilFieldList = null;
            LastFindString = string.Empty;
            LastActiveOilFieldIndex = -1;
        }

        public void StartFind(string findStr)
        {
            findStr = findStr.Trim();
            this.Visible = false;
            
            if (worker.IsBusy)
            {
                worker.CancelAsync();
            }
            else
            {
                Clear();
                worker.RunWorkerAsync(findStr);
            }
            LastFindString = findStr;
        }
        public List<Well>[] FindWell(BackgroundWorker worker, DoWorkEventArgs e, string WellName, string OilFieldName)
        {
            List<Well>[] retArray = new List<Well>[4];
            LastActiveOilFieldIndex = -1;
            if ((canv != null) && (canv.MapProject != null) && (canv.MapProject.OilFields != null) && (canv.MapProject.OilFields.Count > 1))
            {
                OilField of;
                Well w;
                // поиск по имени скважины
                string temp;
                char[] digits = new char[10] { '0','1','2','3','4','5','6','7','8','9' };
                if (canv.twActiveOilField.ActiveOilFieldIndex != -1) // сначала ищем в активном месторождении
                {
                    of = canv.MapProject.OilFields[canv.twActiveOilField.ActiveOilFieldIndex];
                    if ((OilFieldName.Length == 0) || (of.Name.StartsWith(OilFieldName, StringComparison.OrdinalIgnoreCase)))
                    {
                        if ((of.Wells != null) && (of.Wells.Count > 0))
                        {
                            for (int j = 0; j < of.Wells.Count; j++)
                            {
                                w = of.Wells[j];
                                if (w.Missing) continue;
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return null;
                                }
                                // поиск по полному совпадению
                                if (w.UpperCaseName.Equals(WellName, StringComparison.OrdinalIgnoreCase))
                                {
                                    if (retArray[0] == null) retArray[0] = new List<Well>();
                                    retArray[0].Add(of.Wells[j]);
                                }
                            }
                        }
                        if (of.ProjWells != null && of.ProjWells.Count > 0)
                        {
                            for (int j = 0; j < of.ProjWells.Count; j++)
                            {
                                w = of.ProjWells[j];
                                if (w.Missing) continue;
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return null;
                                }
                                // поиск по полному совпадению
                                if (w.UpperCaseName.Equals(WellName, StringComparison.OrdinalIgnoreCase))
                                {
                                    if (retArray[0] == null) retArray[0] = new List<Well>();
                                    retArray[0].Add(w);
                                }
                            }
                        }
                    }
                    LastActiveOilFieldIndex = canv.twActiveOilField.ActiveOilFieldIndex;
                }
                for (int i = 1; i < canv.MapProject.OilFields.Count; i++)
                {
                    of = canv.MapProject.OilFields[i];
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return null;
                    }
                    if ((OilFieldName != "") && (!of.Name.StartsWith(OilFieldName, StringComparison.OrdinalIgnoreCase)))
                    {
                        continue;
                    }
                    if ((of.Wells != null) && (of.Wells.Count > 0))
                    {
                        for (int j = 0; j < of.Wells.Count; j++)
                        {
                            w = of.Wells[j];
                            if (w.Missing) continue;
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return null;
                            }
                            // поиск по полному совпадению
                            if (w.UpperCaseName.Equals(WellName, StringComparison.OrdinalIgnoreCase))
                            {
                                if ((retArray[0] == null) || (retArray[0].IndexOf(w) == -1))
                                {
                                    if (retArray[1] == null) retArray[1] = new List<Well>();
                                    retArray[1].Add(w);
                                }
                            }
                            else if (w.UpperCaseName.StartsWith(WellName, StringComparison.OrdinalIgnoreCase))
                            {
                                temp = w.UpperCaseName.Remove(0, WellName.Length);
                                if (temp.IndexOfAny(digits) != 0)
                                {
                                    if (retArray[2] == null) retArray[2] = new List<Well>();
                                    retArray[2].Add(w);
                                }
                                else
                                {
                                    if (retArray[3] == null) retArray[3] = new List<Well>();
                                    retArray[3].Add(w);
                                }
                            }
                        }
                    }
                    if ((of.ProjWells != null) && (of.ProjWells.Count > 0))
                    {
                        for (int j = 0; j < of.ProjWells.Count; j++)
                        {
                            w = of.ProjWells[j];
                            if (w.Missing) continue;
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return null;
                            }
                            // поиск по полному совпадению
                            if (w.UpperCaseName.Equals(WellName, StringComparison.OrdinalIgnoreCase))
                            {
                                if ((retArray[0] == null) || (retArray[0].IndexOf(w) == -1))
                                {
                                    if (retArray[1] == null) retArray[1] = new List<Well>();
                                    retArray[1].Add(w);
                                }
                            }
                            else if (w.UpperCaseName.StartsWith(WellName, StringComparison.OrdinalIgnoreCase))
                            {
                                temp = w.UpperCaseName.Remove(0, WellName.Length);
                                if (temp.IndexOfAny(digits) != 0)
                                {
                                    if (retArray[2] == null) retArray[2] = new List<Well>();
                                    retArray[2].Add(w);
                                }
                                else
                                {
                                    if (retArray[3] == null) retArray[3] = new List<Well>();
                                    retArray[3].Add(w);
                                }
                            }
                        }
                    }
                }
            }
            return retArray;
        }
        public List<OilField> FindOilField(BackgroundWorker worker, DoWorkEventArgs e, string OilFieldName)
        {
            List<OilField> retArray = null;
            if ((canv.MapProject != null) && (canv.MapProject.OilFields != null) && (canv.MapProject.OilFields.Count > 1))
            {
                for (int i = 1; i < canv.MapProject.OilFields.Count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return null;
                    }
                    if (canv.MapProject.OilFields[i].Name.StartsWith(OilFieldName, StringComparison.OrdinalIgnoreCase))
                    {
                        if (retArray == null) retArray = new List<OilField>();
                        retArray.Add(canv.MapProject.OilFields[i]);
                    }
                }
            }
            return retArray;
        }
        public List<Area>[] FindOilfieldArea(BackgroundWorker worker, DoWorkEventArgs e, string AreaName, string OilFieldName)
        {
            List<Area>[] retArray = new List<Area>[2];
            LastActiveOilFieldIndex = -1;
            if ((canv != null) && (canv.MapProject != null) && (canv.MapProject.OilFields != null) && (canv.MapProject.OilFields.Count > 1))
            {
                OilField of;
                Area area;
                bool needTestAreaName2 = false;
                string AreaName2 = string.Empty;
                if (AreaName.IndexOf("_C", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    AreaName2 = AreaName.ToUpper().Replace("_C", "_С");
                    needTestAreaName2 = true;
                }
                else if (AreaName.IndexOf("_С", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    AreaName2 = AreaName.ToUpper().Replace("_С", "_C");
                    needTestAreaName2 = true;
                }
                // поиск по имени скважины
                if (canv.twActiveOilField.ActiveOilFieldIndex != -1) // сначала ищем в активном месторождении
                {
                    of = canv.MapProject.OilFields[canv.twActiveOilField.ActiveOilFieldIndex];
                    if ((OilFieldName.Length == 0) || (of.Name.StartsWith(OilFieldName, StringComparison.OrdinalIgnoreCase)))
                    {
                        if ((of.Areas != null) && (of.Areas.Count > 0))
                        {
                            for (int j = 0; j < of.Areas.Count; j++)
                            {
                                area = of.Areas[j];
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return null;
                                }
                                
                                // поиск только по неполному совпадению
                                if (area.Name.StartsWith(AreaName, StringComparison.OrdinalIgnoreCase))
                                {
                                    if (retArray[0] == null) retArray[0] = new List<Area>();
                                    retArray[0].Add(of.Areas[j]);
                                }
                                else if (needTestAreaName2 && area.Name.StartsWith(AreaName2, StringComparison.OrdinalIgnoreCase))
                                {
                                    if (retArray[0] == null) retArray[0] = new List<Area>();
                                    retArray[0].Add(of.Areas[j]);
                                }
                            }
                        }
                    }
                    LastActiveOilFieldIndex = canv.twActiveOilField.ActiveOilFieldIndex;
                }
                for (int i = 1; i < canv.MapProject.OilFields.Count; i++)
                {
                    of = canv.MapProject.OilFields[i];
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return null;
                    }
                    if ((OilFieldName.Length > 0) && (!of.Name.StartsWith(OilFieldName, StringComparison.OrdinalIgnoreCase)))
                    {
                        continue;
                    }
                    if ((of.Areas != null) && (of.Areas.Count > 0))
                    {
                        for (int j = 0; j < of.Areas.Count; j++)
                        {
                            area = of.Areas[j];
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return null;
                            }
                            // поиск только по неполному совпадению
                            if (area.Name.StartsWith(AreaName, StringComparison.OrdinalIgnoreCase))
                            {
                                if ((retArray[0] == null) || (retArray[0].IndexOf(area) == -1))
                                {
                                    if (retArray[1] == null) retArray[1] = new List<Area>();
                                    retArray[1].Add(area);
                                }
                            }
                            else if (needTestAreaName2 && area.Name.StartsWith(AreaName2, StringComparison.OrdinalIgnoreCase))
                            {
                                if ((retArray[0] == null) || (retArray[0].IndexOf(area) == -1))
                                {
                                    if (retArray[1] == null) retArray[1] = new List<Area>();
                                    retArray[1].Add(area);
                                }
                            }
                        }
                    }
                }
            }
            return retArray;
        }

        void workerFind(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.CurrentThread.Priority = System.Threading.ThreadPriority.AboveNormal;
            if (e.Argument != null)
            {
                int index, i, j, index2;
                string findStr = (string)e.Argument;
                char[] parser = new char[] { ' ' };
                string[] strList = findStr.Split(parser);
                if ((strList.Length > 0) && (strList[0].Length > 0) && (canv != null) && (canv.MapProject != null) && (!canv._disabled))
                {
                    char[] numbList = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                    index = strList[0].IndexOfAny(numbList, 0);
                    index2 = -1;
                    if (strList.Length > 1) index2 = strList[1].IndexOfAny(numbList, 0);
                    string objectName, ofName;
                    
                    
                    if (index > -1 || index2 > -1)
                    {
                        objectName = strList[0];
                        ofName = string.Empty;
                        for (i = 1; i < strList.Length; i++)
                        {
                            if (strList[i].Length > 0)
                            {
                                ofName = strList[i];
                                break;
                            }
                        }
                        if ((index == -1) && (index2 > -1))
                        {
                            objectName = ofName;
                            ofName = strList[0];
                        }
                        int pos = objectName.IndexOf('_');
                        areaLists = null;
                        if (pos > 0 && pos < objectName.Length)
                        {
                            index = objectName.Remove(0, pos + 1).IndexOfAny(numbList, 0);
                            if (index == -1 && (objectName.Length > pos + 1))
                            {
                                areaLists = FindOilfieldArea(worker, e, objectName, ofName);
                            }
                        }
                        if (areaLists == null)
                        {
                            wellLists = FindWell(worker, e, objectName, ofName);
                        }
                    }
                    else
                    {
                        oilFieldList = FindOilField(worker, e, strList[0]);
                    }
                }
            }
        }
        void workerFind_Stop(object sender, RunWorkerCompletedEventArgs e)
        {
           if (e.Cancelled)
            {
                if (LastFindString.Length > 0)
                {
                    tmrLeave.Stop();
                    if (areaLists != null)
                    {
                        for (int i = 0; i < 2; i++) areaLists[i] = null;
                        areaLists = null;
                    }
                    if (wellLists != null)
                    {
                        for (int i = 0; i < 4; i++) wellLists[i] = null;
                        wellLists = null;
                    }
                    oilFieldList = null;
                    worker.RunWorkerAsync(LastFindString);
                }
            }
            else
            {
                if (oilFieldList != null)
                {
                    this.ShowForm();
                }
                else if (wellLists != null)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        if (wellLists[i] != null)
                        {
                            this.ShowForm();
                            break;
                        }
                    }
                }
                else if (areaLists != null)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        if (areaLists[i] != null)
                        {
                            this.ShowForm();
                            break;
                        }
                    }
                }
            }
        }

        public void KeyPress(Keys KeyCode)
        {
            if ((Count > 0) && (canv != null) && (canv.mainForm != null) && (canv.mainForm.tbFinderEx != null))
            {
                int count = Count;
                if (scroll.Visible) count = MaxVisbleRows;
                switch (KeyCode)
                {
                    case Keys.Up:
                        mouseRowInd--;
                        if (mouseRowInd < 0) mouseRowInd = 0;
                        if (scroll.Visible)
                        {
                            if (mouseRowInd < scroll.Value) scroll.Value = mouseRowInd;
                        }
                        box.Invalidate();
                        break;
                    case Keys.Down:
                        mouseRowInd++;
                        if (mouseRowInd > Count - 1) mouseRowInd = Count - 1;
                        if (scroll.Visible)
                        {
                            if (mouseRowInd > scroll.Value + MaxVisbleRows - 1) scroll.Value = mouseRowInd - MaxVisbleRows + 1;
                            if (mouseRowInd < scroll.Value) mouseRowInd = scroll.Value;
                        }
                        box.Invalidate();
                        break;
                    case Keys.PageUp:
                        mouseRowInd -= count;
                        if (mouseRowInd < 0) mouseRowInd = 0;
                        if (scroll.Visible)
                        {
                            if (mouseRowInd < scroll.Value) scroll.Value = mouseRowInd;
                        }
                        box.Invalidate();
                        break;
                    case Keys.PageDown:
                        mouseRowInd += count;
                        if (mouseRowInd > Count - 1) mouseRowInd = Count - 1;
                        if (scroll.Visible)
                        {
                            if (mouseRowInd > scroll.Value + MaxVisbleRows - 1) scroll.Value = mouseRowInd - MaxVisbleRows + 1;
                            if (mouseRowInd < scroll.Value) mouseRowInd = scroll.Value;
                        }
                        box.Invalidate();
                        break;
                    case Keys.Enter:
                        SelectRow();
                        break;
                }
            }
        }
        private void SelectRow()
        {
            if ((mouseRowInd != -1) && (mouseRowInd < Count))
            {
                int ind = mouseRowInd;
                if (oilFieldList != null)
                {
                    this.Visible = false;
                    canv.SetCenterOilField(oilFieldList[ind].Index, true);
                }
                else if (wellLists != null)
                {
                    int sum = 0;
                    for (int i = 0; i < 4; i++)
                    {
                        if ((ind >= sum) && (wellLists[i] != null) && (ind < sum + wellLists[i].Count))
                        {
                            this.Visible = false;
                            canv.SetCenterSkv(wellLists[i][ind - sum].OilFieldIndex, wellLists[i][ind - sum].Index, wellLists[i][ind - sum].ProjectDest > 0, true);
                        }
                        if (wellLists[i] != null) sum += wellLists[i].Count;
                    }
                }
                else if (areaLists != null)
                {
                    int sum = 0;
                    for (int i = 0; i < 2; i++)
                    {
                        if ((ind >= sum) && (areaLists[i] != null) && (ind < sum + areaLists[i].Count))
                        {
                            this.Visible = false;
                            canv.SetCenterArea(areaLists[i][ind - sum].OilFieldIndex, areaLists[i][ind - sum].Index, true);
                        }
                        if (areaLists[i] != null) sum += areaLists[i].Count;
                    }
                }
            }
        }
        public void ShowUpdate()
        {
            Bounds = this.ClientRectangle;
            Bounds.Y -= 60;
            Bounds.X -= 15;
            Bounds.Width += 30;
            Bounds.Height += 75;
            if (LastActiveOilFieldIndex != canv.twActiveOilField.ActiveOilFieldIndex)
            {
                StartFind(LastFindString);
            }
            else
            {
                this.Show();
            }
            canv.mainForm.tbFinderEx.TextBox.Focus();
        }
        public void ShowForm()
        {
            int count = Count;
            Bounds = Rectangle.Empty;
            if (count > 0)
            {
                Point startPos = canv.mainForm.PointToScreen(canv.mainForm.tbFinderEx.Bounds.Location);
                startPos.Y += canv.mainForm.tbFinderEx.Bounds.Height + 2;
                this.Location = startPos;
                scroll.Visible = false;
                scroll.Value = 0;
                scroll.Maximum = 15;
                mouseRowInd = 0;
                if (count <= MaxVisbleRows)
                {
                    this.Height = (int)Math.Floor(heightRow * count);
                    box.Width = this.Width;
                }
                else
                {
                    scroll.Visible = true;
                    box.Width = this.Width - scroll.Width;
                    this.Height = (int)Math.Floor(heightRow * MaxVisbleRows);
                    scroll.Maximum = count;
                    scroll.LargeChange = MaxVisbleRows;
                    scroll.SmallChange = 1;
                }
                this.Height += 2;
                box.Height = this.Height;
                Bounds = this.ClientRectangle;
                Bounds.Y -= 60;
                Bounds.X -= 15;
                Bounds.Width += 30;
                Bounds.Height += 75;
                this.Show();
                canv.mainForm.tbFinderEx.TextBox.Focus();
            }
        }
        public void TimerLeaveStart()
        {
            tmrLeave.Start();
        }
        void scroll_ValueChanged(object sender, EventArgs e)
        {
            box.Invalidate();
            canv.mainForm.tbFinderEx.TextBox.Focus();
        }
        void scroll_MouseLeave(object sender, EventArgs e)
        {
            tmrLeave.Start();
        }
        void box_MouseLeave(object sender, EventArgs e)
        {
            tmrLeave.Start();
        }
        void tmrLeave_Tick(object sender, EventArgs e)
        {
            Point pt = PointToClient(Cursor.Position);
            if ((!Bounds.IsEmpty) && (!Bounds.Contains(pt)))
            {
                tmrLeave.Stop();
                Bounds = Rectangle.Empty;
                mouseRowInd = -1;
                this.Visible = false;
            }
        }
        void Finder_MouseWheel(object sender, MouseEventArgs e)
        {
            box_MouseWheel(e.Delta);
        }
        public void box_MouseWheel(int Delta)
        {
            if (scroll.Visible)
            {
                int k = -(int)(Delta / 120F);
                if (k == 0) k = 1;
                int val = scroll.Value;
                val += k;
                if (k > 0)
                {
                    if (val > scroll.Maximum - scroll.LargeChange + 1)
                    {
                        val = scroll.Maximum - scroll.LargeChange + 1;
                    }
                }
                else
                {
                    if (val < scroll.Minimum)
                    {
                        val = scroll.Minimum;
                    }
                }
                scroll.Value = val;
            }
        }
        void box_MouseMove(object sender, MouseEventArgs e)
        {
            Point mousePt = e.Location;
            int oldIndex = mouseRowInd;
            int count = Count;
            int startIndex = 0;
            if (scroll.Visible) startIndex = -scroll.Value;
            for (int i = startIndex; i < count; i++)
            {
                if ((i * heightRow < mousePt.Y) && (mousePt.Y <= (i + 1) * heightRow))
                {
                    mouseRowInd = i - startIndex;
                }
            }
            if (oldIndex != mouseRowInd) box.Invalidate();
        }
        void box_MouseDown(object sender, MouseEventArgs e)
        {
            SelectRow();
        }

        #region DRAWING
        private void DrawItems(Graphics grfx)
        {
            if (grfx != null)
            {
                grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                grfx.Clear(Color.White);
                float x = 5, y = 2;
                string str, areaName = string.Empty;
                OilField of;
                float xCoordYes;
                int startIndex = 0;
                if (scroll.Visible)
                {
                    startIndex = scroll.Value;
                }
                if (oilFieldList != null)
                {
                    for (int i = startIndex; i < oilFieldList.Count; i++)
                    {
                        of = oilFieldList[i];
                        str = string.Format("{0}, {1} [Скважин: {2}]", of.Name, of.ParamsDict.NGDUName, of.Wells.Count);
                        if (mouseRowInd == i)
                        {
                            grfx.FillRectangle(Brushes.LightGray, 0, y - 1, this.Width, heightRow + 1);
                        }

                        grfx.DrawString(str, mainFont, Brushes.Black, x, y, StringFormat.GenericTypographic);
                        y += heightRow;
                        if (y > this.Height) break;
                    }
                }
                else if (wellLists != null)
                {
                    if (dictArea == null)
                    {
                        dictArea = (OilFieldAreaDictionary)canv.MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                    }
                    xCoordYes = grfx.MeasureString("Коорд.: Есть", mainFont, PointF.Empty, StringFormat.GenericTypographic).Width;
                    xCoordYes = box.Width - xCoordYes - 5 ;
                    int sum = 0;
                    Brush brush;
                    for (int j = 0; j < 4; j++)
                    {
                        switch (j)
                        {
                            case 0:
                                brush = Brushes.Green;
                                break;
                            case 1:
                                brush = Brushes.Black;
                                break;
                            case 2:
                                brush = Brushes.Red;
                                break;
                            case 3:
                                brush = Brushes.Blue;
                                break;
                            default:
                                brush = Brushes.Black;
                                break;
                        }

                        if(wellLists[j] != null)
                        {
                            for (int i = startIndex; i < sum + wellLists[j].Count; i++)
                            {
                                if (i >= sum)
                                {
                                    of = canv.MapProject.OilFields[wellLists[j][i - sum].OilFieldIndex];
                                    areaName = dictArea.GetShortNameByCode(wellLists[j][i - sum].OilFieldAreaCode);
                                    
                                    if (areaName.Length > 0) areaName = " [" + areaName + " пл.]";
                                    str = string.Format("{0}, {1}{2}", wellLists[j][i - sum].UpperCaseName, of.Name, areaName);
                                    if (mouseRowInd == i)
                                    {
                                        grfx.FillRectangle(Brushes.LightGray, 0, y - 1, this.Width, heightRow + 1);
                                    }
                                    grfx.DrawString(str, mainFont, brush, x, y, StringFormat.GenericTypographic);
                                    if (wellLists[j][i - sum].ProjectDest == 0)
                                    {
                                        if (wellLists[j][i - sum].CoordLoaded && wellLists[j][i - sum].coord.Count > 0)
                                        {
                                            grfx.DrawString("Коорд.: Есть", mainFont, brush, xCoordYes, y, StringFormat.GenericTypographic);
                                        }
                                        else
                                        {
                                            grfx.DrawString("Коорд.: Нет", mainFont, brush, xCoordYes, y, StringFormat.GenericTypographic);
                                        }
                                    }
                                    else
                                    {
                                        grfx.DrawString("Проектн.скв.", mainFont, brush, xCoordYes, y, StringFormat.GenericTypographic);
                                    }
                                    y += heightRow;
                                    if (y > this.Height) break;
                                }
                            }
                            sum += wellLists[j].Count;
                        }
                    }
                }
                else if (areaLists != null)
                {
                    if (dictArea == null)
                    {
                        dictArea = (OilFieldAreaDictionary)canv.MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                    }
                    xCoordYes = grfx.MeasureString("ЯЗ", mainFont, PointF.Empty, StringFormat.GenericTypographic).Width;
                    xCoordYes = box.Width - xCoordYes - 5;
                    int sum = 0;
                    Brush brush;
                    for (int j = 0; j < 2; j++)
                    {
                        switch (j)
                        {
                            case 0:
                                brush = Brushes.Green;
                                break;
                            case 1:
                                brush = Brushes.Black;
                                break;
                            default:
                                brush = Brushes.Black;
                                break;
                        }

                        if (areaLists[j] != null)
                        {
                            for (int i = startIndex; i < sum + areaLists[j].Count; i++)
                            {
                                if (i >= sum)
                                {
                                    of = canv.MapProject.OilFields[areaLists[j][i - sum].OilFieldIndex];
                                    areaName = dictArea.GetShortNameByCode(areaLists[j][i - sum].OilFieldAreaCode);

                                    if (areaName.Length > 0) areaName = string.Format("[{0} пл.]", areaName);
                                    str = string.Format("{0}, {1} {2}", areaLists[j][i - sum].Name, of.Name, areaName);
                                    if (mouseRowInd == i)
                                    {
                                        grfx.FillRectangle(Brushes.LightGray, 0, y - 1, this.Width, heightRow + 1);
                                    }
                                    grfx.DrawString(str, mainFont, brush, x, y, StringFormat.GenericTypographic);
                                    grfx.DrawString("ЯЗ", mainFont, brush, xCoordYes, y, StringFormat.GenericTypographic);
                                    y += heightRow;
                                    if (y > this.Height) break;
                                }
                            }
                            sum += areaLists[j].Count;
                        }
                    }
                }
                Rectangle rect = this.ClientRectangle;
                rect.Width--;
                if(scroll.Visible) rect.Width -= scroll.Width;
                rect.Height--;
                grfx.DrawRectangle(Pens.Black, rect);
            }
        }
        void box_Paint(object sender, PaintEventArgs e)
        {
            Graphics grfx = e.Graphics;
            DrawItems(grfx);
        }
        #endregion
    }
}