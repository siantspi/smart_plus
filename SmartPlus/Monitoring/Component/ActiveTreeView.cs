﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public sealed class ActiveTreeView : TreeView
    {
        public int ActiveOilFieldIndex;
        CanvasMap _canv;
        TreeNode dragNode;
        LayersTreeView Inspector;
        bool callGC = false;

        #region ContextMenuStrip

        public ContextMenuStrip cMenuOilField, cMenuTreeView, cMenuContour, cMenuWellList;
        public ContextMenuStrip cMenuSelect;
        public ContextMenuStrip cMenuAlias;

        ToolStripMenuItem cmSK, cmEditMode, cmRotateSK, cmDropCoef, cmDrawWellHead;
        ToolStripMenuItem cmNameEditMode;
        ToolStripMenuItem cmContourLoad;
        ToolStripMenuItem cmSelectAll, cmDeSelectAll, cmDeSelectOther;
        ToolStripMenuItem cmAliasRemove;


        // constructor context menu
        private void InitializeContextMenus()
        {

            // CmenuUserData - Месторождение
            cMenuOilField = new ContextMenuStrip();
            cmContourLoad = (ToolStripMenuItem)cMenuOilField.Items.Add("");
            cmContourLoad.Text = "Загрузить контур";
            cmContourLoad.Click += new EventHandler(cmContourLoad_Click);
            cmContourLoad.Visible = false;

            //cMenuOilField.Items.Add("-");

            cmSK = (ToolStripMenuItem)cMenuOilField.Items.Add("");
            cmSK.Text = "Система координат";
            cmSK.Visible = false;

            // Режим редактирования
            cmEditMode = (ToolStripMenuItem)cmSK.DropDownItems.Add("");
            cmEditMode.Text = "Режим редактирования";
            cmEditMode.CheckOnClick = true;
            cmEditMode.CheckedChanged += new EventHandler(cmEditMode_CheckedChanged);
            cmEditMode.Click += new EventHandler(cmEditMode_Click);

            // Поворот системы координат
            cmRotateSK = (ToolStripMenuItem)cmSK.DropDownItems.Add("");
            cmRotateSK.Text = "Поворот СК";
            cmRotateSK.CheckOnClick = true;
            cmRotateSK.Enabled = false;
            cmRotateSK.Click += new EventHandler(cmRotateSK_Click);
            cmRotateSK.Visible = false;

            // Сбросить систему координат
            cmDropCoef = (ToolStripMenuItem)cmSK.DropDownItems.Add("");
            cmDropCoef.Text = "Сбросить коэффициенты";
            cmDropCoef.Enabled = false;
            cmDropCoef.Click += new EventHandler(cmDropCoef_Click);
            cmDropCoef.Visible = false;

            cMenuWellList = new ContextMenuStrip();
            // Отобразить устья скважин
            cmDrawWellHead = (ToolStripMenuItem)cMenuWellList.Items.Add("");
            cmDrawWellHead.Text = "Отобразить устья скважин";
            cmDrawWellHead.CheckOnClick = true;
            cmDrawWellHead.Click += new EventHandler(cmDrawWellHead_Click);

            // CmenuUserData - Контур
            cMenuContour = new ContextMenuStrip();
            cmNameEditMode = (ToolStripMenuItem)cMenuContour.Items.Add("");
            cmNameEditMode.Text = "Сдвиг названия";
            cmNameEditMode.CheckOnClick = true;
            cmNameEditMode.Click += new EventHandler(cmNameEditMode_Click);

            cMenuSelect = new ContextMenuStrip();
            // Выбрать все
            cmSelectAll = (ToolStripMenuItem)cMenuSelect.Items.Add("");
            cmSelectAll.Text = "Включить все";
            cmSelectAll.Image = SmartPlus.Properties.Resources.SellAll;
            cmSelectAll.Click += new EventHandler(cmSelectAll_Click);
            // Откл все
            cmDeSelectAll = (ToolStripMenuItem)cMenuSelect.Items.Add("");
            cmDeSelectAll.Text = "Отключить все";
            cmDeSelectAll.Image = SmartPlus.Properties.Resources.DeSellAll;
            cmDeSelectAll.Click += new EventHandler(cmDeSelectAll_Click);
            // Откл остальные
            cmDeSelectOther = (ToolStripMenuItem)cMenuSelect.Items.Add("");
            cmDeSelectOther.Text = "Отключить остальные";
            cmDeSelectOther.Image = SmartPlus.Properties.Resources.DeSellOther;
            cmDeSelectOther.Click += new EventHandler(cmDeSelectOther_Click);

            // CmenuUserData - Месторождение
            cMenuAlias = new ContextMenuStrip();
            cmAliasRemove = (ToolStripMenuItem)cMenuAlias.Items.Add("");
            cmAliasRemove.Text = "Удалить псевдоним";
            cmAliasRemove.Click += new EventHandler(cmAliasRemove_Click);
        }

        // Псевдонимы месторождения
        void cmAliasRemove_Click(object sender, EventArgs e)
        {
            TreeNode tnAlias = this.SelectedNode, tnParent = tnAlias.Parent, ofNode = null;
            if ((tnAlias.Tag == null) && (tnParent != null) && (tnParent.Text.StartsWith("Псевдонимы")))
            {
                OilField of = null;
                TreeNode MainNode = null;
                if (tnParent.Parent != null) ofNode = tnParent.Parent;
                if ((ofNode != null) && (ofNode.Tag != null))
                {

                    if (((BaseObj)ofNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                    {
                        of = _canv.MapProject.OilFields[((C2DLayer)ofNode.Tag).oilFieldIndex];
                        MainNode = ((C2DLayer)ofNode.Tag).node;
                    }
                }
                if (of != null)
                {
                    if (MainNode != null)
                    {
                        _canv.RemoveAliasByName(MainNode, tnAlias.Text);
                    }
                    of.RemoveAlias(tnAlias.Text);
                    this.BeginUpdate();
                    tnParent.Nodes.Remove(tnAlias);
                    if (tnParent.Nodes.Count == 0) ofNode.Nodes.Remove(tnParent);
                    else
                    {
                        tnParent.Text = "Псевдонимы [" + tnParent.Nodes.Count.ToString() + "]";
                    }
                    this.EndUpdate();

                }
            }
        }

        // установка иконки узла дерева
        private void SetTNImageIndex(TreeNode tn, int ImageIndex)
        {
            tn.ImageIndex = ImageIndex;
            tn.SelectedImageIndex = ImageIndex;
        }

        //CmenuOilField    
        void cmDrawWellHead_Click(object sender, EventArgs e)
        {
            _canv.twLayers.SetDrawWellHead(((C2DLayer)this.SelectedNode.Tag).node, cmDrawWellHead.Checked);
            //if (cmDrawWellHead.Checked) SetTNImageIndex(this.SelectedNode, 2);
            //else SetTNImageIndex(this.SelectedNode, 0);
            _canv.DrawLayerList();
        }
        public void SetSKEditCheck(bool EditCheck)
        {
            cmEditMode.Checked = EditCheck;
        }
        void cmEditMode_Click(object sender, EventArgs e)
        {
            if (cmEditMode.Checked)
            {
                SetTNImageIndex(this.SelectedNode, 1);
                cmNameEditMode.Checked = false;
            }
            else
            {
                SetTNImageIndex(this.SelectedNode, 0);
            }
            _canv.twLayers.SetSKEditMode(((C2DLayer)this.SelectedNode.Tag).node, cmEditMode.Checked);
            this.Update();
        }
        void cmEditMode_CheckedChanged(object sender, EventArgs e)
        {
            cmDropCoef.Enabled = cmEditMode.Checked;
            cmRotateSK.Enabled = cmEditMode.Checked;
            if (!cmEditMode.Checked) cmRotateSK.Checked = false;
        }
        private void SetEditMode(TreeNode tn, bool EditMode)
        {
            if (tn.Tag != null)
            {
                if (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.OILFIELD)
                {
                    _canv.Rotate = false;
                    if (EditMode)
                        _canv.EditedOilFieldIndex = ((OilField)tn.Tag).Index;
                    else
                        _canv.EditedOilFieldIndex = -1;
                }
                else if (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    C2DLayer layer = (C2DLayer)tn.Tag;
                    if (layer != null)
                    {
                        _canv.Rotate = false;
                        if (EditMode) _canv.EditedLayer = layer;
                        else _canv.EditedLayer = null;
                    }
                }
            }

        }
        void cmRotateSK_Click(object sender, EventArgs e)
        {
            _canv.Rotate = cmRotateSK.Checked;
        }
        public void ResetRotate()
        {
            cmRotateSK.Checked = false;
            _canv.ResetRotate();
        }
        public void ResetEditMode()
        {
            //cmEditMode.Checked = false;
            cmEditMode.PerformClick();
            cmNameEditMode.PerformClick();
        }
        void cmDropCoef_Click(object sender, EventArgs e)
        {
            _canv.ResetSK(((C2DLayer)this.SelectedNode.Tag).Index);
        }

        void cmContourLoad_Click(object sender, EventArgs e)
        {
            _canv.AddContour((OilField)this.SelectedNode.Tag);
        }

        // cMenuTreeView
        private void CheckNodes(TreeNode tnSource, TreeNode tnDest)
        {
            tnDest.Checked = tnSource.Checked;
            if (tnDest.Nodes.Count > 0)
            {
                for (int i = 0; i < tnDest.Nodes.Count; i++)
                {
                    CheckNodes(tnSource.Nodes[i], tnDest.Nodes[i]);
                }
            }
        }

        // cMenuContour
        void cmNameEditMode_Click(object sender, EventArgs e)
        {
            if (cmNameEditMode.Checked)
            {
                SetTNImageIndex(this.SelectedNode, 1);
                cmEditMode.Checked = false;
            }
            else
            {
                SetTNImageIndex(this.SelectedNode, 0);
                if ((this.SelectedNode.Tag != null) && 
                    (((C2DLayer)this.SelectedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR || 
                     ((C2DLayer)this.SelectedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR))
                {
                    if (((C2DLayer)this.SelectedNode.Tag).oilfield.ContoursEdited)
                        _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_OF_CONTOUR_TO_CACHE, ((C2DLayer)this.SelectedNode.Tag).oilfield);
                }
            }
            SetEditMode(this.SelectedNode, cmNameEditMode.Checked);
            this.Update();
        }

        // cMenuSelecting
        void SelectNodes(TreeNode node, int SelectType)
        {
            if ((node != null) && (node.Parent != null))
            {
                int index = node.Index;
                TreeNode parent = node.Parent;
                bool check = (SelectType == 0);
                if (parent.Tag != null)
                {
                    bool callGC = false;
                    for (int i = 0; i < parent.Nodes.Count; i++)
                    {
                        parent.Nodes[i].Checked = check;
                        if (parent.Nodes[i].Tag != null)
                        {
                            ((C2DObject)parent.Nodes[i].Tag).Visible = check;
                            if ((((C2DObject)parent.Nodes[i].Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                (((C2DLayer)parent.Nodes[i].Tag).node != null))
                            {
                                ((C2DLayer)parent.Nodes[i].Tag).node.Checked = check;
                                if ((((C2DLayer)parent.Nodes[i].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID))
                                {
                                    if ((SelectType != 2) || (i != node.Index))
                                    {
                                        Grid grid = (Grid)((C2DLayer)parent.Nodes[i].Tag).ObjectsList[0];
                                        OilField of;
                                        if ((!grid.DataLoaded) && (check))
                                        {
                                            _canv.LoadGridMode = 1;
                                            _canv.LoadGridNode = parent;
                                            for (int j = 0; j < _canv.MapProject.OilFields.Count; j++)
                                            {
                                                of = _canv.MapProject.OilFields[j];
                                                if (of.OilFieldCode == grid.OilFieldCode)
                                                {
                                                    _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE, of, grid.Index);
                                                    break;
                                                }
                                            }
                                        }
                                        else if (!check)
                                        {
                                            grid.FreeDataMemory();
                                            callGC = true;
                                        }
                                    }
                                }
                                else if ((((C2DLayer)parent.Nodes[i].Tag).oilfield != null) && (((C2DLayer)parent.Nodes[i].Tag).Level == -1))
                                {
                                    C2DLayer layer = (C2DLayer)parent.Nodes[i].Tag;
                                    Constant.BASE_OBJ_TYPES_ID type = layer.GetObjTypeIDRecursive();
                                    if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                        (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) &&
                                        (layer.ObjectsList.Count > 0))
                                    {
                                        if (check)
                                        {
                                            _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE, layer);
                                        }
                                        else
                                        {
                                            if ((!_canv.mainForm.worker.IsBusy) || (_canv.mainForm.worker.GetWorkType() != (int)Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE))
                                            {
                                                layer.oilfield.FreeContoursData();
                                                callGC = true;
                                            }
                                            else
                                            {
                                                _canv.mainForm.worker.CancelAsync();
                                            }
                                        }
                                    }
                                }
                            }
                            if (callGC) GC.GetTotalMemory(true);
                        }
                    }
                }

                if (SelectType == 2)
                {
                    node.Checked = true;
                    if (node.Tag != null)
                    {
                        ((BaseObj)node.Tag).Visible = true;
                        if ((((BaseObj)node.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                            (((C2DLayer)node.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID))
                        {
                            if (((C2DLayer)node.Tag).node != null) ((C2DLayer)node.Tag).node.Checked = check;
                            Grid grid = (Grid)((C2DLayer)node.Tag).ObjectsList[0];
                            OilField of;
                            if (!grid.DataLoaded)
                            {
                                _canv.LoadGridMode = 0;
                                _canv.LoadGridNode = node;
                                for (int j = 0; j < _canv.MapProject.OilFields.Count; j++)
                                {
                                    of = _canv.MapProject.OilFields[j];
                                    if (of.OilFieldCode == grid.OilFieldCode)
                                    {
                                        _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE, of, grid.Index);
                                        break;
                                    }
                                }
                            }
                        }
                        else if (((C2DObject)node.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER && (((C2DLayer)node.Tag).oilfield != null) && (((C2DLayer)node.Tag).Level == -1))
                        {
                            C2DLayer layer = (C2DLayer)node.Tag;
                            Constant.BASE_OBJ_TYPES_ID type = layer.GetObjTypeIDRecursive();
                            if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) &&
                                (layer.ObjectsList.Count > 0))
                            {
                                _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE, layer);
                            }
                        }
                    }
                }
                _canv.DrawLayerList();
            }
        }
        void cmSelectAll_Click(object sender, EventArgs e)
        {
            SelectNodes(this.SelectedNode, 0);
        }
        void cmDeSelectAll_Click(object sender, EventArgs e)
        {
            SelectNodes(this.SelectedNode, 1);
        }
        void cmDeSelectOther_Click(object sender, EventArgs e)
        {
            SelectNodes(this.SelectedNode, 2);
        }
        #endregion

        // constructor TreeView
        public ActiveTreeView(Control Parent, CanvasMap Canvas)
        {
            this._canv = Canvas;
            this.ActiveOilFieldIndex = -1;
            this.Inspector = Canvas.twLayers;
            this.Parent = Parent;
            this.Dock = DockStyle.Fill;
            this.CheckBoxes = false;
            this.HideSelection = false;
            this.Font = new Font("Calibri", 8.25f);
            this.AllowDrop = true;
            this.MouseDown += new MouseEventHandler(TreeViewLayers_MouseDown);
            this.AfterSelect += new TreeViewEventHandler(TreeViewLayers_AfterSelect);
            this.BeforeExpand += new TreeViewCancelEventHandler(TreeViewLayers_BeforeExpand);
            this.DoubleClick += new EventHandler(TreeViewLayers_DoubleClick);
            InitializeContextMenus();
            this.ContextMenuStrip = cMenuTreeView;

            this.ImageList = new ImageList();
            this.ImageList.ColorDepth = ColorDepth.Depth24Bit;
            this.ImageList.Images.Add(Properties.Resources.def);            //0
            this.ImageList.Images.Add(Properties.Resources.EditIcon16);     //1
            this.ImageList.Images.Add(Properties.Resources.Folder);         //2
            this.ImageList.Images.Add(Properties.Resources.doc);            //3
            this.ImageList.Images.Add(Properties.Resources.pdf);            //4
            this.ImageList.Images.Add(Properties.Resources.ppt);            //5
            this.ImageList.Images.Add(Properties.Resources.png);            //6
            this.ImageList.Images.Add(Properties.Resources.txt);            //7
            this.ImageList.Images.Add(Properties.Resources.xls);            //8
            this.ImageList.Images.Add(Properties.Resources.Oilfield);       //9
            this.ImageList.Images.Add(Properties.Resources.OilfieldArea);   //10
            this.ImageList.Images.Add(Properties.Resources.WellPad);        //11
            this.ImageList.Images.Add(Properties.Resources.Well);           //12
            this.ImageList.Images.Add(Properties.Resources.ProjectWell);    //13
            this.ImageList.Images.Add(Properties.Resources.Contour);        //14
            this.ImageList.Images.Add(Properties.Resources.Profile);        //15
            this.ImageList.Images.Add(Properties.Resources.Marker);         //16
            this.ImageList.Images.Add(Properties.Resources.Grid);           //17
            this.ImageList.Images.Add(Properties.Resources.WellList);       //18
            this.ImageList.Images.Add(Properties.Resources.VoronoiMap);     //19
            this.ImageList.Images.Add(Properties.Resources.PVT);            //20
            this.ImageList.Images.Add(Properties.Resources.Bashneft16);     //21
            this.ImageList.Images.Add(Properties.Resources.NGDU);           //22
            this.ImageList.Images.Add(Properties.Resources.Image);          //23
            this.ImageList.Images.Add(Properties.Resources.Deposit);        //24

            this.ItemDrag += new ItemDragEventHandler(TreeViewLayers_ItemDrag);
            this.DragDrop += new DragEventHandler(TreeViewLayers_DragDrop);
            this.DragOver += new DragEventHandler(TreeViewActive_DragOver);
        }
        void TreeViewActive_DragOver(object sender, DragEventArgs e)
        {
            Point pointClick = this.PointToClient(Cursor.Position);
            TreeNode targetNode = this.GetNodeAt(pointClick);

            e.Effect = DragDropEffects.Move;
        }

        #region DragDrop
        void TreeViewLayers_DragDrop(object sender, DragEventArgs e)
        {
            Point pointClick = this.PointToClient(Cursor.Position);
            TreeNode targetNode = this.GetNodeAt(pointClick);
        }
        void TreeViewLayers_ItemDrag(object sender, ItemDragEventArgs e)
        {
            dragNode = (TreeNode)e.Item;
            this.SelectedNode = dragNode;
            DoDragDrop(dragNode, DragDropEffects.Move);
        }
        #endregion

        // other
        public void Clear()
        {
            this.ActiveOilFieldIndex = -1;
            this.Nodes.Clear();
        }

        int GetImageIndex(Constant.BASE_OBJ_TYPES_ID Type)
        {
            switch (Type)
            {
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    return 9;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    return 10;
                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                    return 11;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    return 12;
                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                    return 13;
                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                    return 14;
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    return 14;
                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                    return 15;
                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                    return 16;
                case Constant.BASE_OBJ_TYPES_ID.GRID:
                    return 17;
                case Constant.BASE_OBJ_TYPES_ID.WELL_LIST:
                    return 18;
                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                    return 18;
                case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                    return 19;
                case Constant.BASE_OBJ_TYPES_ID.PVT:
                    return 20;
                case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                    return 23;
                case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                    return 24;
                default:
                    return 0;
            }
        }

        public void UpdateRecursiveTreeView()
        {
            if (Nodes.Count > 0)
            {
                UpdateRecursiveActiveTreeView(Nodes[0]);
            }
        }
        void UpdateRecursiveActiveTreeView(TreeNode tn)
        {
            if (tn != null)
            {
                TreeNode parentNode = tn;
                int ofIndex = -1;
                while ((parentNode != null))
                {
                    if ((parentNode.Tag != null) && (((BaseObj)parentNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                        (((C2DLayer)parentNode.Tag).oilFieldIndex > -1))
                    {
                        ofIndex = ((C2DLayer)parentNode.Tag).oilFieldIndex;
                        break;
                    }
                    parentNode = parentNode.Parent;
                }
                if ((ofIndex > -1) && (ofIndex == ActiveOilFieldIndex))
                {
                    UpdateActiveRecursiveTreeNode(Nodes[0]);
                }
            }
        }
        void UpdateActiveRecursiveTreeNode(TreeNode tn)
        {
            if (tn.Nodes.Count > 0)
            {
                for (int i = 0; i < tn.Nodes.Count; i++)
                {
                    UpdateActiveRecursiveTreeNode(tn.Nodes[i]);
                }
            }
            if ((tn.Tag != null) && (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                tn.Checked = ((C2DLayer)tn.Tag).Visible;
                if (((C2DLayer)tn.Tag).node != null)
                {
                    tn.ImageIndex = ((C2DLayer)tn.Tag).node.ImageIndex;
                    tn.SelectedImageIndex = ((C2DLayer)tn.Tag).node.SelectedImageIndex;
                }
            }
        }

        // Обработка методов TreeView
        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == 515)
            { /* WM_LBUTTONDBLCLK */
            }
            else
                base.DefWndProc(ref m);
        }
        void TreeViewLayers_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeNode tnBase = e.Node, tn, tn2;
            int ind;
            int imgIndex;
            if ((tnBase.Tag != null)
                && (tnBase.Nodes.Count < 2)
                && (((BaseObj)tnBase.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                && ((((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL) ||
                    (((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL_PAD) ||
                    (((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL)))
            {
                C2DLayer layer = (C2DLayer)tnBase.Tag;
                this.BeginUpdate();
                tnBase.Nodes.Clear();
                imgIndex = GetImageIndex(layer.ObjTypeID);
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) imgIndex = GetImageIndex(((C2DLayer)layer.ObjectsList[i]).ObjTypeID);
                    tn = tnBase.Nodes.Add("", ((C2DObject)layer.ObjectsList[i]).Name, imgIndex, imgIndex);
                    tn.Tag = (C2DObject)layer.ObjectsList[i];
                    tn.Checked = ((C2DObject)layer.ObjectsList[i]).Visible;
                    tn.ContextMenuStrip = cMenuSelect;
                    if ((((C2DObject)layer.ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.WELL))
                    {
                        tn.Nodes.Add("Empty");
                    }
                }
                this.EndUpdate();
            }
            if ((tnBase.Tag != null)
                && (tnBase.Nodes.Count < 2)
                && (((BaseObj)tnBase.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                && (((C2DLayer)tnBase.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                && (tnBase.Nodes[0].Text == "Empty"))
            {
                C2DLayer layer = (C2DLayer)tnBase.Tag;
                this.BeginUpdate();
                tnBase.Nodes.Clear();
                imgIndex = GetImageIndex(layer.ObjTypeID);
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) imgIndex = GetImageIndex(((C2DLayer)layer.ObjectsList[i]).ObjTypeID);
                    tn = tnBase.Nodes.Add("", ((C2DObject)layer.ObjectsList[i]).Name, imgIndex, imgIndex);
                    tn.Tag = (C2DObject)layer.ObjectsList[i];
                    tn.Checked = ((C2DObject)layer.ObjectsList[i]).Visible;
                }
                this.EndUpdate();
            }
            else if ((tnBase.Tag != null)
                && (tnBase.Nodes.Count < 2)
                && (((BaseObj)tnBase.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                && (tnBase.Nodes[0].Text == "Empty"))
            {
                DataDictionary dictLogs = (DataDictionary)_canv.MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LOG_MNEMONIC);
                this.BeginUpdate();
                tnBase.Nodes.Clear();
                Well w = (Well)tnBase.Tag;
                OilField of = _canv.MapProject.OilFields[w.OilFieldIndex];
                if (w.CoordLoaded)
                {
                    tn = tnBase.Nodes.Add("Координаты");
                    tn.Checked = true;
                }
                of.LoadMerFromCache(w.Index, true);
                if (w.MerLoaded)
                {
                    tn = tnBase.Nodes.Add("МЭР");
                    tn.Checked = true;
                }
                of.LoadGisFromCache(w.Index, true);
                if (w.GisLoaded)
                {
                    tn = tnBase.Nodes.Add("ГИС");
                    tn.Checked = true;
                }
                of.LoadChessFromCache(w.Index, true);
                if (w.ChessLoaded)
                {
                    tn = tnBase.Nodes.Add("Шахматка");
                    tn.Checked = true;
                }
                of.LoadCoreFromCache(w.Index, true);
                of.LoadCoreTestFromCache(w.Index, true);
                if ((w.CoreLoaded) || (w.CoreTestLoaded))
                {
                    tn = tnBase.Nodes.Add("Керн");
                    tn.Checked = true;
                    if (w.CoreLoaded)
                    {
                        tn2 = tn.Nodes.Add("Наличие керна");
                        tn2.Checked = true;
                        w.core = null;
                    }
                    if (w.CoreTestLoaded)
                    {
                        tn2 = tn.Nodes.Add("Исследования керна");
                        tn2.Checked = true;
                        w.coreTest = null;
                    }
                }
                of.LoadLogsFromCache(w.Index, false, true);
                of.LoadLogsBaseFromCache(w.Index, false, true);
                if ((w.LogsLoaded) || (w.LogsBaseLoaded))
                {
                    tn = tnBase.Nodes.Add("Каротаж");
                    tn.Checked = true;
                    if (w.LogsBaseLoaded)
                    {
                        for (int i = 0; i < w.logsBase.Length; i++)
                        {
                            ind = dictLogs.GetIndexByCode(w.logsBase[i].MnemonikaId);
                            if (ind > 0)
                            {
                                tn2 = tn.Nodes.Add(dictLogs[ind].ShortName + " [Базовый]");
                                tn2.Checked = true;
                            }
                        }
                        w.logsBase = null;
                    }
                    if (w.LogsLoaded)
                    {
                        for (int i = 0; i < w.logs.Length; i++)
                        {
                            ind = dictLogs.GetIndexByCode(w.logs[i].MnemonikaId);
                            if (ind > 0)
                            {
                                tn2 = tn.Nodes.Add(dictLogs[ind].ShortName);
                                tn2.Checked = true;
                            }
                        }
                        w.logs = null;
                    }
                }
                of.ClearBufferList(false);
                this.EndUpdate();
            }
        }
        void TreeViewLayers_MouseDown(object sender, MouseEventArgs e)
        {
            TreeView tw = (TreeView)sender;
            TreeNode clickedNode = tw.GetNodeAt(e.Location);
            C2DLayer layer;
           // _canv.// mainForm.StatUsage.AddMessage(CollectorStatId.ACTIVE_MEST_MOUSE_DOWN);
            if (clickedNode != null)
            {
                if ((e.Location.X >= clickedNode.Bounds.X - 32) && (e.Location.X < clickedNode.Bounds.X - 16))
                {
                    bool check, res;
                    check = !clickedNode.Checked;
                    if (clickedNode.Tag != null)
                    {
                        ((BaseObj)clickedNode.Tag).Visible = check;
                        if ((((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                            (((C2DLayer)clickedNode.Tag).node != null))
                            ((C2DLayer)clickedNode.Tag).node.Checked = check;
                    }

                    #region Shift + Click
                    //if (((Control.ModifierKeys & Keys.Shift) == Keys.Shift) &&
                    //    (clickedNode.Nodes.Count > 0) &&
                    //    (((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    //{
                    //    callGC = false;
                    //    for (int i = 0; i < clickedNode.Nodes.Count; i++)
                    //    {
                    //        ((C2DObject)clickedNode.Nodes[i].Tag).Visible = check;
                    //        clickedNode.Nodes[i].Checked = check;
                    //        if ((((C2DObject)clickedNode.Nodes[i].Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                    //            (((C2DLayer)clickedNode.Nodes[i].Tag)._Node != null))
                    //        {
                    //            ((C2DLayer)clickedNode.Nodes[i].Tag)._Node.Checked = check;
                    //            if ((((C2DLayer)clickedNode.Nodes[i].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID))
                    //            {
                    //                GridEx grid = (GridEx)((C2DLayer)clickedNode.Nodes[i].Tag)._ObjectsList[0];
                    //                OilField of;
                    //                if ((!grid.DataLoaded) && (check))
                    //                {
                    //                    _canv.LoadGridMode = 1;
                    //                    _canv.LoadGridNode = clickedNode;
                    //                    for (int j = 0; j < _canv.MapProject.OilFields.Count; j++)
                    //                    {
                    //                        of = _canv.MapProject.OilFields[j];
                    //                        if (of.OilFieldCode == grid.OilFieldCode)
                    //                        {
                    //                            _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE, of, grid.Index);
                    //                            break;
                    //                        }
                    //                    }
                    //                }
                    //                else if (!check)
                    //                {
                    //                    grid.FreeDataMemory();
                    //                    callGC = true;
                    //                }
                    //            }
                    //        }
                    //    }
                    //    if (callGC) GC.GetTotalMemory(true);
                    //}
                    #endregion

                    if ((clickedNode.Tag != null) && (((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    {
                        layer = (C2DLayer)clickedNode.Tag;
                        if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
                        {
                            if (_canv.mainForm.AppSettings != null && (!_canv.mainForm.AppSettings.ShowAllOilfieldAreas))
                            {
                                TreeNode parent = clickedNode.Parent;
                                if (parent != null && parent.Tag != null && ((BaseObj)parent.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                                {
                                    C2DLayer parentLayer = (C2DLayer)parent.Tag, layer2;
                                    if (parentLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                                    {
                                        for (int j = 0; j < parentLayer.ObjectsList.Count; j++)
                                        {
                                            layer2 = (C2DLayer)parentLayer.ObjectsList[j];
                                            if (layer2 != layer)
                                            {
                                                layer2.Visible = false;
                                                if (layer2.node != null) layer2.node.Checked = false;
                                                if (j < parent.Nodes.Count) parent.Nodes[j].Checked = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                        {
                            Grid grid = (Grid)layer.ObjectsList[0];
                            if ((!grid.DataLoaded) && (check))
                            {
                                _canv.LoadGridMode = 0;
                                _canv.LoadGridNode = clickedNode;
                                for (int j = 1; j < _canv.MapProject.OilFields.Count; j++)
                                {
                                    OilField of = _canv.MapProject.OilFields[j];
                                    if (of.OilFieldCode == grid.OilFieldCode)
                                    {
                                        _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE, of, grid.Index);
                                        break;
                                    }
                                }

                            }
                            else if (!check)
                            {
                                grid.FreeDataMemory();
                                GC.GetTotalMemory(true);
                            }
                        }
                        else if ((layer.oilfield != null) && (layer.Level == -1))
                        {
                            Constant.BASE_OBJ_TYPES_ID type = layer.GetObjTypeIDRecursive();
                            if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) &&
                                (layer.ObjectsList.Count > 0))
                            {
                                if (check)
                                {
                                    _canv.mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE, layer);
                                }
                                else
                                {
                                    if ((!_canv.mainForm.worker.IsBusy) || (_canv.mainForm.worker.GetWorkType() != (int)Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE))
                                    {
                                        layer.oilfield.FreeContoursData();
                                    }
                                    else
                                    {
                                        _canv.mainForm.worker.CancelAsync();
                                    }
                                }
                            }
                        }
                    }
                    _canv.DrawLayerList();
                    clickedNode.Checked = check;
                }
                else
                {
                    if (e.Button == MouseButtons.Right)
                    {
                        cmSelectAll.Visible = true;
                        cmDeSelectAll.Visible = true;
                        if (_canv.mainForm.AppSettings != null &&
                            (!_canv.mainForm.AppSettings.ShowAllOilfieldAreas) &&
                            (clickedNode.Tag != null) &&
                            (((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                            (((C2DLayer)clickedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA))
                        {
                            cmSelectAll.Visible = false;
                            cmDeSelectAll.Visible = false;
                        }
                    }
                }
                tw.SelectedNode = clickedNode;
            }
        }
        void TreeViewLayers_AfterSelect(object sender, TreeViewEventArgs e)
        {
            /*
            this.BeginUpdate();
            if ((e.Node.Tag != null) && (((BaseObj)e.Node.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)) {
                for (int i = 0; i < _canv.LayerList.Count; i++) ((C2DLayer)_canv.LayerList[i]).Selected = false;
                ((C2DLayer)e.Node.Tag).Selected = true;
            }
           // _canv.DrawLayerList();
            this.EndUpdate();
             */
        }
        void TreeViewLayers_DoubleClick(object sender, EventArgs e)
        {
        }
    }
}
