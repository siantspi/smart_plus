﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SmartPlus
{
    public delegate void OnTextBoxExButtonClickDelegate();

    public sealed class ToolStripTextBoxEx : ToolStripControlHost
    {
        public event OnTextBoxExButtonClickDelegate OnButtonClick;

        public ToolStripTextBoxEx() : base(new TextBoxEx())
        {
            this.AutoSize = false;
            ((TextBoxEx)base.Control).PanelButton.MouseClick += new MouseEventHandler(PanelButton_MouseClick);
        }

        void PanelButton_MouseClick(object sender, MouseEventArgs e)
        {
            if (OnButtonClick != null) OnButtonClick();   
        }
        new public bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                this.TextBox.Enabled = value;
                base.Enabled = value;
            }
        }
        new public string ToolTipText
        {
            get { return ((TextBoxEx)base.Control).ToolTipText; }
            set
            {
                ((TextBoxEx)base.Control).ToolTipText = value;
            }
        }

        public TextBox TextBox { get { return (TextBox)((TextBoxEx)base.Control).TextBox; } }
    }
    

    class TextBoxEx : Panel
    {
        TextBox textBox;
        Panel btn;
        bool MouseOver, MouseDown;
        LinearGradientBrush brush, brushDown;
        string toolTipText;
        new public bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                base.Enabled = value;
                textBox.Enabled = value;
                btn.Enabled = value;
                btn.Invalidate();
            }
        }
        public bool ReadOnly
        {
            get { return textBox.ReadOnly; }
            set
            {
                textBox.ReadOnly = value;
                if (textBox.ReadOnly) textBox.BackColor = Color.White;
            }
        }
        public string ToolTipText
        {
            get { return toolTipText; }
            set
            {
                toolTipText = value;
                ToolTip tip = new ToolTip();
                tip.InitialDelay = 1000;
                tip.SetToolTip(textBox, value);
            }
        }        

        public TextBoxEx()
        {
            this.Height = 21;
            this.Width = 100;
            this.BackColor = Color.White;
            this.MouseDown = false;
            this.MouseOver = false;
            textBox = new TextBox();
            btn = new Panel();
            this.Controls.Add(textBox);
            this.Controls.Add(btn);
            this.BorderStyle = BorderStyle.FixedSingle;
            textBox.BorderStyle = BorderStyle.None;
            
            textBox.Font = new Font("Tahoma", 8.25f);
            textBox.Location = new Point(1, 2);
            textBox.Size = new Size(this.Width - 20, this.Height - 2);
            textBox.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top;

            btn.Location = new Point(this.Width - 18, 0);
            btn.Size = new Size(18, this.Height);
            btn.Anchor = AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top;
            btn.BackColor = Color.White;

            btn.MouseEnter += new EventHandler(btn_MouseEnter);
            btn.MouseLeave += new EventHandler(btn_MouseLeave);
            btn.MouseDown += new MouseEventHandler(btn_MouseDown);
            btn.MouseUp += new MouseEventHandler(btn_MouseUp);
            btn.Paint += new PaintEventHandler(btn_Paint);
            btn.Click += new EventHandler(btn_Click);
            this.Resize += new EventHandler(TextBoxEx_Resize);
        }

        public void Clear()
        {
            textBox.Clear();
            this.Tag = null;
        }
        void TextBoxEx_Resize(object sender, EventArgs e)
        {
            if (brush != null) brush.Dispose();
            if (brushDown != null) brushDown.Dispose();
            brush = new LinearGradientBrush(new Point(0, 0), new Point(0, this.Height), Color.White, Color.LightSkyBlue);
            brushDown = new LinearGradientBrush(new Point(0, 0), new Point(0, this.Height), Color.FromArgb(220, 255, 255), Color.FromArgb(115, 186, 215));
        }

        void btn_Click(object sender, EventArgs e)
        {
            Clear();
        }
        
        #region Paint Button
        void btn_MouseUp(object sender, MouseEventArgs e)
        {
            MouseDown = false;
            btn.Invalidate();
        }
        void btn_MouseDown(object sender, MouseEventArgs e)
        {
            if (MouseOver) MouseDown = true;
            btn.Invalidate();
        }
        void btn_MouseLeave(object sender, EventArgs e)
        {
            MouseOver = false;
            MouseDown = false;
            btn.Invalidate();
        }
        void btn_MouseEnter(object sender, EventArgs e)
        {
            MouseOver = true;
            btn.Invalidate();
        }
        void btn_Paint(object sender, PaintEventArgs e)
        {
            if (btn.Enabled && MouseOver)
            {
                e.Graphics.FillRectangle(((MouseDown) ? brushDown : brush), btn.ClientRectangle);
            }
            if (btn.Enabled)
            {
                e.Graphics.DrawImageUnscaled(SmartPlus.Properties.Resources.TextBoxClear, 1, 1);
            }
            else
            {
                e.Graphics.DrawImageUnscaled(SmartPlus.Properties.Resources.TextBoxClearGray, 1, 1);
            }
        }
        #endregion

        public TextBox TextBox { get { return textBox; } }
        public Panel PanelButton { get { return btn; } }
    }
}
