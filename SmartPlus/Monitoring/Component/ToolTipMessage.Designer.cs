﻿namespace SmartPlus
{
    partial class ToolTipMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelForm = new System.Windows.Forms.Panel();
            this.pClose = new System.Windows.Forms.Panel();
            this.img = new System.Windows.Forms.PictureBox();
            this.lText = new System.Windows.Forms.Label();
            this.lTitle = new System.Windows.Forms.Label();
            this.panelForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.Color.Cornsilk;
            this.panelForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelForm.Controls.Add(this.pClose);
            this.panelForm.Controls.Add(this.img);
            this.panelForm.Controls.Add(this.lText);
            this.panelForm.Controls.Add(this.lTitle);
            this.panelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(306, 74);
            this.panelForm.TabIndex = 4;
            this.panelForm.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelForm_MouseMove);
            this.panelForm.Click += new System.EventHandler(this.panelForm_Click);
            // 
            // pClose
            // 
            this.pClose.BackColor = System.Drawing.Color.Cornsilk;
            this.pClose.BackgroundImage = global::SmartPlus.Properties.Resources.delete;
            this.pClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pClose.Location = new System.Drawing.Point(279, 2);
            this.pClose.Name = "pClose";
            this.pClose.Size = new System.Drawing.Size(22, 22);
            this.pClose.TabIndex = 8;
            this.pClose.MouseLeave += new System.EventHandler(this.pClose_MouseLeave);
            this.pClose.Click += new System.EventHandler(this.pClose_Click);
            this.pClose.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pClose_MouseDown);
            this.pClose.MouseEnter += new System.EventHandler(this.pClose_MouseEnter);
            // 
            // img
            // 
            this.img.Location = new System.Drawing.Point(8, 3);
            this.img.Name = "img";
            this.img.Size = new System.Drawing.Size(42, 39);
            this.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img.TabIndex = 6;
            this.img.TabStop = false;
            this.img.Click += new System.EventHandler(this.img_Click);
            // 
            // lText
            // 
            this.lText.AutoSize = true;
            this.lText.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lText.Location = new System.Drawing.Point(59, 27);
            this.lText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lText.MaximumSize = new System.Drawing.Size(260, 100);
            this.lText.MinimumSize = new System.Drawing.Size(233, 37);
            this.lText.Name = "lText";
            this.lText.Size = new System.Drawing.Size(233, 37);
            this.lText.TabIndex = 5;
            this.lText.Text = "Title\r\n";
            this.lText.Click += new System.EventHandler(this.lText_Click);
            this.lText.ClientSizeChanged += new System.EventHandler(this.lText_ClientSizeChanged);
            // 
            // lTitle
            // 
            this.lTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lTitle.Location = new System.Drawing.Point(57, 2);
            this.lTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTitle.Name = "lTitle";
            this.lTitle.Size = new System.Drawing.Size(215, 21);
            this.lTitle.TabIndex = 4;
            this.lTitle.Text = "Title";
            this.lTitle.Click += new System.EventHandler(this.lTitle_Click);
            // 
            // ToolTipMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(306, 74);
            this.ControlBox = false;
            this.Controls.Add(this.panelForm);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ToolTipMessage";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ToolTipMessage";
            this.panelForm.ResumeLayout(false);
            this.panelForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.PictureBox img;
        private System.Windows.Forms.Label lText;
        private System.Windows.Forms.Label lTitle;
        private System.Windows.Forms.Panel pClose;

    }
}