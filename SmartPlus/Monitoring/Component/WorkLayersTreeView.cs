﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using SmartPlus.Dialogs;
using System.IO;
using System.Collections;

namespace SmartPlus
{
    public sealed class TreeViewWorkLayers : TreeView
    {
        // todo: Копирование/вставка списков скважин в рабочей папке
        // MultiSelect
        private List<TreeNode> m_SelectedNodes = new List<TreeNode>();
        private TreeNode LastSelectNode = null;
        private TreeNode CurrentShiftNode = null;
        private TreeNode selectedProfileNode = null;
        private TreeNode selectedMarkerNode = null;
        private TreeNode selectedWellListNode = null;
        public TreeNode SelectedProfileNode { get { return selectedProfileNode; } }
        public TreeNode SelectedMarkerNode { get { return selectedMarkerNode; } }
        public TreeNode SelectedWellListNode { get {return selectedWellListNode;} }

        /// <summary>
        /// List of selected nodes
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<TreeNode> SelectedNodes
        {
            get { return m_SelectedNodes; }
            set
            {
                BeginUpdate();

                for (int i = m_SelectedNodes.Count - 1; i >= 0; --i)
                    removeSelectedNode(m_SelectedNodes[i]);
                for (int i = value.Count - 1; i >= 0; --i)
                    addSelectedNode(value[i]);

                EndUpdate();
            }
        }

        // MultiSelect
        CanvasMap _canv;
        NewTextForm fText;
        bool localDrag;
        bool dragTargetNodeSelected = false;
        bool cancelDouble = false;
        Font FontBold;
        public ContextMenuStrip cMenu; //, cMenuImage, cMenuContour, cMenuWellList, cMenuWorkFolder, cMenuGrid, cMenuProfile;

        ToolStripMenuItem cmImageEditMode, cmImageDropCoef;
        ToolStripMenuItem cmContourResetIndex, cmContourSetNearPoints, cmContourOperations;
        ToolStripMenuItem cmCntrOpUnion, cmCntrOpDiff, cmCntrOpCross, cmCntrOpSymDiff, cmCntrOpAverageStep;
        ToolStripMenuItem cmCntrRemove, cmCntrSettings;
        ToolStripMenuItem cmVoronoiShowReserves, cmVoronoiExportContours;

        ToolStripMenuItem cmWFCreate, cmWFSetColor;
        ToolStripMenuItem cmImportGrid, cmImportContours;

        //ToolStripMenuItem cmGridEditPalette, cmGridDrawLines;
        ToolStripMenuItem cmCopy, cmPaste, cmRename, cmDelete, cmSendMail;
        ToolStripSeparator cmSeparator;
        ToolStripMenuItem cmSettings;
        ToolStripSeparator cmSettingsSep;

        BackgroundWorker loader;
        List<TreeNode> lastLoadedNodes;
        Timer loaderTimer;

        public TreeViewWorkLayers(Control Parent, CanvasMap Canvas)
        {
            this._canv = Canvas;
            this.Parent = Parent;
            this.Dock = DockStyle.None;
            this.Anchor = (AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top);
            this.Left = Parent.ClientRectangle.X;
            this.Top = Parent.ClientRectangle.Y + 26;
            this.Width = Parent.Width;
            this.Height = Parent.Height - 26;
            this.Font = new Font("Calibri", 8.25f);
            this.FontBold = new Font("Calibri", 8.25f, FontStyle.Bold);
            this.AllowDrop = true;
            this.fText = new NewTextForm(_canv.mainForm);
            this.localDrag = false;
            this.CheckBoxes = false;
            this.FullRowSelect = false;
            this.HideSelection = false;
            this.Scrollable = true;

            this.ImageList = new ImageList();
            this.ImageList.ColorDepth = ColorDepth.Depth24Bit;
            this.ImageList.Images.Add(Properties.Resources.def);            //0
            this.ImageList.Images.Add(Properties.Resources.EditIcon16);     //1
            this.ImageList.Images.Add(Properties.Resources.Folder);         //2
            this.ImageList.Images.Add(Properties.Resources.doc);            //3
            this.ImageList.Images.Add(Properties.Resources.pdf);            //4
            this.ImageList.Images.Add(Properties.Resources.ppt);            //5
            this.ImageList.Images.Add(Properties.Resources.png);            //6
            this.ImageList.Images.Add(Properties.Resources.txt);            //7
            this.ImageList.Images.Add(Properties.Resources.xls);            //8
            this.ImageList.Images.Add(Properties.Resources.Oilfield);       //9
            this.ImageList.Images.Add(Properties.Resources.OilfieldArea);   //10
            this.ImageList.Images.Add(Properties.Resources.WellPad);        //11
            this.ImageList.Images.Add(Properties.Resources.Well);           //12
            this.ImageList.Images.Add(Properties.Resources.ProjectWell);    //13
            this.ImageList.Images.Add(Properties.Resources.Contour);        //14
            this.ImageList.Images.Add(Properties.Resources.Profile);        //15
            this.ImageList.Images.Add(Properties.Resources.Marker);         //16
            this.ImageList.Images.Add(Properties.Resources.Grid);           //17
            this.ImageList.Images.Add(Properties.Resources.WellList);       //18
            this.ImageList.Images.Add(Properties.Resources.VoronoiMap);     //19
            this.ImageList.Images.Add(Properties.Resources.PVT);            //20
            this.ImageList.Images.Add(Properties.Resources.Bashneft16);     //21
            this.ImageList.Images.Add(Properties.Resources.NGDU);           //22
            this.ImageList.Images.Add(Properties.Resources.Image);          //23
            this.ImageList.Images.Add(Properties.Resources.Deposit);        //24
            this.ImageList.Images.Add(Properties.Resources.wait_icon);      //25
            this.ImageList.Images.Add(Properties.Resources.wait_icon1);     //26
            this.ImageList.Images.Add(Properties.Resources.wait_icon2);     //27
            this.ImageList.Images.Add(Properties.Resources.wait_icon3);     //28
            this.ImageList.Images.Add(Properties.Resources.wait_icon4);     //29
            this.ImageList.Images.Add(Properties.Resources.wait_icon5);     //30
            this.ImageList.Images.Add(Properties.Resources.wait_icon6);     //31
            this.ImageList.Images.Add(Properties.Resources.wait_icon7);     //32
            this.ImageList.Images.Add(Properties.Resources.wait_icon8);     //33
            this.ImageList.Images.Add(Properties.Resources.wait_icon9);     //34
            this.ImageList.Images.Add(Properties.Resources.wait_icon10);    //35
            this.ImageList.Images.Add(Properties.Resources.wait_icon11);    //36

            InitializeContextMenus();

            loader = new BackgroundWorker();
            loader.DoWork += loader_DoWork;
            loader.RunWorkerCompleted += loader_RunWorkerCompleted;
            lastLoadedNodes = new List<TreeNode>();
            loader.WorkerSupportsCancellation = true;
            loader.WorkerReportsProgress = true;
            loaderTimer = new Timer();
            loaderTimer.Enabled = false;
            loaderTimer.Interval = 100;
            loaderTimer.Tick += loaderUpdater_Tick;

            this.MouseDown += new MouseEventHandler(TreeViewWorkLayers_MouseDown);
            this.KeyDown += new KeyEventHandler(TreeViewWorkLayers_KeyDown);
            this.MouseEnter += new EventHandler(TreeViewWorkLayers_MouseEnter);
            this.ItemDrag += new ItemDragEventHandler(TreeViewWorkLayers_ItemDrag);
            this.DragEnter += new DragEventHandler(TreeViewWorkLayers_DragEnter);
            this.DragDrop += new DragEventHandler(TreeViewWorkLayers_DragDrop);
            this.DragOver += new DragEventHandler(TreeViewWorkLayers_DragOver);
        }

        #region MultiSelect
        protected override void OnBeforeSelect(TreeViewCancelEventArgs e)
        {
            base.OnBeforeSelect(e);
            _canv.Disable();
            if (e.Action != TreeViewAction.Unknown)
            {
                BeginUpdate();
                CurrentShiftNode = null;
                if (ModifierKeys == Keys.Control)
                {
                    if (m_SelectedNodes.Contains(e.Node))
                        removeSelectedNode(e.Node);
                    else
                        addSelectedNode(e.Node);
                    LastSelectNode = e.Node;
                }
                else if ((ModifierKeys == Keys.Shift) && (LastSelectNode != null) && (LastSelectNode != e.Node))
                {
                    for (int i = m_SelectedNodes.Count - 1; i >= 0; i--)
                    {
                        removeSelectedNode(m_SelectedNodes[i]);
                    }
                    SetShiftSelectNodes(e.Node);
                }
                else
                {
                    for (int i = m_SelectedNodes.Count - 1; i >= 0; i--)
                    {
                        removeSelectedNode(m_SelectedNodes[i]);
                    }
                    addSelectedNode(e.Node);
                    LastSelectNode = e.Node;
                }
                EndUpdate();
            }
            _canv.Enable();
            _canv.DrawLayerList();
            e.Cancel = true;
        }
        protected TreeNode SelectRecusiveNodes(TreeNode currNode, ref TreeNode startNode)
        {
            TreeNode endNode = null;
            if (startNode == null)
            {
                if (currNode == LastSelectNode) startNode = LastSelectNode;
                else if (currNode == CurrentShiftNode) startNode = CurrentShiftNode;
                if ((startNode != null) && (!m_SelectedNodes.Contains(startNode))) addSelectedNode(startNode);
            }
            else if (endNode == null)
            {
                if ((startNode == LastSelectNode) && (currNode == CurrentShiftNode))
                    endNode = CurrentShiftNode;
                else if ((startNode == CurrentShiftNode) && (currNode == LastSelectNode))
                    endNode = LastSelectNode;
                if (!m_SelectedNodes.Contains(currNode)) addSelectedNode(currNode);
            }

            if ((endNode == null) && (currNode.Nodes.Count > 0) && (currNode.IsExpanded))
            {
                int i = 0;
                while ((endNode == null) && (i < currNode.Nodes.Count))
                {
                    endNode = SelectRecusiveNodes(currNode.Nodes[i], ref startNode);
                    i++;
                }
            }
            return endNode;
        }
        protected void SetShiftSelectNodes(TreeNode tn)
        {
            CurrentShiftNode = tn;
            TreeNode startNode = null, endNode = null;
            if (this.Nodes.Count > 0)
            {
                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (SelectRecusiveNodes(Nodes[i], ref startNode) != null) break;
                }
            }
        }
        public void ClearSelectedList()
        {
            BeginUpdate();
            for (int i = m_SelectedNodes.Count - 1; i >= 0; --i)
                removeSelectedNode(m_SelectedNodes[i]);
            EndUpdate();
        }
        public void SetOneSelectedNode(TreeNode tn)
        {
            BeginUpdate();
            for (int i = m_SelectedNodes.Count - 1; i >= 0; --i)
                removeSelectedNode(m_SelectedNodes[i]);
            addSelectedNode(tn);
            while (tn.Parent != null)
            {
                tn = tn.Parent;
                if (!tn.IsExpanded) tn.Expand();
            }
            EndUpdate();
        }

        /// <summary>
        /// Add selected node to list
        /// </summary>
        /// <param name="node">Selected node</param>
        protected void addSelectedNode(TreeNode node)
        {
            if ((node.Tag != null) && (((C2DLayer)node.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
            {
                int ind = _canv.selLayerList.IndexOf((C2DLayer)node.Tag);
                if (ind == -1)
                {
                    ((C2DLayer)node.Tag).Selected = true;
                    _canv.selLayerList.Add((C2DLayer)node.Tag);
                    _canv.DrawLayerList();
                }
            }
            m_SelectedNodes.Add(node);
            node.BackColor = SystemColors.Highlight;
            node.ForeColor = SystemColors.HighlightText;
        }
        /// <summary>
        /// Remove selected node from list
        /// </summary>
        /// <param name="node">Selected node</param>
        protected void removeSelectedNode(TreeNode node)
        {
            if ((node == selectedProfileNode) ||
                (node == selectedMarkerNode) ||
                (node == selectedWellListNode))
            {
                SetNodeFontBold(node, true);
            }
            else
            {
                node.BackColor = BackColor;
                node.ForeColor = ForeColor;
            }
            if ((node.Tag != null) && (((C2DLayer)node.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
            {
                int ind = _canv.selLayerList.IndexOf((C2DLayer)node.Tag);
                if (ind > -1)
                {
                    ((C2DLayer)_canv.selLayerList[ind]).Selected = false;
                    _canv.selLayerList.RemoveAt(ind);
                    _canv.DrawLayerList();
                }
            }
            m_SelectedNodes.Remove(node);
        }
        #endregion

        #region ContextMenuStrip

        // constructor context menu
        //
        private void InitializeContextMenus()
        {
            cMenu = new ContextMenuStrip();

            // Меню Image
            // Режим редактирования
            cmImageEditMode = (ToolStripMenuItem)cMenu.Items.Add("");
            cmImageEditMode.Text = "Режим редактирования";
            cmImageEditMode.CheckOnClick = true;
            cmImageEditMode.CheckedChanged += new EventHandler(cmEditMode_CheckedChanged);
            cmImageEditMode.Click += new EventHandler(cmEditMode_Click);

            // Сбросить систему координат
            cmImageDropCoef = (ToolStripMenuItem)cMenu.Items.Add("");
            cmImageDropCoef.Text = "Сбросить коэффициенты";
            cmImageDropCoef.Visible = false;
            cmImageDropCoef.Click += new EventHandler(cmDropCoef_Click);

            // Меню Contour
            //cmContourResetIndex = (ToolStripMenuItem)cMenu.Items.Add("");
            //cmContourResetIndex.Text = "Установить индекс...";
            //cmContourResetIndex.Click += new EventHandler(cmResetIndex_Click);

            cmContourOperations = (ToolStripMenuItem)cMenu.Items.Add("Операции над контурами");
            cmCntrOpUnion = (ToolStripMenuItem)cmContourOperations.DropDownItems.Add("Объединение");
            cmCntrOpUnion.Image = SmartPlus.Properties.Resources.Cntr_Union;
            cmCntrOpUnion.Click += new EventHandler(cmCntrOpUnion_Click);

            cmCntrOpDiff = (ToolStripMenuItem)cmContourOperations.DropDownItems.Add("Разность...");
            cmCntrOpDiff.Image = SmartPlus.Properties.Resources.Cntr_Diff;
            cmCntrOpDiff.Click += new EventHandler(cmCntrOpDiff_Click);

            cmCntrOpCross = (ToolStripMenuItem)cmContourOperations.DropDownItems.Add("Пересечение");
            cmCntrOpCross.Image = SmartPlus.Properties.Resources.Cntr_Cross;
            cmCntrOpCross.Click += new EventHandler(cmCntrOpCross_Click);

            cmCntrOpSymDiff = (ToolStripMenuItem)cmContourOperations.DropDownItems.Add("Симметрическая разность...");
            cmCntrOpSymDiff.Image = SmartPlus.Properties.Resources.Cntr_SymDiff;
            cmCntrOpSymDiff.Click += new EventHandler(cmCntrOpSymDiff_Click);

            cmCntrOpAverageStep = (ToolStripMenuItem)cMenu.Items.Add("Усреднить шаг точек...");
            cmCntrOpAverageStep.Click += new EventHandler(cmCntrOpAverageStep_Click);

            // Меню ПАПКИ
            cmWFCreate = (ToolStripMenuItem)cMenu.Items.Add("");
            cmWFCreate.Text = "Создать папку";
            cmWFCreate.Image = SmartPlus.Properties.Resources.Folder_add;
            cmWFCreate.Click += new EventHandler(cmCreateFolderWF_Click);

            cmImportGrid = (ToolStripMenuItem)cMenu.Items.Add("Загрузить сетку...", SmartPlus.Properties.Resources.Add_Grid16);
            cmImportGrid.Click += new EventHandler(cmImportGrid_Click);

            cmImportContours = (ToolStripMenuItem)cMenu.Items.Add("Загрузить контуры...", SmartPlus.Properties.Resources.AddContour);
            cmImportContours.Click += new EventHandler(cmImportContours_Click);
            //cmWFSetColor = (ToolStripMenuItem)cMenu.Items.Add("");
            //cmWFSetColor.Text = "Установить цвет...";
            //cmWFSetColor.Click += new EventHandler(cmSetColorWF_Click);

            // GRID
            //cmGridDrawLines = (ToolStripMenuItem)cMenu.Items.Add("");
            //cmGridDrawLines.CheckOnClick = true;
            //cmGridDrawLines.Text = "Отобразить линии сетки";
            //cmGridDrawLines.CheckedChanged += new EventHandler(cmDrawGridLines_CheckedChanged);

            //cmGridEditPalette = (ToolStripMenuItem)cMenu.Items.Add("");
            //cmGridEditPalette.Text = "Изменить палитру";
            //cmGridEditPalette.Click += new EventHandler(cmEditPalette_Click);


            // VORONOI MAP
            cmVoronoiShowReserves = (ToolStripMenuItem)cMenu.Items.Add("Запасы по областям Вороного...");
            cmVoronoiShowReserves.Click += new EventHandler(cmVoronoiShowReserves_Click);

            cmVoronoiExportContours = (ToolStripMenuItem)cMenu.Items.Add("Экспорт контуров областей Вороного...");
            cmVoronoiExportContours.Click += cmVoronoiExportContours_Click;

            // COMMON
            cmSendMail = (ToolStripMenuItem)cMenu.Items.Add("Отправить по почте", SmartPlus.Properties.Resources.Send_Email16, cmSendMail_Click);

            cmCopy = (ToolStripMenuItem)cMenu.Items.Add("");
            cmCopy.Text = "Копировать";
            cmCopy.Click += new EventHandler(cmCopy_Click);

            cmPaste = (ToolStripMenuItem)cMenu.Items.Add("");
            cmPaste.Text = "Вставить";
            cmPaste.Click += new EventHandler(cmPaste_Click);

            cmRename = (ToolStripMenuItem)cMenu.Items.Add("");
            cmRename.Text = "Переименовать...";
            cmRename.Click += new EventHandler(RenameWorkLayer_Click);

            cmSeparator = (ToolStripSeparator)cMenu.Items.Add("-");
            // Удалить
            cmDelete = (ToolStripMenuItem)cMenu.Items.Add("");
            cmDelete.Text = "Удалить";
            cmDelete.Image = SmartPlus.Properties.Resources.Delete16;
            cmDelete.Click += new EventHandler(DeleteWorkLayer);

            cmSettingsSep = (ToolStripSeparator)cMenu.Items.Add("-");
            cmSettings = (ToolStripMenuItem)cMenu.Items.Add("");
            cmSettings.Text = "Настройки...";
            cmSettings.Image = SmartPlus.Properties.Resources.Settings16;
            cmSettings.Click += new EventHandler(cmSettings_Click);

            #region ContextMenu Other
            //cMenuImage = new ContextMenuStrip();
            //cMenuContour = new ContextMenuStrip();
            //cMenuWellList = new ContextMenuStrip();
            //cMenuWorkFolder = new ContextMenuStrip();
            //cMenuProfile = new ContextMenuStrip();
            //// Меню Image
            //// Режим редактирования
            //cmEditMode = (ToolStripMenuItem)cMenuImage.Items.Add("");
            //cmEditMode.Text = "Режим редактирования";
            //cmEditMode.CheckOnClick = true;
            //cmEditMode.CheckedChanged += new EventHandler(cmEditMode_CheckedChanged);
            //cmEditMode.Click += new EventHandler(cmEditMode_Click);

            //// Сбросить систему координат
            //cmDropCoef = (ToolStripMenuItem)cMenuImage.Items.Add("");
            //cmDropCoef.Text = "Сбросить коэффициенты";
            //cmDropCoef.Enabled = false;
            //cmDropCoef.Click += new EventHandler(cmDropCoef_Click);
            //cMenuImage.Items.Add("-");

            //// Удалить
            //cmDelImage = (ToolStripMenuItem)cMenuImage.Items.Add("");
            //cmDelImage.Text = "Удалить";
            //cmDelImage.Click += new EventHandler(DeleteWorkLayer);

            //// Меню Contour
            //cmRenameContour = (ToolStripMenuItem)cMenuContour.Items.Add("");
            //cmRenameContour.Text = "Переименовать...";
            //cmRenameContour.Click += new EventHandler(RenameWorkLayer);

            //cmResetIndex = (ToolStripMenuItem)cMenuContour.Items.Add("");
            //cmResetIndex.Text = "Установить индекс...";
            //cmResetIndex.Click += new EventHandler(cmResetIndex_Click);

            //cmCopyContour = (ToolStripMenuItem)cMenuContour.Items.Add("");
            //cmCopyContour.Text = "Копировать";
            //cmCopyContour.Click += new EventHandler(cmCopyContour_Click);

            //cmPasteContour = (ToolStripMenuItem)cMenuContour.Items.Add("");
            //cmPasteContour.Text = "Вставить";
            //cmPasteContour.Click += new EventHandler(cmPasteContour_Click);

            //cmSetColorCntr = (ToolStripMenuItem)cMenuContour.Items.Add("");
            //cmSetColorCntr.Text = "Установить цвет...";
            //cmSetColorCntr.Click += new EventHandler(cmSetColorCntr_Click);

            //cMenuContour.Items.Add("-");

            //cmDelContour = (ToolStripMenuItem)cMenuContour.Items.Add("");
            //cmDelContour.Text = "Удалить";
            //cmDelContour.Click += new EventHandler(DeleteWorkLayer);

            //// Меню Списка скважин
            //// Удалить
            //cmRenameWellList = (ToolStripMenuItem)cMenuWellList.Items.Add("");
            //cmRenameWellList.Text = "Переименовать...";
            //cmRenameWellList.Click += new EventHandler(RenameWorkLayer);

            //cMenuWellList.Items.Add("-");

            //cmDelWellList = (ToolStripMenuItem)cMenuWellList.Items.Add("");
            //cmDelWellList.Text = "Удалить";
            //cmDelWellList.Click += new EventHandler(DeleteWorkLayer);

            //// Меню Рабочей папки
            //cmCreateFolderWF = (ToolStripMenuItem)cMenuWorkFolder.Items.Add("");
            //cmCreateFolderWF.Text = "Создать папку";
            //cmCreateFolderWF.Click += new EventHandler(cmCreateFolderWF_Click);

            //cmRenameWF = (ToolStripMenuItem)cMenuWorkFolder.Items.Add("");
            //cmRenameWF.Text = "Переименовать...";
            //cmRenameWF.Click += new EventHandler(cmRenameWF_Click);

            //cmSetColorWF = (ToolStripMenuItem)cMenuWorkFolder.Items.Add("");
            //cmSetColorWF.Text = "Установить цвет...";
            //cmSetColorWF.Click += new EventHandler(cmSetColorWF_Click);

            //cMenuWorkFolder.Items.Add("-");

            //cmDeleteWF = (ToolStripMenuItem)cMenuWorkFolder.Items.Add("");
            //cmDeleteWF.Text = "Удалить папку";
            //cmDeleteWF.Click += new EventHandler(cmDeleteWF_Click);

            //// GRID

            //cMenuGrid = new ContextMenuStrip();
            //cmDrawGridLines = (ToolStripMenuItem)cMenuGrid.Items.Add("");
            //cmDrawGridLines.CheckOnClick = true;
            //cmDrawGridLines.Text = "Отобразить линии сетки";
            //cmDrawGridLines.CheckedChanged += new EventHandler(cmDrawGridLines_CheckedChanged);

            //cmEditPalette = (ToolStripMenuItem)cMenuGrid.Items.Add("");
            //cmEditPalette.Text = "Изменить палитру";
            //cmEditPalette.Click += new EventHandler(cmEditPalette_Click);

            //cMenuGrid.Items.Add("-");
            //cmDeleteGrid = (ToolStripMenuItem)cMenuGrid.Items.Add("");
            //cmDeleteGrid.Text = "Удалить";
            //cmDeleteGrid.Click += new EventHandler(DeleteWorkLayer);

            //// Меню Профиль
            //// Переименовать
            //cmRenameProfile = (ToolStripMenuItem)cMenuProfile.Items.Add("");
            //cmRenameProfile.Text = "Переименовать...";
            //cmRenameProfile.Click += new EventHandler(RenameWorkLayer);

            //cMenuProfile.Items.Add("-");
            //// Удалить
            //cmDeleteProfile = (ToolStripMenuItem)cMenuProfile.Items.Add("");
            //cmDeleteProfile.Text = "Удалить";
            //cmDeleteProfile.Click += new EventHandler(DeleteWorkLayer);
            #endregion ContextMenu
        }

        // Меню Image
        //
        private void SetTNImageIndex(TreeNode tn, int ImageIndex)
        {
            tn.ImageIndex = ImageIndex;
            tn.SelectedImageIndex = ImageIndex;
        }
        void cmEditMode_Click(object sender, EventArgs e)
        {
            if (cmImageEditMode.Checked)
            {
                SetTNImageIndex(this.SelectedNodes[0], 1);
            }
            else if (this.SelectedNodes[0].Tag != null)
            {
                int imgIndex = _canv.GetImageIndex(((C2DLayer)this.SelectedNodes[0].Tag).ObjTypeID);
                SetTNImageIndex(this.SelectedNodes[0], imgIndex);
            }
            else
            {
                SetTNImageIndex(this.SelectedNodes[0], 0);
            }
            SetEditMode(this.SelectedNodes[0], cmImageEditMode.Checked);
            this.Update();
        }
        void cmEditMode_CheckedChanged(object sender, EventArgs e)
        {
            cmImageDropCoef.Enabled = cmImageEditMode.Checked;
        }
        private void SetEditMode(TreeNode tn, bool EditMode)
        {
            if (tn.Tag != null)
            {
                C2DLayer layer = (C2DLayer)tn.Tag;
                _canv.Rotate = false;
                if (EditMode) _canv.EditedLayer = layer;
                else _canv.EditedLayer = null;
            }
        }
        public void ResetEditMode()
        {
            cmImageEditMode.PerformClick();
        }

        // Сброс коэфициентов
        //
        void cmDropCoef_Click(object sender, EventArgs e)
        {
            _canv.ResetSK(((C2DLayer)this.SelectedNodes[0].Tag).Index);
        }

        // Меню Contour
        // 
        void cmResetIndex_Click(object sender, EventArgs e)
        {
            TreeNode tn = this.SelectedNodes[0];
            int ind = -1;
            bool cycle = true, ResetIndex = false;
            if ((tn.Tag != null) && ((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                TreeNode parentNode = tn.Parent;
                while (cycle)
                {
                    if (fText.ShowNewIndex(((C2DObject)tn.Tag).Index + 1) == DialogResult.OK)
                    {
                        try
                        {
                            ind = Convert.ToInt32(fText.NewText) - 1;
                        }
                        catch (Exception ex)
                        {
                            ind = -1;
                        }

                        if ((ind > -1) && (ind < parentNode.Nodes.Count))
                        {
                            cycle = false;
                            if (((C2DObject)parentNode.Nodes[ind].Tag).Index == ind)
                            {
                                cycle = false;
                                ResetIndex = true;
                            }
                            else
                            {
                                MessageBox.Show("Ошибка установки индекса!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Введите правильный индекс!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else cycle = false;
                }

                if ((ResetIndex) && (((C2DObject)tn.Tag).Index != ind))
                {
                    C2DLayer layer = (C2DLayer)tn.Tag;
                    C2DLayer lr;
                    C2DLayer parentLayer = (C2DLayer)parentNode.Tag;
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                    {
                        int oldIndex = layer.Index;
                        parentLayer.ObjectsList.Insert(ind, layer);
                        if (ind <= oldIndex)
                        {
                            oldIndex++;
                        }
                        parentLayer.ObjectsList.RemoveAt(oldIndex);
                        this.BeginUpdate();
                        for (int i = 0; i < parentLayer.ObjectsList.Count; i++)
                        {
                            lr = (C2DLayer)parentLayer.ObjectsList[i];
                            lr.Index = i;
                            if (lr.Name.IndexOf("Ячейк") > -1)
                            {
                                lr.Name = "Ячейка(" + (lr.Index + 1).ToString() + ")";
                                ((C2DObject)lr.ObjectsList[0]).Name = lr.Name;
                                lr.node.Text = lr.Name;
                            }
                        }
                        ((Contour)layer.ObjectsList[0]).VisibleName = true;
                        TreeNode copyNode = (TreeNode)tn.Clone();
                        parentNode.Nodes.Insert(ind, copyNode);
                        copyNode.Tag = layer;
                        layer.node = parentNode.Nodes[ind];
                        copyNode.Text = layer.Name;
                        copyNode.Checked = tn.Checked;
                        parentNode.Nodes.Remove(tn);
                        this.EndUpdate();
                        //this.SelectedNode = parentNode.Nodes[ind];
                        SetOneSelectedNode(parentNode.Nodes[ind]);
                        //parentNode.Nodes[ind].BackColor = Color.LightGreen;
                        _canv.LayerWorkListChanged = true;
                    }
                }
                else if (ResetIndex)
                {
                    if (((C2DObject)tn.Tag).Name.IndexOf("Ячейк") > -1)
                    {
                        ((Contour)((C2DLayer)tn.Tag).ObjectsList[0]).VisibleName = true;
                    }
                }
            }
        }
        void cmContourSetNearPoints_Click(object sender, EventArgs e)
        {
            TreeNode tn = this.SelectedNodes[0];
            if (tn.Tag != null) _canv.SetContoursNearPoint((C2DLayer)tn.Tag);
        }
        void cmCntrOpUnion_Click(object sender, EventArgs e)
        {
            if (SelectedNodes.Count > 1)
            {
                List<C2DLayer> layers = new List<C2DLayer>();
                for (int i = 0; i < SelectedNodes.Count; i++)
                {
                    if (SelectedNodes[i].Tag != null)
                    {
                        layers.Add((C2DLayer)SelectedNodes[i].Tag);
                    }

                }
                if (layers.Count > 0) _canv.OperateContours(layers, ContoursOperation.Union);
            }
        }
        void cmCntrOpDiff_Click(object sender, EventArgs e)
        {
            if (SelectedNodes.Count > 0)
            {
                List<C2DLayer> layers = new List<C2DLayer>();
                for (int i = 0; i < SelectedNodes.Count; i++)
                {
                    if (SelectedNodes[i].Tag != null)
                    {
                        layers.Add((C2DLayer)SelectedNodes[i].Tag);
                    }

                }
                WorkFolderObjectSelectForm form = new WorkFolderObjectSelectForm(_canv.mainForm);
                if (form.Show(Constant.BASE_OBJ_TYPES_ID.CONTOUR, _canv.LayerWorkList, layers, "Выберите вычитаемые контуры:") == DialogResult.OK)
                {
                    if ((layers.Count > 0) && (form.SelectedLayerList.Count > 0)) _canv.OperateContours(layers, ContoursOperation.Difference, form.SelectedLayerList);
                }
            }
        }
        void cmCntrOpCross_Click(object sender, EventArgs e)
        {
            if (SelectedNodes.Count > 1)
            {
                List<C2DLayer> layers = new List<C2DLayer>();
                for (int i = 0; i < SelectedNodes.Count; i++)
                {
                    if (SelectedNodes[i].Tag != null)
                    {
                        layers.Add((C2DLayer)SelectedNodes[i].Tag);
                    }

                }
                if (layers.Count > 0) _canv.OperateContours(layers, ContoursOperation.Crossing);
            }

        }
        void cmCntrOpSymDiff_Click(object sender, EventArgs e)
        {
            if (SelectedNodes.Count > 0)
            {
                List<C2DLayer> layers = new List<C2DLayer>();
                for (int i = 0; i < SelectedNodes.Count; i++)
                {
                    if (SelectedNodes[i].Tag != null)
                    {
                        layers.Add((C2DLayer)SelectedNodes[i].Tag);
                    }

                }
                WorkFolderObjectSelectForm form = new WorkFolderObjectSelectForm(_canv.mainForm);
                if (form.Show(Constant.BASE_OBJ_TYPES_ID.CONTOUR, _canv.LayerWorkList, layers, "Выберите вычитаемые контуры:") == DialogResult.OK)
                {
                    if ((layers.Count > 0) && (form.SelectedLayerList.Count > 0)) _canv.OperateContours(layers, ContoursOperation.SymmetricDifference, form.SelectedLayerList);
                }
            }
        }
        void cmCntrOpAverageStep_Click(object sender, EventArgs e)
        {
            if ((SelectedNodes.Count == 1) && (SelectedNodes[0].Tag != null))
            {
                _canv.SetContoursAverageStep((C2DLayer)SelectedNodes[0].Tag);
            }
        }

        // Рабочая папка
        //
        public int GetFileIconIndex(string Extention)
        {
            switch (Extention)
            {
                case ".doc": return 3;
                case ".docx": return 3;
                case ".docm": return 3;
                case ".pdf": return 4;
                case ".ppt": return 5;
                case ".pptx": return 5;
                case ".pptm": return 5;
                case ".bmp": return 6;
                case ".jpg": return 6;
                case ".jpeg": return 6;
                case ".png": return 6;
                case ".tif": return 6;
                case ".tiff": return 6;
                case ".txt": return 7;
                case ".xls": return 8;
                case ".xlsx": return 8;
                case ".xlsm": return 8;
                case ".xlsb": return 8;
                default: return 0;
            }
        }
        
        void cmCreateFolderWF_Click(object sender, EventArgs e)
        {
            TreeNode selNode = this.SelectedNodes[0], tn;
            string name = _canv.mainForm.UserSmartPlusFolder + "\\" + selNode.FullPath;
            bool cancel = false;
            if (Directory.Exists(name))
            {
                bool cycle = true;
                while (cycle)
                {
                    if (fText.ShowNewLayer() == DialogResult.OK)
                    {
                        if (Directory.Exists(name + "\\" + fText.NewText))
                        {
                            MessageBox.Show("Папка " + fText.NewText + " уже существует. Введите другое имя!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                            cycle = false;
                    }
                    else
                    {
                        cancel = true;
                        break;
                    }
                }
                if ((!cancel) && (fText.NewText != ""))
                {
                    Directory.CreateDirectory(name + "\\" + fText.NewText);
                    tn = selNode.Nodes.Add("", "", 2, 2);
                    tn.Text = fText.NewText;
                    tn.Checked = true;
                    tn.ContextMenuStrip = this.cMenu;

                    _canv.CreateSubFolder(fText.NewText, tn);
                }
            }
        }
        void cmSetColorWF_Click(object sender, EventArgs e)
        {
            SetLayerColor();
        }
        void cmImportContours_Click(object sender, EventArgs e)
        {
            _canv.LoadContoursFromFile();
        }

        // GRID
        //void cmDrawGridLines_CheckedChanged(object sender, EventArgs e)
        //{
        //    TreeNode tn = this.SelectedNodes[0];
        //    if ((tn.Tag != null) && (((BaseObj)tn.Tag).TypeID == (int)(Constant.BASE_OBJ_TYPES_ID.LAYER)) &&
        //        (((C2DLayer)tn.Tag).ObjTypeID == (int)(Constant.BASE_OBJ_TYPES_ID.GRID)))
        //    {
        //        ((GridEx)((C2DLayer)tn.Tag)._ObjectsList[0]).DrawGridLines = cmGridDrawLines.Checked;
        //        _canv.DrawLayerList();
        //    }
        //}
        //void cmEditPalette_Click(object sender, EventArgs e)
        //{
        //    TreeNode tn = this.SelectedNodes[0];
        //    if ((tn.Tag != null) && (((BaseObj)tn.Tag).TypeID == (int)(Constant.BASE_OBJ_TYPES_ID.LAYER)) &&
        //        (((C2DLayer)tn.Tag).ObjTypeID == (int)(Constant.BASE_OBJ_TYPES_ID.GRID)))
        //    {
        //        GridEx grid = ((GridEx)((C2DLayer)tn.Tag)._ObjectsList[0]);
        //        using (PaletteEditor palEdit = new PaletteEditor())
        //        {
        //            palEdit.SetPalette(grid.palette);
        //            if(palEdit.ShowDialog() == DialogResult.OK)
        //            {
        //                grid.UpdatePalette(palEdit.palette);
        //            }
        //            _canv.DrawLayerList();
        //        }
        //    }
        //}

        // Общие методы переименование и удаление и экспорт файла, копирование вставка
        void cmImportGrid_Click(object sender, EventArgs e)
        {
            _canv.LoadGridFromFile();
        }

        // Общие пункты
        //
        void cmCopy_Click(object sender, EventArgs e)
        {
            TreeNode tn = this.SelectedNodes[0];
            if (tn.Tag != null) _canv.CopyWorkLayer((C2DLayer)tn.Tag);
        }
        void cmPaste_Click(object sender, EventArgs e)
        {
            _canv.PasteWorkLayer(false);
        }
        void cmSendMail_Click(object sender, EventArgs e)
        {
            if (SelectedNodes.Count > 0)
            {
                Mapi mapi = new Mapi();
                int count = 0;
                string fileNames = string.Empty;
                for (int i = 0; i < SelectedNodes.Count; i++)
                {
                    TreeNode node = SelectedNodes[i];
                    if ((node.Level > 0) && (node.Tag != null) && ((C2DLayer)node.Tag).ObjTypeID != Constant.BASE_OBJ_TYPES_ID.LAYER)
                    {
                        C2DLayer layer = (C2DLayer)node.Tag;
                        string FileName = string.Format("{0}\\{1}\\{2}{3}", _canv.mainForm.UserSmartPlusFolder, node.Parent.FullPath, node.Text, GetFileExt(layer.ObjTypeID));
                        if (count > 0) fileNames += "; ";
                        fileNames += node.Text + GetFileExt(layer.ObjTypeID);
                        count++;
                        mapi.AddAttachment(FileName);
                    }
                }
                if (count > 0) mapi.SendMailPopup(string.Format("Отправка: {0} файл{1}", count, ((count != 11) && (count % 10 == 1)) ? "а" : "ов"), "Файлы для отправки: \n" + fileNames);
            }
        }
        void RenameWorkLayer()
        {
            if (this.SelectedNodes.Count == 1)
            {
                TreeNode tn = this.SelectedNodes[0];
                int findInd;
                string name = tn.Text;
                string ext = string.Empty;
                if (tn.Tag != null && ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                {
                    name = Path.GetFileNameWithoutExtension(tn.Text);
                    ext = Path.GetExtension(tn.Text);
                }
                bool cycle = true, rename = false;
                TreeNode parentNode = tn.Parent;
                while (cycle)
                {
                    if (fText.ShowReNameLayer(name) == DialogResult.OK)
                    {
                        name = fText.NewText;
                        findInd = FindNodes(parentNode, name + ext);

                        if ((findInd > -1) && (findInd == tn.Index))
                        {
                            cycle = false;
                        }
                        else if (findInd > -1)
                        {
                            MessageBox.Show("В данной папке уже существует слой с именем '" + name + "'. Выберите новое имя!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            cycle = false;
                            rename = true;
                        }
                    }
                    else cycle = false;
                }

                if (rename) RenameWorkNode(tn, name + ext);
            }
        }
        void RenameWorkLayer_Click(object sender, EventArgs e)
        {
            RenameWorkLayer();
        }
        void RenameWorkNode(TreeNode tn, string newName)
        {
            if ((tn.Tag != null) && (tn.TreeView != null) && ((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                C2DLayer layer = (C2DLayer)tn.Tag;
                string fileExt = "";
                string oldName = tn.Text;
                string path = _canv.mainForm.UserSmartPlusFolder + "\\" + tn.Parent.FullPath + "\\";
                if (layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    tn.Text = newName.Replace("\\", "-").Replace("/", "-");
                    layer.Name = newName;
                    fileExt = GetFileExt(layer.ObjTypeID);
                    switch (layer.ObjTypeID)
                    {
                        case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                            _canv.mainForm.WellListUpdateTitle();
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                            ((Profile)layer.ObjectsList[0]).Name = newName;
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.MARKER:
                            ((Marker)layer.ObjectsList[0]).Name = newName;
                            _canv.mainForm.corSchUpdateWindow(true);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.DATAFILE:
                            if (!File.Exists(path + "\\" + newName))
                            {
                                File.Move(path + "\\" + oldName, path + "\\" + newName);
                            }
                            break;
                    }
                    if (File.Exists(path + oldName + fileExt) && (!File.Exists(path + newName + fileExt)))
                    {
                        try
                        {
                            if (_canv.WriteWorkLayer((C2DLayer)tn.Tag))
                            {
                                File.Delete(path + oldName + fileExt);
                            }
                        }
                        catch
                        {
                            tn.Text = oldName;
                            layer.Name = oldName;
                            fileExt = GetFileExt(layer.ObjTypeID);
                            switch (layer.ObjTypeID)
                            {
                                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                                    _canv.mainForm.WellListUpdateTitle();
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.GRID:
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                                    ((Profile)layer.ObjectsList[0]).Name = oldName;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                                    ((Marker)layer.ObjectsList[0]).Name = oldName;
                                    _canv.mainForm.corSchUpdateWindow(true);
                                    break;
                            }
                        }
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    if (Directory.Exists(path + oldName) && (!Directory.Exists(path + newName)))
                    {
                        try
                        {
                            Directory.Move(path + oldName, path + newName);
                            tn.Text = newName;
                            layer.Name = newName;
                        }
                        catch
                        {
                            MessageBox.Show("Ошибка переименования папки!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }
        void DeleteWorkLayer(object sender, EventArgs e)
        {
            DeleteSelectedWorkLayer();
        }
        public void DeleteSelectedWorkLayer()
        {
            if ((this.SelectedNodes.Count > 0) && (this.SelectedNodes[0].Level > 0))
            {
                string quest = "";
                if (this.SelectedNodes.Count == 1)
                {
                    quest = "Удалить объект '" + this.SelectedNodes[0].Text + "'?";
                }
                else
                {
                    quest = String.Format("Удалить выбранные объекты ({0} шт.)?", SelectedNodes.Count);
                }
                TreeNode tn = null;
                if (MessageBox.Show(quest, "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    ArrayList list = new ArrayList();
                    for (int i = 0; i < this.SelectedNodes.Count; i++)
                    {
                        tn = this.SelectedNodes[i];
                        if ((tn.Tag != null) && (tn.Text == ((C2DLayer)tn.Tag).Name))
                        {
                            if (tn == selectedWellListNode)
                            {
                                _canv.mainForm.UpdateWellListSettings(null);
                                selectedWellListNode = null;
                            }
                            if (tn == selectedProfileNode)
                            {
                                _canv.mainForm.corSchClearAll();
                                selectedProfileNode = null;
                            }
                            if (tn == selectedMarkerNode)
                            {
                                _canv.mainForm.corSchSetCurrentMarker(null);
                                selectedMarkerNode = null;
                            }
                            if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                            {
                                ((Marker)((C2DLayer)tn.Tag).ObjectsList[0]).ClearWellList();
                                list.Add((Marker)((C2DLayer)tn.Tag).ObjectsList[0]);
                            }
                            _canv.RemoveWorkLayer(tn);
                        }
                    }
                    if (list.Count > 0)
                    {
                        _canv.mainForm.corSchRemoveMarkers(list);
                    }
                    _canv.DrawLayerList();
                }
            }
        }

        // VORONOI MAP
        //
        void cmVoronoiShowReserves_Click(object sender, EventArgs e)
        {
            if ((SelectedNodes.Count == 1) && (SelectedNodes[0].Tag != null))
            {
                C2DLayer layer = (C2DLayer)SelectedNodes[0].Tag;
                if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                {
                    _canv.ShowVoronoiCellsParams((VoronoiMap)layer.ObjectsList[0], true);
                }
            }
        }
        void cmVoronoiExportContours_Click(object sender, EventArgs e)
        {
            if ((SelectedNodes.Count == 1) && (SelectedNodes[0].Tag != null))
            {
                C2DLayer layer = (C2DLayer)SelectedNodes[0].Tag;
                if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                {
                    var vm = (VoronoiMap)layer.ObjectsList[0];
                    C2DLayer lr = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, -1);
                    for (int i = 0; i < vm.Cells.Length; i++)
                    {
                        lr.Add(vm.Cells[i].Cntr);
                        lr.GetObjectsRect();
                        lr.ResetFontsPens();
                    }
                    ContourExportForm.ShowForm(_canv.mainForm, _canv.MapProject, _canv.twActiveOilField.ActiveOilFieldIndex, lr);
                }
            }
        }

        // Настройки
        //
        void cmSettings_Click(object sender, EventArgs e)
        {
            if ((this.SelectedNodes.Count == 1) && (this.SelectedNodes[0].Tag != null))
            {
                if ((((BaseObj)this.SelectedNodes[0].Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    _canv.WorkObjectSettingsShow((C2DLayer)this.SelectedNodes[0].Tag);
                }
            }
        }

        // Установка контексного меню по типу объекта
        //
        public void SetContextMenu()
        {
            cmImportContours.Visible = false;
            cmImportGrid.Visible = false;
            cmImageDropCoef.Visible = false;
            cmImageEditMode.Visible = false;
            cmCopy.Visible = false;
            cmPaste.Visible = false;
            //cmContourResetIndex.Visible = false;
            cmWFCreate.Visible = false;
            //cmWFSetColor.Visible = false;
            cmContourOperations.Visible = false;
            cmCntrOpAverageStep.Visible = false;
            cmSettingsSep.Visible = false;
            cmSettings.Visible = false;
            cmVoronoiShowReserves.Visible = false;
            cmVoronoiExportContours.Visible = false;
            cmRename.Enabled = true;
            cmDelete.Enabled = true;
            cmSendMail.Enabled = true;
            cmRename.Visible = true;
            cmDelete.Visible = true;
            cmSendMail.Visible = true;
            cmSeparator.Visible = true;

            if (SelectedNodes.Count == 0)
            {
                cmRename.Visible = false;
                cmDelete.Visible = false;
                cmSendMail.Visible = false;
                cmSeparator.Visible = false;
            }
            else if ((SelectedNodes.Count == 1) && (SelectedNodes[0].Level == 0))
            {
                cmImportGrid.Visible = true;
                cmWFCreate.Visible = true;
                cmRename.Enabled = false;
                cmDelete.Enabled = false;
                cmSendMail.Enabled = false;
                cmImportContours.Visible = true;
            }
            else if ((SelectedNodes.Count == 1) && (SelectedNodes[0].Tag != null))
            {
                C2DLayer layer = (C2DLayer)SelectedNodes[0].Tag;
                switch (layer.ObjTypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.MARKER:
                        cmSettings.Visible = true;
                        cmSettingsSep.Visible = true;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.LAYER:
                        cmWFCreate.Visible = true;
                        cmSendMail.Enabled = false;
                        //cmWFSetColor.Visible = true;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                        cmCopy.Visible = true;
                        cmPaste.Visible = true;
                        //cmContourResetIndex.Visible = true;
                        cmCntrOpAverageStep.Visible = true;
                        cmSettings.Visible = true;
                        cmSettingsSep.Visible = true;

                        cmContourOperations.Visible = true;
                        cmCntrOpDiff.Visible = true;
                        cmCntrOpSymDiff.Visible = true;
                        cmCntrOpUnion.Visible = false;
                        cmCntrOpCross.Visible = false;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                        cmSettings.Visible = true;
                        cmSettingsSep.Visible = true;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.GRID:
                        cmSettings.Visible = true;
                        cmSettingsSep.Visible = true;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                        //cmImageDropCoef.Visible = true;
                        cmImageEditMode.Visible = true;
                        cmSettings.Visible = true;
                        cmSettingsSep.Visible = true;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                        cmVoronoiShowReserves.Visible = true;
                        cmVoronoiExportContours.Visible = false;
                        break;
                }
            }
            else if (SelectedNodes.Count > 0)
            {
                Constant.BASE_OBJ_TYPES_ID allType = Constant.BASE_OBJ_TYPES_ID.NONE;
                for (int i = 0; i < SelectedNodes.Count; i++)
                {
                    if (SelectedNodes[i].Tag != null)
                    {
                        if (allType == Constant.BASE_OBJ_TYPES_ID.NONE) allType = ((C2DLayer)SelectedNodes[i].Tag).ObjTypeID;
                        if (allType != ((C2DLayer)SelectedNodes[i].Tag).ObjTypeID)
                        {
                            allType = Constant.BASE_OBJ_TYPES_ID.NONE;
                            break;
                        }
                    }
                }
                switch (allType)
                {
                    case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                        cmContourOperations.Visible = true;
                        cmCntrOpDiff.Visible = true;
                        cmCntrOpSymDiff.Visible = true;
                        cmCntrOpUnion.Visible = true;
                        cmCntrOpCross.Visible = true;
                        break;
                }
                cmRename.Enabled = false;
            }
        }
        #endregion ContextMenuStrip

        public void CheckState(TreeNode SourceNode, TreeNode TargetNode, bool SetCheckState)
        {
            if ((SourceNode != null) && (TargetNode != null) && ((SetCheckState) || (SourceNode.Nodes.Count == TargetNode.Nodes.Count)))
            {
                for (int i = 0; i < TargetNode.Nodes.Count; i++)
                {
                    if (TargetNode.Nodes[i].Nodes.Count > 0)
                    {
                        CheckState(SourceNode.Nodes[i], TargetNode.Nodes[i], SetCheckState);
                    }
                    else
                    {
                        if (TargetNode.Nodes[i].Tag != null)
                        {
                            ((C2DLayer)TargetNode.Nodes[i].Tag).node = TargetNode.Nodes[i];
                        }
                        if (SetCheckState)
                            TargetNode.Nodes[i].Checked = true;
                        else
                            TargetNode.Nodes[i].Checked = SourceNode.Nodes[i].Checked;
                    }
                }
            }
            if (SetCheckState)
                TargetNode.Checked = true;
            else
                TargetNode.Checked = SourceNode.Checked;
        }
        public void SetTags(TreeNode node, C2DLayer layer)
        {
            if ((node != null) && (layer != null))
            {
                C2DLayer lr;
                if ((node.Tag != null)
                    && (node.Nodes.Count == 1)
                    && (node.Nodes[0].Text == "Empty")
                    && (((BaseObj)node.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    TreeNode tn;
                    node.Nodes.Clear();
                    for (int i = 0; i < layer.ObjectsList.Count; i++)
                    {
                        tn = node.Nodes.Add(((C2DObject)layer.ObjectsList[i]).Name);
                        tn.Tag = (C2DObject)layer.ObjectsList[i];
                    }
                }

                for (int i = 0; i < node.Nodes.Count; i++)
                {
                    if (((BaseObj)layer.ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                    {
                        if (((C2DLayer)layer.ObjectsList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                        {
                            SetTags(node.Nodes[i], (C2DLayer)layer.ObjectsList[i]);
                        }
                        else
                        {
                            lr = (C2DLayer)layer.ObjectsList[i];
                            node.Nodes[i].Tag = lr;
                            lr.Visible = node.Nodes[i].Checked;
                            lr.node = node.Nodes[i];
                            lr.SetContoursType(0);
                            node.Nodes[i].ContextMenuStrip = this.cMenu;
                            node.Nodes[i].ImageIndex = 0;
                            node.Nodes[i].SelectedImageIndex = 0;
                        }
                    }
                }
            }
            node.Tag = layer;
            layer.node = node;
            layer.Visible = node.Checked;
            if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                node.ContextMenuStrip = this.cMenu;
                node.ImageIndex = 2;
                node.SelectedImageIndex = 2;
            }
            else
            {
                layer.SetContoursType(0);
                node.ContextMenuStrip = this.cMenu;
                node.ImageIndex = 0;
                node.SelectedImageIndex = 0;
            }
        }
        public void SetLayerColor()
        {
            TreeNode tn = this.SelectedNodes[0];
            if ((tn.Tag != null) && (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                ColorDialog dlg = new ColorDialog();
                dlg.AllowFullOpen = true;
                dlg.AnyColor = true;
                dlg.SolidColorOnly = true;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    ((C2DLayer)tn.Tag).SetObjColor(dlg.Color);
                    //_canv.mainForm.WriteLog("Установка цвета узла " + tn.FullPath + dlg.Color.ToString());
                    _canv.DrawLayerList();
                    _canv.LayerWorkListChanged = true;
                }
            }
        }

        // Select By Type
        public void SelectNodeByType(Constant.BASE_OBJ_TYPES_ID Type, TreeNode tn)
        {
            TreeNode oldNode = null;
            switch (Type)
            {
                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                    oldNode = this.selectedProfileNode;
                    this.selectedProfileNode = tn;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                    oldNode = this.selectedMarkerNode;
                    this.selectedMarkerNode = tn;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL_LIST:
                    oldNode = this.selectedWellListNode;
                    this.selectedWellListNode = tn;
                    break;
            }
            if (!m_SelectedNodes.Contains(oldNode))
            {
                SetNodeFontBold(oldNode, false);
            }
            if (!m_SelectedNodes.Contains(tn))
            {
                SetNodeFontBold(tn, true);
            }
        }
        public void SetNodeFontBold(TreeNode tn, bool IsFontBold)
        {
            if (tn != null)
            {
                if (IsFontBold)
                {
                    tn.ForeColor = this.ForeColor;
                    tn.NodeFont = this.FontBold;
                    tn.BackColor = Color.SkyBlue;
                }
                else
                {
                    tn.NodeFont = this.Font;
                    tn.BackColor = this.BackColor;
                    tn.ForeColor = this.ForeColor;
                }
            }
        }
        private int FindNodes(TreeNode node, string NodeText)
        {
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                if (node.Nodes[i].Text == NodeText)
                {
                    return i;
                }
            }
            return -1;
        }
        public void Clear()
        {
            loader.CancelAsync();
            this.Nodes.Clear();
        }

        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == 515)
            { /* WM_LBUTTONDBLCLK */
                if ((!cancelDouble) && (this.SelectedNodes.Count == 1) && (this.SelectedNodes[0].Tag != null))
                {
                    if ((((BaseObj)this.SelectedNodes[0].Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    {
                        switch (((C2DLayer)this.SelectedNodes[0].Tag).ObjTypeID)
                        {
                            case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                               // _canv.// mainForm.StatUsage.AddMessage(CollectorStatId.WF_SET_WELLLIST);
                                _canv.mainForm.ShowWellListPane();
                                _canv.mainForm.UpdateWellListSettings((C2DLayer)this.SelectedNodes[0].Tag);
                                _canv.SetSelectedObj((C2DLayer)this.SelectedNodes[0].Tag);
                                //this.Focus();
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                                //_canv.mainForm.corSchShowPane();
                               // _canv.// mainForm.StatUsage.AddMessage(CollectorStatId.WF_SET_PROFILE);
                                _canv.mainForm.corSchLoadProfile((C2DLayer)this.SelectedNodes[0].Tag);
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.MARKER:
                                _canv.mainForm.corSchSetMarkerEditMode(true);
                                _canv.mainForm.corSchSetCurrentMarker((C2DLayer)this.SelectedNodes[0].Tag);
                                _canv.mainForm.corSchUpdateWindow(true);
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.PVT:
                                PVTParamsEditForm form = new PVTParamsEditForm(_canv.mainForm);
                                PVTWorkObject pvt = (PVTWorkObject)((C2DLayer)this.SelectedNodes[0].Tag).ObjectsList[0];
                                if (form.ShowDialog(pvt.Item, string.Empty) == DialogResult.OK)
                                {
                                    pvt.Item = form.PvtItem;
                                    _canv.WriteWorkLayer((C2DLayer)this.SelectedNodes[0].Tag);
                                }
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.DATAFILE:
                                DataFile file = (DataFile)((C2DLayer)this.SelectedNodes[0].Tag).ObjectsList[0];
                                file.OpenFile();
                                break;
                        }
                    }
                }
            }
            else
                base.DefWndProc(ref m);
        }
        void TreeViewWorkLayers_MouseDown(object sender, MouseEventArgs e)
        {
            TreeView tw = (TreeView)sender;
            TreeNode tn;
            Point pointClick = e.Location;
            TreeNode clickedNode = tw.GetNodeAt(pointClick);
           // _canv.// mainForm.StatUsage.AddMessage(CollectorStatId.WORKFOLDER_MOUSE_DOWN);
            if (clickedNode != null)
            {
                if ((pointClick.X >= clickedNode.Bounds.X - 32) && (pointClick.X < clickedNode.Bounds.X - 16))
                {
                    cancelDouble = true;
                    bool check = !clickedNode.Checked;
                    _canv.LayerWorkListChanged = true;
                    if (clickedNode.Level == 0)
                    {
                        for (int i = 0; i < clickedNode.Nodes.Count; i++)
                        {
                            tn = clickedNode.Nodes[i];
                            tn.Checked = check;
                            if (tn.Tag != null) ((BaseObj)tn.Tag).Visible = check;
                        }
                    }
                    else
                    {
                        if (clickedNode.Tag != null)
                        {
                            if (((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                            {
                                if (!check) ((C2DLayer)clickedNode.Tag).Selected = false;
                                if (check != ((C2DLayer)clickedNode.Tag).Visible)
                                {
                                    ((C2DLayer)clickedNode.Tag).Visible = check;
                                    if (((C2DLayer)clickedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                                    {
                                        _canv.mainForm.corSchUpdateWindow(true);
                                    }
                                    else if (((C2DLayer)clickedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                                    {
                                        if (check)
                                        {
                                            StartLoadData(clickedNode);
                                            check = false;
                                        }
                                        else
                                        {
                                            _canv.ClearWorkGridData((C2DLayer)clickedNode.Tag, true);
                                        }
                                    }
                                }
                            }
                            ((C2DObject)clickedNode.Tag).Visible = check;

                            if (((Control.ModifierKeys & Keys.Shift) == Keys.Shift) &&
                                (clickedNode.Nodes.Count > 0) &&
                                (((BaseObj)clickedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                            {
                                for (int i = 0; i < clickedNode.Nodes.Count; i++)
                                {
                                    ((C2DObject)clickedNode.Nodes[i].Tag).Visible = check;
                                    clickedNode.Nodes[i].Checked = check;
                                }
                            }
                        }
                    }
                    _canv.DrawLayerList();
                    clickedNode.Checked = check;
                }
                else if (pointClick.X < clickedNode.Bounds.Right)
                {
                    cancelDouble = false;
                }
                if (pointClick.X < clickedNode.Bounds.Right)
                {
                    if (_canv.copyLayer == null)
                        cmPaste.Enabled = false;
                    else
                        cmPaste.Enabled = true;
                    if (e.Button == MouseButtons.Right)
                    {
                        if (!SelectedNodes.Contains(clickedNode))
                        {
                            SetOneSelectedNode(clickedNode);
                        }
                        else if (SelectedNodes.Count > 0)
                        {

                        }
                        SetContextMenu();
                    }
                }
                else if ((Control.ModifierKeys != Keys.Control) && (Control.ModifierKeys != Keys.Shift))
                {
                    ClearSelectedList();
                    SetContextMenu();
                }
            }
            else if ((Control.ModifierKeys != Keys.Control) && (Control.ModifierKeys != Keys.Shift))
            {
                ClearSelectedList();
                SetContextMenu();
            }
        }
        void TreeViewWorkLayers_MouseEnter(object sender, EventArgs e)
        {
            localDrag = false;
        }
        void TreeViewWorkLayers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteSelectedWorkLayer();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                _canv.SelectWellsByPhantom(false);
            }
            else if ((e.KeyCode == Keys.F2) && (SelectedNodes.Count == 1))
            {
                RenameWorkLayer();
            }
        }

        // DRAG DROP
        void TreeViewWorkLayers_ItemDrag(object sender, ItemDragEventArgs e)
        {
            TreeNode tn = (TreeNode)e.Item;
            if (!SelectedNodes.Contains(tn)) SetOneSelectedNode(tn);
            if (SelectedNodes.Contains(tn))
            {
                if ((tn != null) && (tn.Tag != null) &&
                    ((C2DObject)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER &&
                    (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE ||
                    ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    localDrag = true;
                    if (SelectedNodes.Contains(Nodes[0])) removeSelectedNode(Nodes[0]);
                    DoDragDrop(tn, DragDropEffects.Move);
                }
            }
        }
        void TreeViewWorkLayers_DragEnter(object sender, DragEventArgs e)
        {
            TreeNode dragNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

            if ((dragNode != null) && (dragNode.Tag != null) &&
                (((BaseObj)dragNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE ||
                ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                e.Effect = DragDropEffects.All;
            }
            else if (e.Data.GetDataPresent(DataFormats.FileDrop, false) || e.Data.GetDataPresent("FileGroupDescriptor", false))
            {
                e.Effect = DragDropEffects.All;
            }
            else if (e.Data.GetDataPresent("FileServerDragObject"))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        void TreeViewWorkLayers_DragOver(object sender, DragEventArgs e)
        {
            if (localDrag)
            {
                TreeNode targetNode = this.GetNodeAt(this.PointToClient(Cursor.Position));
                TreeNode dragNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

                if ((dragNode != null) && (dragNode.Tag != null) &&
                    (!SelectedNodes.Contains(targetNode)) &&
                    (((BaseObj)dragNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                    (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    e.Effect = DragDropEffects.All;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
            else if (e.Data.GetDataPresent("FileServerDragObject"))
            {
                e.Effect = DragDropEffects.All;
            }
            else if (e.Data.GetDataPresent(DataFormats.FileDrop, false) || e.Data.GetDataPresent("FileGroupDescriptor", false))
            {
                e.Effect = DragDropEffects.All;
            }
        }
        void TreeViewWorkLayers_DragDrop(object sender, DragEventArgs e)
        {
            if (_canv.MapProject == null) return;
            
                TreeNode dragNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

                if (!localDrag)
                {
                    #region Inspector Drag
                    if ((dragNode != null) && (dragNode.Tag != null) &&
                        (((BaseObj)dragNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                        (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE ||
                    ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE ||
                        ((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    {
                        bool layerExist = false;
                        if (!Directory.Exists(_canv.mainForm.UserWorkDataFolder))
                        {
                            Directory.CreateDirectory(_canv.mainForm.UserWorkDataFolder);
                        }

                        if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                        {
                            layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name + ".cntr");
                        }
                        else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                        {
                            layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name + ".wl");
                        }
                        else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                        {
                            layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name + ".grid");
                        }
                        else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE)
                        {
                            layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name + ".prfl");
                        }
                        else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                        {
                            layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name + ".mrk");
                        }
                        else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT)
                        {
                            layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name + ".pvt");
                        }
                        else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                        {
                            layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name + ".vm");
                        }
                        else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                        {
                            layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name);
                        }
                    else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
                    {
                        layerExist = File.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name + ".img");
                    }
                        else if (((C2DLayer)dragNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                        {
                            layerExist = Directory.Exists(_canv.mainForm.UserSmartPlusFolder + "\\Рабочая папка\\" + ((C2DLayer)dragNode.Tag).Name);
                        }

                        if (layerExist)
                        {
                            if (MessageBox.Show("В Рабочей папке уже существует слой с названием '" + ((C2DLayer)dragNode.Tag).Name + "'. Заменить его?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            {
                                return;
                            }
                            else
                            {
                                int indDel = FindNodes(this.Nodes[0], ((C2DLayer)dragNode.Tag).Name);
                                if (indDel > -1) _canv.RemoveWorkLayer(this.Nodes[0].Nodes[indDel]);
                            }
                        }

                        _canv.CopyWorkNode(dragNode);
                        _canv.copyNode = dragNode;
                        if (_canv.copyLayer != null)
                        {
                            _canv.copyLayer.ReplaceObjType(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, Constant.BASE_OBJ_TYPES_ID.CONTOUR);
                            _canv.copyLayer.ReplaceObjType(Constant.BASE_OBJ_TYPES_ID.AREA, Constant.BASE_OBJ_TYPES_ID.CONTOUR);
                        }
                        TreeNode tnPaste = _canv.PasteWorkNode(true);

                        //CheckState(dragNode, tnPaste, true);
                        //SetTags(tnPaste, (C2DLayer)tnPaste.Tag);

                        //_canv.mainForm.WriteLog("Перенос из инспектора узла " + dragNode.FullPath);
                        _canv.DrawLayerList();
                        if (_canv.copyLayer != null) _canv.LayerWorkListChanged = true;
                    }
                    else if (e.Data.GetDataPresent(DataFormats.FileDrop, false) || e.Data.GetDataPresent("FileGroupDescriptor", false))
                    {
                        string[] files = null;

                    List<string> extentions = new List<string>(new string[] { ".img", ".cntr", ".prfl", ".pvt", ".mrk", ".wl", ".grid", ".vm" });
                        List<string> extentionsOther = new List<string>(new string[] { ".doc", ".docx", ".docm", ".pdf", ".ppt", ".pptx", ".pptm", ".xls", ".xlsx", ".xlsb", ".xlsm" });
                        if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
                        {
                            files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                            int k = 0;
                            string name, ext;
                            for (int i = 0; i < files.Length; i++)
                            {
                                name = Path.GetFileName(files[i]);
                                ext = Path.GetExtension(files[i]);
                                if (extentions.IndexOf(ext) > -1 || extentionsOther.IndexOf(ext) > -1)
                                {
                                    k = 0;
                                    while (File.Exists(_canv.mainForm.UserWorkDataFolder + "\\" + name))
                                    {
                                        k++;
                                        name = string.Format("{0} ({1}){2}", Path.GetFileNameWithoutExtension(name), k, Path.GetExtension(files[i]));
                                    }
                                    if (File.Exists(files[i])) File.Copy(files[i], _canv.mainForm.UserWorkDataFolder + "\\" + name);
                                }
                                files[i] = name;
                            }
                        }
                        else
                        {
                            try
                            {
                                Stream stream = (Stream)e.Data.GetData("FileGroupDescriptor");
                                char[] buff;
                                int i, j, k, count;
                                BinaryReader br = new BinaryReader(stream, Encoding.GetEncoding(1251));
                                string filename = string.Empty;

                                byte[] b = new byte[stream.Length];
                                stream.Read(b, 0, (int)stream.Length);

                                stream.Seek(0, SeekOrigin.Begin);
                                count = br.ReadByte();
                                k = 0;
                                files = new string[count];

                                stream.Seek(76, SeekOrigin.Begin);
                                for (i = 76; (i < stream.Length) && (k < count); i++)
                                {
                                    if (br.ReadByte() != 0)
                                    {
                                        for (j = i + 1; j < stream.Length; j++)
                                        {
                                            if (br.ReadByte() == 0) break;
                                        }
                                        if ((j < stream.Length) && (k < count))
                                        {
                                            br.BaseStream.Seek(i, SeekOrigin.Begin);
                                            buff = br.ReadChars(j - i);
                                            files[k] = new string(buff);
                                            k++;
                                        }
                                        i += j - i - 1;
                                    }
                                }
                                stream.Close();
                                MemoryStream[] fileContents = (MemoryStream[])ClipboardHelper.GetOutlookFileContents(e.Data, count);
                                k = 0;
                                string name;
                                for (i = 0; (i < count) && (i < fileContents.Length); i++)
                                {
                                    if (extentions.IndexOf(Path.GetExtension(files[i])) != -1)
                                    {
                                        k = 0;
                                        name = files[i];
                                        while (File.Exists(_canv.mainForm.UserWorkDataFolder + "\\" + name))
                                        {
                                            k++;
                                            name = string.Format("{0} ({1}){2}", Path.GetFileNameWithoutExtension(files[i]), k, Path.GetExtension(files[i]));
                                        }

                                        files[i] = name;
                                        FileStream fs = new FileStream(_canv.mainForm.UserWorkDataFolder + "\\" + files[i], FileMode.Create);
                                        fileContents[i].WriteTo(fs);
                                        fs.Close();
                                        fileContents[i].Dispose();
                                    }
                                }
                            }
                            catch
                            {
                                files = null;
                            }
                        }
                        if (files != null)
                        {
                            _canv.LoadDragFiles(files);
                            _canv.DrawLayerList();
                        }
                    }
                    else if (e.Data.GetDataPresent("FileServerDragObject", false))
                    {
                        var dragObject = (Network.FileServer.DragObject)e.Data.GetData("FileServerDragObject");
                        if (dragObject != null && dragObject.client.IsInitialized)
                        {
                            dragObject.client.DownloadFile(dragObject.file.Id);
                        }
                    }
                    #endregion
                }
                else // LOCAL DRAG
                {
                Point pt = PointToClient(new Point(e.X, e.Y));
                TreeNode targetNode = this.GetNodeAt(pt);
                    TreeNode newNode = null;
                    ArrayList selNodes = new ArrayList(SelectedNodes.Count);
                    for (int i = 0; i < SelectedNodes.Count; i++)
                    {
                        if (!IsParentInSelectedNodes(SelectedNodes[i]))
                        {
                            selNodes.Add(SelectedNodes[i]);
                        }
                    }
                    ClearSelectedList();
                    if ((targetNode != null) && ((dragNode == targetNode) || (dragNode == targetNode.Parent))) return;
                    while (selNodes.Count > 0)
                    {
                        dragNode = (TreeNode)selNodes[0];
                        newNode = null;
                        if (!IsParentInSelectedNodes(dragNode))
                        {
                            if ((targetNode != null) && (targetNode.Tag != null) &&         // DRAG в конец подпапки
                                    (((BaseObj)targetNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                    (((C2DLayer)targetNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                            {
                                if ((dragNode != null) && (dragNode.Tag != null) &&
                                    (((BaseObj)dragNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                                {
                                    newNode = MoveWorkLayer(targetNode, dragNode, -1);
                                }
                            }
                            else if ((targetNode != null) && (targetNode.Tag != null) &&         // DRAG в внутрь подпапки
                                    (((BaseObj)targetNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                    (((C2DLayer)targetNode.Tag).ObjTypeID != Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                    (targetNode.Parent != null) &&
                                    ((targetNode.Parent.Level == 0) ||
                                     ((targetNode.Parent.Tag != null) &&
                                     (((BaseObj)targetNode.Parent.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                     (((C2DLayer)targetNode.Parent.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))))
                            {
                                if ((dragNode != null) && (dragNode.Tag != null) &&
                                    (((BaseObj)dragNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                                {
                                    newNode = MoveWorkLayer(targetNode.Parent, dragNode, targetNode.Index);
                                }
                            }
                            else if ((dragNode != null) && (dragNode.Tag != null) &&    // DRAG в корень раб.папки 
                                    (((BaseObj)dragNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                            {
                                newNode = MoveWorkLayer(this.Nodes[0], dragNode, -1);
                            }
                        }
                        if (newNode != null) addSelectedNode(newNode);
                        selNodes.RemoveAt(0);
                    }
                    _canv.DrawLayerList();
                    _canv.LayerWorkListChanged = true;
                }
                localDrag = false;
            }
        bool IsParentInSelectedNodes(TreeNode tn)
        {
            bool res = false;
            while (tn.Parent != null)
            {
                if (SelectedNodes.Contains(tn.Parent))
                {
                    res = true;
                    break;
                }
                tn = tn.Parent;
            }
            return res;
        }
        string GetFileExt(Constant.BASE_OBJ_TYPES_ID Type)
        {
            switch (Type)
            {
                case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                    return ".img";
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    return ".cntr";
                case Constant.BASE_OBJ_TYPES_ID.GRID:
                    return ".grid";
                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                    return ".prfl";
                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                    return ".mrk";
                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                    return ".wl";
                case Constant.BASE_OBJ_TYPES_ID.PVT:
                    return ".pvt";
                case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                    return ".vm";
            }
            return string.Empty;
        }
        
        // LOADER DATA
        void StartLoadData(TreeNode node)
        {
            if (!loaderTimer.Enabled) loaderTimer.Start();
            if (lastLoadedNodes.IndexOf(node) == -1)
            {
                lastLoadedNodes.Add(node);
                if(!loader.IsBusy) loader.RunWorkerAsync();
            }
        }
        void SetDefaultImage(TreeNode node)
        {
            if (node.Tag != null)
            {
                int index = _canv.GetImageIndex(((C2DLayer)node.Tag).ObjTypeID);
                this.BeginUpdate();
                node.ImageIndex = index;
                node.SelectedImageIndex = index;
                node.Checked = ((C2DLayer)node.Tag).Visible;
                this.EndUpdate();
            }
        }
        void loader_DoWork(object sender, DoWorkEventArgs e)
        {
            if (lastLoadedNodes.Count > 0)
            {
                TreeNode tn = (TreeNode)lastLoadedNodes[0];
                if (tn.Tag != null)
                {
                    C2DLayer layer = (C2DLayer)tn.Tag;
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                    {
                        _canv.LoadWorkGrid(loader, e, layer);
                    }
                }
            }
        }
        void loader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                lastLoadedNodes.Clear();
                loaderTimer.Stop();
                return;
            }
            if (lastLoadedNodes.Count > 0)
            {
                if (lastLoadedNodes[0].Tag != null &&
                    ((C2DLayer)lastLoadedNodes[0].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID && 
                    ((C2DLayer)lastLoadedNodes[0].Tag).ObjectsList.Count > 0 &&
                    ((Grid)((C2DLayer)lastLoadedNodes[0].Tag).ObjectsList[0]).DataLoaded)
                {
                    lastLoadedNodes[0].Checked = true;
                }
                SetDefaultImage(lastLoadedNodes[0]);
                lastLoadedNodes.RemoveAt(0);
                _canv.DrawLayerList();
            }

            if (lastLoadedNodes.Count > 0) loader.RunWorkerAsync();
        }
        void loaderUpdater_Tick(object sender, EventArgs e)
        {
            int index;
            if (loader.IsBusy || lastLoadedNodes.Count > 0)
            {
                for (int i = 0; i < lastLoadedNodes.Count; i++)
                {
                    index = lastLoadedNodes[i].ImageIndex + 1;
                    if (index < 25) index = 25;
                    if (index > 36) index = 25;
                    lastLoadedNodes[i].ImageIndex = index;
                    lastLoadedNodes[i].SelectedImageIndex = index;
                }
            }
            else if (lastLoadedNodes.Count == 0)
            {
                loaderTimer.Stop();
            }
        }

        TreeNode MoveWorkLayer(TreeNode targetNode, TreeNode dragNode, int InsertIndex)
        {
            int i;
            C2DLayer layer = (C2DLayer)dragNode.Tag;
            C2DLayer layerGroup = layerGroup = (C2DLayer)targetNode.Tag;
            bool IsOneParent = (targetNode == dragNode.Parent);
            bool IsLayerList = (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER);
            bool IsTargetWorkFolder = (targetNode.Level == 0);

            string targPath = _canv.mainForm.UserSmartPlusFolder + "\\" + targetNode.FullPath;

            string fileEnd = GetFileExt(layer.ObjTypeID);
            string dragPath = _canv.mainForm.UserSmartPlusFolder + "\\" + dragNode.FullPath + fileEnd;
            string targFileName = targPath + "\\" + dragNode.Text + fileEnd;

            if (!IsOneParent && File.Exists(targFileName))
            {
                string quest = "";
                if (IsLayerList) quest = "В данной папке уже существует папка с названием '" + dragNode.Text + "'. Заменить?";
                else quest = "В папке '" + targetNode.Text + "' уже существует слой с названием '" + dragNode.Text + "'. Заменить его?";
                if (MessageBox.Show(quest, "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return null;
                }
                else
                {
                    int indDel = FindNodes(targetNode, dragNode.Text);
                    if (indDel > -1) _canv.RemoveWorkLayer(targetNode.Nodes[indDel]);
                }
            }

            TreeNode tn = (TreeNode)dragNode.Clone();
            if (dragNode == selectedMarkerNode) selectedMarkerNode = tn;
            if (dragNode == selectedProfileNode) selectedProfileNode = tn;
            if (dragNode == selectedWellListNode) selectedWellListNode = tn;

            if (InsertIndex == -1)
            {
                targetNode.Nodes.Add(tn);
            }
            else
            {
                targetNode.Nodes.Insert(InsertIndex, tn);
            }

            if (IsLayerList) CheckState(dragNode, tn, false);

            if (dragNode.Level == 1)
            {
                _canv.LayerWorkList.RemoveAt(layer.Index);
                for (i = 0; i < _canv.LayerWorkList.Count; i++) ((C2DLayer)_canv.LayerWorkList[i]).Index = i;
            }
            else if (dragNode.Parent.Tag != null)
            {
                C2DLayer parentLayer = (C2DLayer)dragNode.Parent.Tag;
                _canv.AddSelectLayer((C2DLayer)dragNode.Tag);
                parentLayer.ObjectsList.RemoveAt(((C2DLayer)dragNode.Tag).Index);
                for (i = 0; i < parentLayer.ObjectsList.Count; i++) ((C2DLayer)parentLayer.ObjectsList[i]).Index = i;
            }

            if (tn != null)
            {
                tn.Checked = dragNode.Checked;
                layer.node = tn;
            }
            if (!IsTargetWorkFolder)
            {
                if (InsertIndex == -1)
                {
                    layer.Index = layerGroup.ObjectsList.Count;
                    layerGroup.Add(layer);
                    if ((tn.Parent != null) && (!tn.Parent.IsExpanded)) tn.Parent.Expand();
                }
                else
                {
                    layer.Index = InsertIndex;
                    layerGroup.Insert(InsertIndex, layer);
                    for (i = 0; i < layerGroup.ObjectsList.Count; i++)
                        ((C2DLayer)layerGroup.ObjectsList[i]).Index = i;
                }
                layerGroup.GetObjectsRect();
            }
            else
            {
                if (InsertIndex == -1)
                {
                    layer.Index = _canv.LayerWorkList.Count;
                    _canv.LayerWorkList.Add(layer);
                }
                else
                {
                    layer.Index = InsertIndex;
                    _canv.LayerWorkList.Insert(InsertIndex, layer);
                    for (i = 0; i < _canv.LayerWorkList.Count; i++)
                        ((C2DLayer)_canv.LayerWorkList[i]).Index = i;
                }
            }
            if (IsLayerList)
            {
                if ((Directory.Exists(targPath) && Directory.Exists(dragPath)) && (!IsOneParent))
                {
                    Directory.Move(dragPath, targPath + "\\" + dragNode.Text);
                }
            }
            else
            {
                if (Directory.Exists(targPath) && File.Exists(dragPath) && (!IsOneParent))
                {
                    File.Move(dragPath, targFileName);
                }
            }

            if (SelectedNodes.Contains(dragNode)) removeSelectedNode(dragNode);
            dragNode.Parent.Nodes.RemoveAt(dragNode.Index);
            _canv.ResetWorkLayerIndexes();
            return tn;
        }
    }
}
