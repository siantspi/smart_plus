﻿using System;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing;


namespace SmartPlus
{
    class ComboBoxLineType : System.Windows.Forms.ComboBox
    {
        Pen[] pens;
        string[] tips;
        Brush grayBrush;
        ToolTip itemTip;
        public DashStyle SelectedDashStyle
        {
            get
            {
                switch (this.SelectedIndex)
                {
                    case 0:
                        return DashStyle.Solid;
                    case 1:
                        return DashStyle.Dot;
                    case 2:
                        return DashStyle.Dash;
                    case 3:
                        return DashStyle.DashDot;
                    case 4:
                        return DashStyle.DashDotDot;
                    default:
                        this.SelectedIndex = 0;
                        return DashStyle.Solid;
                }
            }
            set
            {
                switch (value)
                {
                    case DashStyle.Solid:
                        this.SelectedIndex = 0;
                        break;
                    case DashStyle.Dot:
                        this.SelectedIndex = 1;
                        break;
                    case DashStyle.Dash:
                        this.SelectedIndex = 2;
                        break;
                    case DashStyle.DashDot:
                        this.SelectedIndex = 3;
                        break;
                    case DashStyle.DashDotDot:
                        this.SelectedIndex = 4;
                        break;
                    default:
                        this.SelectedIndex = 0;
                        break;
                }
            }
        }
        public ComboBoxLineType()
        {
            DrawMode = DrawMode.OwnerDrawFixed;
            pens = new Pen[5];
            tips = new string[5];
            grayBrush = new SolidBrush(Color.FromArgb(125, Color.Gray));
            pens[0] = new Pen(Color.Black, 3); pens[0].DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            pens[1] = new Pen(Color.Black, 3); pens[1].DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            pens[2] = new Pen(Color.Black, 3); pens[2].DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            pens[3] = new Pen(Color.Black, 3); pens[3].DashStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
            pens[4] = new Pen(Color.Black, 3); pens[4].DashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot;
            this.Items.Add("Сплошная линия"); 
            this.Items.Add("Точки");
            this.Items.Add("Штрих");
            this.Items.Add("Штрих-пунктир");
            this.Items.Add("Штрих-двойной пунктир");
            this.SelectedDashStyle = DashStyle.Solid;
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            itemTip = new ToolTip();
            itemTip.AutoPopDelay = 500;
            itemTip.InitialDelay = 500;
            itemTip.ShowAlways = true;
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();

            Rectangle bounds = e.Bounds;
            if (e.Index != -1)
            {
                e.Graphics.DrawLine(pens[e.Index], bounds.Left + 2, bounds.Top + bounds.Height / 2, bounds.Right - 2, bounds.Top + bounds.Height / 2);
                if (!this.Enabled) e.Graphics.FillRectangle(grayBrush, bounds);
                if ((this.DroppedDown) && (e.State & DrawItemState.Selected) == DrawItemState.Selected)
                {
                    itemTip.Show((string)SelectedItem, this, e.Bounds.Right, e.Bounds.Bottom + 5);
                }
                else
                {
                    itemTip.Hide(this);
                }
            }
            base.OnDrawItem(e);
        }
    }
}
