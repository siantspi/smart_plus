﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;

namespace SmartPlus
{
    class ComboBoxEx : System.Windows.Forms.ComboBox
    {
        ImageList imgList;

        public ComboBoxEx()
        {
            DrawMode = DrawMode.OwnerDrawFixed;
        }
        public void SetImageList(ImageList imageList, string[] TextList)
        {
            this.Items.Clear();
            CombobBoxExItem item;
            for (int i = 0; (i < imageList.Images.Count) && (i < TextList.Length); i++)
            {
                item = new CombobBoxExItem(TextList[i], i);
                this.Items.Add(item);
            }
            this.imgList = imageList;
        }


        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();

            CombobBoxExItem item;
            Size imageSize = imgList.ImageSize;
            Rectangle bounds = e.Bounds;
            try 
            {
                item = (CombobBoxExItem)Items[e.Index];
                if (item.ImageIndex != -1)
                {
                    imgList.Draw(e.Graphics, bounds.Left, bounds.Top, item.ImageIndex);
                    e.Graphics.DrawString(item.Text, e.Font, new SolidBrush(e.ForeColor), bounds.Left + imageSize.Width, bounds.Top);
                }
                else
                {
                    e.Graphics.DrawString(item.Text, e.Font, new SolidBrush(e.ForeColor), bounds.Left, bounds.Top);
                }
            }
            catch
            {
                if (e.Index != -1)
                {
                    e.Graphics.DrawString(Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), bounds.Left, bounds.Top);
                }
                else
                {
                    e.Graphics.DrawString(Text, e.Font, new SolidBrush(e.ForeColor), bounds.Left, bounds.Top);
                }
            }
            base.OnDrawItem(e);
        }
    }

    class CombobBoxExItem
    {
        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
        private int _imgIndex;
        public int ImageIndex
        {
            get { return _imgIndex; }
            set { _imgIndex = value; }
        }

        public CombobBoxExItem() : this("")
        { }

        public CombobBoxExItem(string text) : this(text, -1)
        { }
        public CombobBoxExItem(string text, int imgIndex)
        {
            _text = text;
            _imgIndex = imgIndex;
        }
        public override string ToString()
        {
            return _text;
        }

    }

    class ComboBoxExColor : ComboBoxEx
    {
        string[] textList;
        Color[] clrList;
        ImageList imgList;
        Graphics gr;
        bool SetCustomColor = false;

        public Color SelectedColor
        {
            get
            {
                if ((SelectedIndex > -1) && (clrList.Length > 0))
                {
                    return clrList[SelectedIndex];
                }
                else
                    return Color.White;
            }
            set
            {
                bool find = false;
                for (int i = 1; i < clrList.Length; i++)
                {
                    if (clrList[i].ToArgb() == value.ToArgb())
                    {
                        this.SelectedIndex = i;
                        find = true;
                        break;
                    }
                }

                if (!find)
                {
                    SetCustomColor = true;
                    clrList[0] = value;
                    this.SelectedIndex = 0;
                    SetCustomColor = false;
                }
                this.Invalidate();
            }
        }
        public void SelectByArgb(int Argb)
        {
            bool res = false;
            for (int i = 1; (i < clrList.Length) && (!res); i++)
            {
                if (clrList[i].ToArgb() == Argb)
                {
                    this.SelectedIndex = i;
                    res = true;
                }
            }

            if (!res)
            {
                SetCustomColor = true;
                clrList[0] = Color.FromArgb(Argb); 
                this.SelectedIndex = 0;
                SetCustomColor = false;
            }
        }

        public ComboBoxExColor() : base()
        {
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            imgList = new ImageList();
            clrList = new Color[18];
            clrList[0] = Color.White;
            clrList[1] = Color.White;
            clrList[2] = Color.LemonChiffon;
            clrList[3] = Color.LightGray;
            clrList[4] = Color.Gold;
            clrList[5] = Color.LightCoral;
            clrList[6] = Color.OrangeRed;
            clrList[7] = Color.FromArgb(255, 35, 35);
            clrList[8] = Color.Peru;
            clrList[9] = Color.SaddleBrown;
            clrList[10] = Color.LightGreen;
            clrList[11] = Color.Green;
            clrList[12] = Color.SkyBlue;
            clrList[13] = Color.Purple;
            clrList[14] = Color.Gray;
            clrList[15] = Color.CornflowerBlue;
            clrList[16] = Color.FromArgb(0, 82, 164);
            clrList[17] = Color.FromArgb(35, 35, 35);
            imgList.Images.Add(Properties.Resources.Custom);
            imgList.Images.Add(Properties.Resources.White);
            imgList.Images.Add(Properties.Resources.Lemon);
            imgList.Images.Add(Properties.Resources.LightGray);
            imgList.Images.Add(Properties.Resources.Gold);
            imgList.Images.Add(Properties.Resources.LightCoral);
            imgList.Images.Add(Properties.Resources.OrangeRed);
            imgList.Images.Add(Properties.Resources.Red);
            imgList.Images.Add(Properties.Resources.Peru);
            imgList.Images.Add(Properties.Resources.SaddleBrown);
            imgList.Images.Add(Properties.Resources.LightGreen);
            imgList.Images.Add(Properties.Resources.Green);
            imgList.Images.Add(Properties.Resources.SkyBlue);
            imgList.Images.Add(Properties.Resources.Purple);
            imgList.Images.Add(Properties.Resources.Gray);
            imgList.Images.Add(Properties.Resources.CornflowerBlue);
            imgList.Images.Add(Properties.Resources.Blue);
            imgList.Images.Add(Properties.Resources.Black);
            textList = new string[18];
            textList[0] = "Custom";
            textList[1] = "White";
            textList[2] = "Lemon";
            textList[3] = "LightGray";
            textList[4] = "Gold";
            textList[5] = "LightCoral";
            textList[6] = "OrangeRed";
            textList[7] = "Red";
            textList[8] = "Peru";
            textList[9] = "SaddleBrown";
            textList[10] = "LightGreen";
            textList[11] = "Green";
            textList[12] = "SkyBlue";
            textList[13] = "Purple";
            textList[14] = "Gray";
            textList[15] = "CornflowerBlue";
            textList[16] = "Blue";
            textList[17] = "Black";

            SetImageList(imgList, textList);
            this.SelectedIndexChanged += new EventHandler(ComboBoxExColor_SelectedIndexChanged);
        }

        void ComboBoxExColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((SelectedIndex == 0) && (!SetCustomColor))
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = clrList[0];
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        clrList[0] = dlg.Color;
                    }
                }
            }
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == 15) // WM_PAINT
            {
                if ((!Focused) && (SelectedIndex > -1))
                {
                    gr = this.CreateGraphics();
                    
                    Rectangle rect = this.ClientRectangle;
                    rect.Width -= 27;
                    rect.Height -= 6;
                    rect.X = 3;
                    rect.Y = 3;

                    int trans = 255;
                    if (!this.Enabled)
                    {
                        trans = 125;
                        gr.FillRectangle(Brushes.White, rect);
                    }
                    gr.FillRectangle(new SolidBrush(Color.FromArgb(trans, SelectedColor)), rect);

                    rect.Width += 1;
                    rect.Height += 1;
                    rect.X = 2;
                    rect.Y = 2;
                    if (this.Enabled)
                    {
                        gr.DrawRectangle(Pens.Black, rect);
                    }
                    else
                    {
                        gr.DrawRectangle(Pens.Gray, rect);
                    }
                }
            }
        }
    }
}
