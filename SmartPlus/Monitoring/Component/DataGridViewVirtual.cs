﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace SmartPlus
{
    public delegate void OnStartCopyToClipboardDelegate();
    public delegate void OnEndCopyToClipboardDelegate();

    class DataGridViewVirtual : DataGridView
    {
        public event OnStartCopyToClipboardDelegate OnStartCopyToClipboard;
        public event OnEndCopyToClipboardDelegate OnEndCopyToClipboard;
        BackgroundWorker worker;

        public DataGridViewVirtual()
        {
            this.DoubleBuffered = true;
            this.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.EnableHeadersVisualStyles = false;
            this.RowHeadersVisible = false;
            this.VirtualMode = true;
            this.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            this.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.KeyDown += new KeyEventHandler(DataGridViewVirtual_KeyDown);
        }

        #region Worker Copy to Clipboard
        private string CopySelectedRowsToClipboard()
        {
            //if (this.GetClipboardContent() != null)
            //{
            //    Clipboard.SetDataObject(this.GetClipboardContent());
            //    Clipboard.GetData(DataFormats.Text);
            //    IDataObject dt = Clipboard.GetDataObject();
            //    if (dt.GetDataPresent(typeof(string)))
            //    {
            //        string tb = (string)(dt.GetData(typeof(string)));
            //        System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
            //        byte[] dataStr = encoding.GetBytes(tb);
            //        Clipboard.SetDataObject(encoding.GetString(dataStr));
            //    }
            //}


            //string str = this.GetClipboardContent().GetText(TextDataFormat.UnicodeText);
            //System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
            //byte[] dataStr = encoding.GetBytes(str);
            //return encoding.GetString(dataStr);
            return this.GetClipboardContent().GetText(TextDataFormat.UnicodeText);
        }
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = CopySelectedRowsToClipboard();
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null || e.Cancelled)
            {
                Clipboard.Clear();
            }
            else if (e.Result != null)
            {
                Clipboard.SetDataObject((string)e.Result);
            }
            if (this.OnEndCopyToClipboard != null) this.OnEndCopyToClipboard();
        }
        #endregion
        
        void DataGridViewVirtual_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
            {
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    if (this.OnStartCopyToClipboard != null) this.OnStartCopyToClipboard();
                    this.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
                    worker.RunWorkerAsync();
                }
                e.Handled = true;
            }
        }
    }
}
