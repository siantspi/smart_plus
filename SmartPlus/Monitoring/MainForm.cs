using System;
using System.Configuration;
using System.ComponentModel;
using System.Threading;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinDock;
using RDF.Objects;
using RDF;

using System.Collections;
using System.IO;

using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Deployment.Application;
using SmartPlus.DataChartEx;
using Microsoft.Win32;
using System.Security.Principal;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public partial class MainForm : Form
    {
        public bool IsOSXP;
        string CurrDir;
        public int MaxMonitorHeight = 0;
        public int MaxMonitorWidth = 0;
        public int indActiveProject = -1;
        public int indActiveOilField = -1;
        public int indActiveWorkLayer = -1;
        public string UserSmartPlusFolder;
        public string UserProjectsFolder;
        public string UserWorkDataFolder;
        public string UserAppSettingsFolder;
        public string UserTemplatesFolder;
        public string ServerName;
        public string ServerProjectsFolder;
        string ServerStatisticFolder;
        string UserDockManagerSettings;
        public string UserLogFile;

        public string ServerUser, ServerPass;
        public bool IsLoadByHttp;
        
        public int BGWorkerWorkType;
        public bool BGWorkerWorking;
        public string AssemblyVersion;
        public bool SumParamsBySumma;
        public bool SumParamsByMonth;
        public bool AppRestart = false;
        public bool PackageMetadataRemoved = false;
        public ToolTip UpdateTip;

        DataGridMer dgMer;                       // ������� ���
        DataGridGIS dgGIS;                       // ������� ���
        DataGridChess dgChess;                   // ������� ��������
        DataGridAreas dgAreas;                   // ������� ������ ����������
        DataGridCore dgCore;                     // ������� ����
        DataGridCoreTest dgCoreTest;             // ������� ������������ �����

        DataChart chart;                         // �������
        public CanvasMap canv;                   // �����
        public FilterOilObjects filterOilObj;    // ������ ��������
        public BubbleSetting fBubbleSet;         // ��������� ���� �������
        CorrelationScheme corScheme;             // ���.����� 
        CorrelationScheme planeWell;             // ������� ��������
        public DataUpdater dataUpdater;      // ������� ��������� ������
        Finder finder;                           // ����� ������� � �������������
        ToolTip ToolTipFind;
        ReportBuilder.UI.ReportBuilder ReportBuilder; // ���������� �������
        CorSchemeSettingsForm corSchemeSettings = null; // ���� ��������� ��������
        public PlaneBaseSchemes baseSchemes;
        Network.FileServer fileServer = null;
        Network.Discuss discuss = null;

        public OutputTextForm OutBox;             // ���� ������ ������

        // ���� ������� � ������ ����������, ��������, ��������, �����, ����.�����
        public DockableControlPane chartPane, dgAreasPane, corSchPane, planeWellPane, corePane, coreTestPane, merViewPane;
        public DockableControlPane  dcpDiscuss, dcpActiveOilfield;
        DockableControlPane dcpWellSetting;

        List<Project> ProjList;
        public BGWorker worker, planeWorker;

        public ToolStripTextBoxEx tbFinderEx, WL_FinderEx; // �����


        // �����
        WellDataFilterForm WDFilterForm;         // ������ ������� �� ������ 
        public IconSelectForm fIconSelect;
        public IconListForm fIconList;
        WellListSettings WLSettings;
        GlobalProjectsForm fGlobProj;
        public ProgressForm fProgress;
        public ToolTipMessage ToolTip;
        //public UpdateProjForm fUpdate;
        UpdateAppForm fUpdateApp;
        public AppSettingsForm fAppSettings;
        ExportProjectForm exportProjForm = null;
        private AnnoyDialogForm AnnoyDialog = null;

        // Timers
        System.Windows.Forms.Timer AnnoyTimer = null;

        public Bitmap[] bmpWait;

        // Update Application
        public Constant.ApplicationUpdateState UpdateState;
        bool AppClosing;

        bool ShowFormUpdate;
        AboutForm formAbout;
        WatsNewForm formWatsNew;
        System.Windows.Forms.Timer tmrUpdateApp;
        System.Windows.Forms.Timer tmrPrgHide;
        ApplicationDeployment ad;
        ArrayList logList;

        // ����� ��������� ����������
        public AppCommonSettings AppSettings;

        public bool UserDialogShowing
        {
            get
            {
                if ((fProgress != null) && (fProgress.Visible)) return true;
                if (UpdateProjForm.FormVisible) return true;
                if ((fGlobProj != null) && (fGlobProj.Visible)) return true;
                if ((formAbout != null) && (formAbout.Visible)) return true;
                if ((formWatsNew != null) && (formWatsNew.Visible)) return true;
                if (tbFinderEx.TextBox.Focused) return true;
                if (ReportBuilder != null) return ReportBuilder.UserDialogShowing;
                return false;
            }
        }
        public bool IsActivated;
        bool ShortCutPinned;

        public MainForm()
        {
            InitializeComponent();

            tbFinderEx = new ToolStripTextBoxEx();
            tbFinderEx.ToolTipText = "";
            mMenu.Items.Add(tbFinderEx);
            tbFinderEx.Width = 400;
            WL_FinderEx = new ToolStripTextBoxEx();

            AppClosing = false;
            IsActivated = true;
            logList = new ArrayList();
            System.OperatingSystem os = System.Environment.OSVersion;
            IsOSXP = ((os.Platform == PlatformID.Win32Windows) || ((os.Platform == PlatformID.Win32NT) && (os.Version.Major < 6)));

            // �������� ����� Publish ������ 
            int ver_build = 0;
            
            if (ApplicationDeployment.IsNetworkDeployed) 
            {
                 ad = ApplicationDeployment.CurrentDeployment;
                 Version ver = ad.CurrentVersion;
                 ver_build = ver.Build;
                 AssemblyVersion = String.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision);
            }
            else
            {
                System.Reflection.Assembly assem = System.Reflection.Assembly.GetEntryAssembly();
                System.Reflection.AssemblyName assemName = assem.GetName();
                Version ver = assemName.Version;
                AssemblyVersion = ver.ToString();
            }

            this.Text = String.Format("SmartPlus: ���������� � ���������� [������: {0}]", AssemblyVersion);
#if DEBUG
            this.Text += " DEBUG";
#endif
            planeWellPane = null;
            corSchPane = null;
            chartPane = null;
            dgAreasPane = null;

            UserDockManagerSettings = "udm.bin";
            if ((uDockManager != null) && ((uDockManager.ControlPanes.Count < 19) && File.Exists(UserDockManagerSettings)))
            {
                uDockManager.LoadFromBinary(UserDockManagerSettings);
            }
#if DEBUG
            //if (File.Exists(UserDockManagerSettings)) uDockManager.LoadFromBinary(UserDockManagerSettings);
#endif

            SumParamsByMonth = true;
            SumParamsBySumma = true;

            ProjList = new List<Project>();
            UpdateTip = new ToolTip();
            UpdateTip.Active = true;
            UpdateTip.AutomaticDelay = 0;
            UpdateTip.AutoPopDelay = 25000;
            UpdateTip.InitialDelay = 0;
            UpdateTip.ShowAlways = true;
            UpdateTip.ToolTipIcon = ToolTipIcon.Info;
            UpdateTip.ToolTipTitle = "��������!";
            
            UpdateTip.UseAnimation = true;
            UpdateTip.UseFading = true;
            UpdateTip.SetToolTip(this, "�������� ���������� ������ �� �������!");

            CurrDir = System.Windows.Forms.Application.StartupPath;
            UserSmartPlusFolder = App.GetRegistryPath();
            if(UserSmartPlusFolder.Length == 0) UserSmartPlusFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            
            UserSmartPlusFolder += "\\SmartPlus";
            //UserSmartPlusFolder += "\\���";
            ServerName = "bashirovia01";
            ServerProjectsFolder = @"\\" + ServerName + "\\Monitoring\\MonitoringProjects";
            ServerStatisticFolder = @"\\" + ServerName + "\\Monitoring\\Application Files\\Stat";

            //mMenuReports.Visible = false;

            UserLogFile = UserSmartPlusFolder + "\\log.csv";
            
            UserProjectsFolder = UserSmartPlusFolder + "\\�������";
            UserWorkDataFolder = UserSmartPlusFolder + "\\������� �����";
            UserAppSettingsFolder = UserSmartPlusFolder + "\\���������";

            // BITMAP
            Screen[] all_scr = Screen.AllScreens;
            for (int i = 0; i < all_scr.Length; i++)
            {
                if (MaxMonitorWidth < all_scr[i].Bounds.Width) MaxMonitorWidth = all_scr[i].Bounds.Width;
                if (MaxMonitorHeight < all_scr[i].Bounds.Height) MaxMonitorHeight = all_scr[i].Bounds.Height;
            }
            canv = new CanvasMap(this);
            
            this.tsCanvContainer.ContentPanel.Cursor = Cursors.Default;
            canv.reLog = reLog;

            filterOilObj = new FilterOilObjects(this);
            dgMer = new DataGridMer(this);
            dgGIS = new DataGridGIS(this);
            dgChess = new DataGridChess(this);
            dgAreas = new DataGridAreas(this);
            dgCore = new DataGridCore(this);
            dgCoreTest = new DataGridCoreTest(this);

            chart = new DataChart(this);

            fBubbleSet = new BubbleSetting();
            WLSettings = new WellListSettings(this);
            WDFilterForm = new WellDataFilterForm(this);
            
            UpdateWellListSettingsToolBar();

            fProgress = new ProgressForm(this);
            ToolTip = new ToolTipMessage(this);
            fUpdateApp = new UpdateAppForm(this);
            //fSetting = new SettingForm(this);
            fAppSettings = new AppSettingsForm(this);
            finder = new Finder(canv);
            finder.Owner = this;

            OutBox = new OutputTextForm(this);

            ad = null;
            // Timers

           // tmrUpdateApp = new System.Windows.Forms.Timer();
           // tmrUpdateApp.Enabled = true;
           // tmrUpdateApp.Interval = 14400000;
           // tmrUpdateApp.Tick += new EventHandler(tmrUpdateApp_Tick);

            AnnoyTimer = new System.Windows.Forms.Timer();
            AnnoyTimer.Enabled = false;
            AnnoyTimer.Interval = 300000;
            AnnoyTimer.Tick += new EventHandler(AnnoyTimer_Tick);

            pgProperties.PropertySort = PropertySort.Categorized;
            pgProperties.Font = new Font("Calibri", 8.25f);
            tsbUpdates.Visible = false;

            worker = new BGWorker(this);
            planeWorker = new BGWorker(this);
            this.tsbCancel.Visible = false;

            bmpWait = new Bitmap[12];
            bmpWait[0] = Properties.Resources.wait_icon;
            bmpWait[1] = Properties.Resources.wait_icon1;
            bmpWait[2] = Properties.Resources.wait_icon2;
            bmpWait[3] = Properties.Resources.wait_icon3;
            bmpWait[4] = Properties.Resources.wait_icon4;
            bmpWait[5] = Properties.Resources.wait_icon5;
            bmpWait[6] = Properties.Resources.wait_icon6;
            bmpWait[7] = Properties.Resources.wait_icon7;
            bmpWait[8] = Properties.Resources.wait_icon8;
            bmpWait[9] = Properties.Resources.wait_icon9;
            bmpWait[10] = Properties.Resources.wait_icon10;
            bmpWait[11] = Properties.Resources.wait_icon11;

            InitializeWindows();

            mMenuProjOpen.Visible = false;
            mMenuSaveCoef.Visible = false;

            mMenuProj.Visible = false;
            mMenuFlooding.Visible = false;
            tsbCalcAccumBubble.Visible = true;
            tsbCalcBubble.Visible = true;
            mMenuOpenImage.Enabled = false;
            mMenuExport.Enabled = false;
            mMenuCoreLoading.Visible = false;
            tsbShowLegend.Visible = true;
            mMenuCheckUpdates.Visible = false;

            if (!File.Exists(Path.Combine(Application.StartupPath, "whatnews.txt")))
                this.mMenuWatsNew.Visible = false;

            tbFinderEx.Enabled = false;
            mMenuWinFileServer.Visible = false;

            #region Graphics Properties

            this.imgList.Images.Add(Properties.Resources.apply);
            this.scGraphics.Panel2Collapsed = true;
            tsbTable.Image = Properties.Resources.unchecked16;
            tsbIntervalMonth.Checked = true;
            tsbShowProjParams.Visible = false;
            tsbShowProjParams.Checked = true;
            tsbShowBPParams.Visible = false;
            tsbShowBPParams.Checked = false;
            tsbGraphicsSettings.Visible = false;

            GR_SnapShoot.Tag = 1;
            GR_SnapShoot.ButtonClick += new EventHandler(GR_SnapShootAll_Click);
            GR_SnapShoot.Image = GR_SnapShoot.DropDownItems[1].Image;
            GR_SnapShoot.ToolTipText = GR_SnapShoot.DropDownItems[1].Text;
            #endregion

            #region Well List ToolBar
            WL_AddWell.Tag = 2;
            WL_AddWell.Image = WL_AddWell.DropDownItems[2].Image;
            WL_AddWell.ToolTipText = WL_AddWell.DropDownItems[2].ToolTipText;
            WL_AddWell.ButtonClick += new EventHandler(WL_AddClipBoardWell_Click);
            WL_Graphics.Tag = 0;
            WL_Graphics.Image = WL_Graphics.DropDownItems[0].Image;
            WL_Graphics.ToolTipText = WL_Graphics.DropDownItems[0].ToolTipText;
            WL_Graphics.ButtonClick += new EventHandler(WL_GraphicsByOneDate_Click);
            tsMenuWellListSettings.Items.Insert(tsMenuWellListSettings.Items.Count - 1, WL_FinderEx);
            WL_FinderEx.ToolTipText = "������� ����� �������� ��� ������\n������ '�����' � '����' ��� ��������\n�� ��������� ��������";
            WL_FinderEx.Width = 50;
            #endregion

            #region CANVAS TOOLBAR
            tsbMapSnapShoot.Tag = 0;
            tsbMapSnapShoot.ButtonClick += new EventHandler(tsbPartMapSnapShoot_Click);
            tsbMapSnapShoot.Image = tsbMapSnapShoot.DropDownItems[0].Image;
            tsbMapSnapShoot.ToolTipText = tsbMapSnapShoot.DropDownItems[0].Text;
            #endregion

            #region COR SCHEME SNAPSHOOT
            COR_SCH_SnapShoot.Tag = 0;
            COR_SCH_SnapShoot.ButtonClick += new EventHandler(COR_SCH_PartSnapShoot_Click);
            COR_SCH_SnapShoot.Image = COR_SCH_SnapShoot.DropDownItems[0].Image;
            COR_SCH_SnapShoot.ToolTipText = COR_SCH_SnapShoot.DropDownItems[0].Text;
            #endregion

            #region PLANE SNAPSHOOT
            PLN_SnapShoot.Tag = 0;
            PLN_SnapShoot.ButtonClick += new EventHandler(PLN_PartSnapShoot_Click);
            PLN_SnapShoot.Image = PLN_SnapShoot.DropDownItems[0].Image;
            PLN_SnapShoot.ToolTipText = PLN_SnapShoot.DropDownItems[0].Text;
            #endregion

            mMenuLoading.Visible = false;
            mMenuReportFloodingAnalysis.Visible = false;
            mMenuPrintMap.Visible = false;
            tsbShowBizPlanOnMap.Visible = false;
            mMenuReportDepositOIZ.Visible = false;

#if DEBUG
                //tsbGraphicsSettings.Visible = true;
                //mMenuTestButton.Visible = true;
                mMenuSaveCoef.Visible = true;
                //mMenuFlooding.Visible = true;
                //tsbCalcHistoryBubble.Visible = true;
                //tsbBubbleSettings.Visible = true;
                //mMenuLoadOilAreas.Visible = true;
                //tsbMarkerEditMode.Visible = true;
                //mMenuCoreLoading.Visible = true;
                //mMenuReportFloodingAnalysis.Visible = true;
                //mMenuLoading.Visible = true;
                //tsbCreateVoronoiMap.Visible = true;
#endif

#if BASHNEFT
            string userName = Environment.UserName.ToUpper();
            List<string> DevelopGroup = new List<string>(new string[] {  });
            List<string> GTMsectorGroup = new List<string>(new string[] {  });
            List<string> FloodingGroup = new List<string>(new string[] {  });

            List<string> MonitoringGroup = new List<string>(new string[] {  });

            List<string> GIPGroup = new List<string>(new string[] {  });
            List<string> RazrabGroup = new List<string>(new string[] {  });
            List<string> OtherGroup = new List<string>(new string[] {  });
            List<string> ANKGroup = new List<string>(new string[] {  });

            bool IsUserDevelopGroup = DevelopGroup.IndexOf(userName) > -1;
            bool IsUserFloodingGroup = FloodingGroup.IndexOf(userName) > -1;
            bool IsUserMonitoringGroup = MonitoringGroup.IndexOf(userName) > -1;
            bool IsUserGIPGroup = GIPGroup.IndexOf(userName) > -1;
            bool IsUserRazrabGroup = RazrabGroup.IndexOf(userName) > -1;
            bool IsUserOtherGroup = OtherGroup.IndexOf(userName) > -1;
            bool IsUserANKGroup = ANKGroup.IndexOf(userName) > -1;
            bool IsUserGtmSectorGroup = GTMsectorGroup.IndexOf(userName) > -1;

            mMenuFlooding.Visible = IsUserDevelopGroup || IsUserFloodingGroup;

            tsbShowBPParams.Visible = IsUserDevelopGroup || IsUserFloodingGroup || IsUserMonitoringGroup || IsUserGIPGroup;
            tsbShowBPParams.Tag = IsUserDevelopGroup || IsUserFloodingGroup || IsUserMonitoringGroup || IsUserGIPGroup;
            tsbShowBizPlanOnMap.Visible = IsUserDevelopGroup || IsUserFloodingGroup || IsUserMonitoringGroup ||IsUserGIPGroup;

            mMenuAutoMatchGTM.Visible = IsUserDevelopGroup || IsUserGtmSectorGroup;

            mMenuWinDiscussion.Checked = true;
            //mMenuPrintMap.Visible = true;
#if !DEBUG
            fileServer = new SmartPlus.Network.FileServer(this);
            discuss = new SmartPlus.Network.Discuss(this, fileServer);
#endif

            if (IsUserDevelopGroup || IsUserFloodingGroup || IsUserMonitoringGroup || IsUserGIPGroup || IsUserOtherGroup)
            {
                mMenuReportBuilder.Visible = true;
                ReportBuilder = new SmartPlus.ReportBuilder.UI.ReportBuilder(this);
            }

            mMenuReportDepositOIZ.Visible = IsUserDevelopGroup;
            mMenuReportCore.Visible = IsUserDevelopGroup;

#endif
            tmrPrgHide = new System.Windows.Forms.Timer();
            tmrPrgHide.Enabled = false;
            tmrPrgHide.Interval = 250;
            tmrPrgHide.Tick += new EventHandler(tmrPrgHide_Tick);

            // ����� ���������
            AppSettings = fAppSettings.ReadAppCommonSettings();

            // ���.�����
            baseSchemes = fAppSettings.ReadCorSchemeBaseFromAppSettings();
            corScheme = new CorrelationScheme(this, false);
            corScheme.SetBaseScheme(baseSchemes[0]);

            // �������
            planeWell = new CorrelationScheme(this, true);
            planeWell.SetBaseScheme(baseSchemes[1]);
            planeWell.SetVisibleByType(Constant.Plane.ObjectType.MARKER, false);
            UpdateCorSchemeToolBars();

            
            if (AppSettings.UpdatebyHttps)
            {
                ServerName = AppSettings.UpdatebyHttpsServerName;
            }


#if (!DEBUG)
            CheckForShortcut(true);
#endif
            formAbout = new AboutForm(this);
            formWatsNew = new WatsNewForm(this);
            this.Shown += new EventHandler(fMain_Shown);
            this.LocationChanged += new EventHandler(fMain_LocationChanged);
            this.Activated += new EventHandler(fMain_Activated);
            this.Deactivate += new EventHandler(fMain_Deactivate);
            this.SizeChanged += new EventHandler(fMain_SizeChanged);
            this.MouseWheel += new MouseEventHandler(fMain_MouseWheel);

            // ����� �� ���������
            tbFinderEx.TextBox.TextChanged += new EventHandler(tbFinder_TextChanged);
            tbFinderEx.TextBox.MouseEnter += new EventHandler(tbFinder_MouseEnter);
            tbFinderEx.TextBox.MouseDown += new MouseEventHandler(tbFinder_MouseDown);
            tbFinderEx.TextBox.KeyDown += new KeyEventHandler(tbFinder_KeyDown);
            tbFinderEx.TextBox.MouseLeave += new EventHandler(tbFinder_MouseLeave);

            this.WL_FinderEx.TextBox.TextChanged += new EventHandler(WL_FindWellBox_TextChanged);
            this.WL_FinderEx.TextBox.KeyDown += new KeyEventHandler(WL_FindWellBox_KeyDown);
        }

        void AnnoyTimer_Tick(object sender, EventArgs e)
        {
            if (AnnoyDialog == null)
            {
                AnnoyDialog = new AnnoyDialogForm();
                AnnoyDialog.ShowDialog();
                AnnoyDialog = null;
            }
        }

        void tmrPrgHide_Tick(object sender, EventArgs e)
        {
            if((fProgress == null) || (!fProgress.hided)) tmrPrgHide.Stop();
            if ((fProgress != null) && (fProgress.hided) && (fProgress.Visible))
            {
                fProgress.hided = false;
                fProgress.Hide();
            }
        }
        void fMain_SizeChanged(object sender, EventArgs e)
        {
            if ((fProgress.hided) && (this.WindowState == FormWindowState.Maximized))
            {
                pb.Value = pb.Maximum;
                tmrPrgHide.Start();
            }
            int width = this.ClientRectangle.Width - (tbFinderEx.Owner.Location.X + tbFinderEx.ContentRectangle.X + 400);
            if (width > 400) width = 400;
            tbFinderEx.Width = width;
        }
        void fMain_Activated(object sender, EventArgs e)
        {
            IsActivated = true;
        }
        void fMain_Deactivate(object sender, EventArgs e)
        {
            IsActivated = false;
        }

        #region Initialize Function
        private void InitializeWindows()
        {
            int ind;
            string MenuText = "";
            dcpWellSetting = null;
            chartPane = null;
            if (uDockManager != null)
            {
                for (int i = 0; i < uDockManager.ControlPanes.Count; i++)
                {
                    MenuText = "";
                    DockableControlPane dcp = uDockManager.ControlPanes[i];
                    try
                    {
                        ind = Convert.ToInt32(dcp.Key);
                    }
                    catch
                    {
                        ind = -1;
                    }

                    switch (ind)
                    {
                        case 1:
                            dcp.TextTab = "�������";
                            dcp.Text = "�������";
                            chartPane = dcp;
                            MenuText = "�������";
                            break;
                        case 2:
                            dcp.Activate();
                            MenuText = dcp.Text;
                            break;
                        case 3:
                            dcp.TextTab = "����: �������";
                            dcp.Text = "����: �������";
                            corePane = dcp;
                            MenuText = dcp.Text;
                            break;
                        case 4:
                            dcp.TextTab = "����: ������������";
                            dcp.Text = "����: ������������";
                            coreTestPane = dcp;
                            MenuText = dcp.Text;
                            break;
                        case 6:
                            dcp.TextTab = "���.�����";
                            dcp.Text = "���.�����";
                            corSchPane = dcp;
                            MenuText = dcp.Text;
                            break;
                        case 7:
                            MenuText = dcp.Text;
                            dcpActiveOilfield = dcp;
                            break;
                        case 8:
                            MenuText = dcp.Text;
                            merViewPane = dcp;
                            break;
                        case 9:
                            dcpDiscuss = dcp;
                            dcp.TextTab = "����������";
                            dcp.Text = "����������";
                            MenuText = dcp.Text;
                            break;
                        case 10:
                            dcp.TextTab = "�������";
                            dcp.Text = "�������";
                            planeWellPane = dcp;
                            MenuText = dcp.Text;
                            break;
                        case 14:
                            dcpWellSetting = dcp;
                            MenuText = "������ �������";
                            dcp.Text = "������ �������";
                            break;
                        case 15:
                            dcp.Closed = true;
                            dcp.TextTab = "�����";
                            dcp.Text = "�����";
                            MenuText = dcp.Text;
                            break;
                        case 18:
                            dgAreasPane = dcp;
                            MenuText = dcp.Text;
                            break;
                        default:
                            MenuText = dcp.Text;
                            break;
                    }
                    if ((ind > -1) && (ind < mMenuWindows.DropDownItems.Count))
                    {
                        mMenuWindows.DropDownItems[ind].Tag = dcp;
                        dcp.Tag = (ToolStripMenuItem)mMenuWindows.DropDownItems[ind];
                        ((ToolStripMenuItem)mMenuWindows.DropDownItems[ind]).Checked = !dcp.Closed;
                    }
                }
                uDockManager.PaneHidden += uDockManager_PaneHidden;
                uDockManager.PaneDisplayed += uDockManager_PaneDisplayed;
                uDockManager.PaneActivate += uDockManager_PaneActivate;
            }
        }
        void uDockManager_PaneActivate(object sender, ControlPaneEventArgs e)
        {
            int ind = -1;
            DockableControlPane dcp = e.Pane;
            try
            {
                ind = Convert.ToInt32(dcp.Key);
            }
            catch
            {
                ind = -1;
            }
            switch (ind)
            {
                case 0:
                    if (dgGIS != null) dgGIS.UpdateBySourceWell();
                    break;
                case 3:
                    if (dgCore != null) dgCore.UpdateBySourceWell();
                    break;
                case 4:
                    if (dgCoreTest != null) dgCoreTest.UpdateBySourceWell();
                    break;
                case 8:
                    if (dgMer != null) dgMer.UpdateBySourceWell();
                    break;
                case 17:
                    if (dgChess != null) dgChess.UpdateBySourceWell();
                    break;
            }
        }
        void uDockManager_PaneDisplayed(object sender, PaneDisplayedEventArgs e)
        {
            int ind = -1;
            DockableControlPane dcp = e.Pane;
            try
            {
                ind = Convert.ToInt32(dcp.Key);
            }
            catch
            {
                ind = -1;
            }
            if (ind == 9)
            {
                if (discuss != null) discuss.UpdateData(canv.LastSelectObject, true);
                if (fileServer != null) fileServer.UpdateData(canv.LastSelectObject, true);
            }
        }

        void uDockManager_PaneHidden(object sender, PaneHiddenEventArgs e)
        {
            if (e.Pane.Tag != null) ((ToolStripMenuItem)e.Pane.Tag).Checked = !e.Pane.Closed;
        }

        void WindowClick(object sender, EventArgs e)
        {
            ToolStripMenuItem newWindow = sender as ToolStripMenuItem;
            DockableControlPane dcp;
            if (newWindow.Tag != null)
            {
                dcp = (DockableControlPane)newWindow.Tag;
                if (newWindow.Checked)
                {
                    dcp.Close();
                    newWindow.Checked = false;
                }
                else
                {
                    dcp.Show();
                    newWindow.Checked = true;
                }
            }
        }

        private void CheckForShortcut(bool testFirstRun)
        {
            
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                if (!testFirstRun || ad.IsFirstRun)
                {
#if BASHNEFT
                string desktopPath = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "\\���.appref-ms");
                string shortcutName = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Programs), "\\BN\\���.appref-ms");
                if (File.Exists(shortcutName)) File.Copy(shortcutName, desktopPath, true);

                desktopPath = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "\\��� (SmartPlus).appref-ms");
                shortcutName = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Programs), "\\SmartPlus\\��� (SmartPlus).appref-ms");
                if (File.Exists(shortcutName)) File.Copy(shortcutName, desktopPath, true);
                string pinnedPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Microsoft\Internet Explorer\Quick Launch\User Pinned\StartMenu";
                if (ShortCutPinned && File.Exists(shortcutName) && Directory.Exists(pinnedPath)) File.Copy(shortcutName, pinnedPath + "\\��� (SmartPlus).appref-ms", true);
#else
                string desktopPath = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "\\SmartPlus.appref-ms");
                string shortcutName = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Programs), "\\SmartPlus\\SmartPlus.appref-ms");
                if (File.Exists(shortcutName)) File.Copy(shortcutName, desktopPath, true);
#endif
                }
            }
        }

        #endregion

        #region BackgroundWorker Functions

        public void AfterBGWorkerCompleted(Constant.BG_WORKER_WORK_TYPE workType, RunWorkerCompletedEventArgs e) 
        {
            switch (workType)
            {
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PROJECT:
                    mMenuClose.Enabled = true;
                    mMenuOpenImage.Enabled = true;
                    mMenuExport.Enabled = true;
                    AddMMenuProject((ProjList[indActiveProject]).Name);
                    stBar1.Text = "��������� �������.���������� ���������..";
                    SetComponentProject();
                    if(discuss != null) discuss.InitModules();
                    canv.SortInspectorByNGDU();
                    canv.LoadWorkTree();
                    canv.Enable();
                    canv.SetCenterProject();
                    filterOilObj.SetProjectStratumcodes();
                    filterOilObj.UpdateTreeView();
                    canv.DrawLayerList();
                    stBar1.Text = "";
                    tsbCancel.Visible = false;
                    tbFinderEx.Enabled = true;
                    //Stat.AddMessage(CollectorStatMessageId.LOAD_PROJECT, "������ '" +(ProjList[indActiveProject]).Name + "' ��������");
                    CreateDataUpdater(ProjList[indActiveProject]);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_GLOBAL_PROJECT:
                    mMenuClose.Enabled = true;
                    mMenuOpenImage.Enabled = true;
                    mMenuExport.Enabled = true;
                    AddMMenuProject((ProjList[indActiveProject]).Name);
                    stBar1.Text = "��������� �������.���������� ���������..";
                    SetComponentProject();
                    if(discuss != null) discuss.InitModules();
                    canv.SortInspectorByNGDU();
                    canv.LoadWorkTree();
                    canv.Enable();
                    canv.SetCenterProject();
                    filterOilObj.SetProjectStratumcodes();
                    filterOilObj.UpdateTreeView();
                    canv.DrawLayerList();
                    stBar1.Text = "";
                    tsbCancel.Visible = false;
                    tbFinderEx.Enabled = true;
                    //Stat.AddMessage(CollectorStatMessageId.LOAD_PROJECT, "������ '" + (ProjList[indActiveProject]).Name + "' ��������");
                    CreateDataUpdater(ProjList[indActiveProject]);
                    canv.TestConvertWorkFolder();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.RELOAD_PROJECT:
                    mMenuOpenImage.Enabled = true;
                    mMenuExport.Enabled = true;
                    AddMMenuProject((ProjList[indActiveProject]).Name);
                    stBar1.Text = "��������� �������.���������� ���������..";
                    SetComponentProject();
                    canv.SortInspectorByNGDU();
                    canv.LoadWorkTree();
                    canv.Enable();
                    canv.SetCenterProject();
                    filterOilObj.SetProjectStratumcodes();
                    filterOilObj.UpdateTreeView();
                    canv.DrawLayerList();
                    tbFinderEx.Enabled = true;
                    stBar1.Text = "";
                    tsbUpdates.Visible = false;
                    CreateDataUpdater(ProjList[indActiveProject]);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CONVERT_WORK_FOLDER:
                    if (canv.MapProject != null) canv.MapProject.tempStr = string.Empty;
                    ReLoadProject();
                    if (e.Result == null || ((List<string>)e.Result).Count == 0)
                    {
                        MessageBox.Show(this, "����������� ������ ������ ������� ����� ������ �������!", "��������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (e.Result != null)
                    {
                        List<string> errors = (List<string>)e.Result;
                        if (errors.Count > 0)
                        {
                            MSOffice.Excel excel = new MSOffice.Excel();
                            try
                            {
                                excel.EnableEvents = false;
                                excel.Visible = false;
                                excel.NewWorkbook();
                                excel.SetValue(0, 0, "��� ����������� ������ ������ ������� ����� �� ������� ��������������� ��������� �����:");
                                for (int i = 0; i < errors.Count; i++)
                                {
                                    excel.SetValue(i + 1, 0, errors[i]);
                                }
                            }
                            finally
                            {
                                excel.Visible = true;
                                excel.EnableEvents = true;
                                excel.Disconnect();
                            }
                        }
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CHESS_FROM_CACHE:
                    stBar1.Text = "";
                    this.canv.twLayers.cMenuOilField.Items[1].Enabled = true;
                    tsbCancel.Visible = false;
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYERS:
                    if (Directory.Exists(UserWorkDataFolder + "1") && (!Directory.Exists(UserWorkDataFolder)))
                        Directory.Move(UserWorkDataFolder + "1", UserWorkDataFolder);
                    stBar1.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_TABLES_81:
                    stBar1.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_AREAS_CODES:
                    stBar1.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CALC_AREAS_COMPENSATION:
                    stBar1.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CONSTRUCTION:
                    canv.LoadLayers();
                    canv.SortInspectorByNGDU();
                    canv.DrawLayerList();
                    stBar1.Text = "";
                    break;
            }
        }

        private void tsbCancel_Click(object sender, EventArgs e) 
        {
            worker.CancelAsync();
        }
        
        #endregion BackgroundWorker Functions

        // Data Updater
        void CreateDataUpdater(Project project)
        {
            if (AppSettings.UpdatebyHttps)
            {
                dataUpdater = new DataUpdater(ProjList[indActiveProject], UserProjectsFolder, AppSettings);
            }
            else
            {
                dataUpdater = new DataUpdater(ProjList[indActiveProject], ServerName, ServerProjectsFolder, UserProjectsFolder);
            }
            dataUpdater.OnCheckUpdates += new OnEndCheckUpdatesDelegate(dataUpdater_OnCheckUpdates);
            dataUpdater.OnStartDownload += new OnStartDownloadDelegate(dataUpdater_OnStartDownload);
            dataUpdater.OnEndDownload += new OnEndDownloadDelegate(dataUpdater_OnEndDownload);
            dataUpdater.Start();
        }
        void dataUpdater_OnEndDownload()
        {
            if (dataUpdater != null && dataUpdater.UpdatesAvailable)
            {
                if (!dataUpdater.UpdatesInstalled && dataUpdater.UpdatesPreLoadAvailable)
                {
                    if (!UpdateProjForm.FormVisible)
                    {
                        string str = "������ ���������� ������� ��� ��������� �� ���������!\n[������� ���� ��� ����������...]";
                        ToolTipMessage.Show(this, Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE_LOADED, "��������!", str);
                    }
                    tsbUpdates.Image = Properties.Resources.refresh_RED_16;
                }
                else if (dataUpdater.UpdatesInstalled)
                {
                    ReLoadProject();
                }
            }
        }
        void dataUpdater_OnCheckUpdates()
        {
            if (dataUpdater != null && dataUpdater.UpdatesAvailable)
            {
                tsbUpdates.Visible = true;
                tsbUpdates.Image = (dataUpdater.UpdatesPreLoadAvailable) ? Properties.Resources.refresh_RED_16 : Properties.Resources.refresh16;

                if (AppSettings.UpdatebyHttps && !dataUpdater.UpdatesPreLoadAvailable)
                {
                    //string str = "���������� �� ������� �������� � ������ ����� ����������.\n��� ������ ����������������� �����.";
                    //MessageBox.Show(str, "��������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //UpdateProjForm.ShowForm(this);

                    const string message = "�� ������� �������� ���������� �������. ��������� �������� ������?";
                    const string caption = "�������� ���������� �������";
                    var result = MessageBox.Show(message,caption,
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question);

                    if (result == DialogResult.No)
                    {
                        dataUpdater.UpdatesAvailable = false;
                        tsbUpdates.Visible = false;
                    }
                }

                if (dataUpdater.UpdatesPreLoadAvailable)
                {
                    string str = "���������� ������� ��� ��������� � ������ ����� �����������.\n��� ������ ����������������� �����.";
                    MessageBox.Show(str, "��������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    UpdateProjForm.ShowForm(this);
                }
                else if (dataUpdater.NeedHaveFreeSpace > 0 && !dataUpdater.IsFirstUpdate)
                {
                    string diskName = string.Empty;
                    if (dataUpdater.UpdateOilFieldList.Count > 0 && dataUpdater.UpdateOilFieldList[0].Count > 0)
                    {
                        diskName = dataUpdater.UpdateOilFieldList[0][0].PreLoadPath.Remove(1, dataUpdater.UpdateOilFieldList[0][0].PreLoadPath.Length - 1);
                    }
                    string str = string.Format("��� ������� �������� ���������� ������������ ����� �� ����� '{0}:\\'\n������� ���� ��� ������� ���������� �������...", diskName);
                    ToolTipMessage.Show(this, Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE, "��������!", str);
                } 
                else if (dataUpdater.NeedHaveFreeSpace > 0 && dataUpdater.IsFirstUpdate)
                {
                    string diskName = string.Empty;
                    if (dataUpdater.UpdateOilFieldList.Count > 0 && dataUpdater.UpdateOilFieldList[0].Count > 0)
                    {
                        diskName = dataUpdater.UpdateOilFieldList[0][0].PreLoadPath.Remove(1, dataUpdater.UpdateOilFieldList[0][0].PreLoadPath.Length - 1);
                    }
                    string str = string.Format("��� �������� ���������� ������������ ����� �� ����� '{0}:\\'\n������� ���� ��� ������� ���������� �������...", diskName);
                    ToolTipMessage.Show(this, Constant.TOOLTIP_TYPE_ID.CHECK_PROJ_UPDATE, "��������!", str);
                }
            }
        }
        void dataUpdater_OnStartDownload()
        {
            if (AppSettings.UpdatebyHttps)
            {
                UpdateProjForm.ShowForm(this);
            }
        }

        // Main Menu Commands
        #region Update APP

        public void CancelAppUpdate()
        {
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                switch (UpdateState)
                {
                    case Constant.ApplicationUpdateState.StartCheckUpdate:
                        ApplicationDeployment.CurrentDeployment.CheckForUpdateAsyncCancel();
                        break;
                }
            }
        }
        void RemovePackageMetadata()
        {
            PackageMetadataRemoved = false;
            try
            {
                WindowsIdentity wid = WindowsIdentity.GetCurrent();
                if (wid != null)
                {
                    string sid = wid.User.Value + "_Classes";
                    RegistryKey key = Registry.Users.OpenSubKey(sid + @"\Software\Microsoft\Windows\CurrentVersion\Deployment\SideBySide\2.0\PackageMetadata", true);
                    if (key != null)
                    {
                        key.DeleteSubKeyTree("{2ec93463-b0c3-45e1-8364-327e96aea856}_{3f471841-eef2-47d6-89c0-d028f03a4ad5}");
                        key.DeleteSubKeyTree("{2ec93463-b0c3-45e1-8364-327e96aea856}_{60051b8f-4f12-400a-8e50-dd05ebd438d1}");
                        key.Close();
                        PackageMetadataRemoved = true;
                    }
                }
            }
            catch (Exception ex)
            {
                File.WriteAllText(UserSmartPlusFolder + "\\UpdateRegError.txt", ex.Message);
            }
        }
        void tmrUpdateApp_Tick(object sender, EventArgs e)
        {
            tmrUpdateApp.Stop();
            //Stat.AddMessage(CollectorStatMessageId.UPDATE_APP, "�������� ���������� ��������� �� �������");
            UpdateApplication(false);
            tmrUpdateApp.Start();
        }

        private void UpdateApplication(bool ShowUpdateForm)
        {
            if ((ApplicationDeployment.IsNetworkDeployed) && (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable()))
            {
                this.ShowFormUpdate = ShowUpdateForm;
                if (ad == null)
                {
                    ad = ApplicationDeployment.CurrentDeployment;
                    ad.CheckForUpdateCompleted += new CheckForUpdateCompletedEventHandler(ad_CheckForUpdateCompleted);
                    ad.CheckForUpdateProgressChanged += new DeploymentProgressChangedEventHandler(ad_CheckForUpdateProgressChanged);
                    ad.UpdateCompleted += new AsyncCompletedEventHandler(ad_UpdateCompleted);
                    ad.UpdateProgressChanged += new DeploymentProgressChangedEventHandler(ad_UpdateProgressChanged);
                }
                UpdateState = Constant.ApplicationUpdateState.StartCheckUpdate;
                ad.CheckForUpdateAsync();
            }
			/*
            else
            {
                HTTPSUpdateAppForm up = new HTTPSUpdateAppForm(this);

                if (up.download()) // ���� �������� ���������� ����� https
                {
                    up.Show(); // ��������� ������ ���������� ���������
                }
            }*/
        }

        void ad_CheckForUpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
        {

        }

        void ad_CheckForUpdateCompleted(object sender, CheckForUpdateCompletedEventArgs e)
        {
            UpdateState = Constant.ApplicationUpdateState.StopCheckUpdate;
            //if (this.ShowFormUpdate)
            //{
            //    if (e.Error != null)
            //    {
            //        fUpdateApp.SetCancelUpdate("�� ������� ��������� ����������.", "������:" + e.Error.Message);
            //        return;
            //    }
            //    else if (e.Cancelled == true)
            //    {
            //        MessageBox.Show("�������� ���������� ��������");
            //        return;
            //    }
            //}
            if (e.Cancelled || (e.Error != null))
            {
                return;
            }
            if (e.UpdateAvailable)
            {
                //Stat.SaveLocalBeforeUpdate();
                //StatUsage.SaveLocalBeforeUpdate();
                UpdateState = Constant.ApplicationUpdateState.StartUpdateFiles;
                ApplicationDeployment.CurrentDeployment.UpdateAsync();
                //if (this.ShowFormUpdate)
                //{
                //    ShowUpdateAppForm();
                //}
                //else
                //{
                //    ToolTip.Show(Constant.TOOLTIP_TYPE_ID.CHECK_APP_UPDATE, "�������� ����������", "�������� ����� ������ �� \"���\"");
                //}
            }
            //else
            //{
            //    if (this.ShowFormUpdate)
            //    {
            //        fUpdateApp.SetCancelUpdate("��� ��������� ���������� �� �������.", "");
            //    }
            //}
        }

        public void BeginAppUpdate()
        {
            ApplicationDeployment.CurrentDeployment.UpdateAsync();
        }

        void ad_UpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
        {
            if ((e.State == DeploymentProgressState.DownloadingApplicationFiles) && (fUpdateApp.Visible))
            {
                fUpdateApp.SetProgress(e.BytesCompleted, e.BytesTotal);
            }
        }

        void ad_UpdateCompleted(object sender, AsyncCompletedEventArgs e)
        {
            UpdateState = Constant.ApplicationUpdateState.StopUpdateFiles;
            if (AppClosing)
            {
                if (fUpdateApp.Visible) fUpdateApp.Close();
                this.Close();
            }
            if (e.Cancelled)
            {
                //fUpdateApp.SetCancelUpdate("���������� ��������.", "");
                RemovePackageMetadata();
                return;
            }
            else if (e.Error != null)
            {
                File.WriteAllText(UserSmartPlusFolder + "\\UpdateError.txt", e.Error.Message);
                RemovePackageMetadata();
                //if (!PackageMetadataRemoved)
                //{
                //    RemovePackageMetadata();
                //    //UpdateState = Constant.ApplicationUpdateState.StartUpdateFiles;
                //    //ApplicationDeployment.CurrentDeployment.UpdateAsync();
                //}
                //else
                //{
                //    //fUpdateApp.SetCancelUpdate("�� ������� �������� ����������.", "������:" + e.Error.Message);
                //    File.WriteAllText(UserMiRFolder + "\\Error_" + DateTime.Now.Ticks.ToString() + ".txt", DateTime.Now.ToString() + "\n"  + PackageMetadataRemoved.ToString() + "\n" + e.Error.Message, Encoding.GetEncoding(1251));
                //    return;
                //}
            }
            else
            {
                CheckForShortcut(false);
                ToolTipMessage.Show(this, Constant.TOOLTIP_TYPE_ID.CHECK_APP_UPDATE, "���������� �����������!", "��� ���������� ���������� ������������� ����������.");
            }
            tmrUpdateApp.Stop();
            //fUpdateApp.SetRestart();
        }

        private void mMenuCheckUpdates_Click(object sender, EventArgs e)
        {
            //Stat.AddMessage(CollectorStatMessageId.UPDATE_APP, "������ �������� ���������� ���������");
            UpdateApplication(true);
        }

        public void ShowUpdateAppForm()
        {
            fUpdateApp.ShowUpdateForm();
        }
        #endregion

        #region File

        // Global Projects
        public void ShowGlobalProjForm()
        {
            if (fGlobProj == null) fGlobProj = new GlobalProjectsForm(this);
            if (fGlobProj.ShowDialog() == DialogResult.OK)
            {
                string selectedPath = fGlobProj.SelectedProjPath;
                string[] parseList = selectedPath.Split(new char[] { '\\' });
                if (parseList.Length > 0)
                {
                    if (!(ExistsProject(parseList[parseList.Length - 1])))
                    {
                        Project proj = new Project(selectedPath);
                        proj.Name = parseList[parseList.Length - 1];
                        
                        this.reLog.Clear();
                        ProjList.Add(proj);
                        indActiveProject = ProjList.Count - 1;
                        SetComponentProject();
                        canv.Disable();
                        mMenuProjOpen.Enabled = false;
                        pb.Visible = true;
                        stBar1.Text = "�������� ������� '" + parseList[parseList.Length - 1] + "'...";
                        worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_GLOBAL_PROJECT, proj);
                    }
                    else
                    {
                        MessageBox.Show(this, "������ ������ ��� ��������!" + Environment.NewLine,
                            "��������!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    }
                }
            }
            //if (DateTime.Now.Year > 2012 && DateTime.Now.Month > 4)
            //{
            //    AnnoyTimer.Start();
            //}
        }
        private void mMenuGlobalProjShow_Click(object sender, EventArgs e)
        {
            ShowGlobalProjForm();
        }

        // Open
        private void mMenuProjOpen_Click(object sender, EventArgs e) 
        {
            PreOpenProject();
        }
        public void OpenProject(string ProjectPath)
        {
            Project proj = new Project(ProjectPath);
            this.reLog.Clear();
            bool res = proj.LoadProject(ProjectPath);
            if (res)
            {
                if (!(ExistsProject(proj.Name)))
                {
                    ProjList.Add(proj);
                    indActiveProject = ProjList.Count - 1;
                    SetComponentProject();
                    canv.Disable();
                    mMenuProjOpen.Enabled = false;
                    pb.Visible = true;
                    stBar1.Text = "�������� ������� '" + proj.Name + "'...";
                    worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_PROJECT, proj);
                }
                else
                {
                    MessageBox.Show(this, "������ � ����� ��������� ��� ��������!" + Environment.NewLine + "������� ������ ��� �������",
                        "��������!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    proj = null;
                }
            }
        }
        private void PreOpenProject() 
        {
#if DEBUG
            bool res = false;
            FolderBrowserDialog fdlg = new FolderBrowserDialog();
            fdlg.Description = "�������� ������ ...";
            if (fdlg.ShowDialog() == DialogResult.OK) 
            {
                OpenProject(fdlg.SelectedPath);
            }
            fdlg.Dispose();
#endif
        }

        // open image
        private void mMenuOpenImage_Click(object sender, EventArgs e) 
        {
            OpenFileDialog dlg = new OpenFileDialog();
            Image img;
            if (indActiveProject >= 0)
            {
                dlg.Title = "�������� ���� � ������������";
                dlg.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
                dlg.Multiselect = false;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    img = new Image(dlg.FileName);
                    img.Name = "�����������(" + System.IO.Path.GetFileName(dlg.FileName) + ")";
                    canv.AddNewImage(img);
                    //((ProjList[indActiveProject]).OilFields[0]).AddImage(img);
                    //canv.LoadLayers();
                    canv.DrawLayerList();
                }
            }
            else {
                MessageBox.Show("���������� ������� ������.", "��������!");
            }
        }

        // EXPORT CONTOURS AREA
        private void mmExportAreasContours_Click(object sender, EventArgs e)
        {
            if (indActiveProject > -1)
            {
                Project proj = ProjList[indActiveProject];
                bool check = false;
                C2DLayer layer;
#if !DEBUG
                if (canv.selLayerList.Count > 0)
                {
                    layer = (C2DLayer)canv.selLayerList[0];
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                    {
                        if((layer.ObjectsList.Count > 0) &&
                            (((Contour)layer.ObjectsList[0])._ContourType < 0) &&
                            (((Contour)layer.ObjectsList[0])._ContourType > -4))
                        {
                            check = true;
                        }
                    }
                }
#else
                check = true;
#endif
                if (check)
                {
                    FolderBrowserDialog dlg = new FolderBrowserDialog();
                    dlg.Description = "�������� ����� ��� �������� ��������:";
                    dlg.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    dlg.ShowNewFolderButton = true;
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        proj.tempStr = dlg.SelectedPath;
                        worker.Run(Constant.BG_WORKER_WORK_TYPE.EXPORT_AREAS_CONTOURS, canv);
                    }
                }
                else
                {
                    MessageBox.Show("�������� �������������, ���� ��� ������", "��������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }

        // CLOSE
        private void mMenuClose_Click(object sender, EventArgs e) 
        {
            int ind;
            ClearComponentProject();
            if (indActiveProject > 0)
            {
                ind = indActiveProject - 1;
            }
            else if (ProjList.Count > 1)
            {
                ind = indActiveProject;
            }
            else ind = -1;
            RemoveMMenuProject(ind);
            ProjList[indActiveProject].Close();
            ProjList.RemoveAt(indActiveProject);
            indActiveProject = ind;
            mMenuProjOpen.Enabled = true;
            if (ProjList.Count == 0) 
            {
                mMenuClose.Enabled = false;
                mMenuOpenImage.Enabled = false;
                mMenuExport.Enabled = false;
                tbFinderEx.Enabled = false;
            }
            tsbUpdates.Visible = false;
            this.ToolTip.Hide();
            canv.DrawLayerList();
            corScheme.ClearAll();
            planeWell.ClearAll();
            finder.Clear();
            dActiveWindow("pInspector");
        }

        //Save
        private void mMenuSave_Click(object sender, EventArgs e) 
        {
            FolderBrowserDialog fdlg = new FolderBrowserDialog();
            fdlg.Description = "�������� ����� �������� ��������...";
            fdlg.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (fdlg.ShowDialog() == DialogResult.OK) 
            {
                Project proj = ProjList[indActiveProject];
                proj.tempStr = proj.path;
                proj.path = fdlg.SelectedPath + "\\" + proj.Name;
                worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_PROJECT, proj);
            }
            fdlg.Dispose();
        }

        // Exit
        private void mMenuExit_Click(object sender, EventArgs e) 
        {
            this.Close();
        }
        #endregion

        #region Flooding
        // ����������
        private void mMenuLoadCompensation_Click(object sender, EventArgs e)
        {
            if ((ProjList.Count > 0) && (indActiveProject < ProjList.Count))
            {
                Project proj = ProjList[indActiveProject];
                proj.LoadAreasCompFromFile();
                canv.DrawLayerList();
            }
        }
        private void tsbDrawAreasOverlay_Click(object sender, EventArgs e)
        {
            if ((ProjList.Count > 0) && (indActiveProject < ProjList.Count))
            {
                Project proj = ProjList[indActiveProject];
                proj.SetAreasOverlay(tsbDrawAreasOverlay.Checked);
                canv.DrawLayerList();
            }
        }
        private void tsbAreasParamsByDate_Click(object sender, EventArgs e)
        {
            if ((indActiveProject < ProjList.Count) && (ProjList.Count > 0))
            {
                bool calc = true;
                if (dgAreas.DataLoaded)
                {
                    if (MessageBox.Show("��������� ����� ���������� ��� ���������. ���������� �����?", "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        calc = false;
                    }
                }
                if (calc && indActiveProject > -1)
                {
                    Project proj = ProjList[indActiveProject];
                    AreaParamsDatesForm form = new AreaParamsDatesForm(this, proj.maxMerDate);
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        proj.tempStr = string.Format("{0:dd.MM.yyyy};{1:dd.MM.yyyy}", form.SelectDateStart, form.SelectDateEnd);
                        dgAreas.ClearDataSet();
                        worker.Run(Constant.BG_WORKER_WORK_TYPE.FILL_AREAS_PARAMS_TO_TABLE_BY_DATE, proj);
                    }
                }
            }
        }
        #endregion

        #region Loading
        // Loading Data
        private void LoadWorkLayersFromWorkFolder()
        {
            int i, j, k, m, type, x, y;
            bool areaEdit = false, cntrEdit = false;
            TreeNode pNode, tn, tnNGDU, tnOilField, tn2;
            OilField of;
            C2DLayer layer;
            WorkObject job;
            Contour cntr, cntrSrc;
            bool clear = false, find;
            int oilobjCode = -1;
            pNode = canv.twWork.Nodes[0];
            Project proj = ProjList[indActiveProject];
            for (i = 0; i < pNode.Nodes.Count; i++)
            {
                tnNGDU = pNode.Nodes[i];
                for (j = 0; j < tnNGDU.Nodes.Count; j++)
                {
                    if (tnNGDU.Text != "����� ������")
                    {
                        tnOilField = tnNGDU.Nodes[j];
                        of = null;
                        find = false;
                        for (x = 0; x < proj.OilFields.Count; x++)
                        {
                            if (tnOilField.Text.Equals((proj.OilFields[x]).Name, StringComparison.CurrentCultureIgnoreCase))
                            {
                                of = proj.OilFields[x];
                                find = true;
                                break;
                            }
                        }
                        if (!find)
                        {
                            MessageBox.Show("�� ������� ����" + tnOilField.Text);
                        }

                    }
                    else
                    {
                        of = proj.OilFields[0];
                        tnOilField = tnNGDU;
                    }

                    if (of != null)
                    {
                        clear = true;
                        oilobjCode = -1;
                        areaEdit = false;
                        cntrEdit = false;
                        for (k = 0; k < tnOilField.Nodes.Count; k++)
                        {
                            tn = tnOilField.Nodes[k];
                            if (tn.Text == "�������") continue;
                            if (tn.Text == "�����") continue;
                            if (tn.Text == "�������� �������")
                            {
                                if (tn.Nodes.Count == 0) continue;
                                tn = tn.Nodes[0];
                            }

                            if (tn.Text == "������")
                            {
                                if (tn.Nodes.Count == 0) continue;
                                if (clear)
                                {
                                    of.Areas.Clear();
                                    clear = false;
                                }
                                for (m = 0; m < tn.Nodes.Count; m++)
                                {
                                    tn2 = tn.Nodes[m];
                                    if ((tn2.Tag != null) && ((C2DLayer)tn2.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                                    {
                                        layer = (C2DLayer)tn2.Tag;
                                        if ((of == null) && (layer.oilfield != null)) of = layer.oilfield;
                                        cntrSrc = (Contour)layer.ObjectsList[0];

                                        string name = cntrSrc.Name;
                                        int pos1, pos2;
                                        pos1 = name.IndexOf("(");
                                        Area area;
                                        if (pos1 > 0)
                                        {
                                            pos2 = name.IndexOf(",", pos1);
                                            if (pos2 == -1) pos2 = name.IndexOf(")", pos1);
                                            name = name.Substring(pos1 + 1, pos2 - pos1 - 1);
                                            int ind = Convert.ToInt32(name);
                                            cntrSrc.Code = ind;
                                            cntrSrc._ContourType = -4;
                                            if ((oilobjCode == -1) && (cntrSrc.StratumCode > -1))
                                            {
                                                oilobjCode = cntrSrc.StratumCode;
                                            }
                                            if (cntrSrc.StratumCode == -1)
                                            {
                                                cntrSrc.StratumCode = oilobjCode;
                                            }

                                            area = new Area();
                                            area.contour = new Contour(cntrSrc);
                                            area.contour._ContourType = -4;
                                            //area.contour._FillBrush = 33;
                                            area.contour.StratumCode = 0;
                                            area.Name = area.contour.Name;
                                            area.OilFieldIndex = of.Index;
                                            area.Index = of.Areas.Count;
                                            of.Areas.Add(area);
                                            areaEdit = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if ((tn.Tag != null) && ((C2DLayer)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                                {
                                    layer = (C2DLayer)tn.Tag;
                                    if ((of == null) && (layer.oilfield != null)) of = layer.oilfield;
                                    cntrSrc = (Contour)layer.ObjectsList[0];

                                    type = 0;

                                    if (tnOilField.Text != "����� ������")
                                    {
                                        bool repl = false;
                                        for (x = 0; x < of.Contours.Count; x++)
                                        {
                                            cntr = (Contour)of.Contours[x];
                                            if (!repl)
                                            {
                                                if (cntr._ContourType == -3)
                                                {
                                                    while (cntr._points.Count > 0) cntr._points.RemoveAt(0);

                                                    for (y = 0; y < cntrSrc._points.Count; y++)
                                                    {
                                                        cntr.Add(cntrSrc._points[y].X, cntrSrc._points[y].Y);
                                                    }
                                                    cntr.Name = cntrSrc.Name;
                                                    cntr._OilFieldCode = of.OilFieldCode;
                                                    cntr._FillBrush = 11;
                                                    cntrEdit = true;
                                                    repl = true;
                                                }
                                            }
                                            else if (cntr._ContourType == -3)
                                            {
                                                of.Contours.RemoveAt(x);
                                                break;
                                            }
                                        }
                                        if (!repl)
                                        {
                                            cntr = new Contour(cntrSrc);
                                            cntr._ContourType = -3;
                                            cntr.Name = cntrSrc.Name;
                                            cntr._OilFieldCode = of.OilFieldCode;
                                            cntr._FillBrush = 11;
                                            cntrEdit = true;
                                            of.Contours.Add(cntr);
                                        }
                                    }
                                    else
                                    {
                                        bool repl = false;
                                        for (x = 0; x < of.Contours.Count; x++)
                                        {
                                            cntr = (Contour)of.Contours[x];
                                            string cntrName = cntr.Name.Replace("\"", "");

                                            if (cntrName == cntrSrc.Name)
                                            {
                                                while (cntr._points.Count > 0) cntr._points.RemoveAt(0);

                                                for (y = 0; y < cntrSrc._points.Count; y++)
                                                {
                                                    cntr.Add(cntrSrc._points[y].X, cntrSrc._points[y].Y);
                                                }
                                                cntr.Name = cntrSrc.Name;
                                                cntrEdit = true;
                                                //cntr.GetMedian();
                                                repl = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        for (x = 0; x < of.Areas.Count; x++) ((Area)of.Areas[x]).Code = 0;
                        job.DataObject = of;
                        job.ObjectIndex = -1;
                        if (areaEdit)
                        {
                            job.WorkType = Constant.BG_WORKER_WORK_TYPE.RESET_AREAS_ALL_PARAMS;
                            worker.Queue.Add(job);
                        }
                        if (cntrEdit)
                        {
                            job.WorkType = Constant.BG_WORKER_WORK_TYPE.SAVE_OF_CONTOUR_TO_CACHE;
                            worker.Queue.Add(job);
                        }
                    }
                    else
                    {
#if DEBUG
                        reLog.AppendText(tnOilField.Text.ToUpper() + Environment.NewLine);
#endif
                    }
                }
            }
            if (worker.Queue.Count > 0) worker.Run();
            canv.LoadLayers();
            canv.SortInspectorByNGDU();
        }
        private void mMenuLoadContoursAreasFromWF_Click(object sender, EventArgs e)
        {
            // �������� ����� � �������� ������������� �� ��� �����
            LoadWorkLayersFromWorkFolder();
        }
        private void mMenuLoadLeakingFromFile_Click(object sender, EventArgs e)
        {
            if (indActiveProject < ProjList.Count)
            {
                Project proj = ProjList[indActiveProject];
                int count = 0;
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Title = "�������� ���� c ������ ��� � ��������������� ��";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                dlg.Filter = "�������� Excel|*.xls;*.xlsx;*.xlxb;*.xlsm";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    proj.tempStr = dlg.FileName;
                    count++;
                }
                if (count == 1) worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_WELL_STATE_RESEARCH, proj);
            }
        }
        private void mMenuLoadTable81FromFile_Click(object sender, EventArgs e)
        {
            if (indActiveProject < ProjList.Count)
            {
                Project proj = ProjList[indActiveProject];
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Title = "�������� ���� � ����������� ������ 8.1";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                dlg.Multiselect = false;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    proj.tempStr = dlg.FileName;
                    worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_TABLES_81, proj);
                }
            }
        }
        private void mMenuLoadBizPlanFromFile_Click(object sender, EventArgs e)
        {
            if (indActiveProject < ProjList.Count)
            {
                Project proj = ProjList[indActiveProject];
                int count = 0;
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Title = "�������� ���� c ������ ������-�����";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                dlg.Filter = "�������� Excel|*.xls;*.xlsx;*.xlxb;*.xlsm";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    proj.tempStr = dlg.FileName;
                    count++;
                }
                if (count == 1) worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OILFIELD_BIZPLAN, proj);
            }
        }
        // ����
        private void mMenuCoreLoad_Click(object sender, EventArgs e)
        {
            if (indActiveProject < ProjList.Count)
            {
                Project proj = ProjList[indActiveProject];
                OpenFileDialog dlg = new OpenFileDialog();

                dlg.Title = "�������� ���� � ������� ����� �� ��������������";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                dlg.Multiselect = false;
                dlg.Filter = "CSV ����� | *.csv";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    proj.tempStr = dlg.FileName;
                    worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_CORE_FROM_FILE, proj);
                }
            }
        }
        private void mMenuCoreTestLoad_Click(object sender, EventArgs e)
        {
            if (indActiveProject < ProjList.Count)
            {
                Project proj = ProjList[indActiveProject];
                OpenFileDialog dlg = new OpenFileDialog();

                dlg.Title = "�������� ���� � ������� ������������ ����� �� ��������������";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                dlg.Multiselect = false;
                dlg.Filter = "CSV ����� | *.csv";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    proj.tempStr = dlg.FileName;
                    worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_CORE_TEST_FROM_FILE, proj);
                }
            }
        }
        #endregion

        #region SERVICE
        // ����� 
        private void mmFindSkv_Click(object sender, EventArgs e)
        {
            ShowFindToolTip();
            tbFinderEx.TextBox.SelectAll();
            tbFinderEx.TextBox.Focus();
            if ((finder != null) && (!finder.Visible) && (finder.Count > 0))
            {
                finder.ShowUpdate();
            }
        }
        // ������
        private void mMenuSaveCoef_Click(object sender, EventArgs e) 
        {
            if (MessageBox.Show("��������� ���������� ������������ ��������� �������������?", "��������!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DictionaryCollection dictList = GetLastProject().DictList;
                var coefDict = (OilFieldCoefDictionary)dictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT);
                string path = string.Empty;
                if (Directory.Exists(dictList.DictionaryPath)) path = dictList.DictionaryPath;
                else path = dictList.DefaultPath;
                coefDict.WriteToFile(path + "\\" + coefDict.FileName);
            }
        }
        private void mMenuLoadOilAreas_Click(object sender, EventArgs e)
        {
            if (indActiveProject < ProjList.Count)
            {
                Project proj = ProjList[indActiveProject];
                OilField of;
                Well w;
                int i, j, ind;
                ArrayList list = new ArrayList();
                for (i = 0; i < proj.OilFields.Count; i++)
                {
                    of = proj.OilFields[i];
                    list.Clear();
                    for (j = 0; j < of.Wells.Count; j++)
                    {
                        w = of.Wells[j];

                        if (w.OilFieldAreaCode > -1)
                        {
                            ind = list.IndexOf(w.OilFieldAreaCode);
                            if(ind == -1)
                            {
                                ind = list.Count;
                                list.Add(w.OilFieldAreaCode);
                            }
                        }
                     }
                 }
            }
        }
        // ��������� ����������
        private void mMenuAppSettings_Click(object sender, EventArgs e)
        {
            if (fAppSettings.ShowDialog() == DialogResult.OK)
            {
                if (fAppSettings.LiquidInCubeChanged)
                {
                    canv.UpdateBubbleMap();
                    chart.ReLoadByActiveScheme();
                    if (dgMer != null)
                    {
                        dgMer.DataLoaded = false;
                        dgMer.UpdateBySourceWell();
                    }
                }
                if (fAppSettings.OilObjColorsChanged)
                {
                    worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_APP_SETTINGS, fAppSettings.AppSett);
                    if (canv.MapProject != null)
                    {
                        chart.UpdateOilObjColors();
                        canv.UpdateCntrBrushByOilObj();
                        canv.DrawLayerList();
                    }
                }
                if (!AppSettings.ShowAllOilfieldAreas)
                {
                    canv.twLayers.UpdateAreasNodeVisible();
                    canv.twActiveOilField.UpdateRecursiveTreeView();
                }
                if (fAppSettings.AttachAreasToFilterChanged)
                {
                    canv.SetAreasVisibleByFilter();
                    fAppSettings.AttachAreasToFilterChanged = false;
                }
            }
        }
        #endregion

        #region REPORTS
        // ����� �� ��������
        private void mMenuReportChess_Click(object sender, EventArgs e)
        {
            if (indActiveProject > -1 && ProjList.Count > 0)
            {
                ReportChessDateForm form = new ReportChessDateForm(this);
                if (form.ShowDialog() == DialogResult.OK)
                {
                    (ProjList[indActiveProject]).tempStr = string.Format("{0:dd.MM.yyyy};{1:dd.MM.yyyy};{2:dd.MM.yyyy};", form.SelectDate1, form.SelectDate2, form.SelectDate3);
                    worker.Run(Constant.BG_WORKER_WORK_TYPE.REPORT_CHESS, ProjList[indActiveProject]);
                }
            }
        }
        
        // ������ ���������� 
        private void mMenuReportFloodingAnalysis_Click(object sender, EventArgs e)
        {
            worker.Run(Constant.BG_WORKER_WORK_TYPE.REPORT_FLOODING_ANALYSIS, ProjList[indActiveProject]);
        }

        // ����� �� ������
        private void mMenuReportMerProduction_Click(object sender, EventArgs e)
        {
            var project = GetLastProject();
            if (project != null)
            {
                worker.Run(Constant.BG_WORKER_WORK_TYPE.REPORT_MER_PRODUCTION, project);
            }
        }

        // ������� �� ������� ����������
        private void mMenuReportAreaPredict_Click(object sender, EventArgs e)
        {
            if (canv != null && canv.MapProject != null)
            {
                ReportPredictAreaForm form = new ReportPredictAreaForm(this);
                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    int count = 1;
                    ProjList[indActiveProject].tempStr = form.templatePath;
                    if (form.TRFilePath.Length > 0) 
                    {
                        count++;
                        ProjList[indActiveProject].tempStr += ";" + form.TRFilePath;
                    }
                    if (count > 0) worker.Run(Constant.BG_WORKER_WORK_TYPE.REPORT_AREAS_PREDICT, canv.MapProject);
                }
                form.Dispose();
            }
        }

        // ��������� �� �������
        private void mMenuReportDepositOIZ_Click(object sender, EventArgs e)
        {
            Project proj = GetLastProject();
            if (proj == null) return;
            worker.Run(Constant.BG_WORKER_WORK_TYPE.DEPOSIT_OIZ_REPORT, proj);
        }

        private void mMenuCreateGtmList_Click(object sender, EventArgs e)
        {
            if (GetLastProject() != null)
            {
                Dialogs.CreateGTMLayersForm form = new Dialogs.CreateGTMLayersForm(this);
                form.ShowDialog();
            }
        }
        private void mMenuReportCore_Click(object sender, EventArgs e)
        {
            Project proj = GetLastProject();
            if (proj == null) return;
            worker.Run(Constant.BG_WORKER_WORK_TYPE.REPORT_WELL_CORE, proj);
        }
        private void mMenuAutoMatchGTM_Click(object sender, EventArgs e)
        {
            if (GetLastProject() != null)
            {
                Dialogs.GTMAutoMatchForm form = new Dialogs.GTMAutoMatchForm(this);
                if (fAppSettings != null)
                {
                    fAppSettings.ReadGtmAutoMatchFormSettings(form);
                }
                form.Show();
            }
        }
        #endregion

        #region PROJECT SELECT
        // other
        public Project GetLastProject()
        {
            if ((indActiveProject > -1) && (indActiveProject < ProjList.Count))
            {
                return ProjList[indActiveProject];
            }
            return null;
        }
        public bool ExistsProject(string ProjectName) 
        {
            bool retValue = false;
            for (int i = 0; i < ProjList.Count; i++) 
            {
                if ((ProjList[i]).Name == ProjectName) 
                {
                    retValue = true;
                    break;
                }
            }
            return retValue;
        }
        public void AddMMenuProject(string ProjectName) 
        {
            for (int i = 0; i < mMenuProj.DropDownItems.Count; i++) 
            {
                ((ToolStripMenuItem)mMenuProj.DropDownItems[i]).Checked = false;
            }
            ToolStripMenuItem newItem = (ToolStripMenuItem)mMenuProj.DropDownItems.Add(ProjectName);
            newItem.Click += new EventHandler(newItem_Click);
            newItem.Checked = true;
        }
        public void ReNameMMenuProject(string ProjectName) {
            ToolStripMenuItem item = (ToolStripMenuItem)mMenuProj.DropDownItems[indActiveProject];
            item.Text = ProjectName;
        }
        public void RemoveMMenuProject(int newIndex) 
        {
            mMenuProj.DropDownItems.RemoveAt(indActiveProject);
            if(newIndex >= 0) ((ToolStripMenuItem)mMenuProj.DropDownItems[newIndex]).Checked = true;
        }

        private void newItem_Click(object sender, EventArgs e) {
            ToolStripMenuItem newItem = sender as ToolStripMenuItem;
            indActiveProject = mMenuProj.DropDownItems.IndexOf(newItem);
            ChangeActiveProject();
        }

        private void ChangeActiveProject() 
        {
            for (int i = 0; i < mMenuProj.DropDownItems.Count; i++) 
            {
                    ((ToolStripMenuItem)mMenuProj.DropDownItems[i]).Checked = false;
            }
            ((ToolStripMenuItem)mMenuProj.DropDownItems[indActiveProject]).Checked = true;
            SetComponentProject();
            canv.DrawLayerList();
        }
        private void SetComponentProject() 
        {
            canv.SetProject(ProjList[indActiveProject]);
            dgMer.SetProject(ProjList[indActiveProject]);
            dgGIS.SetProject(ProjList[indActiveProject]);
            dgAreas.SetProject(ProjList[indActiveProject]);
            dgCore.SetProject(ProjList[indActiveProject]);
            dgCoreTest.SetProject(ProjList[indActiveProject]);
            chart.SetProject(ProjList[indActiveProject]);
            dgChess.SetProject(ProjList[indActiveProject]);
            filterOilObj.SetProject(ProjList[indActiveProject]);
            fBubbleSet.SetProject(ProjList[indActiveProject]);
            WDFilterForm.SetProject(ProjList[indActiveProject]);
            corScheme.SetProject(ProjList[indActiveProject]);
            planeWell.SetProject(ProjList[indActiveProject]);
        }
        private void ClearComponentProject()
        {
            canv.ClearProject();
            dgMer.ClearProject();
            dgGIS.ClearProject();
            dgChess.ClearProject();
            dgAreas.ClearProject();
            dgCore.ClearProject();
            dgCoreTest.ClearProject();
            chart.ClearProject();
            filterOilObj.ClearProject();
            fBubbleSet.ClearProject();
            WLSettings.Clear();
            WDFilterForm.ClearProject();
            if(discuss != null) discuss.Clear(true);
            if(fileServer != null) fileServer.Clear(true);
        }

        #endregion

        #region ����
        private void mMenuResetWindows_Click(object sender, EventArgs e)
        {
            UserDockManagerSettings = "udm.bin";
            if ((uDockManager != null) && File.Exists(UserDockManagerSettings))
            {
                uDockManager.PaneHidden -= uDockManager_PaneHidden;
                uDockManager.PaneDisplayed -= uDockManager_PaneDisplayed;
                uDockManager.PaneActivate -= uDockManager_PaneActivate;
                uDockManager.LoadFromBinary(UserDockManagerSettings);
                InitializeWindows();
            }
        }
        #endregion

        #region HELP
        // C������
        private void mMenuAbout_Click(object sender, EventArgs e)
        {
            formAbout.ShowDialog();
        }
        private void mMenuSendSupport_Click(object sender, EventArgs e)
        {
            string AssemblyVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string AppName = string.Empty;

            AppName = "���(SmartPlus)";

            string Body = "������ ������������� �� " + AppName +".\r\n������� ���� ��������:\r\n\r\n\r\n\r\n\r\n______________________________________________________________\r\n������� �� ������ �� ��������� ������������ ��������� " + AppName;
            Body += "\r\n������ ������ �������� ������ ����������, ����������� ������������� ��� ���������� ����� ��������\r\n";
            Body += AppName + " (version: " + AssemblyVersion + "), Windows version: " + Environment.OSVersion.ToString();

            Mapi mapi = new Mapi();
            mapi.AddRecipientTo("support@ufntc.ru");
            mapi.SendMailPopup("��������� �� ������. �� " + AppName, Body);
        }
        private void mMenuWatsNew_Click(object sender, EventArgs e)
        {
            formWatsNew.ShowDialog();
        }
        #endregion

        #region Mouse Wheel
        void fMain_MouseWheel(object sender, MouseEventArgs e)
        {
            if ((tbFinderEx.TextBox.Focused) && (finder != null) && (finder.Visible))
            {
                finder.box_MouseWheel(e.Delta);
            }
        }
        #endregion

        #region Form Functions

        public void WriteLog(string text)
        {
            logList.Add(DateTime.Now.ToString() + ";" + text);
        }
        public void FlushLog()
        {
            if ((logList.Count > 0) && (UserLogFile != null))
            {
                StreamWriter file = new StreamWriter(UserLogFile, true, System.Text.Encoding.GetEncoding(1251));
                for (int i = 0; i < logList.Count; i++)
                {
                    file.WriteLine((string)logList[i]);
                }
                logList.Clear();
                file.Close();
            }
        }
        public void SetStatus(string StatusText)
        {
            if (!worker.IsBusy)
            {
                StatusText = StatusText.Replace('\n', ' ');
                if (stBar1.Text != StatusText) stBar1.Text = StatusText;
            }
        }
        public void SetProgress(int value, int max, string workName, string element)
        {
            int progress;
            if (pb.Visible)
            {
                progress = (int)((float)value * pb.Maximum / (float)max);
                pb.Value = progress;
                SetStatus(workName);
            }
            if (fProgress.Visible)
            {
                fProgress.lElement.Text = element;
                progress = (int)((float)value * fProgress.pb.Maximum / (float)max);
                fProgress.lMain.Text = string.Format("{0} ({1:0} %)", workName, progress);
                fProgress.pb.Value = progress;
                if (progress % 10 == 0) Application.DoEvents();
            }
        }
        void fMain_LocationChanged(object sender, EventArgs e)
        {
            Point pt = Point.Empty;
            int dx = (this.Size.Width - this.ClientSize.Width) / 2;
            pt.X = this.Location.X + dx + 5;
            pt.Y = this.Location.Y + this.Size.Height - ToolTip.Size.Height - dx - this.stBar1.Height - 10;
            ToolTip.Location = pt;
            if (finder != null)
            {
                pt = PointToScreen(tbFinderEx.Bounds.Location);
                pt.Y += canv.mainForm.tbFinderEx.Bounds.Height + 2;
                finder.Location = pt;
                //finder.Hide();
            }
            if (ReportBuilder != null) ReportBuilder.UserDialogShowing = false;
            if (ToolTipFind != null) ToolTipFind.Dispose();
        }
        void fMain_Shown(object sender, EventArgs e)
        {
            dChartSetWindowText("�������");
            if(!ToolTip.Visible) ShowGlobalProjForm();
            //UpdateApplication(false);
        }

        // CLOSING
        private void fMain_FormClosing(object sender, FormClosingEventArgs e) 
        {
            AppClosing = true;
            CancelAppUpdate();
            finder.Hide();
            finder.Clear();
            if(dataUpdater != null) dataUpdater.CancelUpdate();
            if (!AppRestart)
            {

                if (canv.LayerWorkListChanged && canv.LayerWorkList.Count > 0)
                {
                    if (MessageBox.Show("� ������� ����� ���� ������������� ������.\n��������� ������?", "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        fProgress.Show();
                        fProgress.Start("���������� ������ ������� �����...");
                        worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYERS);
                    }
                    else
                        canv.LayerWorkListChanged = false;
                }

                int i = 0, j = 0;
                if ((worker.IsBusy) && (worker.GetWorkType() != (int)Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYERS))
                {
                    worker.CancelAsync();
                }

                while (worker.IsBusy)
                {
                    Thread.Sleep(10);
                    if (!canv.MessageShown)
                    {
                        i++;
                        j++;
                    }
                    if ((i == 1000) || (j == 1500))
                    {

                        if ((worker.GetWorkType() == (int)Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYERS) && 
                            (MessageBox.Show("���������� ������� ����� ����������� ��������������� �����.��������?", "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                        {
                            worker.CancelAsync();
                        }
                        else
                        {
                            if (MessageBox.Show("������� �������� ����������� ��������������� �����.��������?", "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                worker.CancelAsync();
                            }
                            i = 0;
                            j = 0;
                        }
                    }
                    Application.DoEvents();
                }
                if ((!worker.IsBusy) && (canv.LayerWorkListChanged && canv.LayerWorkList.Count > 0))
                    e.Cancel = true;
            }

            if (UpdateState == Constant.ApplicationUpdateState.StartUpdateFiles)
            {
                if (fUpdateApp.ShowDownloadUpdate() == DialogResult.Abort)
                {
                    if(ApplicationDeployment.IsNetworkDeployed)
                    {
                        e.Cancel = true;
                        ApplicationDeployment.CurrentDeployment.UpdateAsyncCancel();
                        MessageBox.Show("����������� ������ ��������� ���������� ����������. ���������� ��������� �������������.", "��������!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

#if DEBUG
            uDockManager.SaveAsBinary(UserDockManagerSettings);
#endif
        }
        
        // ����
        public void coreSetPaneTitle(string Title)
        {
            if (corePane != null) corePane.Text = Title;
        }
        public void coreTestSetPaneTitle(string Title)
        {
            if (coreTestPane != null) coreTestPane.Text = Title;
        }

        // ���
        public void dGisSelectRow(double DepthAbs)
        {
            dgGIS.SelectRowByDepth(DepthAbs);
        }

        //����
        public void dCoreTestSelectRow(double DepthMD, bool SetFirstDisplayed)
        {
            dgCoreTest.SelectRowByDepth(DepthMD, SetFirstDisplayed);
        }

        // ���.�����
        public void EnableMarkers()
        {
            if (!tsbVisibleAreas.Checked) tsbVisibleAreas.PerformClick();
        }
        public void corSchToolBarEnable(bool Enable)
        {
            tsbCS_DepthAbsMd.Enabled = Enable;
            tsbCS_ShowDuplicate.Enabled = Enable;
            tsbCorSchShowHead.Enabled = Enable;
            tsbShowDepth.Enabled = Enable;
            tsbShowGis.Enabled = Enable;
            tsbShowOilObjName.Enabled = Enable;
            tsbShowMarker.Enabled = Enable;
            tsbMarkerEditMode.Enabled = Enable;
            tsbCorSchSettings.Enabled = Enable;
            COR_SCH_SnapShoot.Enabled = Enable;
        }
        public void corSchLoadWell(Well w)
        {
            C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PROFILE, 0);
            Profile prfl = new Profile();
            prfl.AddWell(w);
            layer.Add(prfl);
            corScheme.SetProfileSource(layer);
            corScheme.LoadProfileData(null, null);
        }
        public void corSchShowPane()
        {
            if (corScheme != null) corScheme.ShowPane();
        }
        public void corSchLoadProfile(C2DLayer layer)
        {
            corSchShowPane();
            if ((worker.IsBusy) && (worker.GetWorkType() == (int)Constant.BG_WORKER_WORK_TYPE.LOAD_CORSCHEME_BY_PROFILE) && 
                ((corScheme.ObjSrc.TypeID != Constant.BASE_OBJ_TYPES_ID.LAYER) || (layer != corScheme.ObjSrc)))
            {
                worker.CancelAsync();
            }
            if (corScheme.SetProfileSource(layer))
            {
                if (layer.ObjectsList.Count > 0)
                {
                    int ofIndex = -1;
                    Profile prfl = (Profile)layer.ObjectsList[0];
                    for (int i = 0; i < prfl.Count; i++)
                    {
                        if (ofIndex == -1) ofIndex = prfl[i].OilFieldIndex;
                        if (ofIndex != prfl[i].OilFieldIndex)
                        {
                            ofIndex = -1;
                            break;
                        }
                    }
                    if (ofIndex != -1)
                    {
                        canv.SetActiveOilField(ofIndex);
                    }
                }
                corScheme.StartDrawWaitIcon();
                worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_CORSCHEME_BY_PROFILE, corScheme);
                corScheme.Focus();
                canv.DrawLayerList();
            }
        }
        public void corSchSaveProfileLayer()
        {
            if (corScheme.ObjSrc != null)
            {
                canv.WorkLayerActive = (C2DLayer)corScheme.ObjSrc;
                worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER, canv);
            }
        }
        public void corSchSaveMarkerLayer()
        {
            if (corScheme.currMrkLayer != null)
            {
                canv.WorkLayerActive = corScheme.currMrkLayer;
                worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER, canv);
            }
        }
        public void corSchSetMarkerEditMode(bool MarkerEditMode)
        {
            tsbMarkerEditMode.Checked = MarkerEditMode;
            corScheme.SetMarkerEditMode(MarkerEditMode);
        }
        public void corSchSelectProfileNode()
        {
            if ((corScheme.ObjSrc != null) && (corScheme.ObjSrc.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) && 
                (((C2DLayer)corScheme.ObjSrc).node != null))
            {
                canv.twWork.SelectedNode = ((C2DLayer)corScheme.ObjSrc).node;
            }
        }
        public void corSchAddWell(int wellIndex)
        {
            int index = -1;
            if (canv.selWellList.Count > 0)
            {
                index = corScheme.AddWellToProfile((Well)canv.selWellList[wellIndex]);
                canv.DrawLayerList();

                if (worker.IsBusy)
                {
                    WorkObject job;
                    job.WorkType = Constant.BG_WORKER_WORK_TYPE.ADD_WELL_TO_PROFILE;
                    job.DataObject = corScheme;
                    job.ObjectIndex = wellIndex;
                    worker.Queue.Add(job);
                }
                else
                    worker.Run(Constant.BG_WORKER_WORK_TYPE.ADD_WELL_TO_PROFILE, corScheme, wellIndex);
            }
        }
        public void corSchUpdateWindow(bool OnlyDraw)
        {
            corScheme.UpdateWindowTitle();
            corScheme.DrawData();
            if (!OnlyDraw)
            {
                corScheme.StopDrawWaitIcon();
                canv.DrawLayerList();
            }
        }
        public void corSchMedianDepth()
        {
            corScheme.SetMedianDepth(corScheme.GetMedianDepth());
        }
        public void corSchSetCurrentMarker(C2DLayer mrklayer)
        {
            corScheme.SetCurrentMarker(mrklayer);
        }
        public void corSchRemoveMarkers(ArrayList MarkerList)
        {
            corScheme.RemoveMarkerList(MarkerList);
        }
        public void CorSchReloadMarkers()
        {
            corScheme.ReloadMarkers();
        }

        public void corSchClearAll()
        {
            corScheme.ClearAll();
        }

        // �������
        public void planeSelectGISdepth(float depthMD)
        {
            planeWell.SetSelectedData(Constant.Plane.ObjectType.GIS, depthMD);
        }
        public void planeSelectCoreTestdepth(float depthMD)
        {
            planeWell.SetSelectedData(Constant.Plane.ObjectType.CORE, depthMD);
        }
        public void planeShowPane()
        {
            if (planeWell != null) planeWell.ShowPane();
        }
        public void planeLoadWell(Well w)
        {
            if ((planeWorker.IsBusy) && (planeWorker.GetWorkType() == (int)Constant.BG_WORKER_WORK_TYPE.LOAD_PLANE_BY_WELL) && 
                (w != planeWell.ObjSrc))
            {
                planeWorker.CancelAsync();
            }
            if (planeWell.SetWellSource(w))
            {
                planeWell.StartDrawWaitIcon();
                planeWorker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_PLANE_BY_WELL, planeWell);
                planeWell.Focus();
                canv.DrawLayerList();
            }
        }
        public void planeUpdateWindow(bool OnlyDraw)
        {
            if (!OnlyDraw)
            {
                planeWell.StopDrawWaitIcon();
                canv.DrawLayerList();
            }
            planeWell.UpdateWindowTitle();
            planeWell.DrawData();
        }
        public void planeLastPerfDepth()
        {
            planeWell.SetMedianDepth(planeWell.GetLastPerfDepth());
        }
        public void planeClearAll()
        {
            planeWell.ClearAll();
        }
        public void PlaneReloadMarkers()
        {
            planeWell.ReloadMarkers();
        }
        public void PlaneUpdateMarkers(Well w)
        {
            planeWell.UpdateMarkers(w);
        }
        
        // Grids
        public void PropertyGridSetObj(object SelectedObject)
        {
            pgProperties.SelectedObject = SelectedObject;
            pgProperties.Refresh();
        }
        public void dGridShowMer(int oilFieldIndex, int wellIndex) 
        {
            if (oilFieldIndex != -1 && wellIndex != -1)
            {
                dgMer.ShowMer(oilFieldIndex, wellIndex);
            }
            else
            {
                dgMer.ClearTable();
            }
        }
        public void dGridShowChess(int oilFieldIndex, int wellIndex)
        {
            if (oilFieldIndex != -1 && wellIndex != -1)
            {
                dgChess.ShowChess(oilFieldIndex, wellIndex);
            }
            else
            {
                dgChess.ClearTable();
            }
        }
        public void dGridShowGIS(int oilFieldIndex, int wellIndex)
        {
            if (oilFieldIndex != -1 && wellIndex != -1)
            {
                dgGIS.ShowGIS(oilFieldIndex, wellIndex);
            }
            else
            {
                dgGIS.ClearTable();
            }
        }
        public void dGridShowCore(int oilFieldIndex, int wellIndex)
        {
            if (oilFieldIndex != -1 && wellIndex != -1)
            {
                dgCore.ShowCore(oilFieldIndex, wellIndex);
            }
            else
            {
                dgCore.ClearTable();
            }
        }
        public void dGridShowCoreTest(int oilFieldIndex, int wellIndex)
        {
            if (oilFieldIndex != -1 && wellIndex != -1)
            {
                dgCoreTest.ShowCoreTest(oilFieldIndex, wellIndex);
            }
            else
            {
                dgCoreTest.ClearTable();
            }
        }

        // Chart
        public bool dChartGetByOilObjMode()
        {
            return chart.ByPlastMode;
        }
        public void dChartShowData(int oilFieldIndex, int wellIndex)
        {
            canv.IsGraphicsByOneDate = false;
            chart.ShowData(oilFieldIndex, wellIndex);
        }
        public void dChartSetPIButtonTag(bool tag)
        {
            tsbPISeriesShow.Tag = tag;
        }
        public void dChartStopWaitIcon()
        {
            chart.StopDrawWaitIcon();
        }
        public bool dChartSetSourceObj(BaseObj SourceObj)
        {
            return chart.SetSourceObject(SourceObj);
        }
        public void dChartSumParamsShow()
        {
            chart.ShowProjParams = tsbShowProjParams.Checked;
            chart.ShowBPParams = tsbShowBPParams.Checked;
            chart.ReLoadByActiveScheme();
        }
        public void dChartSetIntervalType(int IntervalType)
        {
            switch (IntervalType)
            {
                case 0:
                    tsbIntervalYear.PerformClick();
                    break;
                case 1:
                    tsbIntervalMonth.PerformClick();
                    break;
                case 2:
                    tsbIntervalDay.PerformClick();
                    break;
            }
        }
        public void dChartHideChartAreas()
        {
            chart.StartDrawWaitIcon();
        }
        public void dChartSetWindowText(string text)
        {
            if (chartPane != null) chartPane.Text = text;
        }
        public void dChartSetProjName(string Name)
        {
            if (Name == "") chart.ProjParamsXValue = -1;
            chart.ProjParamsName = Name;
        }
        public void dChartSetShowBizParams(bool Checked)
        {
            if (chart.ShowBPParams != Checked)
            {
                tsbShowBPParams.Checked = Checked;
                chart.ShowBPParams = Checked;
                if (tsbShowBPParams.Checked)
                {
                    tsbShowBPParams.ToolTipText = "������ ���������� ������-�����";
                }
                else
                {
                    tsbShowBPParams.ToolTipText = "���������� ���������� ������-�����";
                }
            }
        }

        // DataGrid AREAS
        public void dAreasSetDateComment()
        {
            if ((dgAreas.calcDate != DateTime.MinValue) && (dgAreas.lastDateMer != DateTime.MaxValue) && (dgAreas.table.Rows.Count > 0))
            {
                tslComment.Text = "���� �������:" + dgAreas.calcDate.ToShortDateString() + ", ���� ���:" + dgAreas.lastDateMer.ToShortDateString();
            }
            else
            {
                tslComment.Text = "";
            }
        }
        public int dAreasTableCount()
        {
            return dgAreas.table.Rows.Count;
        }
        public void dAreasLoadTable(BackgroundWorker worker, DoWorkEventArgs e)
        {
            dgAreas.LoadAreasParamsFromCache(worker, e);
        }
        public void dAreasFillTable(BackgroundWorker worker, DoWorkEventArgs e)
        {
            dgAreas.ShowAreas(worker, e);
        }
        public void dAreasFillTable(BackgroundWorker worker, DoWorkEventArgs e, DateTime Start, DateTime End)
        {
            dgAreas.ShowAreasByDate(worker, e, Start, End);
        }

        public void dAreasWriteToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            dgAreas.WriteAreasParamsToCache(worker, e);
        }
        public void dAreasSetAreaRow(int OilFieldIndex, int AreaIndex)
        {
            dgAreas.SelectAreaRow(OilFieldIndex, AreaIndex);
        }
        public void dAreasClearDataSet()
        {
            dgAreas.ClearDataSet();
        }
        public void dAreaSetDataSet()
        {
            dgAreas.SetDataSource();
        }

        public void dActiveWindow(string WindowName)
        {
            if (uDockManager != null)
            {
                for (int i = 0; i < uDockManager.ControlPanes.Count; i++)
                {
                    DockableControlPane dcp = uDockManager.ControlPanes[i];

                    if (dcp.Control.Name == WindowName)
                    {
                        dcp.Activate();
                        break;
                    }
                }
            }
        }

        public void ClearAllTables()
        {
            dgMer.ClearTable();
            dgChess.ClearTable();
        }
        public void ReLoadProject()
        {
            if(indActiveProject < ProjList.Count)
            {
                string projectPath = (ProjList[indActiveProject]).path;
                mMenuClose.PerformClick();

                string[] parseList = projectPath.Split(new char[] { '\\' });
                if (parseList.Length > 0)
                {
                    Project proj = new Project(projectPath);
                    proj.Name = parseList[parseList.Length - 1];
                    this.reLog.Clear();
                    ProjList.Add(proj);
                    indActiveProject = ProjList.Count - 1;
                    SetComponentProject();
                    
                    pb.Visible = true;
                    stBar1.Text = "�������� ������� '" + parseList[parseList.Length - 1] + "'...";
                    worker.Run(Constant.BG_WORKER_WORK_TYPE.RELOAD_PROJECT, proj);
                }
            }
 
        }

        // Discuss
        public void dDiscussWindowText(string Text)
        {
            if (dcpDiscuss != null) dcpDiscuss.Text = Text;
        }
        public void OpenInvite(SmartPlus.Network.NetworkInviteItem invite)
        {
            if (discuss != null)
            {
                BaseObj obj = null;
                switch (invite.ObjectType)
                {
                    case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                        obj = new BaseObj(invite.ObjectType);
                        obj.Code = invite.RegionCode;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.NGDU:
                        obj = new BaseObj(invite.ObjectType);
                        obj.Code = invite.RegionCode;
                        obj.Index = invite.NGDUCode;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                        obj = canv.MapProject.OilFields[invite.OilfieldIndex];
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.AREA:
                        obj = canv.MapProject.OilFields[invite.OilfieldIndex].Areas[invite.ObjectIndex];
                        obj.Code = invite.OilfieldIndex;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.WELL:
                        obj = canv.MapProject.OilFields[invite.OilfieldIndex].Wells[invite.ObjectIndex];
                        obj.Code = invite.OilfieldIndex;
                        break;
                }
                dActiveWindow("pDiscussion");
                canv.LastSelectObject = obj;
                discuss.UpdateData(obj, invite.ThemeId, true);
                fileServer.UpdateData(canv.LastSelectObject, true);
            }
        }

        // Progress Bar
        private void pb_Click(object sender, EventArgs e)
        {
            fProgress.UserClosing = false;
            fProgress.Start("���������� ��������...");
            fProgress.Show();
        }

        #endregion

        #region �����
        void tbFinder_TextChanged(object sender, EventArgs e)
        {
            if (finder != null) finder.StartFind(tbFinderEx.TextBox.Text);
            HideFindToolTip();
        }
        void tbFinder_MouseEnter(object sender, EventArgs e)
        {
            if ((finder != null) && (!finder.Visible) && (finder.Count > 0))
            {
                finder.ShowUpdate();
            }
        }
        void tbFinder_MouseLeave(object sender, EventArgs e)
        {
            finder.TimerLeaveStart();
        }
        void tbFinder_MouseDown(object sender, MouseEventArgs e)
        {
            if ((finder != null) && (!finder.Visible) && (finder.Count > 0))
            {
                finder.ShowUpdate();
            }
        }
        void tbFinder_KeyDown(object sender, KeyEventArgs e)
        {
            finder.KeyPress(e.KeyCode);
            if ((e.KeyCode == Keys.Up) || (e.KeyCode == Keys.Down))
            {
                e.Handled = true;
            }
        }
        void ShowFindToolTip()
        {
            if(tbFinderEx.Placement == ToolStripItemPlacement.Main && (AppSettings == null || AppSettings.ShowFindTooltip))
            {
                if (ToolTipFind != null) ToolTipFind.Dispose();
                ToolTipFind = new ToolTip();
                ToolTipFind.ToolTipTitle = "����������";
                ToolTipFind.ToolTipIcon = ToolTipIcon.Info;
                ToolTipFind.UseFading = true;
                ToolTipFind.UseAnimation = true;
                ToolTipFind.IsBalloon = true;
                ToolTipFind.Show("������� ����� �������� ��� ������������� ��� ������.", tbFinderEx.TextBox, 10, tbFinderEx.TextBox.Height - 3, 3000);
                ToolTipFind.Show("������� ����� �������� ��� ������������� ��� ������.", tbFinderEx.TextBox, 10, tbFinderEx.TextBox.Height - 3, 3000);
                // ������ ����� ����� ��� ������������ ���������!
                if (AppSettings == null) AppSettings = new AppCommonSettings();
                AppSettings.ShowFindTooltip = false;
                fAppSettings.WriteAppCommonSettings(AppSettings);
                AppSettings = null;
            }
        }
        void HideFindToolTip()
        {
            if (ToolTipFind != null) ToolTipFind.Dispose();
        }
        #endregion

        // Toolbars
        #region CanvasMap ToolBar 
            public void SetShowDiscussLetterBoxBtnEnable(bool enable)
            {
                tsbShowDiscussLetterBox.Enabled = enable;
            }
            public void SetShowDiscussLetterBoxBtnChecked(bool check)
            {
                tsbShowDiscussLetterBox.Checked = check;
                if (!check) canv.ShowLetterBox = false;
            }

            private void tsbAddContour_Click(object sender, EventArgs e)
            {
                canv.AddNewContour();
                //StatUsage.AddMessage(CollectorStatId.BTN_ADD_CONTOUR);
            }

            private void tsbAddProfileByLine_Click(object sender, EventArgs e)
            {
                canv.ClearSelectedList();
                //StatUsage.AddMessage(CollectorStatId.BTN_ADD_PROFILE_LINE);
                worker.Run(Constant.BG_WORKER_WORK_TYPE.CREATE_PROFILE_BY_NEWCONTOUR, canv);
            }

            private void tsbAddProfileByWell_Click(object sender, EventArgs e)
            {
                canv.ClearSelectedList();
                //StatUsage.AddMessage(CollectorStatId.BTN_ADD_PROFILE_NEAR_POINT);
                worker.Run(Constant.BG_WORKER_WORK_TYPE.CREATE_PROFILE_BY_NEAR_WELL, canv);
            }

            public void SetWellTrajectoryButtonCheck()
            {
                tsbLoadWellsTrajectory.Checked = true;
            }
            private void tsbLoadWellsTrajectory_Click(object sender, EventArgs e)
            {
                if (tsbLoadWellsTrajectory.Checked)
                {
                    bool res = canv.HideWellTrajectory();
                    if (res)
                    {
                        tsbLoadWellsTrajectory.Checked = false;
                    }
                    else
                    {
                        canv.ShowWellTrajectory();
                    }
                }
                else
                {
                    canv.ShowWellTrajectory();
                }
            }

            private void tsbCalcBubble_Click(object sender, EventArgs e)
            {
                if (tsbCalcBubble.Checked)
                {
                    bool res = canv.HideBubbleMap();
                    if (res) tsbCalcBubble.Checked = false;
                    else canv.CalcBubbleMap(Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT);
                }
                else
                {
                    //StatUsage.AddMessage(CollectorStatId.BTN_CURRENT_BUBBLE);
                    canv.ShowBubbleMap(Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT);
                }
            }

            private void tsbCalcAccumBubble_Click(object sender, EventArgs e)
            {
                if (tsbCalcAccumBubble.Checked)
                {
                    bool res = canv.HideBubbleMap();
                    if (res) tsbCalcAccumBubble.Checked = false;
                    else canv.CalcBubbleMap(Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM);
                }
                else
                {
                    //StatUsage.AddMessage(CollectorStatId.BTN_ACCUM_BUBBLE);
                    if (fBubbleSet.BAStartDate < fBubbleSet.BALastDate)
                    {
                        canv.ShowBubbleMap(Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM);
                    }
                }
            }

            private void tsbCalcHistoryBubble_Click(object sender, EventArgs e)
            {
                if (tsbCalcHistoryBubble.Checked)
                {
                    bool res = canv.HideBubbleMap();
                    if (res) tsbCalcHistoryBubble.Checked = false;
                    else canv.CalcBubbleMap(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY);
                }
                else
                {
                    //StatUsage.AddMessage(CollectorStatId.BTN_HYSTORY_BUBBLE);
                    if (fBubbleSet.BHStartDate < fBubbleSet.BHLastDate)
                    {
                        canv.ShowBubbleMap(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY);
                    }
                }
            }

            private void tsbBubbleSettings_Click(object sender, EventArgs e)
            {
                //StatUsage.AddMessage(CollectorStatId.BTN_SETTING_BUBBLE);
                if (fBubbleSet.ShowSettingDialog() == DialogResult.OK)
                {
                    if (fBubbleSet.LastBubbleEdit != Constant.BUBBLE_MAP_TYPE.NOT_SET)
                    {
                        if ((fBubbleSet.UpdateBubbleMap == 0) || 
                            ((fBubbleSet.UpdateBubbleMap == 1) && (canv.BubbleMapType != fBubbleSet.LastBubbleEdit)))
                        {
                            canv.CalcBubbleMap(fBubbleSet.LastBubbleEdit);
                        }
                        else if ((fBubbleSet.UpdateBubbleMap == 1) && (canv.BubbleMapType == fBubbleSet.LastBubbleEdit))
                        {
                            canv.UpdateBubbleMap();
                        }
                    }
                }
            }

            public void SetBubbleMapCheck(Constant.BUBBLE_MAP_TYPE BubbleMapType)
            {
                tsbCalcBubble.Checked = false;
                tsbCalcAccumBubble.Checked = false;
                tsbCalcHistoryBubble.Checked = false;
                switch (BubbleMapType)
                {
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                        tsbCalcBubble.Checked = true;
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                        tsbCalcAccumBubble.Checked = true;
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                        tsbCalcHistoryBubble.Checked = true;
                        break;
                } 
            }

            private void tsbVisibleAreas_Click(object sender, EventArgs e)
            {
                canv.SetAreasVisible(tsbVisibleAreas.Checked);
            }

            private void tsbVisibleWellPad_Click(object sender, EventArgs e)
            {
                canv.SetVisibleWellHead(tsbVisibleWellPad.Checked);
            }

            private void tsbContoursColor_Click(object sender, EventArgs e)
            {
                canv.SetContourColorMode(tsbContoursColor.Checked);
            }

            private void tsbGrayIcons_Click(object sender, EventArgs e)
            {
                canv.SetGrayIconsMode(tsbGrayIcons.Checked);
            }

            private void tsbPartMapSnapShoot_Click(object sender, EventArgs e)
            {
                switch ((int)tsbMapSnapShoot.Tag)
                {
                    case 0:
                        tsbMapSnapShoot.ButtonClick -= tsbPartMapSnapShoot_Click;
                        break;
                    case 1:
                        tsbMapSnapShoot.ButtonClick -= tsbAllMapSnapShot_Click;
                        break;
                }
                tsbMapSnapShoot.Tag = 0;

                tsbMapSnapShoot.ButtonClick += new EventHandler(tsbPartMapSnapShoot_Click);
                tsbMapSnapShoot.Image = tsbMapSnapShoot.DropDownItems[0].Image;
                tsbMapSnapShoot.ToolTipText = tsbMapSnapShoot.DropDownItems[0].Text;

                //StatUsage.AddMessage(CollectorStatId.CANVAS_BTN_SNAPSHOOT);
                canv.SetSnapShootMode(false);
            }

            private void tsbAllMapSnapShot_Click(object sender, EventArgs e)
            {
                switch ((int)tsbMapSnapShoot.Tag)
                {
                    case 0:
                        tsbMapSnapShoot.ButtonClick -= tsbPartMapSnapShoot_Click;
                        break;
                    case 1:
                        tsbMapSnapShoot.ButtonClick -= tsbAllMapSnapShot_Click;
                        break;
                }
                tsbMapSnapShoot.Tag = 1;

                tsbMapSnapShoot.ButtonClick += new EventHandler(tsbAllMapSnapShot_Click);
                tsbMapSnapShoot.Image = tsbMapSnapShoot.DropDownItems[1].Image;
                tsbMapSnapShoot.ToolTipText = tsbMapSnapShoot.DropDownItems[1].Text;

                //StatUsage.AddMessage(CollectorStatId.CANVAS_BTN_SNAPSHOOT);
                canv.SetSnapShootMode(true);
            }

            private void tsbShowBizPlan_Click(object sender, EventArgs e)
            {
                canv.ShowBizPlanParams = tsbShowBizPlanOnMap.Checked;
                canv.DrawLayerList();
            }

            private void tsbCreateVoronoiMap_Click(object sender, EventArgs e)
            {
                canv.CreateVoronoiMap();
            }

            private void tsbShowDiscussLetterBox_Click(object sender, EventArgs e)
            {
                canv.ShowLetterBox = tsbShowDiscussLetterBox.Checked;
                if (tsbShowDiscussLetterBox.Checked)
                {
                    tsbShowDiscussLetterBox.Enabled = false;
                    canv.StartLetterBoxUpdate();
                }
                else
                {
                    canv.StopLetterBoxUpdate();
                    canv.DrawLayerList();
                }
            }
        #endregion

        #region WorkLayers Toolbar
            private void tsbSaveWork_Click(object sender, EventArgs e)
            {
                fProgress.Show();
                fProgress.Start("���������� ������ ������� �����...");
                worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYERS);
                canv.LayerWorkListChanged = false;
            }
        #endregion

        #region Graphics Toolbar

            private void tsbShowInfo_Click(object sender, EventArgs e)
            {
                //StatUsage.AddMessage(CollectorStatId.GRAPHICS_BTN_INFO);
                chart.ShowInfo = tsbShowInfo.Checked;
                chart.ReDrawChartAreas();
            }
            private void tsbShowLegend_Click(object sender, EventArgs e)
            {
                //StatUsage.AddMessage(CollectorStatId.GRAPHICS_BTN_LEGEND);
                chart.LegendVisible = tsbShowLegend.Checked;
                chart.ReDrawChartAreas();
            }
            private void tsbShowProjParams_Click(object sender, EventArgs e)
            {
                chart.ShowProjParams = tsbShowProjParams.Checked;
                if (tsbShowProjParams.Checked) tsbShowProjParams.ToolTipText = "������ ��������� ����������";
                else tsbShowProjParams.ToolTipText = "���������� ��������� ����������";
                chart.ReLoadByActiveScheme();
            }
            private void tsbShowBPParams_Click(object sender, EventArgs e)
            {
                chart.ShowBPParams = tsbShowBPParams.Checked;
                if (tsbShowBPParams.Checked)
                {
                    tsbShowBPParams.ToolTipText = "������ ���������� ������-�����";
                }
                else
                {
                    tsbShowBPParams.ToolTipText = "���������� ���������� ������-�����";
                }
                chart.ReLoadByActiveScheme();
            }
            private void tscbScheme_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (chart != null)
                {
                    //StatUsage.AddMessage(CollectorStatId.GRAPHICS_CHANGE_SCHEME);
                    chart.ShowProjParams = tsbShowProjParams.Checked;
                    chart.ShowBPParams = tsbShowBPParams.Checked;
                    chart.SchemeActivate(tscbScheme.SelectedIndex);
                }
            }

            public void SetInervalChecked(int Index)
            {
                switch (Index)
                {
                    case 0:
                        tsbInterval.Text = tsbIntervalYear.Text;
                        tsbIntervalYear.Checked = true;
                        tsbIntervalMonth.Checked = false;
                        tsbIntervalDay.Checked = false;
                        break;
                    case 1:
                        tsbInterval.Text = tsbIntervalMonth.Text;
                        tsbIntervalYear.Checked = false;
                        tsbIntervalMonth.Checked = true;
                        tsbIntervalDay.Checked = false;
                        break;
                    case 2:
                        tsbInterval.Text = tsbIntervalDay.Text;
                        tsbIntervalYear.Checked = false;
                        tsbIntervalMonth.Checked = false;
                        tsbIntervalDay.Checked = true;
                        break;
                }
            }

            public void SetIntervalEnabled(int Index, bool Enabled)
            {
                switch (Index)
                {
                    case 0:
                        tsbIntervalYear.Enabled = Enabled;
                        break;
                    case 1:
                        tsbIntervalMonth.Enabled = Enabled;
                        break;
                    case 2:
                        tsbIntervalDay.Enabled = Enabled;
                        break;
                }
            }

            private void tsbIntervalYear_Click(object sender, EventArgs e)
            {
                if (chart != null)
                {
                    //StatUsage.AddMessage(CollectorStatId.GRAPHICS_CHANGE_DATE_INTERVAL);
                    tsbIntervalYear.Checked = true;
                    tsbIntervalMonth.Checked = false;
                    tsbIntervalDay.Checked = false;

                    tsbInterval.Text = "�� �����";
                    chart.AllOilObjChecked = filterOilObj.AllCurrentSelected;
                    if ((chart.SourceObj != null) &&
                        ((chart.SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.OILFIELD) ||
                         (chart.SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT)))
                    {
                        chart.AllOilObjChecked = filterOilObj.AllCurrentOilFieldSelected;
                    }
                    chart.ShowProjParams = tsbShowProjParams.Checked;
                    chart.ShowBPParams = tsbShowBPParams.Checked;
                    chart.ReLoadByActiveScheme(0);
                }
            }
            private void tsbIntervalMonth_Click(object sender, EventArgs e)
            {
                if (chart != null)
                {
                    //StatUsage.AddMessage(CollectorStatId.GRAPHICS_CHANGE_DATE_INTERVAL);
                    tsbInterval.Text = "�� �������";
                    tsbIntervalYear.Checked = false;
                    tsbIntervalMonth.Checked = true;
                    tsbIntervalDay.Checked = false;

                    chart.AllOilObjChecked = filterOilObj.AllCurrentSelected;
                    if ((chart.SourceObj != null) &&
                        ((chart.SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.OILFIELD) ||
                         (chart.SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT)))
                    {
                        chart.AllOilObjChecked = filterOilObj.AllCurrentOilFieldSelected;
                    }

                    //chart.HideChartAreas();
                    //Application.DoEvents();
                    chart.ReLoadByActiveScheme(1);
                    //chart.ShowChartAreas();
                }
            }
            private void tsbIntervalDay_Click(object sender, EventArgs e)
            {
                if (chart != null)
                {
                    //StatUsage.AddMessage(CollectorStatId.GRAPHICS_CHANGE_DATE_INTERVAL);
                    tsbInterval.Text = "���������";
                    tsbIntervalYear.Checked = false;
                    tsbIntervalMonth.Checked = false;
                    tsbIntervalDay.Checked = true;

                    chart.AllOilObjChecked = filterOilObj.AllCurrentSelected;
                    if ((chart.SourceObj != null) &&
                        ((chart.SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.OILFIELD) ||
                         (chart.SourceObj.TypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT)))
                    {
                        chart.AllOilObjChecked = filterOilObj.AllCurrentOilFieldSelected;
                    }

                    //chart.HideChartAreas();
                    //Application.DoEvents();
                    chart.ReLoadByActiveScheme(2);
                    //chart.ShowChartAreas();
                }
            }

            private void tscbByObjType_SelectedIndexChanged(object sender, EventArgs e)
            {
                //if (chart != null)
                //{
                //    chart.OilObjTypeActivate(tscbByObjType.SelectedIndex);
                //}
            }
            
            private void tsbGraphicsSettings_Click(object sender, EventArgs e)
            {
                chart.ShowSettings();
            }
            private void tsbGraphicsSnapShoot_Click(object sender, EventArgs e)
            {

            }
            
            private void GR_SnapShootPart_Click(object sender, EventArgs e)
            {
                switch ((int)GR_SnapShoot.Tag)
                {
                    case 0:
                        GR_SnapShoot.ButtonClick -= GR_SnapShootPart_Click;
                        break;
                    case 1:
                        GR_SnapShoot.ButtonClick -= GR_SnapShootAll_Click;
                        break;
                }
                GR_SnapShoot.Tag = 0;

                GR_SnapShoot.ButtonClick += new EventHandler(GR_SnapShootPart_Click);
                GR_SnapShoot.Image = GR_SnapShoot.DropDownItems[0].Image;
                GR_SnapShoot.ToolTipText = GR_SnapShoot.DropDownItems[0].Text;
                //StatUsage.AddMessage(CollectorStatId.CANVAS_BTN_SNAPSHOOT);
                chart.SnapToClipboard(false);
            }

            private void GR_SnapShootAll_Click(object sender, EventArgs e)
            {
                switch ((int)GR_SnapShoot.Tag)
                {
                    case 0:
                        GR_SnapShoot.ButtonClick -= GR_SnapShootPart_Click;
                        break;
                    case 1:
                        GR_SnapShoot.ButtonClick -= GR_SnapShootAll_Click;
                        break;
                }
                GR_SnapShoot.Tag = 1;
                GR_SnapShoot.ButtonClick += new EventHandler(GR_SnapShootAll_Click);
                GR_SnapShoot.Image = GR_SnapShoot.DropDownItems[1].Image;
                GR_SnapShoot.ToolTipText = GR_SnapShoot.DropDownItems[1].Text;
                //StatUsage.AddMessage(CollectorStatId.GRAPHICS_BTN_SNAPSHOOT);
                chart.SnapToClipboard(true);
            }

            private void GR_ShowGTM_Click(object sender, EventArgs e)
            {
                GR_ShowGTM.Checked = !GR_ShowGTM.Checked;
                chart.SetChartLabelsEnabled(0, GR_ShowGTM.Checked);
            }
            private void GR_ShowWellAction_Click(object sender, EventArgs e)
            {
                GR_ShowWellAction.Checked = !GR_ShowWellAction.Checked;
                chart.SetChartLabelsEnabled(1, GR_ShowWellAction.Checked);
            }
            private void GR_ShowWellLeak_Click(object sender, EventArgs e)
            {
                GR_ShowWellLeak.Checked = !GR_ShowWellLeak.Checked;
                chart.SetAxisLabelsEnabled(0, GR_ShowWellLeak.Checked);
            }

            private void GR_ShowWellSuspend_Click(object sender, EventArgs e)
            {
                GR_ShowWellSuspend.Checked = !GR_ShowWellSuspend.Checked;
                chart.SetAxisLabelsEnabled(1, GR_ShowWellSuspend.Checked);
            }

            private void GR_ShowWellStop_Click(object sender, EventArgs e)
            {
                GR_ShowWellStop.Checked = !GR_ShowWellStop.Checked;
                chart.SetAxisColorZonesEnabled(0, GR_ShowWellStop.Checked);
            }

            private void tsbPISeriesShow_Click(object sender, EventArgs e)
            {
                chart.ShowPIseries = tsbPISeriesShow.Checked;
                if (tsbPISeriesShow.Checked)
                {
                    tsbPISeriesShow.ToolTipText = "������ ������� ����� � Q�.������.";
                }
                else
                {
                    tsbPISeriesShow.ToolTipText = "���������� ������� ����� � Q�.������.";
                }
                chart.ReLoadByActiveScheme();
            }

            private void tsbTable_Click(object sender, EventArgs e)
            {
                tsbTable.Checked = !tsbTable.Checked;
                chart.TableVisible = tsbTable.Checked;
                //StatUsage.AddMessage(CollectorStatId.GRAPHICS_SHOW_TABLE);
                if (tsbTable.Checked)
                {
                    tsbTable.Image = Properties.Resources.checked16;
                    chart.ShowDataTable();
                }
                else
                    tsbTable.Image = Properties.Resources.unchecked16;
                scGraphics.Panel2Collapsed = !scGraphics.Panel2Collapsed;
                chart.SetLastRow();
            }

        #endregion

        #region AREAS ToolBar
            private void tsbLoadAreasParamsFromCache_Click(object sender, EventArgs e)
            {
                if ((indActiveProject < ProjList.Count) && (ProjList.Count > 0))
                {
                    bool calc = true;
                    if (dgAreas.DataLoaded)
                    {
                        if (MessageBox.Show("��������� ����� ���������� ��� ���������. ��������� �����?", "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            calc = false;
                        }
                    }
                    if (calc)
                    {
                        dgAreas.ClearDataSet();
                        worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_AREAS_PARAMS_FROM_CACHE, ProjList[indActiveProject]);
                    }
                }
            }
            private void tsbLoadAreasParams_Click(object sender, EventArgs e)
            {
                if ((indActiveProject < ProjList.Count) && (ProjList.Count > 0))
                {
                    bool calc = true;
                    if (dgAreas.DataLoaded) 
                    {
                        if (MessageBox.Show("��������� ����� ���������� ��� ���������. ���������� �����?", "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            calc = false;
                        }
                    }
                    if (calc)
                    {
                        //StatUsage.AddMessage(CollectorStatId.BTN_AP_CALC_PARAMS);
                        dgAreas.ClearDataSet();
                        worker.Run(Constant.BG_WORKER_WORK_TYPE.FILL_AREAS_PARAMS_TO_TABLE, ProjList[indActiveProject]);
                    }
                }
            }
            private void tsbDrawAreasComp_Click(object sender, EventArgs e)
            {
                dgAreas.DrawAreasByComp(false);
                canv.DrawLayerList();
            }

            private void tsbDrawAreasAccComp_Click(object sender, EventArgs e)
            {
                dgAreas.DrawAreasByComp(true);
                canv.DrawLayerList();
            }
        #endregion

        #region WELL LIST ToolBar

            public void WL_SelectWell(Well w)
            {
                WLSettings.SelectWell(w);
            }

            private void tsbAdd_Click(object sender, EventArgs e)
            {
                //StatUsage.AddMessage(CollectorStatId.BTN_WL_NEW_EMPTY);
                canv.AddWellLayer(true);
            }

            private void tsbAddWellListByFilter_Click(object sender, EventArgs e)
            {
                if ((indActiveProject > -1) && (indActiveProject < ProjList.Count))
                {
                    if (WDFilterForm.ShowDialog() == DialogResult.OK)
                    {
                        //StatUsage.AddMessage(CollectorStatId.WL_BY_FILTER);
                        worker.Run(Constant.BG_WORKER_WORK_TYPE.CREATE_WELLLIST_BY_FILTER, WDFilterForm);
                    }
                }
            }

            private void WL_AddSelectWell_Click(object sender, EventArgs e)
            {
                switch ((int)WL_AddWell.Tag)
                {
                    case 0:
                        WL_AddWell.ButtonClick -= WL_AddSelectWell_Click;
                        break;
                    case 1:
                        WL_AddWell.ButtonClick -= WL_AddContourWell_Click;
                        break;
                    case 2:
                        WL_AddWell.ButtonClick -= WL_AddClipBoardWell_Click;
                        break;
                }
                WL_AddWell.Tag = 0;

                WL_AddWell.ButtonClick += new EventHandler(WL_AddSelectWell_Click);
                WL_AddWell.Image = WL_AddWell.DropDownItems[0].Image;
                WL_AddWell.ToolTipText = WL_AddWell.DropDownItems[0].ToolTipText;
                //StatUsage.AddMessage(CollectorStatId.BTN_WL_ADD_SEL_WELL);
                WLSettings.AddSelectedWellList();
            }

            private void WL_AddContourWell_Click(object sender, EventArgs e)
            {
                switch ((int)WL_AddWell.Tag)
                {
                    case 0:
                        WL_AddWell.ButtonClick -= WL_AddSelectWell_Click;
                        break;
                    case 1:
                        WL_AddWell.ButtonClick -= WL_AddContourWell_Click;
                        break;
                    case 2:
                        WL_AddWell.ButtonClick -= WL_AddClipBoardWell_Click;
                        break;
                }
                WL_AddWell.Tag = 1;
                WL_AddWell.ButtonClick += new EventHandler(WL_AddContourWell_Click);
                WL_AddWell.Image = WL_AddWell.DropDownItems[1].Image;
                WL_AddWell.ToolTipText = WL_AddWell.DropDownItems[1].ToolTipText;
                //StatUsage.AddMessage(CollectorStatId.BTN_WL_ADD_WELL_CNTR);
                WLSettings.AddCntrWellList(true);
            }

            private void WL_AddClipBoardWell_Click(object sender, EventArgs e)
            {
                switch ((int)WL_AddWell.Tag)
                {
                    case 0:
                        WL_AddWell.ButtonClick -= WL_AddSelectWell_Click;
                        break;
                    case 1:
                        WL_AddWell.ButtonClick -= WL_AddContourWell_Click;
                        break;
                    case 2:
                        WL_AddWell.ButtonClick -= WL_AddClipBoardWell_Click;
                        break;
                }
                WL_AddWell.Tag = 2;
                WL_AddWell.ButtonClick += new EventHandler(WL_AddClipBoardWell_Click);
                WL_AddWell.Image = WL_AddWell.DropDownItems[2].Image;
                WL_AddWell.ToolTipText = WL_AddWell.DropDownItems[2].ToolTipText;
                //StatUsage.AddMessage(CollectorStatId.BTN_WL_ADD_WELL_CLIPBOARD);
                WLSettings.AddClipboardWellList();
            }

            private void tsbDelete_Click(object sender, EventArgs e)
            {
                WLSettings.RemoveSelectedWells(true);
            }

            private void tsbEditIcons_Click(object sender, EventArgs e)
            {
                //StatUsage.AddMessage(CollectorStatId.BTN_WL_SETTING_ICON);
                WLSettings.EditIconList();
            }

            private void WL_GraphicsByOneDate_Click(object sender, EventArgs e)
            {
                switch ((int)WL_Graphics.Tag)
                {
                    case 0:
                        WL_Graphics.ButtonClick -= WL_GraphicsByOneDate_Click;
                        break;
                    case 1:
                        WL_Graphics.ButtonClick -= WL_GraphicsByAllDate_Click;
                        break;
                }
                WL_Graphics.Tag = 0;
                WL_Graphics.ButtonClick += new EventHandler(WL_GraphicsByOneDate_Click);
                WL_Graphics.Image = WL_Graphics.DropDownItems[0].Image;
                WL_Graphics.ToolTipText = WL_Graphics.DropDownItems[0].ToolTipText;
                WLSettings.DrawGraphicsWellList(true);
                ShowDataChartPane();
            }

            private void WL_GraphicsByAllDate_Click(object sender, EventArgs e)
            {
                switch ((int)WL_Graphics.Tag)
                {
                    case 0:
                        WL_Graphics.ButtonClick -= WL_GraphicsByOneDate_Click;
                        break;
                    case 1:
                        WL_Graphics.ButtonClick -= WL_GraphicsByAllDate_Click;
                        break;
                }
                WL_Graphics.Tag = 1;
                WL_Graphics.ButtonClick += new EventHandler(WL_GraphicsByAllDate_Click);
                WL_Graphics.Image = WL_Graphics.DropDownItems[1].Image;
                WL_Graphics.ToolTipText = WL_Graphics.DropDownItems[1].ToolTipText;
                WLSettings.DrawGraphicsWellList(false);
                ShowDataChartPane();
            }

            private void tsbWelllistSettings_Click(object sender, EventArgs e)
            {
                WLSettings.ShowAdvWellListSettings();
            }

            public void UpdateWellListSettings(C2DLayer layer)
            {
                if ((WLSettings != null) && ((layer == null) || (WLSettings.Visible)))
                {
                    WLSettings.SetLayerSource(layer);
                    WLSettings.ShowWellList();
                }
                UpdateWellListSettingsToolBar();
            }

            public void UpdateWellListSettingsToolBar()
            {
                if (WLSettings != null)
                {
                    if (WLSettings.LayerFixed)
                    {
                        WL_AddWell.Enabled = true;
                        WL_Graphics.Enabled = true;
                        tsbDelete.Enabled = true;
                        tsLabel.Enabled = true;
                        tsbEditIcons.Enabled = true;
                        tsbWelllistSettings.Enabled = true;
                        WL_FinderEx.Enabled = true;
                    }
                    else
                    {
                        WL_AddWell.Enabled = false;
                        WL_Graphics.Enabled = false;
                        tsbDelete.Enabled = false;
                        tsLabel.Enabled = false;
                        tsbEditIcons.Enabled = false;
                        tsbWelllistSettings.Enabled = false;
                        WL_FinderEx.Enabled = false;
                    }
                }
            }

            public void ShowWellListPane()
            {
                if ((dcpWellSetting != null) && (!dcpWellSetting.IsVisible))
                {
                    dcpWellSetting.Closed = false;
                    ((ToolStripMenuItem)dcpWellSetting.Tag).Checked = !dcpWellSetting.Closed;
                    if (!dcpWellSetting.IsActive)
                    {
                        dcpWellSetting.Activate();
                    }
                }
                WLSettings.Focus();
            }
            public void ShowDataChartPane()
            {
                if (chartPane != null)
                {
                    chartPane.Closed = false;
                    ((ToolStripMenuItem)chartPane.Tag).Checked = !chartPane.Closed;
                    if (!chartPane.IsActive)
                    {
                        chartPane.Activate();
                    }
                }
            }

            public void WellListUpdateTitle()
            {
                WLSettings.UpdateTitle();
            }

            public void WellListSetWindowText(string text)
            {
                if (dcpWellSetting != null) dcpWellSetting.Text = text;
            }

            void WL_FindWellBox_TextChanged(object sender, EventArgs e)
            {
                WLSettings.FindWells(WL_FinderEx.TextBox.Text);
            }

            void WL_FindWellBox_KeyDown(object sender, KeyEventArgs e)
            {
                WLSettings.KeyPress(e.KeyCode);
                if ((e.KeyCode == Keys.Up) || (e.KeyCode == Keys.Down))
                {
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    WL_FinderEx.TextBox.Focus();
                }
            }
        #endregion

        #region CORSCHEME TOOLBAR
            public void UpdateCorSchemeToolBars()
            {
                // CORSCHEME TOOLBAR
                int visible = -1;
                visible = corScheme.BaseScheme.GetVisibleByType(Constant.Plane.ObjectType.DEPTH_MARKS);
                if (visible != -1) tsbShowDepth.Checked = (visible == 1);
                visible = corScheme.BaseScheme.GetVisibleByType(Constant.Plane.ObjectType.GIS);
                if (visible != -1) tsbShowGis.Checked = (visible == 1);
                visible = corScheme.BaseScheme.GetVisibleByType(Constant.Plane.ObjectType.OIL_OBJ);
                if (visible != -1) tsbShowOilObjName.Checked = (visible == 1);
            }
            
            private void tsbDrawHead_Click(object sender, EventArgs e)
            {
                corScheme.SetShowColumnHead(tsbCorSchShowHead.Checked);
            }

            private void tsbGisMer_Click(object sender, EventArgs e)
            {
                corScheme.SetVisibleByType(Constant.Plane.ObjectType.OIL_OBJ, tsbShowOilObjName.Checked);
                if (corSchemeSettings != null) corSchemeSettings.LoadColumnVisible();
            }

            private void tsbCS_DepthAbsMd_Click(object sender, EventArgs e)
            {
                corScheme.SetAbsMdDepth(tsbCS_DepthAbsMd.Checked);
            }

            private void tsbShowDepth_Click(object sender, EventArgs e)
            {
                corScheme.SetVisibleByType(Constant.Plane.ObjectType.DEPTH_MARKS, tsbShowDepth.Checked);
                if (corSchemeSettings != null) corSchemeSettings.LoadColumnVisible();
            }

            private void tsbShowGis_Click(object sender, EventArgs e)
            {
                corScheme.SetVisibleByType(Constant.Plane.ObjectType.GIS, tsbShowGis.Checked);
                if (corSchemeSettings != null) corSchemeSettings.LoadColumnVisible();
            }

            private void tsbShowMarker_Click(object sender, EventArgs e)
            {
                corScheme.SetVisibleByType(Constant.Plane.ObjectType.MARKER, tsbShowMarker.Checked);
            }

            private void tsbCS_ShowDuplicate_Click(object sender, EventArgs e)
            {
                corScheme.ShowDuplicatesLog = tsbCS_ShowDuplicate.Checked;
            }

            private void tsbMarkerEditMode_Click(object sender, EventArgs e)
            {
                if ((!tsbShowMarker.Checked) && (tsbMarkerEditMode.Checked))
                {
                    MessageBox.Show("�������� ����������� ��������!", "��������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    corScheme.SetMarkerEditMode(tsbMarkerEditMode.Checked);
                }
            }

            private void tsbCorSchSettings_Click(object sender, EventArgs e)
            {
                if(corSchemeSettings == null)
                {
                    corSchemeSettings = new CorSchemeSettingsForm(this);
                    corSchemeSettings.ShowForm(this.corScheme);
                    corSchemeSettings.FormClosing += new FormClosingEventHandler(CorSchemeSettings_FormClosing);
                }
            }
            
            void CorSchemeSettings_FormClosing(object sender, FormClosingEventArgs e)
            {
                corSchemeSettings = null;
            }
            
            private void COR_SCH_PartSnapShoot_Click(object sender, EventArgs e)
            {
                switch ((int)COR_SCH_SnapShoot.Tag)
                {
                    case 0:
                        COR_SCH_SnapShoot.ButtonClick -= COR_SCH_PartSnapShoot_Click;
                        break;
                    case 1:
                        COR_SCH_SnapShoot.ButtonClick -= COR_SCH_AllSnapShoot_Click;
                        break;
                }
                COR_SCH_SnapShoot.Tag = 0;
                COR_SCH_SnapShoot.ButtonClick += new EventHandler(COR_SCH_PartSnapShoot_Click);
                COR_SCH_SnapShoot.Image = COR_SCH_SnapShoot.DropDownItems[0].Image;
                COR_SCH_SnapShoot.ToolTipText = COR_SCH_SnapShoot.DropDownItems[0].Text;
                corScheme.SetSnapShootMode(0);
            }

            private void COR_SCH_AllSnapShoot_Click(object sender, EventArgs e)
            {
                switch ((int)COR_SCH_SnapShoot.Tag)
                {
                    case 0:
                        COR_SCH_SnapShoot.ButtonClick -= COR_SCH_PartSnapShoot_Click;
                        break;
                    case 1:
                        COR_SCH_SnapShoot.ButtonClick -= COR_SCH_AllSnapShoot_Click;
                        break;
                }
                COR_SCH_SnapShoot.Tag = 1;
                COR_SCH_SnapShoot.ButtonClick += new EventHandler(COR_SCH_AllSnapShoot_Click);
                COR_SCH_SnapShoot.Image = COR_SCH_SnapShoot.DropDownItems[1].Image;
                COR_SCH_SnapShoot.ToolTipText = COR_SCH_SnapShoot.DropDownItems[1].Text;
                corScheme.SetSnapShootMode(2);
            }
        #endregion

        #region PLANE TOOLBAR
            private void tsbPlaneShowHead_Click(object sender, EventArgs e)
            {
                planeWell.SetShowColumnHead(tsbPlaneShowHead.Checked);
            }
        
            private void tsbPlane_DepthAbsMd_Click(object sender, EventArgs e)
            {
                planeWell.SetAbsMdDepth(tsbPlane_DepthAbsMd.Checked);
            }

            private void tsbPlane_ShowDuplicates_Click(object sender, EventArgs e)
            {
                planeWell.ShowDuplicatesLog = tsbPlane_ShowDuplicates.Checked;
            }

            private void tsbPlaneBindWidth_Click(object sender, EventArgs e)
            {
                planeWell.SetBindWidth(tsbPlaneBindWidth.Checked);
            }
            
            private void PLN_PartSnapShoot_Click(object sender, EventArgs e)
            {
                switch ((int)PLN_SnapShoot.Tag)
                {
                    case 0:
                        PLN_SnapShoot.ButtonClick -= PLN_PartSnapShoot_Click;
                        break;
                    case 1:
                        PLN_SnapShoot.ButtonClick -= PLN_AllSnapShoot_Click;
                        break;
                }
                PLN_SnapShoot.Tag = 0;
                PLN_SnapShoot.ButtonClick += new EventHandler(PLN_PartSnapShoot_Click);
                PLN_SnapShoot.Image = PLN_SnapShoot.DropDownItems[0].Image;
                PLN_SnapShoot.ToolTipText = PLN_SnapShoot.DropDownItems[0].Text;
                planeWell.SetSnapShootMode(0);
            }

            private void PLN_AllSnapShoot_Click(object sender, EventArgs e)
            {
                switch ((int)PLN_SnapShoot.Tag)
                {
                    case 0:
                        PLN_SnapShoot.ButtonClick -= PLN_PartSnapShoot_Click;
                        break;
                    case 1:
                        PLN_SnapShoot.ButtonClick -= PLN_AllSnapShoot_Click;
                        break;
                }
                PLN_SnapShoot.Tag = 1;
                PLN_SnapShoot.ButtonClick += new EventHandler(PLN_AllSnapShoot_Click);
                PLN_SnapShoot.Image = PLN_SnapShoot.DropDownItems[1].Image;
                PLN_SnapShoot.ToolTipText = PLN_SnapShoot.DropDownItems[1].Text;
                planeWell.SetSnapShootMode(2);
            }

            private void tsbPlaneShowMarker_Click(object sender, EventArgs e)
            {
                planeWell.SetVisibleByType(Constant.Plane.ObjectType.MARKER, tsbPlaneShowMarker.Checked);
            }

            private void tsbPlaneWellSettings_Click(object sender, EventArgs e)
            {
                if (corSchemeSettings == null)
                {
                    corSchemeSettings = new CorSchemeSettingsForm(this);
                    corSchemeSettings.ShowForm(this.planeWell);
                    corSchemeSettings.FormClosing += new FormClosingEventHandler(CorSchemeSettings_FormClosing);
                }
            }
        #endregion

        #region REPORT BUILDER TOOLBAR
            private void rb_AddOilfield_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.OilField);
                }
            }
            private void rb_AddArea_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.Area);
                }
            }
            private void rb_AddWell_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.Well);
                }
            }
            private void rb_AddSumParams_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.SumParameters);
                }
            }
            private void rb_AddMer_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.Mer);
                }
            }
            private void rb_AddGis_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.Gis);
                }
            }
            private void rb_AddPerf_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.Perforation);
                }
            }
            private void rb_AddGtm_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.GTM);
                }
            }
            private void rb_AddRepair_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.RepairAction);
                }
            }

            private void rb_AddResearch_Click(object sender, EventArgs e)
            {
                if (ReportBuilder != null)
                {
                    ReportBuilder.AddFlyObject(SmartPlus.ReportBuilder.Base.ObjectType.WellResearch);
                }
            }
            
            private void rb_DeleteSelected_Click(object sender, EventArgs e)
            {
                if(ReportBuilder != null)
                {
                    ReportBuilder.RemoveSelected();
                }
            }
        #endregion

        #region UPDATE PROJECT DATA FORM
            private void tsbUpdates_Click(object sender, EventArgs e)
            {
                UpdateProjForm.ShowForm(this);
            }

            public void ShowUpdateForm()
            {
                UpdateProjForm.ShowForm(this);
            }
        #endregion

        #region PROJECT STATE
        private void SaveProjectState_Click(object sender, EventArgs e)
        {

        }

        private void LoadProjectState_Click(object sender, EventArgs e)
        {

        }
        #endregion

    }

    //������������ � ������� ����������� � ������� 
    public enum Messages { TV_FIRST = 0x1100, TVM_SETITEMW = TV_FIRST + 63 };
    public enum MaskFlag { TVIF_HANDLE = 0x0010, TVIF_STATE = 0x0008 };
    public enum StateFlag { TVIS_BOLD = 0x0010 };
    
    /*
    [StructLayout(LayoutKind.Sequential)]
    public struct TVITEMEX
    {
        public MaskFlag mask;
        public IntPtr hItem;
        public StateFlag state;
        public StateFlag stateMask;
        public string pszText;
        public int cchTextMax;
        public int iImage;
        public int iSelectedImage;
        public int cChildren;
        public IntPtr lParam;
        public int iIntegral;
    } 
     */

    public struct IconInfo
    {
        public bool fIcon;
        public int xHotspot;
        public int yHotspot;
        public IntPtr hbmMask;
        public IntPtr hbmColor;
    }

    public static class NativeMethods
    {
        [DllImport("uxtheme", ExactSpelling = true, CharSet = CharSet.Unicode)]
        public extern static Int32 SetWindowTheme(IntPtr hWnd, String textSubAppName, String textSubIdList);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);

        [DllImport("user32.dll")]
        public static extern IntPtr CreateIconIndirect(ref IconInfo icon);

        [DllImport("User32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Messages uMsg, int wParam, IntPtr lParam);

        [DllImport("User32.dll")]
        public static extern int SendMessage(IntPtr hWnd, uint uMsg, int wParam, IntPtr lParam);

        public static Cursor CreateCursor(Bitmap bmp, int xHotSpot, int yHotSpot)
        {
            IntPtr ptr = bmp.GetHicon();
            IconInfo tmp = new IconInfo();
            GetIconInfo(ptr, ref tmp);
            tmp.xHotspot = xHotSpot;
            tmp.yHotspot = yHotSpot;
            tmp.fIcon = false;
            ptr = CreateIconIndirect(ref tmp);
            return new Cursor(ptr);
        }
    }
}
