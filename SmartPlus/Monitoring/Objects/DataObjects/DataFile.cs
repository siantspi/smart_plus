﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus
{
    class DataFile : C2DObject
    {
        string FilePath;

        public DataFile(string Path, string Name)
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.DATAFILE;
            this.FilePath = Path;
            this.Name = Name;
        }

        public DataFile(DataFile file)
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.DATAFILE;
            this.FilePath = file.FilePath;
            this.Name = file.Name;
        }

        public string GetNameWithoutExt()
        {
            return Path.GetFileNameWithoutExtension(Name);
        }

        public void OpenFile()
        {
            string fullName = FilePath + "\\" + Name;
            if (File.Exists(fullName))
            {
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = fullName;
                proc.StartInfo.UseShellExecute = true;
                proc.Start(); 
            }
        }
    }
}
