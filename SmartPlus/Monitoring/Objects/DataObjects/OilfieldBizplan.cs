﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus
{
    public sealed class OilfieldBizplanCollection
    {
        OilfieldBizplan[] Items;
        bool DataLoaded;

        public OilfieldBizplanCollection()
        {
            Items = null;
        }
        public int Count { get { return (Items == null) ? 0 : Items.Length; } }
        public void Add(OilfieldBizplan BizPlan)
        {
            if (Items == null)
            {
                Items = new OilfieldBizplan[1];
                Items[0] = BizPlan;
            }
            else
            {
                OilfieldBizplan[] newItems = new OilfieldBizplan[Items.Length + 1];
                for (int i = 0; i < Items.Length; i++)
                {
                    newItems[i] = Items[i];
                }
                newItems[newItems.Length - 1] = BizPlan;
            }
        }
        public OilfieldBizplan GetLastBizPlan()
        {
            if(Items == null || Items.Length == 0) return null;
            return Items[Items.Length - 1];
        }
        #region Load Write
        public bool ReadFromCache(BinaryReader br, int Index)
        {
            try
            {
                List<OilfieldBizplan> bpList = new List<OilfieldBizplan>();
                int count = br.ReadInt32();
                int start = (int)br.BaseStream.Position;
                br.BaseStream.Seek(Index * 4, SeekOrigin.Current);
                int shift = br.ReadInt32();
                br.BaseStream.Seek(start, SeekOrigin.Current);
                Items[Index].ReadFromCache(br, true);
            }
            catch
            {
                return false;
            }
            return true;
        }
        public bool ReadHeadFromCache(BinaryReader br, bool LoadLastBizplan)
        {
            try
            {
                List<OilfieldBizplan> bpList = new List<OilfieldBizplan>();
                int count = br.ReadInt32();
                br.BaseStream.Seek(count * 4, SeekOrigin.Current);
                for (int i = 0; i < count; i++)
                {
                    OilfieldBizplan bp = new OilfieldBizplan();
                    if (bp.ReadFromCache(br, (LoadLastBizplan && (i == count - 1))))
                    {
                        bpList.Add(bp);
                    }
                }
                Items = bpList.ToArray();
            }
            catch
            {
                return false;
            }
            return true;
        }
        public bool ReadFromCache(BinaryReader br, bool LoadLastBizplan)
        {
            try
            {
                List<OilfieldBizplan> bpList = new List<OilfieldBizplan>();
                int count = br.ReadInt32();
                br.BaseStream.Seek(count * 4, SeekOrigin.Current);
                for (int i = 0; i < count; i++)
                {
                    OilfieldBizplan bp = new OilfieldBizplan();
                    if (bp.ReadFromCache(br, true))
                    {
                        bpList.Add(bp);
                    }
                }
                Items = bpList.ToArray();
            }
            catch
            {
                return false;
            }
            return true;
        }
        public bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(Items.Length);
                int[] pos = new int[Items.Length];
                int i, startPos = (int)bw.BaseStream.Position;
                bw.BaseStream.Seek(Items.Length * 4, SeekOrigin.Current);
                for (i = 0; i < Items.Length; i++)
                {
                    pos[i] = (int)bw.BaseStream.Position;
                    Items[i].WriteToCache(bw);
                }
                bw.BaseStream.Seek(startPos, SeekOrigin.Begin);
                for (i = 0; i < Items.Length; i++)
                {
                    bw.Write(pos[i]);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        #endregion
    }
    

    public sealed class OilfieldBizplan
    {
        public static int Version;

        public string Name;
        public DateTime ApprovalDate;
        public DateTime StartDate;
        public OilfieldBizplanLevel[] MonthLevels;
        public OilfieldBizplanLevel[] HalfYearLevels;

        static OilfieldBizplan() { Version = 0; }
        public OilfieldBizplan() { }

        public bool ReadFromCache(BinaryReader br, bool LoadData)
        {
            try
            {
                int version = br.ReadInt32();
                if(version > OilfieldBizplan.Version) return false;
                Name = br.ReadString();
                ApprovalDate = DateTime.FromOADate(br.ReadDouble());
                StartDate = DateTime.FromOADate(br.ReadDouble());
                if (LoadData)
                {
                    int count = br.ReadInt32();
                    MonthLevels = new OilfieldBizplanLevel[count];
                    for (int i = 0; i < count; i++)
                    {
                        MonthLevels[i].Liquid = br.ReadSingle();
                        MonthLevels[i].Oil = br.ReadSingle();
                        MonthLevels[i].Injection = br.ReadSingle();
                    }
                    count = br.ReadInt32();
                    HalfYearLevels = new OilfieldBizplanLevel[count];
                    for (int i = 0; i < count; i++)
                    {
                        HalfYearLevels[i].Liquid = br.ReadSingle();
                        HalfYearLevels[i].Oil = br.ReadSingle();
                        HalfYearLevels[i].Injection = br.ReadSingle();
                    }
                    //if (Version > 0) { }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(OilfieldBizplan.Version);
                bw.Write(Name);
                bw.Write(ApprovalDate.ToOADate());
                bw.Write(StartDate.ToOADate());
                bw.Write(MonthLevels.Length);
                for (int i = 0; i < MonthLevels.Length; i++)
                {
                    bw.Write(MonthLevels[i].Liquid);
                    bw.Write(MonthLevels[i].Oil);
                    bw.Write(MonthLevels[i].Injection);
                }
                bw.Write(HalfYearLevels.Length);
                for (int i = 0; i < HalfYearLevels.Length; i++)
                {
                    bw.Write(HalfYearLevels[i].Liquid);
                    bw.Write(HalfYearLevels[i].Oil);
                    bw.Write(HalfYearLevels[i].Injection);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
    public struct OilfieldBizplanLevel
    {
        public float Liquid;    // тыс. т
        public float Oil;       // тыс. т
        public float Injection; // тыс. м3
    }
}
