﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus
{
    public struct WellTrajectoryItem
    {
        public float X;
        public float Y;
        public float Z;
        public float Zabs;
    }
    public sealed class WellTrajectory : WellDataObject<WellTrajectoryItem>
    {
        public static int Version = 0;

        public bool LoadFromCache(BinaryReader br, int Version)
        {
            try
            {
                int count = br.ReadInt32();
                WellTrajectoryItem[] newItems = new WellTrajectoryItem[count];
                for (int i = 0; i < count; i++)
                {
                    newItems[i].X = br.ReadSingle();
                    newItems[i].Y = br.ReadSingle();
                    newItems[i].Z = br.ReadSingle();
                    newItems[i].Zabs = br.ReadSingle();
                }
                SetItems(newItems);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(Count);
                for (int i = 0; i < Items.Length; i++)
                {
                    bw.Write(Items[i].X);
                    bw.Write(Items[i].Y);
                    bw.Write(Items[i].Z);
                    bw.Write(Items[i].Zabs);
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }
    }
}
