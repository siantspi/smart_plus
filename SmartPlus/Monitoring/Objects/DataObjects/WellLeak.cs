﻿using System;
using System.IO;

namespace SmartPlus
{
    public struct Interval
    {
        public float Top;
        public float Bottom;
    }
    public struct WellLeakItem
    {
        public DateTime Date;
        public ushort LeakType;
        public ushort LeakHoleType;
        public ushort OperationCode;
        public Interval[] Intervals;
        public string Comment;

        public int GetLength()
        {
            return (8 + 2 * 3 + 4 + ((Intervals == null) ? 0 : Intervals.Length * 8)  + 4 + ((Comment == null) ? 0 : System.Text.Encoding.UTF8.GetByteCount(Comment)));
        }
    }
    public sealed class WellLeak: WellDataObject<WellLeakItem>
    {
        public static int Version = 0;

        public int GetLength()
        {
            int length = 0;
            for (int i = 0; i < Count; i++)
            {
                length += this[i].GetLength();
            }
            return length;
        }
        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            try
            {
                int count = br.ReadInt32(), count2;
                WellLeakItem[] newItems = new WellLeakItem[count];
                for (int i = 0; i < count; i++)
                {
                    newItems[i].Date = DateTime.FromOADate(br.ReadDouble());
                    newItems[i].LeakHoleType = br.ReadUInt16();
                    newItems[i].LeakType = br.ReadUInt16();
                    newItems[i].OperationCode = br.ReadUInt16();
                    count2 = br.ReadInt32();
                    newItems[i].Intervals = new Interval[count2];
                    for (int j = 0; j < count2; j++)
                    {
                        newItems[i].Intervals[j].Top = br.ReadSingle();
                        newItems[i].Intervals[j].Bottom = br.ReadSingle();
                    }
                    count2 = br.ReadInt32();
                    char[] buff = br.ReadChars(count2);
                    newItems[i].Comment = new string(buff);
                }
                SetItems(newItems);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(Count);
                for (int i = 0; i < Items.Length; i++)
                {
                    bw.Write(Items[i].Date.ToOADate());
                    bw.Write(Items[i].LeakHoleType);
                    bw.Write(Items[i].LeakType);
                    bw.Write(Items[i].OperationCode);
                    if (Items[i].Intervals != null)
                    {
                        bw.Write(Items[i].Intervals.Length);
                        for (int j = 0; j < Items[i].Intervals.Length; j++)
                        {
                            bw.Write(Items[i].Intervals[j].Top);
                            bw.Write(Items[i].Intervals[j].Bottom);
                        }
                    }
                    else
                    {
                        bw.Write(0);
                    }
                    if (Items[i].Comment != null)
                    {
                        char[] buff = Items[i].Comment.ToCharArray();
                        bw.Write(buff.Length);
                        bw.Write(buff);
                    }
                    else
                    {
                        bw.Write(0);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        static public string GetOperationName(int OpeartionCode)
        {
            switch (OpeartionCode)
            {
                case 0:
                    return "Не устранена";
                case 1:
                    return "Замена части обс.колонны";
                case 2:
                    return "Наращивание ЦК";
                case 3:
                    return "Отсечение пакером";
                case 4:
                    return "РИР";
                case 5:
                    return "Сварка";
                case 6:
                    return "Спуск доп.колонны меньшего диаметра";
                case 7:
                    return "Тампонирование";
                case 8:
                    return "Установка металл.пластыря";
                case 9:
                    return "Установка ЦМ";
                case 10:
                    return "Устранена";
                case 11:
                    return "Цем.заливка негермет.ЦК ч/з негермет. без оставления ЦМ";
                case 12:
                    return "Цем.заливка негермет.ЦК ч/з негермет. с оставлением ЦМ";
                case 13:
                    return "Цем.заливка негермет.ЦК ч/з перф.интервал без оставления ЦМ";
                case 14:
                    return "Цем.заливка негермет.ЦК ч/з перф.интервал с оставлением ЦМ";
                case 15:
                    return "Цем.заливка негермет.ЦК ч/з спецотв. без оставления ЦМ";
                case 16:
                    return "Цем.заливка негермет.ЦК ч/з спецотв. с оставлением ЦМ";
            }
            return string.Empty;
        }
        static public string GetLeakHoleName(int LeakHoleCode)
        {
            switch (LeakHoleCode)
            {
                case 0:
                    return "Н/Д";
                case 1:
                    return "Спецотверстие";
                case 2:
                    return "Порыв колонны";
                case 3:
                    return "Негерм.муфт.соединений";
                case 4:
                    return "Изолир.интервал перфорации";
                case 5:
                    return "Ранее устраненная негерметичн.";
            }
            return string.Empty;
        }
    }
}
