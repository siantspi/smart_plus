﻿using System;
using System.Collections.Generic;
using System.Text;
using RDF.Objects;

namespace Monitoring
{
    public class WellMerComp
    {
        MerCompItem[] Items;
        public int Count
        {
            get
            {
                if (Items == null)
                {
                    return 0;
                }
                else
                {
                    return Items.Length;
                }
            }
        }
        public MerCompItem this[int index]
        {
            get
            {
                if (index > -1 || index < Count)
                {
                    return Items[index];
                }
                else
                {
                    throw new IndexOutOfRangeException("Индекс за пределами диапазона");
                }
            }
        }

        public WellMerComp(Mer Mer)
        {
            if ((Mer != null) && (Mer.Count > 0))
            {
                DateTime currDate;
                DateTime dt = Mer.Items[0].Date, dt2 = Mer.Items[Mer.Count - 1].Date;
                int count = (dt2.Year - dt.Year) * 12 + dt2.Month - dt.Month + 1;
                Items = new MerCompItem[count];
                int i = 0, j = 0;
                currDate = dt;
                while (i < Mer.Count)
                {
                    dt = Mer.Items[i].Date;
                    while (currDate < dt)
                    {
                        Items[j] = new MerCompItem(currDate);
                        currDate = currDate.AddMonths(1);
                        j++;
                    }
                    Items[j] = new MerCompItem(dt);
                    while (i < Mer.Count && Mer.Items[i].Date == dt)
                    {
                        Items[j].RetriveItem(Mer, i);
                        i++;
                    }
                    j++;
                    currDate = currDate.AddMonths(1);
                }
            }
        }
    }
    public class MerCompItem
    {
        public DateTime Date;
        public int CharWorkId;
        public int StateId;
        public int MethodId;
        public List<MerCompPlastItem> PlastItems;
        public int SumWorkTimeProd
        {
            get
            {
                int sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].WorkTimeProd;
                }
                return sum;
            }
        }
        public int SumWorkTimeInj
        {
            get
            {
                int sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].WorkTimeProd;
                }
                return sum;
            }
        }
        public double SumLiquid
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Liq;
                }
                return sum;
            }
        }
        public double SumOil
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Oil;
                }
                return sum;
            }
        }
        public double SumInjection
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Inj;
                }
                return sum;
            }
        }
        public double SumNaturalGas
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].NatGas;
                }
                return sum;
            }
        }
        public double SumGasCondensate
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Condensate;
                }
                return sum;
            }
        }

        public MerCompItem(DateTime Date)
        {
            this.Date = Date;
            PlastItems = new List<MerCompPlastItem>();
        }
        public void RetriveItem(Mer mer, int index)
        {

            if (StateId != mer.Items[index].StateId) StateId = mer.Items[index].StateId;
            if (MethodId != mer.Items[index].MethodId) MethodId = mer.Items[index].MethodId;
            if (CharWorkId != mer.Items[index].CharWorkId) CharWorkId = mer.Items[index].CharWorkId;
            MerCompPlastItem plastItem = GetItem(mer.Items[index].PlastId);
            
            if (mer.Items[index].CharWorkId != 20)
            {
                if(plastItem.TimeCollectionProd == null)
                {
                    plastItem.TimeCollectionProd = new MerPlastWorkTime();
                }
                plastItem.TimeCollectionProd.Add(mer.Items[index]);
            }
            else
            {
                if (plastItem.TimeCollectionInj == null)
                {
                    plastItem.TimeCollectionInj = new MerPlastWorkTime();
                }
                plastItem.TimeCollectionInj.Add(mer.Items[index]);
            }

            plastItem.Liq += mer.Items[index].Wat + mer.Items[index].Oil;
            plastItem.Oil += mer.Items[index].Oil;
            plastItem.LiqV += mer.Items[index].WatV + mer.Items[index].OilV;
            plastItem.OilV += mer.Items[index].OilV;
            if (mer.CountEx > 0)
            {
                plastItem.NatGas += mer.GetItemEx(index).NaturalGas;
                plastItem.Inj += mer.GetItemEx(index).Injection;
                plastItem.Condensate += mer.GetItemEx(index).GasCondensate;
            }
        }
        public MerCompPlastItem GetItem(int PlastCode)
        {
            int ind = -1;
            for (int i = 0; i < PlastItems.Count; i++)
            {
                if (PlastItems[i].PlastCode == PlastCode)
                {
                    ind = i;
                }
            }
            if (ind == -1)
            {
                ind = PlastItems.Count;
                int maxHours = (this.Date.AddMonths(1) - this.Date).Days * 24;
                PlastItems.Add(new MerCompPlastItem(this));
                PlastItems[ind].PlastCode = PlastCode;
            }
            return PlastItems[ind];
        }
        public void Clear()
        {
            Date = DateTime.MinValue;
            CharWorkId = 0;
            StateId = 0;
            MethodId = 0;
            PlastItems.Clear();
        }
    }
    public class MerCompPlastItem
    {
        public MerCompItem ParentItem;
        public int PlastCode;
        public double Liq;
        public double Oil;
        public double LiqV;
        public double OilV;
        public double Inj;
        public double NatGas;
        public double Condensate;
        public MerPlastWorkTime TimeCollectionProd, TimeCollectionInj;
        public int WorkTimeProd
        {
            get
            {
                int workTime = 0;
                if (TimeCollectionProd != null)
                {
                    workTime = TimeCollectionProd.WorkTime + TimeCollectionProd.CollTime;
                    int MaxHours = (ParentItem.Date.AddMonths(1) - ParentItem.Date).Days * 24;
                    if (workTime > MaxHours) workTime = MaxHours;
                }
                return workTime;
            }
        }
        public int WorkTimeInj
        {
            get
            {
                int workTime = 0;
                if (TimeCollectionInj != null)
                {
                    workTime = TimeCollectionInj.WorkTime + TimeCollectionInj.CollTime;
                    int MaxHours = (ParentItem.Date.AddMonths(1) - ParentItem.Date).Days * 24;
                    if (workTime > MaxHours) workTime = MaxHours;
                }
                return workTime;
            }
        }

        public MerCompPlastItem(MerCompItem MerExItem)
        {
            this.ParentItem = MerExItem;
        }
    }
    public class MerPlastWorkTime
    {
        int _WorkTime, _CollTime;
        List<MerWorkTimeItem> Items;
        public MerPlastWorkTime()
        {
            _WorkTime = 0;
            _CollTime = 0;
            Items = new List<MerWorkTimeItem>();
        }

        public void Add(MerItem MerItem)
        {
            bool find = false;
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Equals(MerItem))
                {
                    find = true;
                    break;
                }
            }
            if (!find)
            {
                MerWorkTimeItem timeObj = new MerWorkTimeItem();
                timeObj.Retrieve(MerItem);
                Items.Add(timeObj);
                _WorkTime += timeObj.WorkTime;
                _CollTime += timeObj.CoolTime;
            }
        }
        public void Clear()
        {
            Items.Clear();
            _CollTime = 0;
            _WorkTime = 0;
        }
        public int WorkTime
        {
            get { return _WorkTime; }
        }
        public int CollTime
        {
            get { return _CollTime; }
        }
    }
    public struct MerWorkTimeItem
    {
        int CharWorkId;
        int MethodId;
        int StateId;
        public int WorkTime;
        public int CoolTime;

        public void Retrieve(MerItem MerItem)
        {
            CharWorkId = MerItem.CharWorkId;
            StateId = MerItem.StateId;
            MethodId = MerItem.MethodId;
            WorkTime = MerItem.WorkTime;
            CoolTime = MerItem.CollTime;
        }
        public bool Equals(MerItem MerItem)
        {
            return ((CharWorkId == MerItem.CharWorkId) &&
                    (StateId == MerItem.StateId) &&
                    (MethodId == MerItem.MethodId) &&
                    (WorkTime == MerItem.WorkTime) &&
                    (CoolTime == MerItem.CollTime));
        }
    }    
}
