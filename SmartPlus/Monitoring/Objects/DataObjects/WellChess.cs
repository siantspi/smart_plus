﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using RDF;

namespace SmartPlus
{
    #region CHESS
    public struct ChessItem
    {
        public DateTime Date;
        public float Qliq;
        public float Qoil;
        public float QliqV;
        public float QoilV;
        public float Hdyn;
        public int StayTime;
        public int StayReason;
    }
    public sealed class WellChess : WellDataObject<ChessItem>
    {
        public static int Version = 2;

        public WellChess() { }
        public WellChess(ChessItem[] DailyItems)
        {
            Items = DailyItems;
        }
        public bool ReadFromRDF(RdfConnector connector, string key)
        {
            int i, j;
            MemoryStream ms = new MemoryStream();
            ms = connector.ReadBinary(key, false);

            if (ms == null) return false;

            ms.Seek(0, SeekOrigin.Begin);
            BinaryReader br = new BinaryReader(ms);

            Int16 Version = br.ReadInt16();

            if (Version != 1) return false;
            br.ReadDouble(); // read GUID
            br.ReadDouble(); // read GUID

            Version = br.ReadInt16();
            int fieldsCount = br.ReadInt32();
            if (fieldsCount == 0) return false;
            string fieldCap;
            string[] fieldNames = new string[fieldsCount];
            int[] fieldSizes = new int[fieldsCount];
            for (i = 0; i < fieldsCount; i++)
            {
                Version = br.ReadInt16();
                fieldNames[i] = ConvertEx.ReadStrUI16(ms, RdfConnector.encoding);
                fieldCap = ConvertEx.ReadStrUI16(ms, RdfConnector.encoding);
                br.ReadByte();                  // fieldType
                fieldSizes[i] = br.ReadInt32(); // fieldSize
                br.ReadByte();                  // Frequired
            }
            byte packed = br.ReadByte();
            if (packed != 0) return false;
            int rowCount = br.ReadInt32(), col;
            int[] dateIndexes = new int[rowCount];
            ChessItem[] items = new ChessItem[rowCount];

            int[] charworks = new int[rowCount];
            double[] watering = new double[rowCount];
            DateTime dt = DateTime.MinValue;
            try
            {
                for (j = 0; j < fieldNames.Length; j++)
                {
                    switch (fieldNames[j])
                    {
                        case "DATE":
                            for (i = 0; i < rowCount; i++)
                            {
                                dt = DateTime.FromOADate(br.ReadDouble());
                                items[i].Date = new DateTime(dt.Year, dt.Month, dt.Day);
                            }
                            break;
                        case "KCHARWORK":
                            for (i = 0; i < rowCount; i++) charworks[i] = br.ReadInt32();
                            break;
                        case "LIQ_RATE":
                            for (i = 0; i < rowCount; i++) items[i].QliqV = (float)br.ReadDouble(); // m3/сут
                            break;
                        case "WATER_CUT":
                            for (i = 0; i < rowCount; i++) watering[i] = br.ReadDouble(); // весовая 
                            break;
                        case "OIL_RATE":
                            for (i = 0; i < rowCount; i++) items[i].Qoil = (float)br.ReadDouble(); // т/сут
                            break;
                        case "H_DYN":
                            for (i = 0; i < rowCount; i++) items[i].Hdyn = (float)br.ReadDouble();
                            break;
                        default:
                            br.BaseStream.Seek(rowCount * fieldSizes[j], SeekOrigin.Current);
                            break;
                    }
                }
                for (i = 0; i < rowCount; i++)
                {
                    items[i].QoilV = (float)(items[i].QliqV * (1 - watering[i] / 100));
                }
                List<ChessItem> newItems = new List<ChessItem>();
                dt = DateTime.MinValue;
                for (i = 0; i < rowCount; i++)
                {
                    if (charworks[i] == 11)
                    {
                        if (items[i].Date != dt)
                        {
                            newItems.Add(items[i]);
                            dt = items[i].Date;
                        }
                        else
                        {
                            newItems[newItems.Count - 1] = items[i];
                        }
                    }
                }
                Items = newItems.ToArray();
            }
            catch
            {
            }
            finally
            {
                br.Close();
                ms.Dispose();
            }
            return (Items.Length > 0);
        }

        public int GetIndexByDate(DateTime Date, int startIndex)
        {
            int index = -1;
            if (startIndex < 0) startIndex = 0;
            for (int i = startIndex; i < Items.Length; i++)
            {
                if (Items[i].Date > Date) break;
                index = i;
            }
            return index;
        }
        public List<ChessItem> GetItemsByDate(DateTime MinDate)
        {
            List<ChessItem> items = new List<ChessItem>();
            for (int i = Items.Length - 1; i >= 0; i--)
            {
                if (Items[i].Date < MinDate) break;
                items.Add(Items[i]);
            }
            return items;
        }
        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            int count = br.ReadInt32();
            ChessItem[] items = new ChessItem[count];
            for (int i = 0; i < items.Length; i++)
            {
                items[i].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                items[i].Qliq = br.ReadSingle();
                items[i].Qoil = br.ReadSingle();
                items[i].QliqV = br.ReadSingle();
                items[i].QoilV = br.ReadSingle();
                items[i].StayTime = br.ReadByte();
                items[i].Hdyn = br.ReadSingle();
                items[i].StayReason = br.ReadInt32();
            }
            SetItems(items);
            return true;
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            bw.Write(Items.Length);
            for (int i = 0; i < Items.Length; i++)
            {
                bw.Write(RdfSystem.PackDateTime(Items[i].Date));
                bw.Write(Items[i].Qliq);
                bw.Write(Items[i].Qoil);
                bw.Write(Items[i].QliqV);
                bw.Write(Items[i].QoilV);
                bw.Write((byte)Items[i].StayTime);
                bw.Write(Items[i].Hdyn);
                bw.Write(Items[i].StayReason);
            }
            return true;
        }
    }
    #endregion

    #region CHESS INJECTION
    public struct ChessInjItem
    {
        public DateTime Date;
        public float Wwat;
        public float Pust;
        public int CycleTime;
        public int StayTime;
        public int StayReason;
    }
    public sealed class WellChessInj : WellDataObject<ChessInjItem>
    {
        // todo : Загрузка и отображение устьевых давлений на наг скважинах
        public static int Version = 2;

        public WellChessInj() { }
        public WellChessInj(ChessInjItem[] DailyItems)
        {
            Items = DailyItems;
        }

        public bool ReadFromRDF(RdfConnector connector, string key)
        {
            int i, j;
            MemoryStream ms = new MemoryStream();
            ms = connector.ReadBinary(key, false);

            if (ms == null) return false;

            ms.Seek(0, SeekOrigin.Begin);
            BinaryReader br = new BinaryReader(ms);

            Int16 Version = br.ReadInt16();

            if (Version != 1) return false;
            br.ReadDouble(); // read GUID
            br.ReadDouble(); // read GUID

            Version = br.ReadInt16();
            int fieldsCount = br.ReadInt32();
            if (fieldsCount == 0) return false;
            string fieldCap;
            string[] fieldNames = new string[fieldsCount];
            int[] fieldSizes = new int[fieldsCount];
            bool IsInj = false;
            for (i = 0; i < fieldsCount; i++)
            {
                Version = br.ReadInt16();
                fieldNames[i] = ConvertEx.ReadStrUI16(ms, RdfConnector.encoding);
                if (!IsInj && fieldNames[i] == "INTAKE") IsInj = true;
                fieldCap = ConvertEx.ReadStrUI16(ms, RdfConnector.encoding);
                br.ReadByte();                  // fieldType
                fieldSizes[i] = br.ReadInt32(); // fieldSize
                br.ReadByte();                  // Frequired
            }
            if (!IsInj)
            {
                br.Close();
                ms.Dispose();
                return false;
            }

            byte packed = br.ReadByte();
            if (packed != 0) return false;
            int rowCount = br.ReadInt32();
            int[] dateIndexes = new int[rowCount];
            ChessInjItem[] items = new ChessInjItem[rowCount];

            int[] charworks = new int[rowCount];
            double[] watering = new double[rowCount];
            DateTime dt = DateTime.MinValue;
            try
            {
                for (j = 0; j < fieldNames.Length; j++)
                {
                    switch (fieldNames[j])
                    {
                        case "DATE":
                            for (i = 0; i < rowCount; i++)
                            {
                                dt = DateTime.FromOADate(br.ReadDouble());
                                items[i].Date = new DateTime(dt.Year, dt.Month, dt.Day);
                            }
                            break;
                        case "KCHARWORK":
                            for (i = 0; i < rowCount; i++) charworks[i] = br.ReadInt32();
                            break;
                        case "INTAKE":
                            for (i = 0; i < rowCount; i++) items[i].Wwat = (float)br.ReadDouble(); // m3/сут
                            break;
                        default:
                            br.BaseStream.Seek(rowCount * fieldSizes[j], SeekOrigin.Current);
                            break;
                    }
                }

                List<ChessInjItem> newItems = new List<ChessInjItem>();
                dt = DateTime.MinValue;
                for (i = 0; i < rowCount; i++)
                {
                    if (charworks[i] == 20)
                    {
                        if (items[i].Date != dt)
                        {
                            newItems.Add(items[i]);
                            dt = items[i].Date;
                        }
                        else
                        {
                            newItems[newItems.Count - 1] = items[i];
                        }
                    }
                }
                Items = newItems.ToArray();
            }
            catch
            {
            }
            finally
            {
                br.Close();
                ms.Dispose();
            }
            return (Items.Length > 0);
        }

        public List<ChessInjItem> GetItemsByDate(DateTime MinDate)
        {
            List<ChessInjItem> items = new List<ChessInjItem>();
            for (int i = Items.Length - 1; i >= 0; i--)
            {
                if (Items[i].Date < MinDate) break;
                items.Add(Items[i]);
            }
            return items;
        }
        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            int count = br.ReadInt32();
            ChessInjItem[] items = new ChessInjItem[count];
            for (int i = 0; i < items.Length; i++)
            {
                items[i].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                items[i].Wwat = br.ReadSingle();
                items[i].Pust = br.ReadSingle();
                items[i].CycleTime = br.ReadByte();
                items[i].StayTime = br.ReadByte();
                items[i].StayReason = br.ReadInt32();
            }
            Items = items;
            return true;
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            bw.Write(Items.Length);
            for (int i = 0; i < Items.Length; i++)
            {
                bw.Write(RdfSystem.PackDateTime(Items[i].Date));
                bw.Write(Items[i].Wwat);
                bw.Write(Items[i].Pust);
                bw.Write((byte)Items[i].CycleTime);
                bw.Write((byte)Items[i].StayTime);
                bw.Write(Items[i].StayReason);
            }
            return true;
        }
    }
    #endregion
}
