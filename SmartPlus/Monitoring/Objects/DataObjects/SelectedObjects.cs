﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Drawing;
using SmartPlus;
using System.Collections;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using System.Globalization;
using SmartPlus;

namespace Selecting
{
    #region WELL
    [ObfuscationAttribute(Feature = "renaming", Exclude = true, ApplyToMembers = true)]
    class SelectedWell : Object
    {
        __ ___;
        string _oilFieldAreaName;
        string _oilFieldName;
        string _name;
        bool _visibleName;
        bool _visibleHBubbleColumns;
        bool _visibleText;
        ArrayList _icons;

        string fontName;
        int fontSize;


        public SelectedWell(__ _) {  this.___ = _; }

        [DisplayName("Номер скважины")]
        [Category("Информация")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DisplayName("Месторождение")]
        [Category("Информация")]
        public string OilFieldName
        {
            get { return _oilFieldName; }
            set { _oilFieldName = value; }
        }

        [DisplayName("Площадь")]
        [Category("Информация")]
        public string OilFieldAreaName
        {
            get { return _oilFieldAreaName; }
            set { _oilFieldAreaName = value; }
        }

        [DisplayName("Значок")]
        [Category("Отображение")]
        [Editor(typeof(WellIconEditor), typeof(UITypeEditor))]
        public ArrayList Icon
        {
            get { return _icons; }
            set { _icons = value; _(); }
        }

        [DisplayName("Отображать имя")]
        [Category("Отображение")]
        [TypeConverter(typeof(BooleanYesNoConverter))]
        public bool VisibleName
        {
            get { return _visibleName; }
            set { _visibleName = value; _(); }
        }

        [DisplayName("Отображать подписи")]
        [Category("Карты отборов")]
        [TypeConverter(typeof(BooleanYesNoConverter))]
        public bool VisibleText
        {
            get { return _visibleText; }
            set { _visibleText = value; _(); }
        }

        [DisplayName("Отображать столбики изменений")]
        [Category("Карты отборов")]
        [TypeConverter(typeof(BooleanYesNoConverter))]
        public bool VisibleHBubbleCol
        {
            get { return _visibleHBubbleColumns; }
            set { _visibleHBubbleColumns = value; _(); }
        }

        [DisplayName("Имя")]
        [Category("Шрифт")]
        public string FontName
        {
            get { return fontName; }
            set { fontName = value; }
        }

        [DisplayName("Размер")]
        [Category("Шрифт")]
        public int FontSize
        {
            get { return fontSize; }
            set { fontSize = value; }
        }

        public void _() { ___._(this, 11); }
    }
    #endregion

    #region AREA
    [ObfuscationAttribute(Feature = "renaming", Exclude = true, ApplyToMembers = true)]
    class SelectedArea : Object
    {
        string _oilFieldAreaName;
        string _oilFieldName;
        string _name;

        [DisplayName("Имя ячейки")]
        [Category("Информация")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DisplayName("Месторождение")]
        [Category("Информация")]
        public string OilFieldName
        {
            get { return _oilFieldName; }
            set { _oilFieldName = value; }
        }

        [DisplayName("Площадь")]
        [Category("Информация")]
        public string OilFieldAreaName
        {
            get { return _oilFieldAreaName; }
            set { _oilFieldAreaName = value; }
        }
    }
    #endregion

    #region WELL LIST
    [ObfuscationAttribute(Feature = "renaming", Exclude = true, ApplyToMembers = true)]
    class SelectedWellList : Object
    {
        __ ___;
        int _wellCount;
        string _name;

        int _iconIndex;
        int _iconSize;
        bool _visibleName;
        Color _iconColor;
        ArrayList _icons;


        public SelectedWellList(__ _) { this.___ = _; }

        [DisplayName("Имя слоя")]
        [Category("Информация")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DisplayName("Количество скважин")]
        [Category("Информация")]
        public int WellCount
        {
            get { return _wellCount; }
            set { _wellCount = value; }
        }

        [DisplayName("Значок")]
        [Category("Отображение")]
        [Editor(typeof(WellIconEditor), typeof(UITypeEditor))]
        public ArrayList Icon
        {
            get { return _icons; }
            set { _icons = value; _(); }
        }

        [DisplayName("Отображать имя")]
        [Category("Отображение")]
        [TypeConverter(typeof(BooleanYesNoConverter))]
        public bool VisibleName
        {
            get { return _visibleName; }
            set { _visibleName = value; _(); }
        }

        public void _() { ___._(this, 15); }
    }
    #endregion

    public class WellIconEditor : UITypeEditor
    {

        public override Object EditValue(ITypeDescriptorContext context, IServiceProvider provider, Object value)
        {
            if ((context != null) && (provider != null))
            {
                IWindowsFormsEditorService svc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

                if (svc != null)
                {
                    ArrayList paramsList = (ArrayList)value;
                    if (paramsList != null && paramsList.Count > 0)
                    {
                    using (IconListForm fIconList = new IconListForm((ArrayList)value, true))
                    {
                        if (svc.ShowDialog(fIconList) == DialogResult.OK)
                        {
                            ArrayList list = (ArrayList)fIconList.iconList;
                            ArrayList res = new ArrayList();
                            if (list.Count > 0)
                            {
                                for (int i = 0; i < list.Count; i++) res.Add((WellIconCode)list[i]);
                                    res.Add((FontFamily[])paramsList[paramsList.Count - 1]);
                                value = res;
                            }
                            else
                                value = null;
                        }
                    }
                }
            }
            }

            return base.EditValue(context, provider, value);
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if (context != null)
                return UITypeEditorEditStyle.Modal;
            else
                return base.GetEditStyle(context);
        }

        public override bool GetPaintValueSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override void PaintValue(PaintValueEventArgs e)
        {
            ArrayList iconList = (ArrayList)e.Value;
            Graphics grfx = e.Graphics;
            SizeF iconSize = SizeF.Empty;
            string s;
            Font f;
            WellIconCode icon;
            int i;

            if ((iconList == null) || (iconList.Count < 2)) return;

            int maxSize = 0;
            SolidBrush br;
            FontFamily[] FontFamilyList = (FontFamily[])iconList[iconList.Count - 1];
            for (i = 0; i < iconList.Count - 1; i++)
            {
                icon = (WellIconCode)iconList[i];
                if (maxSize < icon.Size) maxSize = icon.Size * 2;
            }
            if (maxSize == 0) return;

            int size = maxSize + maxSize / 4;
            if (size < e.Bounds.Height) size = e.Bounds.Height;
            using (Bitmap bmp = new Bitmap(size, size))
            {
                Graphics gr = Graphics.FromImage(bmp);

                float x = size / 2, y = size / 2;
                Rectangle rect = Rectangle.Empty;
                Rectangle destRect = e.Bounds;

                destRect.X = destRect.X + (int)((destRect.Width - destRect.Height) / 2);
                destRect.Width = destRect.Height;
                rect.Width = size;
                rect.Height = size;
                gr.Clear(Color.White);

                for (i = 0; i < iconList.Count - 1; i++)
                {
                    icon = (WellIconCode)iconList[i];
                    s = Convert.ToChar(icon.CharCode).ToString();
                    f = new Font(FontFamilyList[icon.FontCode], icon.Size * 2);

                    br = new SolidBrush(Color.FromArgb(icon.IconColor));
                    iconSize = grfx.MeasureString(s, f, PointF.Empty, StringFormat.GenericTypographic);

                    gr.DrawString(s, f, br, x - iconSize.Width / 2, y - iconSize.Height / 2, StringFormat.GenericTypographic);
                    f.Dispose();
                    br.Dispose();
                }

                grfx.DrawImage(bmp, destRect, rect, GraphicsUnit.Pixel);
            }
            FontFamilyList = null;
        }
    }

    public class BooleanYesNoConverter : BooleanConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture,
                                            object value, Type destType)
        {
            return (bool)value ?
              "Да" : "Нет";
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
                                            CultureInfo culture, object value)
        {
            return (string)value == "Да";
        }
    }
    
    public sealed class __
    {
        CanvasMap canv;
        public bool SetParams;
        public __(CanvasMap canv)
        {
            this.canv = canv;
            SetParams = false;
        }

        public void _(object obj, int ind)
        {
            if(!SetParams) canv.SelObjUpdate(obj, ind);
        }

    }
}

