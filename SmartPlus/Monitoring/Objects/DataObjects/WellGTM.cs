﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus
{
    public struct WellGTMItem
    {
        public DateTime Date;
        public ushort GtmCode;
    }
    public sealed class WellGTM : WellDataObject<WellGTMItem>
    {
        public static int Version = 0;
        public bool LoadFromCache(BinaryReader br, int Version)
        {
            try
            {
                int count = br.ReadInt32();
                WellGTMItem[] newItems = new WellGTMItem[count];
                for (int i = 0; i < count; i++)
                {
                    newItems[i].Date = DateTime.FromOADate(br.ReadDouble());
                    newItems[i].GtmCode = br.ReadUInt16();
                }
                SetItems(newItems);
                //if (Version > 0) { }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(Count);
                for (int i = 0; i < Items.Length; i++)
                {
                    bw.Write(Items[i].Date.ToOADate());
                    bw.Write(Items[i].GtmCode);
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public WellGTMItem[] ToArray()
        {
            WellGTMItem[] newItems = new WellGTMItem[this.Count];
            for (int i = 0; i < this.Count; i++)
            {
                newItems[i] = this[i];
            }
            return newItems;
        }
    }
}
