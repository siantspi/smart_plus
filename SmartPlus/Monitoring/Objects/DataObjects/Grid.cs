﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.ComponentModel;
using System.Collections;
using SmartPlus.DictionaryObjects;
using RDF;

namespace SmartPlus
{
    public sealed class Grid : C2DObject
    {
        public enum GridOperations : int
        {
            Addition,
            Substraction,
            Multiplication,
            Division
        }
        public static int Version = 0;
        internal class SmallGrid : C2DObject
        {
            double angle, sinAngle, cosAngle;
            public double Angle
            {
                get { return angle; }
                set
                {
                    angle = value;
                    if (value != 0)
                    {
                        sinAngle = Math.Sin(angle);
                        cosAngle = Math.Cos(angle);
                    }
                    else
                    {
                        sinAngle = 1;
                        cosAngle = 1;
                    }
                }
            }
            
            public double Xmin, Ymin;
            public double dx, dy;
            public float blank;
            public int stRow, stCol, maxRow, maxCol, nx, ny;
            public float[][] ZValue;
            public int[][] points;  // используется для оптимизации при загрузке
            public bool DrawGridLines;

            Palette currPal;
            Bitmap bmp = null;

            public SmallGrid()
            {
                stRow = 0;
                stCol = 0;
                nx = 0;
                ny = 0;
                ZValue = null;
                points = null;
                blank = 0;
                Angle = 0;
            }
            public SmallGrid(int startRow, int startCol, float BlankVal)
            {
                stRow = startRow;
                stCol = startCol;
                nx = 1; ny = 1;
                ZValue = null;
                points = null;
                blank = BlankVal;
            }
            public SmallGrid(SmallGrid grid) : this()
            {
                Xmin = grid.Xmin;
                Ymin = grid.Ymin;
                dx = grid.dx;
                dy = grid.dy;
                blank = grid.blank;
                stRow = grid.stRow;
                stCol = grid.stCol;
                maxRow = grid.maxRow;
                maxCol = grid.maxCol;
                nx = grid.nx;
                ny = grid.ny;
                if (grid.ZValue != null)
                {
                    ZValue = new float[grid.ZValue.Length][];
                    for (int i = 0; i < grid.ZValue.Length; i++)
                    {
                        ZValue[i] = new float[grid.ZValue[i].Length];
                        for (int j = 0; j < grid.ZValue[i].Length; j++)
                        {
                            ZValue[i][j] = grid.ZValue[i][j];
                        }
                    }
                }
                if (grid.points != null)
                {
                    points = new int[grid.points.Length][];
                    for (int i = 0; i < grid.points.Length; i++)
                    {
                        points[i] = new int[grid.points[i].Length];
                        for (int j = 0; j < grid.points[i].Length; j++)
                        {
                            points[i][j] = grid.points[i][j];
                        }
                    }
                }
                if (grid.bmp != null) bmp = (Bitmap)grid.bmp.Clone();
            }

            public void AddPointList(List<Point> pointList, float[][] Zvalue)
            {
                Point pt;
                int i, j;
                int[] point;
                bool find = false;
                List<int> rowList = new List<int>();
                List<int[]> dataList = new List<int[]>();
                for (i = 0; i < pointList.Count; i++)
                {
                    pt = (Point)pointList[i];
                    find = false;
                    for (j = 0; j < rowList.Count; j++)
                    {
                        if (rowList[j] == pt.X)
                        {
                            point = dataList[j];
                            if (point[0] > pt.Y) point[0] = pt.Y;
                            if (point[1] < pt.Y) point[1] = pt.Y;
                            find = true;
                        }
                    }
                    if (!find)
                    {
                        for (j = 0; j < rowList.Count; j++)
                        {
                            if (rowList[j] < pt.X)
                            {
                                rowList.Insert(j, pt.X);
                                point = new int[2];
                                point[0] = pt.Y; point[1] = pt.Y;
                                dataList.Insert(j, point);
                                find = true;
                                break;
                            }
                        }
                        if (!find)
                        {
                            rowList.Add(pt.X);
                            point = new int[2];
                            point[0] = pt.Y; point[1] = pt.Y;
                            dataList.Add(point);
                        }
                    }
                }

                points = new int[rowList.Count][];
                for (i = rowList.Count - 1; i >= 0; i--)
                {
                    points[i] = new int[3];
                    points[i][0] = rowList[i];

                    point = dataList[i];
                    points[i][1] = point[0];
                    points[i][2] = point[1];
                }
                rowList.Clear();
                dataList.Clear();
            }
            public bool Contains(int row, int col)
            {
                if ((points != null) && (row >= stRow) && (col >= stCol) && (row < stRow + ny) && (col < stCol + nx))
                {
                    for (int i = 0; i < points.Length; i++)
                    {
                        if (points[i][0] == row)
                            if ((points[i][1] <= col) && (col <= points[i][2]))
                                return true;
                    }
                }
                return false;
            }

            public void TryExtension(double Left, double Top, double Right, double Bottom)
            {
                int shiftX = 0, countX = this.nx;
                if (Left < this.Xmin)
                {
                    shiftX = (int)Math.Ceiling((this.Xmin - Left) / this.dx);
                    countX += shiftX;
                }
                if (this.Xmin + dx * nx < Right)
                {
                    countX += (int)((Right - (this.Xmin + dx * nx)) / this.dx);
                }
                int shiftY = 0, countY = this.ny;
                if (Top > this.Ymin)
                {
                    shiftY = (int)Math.Ceiling((Top - this.Ymin) / this.dy);
                    countY += shiftY;
                }
                if (this.Ymin - dy * ny > Bottom)
                {
                    countY += (int)((this.Ymin - dy * ny - Bottom) / this.dy);
                }

                if (shiftX > 0 || shiftY > 0 || countX != nx || countY != ny)
                {
                    float[][] newZValue = new float[countY][];
                    for (int i = 0; i < countY; i++)
                    {
                        newZValue[i] = new float[countX];
                        if (i < shiftY || i >= shiftY + ny)
                        {
                            for (int j = 0; j < countX; j++)
                            {
                                newZValue[i][j] = this.blank;
                            }
                        }
                        else
                        {
                            for (int j = 0; j < countX; j++)
                            {
                                if (j < shiftX || j >= shiftX + nx)
                                {
                                    newZValue[i][j] = this.blank;
                                }
                                else
                                {
                                    newZValue[i][j] = ZValue[i - shiftY][j - shiftX];
                                }
                            }
                        }
                    }
                    ZValue = newZValue;
                    Xmin -= shiftX * dx;
                    Ymin += shiftY * dy;
                    stCol -= shiftX;
                    stRow -= shiftY;
                    maxRow = stRow + ny;
                    maxCol = stCol + nx;
                    ny = countY;
                    nx = countX;
                }
            }

            public void GridOperation(GridOperations Operation, SmallGrid grid, ref float Zmin, ref float Zmax)
            {
                if (Operation == GridOperations.Addition)
                {
                    this.TryExtension(grid.Xmin, grid.Ymin, grid.Xmin + grid.nx * grid.dx, grid.Ymin - grid.dy * grid.ny);
                }
                double x1, x2, x3, x4, stepX = 0;
                double y1, y2, y3, y4, stepY = 0;
                double op;
                this.dx = Math.Round(this.dx, 7);
                this.dy = Math.Round(this.dy, 7);
                grid.dx = Math.Round(grid.dx, 7);
                grid.dy = Math.Round(grid.dy, 7);

                x1 = Xmin;
                x3 = grid.Xmin;

                int shiftX1 = 0, shiftX2 = 0, shiftY1 = 0, shiftY2 = 0;
                double sumX1 = 0, sumX2 = 0, sumY1 = 0, sumY2 = 0;
                double initX = 0, initY;
                if (x1 < x3)
                {
                    shiftX1 = (int)((x3 - x1) / this.dx);
                    x1 += shiftX1 * this.dx; x2 = x1 + this.dx;
                    initX = x1 - x3;
                }
                else
                {
                    shiftX2 = (int)((x1 - x3) / grid.dx);
                    x3 += shiftX2 * grid.dx;
                }
                sumX2 = x1 - x3;

                int ix = shiftX1, jx = shiftX2;
                int iy, jy;
                x2 = x1 + this.dx; x4 = x3 + grid.dx;
                while (ix < this.nx)
                {
                    if (x4 < x2)
                    {
                        stepX = x4 - x1;
                        x1 = x4;
                        x4 += grid.dx;
                    }
                    else
                    {
                        stepX = x2 - x1;
                        x1 = x2;
                        x2 += this.dx;
                    }
                    sumX1 += stepX;
                    sumX2 += stepX;
                    stepX += initX;
                    initX = 0;

                    y1 = Ymin;
                    y3 = grid.Ymin;
                    y2 = y1 - this.dy;
                    y4 = y3 - grid.dy;

                    sumY1 = 0; sumY2 = 0;
                    initY = 0;
                    if (y1 > y3)
                    {
                        shiftY1 = (int)((y1 - y3) / this.dy);
                        y1 -= shiftY1 * this.dy; y2 = y1 - this.dy;
                        initY = y3 - y1;
                    }
                    else
                    {
                        shiftY2 = (int)((y3 - y1) / grid.dy);
                        y3 -= shiftY2 * grid.dy; y4 = y3 - grid.dy;
                    }
                    sumY2 = y3 - y1;
                    // Пробегаем по Y
                    iy = shiftY1; jy = shiftY2;
                    while (iy < this.ny)
                    {
                        if (y4 > y2)
                        {
                            stepY = y1 - y4;
                            y1 = y4;
                            y4 -= grid.dy;
                        }
                        else
                        {
                            stepY = y1 - y2;
                            y1 = y2;
                            y2 -= this.dy;
                        }
                        sumY1 += stepY;
                        sumY2 += stepY;
                        stepY += initY;
                        initY = 0;

                        // Суммируем по ячейке

                        if (grid.ZValue[jy][jx] != grid.blank)
                        {
                            if (ZValue[iy][ix] == this.blank) ZValue[iy][ix] = 0;
                            op = stepX * stepY * grid.ZValue[jy][jx] / (this.dx * this.dy);
                            ZValue[iy][ix] += Convert.ToSingle(op);
                            if (Zmin > ZValue[iy][ix]) Zmin = ZValue[iy][ix];
                            if (Zmax < ZValue[iy][ix]) Zmax = ZValue[iy][ix];
                            // todo : добавить поддержку разных операций над сетками
                        }

                        if (sumY1 >= this.dy)
                        {
                            iy++;
                            sumY1 = 0;
                        }
                        if (sumY2 >= grid.dy)
                        {
                            jy++;
                            sumY2 = 0;
                            if (jy >= grid.ny) break;
                        }
                    }

                    if (sumX1 >= this.dx)
                    {
                        ix++;
                        sumX1 = 0;
                    }
                    if (sumX2 >= grid.dx)
                    {
                        jx++;
                        sumX2 = 0;
                        if (jx >= grid.nx) break;
                    }
                }
            }
            public void LoadGridData(BinaryReader br)
            {
                if (br != null)
                {
                    stRow = br.ReadInt32();
                    stCol = br.ReadInt32();
                    ny = br.ReadInt32();
                    nx = br.ReadInt32();
                    maxRow = stRow + ny;
                    maxCol = stCol + nx;
                    if ((ny > 0) && (nx > 0))
                    {
                        ZValue = new float[ny][];
                        for (int i = 0; i < ny; i++)
                        {
                            ZValue[i] = new float[nx];
                            for (int j = 0; j < nx; j++)
                            {
                                ZValue[i][j] = br.ReadSingle();
                            }
                        }
                    }
                }
            }
            public void PackGridData(BinaryWriter bw, bool ClearGridData)
            {
                if (bw != null)
                {
                    bw.Write(stRow);
                    bw.Write(stCol);
                    bw.Write(ny);
                    bw.Write(nx);
                    for (int i = 0; i < ny; i++)
                    {
                        for (int j = 0; j < nx; j++)
                        {
                            bw.Write(ZValue[i][j]);
                        }
                    }
                    if (ClearGridData)
                    {
                        ZValue = null;
                        FreeDataMemory();
                    }
                }
            }
            public void FreeDataMemory()
            {
                if (bmp != null)
                {
                    bmp.Dispose();
                }
                points = null;
                bmp = null;
                if (ZValue != null)
                {
                    for (int i = 0; i < ZValue.Length; i++)
                    {
                        ZValue[i] = null;
                    }
                }
                if (points != null)
                {
                    for (int i = 0; i < points.Length; i++)
                    {
                        points[i] = null;
                    }
                }
                ZValue = null;
                points = null;
            }

            public void SetMinXY(double Xmin, double Ymin, double dX, double dY)
            {
                this.dx = dX;
                this.dy = dY;
                this.Xmin = Xmin + stCol * dX;
                this.Ymin = Ymin - stRow * dY;
            }
            public void SetPalette(Palette pal)
            {
                this.currPal = pal;
            }
            public void UpdateBrushesBitMap(Palette pal)
            {
                if ((pal != null) && (ZValue != null))
                {
                    SetPalette(pal);
                    CreateBitmap();
                }
            }
            void CreateBitmap()
            {
                if ((nx + ny > 0) && (ZValue != null) && (currPal != null))
                {
                    Bitmap localBmp = new Bitmap(nx + 2, ny + 2);
                    Graphics grfx = Graphics.FromImage(localBmp);
                    Brush br = null;
                    int scrY = 1, scrX = 1;
                    int X, Y;
                    int i, j;

                    for (i = 0; i < this.ny; i++)
                    {
                        Y = scrY + i;
                        for (j = 0; j < this.nx; j++)
                        {
                            X = scrX + j;
                            if (ZValue[i][j] != blank)
                            {
                                using (br = new SolidBrush(currPal.GetColorByX(ZValue[i][j])))
                                {
                                    grfx.FillRectangle(br, X, Y, 1, 1);

                                }
                            }
                        }
                    }
                    if (bmp != null) bmp.Dispose();
                    bmp = localBmp;
                }
            }

            public void DrawBitMap(Graphics grfx)
            {
                if (bmp != null)
                {
                    float scrDx, scrDy, scrX, scrY, scrX2, scrY2;
                    scrDx = (float)(dx / st_XUnitsInOnePixel);
                    scrDy = (float)(dy / st_YUnitsInOnePixel);
                    scrX = ScreenXfromRealX(Xmin - (dx / 2)) - (scrDx / 2);
                    scrY = ScreenYfromRealY(Ymin - (dy / 2)) - (scrDy / 2);
                    scrX2 = scrX + scrDx * (nx + 2);
                    scrY2 = scrY + scrDy * (ny + 2);

                    if (C2DObject.DrawAllObjects || Geometry.IntersectRectangle(st_BMPRect, scrX, scrY, scrX2, scrY2))
                    {
                        RectangleF rect = new RectangleF(scrX, scrY, scrDx * (nx + 2), scrDy * (ny + 2));
                        grfx.DrawImage(bmp, rect);
                    }
                }
            }
            public override void DrawObject(Graphics grfx)
            {
                Brush br = null;
                float scrX, scrY;
                float scrDx, scrDy;

                scrDx = (float)(dx / st_XUnitsInOnePixel);
                scrDy = (float)(dy / st_YUnitsInOnePixel);
                scrX = ScreenXfromRealX(Xmin - (dx / 2));
                scrY = ScreenYfromRealY(Ymin - (dy / 2));
                float X, Y;
                for (int i = 0; i < ny; i++)
                {
                    Y = scrY + scrDy * i;
                    for (int j = 0; j < nx; j++)
                    {
                        X = scrX + scrDx * j;
                        if ((C2DObject.DrawAllObjects || st_BMPRect.Contains(X, Y) || st_BMPRect.Contains(X + scrDx , Y + scrDy)) && (ZValue[i][j] != blank))
                        {
                            using (br = new SolidBrush(currPal.GetColorByX(ZValue[i][j])))
                            {
                                grfx.FillRectangle(br, X, Y, scrDx, scrDy);
                            }
                        }
                    }
                }
            }
            public void DrawLines(Graphics grfx)
            {
                float X, Y;
                float scrDx = Convert.ToSingle(dx / st_XUnitsInOnePixel);
                float scrDy = Convert.ToSingle(dy / st_YUnitsInOnePixel);
                float scrX = ScreenXfromRealX(Xmin - (dx / 2));
                float scrY = ScreenYfromRealY(Ymin - (dy / 2));

                if (DrawGridLines && (scrDx < 1000) && (scrDy < 1000))
                {
                    X = scrX + scrDx * nx;

                    for (int i = 0; i <= ny; i++)
                    {
                        Y = scrY + scrDy * i;
                        grfx.DrawLine(Pens.Black, scrX, Y, X, Y);
                    }

                    Y = scrY + ny * scrDy;
                    for (int j = 0; j <= nx; j++)
                    {
                        X = scrX + scrDx * j;
                        grfx.DrawLine(Pens.Black, X, scrY, X, Y);
                    }
                }
            }
        }

        enum Direction
        {
            UP,
            DOWN,
            RIGHT,
            LEFT
        }
        enum GridFileType
        {
            TXT,
            DAT,
            GRD_SURF6,
            GRD_SURF7,
            IRAP_CLASSIC,
            DOLLAR,
            UNKNOWN
        }

        public bool DrawGridLines
        {
            get
            {
                if (GridList != null && GridList.Length > 0)
                {
                    return GridList[0].DrawGridLines;
                }
                return false;
            }
            set
            {
                if (GridList != null)
                {
                    for (int i = 0; i < GridList.Length; i++)
                    {
                        GridList[i].DrawGridLines = value;
                    }
                }
            }
        }
        public int OilFieldCode;
        public string StratumName;
        public int StratumCode;
        OilfieldCoefItem coef;
        public bool DataLoading;
        public bool DataLoaded;
        public DateTime Date;
        public int GridType;

        public double Angle;

        public int nx;
        public int ny;
        public double Xmax;
        public double Xmin;
        public double Ymax;
        public double Ymin;
        public float Zmax;
        public float Zmin;
        public float BlankVal;
        public double dx;
        public double dy;
        MemoryStream packedData;
        public bool PackedDataLoaded
        {
            get
            {
                return packedData != null;
            }
        }
        SmallGrid[] GridList;
        public Palette palette;

        public Grid()
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.GRID;
            coef = new OilfieldCoefItem();
            packedData = null;
            palette = null;
            GridList = null;
            DataLoading = false;
            DataLoaded = false;
            StratumName = string.Empty;
            Name = string.Empty;
            Angle = 0;
        }

        public Grid(Grid grid) : this()
        {
            coef = new OilfieldCoefItem(grid.coef);
            if (grid.packedData != null)
            {
                packedData = new MemoryStream();
                grid.packedData.Seek(0, SeekOrigin.Begin);
                grid.packedData.WriteTo(packedData);
            }
            palette = new Palette(grid.palette);
            GridList = new SmallGrid[grid.GridList.Length];
            for (int i = 0; i < grid.GridList.Length; i++)
            {
                GridList[i] = new SmallGrid(grid.GridList[i]);
                GridList[i].SetPalette(palette);
            }
            DataLoaded = grid.DataLoaded;
            DataLoading = grid.DataLoading;
            DrawGridLines = grid.DrawGridLines;
            OilFieldCode = grid.OilFieldCode;
            StratumName = grid.StratumName;
            StratumCode = grid.StratumCode;
            OilFieldCode = grid.OilFieldCode;
            Date = grid.Date;
            GridType = grid.GridType;
            nx = grid.nx;
            ny = grid.ny;
            Xmax = grid.Xmax;
            Xmin = grid.Xmin;
            Ymax = grid.Ymax;
            Ymin = grid.Ymin;
            Zmin = grid.Zmin;
            Zmax = grid.Zmax;
            BlankVal = grid.BlankVal;
            dx = grid.dx;
            dy = grid.dy;
        }

        // FROM SERVER
        public bool Read(MemoryStream ms, OilfieldCoefItem coef)
        {
            if (ms == null) return false;
            bool res = false;
            int i, j, len, palLen;
            float[][] ZValue = null;
            this.coef = new OilfieldCoefItem(coef);
            // Read ZipBlock
            BinaryReader br = new BinaryReader(ms);
            int version = br.ReadInt32();
            if (version == 2)
            {
                ms.Seek(8, SeekOrigin.Begin);
            }
            else
            {
                version = 4;
                ms.Seek(32, SeekOrigin.Begin);
            }

            this.nx = br.ReadInt32();
            this.ny = br.ReadInt32();
            Xmin = br.ReadDouble();
            Ymax = br.ReadDouble();
            this.dx = br.ReadDouble();
            this.dy = br.ReadDouble();

            Xmax = Xmin + nx * dx;
            Ymax = -Ymax + dy;
            Ymin = Ymax - ny * dy;

            this.Zmin = Convert.ToSingle(br.ReadDouble());
            this.Zmax = Convert.ToSingle(br.ReadDouble());
            ms.Seek(24, SeekOrigin.Current);
            this.BlankVal = Convert.ToSingle(br.ReadDouble());

            ms.Seek(40, SeekOrigin.Current);
            palLen = br.ReadInt32();
            ms.Seek(20, SeekOrigin.Current);


            if ((nx > 0) && (ny > 0) && (nx < 50000) && (ny < 50000) && (dx > 0) && (dy > 0))
            {
                len = this.nx * this.ny;
                ZValue = new float[ny][];
                float maxVal = 0;
                for (i = 0; i < ny; i++)
                {
                    ZValue[i] = new float[nx];
                    for (j = 0; j < nx; j++)
                    {
                        ZValue[i][j] = br.ReadSingle();
                        if (maxVal < ZValue[i][j]) maxVal = ZValue[i][j];
                    }
                }
                if (maxVal > Zmax * 10) BlankVal = maxVal;
                if (this.BlankVal >= 1.70141E+38 && ZValue[0][0] == 0)
                {
                    this.BlankVal = 0;
                }
                ZValue = TranslateCoord(coef, ZValue, false);
                DefaultPalette();
                res = true;
            }
            else
                res = false;
            br.Close();
            ms.Close();
            
            if ((res) && (ZValue != null))
            {
                CreateGridList(null, null, ZValue);
            }
            
            return res;
        }

        // FROM FILE
        public bool LoadFromFile(BackgroundWorker worker, DoWorkEventArgs e, string fileName, OilfieldCoefItem coef)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка сетки из файла...";
            userState.Element = this.Name;

            bool res = false;
            GridFileType type = GridFileType.UNKNOWN;
            float[][] ZValue = null;
            this.coef = new OilfieldCoefItem(coef);
            if (File.Exists(fileName))
            {
                FileStream fs = new FileStream(fileName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs, Encoding.GetEncoding(1251));

                if (worker != null) worker.ReportProgress(0, userState);
                if (fileName.IndexOf(".grd") > -1)
                {
                    int grd_type = br.ReadInt32();
                    if (grd_type == 0x42525344)
                        type = GridFileType.GRD_SURF7;
                    else if (grd_type == 0x42425344)
                        type = GridFileType.GRD_SURF6;
                }

                switch (type)
                {
                    case GridFileType.GRD_SURF7:
                        int head_sz, ver, headerId;
                        head_sz = br.ReadInt32();
                        ver = br.ReadInt32();
                        headerId = br.ReadInt32();
                        if ((headerId == 0x44495247) && (head_sz == 4) && (ver == 1))
                        {
                            double xGlob, yGlob;
                            int len = br.ReadInt32();
                            ny = br.ReadInt32();
                            nx = br.ReadInt32();
                            Xmin = (int)Math.Round(br.ReadDouble());
                            Ymin = (int)Math.Round(br.ReadDouble());
                            dx = (int)Math.Round(br.ReadDouble());
                            dy = (int)Math.Round(br.ReadDouble());

                            Xmax = Xmin + nx * dx;
                            Ymax = Ymin + ny * dy;

                            Zmin = Convert.ToSingle(br.ReadDouble());
                            Zmax = Convert.ToSingle(br.ReadDouble());
                            DefaultPalette();
                            br.BaseStream.Seek(8, SeekOrigin.Current);
                            BlankVal = Convert.ToSingle(br.ReadDouble());
                            int dataId = br.ReadInt32();
                            if (dataId == 0x41544144)
                            {
                                len = (int)(br.ReadInt32() / 8);
                                ZValue = new float[ny][];

                                for (int i = ny - 1; i >= 0; i--)
                                {
                                    ZValue[i] = new float[nx];
                                    for (int j = 0; j < nx; j++)
                                    {
                                        ZValue[i][j] = Convert.ToSingle(br.ReadDouble());
                                    }
                                }
                            }
                            ZValue = TranslateCoord(coef, ZValue, false);
                            
                            DataLoaded = true;
                            res = true;
                        }
                        else
                        {
                            nx = 0;
                            ny = 0;
                            res = false;
                        }
                        br.Close();
                        break;
                    default:
                        break;
                }
                if (worker != null) worker.ReportProgress(100, userState);
            }

            if ((res) && (ZValue != null))
            {
                CreateGridList(worker, e, ZValue);
            }
            return res;
        }

        // WORK WITH DATA
        public void CreateGridList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (GridList != null && GridList.Length == 1)
            {
                CreateGridList(worker, e, GridList[0].ZValue);
            }
        }
        public void CreateGridList(BackgroundWorker worker, DoWorkEventArgs e, float[][] ZValue)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Оптимизация сетки...";
            userState.Element = this.Name;
            Direction dir;

            if ((ZValue != null) && (nx > 0) && (ny > 0))
            {
                List<SmallGrid> list = new List<SmallGrid>();
                List<Point> pointList = new List<Point>();
                SmallGrid grid;
                int row, col, i, j, k, d, progress = 0, currMaxProgress = 0, maxProgress = nx * ny;

                bool contain, createSmall = false, breakScan = false;
                if (worker != null) worker.ReportProgress(0, userState);
                while (!breakScan)
                {
                    pointList.Clear();
                    createSmall = false;
                    progress = 0;
                    // глобальное сканирование по строкам
                    for (row = 0; row < ny; row++)
                    {
                        if (createSmall) break;
                        for (col = 0; col < nx; col++)
                        {
                            if ((worker != null) && (worker.CancellationPending))
                            {
                                e.Cancel = true;
                                return;
                            }
                            if ((!createSmall) && (ZValue[row][col] != BlankVal))
                            {
                                if (list.Count > 0)
                                {
                                    contain = false;
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        if (list[k].Contains(row, col))
                                        {
                                            contain = true;
                                            break;
                                        }
                                    }
                                    if (contain) continue;
                                }
                                // не найдено ни в одном Small Grid'e создаем новый
                                createSmall = true;
                                grid = new SmallGrid(row, col, BlankVal);
                                list.Add(grid);
                                // бежим по часовой стрелке пока не придем в начальную точку
                                dir = Direction.RIGHT;
                                int[] dx = new int[3];
                                int[] dy = new int[3];
                                for (i = 0; i < 3; i++)
                                {
                                    dx[i] = 0;
                                    dy[i] = 0;
                                }
                                i = -1; j = -1;

                                while ((i != row) || (j != col))
                                {
                                    if (i == -1)
                                    {
                                        i = row; j = col;
                                    }
                                    pointList.Add(new Point(i, j));
                                    switch (dir)
                                    {
                                        case Direction.RIGHT:
                                            dx[0] = 0; dy[0] = -1;
                                            dx[1] = 0; dy[1] = 0;
                                            dx[2] = -1; dy[2] = 0;
                                            break;
                                        case Direction.LEFT:
                                            dx[0] = -1; dy[0] = 0;
                                            dx[1] = -1; dy[1] = -1;
                                            dx[2] = 0; dy[2] = -1;
                                            break;
                                        case Direction.UP:
                                            dx[0] = -1; dy[0] = -1;
                                            dx[1] = 0; dy[1] = -1;
                                            dx[2] = 0; dy[2] = 0;
                                            break;
                                        case Direction.DOWN:
                                            dx[0] = 0; dy[0] = 0;
                                            dx[1] = -1; dy[1] = 0;
                                            dx[2] = -1; dy[2] = -1;
                                            break;
                                    }
                                    for (d = 0; d < 3; d++)
                                    {
                                        if ((i + dy[d] >= 0) && (i + dy[d] < ny) && (j + dx[d] >= 0) && (j + dx[d] < nx))
                                        {
                                            if (((d == 0) && (ZValue[i + dy[d]][j + dx[d]] != BlankVal) && (ZValue[i + dy[1]][j + dx[1]] != BlankVal)) ||
                                                ((d > 0) && (ZValue[i + dy[d]][j + dx[d]] != BlankVal)))
                                            {
                                                #region SWITCH DIRECTION
                                                switch (dir)
                                                {
                                                    case Direction.RIGHT:
                                                        switch (d)
                                                        {
                                                            case 0:
                                                                dir = Direction.UP;
                                                                break;
                                                            case 1:
                                                                dir = Direction.RIGHT;
                                                                break;
                                                            case 2:
                                                                dir = Direction.DOWN;
                                                                break;
                                                        }
                                                        break;
                                                    case Direction.LEFT:
                                                        switch (d)
                                                        {
                                                            case 0:
                                                                dir = Direction.DOWN;
                                                                break;
                                                            case 1:
                                                                dir = Direction.LEFT;
                                                                break;
                                                            case 2:
                                                                dir = Direction.UP;
                                                                break;
                                                        }
                                                        break;
                                                    case Direction.UP:
                                                        switch (d)
                                                        {
                                                            case 0:
                                                                dir = Direction.LEFT;
                                                                break;
                                                            case 1:
                                                                dir = Direction.UP;
                                                                break;
                                                            case 2:
                                                                dir = Direction.RIGHT;
                                                                break;
                                                        }
                                                        break;
                                                    case Direction.DOWN:
                                                        switch (d)
                                                        {
                                                            case 0:
                                                                dir = Direction.RIGHT;
                                                                break;
                                                            case 1:
                                                                dir = Direction.DOWN;
                                                                break;
                                                            case 2:
                                                                dir = Direction.LEFT;
                                                                break;
                                                        }
                                                        break;
                                                }
                                                switch (dir)
                                                {
                                                    case Direction.RIGHT:
                                                        j++;
                                                        break;
                                                    case Direction.LEFT:
                                                        j--;
                                                        break;
                                                    case Direction.UP:
                                                        i--;
                                                        break;
                                                    case Direction.DOWN:
                                                        i++;
                                                        break;
                                                }
                                                #endregion

                                                if (i < grid.stRow) grid.stRow = i;
                                                if (j < grid.stCol) grid.stCol = j;
                                                if (grid.maxRow < i) grid.maxRow = i;
                                                if (grid.maxCol < j) grid.maxCol = j;
                                                break;
                                            }
                                        }
                                    }
                                }
                                grid.ny = grid.maxRow - grid.stRow;
                                grid.nx = grid.maxCol - grid.stCol;
                                grid.SetMinXY(this.Xmin, this.Ymax, this.dx, this.dy);

                                // переписываем points
                                grid.AddPointList(pointList, ZValue);

                                // переписываем zValues
                                grid.ZValue = new float[grid.ny][];
                                for (i = 0; i < grid.ny; i++)
                                {
                                    grid.ZValue[i] = new float[grid.nx];
                                    for (j = 0; j < grid.nx; j++)
                                    {
                                        if (grid.Contains(grid.stRow + i, grid.stCol + j))
                                        {
                                            grid.ZValue[i][j] = ZValue[grid.stRow + i][grid.stCol + j];
                                        }
                                        else
                                            grid.ZValue[i][j] = BlankVal;
                                    }
                                }
                                break;
                            }
                        }
                        if (worker != null)
                        {
                            progress += nx;
                            if (currMaxProgress < progress) currMaxProgress = progress;
                            if (currMaxProgress > maxProgress) currMaxProgress = maxProgress;
                            worker.ReportProgress((int)(currMaxProgress * 100.0 / maxProgress), userState);
                        }
                    }
                    if (!createSmall) breakScan = true;
                }

                if (list.Count > 0)
                {
                    DefaultPalette();
                    // очистка гридов величиной 1 ячейку по любой оси
                    for (i = 0; i < list.Count; i++)
                    {
                        if (list[i].nx == 1 || list[i].ny == 1)
                        {
                            list.RemoveAt(i);
                            i--;
                        }
                    }
                    GridList = new SmallGrid[list.Count];
                    for (i = 0; i < list.Count; i++)
                    {
                        GridList[i] = list[i];
                        GridList[i].UpdateBrushesBitMap(palette);
                    }
                }
                if (worker != null) worker.ReportProgress(100, userState);
            }
        }
        public void PackGridData(bool ClearGridData)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write(OilFieldCode);
            bw.Write(DrawGridLines);
            bw.Write(nx);
            bw.Write(ny);
            bw.Write(Xmin);
            bw.Write(Xmax);
            bw.Write(Ymin);
            bw.Write(Ymax);
            bw.Write((double)Zmin);
            bw.Write((double)Zmax);
            bw.Write((double)BlankVal);
            bw.Write(dx);
            bw.Write(dy);
            int i;
            if (palette == null)
            {
                bw.Write(0);
            }
            else
            {
                palette.WriteToCache(bw);
            }
            if (ClearGridData) palette = null;
            if (GridList == null)
            {
                bw.Write(0);
            }
            else
            {
                bw.Write(GridList.Length);
                for (i = 0; i < GridList.Length; i++)
                {
                    GridList[i].PackGridData(bw, ClearGridData);
                }
            }
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();
            packedData = ConvertEx.PackStream(ms);
            bw.Close();
        }
        public void FreeDataMemory()
        {
            if (DataLoaded && !DataLoading && GridList != null)
            {
                DataLoaded = false;
                for (int i = 0; i < GridList.Length; i++)
                {
                    GridList[i].FreeDataMemory();
                }
                ClearPackedData();
            }
        }
        public void ClearPackedData()
        {
            if (packedData != null)
            {
                packedData.Dispose();
                packedData = null;
            }
        }

        // GRID OPERATIONS
        public void TryExtent(Grid grid)
        {
            this.UnionGridList();
            grid.UnionGridList();
            SmallGrid gridEx = grid.GridList[0];
            this.GridList[0].TryExtension(gridEx.Xmin, gridEx.Ymin, gridEx.Xmin + gridEx.nx * gridEx.dy, gridEx.Ymin - gridEx.ny * gridEx.dy);
        }
        /// <summary>
        /// Объединение всех smallGrid с расширением границ до начальных
        /// </summary>
        public void UnionAllGrid()
        {
            if (GridList.Length > 1 || (GridList.Length == 1 && (GridList[0].nx != this.nx || GridList[0].ny != this.ny)))
            {
                SmallGrid grid;
                float val;
                float[][] newZValues = new float[this.ny][];
                for (int i = 0; i < this.ny; i++)
                {
                    newZValues[i] = new float[this.nx];
                    for (int j = 0; j < this.nx; j++)
                    {
                        newZValues[i][j] = this.BlankVal;
                    }
                }
                for (int ig = 0; ig < GridList.Length; ig++)
                {
                    grid = GridList[ig];
                    for (int i = 0; i < grid.ny; i++)
                    {
                        for (int j = 0; j < grid.nx; j++)
                        {
                            val = grid.ZValue[i][j];
                            if (val != this.BlankVal)
                            {
                                newZValues[i + grid.stRow][j + grid.stCol] = val;
                            }
                        }
                    }
                }
                grid = new SmallGrid(0, 0, BlankVal);
                grid.ny = this.ny;
                grid.nx = this.nx;
                grid.maxRow = this.ny;
                grid.maxCol = this.nx;
                
                grid.SetMinXY(this.Xmin, this.Ymax, this.dx, this.dy);
                grid.ZValue = newZValues;
                GridList = new SmallGrid[1];
                GridList[0] = grid;
                //GridList[0].UpdateBrushesBitMap(palette);
            }
        }
        /// <summary>
        /// Объединение всех smallGrid, без расширения границ
        /// </summary>
        public void UnionGridList()
        {
            if (GridList.Length > 1)
            {
                int maxRow = 0, maxCol = 0;
                int minRow = int.MaxValue, minCol = int.MaxValue;
                for (int i = 0; i < GridList.Length; i++)
                {
                    if (minRow > GridList[i].stRow) minRow = GridList[i].stRow;
                    if (minCol > GridList[i].stCol) minCol = GridList[i].stCol;
                    if (maxRow < GridList[i].stRow + GridList[i].ny) maxRow = GridList[i].stRow + GridList[i].ny;
                    if (maxCol < GridList[i].stCol + GridList[i].nx) maxCol = GridList[i].stCol + GridList[i].nx;
                }
                if (maxRow > 0 && maxCol > 0 && minRow < int.MaxValue && minCol < int.MaxValue)
                {
                    SmallGrid grid;
                    float val;
                    float[][] newZValues = new float[maxRow - minRow][];
                    for (int i = 0; i < maxRow - minRow; i++)
                    {
                        newZValues[i] = new float[maxCol - minCol];
                        for (int j = 0; j < maxCol - minCol; j++)
                        {
                            newZValues[i][j] = this.BlankVal;
                        }
                    }
                    for (int ig = 0; ig < GridList.Length; ig++)
                    {
                        grid = GridList[ig];
                        for (int i = 0; i < grid.ny; i++)
                        {
                            for (int j = 0; j < grid.nx; j++)
                            {
                                val = grid.ZValue[i][j];
                                if (val != this.BlankVal)
                                {
                                    newZValues[i + grid.stRow - minRow][j + grid.stCol - minCol] = val;
                                }
                            }
                        }
                    }
                    grid = new SmallGrid(minRow, minCol, BlankVal);
                    grid.ny = maxRow - minRow;
                    grid.nx = maxCol - minCol;
                    grid.maxRow = maxRow;
                    grid.maxCol = maxCol;
                    grid.SetMinXY(this.Xmin, this.Ymax, this.dx, this.dy);
                    grid.ZValue = newZValues;
                    GridList = new SmallGrid[1];
                    GridList[0] = grid;
                    //GridList[0].UpdateBrushesBitMap(palette);
                }
            }
        }

        public void ReduceGridResolution()
        {
            if (DataLoaded && this.dx >= 50)
            {
                SmallGrid grid;
                int k = (this.dx >= 100) ? 4 : 2;

                for (int ig = 0; ig < GridList.Length; ig++)
                {
                    grid = GridList[ig];
                    int countX = grid.nx * k;
                    int countY = grid.ny * k;
                    float[][] newZValue = new float[countY][];
                    for (int i = 0; i < countY; i += k)
                    {
                        for (int x = 0; x < k; x++)
                        {
                            newZValue[i + x] = new float[countX];
                            for (int j = 0; j < countX; j += k)
                            {
                                for (int y = 0; y < k; y++)
                                {
                                    newZValue[i + x][j + y] = grid.ZValue[i / k][j / k];
                                }
                            }
                        }
                    }
                    for (int i = 0; i < grid.ny; i++) grid.ZValue[i] = null;
                    grid.dx /= k;
                    grid.dy /= k;
                    grid.nx = countX;
                    grid.ny = countY;
                    grid.stRow *= k;
                    grid.stCol *= k;
                    grid.ZValue = newZValue;
                }
                this.nx *= k; this.ny *= k;
                this.dx /= k; this.dy /= k;
            }
        }
        float[][] TranslateCoord(OilfieldCoefItem coefItem, float[][] Zvalue, bool GeometricCalc)
        {
            double xGlob = 0, yGlob = 0;

            if (coefItem.KfX_Xnew == 1 && coefItem.KfY_Ynew == 1)
            {
                coefItem.GlobalCoordFromLocal(Xmin, Ymin, out xGlob, out yGlob);
                Xmin = xGlob;
                Ymin = yGlob;
                coefItem.GlobalCoordFromLocal(Xmax, Ymax, out xGlob, out yGlob);
                Xmax = xGlob;
                Ymax = yGlob;
                return Zvalue;
            }
            else // существует поворот в координатах
            {
                return RotateGrid(coefItem, Zvalue, GeometricCalc);
            }
        }
        public float[][] RotateGrid(OilfieldCoefItem coefItem, float[][] Zvalue, bool GeometricCalc)
        {
            if (Zvalue != null)
            {
                double xGlob, yGlob;
                double x, y, X, Y;
                int i, j, countX, countY;
                double minX, minY, maxX, maxY;

                coefItem.GlobalCoordFromLocal(Xmin, Ymin, out xGlob, out yGlob);
                minX = xGlob;
                maxX = xGlob;
                minY = yGlob;
                maxY = yGlob;
                coefItem.GlobalCoordFromLocal(Xmax, Ymin, out xGlob, out yGlob);
                if (minX > xGlob) minX = xGlob;
                if (maxX < xGlob) maxX = xGlob;
                if (minY > yGlob) minY = yGlob;
                if (maxY < yGlob) maxY = yGlob;
                coefItem.GlobalCoordFromLocal(Xmin, Ymax, out xGlob, out yGlob);
                if (minX > xGlob) minX = xGlob;
                if (maxX < xGlob) maxX = xGlob;
                if (minY > yGlob) minY = yGlob;
                if (maxY < yGlob) maxY = yGlob;
                coefItem.GlobalCoordFromLocal(Xmax, Ymax, out xGlob, out yGlob);
                if (minX > xGlob) minX = xGlob;
                if (maxX < xGlob) maxX = xGlob;
                if (minY > yGlob) minY = yGlob;
                if (maxY < yGlob) maxY = yGlob;

                countX = (int)Math.Ceiling((maxX - minX) / dx);
                countY = (int)Math.Ceiling((maxY - minY) / dy);
                maxX = minX + dx * countX;
                minY = maxY - dy * countY;
                
                int row, col;
                float[][] newZvalue = new float[countY][];
                for (i = 0; i < countY; i++)
                {
                    newZvalue[i] = new float[countX];
                    for (j = 0; j < countX; j++)
                    {
                        newZvalue[i][j] = this.BlankVal;
                    }
                }
                int stX, endX, stY, endY;
                int ind, count;

                PointD[] cell = new PointD[4];
                PointD[] predRow = new PointD[nx + 1];
                PointD[] predCol = new PointD[ny + 1];
                
                Contour cellCntr, newCellCntr;
                Polygon poly = new Polygon();
                List<Contour> cntrs;
                double square;
                Y = Ymax;
                for (row = 0; row < ny; row++)
                {
                    X = Xmin;
                    for (col = 0; col < nx; col++)
                    {
                        #region Rotate Cell
                        if (row > 0)
                        {
                            cell[1] = predRow[col + 1];
                        }
                        else
                        {
                            coefItem.GlobalCoordFromLocal(X + dx, Y, out cell[1].X, out cell[1].Y);
                        }
                        if (col > 0)
                        {
                            cell[0] = predCol[row];
                            cell[3] = predCol[row + 1];
                        }
                        else
                        {
                            coefItem.GlobalCoordFromLocal(X, Y, out cell[0].X, out cell[0].Y);
                            coefItem.GlobalCoordFromLocal(X, Y - dy, out cell[3].X, out cell[3].Y);
                        }
                        coefItem.GlobalCoordFromLocal(X + dx, Y - dy, out cell[2].X, out cell[2].Y);
                        predRow[col] = cell[3]; predRow[col + 1] = cell[2];
                        predCol[row] = cell[1]; predCol[row + 1] = cell[2];
                        cellCntr = new Contour(cell);
                        cellCntr._Closed = true;
                        #endregion

                        if (Zvalue[row][col] != BlankVal)
                        {
                            #region Вычисляем сектор попадания повернутой ячейки
                            stX = countX; endX = 0;
                            stY = countY; endY = 0;
                            for(i = 0; i < 4; i++)
                            {
                                ind = (int)((cell[i].X - minX) / dx);
                                if (stX > ind) stX = ind;
                                if (endX < ind + 1) endX = ind + 1;
                                ind = (int)((maxY - cell[i].Y) / dy);
                                if (stY > ind) stY = ind;
                                if (endY < ind + 1) endY = ind + 1;
                            }
                            #endregion

                            #region Для каждой ячейки назначения считаем попадания
                            for (i = stY; i < endY; i++)
                            {
                                y = maxY - i * dy;
                                for (j = stX; j < endX; j++)
                                {
                                    x = minX + j * dx;
                                    count = 0;
                                    if (cellCntr.PointBodyEntry(x, y)) count++;
                                    if (cellCntr.PointBodyEntry(x + dx, y)) count++;
                                    if (cellCntr.PointBodyEntry(x, y - dy)) count++;
                                    if (cellCntr.PointBodyEntry(x + dx, y - dy)) count++;

                                    if (!GeometricCalc)
                                    {
                                        if (count > 0)
                                        {
                                            if (newZvalue[i][j] == BlankVal) newZvalue[i][j] = 0;
                                            newZvalue[i][j] += Zvalue[row][col] * (count / 4f);
                                        }
                                    }
                                    else
                                    {
                                        if (count == 4)
                                        {
                                            if (newZvalue[i][j] == BlankVal) newZvalue[i][j] = 0;
                                            newZvalue[i][j] += Zvalue[row][col];
                                        }
                                        else if (count > 0 || cellCntr.PointBodyEntry(x + dx / 2, y - dy / 2))
                                        {
                                            PointD[] points = new PointD[4];
                                            points[0].X = x; points[0].Y = y;
                                            points[1].X = x + dx; points[1].Y = y;
                                            points[2].X = x + dx; points[2].Y = y - dy;
                                            points[3].X = x; points[3].Y = y - dy;

                                            newCellCntr = new Contour(points);
                                            newCellCntr._Closed = true;
                                            poly.Clear();
                                            poly.AddContour(cellCntr);
                                            poly.AddContour(newCellCntr);
                                            poly.OperateByContours(ContoursOperation.Crossing);
                                            cntrs = poly.GetContours();
                                            if (cntrs.Count == 1)
                                            {
                                                square = cntrs[0]._points.GetSquare();
                                                if (newZvalue[i][j] == BlankVal) newZvalue[i][j] = 0;
                                                newZvalue[i][j] += (float)(Zvalue[row][col] * square / (dx * dy));
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        X += dx;
                    }
                    Y -= dy;
                }
                for (i = 0; i < countY; i++)
                {
                    for (j = 0; j < countX; j++)
                    {
                        if (newZvalue[i][j] != BlankVal)
                        {
                            if (Zmin > newZvalue[i][j]) Zmin = newZvalue[i][j];
                            if (Zmax < newZvalue[i][j]) Zmax = newZvalue[i][j];
                        }
                    }
                }
                nx = countX;
                ny = countY;
                Xmin = minX;
                Xmax = Xmin + dx * nx;
                Ymax = maxY;
                Ymin = Ymax - dy * ny;
                return newZvalue;
            }
            return null;
        }
        public void ConvertToReserves(List<Area> areas)
        {
            SmallGrid grid;
            int lastIndex = -1;
            double X, Y;
            if (GridList == null) return;
            for (int ig = 0; ig < GridList.Length; ig++)
            {
                grid = GridList[ig];
                for (int i = 0; i < grid.ny; i++)
                {
                    Y = grid.Ymin - i * grid.dy;
                    for (int j = 0; j < grid.nx; j++)
                    {
                        X = grid.Xmin + j * grid.dx;
                        if (grid.ZValue[i][j] != grid.blank)
                        {
                            if (lastIndex != -1)
                            {
                                if (!areas[lastIndex].contour.PointBodyEntry(X, Y))
                                {
                                    lastIndex = -1;
                                }
                            }
                            if (lastIndex == -1)
                            {
                                for (int k = 0; k < areas.Count; k++)
                                {
                                    if (areas[k].contour.PointBodyEntry(X, Y))
                                    {
                                        lastIndex = k;
                                        break;
                                    }
                                }
                            }
                            if (lastIndex != -1)
                            {
                                PVTParamsItem pvt = areas[lastIndex].pvt;
                                grid.ZValue[i][j] = (float)(grid.ZValue[i][j] * grid.dx * grid.dy * pvt.OilDensity * pvt.Porosity * pvt.OilInitialSaturation * pvt.KIN  * 10 / pvt.OilVolumeFactor);
                            }
                        }
                    }
                }
            }
        }
        public void GridOperation(Grid grid, GridOperations Operation)
        {
            if (this.GridList.Length > 0 && grid.GridList != null && grid.GridList.Length > 0)
            {
                Grid newGrid = new Grid(grid);
                this.UnionAllGrid();
                newGrid.UnionGridList();
                SmallGrid smallGrid = this.GridList[0];
                smallGrid.GridOperation(Operation, newGrid.GridList[0], ref this.Zmin, ref this.Zmax);

                if (smallGrid.Xmin < this.Xmin)
                {
                    this.Xmin = smallGrid.Xmin;
                    smallGrid.stCol = 0;
                }
                if (smallGrid.Ymin > this.Ymax)
                {
                    this.Ymax = smallGrid.Ymin;
                    smallGrid.stRow = 0;
                }
                if (this.nx < smallGrid.nx)
                {
                    this.nx = smallGrid.nx;
                    this.Xmax = this.Xmin + nx * dx;
                }
                if (this.ny < smallGrid.ny)
                {
                    this.ny = smallGrid.ny;
                    this.Ymin = this.Ymax - ny * dy;
                }
                this.DefaultPalette();
                //smallGrid.UpdateBrushesBitMap(palette);
            }
        }

        // PALETTE
        public void DefaultPalette()
        {
            palette = new Palette();
            palette.Add(Zmin, Color.Blue);
            palette.Add(Zmin + (Zmax - Zmin) / 4, Color.FromArgb(0, 255, 255));
            palette.Add(Zmin + (Zmax - Zmin) / 2, Color.FromArgb(0, 255, 0));
            palette.Add(Zmin + 3 * (Zmax - Zmin) / 4, Color.FromArgb(255, 255, 0));
            palette.Add(Zmax, Color.Red);
        }
        public void UpdatePalette(Palette pal)
        {
            this.palette.Clear();
            for (int i = 0; i < pal.Count; i++)
            {
                this.palette.Add(pal[i].X, pal[i].Y);
            }
            for (int i = 0; i < GridList.Length; i++)
            {
                GridList[i].UpdateBrushesBitMap(palette);
            }
        }

        // VALUE
        public float GetZvalue(int row, int col)
        {
            if ((row > -1) && (col > -1) && (DataLoaded) && (!DataLoading) && (row < ny) && (col < nx) && (GridList != null))
            {
                SmallGrid grid;
                float result = BlankVal;
                for (int i = 0; i < GridList.Length; i++)
                {
                    grid = GridList[i];
                    if ((grid.stRow <= row) && (row < grid.maxRow) && (grid.stCol <= col) && (col < grid.maxCol))
                    {
                        if (grid.ZValue[row - grid.stRow][col - grid.stCol] != grid.blank)
                            result = grid.ZValue[row - grid.stRow][col - grid.stCol];
                    }
                }
                return result;
            }
            else
                return BlankVal;
        }
        public void CalcSumValues(BackgroundWorker worker, DoWorkEventArgs e, Contour cntr, out double Square, out double SumValues)
        {
            Square = 0;
            SumValues = 0;
            int row, col;
            double minX, maxX, minY, maPointD;
            minX = cntr._points.GetMinX();
            maxX = cntr._points.GetMaxX();
            minY = cntr._points.GetMinY();
            maPointD = cntr._points.GetMaxY();
            if ((Xmax < minX) || (maxX < Xmin - dx / 2) || (Ymax < minY) || (maPointD < Ymin)) return;
            double sq = this.dx * this.dy;
            double x1 = -1, x2 = -1, y1 = -1, y2 = -1;
            SmallGrid grid;
            int count = 0;
            PointD[] points = new PointD[4];

            for (int grInd = 0; grInd < GridList.Length; grInd++)
            {
                grid = GridList[grInd];
                if ((grid.ZValue == null) || (maxX < grid.Xmin) || (grid.Xmin + grid.nx * grid.dx < minX) || (minY > grid.Ymin) || (maPointD < grid.Ymin - grid.ny * grid.dy)) continue;
                byte[,] inCntr = new byte[grid.ny + 1, grid.nx + 1];
                for (row = 0; row < grid.ny; row++)
                {
                    if ((worker != null) && (worker.CancellationPending))
                    {
                        e.Cancel = true;
                        Square = 0;
                        SumValues = 0;
                        return;
                    }
                    y1 = (Ymax - grid.dy / 2) - (grid.stRow + row) * grid.dy;
                    y2 = y1 - grid.dy;
                    for (col = 0; col < grid.nx; col++)
                    {
                        x1 = Xmin - grid.dx / 2 + (grid.stCol + col) * grid.dx;
                        x2 = x1 + grid.dx;
                        if (grid.ZValue[row][col] != grid.blank)
                        {
                            count = 0;
                            if (inCntr[row, col] == 0) inCntr[row, col] = (byte)((cntr.PointBodyEntry(x1, y1)) ? 1 : 2);
                            if (inCntr[row, col] == 1) count++;

                            if (inCntr[row, col + 1] == 0) inCntr[row, col + 1] = (byte)((cntr.PointBodyEntry(x2, y1)) ? 1 : 2);
                            if (inCntr[row, col + 1] == 1) count++;

                            if (inCntr[row + 1, col] == 0) inCntr[row + 1, col] = (byte)((cntr.PointBodyEntry(x1, y2)) ? 1 : 2);
                            if (inCntr[row + 1, col] == 1) count++;

                            if (inCntr[row + 1, col + 1] == 0) inCntr[row + 1, col + 1] = (byte)((cntr.PointBodyEntry(x2, y2)) ? 1 : 2);
                            if (inCntr[row + 1, col + 1] == 1) count++;

                            if (count > 0)
                            {
                                Square += sq;
                                SumValues += (count / 4f) * sq * grid.ZValue[row][col];
                            }
                        }
                    }
                }
            }
        }

        // Packed Data
        public void LoadPackedData(FileStream fs)
        {
            BinaryReader fr = new BinaryReader(fs);
            int packedLen = fr.ReadInt32();
            if (packedData == null) packedData = new MemoryStream(packedLen);
            ConvertEx.CopyStream(fs, packedData, packedLen);
        }
        // CACHE
        
        public bool LoadFromCache(BackgroundWorker worker, DoWorkEventArgs e, FileStream fs, bool CreateBrushesBitMap)
        {
            if (fs != null)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Загрузка сетки в память";
                userState.WorkCurrentTitle = "Загрузка сетки в память";
                userState.Element = this.Name;

                int packedLen, gridLen;
                BinaryReader fr = new BinaryReader(fs);
                packedLen = fr.ReadInt32();
                MemoryStream ms = new MemoryStream(packedLen);
                ConvertEx.CopyStream(fs, ms, packedLen);
                MemoryStream unpackMS = new MemoryStream();
                ConvertEx.UnPackStream(ms, ref unpackMS);
                BinaryReader br = new BinaryReader(unpackMS);
                OilFieldCode = br.ReadInt32();
                DrawGridLines = br.ReadBoolean();
                nx = br.ReadInt32();
                ny = br.ReadInt32();
                Xmin = br.ReadDouble();
                Xmax = br.ReadDouble();
                Ymin = br.ReadDouble();
                Ymax = br.ReadDouble();
                Zmin = (float)br.ReadDouble();
                Zmax = (float)br.ReadDouble();
                BlankVal = (float)br.ReadDouble();
                dx = br.ReadDouble();
                dy = br.ReadDouble();
                palette = new Palette();
                palette.ReadFromCache(br);
                gridLen = br.ReadInt32();

                if (worker != null) worker.ReportProgress((int)(unpackMS.Position * 100F / (1.1f * unpackMS.Length)), userState);

                if (gridLen > 0)
                {
                    GridList = new SmallGrid[gridLen];
                    for (int i = 0; i < gridLen; i++)
                    {
                        GridList[i] = new SmallGrid();
                        GridList[i].blank = BlankVal;
                        GridList[i].LoadGridData(br);
                        GridList[i].SetMinXY(Xmin, Ymax, dx, dy);
                    }
                    if (worker != null) worker.ReportProgress((int)(unpackMS.Position * 100F / (1.1f * unpackMS.Length)), userState);
                }
                if (CreateBrushesBitMap)
                {
                    try
                    {
                        for (int i = 0; i < gridLen; i++)
                        {
                            GridList[i].UpdateBrushesBitMap(palette);
                        }
                    }
                    catch (ArgumentException)
                    {
                        throw new OutOfMemoryException();
                    }
                }
                DataLoaded = true;
                if (worker != null) worker.ReportProgress(100, userState);
                return true;
            }
            return false;
        }
        public int WriteToCache(FileStream fs)
        {
            int res = 0;
            if (fs != null)
            {
                if (packedData == null) PackGridData(false);

                BinaryWriter bw = new BinaryWriter(fs);
                res = (int)packedData.Length;
                bw.Write((int)packedData.Length);
                packedData.WriteTo(fs);
                fs.Flush();
                packedData.Dispose();
                packedData = null;
            }
            return res;
        }

        // DRAWING
        public override void DrawObject(Graphics grfx)
        {
            if ((DataLoaded) && (GridList != null) && (GridList.Length > 0))
            {
                if ((st_VisbleLevel / this.dx) > 0.12)
                {
                    for (int i = 0; i < GridList.Length; i++)
                    {
                        GridList[i].DrawBitMap(grfx);
                    }
                }
                else
                {
                    for (int i = 0; i < GridList.Length; i++)
                    {
                        GridList[i].DrawObject(grfx);
                    }

                }
                for (int i = 0; i < GridList.Length; i++)
                {
                    GridList[i].DrawLines(grfx);
                }
            }
        }
    }
}
