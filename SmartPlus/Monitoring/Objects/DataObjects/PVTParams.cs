﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using RDF;
using SmartPlus;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    /* PVT
    public class PVTParamsItem
    {
        /// <summary>
        /// Код площади месторождения
        /// </summary>
        public int OilFieldAreaCode;
        /// <summary>
        /// Код пласта
        /// </summary>
        public int PlastCode;
        /// <summary>
        /// Объемный коэф. нефти
        /// </summary>
        public double OilVolumeFactor;
        /// <summary>
        /// Плотность нефти в пов.усл.
        /// </summary>
        public double OilDensity;
        /// <summary>
        /// Вязкость нефти
        /// </summary>
        public double OilViscosity;
        /// <summary>
        /// Нач.н.нас-ть
        /// </summary>
        public double OilInitialSaturation;
        /// <summary>
        /// Объемный коэф. воды
        /// </summary>
        public double WaterVolumeFactor;
        /// <summary>
        /// Плотность воды в пов.усл
        /// </summary>
        public double WaterDensity;
        /// <summary>
        /// Вязкость нефти
        /// </summary>
        public double WaterViscosity;
        /// <summary>
        /// КИН
        /// </summary>
        public double KIN;
        /// <summary>
        /// Коэф.вытеснения
        /// </summary>
        public double DisplacementEfficiency;
        /// <summary>
        /// Пористость
        /// </summary>
        public double Porosity;
        /// <summary>
        /// Начальное пластовое давление
        /// </summary>
        public double InitialPressure;
        /// <summary>
        /// Давление насыщения
        /// </summary>
        public double SaturationPressure;
        /// <summary>
        /// Газовый фактор
        /// </summary>
        public double GasFactor;
        /// <summary>
        /// Проницаемость
        /// </summary>
        public double Permeability;
        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment = string.Empty;
        /// <summary>
        /// Нач.пластовое давление газа
        /// </summary>
        public double GasInitialPressure;
        /// <summary>
        /// Пористость по газу
        /// </summary>
        public double GasPorosity;
        /// <summary>
        /// Плотность свободного газа
        /// </summary>
        public double GasDensityFreeGas;
        /// <summary>
        /// Плотность растворенного газа
        /// </summary>
        public double GasDensitySoluteGas;
        /// <summary>
        /// Нач.насыщенность по газу
        /// </summary>
        public double GasInitialSaturation;
    }
    */
    public struct PVTParamsItem
    {
        /// <summary>
        /// Код площади месторождения
        /// </summary>
        public int OilFieldAreaCode;
        /// <summary>
        /// Код пласта
        /// </summary>
        public int StratumCode;
        /// <summary>
        /// Объемный коэффициент нефти
        /// </summary>
        public double OilVolumeFactor;
        /// <summary>
        /// Плотность нефти в пов.усл.
        /// </summary>
        public double OilDensity;
        /// <summary>
        /// Вязкость нефти
        /// </summary>
        public double OilViscosity;
        /// <summary>
        /// Нач.нефтенасыщенность
        /// </summary>
        public double OilInitialSaturation;
        /// <summary>
        /// Объемный коэффициент воды
        /// </summary>
        public double WaterVolumeFactor;
        /// <summary>
        /// Плотность воды в пов.усл
        /// </summary>
        public double WaterDensity;
        /// <summary>
        /// Вязкость воды
        /// </summary>
        public double WaterViscosity;
        /// <summary>
        /// КИН
        /// </summary>
        public double KIN;
        /// <summary>
        /// Коэффициент вытеснения
        /// </summary>
        public double DisplacementEfficiency;
        /// <summary>
        /// Пористость
        /// </summary>
        public double Porosity;
        /// <summary>
        /// Начальное пластовое давление
        /// </summary>
        public double InitialPressure;
        /// <summary>
        /// Давление насыщения
        /// </summary>
        public double SaturationPressure;
        /// <summary>
        /// Газовый фактор
        /// </summary>
        public double GasFactor;
        /// <summary>
        /// Проницаемость
        /// </summary>
        public double Permeability;
        /// <summary>
        /// Нач.пластовое давление газа
        /// </summary>
        public double GasInitialPressure;
        /// <summary>
        /// Пористость по газу
        /// </summary>
        public double GasPorosity;
        /// <summary>
        /// Плотность свободного газа
        /// </summary>
        public double GasDensityFreeGas;
        /// <summary>
        /// Плотность растворенного газа
        /// </summary>
        public double GasDensitySoluteGas;
        /// <summary>
        /// Нач.насыщенность по газу
        /// </summary>
        public double GasInitialSaturation;
        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment;

        public static PVTParamsItem Empty
        {
            get
            {
                PVTParamsItem Item;
                Item.OilFieldAreaCode = -1;
                Item.StratumCode = -1;
                Item.OilVolumeFactor = 0;
                Item.OilDensity = 0;
                Item.OilViscosity = 0;
                Item.OilInitialSaturation = 0;

                Item.WaterVolumeFactor = 0;
                Item.WaterDensity = 0;
                Item.WaterViscosity = 0;

                Item.KIN = 0;
                Item.DisplacementEfficiency = 0;
                Item.Porosity = 0;
                Item.InitialPressure = 0;
                Item.SaturationPressure = 0;
                Item.GasFactor = 0;
                Item.Permeability = 0;

                Item.GasInitialPressure = 0;
                Item.GasPorosity = 0;
                Item.GasDensityFreeGas = 0;
                Item.GasDensitySoluteGas = 0;
                Item.GasInitialSaturation = 0;
                Item.Comment = string.Empty;
                
                return Item;
           }
        }

        public bool IsEmpty { get { return ((OilFieldAreaCode == -1) && (StratumCode == -1)); } }
        public bool ReadFromBin(BinaryReader file)
        {
            if (file != null)
            {
                try
                {
                    this.OilFieldAreaCode = file.ReadInt32();
                    this.StratumCode = file.ReadInt32();
                    this.OilVolumeFactor = file.ReadDouble();
                    this.OilDensity = file.ReadDouble();
                    this.OilViscosity = file.ReadDouble();
                    this.OilInitialSaturation = file.ReadDouble();

                    this.WaterVolumeFactor = file.ReadDouble();
                    this.WaterDensity = file.ReadDouble();
                    this.WaterViscosity = file.ReadDouble();

                    this.DisplacementEfficiency = file.ReadDouble();
                    this.Porosity = file.ReadDouble();
                    this.InitialPressure = file.ReadDouble();
                    this.SaturationPressure = file.ReadDouble();
                    this.GasFactor = file.ReadDouble();
                    this.Permeability = file.ReadDouble();

                    this.GasInitialPressure = file.ReadDouble();
                    this.GasPorosity = file.ReadDouble();
                    this.GasDensityFreeGas = file.ReadDouble();
                    this.GasDensitySoluteGas = file.ReadDouble();
                    this.GasInitialSaturation = file.ReadDouble();
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
            return false;
        }
        public bool WriteToBin(BinaryWriter file)
        {
            if (file != null)
            {
                file.Write(this.OilFieldAreaCode);
                file.Write(this.StratumCode);
                file.Write(this.OilVolumeFactor);
                file.Write(this.OilDensity);
                file.Write(this.OilViscosity);
                file.Write(this.OilInitialSaturation);

                file.Write(this.WaterVolumeFactor);
                file.Write(this.WaterDensity);
                file.Write(this.WaterViscosity);

                file.Write(this.DisplacementEfficiency);
                file.Write(this.Porosity);
                file.Write(this.InitialPressure);
                file.Write(this.SaturationPressure);
                file.Write(this.GasFactor);
                file.Write(this.Permeability);

                file.Write(this.GasInitialPressure);
                file.Write(this.GasPorosity);
                file.Write(this.GasDensityFreeGas);
                file.Write(this.GasDensitySoluteGas);
                file.Write(this.GasInitialSaturation);
                return true;
            }
            return false;
        }
    }

    public class PVTParams : WellDataObject<PVTParamsItem>
    {
        public static int Version = 0;

        public bool ReadFromRDF(RdfConnector RdfConn, string MainKey)
        {
            DataDictionary plastDict = new DataDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            if (!plastDict.LoadFromRDF(RdfConn)) return false;

            string[] list = RdfConn.GetList(MainKey);
            if (list.Length > 0)
            {
                int ind;
                this.Items = new PVTParamsItem[list.Length];
                for (int i = 0; i < list.Length; i++)
                {
                    ind = plastDict.GetIndexByShortName(list[i]);
                    if (ind != -1)
                    {
                        MemoryStream ms = new MemoryStream();
                        ms = RdfConn.ReadBinary(MainKey + "@" + list[i] + "@physproperty", false);
                        if (ms == null)
                        {
                            Clear();
                            return false;
                        }
                        if (ConvertEx.IsRdfDataSetPacked(ref ms)) ConvertEx.UnpackRDFDataSet(ref ms);

                        BinaryReader br = new BinaryReader(ms);
                        ms.Seek(339, SeekOrigin.Begin);

                        if (ms != null)
                        {
                            this.Items[i] = PVTParamsItem.Empty;
                            this.Items[i].StratumCode = plastDict[ind].Code;
                            this.Items[i].OilVolumeFactor = br.ReadDouble();
                            this.Items[i].WaterVolumeFactor = br.ReadDouble();
                            this.Items[i].InitialPressure = br.ReadDouble();
                            this.Items[i].OilDensity = br.ReadDouble();
                            this.Items[i].OilViscosity = br.ReadDouble();
                            this.Items[i].WaterViscosity = br.ReadDouble();
                            this.Items[i].DisplacementEfficiency = br.ReadDouble();
                            this.Items[i].Porosity = br.ReadDouble();
                            this.Items[i].OilInitialSaturation = br.ReadDouble();
                            this.Items[i].SaturationPressure = br.ReadDouble();
                            this.Items[i].WaterDensity = br.ReadDouble();
                            br.Close();
                        }
                    }
                }
            }
            return true;
        }

        public PVTParamsItem GetItemByPlastCode(int OilFieldAreaCode, int PlastCode)
        {
            for (int i = 0; i < Count; i++)
            {
                if ((Items[i].OilFieldAreaCode == OilFieldAreaCode) &&
                    (Items[i].StratumCode == PlastCode))
                {
                    return Items[i];
                }
            }
            return PVTParamsItem.Empty;
        }
        PVTParamsItem GetItemByPlastNode(int OilFieldAreaCode, StratumTreeNode StratumNode, bool ByParentList)
        {
            PVTParamsItem pvt;
            pvt = GetItemByPlastCode(OilFieldAreaCode, StratumNode.StratumCode);
            if (pvt.IsEmpty)
            {
                List<StratumTreeNode> nodes;
                if (ByParentList)
                {
                    nodes = StratumNode.Parents;
                }
                else
                {
                    nodes = StratumNode.Childs;
                }
                if (nodes != null)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        pvt = GetItemByPlastCode(OilFieldAreaCode, nodes[i].StratumCode);
                        if (!pvt.IsEmpty) break;
                    }
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        pvt = GetItemByPlastNode(OilFieldAreaCode, nodes[i], ByParentList);
                        if (!pvt.IsEmpty) break;
                    }
                }
            }
            return pvt;
        }
        public PVTParamsItem GetItemByPlastHierarchy(int OilFieldAreaCode, int StratumCode, StratumDictionary StratumDict)
        {
            PVTParamsItem pvt = PVTParamsItem.Empty;
            StratumTreeNode plastNode = StratumDict.GetStratumTreeNode(StratumCode);
            pvt = GetItemByPlastCode(OilFieldAreaCode, StratumCode);
            if (!pvt.IsEmpty)
            {
                pvt.Comment = "PVT найдены по коду;0;" + plastNode.Name;
            }
            pvt = GetItemByPlastNode(OilFieldAreaCode, plastNode, true);
            if ((!pvt.IsEmpty) && (pvt.Comment == ""))
            {
                StratumTreeNode plNode = StratumDict.GetStratumTreeNode(pvt.StratumCode);
                pvt.Comment = "PVT взяты с вышележащего по иерархии пласта;1;" + plNode.Name;
            }
            // пробегаем по детям
            pvt = GetItemByPlastNode(OilFieldAreaCode, plastNode, false);
            if ((!pvt.IsEmpty) && (pvt.Comment == ""))
            {
                StratumTreeNode plNode = StratumDict.GetStratumTreeNode(pvt.StratumCode);
                pvt.Comment = "PVT взяты с нижележащего по иерархии пласта;2;" + plNode.Name;
            }
            return pvt;
        }

        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            try
            {
                int count = br.ReadInt32();
                PVTParamsItem[] newItems = new PVTParamsItem[count];
                for (int i = 0; i < count; i++)
                {
                    newItems[i].ReadFromBin(br);
                }
                SetItems(newItems);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(Count);
                for (int i = 0; i < Items.Length; i++)
                {
                    Items[i].WriteToBin(bw);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
