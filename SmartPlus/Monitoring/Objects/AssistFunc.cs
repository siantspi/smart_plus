﻿using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Data.OleDb;
using System.Security.Principal;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Runtime.InteropServices.ComTypes;

namespace SmartPlus
{
    public static class Ending
    {
        public static string GetWellEnding(int count)
        {
            string res = "";
            int end = count % 10;
            if (!((count > 10) && (count < 19)))
            {
                switch (end)
                {
                    case 1:
                        res = "а";
                        break;
                    case 2:
                        res = "ы";
                        break;
                    case 3:
                        res = "ы";
                        break;
                    case 4:
                        res = "ы";
                        break;
                    default:
                        res = "";
                        break;
                }
            }
            return res;
        }
    }

    public static class Geometry
    {
        /// <summary>
        /// Расчет уравнения линейного тренда
        /// </summary>
        /// <param name="points">Набор входных точек X, Y</param>
        /// <param name="k">Коэффициент k в уравнении y = kx + b</param>
        /// <param name="b">Коэффициент b в уравнении y = kx + b</param>
        /// <param name="R2">Достоверность аппроксимации R2</param>
        public static void GetLinearTrend(PointD[] points, out double k, out double b, out double R2)
        {
            k = 0;
            b = 0;
            R2 = 0;
            if (points.Length > 1)
            {
                int count = 0;
                double sumSqx = 0, sumXY = 0, sumX = 0, sumY = 0, Z = 0, Yr, average = 0;
                double ess = 0, tss = 0;
                for (int i = 0; i < points.Length; i++)
                {
                    sumSqx += points[i].X * points[i].X;
                    sumX += points[i].X;
                    sumY += points[i].Y;
                    sumXY += points[i].X * points[i].Y;
                    average += points[i].Y;
                    count++;
                }
                Z = count * sumSqx - sumX * sumX;
                if ((Z == 0) || (count < 2)) return;

                k = (count * sumXY - sumY * sumX) / Z;
                b = (sumY * sumSqx - sumXY * sumX) / Z;

                average /= count;
                for (int i = 0; i < points.Length; i++)
                {
                    if (points[i].Y > 0)
                    {
                        Yr = k * points[i].X + b;
                        ess += (points[i].Y - Yr) * (points[i].Y - Yr);
                        tss += (points[i].Y - average) * (points[i].Y - average);
                    }
                }
                if(tss > 0) R2 = 1 - ess / tss;
            }
        }

        public struct Rect
        {
            public double Xmin;
            public double Ymin;
            public double Xmax;
            public double Ymax;
        }
        public static PointD[] CreateRect(double Xmin, double Ymin, double Xmax, double Ymax)
        {
            PointD[] rect = new PointD[4];
            rect[0].X = Xmin;
            rect[0].Y = Ymin;
            rect[1].X = Xmin;
            rect[1].Y = Ymax;
            rect[2].X = Xmax;
            rect[2].Y = Ymax;
            rect[3].X = Xmax;
            rect[3].Y = Ymin;
            return rect;
        }

        static int Cohen_Sutherland_Vcode(Rect r, PointD p)
        {
            return (p.X < r.Xmin ? 1 : 0) + (p.X > r.Xmax ? 2 : 0) +
                   (p.Y < r.Ymin ? 4 : 0) + (p.Y > r.Ymax ? 8 : 0);
        }

        public static int Cohen_Sutherland(Rect rect, ref PointD a, ref PointD b)
        {
            /* если отрезок ab не пересекает прямоугольник r, функция возвращает -1;
             * если отрезок ab находится внутри прямоугольника r, функция возвращает 0;
               если отрезок ab пересекает прямоугольник r, функция возвращает 1
               и отсекает те части отрезка, которые находятся вне прямоугольника */
            int code_a, code_b, code;
            bool intersect = false;
            bool ca;
            PointD c;
            code_a = Cohen_Sutherland_Vcode(rect, a);
            code_b = Cohen_Sutherland_Vcode(rect, b);

            while (code_a + code_b > 0)
            {
                if ((code_a & code_b) > 0) return -1;
                if (code_a > 0)
                {
                    code = code_a;
                    c = a;
                    ca = true;
                }
                else
                {
                    code = code_b;
                    c = b;
                    ca = false;
                }

                if ((code & 1) > 0)
                {
                    c.Y += (a.Y - b.Y) * (rect.Xmin - c.X) / (a.X - b.X);
                    c.X = rect.Xmin;
                }
                else if ((code & 2) > 0)
                {
                    c.Y += (a.Y - b.Y) * (rect.Xmax - c.X) / (a.X - b.X);
                    c.X = rect.Xmax;
                }

                if ((code & 4) > 0) 
                {
                    c.X += (a.X - b.X) * (rect.Ymin - c.Y) / (a.Y - b.Y);
                    c.Y = rect.Ymin;
                }
                else if ((code & 8) > 0) // ниже bottom
                {
                    c.X += (a.X - b.X) * (rect.Ymax - c.Y) / (a.Y - b.Y);
                    c.Y = rect.Ymax;
                }
                if (ca) a = c; else b = c;

                if (code == code_a)
                    code_a = Cohen_Sutherland_Vcode(rect, a);
                else
                    code_b = Cohen_Sutherland_Vcode(rect, b);
                intersect = true;
            }
            return (intersect ? 1 : 0);
        }
        static void SetPointIntoRect(ref PointD[] poly, PointD point)
        {
            if (point.X == poly[0].X)
            {
                if (poly[1].Y == Constant.DOUBLE_NAN)
                {
                    poly[1] = point;
                }
                else if (poly[1].Y > point.Y)
                {
                    poly[2] = poly[1];
                    poly[1] = point;
                }
                else if (poly[1].Y < point.Y)
                {
                    poly[2] = point;
                }
            }
            else if (point.X == poly[6].X)
            {
                if (poly[7].Y == Constant.DOUBLE_NAN)
                {
                    poly[7] = point;
                }
                else if (poly[7].Y < point.Y)
                {
                    poly[8] = poly[7];
                    poly[7] = point;
                }
                else if (poly[7].Y > point.Y)
                {
                    poly[8] = point;
                }
            }
            else if (point.Y == poly[3].Y)
            {
                if (poly[4].X == Constant.DOUBLE_NAN)
                {
                    poly[4] = point;
                }
                else if (point.X < poly[4].X)
                {
                    poly[5] = poly[4];
                    poly[4] = point;
                }
                else if (poly[4].X < point.X)
                {
                    poly[5] = point;
                }
            }
            else if (point.Y == poly[9].Y)
            {
                if (poly[10].X == Constant.DOUBLE_NAN)
                {
                    poly[10] = point;
                }
                else if (poly[10].X < point.X)
                {
                    poly[11] = poly[10];
                    poly[10] = point;
                }
                else if (poly[10].X > point.X)
                {
                    poly[11] = point;
                }
            }
        }
        static void SetPolyIntersect(ref int[] ind1, ref int[] ind2, PointD[] poly1, PointD[] poly2)
        {
            int i, j;
            ind1 = new int[poly1.Length];
            ind2 = new int[poly2.Length];
            for (i = 0; i < ind1.Length; i++) ind1[i] = -1;
            for (i = 0; i < ind2.Length; i++) ind2[i] = -1;

            for (i = 0; i < poly1.Length; i++)
            {
                for(j = 0;  j < poly2.Length; j++)
                {
                    if ((poly1[i].X != Constant.DOUBLE_NAN) && (poly1[i].X == poly2[j].X) && (poly1[i].Y == poly2[j].Y))
                    {
                        ind1[i] = j;
                        ind2[j] = i;
                        break;
                    }
                }
            }
        }
        public static PointD[] Weiler_Atherton_Rect(PointD[] poly1, PointD[] poly2)
        {
            Rect r;
            PointD a, b;
            PointD[] resPoints = null;
            PointD[] newPoly1 = new PointD[12];
            PointD[] newPoly2 = new PointD[12];
            int[] ind1 = null, ind2 = null;
            int OutSideCount = 0, InSideCount = 0;
            r.Xmin = poly1[0].X;
            r.Ymin = poly1[0].Y;
            r.Xmax = poly1[2].X;
            r.Ymax = poly1[2].Y;
            int i = 0, j, k, res, intersect = 0, ind = 0, indNew = 0, count = 0;
            while (i < 4)
            {
                newPoly1[i * 3].X = poly1[i].X;
                newPoly1[i * 3].Y = poly1[i].Y;
                newPoly1[i * 3 + 1].X = Constant.DOUBLE_NAN;
                newPoly1[i * 3 + 1].Y = Constant.DOUBLE_NAN;
                newPoly1[i * 3 + 2].X = Constant.DOUBLE_NAN;
                newPoly1[i * 3 + 2].Y = Constant.DOUBLE_NAN;

                newPoly2[i * 3].X = poly2[i].X;
                newPoly2[i * 3].Y = poly2[i].Y;
                newPoly2[i * 3 + 1].X = Constant.DOUBLE_NAN;
                newPoly2[i * 3 + 1].Y = Constant.DOUBLE_NAN;
                newPoly2[i * 3 + 2].X = Constant.DOUBLE_NAN;
                newPoly2[i * 3 + 2].Y = Constant.DOUBLE_NAN;
                i++;
            }
            i = 0;
            while (i < 4)
            {
                count += 2; 
                a.X = poly2[i].X;
                a.Y = poly2[i].Y;
                j = (i < 3) ? i + 1 : 0;
                b.X = poly2[j].X;
                b.Y = poly2[j].Y;
                res = Cohen_Sutherland(r, ref a, ref b);

                if (res == 1)
                {
                    k = 0;
                    if (((a.X != poly2[i].X) || (a.Y != poly2[i].Y)) &&
                        ((a.X == r.Xmin) || (a.X == r.Xmax) || (a.Y == r.Ymin) || (a.Y == r.Ymax)))
                    {
                        newPoly2[i * 3 + 1].X = a.X;
                        newPoly2[i * 3 + 1].Y = a.Y;
                        k = 1;
                        count++;
                    }
                    if (((b.X != poly2[j].X) || (b.Y != poly2[j].Y)) && 
                        ((b.X == r.Xmin) || (b.X == r.Xmax) || (b.Y == r.Ymin) || (b.Y == r.Ymax)))
                    {
                        newPoly2[i * 3 + 1 + k].X = b.X;
                        newPoly2[i * 3 + 1 + k].Y = b.Y;
                        count++;
                    }
                    SetPointIntoRect(ref newPoly1, a);
                    SetPointIntoRect(ref newPoly1, b);
                }
                else if (res == 0)
                {
                    InSideCount++;
                }
                else if (res == -1)
                {
                    OutSideCount++;
                }
                i++;
            }
            if (InSideCount == 4) // повернутый целиком внутри параллельного
            {
 
            }
            else if (OutSideCount == 4) // параллельный целиком внутри повернутого
            {

            }
            else // пересечение
            {
                SetPolyIntersect(ref ind1, ref ind2, newPoly1, newPoly2);
                System.Collections.ArrayList list = new System.Collections.ArrayList();
                bool findStart = false, reverse = false;
                PointD[] poly = newPoly2;
                int[] indPoly = ind2; 
                i = 1;
                while(i < poly.Length)
                {
                    if ((!findStart) && (i % 3 != 0) && (poly[i].X != Constant.DOUBLE_NAN))
                    {
                        findStart = true;
                        list.Add(poly[i]);
                        k = i + 1;
                        while ((k < poly.Length) && (poly[k].X == Constant.DOUBLE_NAN))
                        {
                            k++;
                        }
                        if (k < poly.Length)
                        {
                            if (!Geometry.Contains_Point(poly1, poly[k]))
                            {
                                reverse = !reverse;
                                poly = newPoly1;
                                i = indPoly[i] + 1;
                                indPoly = ind1;
                            }
                            else
                                i = k;
                        }
                        continue;
                    }
                    if (findStart)
                    {
                        if (poly[i].X != Constant.DOUBLE_NAN)
                        {
                            if (i % 3 != 0)
                            {
                                if (list.IndexOf(poly[i]) == -1)
                                    list.Add(poly[i]);
                                else
                                    break;
                                reverse = !reverse;
                                if (reverse)
                                {
                                    poly = newPoly1;
                                    i = indPoly[i] + 1;
                                    indPoly = ind1;

                                }
                                else
                                {
                                    poly = newPoly2;
                                    i = indPoly[i] + 1;
                                    indPoly = ind2;
                                }
                            }
                            else
                            {
                                list.Add(poly[i]);
                                i++;
                            }
                        }
                        else
                        {
                            i++;
                            if (i == poly.Length) i = 0;
                        }
                    }
                    else
                        i++;
                }
                resPoints = new PointD[list.Count];
                for (i = 0; i < list.Count; i++)
                {
                    resPoints[i] = (PointD)list[i];
                }
            }
            return resPoints;
        }

        public static double Square_Triangle(PointD[] polygon)
        {
            if (polygon.Length == 3)
            {
                double[] a = new double[3];
                double p = 0, s = 0;
                double x1, y1, x2, y2;
                int i = 0;
                while (i < 3)
                {
                    x1 = polygon[i].X;
                    y1 = polygon[i].Y;
                    if (i < 2)
                    {
                        x2 = polygon[i + 1].X;
                        y2 = polygon[i + 1].Y;
                    }
                    else
                    {
                        x2 = polygon[0].X;
                        y2 = polygon[0].Y;
                    }
                    a[i] = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
                    p += a[i];
                    i++;
                }
                p = p / 2;
                s = Math.Sqrt(p * (p - a[0]) * (p - a[1]) * (p - a[2]));
                return s;
            }
            return 0;
        }
        public static double Intersect_Square(PointD[] poly1, PointD[] poly2)
        {
            double square = 0;
            PointD[] points = Weiler_Atherton_Rect(poly1, poly2);
            PointD[] triangle = new PointD[3];
            if ((points != null) && (points.Length > 3))
            {
                int i;
                triangle[0].X = points[0].X;
                triangle[0].Y = points[0].Y;
                for (i = 1; i < points.Length - 1; i++)
                {
                    triangle[1].X = points[i].X;
                    triangle[1].Y = points[i].Y;
                    triangle[2].X = points[i + 1].X;
                    triangle[2].Y = points[i + 1].Y;
                    square += Square_Triangle(triangle);
                }
            }
            return square;
        }
        public static bool Contains_Point(PointD[] rect, PointD point)
        {
            if((rect.Length == 4) && (point.X != Constant.DOUBLE_NAN) && (point.Y != Constant.DOUBLE_NAN))
            {
                if ((point.X >= rect[0].X) && (point.X <= rect[2].X) &&
                   (point.Y >= rect[0].Y) && (point.Y <= rect[2].Y))
                {
                    return true;
                }
            }
            return false;
        }

        public static PointD GetPointInLineByDistance(PointD pt1, PointD pt2, double Distance)
        {
            PointD res;
            if (pt1.Y == pt2.Y) // гориз
            {
                int k = (pt1.X < pt2.X) ? 1 : -1;
                res.X = pt1.X + k * Distance;
                res.Y = pt1.Y;
            }
            else if (pt1.X == pt2.X) // верт
            {
                int k = (pt1.Y < pt2.Y) ? 1 : -1;
                res.X = pt1.X;
                res.Y = pt1.Y + k * Distance;
            }
            else
            {
                double x1, x2, y1, y2;
                y1 = Distance / Math.Sqrt(Math.Pow((pt2.X - pt1.X) / (pt2.Y - pt1.Y), 2) + 1) + pt1.Y;
                x1 = (pt2.X - pt1.X) * (y1 - pt1.Y) / (pt2.Y - pt1.Y) + pt1.X;

                y2 = -Distance / Math.Sqrt(Math.Pow((pt2.X - pt1.X) / (pt2.Y - pt1.Y), 2) + 1) + pt1.Y;
                x2 = (pt2.X - pt1.X) * (y2 - pt1.Y) / (pt2.Y - pt1.Y) + pt1.X;

                double minX = pt1.X, maxX = pt2.X;
                double minY = pt1.Y, maxY = pt2.Y;
                if (pt2.X < pt1.X) { minX = pt2.X; maxX = pt1.X; }
                if (pt2.Y < pt1.Y) { minY = pt2.Y; maxY = pt1.Y; }

                if ((minX < x1) && (x1 < maxX) && (minY < y1) && (y1 < maxY))
                {
                    res.X = x1;
                    res.Y = y1;
                }
                else
                {
                    res.X = x2;
                    res.Y = y2;
                }
            }
            return res;
        }
        public static PointD GetPointInLineByPoint(PointD pt1, PointD pt2, PointD pt3)
        {
            PointD res;
            if (pt1.Y == pt2.Y) // гориз
            {
                res.X = pt3.X;
                res.Y = pt1.Y;
            }
            else if (pt1.X == pt2.X) // верт
            {
                res.X = pt1.X;
                res.Y = pt3.Y;
            }
            else
            {
                double k1 , k2, b1, b2;
                k1 = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
                k2 = -1 / k1; // перпендикулярная прямая
                b1 = pt1.Y - k1 * pt1.X;
                b2 = pt3.Y - k2 * pt3.X;
                res.X = (b2 - b1) / (k1 - k2);
                res.Y = k1 * res.X + b1;
            }
            return res;
        }
        public static PointD GetIntersectLinesPoint2(PointD pt1, PointD pt2, PointD pt3, PointD pt4)
        {
            PointD res, empty;
            res.X = Constant.DOUBLE_NAN;
            res.Y = Constant.DOUBLE_NAN;
            empty.X = Constant.DOUBLE_NAN;
            empty.Y = Constant.DOUBLE_NAN;

            double k1, k2;
            bool byFirst = true;

            if (pt1.X == pt2.X)
            {
                res.X = pt1.X;
                byFirst = false;
            }
            else if (pt3.X == pt4.X)
            {
                res.X = pt3.X;
                byFirst = true;
            }
            else
            {
                k1 = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
                k2 = (pt4.Y - pt3.Y) / (pt4.X - pt3.X);
                if (k1 != k2)
                {
                    res.X = (pt3.Y - pt1.Y + k1 * pt1.X - k2 * pt3.X) / (k1 - k2);
                    byFirst = true;
                }
                else // 2 гориз линии
                {
                    if (((pt1.X == pt3.X) && (pt1.Y == pt3.Y)) || ((pt1.X == pt4.X) && (pt1.Y == pt4.Y)))
                    {
                        return pt1;
                    }
                    else if (((pt2.X == pt3.X) && (pt2.Y == pt3.Y)) || ((pt2.X == pt4.X) && (pt2.Y == pt4.Y)))
                    {
                        return pt2;
                    }
                    else
                    {
                        return empty;
                    }
                }
            }

            if(res.X != Constant.DOUBLE_NAN)
            {
                if (byFirst)
                {
                    k1 = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
                    res.Y = k1 * (res.X - pt1.X) + pt1.Y;
                }
                else
                {
                    if (pt3.X == pt4.X) // 2 верт линии
                    {
                        if ((pt1.X == pt3.X) && (pt1.Y == pt3.Y))
                        {
                            return pt1;
                        }
                        else if ((pt2.X == pt4.X) && (pt2.Y == pt4.Y))
                        {
                            return pt2;
                        }
                        else
                        {
                            return empty;
                        }
                    }
                    else
                    {
                        k2 = (pt4.Y - pt3.Y) / (pt4.X - pt3.X);
                        res.Y = k2 * (res.X - pt3.X) + pt3.Y;
                    }
                }
                if (res.Y != Constant.DOUBLE_NAN)
                {
                    double min = pt1.X, max = pt2.X;
                    if (pt2.X < pt1.X) { min = pt2.X; max = pt1.X; }
                    if ((min <= res.X) && (res.X <= max))
                    {
                        min = pt1.Y;
                        max = pt2.Y;
                        if (pt2.Y < pt1.Y) { min = pt2.Y; max = pt1.Y; }
                        if ((min <= res.Y) && (res.Y <= max))
                        {
                            min = pt3.X;
                            max = pt4.X;
                            if (pt4.X < pt3.X) { min = pt4.X; max = pt3.X; }
                            if ((min <= res.X) && (res.X <= max))
                            {
                                min = pt3.Y;
                                max = pt4.Y;
                                if (pt4.Y < pt3.Y) { min = pt4.Y; max = pt3.Y; }
                                if ((res.Y < min) || (res.Y > max))
                                {
                                    res = empty;
                                }
                            }
                            else
                            {
                                res = empty;
                            }
                        }
                        else
                        {
                            res = empty;
                        }
                    }
                    else
                    {
                        res = empty;
                    }
                }
            }
            return res;
        }

        public static PointD GetIntersectLinesPoint(PointD pt1, PointD pt2, PointD pt3, PointD pt4)
        {
            PointD res;
            res.X = Constant.DOUBLE_NAN;
            res.Y = Constant.DOUBLE_NAN;

            if ((pt1.X == pt2.X) && (pt3.X == pt4.X)) return res;
            if ((pt1.Y == pt2.Y) && (pt3.Y == pt4.Y)) return res;
            double d = (pt2.X - pt1.X) * (pt4.Y - pt3.Y) - (pt4.X - pt3.X) * (pt2.Y - pt1.Y);
            if (d == 0) return res;
            double r = ((pt1.Y - pt3.Y) * (pt4.X - pt3.X) - (pt1.X - pt3.X) * (pt4.Y - pt3.Y)) / d;
            res.X = (pt2.X - pt1.X) * r + pt1.X;
            res.Y = (pt2.Y - pt1.Y) * r + pt1.Y;
            double minX = pt1.X, maxX = pt2.X, minY = pt1.Y, maxY = pt2.Y;
            if (pt2.X < pt1.X) { minX = pt2.X; maxX = pt1.X; }
            if (pt2.Y < pt1.Y) { minY = pt2.Y; maxY = pt1.Y; }
            if ((minX <= res.X) && (res.X <= maxX) && (minY <= res.Y) && (res.Y <= maxY))
            {
                minX = pt3.X; maxX = pt4.X; minY = pt3.Y; maxY = pt4.Y;
                if (pt4.X < pt3.X) { minX = pt4.X; maxX = pt3.X; }
                if (pt4.Y < pt3.Y) { minY = pt4.Y; maxY = pt3.Y; }
                if ((minX > res.X) || (res.X > maxX) || (minY > res.Y) || (res.Y > maxY))
                {
                    res.X = Constant.DOUBLE_NAN;
                    res.Y = Constant.DOUBLE_NAN;
                }
            }
            else
            {
                res.X = Constant.DOUBLE_NAN;
                res.Y = Constant.DOUBLE_NAN;
            }
            return res;
        }
        public static bool IsPointInLine(PointF line1, PointF line2, PointF pt)
        {
            PointD pt1, pt2, pt3;
            pt1.X = line1.X; pt1.Y = line1.Y;
            pt2.X = line2.X; pt2.Y = line2.Y;
            pt3.X = pt.X; pt3.Y = pt.Y;
            return IsPointInLine(pt1, pt2, pt3);
        }
        public static bool IsPointInLine(PointD line1, PointD line2, PointD pt)
        {
            bool res = false;

            if (line1.X == line2.X)
            {
                double minY = line1.Y, maxY = line2.Y;
                if (line2.Y < line1.Y) { minY = line2.Y; maxY = line1.Y; }
                if ((line1.X == pt.X) && (minY < pt.Y) && (pt.Y < maxY))
                {
                    return true;
                }
            }
            else if (line1.Y == line2.Y)
            {
                double minX = line1.X, maxX = line2.X;
                if (line2.X < line1.X) { minX = line2.X; maxX = line1.X; }
                if ((line1.Y == pt.Y) && (minX < pt.X) && (pt.X < maxX))
                {
                    return true;
                }
            }
            else
            {
                double y = (line2.Y - line1.Y) * (pt.X - line1.X) / (line2.X - line1.X) + line1.Y;
                if (Math.Abs(y - pt.Y) < 0.00001) res = true;
            }
            if (res)
            {
                res = false;
                double minX = line1.X, maxX = line2.X, minY = line1.Y, maxY = line2.Y;
                if (line2.X < line1.X) { minX = line2.X; maxX = line1.X; }
                if (line2.Y < line1.Y) { minY = line2.Y; maxY = line1.Y; }
                if ((minX < pt.X) && (pt.X < maxX) && (minY < pt.Y) && (pt.Y < maxY))
                {
                    res = true;
                }
            }
            return res;
        }
        public static PointD GetNormalPointInLine(PointD pt1, PointD pt2, PointD pt0, double DistByLine, bool GetLeftPoint)
        {
            PointD ptX1, ptX2;
            if (pt1.Y == pt2.Y) // гориз
            {
                ptX1.X = pt0.X;
                ptX1.Y = pt1.Y - DistByLine;
                ptX2.X = pt0.X;
                ptX2.Y = pt1.Y + DistByLine;
            }
            else if (pt1.X == pt2.X) // верт
            {
                ptX1.X = pt1.X - DistByLine;
                ptX1.Y = pt0.Y;
                ptX2.X = pt1.X + DistByLine;
                ptX2.Y = pt0.Y;
            }
            else // наклон к оси
            {
                double k, B, D;
                k = -(pt2.X - pt1.X) / (pt2.Y - pt1.Y);
                B = pt0.Y - k * pt0.X;
                D = Math.Pow(-2 * pt0.X + 2 * k * (B - pt0.Y), 2) - 4 * (1 + k * k) * (pt0.X * pt0.X + Math.Pow(B - pt0.Y, 2) - DistByLine * DistByLine);
                ptX1.X = (-(-2 * pt0.X + 2 * k * (B - pt0.Y)) + Math.Sqrt(D)) / (2 + 2 * k * k);
                ptX1.Y = k * ptX1.X + B;
                ptX2.X = (-(-2 * pt0.X + 2 * k * (B - pt0.Y)) - Math.Sqrt(D)) / (2 + 2 * k * k);
                ptX2.Y = k * ptX2.X + B;
            }
            bool leftX1 = IsPointInLeftSide(pt1, pt2, ptX1);
            if (!GetLeftPoint) leftX1 = !leftX1;

            if (leftX1)
                return ptX1;
            else
                return ptX2;
        }
        //public static double GetDistanceToNormalPointByLine(XY pt1, XY pt2, XY pt0)
        //{
        //    XY pt3;
        //    double k, B1, B2;
        //    k = (pt2._X - pt1._X) / (pt2._Y - pt1._Y);
        //    B1 = pt1._Y - k * pt1._X;
        //    B2 = pt0._Y + k * pt0._X;
        //    pt3._X = (B2 - B1) / (2 * k);
        //    pt3._Y = -k * pt3._X + B2;
        //    return Math.Sqrt(Math.Pow(pt3._X - pt0._X, 2) + Math.Pow(pt3._Y - pt0._Y, 2));
        //}
        public static bool IsPointInLeftSide(PointD pt1, PointD pt2, PointD pt0)
        {
            double k;
            if (pt1.Y == pt2.Y) // гориз
            {
                return (((pt1.X < pt2.X) && (pt0.Y > pt1.Y)) || ((pt1.X > pt2.X) && (pt0.Y < pt1.Y)));
            }
            else if (pt1.X == pt2.X) // вертик
            {
                return (((pt2.Y > pt1.Y) && (pt0.X < pt1.X)) || ((pt2.Y < pt1.Y) && (pt0.X > pt1.X)));
            }
            else // наклон
            {
                k = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
                double Ypr = k * (pt0.X - pt1.X) + pt1.Y;
                bool res = (((k > 0) && (pt0.Y > Ypr)) || ((k < 0) && (pt0.Y < Ypr)));
                if (pt2.Y < pt1.Y) res = !res;
                return res;
            }
        }


        public static bool IntersectRectangle(RectangleF rect1, RectangleF rect2)
        {
            int kX, kY;
            if ((rect2.Left < rect1.Left) && (rect2.Right < rect1.Left))
            {
                kX = 1;
            }
            else if ((rect2.Left > rect1.Right) && (rect2.Right > rect1.Right))
            {
                kX = 1;
            }
            else
            {
                kX = -1;
            }
            if ((rect2.Top < rect1.Top) && (rect2.Bottom < rect1.Top))
            {
                kY = 1;
            }
            else if ((rect2.Top > rect1.Bottom) && (rect2.Bottom > rect1.Bottom))
            {
                kY = 1;
            }
            else
            {
                kY = -1;
            }
            return (kX * kY < 0);
        }
        public static bool IntersectRectangle(RectangleF rect1, float Xmin, float Ymin, float Xmax, float Ymax)
        {
            int kX, kY;
            if ((Xmin < rect1.Left) && (Xmax < rect1.Left))
            {
                kX = 1;
            }
            else if ((Xmin > rect1.Right) && (Xmax > rect1.Right))
            {
                kX = 1;
            }
            else
            {
                kX = -1;
            }
            if ((Ymin < rect1.Top) && (Ymax < rect1.Top))
            {
                kY = 1;
            }
            else if ((Ymin > rect1.Bottom) && (Ymax > rect1.Bottom))
            {
                kY = 1;
            }
            else
            {
                kY = -1;
            }
            return (kX + kY < 0);
        }
        public static bool Contains_Point(PointD pt1, PointD pt2, PointD ptAim)
        {
            double minX = pt1.X, maxX = pt2.X, minY = pt1.Y, maxY = pt2.Y;
            if (pt1.X > pt2.X)
            {
                minX = pt2.X;
                maxX = pt1.X;
            }
            if (pt1.Y > pt2.Y)
            {
                minY = pt2.Y;
                maxY = pt1.Y;
            }
            if ((minX <= ptAim.X) && (ptAim.X <= maxX) && (minY <= ptAim.Y) && (ptAim.Y <= maxY))
            {
                return true;
            }
            return false;
        }

        public static Contour CreateCircleContour(double Xcenter, double Ycenter, double Radius, double Step)
        {
            Contour cntr = new Contour(4);
            if (Step > Radius || Radius == 0 || Step == 0) return cntr;

            double r = Step / Radius;
            double alfa = Math.Acos(1 - r * r / 2);
            double sumAlfa = Math.PI * 2;
            double x, y;
            while (sumAlfa >= 0)
            {
                x = Xcenter + Radius * Math.Cos(sumAlfa);
                y = Ycenter + Radius * Math.Sin(sumAlfa);
                cntr.Add(x, y);
                sumAlfa -= alfa;
            }
            cntr.SetMinMax();
            cntr._Closed = true;
            return cntr;
        }
    }

    public static class App
    {
        public static string GetRegistryPath()
        {
            string result = string.Empty;
            WindowsIdentity wid = WindowsIdentity.GetCurrent();
            if (wid != null)
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\MiR", false);
                if (key == null) key = Registry.CurrentUser.OpenSubKey(@"Software\SmartPlus", false);
                if (key != null)
                {
                    result = Convert.ToString(key.GetValue("CachePath"));
                    key.Close();
                }
            }
            return result;
        }

        public static int MaxMonitorWidth
        {
            get
            {
                int max = 0;
                System.Windows.Forms.Screen[] all_scr = System.Windows.Forms.Screen.AllScreens;
                for (int i = 0; i < all_scr.Length; i++)
                {
                    if (max < all_scr[i].Bounds.Width) max = all_scr[i].Bounds.Width;
                }
                return max;
            }
        }
        public static int MaxMonitorHeight
        {
            get
            {
                int max = 0;
                System.Windows.Forms.Screen[] all_scr = System.Windows.Forms.Screen.AllScreens;
                for (int i = 0; i < all_scr.Length; i++)
                {
                    if (max < all_scr[i].Bounds.Height) max = all_scr[i].Bounds.Height;
                }
                return max;
            }
        }
    }

    public class ClipboardHelper
    {
        [DllImport("user32.dll")]
        static extern bool OpenClipboard(IntPtr hWndNewOwner);
        [DllImport("user32.dll")]
        static extern bool EmptyClipboard();
        [DllImport("user32.dll")]
        static extern IntPtr SetClipboardData(uint uFormat, IntPtr hMem);
        [DllImport("user32.dll")]
        static extern bool CloseClipboard();
        [DllImport("gdi32.dll")]
        static extern IntPtr CopyEnhMetaFile(IntPtr hemfSrc, IntPtr hNULL);
        [DllImport("gdi32.dll")]
        static extern bool DeleteEnhMetaFile(IntPtr hemf);
        [DllImport("User32.dll")]
        public static extern IntPtr GetDC(IntPtr hWnd);


        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("0000000B-0000-0000-C000-000000000046")]
        public interface IStorage
        {
            [return: MarshalAs(UnmanagedType.Interface)]
            IStream CreateStream([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, [In, MarshalAs(UnmanagedType.U4)] int grfMode, [In, MarshalAs(UnmanagedType.U4)] int reserved1, [In, MarshalAs(UnmanagedType.U4)] int reserved2);
            [return: MarshalAs(UnmanagedType.Interface)]
            IStream OpenStream([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, IntPtr reserved1, [In, MarshalAs(UnmanagedType.U4)] int grfMode, [In, MarshalAs(UnmanagedType.U4)] int reserved2);
            [return: MarshalAs(UnmanagedType.Interface)]
            IStorage CreateStorage([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, [In, MarshalAs(UnmanagedType.U4)] int grfMode, [In, MarshalAs(UnmanagedType.U4)] int reserved1, [In, MarshalAs(UnmanagedType.U4)] int reserved2);
            [return: MarshalAs(UnmanagedType.Interface)]
            IStorage OpenStorage([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, IntPtr pstgPriority, [In, MarshalAs(UnmanagedType.U4)] int grfMode, IntPtr snbExclude, [In, MarshalAs(UnmanagedType.U4)] int reserved);
            void CopyTo(int ciidExclude, [In, MarshalAs(UnmanagedType.LPArray)] Guid[] pIIDExclude, IntPtr snbExclude, [In, MarshalAs(UnmanagedType.Interface)] IStorage stgDest);
            void MoveElementTo([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, [In, MarshalAs(UnmanagedType.Interface)] IStorage stgDest, [In, MarshalAs(UnmanagedType.BStr)] string pwcsNewName, [In, MarshalAs(UnmanagedType.U4)] int grfFlags);
            void Commit(int grfCommitFlags);
            void Revert();
            void EnumElements([In, MarshalAs(UnmanagedType.U4)] int reserved1, IntPtr reserved2, [In, MarshalAs(UnmanagedType.U4)] int reserved3, [MarshalAs(UnmanagedType.Interface)] out object ppVal);
            void DestroyElement([In, MarshalAs(UnmanagedType.BStr)] string pwcsName);
            void RenameElement([In, MarshalAs(UnmanagedType.BStr)] string pwcsOldName, [In, MarshalAs(UnmanagedType.BStr)] string pwcsNewName);
            void SetElementTimes([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, [In] System.Runtime.InteropServices.ComTypes.FILETIME pctime, [In] System.Runtime.InteropServices.ComTypes.FILETIME patime, [In] System.Runtime.InteropServices.ComTypes.FILETIME pmtime);
            void SetClass([In] ref Guid clsid);
            void SetStateBits(int grfStateBits, int grfMask);
            void Stat([Out]out System.Runtime.InteropServices.ComTypes.STATSTG pStatStg, int grfStatFlag);
        }


        [ComImport, Guid("0000000A-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface ILockBytes
        {
            void ReadAt([In, MarshalAs(UnmanagedType.U8)] long ulOffset, [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] byte[] pv, [In, MarshalAs(UnmanagedType.U4)] int cb, [Out, MarshalAs(UnmanagedType.LPArray)] int[] pcbRead);
            void WriteAt([In, MarshalAs(UnmanagedType.U8)] long ulOffset, IntPtr pv, [In, MarshalAs(UnmanagedType.U4)] int cb, [Out, MarshalAs(UnmanagedType.LPArray)] int[] pcbWritten);
            void Flush();
            void SetSize([In, MarshalAs(UnmanagedType.U8)] long cb);
            void LockRegion([In, MarshalAs(UnmanagedType.U8)] long libOffset, [In, MarshalAs(UnmanagedType.U8)] long cb, [In, MarshalAs(UnmanagedType.U4)] int dwLockType);
            void UnlockRegion([In, MarshalAs(UnmanagedType.U8)] long libOffset, [In, MarshalAs(UnmanagedType.U8)] long cb, [In, MarshalAs(UnmanagedType.U4)] int dwLockType);
            void Stat([Out]out System.Runtime.InteropServices.ComTypes.STATSTG pstatstg, [In, MarshalAs(UnmanagedType.U4)] int grfStatFlag);
        }

        [DllImport("OLE32.DLL", CharSet = CharSet.Unicode, PreserveSig = false)]
        public static extern IStorage StgCreateDocfileOnILockBytes(ILockBytes plkbyt, uint grfMode, uint reserved);
        
        [DllImport("ole32.dll", PreserveSig = false)]
        public static extern ILockBytes CreateILockBytesOnHGlobal(IntPtr hGlobal, bool fDeleteOnRelease);

        // Metafile mf is set to a state that is not valid inside this function.
        static public bool PutEnhMetafileOnClipboard(IntPtr hWnd, Metafile mf)
        {
            bool bResult = false;
            IntPtr hEMF, hEMF2;
            hEMF = mf.GetHenhmetafile(); // invalidates mf
            if (!hEMF.Equals(new IntPtr(0)))
            {
                hEMF2 = CopyEnhMetaFile(hEMF, new IntPtr(0));
                if (!hEMF2.Equals(new IntPtr(0)))
                {
                    if (OpenClipboard(hWnd))
                    {
                        if (EmptyClipboard())
                        {
                            IntPtr hRes = SetClipboardData(14 /*CF_ENHMETAFILE*/, hEMF2);
                            bResult = hRes.Equals(hEMF2);
                            CloseClipboard();
                        }
                    }
                }
                DeleteEnhMetaFile(hEMF);
            }
            return bResult;
        }

        static public MemoryStream[] GetOutlookFileContents(System.Windows.Forms.IDataObject DataObject, int Count)
        {
            //create a MemoryStream array to store the file contents
            MemoryStream[] fileContents = new MemoryStream[Count];

            //loop for the number of files acording to the file names
            for (int fileIndex = 0; fileIndex < Count; fileIndex++)
            {
                //get the data at the file index and store in array
                fileContents[fileIndex] = GetOutlookFileContent(DataObject, fileIndex);
            }
            return fileContents;
        }

        static private MemoryStream GetOutlookFileContent(System.Windows.Forms.IDataObject DataObject, int index)
        {

            System.Windows.Forms.IDataObject underlyingDataObject = DataObject;
            System.Runtime.InteropServices.ComTypes.IDataObject comUnderlyingDataObject = (System.Runtime.InteropServices.ComTypes.IDataObject)DataObject;
            System.Reflection.FieldInfo innerDataField = underlyingDataObject.GetType().GetField("innerData", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            System.Windows.Forms.IDataObject oleUnderlyingDataObject = (System.Windows.Forms.IDataObject)innerDataField.GetValue(underlyingDataObject);
            System.Reflection.MethodInfo getDataFromHGLOBLALMethod = oleUnderlyingDataObject.GetType().GetMethod("GetDataFromHGLOBLAL", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

            //create a FORMATETC struct to request the data with
            FORMATETC formatetc = new FORMATETC();
            formatetc.cfFormat = (short)System.Windows.Forms.DataFormats.GetFormat("FileContents").Id;
            formatetc.dwAspect = DVASPECT.DVASPECT_CONTENT;
            formatetc.lindex = index;
            formatetc.ptd = new IntPtr(0);
            formatetc.tymed = TYMED.TYMED_ISTREAM | TYMED.TYMED_ISTORAGE | TYMED.TYMED_HGLOBAL;

            //create STGMEDIUM to output request results into
            STGMEDIUM medium = new STGMEDIUM();

            //using the Com IDataObject interface get the data using the defined FORMATETC
            comUnderlyingDataObject.GetData(ref formatetc, out medium);

            //retrieve the data depending on the returned store type
            switch (medium.tymed)
            {
                case TYMED.TYMED_ISTORAGE:
                    //to handle a IStorage it needs to be written into a second unmanaged
                    //memory mapped storage and then the data can be read from memory into
                    //a managed byte and returned as a MemoryStream

                    IStorage iStorage = null;
                    IStorage iStorage2 = null;
                    ILockBytes iLockBytes = null;
                    System.Runtime.InteropServices.ComTypes.STATSTG iLockBytesStat;
                    try
                    {
                        //marshal the returned pointer to a IStorage object
                        iStorage = (IStorage)Marshal.GetObjectForIUnknown(medium.unionmember);
                        Marshal.Release(medium.unionmember);

                        //create a ILockBytes (unmanaged byte array) and then create a IStorage using the byte array as a backing store
                        iLockBytes = CreateILockBytesOnHGlobal(IntPtr.Zero, true);
                        iStorage2 = StgCreateDocfileOnILockBytes(iLockBytes, 0x00001012, 0);

                        //copy the returned IStorage into the new IStorage
                        iStorage.CopyTo(0, null, IntPtr.Zero, iStorage2);
                        iLockBytes.Flush();
                        iStorage2.Commit(0);

                        //get the STATSTG of the ILockBytes to determine how many bytes were written to it
                        iLockBytesStat = new System.Runtime.InteropServices.ComTypes.STATSTG();
                        iLockBytes.Stat(out iLockBytesStat, 1);
                        int iLockBytesSize = (int)iLockBytesStat.cbSize;

                        //read the data from the ILockBytes (unmanaged byte array) into a managed byte array
                        byte[] iLockBytesContent = new byte[iLockBytesSize];
                        iLockBytes.ReadAt(0, iLockBytesContent, iLockBytesContent.Length, null);

                        //wrapped the managed byte array into a memory stream and return it
                        return new MemoryStream(iLockBytesContent);
                    }
                    finally
                    {
                        //release all unmanaged objects
                        Marshal.ReleaseComObject(iStorage2);
                        Marshal.ReleaseComObject(iLockBytes);
                        Marshal.ReleaseComObject(iStorage);
                    }

                case TYMED.TYMED_ISTREAM:
                    //to handle a IStream it needs to be read into a managed byte and
                    //returned as a MemoryStream

                    IStream iStream = null;
                    System.Runtime.InteropServices.ComTypes.STATSTG iStreamStat;
                    try
                    {
                        //marshal the returned pointer to a IStream object
                        iStream = (IStream)Marshal.GetObjectForIUnknown(medium.unionmember);
                        Marshal.Release(medium.unionmember);

                        //get the STATSTG of the IStream to determine how many bytes are in it
                        iStreamStat = new System.Runtime.InteropServices.ComTypes.STATSTG();
                        iStream.Stat(out iStreamStat, 0);
                        int iStreamSize = (int)iStreamStat.cbSize;

                        //read the data from the IStream into a managed byte array
                        byte[] iStreamContent = new byte[iStreamSize];
                        iStream.Read(iStreamContent, iStreamContent.Length, IntPtr.Zero);

                        //wrapped the managed byte array into a memory stream and return it
                        return new MemoryStream(iStreamContent);
                    }
                    finally
                    {
                        //release all unmanaged objects
                        Marshal.ReleaseComObject(iStream);
                    }

                case TYMED.TYMED_HGLOBAL:
                    //to handle a HGlobal the exisitng "GetDataFromHGLOBLAL" method is invoked via
                    //reflection

                    return (MemoryStream)getDataFromHGLOBLALMethod.Invoke(oleUnderlyingDataObject, new object[] { System.Windows.Forms.DataFormats.GetFormat((short)formatetc.cfFormat).Name, medium.unionmember });
            }

            return null;
        }
    }

    public class ExtentionIcons
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct SHFILEINFO
        {
            public IntPtr hIcon;
            public IntPtr iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        };

        private class Win32
        {
            public const uint SHGFI_ICON = 0x100;
            public const uint SHGFI_LARGEICON = 0x0;
            public const uint SHGFI_SMALLICON = 0x1;
            [DllImport("shell32.dll")]
            public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);
        }

        public static Bitmap GetImageBitmapImage(string filename)
        {
            try
            {
                SHFILEINFO shinfo = new SHFILEINFO();
                IntPtr ptr = Win32.SHGetFileInfo(filename, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), Win32.SHGFI_ICON | Win32.SHGFI_SMALLICON);
                Icon icon = Icon.FromHandle(shinfo.hIcon);
                MemoryStream ms = new MemoryStream();
                Bitmap bi = icon.ToBitmap();
                return bi;
            }
            catch
            {
                return null;
            }
        }

 
    }
}

