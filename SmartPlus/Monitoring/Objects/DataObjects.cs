﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OracleClient;
using RDF.Objects;

namespace SmartPlus
{
    #region SumParameters
    public struct SumParamItem
    {
        public DateTime Date;
        public double Liq;
        public double LiqV;
        public double Oil;
        public double OilV;
        public double Watering;
        public double WateringV;
        public double Inj;
        public double InjGas;
        public double Scoop;
        public double NatGas;
        public double GasCondensate;
        public double WorkTimeProd;
        public double WorkTimeInj;
        public double WorkTimeInjGas;
        public int Fprod;
        public int Finj;
        public int Fscoop;
        public int LeadInAll;
        public int LeadInProd;
        public float Pressure;
        public int PressureCounter;
        public float ZabPressure;
        public int ZabPressureCounter;
        public double InjVb;
        public double LiqVb;
        public double AccumInjVb;
        public double AccumLiqVb;

        public void AddSumParam(SumParamItem AddItem)
        {
            this.Liq += AddItem.Liq;
            this.LiqV += AddItem.LiqV;
            this.Oil += AddItem.Oil;
            this.OilV += AddItem.OilV;
            this.Watering = 0;
            this.WateringV = 0;
            if (this.Liq > 0) Watering = (Liq - Oil) * 100 / Liq;
            if (this.LiqV > 0) WateringV = (LiqV - OilV) * 100 / LiqV;

            this.Inj += AddItem.Inj;
            if (AddItem.InjGas > -1)
            {
                if (this.InjGas < 0) this.InjGas = 0;
                this.InjGas += AddItem.InjGas;
            }
            if (AddItem.NatGas > -1)
            {
                if (this.NatGas < 0) this.NatGas = 0;
                this.NatGas += AddItem.NatGas;
            }
            if (AddItem.GasCondensate > -1)
            {
                if (this.GasCondensate == -1) this.GasCondensate = 0;
                this.GasCondensate += AddItem.GasCondensate;
            }
            this.Scoop += AddItem.Scoop;
            this.WorkTimeProd += AddItem.WorkTimeProd;
            this.WorkTimeInj += AddItem.WorkTimeInj;
            this.WorkTimeInjGas += AddItem.WorkTimeInjGas;
            this.Fprod += AddItem.Fprod;
            this.Finj += AddItem.Finj;
            this.Fscoop += AddItem.Fscoop;
            this.LeadInAll +=AddItem.LeadInAll;
            this.LeadInProd += AddItem.LeadInProd;
            LiqVb += AddItem.LiqVb;
            InjVb += AddItem.InjVb;
            AccumInjVb += AddItem.AccumInjVb;
            AccumLiqVb += AddItem.AccumLiqVb;
        }
        public void SubSumParam(SumParamItem SubItem)
        {
            
            this.Liq -= SubItem.Liq;
            LiqV -= SubItem.LiqV;
            this.Oil -= SubItem.Oil;
            OilV -= SubItem.OilV;
            this.Watering = 0;
            this.WateringV = 0;
            if (this.Liq > 0) this.Watering = (this.Liq - this.Oil) * 100 / this.Liq;
            if (this.LiqV > 0) this.WateringV = (this.LiqV - this.OilV) * 100 / this.LiqV;
            this.Inj -= SubItem.Inj;
            if (SubItem.InjGas > 0)
            {
                this.InjGas -= SubItem.InjGas;
                if (this.InjGas == 0) this.InjGas = -1;
            }
            if (SubItem.NatGas > 0)
            {
                this.NatGas -= SubItem.NatGas;
                if (this.NatGas == 0) this.NatGas = -1;
            }
            if (SubItem.GasCondensate > 0)
            {
                this.GasCondensate -= SubItem.GasCondensate;
                if (this.GasCondensate == 0) this.GasCondensate = -1;
            }
            this.Scoop -= SubItem.Scoop;
            this.WorkTimeProd -= SubItem.WorkTimeProd;
            this.WorkTimeInj -= SubItem.WorkTimeInj;
            this.WorkTimeInjGas -= SubItem.WorkTimeInjGas;
            this.Fprod -= SubItem.Fprod;
            this.Finj -= SubItem.Finj;
            this.Fscoop -= SubItem.Fscoop;
            this.LeadInAll -= SubItem.LeadInAll;
            this.LeadInProd -= SubItem.LeadInProd;
            LiqVb -= SubItem.LiqVb;
            InjVb -= SubItem.InjVb;
            AccumInjVb -= SubItem.AccumInjVb;
            AccumLiqVb -= SubItem.AccumLiqVb;

            if (this.Liq < 0) { this.Liq = 0; this.Watering = 0; }
            if (this.LiqV < 0) { this.LiqV = 0; this.WateringV = 0; }
            if (this.Oil < 0) 
            { 
                this.Oil = 0; 
                if (this.Liq > 0) this.Watering = (this.Liq - this.Oil) * 100 / this.Liq;
                if (this.LiqV > 0) this.WateringV = (this.LiqV - this.OilV) * 100 / this.LiqV; 
            }
            if (this.Inj < 0) this.Inj = 0;
            if (this.InjGas < 0) this.InjGas = 0;
            if (this.Scoop < 0) this.Scoop = 0;
            if (this.WorkTimeProd < 0) this.WorkTimeProd = 0;
            if (this.WorkTimeInj < 0) this.WorkTimeInj = 0;
            if (this.Fprod < 0) this.Fprod = 0;
            if (this.Finj < 0) this.Finj = 0;
            if (this.Fscoop < 0) this.Fscoop = 0;
            if (this.LeadInAll < 0) this.LeadInAll = 0;
            if (this.LeadInProd < 0) this.LeadInProd = 0;
            if (this.LiqVb < 0) this.LiqVb = 0;
            if (this.InjVb < 0) this.InjVb = 0;
            if (AccumInjVb < 0) AccumInjVb = 0;
            if (AccumLiqVb < 0) AccumLiqVb = 0;
        }
    }
    
    public sealed class SumObjParameters
    {
        public int OilObjCode;
        public int ShiftMonthIndex;
        public int ShiftYearIndex;
        SumParamItem[] _monthlyItems = null;
        SumParamItem[] _yearlyItems = null;
        public SumObjParameters(int OilObjCode, SumParamItem[] MonthlyItems, SumParamItem[] YearlyItems)
        {
            this.OilObjCode = OilObjCode;
            this.ShiftMonthIndex = -1;
            this.ShiftYearIndex = -1;
            _monthlyItems = MonthlyItems;
            _yearlyItems = YearlyItems;
        }
        public SumObjParameters(int OilObjCode, SumParamItem[] MonthlyItems, SumParamItem[] YearlyItems, int ShiftMonth, int ShiftYear) 
            : this( OilObjCode, MonthlyItems, YearlyItems)
        {
            this.ShiftMonthIndex = ShiftMonth;
            this.ShiftYearIndex = ShiftYear;
        }
        public SumParamItem[] MonthlyItems
        {
            get { return _monthlyItems; }
            set { _monthlyItems = value; }
        }
        public SumParamItem[] YearlyItems
        {
            get { return _yearlyItems; }
            set { _yearlyItems = value; }
        }
    }

    public sealed class SumParameters
    {
        public static int Version = 3;
        System.Collections.ArrayList items;
        public SumParameters(int Length)
        {
            items = new System.Collections.ArrayList(Length);
        }
        public void Clear()
        {
            items.Clear();
        }
        public int Count { get { return items.Count; } }
        public void Add(int OilObjCode, SumParamItem[] MonthlyItems, SumParamItem[] YearlyItems)
        {
            this.Add(OilObjCode, MonthlyItems, YearlyItems, -1, -1);
        }
        public void Add(int OilObjCode, SumParamItem[] MonthlyItems, SumParamItem[] YearlyItems, int ShiftMonth, int ShiftYear)
        {
            SumObjParameters sumObj = new SumObjParameters(OilObjCode, MonthlyItems, YearlyItems, ShiftMonth, ShiftYear);
            items.Add(sumObj);
        }

        public SumObjParameters this[int index]
        {
            get
            {
                if (index < items.Count) return (SumObjParameters)items[index];
                else return null;
            }
            set
            {
                if ((index > -1) && (index < items.Count))
                {
                    items[index] = value;
                }
            }
        }
        public SumObjParameters GetItemByObjCode(int OilObjCode)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (((SumObjParameters)items[i]).OilObjCode == OilObjCode)
                {
                    return (SumObjParameters)items[i];
                }
            }
            return null;
        }
        public int GetIndexByObjCode(int OilObjCode)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (((SumObjParameters)items[i]).OilObjCode == OilObjCode)
                {
                    return i;
                }
            }
            return -1;
        }

        public void AddDaySumParams(int OilObjCode, SumParamItem[] AddParams)
        {
            if ((AddParams != null) && (AddParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                int indObj = -1, index1 = -1, index2 = -1;
                if (sumObj == null)
                {
                    sumObj = new SumObjParameters(OilObjCode, null, null);
                    indObj = Count;
                    items.Add(sumObj);
                }
                SumParams = sumObj.MonthlyItems;

                if ((SumParams == null) || (SumParams.Length == 0) ||
                    (SumParams.Length < AddParams.Length) ||
                    (AddParams[0].Date < SumParams[0].Date) ||
                    (AddParams[AddParams.Length - 1].Date > SumParams[SumParams.Length - 1].Date))
                {
                    DateTime dtMin = DateTime.MaxValue, dtMax = DateTime.MinValue, dt;
                    if (SumParams != null)
                    {
                        if (dtMin > SumParams[0].Date) dtMin = SumParams[0].Date;
                        if (dtMax < SumParams[SumParams.Length - 1].Date) dtMax = SumParams[SumParams.Length - 1].Date;
                    }
                    if (dtMin > AddParams[0].Date) dtMin = AddParams[0].Date;
                    if (dtMax < AddParams[AddParams.Length - 1].Date) dtMax = AddParams[AddParams.Length - 1].Date;

                    int count = (dtMax - dtMin).Days + 1;

                    if ((SumParams != null) && (SumParams.Length > 0))
                    {
                        index1 = (SumParams[0].Date - dtMin).Days;
                    }

                    SumParamItem[] newSumParam = new SumParamItem[count];
                    dt = dtMin;
                    for (int i = 0; i < count; i++)
                    {
                        if ((index1 != -1) && (i >= index1) && (i - index1 < SumParams.Length))
                        {
                            newSumParam[i].AddSumParam(SumParams[i - index1]);
                        }
                        newSumParam[i].Date = dt;
                        dt = dt.AddDays(1);
                    }
                    SumParams = newSumParam;
                    sumObj.MonthlyItems = SumParams;
                }
                index2 = (AddParams[0].Date - SumParams[0].Date).Days;

                for (int i = 0; i < SumParams.Length; i++)
                {
                    if ((index2 != -1) && (i >= index2) && (i - index2 < AddParams.Length))
                    {
                        SumParams[i].AddSumParam(AddParams[i - index2]);
                    }
                }
            }
        }
        public void AddMonthSumParams(int OilObjCode, SumParamItem[] AddParams)
        {
            if ((AddParams != null) && (AddParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                int indObj = -1, index1 = -1, index2 = -1;
                if (sumObj == null)
                {
                    sumObj = new SumObjParameters(OilObjCode, null, null);
                    indObj = Count;
                    items.Add(sumObj);
                }
                SumParams = sumObj.MonthlyItems;
                
                if ((SumParams == null) || (SumParams.Length == 0) ||
                    (SumParams.Length < AddParams.Length) ||
                    (AddParams[0].Date < SumParams[0].Date) ||
                    (AddParams[AddParams.Length - 1].Date > SumParams[SumParams.Length - 1].Date))
                {
                    DateTime dtMin = DateTime.MaxValue, dtMax = DateTime.MinValue, dt;
                    if (SumParams != null)
                    {
                        if (dtMin > SumParams[0].Date) dtMin = SumParams[0].Date;
                        if (dtMax < SumParams[SumParams.Length - 1].Date) dtMax = SumParams[SumParams.Length - 1].Date;
                    }
                    if (dtMin > AddParams[0].Date) dtMin = AddParams[0].Date;
                    if (dtMax < AddParams[AddParams.Length - 1].Date) dtMax = AddParams[AddParams.Length - 1].Date;

                    int count = (dtMax.Year - dtMin.Year) * 12 + dtMax.Month - dtMin.Month + 1;
                    
                    if ((SumParams != null) && (SumParams.Length > 0))
                    {
                        index1 = (SumParams[0].Date.Year - dtMin.Year) * 12 + SumParams[0].Date.Month - dtMin.Month;
                    }
                    
                    SumParamItem[] newSumParam = new SumParamItem[count];
                    dt = dtMin;
                    for (int i = 0; i < count; i++)
                    {
                        if ((index1 != -1) && (i >= index1) && (i - index1 < SumParams.Length))
                        {
                            newSumParam[i].AddSumParam(SumParams[i - index1]);
                        }
                        newSumParam[i].Date = dt;
                        newSumParam[i].InjGas = -1;
                        newSumParam[i].NatGas = -1;
                        newSumParam[i].GasCondensate = -1;
                        dt = dt.AddMonths(1);
                    }
                    SumParams = newSumParam;
                    sumObj.MonthlyItems = SumParams;
                }
                index2 = (AddParams[0].Date.Year - SumParams[0].Date.Year) * 12 + AddParams[0].Date.Month - SumParams[0].Date.Month;

                for (int i = 0; i < SumParams.Length; i++)
                {
                    if ((index2 != -1) && (i >= index2) && (i - index2 < AddParams.Length))
                    {
                        SumParams[i].AddSumParam(AddParams[i - index2]);
                    }
                }
            }
        }
        public void AddYearSumParams(int OilObjCode, SumParamItem[] AddParams)
        {
            if ((AddParams != null) && (AddParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                int indObj = -1, index1 = -1, index2 = -1;
                if (sumObj == null)
                {
                    sumObj = new SumObjParameters(OilObjCode, null, null);
                    indObj = Count;
                    items.Add(sumObj);
                }
                SumParams = sumObj.YearlyItems;

                if ((SumParams == null) || (SumParams.Length == 0) ||
                    (SumParams.Length < AddParams.Length) ||
                    (AddParams[0].Date < SumParams[0].Date) ||
                    (AddParams[AddParams.Length - 1].Date > SumParams[SumParams.Length - 1].Date))
                {
                    DateTime dtMin = DateTime.MaxValue, dtMax = DateTime.MinValue, dt;
                    if (SumParams != null)
                    {
                        if (dtMin > SumParams[0].Date) dtMin = SumParams[0].Date;
                        if (dtMax < SumParams[SumParams.Length - 1].Date) dtMax = SumParams[SumParams.Length - 1].Date;
                    }
                    if (dtMin > AddParams[0].Date) dtMin = AddParams[0].Date;
                    if (dtMax < AddParams[AddParams.Length - 1].Date) dtMax = AddParams[AddParams.Length - 1].Date;

                    int count = dtMax.Year - dtMin.Year + 1;

                    if ((SumParams != null) && (SumParams.Length > 0))
                    {
                        index1 = SumParams[0].Date.Year - dtMin.Year;
                    }
                    dt = dtMin;
                    SumParamItem[] newSumParam = new SumParamItem[count];
                    for (int i = 0; i < count; i++)
                    {
                        if ((index1 != -1) && (i >= index1) && (i - index1 < SumParams.Length))
                        {
                            newSumParam[i].AddSumParam(SumParams[i - index1]);
                        }
                        newSumParam[i].Date = dt;
                        newSumParam[i].InjGas = -1;
                        newSumParam[i].NatGas = -1;
                        newSumParam[i].GasCondensate = -1;
                        dt = dt.AddYears(1);
                    }
                    SumParams = newSumParam;
                    sumObj.YearlyItems = SumParams;
                }
                index2 = AddParams[0].Date.Year - SumParams[0].Date.Year;

                for (int i = 0; i < SumParams.Length; i++)
                {
                    if ((index2 != -1) && (i >= index2) && (i - index2 < AddParams.Length))
                    {
                        SumParams[i].AddSumParam(AddParams[i - index2]);
                    }
                }
            }
        }
        
        public void SubDaySumParams(int OilObjCode, SumParamItem[] SubParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((SubParams != null) && (SubParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                if (sumObj != null) SumParams = sumObj.MonthlyItems;
                if ((SumParams != null) && (SumParams.Length > 0))
                {
                    int index1 = -1, index2 = -1;
                    int TrimCount = (TrimMaxDate - TrimMinDate).Days + 1;
                    DateTime dt;
                    if ((TrimCount > -1) && (TrimCount < SumParams.Length))
                    {
                        if ((SumParams != null) && (SumParams.Length > 0))
                        {
                            index1 = (SumParams[0].Date - TrimMinDate).Days;
                        }
                        dt = TrimMinDate;
                        SumParamItem[] newSumParam = new SumParamItem[TrimCount];
                        for (int i = 0; i < TrimCount; i++)
                        {
                            if ((index1 != -1) && (i >= index1) && (i - index1 < SumParams.Length))
                            {
                                newSumParam[i].AddSumParam(SumParams[i - index1]);
                            }
                            newSumParam[i].Date = dt;
                            dt = dt.AddDays(1);
                        }
                        SumParams = newSumParam;
                        sumObj.MonthlyItems = newSumParam;
                    }
                    index2 = (SubParams[0].Date - SumParams[0].Date).Days;

                    for (int i = 0; i < SumParams.Length; i++)
                    {
                        if ((index2 != -1) && (i >= index2) && (i - index2 < SubParams.Length))
                        {
                            SumParams[i].SubSumParam(SumParams[i - index2]);
                        }
                    }
                }
            }
        }
        public void SubMonthSumParams(int OilObjCode, SumParamItem[] SubParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((SubParams != null) && (SubParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                if (sumObj != null) SumParams = sumObj.MonthlyItems;
                if ((SumParams != null) && (SumParams.Length > 0))
                {
                    int index1 = -1, index2 = -1;
                    int TrimCount = (TrimMaxDate.Year - TrimMinDate.Year) * 12 + TrimMaxDate.Month - TrimMinDate.Month + 1;
                    DateTime dt;
                    if ((TrimCount > -1) && (TrimCount < SumParams.Length))
                    {
                        if ((SumParams != null) && (SumParams.Length > 0))
                        {
                            index1 = (SumParams[0].Date.Year - TrimMinDate.Year) * 12 + SumParams[0].Date.Month - TrimMinDate.Month;
                        }
                        dt = TrimMinDate;
                        SumParamItem[] newSumParam = new SumParamItem[TrimCount];
                        for (int i = 0; i < TrimCount; i++)
                        {
                            if ((SumParams != null) && (i - index1 > -1) && (i >= index1) && (i - index1 < SumParams.Length))
                            {
                                newSumParam[i].AddSumParam(SumParams[i - index1]);
                            }
                            newSumParam[i].Date = dt;
                            dt = dt.AddMonths(1);
                        }
                        SumParams = newSumParam;
                        sumObj.MonthlyItems = SumParams;
                    }

                    index2 = (SubParams[0].Date.Year - SumParams[0].Date.Year) * 12 + SubParams[0].Date.Month - SumParams[0].Date.Month;
                    for (int i = 0; i < SumParams.Length; i++)
                    {
                        if ((i - index2 > -1) && (i >= index2) && (i - index2 < SubParams.Length))
                        {
                            SumParams[i].SubSumParam(SubParams[i - index2]);
                        }
                    }
                }
            }
        }
        public void SubYearSumParams(int OilObjCode, SumParamItem[] SubParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((SubParams != null) && (SubParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                if (sumObj != null) SumParams = sumObj.YearlyItems;
                if ((SumParams != null) && (SumParams.Length > 0))
                {
                    int index1 = -1, index2 = -1;
                    int TrimCount = TrimMaxDate.Year - TrimMinDate.Year + 1;
                    DateTime dt;
                    if ((TrimCount > -1) && (TrimCount < SumParams.Length))
                    {
                        if ((SumParams != null) && (SumParams.Length > 0))
                        {
                            index1 = SumParams[0].Date.Year - TrimMinDate.Year;
                        }
                        dt = new DateTime(TrimMinDate.Year, 1, 1);
                        SumParamItem[] newSumParam = new SumParamItem[TrimCount];
                        for (int i = 0; i < TrimCount; i++)
                        {
                            if ((i - index1 > -1) && (i >= index1) && (i - index1 < SumParams.Length))
                            {
                                newSumParam[i].AddSumParam(SumParams[i - index1]);
                            }
                            newSumParam[i].Date = dt;
                            dt = dt.AddYears(1);
                        }
                        SumParams = newSumParam;
                    }
                    index2 = SubParams[0].Date.Year - SumParams[0].Date.Year;

                    for (int i = 0; i < SumParams.Length; i++)
                    {
                        if ((i - index2 > -1) && (i >= index2) && (i - index2 < SubParams.Length))
                        {
                            SumParams[i].SubSumParam(SubParams[i - index2]);
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region Table8_1

    public sealed class Table81Params
    {
        public static int Version = 1;
        
        public string ProjectDocName;
        public int NewPtdYear;
        int count;
        SumObjParameters Table;

        public Table81Params()
        {
            ProjectDocName = null;
            Table = null;
            count = -1;
            NewPtdYear = 0;
        }

        public void SetTable(SumParamItem[] YearlyItems)
        {
            Table = new SumObjParameters(-1, null, YearlyItems);
            count = Table.YearlyItems.Length;
        }

        public SumParamItem[] Items
        {
            get
            {
                if (Table != null) return Table.YearlyItems;
                else return null;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }
        }
    }
    #endregion
}
