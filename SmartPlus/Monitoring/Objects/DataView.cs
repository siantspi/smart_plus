﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.ComponentModel;
using System.Collections;
using RDF;
using RDF.Objects;
using System.IO;
using Selecting;
using System.Collections.Generic;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    #region Таблица МЭР
    sealed class DataGridMer : DataGridView 
    {
        Well srcWell;
        Project _proj;
        MainForm mainForm;
        public DataTable table;
        public bool DataLoaded;
        bool DataLoading;
        Font _font, _font_bold;
        MerItem merItem;
        DataGridViewCellStyle HeaderCellStyle, TableCellStyleFloat, TableCellStyleQLiq, TableCellStyleQOil, TableCellStyleAll;
        DataGridViewCellStyle TableCellStyleDate;
        DataGridViewCell lastSelectCell;

        string[] tableHeaderText = { "Скважина", "Дата", "Метод", "Хар-р", "Состояние", "Раб.вр, ч", 
                                     "Вр.нак, ч", "Жидкость, т", "Нефть, т", "Жидкость, м3", "Нефть, м3", 
                                     "Попутный газ, м3", "Закачка, м3","Закачка газа, м3", "Природный газ, м3", "Доб.конденсата, т", "Простой, ч", 
                                     "Qж, т/сут", "Qн, т/сут", "W, м3/сут","Wгаза, м3/сут", "%", "Пласт"};
        int[] columnWidth = { 50, 62, 30, 30, 30, 26, 26, 55, 55, 55, 55, 55, 55, 55, 55, 55, 26, 42, 42, 55, 55, 22, 80 };

        public DataGridMer(MainForm MainForm) 
        {
            InitContextMenu();
            HeaderCellStyle = new DataGridViewCellStyle();
            TableCellStyleAll = new DataGridViewCellStyle();
            TableCellStyleFloat = new DataGridViewCellStyle();
            TableCellStyleQLiq = new DataGridViewCellStyle();
            TableCellStyleQOil = new DataGridViewCellStyle();
            TableCellStyleDate = new DataGridViewCellStyle();

            DataLoaded = false;
            DataLoading = false;
            _font = new Font("Calibri", 8.25f);
            _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);

            this.mainForm = MainForm;
            this.Parent = mainForm.pMerView;
            this.Dock = DockStyle.Fill;

            table = new DataTable("MER");
            
            table.Columns.Add("SKVNAME", Type.GetType("System.String"));    // 0
            table.Columns.Add("DATE", Type.GetType("System.DateTime"));     // 1
            table.Columns.Add("METHOD", Type.GetType("System.String"));     // 2
            table.Columns.Add("CHARWORK", Type.GetType("System.String"));   // 3
            table.Columns.Add("STATE", Type.GetType("System.String"));      // 4
            table.Columns.Add("WORKTIME", Type.GetType("System.Int32"));    // 5
            table.Columns.Add("COLLTIME", Type.GetType("System.Int32"));    // 6
            table.Columns.Add("OIL", Type.GetType("System.Double"));        // 7
            table.Columns.Add("LIQUID", Type.GetType("System.Double"));     // 8
            table.Columns.Add("OIL_V", Type.GetType("System.Double"));      // 9
            table.Columns.Add("LIQUID_V", Type.GetType("System.Double"));   // 10
            table.Columns.Add("GAS", Type.GetType("System.Double"));        // 11
            table.Columns.Add("INJECTION", Type.GetType("System.Double"));  // 12
            table.Columns.Add("INJECTION_GAS", Type.GetType("System.Double"));  // 13
            table.Columns.Add("NAT_GAS", Type.GetType("System.Double"));    // 14
            table.Columns.Add("CONDENSATE", Type.GetType("System.Double")); // 15
            table.Columns.Add("STAYTIME", Type.GetType("System.Int32"));    // 16
            table.Columns.Add("QLIQ", Type.GetType("System.Double"));       // 17
            table.Columns.Add("QOIL", Type.GetType("System.Double"));       // 18
            table.Columns.Add("W", Type.GetType("System.Double"));          // 19
            table.Columns.Add("W_GAS", Type.GetType("System.Double"));      // 20
            table.Columns.Add("WAT", Type.GetType("System.Double"));        // 21
            table.Columns.Add("PLAST", Type.GetType("System.String"));      // 22

            this.DataSource = table;
            TableCellStyleDate.Format = "MM.yyyy";
            TableCellStyleDate.Font = _font;

            TableCellStyleFloat.Format = "0.##";
            TableCellStyleFloat.Font = _font;

            TableCellStyleAll.Font = _font;

            TableCellStyleQLiq.Format = "0.##";
            TableCellStyleQLiq.Font = _font_bold;
            TableCellStyleQLiq.ForeColor = Color.DarkGreen;

            TableCellStyleQOil.Format = "0.##";
            TableCellStyleQOil.Font = _font_bold;
            TableCellStyleQOil.ForeColor = Color.Peru;

            SetHeaderStyle();
            // Настройка таблицы МЭР
            this.RowTemplate.Height = 13;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = true;
            this.DoubleBuffered = true;
            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            this.BackgroundColor = SystemColors.ControlDark;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.Resize += new EventHandler(DataGridMer_Resize);
            this.Paint += new PaintEventHandler(DataGridMer_Paint); // перерисовка окна
            this.ColumnWidthChanged += new DataGridViewColumnEventHandler(DataGridMer_ColumnWidthChanged);
            this.MouseDown += new MouseEventHandler(DataGridMer_MouseDown);
            this.KeyDown += new KeyEventHandler(DataGridMer_KeyDown);
        }

        #region Context Menu
        ContextMenuStrip cMenu;
        void InitContextMenu()
        {
            cMenu = new ContextMenuStrip();
            ToolStripMenuItem cmCopyCells = (ToolStripMenuItem)cMenu.Items.Add("Копировать в буфер обмена", Properties.Resources.Copy16);
            cmCopyCells.Click += new EventHandler(cmCopyCells_Click);
        }
        void cmCopyCells_Click(object sender, EventArgs e)
        {
            copySelectedRowsToClipboard((DataGridView)this);
        }
        #endregion

        void DataGridMer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
            {
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    copySelectedRowsToClipboard((DataGridView)sender);
                }
            }            
        }

        void DataGridMer_MouseDown(object sender, MouseEventArgs e)
        {
            // mainForm.StatUsage.AddMessage(CollectorStatId.MER_MOUSE_DOWN);
            lastSelectCell = null;
            ContextMenuStrip = null;
            HitTestInfo hit = HitTest(e.X, e.Y);
            if ((e.Button == MouseButtons.Right) && (hit.ColumnIndex != -1) && (hit.RowIndex != -1))
            {
                lastSelectCell = Rows[hit.RowIndex].Cells[hit.ColumnIndex];
                if (lastSelectCell.Selected) ContextMenuStrip = cMenu;
            }
        }

        void DataGridMer_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            //mainForm.reLog.AppendText(e.Column.Width.ToString() + Environment.NewLine);
        }

        public void SetProject(Project aProject){
            _proj = aProject;
        }
        public void ClearProject() { this._proj = null; this.ClearTable(); }
        private void copySelectedRowsToClipboard(DataGridView dgv)
        {
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            Clipboard.Clear();
            dgv.Columns[1].DefaultCellStyle.Format = "dd.MM.yyyy";

            if (dgv.GetClipboardContent() != null)
            {
                Clipboard.SetDataObject(dgv.GetClipboardContent());
                Clipboard.GetData(DataFormats.Text);
                IDataObject dt = Clipboard.GetDataObject();
                if (dt.GetDataPresent(typeof(string)))
                {
                    string tb = (string)(dt.GetData(typeof(string)));
                    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                    byte[] dataStr = encoding.GetBytes(tb);
                    Clipboard.SetDataObject(encoding.GetString(dataStr));
                }
            }
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            dgv.Columns[1].DefaultCellStyle.Format = "MM.yyyy";
        }

        public void ClearTable()
        {
            srcWell = null;
            DataLoading = true;
            table.Clear();
            DataLoaded = false;
            DataLoading = false;
            this.BackgroundColor = SystemColors.ControlDark;
        }
        void SetHeaderStyle()
        {
            bool LiquidInCube = false;
            if (mainForm.AppSettings != null) LiquidInCube = mainForm.AppSettings.ShowDataLiquidInCube;

            if (!DataLoading)
            {
                for (int i = 0; i < this.Columns.Count; i++)
                {

                    if (i == 1) this.Columns[i].DefaultCellStyle = TableCellStyleDate;
                    else if (i == 17) this.Columns[i].DefaultCellStyle = TableCellStyleQLiq;
                    else if (i == 18) this.Columns[i].DefaultCellStyle = TableCellStyleQOil;
                    else if ((i > 6) && (i != 16) && (i < 22)) this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    else this.Columns[i].DefaultCellStyle = TableCellStyleAll;
                    this.Columns[i].Width = columnWidth[i];
                    this.Columns[i].HeaderText = tableHeaderText[i];
                    if (i == 17 && LiquidInCube) this.Columns[i].HeaderText = "Qж, м3/сут";
                }
                HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
                HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
                HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
                HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
                this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;
            }
        }
        void DataGridMer_Resize(object sender, EventArgs e)
        {
           if((!DataLoaded)&&(!DataLoading)) this.Invalidate();
           SetHeaderStyle();
        }
        void DataGridMer_Paint(object sender, PaintEventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
            {
                System.Drawing.Graphics gr = e.Graphics;
                gr.DrawString("Нет данных для отображения", _font, Brushes.Black, this.Left + (int)(this.Width / 2), this.Top + (int)(this.Height / 2));
            }
        }
        public void ShowMer(int oilFieldIndex, int wellIndex) 
        {
            if ((oilFieldIndex >= 0) && (this.Parent.Visible))
            {
                bool LiquidInCube = false;
                if (mainForm.AppSettings != null) LiquidInCube = mainForm.AppSettings.ShowDataLiquidInCube;

                OilField of = _proj.OilFields[oilFieldIndex];
                var dictStratum = (StratumDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                var dictCharwork = (DataDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.CHARWORK);
                var dictMethod = (DataDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.METHOD);
                var dictState = (DataDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STATE);

                Well w = of.Wells[wellIndex];
                if (w == null) return;
                DataRow row;
                double sumOil, sumLiq, sumGas, sumInj, sumInjGas, sumLiqV, sumOilV;
                string plasts;
                ArrayList plast_ids = new ArrayList();
                int i, j, hours;
                MerWorkTimeCollection ProdTimeList = new MerWorkTimeCollection();
                MerWorkTimeCollection InjTimeList = new MerWorkTimeCollection();
                DataLoading = true;
                this.DataSource = null;
                table.Clear();
                DataLoaded = false;
                int wt;
                MerWorkTimeItem wtItem;
                string charWork;
                sumOil = 0; sumLiq = 0; sumGas = 0; sumInj = 0; sumInjGas = 0; sumLiqV = 0; sumOilV = 0;

                plasts = ""; plast_ids.Clear();
                if (!w.MerLoaded) (_proj.OilFields[w.OilFieldIndex]).LoadMerFromCache(w.Index);
                if ((w.MerLoaded) && (w.mer.Count > 0))
                {
                    MerComp merComp = new MerComp(w.mer);
                    for (i = 0; i < merComp.Count; i++)
                    {
                        row = table.NewRow();
                        plasts = string.Empty;
                        for (j = 0; j < merComp[i].PlastItems.Count; j++)
                        {
                            if (j > 0) plasts += ",";
                            if (dictStratum != null)
                            {
                                plasts += dictStratum.GetShortNameByCode(merComp[i].PlastItems[j].PlastCode);
                            }
                            else
                                plasts += merComp[i].PlastItems[j].PlastCode.ToString();
                        }
                        row[0] = w.UpperCaseName;
                        row[1] = merComp[i].Date;
                        row[2] = dictMethod.GetShortNameByCode(merComp[i].MethodId);
                        charWork = string.Empty;
                        for (j = 0; j < merComp[i].CharWorkIds.Count; j++)
                        {
                            if(j > 0) charWork += "\\";
                            charWork = dictCharwork.GetShortNameByCode(merComp[i].CharWorkIds[j]);
                        }
                        row[3]= charWork;
                        row[4] = dictState.GetShortNameByCode(merComp[i].StateId);

                        
                        wtItem = merComp[i].TimeItems.GetAllTime();
                        hours = (merComp[i].Date.AddMonths(1) - merComp[i].Date).Days * 24;
                        wt = (wtItem.WorkTime + wtItem.CollTime > hours) ? hours : wtItem.WorkTime + wtItem.CollTime;

                        sumLiq = merComp[i].SumLiquid;
                        sumLiqV = merComp[i].SumLiquidV;
                        sumOil = merComp[i].SumOil;
                        sumOilV = merComp[i].SumOilV;
                        sumInj = merComp[i].SumInjection;
                        sumInjGas = merComp[i].SumInjectionGas;

                        row[5] = wt;
                        row[6] = wtItem.CollTime;
                        row[7] = sumLiq;
                        row[8] = sumOil;
                        row[9] = merComp[i].SumLiquidV;
                        row[10] = merComp[i].SumOilV;
                        row[11] = merComp[i].SumGas;
                        row[12] = sumInj;
                        row[13] = sumInjGas;
                        row[14] = merComp[i].SumNaturalGas;
                        row[15] = merComp[i].SumGasCondensate;
                        row[16] = wtItem.StayTime;
                        row[17] = 0;
                        row[18] = 0;
                        row[19] = 0;
                        if (wt > 0)
                        {
                            row[17] = (LiquidInCube ? sumLiqV : sumLiq) * 24 / wt;
                            row[18] = sumOil * 24 / wt;
                            row[19] = sumInj * 24 / wt;
                            row[20] = sumInjGas * 24 / wt; 
                        }
                        if (LiquidInCube)
                        {
                            row[21] = (sumLiqV > 0) ? (sumLiqV - sumOilV) * 100 / sumLiqV : 0;
                        }
                        else
                        {
                            row[21] = (sumLiq > 0) ? (sumLiq - sumOil) * 100 / sumLiq : 0;
                        }
                        row[22] = plasts;
                        table.Rows.Add(row);
                    }
                    DataLoaded = merComp.Count > 0;
                }
                srcWell = w;
                DataLoading = false;
                this.DataSource = table;
                SetHeaderStyle();
                if ((this.Rows.Count > 0) && (this.ClientRectangle.Height > 15))
                {
                    this.FirstDisplayedScrollingRowIndex = this.Rows.Count - 1;
                }
            }
            else if ((oilFieldIndex >= 0) && (!this.Parent.Visible))
            {
                OilField of = _proj.OilFields[oilFieldIndex];
                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataLoading = false;
                DataLoaded = false;
                table.Clear();
            }
        }
        public void UpdateBySourceWell()
        {
            if ((srcWell != null) && (!DataLoaded))
            {
                ShowMer(srcWell.OilFieldIndex, srcWell.Index);
            }
        }
    }
    #endregion

    #region Таблица ГИС
    sealed class DataGridGIS : DataGridView
    {
        Project _proj;
        MainForm mainForm;
        int SelectedRowIndex = -1;
        public DataTable table;
        public bool DataLoaded;
        bool DataLoading;
        Font _font, _font_bold;
        SkvGisItem gisItem;
        ImageList imgList;
        Well srcWell;
        float SelectingDepth;
        Timer SelectUpdater;
        DataGridViewCell lastSelectCell;
        DataGridViewCellStyle HeaderCellStyle, TableCellStyleAll, TableCellStyleFloat;
        string[] tableHeaderText = { "Скв", "Пласт", "Пропл.", "H(md)", "L(md)", "H+L(md)", 
                                         "H(abs)", "L(abs)", "H+L(abs)",
                                         "Лит", "Колл.", "Нач.нас.", "Нас-ть", "Уд.сопр.", 
                                         "альфа ПС", "альфа ГК", "альфа НКТ", "Пор-ть", "Прониц-ть", 
                                         "Нефтенас-ть", "Газонас-ть", "Коэф.глин."};
        int[] columnWidth = { 50, 46, 46, 45, 30, 45, 45, 30, 45, 30, 
                                  30, 23, 30, 30, 33, 33, 33, 33, 33, 33, 
                                  33, 33 };

        public DataGridGIS(MainForm MainForm)
        {
            InitContextMenu();
            HeaderCellStyle = new DataGridViewCellStyle();
            TableCellStyleAll = new DataGridViewCellStyle();
            TableCellStyleFloat = new DataGridViewCellStyle();

            srcWell = null;
            DataLoaded = false;
            DataLoading = false;
            _font = new Font("Calibri", 8.25f);
            _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);

            this.mainForm = MainForm;
            this.Parent = mainForm.pGisView;
            this.Dock = DockStyle.Fill;

            imgList = new ImageList();
            imgList.Images.Add(Properties.Resources.table_up_angle_dot);
            imgList.Images.Add(Properties.Resources.table_line_dot);
            imgList.Images.Add(Properties.Resources.table_angle_dot);

            table = new DataTable("GIS");

            table.Columns.Add("SKVNAME", Type.GetType("System.String"));        // 0
            table.Columns.Add("PLAST", Type.GetType("System.String"));          // 1
            table.Columns.Add("PLASTZONE", Type.GetType("System.String"));      // 2
            table.Columns.Add("H", Type.GetType("System.Single"));              // 3
            table.Columns.Add("L", Type.GetType("System.Single"));              // 4
            table.Columns.Add("HL", Type.GetType("System.Single"));             // 5
            table.Columns.Add("HABS", Type.GetType("System.Single"));           // 6
            table.Columns.Add("LABS", Type.GetType("System.Single"));           // 7
            table.Columns.Add("HLABS", Type.GetType("System.Single"));          // 8
            table.Columns.Add("LIT", Type.GetType("System.String"));            // 9
            table.Columns.Add("COLLECTOR", Type.GetType("System.String"));      // 10
            table.Columns.Add("SAT0", Type.GetType("System.String"));           // 11
            table.Columns.Add("SAT", Type.GetType("System.String"));            // 12
            table.Columns.Add("RO", Type.GetType("System.Single"));             // 13
            table.Columns.Add("APS", Type.GetType("System.Single"));            // 14
            table.Columns.Add("AGK", Type.GetType("System.Single"));            // 15
            table.Columns.Add("ANKT", Type.GetType("System.Single"));           // 16

            table.Columns.Add("KPOR", Type.GetType("System.Single"));           // 17
            table.Columns.Add("KPR", Type.GetType("System.Single"));            // 18
            table.Columns.Add("KN", Type.GetType("System.Single"));             // 19
            table.Columns.Add("KV", Type.GetType("System.Single"));             // 20
            table.Columns.Add("KGL", Type.GetType("System.Single"));            // 21
            table.Columns.Add("PERF", Type.GetType("System.Boolean"));          // 22
            table.Columns.Add("PLASTID", Type.GetType("System.Int32"));         // 23
            table.Columns.Add("LITID", Type.GetType("System.Int32"));           // 24
            table.Columns.Add("COL", Type.GetType("System.Boolean"));           // 25
            table.Columns.Add("SAT0ID", Type.GetType("System.Int32"));          // 26
            table.Columns.Add("SATID", Type.GetType("System.Int32"));           // 27

            this.DataSource = table;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            
            DataGridViewImageColumn col = new DataGridViewImageColumn();
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ImageLayout = DataGridViewImageCellLayout.NotSet;
            col.DisplayIndex = 0;
            col.Width = 24;
            col.HeaderText = "";
            this.Columns.Add(col);
            this.Columns[this.Columns.Count - 1].DisplayIndex = 0;

            // SELECT
            SelectingDepth = -1;
            SelectUpdater = new Timer();
            SelectUpdater.Tick += new EventHandler(SelectUpdater_Tick);
            SelectUpdater.Interval = 100;
            SelectUpdater.Enabled = false;


            TableCellStyleFloat.Format = "0.##";
            TableCellStyleFloat.Font = _font;

            TableCellStyleAll.Font = _font;
            SetHeaderStyle();

            // Настройка таблицы ГИС
            this.RowTemplate.Height = 13;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = true;
            this.DoubleBuffered = true;

            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            SetHeaderStyle();

            this.BackgroundColor = SystemColors.ControlDark;

            this.Resize += new EventHandler(DataGridGIS_Resize);
            this.Paint += new PaintEventHandler(DataGridGIS_Paint);
            this.ColumnWidthChanged +=new DataGridViewColumnEventHandler(DataGridGIS_ColumnWidthChanged);
            this.KeyDown += new KeyEventHandler(DataGridGIS_KeyDown);
            this.MouseDown += new MouseEventHandler(DataGridGIS_MouseDown);
            this.MouseMove += new MouseEventHandler(DataGridGIS_MouseMove);
            this.MouseLeave += new EventHandler(DataGridGIS_MouseLeave);
        }

        #region Context Menu
        ContextMenuStrip cMenu;
        void InitContextMenu()
        {
            cMenu = new ContextMenuStrip();
            ToolStripMenuItem cmCopyCells = (ToolStripMenuItem)cMenu.Items.Add("Копировать в буфер обмена", Properties.Resources.Copy16);
            cmCopyCells.Click += new EventHandler(cmCopyCells_Click);
        }
        void cmCopyCells_Click(object sender, EventArgs e)
        {
            copySelectedRowsToClipboard((DataGridView)this);
        }
        #endregion

        void DataGridGIS_MouseLeave(object sender, EventArgs e)
        {
            mainForm.planeSelectGISdepth(-1);
        }
        void DataGridGIS_MouseDown(object sender, MouseEventArgs e)
        {
            // mainForm.StatUsage.AddMessage(CollectorStatId.GIS_MOUSE_DOWN);
            HitTestInfo hit = this.HitTest(e.X, e.Y);
            if ((hit.RowIndex == -1) || (hit.ColumnIndex == -1))
            {
                ArrayList list = new ArrayList();
                for (int i = 0; i < this.SelectedRows.Count; i++)
                {
                    list.Add(this.SelectedRows[i].Index);
                }
                for (int i = 0; i < list.Count; i++)
                {
                    this.Rows[(int)list[i]].Selected = false;
                }
            }
            lastSelectCell = null;
            ContextMenuStrip = null;
            if ((e.Button == MouseButtons.Right) && (hit.ColumnIndex != -1) && (hit.RowIndex != -1))
            {
                lastSelectCell = Rows[hit.RowIndex].Cells[hit.ColumnIndex];
                if (lastSelectCell.Selected) ContextMenuStrip = cMenu;
            }
        }
        void DataGridGIS_MouseMove(object sender, MouseEventArgs e)
        {
            HitTestInfo hit = this.HitTest(e.X, e.Y);
            if ((hit.RowIndex != -1) && (hit.ColumnIndex != -1))
            {
                float depthMD = (float)this.table.Rows[hit.RowIndex].ItemArray[3];
                mainForm.planeSelectGISdepth(depthMD);
            }
            else
            {
                mainForm.planeSelectGISdepth(-1);
            }
        }

        void DataGridGIS_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
            {
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    copySelectedRowsToClipboard((DataGridView)sender);
                }
            }
        }
        void DataGridGIS_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {

        }
        private void copySelectedRowsToClipboard(DataGridView dgv)
        {
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            Clipboard.Clear();
            if (dgv.GetClipboardContent() != null)
            {
                Clipboard.SetDataObject(dgv.GetClipboardContent());
                Clipboard.GetData(DataFormats.Text);
                IDataObject dt = Clipboard.GetDataObject();
                if (dt.GetDataPresent(typeof(string)))
                {
                    string tb = (string)(dt.GetData(typeof(string)));
                    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                    byte[] dataStr = encoding.GetBytes(tb);
                    Clipboard.SetDataObject(encoding.GetString(dataStr));
                }
            }
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
        }
        void SetHeaderStyle()
        {
            if (!DataLoading)
            {
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (i < 23)
                    {
                        if (((i > 3) && (i < 10)) || ((i > 13) && (i < 23)))
                            this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                        else
                            this.Columns[i].DefaultCellStyle = TableCellStyleAll;
                        if (i > 0)
                        {
                            this.Columns[i].Width = columnWidth[i - 1];
                            this.Columns[i].HeaderText = tableHeaderText[i - 1];
                        }
                    }
                    else
                    {
                        this.Columns[i].Visible = false;
                    }
                }
                HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
                HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
                HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
                HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
                this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;
            }
        }
        public void SetCellsStyle()
        {
            bool startPerforation = false;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if ((bool)table.Rows[i].ItemArray[22])
                {
                    if (!startPerforation)
                    {

                        this.Rows[i].Cells[23].Value = imgList.Images[0];
                        startPerforation = true;
                    }
                    else
                    {
                        if ((i == table.Rows.Count - 1) || (!(bool)table.Rows[i + 1].ItemArray[22]))
                        {
                            this.Rows[i].Cells[23].Value = imgList.Images[2];
                            startPerforation = false;
                        }
                        else
                            this.Rows[i].Cells[23].Value = imgList.Images[1];
                    }
                }
                SetCellsBackColor(i);
            }
        }
        void SetCellsBackColor(int RowIndex)
        {
            if ((RowIndex > -1) && (RowIndex < table.Rows.Count))
            {
                if ((bool)table.Rows[RowIndex].ItemArray[25])
                {
                    int sat = (int)table.Rows[RowIndex].ItemArray[26];
                    switch(sat)
                    {
                        case 1:
                            this.Rows[RowIndex].DefaultCellStyle.BackColor = Color.FromArgb(200, 122, 85);
                            break;
                        case 2:
                            this.Rows[RowIndex].DefaultCellStyle.BackColor = Color.FromArgb(155, 226, 255);
                            break;
                        case 9:
                            goto case 11;
                        case 11:
                            this.Rows[RowIndex].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 170);
                            break;
                        default:
                            this.Rows[RowIndex].DefaultCellStyle.BackColor = Color.White;
                            break;
                    }
                }
                else
                {
                    this.Rows[RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
        }
        public void SelectRowByDepth(double DepthAbs)
        {
            SetCellsBackColor(SelectedRowIndex);
            SelectedRowIndex = -1;
            float Habs, Labs;
            if (DepthAbs > -1)
            {
                for (int i = 0; i < this.table.Rows.Count; i++)
                {
                    Habs = (float)table.Rows[i].ItemArray[6];
                    Labs = (float)table.Rows[i].ItemArray[7];
                    if ((Habs < DepthAbs) && (DepthAbs < Habs + Labs))
                    {
                        SelectedRowIndex = i;
                        this.Rows[i].DefaultCellStyle.BackColor = Color.Lime;
                        break;
                    }
                }
            }
        }
        void SelectUpdater_Tick(object sender, EventArgs e)
        {

            SelectUpdater.Stop();
        }

        void DataGridGIS_Resize(object sender, EventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
                this.Invalidate();
            SetHeaderStyle();
        }
        void DataGridGIS_Paint(object sender, PaintEventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
            {
                System.Drawing.Graphics gr = e.Graphics;
                gr.DrawString("Нет данных для отображения", _font, Brushes.Black, this.Left + (int)(this.Width / 2), this.Top + (int)(this.Height / 2));
            }            
        }
        public void SetProject(Project aProject)
        {
            _proj = aProject;
        }
        public void ClearProject() 
        { 
            this._proj = null; 
            ClearTable(); 
        }
        public void ClearTable()
        {
            srcWell = null;
            DataLoading = true;
            table.Clear();
            DataLoaded = false;
            DataLoading = false;
            this.BackgroundColor = SystemColors.ControlDark;
        }
        public void ShowGIS(int oilFieldIndex, int wellIndex)
        {
            if (oilFieldIndex >= 0 && this.Parent.Visible)
            {
                OilField of = _proj.OilFields[oilFieldIndex];
                var dictPlast = (StratumDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                var dictSat = (DataDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
                var dictLit = (DataDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LITOLOGY);

                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataRow row;
                int i, ind;
                float EmptyValue = -999.25f;
                DataLoading = true;
                DataLoaded = false;
                this.DataSource = null;
                table.Clear();
                float predBottom = -1, predBottomAbs = -1;
                if (!w.GisLoaded) (_proj.OilFields[w.OilFieldIndex]).LoadGisFromCache(w.Index);

                if ((w.GisLoaded) && (w.gis.Count > 0))
                {
                    for (i = 0; i < w.gis.Count; i++)
                    {
                        gisItem = w.gis.Items[i];
                        row = table.NewRow();
                        if ((i > 0) && (predBottom < gisItem.H) && (predBottomAbs < gisItem.Habs))
                        {
                            row[0] = w.UpperCaseName;
                            row[1] = "н/д";
                            row[3] = predBottom;
                            row[4] = gisItem.H - predBottom;
                            row[5] = gisItem.H;
                            row[6] = predBottomAbs;
                            row[7] = gisItem.Habs - predBottomAbs;
                            row[8] = gisItem.Habs;

                            row[22] = false;
                            row[23] = 0;
                            row[24] = 0;
                            row[25] = false;
                            row[26] = 0;
                            row[27] = 0;
                            table.Rows.Add(row);
                            row = table.NewRow();
                        }
                        predBottom = gisItem.H + gisItem.L;
                        predBottomAbs = gisItem.Habs + gisItem.Labs;

                        row[0] = w.UpperCaseName;
                        row[23] = gisItem.PlastId;
                        if (dictPlast != null)
                        {
                            ind = dictPlast.GetIndexByCode(gisItem.PlastId);
                            row[1] = (ind > 0) ? dictPlast[ind].ShortName : "н/д";
                            ind = dictPlast.GetIndexByCode(gisItem.PlastZoneId);
                            row[2] = (ind > 0) ? dictPlast[ind].ShortName : "н/д";
                        }
                        else
                        {
                            row[1] = "н/д";
                            row[2] = "н/д";
                        }

                        row[3] = gisItem.H;
                        row[4] = gisItem.L;
                        row[5] = gisItem.H + gisItem.L;
                        row[6] = gisItem.Habs;
                        row[7] = gisItem.Labs;
                        row[8] = gisItem.Habs + gisItem.Labs;

                        if (dictLit != null)
                        {
                            row[9] = dictLit.GetShortNameByCode(gisItem.LitId);
                        }
                        row[24] = gisItem.LitId;

                        if (gisItem.Collector) row[10] = "Да"; else row[10] = "Нет";
                        row[25] = gisItem.Collector;

                        if (dictSat != null)
                        {
                            row[11] = dictSat.GetShortNameByCode(gisItem.Sat0);
                            row[12] = dictSat.GetShortNameByCode(gisItem.Sat);
                        }

                        row[26] = gisItem.Sat0;
                        row[27] = gisItem.Sat;

                        if(gisItem.Ro != EmptyValue) row[13] = gisItem.Ro;
                        if (gisItem.APS != EmptyValue) row[14] = gisItem.APS;
                        if (gisItem.aGK != EmptyValue) row[15] = gisItem.aGK;
                        if (gisItem.aNKT != EmptyValue) row[16] = gisItem.aNKT;
                        if (gisItem.Kpor != EmptyValue) row[17] = gisItem.Kpor;
                        if (gisItem.Kpr != EmptyValue) row[18] = gisItem.Kpr;
                        if (gisItem.Kn != EmptyValue) row[19] = gisItem.Kn;
                        if (gisItem.Kgas != EmptyValue) row[20] = gisItem.Kgas;
                        if (gisItem.Kgl != EmptyValue) row[21] = gisItem.Kgl;
                        row[22] = 0;

                        table.Rows.Add(row);
                    }
                    DataLoaded = true;
                    this.BackgroundColor = Color.White;
                }
                DataLoading = false;
                this.DataSource = table;
                SetHeaderStyle();
                SetCellsStyle();
                if (this.SelectedRows.Count > 0) this.SelectedRows[0].Selected = false;
            }
            else if ((oilFieldIndex >= 0) && (!this.Parent.Visible))
            {
                OilField of = _proj.OilFields[oilFieldIndex];
                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataLoading = false;
                DataLoaded = false;
                table.Clear();
            }
        }
        public void UpdateBySourceWell()
        {
            if ((srcWell != null) && (!DataLoaded))
            {
                ShowGIS(srcWell.OilFieldIndex, srcWell.Index);
            }
        }
    }
    #endregion

    #region КЕРН

    #region КЕРН Наличие
    sealed class DataGridCore : DataGridView
    {
        Project _proj;
        MainForm mainForm;
        int SelectedRowIndex = -1;
        public DataTable table;
        public bool DataLoaded;
        bool DataLoading;
        Font _font, _font_bold;
        SkvCoreItem coreItem;
        ImageList imgList;
        Well srcWell;
        DataGridViewCellStyle HeaderCellStyle, TableCellStyleAll, TableCellStyleFloat;
        string[] tableHeaderText = { "Инт.отбора, начало, м", 
                                     "Инт.отбора, конец, м", 
                                     "Проходка отбора, м",
                                     "Вынос керна, м", 
                                     "Кол-во хранимого керна, пог.м", 
                                     "Количество ящиков хранения, шт", 
                                     "Состояние керна", 
                                     "Состояние маркировки"
                                     };
        int[] columnWidth = { 50, 50, 50, 50, 50, 50, 50, 50 };

        public DataGridCore(MainForm MainForm)
        {
            HeaderCellStyle = new DataGridViewCellStyle();
            TableCellStyleAll = new DataGridViewCellStyle();
            TableCellStyleFloat = new DataGridViewCellStyle();

            srcWell = null;
            DataLoaded = false;
            DataLoading = false;
            _font = new Font("Calibri", 8.25f);
            _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);

            this.mainForm = MainForm;
            this.Parent = mainForm.pCore;
            this.Dock = DockStyle.Fill;

            table = new DataTable("CORE");

            table.Columns.Add("TOP", Type.GetType("System.Single"));               // 0
            table.Columns.Add("BOTTOM", Type.GetType("System.Single"));            // 1
            table.Columns.Add("RECOVERY", Type.GetType("System.Single"));          // 2 
            table.Columns.Add("DRIFTING", Type.GetType("System.Single"));          // 3
            table.Columns.Add("STORED", Type.GetType("System.Single"));            // 4
            table.Columns.Add("STORED_BOX", Type.GetType("System.Byte"));          // 5
            table.Columns.Add("STATE", Type.GetType("System.String"));             // 6
            table.Columns.Add("MARK_STATE", Type.GetType("System.String"));        // 7

            this.DataSource = table;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            TableCellStyleFloat.Format = "0.##";
            TableCellStyleFloat.Font = _font;

            TableCellStyleAll.Font = _font;
            SetHeaderStyle();

            // Настройка таблицы Наличие керна
            this.RowTemplate.Height = 13;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = true;
            this.DoubleBuffered = true;

            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            SetHeaderStyle();

            this.BackgroundColor = SystemColors.ControlDark;

            this.Resize += new EventHandler(DataGridCore_Resize);
            this.Paint += new PaintEventHandler(DataGridCore_Paint);
            this.KeyDown += new KeyEventHandler(DataGridCore_KeyDown);
            this.MouseDown += new MouseEventHandler(DataGridCore_MouseDown);
        }

        void DataGridCore_MouseDown(object sender, MouseEventArgs e)
        {
            // mainForm.StatUsage.AddMessage(CollectorStatId.CORE_TABLE_MOUSE_DOWN);
            HitTestInfo hit = this.HitTest(e.X, e.Y);
            if ((hit.RowIndex == -1) || (hit.ColumnIndex == -1))
            {
                ArrayList list = new ArrayList();
                for (int i = 0; i < this.SelectedRows.Count; i++)
                {
                    list.Add(this.SelectedRows[i].Index);
                }
                for (int i = 0; i < list.Count; i++)
                {
                    this.Rows[(int)list[i]].Selected = false;
                }
            }            
        }

        void DataGridCore_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    copySelectedRowsToClipboard((DataGridView)sender);
                }
        }
        private void copySelectedRowsToClipboard(DataGridView dgv)
        {
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            Clipboard.Clear();
            if (dgv.GetClipboardContent() != null)
            {
                Clipboard.SetDataObject(dgv.GetClipboardContent());
                Clipboard.GetData(DataFormats.Text);
                IDataObject dt = Clipboard.GetDataObject();
                if (dt.GetDataPresent(typeof(string)))
                {
                    string tb = (string)(dt.GetData(typeof(string)));
                    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                    byte[] dataStr = encoding.GetBytes(tb);
                    Clipboard.SetDataObject(encoding.GetString(dataStr));
                }
            }
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
        }
        void SetHeaderStyle()
        {
            if (!DataLoading)
            {
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (i < 5)
                        this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    else
                        this.Columns[i].DefaultCellStyle = TableCellStyleAll;

                    this.Columns[i].Width = columnWidth[i];
                    this.Columns[i].HeaderText = tableHeaderText[i];
                    this.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
                HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
                HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
                HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
                HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
                this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;
            }
        }
        public void SetCellsStyle()
        {
            //bool startPerforation = false;
            //for (int i = 0; i < table.Rows.Count; i++)
            //{
            //    if ((bool)table.Rows[i].ItemArray[22])
            //    {
            //        if (!startPerforation)
            //        {

            //            this.Rows[i].Cells[23].Value = imgList.Images[0];
            //            startPerforation = true;
            //        }
            //        else
            //        {
            //            if ((i == table.Rows.Count - 1) || (!(bool)table.Rows[i + 1].ItemArray[22]))
            //            {
            //                this.Rows[i].Cells[23].Value = imgList.Images[2];
            //                startPerforation = false;
            //            }
            //            else
            //                this.Rows[i].Cells[23].Value = imgList.Images[1];
            //        }
            //    }
            //    SetCellsBackColor(i);
            //}
        }
        void SetCellsBackColor(int RowIndex)
        {
            //if ((RowIndex > -1) && (RowIndex < table.Rows.Count))
            //{
            //    if ((bool)table.Rows[RowIndex].ItemArray[25])
            //    {
            //        if ((int)table.Rows[RowIndex].ItemArray[26] == 1)
            //            this.Rows[RowIndex].DefaultCellStyle.BackColor = Color.FromArgb(200, 122, 85);
            //        else if ((int)table.Rows[RowIndex].ItemArray[26] == 2)
            //            this.Rows[RowIndex].DefaultCellStyle.BackColor = Color.FromArgb(155, 226, 255);
            //    }
            //    else
            //    {
            //        this.Rows[RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
            //    }
            //}
        }

        void DataGridCore_Resize(object sender, EventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
            {
                this.Invalidate();
            }
            SetHeaderStyle();
        }
        void DataGridCore_Paint(object sender, PaintEventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
            {
                System.Drawing.Graphics gr = e.Graphics;
                gr.DrawString("Нет данных для отображения", _font, Brushes.Black, this.Left + (int)(this.Width / 2), this.Top + (int)(this.Height / 2));
            }
        }
        public void SetProject(Project aProject)
        {
            _proj = aProject;
        }
        public void ClearProject() { this._proj = null; ClearTable(); }

        public void ClearTable()
        {
            srcWell = null;
            DataLoading = true;
            table.Clear();
            DataLoaded = false;
            DataLoading = false;
            UpdateTitle();
            this.BackgroundColor = SystemColors.ControlDark;
        }

        string GetStateName(byte StateID)
        {
            string res = "";
            switch (StateID)
            {
                case 0:
                    res = string.Empty;
                    break;
                case 1:
                    res = "Неуд";
                    break;
                case 2:
                    res = "Удовл";
                    break;
                case 3:
                    res = "Хор";
                    break;
            }
            return res;
        }

        public void ShowCore(int oilFieldIndex, int wellIndex)
        {
            if ((oilFieldIndex >= 0) && (this.Parent.Visible))
            {
                OilField of = _proj.OilFields[oilFieldIndex];

                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataRow row;
                int i;

                DataLoading = true;
                DataLoaded = false;
                this.DataSource = null;
                table.Clear();
                if (!w.CoreLoaded) (_proj.OilFields[w.OilFieldIndex]).LoadCoreFromCache(w.Index);

                if ((w.CoreLoaded) && (w.core.Count > 0))
                {
                    for (i = 0; i < w.core.Count; i++)
                    {
                        coreItem = w.core.Items[i];
                        row = table.NewRow();
                        row[0] = coreItem.Top;
                        row[1] = coreItem.Bottom;
                        row[2] = coreItem.Bottom - coreItem.Top;
                        row[3] = coreItem.CoreRecovery;
                        row[4] = coreItem.CoreStored;
                        row[5] = coreItem.StoredBoxCount;
                        row[6] = GetStateName(coreItem.CoreState);
                        row[7] = GetStateName(coreItem.CoreMarkState);
                        table.Rows.Add(row);
                    }
                    DataLoaded = true;
                    this.BackgroundColor = Color.White;
                }
                DataLoading = false;
                this.DataSource = table;
                SetHeaderStyle();
                SetCellsStyle();
                UpdateTitle();
                if (this.SelectedRows.Count > 0) this.SelectedRows[0].Selected = false;
            }
            else if ((oilFieldIndex >= 0) && (!this.Parent.Visible))
            {
                OilField of = _proj.OilFields[oilFieldIndex];
                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataLoading = false;
                DataLoaded = false;
                table.Clear();
            }
        }
        public void UpdateBySourceWell()
        {
            if ((srcWell != null) && (!DataLoaded))
            {
                ShowCore(srcWell.OilFieldIndex, srcWell.Index);
            }
        }
        public void UpdateTitle()
        {
            if (srcWell != null)
                mainForm.coreSetPaneTitle("Керн: наличие - Скв: " + srcWell.Name);
            else
                mainForm.coreSetPaneTitle("Керн: наличие");
        }
    }
    #endregion

    #region КЕРН Исследования
    sealed class DataGridCoreTest : DataGridView
    {
        Project _proj;
        MainForm mainForm;
        ArrayList SelectedRowsList;
        public DataTable table;
        public bool DataLoaded;
        bool DataLoading;
        Font _font, _font_bold;
        SkvCoreTestItem coreTestItem;
        Well srcWell;
        DataGridViewCellStyle HeaderCellStyle, TableCellStyleAll, TableCellStyleFloat;

        #region Header Style
        string[] tableHeaderText = { "Номер образца", 
                                     "Дата", 
                                     "Инт.отбора, начало, м", 
                                     "Инт.отбора, конец, м", 
                                     "Вынос керна, м", 
                                     "Расстояние от верха, м", 
                                     "Литология", 
                                     "Характер насыщения", 
                                     "Пористость открытая жидкостенасыщением, %",
                                     "Проницаемость по воздуху ПП, мД", 
                                     "Проницаемость по воздуху ПР, мД", 
                                     "Давление, атм",
                                     "Пористость открытая по гелию, %",
                                     "Проницаемость по гелию ПП, мД", 
                                     "Проницаемость по гелию ПР, мД", 
                                     "Карбонатность кальцит, %", 
                                     "Карбонатность доломит, %", 
                                     "Карбонатность нераствор.ост., %", 
                                     "Ада до экстракции, мВ", 
                                     "Ада после экстракции, мВ",
                                     "Смачиваемость",
                                     "Пористость", 
                                     "Остат.водонасыщенность, %", 
                                     "Минералогическая плотность",
                                     "Объемная плотность",
                                     "Гранулометрич.анализ,>500 мкм, %",
                                     "Гранулометрич.анализ,>250 мкм, %",
                                     "Гранулометрич.анализ,>100 мкм, %",
                                     "Гранулометрич.анализ,>50 мкм, %",
                                     "Гранулометрич.анализ,>10 мкм, %",
                                     "Гранулометрич.анализ,<10 мкм, %",
                                     "Радиактивность К, Бк/кг",
                                     "Радиактивность К, %",
                                     "Радиактивность Th, Бк/кг",
                                     "Радиактивность Th, %",
                                     "Радиактивность Ra, Бк/кг",
                                     "Радиактивность Ra, %",
                                     "Радиактивность A, Бк/кг",
                                     "Примечание"
                                   };

        int[] columnWidth = { 50, 46, 46, 45, 30, 45, 45, 30, 45, 30, 
                              30, 23, 30, 30, 33, 33, 33, 33, 33, 33, 
                              30, 23, 30, 30, 33, 33, 33, 33, 33, 33,
                              30, 23, 30, 30, 33, 33, 33, 33, 33};
        #endregion

        public DataGridCoreTest(MainForm MainForm)
        {
            HeaderCellStyle = new DataGridViewCellStyle();
            TableCellStyleAll = new DataGridViewCellStyle();
            TableCellStyleFloat = new DataGridViewCellStyle();

            SelectedRowsList = new ArrayList();
            srcWell = null;
            DataLoaded = false;
            DataLoading = false;
            _font = new Font("Calibri", 8.25f);
            _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);

            this.mainForm = MainForm;
            this.Parent = mainForm.pCoreTest;
            this.Dock = DockStyle.Fill;

            #region Create Table
            table = new DataTable("CORE_TEST");

            table.Columns.Add("NAME", Type.GetType("System.String"));           // 0
            table.Columns.Add("DATE", Type.GetType("System.DateTime"));         // 1
            table.Columns.Add("TOP", Type.GetType("System.Single"));            // 2
            table.Columns.Add("BOTTOM", Type.GetType("System.Single"));         // 3
            table.Columns.Add("RECOVERY", Type.GetType("System.Single"));       // 4
            table.Columns.Add("DIST_TOP", Type.GetType("System.Single"));       // 5
            table.Columns.Add("LIT", Type.GetType("System.String"));            // 6
            table.Columns.Add("SAT", Type.GetType("System.String"));            // 7

            table.Columns.Add("KPOR_LIQ", Type.GetType("System.Single"));       // 8
            table.Columns.Add("KPR_AIR_PP", Type.GetType("System.Single"));     // 9
            table.Columns.Add("KPR_AIR_PR", Type.GetType("System.Single"));     // 10

            table.Columns.Add("PRESSURE", Type.GetType("System.Single"));       // 11
            table.Columns.Add("KPOR_HEL", Type.GetType("System.Single"));       // 12
            table.Columns.Add("KPR_HEL_PP", Type.GetType("System.Single"));     // 13
            table.Columns.Add("KPR_HEL_PR", Type.GetType("System.Single"));     // 14

            table.Columns.Add("CARBON_TIFF", Type.GetType("System.Single"));    // 15
            table.Columns.Add("CARBON_DOLOM", Type.GetType("System.Single"));   // 16
            table.Columns.Add("CARBON_INSOL", Type.GetType("System.Single"));   // 17

            table.Columns.Add("ADA_BEFORE", Type.GetType("System.Single"));     // 18
            table.Columns.Add("ADA_AFTER", Type.GetType("System.Single"));      // 19

            table.Columns.Add("WETTA", Type.GetType("System.Single"));          // 20
            table.Columns.Add("POROSITY", Type.GetType("System.Single"));       // 21
            table.Columns.Add("SAT_WAT_FINAL", Type.GetType("System.Single"));  // 22

            table.Columns.Add("DENS_MIN", Type.GetType("System.Single"));       // 23
            table.Columns.Add("DENS_VOL", Type.GetType("System.Single"));       // 24
            table.Columns.Add("PART_500", Type.GetType("System.Single"));       // 25
            table.Columns.Add("PART_250", Type.GetType("System.Single"));       // 26
            table.Columns.Add("PART_100", Type.GetType("System.Single"));       // 27
            table.Columns.Add("PART_50", Type.GetType("System.Single"));        // 28
            table.Columns.Add("PART_10", Type.GetType("System.Single"));        // 29
            table.Columns.Add("PART_0", Type.GetType("System.Single"));         // 30

            table.Columns.Add("RADIO_ACTIVE_K", Type.GetType("System.Single"));         // 31
            table.Columns.Add("RADIO_ACTIVE_K_PERS", Type.GetType("System.Single"));    // 32
            table.Columns.Add("RADIO_ACTIVE_TH", Type.GetType("System.Single"));        // 33
            table.Columns.Add("RADIO_ACTIVE_TH_PERS", Type.GetType("System.Single"));   // 34
            table.Columns.Add("RADIO_ACTIVE_RA", Type.GetType("System.Single"));        // 35
            table.Columns.Add("RADIO_ACTIVE_RA_PERS", Type.GetType("System.Single"));   // 36
            table.Columns.Add("RADIO_ACTIVE_A", Type.GetType("System.Single"));         // 37
            table.Columns.Add("COMMENT", Type.GetType("System.String"));                // 38
            table.Columns.Add("SAT_ID", Type.GetType("System.Byte"));                   // 39
            table.Columns.Add("NAME_ID", Type.GetType("System.Boolean"));               // 40

            #endregion
            
            this.DataSource = table;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


            TableCellStyleFloat.Format = "0.##";
            TableCellStyleFloat.Font = _font;

            TableCellStyleAll.Font = _font;

            SetHeaderStyle();
            // Настройка таблицы Исследования Керна
            this.RowTemplate.Height = 13;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = true;
            this.DoubleBuffered = true;

            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            SetHeaderStyle();

            this.BackgroundColor = SystemColors.ControlDark;

            this.Resize += new EventHandler(DataGridCoreTest_Resize);
            this.Paint += new PaintEventHandler(DataGridCoreTest_Paint);
            this.KeyDown += new KeyEventHandler(DataGridCoreTest_KeyDown);
            this.MouseDown += new MouseEventHandler(DataGridCoreTest_MouseDown);
            this.MouseMove += new MouseEventHandler(DataGridCoreTest_MouseMove);
        }

        void DataGridCoreTest_MouseDown(object sender, MouseEventArgs e)
        {
            // mainForm.StatUsage.AddMessage(CollectorStatId.CORE_TEST_MOUSE_DOWN);
            HitTestInfo hit = this.HitTest(e.X, e.Y);
            if ((hit.RowIndex == -1) || (hit.ColumnIndex == -1))
            {
                ArrayList list = new ArrayList();
                for (int i = 0; i < this.SelectedRows.Count; i++)
                {
                    list.Add(this.SelectedRows[i].Index);
                }
                for (int i = 0; i < list.Count; i++)
                {
                    this.Rows[(int)list[i]].Selected = false;
                }
            }            
        }
        void DataGridCoreTest_MouseMove(object sender, MouseEventArgs e)
        {
            HitTestInfo hit = this.HitTest(e.X, e.Y);
            if ((hit.RowIndex != -1) && (hit.ColumnIndex != -1))
            {
                float depthMD = (float)this.table.Rows[hit.RowIndex].ItemArray[2];
                depthMD += (float)this.table.Rows[hit.RowIndex].ItemArray[5];
                mainForm.planeSelectCoreTestdepth(depthMD);
            }
        }
        void DataGridCoreTest_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    copySelectedRowsToClipboard((DataGridView)sender);
                }
        }
        private void copySelectedRowsToClipboard(DataGridView dgv)
        {
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            Clipboard.Clear();
            if (dgv.GetClipboardContent() != null)
            {
                Clipboard.SetDataObject(dgv.GetClipboardContent());
                Clipboard.GetData(DataFormats.Text);
                IDataObject dt = Clipboard.GetDataObject();
                if (dt.GetDataPresent(typeof(string)))
                {
                    string tb = (string)(dt.GetData(typeof(string)));
                    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                    byte[] dataStr = encoding.GetBytes(tb);
                    Clipboard.SetDataObject(encoding.GetString(dataStr));
                }
            }
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
        }
        void SetHeaderStyle()
        {
            if (!DataLoading)
            {
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (((i > 1) && (i < 6)) || ((i > 7) && (i < 38)))
                        this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    else
                        this.Columns[i].DefaultCellStyle = TableCellStyleAll;
                    if (i < 39)
                    {
                        this.Columns[i].Width = columnWidth[i];
                        this.Columns[i].HeaderText = tableHeaderText[i];
                        this.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
                HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
                HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
                HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
                HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
                this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;
            }
            if (this.Columns.Count > 0) this.Columns[39].Visible = false;
        }
        public void SetCellsStyle()
        {
            byte b;
            bool name_id;
            for (int i = 0; i < table.Rows.Count; i++)
            {

                b = (byte)table.Rows[i].ItemArray[39];
                name_id = (bool)table.Rows[i].ItemArray[40];
                if (name_id)
                {
                    this.Rows[i].DefaultCellStyle.BackColor = Color.White;
                }
                else
                {
                    this.Rows[i].DefaultCellStyle.BackColor = Color.LightGray;
                }
                if (b != 14)
                {
                    switch(b)
                    {
                        case 1:
                            this.Rows[i].Cells[7].Style.BackColor = Color.FromArgb(200, 122, 85);
                            break;
                        case 2:
                            this.Rows[i].Cells[7].Style.BackColor = Color.FromArgb(155, 226, 255);
                            break;
                        case 5:
                            this.Rows[i].Cells[7].Style.BackColor = Color.FromArgb(255, 177, 140);
                            break;
                    }
                }
            }
        }

        void DataGridCoreTest_Resize(object sender, EventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
                this.Invalidate();
            SetHeaderStyle();
        }
        void DataGridCoreTest_Paint(object sender, PaintEventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
            {
                System.Drawing.Graphics gr = e.Graphics;
                gr.DrawString("Нет данных для отображения", _font, Brushes.Black, this.Left + (int)(this.Width / 2), this.Top + (int)(this.Height / 2));
            }
        }
        public void SetProject(Project aProject)
        {
            _proj = aProject;
        }
        public void ClearProject() { this._proj = null; ClearTable(); }
        public void ClearTable()
        {
            srcWell = null;
            DataLoading = true;
            table.Clear();
            DataLoaded = false;
            DataLoading = false;
            UpdateTitle();
            this.BackgroundColor = SystemColors.ControlDark;
        }

        void SetDefaultBackColor(ArrayList RowIndexList)
        {
            int ind;
            byte b;
            bool name_id;
            for (int i = 0; i < RowIndexList.Count; i++)
            {
                ind = (int)RowIndexList[i];
                if (ind < table.Rows.Count)
                {
                    name_id = (bool)table.Rows[ind].ItemArray[40];
                    if (name_id)
                    {
                        this.Rows[ind].DefaultCellStyle.BackColor = Color.White;
                    }
                    else
                    {
                        this.Rows[ind].DefaultCellStyle.BackColor = Color.LightGray;
                    }

                    b = (byte)table.Rows[ind].ItemArray[39];
                    if (b != 14)
                    {
                        switch (b)
                        {
                            case 1:
                                this.Rows[ind].Cells[7].Style.BackColor = Color.FromArgb(200, 122, 85);
                                break;
                            case 2:
                                this.Rows[ind].Cells[7].Style.BackColor = Color.FromArgb(155, 226, 255);
                                break;
                            case 5:
                                this.Rows[ind].Cells[7].Style.BackColor = Color.FromArgb(255, 177, 140);
                                break;
                        }
                    }
                }
            }
        }
        public void SelectRowByDepth(double DepthMd, bool SetFirstDisplayed)
        {
            SetDefaultBackColor(SelectedRowsList);
            SelectedRowsList.Clear();
            float H, L;
            if ((DepthMd > -1) && (this.table.Rows.Count > 0))
            {
                int first = -1;
                for (int i = 0; i < this.table.Rows.Count; i++)
                {
                    H = (float)table.Rows[i].ItemArray[2];
                    L = (float)table.Rows[i].ItemArray[5];
                    if (Math.Abs(H + L - DepthMd) < 0.001)
                    {
                        SelectedRowsList.Add(i);
                        if (first == -1) first = i;
                        this.Rows[i].DefaultCellStyle.BackColor = Color.Lime;
                    }
                }
                if ((SetFirstDisplayed) && (first > -1))
                {
                    int count = this.DisplayedRowCount(false);
                    count = (int)(count / 2);
                    first = first - count;
                    if (first < 0) first = 0;
                    if (this.ClientRectangle.Height > 15) this.FirstDisplayedScrollingRowIndex = first;
                }
            }
        }

        public void ShowCoreTest(int oilFieldIndex, int wellIndex)
        {
            const float NaN = -9999.0F;
            if ((oilFieldIndex >= 0) && (this.Parent.Visible))
            {
                OilField of = _proj.OilFields[oilFieldIndex];
                var dict = (DataDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataRow row;
                int i;

                DataLoading = true;
                DataLoaded = false;
                this.DataSource = null;
                table.Clear();
                if (!w.CoreTestLoaded) (_proj.OilFields[w.OilFieldIndex]).LoadCoreTestFromCache(w.Index);
                bool name_id = true;
                string predName = "";
                if ((w.CoreTestLoaded) && (w.coreTest.Count > 0))
                {
                    for (i = 0; i < w.coreTest.Count; i++)
                    {
                        coreTestItem = w.coreTest.Items[i];
                        row = table.NewRow();
                        row[0] = coreTestItem.Name;
                        if (coreTestItem.Name != predName)
                        {
                            name_id = !name_id;
                            predName = coreTestItem.Name;
                        }
                        row[40] = name_id;
                        row[1] = coreTestItem.Date;
                        row[2] = coreTestItem.Top;
                        row[3] = coreTestItem.Bottom;
                        row[4] = coreTestItem.CoreRecovery;
                        row[5] = coreTestItem.DistanceTop;
                        row[6] = coreTestItem.Litology;
                        row[7] = dict.GetShortNameByCode(coreTestItem.SatId);
                        row[39] = coreTestItem.SatId;
                        // с проверками на NaN(-9999.0F)
                        if (coreTestItem.KporLiquid != NaN)         row[8] = coreTestItem.KporLiquid;
                        if (coreTestItem.KprAirPP != NaN)           row[9] = coreTestItem.KprAirPP;
                        if (coreTestItem.KprAirPR != NaN)           row[10] = coreTestItem.KprAirPR;
                        if (coreTestItem.Pressure != NaN)           row[11] = (float)Math.Round(coreTestItem.Pressure / 14.696F, 2);
                        if (coreTestItem.KporHelium != NaN)         row[12] = coreTestItem.KporHelium;
                        if (coreTestItem.KprHeliumPP != NaN)        row[13] = coreTestItem.KprHeliumPP;
                        if (coreTestItem.KprHeliumPR != NaN)        row[14] = coreTestItem.KprHeliumPR;
                        if (coreTestItem.CarbonateTiff != NaN)      row[15] = coreTestItem.CarbonateTiff;
                        if (coreTestItem.CarbonateDolomite != NaN)  row[16] = coreTestItem.CarbonateDolomite;
                        if (coreTestItem.CarbonateInsoluble != NaN) row[17] = coreTestItem.CarbonateInsoluble;
                        if (coreTestItem.AdaBeforeExtract != NaN)   row[18] = coreTestItem.AdaBeforeExtract;
                        if (coreTestItem.AdaAfterExtract != NaN)    row[19] = coreTestItem.AdaAfterExtract;
                        if (coreTestItem.Wettability != NaN)        row[20] = coreTestItem.Wettability;
                        if (coreTestItem.Porosity != NaN)           row[21] = coreTestItem.Porosity;
                        if (coreTestItem.SatWatFinal != NaN)        row[22] = coreTestItem.SatWatFinal;
                        if (coreTestItem.DensityMineral != NaN)     row[23] = coreTestItem.DensityMineral;
                        if (coreTestItem.DensityVolume != NaN)      row[24] = coreTestItem.DensityVolume;
                        if (coreTestItem.PartSize500 != NaN)        row[25] = coreTestItem.PartSize500;
                        if (coreTestItem.PartSize250 != NaN)        row[26] = coreTestItem.PartSize250;
                        if (coreTestItem.PartSize100 != NaN)        row[27] = coreTestItem.PartSize100;
                        if (coreTestItem.PartSize50 != NaN)         row[28] = coreTestItem.PartSize50;
                        if (coreTestItem.PartSize10 != NaN)         row[29] = coreTestItem.PartSize10;
                        if (coreTestItem.PartSize0 != NaN)          row[30] = coreTestItem.PartSize0;
                        if (coreTestItem.RadioActivityK != NaN)     row[31] = coreTestItem.RadioActivityK;
                        if (coreTestItem.RadioActivityKPercent != NaN)  row[32] = coreTestItem.RadioActivityKPercent;
                        if (coreTestItem.RadioActivityTh != NaN)        row[33] = coreTestItem.RadioActivityTh;
                        if (coreTestItem.RadioActivityThPercent != NaN) row[34] = coreTestItem.RadioActivityThPercent;
                        if (coreTestItem.RadioActivityRa != NaN)        row[35] = coreTestItem.RadioActivityRa;
                        if (coreTestItem.RadioActivityRaPercent != NaN) row[36] = coreTestItem.RadioActivityRaPercent;
                        if (coreTestItem.RadioActivityA != NaN)         row[37] = coreTestItem.RadioActivityA;
                        row[38] = coreTestItem.Comment;
                        table.Rows.Add(row);
                    }
                    DataLoaded = true;
                    this.BackgroundColor = Color.White;
                }
                DataLoading = false;
                this.DataSource = table;
                SetHeaderStyle();
                SetCellsStyle();
                UpdateTitle();
                if (this.SelectedRows.Count > 0) this.SelectedRows[0].Selected = false;
            }
            else if ((oilFieldIndex >= 0) && (!this.Parent.Visible))
            {
                OilField of = _proj.OilFields[oilFieldIndex];
                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataLoading = false;
                DataLoaded = false;
                table.Clear();
            }
        }
        public void UpdateBySourceWell()
        {
            if ((srcWell != null) && (!DataLoaded))
            {
                ShowCoreTest(srcWell.OilFieldIndex, srcWell.Index);
            }
        }
        public void UpdateTitle()
        {
            if (srcWell != null)
                mainForm.coreTestSetPaneTitle("Керн: исследования - Скв: " + srcWell.Name);
            else
                mainForm.coreTestSetPaneTitle("Керн: исследования");
        }
    }
    #endregion

    #endregion

    #region Таблица Ячейки
    sealed class DataGridAreas : DataGridView
    {
        Project _proj;
        MainForm mainForm;
        public DateTime lastDateMer = DateTime.MinValue, calcDate = DateTime.MinValue;
        DataSet _dsAreas;
        Infragistics.Win.UltraWinDock.DockableControlPane dcp;
        public DataTable table;
        public bool DataLoaded;
        bool DataLoading;
        Font _font, _font_bold;
        ImageList imgList;
        bool ByDate;
        DataGridViewCellStyle HeaderCellStyle, TableCellStyleAll, TableCellStyleFloat, TableCellStyleFloatPVT, TableCellStylePercent, TableCellStyleDatetime;
        string[] tableHeaderText = {"Мест.", "Площадь", "Ячейка", "Пласт", "Кол-во скв всего, шт", "Кол-во раб.скв, шт",
                                    "Жидк., т", "Нефть, т", "Зак., м3", "Тек.Комп, %", 
                                    "Кол-во раб.скв за историю, шт", "Нак.жидк., т", "Нак.нефть, т", "Нак.зак., м3", "Нак.комп, %",  
                                    "Объем.коэф.нефти, д.ед","Объем.коэф.воды, д.ед","Нач.пласт.давл., атм",
                                    "Плотн.нефти, г/см3","Вязк.нефти, мПа*с","Коэф.вытесн.,д.ед",
                                    "Коэф.порист., д.ед","Нач.нефтенасыщ-ть, д.ед","Давл.насыщ., атм",
                                    "Плотн.воды, г/см3","Вязк.воды, мПа*с", "Проницаемость, мД", "Комментарий", "Дата", "Текущее давление, атм", 
                                    "НГЗ, тыс. т", "НИЗ, тыс. т", "ОИЗ, тыс.т", "НГДУ", "% воды текущий", "% воды накопленный"};


        #region ContexMenuStrip

        ContextMenuStrip cMenu;
        ToolStripMenuItem cmExpandAll, cmCollapseAll;

        // constructor context menu
        private void InitializeContextMenus()
        {
            // CmenuUserData - Месторождение
            cMenu = new ContextMenuStrip();
            cmExpandAll = (ToolStripMenuItem)cMenu.Items.Add("");
            cmExpandAll.Text = "Развернуть все";
            cmExpandAll.Click += new EventHandler(cmExpandAll_Click);

            cmCollapseAll = (ToolStripMenuItem)cMenu.Items.Add("");
            cmCollapseAll.Text = "Свернуть все";
            cmCollapseAll.Click += new EventHandler(cmCollapseAll_Click);
        }

        void cmExpandAll_Click(object sender, EventArgs e)
        {
            if (this.Rows.Count > 0)
            {
                for (int i = 0; i < this.Rows.Count; i++)
                {
                    if (((int)this.Rows[i].Cells[1].Value == -1) && ((int)this.Rows[i].Cells[2].Value == 1))
                    {
                        this.Rows[i].Cells[table.Columns.Count].Value = imgList.Images[1];
                        this.Rows[i].Cells[table.Columns.Count + 1].Value = 1;
                    }
                    else
                    {
                        this.Rows[i].Visible = true;
                    }
                }
            }
        }

        void cmCollapseAll_Click(object sender, EventArgs e)
        {
            if (this.Rows.Count > 0)
            {
                CurrencyManager cm = (CurrencyManager)BindingContext[this.DataSource];
                cm.SuspendBinding();
                int all_index = 0;
                bool sel = false;
                for (int i = 0; i < this.Rows.Count; i++)
                {
                    if (((int)this.Rows[i].Cells[1].Value == -1) && ((int)this.Rows[i].Cells[2].Value == 1))
                    {
                        this.Rows[i].Cells[table.Columns.Count].Value = imgList.Images[0];
                        this.Rows[i].Cells[table.Columns.Count + 1].Value = 0;
                        all_index = i;
                    }
                    else
                    {
                        if ((!sel) && (this.Rows[i].Selected))
                        {
                            this.Rows[all_index].Selected = true;
                            sel = true;
                        }
                        this.Rows[i].Selected = false;
                        this.Rows[i].Visible = false;
                    }
                }
                cm.ResumeBinding();
            }            
        }
        #endregion

        public DataGridAreas(MainForm MainForm)
        {
            int[] columnWidth = { 100, 100, 70, 100, 55, 55, 70, 70, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 200, 24};

            InitializeContextMenus();
            this.ContextMenuStrip = cMenu;
            HeaderCellStyle = new DataGridViewCellStyle();
            TableCellStyleAll = new DataGridViewCellStyle();
            TableCellStyleFloat = new DataGridViewCellStyle();
            TableCellStyleFloatPVT = new DataGridViewCellStyle();
            TableCellStylePercent = new DataGridViewCellStyle();
            TableCellStyleDatetime = new DataGridViewCellStyle();

            ByDate = false;
            DataLoaded = false;
            DataLoading = false;
            _font = new Font("Calibri", 8.25f);
            _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);


            this.mainForm = MainForm;
            this.Parent = mainForm.pAreasView;
            dcp = mainForm.dgAreasPane;
            this.Dock = DockStyle.None;
            this.Anchor = (AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top);
            this.Left = Parent.ClientRectangle.X;
            this.Top = Parent.ClientRectangle.Y + 26;
            this.Width = Parent.Width;
            this.Height = Parent.Height - 26;
            imgList = new ImageList();
            imgList.Images.Add(Properties.Resources.Zoom_In16);
            imgList.Images.Add(Properties.Resources.Zoom_Out16);
            imgList.Images.Add(Properties.Resources.table_line_dot);
            imgList.Images.Add(Properties.Resources.table_angle_dot);

            _dsAreas = new DataSet("dsAreas");
            table = new DataTable("AREAS");

            table.Columns.Add("OILFIELD_INDEX", Type.GetType("System.Int32"));      // 0
            table.Columns.Add("AREA_INDEX", Type.GetType("System.Int32"));          // 1
            table.Columns.Add("IS_OF_TOTAL", Type.GetType("System.Int32"));         // 2

            table.Columns.Add("OILFIELD", Type.GetType("System.String"));           // 3
            table.Columns.Add("OILFIELD_AREA", Type.GetType("System.String"));      // 4
            table.Columns.Add("AREANAME", Type.GetType("System.String"));           // 5
            table.Columns.Add("OILOBJECT", Type.GetType("System.String"));          // 6

            table.Columns.Add("WELL_COUNT_ALL", Type.GetType("System.Int32"));      // 7
            table.Columns.Add("WELL_COUNT_WORK", Type.GetType("System.Int32"));     // 8
            table.Columns.Add("LIQ", Type.GetType("System.Double"));                // 9
            table.Columns.Add("OIL", Type.GetType("System.Double"));                // 10
            table.Columns.Add("INJ", Type.GetType("System.Double"));                // 11
            table.Columns.Add("CURR_COMP", Type.GetType("System.Double"));          // 12

            table.Columns.Add("ACC_WELL_COUNT_WORK", Type.GetType("System.Int32")); // 13
            table.Columns.Add("ACC_LIQ", Type.GetType("System.Double"));            // 14
            table.Columns.Add("ACC_OIL", Type.GetType("System.Double"));            // 15
            table.Columns.Add("ACC_INJ", Type.GetType("System.Double"));            // 16
            table.Columns.Add("ACC_COMP", Type.GetType("System.Double"));           // 17

            table.Columns.Add("VOIL", Type.GetType("System.Double"));               // 18
            table.Columns.Add("VWAT", Type.GetType("System.Double"));               // 19
            table.Columns.Add("INIT_PRESSURE", Type.GetType("System.Double"));      // 20
            table.Columns.Add("OIL_DENSITY", Type.GetType("System.Double"));        // 21
            table.Columns.Add("OIL_VISCOSITY", Type.GetType("System.Double"));      // 22
            table.Columns.Add("DISPL_EFF", Type.GetType("System.Double"));          // 23
            table.Columns.Add("POROSITY", Type.GetType("System.Double"));           // 24
            table.Columns.Add("OIL_INIT_SAT", Type.GetType("System.Double"));       // 25
            table.Columns.Add("SAT_PRESSURE", Type.GetType("System.Double"));       // 26
            table.Columns.Add("WAT_DENSITY", Type.GetType("System.Double"));        // 27
            table.Columns.Add("WAT_VISCOSITY", Type.GetType("System.Double"));      // 28
            table.Columns.Add("PERMEABILITY", Type.GetType("System.Double"));       // 29
            table.Columns.Add("COMMENT", Type.GetType("System.String"));            // 30
            table.Columns.Add("DATE", Type.GetType("System.DateTime"));             // 31
            table.Columns.Add("PRESSURE", Type.GetType("System.Double"));           // 32
            table.Columns.Add("BALANS_RESERV", Type.GetType("System.Double"));      // 33
            table.Columns.Add("INIT_IZ", Type.GetType("System.Double"));            // 34
            table.Columns.Add("OIZ", Type.GetType("System.Double"));                // 35
            table.Columns.Add("NGDU", Type.GetType("System.String"));               // 36
            table.Columns.Add("WATER_CURRENT", Type.GetType("System.Double"));      // 37
            table.Columns.Add("WATER_ACCUM", Type.GetType("System.Double"));        // 38

            _dsAreas.Tables.Add(table);
            this.DataSource = _dsAreas.Tables[0];
            TableCellStyleFloat.Format = "0.##";
            TableCellStyleFloat.Font = _font;

            TableCellStyleFloatPVT.Format = "0.###";
            TableCellStyleFloatPVT.Font = _font;

            TableCellStyleAll.Font = _font;

            TableCellStylePercent.Format = "0.#";
            TableCellStylePercent.Font = _font_bold;

            TableCellStyleDatetime.Format = "dd.MM.yyyy";
            TableCellStyleDatetime.Font = _font;
            SetHeaderStyle();

            // Настройка таблицы Ячейки
            this.RowTemplate.Height = 16;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = true;
            this.DoubleBuffered = true;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            this.BackgroundColor = SystemColors.ControlDark;

            this.Resize += new EventHandler(DataGridMer_Resize);
            this.Paint += new PaintEventHandler(DataGridMer_Paint); // перерисовка окна
            this.ColumnWidthChanged += new DataGridViewColumnEventHandler(DataGridMer_ColumnWidthChanged);
            this.DoubleClick += new EventHandler(DataGridAreas_DoubleClick);
            this.CellClick += new DataGridViewCellEventHandler(DataGridAreas_CellClick);
            this.KeyDown += new KeyEventHandler(DataGridAreas_KeyDown);
            this.MouseMove += new MouseEventHandler(DataGridAreas_MouseMove);
            this.MouseDown += new MouseEventHandler(DataGridAreas_MouseDown);
        }

        void DataGridAreas_MouseDown(object sender, MouseEventArgs e)
        {
            // mainForm.StatUsage.AddMessage(CollectorStatId.AREAS_TABLE_MOUSE_DOWN);
        }
        void DataGridAreas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    copySelectedRowsToClipboard((DataGridView)sender);
                }              
        }
        private void copySelectedRowsToClipboard(DataGridView dgv)
        {
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            Clipboard.Clear();
            if (dgv.GetClipboardContent() != null)
            {
                Clipboard.SetDataObject(dgv.GetClipboardContent());
                Clipboard.GetData(DataFormats.Text);
                IDataObject dt = Clipboard.GetDataObject();
                if (dt.GetDataPresent(typeof(string)))
                {
                    string tb = (string)(dt.GetData(typeof(string)));
                    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                    byte[] dataStr = encoding.GetBytes(tb);
                    Clipboard.SetDataObject(encoding.GetString(dataStr));
                }
            }
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
        }

        void DataGridMer_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            //mainForm.reLog.AppendText(e.Column.Width.ToString() + Environment.NewLine);
        }
        public void SetProject(Project aProject)
        {
            _proj = aProject;
        }
        public void ClearProject() { this._proj = null; ClearTable(); }
        void DataGridAreas_MouseMove(object sender, MouseEventArgs e)
        {
            if ((!mainForm.UserDialogShowing) && (mainForm.IsActivated) && 
                (dcp != null) && (dcp.DockedState == Infragistics.Win.UltraWinDock.DockedState.Docked)) 
                this.Focus();
        }
        void DataGridMer_Resize(object sender, EventArgs e)
        {
            SetHeaderStyle();
        }
        void DataGridMer_Paint(object sender, PaintEventArgs e)
        {
            //if ((!DataLoaded) && (!DataLoading))
            //{
            //    System.Drawing.Graphics gr = e.Graphics;
            //    gr.DrawString("Нет данных для отображения", _font, Brushes.Black, this.Left + (int)(this.Width / 2), this.Top + (int)(this.Height / 2));
            //}
        }
        void DataGridAreas_DoubleClick(object sender, EventArgs e)
        {
            if ((this.Rows.Count > 0) && (this.CurrentCell.ColumnIndex > 2) && (this.CurrentCell.ColumnIndex < table.Columns.Count))
            {
                int ofIndex, areaIndex;
                DataGridViewRow row = this.CurrentRow;
                ofIndex = (int)row.Cells[0].Value;
                areaIndex = (int)row.Cells[1].Value;
                if (areaIndex > -1)
                {
                    mainForm.canv.SetCenterArea(ofIndex, areaIndex, true);   
                }
                else
                {
                    mainForm.canv.ClearSelectedList();
                    mainForm.canv.SetCenterOilField(ofIndex, true);
                    OilField of = _proj.OilFields[ofIndex];

                    mainForm.dChartSetWindowText(String.Format("Графики - Месторождение {0} [{1}]", of.Name, of.ParamsDict.NGDUName));

                    mainForm.filterOilObj.SetOilFieldOilObj(_proj.OilFields[ofIndex], mainForm.canv.selLayerList.Count == 0);
                    mainForm.filterOilObj.UpdateCurrentTreeView();
                    //mainForm.canv.AddSumOilField(cntr);

                    //
                    //mainForm.canv.SetActiveOilField(ofIndex);

                    //mainForm.canv.SetCenterOilField(ofIndex);
                    ////mainForm.canv.
                    //mainForm.canv.AddSelectLayer(layer);
                    //mainForm.canv.DrawLayerList();
                }
            }
        }
        void DataGridAreas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == table.Columns.Count) && (e.RowIndex > -1))
            {
                bool isEmtpy = true;
                if (((int)this.Rows[e.RowIndex].Cells[table.Columns.Count + 1].Value == 0) && ((int)this.Rows[e.RowIndex].Cells[1].Value == -1))
                {
                    int i = e.RowIndex + 1;
                    int lastTotal = -1;
                    while ((i < this.Rows.Count) && 
                        !(((int)this.Rows[i].Cells[1].Value == -1) && ((int)this.Rows[i].Cells[2].Value == 1)))
                    {
                        if ((int)this.Rows[i].Cells[2].Value == 1)
                        {
                            this.Rows[i].Visible = true;
                            lastTotal = i;
                        }
                        isEmtpy = false;
                        i++;
                    }
                    if (lastTotal < this.Rows.Count) 
                    {
                        if ((i == this.Rows.Count) || (((int)this.Rows[i].Cells[1].Value == -1) && ((int)this.Rows[i].Cells[2].Value == 1)))
                        {
                            if (lastTotal != -1) this.Rows[lastTotal].Cells[table.Columns.Count].Value = imgList.Images[3];
                        }
                    }
                    if (!isEmtpy)
                    {
                        this.Rows[e.RowIndex].Cells[table.Columns.Count].Value = imgList.Images[1];
                        this.Rows[e.RowIndex].Cells[table.Columns.Count + 1].Value = 1;
                    }
                }
                else if ((int)this.Rows[e.RowIndex].Cells[1].Value == -1)
                {
                    int i = e.RowIndex + 1;
                    while ((i < this.Rows.Count) && 
                        !(((int)this.Rows[i].Cells[1].Value == -1) && ((int)this.Rows[i].Cells[2].Value == 1)))
                    {
                        this.Rows[i].Visible = false;
                        isEmtpy = false;
                        i++;
                    }
                    if (!isEmtpy)
                    {
                        this.Rows[e.RowIndex].Cells[table.Columns.Count].Value = imgList.Images[0];
                        this.Rows[e.RowIndex].Cells[table.Columns.Count + 1].Value = 0;
                    }
                }
            }
        }

        public void ClearTable()
        {
            DataLoading = true;
            table.Clear();
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();
            DataLoaded = false;
            DataLoading = false;
            this.BackgroundColor = SystemColors.ControlDark;
        }
        public void ClearDataSet()
        {
            if (this.Columns.Count > table.Columns.Count)
            {
                this.Columns.RemoveAt(table.Columns.Count);
                this.Columns.RemoveAt(table.Columns.Count);
                //this.Columns.RemoveAt(31);
                //this.Columns.RemoveAt(31);
            }
            this.DataSource = null;
        }

        void SetHeaderStyle()
        {
            if (!DataLoading)
            {
                for (int i = 3; i < this.Columns.Count; i++)
                {
                    if (i == table.Columns.Count) break;
                    if ((i == 12) || (i == 17)) this.Columns[i].DefaultCellStyle = TableCellStylePercent;
                    else if ((i > 8 && i < 18) || /*(i >= 32 && i <= 35) ||*/ i == 37 || i == 38) this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    else if (i > 17 && i < 30) this.Columns[i].DefaultCellStyle = TableCellStyleFloatPVT;
                    else if (i == 31) this.Columns[i].DefaultCellStyle = TableCellStyleDatetime;
                    else this.Columns[i].DefaultCellStyle = TableCellStyleAll;

                    this.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    this.Columns[i].HeaderText = tableHeaderText[i - 3];
                }
                this.Columns[0].Visible = false;
                this.Columns[1].Visible = false;
                this.Columns[2].Visible = false;
                this.Columns[31].Visible = ByDate;
                this.Columns[36].DisplayIndex = 1;
                this.Columns[33].DisplayIndex = 8;
                this.Columns[34].DisplayIndex = 9;
                this.Columns[35].DisplayIndex = 10;
                this.Columns[31].DisplayIndex = 8;
                this.Columns[32].DisplayIndex = 26;
                this.Columns[37].DisplayIndex = 16;
                this.Columns[38].DisplayIndex = 22;                

                
                HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
                HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
                HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
                HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
                this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;
            }
        }

        public void SetDataSource()
        {
            this.Visible = false;
            this.DataSource = _dsAreas.Tables[0];
            SetHeaderStyle();
            SetCellsStyle();
            if(DataLoaded) this.Visible = true;
            this.BackgroundColor = Color.White;
        }
        public void SetCellsStyle()
        {
            DataGridViewImageColumn col = new DataGridViewImageColumn();
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ImageLayout = DataGridViewImageCellLayout.NotSet;
            col.DisplayIndex = 0;
            col.Width = 24;
            col.HeaderText = "";
            this.Columns.Add(col);

            DataGridViewTextBoxColumn col2 = new DataGridViewTextBoxColumn();
            col2.Visible = false;
            this.Columns.Add(col2);

            for (int i = 0; i < this.Rows.Count; i++)
            {
                this.Rows[i].Cells[12].Style.BackColor = Color.SkyBlue;
                this.Rows[i].Cells[17].Style.BackColor = Color.SkyBlue;

                if (((int)this.Rows[i].Cells[1].Value == -1) && ((int)this.Rows[i].Cells[2].Value == 1))
                {
                    this.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                    this.Rows[i].Cells[table.Columns.Count].Value = imgList.Images[0];
                    this.Rows[i].Cells[table.Columns.Count + 1].Value = 0;
                }
                else
                {
                    if ((int)this.Rows[i].Cells[2].Value == 1)
                    {
                        this.Rows[i].DefaultCellStyle.BackColor = Color.LightGray;
                    }
                    if ((i < this.Rows.Count - 1) &&
                        !(((int)this.Rows[i + 1].Cells[1].Value == -1) && ((int)this.Rows[i + 1].Cells[2].Value == 1)))
                        this.Rows[i].Cells[table.Columns.Count].Value = imgList.Images[2];
                    else
                        this.Rows[i].Cells[table.Columns.Count].Value = imgList.Images[3];
                    this.Rows[i].Visible = false;
                }
            }
        }

        public void SelectAreaRow(int OilFieldIndex, int AreaIndex)
        {
            if (this.Rows.Count > 0)
            {
                int k = -1;
                for (int i = 0; i < this.Rows.Count; i++)
                {
                    this.Rows[i].Selected = false;
                    if (((int)this.Rows[i].Cells[0].Value == OilFieldIndex) &&
                        ((int)this.Rows[i].Cells[1].Value == AreaIndex) &&
                        ((int)this.Rows[i].Cells[2].Value == 1))
                    {
                        this.Rows[i].Selected = true;
                        k = i;
                    }
                }
                if (k > -1)
                {
                    int count = this.DisplayedRowCount(false), dx, lastDx = -1;
                    dx = k;
                    count = (int)(count / 2);
                    while ((dx > 0) && (count > 0))
                    {
                        if (this.Rows[dx].Visible)
                        {
                            count--;
                            lastDx = dx;
                        }
                        dx--;
                    }
                    if (lastDx < 0) lastDx = 0;
                    if(this.ClientRectangle.Height > 15) this.FirstDisplayedScrollingRowIndex = lastDx;
                }
            }
        }
        private class AveOFElement
        {
            public int OilObjCode;
            public int WellCount;
            public int AccWellCount;
            public double SumLiqPl;
            public double sumLiq;
            public double sumOil;
            public double sumInj;
            public double AccLiq;
            public double AccOil;
            public double AccInj;
            public PVTParamsItem pvt;

            public AveOFElement()
            {
                WellCount = 0;
                AccWellCount = 0;
                sumInj = 0;
                sumLiq = 0;
                sumOil = 0;
                AccLiq = 0;
                AccOil = 0;
                AccInj = 0;
                pvt = PVTParamsItem.Empty;
                pvt.DisplacementEfficiency = 0;
                pvt.GasFactor = 0;
                pvt.InitialPressure = 0;
                pvt.OilDensity = 0;
                pvt.OilFieldAreaCode = 0;
                pvt.OilInitialSaturation = 0;
                pvt.StratumCode = 0;
                pvt.OilViscosity = 0;
                pvt.OilVolumeFactor = 0;
                pvt.Permeability = 0;
                pvt.Porosity = 0;
                pvt.SaturationPressure = 0;
                pvt.WaterDensity = 0;
                pvt.WaterViscosity = 0;
                pvt.WaterVolumeFactor = 0;
                pvt.Comment = null;
            }
        }
        private class AveOFList
        {
            ArrayList list;
            public int AllWellCount;
            public int Count { get { return list.Count; } }

            public AveOFList()
            {
                list = new ArrayList();
                AllWellCount = 0;
            }
            public AveOFElement this[int index]
            {
                get { return (AveOFElement)list[index]; }
            }
            public AveOFElement GetItemByCode(int OilObjCode)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if(OilObjCode == ((AveOFElement)list[i]).OilObjCode)
                        return (AveOFElement)list[i];
                }
                return null;
            }
            public AveOFElement Add(int OilObjCode)
            {
                AveOFElement aveItem = new AveOFElement();
                aveItem.OilObjCode = OilObjCode;
                list.Add(aveItem);
                return aveItem;
            }
            
            public void Clear()
            {
                list.Clear();
                AllWellCount = 0;
            }
        }

        public double GetAveragePressure(Area area, StratumDictionary dict, DateTime date)
        {
            double lastPressure = 0;
            if ((area.WellList != null) && (area.WellList.Length > 0))
            {
                double sumPress = 0, lastPress;
                int countPress = 0;
                StratumTreeNode node;
                DateTime date1 = date.AddMonths(-6), date2 = date.AddMonths(1);
                for (int i = 0; i < area.WellList.Length; i++)
                {
                    if (area.WellList[i].research != null)
                    {
                        WellResearchItem rItem;
                        lastPress = -1;
                        for (int j = 0; j < area.WellList[i].research.Count; j++)
                        {
                            rItem = area.WellList[i].research[j];
                            if ((rItem.PressPerforation > 0) &&
                                ((rItem.ResearchCode == 2 && rItem.TimeDelay > 0) || (rItem.ResearchCode == 4) || (rItem.ResearchCode == 6)))
                            {
                                if ((rItem.Date >= date1) && (rItem.Date < date2))
                                {
                                    if (rItem.PlastCode == area.PlastCode)
                                    {
                                        lastPress = rItem.PressPerforation;
                                    }
                                    else
                                    {
                                        node = dict.GetStratumTreeNode(area.PlastCode);
                                        if ((node != null) && (node.GetParentLevelByCode(area.PlastCode) > -1))
                                        {
                                            lastPress = rItem.PressPerforation;
                                        }
                                    }
                                }
                            }
                        }
                        if (lastPress != -1)
                        {
                            sumPress += lastPress;
                            countPress++;
                        }
                    }
                }
                if (countPress > 0) lastPressure = sumPress / countPress;
            }
            return lastPressure;
        }

        public void ShowAreas(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, j, k;
            DataRow row;
            var dict = (OilFieldAreaDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            var dictPlast = (StratumDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            DataLoading = true;
            DataLoaded = false;
            this.ByDate = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Расчет параметров ячеек заводнения";
            
            this.BackgroundColor = Color.White;
            table.Clear();
            string OFAreaName;
            SumParamItem[] SumParams = null;
            PVTParamsItem pvtItem;
            OilField of;
            Area area;
            int len;

            double sumLiq, sumOil, sumInj, sumInjGas;
            int wellCount;
            calcDate = DateTime.MinValue;
            lastDateMer = DateTime.MinValue;
            if (worker != null) worker.ReportProgress(0, userState);
            for (i = 1; i < _proj.OilFields.Count; i++)
            {
                of = _proj.OilFields[i];
                if (of.Areas == null) continue;
#if DEBUG
                //if (of.ParamsDict.NGDUName != "Ишимбайнефть") continue;
                //if (of.Name != "УРШАКСКОЕ") continue;
#endif
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    DataLoaded = false;
                    table.Clear();
                    calcDate = DateTime.MinValue;
                    lastDateMer = DateTime.MinValue;
                    return;
                }
                if (!of.LoadSumAreaFromCache(worker, e))
                {
                    of.ReCalcSumAreaData(worker, e);
                }
                if (!of.LoadStratumCodesFromCache())
                {
                    of.RecalcStratumCodes(worker, e);
                }
                if (!of.WellResearchLoaded)
                {
                    of.LoadWellResearchFromCache(worker, e);
                }
                if (of.Areas.Count > 0)
                {
                    row = table.NewRow();
                    row[0] = i;
                    row[1] = -1;
                    row[2] = 1;
                    row[3] = of.Name;
                    row[36] = of.ParamsDict.NGDUName;
                    table.Rows.Add(row);
                }
                OFAreaName = string.Empty;
                userState.Element = of.Name;
                for (j = 0; j < of.Areas.Count; j++)
                {
                    area = (Area)of.Areas[j];
#if !DEBUG
                    if ((area.sumObjParams == null) || (area.sumObjParams.MonthlyItems == null)) continue;
#endif

                    row = table.NewRow();
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        DataLoaded = false;
                        table.Clear();
                        calcDate = DateTime.MinValue;
                        lastDateMer = DateTime.MinValue;
                        return;
                    }
                    row[3] = of.Name;
                    row[36] = of.ParamsDict.NGDUName;
                    OFAreaName = dict.GetShortNameByCode(area.OilFieldAreaCode);
                    if (OFAreaName.Length == 0) OFAreaName = "Нет данных";
                    row[4] = OFAreaName;
                    row[5] = area.Name;
                    row[6] = dictPlast.GetShortNameByCode(area.PlastCode);

                    wellCount = ((area.WellList == null) ? 0 : area.WellList.Length);
                    string[] list = new string[wellCount];
                    for (int ii = 0; ii < wellCount; ii++) list[ii] = area.WellList[ii].Name;
                    row[7] = wellCount;

                    row[8] = 0;
                    row[9] = 0;
                    row[10] = 0;
                    row[11] = 0;
                    row[13] = 0;
                    sumLiq = 0;
                    sumOil = 0;
                    sumInj = 0;
                    sumInjGas = 0;

                    SumParams = null;
                    len = 0;
                    if (area.sumObjParams != null && area.sumObjParams.MonthlyItems != null)
                    {
                        SumParams = area.sumObjParams.MonthlyItems;

                        len = SumParams.Length;
                        for (k = 0; k < len; k++)
                        {
                            sumLiq += SumParams[k].Liq;
                            sumOil += SumParams[k].Oil;
                            if (SumParams[k].Inj > 0) sumInj += SumParams[k].Inj;
                            if (SumParams[k].InjGas > 0) sumInjGas += SumParams[k].InjGas;
                        }

                        if (len > 0)
                        {
                            row[8] = SumParams[len - 1].Fprod + SumParams[len - 1].Finj;
                            row[9] = SumParams[len - 1].Liq;
                            row[10] = SumParams[len - 1].Oil;
                            if (SumParams[len - 1].Liq > 0)
                            {
                                row[37] = (SumParams[len - 1].Liq - SumParams[len - 1].Oil) * 100.0 / SumParams[len - 1].Liq;
                            }
                            else
                            {
                                row[37] = 0;
                            }
                            row[11] = SumParams[len - 1].Inj;
                            row[13] = SumParams[len - 1].LeadInAll;
                            if (lastDateMer < SumParams[len - 1].Date) lastDateMer = SumParams[len - 1].Date;
                        }
                    }
                    row[14] = sumLiq;
                    row[15] = sumOil;
                    row[16] = sumInj;
                    if (sumLiq > 0)
                    {
                        row[38] = (sumLiq - sumOil) * 100.0 / sumLiq;
                    }
                    else
                    {
                        row[38] = 0;
                    }

                    row[0] = i;
                    row[1] = j;
                    row[2] = 1;

                    // расчет давлений
                    if (SumParams != null)
                    {
                        row[31] = SumParams[len - 1].Date;
                        row[32] = GetAveragePressure(area, dictPlast, SumParams[len - 1].Date);
                    }
                    else
                    {
                        row[31] = new DateTime();
                        row[32] = 0;
                    }
                    row[33] = area.Params.InitOilReserv / 1000;
                    row[34] = area.Params.InitOilReserv * area.Params.KIN / 1000;
                    row[35] = (area.Params.InitOilReserv * area.Params.KIN - sumOil) / 1000;

                    pvtItem = PVTParamsItem.Empty;

                    if (!area.pvt.IsEmpty)
                    {
                        pvtItem = area.pvt;
                    }
                    else if ((of.PVTList != null) && (of.PVTList.Count > 0))
                    {
                        pvtItem = of.PVTList.GetItemByPlastHierarchy(area.OilFieldAreaCode, area.PlastCode, dictPlast);
                    }
                    if (pvtItem.IsEmpty)
                    {
                        if (!area.pvt.IsEmpty)
                        {
                            pvtItem = area.pvt;
                        }
                        else if ((of.PVTList != null) && (of.PVTList.Count > 0))
                        {
                            pvtItem = of.PVTList.GetItemByPlastHierarchy(-1, area.PlastCode, dictPlast);
                        }
                        if ((!pvtItem.IsEmpty) && (pvtItem.Comment.IndexOf("ALL") == -1))
                        {
                            pvtItem.Comment += ";PVT взяты с площади ALL";
                        }
                    }
                    if (!pvtItem.IsEmpty)
                    {
                        row[18] = (!pvtItem.IsEmpty) ? pvtItem.OilVolumeFactor : 0;
                        row[19] = (!pvtItem.IsEmpty) ? pvtItem.WaterVolumeFactor : 0;
                        row[20] = (!pvtItem.IsEmpty) ? pvtItem.InitialPressure : 0;
                        row[21] = (!pvtItem.IsEmpty) ? pvtItem.OilDensity : 0;
                        row[22] = (!pvtItem.IsEmpty) ? pvtItem.OilViscosity : 0;
                        row[23] = (!pvtItem.IsEmpty) ? pvtItem.DisplacementEfficiency : 0;
                        row[24] = (!pvtItem.IsEmpty) ? pvtItem.Porosity : 0;
                        row[25] = (!pvtItem.IsEmpty) ? pvtItem.OilInitialSaturation : 0;
                        row[26] = (!pvtItem.IsEmpty) ? pvtItem.SaturationPressure : 0;
                        row[27] = (!pvtItem.IsEmpty) ? pvtItem.WaterDensity : 0;
                        row[28] = (!pvtItem.IsEmpty) ? pvtItem.WaterViscosity : 0;
                        row[29] = (!pvtItem.IsEmpty) ? pvtItem.Permeability : 0;
                        row[30] = (!pvtItem.IsEmpty) ? pvtItem.Comment : string.Empty;
                        if (pvtItem.WaterDensity + pvtItem.OilDensity > 0)
                        {
                            row[12] = 100 * ((double)row[11] * pvtItem.WaterVolumeFactor) / (((double)row[9] - (double)row[10]) * pvtItem.WaterVolumeFactor / pvtItem.WaterDensity + ((double)row[10] * pvtItem.OilVolumeFactor) / pvtItem.OilDensity);
                            row[17] = 100 * ((double)row[16] * pvtItem.WaterVolumeFactor) / (((double)row[14] - (double)row[15]) * pvtItem.WaterVolumeFactor / pvtItem.WaterDensity + ((double)row[15] * pvtItem.OilVolumeFactor) / pvtItem.OilDensity);
                            if (((double)row[9] + (double)row[10]) == 0) row[12] = 0;
                            if (((double)row[14] + (double)row[15]) == 0) row[17] = 0;
                        }
                        else
                        {
                            row[12] = 0;
                            row[17] = 0;
                        }
                    }
                    row[30] = pvtItem.KIN.ToString();
                    table.Rows.Add(row);
                }
                if (!DataLoaded) DataLoaded = true;
                calcDate = DateTime.Now;
                SumParams = null;
                of.ClearWellResearchData(false);
                of.ClearSumArea(true);
                if (worker != null) worker.ReportProgress(100, userState);
            }
            this.DataLoading = false;
        }
        public void ShowAreasByDate(BackgroundWorker worker, DoWorkEventArgs e, DateTime DateStart, DateTime DateEnd)
        {
            int i, j, k;
            DataRow row;
            var dict = (OilFieldAreaDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            var dictPlast = (StratumDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            DataLoading = true;
            DataLoaded = false;
            this.ByDate = true;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Расчет параметров ячеек заводнения по датам";

            this.BackgroundColor = Color.White;
            table.Clear();
            string OFAreaName;
            SumParamItem[] SumParams;
            PVTParamsItem pvtItem;
            OilField of;
            Area area;
            int len;

            double sumLiq, sumOil, sumInj, sumInjGas;
            int wellCount;
            calcDate = DateTime.MinValue;
            lastDateMer = DateTime.MinValue;
            if (worker != null) worker.ReportProgress(0, userState);
            for (i = 1; i < _proj.OilFields.Count; i++)
            {
                of = _proj.OilFields[i];
                if (of.Areas == null) continue;
#if DEBUG
                //if (of.Name != "УРШАКСКОЕ") continue;
#endif
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    DataLoaded = false;
                    table.Clear();
                    calcDate = DateTime.MinValue;
                    lastDateMer = DateTime.MinValue;
                    return;
                }
                if (!of.LoadSumAreaFromCache(worker, e)) of.ReCalcSumAreaData(worker, e);
                if (!of.LoadStratumCodesFromCache()) of.RecalcStratumCodes(worker, e);
                if (!of.WellResearchLoaded) of.LoadWellResearchFromCache(worker, e);

                if (of.Areas.Count > 0)
                {
                    row = table.NewRow();
                    row[0] = i;
                    row[1] = -1;
                    row[2] = 1;
                    row[3] = of.Name;
                    row[36] = of.ParamsDict.NGDUName;
                    table.Rows.Add(row);
                }
                OFAreaName = string.Empty;
                userState.Element = of.Name;
                DateTime dt;
                int ind;
                for (j = 0; j < of.Areas.Count; j++)
                {
                    area = (Area)of.Areas[j];
                    if ((area.sumObjParams == null) || (area.sumObjParams.MonthlyItems == null)) continue;
                    dt = DateStart;
                    SumParams = area.sumObjParams.MonthlyItems;
                    if (SumParams.Length > 0)
                    {
                        if (dt < SumParams[0].Date) dt = SumParams[0].Date;
                        pvtItem = PVTParamsItem.Empty;
                        if (!area.pvt.IsEmpty)
                        {
                            pvtItem = area.pvt;
                        }
                        else if ((of.PVTList != null) && (of.PVTList.Count > 0))
                        {
                            pvtItem = of.PVTList.GetItemByPlastHierarchy(area.OilFieldAreaCode, area.PlastCode, dictPlast);
                        }


                        while (dt <= DateEnd)
                        {
                            row = table.NewRow();
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                DataLoaded = false;
                                table.Clear();
                                calcDate = DateTime.MinValue;
                                lastDateMer = DateTime.MinValue;
                                return;
                            }
                            row[3] = of.Name;
                            row[36] = of.ParamsDict.NGDUName;
                            OFAreaName = dict.GetShortNameByCode(area.OilFieldAreaCode);
                            if (OFAreaName.Length == 0) OFAreaName = "Нет данных";
                            row[4] = OFAreaName;
                            row[5] = area.Name;
                            row[6] = dictPlast.GetShortNameByCode(area.PlastCode);
                            row[31] = dt;
                            wellCount = ((area.WellList == null) ? 0 : area.WellList.Length);
                            row[7] = wellCount;


                            sumLiq = 0;
                            sumOil = 0;
                            sumInj = 0;
                            sumInjGas = 0;
                            len = SumParams.Length;
                            for (k = 0; k < len; k++)
                            {
                                if (SumParams[k].Date <= dt)
                                {
                                    sumLiq += SumParams[k].Liq;
                                    sumOil += SumParams[k].Oil;
                                    if (SumParams[k].Inj > 0) sumInj += SumParams[k].Inj;
                                    if (SumParams[k].InjGas > 0) sumInjGas += SumParams[k].InjGas;
                                }
                            }
                            row[8] = 0;
                            row[9] = 0;
                            row[10] = 0;
                            row[11] = 0;
                            row[13] = 0;
                            if (len > 0)
                            {
                                ind = len - 1;
                                while ((ind >= 0) && (SumParams[ind].Date != dt)) ind--;
                                if (ind > -1)
                                {
                                    row[8] = SumParams[ind].Fprod + SumParams[ind].Finj;
                                    row[9] = SumParams[ind].Liq;
                                    row[10] = SumParams[ind].Oil;
                                    row[11] = SumParams[ind].Inj;
                                    if (SumParams[ind].Liq > 0)
                                    {
                                        row[37] = (SumParams[ind].Liq - SumParams[ind].Oil) * 100.0 / SumParams[ind].Liq;
                                    }
                                    else
                                    {
                                        row[37] = 0;
                                    }
                                    row[13] = SumParams[ind].LeadInAll;
                                }
                                if (lastDateMer < SumParams[len - 1].Date) lastDateMer = SumParams[len - 1].Date;
                            }
                            row[14] = sumLiq;
                            row[15] = sumOil;
                            row[16] = sumInj;
                            if (sumLiq > 0)
                            {
                                row[38] = (sumLiq - sumOil) * 100.0 / sumLiq;
                            }
                            else
                            {
                                row[38] = 0;
                            }
                            row[0] = i;
                            row[1] = j;
                            row[2] = 1;
                            // расчет давлений
                            row[32] = GetAveragePressure(area, dictPlast, dt);
                            row[33] = area.Params.InitOilReserv / 1000;
                            row[34] = area.Params.InitOilReserv * area.Params.KIN / 1000;
                            row[35] = (area.Params.InitOilReserv * area.Params.KIN - sumOil) / 1000;

                            if (!pvtItem.IsEmpty)
                            {
                                row[18] = (!pvtItem.IsEmpty) ? pvtItem.OilVolumeFactor : 0;
                                row[19] = (!pvtItem.IsEmpty) ? pvtItem.WaterVolumeFactor : 0;
                                row[20] = (!pvtItem.IsEmpty) ? pvtItem.InitialPressure : 0;
                                row[21] = (!pvtItem.IsEmpty) ? pvtItem.OilDensity : 0;
                                row[22] = (!pvtItem.IsEmpty) ? pvtItem.OilViscosity : 0;
                                row[23] = (!pvtItem.IsEmpty) ? pvtItem.DisplacementEfficiency : 0;
                                row[24] = (!pvtItem.IsEmpty) ? pvtItem.Porosity : 0;
                                row[25] = (!pvtItem.IsEmpty) ? pvtItem.OilInitialSaturation : 0;
                                row[26] = (!pvtItem.IsEmpty) ? pvtItem.SaturationPressure : 0;
                                row[27] = (!pvtItem.IsEmpty) ? pvtItem.WaterDensity : 0;
                                row[28] = (!pvtItem.IsEmpty) ? pvtItem.WaterViscosity : 0;
                                row[29] = (!pvtItem.IsEmpty) ? pvtItem.Permeability : 0;
                                row[30] = (!pvtItem.IsEmpty) ? pvtItem.Comment : string.Empty;
                                if (pvtItem.WaterDensity + pvtItem.OilDensity > 0)
                                {
                                    row[12] = 100 * ((double)row[11] * pvtItem.WaterVolumeFactor) / (((double)row[9] - (double)row[10]) * pvtItem.WaterVolumeFactor / pvtItem.WaterDensity + ((double)row[10] * pvtItem.OilVolumeFactor) / pvtItem.OilDensity);
                                    row[17] = 100 * ((double)row[16] * pvtItem.WaterVolumeFactor) / (((double)row[14] - (double)row[15]) * pvtItem.WaterVolumeFactor / pvtItem.WaterDensity + ((double)row[15] * pvtItem.OilVolumeFactor) / pvtItem.OilDensity);
                                    if (((double)row[9] + (double)row[10]) == 0) row[12] = 0;
                                    if (((double)row[14] + (double)row[15]) == 0) row[17] = 0;
                                }
                                else
                                {
                                    row[12] = 0;
                                    row[17] = 0;
                                }
                            }
                            table.Rows.Add(row);
                            dt = dt.AddMonths(1);
                        } 
                    }
                }
                if (!DataLoaded) DataLoaded = true;
                calcDate = DateTime.Now;
                SumParams = null;
                of.ClearWellResearchData(false);
                of.ClearSumArea(true);
                if (worker != null) worker.ReportProgress(100, userState);
            }
            this.DataLoading = false;
        }
        
        public void LoadAreasParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            DataLoading = true;
            DataLoaded = false;
            table.Clear();
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка параметров ячеек заводнения из кэша";
            userState.Element = "";
            worker.ReportProgress(0, userState);
            int i;

            DataRow row;
            string cacheName = _proj.path + "\\cache\\areas_params.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader file = new BinaryReader(fs);
                    table.Clear();
                    int rowNum;
                    lastDateMer = DateTime.MinValue;
                    calcDate = DateTime.MinValue;
                    this.calcDate = DateTime.FromOADate(file.ReadDouble());
                    this.lastDateMer = DateTime.FromOADate(file.ReadDouble());
                    rowNum = file.ReadInt32();

                    for (i = 0; i < rowNum; i++)
                    {
                        row = table.NewRow();
                        row[0] = file.ReadInt32();
                        row[1] = file.ReadInt32();
                        row[2] = file.ReadInt32();
                        row[3] = file.ReadString();
                        if ((int)row[1] != -1)
                        {
                            row[4] = file.ReadString();
                            row[5] = file.ReadString();
                            row[6] = file.ReadString();

                            row[7] = file.ReadInt32();
                            row[8] = file.ReadInt32();
                            row[9] = file.ReadDouble();
                            row[10] = file.ReadDouble();
                            row[11] = file.ReadDouble();
                            row[12] = file.ReadDouble();

                            row[13] = file.ReadInt32();
                            row[14] = file.ReadDouble();
                            row[15] = file.ReadDouble();
                            row[16] = file.ReadDouble();
                            row[17] = file.ReadDouble();

                            row[18] = file.ReadDouble();
                            row[19] = file.ReadDouble();
                            row[20] = file.ReadDouble();
                            row[21] = file.ReadDouble();
                            row[22] = file.ReadDouble();
                            row[23] = file.ReadDouble();
                            row[24] = file.ReadDouble();
                            row[25] = file.ReadDouble();
                            row[26] = file.ReadDouble();
                            row[27] = file.ReadDouble();
                            row[28] = file.ReadDouble();
                            row[29] = file.ReadDouble();
                            row[30] = file.ReadString();
                            row[31] = DateTime.FromOADate(file.ReadDouble());
                            row[32] = file.ReadDouble();
                            row[33] = file.ReadDouble();
                            row[34] = file.ReadDouble();
                            row[35] = file.ReadDouble();
                            row[37] = file.ReadDouble();
                            row[38] = file.ReadDouble();
                        }
                        row[36] = file.ReadString();
                        table.Rows.Add(row);
                        worker.ReportProgress((int)((float)i * 100 / (float)table.Rows.Count), userState);
                        if (row[3] != DBNull.Value) userState.Element = (string)row[3];
                    }
                    ByDate = file.ReadBoolean();
                    file.Close();
                    fs = null;
                    DataLoaded = true;
                }
                catch
                {
                    table.Clear();
                    DataLoaded = false;
                    if (fs != null) fs.Close();
                }
            }
            DataLoading = false;
        }
        public void WriteAreasParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if ((table.Rows.Count > 1) && (DataLoaded))
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Запись параметров ячеек заводнения в кеш";
                userState.Element = "";
                worker.ReportProgress(0, userState);
                int i, j;
                DataRow row;
                string cacheName = _proj.path + "\\cache\\areas_params.bin";
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter file = new BinaryWriter(fs);
                file.Write(this.calcDate.ToOADate());
                file.Write(this.lastDateMer.ToOADate());
                file.Write(table.Rows.Count);

                for (i = 0; i < table.Rows.Count; i++)
                {
                    row = table.Rows[i];
                    userState.Element = (string)row[3];
                    file.Write((int)row[0]);
                    file.Write((int)row[1]);
                    file.Write((int)row[2]);
                    file.Write((string)row[3]);

                    if ((int)row[1] != -1)
                    {
                        file.Write((string)row[4]);
                        file.Write((string)row[5]);
                        file.Write((string)row[6]);

                        file.Write((int)row[7]);
                        file.Write((int)row[8]);
                        file.Write((double)row[9]);
                        file.Write((double)row[10]);
                        file.Write((double)row[11]);
                        if (row[12] == DBNull.Value) row[12] = 0;
                        file.Write((double)row[12]);

                        file.Write((int)row[13]);
                        file.Write((double)row[14]);
                        file.Write((double)row[15]);
                        file.Write((double)row[16]);
                        if (row[17] == DBNull.Value) row[17] = 0;
                        file.Write((double)row[17]);

                        if (row[18] != DBNull.Value)
                        {
                            file.Write((double)row[18]);
                            file.Write((double)row[19]);
                            file.Write((double)row[20]);
                            file.Write((double)row[21]);
                            file.Write((double)row[22]);
                            file.Write((double)row[23]);
                            file.Write((double)row[24]);
                            file.Write((double)row[25]);
                            file.Write((double)row[26]);
                            file.Write((double)row[27]);
                            file.Write((double)row[28]);
                            file.Write((double)row[29]);
                        }
                        else
                        {
                            for (j = 0; j < 12; j++) file.Write((double)0.0f);
                        }
                        if (row[30] == DBNull.Value) row[30] = string.Empty;
                        file.Write((string)row[30]);
                        file.Write(((DateTime)row[31]).ToOADate());
                        file.Write((row[32] == DBNull.Value) ? 0 : (double)row[32]);
                        file.Write((row[33] == DBNull.Value) ? 0 : (double)row[33]);
                        file.Write((row[34] == DBNull.Value) ? 0 : (double)row[34]);
                        file.Write((row[35] == DBNull.Value) ? 0 : (double)row[35]);
                        file.Write((row[37] == DBNull.Value) ? 0 : (double)row[37]);
                        file.Write((row[38] == DBNull.Value) ? 0 : (double)row[38]);
                    }
                    file.Write((row[36] == DBNull.Value) ? string.Empty : (string)row[36]);
                    worker.ReportProgress((int)((float)i * 100 / (float)table.Rows.Count), userState);
                }
                file.Write(this.ByDate);
                file.Close();
            }
        }
        
        public void DrawAreasByComp(bool DrawByAccumComp)
        {
            if (table.Rows.Count > 0)
            {
                int i, j, k;
                int ofIndex = - 1, areaIndex;
                ArrayList list = new ArrayList();
                OilField of = null;
                Contour cntr;
                double comp;
                Palette pal = new Palette();
                pal.Add(0, Color.Blue);
                pal.Add(100, Color.Green);
                pal.Add(200, Color.Red);

                k = 12;
                if (DrawByAccumComp) k += 5;
                i = 0;
                while (i < table.Rows.Count)
                {
                    if (ofIndex != (int)table.Rows[i].ItemArray[0])
                    {
                        ofIndex = (int)table.Rows[i].ItemArray[0];
                        of = _proj.OilFields[ofIndex];
                    }
                    if ((of != null) && ((int)table.Rows[i].ItemArray[2] == 1))
                    {
                        areaIndex = (int)table.Rows[i].ItemArray[1];
                        if ((areaIndex < of.Areas.Count) && (areaIndex > -1))
                        {
                            cntr = (Contour)((Area)of.Areas[areaIndex]).contour;
                            comp = (double)table.Rows[i].ItemArray[k];
                            if ((int)table.Rows[i].ItemArray[8] == 0)
                            {
                                cntr.fillBrush = new SolidBrush(Color.LightGray);
                                ((Area)of.Areas[areaIndex]).Compensation = 0;
                                ((Area)of.Areas[areaIndex]).AccumCompensation = 0;
                            }
                            else
                            {
                                cntr.fillBrush = new SolidBrush(pal.GetColorByX(comp));
                                ((Area)of.Areas[areaIndex]).Compensation = (double)table.Rows[i].ItemArray[12];
                                ((Area)of.Areas[areaIndex]).AccumCompensation = (double)table.Rows[i].ItemArray[17];
                            }
                        }
                    }
                    i++;
                }
                Area area;
                for (i = 0; i < _proj.OilFields.Count; i++)
                {
                    of = _proj.OilFields[i];
                    if (of.Areas.Count > 0)
                    {
                        for(j = 0; j< of.Areas.Count;j++)
                        {
                            area = (Area)of.Areas[j];
                            if (area.contour.fillBrush == null)
                                area.contour.fillBrush = new SolidBrush(Color.LightGray);
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region Таблица Шахматка
    sealed class DataGridChess : DataGridView
    {
        Well srcWell;
        Project _proj;
        MainForm mainForm;
        DataTable table;
        bool DataLoaded;
        bool DataLoading;
        Font _font, _font_bold;
        DataGridViewCellStyle HeaderCellStyle, TableCellStyleAll, TableCellStyleFloat, TableCellStyleQLiq, TableCellStyleQOil;
        string[] tableHeaderText = { "Скв", "Дата", "Qж, т/сут", "Qн, т/сут", "Обв, %", "Плот.воды", 
                                         "Вр.простоя, ч", "Ндин, м", "Нстат, м", "Pзатр, атм"};
        int[] columnWidth = { 50, 62, 42, 42, 22, 53, 27, 26, 26, 26 };
        public DataGridChess(MainForm MainForm)
        {
            HeaderCellStyle = new DataGridViewCellStyle();
            TableCellStyleAll = new DataGridViewCellStyle();
            TableCellStyleFloat = new DataGridViewCellStyle();
            TableCellStyleQLiq = new DataGridViewCellStyle();
            TableCellStyleQOil = new DataGridViewCellStyle();

            srcWell = null;
            DataLoaded = false;
            DataLoading = false;
            _font = new Font("Calibri", 8.25f);
            _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);

            this.mainForm = MainForm;
            this.Parent =  mainForm.pChessView;
            this.Dock = DockStyle.Fill;

            
            table = new DataTable("CHESS");

            table.Columns.Add("SKVNAME", Type.GetType("System.String"));    // 0
            table.Columns.Add("DATE", Type.GetType("System.DateTime"));     // 1
            table.Columns.Add("QLIQ", Type.GetType("System.Double"));       // 2
            table.Columns.Add("QOIL", Type.GetType("System.Double"));       // 3
            table.Columns.Add("WATERING", Type.GetType("System.Double"));   // 4
            table.Columns.Add("WATDENSITY", Type.GetType("System.Double")); // 5
            table.Columns.Add("STAYTIME", Type.GetType("System.Double"));   // 6
            table.Columns.Add("HDYN", Type.GetType("System.Double"));       // 7
            table.Columns.Add("HSTAT", Type.GetType("System.Double"));      // 8
            table.Columns.Add("PZATR", Type.GetType("System.Double"));      // 9

            this.DataSource = table;

            TableCellStyleFloat.Format = "0.##";
            TableCellStyleFloat.Font = _font;

            TableCellStyleAll.Font = _font;

            TableCellStyleQLiq.Format = "0.##";
            TableCellStyleQLiq.Font = _font_bold;
            TableCellStyleQLiq.ForeColor = Color.DarkGreen;

            TableCellStyleQOil.Format = "0.##";
            TableCellStyleQOil.Font = _font_bold;
            TableCellStyleQOil.ForeColor = Color.Peru;

            //for (int i = 0; i < this.Columns.Count; i++)
            //{
            //    if (i == 2) this.Columns[i].DefaultCellStyle = TableCellStyleQLiq;
            //    else if (i == 3) this.Columns[i].DefaultCellStyle = TableCellStyleQOil;
            //    else if ((i == 2) && (i == 3) && (i == 4) && (i > 6)) this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
            //    else this.Columns[i].DefaultCellStyle = TableCellStyleAll;
            //    this.Columns[i].Width = columnWidth[i];
            //    this.Columns[i].HeaderText = tableHeaderText[i];
            //}

            // Настройка таблицы Шахматка
            this.RowTemplate.Height = 13;
            this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ColumnHeadersHeight = 18;
            this.EnableHeadersVisualStyles = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = true;
            this.DoubleBuffered = true;
            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;


            this.BackgroundColor = SystemColors.ControlDark;
            this.MouseDown += new MouseEventHandler(DataGridChess_MouseDown);
            this.Resize += new EventHandler(DataGridChess_Resize);
            this.Paint += new PaintEventHandler(DataGridChess_Paint); // перерисовка окна
            this.ColumnWidthChanged += new DataGridViewColumnEventHandler(DataGridChess_ColumnWidthChanged);
            this.KeyDown += new KeyEventHandler(DataGridChess_KeyDown);
        }

        void DataGridChess_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
            {
                if (((DataGridView)sender).SelectedCells.Count > 0)
                {
                    copySelectedRowsToClipboard((DataGridView)sender);
                }
            }              
        }

        void DataGridChess_MouseDown(object sender, MouseEventArgs e)
        {
            // mainForm.StatUsage.AddMessage(CollectorStatId.CHESS_MOUSE_DOWN);
        }
        private void copySelectedRowsToClipboard(DataGridView dgv)
        {
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            Clipboard.Clear();
            if (dgv.GetClipboardContent() != null)
            {
                Clipboard.SetDataObject(dgv.GetClipboardContent());
                Clipboard.GetData(DataFormats.Text);
                IDataObject dt = Clipboard.GetDataObject();
                if (dt.GetDataPresent(typeof(string)))
                {
                    string tb = (string)(dt.GetData(typeof(string)));
                    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                    byte[] dataStr = encoding.GetBytes(tb);
                    Clipboard.SetDataObject(encoding.GetString(dataStr));
                }
            }
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
        }
        void DataGridChess_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            //mainForm.reLog.AppendText(e.Column.Width.ToString() + Environment.NewLine);
        }
        void SetHeaderStyle()
        {
            if(!DataLoading)
            {
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (i == 2) this.Columns[i].DefaultCellStyle = TableCellStyleQLiq;
                    else if (i == 3) this.Columns[i].DefaultCellStyle = TableCellStyleQOil;
                    else if ((i == 2) && (i == 3) && (i == 4) && (i > 6)) this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    else this.Columns[i].DefaultCellStyle = TableCellStyleAll;
                    this.Columns[i].Width = columnWidth[i];
                    this.Columns[i].HeaderText = tableHeaderText[i];
                }
                HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
                HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
                HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
                HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
                this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;
            }
        }
        public void SetProject(Project aProject)
        {
            _proj = aProject;
        }
        public void ClearProject() { this._proj = null; this.ClearTable(); }
        void DataGridChess_Resize(object sender, EventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
                this.Invalidate();
            SetHeaderStyle();
        }
        void DataGridChess_Paint(object sender, PaintEventArgs e)
        {
            if ((!DataLoaded) && (!DataLoading))
            {
                System.Drawing.Graphics gr = e.Graphics;
                gr.DrawString("Нет данных для отображения", _font, Brushes.Black, this.Left + (int)(this.Width / 2), this.Top + (int)(this.Height / 2));
            }
        }
        public void ClearTable()
        {
            srcWell = null;
            DataLoading = true;
            table.Clear();
            DataLoaded = false;
            DataLoading = false;
            this.BackgroundColor = SystemColors.ControlDark;
        }

        public void ShowChess(int oilFieldIndex, int wellIndex)
        {
            if ((oilFieldIndex >= 0) &&(this.Parent.Visible))
            {
                OilField of = _proj.OilFields[oilFieldIndex];
                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataRow row;
                DateTime predDT;
                int i;
                DataLoading = true;
                DataLoaded = false;
                this.DataSource = null;
                table.Clear();
                ChessItem DayItem;
                DateTime predDt = DateTime.MinValue;
                double Qliq = 0, Qoil = 0, Wat = 0, sumChess = -1;
                if (!w.ChessLoaded) (_proj.OilFields[w.OilFieldIndex]).LoadChessFromCache(w.Index);

                if ((w.chess != null) && (w.ChessLoaded) && (w.chess.Count > 0))
                {
                    int chessLength = w.chess.Count;
                    for (i = 0; i < chessLength; i++)
                    {
                        DayItem = w.chess[i];
                        row = table.NewRow();
                        row[0] = w.UpperCaseName;
                        row[1] = DayItem.Date;
                        predDt = DayItem.Date.AddDays(1);
                        if ((DayItem.StayTime != 24) || (sumChess != DayItem.Qliq + DayItem.Qoil))
                        {
                            Qliq = DayItem.Qliq;
                            Qoil = DayItem.Qoil;
                        }
                        else
                        {
                            Qliq = 0; Qoil = 0;
                        }
                        row[2] = Qliq;
                        row[3] = Qoil;
                        sumChess = DayItem.Qliq + DayItem.Qoil;
                        
                        if (Qliq > 0)
                            row[4] = Math.Round(((Qliq - Qoil) * 100) / Qliq, 2);
                        else
                            row[4] = 0;
                        if (DayItem.StayTime >= 0) row[6] = DayItem.StayTime;
                        if (DayItem.Hdyn >= 0) row[7] = DayItem.Hdyn;
                        table.Rows.Add(row);
                    }
                    DataLoaded = true;
                    this.BackgroundColor = Color.White;
                }
                DataLoading = false;
                this.DataSource = table;
                SetHeaderStyle();
                if ((this.Rows.Count > 0) && (this.ClientRectangle.Height > 15))
                    this.FirstDisplayedScrollingRowIndex = this.Rows.Count - 1;
            }
            else if ((oilFieldIndex >= 0) && (!this.Parent.Visible))
            {
                OilField of = _proj.OilFields[oilFieldIndex];
                Well w = of.Wells[wellIndex];
                if (w == null) return;
                srcWell = w;
                DataLoading = false;
                DataLoaded = false;
                table.Clear();
            }
        }
        public void UpdateBySourceWell()
        {
            if ((srcWell != null) && (!DataLoaded))
            {
                ShowChess(srcWell.OilFieldIndex, srcWell.Index);
            }
        }
    }
    #endregion

    #region Суммарные показатели
        sealed class DataGridSumParams : DataGridView
        {
            Project _proj;
            MainForm mainForm;
            DataSet _dsSumParams;
            public DataTable table;
            bool DataLoaded, DataLoading;
            bool InThousand;
            Font _font, _font_bold;
            public bool DataMonth;
            ArrayList OFIndexes;
            FilterOilObjects filterOilObj;
            DataGridViewCellStyle TableCellStyleAll, TableCellStyleFloat, TableCellStyleDate;
            string[] tableHeaderText = { "Дата", "Жидк, тыс.т", "Нефть, тыс.т", "%", "Зак, тыс.м3", 
                                             "Вдз, тыс.м3", "Фдоб, шт", "Фнаг, шт", "Фвдз, шт", 

                                             "Пр.Жидк, тыс.т", "Пр.Нефть, тыс.т", "Пр.%", "Пр.Зак, тыс.м3", 
                                             "Пр.Ввод доб., шт", "Пр.Ввод экспл.бур., шт", "Пр.Фдоб, шт", "Пр.Фнаг, шт" };

            int[] columnWidth = { 47, 60, 60, 22, 60, 60, 33, 33, 33,  60, 60, 22, 60, 33, 33, 33, 33 };

            public DataGridSumParams(MainForm MainForm, bool ByMotnh)
            {

                DataGridViewCellStyle HeaderCellStyle = new DataGridViewCellStyle();
                TableCellStyleAll = new DataGridViewCellStyle();
                TableCellStyleFloat = new DataGridViewCellStyle();
                TableCellStyleDate = new DataGridViewCellStyle();
                DataGridViewCellStyle TableCellStyleAlternating = new DataGridViewCellStyle();

                DataMonth = ByMotnh;

                DataLoaded = false;
                DataLoading = false;
                _font = new Font("Calibri", 8.25f);
                _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);

                this.mainForm = MainForm;

                this.Parent = this.mainForm.scGraphics.Panel2;
                this.Visible = false;
                this.Dock = DockStyle.Fill;

                
                filterOilObj = mainForm.filterOilObj;

                _dsSumParams = new DataSet("dsSumParams");
                table = new DataTable("SUM");
                OFIndexes = new ArrayList();

                table.Columns.Add("DATE", Type.GetType("System.DateTime"));         // 0
                table.Columns.Add("LIQ", Type.GetType("System.Double"));            // 1
                table.Columns.Add("OIL", Type.GetType("System.Double"));            // 2
                table.Columns.Add("WATERING", Type.GetType("System.Double"));       // 3
                table.Columns.Add("INJ", Type.GetType("System.Double"));            // 4
                table.Columns.Add("SCOOP", Type.GetType("System.Double"));          // 5
                table.Columns.Add("FPROD", Type.GetType("System.Int32"));           // 6
                table.Columns.Add("FINJ", Type.GetType("System.Int32"));            // 7
                table.Columns.Add("FSCOOP", Type.GetType("System.Int32"));          // 8

                table.Columns.Add("PR_LIQ", Type.GetType("System.Double"));            // 9
                table.Columns.Add("PR_OIL", Type.GetType("System.Double"));            // 10
                table.Columns.Add("PR_WATERING", Type.GetType("System.Double"));       // 11
                table.Columns.Add("PR_INJ", Type.GetType("System.Double"));            // 12
                table.Columns.Add("PR_LEADIN", Type.GetType("System.Int32"));          // 13
                table.Columns.Add("PR_LEADIN_PROD", Type.GetType("System.Int32"));          // 14
                table.Columns.Add("PR_FPROD", Type.GetType("System.Int32"));           // 15
                table.Columns.Add("PR_FINJ", Type.GetType("System.Int32"));            // 16


                _dsSumParams.Tables.Add(table);
                this.DataSource = _dsSumParams.Tables[0];

                TableCellStyleFloat.Format = "0.##";
                TableCellStyleFloat.Font = _font;
                

                if(DataMonth) TableCellStyleDate.Format = "MM.yyyy";
                else TableCellStyleDate.Format = "yyyy";
                TableCellStyleDate.Font = _font;
                

                TableCellStyleAll.Font = _font;

                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (i == 0)
                    {
                        this.Columns[i].DefaultCellStyle = TableCellStyleDate;
                    }
                    else if (((i > 0) && (i < 6) && (i != 3)) || ((i > 8) && (i < 13)))
                    {
                        this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    }
                    else
                    {
                        this.Columns[i].DefaultCellStyle = TableCellStyleAll;
                    }

                    this.Columns[i].Width = columnWidth[i];
                    this.Columns[i].HeaderText = tableHeaderText[i];
                }
                 if(!DataMonth) this.Columns[0].Width = columnWidth[9];

                // Настройка таблицы Шахматка
                this.RowTemplate.Height = 13;
                this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                this.ColumnHeadersHeight = 18;
                this.EnableHeadersVisualStyles = false;
                this.AllowUserToResizeRows = false;
                this.AllowUserToAddRows = false;
                this.AllowUserToDeleteRows = false;
                this.RowHeadersVisible = false;
                this.ReadOnly = true;
                this.DoubleBuffered = true;
                this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                if (!DataMonth)
                {
                    TableCellStyleAlternating.BackColor = Color.LightGray;
                    this.AlternatingRowsDefaultCellStyle = TableCellStyleAlternating;
                }

                HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
                HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
                HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
                HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
                this.BackgroundColor = SystemColors.ControlDark;

                this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;


                this.Resize += new EventHandler(DataGridSumParams_Resize);
                this.Paint += new PaintEventHandler(DataGridSumParams_Paint);
                this.KeyDown += new KeyEventHandler(DataGridSumParams_KeyDown);
            }

            void SetColumnsFormat()
            {
                TableCellStyleFloat = new DataGridViewCellStyle();
                TableCellStyleFloat.Format = "#,#";
                TableCellStyleFloat.Font = _font;
                if (InThousand) 
                    TableCellStyleFloat.Format += "0,";
                else
                    TableCellStyleFloat.Format += "0.##";
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (((i > 0) && (i < 6) && (i != 3)) || ((i == 9) || (i == 10) || (i == 12))) 
                    {
                        
                        this.Columns[i].DefaultCellStyle = TableCellStyleFloat;

                        string str = tableHeaderText[i];
                        if (!InThousand) str = str.Replace("тыс.", "");
                        this.Columns[i].HeaderText = str;
                    }
                }
 
            }

            public void SetProject(Project aProject)
            {
                _proj = aProject;
            }
            public void ClearTable()
            {
                DataLoading = true;
                OFIndexes.Clear();
                table.Clear();
                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();
                DataLoaded = false;
                DataLoading = false;
                this.BackgroundColor = SystemColors.ControlDark;
            }
            public void ClearDataSet()
            {
                this.DataSource = null;
            }
            public void SetDataSource()
            {
                this.DataSource = _dsSumParams.Tables[0];
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (i == 0) 
                        this.Columns[i].DefaultCellStyle = TableCellStyleDate;
                    else if (((i > 0) && (i < 6) && (i != 3)) || ((i > 8) && (i < 13))) 
                        this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    else 
                        this.Columns[i].DefaultCellStyle = TableCellStyleAll;

                    this.Columns[i].Width = columnWidth[i];
                    this.Columns[i].HeaderText = tableHeaderText[i];
                }
                SetColumnsFormat();
                this.ScrollBars = ScrollBars.None;
                if (!DataMonth)
                {
                    this.Columns[0].Width = columnWidth[9];
                }
                else
                    this.RePaintRows();
                this.ScrollBars = ScrollBars.Both;
                if ((this.Rows.Count > 0) && (this.ClientRectangle.Height > 15))
                    this.FirstDisplayedScrollingRowIndex = this.Rows.Count - 1;
                this.BackgroundColor = Color.White;
            }

            public void AddSumParams(BackgroundWorker worker, DoWorkEventArgs e, CanvasMap canv)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Пересчет суммарных показателей";
                var dictPlast = (StratumDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                table.Clear();
                InThousand = false;
                OFIndexes.Clear();
                if ((canv != null) && (filterOilObj != null) && (dictPlast != null))
                {
                    if ((canv.sumParams == null) || (canv.sumParams.Count == 0)) return;
                    userState.Element = "Выбранный список скважин";

                    DataRow row;
                    int i, k, iLen, progress, objIndex, ind;
                    DataLoading = true;
                    DataLoaded = false;
                    SumParamItem item;
                    SumParamItem[] SumParams;
                    worker.ReportProgress(0, userState);
                    for (k = 0; k < filterOilObj.SelectedCurrentObjects.Count; k++)
                    {
                        if (filterOilObj.AllCurrentSelected)
                        {
                            if (DataMonth) SumParams = canv.sumParams[0].MonthlyItems;
                            else SumParams = canv.sumParams[0].YearlyItems;
                        }
                        else
                        {
                            ind = (int)filterOilObj.SelectedCurrentObjects[k];
                            objIndex = canv.sumParams.GetIndexByObjCode(dictPlast[ind].Code);
                            if (objIndex == -1) continue;

                            if (DataMonth) SumParams = canv.sumParams[objIndex].MonthlyItems;
                            else SumParams = canv.sumParams[objIndex].YearlyItems;
                        }
                        iLen = SumParams.Length;
                        AddSumParams(worker, e, SumParams);
                        userState.Element = dictPlast[(int)filterOilObj.SelectedCurrentObjects[k]].ShortName;
                        progress = (int)((float)k * 100 / (float)filterOilObj.SelectedCurrentObjects.Count);
                        worker.ReportProgress(progress, userState);
                        if (filterOilObj.AllCurrentSelected) break;
                    }
                    worker.ReportProgress(100, userState);
                    DataLoading = false;

                }
            }
            public void AddSumParams(BackgroundWorker worker, DoWorkEventArgs e, OilField of, bool Clear)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Пересчет суммарных показателей";
                var dictPlast = (StratumDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                if (Clear)
                {
                    table.Clear();
                    InThousand = false;
                    OFIndexes.Clear();
                }

                if ((of != null)&&(filterOilObj != null) &&(dictPlast != null))
                {
                    bool find = false;
                    for (int i = 0; i < OFIndexes.Count; i++)
                    {
                        if ((int)OFIndexes[i] == of.Index)
                        {
                            find = true;
                            break;
                        }
                    }
                    if (find)
                    {
                        SubSumParams(worker, e, of);
                        if ((!DataMonth) && (of.Table81 != null)) SubTable81Params(worker, e, of.Table81.Items);
                    }
                    else
                    {
                        if ((of.sumParams == null) || (of.sumParams.Count == 0)) return;
                        userState.Element = of.Name;

                        OFIndexes.Add(of.Index);
                        DataRow row;
                        int i, k, iLen, progress, objIndex, ind;
                        DataLoading = true;
                        DataLoaded = false;
                        SumParamItem item;
                        SumParamItem[] SumParams;
                        worker.ReportProgress(0, userState);
                        for (k = 0; k < filterOilObj.SelectedCurrentObjects.Count; k++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            if (filterOilObj.AllCurrentSelected)
                            {
                                if (DataMonth) SumParams = of.sumParams[0].MonthlyItems;
                                else SumParams = of.sumParams[0].YearlyItems;
                            }
                            else
                            {
                                ind = (int)filterOilObj.SelectedCurrentObjects[k];
                                objIndex = of.sumParams.GetIndexByObjCode(dictPlast[ind].Code);
                                if (objIndex == -1) continue;

                                if (DataMonth) SumParams = of.sumParams[objIndex].MonthlyItems;
                                else SumParams = of.sumParams[objIndex].YearlyItems;
                            }
                            iLen = SumParams.Length;
                            AddSumParams(worker, e, SumParams);
                            if (k < filterOilObj.SelectedCurrentObjects.Count)
                                userState.Element = dictPlast[(int)filterOilObj.SelectedCurrentObjects[k]].ShortName;
                            progress = (int)((float)k * 100 / (float)filterOilObj.SelectedCurrentObjects.Count);
                            worker.ReportProgress(progress, userState);
                            if (filterOilObj.AllCurrentSelected) break;
                        }
                        if ((!DataMonth)&&(of.Table81 != null)) AddTable81Params(worker, e, of.Table81.Items);
                        worker.ReportProgress(100, userState);
                        DataLoading = false;
                    }
                }
            }
            public void SubSumParams(BackgroundWorker worker, DoWorkEventArgs e, OilField of)
            {
                if (of != null)
                {
                    WorkerState userState = new WorkerState();
                    userState.WorkMainTitle = "";
                    userState.WorkCurrentTitle = "Пересчет суммарных показателей";
                    var dictPlast = (StratumDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);

                    OilField ofCurr;
                    if (of.sumParams == null) return;
                    userState.Element = of.Name;

                    int i, j, ind, len, objIndex;
                    DataLoading = true;
                    DataLoaded = false;

                    SumParamItem[] SumParams;
                    if (DataMonth) SumParams = of.sumParams[0].MonthlyItems;
                    else SumParams = of.sumParams[0].YearlyItems;
                    for (i = 0; i < OFIndexes.Count; i++)
                    {
                        if ((int)OFIndexes[i] == of.Index)
                        {
                            OFIndexes.RemoveAt(i);
                            break;
                        }
                    }
                    worker.ReportProgress(0, userState);
                    if (SumParams.Length > 0)
                    {
                        DateTime dt, minDT, maxDT, minOFListDT, maxOFListDT;
                        minOFListDT = DateTime.MaxValue;
                        maxOFListDT = DateTime.MinValue;
                        SumParamItem[] sum;
                        for (i = 0; i < OFIndexes.Count; i++)
                        {
                            ofCurr = _proj.OilFields[(int)OFIndexes[i]];
                            if (ofCurr.sumParams != null)
                            {
                                if (filterOilObj.AllCurrentSelected)
                                {
                                    if (DataMonth) sum = ofCurr.sumParams[0].MonthlyItems;
                                    else sum = ofCurr.sumParams[0].YearlyItems;

                                    if (sum[0].Date < minOFListDT) minOFListDT = sum[0].Date;
                                    if (sum[sum.Length - 1].Date > maxOFListDT) maxOFListDT = sum[sum.Length - 1].Date;
                                }
                                else
                                {
                                    for (j = 0; j < filterOilObj.SelectedCurrentObjects.Count; j++)
                                    {
                                        ind = (int)filterOilObj.SelectedCurrentObjects[j];
                                        objIndex = ofCurr.sumParams.GetIndexByObjCode(dictPlast[ind].Code);
                                        if (objIndex == -1) continue;

                                        if (DataMonth) sum = ofCurr.sumParams[objIndex].MonthlyItems;
                                        else sum = ofCurr.sumParams[objIndex].YearlyItems;

                                        if (sum[0].Date < minOFListDT) minOFListDT = sum[0].Date;
                                        if (sum[sum.Length - 1].Date > maxOFListDT) maxOFListDT = sum[sum.Length - 1].Date;
                                    }
                                }
                            }
                            if ((!DataMonth) && (ofCurr.Table81 != null))
                            {
                                sum = ofCurr.Table81.Items;

                                if (sum[0].Date < minOFListDT) minOFListDT = sum[0].Date;
                                if (sum[sum.Length - 1].Date > maxOFListDT) maxOFListDT = sum[sum.Length - 1].Date;
                            }
                        }
                        if ((minOFListDT == DateTime.MaxValue) && (maxOFListDT == DateTime.MinValue))
                        {
                            table.Clear();
                            return;
                        }
                        minDT = (DateTime)table.Rows[0].ItemArray[0];
                        while (minDT != minOFListDT)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            table.Rows.RemoveAt(0);
                            if (table.Rows.Count > 0) minDT = (DateTime)table.Rows[0].ItemArray[0];
                        }
                        len = table.Rows.Count;
                        maxDT = (DateTime)table.Rows[len - 1].ItemArray[0];
                        while ((maxDT != maxOFListDT) && (len > 0))
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            table.Rows.RemoveAt(len - 1);
                            len--;
                            maxDT = (DateTime)table.Rows[len - 1].ItemArray[0];
                        }

                        for (j = 0; j < filterOilObj.SelectedCurrentObjects.Count; j++)
                        {
                            if (filterOilObj.AllCurrentSelected)
                            {
                                if (DataMonth) SumParams = of.sumParams[0].MonthlyItems;
                                else SumParams = of.sumParams[0].YearlyItems;
                            }
                            else
                            {
                                ind = (int)filterOilObj.SelectedCurrentObjects[j];
                                objIndex = of.sumParams.GetIndexByObjCode(dictPlast[ind].Code);
                                if (objIndex == -1) continue;

                                if (DataMonth) SumParams = of.sumParams[objIndex].MonthlyItems;
                                else SumParams = of.sumParams[objIndex].YearlyItems;
                            }
                            SubSumParams(worker, e, SumParams);
                            if (filterOilObj.AllCurrentSelected) break;
                        }
                    }
                    worker.ReportProgress(100, userState);
                }
            }

            private void AddSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumParamItem[] SumParams)
            {
                if (SumParams == null) return;
                int i, ind;
                DataRow row;
                DataLoading = true;
                DataLoaded = false;
                SumParamItem item;
                double maxValue = 0;

                if (SumParams.Length > 0)
                {
                    DateTime dt, minDT, maxDT;
                    dt = SumParams[0].Date;
                    if (table.Rows.Count == 0)
                    {
                        row = table.NewRow();
                        row[0] = dt;
                        row[1] = 0;
                        row[2] = 0;
                        row[3] = 0;
                        row[4] = 0;
                        row[5] = 0;
                        row[6] = 0;
                        row[7] = 0;
                        row[8] = 0;
                        row[9] = 0;
                        row[10] = 0;
                        row[11] = 0;
                        row[12] = 0;
                        row[13] = 0;
                        row[14] = 0;
                        row[15] = 0;
                        row[16] = 0;
                        table.Rows.Add(row);
                        row = table.NewRow();
                        if(DataMonth) row[0] = dt.AddMonths(1);
                        else row[0] = dt.AddYears(1);
                        row[1] = 0;
                        row[2] = 0;
                        row[3] = 0;
                        row[4] = 0;
                        row[5] = 0;
                        row[6] = 0;
                        row[7] = 0;
                        row[8] = 0;
                        row[9] = 0;
                        row[10] = 0;
                        row[11] = 0;
                        row[12] = 0;
                        row[13] = 0;
                        row[14] = 0;
                        row[15] = 0;
                        row[16] = 0;
                        table.Rows.Add(row);
                    }
                    minDT = (DateTime)table.Rows[0].ItemArray[0];
                    maxDT = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[0];

                    if (dt < minDT)
                    {
                        ind = 0;
                        while (dt < minDT)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            row = table.NewRow();
                            row[0] = dt;
                            row[1] = 0;
                            row[2] = 0;
                            row[3] = 0;
                            row[4] = 0;
                            row[5] = 0;
                            row[6] = 0;
                            row[7] = 0;
                            row[8] = 0;
                            row[9] = 0;
                            row[10] = 0;
                            row[11] = 0;
                            row[12] = 0;
                            row[13] = 0;
                            row[14] = 0;
                            row[15] = 0;
                            row[16] = 0;
                            table.Rows.InsertAt(row, ind);
                            if (DataMonth) dt = dt.AddMonths(1);
                            else dt = dt.AddYears(1);
                            ind++;
                        }
                    }
                    dt = SumParams[SumParams.Length - 1].Date;
                    if (dt > maxDT)
                    {
                        while (maxDT < dt)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            if (DataMonth) maxDT = maxDT.AddMonths(1);
                            else maxDT = maxDT.AddYears(1);
                            row = table.NewRow();
                            row[0] = maxDT;
                            row[1] = 0;
                            row[2] = 0;
                            row[3] = 0;
                            row[4] = 0;
                            row[5] = 0;
                            row[6] = 0;
                            row[7] = 0;
                            row[8] = 0;
                            row[9] = 0;
                            row[10] = 0;
                            row[11] = 0;
                            row[12] = 0;
                            row[13] = 0;
                            row[14] = 0;
                            row[15] = 0;
                            row[16] = 0;
                            table.Rows.Add(row);
                        }
                    }
                    minDT = (DateTime)table.Rows[0].ItemArray[0];
                    maxDT = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[0];

                    int sumLength = SumParams.Length;
                    dt = SumParams[0].Date;
                    if (this.DataMonth) ind = (dt.Year - minDT.Year - 1) * 12 + (12 - minDT.Month) + dt.Month;
                    else ind = dt.Year - minDT.Year;
                    for (i = 0; i < sumLength; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        item = SumParams[i];
                        row = table.Rows[ind];
                        if (!InThousand)
                        {
                            if (item.Liq > maxValue) maxValue = item.Liq;
                            if (item.Inj > maxValue) maxValue = item.Inj;
                        }
                        row[1] = (double)row[1] + item.Liq;
                        row[2] = (double)row[2] + item.Oil;
                        if ((double)row[1] > 0) 
                            row[3] = Math.Round(((double)row[1] - (double)row[2]) * 100 / (double)row[1]);
                        row[4] = (double)row[4] + item.Inj;
                        row[5] = (double)row[5] + item.Scoop;
                        row[6] = (int)row[6] + item.Fprod;
                        row[7] = (int)row[7] + item.Finj;
                        row[8] = (int)row[8] + item.Fscoop;
                        ind++;
                    }
                    if (!InThousand)
                    {
                        if (maxValue > 9000) InThousand = true;
                    }
                    table.AcceptChanges();
                    DataLoaded = true;
                }
            }
            private void SubSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumParamItem[] SumParams)
            {
                if (SumParams == null) return;
                DataRow row;
                int i, ind, ind2, len;
                DataLoading = true;
                DataLoaded = false;
                SumParamItem item;
                double maxValue = 0;
                if (SumParams.Length > 0)
                {
                    DateTime dt, minDT, maxDT;
                    len = table.Rows.Count;
                    minDT = (DateTime)table.Rows[0].ItemArray[0];
                    maxDT = (DateTime)table.Rows[len - 1].ItemArray[0];

                    int sumLength = SumParams.Length;

                    if ((SumParams[0].Date <= maxDT) || (SumParams[SumParams.Length - 1].Date >= minDT))
                    {
                        dt = SumParams[0].Date;
                        if (dt >= minDT)
                        {
                            ind = 0;
                            if (this.DataMonth) ind2 = (dt.Year - minDT.Year - 1) * 12 + (12 - minDT.Month) + dt.Month;
                            else ind2 = dt.Year - minDT.Year;
                        }
                        else
                        {
                            if (this.DataMonth) ind = (minDT.Year - dt.Year - 1) * 12 + (12 - dt.Month) + minDT.Month;
                            else ind = minDT.Year - dt.Year;
                            ind2 = 0;
                        }

                        for (i = ind; i < sumLength; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            if (ind2 >= len) break;
                            item = SumParams[i];
                            row = table.Rows[ind2];
                            row[1] = (double)row[1] - item.Liq;
                            row[2] = (double)row[2] - item.Oil;
                            if ((double)row[1] > 0) 
                                row[3] = Math.Round(((double)row[1] - (double)row[2]) * 100 / (double)row[1]);
                            row[4] = (double)row[4] - item.Inj;
                            row[5] = (double)row[5] - item.Scoop;
                            row[6] = (int)row[6] - item.Fprod;
                            row[7] = (int)row[7] - item.Finj;
                            row[8] = (int)row[8] - item.Fscoop;

                            if ((double)row[1] > maxValue) maxValue = (double)row[1];
                            if ((double)row[4] > maxValue) maxValue = (double)row[4];
                            ind2++;
                        }
                        table.AcceptChanges();
                    }
                    if (maxValue > 0) InThousand = true;
                    else InThousand = false;
                    DataLoaded = true;
                }
            }
            private void AddTable81Params(BackgroundWorker worker, DoWorkEventArgs e, SumParamItem[] SumParams)
            {
                if (SumParams == null) return;
                bool find = false;
                int i, ind, iLen, progress;
                DataRow row;
                DataLoading = true;
                DataLoaded = false;
                SumParamItem item;

                if (SumParams.Length > 0)
                {
                    DateTime dt, minDT, maxDT;
                    dt = SumParams[0].Date;
                    if (table.Rows.Count == 0)
                    {
                        row = table.NewRow();
                        row[0] = dt;
                        row[1] = 0;
                        row[2] = 0;
                        row[3] = 0;
                        row[4] = 0;
                        row[5] = 0;
                        row[6] = 0;
                        row[7] = 0;
                        row[8] = 0;
                        row[9] = 0;
                        row[10] = 0;
                        row[11] = 0;
                        row[12] = 0;
                        row[13] = 0;
                        row[14] = 0;
                        row[15] = 0;
                        row[16] = 0;
                        table.Rows.Add(row);
                        row = table.NewRow();
                        if (DataMonth) row[0] = dt.AddMonths(1);
                        else row[0] = dt.AddYears(1);
                        row[1] = 0;
                        row[2] = 0;
                        row[3] = 0;
                        row[4] = 0;
                        row[5] = 0;
                        row[6] = 0;
                        row[7] = 0;
                        row[8] = 0;
                        row[9] = 0;
                        row[10] = 0;
                        row[11] = 0;
                        row[12] = 0;
                        row[13] = 0;
                        row[14] = 0;
                        row[15] = 0;
                        row[16] = 0;
                        table.Rows.Add(row);
                    }
                    minDT = (DateTime)table.Rows[0].ItemArray[0];
                    maxDT = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[0];

                    if (dt < minDT)
                    {
                        ind = 0;
                        while (dt < minDT)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            row = table.NewRow();
                            row[0] = dt;
                            row[1] = 0;
                            row[2] = 0;
                            row[3] = 0;
                            row[4] = 0;
                            row[5] = 0;
                            row[6] = 0;
                            row[7] = 0;
                            row[8] = 0;
                            row[9] = 0;
                            row[10] = 0;
                            row[11] = 0;
                            row[12] = 0;
                            row[13] = 0;
                            row[14] = 0;
                            row[15] = 0;
                            row[16] = 0;
                            table.Rows.InsertAt(row, ind);
                            if (DataMonth) dt = dt.AddMonths(1);
                            else dt = dt.AddYears(1);
                            ind++;
                        }
                    }
                    dt = SumParams[SumParams.Length - 1].Date;
                    if (dt > maxDT)
                    {
                        while (maxDT < dt)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            if (DataMonth) maxDT = maxDT.AddMonths(1);
                            else maxDT = maxDT.AddYears(1);
                            row = table.NewRow();
                            row[0] = maxDT;
                            row[1] = 0;
                            row[2] = 0;
                            row[3] = 0;
                            row[4] = 0;
                            row[5] = 0;
                            row[6] = 0;
                            row[7] = 0;
                            row[8] = 0;
                            row[9] = 0;
                            row[10] = 0;
                            row[11] = 0;
                            row[12] = 0;
                            row[13] = 0;
                            row[14] = 0;
                            row[15] = 0;
                            row[16] = 0;
                            table.Rows.Add(row);
                        }
                    }
                    minDT = (DateTime)table.Rows[0].ItemArray[0];
                    maxDT = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[0];

                    int sumLength = SumParams.Length;
                    dt = SumParams[0].Date;
                    if (this.DataMonth) ind = (dt.Year - minDT.Year - 1) * 12 + (12 - minDT.Month) + dt.Month;
                    else ind = dt.Year - minDT.Year;
                    for (i = 0; i < sumLength; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        item = SumParams[i];
                        row = table.Rows[ind];

                        row[9] = (double)row[9] + item.Liq;
                        row[10] = (double)row[10] + item.Oil;
                        if ((double)row[9] > 0) 
                            row[11] = Math.Round(((double)row[9] - (double)row[10]) * 100 / (double)row[9]);
                        row[12] = (double)row[12] + item.Inj;
                        row[13] = (int)row[13] + item.LeadInAll;
                        row[14] = (int)row[14] + item.LeadInProd;
                        row[15] = (int)row[15] + item.Fprod;
                        row[16] = (int)row[16] + item.Finj;
                       
                        ind++;
                    }
                    table.AcceptChanges();
                    DataLoaded = true;
                }
            }
            private void SubTable81Params(BackgroundWorker worker, DoWorkEventArgs e, SumParamItem[] SumParams)
            {
                if (SumParams == null) return;
                DataRow row;
                OilField of;
                int i, j, ind, ind2, len, progress, objIndex;
                DataLoading = true;
                DataLoaded = false;
                SumParamItem item;

                if ((SumParams.Length > 0) && (table.Rows.Count > 0))
                {
                    DateTime dt, minDT, maxDT, minOFListDT, maxOFListDT;
                    len = table.Rows.Count;
                    minDT = (DateTime)table.Rows[0].ItemArray[0];
                    maxDT = (DateTime)table.Rows[len - 1].ItemArray[0];

                    int sumLength = SumParams.Length;

                    if ((SumParams[0].Date <= maxDT) || (SumParams[SumParams.Length - 1].Date >= minDT))
                    {
                        dt = SumParams[0].Date;
                        if (dt >= minDT)
                        {
                            ind = 0;
                            if (this.DataMonth) ind2 = (dt.Year - minDT.Year - 1) * 12 + (12 - minDT.Month) + dt.Month;
                            else ind2 = dt.Year - minDT.Year;
                        }
                        else
                        {
                            if (this.DataMonth) ind = (minDT.Year - dt.Year - 1) * 12 + (12 - dt.Month) + minDT.Month;
                            else ind = minDT.Year - dt.Year;
                            ind2 = 0;
                        }

                        for (i = ind; i < sumLength; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            if (ind2 >= len) break;
                            item = SumParams[i];
                            row = table.Rows[ind2];
                            row[9] = (double)row[9] - item.Liq;
                            row[10] = (double)row[10] - item.Oil;
                            if ((double)row[9] > 0) 
                                row[11] = Math.Round(((double)row[9] - (double)row[10]) * 100 / (double)row[9]);
                            row[12] = (double)row[12] - item.Inj;
                            row[13] = (int)row[13] - item.LeadInAll;
                            row[14] = (int)row[14] - item.LeadInProd; 
                            row[15] = (int)row[15] - item.Fprod;
                            row[16] = (int)row[16] - item.Finj;
                            ind2++;
                        }
                        table.AcceptChanges();
                    }
                    DataLoaded = true;
                }
            }

            private void copySelectedRowsToClipboard(DataGridView dgv)
            {
                dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

                Clipboard.Clear();
                if (dgv.GetClipboardContent() != null)
                {
                    Clipboard.SetDataObject(dgv.GetClipboardContent());
                    Clipboard.GetData(DataFormats.Text);
                    IDataObject dt = Clipboard.GetDataObject();
                    if (dt.GetDataPresent(typeof(string)))
                    {
                        string tb = (string)(dt.GetData(typeof(string)));
                        System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                        byte[] dataStr = encoding.GetBytes(tb);
                        Clipboard.SetDataObject(encoding.GetString(dataStr));
                    }
                }
                dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            }
            void DataGridSumParams_KeyDown(object sender, KeyEventArgs e)
            {
                if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
                    if (((DataGridView)sender).SelectedCells.Count > 0)
                    {
                        copySelectedRowsToClipboard((DataGridView)sender);
                    }
            }
            public void RePaintRows()
            {
                DateTime dt, predDt;
                bool b = false;
                predDt = DateTime.Now;
                if (this.Rows.Count > 0) predDt = Convert.ToDateTime(this.Rows[0].Cells[0].Value);

                for (int i = 0; i < this.Rows.Count; i++)
                {
                    dt = Convert.ToDateTime(this.Rows[i].Cells[0].Value);
                    if (predDt.Year != dt.Year)
                    {
                        predDt = dt;
                        b = !b;
                    }
                    if (b) this.Rows[i].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
            void DataGridSumParams_Resize(object sender, EventArgs e)
            {
                if ((!DataLoaded) && (!DataLoading)) this.Invalidate();
            }
            void DataGridSumParams_Paint(object sender, PaintEventArgs e)
            {
                //if ((!DataLoaded) && (!DataLoading))
                //{
                //    System.Drawing.Graphics gr = e.Graphics;
                //    gr.DrawString("Нет данных для отображения", _font, Brushes.Black, this.Left + (int)(this.Width / 2), this.Top + (int)(this.Height / 2));
                //}
            }
        }

        sealed class DataGridSumParamsChess : DataGridView
        {
            Project _proj;
            MainForm mainForm;
            bool InThousand;
            DataSet _dsSumParams;
            public DataTable table;
            bool DataLoaded, DataLoading;
            Font _font, _font_bold;
            public bool DataMonth;
            public bool bRePaint;
            ArrayList OFIndexes;
            DataGridViewCellStyle TableCellStyleAll, TableCellStyleFloat, TableCellStyleDate;
            string[] tableHeaderText = { "Дата", "Жидк, тыс.т", "Нефть, тыс.т", "%", "Фдоб, шт"};
            int[] columnWidth = { 62, 60, 60, 22, 33 };

            public DataGridSumParamsChess(MainForm MainForm, bool ByMotnh)
            {

                DataGridViewCellStyle HeaderCellStyle = new DataGridViewCellStyle();
                TableCellStyleAll = new DataGridViewCellStyle();
                TableCellStyleFloat = new DataGridViewCellStyle();
                TableCellStyleDate = new DataGridViewCellStyle();
                DataGridViewCellStyle TableCellStyleAlternating = new DataGridViewCellStyle();

                DataMonth = ByMotnh;

                DataLoaded = false;
                DataLoading = false;
                bRePaint = false;
                _font = new Font("Calibri", 8.25f);
                _font_bold = new Font("Calibri", 8.25f, FontStyle.Bold);

                mainForm = MainForm;

                this.Parent = this.mainForm.scGraphics.Panel2;
                this.Visible = false;
                //this.Parent = mainForm.tpSumChess;
                //if(DataMonth) this.Parent = mainForm.tp;
                //else this.Parent = mainForm.tpSumYears;

                this.Dock = DockStyle.Fill;

                
                _dsSumParams = new DataSet("dsSumParams");
                table = new DataTable("SUM");
                OFIndexes = new ArrayList();

                table.Columns.Add("DATE", Type.GetType("System.DateTime"));         // 0
                table.Columns.Add("LIQ", Type.GetType("System.Double"));            // 1
                table.Columns.Add("OIL", Type.GetType("System.Double"));            // 2
                table.Columns.Add("WATERING", Type.GetType("System.Double"));       // 3
                table.Columns.Add("FPROD", Type.GetType("System.Int32"));           // 6

                _dsSumParams.Tables.Add(table);
                this.DataSource = _dsSumParams.Tables[0];

                TableCellStyleFloat.Format = "0.##";
                TableCellStyleFloat.Font = _font;


                TableCellStyleDate.Format = "dd.MM.yyyy";
                
                TableCellStyleDate.Font = _font;


                TableCellStyleAll.Font = _font;

                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (i == 0) this.Columns[i].DefaultCellStyle = TableCellStyleDate;
                    else if ((i > 0) && (i < 3)) this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    else this.Columns[i].DefaultCellStyle = TableCellStyleAll;
                    this.Columns[i].Width = columnWidth[i];
                    this.Columns[i].HeaderText = tableHeaderText[i];
                }
                //if (!DataMonth) this.Columns[0].Width = columnWidth[9];

                // Настройка таблицы Шахматка
                this.RowTemplate.Height = 13;
                this.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                this.ColumnHeadersHeight = 18;
                this.EnableHeadersVisualStyles = false;
                this.AllowUserToResizeRows = false;
                this.AllowUserToAddRows = false;
                this.AllowUserToDeleteRows = false;
                this.RowHeadersVisible = false;
                this.ReadOnly = true;
                this.DoubleBuffered = true;
                this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                if (!DataMonth)
                {
                    TableCellStyleAlternating.BackColor = Color.LightGray;
                    this.AlternatingRowsDefaultCellStyle = TableCellStyleAlternating;
                }

                HeaderCellStyle.Font = new System.Drawing.Font("Calibri", 8.25f, System.Drawing.FontStyle.Bold);
                HeaderCellStyle.BackColor = System.Drawing.SystemColors.Control;
                HeaderCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
                HeaderCellStyle.WrapMode = DataGridViewTriState.NotSet;
                this.BackgroundColor = SystemColors.ControlDark;

                this.ColumnHeadersDefaultCellStyle = HeaderCellStyle;


                this.Resize += new EventHandler(DataGridSumParams_Resize);
                this.Paint += new PaintEventHandler(DataGridSumParams_Paint);
                this.KeyDown += new KeyEventHandler(DataGridSumParams_KeyDown);
            }


            void SetColumnsFormat()
            {
                TableCellStyleFloat = new DataGridViewCellStyle();
                TableCellStyleFloat.Format = "#,#";
                TableCellStyleFloat.Font = _font;
                if (InThousand)
                    TableCellStyleFloat.Format += "0,";
                else
                    TableCellStyleFloat.Format += "0.##";
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if ((i == 1) || (i == 2))
                    {

                        this.Columns[i].DefaultCellStyle = TableCellStyleFloat;

                        string str = tableHeaderText[i];
                        if (!InThousand) str = str.Replace("тыс.", "");
                        this.Columns[i].HeaderText = str;
                    }
                }

            }
            public void SetProject(Project aProject)
            {
                _proj = aProject;
            }
            public void ClearTable()
            {
                DataLoading = true;
                OFIndexes.Clear();
                table.Clear();
                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();
                DataLoaded = false;
                DataLoading = false;
                this.BackgroundColor = SystemColors.ControlDark;
            }
            public void ClearDataSet()
            {
                this.DataSource = null;
            }
            public void SetDataSource()
            {
                this.DataSource = _dsSumParams.Tables[0];
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (i == 0) this.Columns[i].DefaultCellStyle = TableCellStyleDate;
                    else if ((i > 0) && (i < 3)) this.Columns[i].DefaultCellStyle = TableCellStyleFloat;
                    else this.Columns[i].DefaultCellStyle = TableCellStyleAll;
                    this.Columns[i].Width = columnWidth[i];
                    this.Columns[i].HeaderText = tableHeaderText[i];
                }
                SetColumnsFormat();
                this.ScrollBars = ScrollBars.None;
                this.RePaintRows();
                this.ScrollBars = ScrollBars.Both;
                if ((this.Rows.Count > 0) && (this.ClientRectangle.Height > 15))
                    this.FirstDisplayedScrollingRowIndex = this.Rows.Count - 1;
                this.BackgroundColor = Color.White;
            }

            public void ShowSumParams(BackgroundWorker worker, DoWorkEventArgs e, CanvasMap canv)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Пересчет суммарных показателей";

                table.Clear();
                OFIndexes.Clear();

                if (canv != null)
                {
                    if (canv.sumChessParams == null) return;
                    userState.Element = "";

                    DataRow row;
                    int i, iLen;
                    DataLoading = true;
                    DataLoaded = false;
                    SumParamItem item;
                    SumParamItem[] SumParams;
                    SumParams = canv.sumChessParams[0].MonthlyItems;
                    InThousand = false;
                    iLen = SumParams.Length;
                    worker.ReportProgress(0, userState);
                    if (SumParams.Length > 0)
                    {
                        int sumLength = SumParams.Length;
                        for (i = 0; i < sumLength; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            item = SumParams[i];
                            row = table.NewRow();
                            row[0] = item.Date;
                            row[1] = item.Liq * 1000;
                            row[2] = item.Oil * 1000;
                            row[3] = Math.Round(item.Watering * 100);
                            row[4] = item.Fprod;
                            table.Rows.Add(row);
                            worker.ReportProgress(i, userState);
                        }
                        DataLoaded = true;
                    }
                    bRePaint = false;
                    DataLoading = false;
                }
            }

            public void ShowSumParams(BackgroundWorker worker, DoWorkEventArgs e, OilField of)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Пересчет суммарных показателей";

                table.Clear();
                OFIndexes.Clear();

                if (of != null)
                {
                    if (of.sumChessParams == null) return;
                    userState.Element = of.Name;

                    OFIndexes.Add(of.Index);
                    DataRow row;
                    int i, iLen, progress;
                    DataLoading = true;
                    DataLoaded = false;
                    SumParamItem item;
                    SumParamItem[] SumParams;
                    SumParams = of.sumChessParams[0].MonthlyItems;
                    
                    iLen = SumParams.Length;
                    worker.ReportProgress(0, userState);
                    if (SumParams.Length > 0)
                    {
                        int sumLength = SumParams.Length;
                        for (i = 0; i < sumLength; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            item = SumParams[i];
                            row = table.NewRow();
                            row[0] = item.Date;
                            row[1] = item.Liq;
                            row[2] = item.Oil;
                            row[3] = Math.Round(item.Watering * 100);
                            row[4] = item.Fprod;
                            table.Rows.Add(row);
                            progress = (int)((float)i * 100 / (float)iLen);
                            worker.ReportProgress(progress, userState);
                        }
                        DataLoaded = true;
                    }
                    bRePaint = false;
                    worker.ReportProgress(100, userState);
                    DataLoading = false;
                }
            }

            public void AddSumParams(BackgroundWorker worker, DoWorkEventArgs e, OilField of)
            {
                if (of != null)
                {
                    WorkerState userState = new WorkerState();
                    userState.WorkMainTitle = "";
                    userState.WorkCurrentTitle = "Пересчет суммарных показателей";

                    if (of.sumChessParams == null) return;
                    userState.Element = of.Name;

                    bool find = false;
                    int i, ind, iLen, progress;
                    if (OFIndexes.Count == 0)
                    {
                        ShowSumParams(worker, e, of);
                        return;
                    }
                    for (i = 0; i < OFIndexes.Count; i++)
                    {
                        if ((int)OFIndexes[i] == of.Index)
                        {
                            find = true;
                            break;
                        }
                    }
                    if (find)
                    {
                        SubSumParams(worker, e, of);
                    }
                    else
                    {

                        OFIndexes.Add(of.Index);
                        DataRow row;

                        DataLoading = true;
                        DataLoaded = false;

                        SumParamItem item;
                        SumParamItem[] SumParams;
                        SumParams = of.sumChessParams[0].MonthlyItems;
                        
                        worker.ReportProgress(0, userState);

                        if (SumParams.Length > 0)
                        {
                            DateTime dt, minDT, maxDT;
                            dt = SumParams[0].Date;
                            minDT = (DateTime)table.Rows[0].ItemArray[0];
                            maxDT = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[0];

                            if (dt < minDT)
                            {
                                ind = 0;
                                while (dt < minDT)
                                {
                                    if (worker.CancellationPending)
                                    {
                                        e.Cancel = true;
                                        return;
                                    }
                                    row = table.NewRow();
                                    row[0] = dt;
                                    row[1] = 0;
                                    row[2] = 0;
                                    row[3] = 0;
                                    row[4] = 0;
                                    table.Rows.InsertAt(row, ind);
                                    dt = dt.AddDays(1);
                                    ind++;
                                }
                            }
                            dt = SumParams[SumParams.Length - 1].Date;
                            if (dt > maxDT)
                            {
                                while (maxDT <= dt)
                                {
                                    if (worker.CancellationPending)
                                    {
                                        e.Cancel = true;
                                        return;
                                    }
                                    maxDT = maxDT.AddDays(1);
                                    row = table.NewRow();
                                    row[0] = maxDT;
                                    row[1] = 0;
                                    row[2] = 0;
                                    row[3] = 0;
                                    row[4] = 0;
                                    table.Rows.Add(row);
                                }
                            }
                            minDT = (DateTime)table.Rows[0].ItemArray[0];
                            maxDT = (DateTime)table.Rows[table.Rows.Count - 1].ItemArray[0];

                            int sumLength = SumParams.Length;
                            dt = SumParams[0].Date;
                            TimeSpan ts = dt - minDT;
                            //ind = (dt.Year - minDT.Year - 1) * 12 + (12 - minDT.Month) + dt.Month;
                            ind = ts.Days;
                            for (i = 0; i < sumLength; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return;
                                }
                                item = SumParams[i];
                                row = table.Rows[ind];
                                row[1] = (double)row[1] + item.Liq;
                                row[2] = (double)row[2] + item.Oil;
                                if ((double)row[1] > 0) row[3] = Math.Round(((double)row[1] - (double)row[2]) * 100 / (double)row[1]);
                                row[4] = (int)row[4] + item.Fprod;
                                progress = (int)((float)i * 100 / (float)sumLength);
                                worker.ReportProgress(progress, userState);
                                ind++;
                            }
                            table.AcceptChanges();
                            DataLoaded = true;
                        }
                        worker.ReportProgress(100, userState);
                    }
                    bRePaint = false;
                }
            }

            public void SubSumParams(BackgroundWorker worker, DoWorkEventArgs e, OilField of)
            {
                if (of != null)
                {
                    WorkerState userState = new WorkerState();
                    userState.WorkMainTitle = "";
                    userState.WorkCurrentTitle = "Пересчет суммарных показателей";

                    if (of.sumChessParams == null) return;
                    userState.Element = of.Name;

                    DataRow row;
                    int i, ind, ind2, len, progress;
                    DataLoading = true;
                    DataLoaded = false;

                    SumParamItem item;
                    SumParamItem[] SumParams;
                    SumParams = of.sumChessParams[0].MonthlyItems;
                    
                    for (i = 0; i < OFIndexes.Count; i++)
                    {
                        if ((int)OFIndexes[i] == of.Index)
                        {
                            OFIndexes.RemoveAt(i);
                            break;
                        }
                    }
                    worker.ReportProgress(0, userState);
                    if (SumParams.Length > 0)
                    {
                        DateTime dt, minDT, maxDT, minOFListDT, maxOFListDT;
                        minOFListDT = DateTime.MaxValue;
                        maxOFListDT = DateTime.MinValue;
                        SumParamItem[] sum;
                        for (i = 0; i < OFIndexes.Count; i++)
                        {
                            if ((_proj.OilFields[(int)OFIndexes[i]]).sumChessParams != null)
                            {

                                sum = (_proj.OilFields[(int)OFIndexes[i]]).sumChessParams[0].MonthlyItems;

                                if (sum[0].Date < minOFListDT) minOFListDT = sum[0].Date;
                                if (sum[sum.Length - 1].Date > maxOFListDT) maxOFListDT = sum[sum.Length - 1].Date;
                            }
                        }
                        if ((minOFListDT == DateTime.MaxValue) && (maxOFListDT == DateTime.MinValue)) return;
                        minDT = (DateTime)table.Rows[0].ItemArray[0];
                        while (minDT != minOFListDT)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            table.Rows.RemoveAt(0);
                            if (table.Rows.Count > 0) minDT = (DateTime)table.Rows[0].ItemArray[0];
                        }
                        len = table.Rows.Count;
                        maxDT = (DateTime)table.Rows[len - 1].ItemArray[0];
                        while ((maxDT != maxOFListDT) && (len > 0))
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            table.Rows.RemoveAt(len - 1);
                            len--;
                            maxDT = (DateTime)table.Rows[len - 1].ItemArray[0];
                        }

                        minDT = minOFListDT;
                        maxDT = maxOFListDT;

                        int sumLength = SumParams.Length;
                        TimeSpan ts;
                        if ((SumParams[0].Date <= maxDT) || (SumParams[SumParams.Length - 1].Date >= minDT))
                        {
                            dt = SumParams[0].Date;
                            if (dt >= minDT)
                            {
                                ind = 0;
                                ts = dt - minDT;
                                ind2 = ts.Days;
                                //ind2 = (dt.Year - minDT.Year - 1) * 12 + (12 - minDT.Month) + dt.Month;
                            }
                            else
                            {
                                //ind = (minDT.Year - dt.Year - 1) * 12 + (12 - dt.Month) + minDT.Month;
                                ts = minDT - dt;
                                ind = ts.Days;
                                ind2 = 0;
                            }
                            len = table.Rows.Count;
                            for (i = ind; i < sumLength; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return;
                                }
                                if (ind2 >= len) break;
                                item = SumParams[i];
                                row = table.Rows[ind2];
                                row[1] = (double)row[1] - item.Liq;
                                row[2] = (double)row[2] - item.Oil;
                                if ((double)row[1] > 0) row[3] = Math.Round(((double)row[1] - (double)row[2]) * 100 / (double)row[1]);
                                row[4] = (int)row[4] - item.Fprod;
                                progress = (int)((float)i * 100 / (float)sumLength);
                                worker.ReportProgress(progress, userState);
                                ind2++;
                            }
                            table.AcceptChanges();
                        }
                        DataLoaded = true;
                    }
                    bRePaint = false;
                    worker.ReportProgress(100, userState);
                }
            }

            private void copySelectedRowsToClipboard(DataGridView dgv)
            {
                dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

                Clipboard.Clear();
                if (dgv.GetClipboardContent() != null)
                {
                    Clipboard.SetDataObject(dgv.GetClipboardContent());
                    Clipboard.GetData(DataFormats.Text);
                    IDataObject dt = Clipboard.GetDataObject();
                    if (dt.GetDataPresent(typeof(string)))
                    {
                        string tb = (string)(dt.GetData(typeof(string)));
                        System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1251);
                        byte[] dataStr = encoding.GetBytes(tb);
                        Clipboard.SetDataObject(encoding.GetString(dataStr));
                    }
                }
                dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            }
            void DataGridSumParams_KeyDown(object sender, KeyEventArgs e)
            {
                if (e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.Insert))
                    if (((DataGridView)sender).SelectedCells.Count > 0)
                    {
                        copySelectedRowsToClipboard((DataGridView)sender);
                    }
            }
            public void RePaintRows()
            {
                DateTime dt, predDt;
                bool b = false;
                predDt = DateTime.Now;
                if (this.Rows.Count > 0) predDt = Convert.ToDateTime(this.Rows[0].Cells[0].Value);

                for (int i = 0; i < this.Rows.Count; i++)
                {
                    dt = Convert.ToDateTime(this.Rows[i].Cells[0].Value);
                    if (predDt.Month != dt.Month)
                    {
                        predDt = dt;
                        b = !b;
                    }
                    if (b) this.Rows[i].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
            void DataGridSumParams_Resize(object sender, EventArgs e)
            {
                if ((!DataLoaded) && (!DataLoading)) this.Invalidate();
            }
            void DataGridSumParams_Paint(object sender, PaintEventArgs e)
            {
                //if ((!DataLoaded) && (!DataLoading))
                //{
                //    System.Drawing.Graphics gr = e.Graphics;
                //    gr.DrawString("Нет данных для отображения", _font, Brushes.Black, this.Left + (int)(this.Width / 2), this.Top + (int)(this.Height / 2));
                //}
            }
        }
    #endregion

    #region Фильтр объектов
    public sealed class FilterOilObjects : TreeView
    {
        Project proj;
        MainForm mainForm;
        public StratumDictionary stratumDict;
        bool updateTree, updateAll;
        public bool AllSelected;
        public bool AllCurrentSelected;
        public bool AllCurrentOilFieldSelected;
        ImageList StateImgList;

        public ArrayList AllObjects;
        public ArrayList CurrentObjects;
        public ArrayList CurrentOilFieldObjects;

        public ArrayList SelectedObjects;
        public ArrayList SelectedCurrentObjects;
        public ArrayList SelectedOilFieldObjects;

        #region ContextMenuStrip
        
        public ContextMenuStrip cMenu;
        ToolStripMenuItem cmSelectAll, cmDeSelectAll, cmDeSelectOther;

        private void InitializeContextMenus()
        {
            cMenu = new ContextMenuStrip();
            // Выбрать все
            cmSelectAll = (ToolStripMenuItem)cMenu.Items.Add("");
            cmSelectAll.Text = "Включить все";
            cmSelectAll.Image = Properties.Resources.SellAll;
            cmSelectAll.Click += new EventHandler(cmSelectAll_Click);
            // Откл все
            cmDeSelectAll = (ToolStripMenuItem)cMenu.Items.Add("");
            cmDeSelectAll.Text = "Отключить все";
            cmDeSelectAll.Image = Properties.Resources.DeSellAll;
            cmDeSelectAll.Click += new EventHandler(cmDeSelectAll_Click);
            // Откл остальные
            cmDeSelectOther = (ToolStripMenuItem)cMenu.Items.Add("");
            cmDeSelectOther.Text = "Отключить остальные";
            cmDeSelectOther.Image = Properties.Resources.DeSellOther;
            cmDeSelectOther.Click += new EventHandler(cmDeSelectOther_Click);
        }
        void CheckNodeRecursive(TreeNode tn, bool Check, bool OnUpdateTree, TreeNode IncluseNode)
        {
            if (tn != null)
            {
                for (int i = 0; i < tn.Nodes.Count; i++)
                {
                    CheckNodeRecursive(tn.Nodes[i], Check, true, IncluseNode);
                }
                if (!OnUpdateTree) this.updateTree = false;
                if ((IncluseNode == null) || (IncluseNode != tn))
                {
                    tn.Checked = Check;
                }
            }
        }
        void CheckNodeRecursive(TreeNode tn, int Index, bool Check)
        {
            if (tn != null)
            {
                for (int i = 0; i < tn.Nodes.Count; i++)
                {
                    CheckNodeRecursive(tn.Nodes[i], Index, Check);
                }
                if ((tn.Tag != null) && ((int)tn.Tag == Index))
                {
                    tn.Checked = Check;
                }
            }
        }
        void cmSelectAll_Click(object sender, EventArgs e)
        {
            this.BeginUpdate();
            if (this.SelectedNode != null)
            {
                this.updateTree = true;
                TreeNode tnParent = this.SelectedNode;
                while (tnParent.Parent != null) tnParent = tnParent.Parent;

                if (tnParent != null)
                {
                    tnParent.Checked = true;
                    for (int i = 0; i < tnParent.Nodes.Count; i++)
                    {
                        CheckNodeRecursive(tnParent.Nodes[i], true, (i != tnParent.Nodes.Count - 1), null);
                    }
                }
            }
            this.EndUpdate();
        }
        void cmDeSelectAll_Click(object sender, EventArgs e)
        {
            this.BeginUpdate();
            if (this.SelectedNode != null)
            {
                this.updateTree = true;
                TreeNode tnParent = this.SelectedNode;
                while (tnParent.Parent != null) tnParent = tnParent.Parent;
                if (tnParent != null)
                {
                    tnParent.Checked = false;
                    for (int i = 0; i < tnParent.Nodes.Count; i++)
                    {
                        CheckNodeRecursive(tnParent.Nodes[i], false, (i != tnParent.Nodes.Count - 1), null);
                    }
                }
            }
            this.EndUpdate();
        }
        void cmDeSelectOther_Click(object sender, EventArgs e)
        {
            this.BeginUpdate();
            if (this.SelectedNode != null)
            {
                this.updateTree = true;
                TreeNode tnParent = this.SelectedNode;
                while (tnParent.Parent != null) tnParent = tnParent.Parent;
                if (tnParent != null)
                {
                    for (int i = 0; i < tnParent.Nodes.Count; i++)
                    {
                        CheckNodeRecursive(tnParent.Nodes[i], false, true, this.SelectedNode);
                    }
                    for (int i = 0; i < this.SelectedNode.Nodes.Count; i++)
                    {
                        CheckNodeRecursive(this.SelectedNode.Nodes[i], true, true, null);
                    }
                }
                this.updateTree = false;
                this.SelectedNode.Checked = true;
            }
            this.EndUpdate();
        }
        #endregion

        public FilterOilObjects(MainForm MainForm)
        {
            this.Dock = DockStyle.Fill;
            this.mainForm = MainForm;
            this.Parent = mainForm.pFilterOilObj;
            updateTree = false;
            updateAll = false;
            AllSelected = true;
            AllObjects = new ArrayList();
            CurrentObjects = new ArrayList();
            CurrentOilFieldObjects = new ArrayList();

            SelectedObjects = new ArrayList();
            SelectedCurrentObjects = new ArrayList();
            SelectedOilFieldObjects = new ArrayList();

            this.Font = new Font("Calibri", 8.25f);
            this.HideSelection = false;

            this.StateImgList = new ImageList();
            this.StateImgList.Images.Add(SmartPlus.Properties.Resources.unchecked16);
            this.StateImgList.Images.Add(SmartPlus.Properties.Resources.checked16);
            this.StateImageList = StateImgList;

            NativeMethods.SetWindowTheme(this.Handle, "explorer", null);
           
            this.Nodes.Add("Все объекты разработки");
            this.Nodes.Add("По текущему месторождению");
            this.Nodes.Add("По текущему объекту");

            this.Nodes[0].Checked = false;
            this.Nodes[1].Checked = false;
            this.Nodes[2].Checked = false;
            
            this.InitializeContextMenus();
            this.Click += new EventHandler(FilterOilObjects_Click);
            this.AfterCheck += new TreeViewEventHandler(FilterOilObjects_AfterCheck);
            this.MouseDown += new MouseEventHandler(FilterOilObjects_MouseDown);
        }

        public void SortByOilObjHierarchy()
        {
            if (this.Nodes.Count > 1)
            {
                int i, j, ind, min, x;
                TreeNode copyNode, rootNode, parNode, tn;
                int plastCode;
                StratumTreeNode node;
                // сортировка текущих и по месторождению
                for (int k = 1; k < 3; k++)
                {
                    rootNode = this.Nodes[k];
                    for (i = 0; i < rootNode.Nodes.Count; i++)
                    {
                        tn = rootNode.Nodes[i];
                        ind = (int)tn.Tag;
                        plastCode = stratumDict[ind].Code;
                        node = stratumDict.GetStratumTreeNode(plastCode);
                        x = -1; min = -1;
                        for (j = 0; j < rootNode.Nodes.Count; j++)
                        {
                            if (i != j)
                            {
                                ind = -1;
                                plastCode = (int)rootNode.Nodes[j].Tag;
                                if (plastCode != -1)
                                {
                                    ind = node.GetParentLevelByCode(plastCode);
                                    if (ind != -1)
                                    {
                                        if ((min == -1) || (min > ind))
                                        {
                                            x = j;
                                            min = ind;
                                        }
                                    }
                                }
                            }
                        }
                        if (x != -1)
                        {
                            parNode = rootNode.Nodes[x];
                            copyNode = (TreeNode)tn.Clone();
                            parNode.Nodes.Add(copyNode);
                            CheckStateNode(tn, copyNode);
                            tn.Tag = -1;
                        }
                    }
                    i = 0;
                    while (i < rootNode.Nodes.Count)
                    {
                        if ((int)rootNode.Nodes[i].Tag == -1)
                        {
                            rootNode.Nodes.RemoveAt(i);
                        }
                        else
                        {
                            i++;
                        }
                    }
                }
            }
        }
        
        public void SetProject(Project Proj)
        {
            this.proj = Proj;
            this.stratumDict = (StratumDictionary)Proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
        }
        public void ClearProject() 
        { 
            this.proj = null; 
            this.ClearAll(); 
        }

        public void SetProjectStratumcodes()
        {
            for (int i = 0; i < proj.MerStratumCodes.Count; i++)
            {
                this.AddOilObj(proj.MerStratumCodes[i]);
            }
        }

        public void SetWellOilObj(Well w, bool ClearList)
        {
            if (ClearList) ClearCurrent();
            OilField of = proj.OilFields[w.OilFieldIndex];
            SetOilFieldOilObj(of, ClearList);
            if (w.ProjectDest == 0)
            {
                if (!w.MerLoaded) (proj.OilFields[w.OilFieldIndex]).LoadMerFromCache(w.Index);
                if ((w.MerLoaded) && (w.mer != null) && (w.mer.Count > 0))
                {
                    MerItem item;
                    ArrayList list = new ArrayList();
                    int i;
                    for (i = 0; i < w.mer.Count; i++)
                    {
                        item = w.mer.Items[i];
                        if (list.IndexOf(item.PlastId) == -1)
                            list.Add(item.PlastId);
                    }
                    for (i = 0; i < list.Count; i++)
                    {
                        this.AddCurrOilObj((int)list[i]);
                    }
                }
            }
            else if (w.icons.Count > 0)
            {
                for (int i = 0; i < w.icons.Count; i++)
                {
                    this.AddCurrOilObj(w.icons[i].StratumCode);
                }
            }
        }
        public void SetArrayListOilObj(ArrayList OilObjList, bool ClearList)
        {
            if (ClearList) ClearCurrent();
            for (int i = 0; i < OilObjList.Count; i++)
            {
                this.AddCurrOilObj((int)OilObjList[i]);
            }
            this.AllCurrentSelected = false;
            if (this.SelectedCurrentObjects.Count == this.CurrentObjects.Count)
            {
                this.AllCurrentSelected = true;
            }
        }
        public void SetArrayListOilFieldOilObj(List<int> OFIndexList, bool ClearList)
        {
            if (ClearList) ClearCurrentOilField();
            OilField of;
            for (int i = 0; i < OFIndexList.Count; i++)
            {
                of = proj.OilFields[OFIndexList[i]];
                SetOilFieldOilObj(of, false);
            }
            this.AllCurrentOilFieldSelected = false;
            if (this.SelectedOilFieldObjects.Count == this.CurrentOilFieldObjects.Count)
            {
                this.AllCurrentOilFieldSelected = true;
            }
        }
        public void SetAreaOilObj(Area area, bool ClearList)
        {
            OilField of = proj.OilFields[area.OilFieldIndex];
            SetOilFieldOilObj(of, ClearList);
            if (ClearList) ClearCurrent();
            this.AddCurrOilObj(area.PlastCode);
            if (this.SelectedCurrentObjects.Count == this.CurrentObjects.Count)
            {
                this.AllCurrentSelected = true;
            }
            //if (area.OilObjectList != null)
            //{
            //    for (int i = 0; i < area.OilObjectList.Length; i++)
            //    {
            //        this.AddCurrOilObj((int)area.OilObjectList[i]);
            //    }
            //    this.AllCurrentSelected = false;
            //    if (this.SelectedCurrentObjects.Count == this.CurrentObjects.Count)
            //    {
            //        this.AllCurrentSelected = true;
            //    }
            //}
        }
        public void SetOilFieldOilObj(OilField of, bool ClearList)
        {
            if (ClearList) ClearCurrent();
            if (ClearList) ClearCurrentOilField();
            if (of.MerStratumCodes.Count == 0) of.LoadStratumCodesFromCache();
            for (int i = 0; i < of.MerStratumCodes.Count; i++)
            {
                this.AddCurrOilFieldOilObj(of.MerStratumCodes[i]);
            }
            this.AllCurrentOilFieldSelected = false;
            if (this.SelectedOilFieldObjects.Count == this.CurrentOilFieldObjects.Count)
            {
                this.AllCurrentOilFieldSelected = true;
            }
        }
        public void SetNGDUOilObj(int NGDUCode, bool ClearList)
        {
            if (ClearList) ClearCurrent();
            if (ClearList) ClearCurrentOilField();
            if (NGDUCode > -1)
            {
                OilField of;
                for (int i = 0; i < proj.OilFields.Count; i++)
                {
                    of = proj.OilFields[i];
                    if (of.ParamsDict.NGDUCode == NGDUCode)
                    {
                        SetOilFieldOilObj(of, false);
                    }
                }
            }
        }
        public void SetRegionOilObj(int RegionCode, bool ClearList)
        {
            if (ClearList) ClearCurrent();
            if (ClearList) ClearCurrentOilField();
            if (RegionCode > -1)
            {
                OilField of;
                for (int i = 0; i < proj.OilFields.Count; i++)
                {
                    of = proj.OilFields[i];
                    if (of.ParamsDict.EnterpriseCode == RegionCode)
                    {
                        SetOilFieldOilObj(of, false);
                    }
                }
            }
        }
        
        void CheckStateNode(TreeNode SourceNode, TreeNode TargetNode)
        {
            if (SourceNode.Nodes.Count == TargetNode.Nodes.Count)
            {
                for (int i = 0; i < SourceNode.Nodes.Count; i++)
                {
                    CheckStateNode(SourceNode.Nodes[i], TargetNode.Nodes[i]);
                }
            }
            TargetNode.Checked = SourceNode.Checked;
        }

        public void ClearAll()
        {
            this.Nodes.Clear();
            this.AllSelected = false;
            this.AllCurrentSelected = false;
            this.AllCurrentOilFieldSelected = false;

            AllObjects.Clear();
            CurrentOilFieldObjects.Clear();
            CurrentObjects.Clear();
            SelectedObjects.Clear();
            SelectedCurrentObjects.Clear();
            SelectedOilFieldObjects.Clear();
            this.Nodes.Add("Все объекты разработки");
            this.Nodes.Add("По текущему месторождению");
            this.Nodes.Add("По текущему объекту");
            this.Nodes[0].Checked = false;
            this.Nodes[1].Checked = false;
            this.Nodes[2].Checked = false;
            this.Nodes[0].BackColor = Color.White;
            this.Nodes[1].BackColor = Color.White;
            this.Nodes[2].BackColor = Color.White;
        }
        public void ClearCurrentOilField()
        {
            this.Nodes[1].Nodes.Clear();
            this.Nodes[1].BackColor = Color.White;
            this.AllCurrentOilFieldSelected = false;
            CurrentOilFieldObjects.Clear();
            SelectedOilFieldObjects.Clear();
        }
        public void ClearCurrent()
        {
            this.Nodes[2].Nodes.Clear();
            this.Nodes[2].BackColor = Color.White;
            this.AllCurrentSelected = false;
            CurrentObjects.Clear();
            SelectedCurrentObjects.Clear();
        }

        public void UpdateTreeView()
        {
            this.updateTree = true;
            this.BeginUpdate();
            TreeNode tn = this.Nodes[2], tn2;
            int i, ind;
            mainForm.canv.UpdateFilterOilObj = true;
            tn.Nodes.Clear();
            tn.Checked = true;
            tn.ContextMenuStrip = cMenu;
            tn.Text = String.Format("По текущему объекту[{0}]", CurrentObjects.Count);
            for (i = 0; i < CurrentObjects.Count; i++)
            {
                ind = (int)CurrentObjects[i];
                tn2 = tn.Nodes.Add(stratumDict[ind].ShortName);
                tn2.Tag = ind;
                tn2.Checked = true;
                tn2.ContextMenuStrip = cMenu;
            }
            tn = this.Nodes[1];
            tn.Text = String.Format("По текущему месторождению[{0}]", CurrentOilFieldObjects.Count);
            tn.ContextMenuStrip = cMenu;
            tn.Nodes.Clear();
            tn.Checked = true;
            for (i = 0; i < CurrentOilFieldObjects.Count; i++)
            {
                ind = (int)CurrentOilFieldObjects[i];
                tn2 = tn.Nodes.Add(stratumDict[ind].ShortName);
                tn2.Tag = ind;
                tn2.Checked = true;
                tn2.ContextMenuStrip = cMenu;
            }
            tn = this.Nodes[0];
            tn.ContextMenuStrip = cMenu;
            tn.Text = String.Format("Все объекты разработки[{0}]", AllObjects.Count);
            tn.Nodes.Clear();
            tn.Checked = true;
            for (i = 0; i < AllObjects.Count; i++)
            {
                ind = (int)AllObjects[i];
                tn2 = tn.Nodes.Add(stratumDict[ind].ShortName);
                tn2.Tag = ind;
                tn2.Checked = true;
                tn2.ContextMenuStrip = cMenu;
            }
            this.EndUpdate();
            this.updateTree = false;
        }
        public void UpdateCurrentOilFieldTreeView()
        {
            this.updateTree = true;
            this.BeginUpdate();
            TreeNode tn = this.Nodes[1], tn2;
            int i, j, ind;
            bool check;
            tn.Nodes.Clear();
            tn.Checked = true;
            tn.Text = String.Format("По текущему месторождению[{0}]", CurrentOilFieldObjects.Count);
            for (i = 0; i < CurrentOilFieldObjects.Count; i++)
            {
                ind = (int)CurrentOilFieldObjects[i];
                check = true;
                for (j = 0; j < this.Nodes[0].Nodes.Count; j++)
                {
                    if (((int)this.Nodes[0].Nodes[j].Tag == ind) && (!this.Nodes[0].Nodes[j].Checked))
                    {
                        check = this.Nodes[0].Nodes[j].Checked;
                    }
                }
                tn2 = tn.Nodes.Add(stratumDict[ind].ShortName);
                tn2.Tag = ind;
                tn2.Checked = check;
                tn2.ContextMenuStrip = cMenu;
            }
            this.EndUpdate();
            this.updateTree = false;
        }
        public void UpdateCurrentTreeView()
        {
            UpdateCurrentOilFieldTreeView();
            this.updateTree = true;
            this.BeginUpdate();
            TreeNode tn = this.Nodes[2], tn2;
            int i, j, ind;
            bool check;
            tn.Nodes.Clear();
            tn.Checked = true;
            tn.Text = String.Format("По текущему объекту[{0}]", CurrentObjects.Count);
            for (i = 0; i < CurrentObjects.Count; i++)
            {
                ind = (int)CurrentObjects[i];
                check = true;
                for (j = 0; j < this.Nodes[0].Nodes.Count; j++)
                {
                    if (((int)this.Nodes[0].Nodes[j].Tag == ind) && (!this.Nodes[0].Nodes[j].Checked))
                        check = this.Nodes[0].Nodes[j].Checked;
                }
                tn2 = tn.Nodes.Add(stratumDict[ind].ShortName);
                tn2.Tag = ind;
                tn2.Checked = check;
                tn2.ContextMenuStrip = cMenu;
            }
            SortByOilObjHierarchy();
            if (this.Nodes[1].IsExpanded) this.Nodes[1].ExpandAll();
            this.Nodes[2].ExpandAll();

            this.EndUpdate();
            this.updateTree = false;
        }

        public void AddOilObj(int OilObjCode)
        {
            if ((stratumDict != null) && (OilObjCode > 0))
            {
                int ind = stratumDict.GetIndexByCode(OilObjCode);
                if (ind > 0)
                {
                    if ((AllObjects.Count == 0) || ((int)AllObjects[AllObjects.Count - 1] < ind))
                    {
                        AllObjects.Add(ind);
                    }
                    else
                    {
                        bool ins = false;
                        for (int i = 0; i < AllObjects.Count; i++)
                        {
                            if ((int)AllObjects[i] > ind)
                            {
                                AllObjects.Insert(i, ind);
                                ins = true;
                                break;
                            }
                        }
                        if (!ins) AllObjects.Insert(0, ind);
                    }
                }
            }
        }
        public void AddCurrOilFieldOilObj(int OilObjCode)
        {
            if (stratumDict != null)
            {
                int ind = stratumDict.GetIndexByCode(OilObjCode);
                if ((ind > 0) && (GetCurrOilFieldIndexByDictIndex(ind) == -1))
                {
                    if ((CurrentOilFieldObjects.Count == 0) || ((int)CurrentOilFieldObjects[CurrentOilFieldObjects.Count - 1] < ind))
                    {
                        CurrentOilFieldObjects.Add(ind);
                    }
                    else
                    {
                        bool ins = false;
                        for (int i = 0; i < CurrentOilFieldObjects.Count; i++)
                        {
                            if ((int)CurrentOilFieldObjects[i] > ind)
                            {
                                CurrentOilFieldObjects.Insert(i, ind);
                                ins = true;
                                break;
                            }
                        }
                        if (!ins) CurrentOilFieldObjects.Insert(0, ind);
                    }
                    if (SelectedObjects.IndexOf(ind) != -1) SelectedOilFieldObjects.Add(ind);
                    //if (GetAllIndexByDictIndex(ind) == -1) AddOilObj(OilObjCode);
                }
            }
        }
        public void AddCurrOilObj(int OilObjCode)
        {
            if (stratumDict != null)
            {
                int ind = stratumDict.GetIndexByCode(OilObjCode);
                if ((ind > 0) && (GetCurrIndexByDictIndex(ind) == -1))
                {
                    if ((CurrentObjects.Count == 0) || ((int)CurrentObjects[CurrentObjects.Count - 1] < ind))
                    {
                        CurrentObjects.Add(ind);
                    }
                    else
                    {
                        bool ins = false;
                        for (int i = 0; i < CurrentObjects.Count; i++)
                        {
                            if ((int)CurrentObjects[i] > ind)
                            {
                                CurrentObjects.Insert(i, ind);
                                ins = true;
                                break;
                            }
                        }
                        if (!ins) CurrentObjects.Insert(0, ind);
                    }

                    if (SelectedObjects.IndexOf(ind) != -1) SelectedCurrentObjects.Add(ind);
                    //if (GetAllIndexByDictIndex(ind) == -1) AddOilObj(OilObjCode);
                }
            }
        }
        public void ExpandCurrentOilField()
        {
            if (Nodes.Count == 3)
            {
                Nodes[1].Expand();
            }
        }

        public int GetAllSelectedIndexByDictCode(int dictCode)
        {
            for (int i = 0; i < SelectedObjects.Count; i++)
            {
                if (dictCode == stratumDict[(int)SelectedObjects[i]].Code)
                {
                    return i;
                }
            }
            return -1;
        }
        private int GetAllIndexByDictIndex(int dictIndex)
        {
            for (int i = 0; i < AllObjects.Count; i++)
            {
                if (dictIndex == (int)AllObjects[i]) return i;
            }
            return -1;
        }
        private int GetCurrOilFieldIndexByDictIndex(int dictIndex)
        {
            for (int i = 0; i < CurrentOilFieldObjects.Count; i++)
            {
                if (dictIndex == (int)CurrentOilFieldObjects[i]) return i;
            }
            return -1;
        }
        private int GetCurrIndexByDictIndex(int dictIndex)
        {
            for (int i = 0; i < CurrentObjects.Count; i++)
            {
                if (dictIndex == (int)CurrentObjects[i]) return i;
            }
            return -1;
        }

        void FilterOilObjects_Click(object sender, EventArgs e)
        {
            TreeView tw = (TreeView)sender;
            TreeNode tn;
            Point pointClick = tw.PointToClient(System.Windows.Forms.Cursor.Position);
            TreeNode clickedNode = tw.GetNodeAt(pointClick);
            // mainForm.StatUsage.AddMessage(CollectorStatId.FILTER_OILOBJ_MOUSE_DOWN);
            if (clickedNode != null)
            {
                if ((pointClick.X >= clickedNode.Bounds.X - 16) && (pointClick.X < clickedNode.Bounds.X)&&(((MouseEventArgs)e).Button == MouseButtons.Left))
                {
                    // mainForm.StatUsage.AddMessage(CollectorStatId.FOO_RESET_OBJ);
                    bool check = !clickedNode.Checked;
                    updateTree = true;
                    for (int i = 0; i < clickedNode.Nodes.Count; i++)
                    {
                        CheckNodeRecursive(clickedNode.Nodes[i], check, (i != clickedNode.Nodes.Count - 1), null);
                    }
                    updateTree = false;
                    clickedNode.Checked = check;
                }
                else if (pointClick.X >= clickedNode.Bounds.X)
                {
                    tw.SelectedNode = clickedNode;
                }

            }
        }
        void FilterOilObjects_AfterCheck(object sender, TreeViewEventArgs e)
        {
            TreeNode tn = e.Node;
            if (tn.Level > 0)
            {
                TreeNode parent = tn.Parent;
                while (parent.Parent != null) parent = parent.Parent;

                string path = tn.FullPath;
                if (parent.Index == 0) // все объекты
                {
                    parent = this.Nodes[0];
                    int ind = (int)tn.Tag;
                    int indAll = SelectedObjects.IndexOf(ind);
                    if (tn.Checked)
                    {
                        if (indAll == -1)
                        {
                            SelectedObjects.Add(ind);
                            SelectedObjects.Sort();
                        }
                        tn.BackColor = Color.White;
                    }
                    else
                    {
                        if (indAll > -1) SelectedObjects.RemoveAt(indAll);
                        tn.BackColor = Color.FromArgb(255, 206, 206);
                    }
                    if (SelectedObjects.Count == AllObjects.Count) 
                    {
                        this.AllSelected = true;
                        parent.BackColor = Color.White;
                    }
                    else 
                    {
                        this.AllSelected = false;
                        parent.BackColor = Color.FromArgb(255, 206, 206);
                    }
                    updateAll = true;
                    TreeNode tnCurr = this.Nodes[1];
                    for (int i = 0; i < tnCurr.Nodes.Count; i++)
                    {
                        CheckNodeRecursive(tnCurr.Nodes[i], ind, tn.Checked);
                    }
                    tnCurr = this.Nodes[2];
                    for (int i = 0; i < tnCurr.Nodes.Count; i++)
                    {
                        CheckNodeRecursive(tnCurr.Nodes[i], ind, tn.Checked);
                    }
                    updateAll = false;

                    if (!updateTree)
                    {
                        mainForm.canv.ReCalcSumObjects();
                    }
                }
                else if (parent.Index == 1) // текущие по месторождению
                {
                    parent = this.Nodes[1];
                    int ind = (int)tn.Tag;
                    int indCurr = SelectedOilFieldObjects.IndexOf(ind);
                    if (tn.Checked)
                    {
                        if (indCurr == -1)
                        {
                            SelectedOilFieldObjects.Add(ind);
                            SelectedOilFieldObjects.Sort();
                        }
                        tn.BackColor = Color.White;
                    }
                    else
                    {
                        if (indCurr > -1) SelectedOilFieldObjects.RemoveAt(indCurr);
                        tn.BackColor = Color.FromArgb(255, 206, 206);
                    }

                    if (SelectedOilFieldObjects.Count == CurrentOilFieldObjects.Count)
                    {
                        this.AllCurrentOilFieldSelected = true;
                        parent.BackColor = Color.White;
                    }
                    else
                    {
                        this.AllCurrentOilFieldSelected = false;
                        parent.BackColor = Color.FromArgb(255, 206, 206);
                    }
                    if (!updateAll)
                    {
                        TreeNode tnAll = this.Nodes[0];
                        for (int i = 0; i < tnAll.Nodes.Count; i++)
                        {
                            if (ind == (int)tnAll.Nodes[i].Tag)
                            {
                                tnAll.Nodes[i].Checked = tn.Checked;
                                break;
                            }
                        }
                    }
                }
                else if (parent.Index == 2) // текущие объекты
                {
                    parent = this.Nodes[2];
                    int ind = (int)tn.Tag;
                    int indCurr = SelectedCurrentObjects.IndexOf(ind);
                    if (tn.Checked)
                    {
                        if (indCurr == -1)
                        {
                            SelectedCurrentObjects.Add(ind);
                            SelectedCurrentObjects.Sort();
                        }
                        tn.BackColor = Color.White;
                    }
                    else
                    {
                        if (indCurr > -1) SelectedCurrentObjects.RemoveAt(indCurr);
                        tn.BackColor = Color.FromArgb(255, 206, 206);
                    }

                    if (SelectedCurrentObjects.Count == CurrentObjects.Count)
                    {
                        this.AllCurrentSelected = true;
                        parent.BackColor = Color.White;
                    }
                    else
                    {
                        this.AllCurrentSelected = false;
                        parent.BackColor = Color.FromArgb(255, 206, 206);
                    }
                    if (!updateAll)
                    {
                        TreeNode tnAll = this.Nodes[0];
                        for (int i = 0; i < tnAll.Nodes.Count; i++)
                        {
                            if (ind == (int)tnAll.Nodes[i].Tag)
                            {
                                tnAll.Nodes[i].Checked = tn.Checked;
                                break;
                            }
                        }
                    }
                }
            }
        }
        void FilterOilObjects_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point pointClick = e.Location;
                TreeNode clickedNode = this.GetNodeAt(pointClick);
                if (clickedNode != null)
                {
                    this.SelectedNode = clickedNode;
                    if (clickedNode.Level == 0)
                    {
                        cmDeSelectOther.Visible = false;
                    }
                    else
                    {
                        cmDeSelectOther.Visible = true;
                    }
                }
 
            }
        }

        public void LoadFromCache(string fileName)
        {
            ClearAll();
            if (stratumDict != null)
            {
                TreeNode tnAll = this.Nodes[0], tn;

                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open);
                System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                int ind, len;
                bool check;
                DataDictionaryItem item;
                len = br.ReadInt32();
                for (int i = 0; i < len; i++)
                {
                    ind = br.ReadInt32();
                    check = br.ReadBoolean();
                    item = stratumDict[ind];
                    AllObjects.Add(ind);
                    SelectedObjects.Add(ind);
                    tn = tnAll.Nodes.Add(item.ShortName);
                    tn.Tag = ind;
                    tn.Checked = true;
                }
            }
        }
        public void WriteToCache(string fileName)
        {
            TreeNode tn = this.Nodes[0];
            if (tn.Nodes.Count > 0)
            {
                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open);
                System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs);
                bw.Write(tn.Nodes.Count);
                for (int i = 0; i < tn.Nodes.Count; i++)
                {
                    bw.Write((int)tn.Nodes[i].Tag);
                    bw.Write(tn.Nodes[i].Checked);
                }
            }
        }
    }
    #endregion

    #region Палитра
    public struct PalettePoint
    {
        public double X;
        public Color Y;

        public static PalettePoint Empty
        {
            get
            {
                PalettePoint pp;
                pp.X = 0;
                pp.Y = Color.White;
                return pp;
            }
        }
    }
    public sealed class Palette
    {
        List<PalettePoint> points;

        public Palette()
        {
            points = new List<PalettePoint>();
        }
        public Palette(Palette pal) : this()
        {
            for (int i = 0; i < pal.points.Count; i++)
            {
                Add(pal[i].X, pal[i].Y);
            }
        }
        public int Count { get { return points.Count; } }

        public static Palette DefaultPalette(double Min, double Max)
        {
            Palette palette = new Palette();
            if (Min < Max)
            {
                palette.Add(Min, Color.Blue);
                palette.Add(Min + (Max - Min) / 4, Color.FromArgb(0, 255, 255));
                palette.Add(Min + (Max - Min) / 2, Color.FromArgb(0, 255, 0));
                palette.Add(Min + 3 * (Max - Min) / 4, Color.FromArgb(255, 255, 0));
                palette.Add(Max, Color.Red);
            }
            return palette;
        }
        public void Add(double X, Color Y)
        {
            PalettePoint pp;
            pp.X = Math.Round(X, 2);
            pp.Y = Y;
            points.Add(pp);
        }
        public void Insert(int index, double X, Color Y)
        {
            PalettePoint pp;
            pp.X = Math.Round(X, 2);
            pp.Y = Y;
            points.Insert(index, pp);
        }
        public void RemoveAt(int index)
        {
            if (index < points.Count)
            {
                points.RemoveAt(index);
            }
        }
        public void Set(int index, double X, Color Y)
        {
            PalettePoint pp = points[index];
            pp.X = X;
            pp.Y = Y;
            points[index] = pp;
        }
        public void Clear()
        {
            points.Clear();
        }
        public PalettePoint this[int index]
        {
            get
            {
                if (index < points.Count)
                    return points[index];
                else
                    return PalettePoint.Empty;
            }
        }
        public Color GetColorByX(double X)
        {
            int i;
            int r1, g1, b1, r2, g2, b2;
            double delimeter;
            if (points.Count > 1)
            {
                if (X > this[this.Count - 1].X - 0.001) X = this[this.Count - 1].X;
                if (X < this[0].X + 0.001) X = this[0].X;

                for (i = this.Count - 1; i > 0; i--)
                {
                    if ((this[i - 1].X <= X) && (X <= this[i].X))
                    {
                        r1 = this[i - 1].Y.R;
                        g1 = this[i - 1].Y.G;
                        b1 = this[i - 1].Y.B;
                        r2 = this[i].Y.R;
                        g2 = this[i].Y.G;
                        b2 = this[i].Y.B;
                        delimeter = 0;
                        if (this[i].X - this[i - 1].X != 0) delimeter = 1 / (this[i].X - this[i - 1].X);
                        Color clr = Color.FromArgb(255, (int)(r1 + (r2 - r1) * (X - this[i - 1].X) * delimeter),
                                                       (int)(g1 + (g2 - g1) * (X - this[i - 1].X) * delimeter),
                                                       (int)(b1 + (b2 - b1) * (X - this[i - 1].X) * delimeter));
                        return clr;
                    }
                }
            }
            return Color.White;
        }
        public void Invert()
        {
            if (Count > 0)
            {
                Color[] clr = new Color[Count];
                for (int i = 0; i < Count; i++)
                {
                    clr[i] = points[Count - 1 - i].Y;
                }
                for (int i = 0; i < Count; i++)
                {
                    Set(i, points[i].X, clr[i]);
                }
            }
        }
        public void ReadFromCache(BinaryReader br)
        {
            if (br != null)
            {
                int i, len = br.ReadInt32();
                double X;
                int clr;
                for (i = 0; i < len; i++)
                {
                    X = br.ReadDouble();
                    clr = br.ReadInt32();
                    this.Add(X, Color.FromArgb(clr));
                }
            }
        }
        public void WriteToCache(BinaryWriter bw)
        {
            if(bw != null)
            {
                bw.Write(points.Count);
                for(int i = 0; i < points.Count;i++)
                {
                    bw.Write(points[i].X);
                    bw.Write(points[i].Y.ToArgb());
                }
            }
        }

        public bool Equals(Palette pal)
        {
            if ((pal == null) || (this.Count != pal.Count))
            {
                return false;
            }
            else
            {
                for (int i = 0; i < Count; i++)
                {
                    if ((this[i].X != pal[i].X) || (this[i].Y.ToArgb() != pal[i].Y.ToArgb()))
                    {
                        return false;
                    }
                }
                return true;
            }
        }
    }
    #endregion
}


