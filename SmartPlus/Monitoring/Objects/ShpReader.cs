﻿using System;
using System.Windows.Forms;
using RDF;
using RDF.Objects;
using SmartPlus;
using System.IO;
using System.Data;

namespace SmartPlus 
{
    public sealed class ShpData 
    {
        public string _fileName;
        public int _shapeType;
        public int _fileLength;
        public int _fileVer;
        public double _xMin, _xMax, _yMin, _yMax;
        public double _zMin, _zMax, _mMin, _mMax;
        public System.Collections.ArrayList _ObjList;
        string[] _ObjNames = null;

        public ShpData() 
        {
            _ObjList = new System.Collections.ArrayList();
        }
    }

    public static class ShpReader 
    {

        public static ShpData ReadShapeFile(string fileName) 
        {
            ShpData shp = null;
            if (File.Exists(fileName)) 
            {
                int fileCode;
                int recNumber, recLength, shapeType = 0;
                FileStream fs;
                try 
                {
                    fs = new FileStream(fileName, FileMode.Open);
                }
                catch
                {
                    return null;
                }

                shp = new ShpData();
                shp._fileName = fileName;
                fileCode = ConvertEx.GetInt32BigEndian(fs);
                if (fileCode != 9994) return null; // Проверка типа файла
                // Read Header
                fs.Seek(20, SeekOrigin.Current);
                shp._fileLength = ConvertEx.GetInt32BigEndian(fs);
                shp._fileVer = ConvertEx.GetInt32(fs);
                shp._shapeType = ConvertEx.GetInt32(fs);
                shp._xMin = ConvertEx.GetDouble(fs);
                shp._yMin = ConvertEx.GetDouble(fs);
                shp._xMax = ConvertEx.GetDouble(fs);
                shp._yMax = ConvertEx.GetDouble(fs);
                shp._zMin = ConvertEx.GetDouble(fs);
                shp._zMax = ConvertEx.GetDouble(fs);
                shp._mMin = ConvertEx.GetDouble(fs);
                shp._mMax = ConvertEx.GetDouble(fs);

                // Read Records
                while (fs.Position < shp._fileLength * 2) 
                {
                    recNumber = ConvertEx.GetInt32BigEndian(fs);
                    recLength = ConvertEx.GetInt32BigEndian(fs);
                    shapeType = ConvertEx.GetInt32(fs);
                    switch (shapeType) {
                        case (int)Constant.SHAPEFILE_TYPES_ID.POINT:
                            shpPoint point = GetShpPoint(fs);
                            if (recLength != 10)
                                MessageBox.Show("Ошибка при загрузке.Несоотвествие длины Record.ShapeType - Point");
                            if (point != null) shp._ObjList.Add(point);
                            break;
                        case (int)Constant.SHAPEFILE_TYPES_ID.POLYLINE:
                        case (int)Constant.SHAPEFILE_TYPES_ID.POLYGON:
                            shpPolygon poly = GetShpPolygon(fs);
                            if (recLength * 2 != (44 + poly.numParts * 4 + poly.numPoints * 16)) 
                                 MessageBox.Show("Ошибка при загрузке.Несоотвествие длины Record.ShapeType - Polygon");
                            if (poly != null) shp._ObjList.Add(poly);
                            break;
                        default:
                            MessageBox.Show("Ошибка при загрузке.Неизвестный ShapeType = " + shapeType.ToString());
                            fs.Seek(recLength * 2, SeekOrigin.Current);
                            break;
                    }
                }
                // чтение имен объектов
                int i, colName = -1;
                string folderName = Path.GetDirectoryName(@shp._fileName);
                string dbfFileName = Path.GetFileNameWithoutExtension(shp._fileName) + ".dbf";
                string dbfFileNameReName = "dbfFileName.dbf"; 
                DataTable dt = null;
                if((shp._ObjList.Count > 0) && (File.Exists(folderName + "\\" + dbfFileName))) 
                {
                    try 
                    {
                        System.Data.Odbc.OdbcConnection dbfConn = new System.Data.Odbc.OdbcConnection();
                        dbfConn.ConnectionString = @"Driver={Microsoft dBASE Driver (*.dbf)};DriverID=277;Dbq=" + folderName + ";";
                        dbfConn.Open();
                        System.Data.Odbc.OdbcCommand oCmd = dbfConn.CreateCommand();
                        // Replace Name
                        File.Move(folderName + "\\" + dbfFileName, folderName + "\\" + dbfFileNameReName);
                        oCmd.CommandText = @"SELECT * FROM " + dbfFileNameReName;
                        dt = new DataTable();
                        dt.Load(oCmd.ExecuteReader());
                        File.Move(folderName + "\\" + dbfFileNameReName, folderName + "\\" + dbfFileName);
                        dbfConn.Dispose();
                    }
                    catch(Exception exp)
                    {
                        if (File.Exists(folderName + "\\" + dbfFileNameReName))
                        {
                            File.Move(folderName + "\\" + dbfFileNameReName, folderName + "\\" + dbfFileName);
                        }
                        MessageBox.Show(exp.Message);
                    }
                    if(dt != null)
                    {
                        switch(shp._shapeType)
                        {
                            case (int)Constant.SHAPEFILE_TYPES_ID.POINT:
                                for (i = 0; i < dt.Columns.Count; i++) 
                                {
                                    if (dt.Columns[i].ColumnName == "NC") 
                                    {
                                        colName = i;
                                        break;
                                    }
                                }
                                break;
                            case (int)Constant.SHAPEFILE_TYPES_ID.POLYGON:
                                for (i = 0; i < dt.Columns.Count; i++) 
                                {
                                    if (dt.Columns[i].ColumnName == "NAME_U") 
                                    {
                                        colName = i;
                                        break;
                                    }
                                }
                                break;
                        }
                        if (colName != -1) 
                        {
                            for (i = 0; i < shp._ObjList.Count; i++) 
                            {
                                if(i >= dt.Rows.Count) break;
                                ((shpBase)shp._ObjList[i]).Name = dt.Rows[i].ItemArray[colName].ToString();
                            }
                        }
                    }
                }
            }
            return shp;
        }
        
        static shpPoint GetShpPoint(Stream ms) 
        {
            shpPoint _point = new shpPoint();
            byte[] b = new byte[16];
            ms.Read(b, 0, 16);
            _point.X = BitConverter.ToDouble(b, 0);
            _point.Y = BitConverter.ToDouble(b, 8);
            return _point;
        }

        static shpPolygon GetShpPolygon(Stream ms) 
        {
            shpPolygon poly = new shpPolygon();
            int i;
            poly.box[0] = ConvertEx.GetDouble(ms);
            poly.box[1] = ConvertEx.GetDouble(ms);
            poly.box[2] = ConvertEx.GetDouble(ms);
            poly.box[3] = ConvertEx.GetDouble(ms);
            poly.numParts = ConvertEx.GetInt32(ms);
            poly.numPoints = ConvertEx.GetInt32(ms);
            poly.parts = new int[poly.numParts];
            poly.points = new shpPoint[poly.numPoints];
            for (i = 0; i < poly.numParts; i++) 
            {
                poly.parts[i] = ConvertEx.GetInt32(ms);
            }
            for (i = 0; i < poly.numPoints; i++) 
            {
                poly.points[i] = GetShpPoint(ms);
            }
            return poly;
        }
    }

    public class shpBase 
    {
        public string Name;
        public int TypeID;
    }
    public sealed class shpPoint : shpBase 
    {
        public double X;
        public double Y;
        public shpPoint() 
        {
            TypeID = (int)Constant.SHAPEFILE_TYPES_ID.POINT;
            Name = string.Empty;
        }
    }
    public sealed class shpPolygon : shpBase 
    {
        public double[] box;
        public int numParts;
        public int numPoints;
        public int[] parts;
        public shpPoint[] points;
        public shpPolygon() 
        {
            box = new Double[4];
            TypeID = (int)Constant.SHAPEFILE_TYPES_ID.POLYGON;
            Name = string.Empty;
        }
    }

}