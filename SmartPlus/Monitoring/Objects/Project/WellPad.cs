﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;

namespace SmartPlus
{
    public sealed class WellPad : C2DObject
    {
        public static int Version = 0;
        public List<Well> wells;
        public PointsXY contour;
        public Font font;

        RectangleF pad_center;
        RectangleD MinMax;
        float nameX, nameY;

        public WellPad()
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.WELL_PAD;
            Visible = true;
            wells = new List<Well>();
            pad_center = new Rectangle(0, 0, 0, 0);
            MinMax = new RectangleD(0, 0, 0, 0);
        }
        public WellPad(string WellPadName) : this()
        {
            Name = WellPadName;
        }

        public void CreateContour(SmartPlus.DictionaryObjects.OilfieldCoefItem coef)
        {
            int i, j;
            int max_index = -1, next_index;
            double x1, y1, x2, y2;
            double x_max_name = double.MinValue, x_min_name = double.MaxValue;
            double y_max_name = double.MinValue, y_min_name = double.MaxValue;
            double x_max = double.MinValue, x_min = double.MaxValue;
            double y_max = double.MinValue, y_min = double.MaxValue;


            double pred_alpha, max_alpha, alpha;
            Well w, w2;
            int[] temp_index = new int[wells.Count];

            for (i = 0; i < wells.Count; i++)
            {
                w = wells[i];
                if (w.X != Constant.DOUBLE_NAN && w.CoordLoaded)
                {
                    if (x_max_name == double.MinValue || x_max_name < w.X) { x_max_name = w.X; max_index = i; }
                    if (x_min_name == double.MaxValue || x_min_name > w.X) x_min_name = w.X;
                    if (y_max_name == double.MinValue || y_max_name < w.Y) y_max_name = w.Y;
                    if (y_min_name == double.MaxValue || y_min_name > w.Y) y_min_name = w.Y;
                }
                if (w.coord != null && w.coord.Altitude.X != 0 && w.coord.Altitude.Y != 0 && w.CoordLoaded)
                {
                    //coef.GlobalCoordFromLocal(w.coord.Altitude.X, w.coord.Altitude.Y, out x1, out y1);
                    x1 = w.coord.Altitude.X; y1 = w.coord.Altitude.Y;
                    if (x_max == double.MinValue || x_max < x1) x_max = x1;
                    if (x_min == double.MaxValue || x_min > x1) x_min = x1;
                    if (y_max == double.MinValue || y_max < y1) y_max = y1;
                    if (y_min == double.MaxValue || y_min > y1) y_min = y1;
                }
            }
            MinMax.Left = x_min_name; MinMax.Right = x_max_name;
            MinMax.Top = y_min_name; MinMax.Bottom = y_max_name;

            if (x_min < double.MaxValue)
            {
                this.X = x_min + (x_max - x_min) / 2;
                this.Y = y_min + (y_max - y_min) / 2;
            }
            else
            {
                this.X = x_min_name + (x_max_name - x_min_name) / 2;
                this.Y = y_min_name + (y_max_name - y_min_name) / 2;
            }
            nameX = (float)(x_min_name + (x_max_name - x_min_name) / 2);
            nameY = (float)(y_min_name + (y_max_name - y_min_name) / 2);

            if ((max_index >= 0) && (wells.Count > 2))
            {
                temp_index[0] = max_index;
                pred_alpha = Constant.DOUBLE_NAN;
                for (i = 1; i < wells.Count; i++)
                {
                    w = wells[temp_index[i - 1]];
                    x1 = w.X; y1 = w.Y;
                    next_index = -1;
                    max_alpha = Constant.DOUBLE_NAN;
                    for (j = 0; j < wells.Count; j++)
                    {
                        if (j != temp_index[i - 1])
                        {
                            w2 = (Well)wells[j];
                            x2 = w2.X;
                            y2 = w2.Y;
                            if ((x2 != Constant.DOUBLE_NAN) && (y2 != Constant.DOUBLE_NAN) && w.CoordLoaded)
                            {
                                alpha = Constant.DOUBLE_NAN;
                                if ((x2 >= x1) && (y2 >= y1)) if (x2 == x1) alpha = 90; else alpha = Math.Atan((y2 - y1) / (x2 - x1)) / Math.PI * 180;
                                if ((x2 < x1) && (y2 >= y1)) if (y2 == y1) alpha = 180; else alpha = 180 - Math.Atan((y2 - y1) / (x1 - x2)) / Math.PI * 180;
                                if ((x2 <= x1) && (y2 < y1)) if (x2 == x1) alpha = 270; else alpha = 180 + Math.Atan((y1 - y2) / (x1 - x2)) / Math.PI * 180;
                                if ((x2 > x1) && (y2 < y1)) alpha = 360 - Math.Atan((y1 - y2) / (x2 - x1)) / Math.PI * 180;
                                if ((pred_alpha != Constant.DOUBLE_NAN) && (alpha < pred_alpha)) alpha += 360;
                                if ((max_alpha == Constant.DOUBLE_NAN) || (alpha < max_alpha))
                                {
                                    next_index = j;
                                    max_alpha = alpha;
                                }
                            }
                        }
                    }
                    if ((next_index < 0) || (next_index == temp_index[0])) break;
                    temp_index[i] = next_index;
                    pred_alpha = max_alpha;
                }
            }

            if (i > 2)
            {
                contour = new PointsXY(i);
                for (j = 0; j < i; j++)
                {
                    if (wells[temp_index[j]].CoordLoaded)
                        contour.Add(wells[temp_index[j]].X, wells[temp_index[j]].Y);
                }
            }
        }
        public void ReadFromCache(BinaryReader br, OilField of)
        {
            Name = br.ReadString();
            int count = br.ReadInt32();
            int wellIndex, wellId;
            for (int i = 0; i < count; i++)
            {
                wellIndex = br.ReadInt32();
                wellId = br.ReadInt32();
                if (of.Wells[wellIndex].WellId == wellId && (of.Wells[wellIndex].coord.Count > 0))
                {
                    wells.Add(of.Wells[wellIndex]);
                }
            }
            CreateContour(of.CoefDict);
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(Name);
            bw.Write(wells.Count);
            for (int i = 0; i < wells.Count; i++)
            {
                bw.Write(wells[i].Index);
                bw.Write(wells[i].WellId);
            }
        }
        public override void DrawObject(Graphics grfx)
        {
            int i;
            float screenX, screenY, screenX2, screenY2;
            double realX, realY;
            Well w;
            PointD point;

            if ((st_VisbleLevel <= st_OilfieldXUnits) && (this.wells.Count > 0) && (X != Constant.DOUBLE_NAN) && (Y != Constant.DOUBLE_NAN) && (C2DObject.DrawAllObjects || st_RealScreenRect.IntersectWith(MinMax)))
            {
                screenX = ScreenXfromRealX(X);
                screenY = ScreenYfromRealY(Y);
                pad_center.X = screenX;
                pad_center.Y = screenY;
                pad_center.Width = st_radiusX;
                pad_center.Height = st_radiusX;
                bool centerInScreen = st_RealScreenRect.Contains(X, Y);
                for (i = 0; i < wells.Count; i++)
                {
                    w = (Well)wells[i];
                    if ((!w.TrajectoryLoaded || !w.TrajectoryVisible) /*&& (C2DObject.DrawAllObjects || centerInScreen || st_RealScreenRect.Contains(w.X, w.Y))*/)
                    {
                        screenX2 = ScreenXfromRealX(w.X);
                        screenY2 = ScreenYfromRealY(w.Y);
                        grfx.DrawLine(SmartPlusGraphics.WellPad.Pens.Line, screenX, screenY, screenX2, screenY2);
                    }
                }
                //if (C2DObject.DrawAllObjects || centerInScreen)
                //{
                    grfx.DrawRectangle(SmartPlusGraphics.WellPad.Pens.Line, pad_center.X - st_radiusX / 2, pad_center.Y - st_radiusY / 2, pad_center.Width, pad_center.Height);
                //}
                if ((contour != null) && (contour.Count > 2))
                {
                    Pen _pen_around;
                    _pen_around = (st_XUnitsInOnePixel < 15) ? SmartPlusGraphics.WellPad.Pens.AroundLine3 : SmartPlusGraphics.WellPad.Pens.AroundLine1;
                    realX = contour[0].X;
                    realY = contour[0].Y;
                    screenX = ScreenXfromRealX(realX);
                    screenY = ScreenYfromRealY(realY);
                    //screenX = -100;
                    //screenY = -100;
                    for (i = 1; i < contour.Count; i++)
                    {
                        //point = contour[i];

                        //if (C2DObject.DrawAllObjects || centerInScreen || st_RealScreenRect.Contains(realX, realY) || st_RealScreenRect.Contains(contour[i].X, contour[i].Y))
                        //{
                            //if (screenX == -100)
                            //{
                                //screenX = ScreenXfromRealX(realX);
                                //screenY = ScreenYfromRealY(realY);
                            //}
                            screenX2 = ScreenXfromRealX(contour[i].X);
                            screenY2 = ScreenYfromRealY(contour[i].Y);

                            grfx.DrawLine(_pen_around, screenX, screenY, screenX2, screenY2);
                            screenX = screenX2;
                            screenY = screenY2;
                        //}
                        //else
                        //{
                        //    screenX = -100;
                        //    screenY = -100;
                        //}
                        realX = contour[i].X;
                        realY = contour[i].Y;
                    }

                    //if (C2DObject.DrawAllObjects || centerInScreen || st_RealScreenRect.Contains(realX, realY) || st_RealScreenRect.Contains(contour[0].X, contour[0].Y))
                    //{
                        screenX = ScreenXfromRealX(realX);
                        screenY = ScreenYfromRealY(realY);
                        screenX2 = ScreenXfromRealX(contour[0].X);
                        screenY2 = ScreenYfromRealY(contour[0].Y);
                        grfx.DrawLine(_pen_around, screenX, screenY, screenX2, screenY2);
                    //}
                }
                if (font != null)
                {
                    SizeF size = grfx.MeasureString(Name, font, Point.Empty, StringFormat.GenericTypographic);

                    if (st_VisbleLevel < 32)
                    {
                        screenX = ScreenXfromRealX(nameX);
                        screenY = ScreenYfromRealY(nameY);
                        grfx.DrawString(Name, font, SmartPlusGraphics.WellPad.Brushes.Name, screenX + 2 - size.Width / 2, screenY - size.Height / 2, StringFormat.GenericTypographic);
                    }
                }
            }
        }
    }
}
