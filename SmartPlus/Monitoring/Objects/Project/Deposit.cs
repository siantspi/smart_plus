﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;

namespace SmartPlus
{
    public sealed class Deposit : C2DObject
    {
        public static int Version = 0;

        public int OilfieldIndex;
        public int StratumParentCode;
        public int StratumCode;

        public DepositParamsItem Params;
        public Contour contour;
        public PVTParamsItem pvt;
        public Well[] WellList;
        public Well[] AllWellList;

        public new bool Selected
        {
            get { return contour.Selected; }
            set { contour.Selected = value; }
        }

        public Deposit()
        {
            this.TypeID = Constant.BASE_OBJ_TYPES_ID.DEPOSIT;
            OilfieldIndex = -1;
            StratumParentCode = -1;
            StratumCode = -1;
            pvt = PVTParamsItem.Empty;
            contour = new Contour(3);
            WellList = null;
            AllWellList = null;
        }

        public bool ReadFromCache(BackgroundWorker worker, DoWorkEventArgs e, OilField of, BinaryReader file, int Version)
        {
            if (file != null)
            {
                int j, wellCount, wellIndex;
                Name = file.ReadString();
                Index = file.ReadInt32();
                StratumCode = file.ReadInt32();
                StratumParentCode = file.ReadInt32();

                wellCount = file.ReadInt32();
                if (wellCount > 0)
                {
                    WellList = new Well[wellCount];
                    for (j = 0; j < wellCount; j++)
                    {
                        wellIndex = file.ReadInt32();
                        WellList[j] = of.Wells[wellIndex];
                    }
                }
                wellCount = file.ReadInt32();
                if (wellCount > 0)
                {
                    AllWellList = new Well[wellCount];
                    for (j = 0; j < wellCount; j++)
                    {
                        wellIndex = file.ReadInt32();
                        AllWellList[j] = of.Wells[wellIndex];
                    }
                }

                Params.ReadFromBin(file);

                PVTParamsItem pvtItem = PVTParamsItem.Empty;
                int pvtCount = file.ReadInt32();
                if (pvtCount > 0 && pvtItem.ReadFromBin(file))
                {
                    this.pvt = pvtItem;
                    pvt.KIN = file.ReadDouble();
                }
                contour.ReadFromCacheBin(file, 2);
                //int cntrOutCount = file.ReadInt32();
                //Contour cntrOut;
                //if (cntrOutCount > 0) contour.cntrsOut = new List<Contour>(cntrOutCount);
                //for (j = 0; j < cntrOutCount; j++)
                //{
                //    cntrOut = new Contour();
                //    cntrOut.ReadFromCacheBin(file, 2);
                //    contour.cntrsOut.Add(cntrOut);
                //}
                contour.StratumCode = this.StratumCode;
                //contour.Text = string.Format("НБЗ: {0:0} тыс.т, ОИЗ: {1:0} тыс.т", Params.InitGeoOilReserv / 1000, Params.InitGeoOilReserv * pvt.KIN / 1000);
                X = contour.X;
                Y = contour.Y;
                return true;
            }
            return false;
        }
        public bool WriteToCache(BackgroundWorker worker, DoWorkEventArgs e, BinaryWriter file)
        {
            if (file != null)
            {
                file.Write(this.Name);
                file.Write(this.Index);
                file.Write(this.StratumCode);
                file.Write(this.StratumParentCode);

                if (WellList != null)
                {
                    file.Write(WellList.Length);
                    for (int i = 0; i < WellList.Length; i++)
                    {
                        file.Write(WellList[i].Index);
                    }
                }
                else
                    file.Write(0);
                if (AllWellList != null)
                {
                    file.Write(AllWellList.Length);
                    for (int i = 0; i < AllWellList.Length; i++)
                    {
                        file.Write(AllWellList[i].Index);
                    }
                }
                else
                    file.Write(0);

                Params.WriteToBin(file);

                if (!pvt.IsEmpty)
                {
                    file.Write(1);
                    pvt.WriteToBin(file);
                    file.Write(pvt.KIN);
                }
                else
                    file.Write(0);

                contour.WriteToCacheBin(0, file);
                //if (contour.cntrsOut != null)
                //{
                //    file.Write(contour.cntrsOut.Count);
                //    for (int i = 0; i < contour.cntrsOut.Count; i++)
                //    {
                //        contour.cntrsOut[i].WriteToCacheBin(0, file);
                //    }
                //}
                //else
                //{
                //    file.Write(0);
                //}
                return true;
            }
            return false;
        }
        public override void DrawObject(Graphics grfx)
        {
            if ((st_VisbleLevel > 0) && (st_VisbleLevel <= st_OilfieldXUnits))
            {
                contour.DrawObject(grfx);
                if ((st_VisbleLevel >= 18 || contour._ContourType == -6) && st_VisbleLevel < st_OilfieldXUnits / 2)
                {
                    contour.DrawContourName(grfx);
                }
            }
        }
    }

    public struct DepositParamsItem
    {
        const int version = 0;
        const int bytes = 48;
        /// <summary>
        /// Площадь ячейки, м2
        /// </summary>
        public float Square;
        /// <summary>
        /// Начальная нефтенасыщ.толщина
        /// </summary>
        public float InitHoil;
        /// <summary>
        /// Начальная газонасыщ.толщина
        /// </summary>
        public float InitHgas;
        /// <summary>
        /// Нефтенасыщенный объем
        /// </summary>
        public double OilVolume;
        /// <summary>
        /// Газосодержание
        /// </summary>
        public float GazContent;
        /// <summary>
        /// Начальные балансовые запасы нефти
        /// </summary>
        public double InitGeoOilReserv;
        /// <summary>
        /// Начальные балансовые запасы газа
        /// </summary>
        public double InitGeoGasReserv;
        /// <summary>
        /// Накопленный отбор нефти, т
        /// </summary>
        public double AccumOil;

        public void ReadFromBin(BinaryReader br)
        {
            int ver = br.ReadInt32();
            int bytes = br.ReadInt32();
            if (version == ver)
            {
                Square = br.ReadSingle();
                InitHoil = br.ReadSingle();
                InitHgas = br.ReadSingle();
                OilVolume = br.ReadDouble();
                GazContent = br.ReadSingle();
                InitGeoOilReserv = br.ReadDouble();
                InitGeoGasReserv = br.ReadDouble();
                AccumOil = br.ReadDouble();
            }
            else
            {
                br.BaseStream.Seek(bytes, SeekOrigin.Current);
            }
        }
        public void WriteToBin(BinaryWriter bw)
        {
            bw.Write(version);
            bw.Write(bytes);
            bw.Write(Square);
            bw.Write(InitHoil);
            bw.Write(InitHgas);
            bw.Write(OilVolume);
            bw.Write(GazContent);
            bw.Write(InitGeoOilReserv);
            bw.Write(InitGeoGasReserv);
            bw.Write(AccumOil);
        }
    }
}
