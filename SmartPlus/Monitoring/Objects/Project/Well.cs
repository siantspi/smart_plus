﻿using System;
using System.Collections.Generic;
using System.Text;
using RDF.Objects;
using System.Drawing;
using System.IO;
using System.Collections;
using RDF;
using SmartPlus.DictionaryObjects;


namespace SmartPlus
{
    public sealed class Well : C2DObject
    {
        public static int Version = 0;
        public int WellId { get; set; }
        public int BoreId { get; set; }
        public int MainBoreIndex { get; set; }
        public int OilFieldAreaCode { get; set; }
        public DateTime StartDate { get; set; }

        public int OilFieldCode { get; set; }
        public int OilFieldIndex { get; set; }
        public int AreaIndex { get; set; }

        public bool Missing { get; set; }
        public byte ProjectDest { get; set; }

        // bubble map
        public BubbleParamList BubbleList;
        public WellsIconList BubbleIcons;

        public Font font;
        public Brush brush;
        public Brush brush_name;

        #region Well Data
        public Well[] SideBores { get; set; }
        public SkvCoord coord;
        public Mer mer;
        public MerComp merEx;
        public WellChess chess;
        public WellChessInj chessInj;
        public SkvGIS gis;
        public SkvLog[] logsBase;
        public SkvLog[] logs;
        public MemoryStream PackedLogsBase = null;
        public MemoryStream PackedLogs = null;
        public SkvPerf perf;
        public SkvCore core;
        public SkvCoreTest coreTest;
        public WellGTM gtm;
        public WellAction action;
        public WellResearch research;
        public WellLeak leak;
        public WellSuspend suspend;
        public WellTrajectory trajectory;
        #endregion

        #region DataLoaded
        public bool CoordLoaded { get { return (coord != null); } }
        public bool MerLoaded { get { return ((mer != null) && (mer.Count > 0)); } }
        public bool ChessLoaded { get { return ((chess != null) && (chess.Count > 0)); } }
        public bool ChessInjLoaded { get { return ((chessInj != null) && (chessInj.Count > 0)); } }
        public bool GisLoaded { get { return ((gis != null) && (gis.Count != 0)); } }
        public bool PerfLoaded { get { return ((perf != null) && (perf.Count > 0)); } }
        public bool LogsBaseLoaded { get { return ((logsBase != null) && (logsBase.Length > 0)); } }
        public bool LogsLoaded { get { return ((logs != null) && (logs.Length > 0)); } }
        public bool CoreLoaded { get { return ((core != null) && (core.Count > 0)); } }
        public bool CoreTestLoaded { get { return ((coreTest != null) && (coreTest.Count > 0)); } }
        public bool GTMLoaded { get { return ((gtm != null) && (gtm.Count > 0)); } }
        public bool ActionLoaded { get { return ((action != null) && (action.Count > 0)); } }
        public bool ResearchLoaded { get { return ((research != null) && (research.Count > 0)); } }
        public bool LeakLoaded { get { return ((leak != null) && (leak.Count > 0)); } }
        public bool SuspendLoaded { get { return ((suspend != null) && (suspend.Count > 0)); } }
        public bool TrajectoryLoaded { get { return ((trajectory != null) && (trajectory.Count > 0)); } }
        #endregion

        public int CountAreas;
        public bool VisibleName;
        public bool VisibleText;
        public bool VisibleHBubbleCol;
        public bool SelectedByProfile;
        public int LetterBoxLevel = -1;
        public bool ShowLetterBox = false;
        public bool TrajectoryVisible = false;

        public string UpperCaseName
        {
            get { return this.Name; }
            set { this.Name = value; }
        }
        public WellsIconList icons;
        List<Marker> markerList;

        public Well(int Index)
        {
            this.TypeID = Constant.BASE_OBJ_TYPES_ID.WELL;
            this.Missing = false;
            this.Index = Index;
            this.OilFieldIndex = -1;
            this.OilFieldAreaCode = -1;
            this.AreaIndex = -1;
            this.ProjectDest = 0;
            this.BubbleList = null;
            this.SelectedByProfile = false;
            this.coord = new SkvCoord();
            this.mer = new Mer();
            this.merEx = null;
            this.chess = null;
            this.chessInj = null;
            this.VisibleName = true;
            this.VisibleText = true;
            this.VisibleHBubbleCol = true;
            this.icons = new WellsIconList(5);
            this.BubbleList = null;
            this.BubbleIcons = null;
            this.CountAreas = 0;
            this.X = Constant.DOUBLE_NAN;
            this.Y = Constant.DOUBLE_NAN;
            markerList = null;
        }

        public void SetIconsFonts(Font[] fonts, ExFontList exFontList)
        {
            icons.SetFonts(fonts, exFontList);
        }
        public void ReSetIcons(OilfieldCoefItem coef)
        {
            this.icons.Clear();
            if ((this.MerLoaded) && (mer != null) && (mer.Count > 0))
            {
                this.icons = this.GetIcons(coef, mer.Items[mer.Count - 1].Date);
            }
            else
                this.icons = this.GetIcons(coef, DateTime.MaxValue);
            this.SetActiveAllIcon();
        }
        public WellsIconList GetIcons(OilfieldCoefItem coef, DateTime maxMerDate)
        {
            WellIconCode[] codes = null;
            MerItem item;
            item.PlastId = 0;
            WellsIconList wellIcons = new WellsIconList();
            DateTime dt;
            int x;
            int defColor;
            ArrayList objList = new ArrayList();

            if ((this.ProjectDest == 0) && (this.MerLoaded) && (mer != null) && (mer.Count > 0))
            {
                x = mer.Count - 1;
                if (maxMerDate < mer.Items[x].Date)
                {
                    while ((x >= 0) && (maxMerDate != mer.Items[x].Date)) x--;
                }
                if (x < 0) return wellIcons;
                item = mer.Items[x];
                dt = item.Date;

                while ((x > -1) && (dt == item.Date))
                {
                    item = mer.Items[x];
                    if ((item.PlastId > 0) && (objList.IndexOf(item.PlastId) == -1))
                    {
                        objList.Add(item.PlastId);

                        #region Добывающие скважины
                        if ((item.CharWorkId == 11) || (item.CharWorkId == 12) || (item.CharWorkId == 15))
                        {
                            switch (item.CharWorkId)
                            {
                                case 11:
                                    defColor = Color.FromArgb(35, 35, 35).ToArgb(); // black
                                    break;
                                case 12:
                                    defColor = Color.FromArgb(255, 215, 0).ToArgb(); // gold
                                    break;
                                case 15:
                                    defColor = Color.FromArgb(35, 35, 35).ToArgb(); // black
                                    break;
                                default:
                                    defColor = Color.FromArgb(35, 35, 35).ToArgb(); // black
                                    break;
                            }
                            if (item.StateId == 6)
                            {
                                codes = new WellIconCode[1];
                                codes[0].FontCode = 0;
                                codes[0].IconColor = defColor;
                                codes[0].IconColorDefault = defColor;
                                codes[0].CharCode = 45;
                                codes[0].Size = 10;
                            }
                            else if (item.StateId == 5)
                            {
                                codes = new WellIconCode[1];
                                codes[0].FontCode = 0;
                                codes[0].IconColor = defColor;
                                codes[0].IconColorDefault = defColor;
                                codes[0].CharCode = 46;
                                codes[0].Size = 10;
                            }
                            else if (item.MethodId == 12)
                            {
                                codes = new WellIconCode[2];
                                codes[0].FontCode = 0;
                                codes[0].IconColor = defColor;
                                codes[0].IconColorDefault = defColor;
                                codes[0].CharCode = 42;
                                codes[0].Size = 10;
                                switch (item.StateId)
                                {
                                    case 8:
                                        codes[0].IconColor = Color.LightGray.ToArgb();
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = Color.LightGray.ToArgb();
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 41;
                                        codes[1].Size = 10;
                                        break;
                                    case 22:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 40;
                                        codes[0].Size = 10;
                                        break;
                                    case 2:
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = Color.Gray.ToArgb();
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 48;
                                        codes[1].Size = 10;
                                        break;
                                    case 10:
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = defColor;
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 47;
                                        codes[1].Size = 10;
                                        break;
                                    case 23:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 49;
                                        codes[0].Size = 10;
                                        break;
                                    case 9:
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = defColor;
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 43;
                                        codes[1].Size = 10;
                                        break;
                                    default:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 42;
                                        codes[0].Size = 10;
                                        break;
                                }
                            }
                            else
                            {
                                switch (item.StateId)
                                {
                                    case 1:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 34;
                                        codes[0].Size = 10;
                                        break;
                                    case 8:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.LightGray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 41;
                                        codes[0].Size = 10;
                                        break;
                                    case 10:
                                        codes = new WellIconCode[2];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 47;
                                        codes[0].Size = 10;
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = defColor;
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 34;
                                        codes[1].Size = 10;
                                        break;
                                    case 2:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 48;
                                        codes[0].Size = 10;
                                        break;
                                    case 7:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 34;
                                        codes[0].Size = 10;
                                        break;
                                    case 22:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 40;
                                        codes[0].Size = 10;
                                        break;
                                    case 23:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 49;
                                        codes[0].Size = 10;
                                        break;
                                    case 9:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 43;
                                        codes[0].Size = 10;
                                        break;
                                }
                            }
                        }
                        #endregion

                        #region Нагнетатаельные
                        else if (item.CharWorkId == 20)
                        {
                            defColor = Color.FromArgb(0, 82, 164).ToArgb();
                            switch (item.StateId)
                            {
                                case 2: //Остановлена
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = Color.Gray.ToArgb();
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.Gray.ToArgb();
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 48;
                                    codes[1].Size = 10;
                                    break;
                                case 6: //Пьезометрическая
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = defColor;
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 45;
                                    codes[1].Size = 10;
                                    break;
                                case 8: //ликвидирована
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = Color.LightGray.ToArgb();
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 35;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.LightGray.ToArgb();
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 41;
                                    codes[1].Size = 10;
                                    break;
                                case 9: //Ож.ликвидации
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = defColor;
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 43;
                                    codes[1].Size = 10;
                                    break;
                                case 22: // Б/Д ТГ
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.FromArgb(35, 35, 35).ToArgb();
                                    codes[1].IconColorDefault = Color.FromArgb(35, 35, 35).ToArgb();
                                    codes[1].CharCode = 40;
                                    codes[1].Size = 10;
                                    break;
                                case 23: // Б/Д ПР Лет
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.FromArgb(35, 35, 35).ToArgb();
                                    codes[1].IconColorDefault = Color.FromArgb(35, 35, 35).ToArgb();
                                    codes[1].CharCode = 49;
                                    codes[1].Size = 10;
                                    break;
                                default: // другое
                                    codes = new WellIconCode[1];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    break;
                            }
                        }
                        #endregion

                        #region Водозабор
                        else if (item.CharWorkId == 13)
                        {
                            defColor = Color.CornflowerBlue.ToArgb();
                            switch (item.StateId)
                            {
                                case 2: // Остановлена
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = Color.Gray.ToArgb();
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 37;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.Gray.ToArgb();
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 48;
                                    codes[1].Size = 10;
                                    break;
                                case 8: // Ликвидирована
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = Color.LightGray.ToArgb();
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 37;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.LightGray.ToArgb();
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 41;
                                    codes[1].Size = 10;
                                    break;
                                case 9: // Ож.ликвидации
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 37;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = defColor;
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 43;
                                    codes[1].Size = 10;
                                    break;
                                default: // другое
                                    codes = new WellIconCode[1];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 37;
                                    codes[0].Size = 10;
                                    break;
                            }
                        }
                        #endregion

                        #region Поглощающие
                        else if (item.CharWorkId == 41)
                        {
                            defColor = Color.FromArgb(35, 35, 35).ToArgb();
                            codes = new WellIconCode[2];
                            codes[0].FontCode = 0;
                            codes[0].IconColor = defColor;
                            codes[0].IconColorDefault = defColor;
                            codes[0].CharCode = 50;
                            codes[0].Size = 10;
                            switch (item.StateId)
                            {
                                case 8:
                                    codes[0].IconColor = Color.LightGray.ToArgb();
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.LightGray.ToArgb();
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 41;
                                    codes[1].Size = 10;
                                    break;
                                default:
                                    codes = new WellIconCode[1];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 50;
                                    codes[0].Size = 10;
                                    break;
                            }
                        }
                        #endregion

                        double X = Constant.DOUBLE_NAN, Y = Constant.DOUBLE_NAN;
                        if ((this.MerLoaded) && (this.CoordLoaded) && (this.coord != null) && (this.coord.Count > 0))
                        {
                            SkvCoordItem coordItem;
                            for (int i = 0; i < this.coord.Count; i++)
                            {
                                coordItem = coord.Items[i];
                                if (item.PlastId == coordItem.PlastId && (coordItem.X + coordItem.Y != 0))
                                {
                                    X = coordItem.X;
                                    Y = coordItem.Y;
                                   // coef.GlobalCoordFromLocal(coordItem.X, coordItem.Y, out X, out Y);
                                    break;
                                }
                            }
                        }
                        if (X == Constant.DOUBLE_NAN)
                        {
                            WellIcon allIcon = icons.GetAllIcon();
                            if (allIcon != null)
                            {
                                X = allIcon.X;
                                Y = allIcon.Y;
                            }
                            else
                            {
                                X = this.X;
                                Y = this.Y;
                            }
                        }
                        if ((codes != null) && ((X != Constant.DOUBLE_NAN)))
                        {
                            if ((this.MerLoaded) && (this.mer.Count > 0) && (this.mer.Items[this.mer.Count - 1].Date < maxMerDate))
                            {
                                for (int j = 0; j < codes.Length; j++)
                                {
                                    codes[j].IconColor = Color.LightGray.ToArgb();
                                }
                            }
                            wellIcons.AddIcon(X, Y, item.PlastId, codes);
                            //if (codes.Length > 0) this.SetActiveIcon(item.PlastId);
                        }
                    }
                    x--;
                }

            }
            else if (this.ProjectDest == 0)
            {
                codes = new WellIconCode[1];
                codes[0].FontCode = 0;
                codes[0].IconColor = Color.FromArgb(35, 35, 35).ToArgb();
                codes[0].IconColorDefault = codes[0].IconColor;
                codes[0].CharCode = 33;
                double X = Constant.DOUBLE_NAN, Y = Constant.DOUBLE_NAN;

                if (X == Constant.DOUBLE_NAN)
                {
                    X = this.X;
                    Y = this.Y;
                }
                if ((codes != null) && ((X != Constant.DOUBLE_NAN)))
                {
                    wellIcons.AddIcon(X, Y, item.PlastId, codes);
                    // if (codes.Length > 0) this.SetActiveIcon(item.PlastId);
                }
            }
            else // проектная скважина
            {
                switch (this.ProjectDest)
                {
                    case 1:
                        codes = new WellIconCode[1];
                        codes[0].FontCode = 0;
                        codes[0].IconColor = Color.FromArgb(255, 35, 35).ToArgb();
                        codes[0].IconColorDefault = codes[0].IconColor;
                        codes[0].CharCode = 34;
                        codes[0].Size = 10;
                        break;
                    case 2:
                        codes = new WellIconCode[1];
                        codes[0].FontCode = 0;
                        codes[0].IconColor = Color.FromArgb(255, 35, 35).ToArgb();
                        codes[0].IconColorDefault = codes[0].IconColor;
                        codes[0].CharCode = 36;
                        codes[0].Size = 10;
                        break;
                }
                if (this.coord != null && this.coord.Count > 0)
                {
                    this.X = coord.Items[0].X;
                    this.Y = coord.Items[0].Y;
                    if (codes != null)
                    {
                        for (int k = 0; k < coord.Count; k++)
                        {
                            wellIcons.AddIcon(coord.Items[k].X, coord.Items[k].Y, coord.Items[k].PlastId, codes);
                        }
                    }
                    wellIcons.AddEmptyIcon(this.X, this.Y);
                }
            }
            return wellIcons;
        }
        public void SetActiveIcon(int PlastCode)
        {
            WellsIconList locIcons = this.icons;
            if (locIcons != null)
            {
                this.icons.SetActiveIcon(PlastCode);
                if ((BubbleList != null) && (BubbleIcons != null))
                {
                    BubbleIcons.SetActiveIcon(PlastCode);
                    if (BubbleList.Visible) locIcons = BubbleIcons;
                }
                WellIcon icon = locIcons.GetActiveIcon();
                if (icon != null && (icon.X + icon.Y != 0))
                {
                    this.X = icon.X;
                    this.Y = icon.Y;
                }
                SetBrushFromIcon();
            }
        }
        public void SetActiveAllIcon()
        {
            WellsIconList locIcons = this.icons;
            if (locIcons != null)
            {
                this.icons.SetActiveAllIcon();
                if ((BubbleList != null) && (BubbleIcons != null))
                {
                    BubbleIcons.SetActiveAllIcon();
                    if (BubbleList.Visible) locIcons = BubbleIcons;
                }
                WellIcon icon = locIcons.GetActiveIcon();
                if (icon != null && (icon.X + icon.Y != 0))
                {
                    this.X = icon.X;
                    this.Y = icon.Y;
                }
                SetBrushFromIcon();
            }
        }
        public void SetBrushFromIcon()
        {
            WellsIconList iconList = icons;
            if (BubbleList != null && BubbleList.Visible && BubbleIcons != null && BubbleIcons.Count > 0)
            {
                iconList = BubbleIcons;
            }
            if (iconList.Count > 0)
            {
                int clr = (iconList.IsDefaultIconsColor) ? iconList.DefaultIconsColor : iconList.IconsColor;
                if (clr == -2894893)
                {
                    brush = SmartPlusGraphics.Well.Brushes.LightGray;
                    brush_name = SmartPlusGraphics.Well.Brushes.LightGray;
                }
                else
                {
                    brush = new SolidBrush(Color.FromArgb(clr));
                    brush_name = Brushes.Black;
                }
            }
            else
            {
                brush = SmartPlusGraphics.Well.Brushes.LightGray;
                brush_name = SmartPlusGraphics.Well.Brushes.LightGray;
            }
        }

        // SERVICE
        internal struct Obj
        {
            public int Code;
            public float Top;
            public float Bottom;
            public float GisSweep;
        }

        public void CompareGisLogs(Project _proj, ArrayList list)
        {
            var dict = (WellLogMnemonicDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LOG_MNEMONIC);
            var dictArea = (OilFieldAreaDictionary)_proj.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);

            //RdfDictionary dict = _proj.DictList.Get((int)Constant.DICTIONARY_ID.LOG_MNEMONIC);
            //RdfDictionary dictArea = _proj.DictList.Get((int)Constant.DICTIONARY_ID.OILFIELD_AREA);

            ArrayList GisObjects = new ArrayList();
            float GisTop = 0, GisBottom = 0;
            float sumGisL = 0;
            int i, j, k;
            Obj gisObj, obj, logsObj;
            string str = "", logsStr = "";
            string logName = "";
            string logBasePS = "", logBaseIK = "", logBaseGK = "", logBaseBK = ""; ;
            int countPS = 0, countGK = 0, countIK = 0, countBK = 0;
            ArrayList logs_list;
            double sum, sigma;
            if (GisLoaded)
            {
                for (i = 0; i < gis.Count; i++)
                {
                    obj.Code = 0;
                    obj.Top = gis.Items[i].H;
                    obj.Bottom = gis.Items[i].H + gis.Items[i].L;
                    obj.GisSweep = 0;
                    sumGisL += gis.Items[i].L;
                    GisObjects.Add(obj);
                }
                if (gis.Count > 0)
                {
                    GisTop = gis.Items[0].H;
                    GisBottom = gis.Items[gis.Count - 1].H + gis.Items[gis.Count - 1].L;
                }
            }
            #region BASE LOGS
            if (LogsBaseLoaded)
            {
                for (i = 0; i < logsBase.Length; i++)
                {
                    obj.Top = logsBase[i].StartDepth;
                    obj.Bottom = logsBase[i].EndDepth;
                    obj.GisSweep = 0;

                    if ((sumGisL > 0) && (GisTop < obj.Bottom) && (GisBottom > obj.Top))
                    {
                        for (j = 0; j < GisObjects.Count; j++)
                        {
                            gisObj = (Obj)GisObjects[j];
                            if ((gisObj.Top > obj.Top) && (gisObj.Bottom < obj.Bottom))
                            {
                                obj.GisSweep += gisObj.Bottom - gisObj.Top;
                            }
                            else if ((gisObj.Top < obj.Top) && (gisObj.Bottom > obj.Bottom))
                            {
                                obj.GisSweep += obj.Bottom - obj.Top;
                            }
                            else if ((gisObj.Top < obj.Bottom) && (gisObj.Bottom > obj.Bottom))
                            {
                                obj.GisSweep += obj.Bottom - gisObj.Top;
                            }
                            else if ((gisObj.Top < obj.Top) && (gisObj.Bottom > obj.Top))
                            {
                                obj.GisSweep += gisObj.Bottom - obj.Top;
                            }
                        }
                    }
                    // подсчет среднеквадратичного отклонения
                    sum = 0;
                    for (j = 0; j < logsBase[i].Items.Length; j++)
                    {
                        if (logsBase[i].Items[j] != logsBase[i].NullValue)
                        {
                            sum += logsBase[i].Items[j];
                        }
                    }
                    if (logsBase[i].Items.Length > 0) sum = sum / logsBase[i].Items.Length;
                    sigma = 0;
                    for (j = 0; j < logsBase[i].Items.Length; j++)
                    {
                        if (logsBase[i].Items[j] != logsBase[i].NullValue)
                        {
                            sigma += Math.Pow((logsBase[i].Items[j] - sum), 2);
                        }
                    }
                    if (logsBase[i].Items.Length > 0) sigma = Math.Sqrt(sigma / logsBase[i].Items.Length);


                    logName = dict.GetShortNameByCode(logsBase[i].MnemonikaId);
                    if ((logName.IndexOf("PS") > -1) || (logName.IndexOf("ПС") > -1))
                    {
                        logBasePS += String.Format("{0};{1};{2};{3};{4};", logName, obj.Top, obj.Bottom, obj.GisSweep, sigma);
                        countPS++;
                    }
                    else if ((logName.IndexOf("GK") > -1) || (logName.IndexOf("ГК") > -1))
                    {
                        logBaseGK += String.Format("{0};{1};{2};{3};{4};", logName, obj.Top, obj.Bottom, obj.GisSweep, sigma);
                        countGK++;
                    }
                    else if ((logName.IndexOf("IK") > -1) || (logName.IndexOf("ИК") > -1))
                    {
                        logBaseIK += String.Format("{0};{1};{2};{3};{4};", logName, obj.Top, obj.Bottom, obj.GisSweep, sigma);
                        countIK++;
                    }
                    else if ((logName.IndexOf("BK") > -1) || (logName.IndexOf("БК") > -1))
                    {
                        logBaseBK += String.Format("{0};{1};{2};{3};{4};", logName, obj.Top, obj.Bottom, obj.GisSweep, sigma);
                        countBK++;
                    }
                }
                for (i = countPS; i < 11; i++) logBasePS += ";;;;;";
                for (i = countGK; i < 11; i++) logBaseGK += ";;;;;";
                for (i = countIK; i < 11; i++) logBaseIK += ";;;;;";
                for (i = countBK; i < 11; i++) logBaseBK += ";;;;;";
            }
            #endregion

            //if (LogsBaseLoaded)
            //{
            //    logs_list = new ArrayList();
            //    for (i = 0; i < logsBase.Length; i++)
            //    {
            //        if (logs_list.IndexOf(logsBase[i].MnemonikaId) == -1)
            //        {
            //            logs_list.Add(logsBase[i].MnemonikaId);
            //        }
            //    }
            //    logName = "";
            //    for (i = 0; i < logs_list.Count; i++)
            //    {
            //        logName = dict.GetItemByCode((int)logs_list[i]).ShortName + ";";
            //        str = String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};",
            //            _proj.OilFields[this.OilFieldIndex].Name,
            //            dictArea.GetItemByCode(this.OilFieldAreaCode).ShortName,
            //            this.UpperCaseName,
            //            GisTop, GisBottom, sumGisL,
            //            logName,
            //            logBasePS, logBaseGK, logBaseIK, logBaseBK
            //            );
            //        list.Add(str);
            //    }
            //}

            /*
            #region OTHER LOGS
            if (LogsLoaded)
            {
                for (i = 0; i < logs.Length; i++)
                {
                    obj.Top = logs[i].StartDepth;
                    obj.Bottom = logs[i].EndDepth;
                    obj.GisSweep = 0;

                    if ((sumGisL > 0) && (GisTop < obj.Bottom) && (GisBottom > obj.Top))
                    {
                        for (j = 0; j < GisObjects.Count; j++)
                        {
                            gisObj = (Obj)GisObjects[j];
                            if ((gisObj.Top > obj.Top) && (gisObj.Bottom < obj.Bottom))
                            {
                                obj.GisSweep += gisObj.Bottom - gisObj.Top;
                            }
                            else if ((gisObj.Top < obj.Top) && (gisObj.Bottom > obj.Bottom))
                            {
                                obj.GisSweep += obj.Bottom - obj.Top;
                            }
                            else if ((gisObj.Top < obj.Bottom) && (gisObj.Bottom > obj.Bottom))
                            {
                                obj.GisSweep += obj.Bottom - gisObj.Top;
                            }
                            else if ((gisObj.Top < obj.Top) && (gisObj.Bottom > obj.Top))
                            {
                                obj.GisSweep += gisObj.Bottom - obj.Top;
                            }
                        }
                    }
                    // подсчет среднеквадратичного отклонения
                    sum = 0;
                    for (j = 0; j < logs[i].Items.Length; j++)
                    {
                        if (logs[i].Items[j] != logs[i].NullValue)
                        {
                            sum += logs[i].Items[j];
                        }
                    }
                    if (logs[i].Items.Length > 0) sum = sum / logs[i].Items.Length;
                    sigma = 0;
                    for (j = 0; j < logs[i].Items.Length; j++)
                    {
                        if (logs[i].Items[j] != logs[i].NullValue)
                        {
                            sigma += Math.Pow((logs[i].Items[j] - sum), 2);
                        }
                    }
                    if (logs[i].Items.Length > 0) sigma = Math.Sqrt(sigma / logs[i].Items.Length);


                    logName = dict.GetItemByCode(logs[i].MnemonikaId).ShortName;
                    if ((logName.IndexOf("PS") > -1) || (logName.IndexOf("ПС") > -1))
                    {
                        logBasePS += String.Format("{0};{1};{2};{3};{4};", logName, obj.Top, obj.Bottom, obj.GisSweep, sigma);
                        countPS++;
                    }
                    else if ((logName.IndexOf("GK") > -1) || (logName.IndexOf("ГК") > -1))
                    {
                        logBaseGK += String.Format("{0};{1};{2};{3};{4};", logName, obj.Top, obj.Bottom, obj.GisSweep, sigma);
                        countGK++;
                    }
                    else if ((logName.IndexOf("IK") > -1) || (logName.IndexOf("ИК") > -1))
                    {
                        logBaseIK += String.Format("{0};{1};{2};{3};{4};", logName, obj.Top, obj.Bottom, obj.GisSweep, sigma);
                        countIK++;
                    }
                }
                for (i = countPS; i < 11; i++) logBasePS += ";;;;;";
                for (i = countGK; i < 11; i++) logBaseGK += ";;;;;";
                for (i = countIK; i < 11; i++) logBaseIK += ";;;;;";
            }
            #endregion
            */

            str = String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};",
                _proj.OilFields[this.OilFieldIndex].Name,
                dictArea.GetShortNameByCode(this.OilFieldAreaCode),
                this.UpperCaseName,
                GisTop, GisBottom, sumGisL,
                logName,
                logBasePS, logBaseGK, logBaseIK, logBaseBK
                );
            list.Add(str);

            GisObjects.Clear();
        }

        // MARKER LIST
        public int MarkerCount
        {
            get { return (markerList != null) ? markerList.Count : 0; }
        }
        public void AddMarker(Marker mrk)
        {
            if (markerList == null) markerList = new List<Marker>();
            markerList.Add(mrk);
        }
        public void RemoveMarker(Marker mrk)
        {
            if(markerList != null) markerList.Remove(mrk);
        }
        public int GetMarkerIndex(Marker mrk)
        {
            return (markerList != null) ? markerList.IndexOf(mrk) : -1;
        }
        public Marker GetMarker(int index)
        {
            return (markerList != null) ? markerList[index] : null;
        }
        public float GetMarkerDepth(int index)
        {
            if ((index > -1) && (index < MarkerCount))
            {
                return markerList[index].GetDepthByWell(this);
            }
            return -1;
        }

        // COORD
        public bool LoadCoord(string[] parseStr, OilfieldCoefItem coef, StratumDictionary dict, int ver)
        {
            bool retValue = false;
            int i, j, k, m, iLen, wellIconsLen, iconLen, col;
            int PlastId;
            SkvCoordItem[] items;
            WellIcon icon, activeIcon;
            WellIconCode iconCode;

            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";
            if (parseStr.Length > 4)
            {
                col = 1;
                Index = Convert.ToInt32(parseStr[col++]);
                if (ver > 0) OilFieldAreaCode = Convert.ToInt32(parseStr[col++]);
                if (ver > 3) Missing = Convert.ToBoolean(parseStr[col++]);
                coord.Altitude.Alt = Convert.ToSingle(parseStr[col++], nfi);

                coord.Altitude.X = (int)Convert.ToDouble(parseStr[col++], nfi);
                coord.Altitude.Y = (int)Convert.ToDouble(parseStr[col++], nfi);
                iLen = Convert.ToInt32(parseStr[col++]);
                if (iLen > 0)
                {
                    items = new SkvCoordItem[iLen];
                    for (j = 0; j < iLen; j++)
                    {
                        items[j].PlastId = Convert.ToUInt16(parseStr[col++]);
                        items[j].X = (int)Convert.ToDouble(parseStr[col++], nfi);
                        items[j].Y = (int)Convert.ToDouble(parseStr[col++], nfi);
                    }
                    coord.SetItems(items);
                    bool find = false;
                    j = 0;
                    while ((j < coord.Count) && (coord.Items[j].X == 0) && (coord.Items[j].Y == 0)) j++;

                    if (j < coord.Count)
                    {
                        X = coord.Items[j].X;
                        Y = coord.Items[j].Y;
                    }
                    else
                    {
                        X = coord.Altitude.X;
                        Y = coord.Altitude.Y;
                    }

                    double xGlob, yGlob;
                    bool setActive = false;
                    StratumTreeNode node1, node2;
                    coef.GlobalCoordFromLocal(this.X, this.Y, out xGlob, out yGlob);
                    this.X = xGlob;
                    this.Y = yGlob;
                    if (col < parseStr.Length - 1)
                    {
                        wellIconsLen = Convert.ToInt32(parseStr[col++]);
                        if (wellIconsLen != -1)
                        {
                            setActive = false;
                            for (j = 0; j < wellIconsLen; j++)
                            {
                                PlastId = Convert.ToInt32(parseStr[col++]);
                                iconLen = Convert.ToInt32(parseStr[col++]);

                                k = -1;
                                for (m = 0; m < iLen; m++)
                                {
                                    if (items[m].PlastId == PlastId)
                                    {
                                        k = m;
                                        break;
                                    }
                                }
                                if (k == -1)
                                {
                                    node2 = dict.GetStratumTreeNode(PlastId);
                                    for (m = 0; m < iLen; m++)
                                    {
                                        node1 = dict.GetStratumTreeNode(items[m].PlastId);
                                        if ((node1 != null && node1.GetParentLevelByCode(PlastId) != -1) || (node2 != null && node2.GetParentLevelByCode(items[m].PlastId) != -1))
                                        {
                                            k = m;
                                            break;
                                        }
                                    }
                                }
                                if (k != -1)
                                {
                                    double globX, globY;
                                    if ((items[k].X != 0) && (items[k].Y != 0))
                                    {
                                        coef.GlobalCoordFromLocal(items[k].X, items[k].Y, out globX, out  globY);
                                    }
                                    else
                                    {
                                        globX = this.X;
                                        globY = this.Y;
                                    }
                                    icon = new WellIcon(globX, globY, PlastId);
                                }
                                else
                                {
                                    icon = new WellIcon(this.X, this.Y, PlastId);
                                }


                                for (m = 0; m < iconLen; m++)
                                {
                                    iconCode.FontCode = Convert.ToUInt16(parseStr[col++]);
                                    iconCode.IconColor = Convert.ToInt32(parseStr[col++]);
                                    iconCode.IconColorDefault = iconCode.IconColor;
                                    if (ver > 2) iconCode.IconColorDefault = Convert.ToInt32(parseStr[col++]);
                                    iconCode.CharCode = Convert.ToUInt16(parseStr[col++]);
                                    iconCode.Size = 10;
                                    if (ver > 1) iconCode.Size = Convert.ToUInt16(parseStr[col++]);
                                    icon.AddIconCode(iconCode);
                                }
                                this.icons.AddIcon(icon);
                                if ((!setActive) && (icon.StratumCode != -1))
                                {
                                    this.icons.SetActiveIcon(icon.StratumCode);
                                    setActive = true;
                                }
                            }

                            iconCode.FontCode = 0;
                            iconCode.IconColor = Color.FromArgb(211, 211, 211).ToArgb();
                            iconCode.IconColorDefault = iconCode.IconColor;
                            iconCode.CharCode = 33;
                            iconCode.Size = 10;
                            activeIcon = this.icons.GetActiveIcon();
                            if (activeIcon != null)
                            {
                                icon = new WellIcon(activeIcon.X, activeIcon.Y, -1);
                                this.X = activeIcon.X;
                                this.Y = activeIcon.Y;
                            }
                            else
                                icon = new WellIcon(this.X, this.Y, -1);
                            icon.InsertIconCode(0, iconCode);
                            this.icons.AddIcon(icon);
                            if (activeIcon != null) this.icons.SetActiveIcon(activeIcon.StratumCode);
                        }
                    }
                }
                retValue = true;
                this.SetBrushFromIcon();
            }
            return retValue;
        }
        public bool LoadCoord(FileStream fs)
        {
            bool retValue = false;
            if (fs != null)
            {
                int iLen;
                SkvCoordItem[] items;
                BinaryReader br = new BinaryReader(fs);
                coord.Altitude.Alt = br.ReadSingle();
                coord.Altitude.X = br.ReadInt32();
                coord.Altitude.Y = br.ReadInt32();
                iLen = br.ReadInt32();
                items = new SkvCoordItem[iLen];
                for (int j = 0; j < iLen; j++)
                {
                    items[j].PlastId = br.ReadUInt16();
                    items[j].X = br.ReadInt32();
                    items[j].Y = br.ReadInt32();
                }
                coord.SetItems(items);
                if ((coord.Items != null) && (coord.Items.Length > 0))
                {
                    X = coord.Items[0].X;
                    Y = coord.Items[0].Y;
                }
                retValue = true;
            }
            return retValue;
        }
        public void SetStartedCoord()
        {
            if (CoordLoaded)
            {
                if (coord.Count > 0)
                {
                    X = coord.Items[0].X;
                    Y = coord.Items[0].Y;
                }
                else
                {
                    X = coord.Altitude.X;
                    Y = coord.Altitude.Y;
                }
            }
        }

        // MER
        public string[] GetBoreList(RdfConnector conn, string mainKey)
        {
            string[] res = null;
            if ((conn.Connected) && (mainKey.Length > 0))
            {
                string key = mainKey + String.Format("@{0}@wellinfo@borehole@welllist", this.UpperCaseName);
                MemoryStream ms = new MemoryStream();
                if (conn.IsKeyExist(key))
                {
                    ms = conn.ReadBinary(key, false);
                    if (ms.Length > 0)
                    {
                        ms.Seek(16, SeekOrigin.Begin);
                        BinaryReader br = new BinaryReader(ms, Encoding.GetEncoding(1251));
                        int count = br.ReadInt32();
                        br.ReadByte();
                        short len;
                        res = new string[count];
                        for (int i = 0; i < count; i++)
                        {
                            len = br.ReadInt16();

                            res[i] = new string(br.ReadChars(len)).ToUpper();
                        }
                    }
                }
            }
            return res;
        }
        public DateTime GetBoreMinDate(RdfConnector conn, string mainKey)
        {
            DateTime res = DateTime.MinValue;
            if ((conn.Connected) && (mainKey.Length > 0))
            {
                string key = mainKey + String.Format("@{0}@wellinfo@comminfo", this.UpperCaseName);
                MemoryStream ms = new MemoryStream();
                if (conn.IsKeyExist(key))
                {
                    ms = conn.ReadBinary(key, false);
                    if (ms.Length > 0)
                    {
                        ms.Seek(4, SeekOrigin.Begin);
                        BinaryReader br = new BinaryReader(ms);
                        try
                        {
                            res = DateTime.FromOADate(br.ReadDouble());
                        }
                        catch
                        {
                            res = DateTime.MinValue;
                        }
                    }
                }
            }
            return res;
        }

        // GIS
        public bool LoadGis(RdfConnector aConn, string aKey)
        {
            bool retValue = false;
            if ((aConn.Connected) && (aKey.Length > 0))
            {
                SkvGIS tempGIS = new SkvGIS();
                retValue = tempGIS.Read(aConn, aKey);
                if (retValue)
                {
                    gis = tempGIS;
                }
            }
            return retValue;
        }

        // PERF
        public bool LoadPerf(RdfConnector aConn, string aKey)
        {
            bool retValue = false;
            if ((aConn.Connected) && (aKey.Length > 0))
            {
                SkvPerf tempPERF = new SkvPerf();
                retValue = tempPERF.Read(aConn, aKey);
                if (retValue)
                {
                    perf = tempPERF;
                }
            }
            return retValue;
        }
        public void AddPerforation(SkvPerf newPerf)
        {
            if (this.perf == null || this.perf.Count == 0)
            {
                this.perf = newPerf;
            }
            else if (newPerf != null)
            {
                int i, j;
                bool added;
                SkvPerfItem item;
                List<SkvPerfItem> newItems = new List<SkvPerfItem>();
                for (i = 0; i < this.perf.Count; i++)
                {
                    newItems.Add(this.perf.Items[i]);
                }
                for (i = 0; i < newPerf.Count; i++)
                {
                    j = 0;
                    if (newPerf.Items[i].Date > newItems[newItems.Count - 1].Date)
                    {
                        newItems.Add(newPerf.Items[i]);
                    }
                    else
                    {
                        added = false;
                        for (j = 0; j < newItems.Count; j++)
                        {
                            if ((newPerf.Items[i].Date == newItems[j].Date) &&
                                (Math.Abs(newPerf.Items[i].Top - newItems[j].Top) < 0.1) &&
                                (Math.Abs(newPerf.Items[i].Bottom - newItems[j].Bottom) < 0.1))
                            {
                                added = true;
                                if ((newItems[j].PerfTypeId != 10000) && (newPerf.Items[i].PerfTypeId == 10000))
                                {
                                    item = newItems[j];
                                    item.Source = newPerf.Items[i].Source;
                                    item.PerfTypeId = 10000;
                                    newItems[j] = item;
                                }
                                break;
                            }
                            else if (newPerf.Items[i].Date < newItems[j].Date)
                            {
                                newItems.Insert(j, newPerf.Items[i]);
                                added = true;
                                break;
                            }
                        }
                        if (!added) newItems.Add(newPerf.Items[i]);
                    }
                }
                if (newItems.Count > this.perf.Count)
                {
                    SkvPerfItem[] items = new SkvPerfItem[newItems.Count];
                    for (i = 0; i < newItems.Count; i++)
                    {
                        items[i] = newItems[i];
                    }
                    this.perf.SetItems(items);
                }
            }
        }

        #region CACHE
        public void ReadFromCache(BinaryReader br)
        {
            Index = br.ReadInt32();
            MainBoreIndex = br.ReadInt32();
            WellId = br.ReadInt32();
            BoreId = br.ReadInt32();
            OilFieldAreaCode = br.ReadInt32();
            StartDate = DateTime.FromOADate(br.ReadDouble());
            Name = br.ReadString();
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(Index);
            bw.Write(MainBoreIndex);
            bw.Write(WellId);
            bw.Write(BoreId);
            bw.Write(OilFieldAreaCode);
            bw.Write(StartDate.ToOADate());
            bw.Write(Name);
        }
        #endregion
        public bool WriteToCache(StreamWriter file)
        {
            if (file != null)
            {
                bool firstIcon = true;
                int j, iLen, maxPlasts = 0;
                SkvCoordItem coordItem;
                WellIcon wIcon, emptyIcon;
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                file.Write(this.UpperCaseName.ToString() + ";");
                file.Write(this.Index.ToString() + ";");
                file.Write(this.OilFieldAreaCode.ToString() + ";");
                file.Write(this.Missing.ToString() + ";");
                file.Write(this.coord.Altitude.Alt.ToString(nfi) + ";");
                file.Write(this.coord.Altitude.X.ToString(nfi) + ";");
                file.Write(this.coord.Altitude.Y.ToString(nfi) + ";");
                if (this.coord.Items != null)
                {
                    iLen = this.coord.Items.Length;
                    file.Write(string.Format("{0};", iLen.ToString()));
                    if (maxPlasts < iLen) maxPlasts = iLen;
                    for (j = 0; j < iLen; j++)
                    {
                        coordItem = this.coord.Items[j];
                        file.Write(coordItem.PlastId.ToString() + ";");
                        file.Write(coordItem.X.ToString() + ";");
                        file.Write(coordItem.Y.ToString() + ";");
                    }
                    if (icons != null)
                    {
                        emptyIcon = icons.GetIconByCode(-1);
                        file.Write(string.Format("{0};", icons.Count - ((emptyIcon == null) ? 0 : 1)));
                        firstIcon = true;
                        for (j = 0; j < icons.Count; j++)
                        {
                            wIcon = icons[j];
                            if ((emptyIcon != wIcon) && (wIcon.StratumCode != -1))
                            {
                                if (!firstIcon) file.Write(";");
                                file.Write(string.Format("{0};", wIcon.StratumCode));
                                wIcon.WriteToCache(file, true);
                                firstIcon = false;
                            }
                        }
                        file.WriteLine("");
                    }
                    else file.WriteLine("-1;");
                }
                else file.WriteLine("0;");
                return true;
            }
            return false;

        }

        public void DrawBubbleMap(Graphics grfx)
        {
            float screenX, screenY;
            double realX, realY;
            SizeF s = SizeF.Empty;
            string str = "";
            WellIcon icon = null;
            Brush br1, br2;

            if ((Visible) && (CoordLoaded) && (st_VisbleLevel <= st_OilfieldXUnits) && (X != Constant.DOUBLE_NAN) && (Y != Constant.DOUBLE_NAN))
            {
                icon = icons.GetActiveIcon();
                if ((BubbleList != null) && (BubbleList.Visible) && (BubbleIcons != null))
                {
                    icon = BubbleIcons.GetActiveIcon();
                }

                if (icon == null)
                {
                    realX = X;
                    realY = Y;
                }
                else
                {
                    realX = icon.X;
                    realY = icon.Y;
                }
                if (DrawAllObjects || st_RealScreenRect.Contains(realX, realY))
                {
                    screenX = ScreenXfromRealX(realX);
                    screenY = ScreenYfromRealY(realY);
                    float radX = (float)(50.31f * Math.Pow(st_XUnitsInOnePixel, -1.013));

                    float r = 0, hr = 0, scrTextX, maxR, colX, colY;
                    double dQoil;
                    bool IsLimited = false;
                    SizeF sIcon = SizeF.Empty;

                    if (icon != null) sIcon = icon.GetIconSize(grfx);
                    bool bsIconSize = ((sIcon.Width > 0) && (st_VisbleLevel <= 15));
                    if ((BubbleList != null) && (BubbleList.Visible))
                    {
                        switch (BubbleList.Type)
                        {
                            #region BUBBLE_CURRENT
                            case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                r = 0;
                                if (BubbleList.RSumNatGas > 0)
                                {
                                    r = radX * (BubbleList.RSumNatGas) * BubbleList.K;
                                    if ((bsIconSize) && (r < sIcon.Width / 4))
                                    {
                                        r = sIcon.Width / 4;
                                    }
                                    grfx.FillEllipse(Brushes.Yellow, screenX - r, screenY - r, 2 * r, 2 * r);
                                    grfx.FillPie(Brushes.YellowGreen, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaGasCond, BubbleList.alphaGasCond);
                                    grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                    grfx.DrawPie(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaGasCond, BubbleList.alphaGasCond);
                                }
                                r = 0;
                                if (BubbleList.RSumInj > 0)
                                {
                                    r = radX * (BubbleList.RSumInj) * BubbleList.K;
                                    if ((bsIconSize) && (r < sIcon.Width / 4))
                                    {
                                        r = sIcon.Width / 4;
                                    }
                                    grfx.FillEllipse(Brushes.SkyBlue, screenX - r, screenY - r, 2 * r, 2 * r);
                                    grfx.FillPie(Brushes.Gold, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaInjGaz, BubbleList.alphaInjGaz);
                                    grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                    grfx.DrawPie(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaInjGaz, BubbleList.alphaInjGaz);
                                }
                                r = 0;
                                if (BubbleList.RLiq > 0)
                                {
                                    r = radX * BubbleList.RLiq * BubbleList.K;
                                    if ((bsIconSize) && (r < sIcon.Width / 4))
                                    {
                                        r = sIcon.Width / 4;
                                    }
                                    grfx.FillEllipse(Brushes.LightGreen, screenX - r, screenY - r, 2 * r, 2 * r);
                                    grfx.FillPie(Brushes.Peru, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                    grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                    grfx.DrawPie(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                }
                                break;
                            #endregion

                            #region BUBBLE_ACCUM
                            case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                int itemPos = 0;
                                if (BubbleList.BubbleSignPos != null)
                                {
                                    while (itemPos < BubbleList.BubbleSignPos.Count)
                                    {
                                        switch (BubbleList.BubbleSignPos[itemPos].Type)
                                        {
                                            case Constant.BUBBLE_SIGN_TYPE.Oil:
                                                r = 0;
                                                if (BubbleList.RLiq > 0)
                                                {
                                                    r = radX * BubbleList.RLiq * BubbleList.K;
                                                    if ((bsIconSize) && (r < sIcon.Width / 4)) //  && (st_XUnitsInOnePixel <= 15) ??
                                                    {
                                                        r = sIcon.Width / 4;
                                                    }
                                                    grfx.FillEllipse(Brushes.LightGreen, screenX - r, screenY - r, 2 * r, 2 * r);
                                                    grfx.FillPie(Brushes.Peru, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                                    grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                                    grfx.DrawPie(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                                }
                                                break;
                                            case Constant.BUBBLE_SIGN_TYPE.Inj:
                                                r = 0;
                                                if (BubbleList.RSumInj > 0)
                                                {
                                                    r = radX * BubbleList.RSumInj * BubbleList.K;
                                                    if ((bsIconSize) && (r < sIcon.Width / 4)) //  && (st_XUnitsInOnePixel <= 15) ??
                                                    {
                                                        r = sIcon.Width / 4;
                                                    }
                                                    grfx.FillEllipse(Brushes.SkyBlue, screenX - r, screenY - r, 2 * r, 2 * r);
                                                    grfx.FillPie(Brushes.Gold, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaInjGaz, BubbleList.alphaInjGaz);
                                                    grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                                    grfx.DrawPie(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaInjGaz, BubbleList.alphaInjGaz);
                                                }
                                                break;
                                            case Constant.BUBBLE_SIGN_TYPE.Gas:
                                                r = 0;
                                                if (BubbleList.RSumNatGas > 0)
                                                {
                                                    r = radX * BubbleList.RSumNatGas * BubbleList.K;
                                                    if ((bsIconSize) && (r < sIcon.Width / 4)) //  && (st_XUnitsInOnePixel <= 15) ??
                                                    {
                                                        r = sIcon.Width / 4;
                                                    }
                                                    grfx.FillEllipse(Brushes.Yellow, screenX - r, screenY - r, 2 * r, 2 * r);
                                                    grfx.FillPie(Brushes.YellowGreen, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaGasCond, BubbleList.alphaGasCond);
                                                    grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                                    grfx.DrawPie(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaGasCond, BubbleList.alphaGasCond);
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                        itemPos++;
                                    }
                                }
                                break;
                            #endregion

                            #region BUBBLE_HISTORY
                            case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                if (BubbleList.hBySector)      // приросты по секторам
                                {
                                    if (BubbleList.RLiq > BubbleList.hRLiq)
                                    {
                                        if (BubbleList.Liq > 0)
                                        {
                                            r = (float)(radX * BubbleList.RLiq * BubbleList.K);
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.LightGreen, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                        if (BubbleList.HLiq > 0)
                                        {
                                            hr = (float)(radX * BubbleList.hRLiq * BubbleList.K);
                                            if ((bsIconSize) && (hr < sIcon.Width / 4))
                                            {
                                                hr = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.DarkGray, screenX - hr, screenY - hr, 2 * hr, 2 * hr);
                                            grfx.DrawEllipse(Pens.Black, screenX - hr, screenY - hr, 2 * hr, 2 * hr);
                                        }
                                        maxR = r;
                                    }
                                    else
                                    {
                                        if (BubbleList.HLiq > 0)
                                        {
                                            hr = (float)(radX * BubbleList.hRLiq * BubbleList.K);
                                            if ((bsIconSize) && (hr < sIcon.Width / 4))
                                            {
                                                hr = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.DarkGray, screenX - hr, screenY - hr, 2 * hr, 2 * hr);
                                            grfx.DrawEllipse(Pens.Black, screenX - hr, screenY - hr, 2 * hr, 2 * hr);
                                        }
                                        if (BubbleList.Liq > 0)
                                        {
                                            r = (float)(radX * BubbleList.RLiq * BubbleList.K);
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.LightGreen, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                        maxR = hr;
                                    }


                                    if ((BubbleList.halphaOil < BubbleList.alphaOil) && (BubbleList.Oil > 0))
                                    {
                                        grfx.FillPie(Brushes.Red, screenX - maxR, screenY - maxR, 2 * maxR, 2 * maxR, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                        grfx.FillPie(Brushes.Peru, screenX - maxR, screenY - maxR, 2 * maxR, 2 * maxR, 270 - BubbleList.halphaOil, BubbleList.halphaOil);
                                    }
                                    else if (BubbleList.halphaOil > 0)
                                    {
                                        grfx.FillPie(Brushes.Blue, screenX - maxR, screenY - maxR, 2 * maxR, 2 * maxR, 270 - BubbleList.halphaOil, BubbleList.halphaOil);
                                        grfx.FillPie(Brushes.Peru, screenX - maxR, screenY - maxR, 2 * maxR, 2 * maxR, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                    }
                                    if (maxR > 0)
                                    {
                                        grfx.DrawPie(Pens.Black, screenX - maxR, screenY - maxR, 2 * maxR, 2 * maxR, 270 - BubbleList.halphaOil, BubbleList.halphaOil);
                                        grfx.DrawPie(Pens.Black, screenX - maxR, screenY - maxR, 2 * maxR, 2 * maxR, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                    }
                                    if (BubbleList.InjGas > BubbleList.HInjGas)
                                    {
                                        if (BubbleList.InjGas > 0)
                                        {
                                            r = radX * BubbleList.RInjGas * BubbleList.K;
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.Gold, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                        if (BubbleList.HInjGas > 0)
                                        {
                                            r = radX * BubbleList.hRInjGas * BubbleList.K;
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.Goldenrod, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                    }
                                    else
                                    {
                                        if (BubbleList.HInjGas > 0)
                                        {
                                            r = radX * BubbleList.hRInjGas * BubbleList.K;
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.Goldenrod, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                        if (BubbleList.InjGas > 0)
                                        {
                                            r = radX * BubbleList.RInjGas * BubbleList.K;
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.Gold, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                    }
                                    if (BubbleList.Inj > BubbleList.HInj)
                                    {
                                        if (BubbleList.Inj > 0)
                                        {
                                            r = radX * BubbleList.RInj * BubbleList.K;
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.SkyBlue, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                        if (BubbleList.HInj > 0)
                                        {
                                            r = radX * BubbleList.hRInj * BubbleList.K;
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.DarkGray, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                    }
                                    else
                                    {
                                        if (BubbleList.HInj > 0)
                                        {
                                            r = radX * BubbleList.hRInj * BubbleList.K;
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.DarkGray, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                        if (BubbleList.Inj > 0)
                                        {
                                            r = radX * BubbleList.RInj * BubbleList.K;
                                            if ((bsIconSize) && (r < sIcon.Width / 4))
                                            {
                                                r = sIcon.Width / 4 + 2;
                                            }
                                            grfx.FillEllipse(Brushes.SkyBlue, screenX - r, screenY - r, 2 * r, 2 * r);
                                            grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        }
                                    }
                                }
                                else // View by Column
                                {
                                    br1 = Brushes.LightGreen;
                                    br2 = SmartPlusGraphics.Well.Brushes.OilUp;

                                    float strX;
                                    if ((Math.Abs(BubbleList.Liq - BubbleList.HLiq) > 0.001) && (BubbleList.Liq > -1) && (BubbleList.HLiq > -1) && (BubbleList.HLiq >= BubbleList.Liq))
                                        br1 = Brushes.ForestGreen;

                                    if ((Math.Abs(BubbleList.Oil - BubbleList.HOil) > 0.001) && (BubbleList.Oil > -1) && (BubbleList.HOil > -1) && (BubbleList.HOil >= BubbleList.Oil))
                                        br2 = Brushes.SaddleBrown;

                                    if (BubbleList.RLiq > 0)
                                    {
                                        r = (float)(radX * BubbleList.RLiq * BubbleList.K);
                                        grfx.FillEllipse(br1, screenX - r, screenY - r, 2 * r, 2 * r);
                                        grfx.FillPie(br2, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                        grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                        grfx.DrawPie(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r, 270 - BubbleList.alphaOil, BubbleList.alphaOil);
                                    }
                                    if ((bsIconSize) && (r < sIcon.Width / 4))
                                    {
                                        r = sIcon.Width / 4 + 2;
                                    }

                                    if (sIcon.Width == 0)
                                        colX = screenX - (radX / 2) - 2;
                                    else
                                        colX = screenX - sIcon.Width / 2 - 2;

                                    if (VisibleHBubbleCol)
                                    {
                                        if ((Math.Abs(BubbleList.Liq - BubbleList.HLiq) > 0.001) && (BubbleList.Liq > -1) && (BubbleList.HLiq > -1) && (st_VisbleLevel < 15))
                                        {
                                            IsLimited = false;
                                            dQoil = Math.Abs(BubbleList.Liq - BubbleList.HLiq);
                                            colY = (float)dQoil * radX * BubbleList.K * BubbleList.K;
                                            if (colY > r)
                                            {
                                                IsLimited = true;
                                                colY = r;
                                            }
                                            if (st_VisbleLevel < 8)
                                            {
                                                str = dQoil.ToString("0.##");
                                                s = grfx.MeasureString(str, font);
                                            }
                                            strX = colX - (3 * radX / 2) - s.Width;

                                            if (BubbleList.HLiq < BubbleList.Liq)
                                            {
                                                grfx.FillRectangle(br1, colX - (5f * radX / 2f), screenY - colY, radX, colY);
                                                grfx.DrawRectangle(Pens.Black, colX - (5f * radX / 2f), screenY - colY, radX, colY);
                                                if (st_VisbleLevel < 8) grfx.DrawString(str, font, Brushes.ForestGreen, strX, screenY);
                                            }
                                            else
                                            {
                                                grfx.FillRectangle(br1, colX - (5f * radX / 2f), screenY, radX, colY);
                                                grfx.DrawRectangle(Pens.Black, colX - (5f * radX / 2f), screenY, radX, colY);
                                                if (st_VisbleLevel < 8) grfx.DrawString(str, font, SmartPlusGraphics.Well.Brushes.LiqDownStr, strX, screenY - s.Height);
                                            }
                                            if (IsLimited)
                                            {
                                                if (BubbleList.HLiq < BubbleList.Liq)
                                                    grfx.DrawLine(Pens.Black, colX - 3 * radX, screenY - colY, colX - radX, screenY - colY);
                                                else
                                                    grfx.DrawLine(Pens.Black, colX - 3 * radX, screenY + colY, colX - radX, screenY + colY);
                                            }
                                            grfx.DrawLine(Pens.Black, colX - radX * 3, screenY, colX, screenY);
                                        }

                                        if ((Math.Abs(BubbleList.Oil - BubbleList.HOil) > 0.001) && (BubbleList.Oil > -1) && (BubbleList.HOil > -1) && (st_VisbleLevel < 15))
                                        {
                                            IsLimited = false;
                                            dQoil = Math.Abs(BubbleList.Oil - BubbleList.HOil);
                                            colY = (float)dQoil * radX * BubbleList.K * BubbleList.K;
                                            if (colY > r)
                                            {
                                                IsLimited = true;
                                                colY = r;
                                            }
                                            if (st_VisbleLevel < 8)
                                            {
                                                str = dQoil.ToString("0.##");
                                                s = grfx.MeasureString(str, font);
                                            }

                                            strX = colX - (3 * radX / 2);

                                            if (BubbleList.HOil < BubbleList.Oil)
                                            {
                                                grfx.FillRectangle(br2, colX - (3 * radX / 2), screenY - colY, radX, colY);
                                                grfx.DrawRectangle(Pens.Black, colX - (3 * radX / 2), screenY - colY, radX, colY);
                                                if (st_VisbleLevel < 8) grfx.DrawString(str, font, Brushes.SaddleBrown, strX, screenY);
                                                if (IsLimited) grfx.DrawLine(Pens.Black, colX - 2 * radX, screenY - colY, colX, screenY - colY);
                                            }
                                            else
                                            {
                                                grfx.FillRectangle(br2, colX - (3 * radX / 2), screenY, radX, colY);
                                                grfx.DrawRectangle(Pens.Black, colX - (3 * radX / 2), screenY, radX, colY);
                                                if (st_VisbleLevel < 8) grfx.DrawString(str, font, SmartPlusGraphics.Well.Brushes.OilDownStr, strX, screenY - s.Height);
                                                if (IsLimited) grfx.DrawLine(Pens.Black, colX - 2 * radX, screenY + colY, colX, screenY + colY);
                                            }
                                            grfx.DrawLine(Pens.Black, colX - radX * 3, screenY, colX, screenY);
                                        }
                                    }

                                    // Column Injection
                                    br1 = Brushes.SkyBlue;
                                    if ((Math.Abs(BubbleList.Inj - BubbleList.HInj) > 0.001) && (BubbleList.Inj > -1) && (BubbleList.HInj > -1) && (BubbleList.hRInj >= BubbleList.RInj))
                                    {
                                        br1 = SmartPlusGraphics.Well.Brushes.InjDown;
                                    }
                                    if (BubbleList.RInj > 0)
                                    {
                                        r = radX * BubbleList.RInj * BubbleList.K;
                                        grfx.FillEllipse(br1, screenX - r, screenY - r, 2 * r, 2 * r);
                                        grfx.DrawEllipse(Pens.Black, screenX - r, screenY - r, 2 * r, 2 * r);
                                    }
                                    if ((bsIconSize) && (r < sIcon.Width / 4))
                                    {
                                        r = sIcon.Width / 4 + 2;
                                    }
                                    if (VisibleHBubbleCol)
                                    {
                                        if ((Math.Abs(BubbleList.Inj - BubbleList.HInj) > 0.001) && (BubbleList.Inj > -1) && (BubbleList.HInj > -1) && (st_VisbleLevel < 15))
                                        {
                                            if (sIcon.Width == 0)
                                                colX = screenX - (radX / 2) - 2;
                                            else
                                                colX = screenX - sIcon.Width / 2 - 2;

                                            dQoil = Math.Abs(BubbleList.Inj - BubbleList.HInj);
                                            colY = (float)dQoil * radX * BubbleList.K * BubbleList.K;
                                            if (colY > r)
                                            {
                                                IsLimited = true;
                                                colY = r;
                                            }
                                            if (st_VisbleLevel < 8)
                                            {
                                                str = dQoil.ToString("0.##");
                                                s = grfx.MeasureString(str, font);
                                            }

                                            if (BubbleList.hRInj < BubbleList.RInj)
                                            {
                                                grfx.FillRectangle(br1, colX - (3 * radX / 2), screenY - colY, radX, colY);
                                                grfx.DrawRectangle(Pens.Black, colX - (3 * radX / 2), screenY - colY, radX, colY);
                                                if (st_VisbleLevel < 8)
                                                {
                                                    grfx.DrawString(str, font, SmartPlusGraphics.Well.Brushes.InjDown, colX - radX - s.Width / 2, screenY);
                                                }
                                                if (IsLimited) grfx.DrawLine(Pens.Black, colX - radX * 2, screenY - colY, colX, screenY - colY);
                                            }
                                            else
                                            {
                                                grfx.FillRectangle(br1, colX - (3 * radX / 2), screenY, radX, colY);
                                                grfx.DrawRectangle(Pens.Black, colX - (3 * radX / 2), screenY, radX, colY);
                                                if (st_VisbleLevel < 8)
                                                {
                                                    grfx.DrawString(str, font, SmartPlusGraphics.Well.Brushes.InjDownStr, colX - radX - s.Width / 2, screenY - s.Height);
                                                }
                                                if (IsLimited) grfx.DrawLine(Pens.Black, colX - radX * 2, screenY + colY, colX, screenY + colY);
                                            }
                                            grfx.DrawLine(Pens.Black, colX - radX * 2, screenY, colX, screenY);
                                        }
                                    }
                                }
                                break;
                            #endregion
                        }
                    }
                }
            }
        }
        public void DrawWellHead(Graphics grfx, OilfieldCoefItem coef)
        {
            float screenX, screenY, screenX2, screenY2;
            if ((st_VisbleLevel < st_OilfieldXUnits) && (CoordLoaded) && (this.coord.Altitude.X != 0) && (this.coord.Altitude.Y != 0) && (!TrajectoryLoaded || !TrajectoryVisible))
            {
                //coef.GlobalCoordFromLocal(this.coord.Altitude.X, this.coord.Altitude.Y, out screenX, out screenY);
                screenX = (float)this.coord.Altitude.X; screenY = (float)this.coord.Altitude.Y;
                if (Math.Abs(X - screenX) < 5000 && Math.Abs(this.Y - screenY) < 5000)
                {
                    if (DrawAllObjects || st_RealScreenRect.Contains(screenX, screenY) || st_RealScreenRect.Contains(X, Y))
                    {
                        screenX = ScreenXfromRealX(screenX);
                        screenY = ScreenYfromRealY(screenY);

                        if (st_VisbleLevel > 15)
                        {
                            grfx.FillRectangle(SmartPlusGraphics.Well.Brushes.WellHead, screenX - st_radiusX / 2, screenY - st_radiusX / 2, st_radiusX, st_radiusX);
                        }
                        else
                        {
                            grfx.DrawEllipse(SmartPlusGraphics.Well.Pens.WellHead, (screenX - st_radiusX / 2), (screenY - st_radiusX / 2), st_radiusX, st_radiusX);
                            if (st_radiusX / 8 > 0.1)
                            {
                                grfx.FillEllipse(SmartPlusGraphics.Well.Brushes.WellHead, (screenX - st_radiusX / 8), (screenY - st_radiusX / 8), st_radiusX / 4, st_radiusX / 4);
                            }
                        }
                        if ((X != Constant.DOUBLE_NAN) && (Y != Constant.DOUBLE_NAN))
                        {
                            screenX2 = ScreenXfromRealX(X);
                            screenY2 = ScreenYfromRealY(Y);
                            grfx.DrawLine(SmartPlusGraphics.Well.Pens.WellHead, screenX, screenY, screenX2, screenY2);
                        }
                    }
                }
            }
        }
        public void DrawTrajectory(Graphics grfx)
        {
            float screenX, screenY, screenX2, screenY2;
            if (TrajectoryLoaded && (st_VisbleLevel < st_OilfieldXUnits) && TrajectoryVisible && trajectory.Count > 0)
            {
                int j;

                screenX = ScreenXfromRealX(trajectory[0].X);
                screenY = ScreenYfromRealY(trajectory[0].Y);
                j = 0;
                if (DrawAllObjects || st_RealScreenRect.Contains(trajectory[0].X, trajectory[0].Y) || st_RealScreenRect.Contains(trajectory[trajectory.Count - 1].X, trajectory[trajectory.Count - 1].Y))
                {
                    if (st_VisbleLevel > 15)
                    {
                        grfx.FillRectangle(Brushes.Blue, screenX - st_radiusX / 2, screenY - st_radiusX / 2, st_radiusX, st_radiusX);
                    }
                    else
                    {
                        grfx.DrawEllipse(Pens.Blue, (screenX - st_radiusX / 2), (screenY - st_radiusX / 2), st_radiusX, st_radiusX);
                        if (st_radiusX / 8 > 0.1)
                        {
                            grfx.FillEllipse(Brushes.Blue, (screenX - st_radiusX / 8), (screenY - st_radiusX / 8), st_radiusX / 4, st_radiusX / 4);
                        }
                    }
                    for (int i = 1; i < trajectory.Count; i++)
                    {
                        if (i == trajectory.Count - 1 || Math.Abs(trajectory[i].X - trajectory[j].X) > 25 || Math.Abs(trajectory[i].Y - trajectory[j].Y) > 25)
                        {
                            screenX2 = ScreenXfromRealX(trajectory[i].X);
                            screenY2 = ScreenYfromRealY(trajectory[i].Y);

                            grfx.DrawLine(SmartPlusGraphics.Well.Pens.WellTrajectory, screenX, screenY, screenX2, screenY2);

                            screenX = screenX2;
                            screenY = screenY2;
                            j = i;
                        }
                    }
                }
            }
        }
        void DrawLetterBox(Graphics grfx, float x, float y)
        {
            if (ShowLetterBox && LetterBoxLevel > -1 && (!ShowAreasMode || st_XUnitsInOnePixel < 30))
            {
                Bitmap bmp = null;
                switch (LetterBoxLevel)
                {
                    case 0:
                        bmp = LetterBoxImageGray;
                        break;
                    case 1:
                        bmp = LetterBoxImageOrange;
                        break;
                    case 2:
                        bmp = LetterBoxImageGreen;
                        break;
                }
                if (bmp != null)
                {
                    int side = 12;
                    Rectangle rect = new Rectangle((int)(x), (int)(y - side / 2), side, side);
                    grfx.DrawImage(bmp, rect);
                }
            }
        }
        public override void DrawObject(Graphics grfx)
        {
            float screenX, screenY;
            double realX, realY;
            SizeF s = SizeF.Empty;
            string str = string.Empty;
            WellIcon icon = null;

            if ((Visible) && (CoordLoaded) && (st_VisbleLevel <= st_OilfieldXUnits) && (X != Constant.DOUBLE_NAN) && (Y != Constant.DOUBLE_NAN))
            {
                icon = icons.GetActiveIcon();
                if ((BubbleList != null) && (BubbleList.Visible))
                {
                    icon = BubbleIcons.GetActiveIcon();
                }

                if (icon == null)
                {
                    realX = X;
                    realY = Y;
                }
                else
                {
                    realX = icon.X;
                    realY = icon.Y;
                }
                if (C2DObject.DrawAllObjects || st_RealScreenRect.Contains(realX, realY))
                {
                    screenX = ScreenXfromRealX(realX);
                    screenY = ScreenYfromRealY(realY);

                    float radX = (float)(50.31f * Math.Pow(st_XUnitsInOnePixel, -1.013));

                    float scrTextX;

                    SizeF sIcon = SizeF.Empty;

                    if (icon != null) sIcon = icon.GetIconSize(grfx);
                    bool bsIconSize = ((sIcon.Width > 0) && (st_XUnitsInOnePixel <= 15));

                    if (Selected)
                    {
                        grfx.FillEllipse(Brushes.Orange, screenX - st_radiusX, screenY - st_radiusX, st_radiusX * 2, st_radiusX * 2);
                    }
                    //if (SelectedByProfile)
                    //{
                    //    using (Pen pen = new Pen(Color.Lime, 3))
                    //    {
                    //        float radPrfl = st_radiusX * 1.5f;
                    //        if (radPrfl < 5) radPrfl = 5;
                    //        grfx.DrawEllipse(pen, screenX - radPrfl, screenY - radPrfl, radPrfl * 2, radPrfl * 2);
                    //    }
                    //}
                    if ((st_VisbleLevel > 15) && ((icon == null) || (icon.GetMaxIconFontSize() < 11)))
                    {
                        grfx.FillRectangle(brush, screenX - st_radiusX / 2, screenY - st_radiusX / 2, st_radiusX, st_radiusX);
                    }
                    else
                    {
                        if (icon == null)
                            grfx.FillEllipse(Brushes.LightGray, screenX - (st_radiusX / 2), screenY - (st_radiusX / 2), st_radiusX, st_radiusX);
                        else
                            if (icon != null) icon.DrawObject(grfx);
                    }
                    if (sIcon.Width == 0)
                    {
                        scrTextX = screenX + (radX / 2) + 2;
                    }
                    else
                    {
                        scrTextX = screenX + sIcon.Width / 4;
                    }

                    // Text
                    if ((st_VisbleLevel < 15) && (font != null))
                    {
                        if ((BubbleList != null) && BubbleList.Visible && VisibleText)
                        {
                            str = "";
                            switch (BubbleList.Type)
                            {
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                    if (BubbleList.Liq > 0)
                                    {
                                        if (BubbleList.LiquidInCube)
                                        {
                                            str = string.Format("{0:0} - {1:0}%", BubbleList.LiqV, ((BubbleList.LiqV - BubbleList.OilV) * 100) / BubbleList.LiqV);
                                        }
                                        else
                                        {
                                            str = string.Format("{0:0} - {1:0}%", BubbleList.Liq, ((BubbleList.Liq - BubbleList.Oil) * 100) / BubbleList.Liq);
                                        }
                                    }
                                    if ((BubbleList.Inj + BubbleList.InjGas)> 0)
                                    {
                                        if (str != "") str += " - ";
                                        str += BubbleList.Inj.ToString("0");
                                        if (BubbleList.InjGas > 0)
                                        {
                                            if (str != "") str += " - ";
                                            str += BubbleList.InjGas.ToString("0");
                                        }
                                    }
                                    if ((BubbleList.NatGas + BubbleList.GasCond) > 0)
                                    {
                                        if (str != "") str += " - ";
                                        str += BubbleList.NatGas.ToString("0");
                                        if (BubbleList.GasCond > 0)
                                        {
                                            if (str != "") str += " - ";
                                            str += BubbleList.GasCond.ToString("0");
                                        }
                                    }
                                    break;
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                    if (BubbleList.Liq > 0)
                                    {
                                        str = string.Format("{0:0} - {1:0}", BubbleList.Liq - BubbleList.Oil, BubbleList.Oil);
                                    }
                                    if ((BubbleList.Inj + BubbleList.InjGas) > 0)
                                    {
                                        if (str != "") str += " - ";
                                        str += BubbleList.Inj.ToString("0");
                                        if (BubbleList.InjGas > 0)
                                        {
                                            if (str != "") str += " - ";
                                            str += BubbleList.InjGas.ToString("0");
                                        }
                                    }
                                    if ((BubbleList.NatGas + BubbleList.GasCond) > 0)
                                    {
                                        if (str != "") str += " - ";
                                        str += BubbleList.NatGas.ToString("0");
                                        if (BubbleList.GasCond > 0)
                                        {
                                            if (str != "") str += " - ";
                                            str += BubbleList.GasCond.ToString("0");
                                        }
                                    }
                                    break;
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                    if (BubbleList.HLiq > 0)
                                    {
                                        if (BubbleList.LiquidInCube)
                                        {
                                            str = string.Format("{0:0} - {1:0}", BubbleList.HLiqV, BubbleList.LiqV);
                                        }
                                        else
                                        {
                                            str = string.Format("{0:0} - {1:0}", BubbleList.HLiq, BubbleList.Liq);
                                        }
                                    }
                                    if (BubbleList.HInj > 0)
                                        str = BubbleList.HInj.ToString("0") + " - " + BubbleList.Inj.ToString("0");
                                    break;
                            }
                            if (st_VisbleLevel < 8)
                            {
                                if ((BubbleList.Liq > 0) || (BubbleList.Inj + BubbleList.InjGas > 0) || (BubbleList.NatGas + BubbleList.GasCond) > 0 || (BubbleList.HLiq > 0) || (BubbleList.HInj > 0))
                                {
                                    s = grfx.MeasureString(str, font);
                                    grfx.DrawLine(Pens.Black, scrTextX, screenY + s.Height, screenX + (radX / 2) + 2 + s.Width, screenY + s.Height);
                                    grfx.DrawString(str, font, Brushes.Black, scrTextX, screenY + s.Height);
                                }
                            }
                        }
                        if (VisibleName)
                        {
                            s = grfx.MeasureString(UpperCaseName, font, PointF.Empty, StringFormat.GenericTypographic);
                            if (this.Selected)
                            {
                                grfx.FillRectangle(Brushes.Orange, scrTextX, screenY + s.Height * 0.1f, s.Width, s.Height * 0.8f);
                                //grfx.DrawString(UpperCaseName, _font, Brushes.Orange, scrTextX - 1, screenY, StringFormat.GenericTypographic);
                                //grfx.DrawString(UpperCaseName, _font, Brushes.Orange, scrTextX + 1, screenY, StringFormat.GenericTypographic);
                                //grfx.DrawString(UpperCaseName, _font, Brushes.Orange, scrTextX, screenY - 1, StringFormat.GenericTypographic);
                                //grfx.DrawString(UpperCaseName, _font, Brushes.Orange, scrTextX, screenY + 1, StringFormat.GenericTypographic);
                            }
                            bool IsBubble = (BubbleList != null) && (BubbleList.RInj + BubbleList.RLiq > 0);
                            grfx.DrawString(UpperCaseName, font, IsBubble ? Brushes.Black : brush_name, scrTextX, screenY, StringFormat.GenericTypographic);
                        }

                        //if (this.CountAreas > 0)
                        //{
                        //    SizeF s = grfx.MeasureString(UpperCaseName, _font);
                        //    grfx.DrawString(this.CountAreas.ToString(), _font, Brushes.Black, screenX + (radX / 2) + 2 + s.Width + 2, screenY);
                        //    //TextRenderer.DrawText(grfx, UpperCaseName, _font, new Point((int)(screenX + (radX / 2) + 2), (int)screenY), Color.Black, Color.Transparent);
                        //}
                    }
                    DrawLetterBox(grfx, scrTextX, screenY);
                }
            }
        }
    }
}
