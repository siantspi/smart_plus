﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;
using RDF;
using SmartPlus.DictionaryObjects;
using System.IO;
using RDF.Objects;
using System.Windows.Forms;
using System.Data;
using System.Data.OracleClient;
using System.Drawing;

namespace SmartPlus
{
    public sealed class OilField : C2DObject
    {
        public static int VersionStratumList = 2;
        Project project;

        public OilFieldParamsItem ParamsDict { get; set; }
        public OilfieldCoefItem CoefDict { get; set; }

        public OilFieldParamsDictionary UnionParamsDict;
        
        public RdfConnector RdfConn;
        public RectangleD MinMaxXY;
        public bool LoadUnionObject;
        public bool MerLoaded;
        public bool GisLoaded;
        public bool PerfLoaded;
        public bool LogsBaseLoaded;
        public bool LogsLoaded;

        public bool ChessLoaded;
        public bool CoordLoaded;
        public bool ProjWellLoaded;
        public bool ContoursDataLoaded;

        public bool WellGtmLoaded;
        public bool WellRepairActionLoaded;
        public bool WellResearchLoaded;
        public bool WellLeakLoaded;
        public bool WellSuspendLoaded;
        public bool BizPlanLoaded;
        public bool WellTrajectoryLoaded;

        public bool BubbleMapLoaded;
        public bool ContoursEdited;
        public bool AliasLoaded;
        public DateTime minMerDate;
        public DateTime maxMerDate;
        public int OilFieldCode
        {
            get { return ParamsDict.RDFFieldCode; }
            set { ParamsDict.RDFFieldCode = value; }
        }
        
        public C2DLayer WellsLayer;
        public SumParameters sumParams;
        public SumParameters sumChessParams;
        public Table81Params Table81;
        public OilfieldBizplanCollection BizPlanCollection;
        public List<Well> Wells;
        public List<Well> ProjWells;
        public List<Well> SortedWells;
        public List<WellPad> WellPads;
        public List<Contour> Contours;
        public List<Area> Areas;
        public List<Deposit> Deposits;
        public List<Grid> Grids;

        public PVTParams PVTList;

        public List<int> GeoStratumCodes;
        public List<int> MerStratumCodes;
        public List<int> OilFieldAreaList;
        public List<string> Aliases;

        private MemoryStream[] buffList = null;
        int lockThreadId = -1;
        public double[] BPParams;

        public OilField(Project project)
        {
            this.project = project;
            Index = -1;

            TypeID = Constant.BASE_OBJ_TYPES_ID.OILFIELD;
            
            Wells = new List<Well>();
            ProjWells = new List<Well>();
            WellPads = new List<WellPad>();
            Contours = new List<Contour>();
            Areas = new List<Area>();
            Deposits = new List<Deposit>();
            Grids = new List<Grid>();
            SortedWells = new List<Well>();
            this.minMerDate = DateTime.MinValue;
            this.maxMerDate = DateTime.MaxValue;
            BubbleMapLoaded = false;
            PVTList = null;
            RdfConn = new RdfConnector();

            MinMaxXY = new RectangleD();
            GeoStratumCodes = new List<int>(5);
            MerStratumCodes = new List<int>(5);
            OilFieldAreaList = new List<int>(5);
            Table81 = new Table81Params();
            Aliases = null;
            MerLoaded = false;
            ChessLoaded = false;
            CoordLoaded = false;
            ContoursEdited = false;
            AliasLoaded = false;
            ContoursDataLoaded = false;
            sumParams = null;
            sumChessParams = null;
            this.minMerDate = DateTime.MinValue;
            this.maxMerDate = DateTime.MaxValue;
            BPParams = new double[5];
        }
        public void Destroy()
        {
            Wells.Clear();
            Wells = null;
            ProjWells.Clear();
            ProjWells = null;
            WellPads.Clear();
            WellPads = null;
            Contours.Clear();
            Contours = null;
            Grids.Clear();
            Grids = null;
            Areas.Clear();
            Areas = null;
            Deposits.Clear();
            Deposits = null;
            GeoStratumCodes.Clear();
            MerStratumCodes.Clear();
            OilFieldAreaList.Clear();
            SortedWells.Clear();
            GeoStratumCodes = null;
            MerStratumCodes = null;
            OilFieldAreaList = null;
            RdfConn.Disconnect();
            RdfConn = null;
            CoefDict = null;
            ParamsDict = null;
            BizPlanCollection = null;
            BPParams = null;
        }

        private void ReportProgress(BackgroundWorker worker, float i, float len)
        {
            int percentComplete = (int)(i / len * 100);
            if (percentComplete < 0) percentComplete = 0;
            else if (percentComplete > 100) percentComplete = 100;
            worker.ReportProgress(percentComplete);
        }
        private void ReportProgress(BackgroundWorker worker, float i, float len, WorkerState UserState)
        {
            if (worker != null)
            {
                int percentComplete = (int)(i / (len + 1) * 100);
                if (percentComplete < 0) percentComplete = 0;
                else if (percentComplete > 100) percentComplete = 100;
                worker.ReportProgress(percentComplete, UserState);
            }
        }

        public bool ConnectToBase()
        {
            return ConnectToBase(@"\\" + ParamsDict.RdfServerName + ":" + ParamsDict.RdfPort.ToString(), ParamsDict.RdfLogin, ParamsDict.RdfPass);
        }
        public bool ConnectToBase(string aServ, string aUser, string aPass)
        {
            bool retValue = false;
            RdfConn.Connect(aUser, aPass, aServ);
            retValue = RdfConn.Connected;
            return retValue;
        }

        public void SetMinMaxRect(Well w)
        {
            if (w.X != Constant.DOUBLE_NAN && w.Y != Constant.DOUBLE_NAN)
            {
                if ((MinMaxXY.Left == Constant.DOUBLE_NAN) || (w.X < MinMaxXY.Left)) MinMaxXY.Left = w.X;
                if ((MinMaxXY.Top == Constant.DOUBLE_NAN) || (w.Y < MinMaxXY.Top)) MinMaxXY.Top = w.Y;
                if ((MinMaxXY.Right == Constant.DOUBLE_NAN) || (w.X > MinMaxXY.Right)) MinMaxXY.Right = w.X;
                if ((MinMaxXY.Bottom == Constant.DOUBLE_NAN) || (w.Y > MinMaxXY.Bottom)) MinMaxXY.Bottom = w.Y;
                X = MinMaxXY.Left + MinMaxXY.Width / 2;
                Y = MinMaxXY.Top + MinMaxXY.Height / 2;
            }
        }
        public void SetMinMaxRect(Contour cntr)
        {
            double x, y, x2, y2;
            if (cntr._points != null && cntr._points.Count > 0)
            {
                x = cntr._points.GetMinX();
                x2 = cntr._points.GetMaxX();
                y = cntr._points.GetMinY();
                y2 = cntr._points.GetMaxY();
                if (MinMaxXY.Left == Constant.DOUBLE_NAN || x < MinMaxXY.Left) MinMaxXY.Left = x;
                if (MinMaxXY.Top == Constant.DOUBLE_NAN || y < MinMaxXY.Top) MinMaxXY.Top = y;
                if (MinMaxXY.Right == Constant.DOUBLE_NAN || x2 > MinMaxXY.Right) MinMaxXY.Right = x2;
                if (MinMaxXY.Bottom == Constant.DOUBLE_NAN || y2 > MinMaxXY.Bottom) MinMaxXY.Bottom = y2;
            }
        }

        #region GET WELL INDEX
        public int GetWellIndex(string UpWellName, int OilFieldAreaCode)
        {
            Well w;
            for (int i = 0; i < Wells.Count; i++)
            {
                w = (Well)Wells[i];
                if ((UpWellName.Equals(w.UpperCaseName, StringComparison.OrdinalIgnoreCase)) && (OilFieldAreaCode == w.OilFieldAreaCode))
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetWellIndex(string UpWellName)
        {
            for (int i = 0; i < Wells.Count; i++)
            {
                if (UpWellName.Equals(((Well)Wells[i]).UpperCaseName, StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetProjWellIndex(string UpWellName)
        {
            if (ProjWells != null)
            {
                for (int i = 0; i < ProjWells.Count; i++)
                {
                    if (UpWellName.Equals(ProjWells[i].UpperCaseName, StringComparison.OrdinalIgnoreCase))
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        public List<int> GetWellIndexList(string WellName)
        {
            List<int> list = new List<int>(5);
            for (int i = 0; i < Wells.Count; i++)
            {
                if (WellName.Equals(Wells[i].Name, StringComparison.OrdinalIgnoreCase))
                {
                    list.Add(i);
                }
            }
            return list;
        }

        public List<int> TryGetWellIndexList(string WellName)
        {
            List<int> listInd = new List<int>(5);
            if ((WellName != null) && (WellName != ""))
            {
                string[] ConversionStartSymbols = new string[] { "С", "C", "С1", "C1" };

                listInd = GetWellIndexList(WellName);
                if (listInd.Count == 0)
                {
                    int i;
                    string str1, str2, tempName;
                   
                    listInd = GetWellIndexList(WellName);
                    if (listInd.Count == 0)
                    {
                        for (i = 0; i < ConversionStartSymbols.Length - 1; i += 2)
                        {
                            str1 = ConversionStartSymbols[i];
                            str2 = ConversionStartSymbols[i + 1];

                            if (WellName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                            {
                                tempName = str2 + WellName.Remove(0, str1.Length);
                                listInd = GetWellIndexList(tempName);
                                if (listInd.Count != 0)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return listInd;
        }

        public int TestEqualsWell(string UpWellName)
        {
            int count = -1;
            int k = -1;
            for (int i = 0; i < Wells.Count; i++)
            {
                if (UpWellName == Wells[i].UpperCaseName)
                {
                    count++;
                    k = i;
                    if (count > 0)
                    {
                        count = -2;
                        break;
                    }
                }
            }
            if ((count == 0) && (k > -1)) return k;
            return count;
        }
        #endregion

        #region SERVICE FUNCTIONS
        public void CompareGisLogs(BackgroundWorker worker, DoWorkEventArgs e, string fileName)
        {
            ArrayList list = new ArrayList();
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Сравнение ГИС и Каротажек";
            Well w;
            this.LoadGisFromCache(worker, e);
            this.LoadLogsBaseFromCache(worker, e, false, false);
            //this.LoadZipLogsDataFromCache(worker, e, false, true);
            for (int i = 0; i < Wells.Count; i++)
            {
                w = (Well)Wells[i];
                w.CompareGisLogs(project, list);
                userState.Element = w.UpperCaseName + "[" + this.Name + "]";
                worker.ReportProgress(100, userState);
            }
            if (list.Count > 0)
            {
                StreamWriter file = new StreamWriter(fileName, true, System.Text.Encoding.GetEncoding(1251));
                for (int i = 0; i < list.Count; i++)
                {
                    file.WriteLine((string)list[i]);
                }
                file.Close();
                list.Clear();
            }
            ClearBufferList(false);
            ClearGisData(false);
            ClearLogsData(false);
            ClearLogsBaseData(true);
        }
        #endregion

        #region MEMORY BUFFER LIST
        private MemoryStream GetBuffer(bool IsPrimary)
        {
            if (lockThreadId == -1 || lockThreadId == System.Threading.Thread.CurrentThread.ManagedThreadId)
            {
                lockThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
                int index = -1;
                if (buffList == null)
                {
                    try
                    {
                        buffList = new MemoryStream[2];
                    }
                    catch
                    {
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        buffList = new MemoryStream[2];
                    }
                }
                index = (IsPrimary) ? 0 : 1;
                if (buffList[index] == null) // todo System.OutOfMemoryException
                {
                    try
                    {
                        buffList[index] = new MemoryStream(1 * 1024 * 1024);
                    }
                    catch
                    {
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        buffList[index] = new MemoryStream(1 * 1024 * 1024);
                    }
                }
                else
                {
                    buffList[index].SetLength(1 * 1024 * 1024);
                    this.ClearMemoryStream(buffList[index]);
                    if (buffList == null || buffList[index].Length != (1 * 1024 * 1024))
                    {
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        buffList[index] = new MemoryStream(1 * 1024 * 1024);
                    }
                }
                return buffList[index];
            }
            else
            {
                try
                {
                    return new MemoryStream(1 * 1024 * 1024);
                }
                catch
                {
                     GC.Collect();
                     GC.WaitForPendingFinalizers();
                    return new MemoryStream(1 * 1024 * 1024);
                }
             }
                
           
        }
        private void FreeBuffers()
        {
            if (lockThreadId != -1 && lockThreadId == System.Threading.Thread.CurrentThread.ManagedThreadId)
            {
                lockThreadId = -1;
            }
        }
        public void ClearBufferList(bool GCCollect)
        {
            if (lockThreadId == -1 || lockThreadId == System.Threading.Thread.CurrentThread.ManagedThreadId)
            {
                if (buffList != null)
                {
                    for (int i = 0; i < 2; i++) buffList[i] = null;
                }
                buffList = null;
                if (GCCollect) GC.GetTotalMemory(true);
            }
        }
        void ClearMemoryStream(MemoryStream ms)
        {
            ms.Seek(0, SeekOrigin.Begin);
            while (ms.Position < ms.Length) ms.WriteByte(0);
            ms.Seek(0, SeekOrigin.Begin);
        }
        #endregion

        #region ALIASES
        public void AddAlias(string NewAlias)
        {
            if (Aliases == null) Aliases = new List<string>();
            if (!TestAlias(NewAlias)) Aliases.Add(NewAlias);
            AliasLoaded = true;
            WriteAliasesToCache();
        }
        public bool TestAlias(string alias)
        {
            bool result = false;
            if (Aliases != null)
            {
                for (int i = 0; i < Aliases.Count; i++)
                {
                    if (Aliases[i].Equals(alias, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }
            }
            return result;
        }
        public void RemoveAlias(string RemoveAlias)
        {
            if (Aliases != null)
            {
                int index = -1;
                for (int i = 0; i < Aliases.Count; i++)
                {
                    if (Aliases[i].Equals(RemoveAlias, StringComparison.OrdinalIgnoreCase))
                    {
                        index = i;
                        break;
                    }
                }
                if (index != -1) Aliases.RemoveAt(index);
                if (Aliases.Count == 0)
                {
                    Aliases = null;
                    AliasLoaded = false;
                }
            }
            WriteAliasesToCache();
        }
        public bool LoadAliasesFromCache()
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\alias.bin";
            try
            {
                if (File.Exists(cacheName))
                {
                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    int i, len;
                    len = br.ReadInt32();
                    Aliases = new List<string>(len);
                    string aliasName;
                    for (i = 0; i < len; i++)
                    {
                        aliasName = br.ReadString();
                        Aliases.Add(aliasName);
                    }
                    br.Close();
                    retValue = true;
                }
            }
            catch
            {
                Aliases = null;
                retValue = false;
            }
            AliasLoaded = retValue;
            return retValue;
        }
        public bool WriteAliasesToCache()
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\alias.bin";
            if ((AliasLoaded) && (Aliases != null))
            {
                try
                {
                    FileStream fs = new FileStream(cacheName, FileMode.Create);
                    BinaryWriter bw = new BinaryWriter(fs);
                    int i;
                    bw.Write(Aliases.Count);
                    for (i = 0; i < Aliases.Count; i++)
                    {
                        bw.Write(Aliases[i]);
                    }
                    bw.Close();
                    retValue = true;
                }
                catch
                {
                    retValue = false;
                }
            }
            else
            {
                if (File.Exists(cacheName)) File.Delete(cacheName);
            }
            return retValue;
        }
        #endregion

        #region STRATUMS OBJECTS
        public bool RecalcStratumCodes(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            Well w;
            int i, j;
            bool CallMerLoading = false, CallGisLoading = false;
            if (!GisLoaded)
            {
                LoadGisFromCache(worker, e);
                CallGisLoading = true;
            }
            if (!MerLoaded)
            {
                LoadMerFromCache(worker, e, false);
                CallMerLoading = true;
            }
            ArrayList sumLiqInjOilObj = new ArrayList();
            MerItem item;
            MerItemEx itemEx;
            int index;
            GeoStratumCodes.Clear();
            MerStratumCodes.Clear();
            DateTime MaxDate = DateTime.MinValue, MinDate = DateTime.MaxValue;
            for (i = 0; i < this.Wells.Count; i++)
            {
                w = (Well)this.Wells[i];
                if (w.GisLoaded)
                {
                    for (j = 0; j < w.gis.Count; j++)
                    {
                        if ((w.gis.Items[j].PlastId != 0) && (this.GeoStratumCodes.IndexOf(w.gis.Items[j].PlastId) == -1))
                        {
                            this.GeoStratumCodes.Add(w.gis.Items[j].PlastId);
                        }
                        retValue = true;
                    }
                }
                if (w.MerLoaded)
                {
                    for (j = 0; j < w.mer.Count; j++)
                    {
                        item = w.mer.Items[j];
                        itemEx = w.mer.GetItemEx(j);
                        if (MinDate > item.Date) MinDate = item.Date;
                        if (MaxDate < item.Date) MaxDate = item.Date;

                        index = this.MerStratumCodes.IndexOf(item.PlastId);
                        if ((item.PlastId != 0) && (index == -1))
                        {
                            this.MerStratumCodes.Add(item.PlastId);
                            sumLiqInjOilObj.Add(item.Oil + item.Wat + item.Gas + itemEx.Injection + itemEx.GasCondensate + itemEx.NaturalGas);
                        }
                        else if (index > -1)
                        {
                            sumLiqInjOilObj[index] = (double)sumLiqInjOilObj[index] + item.Oil + item.Wat + item.Gas + itemEx.Injection + itemEx.GasCondensate + itemEx.NaturalGas;
                        }
                        retValue = true;
                    }
                }
            }
            if (retValue) // выбрасываем нулевые объекты
            {
                i = 0;
                while (i < sumLiqInjOilObj.Count)
                {
                    if ((double)sumLiqInjOilObj[i] == 0)
                    {
                        sumLiqInjOilObj.RemoveAt(i);
                        this.MerStratumCodes.RemoveAt(i);
                    }
                    else
                        i++;
                }
            }
            if (MinDate != DateTime.MaxValue) minMerDate = MinDate;
            if (MaxDate != DateTime.MinValue) maxMerDate = MaxDate;

            WriteStratumCodesToCache(); 

            if ((MerLoaded) && (CallMerLoading)) ClearMerData(false);
            if ((GisLoaded) && (CallGisLoading)) ClearGisData(true);

            return retValue;
        }
        public bool RecalcGeoCodes(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            this.LoadGisFromCache(worker, e);
            Well w;
            int i, j;
            GeoStratumCodes.Clear();
            for (i = 0; i < this.Wells.Count; i++)
            {
                w = (Well)this.Wells[i];
                if (w.GisLoaded)
                {
                    for (j = 0; j < w.gis.Count; j++)
                    {
                        if ((w.gis.Items[j].PlastId != 0) && (this.GeoStratumCodes.IndexOf(w.gis.Items[j].PlastId) == -1))
                        {
                            this.GeoStratumCodes.Add(w.gis.Items[j].PlastId);
                        }
                        retValue = true;
                    }
                }
            }
            if (GeoStratumCodes.Count > 0)
                WriteStratumCodesToCache();
            return retValue;
        }
        public bool LoadStratumCodesFromCache()
        {
            if (project.IsSmartProject) return LoadStratumCodesFromCache2();
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\OilObj.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int i, len, dt;
                len = br.ReadInt32();

                for (i = 0; i < len; i++) GeoStratumCodes.Add(br.ReadUInt16());
                GeoStratumCodes.TrimExcess();

                len = br.ReadInt32();
                for (i = 0; i < len; i++) MerStratumCodes.Add(br.ReadInt32());
                MerStratumCodes.TrimExcess();
                if (br.BaseStream.Position < br.BaseStream.Length)
                {
                    dt = br.ReadInt32();
                    minMerDate = (dt != 0) ? RdfSystem.UnpackDateTimeI(dt) : DateTime.MinValue;
                    dt = br.ReadInt32();
                    maxMerDate = (dt != 0) ? RdfSystem.UnpackDateTimeI(dt) : DateTime.MaxValue;
                }
                if (br.BaseStream.Position < br.BaseStream.Length)
                {
                    BPParams[0] = br.ReadDouble();
                    BPParams[1] = br.ReadDouble();
                    BPParams[2] = br.ReadDouble();
                    BPParams[3] = br.ReadDouble();
                    BPParams[4] = br.ReadDouble();
                }
                br.Close();
                retValue = true;
            }
            return retValue;
        }
        public bool WriteStratumCodesToCache()
        {
            if (project.IsSmartProject) return WriteStratumCodesToCache2();
            string cacheName = project.path + "\\cache\\" + this.Name + "\\OilObj.bin";

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            int i;
            bw.Write(1);
            bw.Write(GeoStratumCodes.Count);
            for (i = 0; i < GeoStratumCodes.Count; i++) bw.Write(GeoStratumCodes[i]);
            bw.Write(MerStratumCodes.Count);
            for (i = 0; i < MerStratumCodes.Count; i++) bw.Write(MerStratumCodes[i]);

            bw.Write((minMerDate != DateTime.MinValue) ? RdfSystem.PackDateTime(minMerDate) : 0);
            bw.Write((maxMerDate != DateTime.MaxValue) ? RdfSystem.PackDateTime(maxMerDate) : 0);

            bw.Write(BPParams[0]);
            bw.Write(BPParams[1]);
            bw.Write(BPParams[2]);
            bw.Write(BPParams[3]);
            bw.Write(BPParams[4]);
            bw.Close();
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);
            return true;
        }
        #endregion

        #region WELL LIST
        public void ReSetWellIcons(BackgroundWorker worker, DoWorkEventArgs e, bool IsClearMerData)
        {
            if (!MerLoaded) LoadMerFromCache(worker, e);
            if (MerLoaded)
            {
                Well w;
                for (int i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    if (w.MerLoaded)
                    {
                        w.icons = w.GetIcons(CoefDict, maxMerDate);
                        w.SetActiveAllIcon();
                    }
                }
            }
            if ((IsClearMerData) && (MerLoaded))
            {
                ClearMerData(false);
            }
        }
        public void SetDefaultColorWellIcons(bool SetDefault)
        {
            Well w;
            for (int i = 0; i < Wells.Count; i++)
            {
                w = (Well)Wells[i];
                if (w.icons != null)
                {
                    w.icons.IsDefaultIconsColor = SetDefault;
                    w.SetBrushFromIcon();
                }
            }
        }
        public bool LoadWellListFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            int len = 10000, ver;
            string tempStr;
            string[] parseStr;
            char[] delimeter = new char[] { ';' };
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка координат скважин из кэша";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\skv.csv";
            try
            {
                if (File.Exists(cacheName))
                {
                    StreamReader sr = new StreamReader(cacheName, System.Text.Encoding.GetEncoding(1251));
                    var dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);

                    worker.ReportProgress(0, userState);
                    tempStr = sr.ReadLine();

                    if (tempStr != null)
                    {
                        parseStr = tempStr.Split(delimeter);
                        len = Convert.ToInt32(parseStr[0]);
                    }
                    if (len > 0)
                    {
                        tempStr = sr.ReadLine(); // считываем шапку
                        parseStr = tempStr.Split(delimeter);
                        ver = 0;
                        if (parseStr.Length > 9)
                        {
                            try
                            {
                                ver = Convert.ToInt32(parseStr[0]);
                            }
                            catch
                            {
                                ver = 0;
                            }
                        }
                        tempStr = sr.ReadLine();
                        int i = 0;
                        if (ver < 5)
                        {
                            while (tempStr != null)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    retValue = false;
                                    break;
                                }
                                else
                                {
                                    parseStr = tempStr.Split(delimeter);
                                    if (parseStr.Length > 5)
                                    {
                                        Well w = new Well(i);
                                        w.Name = parseStr[0];
                                        w.LoadCoord(parseStr, this.CoefDict, dict, ver);
                                        w.OilFieldIndex = this.Index;
                                        w.OilFieldCode = this.OilFieldCode;
                                        SetMinMaxRect(w);
                                        Wells.Add(w);
                                        if (!CoordLoaded) CoordLoaded = true;
                                        retValue = true;
                                    }
                                    tempStr = sr.ReadLine();
                                    i++;
                                }
                                // % Progress
                                ReportProgress(worker, i, len);
                            }
                        }
                    }
                    sr.Close();
                }
                else
                {
                    userState.Error = "Не найден файл кэша координат";
                    worker.ReportProgress(100, userState);
                    return false;
                }
            }
            catch
            {
                CoordLoaded = false;
                retValue = false;
            }
            worker.ReportProgress(100, userState);
            if (MinMaxXY.Left == Constant.DOUBLE_NAN) MinMaxXY.Left = this.CoefDict.ShX;
            if (MinMaxXY.Right == Constant.DOUBLE_NAN) MinMaxXY.Right = 0;
            if (MinMaxXY.Top == Constant.DOUBLE_NAN) MinMaxXY.Top = this.CoefDict.ShY;
            if (MinMaxXY.Bottom == Constant.DOUBLE_NAN) MinMaxXY.Bottom = 0;
            return retValue;
        }
        public bool WriteWellListToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\skv.csv";
            StreamWriter file = new StreamWriter(cacheName, false, System.Text.Encoding.GetEncoding(1251));

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись координат скважин в кэш";
            userState.Element = this.Name;
            using (file)
            {
                string[] list = new string[2 + this.Wells.Count];
                int iWellLen, i;

                iWellLen = this.Wells.Count;
                if (iWellLen > 0)
                {
                    file.WriteLine(iWellLen.ToString() + ";");
                    file.WriteLine("4;Индекс скважины;Индекс площади;Альтитуда,м;Устье X,м;Устье Y,м;Пласты;Пласт;Забой X,м;Забой Y,м;");

                    worker.ReportProgress(0, userState);
                    for (i = 0; i < iWellLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            Wells[i].WriteToCache(file);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iWellLen);
                    }
                    if (Wells.Count == 0) retValue = false;
                }
            }
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);
            if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            return retValue;
        }
        #endregion

        #region PROJECT WELL LIST
        public bool LoadProjWellListFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            int len = 10000, ver, col, count, count2;
            int PlastId;
            string tempStr;
            string[] parseStr;
            char[] delimeter = new char[] { ';' };
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка координат проектных скважин из кэша";
            userState.Element = this.Name;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\proj_skv.csv";
            if (File.Exists(cacheName))
            {
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                StreamReader sr = new StreamReader(cacheName, System.Text.Encoding.GetEncoding(1251));
                var dict = project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                worker.ReportProgress(0, userState);
                tempStr = sr.ReadLine();
                if (tempStr != null)
                {
                    parseStr = tempStr.Split(delimeter, StringSplitOptions.RemoveEmptyEntries);
                    ver = Convert.ToInt32(parseStr[0]);
                    len = Convert.ToInt32(parseStr[1]);
                }
                if (len > 0)
                {
                    sr.ReadLine(); // шапка
                    tempStr = sr.ReadLine();
                    int i = 0, j, k, ind;
                    WellIconCode iconCode;
                    while (tempStr != null)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            retValue = false;
                            break;
                        }
                        parseStr = tempStr.Split(delimeter, StringSplitOptions.RemoveEmptyEntries);
                        if (parseStr.Length > 3)
                        {
                            Well w = new Well(i);
                            w.Name = parseStr[1];
                            w.TypeID = Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL;
                            w.ProjectDest = Convert.ToByte(parseStr[2]);
                            count = Convert.ToInt32(parseStr[3]);
                            SkvCoordItem[] items = new SkvCoordItem[count];
                            col = 4;
                            double gX, gY;
                            for (j = 0; j < count; j++)
                            {
                                items[j].PlastId = Convert.ToUInt16(parseStr[col++]);
                                CoefDict.GlobalCoordFromLocal(Convert.ToDouble(parseStr[col++], nfi), Convert.ToDouble(parseStr[col++], nfi), out gX, out gY);
                                items[j].X = (int)gX;
                                items[j].Y = (int)gY;
                            }
                            w.coord.SetItems(items);
                            w.ReSetIcons(this.CoefDict);
                            //w.SetIconsFonts();
                            w.OilFieldIndex = this.Index;
                            ProjWells.Add(w);
                            ProjWellLoaded = true;
                            retValue = true;
                        }
                        tempStr = sr.ReadLine();
                        i++;
                        ReportProgress(worker, i, len);
                    }
                }
                sr.Close();
            }
            else
            {
                userState.Error = "Не найден файл кэша координат проектных скважин";
                ProjWellLoaded = false;
            }
            worker.ReportProgress(100, userState);
            return ProjWellLoaded;
        }
        public bool WriteProjWellListToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись координат проектных скважин в кэш";
            userState.Element = this.Name;

            string cacheName = project.path + "\\cache\\" + this.Name + "\\proj_skv.csv";
            StreamWriter file = new StreamWriter(cacheName, false, System.Text.Encoding.GetEncoding(1251));
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";

            using (file)
            {
                string[] list = new string[2 + this.ProjWells.Count];
                int iWellLen, iLen, maxPlasts = 0, i, j;
                Well w;

                iWellLen = this.ProjWells.Count;
                string str;
                if (iWellLen > 0)
                {
                    file.WriteLine(string.Format("0;{0}", iWellLen));
                    file.WriteLine("Индекс скважины;Скважина;Назначение;Пласты;Пласт;Забой X,м;Забой Y,м;");

                    worker.ReportProgress(0, userState);
                    for (i = 0; i < iWellLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        w = ((Well)ProjWells[i]);
                        str = string.Format("{0};{1};{2};{3};", w.Index, w.Name, w.ProjectDest, (w.coord == null) ? 0 : w.coord.Count);
                        if (w.coord != null)
                        {
                            for (j = 0; j < w.coord.Count; j++)
                            {
                                str += string.Format(nfi, "{0};{1};{2};", w.coord.Items[j].PlastId, w.coord.Items[j].X, w.coord.Items[j].Y);
                            }
                        }
                        file.WriteLine(str);
                        retValue = true;
                        ReportProgress(worker, i, iWellLen);
                    }
                }
            }
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);
            return retValue;
        }
        #endregion

        #region WELL PAD
        public bool LoadWellPadListFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (project.IsSmartProject) return LoadWellPadsFromCache2(worker, e);
            bool retValue = false; // переписать
            string[] list;
            string[] parseStr;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка кустов из кэша";
            userState.Element = ParamsDict.OilFieldName;


            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\wellpads.csv";
            if (File.Exists(cacheName))
            {
                list = File.ReadAllLines(cacheName, System.Text.Encoding.GetEncoding(1251));
                int i = 0, j, num;
                WellPad wp;
                Well w;
                string wellName;
                int wellIndex;
                char[] delimeter = new char[] { ';' };
                while (i < list.Length)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        retValue = false;
                        break;
                    }
                    else
                    {
                        parseStr = list[i].Split(delimeter);
                        if (parseStr[0] == "N")
                        {
                            wp = new WellPad(parseStr[1]);
                            parseStr = list[++i].Split(delimeter);
                            if (parseStr[0] == "A")
                            {
                                num = Convert.ToInt32(parseStr[1]);
                                for (j = 0; j < num; j++)
                                {
                                    parseStr = list[++i].Split(delimeter);
                                    wellName = parseStr[0];
                                    wellIndex = Convert.ToInt32(parseStr[1]);
                                    if (wellIndex < Wells.Count)
                                    {
	                                    w = (Well)Wells[wellIndex];
	                                    if ((w != null) && (w.UpperCaseName == wellName) && (w.CoordLoaded))
                                        {
                                        	wp.wells.Add(w);
                                		}
                            		}
                                }
                            }
                            wp.CreateContour(this.CoefDict);
                            WellPads.Add(wp);
                            retValue = true;
                        }

                    }
                    ReportProgress(worker, i, list.Length, userState);
                    i++;
                }
            }
            worker.ReportProgress(100, userState);
            return retValue;

        }
        public bool WriteWellPadListToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;// переписать
            int i, j, iLen;
            WellPad wp;
            Well w;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись кустов в кэш";
            userState.Element = ParamsDict.OilFieldName;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\wellpads.csv";
            if (!Directory.Exists(project.path + "\\cache\\" + this.Name)) Directory.CreateDirectory(project.path + "\\cache\\" + this.Name);
            if (File.Exists(cacheName)) File.Delete(cacheName);

            StreamWriter file = new StreamWriter(cacheName, false, System.Text.Encoding.GetEncoding(1251));
            using (file)
            {
                iLen = this.WellPads.Count;
                if (iLen > 0)
                {
                    worker.ReportProgress(0, userState);
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            wp = ((WellPad)WellPads[i]);
                            file.WriteLine("N;" + wp.Name);
                            file.WriteLine("A;" + wp.wells.Count.ToString());
                            for (j = 0; j < wp.wells.Count; j++)
                            {
                                w = (Well)wp.wells[j];
                                file.WriteLine(w.Name + ";" + w.Index);
                            }
                            retValue = true;
                            ReportProgress(worker, i, iLen, userState);
                        }
                    }
                }
            }
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);
            if ((iLen == 0) || (!retValue)) File.Delete(cacheName);

            return retValue;
        }
        #endregion

        #region CONTOUR LIST
        public Contour LoadContour(string Name, int Type, int Closed, double[] points)
        {
            Contour cntr = new Contour();
            double x, y;
            cntr.Name = Name;
            cntr._ContourType = Type;
            cntr._Closed = (Closed == 0) ? true : false;
            PointD point;
            cntr._points = new PointsXY(points.Length / 2);
            for (int i = 0; i < (int)(points.Length / 2); i++)
            {
                x = points[i * 2];
                y = points[i * 2 + 1];
                CoefDict.GlobalCoordFromLocal(x, y, out point.X, out point.Y);
                //cntr._points[i] = point;
                cntr.Add(point.X, point.Y);
            }
            this.Contours.Add(cntr);
            return cntr;
        }
        public void CreateUnionContours(BackgroundWorker worker, DoWorkEventArgs e)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Создание суммарных контуров";
            userState.Element = this.Name;
            worker.ReportProgress(0, userState);
            if (this.Contours.Count > 0)
            {
                int i, j;
                ArrayList list = new ArrayList();
                ArrayList list2 = new ArrayList();
                ArrayList points1 = new ArrayList();
                ArrayList points2 = new ArrayList();
                List<Contour> cntrList;
                Contour cntr = new Contour(), UnionCntr, UnionSimply;
                list.Add(cntr);
                cntr._ContourType = -5;
                bool closed = false;
                bool clockWise = false;
                Polygon poly = new Polygon();
                Contour work;
                C2DLayer layer, layer2;
                Pen p = new Pen(Color.Red, 2);
                layer2 = ((C2DLayer)project.canvas.LayerWorkList[project.canvas.LayerWorkList.Count - 2]);
                layer = ((C2DLayer)project.canvas.LayerWorkList[project.canvas.LayerWorkList.Count - 1]);

                for (i = 0; i < Contours.Count; i++)
                {
                    if (((Contour)Contours[i])._ContourType == -5)
                    {
                        Contours.RemoveAt(i);
                        i--;
                    }
                }
                List<Contour> SimplyList = new List<Contour>();
                for (i = 0; i < Contours.Count; i++)
                {
                    closed = false;
                    UnionCntr = (Contour)Contours[i];
                    if ((UnionCntr._Closed) ||
                        ((UnionCntr._points.Count > 3) &&
                         (UnionCntr._points[0].X == UnionCntr._points[UnionCntr._points.Count - 1].X) &&
                         (UnionCntr._points[0].Y == UnionCntr._points[UnionCntr._points.Count - 1].Y)))
                    {
                        closed = true;
                    }
                    if ((closed) &&
                        (UnionCntr._points.Count > 2) &&
                        ((UnionCntr._ContourType == 2) ||
                         (UnionCntr._ContourType == 8) ||
                         (UnionCntr._ContourType == 1002) ||
                         (UnionCntr._ContourType == 1008)))
                    {
                        UnionSimply = UnionCntr.GetSimplyContour(100);
                        clockWise = UnionSimply.GetClockWiseByX(true);
                        if (!clockWise) UnionSimply.Invert();
                        SimplyList.Add(UnionSimply);
                    }
                    else
                    {
                        worker.ReportProgress(i, userState);
                    }
                }
                for (i = 0; i < SimplyList.Count - 1; i++)
                {
                    for (j = i + 1; j < SimplyList.Count; j++)
                    {
                        if (SimplyList[i]._points.Count < SimplyList[j]._points.Count)
                        {
                            cntr = SimplyList[i];
                            SimplyList[i] = SimplyList[j];
                            SimplyList[j] = cntr;
                        }
                    }
                }
                long ticks1 = DateTime.Now.Ticks, ticks2;
                for (i = 0; i < SimplyList.Count; i++)
                {
                    work = new Contour(SimplyList[i]);

                    layer.ObjectsList[0] = work;
                    work.LineColor = Color.Red;
                    layer.GetObjectsRect();
                    layer2.Clear();
                    cntrList = poly.GetContours();
                    for (int ii = 0; ii < cntrList.Count; ii++)
                    {
                        layer2.Add(cntrList[ii]);
                    }
                    layer2.GetObjectsRect();
                    clockWise = SimplyList[i].GetClockWiseByX(true);
                    if (!clockWise) SimplyList[i].Invert();

                    ticks2 = DateTime.Now.Ticks;
                    userState.Time = ((ticks2 - ticks1) / 10000).ToString();
                    ticks1 = ticks2;
                    userState.Element = "Add i=" + i.ToString() + "(" + SimplyList[i]._points.Count.ToString() + ") simCount=" + SimplyList.Count.ToString() + "(all=" + Contours.Count.ToString() + ")";

                    worker.ReportProgress(i, userState);
                    poly.AddContour(SimplyList[i]);

                    ticks2 = DateTime.Now.Ticks;
                    userState.Time = ((ticks2 - ticks1) / 10000).ToString();
                    ticks1 = ticks2;
                    userState.Element = "Union i=" + i.ToString() + "(" + SimplyList[i]._points.Count.ToString() + ") simCount=" + SimplyList.Count.ToString() + "(all=" + Contours.Count.ToString() + ")";
                    worker.ReportProgress(i, userState);

                    poly.UnionContours();
                }
                cntrList = poly.GetContours();
                for (i = 0; i < cntrList.Count; i++)
                {
                    cntr = cntrList[i];
                    cntr._ContourType = -5;
                    cntr.Name = "";
                    cntr._OilFieldCode = this.OilFieldCode;
                    cntr._CCW = !cntr.GetClockWiseByX(true);
                    cntr._Closed = true;
                    Contours.Add(cntr);
                }
            }
            worker.ReportProgress(100, userState);
        }
        public bool LoadContoursFromCacheStr(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка контуров из кэша";
            userState.Element = this.Name;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\contours.csv";
            if (File.Exists(cacheName))
            {
                StreamReader file = new StreamReader(cacheName, System.Text.Encoding.GetEncoding(1251));
                iLen = Convert.ToInt32(file.ReadLine());
                for (i = 0; i < iLen; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        Contour cntr = new Contour();
                        cntr.ReadFromCache(file);
                        this.Contours.Add(cntr);
                        retValue = true;
                    }
                    ReportProgress(worker, i, iLen);
                }
                file.Close();
                this.ContoursEdited = false;
            }
            else
            {
                userState.Error = "Не найден файл кэша контуров";
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadContoursFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool HeadOnly)
        {
            int i, iLen = 0;
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка контуров из кэша";
            userState.Element = this.Name;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\contours.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                iLen = br.ReadInt32();
                int ver = 0;
                if (iLen < 0)
                {
                    ver = -iLen;
                    iLen = br.ReadInt32();
                }
                for (i = 0; i < iLen; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        Contour cntr = new Contour();
                        if (HeadOnly) cntr.ReadHeadLineFromCacheBin(br, ver); else cntr.ReadFromCacheBin(br, ver);
                        if (ParamsDict.NGDUCode == -1)
                        {
                            SetMinMaxRect(cntr);
                        }
                        this.Contours.Add(cntr);
                        retValue = true;
                    }
                    ReportProgress(worker, i, iLen);
                }
                br.Close();
                this.ContoursEdited = false;
            }
            else
            {
                userState.Error = "Не найден файл кэша контуров";
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadContoursDataFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (project.IsSmartProject) return LoadContoursDataFromCache2(worker, e);
            int i, iLen = 0;
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка контуров из кэша";
            userState.Element = this.Name;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\contours.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                iLen = br.ReadInt32();
                int ver = 0;
                if (iLen < 0)
                {
                    ver = -iLen;
                    iLen = br.ReadInt32();
                }
                if (iLen == this.Contours.Count)
                {
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            retValue = false;
                            break;
                        }
                        else
                        {
                            Contour cntr = (Contour)Contours[i];
                            cntr.ReadFromCacheBin(br, ver);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen);
                    }
                }
                br.Close();
                this.ContoursEdited = false;
                ContoursDataLoaded = retValue;
            }
            else
            {
                userState.Error = "Не найден файл кэша контуров";
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteContoursToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            if (Contours != null)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = string.Empty;
                userState.WorkCurrentTitle = "Запись контуров в кэш..";
                userState.Element = Name;

                bool cntrsDataLoad = false;

                string cacheName = project.path + "\\cache\\" + Name + "\\contours.bin";
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                int i, iLen;
                
                using (bw)
                {
                    iLen = Contours.Count;
                    if (iLen > 0)
                    {
                        if (worker != null) worker.ReportProgress(0, userState);
                        bw.Write(Contour.Version);
                        bw.Write(iLen);
                        for (i = 0; i < iLen; i++)
                        {
                            if ((worker != null) && (worker.CancellationPending))
                            {
                                e.Cancel = true;
                            }
                            else
                            {
                                Contours[i].WriteToCacheBin(i, bw);
                                retValue = true;
                            }
                            if (worker != null) ReportProgress(worker, i, iLen);
                        }
                    }
                }

                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) || (iLen == 0)) File.Delete(cacheName);
            }
            return retValue;
        }
        public void FreeContoursData()
        {
            Contour cntr;
            for (int i = 0; i < Contours.Count; i++)
            {
                cntr = (Contour)Contours[i];
                if (cntr._ContourType >= 0) cntr.FreeDataMemory();
            }
            ContoursDataLoaded = false;
        }
        public void ExportCondContours(string destPath)
        {
            if (this.Contours.Count > 0)
            {
                StreamWriter file = new StreamWriter(destPath, true, Encoding.GetEncoding(1251));
                string head = ";0;0;1;1;255;-1;0;16777215;-1;0";
                Contour cntr;
                PointD point;
                for (int i = 0; i < Contours.Count; i++)
                {
                    cntr = (Contour)Contours[i];
                    if ((cntr != null) && (cntr._ContourType == -3))
                    {
                        file.WriteLine("*" + this.Name + ".ktr" + head);
                        for (int j = 0; j < cntr._points.Count; j++)
                        {
                            point = cntr._points[j];
                            file.WriteLine(String.Format("{0} {1}", point.X, point.Y));
                        }
                        if (cntr._points.Count > 0)
                        {
                            point = cntr._points[0];
                            file.WriteLine(String.Format("{0} {1}", point.X, point.Y));
                        }
                        break;
                    }
                }
                file.Close();
            }
        }
        #endregion

        #region AREA LIST
        internal struct WellMerPartsItem
        {
            public int WellIndex;
            public List<WellMerDatePartsItem> DateItems;
        }
        internal struct WellMerDatePartsItem
        {
            public DateTime Date;
            public List<WellMerStratumPartItem> PlastItems;
        }
        internal class WellMerStratumPartItem
        {
            public int StratumCode;
            public double Oil;
            public double Wat;
            public double Inj;
        }
        List<WellMerPartsItem> GetMerWorkPartParts()
        {
            List<WellMerPartsItem> items = new List<WellMerPartsItem>();
            if (this.MerLoaded)
            {
                int plIndex;
                double sumOil, sumWat, sumInj;
                Well w;
                WellMerPartsItem wellItem;
                WellMerDatePartsItem dateItem;
                WellMerStratumPartItem plastItem;
                MerItem item;
                MerItemEx itemEx;
                for (int i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    wellItem = new WellMerPartsItem();
                    wellItem.DateItems = new List<WellMerDatePartsItem>();
                    if (w.MerLoaded)
                    {
                        sumOil = 0; sumWat = 0; sumInj = 0;
                        dateItem = new WellMerDatePartsItem();
                        dateItem.PlastItems = new List<WellMerStratumPartItem>();
                        for (int j = 0; j < w.mer.Count; j++)
                        {
                            item = w.mer.Items[j];
                            itemEx = w.mer.GetItemEx(j);
                            if ((dateItem.Date != DateTime.MinValue) && dateItem.Date != item.Date)
                            {
                                for (int k = 0; k < dateItem.PlastItems.Count; k++)
                                {
                                    if (sumOil > 0) dateItem.PlastItems[k].Oil = dateItem.PlastItems[k].Oil / sumOil;
                                    if (sumWat > 0) dateItem.PlastItems[k].Wat = dateItem.PlastItems[k].Wat / sumWat;
                                    if (sumInj > 0) dateItem.PlastItems[k].Inj = dateItem.PlastItems[k].Inj / sumInj;
                                }
                                sumOil = 0; sumWat = 0; sumInj = 0;
                                wellItem.DateItems.Add(dateItem);
                                dateItem = new WellMerDatePartsItem();
                                dateItem.PlastItems = new List<WellMerStratumPartItem>();
                            }
                            plIndex = dateItem.PlastItems.FindIndex(delegate(WellMerStratumPartItem PlastItem) { return PlastItem.StratumCode == item.PlastId; });
                            if (plIndex == -1)
                            {
                                plastItem = new WellMerStratumPartItem();
                                plIndex = dateItem.PlastItems.Count;
                                dateItem.PlastItems.Add(plastItem);
                            }
                            if (item.CharWorkId == 11)
                            {
                                dateItem.PlastItems[plIndex].Oil += item.Oil;
                                dateItem.PlastItems[plIndex].Wat += item.Wat;
                                sumOil += item.Oil;
                                sumWat += item.Wat;
                            }
                            if ((itemEx.AgentInjId <= 80) && (itemEx.AgentInjId >= 90) && (itemEx.Injection > 0))
                            {
                                dateItem.PlastItems[plIndex].Inj += itemEx.Injection;
                                sumInj += itemEx.Injection;
                            }
                        }
                    }
                    items.Add(wellItem);
                }
            }
            return items;
        }
        public void RemoveAreasWithoutPlastCode()
        {
            int i = 0;
            while (i < Areas.Count)
            {
                if (((Area)Areas[i]).PlastCode == -1)
                {
                    Areas.RemoveAt(i);
                    i--;
                }
                i++;
            }
            for (i = 0; i < Areas.Count; i++)
            {
                ((Area)Areas[i]).Index = i;
            }
        }
        public List<Area> GetAreaList(int StratumCode, bool UseStratumTree)
        {
            var dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            StratumTreeNode node1, node2;
            node1 = dict.GetStratumTreeNode(StratumCode);
            List<Area> areas = new List<Area>();
            for (int i = 0; i < Areas.Count; i++)
            {
                if (Areas[i].PlastCode == StratumCode)
                {
                    areas.Add(Areas[i]);
                }
                else if (UseStratumTree)
                {
                    node2 = dict.GetStratumTreeNode(Areas[i].PlastCode);
                    if ((node1.GetParentLevelByCode(node2.StratumCode) > -1) || (node2.GetParentLevelByCode(node1.StratumCode) > -1))
                    {
                        areas.Add(Areas[i]);
                    }
                }
            }
            return areas;
        }
        public void ResetAreasIndexes(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (this.Areas.Count > 0)
            {
                List<Area> newAreasList = new List<Area>(this.Areas.Count);
                int pos, pos2, ind;
                string name;
                for (int i = 0; i < this.Areas.Count; i++)
                {
                    for (int j = 0; j < this.Areas.Count; j++)
                    {
                        name = ((Area)this.Areas[j]).contour.Name;
                        pos = name.IndexOf('(');
                        if (pos > 0)
                        {
                            pos2 = name.IndexOf(')', pos);
                            if (pos2 == -1) pos2 = name.IndexOf(")", pos);
                            ind = Convert.ToInt32(name.Substring(pos + 1, pos2 - pos - 1));
                            if (ind - 1 == i)
                            {
                                newAreasList.Add((Area)this.Areas[j]);
                                break;
                            }
                        }
                    }
                }
                this.Areas = newAreasList;
            }
        }
        public bool FillAreaWellList(BackgroundWorker worker, DoWorkEventArgs e, bool ClearMerData)
        {
            bool retValue = false;
            Area area;
            Well w;
            int i, j, n, t, indArea;
            double k;
            bool CallMerLoading = false;
            bool find = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Заполнение списка скважин ячеек заводнения";
            userState.Element = this.Name;

            var dict = (StratumDictionary)this.project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            MerItem merItem;
            MerItemEx merItemEx;

            if (worker != null) worker.ReportProgress(0, userState);
            ArrayList list = new ArrayList();
            ArrayList wellList = new ArrayList();
            List<Well> allWellList = new List<Well>();
            ArrayList wellCoefList = new ArrayList();
            ArrayList oilFieldAreaCodeList = new ArrayList();
            ArrayList oilFieldAreaCodeCountList = new ArrayList();
            List<int> oilObjList = new List<int>();
            List<int> workObjList = new List<int>();
            this.LoadWellsAreaCount(worker, e);

            if (!this.MerLoaded)
            {
                this.LoadMerFromCache(worker, e);
                CallMerLoading = true;
            }
            StratumTreeNode itemNode;
            for (j = 0; j < this.Areas.Count; j++)
            {
                area = (Area)this.Areas[j];
                wellList.Clear();
                oilFieldAreaCodeList.Clear();
                oilFieldAreaCodeCountList.Clear();
                wellCoefList.Clear();
                oilObjList.Clear();
                allWellList.Clear();

                for (n = 0; n < this.Wells.Count; n++)
                {
                    w = (Well)this.Wells[n];

                    if (w.CountAreas == 0) continue; else k = 1 / (double)w.CountAreas;

                    if ((w.CoordLoaded) &&
                        (((area.contour.PointBoundsEntry(w.X, w.Y)) &&
                          (area.contour.PointBodyEntry(w.X, w.Y))) ||
                          (area.contour.GetIndexPointByXY(w.X, w.Y) > -1)))
                    {
                        find = false;
                        allWellList.Add(w);
                        if (w.MerLoaded)
                        {
                            workObjList.Clear();
                            for (t = 0; t < w.mer.Count; t++)
                            {
                                merItem = w.mer.Items[t];
                                merItemEx = w.mer.GetItemEx(t);
                                if ((merItem.CharWorkId == 11) ||
                                    (merItem.CharWorkId == 12) ||
                                    (merItem.CharWorkId == 15) ||
                                    (merItem.CharWorkId == 20))
                                {
                                    if ((merItem.WorkTime + merItem.CollTime > 0) &&
                                        (merItem.Oil + merItem.Wat + merItem.Gas + merItemEx.Injection + merItemEx.CapGas + merItemEx.GasCondensate + merItemEx.NaturalGas > 0) &&
                                        (workObjList.IndexOf(merItem.PlastId) == -1))
                                    {
                                        workObjList.Add(merItem.PlastId);
                                    }
                                }
                            }
                            for (t = 0; t < workObjList.Count; t++)
                            {
                                if (area.PlastCode == workObjList[t])
                                {
                                    find = true;
                                    if (oilObjList.IndexOf(workObjList[t]) == -1)
                                    {
                                        oilObjList.Add(workObjList[t]);
                                    }
                                }
                                else
                                {
                                    itemNode = dict.GetStratumTreeNode(workObjList[t]);
                                    if (itemNode.GetParentLevelByCode(area.PlastCode) != -1)
                                    {
                                        find = true;
                                        if (oilObjList.IndexOf(workObjList[t]) == -1)
                                        {
                                            oilObjList.Add(workObjList[t]);
                                        }
                                    }
                                }
                            }
                        }
                        if (!find) continue;
                        wellList.Add(w);
                        wellCoefList.Add(k);
                        indArea = oilFieldAreaCodeList.IndexOf(w.OilFieldAreaCode);
                        if (w.OilFieldAreaCode > -1)
                        {
                            if (indArea == -1)
                            {
                                oilFieldAreaCodeList.Add(w.OilFieldAreaCode);
                                oilFieldAreaCodeCountList.Add(1);
                            }
                            else
                            {
                                oilFieldAreaCodeCountList[indArea] = (int)oilFieldAreaCodeCountList[indArea] + 1;
                            }
                        }
                    }
                }
                if (allWellList.Count > 0) area.AllWellList = allWellList.ToArray();
                indArea = 0;
                if (UnionParamsDict == null)
                {
                    for (n = 0; n < oilFieldAreaCodeList.Count; n++)
                    {
                        if (indArea < (int)oilFieldAreaCodeCountList[n])
                        {
                            area.OilFieldAreaCode = (int)oilFieldAreaCodeList[n];
                            indArea = (int)oilFieldAreaCodeCountList[n];
                        }
                    }
                }
                if (wellList.Count > 0)
                {
                    if (this.UnionParamsDict != null)
                    {
                        i = 0;
                        while (i < wellList.Count)
                        {
                            if (((Well)wellList[i]).OilFieldAreaCode != area.OilFieldAreaCode)
                            {
                                wellList.RemoveAt(i);
                                i--;
                            }
                            i++;
                        }
                    }
                    area.WellList = new Well[wellList.Count];
                    area.WellCoefList = new double[wellList.Count];
                    for (n = 0; n < wellList.Count; n++)
                    {
                        area.WellList[n] = (Well)wellList[n];
                        area.WellCoefList[n] = (double)wellCoefList[n];
                    }
                }
                area.StratumList = null;
                if (oilObjList.Count > 0)
                {
                    area.StratumList = oilObjList.ToArray();
                }
                retValue = true;
            }
            if ((ClearMerData) || (CallMerLoading)) this.ClearMerData(false);
            if (worker != null)
            {
                if (retValue) this.WriteAreasToCache(worker, e);
                worker.ReportProgress(100);
            }
            return retValue;
        }
        public bool LoadAreasFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (project.IsSmartProject) return LoadAreasFromCache2(worker, e);
            int i, iLen = 0;
            bool retValue = false, res;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка ячеек из кэша";
            userState.Element = Name;
            FileStream fs = null;
            string cacheName = project.path + "\\cache\\" + Name + "\\areas.bin";
            try
            {
            if (File.Exists(cacheName))
            {
                    fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int version = br.ReadInt32();
                if (Area.Version >= version)
                {
                    iLen = br.ReadInt32();
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                                break;
                        }
                        else
                        {
                            Area area = new Area();
                            res = area.ReadFromCache(worker, e, this, br, version);
                            area.OilFieldIndex = Index;
                            //area.contour.fillBrush = 33;
                            if (res)
                            {
                                area.Index = Areas.Count;
                                Areas.Add(area);
                            }
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen, userState);
                    }
                }
                br.Close();
                    fs = null;
                }
            }
            catch
            {
                retValue = false;
            }
            finally
            {
                if (fs != null) fs.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteAreasToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\areas.bin";
            if (!Directory.Exists(project.path + "\\cache\\" + this.Name))
            {
                Directory.CreateDirectory(project.path + "\\cache\\" + this.Name);
            }

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter file = new BinaryWriter(fs);
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись ячеек в кэш";
            userState.Element = this.Name;

            int i, iLen;
            using (file)
            {
                iLen = this.Areas.Count;
                file.Write(Area.Version);
                file.Write(iLen);
                if (iLen > 0)
                {
                    if(worker != null) worker.ReportProgress(0, userState);
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker != null && worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        Areas[i].WriteToCache(worker, e, file);
                        retValue = true;
                        if (worker != null) ReportProgress(worker, i, iLen);
                    }
                }
                file.Close();
            }
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);
            return retValue;
        }
        public bool LoadWellsAreaCount(BackgroundWorker worker, DoWorkEventArgs e)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Расчет коэфициентов скважин по ЯЗ";
            userState.Element = this.Name;

            Well w;
            Area area;

            for (int i = 0; i < this.Wells.Count; i++)
            {
                w = (Well)this.Wells[i];
                w.CountAreas = 0;
                for (int j = 0; j < this.Areas.Count; j++)
                {
                    area = (Area)this.Areas[j];
                    if (area.contour.GetIndexPointByXY2(w.X, w.Y) > -1) w.CountAreas++;
                    if (w.CountAreas == 0)
                    {
                        if (area.contour.PointBoundsEntry(w.X, w.Y) && (area.contour.PointBodyEntry(w.X, w.Y)))
                        {
                            w.CountAreas++;
                            break;
                        }
                    }
                }
                if (worker != null) worker.ReportProgress((int)(i * 100f / this.Areas.Count), userState);
            }
            return true;
        }
        public void ExportAreasContours(string destPath)
        {
            if ((Directory.Exists(destPath)) && (this.Areas.Count > 0))
            {
                if (File.Exists(destPath + "\\" + this.Name + ".kts")) File.Delete(destPath + "\\" + this.Name + ".kts");
                StreamWriter file = new StreamWriter(destPath + "\\" + this.Name + ".kts", false, Encoding.GetEncoding(1251));
                string head = ";0;0;1;1;255;-1;0;16777215;-1;0";
                Area area;
                double xLoc, yLoc;
                PointD point;
                for (int i = 0; i < Areas.Count; i++)
                {
                    area = (Area)Areas[i];
                    if (area.contour != null)
                    {
                        file.WriteLine("*" + this.Name + "_" + (i + 1).ToString() + ".ktr" + head);
                        for (int j = 0; j < area.contour._points.Count; j++)
                        {
                            point = area.contour._points[j];
                            CoefDict.LocalCoordFromGlobal(point.X, point.Y, out xLoc, out yLoc);
                            file.WriteLine(String.Format("{0} {1}", xLoc, yLoc));
                        }
                        if (area.contour._points.Count > 0)
                        {
                            point = area.contour._points[0];
                            CoefDict.LocalCoordFromGlobal(point.X, point.Y, out xLoc, out yLoc);
                            file.WriteLine(String.Format("{0} {1}", xLoc, yLoc));
                        }
                    }
                }
                file.Close();
            }
        }
        #endregion

        #region DEPOSIT LIST
        public bool LoadDepositsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false, res;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка залежей из кэша";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\deposit.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader file = new BinaryReader(fs);
                try
                {
                    int ver = file.ReadInt32();
                    if (Deposit.Version >= ver)
                    {
                        iLen = file.ReadInt32();
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                break;
                            }
                            Deposit dep = new Deposit();
                            res = dep.ReadFromCache(worker, e, this, file, ver);
                            if (res)
                            {
                                dep.OilfieldIndex = this.Index;
                                dep.Index = this.Deposits.Count;
                                Deposits.Add(dep);
                                retValue = true;
                            }
                            ReportProgress(worker, i, iLen, userState);
                        }
                    }
                }
                catch
                {
                    this.Deposits.Clear();
                    retValue = false;
                }
                finally
                {
                    file.Close();
                }
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteDepositsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\deposit.bin";
            if (!Directory.Exists(project.path + "\\cache\\" + this.Name))
            {
                Directory.CreateDirectory(project.path + "\\cache\\" + this.Name);
            }

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter file = new BinaryWriter(fs);
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись залежей в кэш";
            userState.Element = this.Name;

            int i, iLen;
            using (file)
            {
                iLen = this.Deposits.Count;
                file.Write(Deposit.Version);
                file.Write(iLen);
                if (iLen > 0)
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker != null && worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        Deposits[i].WriteToCache(worker, e, file);
                        retValue = true;
                        if (worker != null) ReportProgress(worker, i, iLen);
                    }
                }
                file.Close();
            }
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);
            return retValue;
        }
        #endregion

        #region GRID LIST
        public void CalcAreasReserves()
        {
            Area area;
            Contour cntr;
            Grid grid;
            int i, index = -1;
            double square = 0;
            double sumvals = 0;
            double k = 0;
            int res;
            bool loaded;
            List<Grid> grids = new List<Grid>();
            var dictStratum = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            for (int AreaIndex = 0; AreaIndex < Areas.Count; AreaIndex++)
            {
                grids.Clear();
                area = (Area)this.Areas[AreaIndex];
                cntr = area.contour;
                index = -1;
                for (i = 0; i < this.Grids.Count; i++)
                {
                    grid = (Grid)this.Grids[i];
                    if (grid.GridType == 2 && grid.StratumCode == area.PlastCode)
                    {
                        grids.Add(grid);
                    }
                }
                if (grids.Count == 0)
                {
                    StratumTreeNode node;
                    for (i = 0; i < this.Grids.Count; i++)
                    {
                        grid = (Grid)this.Grids[i];
                        if (grid.GridType == 2)
                        {
                            node = dictStratum.GetStratumTreeNode(grid.StratumCode);
                            if (node != null && node.GetParentLevelByCode(area.PlastCode) != -1)
                            {
                                grids.Add(grid);
                            }
                        }
                    }
                }
                if (grids.Count > 0)
                {
                    for (i = 0; i < grids.Count; i++)
                    {
                        square = 0;
                        sumvals = 0;
                        res = 0;
                        loaded = false;
                        if (!grids[i].DataLoaded)
                        {
                            res = this.LoadGridDataFromCacheByIndex(null, null, grids[i].Index, false);
                            loaded = true;
                        }
                        if (res == 0)
                        {
                            grids[i].CalcSumValues(null, null, cntr, out square, out sumvals);
                            if (square != 0)
                            {
                                if (!area.pvt.IsEmpty)
                                {
                                    k = area.pvt.OilDensity * area.pvt.Porosity * area.pvt.OilInitialSaturation / area.pvt.OilVolumeFactor;
                                    area.Params.InitOilReserv = k * sumvals;
                                }
                                break;
                            }
                            if (loaded) grids[i].FreeDataMemory();
                        }
                    }
                }
            }
        }
        public int LoadGridDataFromCacheByIndex(BackgroundWorker worker, DoWorkEventArgs e, int GridIndex, bool CreateBrushesBitmap)
        {
            if (project.IsSmartProject) return LoadGridDataFromCache2(worker, e, GridIndex);
            int i, iLen = 0, ver;
            long seek;
            int retValue = -1;
            Grid grid = null;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка сетки в память";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\grids.bin";
            System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess();
            if (proc.WorkingSet64 > 1300 * 1024 * 1024)
            {
                retValue = -2;
            }
            else if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                ver = br.ReadInt32();
                if (ver == -1)
                {
                    iLen = br.ReadInt32();
                    if ((iLen > 0) && (GridIndex < iLen) && (this.Grids.Count > 0))
                    {
                        grid = (Grid)this.Grids[GridIndex];
                        grid.DataLoading = true;
                        userState.Element = grid.Name;

                        fs.Seek(8 + 8 * GridIndex, SeekOrigin.Begin);
                        seek = br.ReadInt64();
                        fs.Seek(seek, SeekOrigin.Begin);
                        try
                        {
                            if (grid.LoadFromCache(worker, e, fs, CreateBrushesBitmap))
                            {
                                grid.OilFieldCode = this.OilFieldCode;
                                retValue = 0;
                            }
                        }
                        catch (OutOfMemoryException ex)
                        {
                            retValue = -2;
                            grid.FreeDataMemory();
                        }
                    }
                }
                br.Close();
                if (grid != null) grid.DataLoading = false;
            }
            return retValue;
        }
        public bool LoadGridHeadListFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (project.IsSmartProject) return LoadGridsHeadFromCache2(worker, e);
            int i, iLen = 0;
            bool retValue = false;
            Grid grid;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка сеток из кэша";
            userState.Element = this.Name;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\grids.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int ver = br.ReadInt32();
                if (ver == -1)
                {
                    iLen = br.ReadInt32();
                    fs.Seek(8 + 8 * iLen, SeekOrigin.Begin);
                    for (i = 0; i < iLen; i++)
                    {
                        grid = new Grid();
                        grid.Index = i;
                        grid.OilFieldCode = this.OilFieldCode;
                        grid.Visible = false;
                        grid.Date = DateTime.FromOADate(br.ReadDouble());
                        grid.StratumCode = br.ReadInt32();
                        grid.StratumName = br.ReadString();
                        grid.GridType = br.ReadInt32();
                        grid.Name = br.ReadString();
                        this.Grids.Add(grid);
                    }
                }
                br.Close();
                retValue = true;
            }
            else
            {
                userState.Error = "Не найден файл кэша сеток";
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadGridDataFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (project.IsSmartProject) return LoadGridDataFromCache2(worker, e);
            int i, iLen = 0;
            bool retValue = false;
            Grid grid;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка сеток из кэша";
            userState.Element = this.Name;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\grids.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                long seekStart;
                int ver = br.ReadInt32();
                if (ver == -1)
                {
                    iLen = br.ReadInt32();
                    seekStart = br.ReadInt64();
                    fs.Seek(seekStart, SeekOrigin.Begin);

                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            grid = new Grid();
                            grid.Index = i;
                            grid.OilFieldCode = this.OilFieldCode;
                            grid.LoadFromCache(worker, e, fs, true);
                            this.Grids.Add(grid);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen);
                    }
                    fs.Seek(4 + 8 * iLen, SeekOrigin.Begin);
                    for (i = 0; i < iLen; i++)
                    {
                        grid = ((Grid)Grids[i]);
                        grid.Date = DateTime.FromOADate(br.ReadDouble());
                        grid.StratumCode = br.ReadInt32();
                        grid.StratumName = br.ReadString();
                        grid.GridType = br.ReadInt32();
                        grid.Name = br.ReadString();
                    }
                }
                br.Close();
            }
            else
            {
                userState.Error = "Не найден файл кэша сеток";
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteGridListToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\grids.bin";

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись сеток в кэш";
            userState.Element = this.Name;

            int i, iLen;
            long[] seek;
            using (bw)
            {
                iLen = this.Grids.Count;
                if (iLen > 0)
                {
                    worker.ReportProgress(0, userState);
                    bw.Write(Grid.Version);
                    bw.Write(iLen);
                    seek = new long[iLen];
                    fs.Seek(iLen * 8, SeekOrigin.Current);
                    for (i = 0; i < iLen; i++)
                    {
                        bw.Write(Grids[i].Date.ToOADate());
                        bw.Write(Grids[i].StratumCode);
                        bw.Write(Grids[i].StratumName);
                        bw.Write(Grids[i].GridType);
                        bw.Write(Grids[i].Name);
                    }
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            seek[i] = fs.Position;
                            Grids[i].WriteToCache(fs);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen);
                    }
                    fs.Seek(8, SeekOrigin.Begin);
                    for (i = 0; i < iLen; i++)
                    {
                        bw.Write(seek[i]);
                    }
                }
            }
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);

            if ((!retValue) || (iLen == 0)) File.Delete(cacheName);
            return retValue;
        }
        #endregion

        #region MER
        // с поддрежкой новых МЭР
        public bool LoadMerFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadMerFromCache(worker, e, false);
        }
        public bool LoadMerFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            return LoadMerFromCache(worker, e, false, false);
        }
        public bool LoadMerFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotReportProgress)
        {
            if (project.IsSmartProject) return LoadMerFromCache2(worker, e, NotLoadData, NotReportProgress);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных МЭР месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\mer.bin";
            if (File.Exists(cacheName))
            {
                if ((worker != null) && (worker.CancellationPending))
                {
                    e.Cancel = true;
                }
                else
                {
                    int i = 0, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    if (worker != null && !NotReportProgress) worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                        BinaryReader bfr = new BinaryReader(fs);
                        if (Mer.Version != bfr.ReadInt32())
                        {
                            bfr.Close();
                            fs.Close();
                            return false;
                        }
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        this.minMerDate = DateTime.MaxValue;
                        this.maxMerDate = DateTime.MinValue;
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                if ((worker != null) && (worker.CancellationPending))
                                {
                                    e.Cancel = true;
                                    for (j = 0; j < this.Wells.Count; j++)
                                    {
                                        w = (Well)Wells[j];
                                        w.mer = null;
                                    }
                                    this.MerLoaded = false;
                                    return false;
                                }
                                well_index = br.ReadInt32();
                                if (well_index > -1 && well_index < Wells.Count)
                                {
                                    w = (Well)Wells[well_index];
                                    w.mer = new Mer();
                                    w.mer.Read(br, NotLoadData);
                                    if (w.mer.Count > 0)
                                    {
                                        if (w.mer.Items[0].Date < this.minMerDate) this.minMerDate = w.mer.Items[0].Date;
                                        if (w.mer.Items[w.mer.Count - 1].Date > this.maxMerDate) this.maxMerDate = w.mer.Items[w.mer.Count - 1].Date;
                                        if (!NotLoadData) this.MerLoaded = true;
                                    }
                                }
                                retValue = true;
                                if (worker != null && !NotReportProgress) ReportProgress(worker, wellCount + (i + 1), iLen, userState);
                            }
                            wellCount += well_count;
                        }
                        FreeBuffers();
                        if (worker != null && !NotReportProgress) worker.ReportProgress(100, userState);
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception ex)
                    {
                        FreeBuffers();
                        ClearMerData(true);
                        if (fs != null) fs.Close();
                        throw new Exception("Ошибка загрузки данных МЭР", ex);
                    }
                }
            }
            if (this.minMerDate == DateTime.MaxValue) this.minMerDate = DateTime.MinValue;
            if (this.maxMerDate == DateTime.MinValue) this.maxMerDate = DateTime.MaxValue;
            return retValue;
        }
        public bool LoadMerFromCache(int WellIndex)
        {
            return LoadMerFromCache(WellIndex, false);
        }
        public bool LoadMerFromCache(int WellIndex, bool NotLoadData)
        {
            if (project.IsSmartProject) return LoadMerFromCache2(WellIndex, NotLoadData);
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\mer.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (Mer.Version != bfr.ReadInt32())
                    {
                        bfr.Close();
                        fs.Close();
                        return false;
                    }
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();
                        if (WellIndex < wellCount + well_count)
                        {
                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            shift = (WellIndex - wellCount) * 4;
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            shift = br.ReadInt32();
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            well_index = br.ReadInt32();
                            if (WellIndex != well_index)
                            {
                                bfr.Close();
                                return false;
                            }
                            else
                            {
                                w = (Well)Wells[well_index];
                                w.mer = new Mer();
                                w.mer.Read(br, NotLoadData);
                            }
                            break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                    FreeBuffers();
                    bfr.Close();
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        public bool LoadMerFromCache(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            if (project.IsSmartProject) return LoadMerFromCache2(worker, e, WellIndexes);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных МЭР месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\mer.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int lastIndex = 0;
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (Mer.Version != bfr.ReadInt32())
                    {
                        bfr.Close();
                        fs.Close();
                        return false;
                    }
                    WellIndexes.Sort();
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    lastIndex = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();

                        if (WellIndexes[lastIndex] < wellCount + well_count)
                        {
                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            while ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                            {
                                shift = (WellIndexes[lastIndex] - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();
                                if (WellIndexes[lastIndex] != well_index)
                                {
                                    bfr.Close();
                                    return false;
                                }
                                else
                                {
                                    w = (Well)Wells[well_index];
                                    w.mer = new Mer();
                                    w.mer.Read(br, false);
                                    if (w.mer.Count > 0)
                                    {
                                        MerLoaded = true;
                                    }
                                }
                                lastIndex++;
                                if (worker != null) ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                            }
                            if (lastIndex >= WellIndexes.Count) break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                    FreeBuffers();
                    bfr.Close();
                    retValue = true;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteMerToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных МЭР в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\mer.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 1024;
                Well w;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                BinaryReader br;
                MemoryStream ms;
                BinaryWriter bw;
                int i, j, k, h, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int mer_len, iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        len_all_wells += 8 + w.mer.GetBinLength();
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);

                    len_block = 0;
                    k = 0;
                    wCount = 0;

                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        len_shift = 8 + w.mer.GetBinLength();

                        if (len_block + len_shift <= len_all_wells)
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                len_shift += 4 + w.mer.GetBinLength();
                                bw.Write(len_shift);
                            }
                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);
                                w.mer.Write(bw);
                            }
                            unpackSizes.Add((int)ms.Length);
                            packedMS = ConvertEx.PackStream(ms);
                            msArr.Add(packedMS);
                            wellCount.Add(wCount);
                            WellsShift.Clear();

                            ms.SetLength(0);
                            k = i;

                            w = (Well)Wells[i];
                            len_shift = 8 + w.mer.GetBinLength();
                            len_block = len_shift;
                            wCount = 1;
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    len_shift = 4 * wCount;
                    bw.Write(len_shift);
                    for (j = k; j < i - 1; j++)
                    {
                        w = (Well)Wells[j];
                        len_shift += 4 + w.mer.GetBinLength();
                        bw.Write(len_shift);
                    }
                    for (j = k; j < i; j++)
                    {
                        w = (Well)Wells[j];
                        bw.Write(w.Index);
                        w.mer.Write(bw);
                    }
                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(Mer.Version);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearMerData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if (w.mer != null) w.mer.SetItems(null, null);
                    if (w.merEx != null) w.merEx.Clear();
                    w.mer = null;
                    w.merEx = null;
                }
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
            }
            this.MerLoaded = false;
        }

        // загрузка mer по блокам
        public BinaryReader GetMerVReader()
        {
            BinaryReader bfr = null;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\mer.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                bfr = new BinaryReader(fs);
            }
            return bfr;
        }
        public int LoadMerVBlocks(BackgroundWorker worker, DoWorkEventArgs e, BinaryReader bfr, int IndexBlock, int NumBlocks)
        {

            if (bfr == null) return -1;
            bfr.BaseStream.Seek(0, SeekOrigin.Begin);
            MemoryStream ms, unpackMS;
            BinaryReader br;
            if (Mer.Version != bfr.ReadInt32())
            {
                return -1;
            }
            int i, j, k;
            int LastWellIndex = 0;
            int len_block, unpack_len_block, well_count, well_index;
            int num_blocks = bfr.ReadInt32();
            int iLen = bfr.ReadInt32();
            int wellCount = 0;
            Well w;

            for (k = 0; k < IndexBlock; k++)
            {
                len_block = bfr.ReadInt32();
                bfr.BaseStream.Seek(8 + len_block, SeekOrigin.Current);
            }
            for (k = 0; k < NumBlocks; k++)
            {
                if (IndexBlock + k >= num_blocks) break;
                len_block = bfr.ReadInt32();
                unpack_len_block = bfr.ReadInt32();
                well_count = bfr.ReadInt32();

                ms = this.GetBuffer(true);
                unpackMS = this.GetBuffer(false);
                ms.SetLength(len_block);
                br = new BinaryReader(unpackMS);
                ConvertEx.CopyStream(bfr.BaseStream, ms, len_block);
                ConvertEx.UnPackStream(ms, ref unpackMS);

                unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                for (i = 0; i < well_count; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        for (j = 0; j < this.Wells.Count; j++)
                        {
                            w = (Well)Wells[j];
                            w.mer = null;
                        }
                        this.MerLoaded = false;
                        return -1;
                    }
                    well_index = br.ReadInt32();
                    LastWellIndex = well_index;
                    if (well_index > -1)
                    {
                        w = (Well)Wells[well_index];
                        w.mer = new Mer();
                        w.mer.Read(br, false);
                        if (w.mer.Count > 0)
                        {
                            this.MerLoaded = true;
                        }
                    }
                }
                wellCount += well_count;
            }
            FreeBuffers();
            return LastWellIndex;
        }
        #endregion

        #region GIS
        public bool LoadGisFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoaвGisFromCache(worker, e, false, false);
        }
        public bool LoaвGisFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotReportProgress)
        {
            if (project.IsSmartProject) return LoadGisFromCache2(worker, e, NotLoadData, NotReportProgress);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГИС месторождения";
            userState.Element = this.Name;

            // todo утечки

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\gis.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, well_count, gis_len, well_index = -1, iLen, wellCount = 0;
                    int lenItem = 65;
                    Well w;
                    SkvGisItem item;
                    MemoryStream ms = this.GetBuffer(true);
                    MemoryStream unpackMS = this.GetBuffer(false);
                    BinaryReader br = new BinaryReader(unpackMS);
                    if (!NotReportProgress) worker.ReportProgress(0, userState);
                    FileStream fs;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                    }
                    catch (Exception)
                    {
                        FreeBuffers();
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();

                        ClearMemoryStream(ms);
                        ms.SetLength(len_block);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ClearMemoryStream(unpackMS);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                        for (i = 0; i < well_count; i++)
                        {
                            well_index = br.ReadInt32();
                            gis_len = br.ReadInt32();
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                for (j = 0; j < this.Wells.Count; j++)
                                {
                                    w = (Well)Wells[j];
                                    w.gis = null;
                                }
                                this.GisLoaded = false;
                                return false;
                            }
                            if ((well_index > -1) && (gis_len > 0))
                            {
                                w = (Well)Wells[well_index];
                                if (w.gis == null && gis_len > 0) w.gis = new SkvGIS();
                                if (!NotLoadData)
                                {
                                    SkvGisItem[] items = new SkvGisItem[gis_len];
                                    for (j = 0; j < gis_len; j++)
                                    {
                                        items[j] = w.gis.RetrieveItem(br);
                                    }
                                    w.gis.SetItems(items);
                                }
                                else
                                {
                                    unpackMS.Seek(gis_len * lenItem, SeekOrigin.Current);
                                }
                                if (!NotLoadData) this.GisLoaded = true;
                            }
                            retValue = true;
                            if (!NotReportProgress) ReportProgress(worker, wellCount + (i + 1), iLen, userState);
                        }
                        wellCount += well_count;
                    }
                    FreeBuffers();
                    if (iLen == 0 && !NotReportProgress) worker.ReportProgress(100, userState);
                    bfr.Close();
                    br = null;
                    //ms.Dispose();
                    //unpackMS.Dispose();
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша ГИС!";
                if (!NotReportProgress) worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadGisFromCache(int WellIndex)
        {
            return LoadGisFromCache(WellIndex, false);
        }
        public bool LoadGisFromCache(int WellIndex, bool NotLoadData)
        {
            if (project.IsSmartProject) return LoadGisFromCache2(WellIndex, NotLoadData);
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\gis.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, gis_len, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms = this.GetBuffer(true);
                MemoryStream unpackMS = this.GetBuffer(false);
                BinaryReader br = new BinaryReader(unpackMS);

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }

                BinaryReader bfr = new BinaryReader(fs);
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    well_count = bfr.ReadInt32();
                    if (WellIndex < wellCount + well_count)
                    {
                        ms.SetLength(len_block);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();
                        gis_len = br.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if ((well_index > -1) && (gis_len > 0))
                        {
                            w = (Well)Wells[well_index];
                            if (w.gis == null) w.gis = new SkvGIS();
                            if (!NotLoadData)
                            {
                                SkvGisItem[] items = new SkvGisItem[gis_len];
                                for (j = 0; j < gis_len; j++)
                                {
                                    items[j] = w.gis.RetrieveItem(br);
                                }
                                w.gis.SetItems(items);
                            }
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += well_count;
                }
                FreeBuffers();
                bfr.Close();
                br = null;
            }
            return retValue;
        }
        public bool LoadGisFromCache(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            if (project.IsSmartProject) return LoadGisFromCache2(worker, e, WellIndexes);

            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГИС месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\gis.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int lastIndex = 0;
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift, gis_len;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);

                    WellIndexes.Sort();
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    lastIndex = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();

                        if (WellIndexes[lastIndex] < wellCount + well_count)
                        {
                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            while ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                            {
                                shift = (WellIndexes[lastIndex] - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();
                                if (WellIndexes[lastIndex] != well_index)
                                {
                                    bfr.Close();
                                    return false;
                                }
                                else
                                {
                                    w = (Well)Wells[well_index];
                                    gis_len = br.ReadInt32();
                                    if (w.gis == null) w.gis = new SkvGIS();
                                    SkvGisItem[] items = new SkvGisItem[gis_len];
                                    for (j = 0; j < gis_len; j++)
                                    {
                                        items[j] = w.gis.RetrieveItem(br);
                                    }
                                    w.gis.SetItems(items);
                                    GisLoaded = w.gis.Count > 0;
                                }
                                lastIndex++;
                                if (worker != null) ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                            }
                            if (lastIndex >= WellIndexes.Count) break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                    FreeBuffers();
                    bfr.Close();
                    retValue = true;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteGisToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных ГИС в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\gis.bin";
                int MAX_SIZE_PACK_BLOCK = 100 * 1024;
                Well w;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                BinaryReader br;
                MemoryStream ms;
                BinaryWriter bw;
                int[] arr;
                int i, j, k, h, lenItem = 65, lenWrite = 0, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int gis_len, iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        if ((w.GisLoaded) && (w.gis != null) && (w.gis.Items != null))
                            len_all_wells += 12 + w.gis.Count * lenItem;
                        else
                            len_all_wells += 12;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);

                    len_block = 0;
                    k = 0;
                    wCount = 0;

                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        if ((w.GisLoaded) && (w.gis != null) && (w.gis.Items != null))
                            len_shift = 12 + w.gis.Count * lenItem;
                        else
                            len_shift = 12;


                        if (len_block + len_shift <= len_all_wells)
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                if (w.GisLoaded) len_shift += 8 + w.gis.Count * lenItem;
                                else len_shift += 8;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);
                                if (w.GisLoaded)
                                {
                                    gis_len = w.gis.Count;
                                    bw.Write(gis_len);
                                    for (h = 0; h < gis_len; h++)
                                    {
                                        w.gis.RetainItem(bw, h);
                                    }
                                }
                                else
                                {
                                    bw.Write(0);
                                }
                            }
                            unpackSizes.Add((int)ms.Length);
                            packedMS = ConvertEx.PackStream(ms);
                            msArr.Add(packedMS);
                            wellCount.Add(wCount);
                            WellsShift.Clear();

                            ms.SetLength(0);
                            k = i;

                            w = (Well)Wells[i];
                            if ((w.GisLoaded) && (w.gis != null) && (w.gis.Items != null))
                                len_shift = 12 + w.gis.Count * lenItem;
                            else
                                len_shift = 12;

                            len_block = len_shift;
                            wCount = 1;
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    len_shift = 4 * wCount;
                    bw.Write(len_shift);
                    for (j = k; j < i - 1; j++)
                    {
                        w = (Well)Wells[j];
                        if (w.GisLoaded) len_shift += 8 + w.gis.Count * lenItem;
                        else len_shift += 8;
                        bw.Write(len_shift);
                    }

                    for (j = k; j < i; j++)
                    {
                        w = (Well)Wells[j];
                        bw.Write(w.Index);
                        if (w.GisLoaded)
                        {
                            gis_len = w.gis.Count;
                            bw.Write(gis_len);
                            for (h = 0; h < gis_len; h++)
                            {
                                w.gis.RetainItem(bw, h);
                            }
                        }
                        else
                        {
                            bw.Write(0);
                        }
                    }
                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearGisData(bool useGC)
        {
            Well w;
            for (int i = 0; i < Wells.Count; i++)
            {
                w = Wells[i];
                w.gis = null;
            }
            if (useGC)
            {   
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            this.GisLoaded = false;
        }
        #endregion

        #region PERFORATION
        public bool LoadPerfFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadPerfFromCache(worker, e, false, false);
        }
        public bool LoadPerfFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotReportProgress)
        {
            if (project.IsSmartProject) return LoadPerfFromCache2(worker, e, NotLoadData, NotReportProgress);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных перфорации месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\perf.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, iLen, well_index, perf_count;
                    long shift;
                    long[] shiftList;
                    Well w;

                    if (!NotReportProgress) worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);

                        BinaryReader bfr = new BinaryReader(fs);
                        iLen = bfr.ReadInt32();
                        shiftList = new long[iLen];
                        for (i = 0; i < iLen; i++)
                        {
                            shiftList[i] = bfr.ReadInt64();
                        }

                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                for (j = 0; j < this.Wells.Count; j++)
                                {
                                    w = (Well)Wells[j];
                                    w.perf = null;
                                }
                                this.PerfLoaded = false;
                                return false;
                            }
                            shift = shiftList[i] - fs.Position;
                            if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                            well_index = bfr.ReadInt32();
                            perf_count = bfr.ReadInt32();

                            if ((well_index > -1) && (perf_count > 0))
                            {
                                w = (Well)Wells[well_index];
                                if (!NotLoadData)
                                {
                                    if (w.perf == null) w.perf = new SkvPerf();
                                    SkvPerfItem[] items = new SkvPerfItem[perf_count];
                                    for (j = 0; j < perf_count; j++)
                                    {
                                        items[j] = w.perf.RetrieveItem(bfr);
                                    }
                                    w.perf.SetItems(items);
                                }
                                else if (perf_count > 0)
                                {
                                    w.perf = new SkvPerf();
                                }
                            }
                            retValue = true;
                            if (!NotReportProgress) ReportProgress(worker, i + 1, iLen, userState);
                        }
                        if (iLen == 0 && !NotReportProgress) worker.ReportProgress(100, userState);
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша перфорации!";
                if (!NotReportProgress) worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadPerfFromCache(int WellIndex)
        {
            return LoadPerfFromCache(WellIndex, false);
        }
        public bool LoadPerfFromCache(int WellIndex, bool NotLoadData)
        {
            if (project.IsSmartProject) return LoadPerfFromCache2(WellIndex, NotLoadData);
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\perf.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, perf_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    iLen = bfr.ReadInt32();
                    fs.Seek(8 * WellIndex, SeekOrigin.Current);
                    shift = bfr.ReadInt64();
                    fs.Seek(shift, SeekOrigin.Begin);
                    well_index = bfr.ReadInt32();
                    if (WellIndex == well_index)
                    {
                        w = (Well)Wells[well_index];
                        perf_count = bfr.ReadInt32();
                        if ((!NotLoadData) && (perf_count > 0))
                        {
                            if (w.perf == null) w.perf = new SkvPerf();
                            SkvPerfItem[] items = new SkvPerfItem[perf_count];

                            for (j = 0; j < perf_count; j++)
                            {
                                items[j] = w.perf.RetrieveItem(bfr);
                            }
                            w.perf.SetItems(items);
                        }
                        else if (perf_count > 0)
                        {
                            w.perf = new SkvPerf();
                        }
                        retValue = true;
                    }
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        public bool LoadPerfFromCache(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            if (project.IsSmartProject) return LoadPerfFromCache2(worker, e, WellIndexes);

            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных перфорации месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\perf.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int lastIndex = 0;
                int i, j, well_index = -1, iLen;
                int perf_count;
                long shift;
                Well w;
                FileStream fs;

                try
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    WellIndexes.Sort();
                    iLen = bfr.ReadInt32();

                    lastIndex = 0;

                    while (lastIndex < WellIndexes.Count && WellIndexes[lastIndex] < iLen)
                    {
                        fs.Seek(4 + 8 * WellIndexes[lastIndex], SeekOrigin.Begin);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        if (WellIndexes[lastIndex] != well_index)
                        {
                            bfr.Close();
                            return false;
                        }
                        else
                        {
                            w = (Well)Wells[well_index];
                            perf_count = bfr.ReadInt32();
                            if (perf_count > 0)
                            {
                                if (w.perf == null) w.perf = new SkvPerf();
                                SkvPerfItem[] items = new SkvPerfItem[perf_count];

                                for (j = 0; j < perf_count; j++)
                                {
                                    items[j] = w.perf.RetrieveItem(bfr);
                                }
                                w.perf.SetItems(items);
                                PerfLoaded = w.perf.Count > 0;
                            }
                        }
                        lastIndex++;
                        if (worker != null) ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                    }
                    FreeBuffers();
                    bfr.Close();
                    retValue = true;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        public bool WritePerfToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных перфорации в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\perf.bin";
                Well w;
                FileStream fs;
                BinaryWriter bfw;
                int i, j, perf_len;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    bfw = new BinaryWriter(fs);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current);
                    long[] shiftList = new long[iLen];

                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        shiftList[i] = fs.Position;
                        bfw.Write(w.Index);
                        if (w.PerfLoaded)
                        {
                            perf_len = w.perf.Count;
                            bfw.Write(perf_len);
                            for (j = 0; j < perf_len; j++)
                            {
                                w.perf.RetainItem(bfw, j);
                            }
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftList.Length > 0)
                    {
                        fs.Seek(4, SeekOrigin.Begin);
                        for (i = 0; i < shiftList.Length; i++)
                        {
                            bfw.Write(shiftList[i]);
                        }
                    }
                    bfw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearPerfData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    w.perf = null;
                }
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
            }
            this.PerfLoaded = false;
        }
        #endregion

        #region LOGS
        // BASE LOGS
        public bool LoadLogsBaseFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadLogsBaseFromCache(worker, e, false, false);
        }
        public bool LoadLogsBaseFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotLoadValues)
        {
            if (project.IsSmartProject) return LoadLogsBaseFromCache2(worker, e, NotLoadData, NotLoadValues);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных базового каротажа месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\logs_base.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, pack_len, logs_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    SkvGisItem item;
                    int maxPackLen = 0, maxUnPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        iLen = bfr.ReadInt32();
                        shiftList = new long[iLen];
                        for (i = 0; i < iLen; i++)
                        {
                            shiftList[i] = bfr.ReadInt64();
                        }

                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                for (j = 0; j < this.Wells.Count; j++)
                                {
                                    w = (Well)Wells[j];
                                    w.logsBase = null;
                                }
                                this.LogsBaseLoaded = false;
                                return false;
                            }
                            shift = shiftList[i] - fs.Position;
                            if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                            well_index = bfr.ReadInt32();
                            pack_len = bfr.ReadInt32();
                            if (maxPackLen < pack_len) maxPackLen = pack_len;
                            if ((well_index > -1) && (pack_len > 0))
                            {
                                w = (Well)Wells[well_index];
                                
                                
                                ms = this.GetBuffer(true);
                                unpackMS = this.GetBuffer(false);
                                ms.SetLength(pack_len);
                                br = new BinaryReader(unpackMS);
                                ConvertEx.CopyStream(fs, ms, pack_len);

                                ConvertEx.UnPackStream(ms, ref unpackMS);
                                logs_count = br.ReadInt32();
                                if (!NotLoadData && logs_count > 0)
                                    {
                                        w.logsBase = new SkvLog[logs_count];
                                        for (j = 0; j < logs_count; j++)
                                        {
                                            if (w.logsBase[j] == null) w.logsBase[j] = new SkvLog();
                                            w.logsBase[j].RetriveItems(br, !NotLoadValues);
                                        }
                                        if (maxUnPackLen < (int)unpackMS.Position) maxUnPackLen = (int)unpackMS.Position;
                                    }
                                else if (logs_count > 0)
                                {
                                    w.logsBase = new SkvLog[1];
                                }
                            }

                            retValue = true;
                            ReportProgress(worker, i + 1, iLen, userState);
                        }
                        FreeBuffers();
                        if (iLen == 0) worker.ReportProgress(100, userState);
                        bfr.Close();
                        br = null;
                        fs = null;
                    }
                    catch (Exception)
                    {
                        FreeBuffers();
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша каротажа!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoaвLogsBaseFromCache(int WellIndex)
        {
            return LoadLogsBaseFromCache(WellIndex, false, false);
        }
        public bool LoadLogsBaseFromCache(int WellIndex, bool NotLoadData, bool NotLoadValues)
        {
            if (project.IsSmartProject) return LoadLogsBaseFromCache2(WellIndex, NotLoadData, NotLoadValues);
            bool retValue = false;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\logs_base.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, pack_len, logs_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);

                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    iLen = bfr.ReadInt32();
                    if (WellIndex >= iLen) return false;
                    fs.Seek(8 * WellIndex, SeekOrigin.Current);
                    shift = bfr.ReadInt64();
                    fs.Seek(shift, SeekOrigin.Begin);
                    well_index = bfr.ReadInt32();
                    pack_len = bfr.ReadInt32();
                    if (WellIndex != well_index)
                    {
                        MessageBox.Show("Не найден индекс скважины в кеше!");
                    }
                    else if ((well_index > -1) && (pack_len > 0))
                    {
                        w = (Well)Wells[well_index];
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(pack_len);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, pack_len);
                        ConvertEx.UnPackStream(ms, ref unpackMS);
                        logs_count = br.ReadInt32();
                        if ((!NotLoadData) && (logs_count > 0))
                        {
                            w.logsBase = new SkvLog[logs_count];
                            for (j = 0; j < logs_count; j++)
                            {
                                if (w.logsBase[j] == null) w.logsBase[j] = new SkvLog();
                                w.logsBase[j].RetriveItems(br, !NotLoadValues);
                            }
                        }
                    }
                    FreeBuffers();
                    bfr.Close();
                    fs = null;
                }
#if !DEBUG
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
#endif
                finally
                {
                    if (fs != null) fs.Close();
                }
            }
            return retValue;
        }
        public bool LoadLogsBaseFromCache(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            bool retValue = false;

            //string cacheName = this.project.path + "\\cache\\" + this.Name + "\\logs_base.bin";
            //if (File.Exists(cacheName))
            //{
            //    int i, j, k, pack_len, logs_count, well_index = -1, iLen;
            //    long shift;
            //    Well w;
            //    FileStream fs;
            //    try
            //    {
            //        fs = new FileStream(cacheName, FileMode.Open);

            //        MemoryStream ms, unpackMS;

            //        BinaryReader br, bfr = new BinaryReader(fs);
            //        iLen = bfr.ReadInt32();
            //        fs.Seek(8 * WellIndex, SeekOrigin.Current);
            //        shift = bfr.ReadInt64();
            //        fs.Seek(shift, SeekOrigin.Begin);
            //        well_index = bfr.ReadInt32();
            //        pack_len = bfr.ReadInt32();
            //        if (WellIndex != well_index)
            //        {
            //            MessageBox.Show("Не найден индекс скважины в кеше!");
            //        }
            //        else if ((well_index > -1) && (pack_len > 0))
            //        {
            //            w = (Well)Wells[well_index];
            //            ms = this.GetBuffer(true);
            //            unpackMS = this.GetBuffer(false);
            //            ms.SetLength(pack_len);
            //            br = new BinaryReader(unpackMS);
            //            ConvertEx.CopyStream(fs, ms, pack_len);
            //            ConvertEx.UnPackStream(ms, ref unpackMS);
            //            logs_count = br.ReadInt32();
            //            if ((!NotLoadData) && (logs_count > 0))
            //            {
            //                w.logsBase = new SkvLog[logs_count];
            //                for (j = 0; j < logs_count; j++)
            //                {
            //                    if (w.logsBase[j] == null) w.logsBase[j] = new SkvLog();
            //                    w.logsBase[j].RetriveItems(br, !NotLoadValues);
            //                }
            //            }
            //        }
            //        FreeBuffers();
            //        bfr.Close();
            //        br = null;
            //    }
            //    catch (Exception)
            //    {
            //        FreeBuffers();
            //        return false;
            //    }
            //}
            return retValue;
        }
        public bool WriteLogsBaseToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных базового каротажа в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                int i, j;
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\logs_base.bin";
                Well w;
                FileStream fs;
                int logs_count, iLen = Wells.Count;

                if (iLen > 0)
                {
                    long[] shiftWells = new long[iLen];
                    MemoryStream ms = GetBuffer(true);
                    MemoryStream packedMS;
                    BinaryWriter bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    BinaryWriter bfw = new BinaryWriter(fs);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current); // сдвигаемся на начало данных
                    shiftWells[0] = fs.Position;

                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        ClearMemoryStream(ms);
                        if (i > 0)
                        {
                            shiftWells[i] = fs.Position;
                        }
                        bfw.Write(w.Index);
                        if (w.LogsBaseLoaded)
                        {
                            logs_count = w.logsBase.Length;
                            bw.Write(logs_count);
                            for (j = 0; j < logs_count; j++)
                            {
                                w.logsBase[j].RetainItems(bw);
                                w.logsBase[j] = null;
                            }
                            w.logsBase = null;
                            packedMS = ConvertEx.PackStream(ms);
                            bfw.Write((int)packedMS.Length);
                            packedMS.Seek(0, SeekOrigin.Begin);
                            packedMS.WriteTo(fs);
                        }
                        else if (w.PackedLogsBase != null)
                        {
                            bfw.Write((int)w.PackedLogsBase.Length);
                            w.PackedLogsBase.Seek(0, SeekOrigin.Begin);
                            w.PackedLogsBase.WriteTo(fs);
                            w.PackedLogsBase.Dispose();
                            w.PackedLogsBase = null;
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftWells.Length > 0)
                    {
                        fs.Seek(4, SeekOrigin.Begin);
                        for (i = 0; i < shiftWells.Length; i++)
                        {
                            bfw.Write(shiftWells[i]);
                        }
                    }
                    FreeBuffers();
                    bfw.Close();
                    bw = null;
                    shiftWells = null;
                    GC.GetTotalMemory(true);
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }

        // OTHER LOGS
        public bool LoadLogsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadLogsFromCache(worker, e, false, false);
        }
        public bool LoadLogsFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotLoadValues)
        {
            if (project.IsSmartProject) return LoadLogsFromCache2(worker, e, NotLoadData, NotLoadValues);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных каротажа месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\logs.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, pack_len, logs_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    SkvGisItem item;
                    int maxPackLen = 0, maxUnPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        iLen = bfr.ReadInt32();
                        shiftList = new long[iLen];
                        for (i = 0; i < iLen; i++)
                        {
                            shiftList[i] = bfr.ReadInt64();
                        }

                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                for (j = 0; j < this.Wells.Count; j++)
                                {
                                    w = (Well)Wells[j];
                                    w.logs = null;
                                }
                                this.LogsLoaded = false;
                                return false;
                            }
                            shift = shiftList[i] - fs.Position;
                            if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                            well_index = bfr.ReadInt32();
                            pack_len = bfr.ReadInt32();
                            if (maxPackLen < pack_len) maxPackLen = pack_len;
                            if ((well_index > -1) && (pack_len > 0))
                            {
                                w = (Well)Wells[well_index];
                                ms = this.GetBuffer(true);
                                unpackMS = this.GetBuffer(false);
                                ms.SetLength(pack_len);
                                br = new BinaryReader(unpackMS);
                                ConvertEx.CopyStream(fs, ms, pack_len);

                                ConvertEx.UnPackStream(ms, ref unpackMS);
                                logs_count = br.ReadInt32();
                                if ((!NotLoadData) && (logs_count > 0))
                                {
                                    w.logs = new SkvLog[logs_count];
                                    for (j = 0; j < logs_count; j++)
                                    {
                                        if (w.logs[j] == null) w.logs[j] = new SkvLog();
                                        w.logs[j].RetriveItems(br, !NotLoadValues);
                                    }
                                    if (maxUnPackLen < (int)unpackMS.Position) maxUnPackLen = (int)unpackMS.Position;
                                }
                                else if (logs_count > 0)
                                {
                                    w.logs = new SkvLog[1];
                                }
                            }
                            retValue = true;
                            ReportProgress(worker, i + 1, iLen, userState);
                        }
                        FreeBuffers();
                        if (iLen == 0) worker.ReportProgress(100, userState);
                        bfr.Close();
                        br = null;
                        fs = null;
                    }
                    catch (Exception)
                    {
                        FreeBuffers();
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша каротажа!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadLogsFromCache(int WellIndex)
        {
            return LoadLogsFromCache(WellIndex, false, false);
        }
        public bool LoadLogsFromCache(int WellIndex, bool NotLoadData, bool NotLoadValues)
        {
            if (project.IsSmartProject) return LoadLogsFromCache2(WellIndex, NotLoadData, NotLoadValues);
            bool retValue = false;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\logs.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, pack_len, logs_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);

                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    iLen = bfr.ReadInt32();
                    if (WellIndex >= iLen) return false;

                    fs.Seek(8 * WellIndex, SeekOrigin.Current);
                    shift = bfr.ReadInt64();
                    fs.Seek(shift, SeekOrigin.Begin);
                    well_index = bfr.ReadInt32();
                    pack_len = bfr.ReadInt32();
                    if (WellIndex != well_index)
                    {
                        MessageBox.Show("Не найден индекс скважины в кеше!");
                    }
                    else if ((well_index > -1) && (pack_len > 0))
                    {
                        w = (Well)Wells[well_index];
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(pack_len);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, pack_len);
                        ConvertEx.UnPackStream(ms, ref unpackMS);
                        logs_count = br.ReadInt32();
                        if ((!NotLoadData) && (logs_count > 0))
                        {
                            w.logs = new SkvLog[logs_count];
                            for (j = 0; j < logs_count; j++)
                            {
                                if (w.logs[j] == null) w.logs[j] = new SkvLog();
                                w.logs[j].RetriveItems(br, !NotLoadValues);
                            }
                        }
                    }
                    FreeBuffers();
                    br = null;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
            }
            return retValue;
        }
        public bool WriteLogsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных каротажа в кэш";
            userState.Element = this.Name;
            System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                int i, j;
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\logs.bin";
                Well w;
                FileStream fs;
                int logs_count, iLen = Wells.Count;

                if (iLen > 0)
                {
                    long[] shiftWells = new long[iLen];
                    MemoryStream ms = GetBuffer(true);
                    MemoryStream packedMS;
                    BinaryWriter bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    BinaryWriter bfw = new BinaryWriter(fs);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current); // сдвигаемся на начало данных
                    shiftWells[0] = fs.Position;

                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        ClearMemoryStream(ms);
                        if (i > 0)
                        {
                            shiftWells[i] = fs.Position;
                        }
                        bfw.Write(w.Index);
                        if (w.LogsLoaded)
                        {
                            logs_count = w.logs.Length;
                            bw.Write(logs_count);
                            for (j = 0; j < logs_count; j++)
                            {
                                w.logs[j].RetainItems(bw);
                                w.logs[j] = null;
                            }
                            w.logs = null;
                            packedMS = ConvertEx.PackStream(ms);
                            bfw.Write((int)packedMS.Length);
                            packedMS.Seek(0, SeekOrigin.Begin);
                            packedMS.WriteTo(fs);
                        }
                        else if (w.PackedLogs != null)
                        {
                            bfw.Write((int)w.PackedLogs.Length);
                            w.PackedLogs.Seek(0, SeekOrigin.Begin);
                            w.PackedLogs.WriteTo(fs);
                            w.PackedLogs.Dispose();
                            w.PackedLogs = null;
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftWells.Length > 0)
                    {
                        fs.Seek(4, SeekOrigin.Begin);
                        for (i = 0; i < shiftWells.Length; i++)
                        {
                            bfw.Write(shiftWells[i]);
                        }
                    }
                    FreeBuffers();
                    bfw.Close();
                    bw = null;
                    shiftWells = null;
                    GC.GetTotalMemory(true);
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }

        public void ClearLogsData(bool useGC)
        {
            int i, j, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if ((w.LogsLoaded) && (w.logs != null))
                    {
                        for (j = 0; j < w.logs.Length; j++)
                        {
                            w.logs[j] = null;
                        }
                        w.logs = null;
                    }
                }
            }
            if (useGC) GC.GetTotalMemory(true);
            this.LogsLoaded = false;
        }
        public void ClearLogsBaseData(bool useGC)
        {
            int i, j, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if ((w.LogsBaseLoaded) && (w.logsBase != null))
                    {
                        for (j = 0; j < w.logsBase.Length; j++)
                        {
                            w.logsBase[j] = null;
                        }
                        w.logsBase = null;
                    }
                }
            }
            if (useGC) GC.GetTotalMemory(true);
            this.LogsBaseLoaded = false;
        }
        #endregion

        #region CHESS
        public bool LoadChessFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadChessFromCache(worker, e, false);
        }
        public bool LoadChessFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            return LoadChessFromCache(worker, e, NotLoadData, false);
        }
        public bool LoadChessFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotReportProgress)
        {
            if (project.IsSmartProject) return LoadChessFromCache2(worker, e, NotLoadData, NotReportProgress);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных Шахматки месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\chess.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                FileStream fs = null;
                try
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, well_count, chess_len, well_index = -1, iLen, wellCount = 0;
                    int lenDailyItem = 32/*68*/;
                    int lenDailyInjItem = 24;
                    Well w;
                    ChessItem dayItem;
                    ChessInjItem dayInjItem;

                    MemoryStream ms = new MemoryStream(1 * 1024 * 1024);
                    MemoryStream unpackMS = new MemoryStream(1 * 1024 * 1024);
                    BinaryReader br = new BinaryReader(unpackMS);

                    fs = new FileStream(cacheName, FileMode.Open); ;
                    BinaryReader bfr = new BinaryReader(fs);
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    for (k = 0; k < num_blocks; k++)
                    {
                        if (worker != null && worker.CancellationPending)
                        {
                            e.Cancel = true;
                            ClearChessData(false);
                            return false;
                        }
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();
                        ClearMemoryStream(ms);
                        ms.SetLength(len_block);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ClearMemoryStream(unpackMS);
                        ConvertEx.UnPackStream(ms, ref unpackMS);


                        unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                        for (i = 0; i < well_count; i++)
                        {
                            well_index = br.ReadInt32();

                            #region Шахматка добывающих
                            chess_len = br.ReadInt32();
                            if ((well_index > -1) && (chess_len > 0))
                            {
                                w = (Well)Wells[well_index];
                                userState.Element = string.Format("{0}[{1}]", w.UpperCaseName, Name);
                                ChessItem[] dailyItems = null;
                                if (!NotLoadData)
                                {
                                    dailyItems = new ChessItem[chess_len];
                                    for (j = 0; j < chess_len; j++)
                                    {
                                        dayItem = new ChessItem();
                                        dayItem.Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                        dayItem.Qliq = br.ReadSingle();
                                        dayItem.Qoil = br.ReadSingle();
                                        dayItem.QliqV = br.ReadSingle();
                                        dayItem.QoilV = br.ReadSingle();
                                        dayItem.StayTime = br.ReadInt32();
                                        dayItem.Hdyn = br.ReadSingle();
                                        dayItem.StayReason = br.ReadInt32();
                                        dailyItems[j] = dayItem;
                                    }
                                }
                                else
                                    unpackMS.Seek(lenDailyItem * chess_len, SeekOrigin.Current);

                                // br.BaseStream.Seek(4, SeekOrigin.Current);
                                w.chess = new WellChess();
                                if (!NotLoadData)
                                {
                                    w.chess.SetItems(dailyItems);
                                    this.ChessLoaded = true;
                                }
                                userState.Element = string.Format("{0}[{1}]", w.UpperCaseName, Name);
                            }
                            //else
                            //    unpackMS.Seek(4, SeekOrigin.Current);
                            #endregion

                            #region Шахм нагнетатальных
                            chess_len = br.ReadInt32();
                            if ((well_index > -1) && (chess_len > 0))
                            {
                                w = (Well)Wells[well_index];
                                userState.Element = string.Format("{0}[{1}]", w.UpperCaseName, Name);

                                ChessInjItem[] dailyInjItems = null;
                                if (!NotLoadData)
                                {
                                    dailyInjItems = new ChessInjItem[chess_len];
                                    for (j = 0; j < chess_len; j++)
                                    {
                                        dayInjItem = new ChessInjItem();
                                        dayInjItem.Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                        //dayInjItem.Wwat = (float)br.ReadDouble();
                                        dayInjItem.Wwat = br.ReadSingle();
                                        dayInjItem.Pust = br.ReadSingle();
                                        //br.BaseStream.Seek(20, SeekOrigin.Current);
                                        dayInjItem.CycleTime = br.ReadInt32();
                                        dayInjItem.StayTime = br.ReadInt32();
                                        dayInjItem.StayReason = br.ReadInt32();
                                        dailyInjItems[j] = dayInjItem;
                                    }
                                }
                                else
                                    unpackMS.Seek(lenDailyInjItem * chess_len, SeekOrigin.Current);

                                //br.BaseStream.Seek(4, SeekOrigin.Current);
                                w.chessInj = new WellChessInj();
                                if (!NotLoadData)
                                {
                                    w.chessInj.SetItems(dailyInjItems);
                                    this.ChessLoaded = true;
                                }
                                userState.Element = string.Format("{0}[{1}]", w.UpperCaseName, Name);
                            }
                            //else
                            //    unpackMS.Seek(4, SeekOrigin.Current);
                            #endregion

                            retValue = true;
                            if (!NotReportProgress) ReportProgress(worker, wellCount + i, iLen, userState);
                        }
                        wellCount += well_count;
                    }
                    ms.Dispose();
                    br.Close();
                    unpackMS.Dispose();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    ClearChessData(false);
                    throw new Exception("Ошибка загрузки данных Шахматки", ex);
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
            }
            return retValue;
        }
        public bool LoadChessFromCache(int WellIndex)
        {
            return LoadChessFromCache(WellIndex, false);
        }
        public bool LoadChessFromCache(int WellIndex, bool NotLoadData)
        {
            if (project.IsSmartProject) return LoadChessFromCache2(WellIndex, NotLoadData);
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\chess.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, chess_day_len, well_index = -1, iLen, wellCount = 0;
                int chess_inj_day_len;
                int shift;
                Well w;
                ChessItem dayItem;
                ChessInjItem dayInjItem;

                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                //MemoryStream ms = new MemoryStream(1 * 1024 * 1024);
                //MemoryStream unpackMS = new MemoryStream(1 * 1024 * 1024);

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    well_count = bfr.ReadInt32();
                    if (WellIndex < wellCount + well_count)
                    {
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();

                        if (WellIndex != well_index)
                        {
                            bfr.Close();
                            return false;
                        }
                        else if (well_index > -1)
                        {
                            chess_day_len = br.ReadInt32();
                            chess_inj_day_len = 0;
                            ChessItem[] dailyItems = null;
                            ChessInjItem[] dailyInjItems = null;

                            w = (Well)Wells[well_index];
                            if (!NotLoadData)
                            {
                                try
                                {
                                    #region добывающие скв
                                    if (chess_day_len > 0)
                                    {
                                        dailyItems = new ChessItem[chess_day_len];
                                        for (j = 0; j < chess_day_len; j++)
                                        {
                                            dayItem = new ChessItem();
                                            dayItem.Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                            dayItem.Qliq = br.ReadSingle();
                                            dayItem.Qoil = br.ReadSingle();
                                            dayItem.QliqV = br.ReadSingle();
                                            dayItem.QoilV = br.ReadSingle();
                                            dayItem.StayTime = br.ReadInt32();
                                            dayItem.Hdyn = br.ReadSingle();
                                            dayItem.StayReason = br.ReadInt32();
                                            dailyItems[j] = dayItem;
                                        }
                                    }
                                    #endregion

                                    //br.BaseStream.Seek(4, SeekOrigin.Current);

                                    chess_inj_day_len = br.ReadInt32();
                                    if (chess_inj_day_len > 0)
                                    {
                                        dailyInjItems = new ChessInjItem[chess_inj_day_len];
                                        for (j = 0; j < chess_inj_day_len; j++)
                                        {
                                            dayInjItem = new ChessInjItem();
                                            dayInjItem.Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                            //dayInjItem.Wwat = (float)br.ReadDouble();
                                            dayInjItem.Wwat = br.ReadSingle();
                                            dayInjItem.Pust = br.ReadSingle();
                                            //br.BaseStream.Seek(20, SeekOrigin.Current);
                                            dayInjItem.CycleTime = br.ReadInt32();
                                            dayInjItem.StayTime = br.ReadInt32();
                                            dayInjItem.StayReason = br.ReadInt32();
                                            dailyInjItems[j] = dayInjItem;
                                        }
                                    }
                                    //br.BaseStream.Seek(4, SeekOrigin.Current);
                                }
                                catch
                                {
                                    chess_day_len = 0;
                                    chess_inj_day_len = 0;
                                }
                            }

                            if (chess_day_len > 0)
                            {
                                if (!NotLoadData)
                                {
                                    w.chess = new WellChess();
                                    w.chess.SetItems(dailyItems);
                                }
                            }
                            if (chess_inj_day_len > 0)
                            {
                                if (!NotLoadData)
                                {
                                    w.chessInj = new WellChessInj();
                                    w.chessInj.SetItems(dailyInjItems);
                                }
                            }
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += well_count;
                }
                FreeBuffers();
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadChessFromCache(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            if (project.IsSmartProject) return LoadChessFromCache2(worker, e, WellIndexes);
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных Шахматки месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\chess.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, chess_day_len, well_index = -1, iLen, wellCount = 0;
                int chess_inj_day_len;
                int shift, lastIndex;
                Well w;
                ChessItem dayItem;
                ChessInjItem dayInjItem;

                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;
                if (worker != null) worker.ReportProgress(0, userState);

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                lastIndex = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    well_count = bfr.ReadInt32();
                    if ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                    {
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        while ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                        {
                            if (worker != null && worker.CancellationPending)
                            {
                                e.Cancel = true;
                                ClearChessData(false);
                                return false;
                            }
                            shift = (WellIndexes[lastIndex] - wellCount) * 4;
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            shift = br.ReadInt32();
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            well_index = br.ReadInt32();

                            if (WellIndexes[lastIndex] != well_index)
                            {
                                bfr.Close();
                                return false;
                            }
                            else if (well_index > -1)
                            {
                                chess_day_len = br.ReadInt32();
                                chess_inj_day_len = 0;
                                ChessItem[] dailyItems = null;
                                ChessInjItem[] dailyInjItems = null;

                                w = (Well)Wells[well_index];

                                #region добывающие скв
                                if (chess_day_len > 0)
                                {
                                    dailyItems = new ChessItem[chess_day_len];
                                    for (j = 0; j < chess_day_len; j++)
                                    {
                                        dayItem = new ChessItem();
                                        dayItem.Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                        dayItem.Qliq = br.ReadSingle();
                                        dayItem.Qoil = br.ReadSingle();
                                        dayItem.QliqV = br.ReadSingle();
                                        dayItem.QoilV = br.ReadSingle();
                                        dayItem.StayTime = br.ReadInt32();
                                        dayItem.Hdyn = br.ReadSingle();
                                        dayItem.StayReason = br.ReadInt32();
                                        dailyItems[j] = dayItem;
                                    }
                                }
                                //br.BaseStream.Seek(4, SeekOrigin.Current);

                                #endregion

                                chess_inj_day_len = br.ReadInt32();
                                if (chess_inj_day_len > 0)
                                {
                                    dailyInjItems = new ChessInjItem[chess_inj_day_len];
                                    for (j = 0; j < chess_inj_day_len; j++)
                                    {
                                        dayInjItem = new ChessInjItem();
                                        dayInjItem.Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                        //dayInjItem.Wwat = (float)br.ReadDouble();
                                        dayInjItem.Wwat = br.ReadSingle();
                                        dayInjItem.Pust = br.ReadSingle();
                                        //br.BaseStream.Seek(20, SeekOrigin.Current);
                                        dayInjItem.CycleTime = br.ReadInt32();
                                        dayInjItem.StayTime = br.ReadInt32();
                                        dayInjItem.StayReason = br.ReadInt32();
                                        dailyInjItems[j] = dayInjItem;
                                    }
                                }
                                // br.BaseStream.Seek(4, SeekOrigin.Current);

                                if (chess_day_len > 0)
                                {
                                    w.chess = new WellChess();
                                    w.chess.SetItems(dailyItems);
                                    ChessLoaded = true;
                                }
                                if (chess_inj_day_len > 0)
                                {
                                    w.chessInj = new WellChessInj();
                                    w.chessInj.SetItems(dailyInjItems);
                                    ChessLoaded = true;
                                }
                            }
                            lastIndex++;
                            ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                        }
                        if (lastIndex >= WellIndexes.Count) break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += well_count;
                }
                FreeBuffers();
                bfr.Close();
            }
            return ChessLoaded;
        }
        public bool WriteChessToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных Шахматки в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\chess.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                ChessItem dayItem;
                ChessInjItem dayInjItem;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();


                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;
                int i, j, k, h, lenDailyItem = 32, len_all_wells, wCount = 0, len_block;
                int lenDailyInjItem = 24;
                int len_shift;
                int chess_len, iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        len_all_wells += 8;
                        if ((w.ChessLoaded) && (w.chess != null))
                        {
                            len_all_wells += 4 + w.chess.Count * lenDailyItem;
                        }
                        else
                            len_all_wells += 4;
                        if ((w.ChessInjLoaded) && (w.chessInj != null))
                        {
                            len_all_wells += 4 + w.chessInj.Count * lenDailyInjItem;
                        }
                        else
                            len_all_wells += 4;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = (Well)Wells[i];
                            len_shift = 8;
                            if ((w.ChessLoaded) && (w.chess != null))
                            {
                                len_shift += 4 + w.chess.Count * lenDailyItem;
                            }
                            else
                                len_shift += 4;
                            if ((w.ChessInjLoaded) && (w.chessInj != null))
                            {
                                len_shift += 4 + w.chessInj.Count * lenDailyInjItem;
                            }
                            else
                                len_shift += 4;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                len_shift += 4;
                                if (w.ChessLoaded)
                                {
                                    len_shift += 4 + w.chess.Count * lenDailyItem;
                                }
                                else
                                    len_shift += 4;
                                if (w.ChessInjLoaded)
                                {
                                    len_shift += 4 + w.chessInj.Count * lenDailyInjItem;
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);

                                #region Шахматка добывающих скважин
                                if (w.ChessLoaded)
                                {
                                    chess_len = w.chess.Count;
                                    bw.Write(chess_len);
                                    for (h = 0; h < chess_len; h++)
                                    {
                                        dayItem = w.chess[h];
                                        bw.Write(RdfSystem.PackDateTime(dayItem.Date));
                                        bw.Write(dayItem.Qliq);
                                        bw.Write(dayItem.Qoil);
                                        bw.Write(dayItem.QliqV);
                                        bw.Write(dayItem.QoilV);
                                        bw.Write(dayItem.StayTime);
                                        bw.Write(dayItem.Hdyn);
                                        bw.Write(dayItem.StayReason);
                                    }
                                    //bw.Write(0);
                                }
                                else
                                {
                                    bw.Write(0);
                                    //bw.Write(0);
                                }
                                #endregion

                                #region Шахматка нагнет скв
                                if (w.ChessInjLoaded)
                                {
                                    chess_len = w.chessInj.Count;
                                    bw.Write(chess_len);
                                    for (h = 0; h < chess_len; h++)
                                    {
                                        dayInjItem = w.chessInj[h];
                                        bw.Write(RdfSystem.PackDateTime(dayInjItem.Date));
                                        bw.Write(dayInjItem.Wwat);
                                        bw.Write(dayInjItem.Pust);
                                        bw.Write(dayInjItem.CycleTime);
                                        bw.Write(dayInjItem.StayTime);
                                        bw.Write(dayInjItem.StayReason);
                                    }
                                    //bw.Write(0);
                                }
                                else
                                {
                                    bw.Write(0);
                                    //bw.Write(0);
                                }
                                #endregion
                            }
                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                //packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();

                                ms.SetLength(0);

                                GC.Collect(); //new code
                                GC.WaitForPendingFinalizers();
                                k = i;

                                w = (Well)Wells[i];
                                len_shift = 8;
                                if ((w.ChessLoaded) && (w.chess != null))
                                {
                                    len_shift += 4 + w.chess.Count * lenDailyItem;
                                }
                                else
                                    len_shift += 4;
                                if ((w.ChessInjLoaded) && (w.chessInj != null))
                                {
                                    len_shift += 4 + w.chessInj.Count * lenDailyInjItem;
                                }
                                else
                                    len_shift += 4;

                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    unpackSizes.Add((int)ms.Length);
                    //packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearChessData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if (w.ChessLoaded)
                    {
                        if (w.chess != null) w.chess.Clear();
                        w.chess = null;
                    }
                    if (w.ChessInjLoaded)
                    {
                        if (w.chessInj != null) w.chessInj.Clear();
                        w.chessInj = null;
                    }
                }
                if (useGC)
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            this.ChessLoaded = false;
        }
        #endregion

        #region CORE
        public bool LoadCoreFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadCoreFromCache(worker, e, false);
        }
        public bool LoadCoreFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            if (project.IsSmartProject) return LoadCoreFromCache2(worker, e, NotLoadData);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных керна месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\core.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, iLen, well_index, core_count;
                    long shift;
                    long[] shiftList;
                    Well w;
                    SkvGisItem item;

                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);

                        BinaryReader bfr = new BinaryReader(fs);
                        iLen = bfr.ReadInt32();
                        shiftList = new long[iLen];
                        for (i = 0; i < iLen; i++)
                        {
                            shiftList[i] = bfr.ReadInt64();
                        }

                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                for (j = 0; j < this.Wells.Count; j++)
                                {
                                    w = (Well)Wells[j];
                                    w.core = null;
                                }
                                return false;
                            }
                            shift = shiftList[i] - fs.Position;
                            if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                            well_index = bfr.ReadInt32();
                            core_count = bfr.ReadInt32();

                            if ((well_index > -1) && (core_count > 0))
                            {
                                w = (Well)Wells[well_index];
                                if (w.core == null) w.core = new SkvCore();
                                if (!NotLoadData)
                                {
                                    SkvCoreItem[] items = new SkvCoreItem[core_count];
                                    for (j = 0; j < core_count; j++)
                                    {
                                        items[j] = w.core.RetrieveItem(bfr);
                                    }
                                    w.core.SetItems(items);
                                }
                            }
                            retValue = true;
                            ReportProgress(worker, i + 1, iLen, userState);
                        }
                        if (iLen == 0) worker.ReportProgress(100, userState);
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша керна!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadCoreFromCache(int WellIndex)
        {
            return LoadCoreFromCache(WellIndex, false);
        }
        public bool LoadCoreFromCache(int WellIndex, bool NotLoadData)
        {
            if (project.IsSmartProject) return LoadCoreFromCache2(WellIndex, NotLoadData);
            bool retValue = false;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\core.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, core_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    iLen = bfr.ReadInt32();
                    fs.Seek(8 * WellIndex, SeekOrigin.Current);
                    shift = bfr.ReadInt64();
                    fs.Seek(shift, SeekOrigin.Begin);
                    well_index = bfr.ReadInt32();
                    if (WellIndex != well_index)
                    {
                        MessageBox.Show("Не найден индекс скважины в кеше!");
                    }
                    else if (well_index > -1)
                    {
                        w = (Well)Wells[well_index];
                        core_count = bfr.ReadInt32();
                        if ((!NotLoadData) && (core_count > 0))
                        {
                            if (w.core == null) w.core = new SkvCore();
                            SkvCoreItem[] items = new SkvCoreItem[core_count];

                            for (j = 0; j < core_count; j++)
                            {
                                items[j] = w.core.RetrieveItem(bfr);
                            }
                            w.core.SetItems(items);
                        }
                        retValue = true;
                    }
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteCoreToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных керна в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\core.bin";
                Well w;
                FileStream fs;
                BinaryWriter bfw;
                int i, j, core_len;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    bfw = new BinaryWriter(fs);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current);
                    long[] shiftList = new long[iLen];

                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        shiftList[i] = fs.Position;
                        bfw.Write(w.Index);
                        if (w.CoreLoaded)
                        {
                            core_len = w.core.Count;
                            bfw.Write(core_len);
                            for (j = 0; j < core_len; j++)
                            {
                                w.core.RetainItem(bfw, j);
                            }
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftList.Length > 0)
                    {
                        fs.Seek(4, SeekOrigin.Begin);
                        for (i = 0; i < shiftList.Length; i++)
                        {
                            bfw.Write(shiftList[i]);
                        }
                    }
                    bfw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearCoreData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    w.core = null;
                }
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
            }
        }
        #endregion

        #region CORE TEST
        public bool LoadCoreTestFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadCoreTestFromCache(worker, e, false);
        }
        public bool LoadCoreTestFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            if (project.IsSmartProject) return LoadCoreTestFromCache2(worker, e, NotLoadData);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных исследований керна месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\core_test.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, pack_len, core_test_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    SkvGisItem item;
                    int maxPackLen = 0, maxUnPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        iLen = bfr.ReadInt32();
                        shiftList = new long[iLen];
                        for (i = 0; i < iLen; i++)
                        {
                            shiftList[i] = bfr.ReadInt64();
                        }

                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                for (j = 0; j < this.Wells.Count; j++)
                                {
                                    w = (Well)Wells[j];
                                    w.coreTest = null;
                                }
                                return false;
                            }
                            shift = shiftList[i] - fs.Position;
                            if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                            well_index = bfr.ReadInt32();
                            pack_len = bfr.ReadInt32();
                            if (maxPackLen < pack_len) maxPackLen = pack_len;
                            if ((well_index > -1) && (pack_len > 0))
                            {
                                w = (Well)Wells[well_index];
                                ms = this.GetBuffer(true);
                                unpackMS = this.GetBuffer(false);
                                ms.SetLength(pack_len);
                                br = new BinaryReader(unpackMS);
                                ConvertEx.CopyStream(fs, ms, pack_len);

                                ConvertEx.UnPackStream(ms, ref unpackMS);
                                core_test_count = br.ReadInt32();
                                
                                if ((!NotLoadData) && (core_test_count > 0))
                                {
                                    if (w.coreTest == null) w.coreTest = new SkvCoreTest();
                                    SkvCoreTestItem[] items = new SkvCoreTestItem[core_test_count];

                                    for (j = 0; j < core_test_count; j++)
                                    {
                                        items[j] = w.coreTest.RetrieveItem(br);
                                    }
                                    w.coreTest.SetItems(items);
                                }
                            }
                            retValue = true;
                            ReportProgress(worker, i + 1, iLen, userState);
                        }
                        FreeBuffers();
                        if (iLen == 0) worker.ReportProgress(100, userState);
                        bfr.Close();
                        br = null;
                        fs = null;
                    }
                    catch (Exception)
                    {
                        FreeBuffers();
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша исследований керна!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadCoreTestFromCache(int WellIndex)
        {
            return LoadCoreTestFromCache(WellIndex, false);
        }
        public bool LoadCoreTestFromCache(int WellIndex, bool NotLoadData)
        {
            if (project.IsSmartProject) return LoadCoreTestFromCache2(WellIndex, NotLoadData);
            bool retValue = false;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\core_test.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, pack_len, core_test_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);

                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    iLen = bfr.ReadInt32();
                    fs.Seek(8 * WellIndex, SeekOrigin.Current);
                    shift = bfr.ReadInt64();
                    fs.Seek(shift, SeekOrigin.Begin);
                    well_index = bfr.ReadInt32();
                    pack_len = bfr.ReadInt32();
                    if (WellIndex != well_index)
                    {
                        MessageBox.Show("Не найден индекс скважины в кеше!");
                    }
                    else if ((well_index > -1) && (pack_len > 0))
                    {
                        w = (Well)Wells[well_index];
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(pack_len);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, pack_len);
                        ConvertEx.UnPackStream(ms, ref unpackMS);
                        core_test_count = br.ReadInt32();
                        if ((!NotLoadData) && (core_test_count > 0))
                        {
                            if (w.coreTest == null) w.coreTest = new SkvCoreTest();
                            SkvCoreTestItem[] items = new SkvCoreTestItem[core_test_count];

                            for (j = 0; j < core_test_count; j++)
                            {
                                items[j] = w.coreTest.RetrieveItem(br);
                            }
                            w.coreTest.SetItems(items);
                        }
                    }
                    FreeBuffers();
                    bfr.Close();
                    br = null;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteCoreTestToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных исследований керна в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                int i, j;
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\core_test.bin";
                Well w;
                FileStream fs;
                int core_test_count, iLen = Wells.Count;

                if (iLen > 0)
                {
                    long[] shiftWells = new long[iLen];
                    MemoryStream ms = GetBuffer(true);
                    MemoryStream packedMS;
                    BinaryWriter bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    BinaryWriter bfw = new BinaryWriter(fs);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current); // сдвигаемся на начало данных
                    shiftWells[0] = fs.Position;

                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        ClearMemoryStream(ms);
                        if (i > 0)
                        {
                            shiftWells[i] = fs.Position;
                        }
                        bfw.Write(w.Index);

                        if (w.CoreTestLoaded)
                        {
                            core_test_count = w.coreTest.Count;
                            bw.Write(core_test_count);
                            for (j = 0; j < core_test_count; j++)
                            {
                                w.coreTest.RetainItem(bw, j);
                            }
                            w.coreTest = null;
                            packedMS = ConvertEx.PackStream(ms);
                            bfw.Write((int)packedMS.Length);
                            packedMS.Seek(0, SeekOrigin.Begin);
                            packedMS.WriteTo(fs);
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftWells.Length > 0)
                    {
                        fs.Seek(4, SeekOrigin.Begin);
                        for (i = 0; i < shiftWells.Length; i++)
                        {
                            bfw.Write(shiftWells[i]);
                        }
                    }
                    FreeBuffers();
                    bfw.Close();
                    bw = null;
                    shiftWells = null;
                    GC.GetTotalMemory(true);
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearCoreTestData(bool useGC)
        {
            int i, j, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    w.coreTest = null;
                }
            }
            if (useGC) GC.GetTotalMemory(true);
        }
        #endregion

        #region SUM MER
        public bool ReCalcSumParams(BackgroundWorker worker, DoWorkEventArgs e, bool ClearMerData)
        {
            bool retValue = false;
            bool res = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Пересчет суммарных показателей по МЭР";
            userState.Element = this.Name;
            if ((this.Wells.Count > 0) && (this.MerLoaded || this.LoadMerFromCache(worker, e)))
            {
                if (this.MerStratumCodes.Count == 0)
                {
                    if (!this.LoadStratumCodesFromCache()) return false;
                }
                int i, j, k;
                DateTime minDate = DateTime.MaxValue;
                DateTime maxDate = DateTime.MinValue;
                Well w;
                MerItem item;
                DateTime[] minMaxDate = new DateTime[this.MerStratumCodes.Count * 2];
                for (k = 0; k < this.MerStratumCodes.Count; k++)
                {
                    minMaxDate[2 * k] = minDate;
                    minMaxDate[2 * k + 1] = maxDate;
                }
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    if ((w.MerLoaded) && (w.mer.Count > 0))
                    {
                        for (j = 0; j < w.mer.Count; j++)
                        {
                            item = w.mer.Items[j];
                            for (k = 0; k < this.MerStratumCodes.Count; k++)
                            {
                                if (item.Date < minDate) minDate = item.Date;
                                if (item.Date > maxDate) maxDate = item.Date;
                                if (item.PlastId == this.MerStratumCodes[k])
                                {
                                    if (item.Date < minMaxDate[2 * k]) minMaxDate[2 * k] = item.Date;
                                    if (item.Date > minMaxDate[2 * k + 1]) minMaxDate[2 * k + 1] = item.Date;
                                }
                            }
                        }
                    }
                }
                if ((minDate != DateTime.MaxValue) && (maxDate != DateTime.MinValue))
                {
                    int c;
                    int month, years;
                    bool bmonth, bProd, bInj, bScoop, bWorkTime, bAllMonth, bAllProd, bAllInj, bAllScoop;
                    DateTime dt;
                    years = maxDate.Year - minDate.Year + 1;
                    month = (years - 2) * 12 + (13 - minDate.Month) + maxDate.Month;

                    if (month > 0)
                    {
                        this.sumParams = new SumParameters(this.MerStratumCodes.Count + 1);

                        SumParamItem[] SumMonth = new SumParamItem[month];
                        SumParamItem[] SumYear = new SumParamItem[years];

                        dt = minDate;
                        for (i = 0; i < month; i++)
                        {
                            SumMonth[i].Date = dt;
                            SumMonth[i].InjGas = -1;
                            SumMonth[i].NatGas = -1;
                            SumMonth[i].GasCondensate = -1;
                            dt = dt.AddMonths(1);
                        }
                        dt = new DateTime(minDate.Year, 01, 01);
                        for (i = 0; i < years; i++)
                        {
                            SumYear[i].Date = dt;
                            SumYear[i].InjGas = -1;
                            SumYear[i].NatGas = -1;
                            SumYear[i].GasCondensate = -1;
                            dt = dt.AddYears(1);
                        }

                        this.sumParams.Add(-1, SumMonth, SumYear);

                        for (k = 0; k < this.MerStratumCodes.Count; k++)
                        {
                            years = minMaxDate[2 * k + 1].Year - minMaxDate[2 * k].Year + 1;
                            month = (years - 2) * 12 + (13 - minMaxDate[2 * k].Month) + minMaxDate[2 * k + 1].Month;
                            SumMonth = new SumParamItem[month];
                            SumYear = new SumParamItem[years];

                            dt = minMaxDate[2 * k];
                            for (i = 0; i < month; i++)
                            {
                                SumMonth[i].Date = dt;
                                SumMonth[i].InjGas = -1;
                                SumMonth[i].NatGas = -1;
                                SumMonth[i].GasCondensate = -1;
                                dt = dt.AddMonths(1);
                            }
                            dt = new DateTime(minMaxDate[2 * k].Year, 01, 01);

                            for (i = 0; i < years; i++)
                            {
                                SumYear[i].Date = dt;
                                SumYear[i].InjGas = -1;
                                SumYear[i].NatGas = -1;
                                SumYear[i].GasCondensate = -1;
                                dt = dt.AddYears(1);
                            }
                            this.sumParams.Add(this.MerStratumCodes[k], SumMonth, SumYear);
                        }

                        int indMonth, indYears, predIndMonth = -1, predIndYears = -1, predAllIndMonth = -1, predAllIndYears = -1;
                        double[] timeProd2 = new double[sumParams.Count];
                        double[] timeInj2 = new double[sumParams.Count];
                        double[] timeProdYear2 = new double[sumParams.Count];
                        double[] timeInjYear2 = new double[sumParams.Count];
                        int[][] lastYearProd = new int[sumParams.Count][];
                        for (i = 0; i < lastYearProd.Length; i++)
                        {
                            lastYearProd[i] = new int[2];
                        }

                        int iLen = Wells.Count, objIndex;
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return false;
                            }
                            w = (Well)Wells[i];
                            if (w.MerLoaded)
                            {
                                int hours;
                                MerCompItem compItem;
                                MerCompPlastItem plastItem;
                                MerWorkTimeItem wtItemProd, wtItemInj, wtItemScoop;
                                MerComp merComp = new MerComp(w.mer);
                                for (j = 0; j < lastYearProd.Length; j++)
                                {
                                    lastYearProd[j][0] = 0;
                                    lastYearProd[j][1] = 0;
                                }
                                for (j = 0; j < merComp.Count; j++)
                                {
                                    compItem = merComp[j];

                                    #region заполняем по суммарному мэр - объект All
                                    SumMonth = sumParams[0].MonthlyItems;
                                    SumYear = sumParams[0].YearlyItems;
                                    indMonth = (compItem.Date.Year - minDate.Year) * 12 + compItem.Date.Month - minDate.Month;

                                    wtItemProd = compItem.TimeItems.GetAllProductionTime();
                                    wtItemInj = compItem.TimeItems.GetAllInjectionTime();
                                    wtItemScoop = compItem.TimeItems.GetAllScoopTime();
                                    hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                    wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                    wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                    wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                    if (wtItemProd.WorkTime > 0)
                                    {
                                        SumMonth[indMonth].Fprod++;
                                        SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                    }

                                    if (wtItemInj.WorkTime > 0)
                                    {
                                        SumMonth[indMonth].Finj++;
                                        SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                        if (compItem.SumInjectionGas > 0) SumMonth[indMonth].WorkTimeInjGas += wtItemInj.WorkTime;
                                    }

                                    if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                    for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                    {
                                        switch (compItem.CharWorkIds[c])
                                        {
                                            case 11:
                                                SumMonth[indMonth].Liq += compItem.SumLiquid;
                                                SumMonth[indMonth].Oil += compItem.SumOil;
                                                if (SumMonth[indMonth].Liq > 0)
                                                {
                                                    SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                }

                                                SumMonth[indMonth].LiqV += compItem.SumLiquidV;
                                                SumMonth[indMonth].OilV += compItem.SumOilV;
                                                if (SumMonth[indMonth].LiqV > 0)
                                                {
                                                    SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                }
                                                break;
                                            case 12:
                                                if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                SumMonth[indMonth].NatGas += compItem.SumNaturalGas;
                                                break;
                                            case 13:
                                                SumMonth[indMonth].Scoop += compItem.SumScoop;
                                                break;
                                            case 15:
                                                if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                SumMonth[indMonth].GasCondensate += compItem.SumGasCondensate;
                                                break;
                                            case 20:
                                                SumMonth[indMonth].Inj += compItem.SumInjection;
                                                if (merComp.UseInjGas)
                                                {
                                                    if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                    SumMonth[indMonth].InjGas += compItem.SumInjectionGas;
                                                }
                                                break;
                                        }
                                    }
                                    // по годам
                                    indYears = compItem.Date.Year - minDate.Year;
                                    if (wtItemProd.WorkTime > 0)
                                    {
                                        if (lastYearProd[0][0] != compItem.Date.Year)
                                        {
                                            SumYear[indYears].Fprod++;
                                            lastYearProd[0][0] = compItem.Date.Year;
                                        }
                                        SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                    }
                                    if (wtItemInj.WorkTime > 0)
                                    {
                                        if (lastYearProd[0][1] != compItem.Date.Year)
                                        {
                                            SumYear[indYears].Finj++;
                                            lastYearProd[0][1] = compItem.Date.Year;
                                        }
                                        SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                        if (compItem.SumInjectionGas > 0) SumYear[indYears].WorkTimeInjGas += wtItemInj.WorkTime;
                                    }
                                    if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                    for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                    {
                                        switch (compItem.CharWorkIds[c])
                                        {
                                            case 11:
                                                SumYear[indYears].Liq += compItem.SumLiquid;
                                                SumYear[indYears].Oil += compItem.SumOil;
                                                if (SumYear[indYears].Liq > 0)
                                                {
                                                    SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                }
                                                SumYear[indYears].LiqV += compItem.SumLiquidV;
                                                SumYear[indYears].OilV += compItem.SumOilV;
                                                if (SumYear[indYears].LiqV > 0)
                                                {
                                                    SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                }
                                                break;
                                            case 12:
                                                if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                SumYear[indYears].NatGas += compItem.SumNaturalGas;
                                                break;
                                            case 13:
                                                SumYear[indYears].Scoop += compItem.SumScoop;
                                                break;
                                            case 15:
                                                if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                SumYear[indYears].GasCondensate += compItem.SumGasCondensate;
                                                break;
                                            case 20:
                                                SumYear[indYears].Inj += compItem.SumInjection;
                                                if (merComp.UseInjGas)
                                                {
                                                    if (SumYear[indYears].InjGas < 0) SumYear[indYears].InjGas = 0;
                                                    SumYear[indYears].InjGas += compItem.SumInjectionGas;
                                                }
                                                break;
                                        }
                                    }
                                    #endregion

                                    #region По объектам

                                    for (k = 0; k < compItem.PlastItems.Count; k++)
                                    {
                                        plastItem = compItem.PlastItems[k];
                                        objIndex = sumParams.GetIndexByObjCode(plastItem.PlastCode);
                                        if (objIndex > -1)
                                        {
                                            SumMonth = sumParams[objIndex].MonthlyItems;
                                            SumYear = sumParams[objIndex].YearlyItems;

                                            wtItemProd = compItem.TimeItems.GetProductionTime(plastItem.PlastCode);
                                            wtItemInj = compItem.TimeItems.GetInjectionTime(plastItem.PlastCode);
                                            wtItemScoop = compItem.TimeItems.GetScoopTime(plastItem.PlastCode);
                                            hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                            wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                            wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                            wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                            indMonth = (compItem.Date.Year - minMaxDate[2 * objIndex - 2].Year) * 12 + compItem.Date.Month - minMaxDate[2 * objIndex - 2].Month;

                                            if (wtItemProd.WorkTime > 0)
                                            {
                                                SumMonth[indMonth].Fprod++;
                                                SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                            }

                                            if (wtItemInj.WorkTime > 0)
                                            {
                                                SumMonth[indMonth].Finj++;
                                                SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                                if (plastItem.InjGas > 0) SumMonth[indMonth].WorkTimeInjGas += wtItemInj.WorkTime;
                                            }

                                            if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;


                                            for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                            {
                                                switch (compItem.CharWorkIds[c])
                                                {
                                                    case 11:
                                                        SumMonth[indMonth].Liq += plastItem.Liq;
                                                        SumMonth[indMonth].Oil += plastItem.Oil;
                                                        if (SumMonth[indMonth].Liq > 0)
                                                        {
                                                            SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                        }
                                                        SumMonth[indMonth].LiqV += plastItem.LiqV;
                                                        SumMonth[indMonth].OilV += plastItem.OilV;
                                                        if (SumMonth[indMonth].LiqV > 0)
                                                        {
                                                            SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                        }
                                                        break;
                                                    case 12:
                                                        if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                        SumMonth[indMonth].NatGas += plastItem.NatGas;
                                                        break;
                                                    case 13:
                                                        SumMonth[indMonth].Scoop += plastItem.Scoop;
                                                        break;
                                                    case 15:
                                                        if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                        SumMonth[indMonth].GasCondensate += plastItem.Condensate;
                                                        break;
                                                    case 20:
                                                        SumMonth[indMonth].Inj += plastItem.Inj;
                                                        if (merComp.UseInjGas)
                                                        {
                                                            if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                            SumMonth[indMonth].InjGas += plastItem.InjGas;
                                                        }
                                                        break;
                                                }
                                            }
                                            // по годам
                                            indYears = compItem.Date.Year - minMaxDate[2 * objIndex - 2].Year;
                                            if (wtItemProd.WorkTime > 0)
                                            {
                                                if (lastYearProd[objIndex][0] != compItem.Date.Year)
                                                {
                                                    SumYear[indYears].Fprod++;
                                                    lastYearProd[objIndex][0] = compItem.Date.Year;
                                                }
                                                SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                            }
                                            if (wtItemInj.WorkTime > 0)
                                            {
                                                if (lastYearProd[objIndex][1] != compItem.Date.Year)
                                                {
                                                    SumYear[indYears].Finj++;
                                                    lastYearProd[objIndex][1] = compItem.Date.Year;
                                                }
                                                SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                                if (plastItem.InjGas > 0) SumYear[indYears].WorkTimeInjGas += wtItemInj.WorkTime;
                                            }
                                            if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                            for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                            {
                                                switch (compItem.CharWorkIds[c])
                                                {
                                                    case 11:
                                                        SumYear[indYears].Liq += plastItem.Liq;
                                                        SumYear[indYears].Oil += plastItem.Oil;
                                                        if (SumYear[indYears].Liq > 0)
                                                        {
                                                            SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                        }
                                                        SumYear[indYears].LiqV += plastItem.LiqV;
                                                        SumYear[indYears].OilV += plastItem.OilV;
                                                        if (SumYear[indYears].LiqV > 0)
                                                        {
                                                            SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                        }
                                                        break;
                                                    case 12:
                                                        if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                        SumYear[indYears].NatGas += plastItem.NatGas;
                                                        break;
                                                    case 13:
                                                        SumYear[indYears].Scoop += plastItem.Scoop;
                                                        break;
                                                    case 15:
                                                        if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                        SumYear[indYears].GasCondensate += plastItem.Condensate;
                                                        break;
                                                    case 20:
                                                        SumYear[indYears].Inj += plastItem.Inj;
                                                        if (merComp.UseInjGas)
                                                        {
                                                            if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                            SumYear[indYears].InjGas += plastItem.InjGas;
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                        retValue = true;
                    }
                }
            }
            if (retValue)
            {
                ReCalcDeltaBP(worker, e);
            }
            if (ClearMerData) this.ClearMerData(false);
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadSumParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadSumParamsFromCache(worker, e, false);
        }
        public bool LoadSumParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Чтение суммарных показателей из кэша";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\sum.bin";
            if (File.Exists(cacheName))
            {
                BinaryReader bfr = null;
                try
                {
                    int i, j, k, lenMonth, lenYears, len_unpack, lenItems, objCode;
                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    bfr = new BinaryReader(fs);
                    if (SumParameters.Version == bfr.ReadInt32())
                    {
                        len_unpack = bfr.ReadInt32();

                        MemoryStream ms = new MemoryStream(len_unpack);
                        ConvertEx.CopyStream(fs, ms, len_unpack);
                        bfr.Close();
                        MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                        ms.Dispose();
                        BinaryReader br = new BinaryReader(unpackMS);

                        lenItems = br.ReadInt32();
                        this.sumParams = new SumParameters(lenItems + 1);
                        for (k = 0; k < lenItems; k++)
                        {
                            objCode = br.ReadInt32();
                            lenMonth = br.ReadInt32();
                            SumParamItem[] SumMonth = new SumParamItem[lenMonth];
                            for (i = 0; i < lenMonth; i++)
                            {
                                if ((worker != null) && (worker.CancellationPending))
                                {
                                    e.Cancel = true;
                                    br.Close();
                                    return false;
                                }
                                else
                                {
                                    SumMonth[i].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                    SumMonth[i].Liq = br.ReadDouble();
                                    SumMonth[i].Oil = br.ReadDouble();
                                    SumMonth[i].Watering = br.ReadDouble();
                                    SumMonth[i].LiqV = br.ReadDouble();
                                    SumMonth[i].OilV = br.ReadDouble();
                                    SumMonth[i].WateringV = br.ReadDouble();
                                    SumMonth[i].Inj = br.ReadDouble();
                                    SumMonth[i].InjGas = br.ReadDouble();
                                    SumMonth[i].NatGas = br.ReadDouble();
                                    SumMonth[i].GasCondensate = br.ReadDouble();
                                    SumMonth[i].Scoop = br.ReadDouble();
                                    SumMonth[i].WorkTimeProd = br.ReadDouble();
                                    SumMonth[i].WorkTimeInj = br.ReadDouble();
                                    SumMonth[i].WorkTimeInjGas = br.ReadDouble();
                                    SumMonth[i].Fprod = br.ReadInt32();
                                    SumMonth[i].Finj = br.ReadInt32();
                                    SumMonth[i].Fscoop = br.ReadInt32();
                                }
                            }

                            lenYears = br.ReadInt32();
                            SumParamItem[] SumYears = new SumParamItem[lenYears];

                            for (i = 0; i < lenYears; i++)
                            {
                                if ((worker != null) && (worker.CancellationPending))
                                {
                                    e.Cancel = true;
                                    br.Close();
                                    return false;
                                }
                                else
                                {
                                    SumYears[i].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                    SumYears[i].Liq = br.ReadDouble();
                                    SumYears[i].Oil = br.ReadDouble();
                                    SumYears[i].Watering = br.ReadDouble();
                                    SumYears[i].LiqV = br.ReadDouble();
                                    SumYears[i].OilV = br.ReadDouble();
                                    SumYears[i].WateringV = br.ReadDouble();
                                    SumYears[i].Inj = br.ReadDouble();
                                    SumYears[i].InjGas = br.ReadDouble();
                                    SumYears[i].NatGas = br.ReadDouble();
                                    SumYears[i].GasCondensate = br.ReadDouble();
                                    SumYears[i].Scoop = br.ReadDouble();
                                    SumYears[i].WorkTimeProd = br.ReadDouble();
                                    SumYears[i].WorkTimeInj = br.ReadDouble();
                                    SumYears[i].WorkTimeInjGas = br.ReadDouble();
                                    SumYears[i].Fprod = br.ReadInt32();
                                    SumYears[i].Finj = br.ReadInt32();
                                    SumYears[i].Fscoop = br.ReadInt32();
                                }
                            }
                            this.sumParams.Add(objCode, SumMonth, SumYears);
                            retValue = true;
                        }
                    }
                }
                catch
                {
                    retValue = false;
                }
                if (bfr != null) bfr.Close();
            }
            if (worker != null && !NotReportProgress) worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteSumParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись суммарных показателей в кэш";
            userState.Element = this.Name;

            if ((sumParams != null) && (sumParams.Count > 0))
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\sum.bin";

                int i, j, k, lenItem = 56, lenMonth, lenYears;
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(sumParams.Count);
                for (k = 0; k < sumParams.Count; k++)
                {
                    SumParamItem[] SumMonth = sumParams[k].MonthlyItems;
                    SumParamItem[] SumYears = sumParams[k].YearlyItems;
                    lenMonth = SumMonth.Length;
                    lenYears = SumYears.Length;

                    bw.Write(sumParams[k].OilObjCode);
                    bw.Write(SumMonth.Length);
                    for (i = 0; i < lenMonth; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            bw.Close();
                            return false;
                        }
                        else
                        {
                            bw.Write(RdfSystem.PackDateTime(SumMonth[i].Date));
                            bw.Write(SumMonth[i].Liq);
                            bw.Write(SumMonth[i].Oil);
                            bw.Write(SumMonth[i].Watering);
                            bw.Write(SumMonth[i].LiqV);
                            bw.Write(SumMonth[i].OilV);
                            bw.Write(SumMonth[i].WateringV);
                            bw.Write(SumMonth[i].Inj);
                            bw.Write(SumMonth[i].InjGas);
                            bw.Write(SumMonth[i].NatGas);
                            bw.Write(SumMonth[i].GasCondensate);
                            bw.Write(SumMonth[i].Scoop);
                            bw.Write(SumMonth[i].WorkTimeProd);
                            bw.Write(SumMonth[i].WorkTimeInj);
                            bw.Write(SumMonth[i].WorkTimeInjGas);
                            bw.Write(SumMonth[i].Fprod);
                            bw.Write(SumMonth[i].Finj);
                            bw.Write(SumMonth[i].Fscoop);
                        }
                    }
                    bw.Write(SumYears.Length);
                    for (i = 0; i < lenYears; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            bw.Close();
                            return false;
                        }
                        else
                        {
                            bw.Write(RdfSystem.PackDateTime(SumYears[i].Date));
                            bw.Write(SumYears[i].Liq);
                            bw.Write(SumYears[i].Oil);
                            bw.Write(SumYears[i].Watering);
                            bw.Write(SumYears[i].LiqV);
                            bw.Write(SumYears[i].OilV);
                            bw.Write(SumYears[i].WateringV);
                            bw.Write(SumYears[i].Inj);
                            bw.Write(SumYears[i].InjGas);
                            bw.Write(SumYears[i].NatGas);
                            bw.Write(SumYears[i].GasCondensate);
                            bw.Write(SumYears[i].Scoop);
                            bw.Write(SumYears[i].WorkTimeProd);
                            bw.Write(SumYears[i].WorkTimeInj);
                            bw.Write(SumYears[i].WorkTimeInjGas);
                            bw.Write(SumYears[i].Fprod);
                            bw.Write(SumYears[i].Finj);
                            bw.Write(SumYears[i].Fscoop);
                        }
                    }
                }
                //MemoryStream packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                MemoryStream packedMS = ConvertEx.PackStream(ms);
                bw.Close();
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                bw = new BinaryWriter(fs);
                bw.Write(SumParameters.Version);
                bw.Write((int)packedMS.Length);
                packedMS.Seek(0, SeekOrigin.Begin);
                packedMS.WriteTo(fs);
                bw.Close();
                packedMS.Dispose();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public void ClearSumParams(bool useGC)
        {
            if (this.sumParams != null) this.sumParams.Clear();
            this.sumParams = null;
            if (useGC)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region SUM CHESS
        public bool ReCalcSumChessParams(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            bool res = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка суммарных показателей";
            userState.Element = this.Name;
            if (this.Wells.Count > 0)
            {
                if (!this.ChessLoaded) this.LoadChessFromCache(worker, e);
                int i, j;
                DateTime minDate, maxDate;
                Well w;
                ChessItem item;
                ChessInjItem itemInj;
                minDate = DateTime.MaxValue;
                maxDate = DateTime.MinValue;
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    if ((w.ChessLoaded) && (w.chess.Count > 0))
                    {
                        for (j = 0; j < 2; j++)
                        {
                            if (j == 0) item = w.chess[j];
                            else item = w.chess[w.chess.Count - 1];

                            if (item.Date < minDate) minDate = item.Date;
                            if ((item.Date > maxDate) && (item.Date <= DateTime.Now)) maxDate = item.Date;
                        }
                    }
                    if ((w.ChessInjLoaded) && (w.chessInj.Count > 0))
                    {
                        for (j = 0; j < 2; j++)
                        {
                            if (j == 0) itemInj = w.chessInj[j];
                            else itemInj = w.chessInj[w.chessInj.Count - 1];

                            if (itemInj.Date < minDate) minDate = itemInj.Date;
                            if ((itemInj.Date > maxDate) && (itemInj.Date <= DateTime.Now)) maxDate = itemInj.Date;
                        }
                    }
                }

                if ((minDate != DateTime.MaxValue) && (maxDate != DateTime.MinValue))
                {
                    int days;
                    DateTime dt;
                    TimeSpan ts = maxDate - minDate;
                    days = ts.Days + 1;
                    int indDay;
                    double predQliq;
                    DateTime predDT;

                    if (days > 0)
                    {
                        SumParamItem[] SumDay = new SumParamItem[days];

                        dt = minDate;
                        for (i = 0; i < days; i++)
                        {
                            SumDay[i].Date = dt;
                            dt = dt.AddDays(1);
                        }
                        int iLen = Wells.Count;
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return false;
                            }
                            else
                            {
                                w = (Well)Wells[i];
                                predDT = DateTime.Now;
                                predQliq = -1;
                                if (w.ChessLoaded)
                                {
                                    for (j = 0; j < w.chess.Count; j++)
                                    {
                                        item = w.chess[j];
                                        ts = item.Date - minDate;
                                        indDay = ts.Days;

                                        if ((indDay < SumDay.Length) && (SumDay[indDay].Date == item.Date))
                                        {
                                            if (((predQliq == -1) || (predDT != item.Date) || (predQliq != item.Qliq + item.Qoil)) && (item.Qliq + item.Qoil > 0))
                                            {
                                                if (item.StayTime < 24)
                                                {
                                                    SumDay[indDay].Liq += item.Qliq;// *(24 - item.StayTime) / 24;
                                                    SumDay[indDay].Oil += item.Qoil;// *(24 - item.StayTime) / 24;

                                                    SumDay[indDay].LiqV += item.QliqV;// *(24 - item.StayTime) / 24;
                                                    SumDay[indDay].OilV += item.QoilV;// *(24 - item.StayTime) / 24;

                                                    SumDay[indDay].Fprod++;
                                                }
                                                predDT = item.Date;
                                                predQliq = item.Qliq + item.Qoil;
                                            }
                                        }
                                        else if (indDay < SumDay.Length)
                                        {
                                            MessageBox.Show("Не совпадает дата!" + this.Name + "[" + w.UpperCaseName + "-" + item.Date + "]", "Загрузка суммарных показателей");
                                        }
                                    }
                                }
                                if (w.ChessInjLoaded)
                                {
                                    if (w.chessInj.Count > 0)
                                    {
                                        indDay = 0;
                                        j = 0;
                                        int chessLen = w.chessInj.Count - 1;
                                        itemInj = w.chessInj[0];
                                        while (indDay < SumDay.Length)
                                        {
                                            if ((j < chessLen) && (SumDay[indDay].Date == w.chessInj[j + 1].Date))
                                            {
                                                while ((j < chessLen) && (SumDay[indDay].Date == w.chessInj[j + 1].Date))
                                                {
                                                    j++;
                                                    itemInj = w.chessInj[j];
                                                }
                                                if ((itemInj.Wwat != -1) && (itemInj.StayTime != 24))
                                                {
                                                    SumDay[indDay].Inj += itemInj.Wwat;
                                                    SumDay[indDay].Finj++;
                                                }
                                            }
                                            else if (SumDay[indDay].Date >= itemInj.Date)
                                            {
                                                if ((itemInj.Wwat != -1) && (itemInj.StayTime != 24))
                                                {
                                                    SumDay[indDay].Inj += itemInj.Wwat;
                                                    SumDay[indDay].Finj++;
                                                }
                                            }
                                            indDay++;
                                        }
                                    }
                                }
                                w.chess = null;
                                w.chessInj = null;
                                worker.ReportProgress((int)((float)i * 100 / (float)iLen), userState);
                            }
                        }
                        this.ChessLoaded = false;
                        this.sumChessParams = new SumParameters(1);
                        this.sumChessParams.Add(-1, SumDay, null);

                        retValue = true;
                    }
                }
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadSumChessParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка суммарных показателей Шахматки из кэша";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\sum_chess.bin";
            if (File.Exists(cacheName))
            {
                BinaryReader bfr = null;
                try
                {
                    int i, j, lenMonth, lenYears, len_unpack;

                    FileStream fs = new FileStream(cacheName, FileMode.Open); ;
                    bfr = new BinaryReader(fs);
                    if (SumParameters.Version == bfr.ReadInt32())
                    {
                        len_unpack = bfr.ReadInt32();

                        MemoryStream ms = new MemoryStream(len_unpack);
                        ConvertEx.CopyStream(fs, ms, len_unpack);
                        bfr.Close();
                        MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                        //MemoryStream unpackMS = ConvertEx.UnPackStream(ms);
                        ms.Dispose();
                        BinaryReader br = new BinaryReader(unpackMS);

                        lenMonth = br.ReadInt32();
                        SumParamItem[] SumMonth = new SumParamItem[lenMonth];
                        for (i = 0; i < lenMonth; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                br.Close();
                                return false;
                            }
                            else
                            {
                                SumMonth[i].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                SumMonth[i].Liq = br.ReadDouble();
                                SumMonth[i].Oil = br.ReadDouble();
                                SumMonth[i].LiqV = br.ReadDouble();
                                SumMonth[i].OilV = br.ReadDouble();
                                SumMonth[i].Inj = br.ReadDouble();
                                //SumMonth[i].Scoop = br.ReadDouble();
                                SumMonth[i].Fprod = br.ReadInt32();
                                SumMonth[i].Finj = br.ReadInt32();
                                //SumMonth[i].Fscoop = br.ReadInt32();
                            }
                        }
                        this.sumChessParams = new SumParameters(1);
                        this.sumChessParams.Add(-1, SumMonth, null);
                        retValue = true;
                    }
                }
                catch
                {
                    retValue = false;
                }
                if (bfr != null) bfr.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteSumChessParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись суммарных показателей Шахматки в кэш";
            userState.Element = this.Name;

            if (sumChessParams != null)
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\sum_chess.bin";

                int i, j, lenItem = 32, lenMonth, lenYears;

                lenMonth = sumChessParams[0].MonthlyItems.Length;
                MemoryStream ms = new MemoryStream(8 + (lenMonth + 0) * lenItem);
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(lenMonth);
                for (i = 0; i < lenMonth; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        bw.Close();
                        return false;
                    }
                    else
                    {
                        bw.Write(RdfSystem.PackDateTime(sumChessParams[0].MonthlyItems[i].Date));
                        bw.Write(sumChessParams[0].MonthlyItems[i].Liq);
                        bw.Write(sumChessParams[0].MonthlyItems[i].Oil);
                        bw.Write(sumChessParams[0].MonthlyItems[i].LiqV);
                        bw.Write(sumChessParams[0].MonthlyItems[i].OilV);
                        bw.Write(sumChessParams[0].MonthlyItems[i].Inj);

                        bw.Write(sumChessParams[0].MonthlyItems[i].Fprod);
                        bw.Write(sumChessParams[0].MonthlyItems[i].Finj);

                    }
                }
                bw.Write(0);
                MemoryStream packedMS = ConvertEx.PackStream(ms);
                bw.Close();
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                bw = new BinaryWriter(fs);
                bw.Write(SumParameters.Version);
                bw.Write((int)packedMS.Length);
                packedMS.Seek(0, SeekOrigin.Begin);
                packedMS.WriteTo(fs);
                bw.Close();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public void ClearSumChessParams(bool useGC)
        {
            if (sumChessParams != null) sumChessParams.Clear();
            sumChessParams = null;
            if (useGC)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region SUM AREA
        public bool ReCalcSumAreaData(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка суммарных показателей";
            userState.Element = this.Name;
            var dictStratum = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                if (this.Areas.Count > 0)
                {
                    int i;
                    Area area;
                    if (!this.MerLoaded) this.LoadMerFromCache(worker, e);
                    if (this.MerLoaded)
                    {
                        for (i = 0; i < this.Areas.Count; i++)
                        {
                            area = (Area)Areas[i];
                            area.SumParamsReCalc(worker, e, dictStratum);
                        }
                    }
                    this.ClearMerData(false);
                    this.WriteSumAreaToCache(worker, e);
                    retValue = true;
                }
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadSumAreaFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка параметров ячеек заводнения..";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\areas_param.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, bl_ind, num_blocks, unpack_len_block, len_block, area_count, month_len, year_len, area_index = -1, iLen = 0, areaCount = 0;
                    int objCode, len;
                    Area area;
                    SumParamItem[] monthParams, yearParams;
                    MemoryStream ms;
                    worker.ReportProgress(0, userState);
                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    try
                    {
                        if (Area.SumVersion == bfr.ReadInt32())
                        {
                            num_blocks = bfr.ReadInt32();
                            iLen = bfr.ReadInt32();
                            for (bl_ind = 0; bl_ind < num_blocks; bl_ind++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return false;
                                }
                                len_block = bfr.ReadInt32();
                                unpack_len_block = bfr.ReadInt32();
                                area_count = bfr.ReadInt32();

                                ms = new MemoryStream(len_block);
                                if (ms == null) return false;
                                ConvertEx.CopyStream(fs, ms, len_block);
                                MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                                ms.Dispose();

                                BinaryReader br = new BinaryReader(unpackMS);
                                unpackMS.Seek(4 * area_count, SeekOrigin.Begin);
                                for (i = 0; i < area_count; i++)
                                {
                                    area_index = br.ReadInt32();
                                    if (worker.CancellationPending)
                                    {
                                        e.Cancel = true;
                                        this.ClearSumArea(false);
                                        br.Close();
                                        unpackMS.Dispose();
                                        return false;
                                    }
                                    if (area_index > -1)
                                    {
                                        area = (Area)Areas[area_index];
                                        month_len = br.ReadInt32();
                                        year_len = br.ReadInt32();
                                        if (month_len > 0)
                                        {
                                            monthParams = new SumParamItem[month_len];
                                            for (k = 0; k < month_len; k++)
                                            {
                                                monthParams[k].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                                monthParams[k].Liq = br.ReadDouble();
                                                monthParams[k].Oil = br.ReadDouble();
                                                monthParams[k].Watering = br.ReadDouble();
                                                monthParams[k].LiqV = br.ReadDouble();
                                                monthParams[k].OilV = br.ReadDouble();
                                                monthParams[k].WateringV = br.ReadDouble();
                                                monthParams[k].Inj = br.ReadDouble();
                                                monthParams[k].InjGas = br.ReadDouble();
                                                monthParams[k].NatGas = br.ReadDouble();
                                                monthParams[k].GasCondensate = br.ReadDouble();
                                                monthParams[k].Scoop = br.ReadDouble();
                                                monthParams[k].Fprod = br.ReadInt32();
                                                monthParams[k].Finj = br.ReadInt32();
                                                monthParams[k].Fscoop = br.ReadInt32();
                                                monthParams[k].LeadInAll = br.ReadInt32();
                                                monthParams[k].LeadInProd = br.ReadInt32();
                                                monthParams[k].WorkTimeProd = br.ReadDouble();
                                                monthParams[k].WorkTimeInj = br.ReadDouble();
                                            }
                                            yearParams = new SumParamItem[year_len];
                                            for (k = 0; k < year_len; k++)
                                            {
                                                yearParams[k].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                                yearParams[k].Liq = br.ReadDouble();
                                                yearParams[k].Oil = br.ReadDouble();
                                                yearParams[k].Watering = br.ReadDouble();
                                                yearParams[k].LiqV = br.ReadDouble();
                                                yearParams[k].OilV = br.ReadDouble();
                                                yearParams[k].WateringV = br.ReadDouble();
                                                yearParams[k].Inj = br.ReadDouble();
                                                yearParams[k].InjGas = br.ReadDouble();
                                                yearParams[k].NatGas = br.ReadDouble();
                                                yearParams[k].GasCondensate = br.ReadDouble();
                                                yearParams[k].Scoop = br.ReadDouble();
                                                yearParams[k].Fprod = br.ReadInt32();
                                                yearParams[k].Finj = br.ReadInt32();
                                                yearParams[k].Fscoop = br.ReadInt32();
                                                yearParams[k].LeadInAll = br.ReadInt32();
                                                yearParams[k].LeadInProd = br.ReadInt32();
                                                yearParams[k].WorkTimeProd = br.ReadDouble();
                                                yearParams[k].WorkTimeInj = br.ReadDouble();
                                            }
                                            area.sumObjParams = new SumObjParameters(area.PlastCode, monthParams, yearParams);
                                        }
                                        month_len = br.ReadInt32();
                                        if (month_len > 0)
                                        {
                                            monthParams = new SumParamItem[month_len];
                                            for (k = 0; k < month_len; k++)
                                            {
                                                monthParams[k].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                                monthParams[k].Liq = br.ReadDouble();
                                                monthParams[k].Oil = br.ReadDouble();
                                                monthParams[k].Watering = br.ReadDouble();
                                                monthParams[k].LiqV = br.ReadDouble();
                                                monthParams[k].OilV = br.ReadDouble();
                                                monthParams[k].WateringV = br.ReadDouble();
                                                monthParams[k].Inj = br.ReadDouble();
                                                monthParams[k].Scoop = br.ReadDouble();
                                                monthParams[k].Fprod = br.ReadInt32();
                                                monthParams[k].Finj = br.ReadInt32();
                                                monthParams[k].Fscoop = br.ReadInt32();
                                                monthParams[k].LeadInAll = br.ReadInt32();
                                                monthParams[k].LeadInProd = br.ReadInt32();
                                                monthParams[k].WorkTimeProd = br.ReadDouble();
                                                monthParams[k].WorkTimeInj = br.ReadDouble();
                                            }
                                            area.sumObjChessParams = new SumObjParameters(area.PlastCode, monthParams, null);
                                        }
                                    }
                                    retValue = true;
                                    ReportProgress(worker, areaCount + (i + 1), iLen, userState);
                                }
                                areaCount += area_count;
                                br.Close();
                                unpackMS.Dispose();
                            }
                        }
                    }
                    catch
                    {
                        retValue = false;
                    }
                    finally
                    {
                        bfr.Close();
                    }
                    if (iLen == 0) worker.ReportProgress(100, userState);
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша ЯЗ!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadSumAreaFromCache(int AreaIndex)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\areas_param.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, area_count, sum_len, area_index = -1, iLen, areaCount = 0;
                int monthLen, yearLen, chessLen;
                int shift;
                Area area;
                MemoryStream ms;
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader bfr = new BinaryReader(fs);
                SumParamItem[] monthParams, yearParams;
                try
                {
                    if (Area.SumVersion == bfr.ReadInt32())
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        areaCount = 0;
                        for (i = 0; i < num_blocks; i++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            area_count = bfr.ReadInt32();
                            if (AreaIndex < areaCount + area_count)
                            {
                                ms = new MemoryStream(unpack_len_block);
                                if (ms == null) return false;
                                ConvertEx.CopyStream(fs, ms, len_block);
                                MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                                ms.Dispose();
                                BinaryReader br = new BinaryReader(unpackMS);
                                shift = (AreaIndex - areaCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);

                                area_index = br.ReadInt32();
                                if (AreaIndex != area_index)
                                {
                                    bfr.Close();
                                    return false;
                                }
                                else if (area_index > -1)
                                {
                                    area = (Area)Areas[area_index];
                                    monthLen = br.ReadInt32();
                                    yearLen = br.ReadInt32();
                                    if (monthLen > 0)
                                    {
                                        monthParams = new SumParamItem[monthLen];
                                        yearParams = new SumParamItem[yearLen];
                                        for (k = 0; k < monthLen; k++)
                                        {
                                            monthParams[k].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                            monthParams[k].Liq = br.ReadDouble();
                                            monthParams[k].Oil = br.ReadDouble();
                                            monthParams[k].Watering = br.ReadDouble();
                                            monthParams[k].LiqV = br.ReadDouble();
                                            monthParams[k].OilV = br.ReadDouble();
                                            monthParams[k].WateringV = br.ReadDouble();
                                            monthParams[k].Inj = br.ReadDouble();
                                            monthParams[k].InjGas = br.ReadDouble();
                                            monthParams[k].NatGas = br.ReadDouble();
                                            monthParams[k].GasCondensate = br.ReadDouble();
                                            monthParams[k].Scoop = br.ReadDouble();
                                            monthParams[k].Fprod = br.ReadInt32();
                                            monthParams[k].Finj = br.ReadInt32();
                                            monthParams[k].Fscoop = br.ReadInt32();
                                            monthParams[k].LeadInAll = br.ReadInt32();
                                            monthParams[k].LeadInProd = br.ReadInt32();
                                            monthParams[k].WorkTimeProd = br.ReadDouble();
                                            monthParams[k].WorkTimeInj = br.ReadDouble();
                                            if (monthParams[k].LiqV > 0)
                                            {
                                                monthParams[k].LiqVb = monthParams[k].OilV * area.pvt.OilVolumeFactor + (monthParams[k].LiqV - monthParams[k].OilV) * area.pvt.WaterVolumeFactor;
                                                monthParams[k].AccumLiqVb = monthParams[k].LiqVb;
                                                if (k > 0) monthParams[k].AccumLiqVb += monthParams[k - 1].AccumLiqVb; 
                                            }
                                            monthParams[k].InjVb = monthParams[k].Inj * area.pvt.WaterVolumeFactor;
                                            monthParams[k].AccumInjVb = monthParams[k].InjVb;
                                            if (k > 0) monthParams[k].AccumInjVb += monthParams[k - 1].AccumInjVb;
                                        }
                                        for (k = 0; k < yearLen; k++)
                                        {
                                            yearParams[k].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                            yearParams[k].Liq = br.ReadDouble();
                                            yearParams[k].Oil = br.ReadDouble();
                                            yearParams[k].Watering = br.ReadDouble();
                                            yearParams[k].LiqV = br.ReadDouble();
                                            yearParams[k].OilV = br.ReadDouble();
                                            yearParams[k].WateringV = br.ReadDouble();
                                            yearParams[k].Inj = br.ReadDouble();
                                            yearParams[k].InjGas = br.ReadDouble();
                                            yearParams[k].NatGas = br.ReadDouble();
                                            yearParams[k].GasCondensate = br.ReadDouble();
                                            yearParams[k].Scoop = br.ReadDouble();
                                            yearParams[k].Fprod = br.ReadInt32();
                                            yearParams[k].Finj = br.ReadInt32();
                                            yearParams[k].Fscoop = br.ReadInt32();
                                            yearParams[k].LeadInAll = br.ReadInt32();
                                            yearParams[k].LeadInProd = br.ReadInt32();
                                            yearParams[k].WorkTimeProd = br.ReadDouble();
                                            yearParams[k].WorkTimeInj = br.ReadDouble();
                                            if (yearParams[k].LiqV > 0)
                                            {
                                                yearParams[k].LiqVb = yearParams[k].OilV * area.pvt.OilVolumeFactor + (yearParams[k].LiqV - yearParams[k].OilV) * area.pvt.WaterVolumeFactor;
                                                yearParams[k].AccumLiqVb = yearParams[k].LiqVb;
                                                if (k > 0) yearParams[k].AccumLiqVb += yearParams[k - 1].AccumLiqVb; 
                                            }
                                            yearParams[k].InjVb = yearParams[k].Inj * area.pvt.WaterVolumeFactor;
                                            yearParams[k].AccumInjVb = yearParams[k].InjVb;
                                            if (k > 0) yearParams[k].AccumInjVb += yearParams[k - 1].AccumInjVb;
                                        }
                                        area.sumObjParams = new SumObjParameters(area.PlastCode, monthParams, yearParams);
                                    }
                                    chessLen = br.ReadInt32();
                                    if (chessLen > 0)
                                    {
                                        monthParams = new SumParamItem[chessLen];
                                        for (k = 0; k < chessLen; k++)
                                        {
                                            monthParams[k].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                                            monthParams[k].Liq = br.ReadDouble();
                                            monthParams[k].Oil = br.ReadDouble();
                                            monthParams[k].Watering = br.ReadDouble();
                                            monthParams[k].LiqV = br.ReadDouble();
                                            monthParams[k].OilV = br.ReadDouble();
                                            monthParams[k].WateringV = br.ReadDouble();
                                            monthParams[k].Inj = br.ReadDouble();
                                            monthParams[k].Scoop = br.ReadDouble();
                                            monthParams[k].Fprod = br.ReadInt32();
                                            monthParams[k].Finj = br.ReadInt32();
                                            monthParams[k].Fscoop = br.ReadInt32();
                                            monthParams[k].LeadInAll = br.ReadInt32();
                                            monthParams[k].LeadInProd = br.ReadInt32();
                                            monthParams[k].WorkTimeProd = br.ReadDouble();
                                            monthParams[k].WorkTimeInj = br.ReadDouble();
                                        }
                                        area.sumObjChessParams = new SumObjParameters(area.PlastCode, monthParams, null);
                                    }
                                }
                                retValue = true;
                                br.Close();
                                break;
                            }
                            else
                            {
                                fs.Seek(len_block, SeekOrigin.Current);
                            }
                            areaCount += area_count;
                        }
                    }
                }
                catch
                {
                    retValue = false;
                }
                finally
                {
                    bfr.Close();
                }
            }
            return retValue;
        }
        public bool WriteSumAreaToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись параметров ячеек заводнения в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\areas_param.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 1024;
                Area area;
                SumParamItem sumItem;
                ArrayList msArr = new ArrayList();
                ArrayList areaCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList AreasShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                BinaryReader br;
                MemoryStream ms;
                BinaryWriter bw;

                int[] arr;
                int i, j, k, h, n, lenItemMer = 128, lenItemChess = 80, lenWrite = 0, len_all_areas, aCount = 0, len_block;
                int len_shift;
                int params_len;

                if (Areas.Count > 0)
                {
                    len_all_areas = 0;
                    for (i = 0; i < Areas.Count; i++)
                    {
                        area = (Area)Areas[i];
                        len_all_areas += 16; // Index + CountMer * 2 + CountChess
                        if (area.sumObjParams != null)
                        {
                            if (area.sumObjParams.MonthlyItems != null)
                            {
                                len_all_areas += area.sumObjParams.MonthlyItems.Length * lenItemMer;
                            }
                            if (area.sumObjParams.YearlyItems != null)
                            {
                                len_all_areas += area.sumObjParams.YearlyItems.Length * lenItemMer;
                            }
                        }
                        if ((area.sumObjChessParams != null) && (area.sumObjChessParams.MonthlyItems != null))
                        {
                            len_all_areas += area.sumObjChessParams.MonthlyItems.Length * lenItemChess;
                        }
                    }
                    if (len_all_areas > MAX_SIZE_PACK_BLOCK) len_all_areas = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_areas);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);

                    len_block = 0;
                    k = 0;
                    aCount = 0;

                    for (i = 0; i <= Areas.Count; i++)
                    {
                        len_shift = 0;
                        if (i < Areas.Count)
                        {
                            area = (Area)Areas[i];

                            len_shift = 16; // Index + CountMer * 2 + CountChess
                            if (area.sumObjParams != null)
                            {
                                if (area.sumObjParams.MonthlyItems != null)
                                {
                                    len_shift += area.sumObjParams.MonthlyItems.Length * lenItemMer;
                                }
                                if (area.sumObjParams.YearlyItems != null)
                                {
                                    len_shift += area.sumObjParams.YearlyItems.Length * lenItemMer;
                                }
                            }
                            if ((area.sumObjChessParams != null) && (area.sumObjChessParams.MonthlyItems != null))
                            {
                                len_shift += area.sumObjChessParams.MonthlyItems.Length * lenItemChess;
                            }
                        }
                        if ((len_block + len_shift <= len_all_areas) && (i < Areas.Count))
                        {
                            aCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * aCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                area = (Area)Areas[j];
                                len_shift += 16;// Index + CountMer * 2 + CountChess
                                if (area.sumObjParams != null)
                                {
                                    if (area.sumObjParams.MonthlyItems != null)
                                    {
                                        len_shift += area.sumObjParams.MonthlyItems.Length * lenItemMer;
                                    }
                                    if (area.sumObjParams.YearlyItems != null)
                                    {
                                        len_shift += area.sumObjParams.YearlyItems.Length * lenItemMer;
                                    }
                                }
                                if ((area.sumObjChessParams != null) && (area.sumObjChessParams.MonthlyItems != null))
                                {
                                    len_shift += area.sumObjChessParams.MonthlyItems.Length * lenItemChess;
                                }
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                area = (Area)Areas[j];
                                bw.Write(area.Index);
                                if ((area.sumObjParams != null) && (area.sumObjParams.MonthlyItems != null))
                                {
                                    bw.Write(area.sumObjParams.MonthlyItems.Length);
                                    bw.Write(area.sumObjParams.YearlyItems.Length);
                                    for (n = 0; n < area.sumObjParams.MonthlyItems.Length; n++)
                                    {
                                        sumItem = area.sumObjParams.MonthlyItems[n];

                                        bw.Write(RdfSystem.PackDateTime(sumItem.Date));
                                        bw.Write(sumItem.Liq);
                                        bw.Write(sumItem.Oil);
                                        bw.Write(sumItem.Watering);
                                        bw.Write(sumItem.LiqV);
                                        bw.Write(sumItem.OilV);
                                        bw.Write(sumItem.WateringV);
                                        bw.Write(sumItem.Inj);
                                        bw.Write(sumItem.InjGas);
                                        bw.Write(sumItem.NatGas);
                                        bw.Write(sumItem.GasCondensate);
                                        bw.Write(sumItem.Scoop);
                                        bw.Write(sumItem.Fprod);
                                        bw.Write(sumItem.Finj);
                                        bw.Write(sumItem.Fscoop);
                                        bw.Write(sumItem.LeadInAll);
                                        bw.Write(sumItem.LeadInProd);
                                        bw.Write(sumItem.WorkTimeProd);
                                        bw.Write(sumItem.WorkTimeInj);
                                    }
                                    for (n = 0; n < area.sumObjParams.YearlyItems.Length; n++)
                                    {
                                        sumItem = area.sumObjParams.YearlyItems[n];

                                        bw.Write(RdfSystem.PackDateTime(sumItem.Date));
                                        bw.Write(sumItem.Liq);
                                        bw.Write(sumItem.Oil);
                                        bw.Write(sumItem.Watering);
                                        bw.Write(sumItem.LiqV);
                                        bw.Write(sumItem.OilV);
                                        bw.Write(sumItem.WateringV);
                                        bw.Write(sumItem.Inj);
                                        bw.Write(sumItem.InjGas);
                                        bw.Write(sumItem.NatGas);
                                        bw.Write(sumItem.GasCondensate);
                                        bw.Write(sumItem.Scoop);
                                        bw.Write(sumItem.Fprod);
                                        bw.Write(sumItem.Finj);
                                        bw.Write(sumItem.Fscoop);
                                        bw.Write(sumItem.LeadInAll);
                                        bw.Write(sumItem.LeadInProd);
                                        bw.Write(sumItem.WorkTimeProd);
                                        bw.Write(sumItem.WorkTimeInj);
                                    }
                                }
                                else
                                {
                                    bw.Write(0);
                                    bw.Write(0);
                                }
                                if (area.sumObjChessParams != null && area.sumObjChessParams.MonthlyItems != null)
                                {
                                    bw.Write(area.sumObjChessParams.MonthlyItems.Length);
                                    for (n = 0; n < area.sumObjChessParams.MonthlyItems.Length; n++)
                                    {
                                        sumItem = area.sumObjChessParams.MonthlyItems[n];

                                        bw.Write(RdfSystem.PackDateTime(sumItem.Date));
                                        bw.Write(sumItem.Liq);
                                        bw.Write(sumItem.Oil);
                                        bw.Write(sumItem.Watering);
                                        bw.Write(sumItem.LiqV);
                                        bw.Write(sumItem.OilV);
                                        bw.Write(sumItem.WateringV);
                                        bw.Write(sumItem.Inj);
                                        bw.Write(sumItem.Scoop);
                                        bw.Write(sumItem.Fprod);
                                        bw.Write(sumItem.Finj);
                                        bw.Write(sumItem.Fscoop);
                                        bw.Write(sumItem.LeadInAll);
                                        bw.Write(sumItem.LeadInProd);
                                        bw.Write(sumItem.WorkTimeProd);
                                        bw.Write(sumItem.WorkTimeInj);
                                    }
                                }
                                else
                                {
                                    bw.Write(0);
                                }
                            }
                            unpackSizes.Add((int)ms.Length);
                            //packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                            packedMS = ConvertEx.PackStream(ms);
                            msArr.Add(packedMS);
                            areaCount.Add(aCount);
                            AreasShift.Clear();
                            if (i < Areas.Count)
                            {
                                ms.SetLength(0);
                                k = i;

                                area = (Area)Areas[i];

                                len_shift = 16;// Index + CountMer * 2 + CountChess
                                if (area.sumObjParams != null)
                                {
                                    if (area.sumObjParams.MonthlyItems != null)
                                    {
                                        len_shift += area.sumObjParams.MonthlyItems.Length * lenItemMer;
                                    }
                                    if (area.sumObjParams.YearlyItems != null)
                                    {
                                        len_shift += area.sumObjParams.YearlyItems.Length * lenItemMer;
                                    }
                                }
                                if ((area.sumObjChessParams != null) && (area.sumObjChessParams.MonthlyItems != null))
                                {
                                    len_shift += area.sumObjChessParams.MonthlyItems.Length * lenItemChess;
                                }
                                len_block = len_shift;
                                aCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, Areas.Count);
                    }
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(Area.SumVersion); // Version
                    bw.Write(msArr.Count);
                    bw.Write(Areas.Count);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)areaCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    areaCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearSumArea(bool useGC)
        {
            int i, j, len = Areas.Count;
            Area area;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    area = (Area)Areas[i];
                    if (area.sumObjParams != null)
                    {
                        area.sumObjParams.MonthlyItems = null;
                        area.sumObjParams.YearlyItems = null;
                        area.sumObjParams = null;
                    }

                    if (area.sumObjChessParams != null)
                    {
                        area.sumObjChessParams.MonthlyItems = null;
                        area.sumObjChessParams = null;
                    }
                }
                if (useGC)
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
        }
        #endregion

        #region TABLE 8.1
        public bool LoadTable81ParamsFromCacheOld(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (project.IsSmartProject) return LoadTable81ParamsFromCache2(worker, e);
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей Таблиц 8.1 из кэша";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\table81.bin";

            if (File.Exists(cacheName))
            {
                int i, j, lenYears, len_unpack;

                FileStream fs = new FileStream(cacheName, FileMode.Open); ;
                BinaryReader bfr = new BinaryReader(fs);
                if (Table81Params.Version == bfr.ReadInt32())
                {
                    len_unpack = bfr.ReadInt32();

                    MemoryStream ms = new MemoryStream(len_unpack);
                    ConvertEx.CopyStream(fs, ms, len_unpack);
                    bfr.Close();
                    MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                    //MemoryStream unpackMS = ConvertEx.UnPackStream(ms);
                    ms.Dispose();
                    BinaryReader br = new BinaryReader(unpackMS);

                    Table81.ProjectDocName = br.ReadString();
                    Table81.NewPtdYear = br.ReadInt32();
                    lenYears = br.ReadInt32();

                    SumParamItem[] SumYear = new SumParamItem[lenYears];
                    for (i = 0; i < lenYears; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            br.Close();
                            return false;
                        }
                        else
                        {
                            SumYear[i].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                            SumYear[i].Liq = br.ReadDouble();
                            SumYear[i].Oil = br.ReadDouble();
                            SumYear[i].Watering = br.ReadDouble();
                            SumYear[i].Inj = br.ReadDouble();
                            SumYear[i].InjGas = br.ReadDouble();
                            SumYear[i].Fprod = br.ReadInt32();
                            SumYear[i].Finj = br.ReadInt32();
                            SumYear[i].LeadInAll = br.ReadInt32();
                            SumYear[i].LeadInProd = br.ReadInt32();
                        }
                    }
                    this.Table81.SetTable(SumYear);
                }
                bfr.Close();
                retValue = true;
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteTable81ParamsToCacheOld(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись показателей Таблиц 8.1 в кэш";
            userState.Element = this.Name;

            if (Table81 != null)
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\table81.bin";
                int i, j, lenItem = 32, lenYears;

                lenYears = Table81.Count;
                MemoryStream ms = new MemoryStream(8 + (lenYears + 0) * lenItem);
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(Table81.ProjectDocName);
                bw.Write(Table81.NewPtdYear);
                bw.Write(lenYears);
                for (i = 0; i < lenYears; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        bw.Close();
                        return false;
                    }
                    else
                    {
                        bw.Write(RdfSystem.PackDateTime(Table81.Items[i].Date));
                        bw.Write(Table81.Items[i].Liq);
                        bw.Write(Table81.Items[i].Oil);
                        bw.Write(Table81.Items[i].Watering);
                        bw.Write(Table81.Items[i].Inj);
                        bw.Write(Table81.Items[i].InjGas);
                        bw.Write(Table81.Items[i].Fprod);
                        bw.Write(Table81.Items[i].Finj);
                        bw.Write(Table81.Items[i].LeadInAll);
                        bw.Write(Table81.Items[i].LeadInProd);
                    }
                    worker.ReportProgress(i, userState);
                }
                bw.Write(0);
                //MemoryStream packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                MemoryStream packedMS = ConvertEx.PackStream(ms);
                bw.Close();
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                bw = new BinaryWriter(fs);
                bw.Write(Table81Params.Version);
                bw.Write((int)packedMS.Length);
                packedMS.Seek(0, SeekOrigin.Begin);
                packedMS.WriteTo(fs);
                bw.Close();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadTable81ParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей Таблиц 8.1 из кэша";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\table81.bin";

            if (File.Exists(cacheName))
            {
                int i, lenYears, len_unpack;

                FileStream fs = new FileStream(cacheName, FileMode.Open); ;
                BinaryReader bfr = new BinaryReader(fs);
                if (Table81Params.Version == bfr.ReadInt32())
                {
                    len_unpack = bfr.ReadInt32();

                    MemoryStream ms = new MemoryStream(len_unpack);
                    ConvertEx.CopyStream(fs, ms, len_unpack);
                    MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                    ms.Dispose();
                    BinaryReader br = new BinaryReader(unpackMS);

                    Table81.ProjectDocName = br.ReadString();
                    Table81.NewPtdYear = br.ReadInt32();
                    lenYears = br.ReadInt32();

                    SumParamItem[] SumYear = new SumParamItem[lenYears];
                    for (i = 0; i < lenYears; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            br.Close();
                            return false;
                        }
                        else
                        {
                            SumYear[i].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                            SumYear[i].Liq = br.ReadDouble();
                            SumYear[i].Oil = br.ReadDouble();
                            SumYear[i].Watering = br.ReadDouble();
                            SumYear[i].Inj = br.ReadDouble();
                            SumYear[i].Fprod = br.ReadInt32();
                            SumYear[i].Finj = br.ReadInt32();
                            SumYear[i].LeadInAll = br.ReadInt32();
                            SumYear[i].LeadInProd = br.ReadInt32();
                            SumYear[i].NatGas = br.ReadDouble();
                            SumYear[i].InjGas = br.ReadDouble();
                        }
                    }
                    Table81.SetTable(SumYear);
                    br.Close();
                }
                bfr.Close();
                retValue = true;
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteTable81ParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись показателей Таблиц 8.1 в кэш";
            userState.Element = Name;

            if (Table81 != null)
            {
                string cacheName = project.path + "\\cache\\" + Name + "\\table81.bin";
                int i, lenItem = 48, lenYears;

                lenYears = Table81.Count;
                MemoryStream ms = new MemoryStream(8 + (lenYears + 0) * lenItem);
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(Table81.ProjectDocName);
                bw.Write(Table81.NewPtdYear);
                bw.Write(lenYears);
                for (i = 0; i < lenYears; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        bw.Close();
                        return false;
                    }
                    else
                    {
                        bw.Write(RdfSystem.PackDateTime(Table81.Items[i].Date));
                        bw.Write(Table81.Items[i].Liq);
                        bw.Write(Table81.Items[i].Oil);
                        bw.Write(Table81.Items[i].Watering);
                        bw.Write(Table81.Items[i].Inj);
                        bw.Write(Table81.Items[i].Fprod);
                        bw.Write(Table81.Items[i].Finj);
                        bw.Write(Table81.Items[i].LeadInAll);
                        bw.Write(Table81.Items[i].LeadInProd);
                        bw.Write(Table81.Items[i].NatGas);
                        bw.Write(Table81.Items[i].InjGas);
                    }
                    worker.ReportProgress(i, userState);
                }
                bw.Write(0);
                //MemoryStream packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                MemoryStream packedMS = ConvertEx.PackStream(ms);
                bw.Close();
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                bw = new BinaryWriter(fs);
                bw.Write(Table81Params.Version);
                bw.Write((int)packedMS.Length);
                packedMS.Seek(0, SeekOrigin.Begin);
                packedMS.WriteTo(fs);
                bw.Close();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            userState.Element = Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        #endregion

        #region BIZPLAN
        public void ReCalcDeltaBP(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (!BizPlanLoaded) LoadBizPlanHeadFromCache(worker, e);
            if (sumParams == null) LoadSumParamsFromCache(worker, e);
            if (BizPlanLoaded && sumParams != null && sumParams.Count > 0)
            {
                SumParamItem[] Items = sumParams.GetItemByObjCode(-1).MonthlyItems;
                OilfieldBizplan bp = BizPlanCollection.GetLastBizPlan();
                if (Items[Items.Length - 1].Date >= bp.StartDate && Items[Items.Length - 1].Date <= bp.StartDate.AddYears(2))
                {
                    BPParams[0] = bp.StartDate.ToOADate();
                    int ind = (Items[Items.Length - 1].Date.Year - bp.StartDate.Year) * 12 + Items[Items.Length - 1].Date.Month - bp.StartDate.Month;
                    double sumFact = 0, sumBP = 0;

                    BPParams[2] = Items[Items.Length - 1].Oil - bp.MonthLevels[ind].Oil * 1000;
                    if (bp.MonthLevels[ind].Oil > 0)
                    {
                        BPParams[4] = (BPParams[2] * 100) / (bp.MonthLevels[ind].Oil * 1000);
                    }
                    int i = Items.Length - 1;
                    while (ind >= 0 && i >= 0)
                    {
                        sumFact += Items[i].Oil;
                        sumBP += bp.MonthLevels[ind].Oil * 1000;
                        i--;
                        ind--;
                    }
                    if (sumFact > 0 || sumBP > 0)
                    {
                        BPParams[1] = sumFact - sumBP;
                        if (sumBP > 0)
                        {
                            BPParams[3] = (BPParams[1] * 100) / sumBP;
                        }
                    }
                    //BPParams[0] = 0;
                    WriteStratumCodesToCache();
                }
            }
        }
        public bool LoadBizPlanFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей Бизнес-плана из кэша";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\bizplan.bin";

            if (File.Exists(cacheName))
            {
                BizPlanCollection = new OilfieldBizplanCollection();
                FileStream fs = new FileStream(cacheName, FileMode.Open); ;
                BinaryReader bfr = new BinaryReader(fs);
                retValue = BizPlanCollection.ReadFromCache(bfr, true);
            }
            BizPlanLoaded = retValue;
            return retValue;
        }
        public bool LoadBizPlanHeadFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей Бизнес-плана из кэша";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\bizplan.bin";

            if (File.Exists(cacheName))
            {
                BizPlanCollection = new OilfieldBizplanCollection();
                FileStream fs = new FileStream(cacheName, FileMode.Open); ;
                BinaryReader bfr = new BinaryReader(fs);
                retValue = BizPlanCollection.ReadHeadFromCache(bfr, true);
            }
            BizPlanLoaded = retValue;
            return retValue;
        }
        public bool WriteBizPlanToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись показателей Бизнес-плана в кэш";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\bizplan.bin";

            if (BizPlanCollection != null)
            {
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);

                BizPlanCollection.WriteToCache(bw);
                ms.Seek(0, SeekOrigin.Begin);
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                ms.WriteTo(fs);
                fs.Close();
                ms.Dispose();
                return true;
            }
            return false;
        }
        #endregion

        #region PVT
        public bool LoadPVTParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Чтение данных PVT из кэша";
            userState.Element = this.Name;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\PVTParams.bin";

            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader file = new BinaryReader(fs);

                int Count = file.ReadInt32();
                int ver = 0;
                if (Count < 0)
                {
                    ver = Count * (-1);
                    Count = file.ReadInt32();
                }
                PVTParamsItem pvtItem = PVTParamsItem.Empty;
                List<PVTParamsItem> pvtItems = new List<PVTParamsItem>();
                for (int i = 0; i < Count; i++)
                {
                    pvtItem.ReadFromBin(file);
                    pvtItems.Add(pvtItem);
                }
                PVTList = new PVTParams();
                PVTList.SetItems(pvtItems.ToArray());
                retValue = true;
                file.Close();
            }
            return retValue;
        }
        public bool WritePVTParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных PVT в кэш";
            userState.Element = this.Name;
            if ((PVTList != null) && (PVTList.Count > 0))
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\PVTParams.bin";
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter file = new BinaryWriter(fs);

                PVTList.WriteToCache(file);
                retValue = true;
                file.Close();
            }
            return retValue;
        }
        #endregion

        #region TRAJECTORY
        public void SetVisibleWellTrajectory(bool Visible)
        {
            Well w;
            int i = 0;
            for (i = 0; i < Wells.Count; i++)
            {
                w = (Well)Wells[i];
                w.TrajectoryVisible = Visible;
            }
        }
        public bool LoadTrajectoryFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных траекторий месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\trajectory.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                        BinaryReader bfr = new BinaryReader(fs);
                        int version = bfr.ReadInt32();
                        if (version < WellTrajectory.Version)
                        {
                            bfr.Close();
                            return false;
                        }
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();

                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            wellBlockCount = bfr.ReadInt32();

                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                            for (i = 0; i < wellBlockCount; i++)
                            {
                                well_index = br.ReadInt32();
                                if (well_index > -1)
                                {
                                    w = (Well)Wells[well_index];
                                    userState.Element = w.UpperCaseName;
                                    w.trajectory = new WellTrajectory();
                                    w.trajectory.LoadFromCache(br, version);
                                    if (w.trajectory.Count == 0) w.trajectory = null;
                                    w.TrajectoryVisible = w.TrajectoryLoaded;
                                }
                                retValue = true;
                                this.WellTrajectoryLoaded = true;
                                ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                            wellCount += wellBlockCount;
                        }
                        this.WellTrajectoryLoaded = true;
                        FreeBuffers();
                        bfr.Close();
                        fs = null;
                    }
                    catch
                    {
                        retValue = false;
                    }
                    finally
                    {
                        if (fs != null) fs.Close();
                    }
                }
            }
            return retValue;
        }
        public bool LoadTrajectoryFromCache(int WellIndex)
        {
            return LoadTrajectoryFromCache(WellIndex, false);
        }
        public bool LoadTrajectoryFromCache(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\trajectory.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, len, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms = this.GetBuffer(true);
                MemoryStream unpackMS = this.GetBuffer(false);
                BinaryReader br = new BinaryReader(unpackMS);

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }

                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (version < WellTrajectory.Version)
                {
                    bfr.Close();
                    return false;
                }
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    well_count = bfr.ReadInt32();
                    if (WellIndex < wellCount + well_count)
                    {
                        ms.SetLength(len_block);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();
                        len = br.ReadInt32();
                        if ((WellIndex == well_index) && (len > 0))
                        {
                            w = (Well)Wells[well_index];
                            if (!NotLoadData)
                            {
                                w.trajectory = new WellTrajectory();
                                w.trajectory.LoadFromCache(br, version);
                                if (w.trajectory.Count == 0) w.trajectory = null;
                                w.TrajectoryVisible = w.TrajectoryLoaded;
                            }
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += well_count;
                }
                FreeBuffers();
                bfr.Close();
                br = null;
            }
            return retValue;
        }
        public bool LoadTrajectoryFromCache(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГИС месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\trajectory.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int lastIndex = 0;
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (version < WellTrajectory.Version)
                    {
                        bfr.Close();
                        return false;
                    }
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();

                    WellIndexes.Sort();
                    wellCount = 0;
                    lastIndex = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();

                        if (WellIndexes[lastIndex] < wellCount + well_count)
                        {
                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            while ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                            {
                                shift = (WellIndexes[lastIndex] - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();
                                if (WellIndexes[lastIndex] != well_index)
                                {
                                    bfr.Close();
                                    return false;
                                }
                                else
                                {
                                    w = (Well)Wells[well_index];
                                    w.trajectory = new WellTrajectory();
                                    w.trajectory.LoadFromCache(br, version);
                                    if (w.trajectory.Count == 0) w.trajectory = null;
                                    w.TrajectoryVisible = w.TrajectoryLoaded;
                                }
                                lastIndex++;
                                if (worker != null) ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                            }
                            if (lastIndex >= WellIndexes.Count) break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                    FreeBuffers();
                    bfr.Close();
                    retValue = true;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteTrajectoryToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных траекторий в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\trajectory.bin";
                int MAX_SIZE_PACK_BLOCK = 100 * 1024;
                Well w;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                BinaryReader br;
                MemoryStream ms;
                BinaryWriter bw;
                int[] arr;
                int i, j, k, h, lenItem = 16, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int gis_len, iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        if (w.TrajectoryLoaded)
                            len_all_wells += 12 + w.trajectory.Count * lenItem;
                        else
                            len_all_wells += 12;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);

                    len_block = 0;
                    k = 0;
                    wCount = 0;

                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        if (w.TrajectoryLoaded)
                            len_shift = 12 + w.trajectory.Count * lenItem;
                        else
                            len_shift = 12;


                        if (len_block + len_shift <= len_all_wells)
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                if (w.TrajectoryLoaded)
                                {
                                    len_shift += 8 + w.trajectory.Count * lenItem;
                                }
                                else
                                {
                                    len_shift += 8;
                                }
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);
                                if (w.TrajectoryLoaded)
                                {
                                    w.trajectory.WriteToCache(bw);
                                }
                                else
                                {
                                    bw.Write(0);
                                }
                            }
                            unpackSizes.Add((int)ms.Length);
                            packedMS = ConvertEx.PackStream(ms);
                            msArr.Add(packedMS);
                            wellCount.Add(wCount);
                            WellsShift.Clear();

                            ms.SetLength(0);
                            k = i;

                            w = (Well)Wells[i];
                            if (w.TrajectoryLoaded)
                            {
                                len_shift = 12 + w.trajectory.Count * lenItem;
                            }
                            else
                            {
                                len_shift = 12;
                            }

                            len_block = len_shift;
                            wCount = 1;
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    len_shift = 4 * wCount;
                    bw.Write(len_shift);
                    for (j = k; j < i - 1; j++)
                    {
                        w = (Well)Wells[j];
                        if (w.TrajectoryLoaded)
                        {
                            len_shift += 8 + w.trajectory.Count * lenItem;
                        }
                        else
                        {
                            len_shift += 8;
                        }
                        bw.Write(len_shift);
                    }

                    for (j = k; j < i; j++)
                    {
                        w = (Well)Wells[j];
                        bw.Write(w.Index);
                        if (w.TrajectoryLoaded)
                        {
                            w.trajectory.WriteToCache(bw);
                        }
                        else
                        {
                            bw.Write(0);
                        }
                    }
                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellTrajectory.Version);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearTrajectoryData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if (w.TrajectoryLoaded)
                    {
                        w.trajectory.Clear();
                        w.trajectory = null;
                    }
                    w.TrajectoryVisible = false;
                }
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
                this.WellTrajectoryLoaded = false;
            }
        }
        #endregion

        #region GTM
        public bool LoadGTMFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\gtm.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (version < WellGTM.Version)
                {
                    bfr.Close();
                    return false;
                }
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    wellBlockCount = bfr.ReadInt32();
                    if (WellIndex < wellCount + wellBlockCount)
                    {
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();

                        if (WellIndex != well_index)
                        {
                            MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if (well_index > -1)
                        {
                            w = (Well)Wells[well_index];
                            w.gtm = new WellGTM();
                            w.gtm.LoadFromCache(br, version);
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += wellBlockCount;
                }
                FreeBuffers();
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadGTMFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadGTMFromCache(worker, e, false);
        }
        public bool LoadGTMFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГТМ месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\gtm.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (version < WellGTM.Version)
                    {
                        bfr.Close();
                        return false;
                    }
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();

                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();

                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                        for (i = 0; i < wellBlockCount; i++)
                        {
                            well_index = br.ReadInt32();
                            if (well_index > -1)
                            {
                                w = (Well)Wells[well_index];
                                userState.Element = w.UpperCaseName;
                                w.gtm = new WellGTM();
                                w.gtm.LoadFromCache(br, version);
                                if (w.gtm.Count == 0) w.gtm = null;
                            }
                            retValue = true;
                            this.WellGtmLoaded = true;
                            if (!NotReportProgress) ReportProgress(worker, wellCount + i, iLen, userState);
                        }
                        wellCount += wellBlockCount;
                    }
                    FreeBuffers();
                    bfr.Close();
                    GC.GetTotalMemory(true);
                }
            }
            return retValue;
        }
        public bool WriteGTMToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных ГТМ в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\gtm.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, h, lenItem = 10, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        len_all_wells += 8;
                        if ((w.GTMLoaded) && (w.gtm != null))
                        {
                            len_all_wells += 4 + w.gtm.Count * lenItem;
                        }
                        else
                            len_all_wells += 4;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = (Well)Wells[i];
                            len_shift = 8;
                            if ((w.GTMLoaded) && (w.gtm != null))
                            {
                                len_shift += 4 + w.gtm.Count * lenItem;
                            }
                            else
                                len_shift += 4;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                len_shift += 4;
                                if (w.GTMLoaded)
                                {
                                    len_shift += 4 + w.gtm.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);
                                if (w.GTMLoaded)
                                {
                                    w.gtm.WriteToCache(bw);
                                }
                                else bw.Write(0);
                            }

                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = (Well)Wells[i];
                                len_shift = 8;
                                if ((w.GTMLoaded) && (w.gtm != null))
                                {
                                    len_shift += 4 + w.gtm.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellGTM.Version);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearGTMData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if (w.GTMLoaded)
                    {
                        w.gtm.Clear();
                        w.gtm = null;
                    }
                }
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
                this.WellGtmLoaded = false;
            }
        }
        #endregion

        #region WELL ACTION
        public bool LoadWellActionFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_action.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (version > WellAction.Version)
                {
                    bfr.Close();
                    return false;
                }
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    wellBlockCount = bfr.ReadInt32();
                    if (WellIndex < wellCount + wellBlockCount)
                    {
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();

                        if (WellIndex != well_index)
                        {
                            bfr.Close();
                            return false;
                        }
                        else if (well_index > -1)
                        {
                            w = (Well)Wells[well_index];
                            w.action = new WellAction();
                            w.action.ReadFromCache(br, version);
                            if (w.action.Count == 0) w.action = null;
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += wellBlockCount;
                }
                FreeBuffers();
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadWellActionFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadWellActionFromCache(worker, e, false);
        }
        public bool LoadWellActionFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных мероприятий на скважинах";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_action.bin";
            if (File.Exists(cacheName))
            {
                if (worker != null && worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (version < WellAction.Version)
                    {
                        bfr.Close();
                        return false;
                    }
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();

                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();

                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                        for (i = 0; i < wellBlockCount; i++)
                        {
                            well_index = br.ReadInt32();
                            if (well_index > -1)
                            {
                                w = (Well)Wells[well_index];
                                userState.Element = w.UpperCaseName;
                                w.action = new WellAction();
                                w.action.ReadFromCache(br, version);
                                if (w.action.Count == 0) w.action = null;
                            }
                            retValue = true;
                            WellRepairActionLoaded = true;
                            if (!NotReportProgress && worker != null) 
                            {
                                ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                        }
                        wellCount += wellBlockCount;
                    }
                    FreeBuffers();
                    bfr.Close();
                    GC.GetTotalMemory(true);
                }
            }
            return retValue;
        }
        public bool WriteWellActionToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных мероприятий на скважинах в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_action.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, h, lenItem = 10, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        len_all_wells += 8;
                        if ((w.ActionLoaded) && (w.action != null))
                        {
                            len_all_wells += 4 + w.action.Count * lenItem;
                        }
                        else
                            len_all_wells += 4;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = (Well)Wells[i];
                            len_shift = 8;
                            if ((w.ActionLoaded) && (w.action != null))
                            {
                                len_shift += 4 + w.action.Count * lenItem;
                            }
                            else
                                len_shift += 4;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                len_shift += 4;
                                if (w.ActionLoaded)
                                {
                                    len_shift += 4 + w.action.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);
                                if (w.ActionLoaded)
                                {
                                    w.action.WriteToCache(bw);
                                }
                                else bw.Write(0);
                            }

                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = (Well)Wells[i];
                                len_shift = 8;
                                if ((w.ActionLoaded) && (w.action != null))
                                {
                                    len_shift += 4 + w.action.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellAction.Version);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearWellActionData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if (w.ActionLoaded)
                    {
                        w.action.Clear();
                        w.action = null;
                    }
                }
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
                WellRepairActionLoaded = false;
            }
        }
        #endregion

        #region RESEARCH
        public bool LoadWellResearchFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_research.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    if (fs.Length == 0) return false;
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (version > WellResearch.Version)
                {
                    bfr.Close();
                    return false;
                }
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    wellBlockCount = bfr.ReadInt32();
                    if (WellIndex < wellCount + wellBlockCount)
                    {
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();

                        if (WellIndex != well_index)
                        {
                            MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if (well_index > -1)
                        {
                            w = (Well)Wells[well_index];
                            w.research = new WellResearch();
                            w.research.ReadFromCache(br, version);
                            if (w.research.Count == 0) w.research = null;
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += wellBlockCount;
                }
                FreeBuffers();
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadWellResearchFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadWellResearchFromCache(worker, e, false);
        }
        public bool LoadWellResearchFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных давлений на скважинах";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_research.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (version > WellResearch.Version)
                    {
                        bfr.Close();
                        return false;
                    }
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();

                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();

                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                        for (i = 0; i < wellBlockCount; i++)
                        {
                            well_index = br.ReadInt32();
                            if (well_index > -1)
                            {
                                w = (Well)Wells[well_index];
                                userState.Element = w.UpperCaseName;
                                w.research = new WellResearch();
                                w.research.ReadFromCache(br, version);
                                if (w.research.Count > 0)
                                {
                                    WellResearchLoaded = true;
                                }
                                else
                                {
                                    w.research = null;
                                }
                            }
                            retValue = true;
                            if (!NotReportProgress) ReportProgress(worker, wellCount + i, iLen, userState);
                        }
                        wellCount += wellBlockCount;
                    }
                    FreeBuffers();
                    bfr.Close();
                    GC.GetTotalMemory(true);
                }
            }
            return retValue;
        }
        public bool LoadWellResearchFromCache(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных давлений на скважинах";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_research.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Open);
                    if (fs.Length == 0) return false;
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (version > WellResearch.Version)
                    {
                        bfr.Close();
                        return false;
                    }
                    WellIndexes.Sort();
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    int lastIndex = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();
                        if (WellIndexes[lastIndex] < wellCount + well_count)
                        {
                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            while ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                            {
                                shift = (WellIndexes[lastIndex] - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();

                                if (WellIndexes[lastIndex] != well_index)
                                {
                                    bfr.Close();
                                    return false;
                                }
                                else
                                {
                                    w = (Well)Wells[well_index];
                                    w.research = new WellResearch();
                                    w.research.ReadFromCache(br, version);
                                    if (w.research.Count == 0) w.research = null;
                                    if (w.ResearchLoaded) this.WellResearchLoaded = true;
                                }
                                lastIndex++;
                                if (worker != null) ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                            }
                            if (lastIndex >= WellIndexes.Count) break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                    FreeBuffers();
                    bfr.Close();
                    retValue = true;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteWellResearchToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных давлений на скважинах в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_research.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, h, lenItem = 28, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        len_all_wells += 8;
                        if ((w.ResearchLoaded) && (w.research != null))
                        {
                            len_all_wells += 4 + w.research.Count * lenItem;
                        }
                        else
                            len_all_wells += 4;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = (Well)Wells[i];
                            len_shift = 8;
                            if ((w.ResearchLoaded) && (w.research != null))
                            {
                                len_shift += 4 + w.research.Count * lenItem;
                            }
                            else
                                len_shift += 4;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                len_shift += 4;
                                if (w.ResearchLoaded)
                                {
                                    len_shift += 4 + w.research.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);
                                if (w.ResearchLoaded)
                                {
                                    w.research.WriteToCache(bw);
                                }
                                else
                                    bw.Write(0);
                            }

                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = (Well)Wells[i];
                                len_shift = 8;
                                if ((w.ResearchLoaded) && (w.research != null))
                                {
                                    len_shift += 4 + w.research.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellResearch.Version);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearWellResearchData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if (w.ResearchLoaded)
                    {
                        w.research.Clear();
                        w.research = null;
                    }
                }
                WellResearchLoaded = false;
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
            }
        }
        #endregion

        #region SUSPEND
        public bool LoadWellSuspendFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_suspend.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    if (fs.Length == 0) return false;
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (version < WellSuspend.Version)
                {
                    bfr.Close();
                    return false;
                }
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    wellBlockCount = bfr.ReadInt32();
                    if (WellIndex < wellCount + wellBlockCount)
                    {
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();

                        if (WellIndex != well_index)
                        {
                            MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if (well_index > -1)
                        {
                            w = (Well)Wells[well_index];
                            w.suspend = new WellSuspend();
                            w.suspend.ReadFromCache(br, version);
                            if (w.suspend.Count == 0) w.suspend = null;
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += wellBlockCount;
                }
                FreeBuffers();
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadWellSuspendFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadWellSuspendFromCache(worker, e, false);
        }
        public bool LoadWellSuspendFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных КВЧ на скважинах";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_suspend.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (version < WellSuspend.Version)
                    {
                        bfr.Close();
                        return false;
                    }
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();

                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();

                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                        for (i = 0; i < wellBlockCount; i++)
                        {
                            well_index = br.ReadInt32();
                            if (well_index > -1)
                            {
                                w = (Well)Wells[well_index];
                                userState.Element = w.UpperCaseName;
                                w.suspend = new WellSuspend();
                                w.suspend.ReadFromCache(br, version);
                                if (w.suspend.Count > 0)
                                {
                                    WellSuspendLoaded = true;
                                }
                                else
                                {
                                    w.suspend = null;
                                }
                            }
                            retValue = true;
                            if (!NotReportProgress) ReportProgress(worker, wellCount + i, iLen, userState);
                        }
                        wellCount += wellBlockCount;
                    }
                    FreeBuffers();
                    bfr.Close();
                    GC.GetTotalMemory(true);
                }
            }
            return retValue;
        }
        public bool WriteWellSuspendToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных КВЧ на скважинах в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_suspend.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, h, lenItem = 12, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        len_all_wells += 8;
                        if ((w.SuspendLoaded) && (w.suspend != null))
                        {
                            len_all_wells += 4 + w.suspend.Count * lenItem;
                        }
                        else
                            len_all_wells += 4;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = (Well)Wells[i];
                            len_shift = 8;
                            if ((w.SuspendLoaded) && (w.suspend != null))
                            {
                                len_shift += 4 + w.suspend.Count * lenItem;
                            }
                            else
                                len_shift += 4;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                len_shift += 4;
                                if (w.SuspendLoaded)
                                {
                                    len_shift += 4 + w.suspend.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);
                                if (w.SuspendLoaded)
                                {
                                    w.suspend.WriteToCache(bw);
                                }
                                else
                                    bw.Write(0);
                            }

                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = (Well)Wells[i];
                                len_shift = 8;
                                if ((w.SuspendLoaded) && (w.suspend != null))
                                {
                                    len_shift += 4 + w.suspend.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellSuspend.Version);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearWellSuspendData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if (w.SuspendLoaded)
                    {
                        w.suspend.Clear();
                        w.suspend = null;
                    }
                }
                WellSuspendLoaded = false;
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
            }
        }
        #endregion

        #region WELL LEAKS
        public bool LoadWellLeakFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_leak.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (version < WellLeak.Version)
                {
                    bfr.Close();
                    return false;
                }
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    wellBlockCount = bfr.ReadInt32();
                    if (WellIndex < wellCount + wellBlockCount)
                    {
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();

                        if (WellIndex != well_index)
                        {
                            MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if (well_index > -1)
                        {
                            w = (Well)Wells[well_index];
                            w.leak = new WellLeak();
                            w.leak.ReadFromCache(br, version);
                            if (w.leak.Count == 0) w.leak = null;
                            retValue = w.LeakLoaded;
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += wellBlockCount;
                }
                FreeBuffers();
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadWellLeakFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadWellLeakFromCache(worker, e, false);
        }
        public bool LoadWellLeakFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка исследований ЭК на скважинах";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_leak.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (version < WellLeak.Version)
                    {
                        bfr.Close();
                        return false;
                    }
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();

                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();

                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                        for (i = 0; i < wellBlockCount; i++)
                        {
                            well_index = br.ReadInt32();
                            if (well_index > -1)
                            {
                                w = (Well)Wells[well_index];
                                userState.Element = w.UpperCaseName;
                                w.leak = new WellLeak();
                                w.leak.ReadFromCache(br, version);
                                if (w.leak.Count > 0)
                                {
                                    WellLeakLoaded = true;
                                }
                                else
                                {
                                    w.leak = null;
                                }

                            }
                            retValue = true;
                            if (!NotReportProgress) ReportProgress(worker, wellCount + i, iLen, userState);
                        }
                        wellCount += wellBlockCount;
                    }
                    FreeBuffers();
                    bfr.Close();
                    GC.GetTotalMemory(true);
                }
            }
            return retValue;
        }
        public bool WriteWellLeakToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись исследований ЭК на скважинах в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = this.project.path + "\\cache\\" + this.Name + "\\well_leak.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                ArrayList msArr = new ArrayList();
                ArrayList wellCount = new ArrayList(5);
                ArrayList unpackSizes = new ArrayList(5);
                ArrayList WellsShift = new ArrayList();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, h, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = (Well)Wells[i];
                        len_all_wells += 8;
                        if ((w.LeakLoaded) && (w.leak != null))
                        {
                            len_all_wells += 4 + w.leak.GetLength();
                        }
                        else
                            len_all_wells += 4;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = (Well)Wells[i];
                            len_shift = 8;
                            if ((w.LeakLoaded) && (w.leak != null))
                            {
                                len_shift += 4 + w.leak.GetLength();
                            }
                            else
                                len_shift += 4;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = (Well)Wells[j];
                                len_shift += 4;
                                if (w.LeakLoaded)
                                {
                                    len_shift += 4 + w.leak.GetLength();
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = (Well)Wells[j];
                                bw.Write(w.Index);
                                if (w.LeakLoaded)
                                {
                                    w.leak.WriteToCache(bw);
                                }
                                else
                                {
                                    bw.Write(0);
                                }
                            }

                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = (Well)Wells[i];
                                len_shift = 8;
                                if ((w.LeakLoaded) && (w.leak != null))
                                {
                                    len_shift += 4 + w.leak.GetLength();
                                }
                                else
                                    len_shift += 4;
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellLeak.Version);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearWellLeakData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = (Well)Wells[i];
                    if (w.LeakLoaded)
                    {
                        w.leak.Clear();
                        w.leak = null;
                    }
                }
                WellLeakLoaded = false;
                if (useGC)
                {
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                }
            }
        }
        #endregion

        #region BUBBLE
        public void CreateSortedWellList()
        {
            SortedWells.Clear();
            int i, j, k;
            Well w;
            bool ins;
            for (i = 0; i < Wells.Count; i++)
            {
                SortedWells.Add(Wells[i]);
            }
            Well w1, w2;
            for (i = 0; i < SortedWells.Count - 1; i++)
            {
                for (j = i + 1; j < SortedWells.Count; j++)
                {
                    w1 = SortedWells[i];
                    w2 = SortedWells[j];
                    if ((w2.BubbleList == null) || (w2.BubbleList.MaxVal == 0.0) || (w1.BubbleList.MaxVal > 0 && w1.BubbleList.MaxVal < w2.BubbleList.MaxVal))
                    {
                        SortedWells[j] = w1;
                        SortedWells[i] = w2;
                    }
                }
            }
            if (WellsLayer != null)
            {
                WellsLayer.ObjectsList.Clear();
                for (i = 0; i < SortedWells.Count; i++)
                {
                    WellsLayer.ObjectsList.Add(SortedWells[i]);
                }
            }
        }
        public void SetVisibleBubbleMap(bool Visible)
        {
            Well w;
            int i = 0;
            for (i = 0; i < Wells.Count; i++)
            {
                w = (Well)Wells[i];
                if (w.BubbleList != null)
                {
                    w.BubbleList.Visible = Visible;
                    w.SetBrushFromIcon();
                }
            }
        }
        public bool CalcBubbleMap(BackgroundWorker worker, DoWorkEventArgs e, bool LiquidInCube, DateTime maxDT, int AveMonth, float K, FilterOilObjects filter)
        {
            if (this.MerLoaded)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Расчет карты текущих отборов";
                userState.WorkCurrentTitle = "Расчет карты текущих отборов";
                int i, j, ave, ind, lastPlastId;
                Well w;
                MerItem item;
                bool lastMonth = maxDT == DateTime.MaxValue;
                var dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                DateTime dt;
                double sumLiq, sumOil, sumInj, sumInjGas, sumNatGas, sumGasCond, sumLiqV, sumOilV;

                MerWorkTimeItem wtItem, newWtItem;
                BubbleParamItem bubbleItem;

                MerWorkTimeCollection ProdWorkTime = new MerWorkTimeCollection();
                MerWorkTimeCollection InjWorkTime = new MerWorkTimeCollection();

                if (AveMonth == 0) AveMonth++;

                if ((lastMonth) || (this.maxMerDate < maxDT))
                {
                    maxDT = this.maxMerDate;
                }
                this.project.BubbleCalcDT2 = maxDT;

                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    userState.Element = w.UpperCaseName;
                    if ((w.MerLoaded) && (w.mer != null) && (w.mer.Items != null))
                    {
                        j = w.mer.Count - 1;
                        while ((j > -1) && (maxDT != w.mer.Items[j].Date))
                        {
                            j--;
                        }
                        ave = AveMonth;
                        dt = maxDT;
                        sumLiq = 0; sumOil = 0; sumInj = 0; sumInjGas = 0; sumNatGas = 0; sumGasCond = 0; sumLiqV = 0; sumOilV = 0;
                        ProdWorkTime.Clear();
                        InjWorkTime.Clear();

                        while (ave > 0)
                        {
                            lastPlastId = -1;
                            ind = 0;
                            while ((j > -1) && (dt == w.mer.Items[j].Date))
                            {
                                item = w.mer.Items[j];
                                if (lastPlastId != item.PlastId)
                                {
                                    ind = dict.GetIndexByCode(item.PlastId);
                                    lastPlastId = item.PlastId;
                                }

                                if (ind > 0)
                                {
                                    if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT, LiquidInCube);
                                    bubbleItem = w.BubbleList.GetItemByObjIndex(ind);
                                    if (bubbleItem == null)
                                    {
                                        bubbleItem = new BubbleParamItem(ind);
                                        w.BubbleList.Add(bubbleItem);
                                    }
                                    if (item.CharWorkId == 11)
                                    {
                                        bubbleItem.Liq += item.Oil + item.Wat;
                                        bubbleItem.Oil += item.Oil;
                                        bubbleItem.LiqV += item.OilV + item.WatV;
                                        bubbleItem.OilV += item.OilV;
                                        sumLiq += item.Oil + item.Wat;
                                        sumOil += item.Oil;
                                        sumLiqV += item.OilV + item.WatV;
                                        sumOilV += item.OilV;
                                        if (bubbleItem.ProdWorkTime == null) bubbleItem.ProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.ProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 12)
                                    {
                                        bubbleItem.NatGas += w.mer.GetItemEx(j).NaturalGas;
                                        sumNatGas += w.mer.GetItemEx(j).NaturalGas;
                                        if (bubbleItem.ProdWorkTime == null) bubbleItem.ProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.ProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 15)
                                    {
                                        bubbleItem.GasCond += w.mer.GetItemEx(j).GasCondensate;
                                        sumGasCond += w.mer.GetItemEx(j).GasCondensate;
                                        if (bubbleItem.ProdWorkTime == null) bubbleItem.ProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.ProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 20)
                                    {
                                        int agent = w.mer.GetItemEx(j).AgentInjId;
                                        if ((agent > 80) && (agent < 90))
                                        {
                                            bubbleItem.InjGas += w.mer.GetItemEx(j).Injection;
                                            sumInjGas += w.mer.GetItemEx(j).Injection;
                                        }
                                        else
                                        {
                                            bubbleItem.Inj += w.mer.GetItemEx(j).Injection;
                                            sumInj += w.mer.GetItemEx(j).Injection;
                                        }

                                        if (bubbleItem.InjWorkTime == null) bubbleItem.InjWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.InjWorkTime.Add(w.mer, j);
                                        InjWorkTime.Add(w.mer, j);
                                    }
                                }
                                j--;
                            }
                            if ((j <= -1) || (j >= w.mer.Count)) break;
                            dt = w.mer.Items[j].Date;
                            ave--;
                        }
                        if (w.BubbleList != null)
                        {
                            bubbleItem = new BubbleParamItem(-1);
                            bubbleItem.Liq = sumLiq;
                            bubbleItem.LiqV = sumLiqV;
                            bubbleItem.Oil = sumOil;
                            bubbleItem.OilV = sumOilV;
                            bubbleItem.Inj = sumInj;
                            bubbleItem.InjGas = sumInjGas / 1000;
                            bubbleItem.NatGas = sumNatGas / 1000;
                            bubbleItem.GasCond = sumGasCond;
                            wtItem = ProdWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 744 * AveMonth)
                            {
                                newWtItem = new MerWorkTimeItem();
                                newWtItem.WorkTime = 744 * AveMonth;
                                newWtItem.CollTime = 0;
                                ProdWorkTime.Clear();
                                ProdWorkTime.Add(newWtItem);
                            }
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                bubbleItem.ProdWorkTime = ProdWorkTime;
                                ProdWorkTime = new MerWorkTimeCollection();
                            }
                            wtItem = InjWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 744 * AveMonth)
                            {
                                newWtItem = new MerWorkTimeItem();
                                newWtItem.WorkTime = 744 * AveMonth;
                                newWtItem.CollTime = 0;
                                InjWorkTime.Clear();
                                InjWorkTime.Add(newWtItem);
                            }
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                bubbleItem.InjWorkTime = InjWorkTime;
                                InjWorkTime = new MerWorkTimeCollection();
                            }
                            w.BubbleList.Add(bubbleItem);
                            w.BubbleList.K = K;
                        }
                    }
                    if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT, LiquidInCube);
                    SetBubbleIconsList(w, maxDT, filter);
                    if (w.BubbleList != null) w.BubbleList.SetBubbleByFilter(filter);
                    worker.ReportProgress((int)((float)i * 100 / (float)Wells.Count), userState);
                }
                CreateSortedWellList();
                userState.Element = this.ParamsDict.OilFieldName;
                worker.ReportProgress(100, userState);
                this.BubbleMapLoaded = true;
                return true;
            }
            return false;
        }
        public bool CalcBubbleAccumMap(BackgroundWorker worker, DoWorkEventArgs e, bool LiquidInCube, DateTime minDT, DateTime maxDT, float K, FilterOilObjects filter)
        {

            if (this.MerLoaded)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Расчет карты накопленных отборов";
                userState.WorkCurrentTitle = "Расчет карты накопленных отборов";
                int i, j, ind, lastPlastId;
                Well w;
                MerItem item;
                bool startMonth = minDT == DateTime.MinValue;
                bool lastMonth = maxDT == DateTime.MaxValue;
                double sumLiq, sumOil, sumInj, sumInjGas, sumNatGas, sumGasCond, sumLiqV, sumOilV;
                var dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                BubbleParamItem bubbleItem;

                if ((startMonth) || (this.minMerDate > minDT))
                {
                    minDT = this.minMerDate;
                }

                if ((lastMonth) || (this.maxMerDate < maxDT))
                {
                    maxDT = this.maxMerDate;
                }
                this.project.BubbleCalcDT1 = minDT;
                this.project.BubbleCalcDT2 = maxDT;
                SortedWells.Clear();
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    userState.Element = w.UpperCaseName;
                    if ((w.MerLoaded) && (w.mer != null) && (w.mer.Items != null))
                    {
                        j = 0;
                        sumInj = 0; sumInjGas = 0; sumLiq = 0; sumOil = 0; sumNatGas = 0; sumGasCond = 0; sumLiqV = 0; sumOilV = 0;
                        lastPlastId = -1;
                        ind = 0;
                        while (j < w.mer.Count)
                        {
                            if (w.mer.Items[j].Date < minDT)
                                j++;
                            else
                                break;
                        }

                        while ((j < w.mer.Count) && (w.mer.Items[j].Date <= maxDT))
                        {
                            item = w.mer.Items[j];
                            if (lastPlastId != item.PlastId)
                            {
                                ind = dict.GetIndexByCode(item.PlastId);
                                lastPlastId = item.PlastId;
                            }
                            if (ind > 0)
                            {
                                if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM, LiquidInCube);
                                bubbleItem = w.BubbleList.GetItemByObjIndex(ind);
                                if (bubbleItem == null)
                                {
                                    bubbleItem = new BubbleParamItem(ind);
                                    w.BubbleList.Add(bubbleItem);
                                }
                                if (item.CharWorkId == 11)
                                {
                                    bubbleItem.Liq += item.Oil + item.Wat;
                                    bubbleItem.Oil += item.Oil;
                                    sumLiq += item.Oil + item.Wat;
                                    sumOil += item.Oil;
                                    bubbleItem.LiqV += item.OilV + item.WatV;
                                    bubbleItem.OilV += item.OilV;
                                    sumLiqV += item.OilV + item.WatV;
                                    sumOilV += item.OilV;
                                }
                                else if (item.CharWorkId == 12)
                                {
                                    bubbleItem.NatGas += w.mer.GetItemEx(j).NaturalGas;
                                    sumNatGas += w.mer.GetItemEx(j).NaturalGas;
                                }
                                else if (item.CharWorkId == 15)
                                {
                                    bubbleItem.GasCond += w.mer.GetItemEx(j).GasCondensate;
                                    sumGasCond += w.mer.GetItemEx(j).GasCondensate;
                                }
                                else if (item.CharWorkId == 20)
                                {
                                    int agent = w.mer.GetItemEx(j).AgentInjId;
                                    if ((agent > 80) && (agent < 90))
                                    {
                                        bubbleItem.InjGas += w.mer.GetItemEx(j).Injection;
                                        sumInjGas += w.mer.GetItemEx(j).Injection;
                                    }
                                    else
                                    {
                                        bubbleItem.Inj += w.mer.GetItemEx(j).Injection;
                                        sumInj += w.mer.GetItemEx(j).Injection;
                                    }

                                }
                            }
                            j++;
                        }
                        if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM, LiquidInCube);
                        if (w.BubbleList != null)
                        {
                            for (j = 0; j < w.BubbleList.Count; j++)
                            {
                                bubbleItem = w.BubbleList[j];
                                bubbleItem.Liq /= 1000;
                                bubbleItem.LiqV /= 1000;
                                bubbleItem.Oil /= 1000;
                                bubbleItem.OilV /= 1000;
                                bubbleItem.Inj /= 1000;
                                bubbleItem.InjGas /= 1000000;
                                bubbleItem.NatGas /= 1000000;
                                bubbleItem.GasCond /= 1000;
                            }
                            bubbleItem = new BubbleParamItem(-1);
                            bubbleItem.Liq = sumLiq / 1000;
                            bubbleItem.LiqV = sumLiqV / 1000;
                            bubbleItem.Oil = sumOil / 1000;
                            bubbleItem.OilV = sumOilV / 1000;
                            bubbleItem.Inj = sumInj / 1000;
                            bubbleItem.InjGas = sumInjGas / 1000000;
                            bubbleItem.NatGas = sumNatGas / 1000000;
                            bubbleItem.GasCond = sumGasCond / 1000;
                            w.BubbleList.Add(bubbleItem);
                            w.BubbleList.K = K;
                        }
                        worker.ReportProgress((int)((float)i * 100 / (float)Wells.Count), userState);
                    }
                    if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM, LiquidInCube);
                    SetBubbleIconsList(w, maxDT, filter);
                    if (w.BubbleList != null) w.BubbleList.SetBubbleByFilter(filter);
                }
                CreateSortedWellList();
                userState.Element = this.ParamsDict.OilFieldName;
                worker.ReportProgress(100, userState);
                this.BubbleMapLoaded = true;
                return true;
            }
            return false;
        }
        public bool CalcBubbleHistoryMap(BackgroundWorker worker, DoWorkEventArgs e, bool LiquidInCube, DateTime minDT, DateTime maxDT, int StartAveMonth, int LastAveMonth, bool ViewBySector, float K, bool DataSource, FilterOilObjects filter)
        {
            #region По МЭР
            if (!DataSource && this.MerLoaded)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Расчет карты изменений отборов";
                userState.WorkCurrentTitle = "Расчет карты изменений отборов";
                int i, j, p, ave, ind, lastPlastId;
                Well w;
                MerItem item;
                SortedWells.Clear();
                bool startMonth = minDT == DateTime.MinValue;
                bool lastMonth = maxDT == DateTime.MaxValue;
                if (StartAveMonth == 0) StartAveMonth++;
                if (LastAveMonth == 0) LastAveMonth++;
                DateTime dt;

                var dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                double sumLiq, sumOil, sumInj, sumInjGas, sumNatGas, sumGasCond, sumLiqV, sumOilV;
                BubbleParamItem bubbleItem;
                MerWorkTimeItem wtItem, newWtItem;
                MerWorkTimeCollection ProdWorkTime = new MerWorkTimeCollection();
                MerWorkTimeCollection InjWorkTime = new MerWorkTimeCollection();

                if (this.minMerDate > minDT)
                {
                    minDT = this.minMerDate;
                }
                if (startMonth)
                {
                    minDT = new DateTime(this.maxMerDate.Year, 1, 1);
                }

                if ((lastMonth) || (this.maxMerDate < maxDT))
                {
                    maxDT = this.maxMerDate;
                }
                this.project.BubbleCalcDT1 = minDT;
                this.project.BubbleCalcDT2 = maxDT;
                this.project.BubbleHistoryByChess = DataSource;

                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    sumLiq = 0; sumOil = 0; sumInj = 0; sumInjGas = 0; sumNatGas = 0; sumGasCond = 0; sumLiqV = 0; sumOilV = 0;
                    ProdWorkTime.Clear();
                    InjWorkTime.Clear();

                    userState.Element = w.UpperCaseName;
                    if ((w.MerLoaded) && (w.mer != null) && (w.mer.Items != null))
                    {
                        j = 0;
                        while (j < w.mer.Count)
                        {
                            if (w.mer.Items[j].Date < minDT)
                            {
                                j++;
                            }
                            else
                            {
                                break;
                            }
                        }

                        sumLiq = 0; sumOil = 0; sumInj = 0; sumInjGas = 0; sumNatGas = 0; sumGasCond = 0; sumLiqV = 0; sumOilV = 0;
                        ave = StartAveMonth;
                        dt = minDT;
                        while (ave > 0)
                        {
                            lastPlastId = -1;
                            ind = 0;
                            while ((j < w.mer.Count) && (j > -1) && (dt == w.mer.Items[j].Date))
                            {
                                item = w.mer.Items[j];
                                if (lastPlastId != item.PlastId)
                                {
                                    ind = dict.GetIndexByCode(item.PlastId);
                                    lastPlastId = item.PlastId;
                                }
                                if (ind > 0)
                                {
                                    if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);

                                    bubbleItem = w.BubbleList.GetItemByObjIndex(ind);
                                    if (bubbleItem == null)
                                    {
                                        bubbleItem = new BubbleParamItem(ind);
                                        w.BubbleList.Add(bubbleItem);
                                    }
                                    if (item.CharWorkId == 11)
                                    {
                                        bubbleItem.hLiq += item.Oil + item.Wat;
                                        bubbleItem.hOil += item.Oil;
                                        sumLiq += item.Oil + item.Wat;
                                        sumOil += item.Oil;
                                        bubbleItem.hLiqV += item.OilV + item.WatV;
                                        bubbleItem.hOilV += item.OilV;
                                        sumLiqV += item.OilV + item.WatV;
                                        sumOilV += item.OilV;
                                        if (bubbleItem.HistProdWorkTime == null) bubbleItem.HistProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.HistProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 12)
                                    {
                                        bubbleItem.hNatGas += w.mer.GetItemEx(j).NaturalGas;
                                        sumNatGas += w.mer.GetItemEx(j).NaturalGas;
                                        if (bubbleItem.HistProdWorkTime == null) bubbleItem.HistProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.HistProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 15)
                                    {
                                        bubbleItem.hGasCond += w.mer.GetItemEx(j).GasCondensate;
                                        sumGasCond += w.mer.GetItemEx(j).GasCondensate;
                                        if (bubbleItem.HistProdWorkTime == null) bubbleItem.HistProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.HistProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 20)
                                    {
                                        int agent = w.mer.GetItemEx(j).AgentInjId;
                                        if ((agent > 80) && (agent < 90))
                                        {
                                            bubbleItem.hInjGas += w.mer.GetItemEx(j).Injection;
                                            sumInjGas += w.mer.GetItemEx(j).Injection;
                                        }
                                        else
                                        {
                                            bubbleItem.hInj += w.mer.GetItemEx(j).Injection;
                                            sumInj += w.mer.GetItemEx(j).Injection;
                                        }
                                        if (bubbleItem.HistInjWorkTime == null) bubbleItem.HistInjWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.HistInjWorkTime.Add(w.mer, j);
                                        InjWorkTime.Add(w.mer, j);
                                    }
                                }
                                j++;
                            }
                            if ((j <= -1) || (j >= w.mer.Count)) break;
                            dt = w.mer.Items[j].Date;
                            ave--;
                        }
                        if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);
                        if (w.BubbleList != null)
                        {
                            bubbleItem = new BubbleParamItem(-1);
                            bubbleItem.hLiq = sumLiq;
                            bubbleItem.hLiqV = sumLiqV;
                            bubbleItem.hOil = sumOil;
                            bubbleItem.hOilV = sumOilV;
                            bubbleItem.hInj = sumInj;
                            bubbleItem.hInjGas = sumInjGas / 1000;
                            bubbleItem.NatGas = sumNatGas / 1000;
                            bubbleItem.GasCond = sumGasCond;
                            wtItem = ProdWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 744 * StartAveMonth)
                            {
                                newWtItem = new MerWorkTimeItem();
                                newWtItem.WorkTime = 744 * StartAveMonth;
                                newWtItem.CollTime = 0;
                                ProdWorkTime.Clear();
                                ProdWorkTime.Add(newWtItem);
                            }
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                bubbleItem.HistProdWorkTime = ProdWorkTime;
                                ProdWorkTime = new MerWorkTimeCollection();
                            }
                            wtItem = InjWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 744 * StartAveMonth)
                            {
                                newWtItem = new MerWorkTimeItem();
                                newWtItem.WorkTime = 744 * StartAveMonth;
                                newWtItem.CollTime = 0;
                                InjWorkTime.Clear();
                                InjWorkTime.Add(newWtItem);
                            }
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                bubbleItem.HistInjWorkTime = InjWorkTime;
                                InjWorkTime = new MerWorkTimeCollection();
                            }
                            w.BubbleList.Add(bubbleItem);
                            w.BubbleList.hBySector = ViewBySector;
                            w.BubbleList.K = K;
                        }

                        if (lastMonth)
                        {
                            maxDT = w.mer.Items[w.mer.Count - 1].Date;
                            j = w.mer.Count - 1;
                            while ((j > -1) && (maxDT == w.mer.Items[j].Date))
                            {
                                j--;
                            }
                            j++;
                        }
                        else
                        {
                            while (j < w.mer.Count)
                            {
                                if (w.mer.Items[j].Date < maxDT)
                                    j++;
                                else
                                    break;
                            }
                        }
                        sumLiq = 0; sumOil = 0; sumInj = 0; sumNatGas = 0; sumGasCond = 0; sumLiqV = 0; sumOilV = 0;
                        ProdWorkTime.Clear();
                        InjWorkTime.Clear();

                        ave = LastAveMonth;
                        dt = maxDT;
                        while (ave > 0)
                        {
                            lastPlastId = -1;
                            ind = 0;
                            while ((j < w.mer.Count) && (dt == w.mer.Items[j].Date))
                            {
                                item = w.mer.Items[j];
                                if (lastPlastId != item.PlastId)
                                {
                                    ind = dict.GetIndexByCode(item.PlastId);
                                    lastPlastId = item.PlastId;
                                }
                                if (ind > 0)
                                {
                                    if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);
                                    bubbleItem = w.BubbleList.GetItemByObjIndex(ind);
                                    if (bubbleItem == null)
                                    {
                                        bubbleItem = new BubbleParamItem(ind);
                                        w.BubbleList.Add(bubbleItem);
                                    }
                                    if (item.CharWorkId == 11)
                                    {
                                        bubbleItem.Liq += item.Oil + item.Wat;
                                        bubbleItem.Oil += item.Oil;
                                        sumLiq += item.Oil + item.Wat;
                                        sumOil += item.Oil;
                                        bubbleItem.LiqV += item.OilV + item.WatV;
                                        bubbleItem.OilV += item.OilV;
                                        sumLiqV += item.OilV + item.WatV;
                                        sumOilV += item.OilV;
                                        if (bubbleItem.ProdWorkTime == null) bubbleItem.ProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.ProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 12)
                                    {
                                        bubbleItem.NatGas += w.mer.GetItemEx(j).NaturalGas;
                                        sumNatGas += w.mer.GetItemEx(j).NaturalGas;
                                        if (bubbleItem.ProdWorkTime == null) bubbleItem.ProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.ProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 15)
                                    {
                                        bubbleItem.GasCond += w.mer.GetItemEx(j).GasCondensate;
                                        sumGasCond += w.mer.GetItemEx(j).GasCondensate;
                                        if (bubbleItem.ProdWorkTime == null) bubbleItem.ProdWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.ProdWorkTime.Add(w.mer, j);
                                        ProdWorkTime.Add(w.mer, j);
                                    }
                                    else if (item.CharWorkId == 20)
                                    {
                                        int agent = w.mer.GetItemEx(j).AgentInjId;
                                        if ((agent > 80) && (agent < 90))
                                        {

                                            bubbleItem.InjGas += w.mer.GetItemEx(j).Injection;
                                            sumInjGas += w.mer.GetItemEx(j).Injection;
                                        }
                                        else
                                        {
                                            bubbleItem.Inj += w.mer.GetItemEx(j).Injection;
                                            sumInj += w.mer.GetItemEx(j).Injection;
                                        }
                                        if (bubbleItem.InjWorkTime == null) bubbleItem.InjWorkTime = new MerWorkTimeCollection();
                                        bubbleItem.InjWorkTime.Add(w.mer, j);
                                        InjWorkTime.Add(w.mer, j);
                                    }
                                }
                                j++;
                            }
                            if (j >= w.mer.Count) break;
                            dt = w.mer.Items[j].Date;
                            ave--;
                        }

                        if (w.BubbleList != null)
                        {
                            bubbleItem = w.BubbleList.GetItemByObjIndex(-1);
                            if (bubbleItem == null)
                            {
                                bubbleItem = new BubbleParamItem(-1);
                                w.BubbleList.Add(bubbleItem);
                            }
                            bubbleItem.Liq = sumLiq;
                            bubbleItem.LiqV = sumLiqV;
                            bubbleItem.Oil = sumOil;
                            bubbleItem.OilV = sumOilV;
                            bubbleItem.Inj = sumInj;
                            bubbleItem.InjGas = sumInjGas / 1000;
                            bubbleItem.NatGas = sumNatGas / 1000;
                            bubbleItem.GasCond = sumGasCond;

                            wtItem = ProdWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 744 * LastAveMonth)
                            {
                                newWtItem = new MerWorkTimeItem();
                                newWtItem.WorkTime = 744 * LastAveMonth;
                                newWtItem.CollTime = 0;
                                ProdWorkTime.Clear();
                                ProdWorkTime.Add(newWtItem);
                            }
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                bubbleItem.ProdWorkTime = ProdWorkTime;
                                ProdWorkTime = new MerWorkTimeCollection();
                            }
                            wtItem = InjWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 744 * LastAveMonth)
                            {
                                newWtItem = new MerWorkTimeItem();
                                newWtItem.WorkTime = 744 * LastAveMonth;
                                newWtItem.CollTime = 0;
                                InjWorkTime.Clear();
                                InjWorkTime.Add(newWtItem);
                            }
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                bubbleItem.InjWorkTime = InjWorkTime;
                                InjWorkTime = new MerWorkTimeCollection();
                            }
                            w.BubbleList.hBySector = ViewBySector;
                            w.BubbleList.K = K;
                        }
                    }
                    if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);
                    SetBubbleIconsList(w, maxDT, filter);
                    if (w.BubbleList != null)
                    {
                        w.BubbleList.SetBubbleByFilter(filter);
                        w.BubbleList.hBySector = ViewBySector;
                        w.BubbleList.K = K;
                    }
                    worker.ReportProgress((int)((float)i * 100 / (float)Wells.Count), userState);
                }
                CreateSortedWellList();
                userState.Element = this.ParamsDict.OilFieldName;
                worker.ReportProgress(100, userState);
                this.BubbleMapLoaded = true;
                return true;
            }
            #endregion
            else if (DataSource && this.ChessLoaded)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Расчет карты изменений отборов";
                userState.WorkCurrentTitle = "Расчет карты изменений отборов";
                int i, j, p, ave, ind, lastPlastId;
                Well w;
                ChessItem item;
                ChessInjItem itemInj;
                SortedWells.Clear();
                bool startMonth = minDT == DateTime.MinValue;
                bool lastMonth = maxDT == DateTime.MaxValue;
                if (StartAveMonth == 0) StartAveMonth++;
                if (LastAveMonth == 0) LastAveMonth++;
                DateTime dt;
                BubbleParamItem bubbleItem;

                MerWorkTimeItem wtItem;
                MerWorkTimeCollection ProdWorkTime = new MerWorkTimeCollection();
                MerWorkTimeCollection InjWorkTime = new MerWorkTimeCollection();

                if (startMonth)
                {
                    minDT = new DateTime(this.maxMerDate.Year, 1, 1);
                }
                if (lastMonth)
                {
                    maxDT = maxMerDate;
                }
                maxDT = maxDT.AddDays(1 - LastAveMonth);

                this.project.BubbleCalcDT1 = minDT;
                this.project.BubbleCalcDT2 = maxDT;
                this.project.BubbleHistoryByChess = DataSource;
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];

                    userState.Element = w.UpperCaseName;
                    #region Начальная дата
                    if (w.ChessLoaded && (w.chess != null))
                    {
                        j = 0;
                        while (j < w.chess.Count)
                        {
                            if (w.chess[j].Date < minDT)
                            {
                                j++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        ave = StartAveMonth;
                        dt = minDT;
                        while (ave > 0)
                        {
                            if (j < w.chess.Count)
                            {
                                item = w.chess[j];
                                if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);

                                bubbleItem = w.BubbleList.GetItemByObjIndex(-1);
                                if (bubbleItem == null)
                                {
                                    bubbleItem = new BubbleParamItem(-1);
                                    w.BubbleList.Add(bubbleItem);
                                }
                                if (item.StayTime < 24)
                                {
                                    bubbleItem.hLiq += item.Qliq;
                                    bubbleItem.hOil += item.Qoil;
                                    bubbleItem.hLiqV += item.QliqV;
                                    bubbleItem.hOilV += item.QoilV;
                                    if (bubbleItem.HistProdWorkTime == null) bubbleItem.HistProdWorkTime = new MerWorkTimeCollection();
                                    wtItem = new MerWorkTimeItem();
                                    wtItem.StateId = ave;
                                    wtItem.WorkTime = 24;
                                    bubbleItem.HistProdWorkTime.Add(wtItem);
                                }
                                while ((j < w.chess.Count) && (dt == w.chess[j].Date)) j++;
                                if ((j <= -1) || (j >= w.chess.Count)) break;
                                dt = w.chess[j].Date;
                            }
                            ave--;
                        }
                    }
                    if (w.ChessInjLoaded && (w.chessInj != null))
                    {
                        j = 0;
                        while (j < w.chessInj.Count)
                        {
                            if (w.chessInj[j].Date < minDT)
                            {
                                j++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        ave = StartAveMonth;
                        dt = minDT;
                        while (ave > 0)
                        {
                            if (j < w.chessInj.Count)
                            {
                                itemInj = w.chessInj[j];
                                if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);

                                bubbleItem = w.BubbleList.GetItemByObjIndex(-1);
                                if (bubbleItem == null)
                                {
                                    bubbleItem = new BubbleParamItem(-1);
                                    w.BubbleList.Add(bubbleItem);
                                }
                                if (itemInj.StayTime < 24)
                                {
                                    bubbleItem.hInj += itemInj.Wwat;
                                    if (bubbleItem.HistInjWorkTime == null) bubbleItem.HistInjWorkTime = new MerWorkTimeCollection();
                                    wtItem = new MerWorkTimeItem();
                                    wtItem.StateId = ave;
                                    wtItem.WorkTime = 24;
                                    bubbleItem.HistInjWorkTime.Add(wtItem);
                                }
                                while ((j < w.chessInj.Count) && (dt == w.chessInj[j].Date)) j++;
                                if ((j <= -1) || (j >= w.chessInj.Count)) break;
                                dt = w.chessInj[j].Date;
                            }
                            ave--;
                        }
                    }
                    #endregion

                    #region Конечная дата
                    if (w.ChessLoaded && (w.chess != null))
                    {
                        j = 0;
                        while (j < w.chess.Count)
                        {
                            if (w.chess[j].Date < maxDT)
                            {
                                j++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        ave = LastAveMonth;
                        dt = minDT;
                        while (ave > 0)
                        {
                            if (j < w.chess.Count)
                            {
                                item = w.chess[j];
                                if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);

                                bubbleItem = w.BubbleList.GetItemByObjIndex(-1);
                                if (bubbleItem == null)
                                {
                                    bubbleItem = new BubbleParamItem(-1);
                                    w.BubbleList.Add(bubbleItem);
                                }
                                if (item.StayTime < 24)
                                {
                                    bubbleItem.Liq += item.Qliq;
                                    bubbleItem.Oil += item.Qoil;
                                    bubbleItem.LiqV += item.QliqV;
                                    bubbleItem.OilV += item.QoilV;
                                    if (bubbleItem.ProdWorkTime == null) bubbleItem.ProdWorkTime = new MerWorkTimeCollection();
                                    wtItem = new MerWorkTimeItem();
                                    wtItem.StateId = ave;
                                    wtItem.WorkTime = 24;
                                    bubbleItem.ProdWorkTime.Add(wtItem);
                                }
                                while ((j < w.chess.Count) && (dt == w.chess[j].Date)) j++;
                                if ((j <= -1) || (j >= w.chess.Count)) break;
                                dt = w.chess[j].Date;
                            }
                            ave--;
                        }
                    }
                    if (w.ChessInjLoaded && (w.chessInj != null))
                    {
                        j = 0;
                        while (j < w.chessInj.Count)
                        {
                            if (w.chessInj[j].Date < maxDT)
                            {
                                j++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        ave = LastAveMonth;
                        dt = minDT;
                        while (ave > 0)
                        {
                            if (j < w.chessInj.Count)
                            {
                                itemInj = w.chessInj[j];
                                if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);

                                bubbleItem = w.BubbleList.GetItemByObjIndex(-1);
                                if (bubbleItem == null)
                                {
                                    bubbleItem = new BubbleParamItem(-1);
                                    w.BubbleList.Add(bubbleItem);
                                }
                                if (itemInj.StayTime < 24)
                                {
                                    bubbleItem.Inj += itemInj.Wwat;
                                    if (bubbleItem.InjWorkTime == null) bubbleItem.InjWorkTime = new MerWorkTimeCollection();
                                    wtItem = new MerWorkTimeItem();
                                    wtItem.StateId = ave;
                                    wtItem.WorkTime = 24;
                                    bubbleItem.InjWorkTime.Add(wtItem);
                                }
                                while ((j < w.chessInj.Count) && (dt == w.chessInj[j].Date)) j++;
                                if ((j <= -1) || (j >= w.chessInj.Count)) break;
                                dt = w.chessInj[j].Date;
                            }
                            ave--;
                        }
                    }
                    #endregion
                    if (w.BubbleList == null) w.BubbleList = new BubbleParamList(Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY, LiquidInCube);
                    //SetBubbleIconsList(w, dt, filter);
                    if (w.BubbleList != null)
                    {
                        w.BubbleList.SetBubbleByFilter(filter);
                        w.BubbleList.hBySector = ViewBySector;
                        w.BubbleList.K = K;
                    }
                    worker.ReportProgress((int)((float)i * 100 / (float)Wells.Count), userState);
                }
                ClearChessData(false);
                dt = new DateTime(maxDT.Year, maxDT.Month, 1);
                if (dt > this.maxMerDate) dt = maxMerDate;
                if (!MerLoaded) LoadMerFromCache(worker, e);
                for (i = 0; i < Wells.Count; i++)
                {
                    SetBubbleIconsList((Well)Wells[i], dt, filter);
                }
                CreateSortedWellList();
                userState.Element = this.ParamsDict.OilFieldName;
                worker.ReportProgress(100, userState);
                this.BubbleMapLoaded = true;
                return true;
            }
            return false;
        }
        public bool UpdateBubbleMap(BackgroundWorker worker, DoWorkEventArgs e, bool LiquidInCube, float K, FilterOilObjects filter)
        {
            if (this.BubbleMapLoaded)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Обновление карты текущих отборов";
                userState.Element = this.Name;
                Well w;
                int i;
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    if (w.BubbleList != null)
                    {
                        w.BubbleList.LiquidInCube = LiquidInCube;
                        w.BubbleList.K = K;
                        w.BubbleList.SetBubbleByFilter(filter);
                    }
                    worker.ReportProgress((int)((float)i * 100 / (float)Wells.Count), userState);
                }
                CreateSortedWellList();
                worker.ReportProgress(100, userState);
                return true;
            }
            return false;
        }
        public bool UpdateBubbleAccumMap(BackgroundWorker worker, DoWorkEventArgs e, bool LiquidInCube, float K, FilterOilObjects filter)
        {
            if (this.BubbleMapLoaded)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Обновление карты накопленных отборов";
                userState.Element = this.Name;
                Well w;
                int i;
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    if (w.BubbleList != null)
                    {
                        w.BubbleList.LiquidInCube = LiquidInCube;
                        w.BubbleList.K = K;
                        w.BubbleList.SetBubbleByFilter(filter);
                    }
                    worker.ReportProgress((int)((float)i * 100 / (float)Wells.Count), userState);
                }
                worker.ReportProgress(100, userState);
                return true;
            }
            return false;
        }
        public bool UpdateBubbleHistoryMap(BackgroundWorker worker, DoWorkEventArgs e, bool LiquidInCube, bool ViewBySector, float K, FilterOilObjects filter)
        {
            if (this.BubbleMapLoaded)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Обновление карты изменений отборов";
                userState.Element = this.Name;
                Well w;
                int i;
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    if (w.BubbleList != null)
                    {
                        w.BubbleList.hBySector = ViewBySector;
                        w.BubbleList.K = K;
                        w.BubbleList.LiquidInCube = LiquidInCube;
                        w.BubbleList.SetBubbleByFilter(filter);
                    }
                    worker.ReportProgress((int)((float)i * 100 / (float)Wells.Count), userState);
                }
                worker.ReportProgress(100, userState);
                return true;
            }
            return false;
        }
        public void SetBubbleIconsList(Well w, DateTime MaxDate, FilterOilObjects filter)
        {
            var dict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            w.BubbleIcons = w.GetIcons(this.CoefDict, MaxDate);
            WellIcon allIcon = w.icons.GetAllIcon();
            if (allIcon != null)
            {
                w.BubbleIcons.AddEmptyIcon(allIcon.X, allIcon.Y);
            }
            else
            {
                w.BubbleIcons.AddEmptyIcon(w.X, w.Y);
            }
            if ((w.icons != null) && (w.icons.Count > 0))
            {
                w.BubbleIcons.SetFonts(w.icons[0].iconFonts, w.icons[0].ExIconFonts);
            }
            if (filter.AllSelected)
            {
                w.SetActiveAllIcon();
            }
            else
            {
                int k, ind;
                WellIcon well_icon = null;
                if (w.BubbleIcons.Count < 2)
                {
                    well_icon = w.BubbleIcons.GetActiveIcon();
                }
                else
                {
                    for (k = 0; k < filter.SelectedObjects.Count; k++)
                    {
                        well_icon = w.BubbleIcons.GetIconByCode(dict[(int)filter.SelectedObjects[k]].Code);
                        if (well_icon != null) break;
                    }
                }
                if (well_icon != null)
                {
                    w.SetActiveIcon(well_icon.StratumCode);
                }
                else
                {
                    double xG, yG;
                    bool findCoord = false;
                    if (w.coord.Count > 0)
                    {
                        for (k = 0; k < filter.SelectedObjects.Count; k++)
                        {
                            ind = w.coord.GetIndexByPlastCode(dict[(int)filter.SelectedObjects[k]].Code);
                            if (ind != -1 && (w.coord.Items[ind].X + w.coord.Items[ind].Y != 0))
                            {
                                xG = w.coord.Items[ind].X;
                                yG = w.coord.Items[ind].Y;
                                //CoefDict.GlobalCoordFromLocal(w.coord.Items[ind].X, w.coord.Items[ind].Y, out xG, out yG);
                                w.BubbleIcons.SetEmptyIcon(xG, yG);
                                w.X = xG;
                                w.Y = yG;
                                findCoord = true;
                                break;
                            }
                        }
                    }
                    if (!findCoord)
                    {
                        w.BubbleIcons.SetEmptyIcon(w.X, w.Y);
                    }
                }
            }
            w.SetBrushFromIcon();
        }
        public void ClearBubbleMap(BackgroundWorker worker, DoWorkEventArgs e)
        {
            this.SortedWells.Clear();
            Well w;
            if (WellsLayer != null)
            {
                WellsLayer.ObjectsList.Clear();
                for (int i = 0; i < Wells.Count; i++)
                {
                    WellsLayer.ObjectsList.Add((Well)Wells[i]);
                }
            }

            for (int i = 0; i < Wells.Count; i++)
            {
                w = (Well)Wells[i];
                if (w.BubbleList != null)
                {
                    w.BubbleIcons.Clear();
                    w.BubbleIcons = null;
                    w.SetBrushFromIcon();
                    w.BubbleList.Clear();
                    w.BubbleList = null;
                }
                if (w.icons != null) w.icons.IsDefaultIconsColor = false;
            }
            BubbleMapLoaded = false;
        }
        #endregion

        #region Load New Project

        // Stratum Codes
        public bool LoadStratumCodesFromCache2()
        {
            bool retValue = false;
            string cacheName = project.path + "\\cache\\" + Name + "\\OilObj.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int i, len;
                int Version = br.ReadInt32();

                if (Version == 0)
                {
                    len = br.ReadInt32();
                    for (i = 0; i < len; i++) MerStratumCodes.Add(br.ReadInt32());

                    if (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        minMerDate = DateTime.FromOADate(br.ReadDouble());
                        maxMerDate = DateTime.FromOADate(br.ReadDouble());
                    }
                }
                else if (Version == 1)
                {
                    len = br.ReadInt32();
                    for (i = 0; i < len; i++) MerStratumCodes.Add(br.ReadInt32());

                    if (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        minMerDate = DateTime.FromOADate(br.ReadDouble());
                        maxMerDate = DateTime.FromOADate(br.ReadDouble());
                    }

                    len = br.ReadInt32();
                    for (i = 0; i < len; i++) GeoStratumCodes.Add(br.ReadInt32());
                }
                else if (Version == 2)
                {
                    len = br.ReadInt32();
                    for (i = 0; i < len; i++) GeoStratumCodes.Add(br.ReadInt32());

                    len = br.ReadInt32();
                    for (i = 0; i < len; i++) MerStratumCodes.Add(br.ReadInt32());

                    if (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        minMerDate = DateTime.FromOADate(br.ReadDouble());
                        maxMerDate = DateTime.FromOADate(br.ReadDouble());
                    }
                }
                br.Close();
                retValue = true;
            }
            return retValue;
        }
        public bool WriteStratumCodesToCache2()
        {
            string cacheName = project.path + "\\cache\\" + Name + "\\OilObj.bin";

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);

            bw.Write(VersionStratumList);

            bw.Write(GeoStratumCodes.Count);
            for (int i = 0; i < GeoStratumCodes.Count; i++) bw.Write(GeoStratumCodes[i]);

            bw.Write(MerStratumCodes.Count);
            for (int i = 0; i < MerStratumCodes.Count; i++) bw.Write(MerStratumCodes[i]);

            bw.Write(minMerDate.ToOADate());
            bw.Write(maxMerDate.ToOADate());
            bw.Close();
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);

            return true;
        }
        // Wells
        public bool LoadWellsFromCache2(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка скважин из кэша";
            userState.Element = Name;
            string cacheName = project.path + "\\cache\\" + Name + "\\wellList.bin";
            try
            {
                if (!File.Exists(cacheName))
                {
                    userState.Error = "Не найден файл списка скважин";
                    worker.ReportProgress(100, userState);
                    return false;
                }
                Well w;
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);

                worker.ReportProgress(0, userState);

                if (br.ReadInt32() == Well.Version)
                {
                    int count = br.ReadInt32();
                    bool inserted;
                    if (Wells == null) Wells = new List<Well>(count);
                    for (int i = 0; i < count; i++)
                    {
                        w = new Well(i);
                        w.OilFieldIndex = this.Index;
                        w.OilFieldCode = this.ParamsDict.RDFFieldCode;
                        w.ReadFromCache(br);
                        Wells.Add(w);
                    }
                    // Заполнение SideBores
                    List<int>[] boreIndexes = new List<int>[count];
                    for (int i = 0; i < count; i++)
                    {
                        w = Wells[i];
                        if (w.Index != w.MainBoreIndex)
                        {
                            if (boreIndexes[w.MainBoreIndex] == null) boreIndexes[w.MainBoreIndex] = new List<int>();
                            inserted = false;
                            if (w.BoreId < 0)
                            {
                                for (int j = 0; j < boreIndexes[w.MainBoreIndex].Count; j++)
                                {
                                    if (Wells[boreIndexes[w.MainBoreIndex][j]].BoreId > 0 || (w.BoreId > Wells[boreIndexes[w.MainBoreIndex][j]].BoreId))
                                    {
                                        boreIndexes[w.MainBoreIndex].Insert(j, w.Index);
                                        inserted = true;
                                        break;
                                    }
                                }
                            }
                            if (!inserted)
                            {
                                boreIndexes[w.MainBoreIndex].Add(w.Index);
                            }
                            if (w.OilFieldAreaCode != Wells[w.MainBoreIndex].OilFieldAreaCode) // todo: почему dataloader не пишет OilFieldAreaCode
                            {
                                w.OilFieldAreaCode = Wells[w.MainBoreIndex].OilFieldAreaCode;
                            }
                        }
                    }
                    for (int i = 0; i < count; i++)
                    {
                        if (boreIndexes[i] != null)
                        {
                            Wells[i].SideBores = new Well[boreIndexes[i].Count];
                            for (int j = 0; j < boreIndexes[i].Count; j++)
                            {
                                Wells[i].SideBores[j] = Wells[boreIndexes[i][j]];
                                Wells[i].SideBores[j].Name = string.Format("{0}С{1}", Wells[i].Name, j + 1);
                            }
                        }
                    }
                    retValue = true;
                }
                br.Close();
            }
            catch
            {
                retValue = false;
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadCoordFromCache2(BackgroundWorker worker, DoWorkEventArgs e)
        {
            CoordLoaded = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка скважин из кэша";
            userState.Element = Name;
            string cacheName = project.path + "\\cache\\" + Name + "\\coord.bin";
            try
            {
                if (File.Exists(cacheName))
                {
                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    worker.ReportProgress(0, userState);

                    if (SkvCoord.Version != br.ReadInt32())
                    {
                        br.Close();
                        CoordLoaded = false;
                        return false;
                    }

                    int count = br.ReadInt32();
                    if (count != Wells.Count)
                    {
                        br.Close();
                        CoordLoaded = false;
                        return false;
                    }
                    int wellIndex;
                    for (int i = 0; i < count; i++)
                    {
                        wellIndex = br.ReadInt32();
                        if (wellIndex != i)
                        {
                            br.Close();
                            CoordLoaded = false;
                            return false;
                        }
                        Wells[i].coord = new SkvCoord();
                        Wells[i].coord.Read(br);
                        Wells[i].SetStartedCoord();

                        SetMinMaxRect(Wells[i]);

                        Wells[i].icons.Read(br, Wells[i].X, Wells[i].Y);
                        Wells[i].SetActiveAllIcon();
                        Wells[i].SetBrushFromIcon();
                    }
                    br.Close();
                    CoordLoaded = true;
                }
            }
            catch
            {
                CoordLoaded = false;
            }

            worker.ReportProgress(100, userState);
            return CoordLoaded;
        }
        // Areas
        bool LoadAreasFromCache2(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false, res;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка ячеек из кэша";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\areas.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int version = br.ReadInt32();
                if (Area.Version >= version)
                {
                    iLen = br.ReadInt32();
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            Area area = new Area();
                            res = area.ReadFromCache2(worker, e, this, br, version);
                            area.OilFieldIndex = Index;
                            //area.contour.fillBrush = 33;
                            if (res)
                            {
                                area.Index = Areas.Count;
                                Areas.Add(area);
                            }
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen, userState);
                    }
                }
                br.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        // WellPads
        bool LoadWellPadsFromCache2(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка кустов с кеша";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\wellpads.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);

                int count;
                WellPad wp;
                if (WellPad.Version == br.ReadInt32())
                {
                    count = br.ReadInt32();
                    for (int i = 0; i < count; i++)
                    {
                        wp = new WellPad();
                        wp.ReadFromCache(br, this);
                        if (wp.wells.Count > 0)
                        {
                            WellPads.Add(wp);
                            retValue = true;
                        }
                        ReportProgress(worker, i, count, userState);
                    }
                }
                br.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        // Contours
        public bool LoadContoursFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool HeadOnly)
        {
            int i, iLen = 0;
            bool retValue = false;
            ContoursDataLoaded = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка контуров из кэша";
            userState.Element = Name;
            string cacheName = project.path + "\\cache\\" + Name + "\\contours.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int version = br.ReadInt32();
                if (Contour.Version == version)
                {
                    ContoursDataLoaded = true;
                    iLen = br.ReadInt32();
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            Contour cntr = new Contour();
                            if (HeadOnly)
                            {
                                cntr.ReadHeadLineFromCacheBin2(br);
                            }
                            else
                            {
                                cntr.ReadFromCacheBin2(br);
                            }
                            if (cntr._points == null) ContoursDataLoaded = false;
                            if (ParamsDict.NGDUCode == -1)
                            {
                                SetMinMaxRect(cntr);
                            }
                            if (Contours == null) Contours = new List<Contour>();
                            Contours.Add(cntr);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen);
                    }
                }
                br.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        bool LoadContoursDataFromCache2(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка контуров из кэша";
            userState.Element = Name;
            string cacheName = project.path + "\\cache\\" + Name + "\\contours.bin";

            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int version = br.ReadInt32();
                if (Contour.Version >= version)
                {
                    iLen = br.ReadInt32();
                    if (iLen == Contours.Count)
                    {
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                retValue = false;
                                break;
                            }
                            else
                            {
                                Contours[i].ReadFromCacheBin2(br);
                                ContoursDataLoaded = true;
                                retValue = true;
                            }
                            ReportProgress(worker, i, iLen);
                        }
                    }
                }
                br.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        // Grids
        int LoadGridDataFromCache2(BackgroundWorker worker, DoWorkEventArgs e, int GridIndex)
        {
            int iLen = 0, ver;
            long seek;
            int retValue = -1;
            Grid grid = null;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка сетки в память";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\grids.bin";
            System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess();
            if (proc.WorkingSet64 > 1300 * 1024 * 1024)
            {
                retValue = -2;
            }
            else if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                if (Grid.Version == br.ReadInt32())
                {
                    iLen = br.ReadInt32();
                    if ((iLen > 0) && (GridIndex < iLen) && (Grids.Count > 0))
                    {
                        grid = Grids[GridIndex];
                        grid.DataLoading = true;
                        userState.Element = grid.Name;

                        fs.Seek(8 + 8 * GridIndex, SeekOrigin.Begin);
                        seek = br.ReadInt64();
                        fs.Seek(seek, SeekOrigin.Begin);
                        try
                        {
                            if (grid.LoadFromCache(worker, e, fs, true))
                            {
                                retValue = 0;
                            }
                        }
                        catch (OutOfMemoryException exc)
                        {
                            retValue = -2;
                            grid.FreeDataMemory();
                        }
                    }
                }
                br.Close();
                if (grid != null) grid.DataLoading = false;
            }
            return retValue;
        }
        bool LoadGridsHeadFromCache2(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false;
            Grid grid;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка сеток из кэша";
            userState.Element = Name;
            string cacheName = project.path + "\\cache\\" + Name + "\\grids.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                if (Grid.Version == br.ReadInt32())
                {
                    iLen = br.ReadInt32();
                    fs.Seek(8 + 8 * iLen, SeekOrigin.Begin);
                    for (i = 0; i < iLen; i++)
                    {
                        grid = new Grid();
                        grid.Index = i;
                        grid.OilFieldCode = ParamsDict.RDFFieldCode;
                        grid.Date = DateTime.FromOADate(br.ReadDouble());
                        grid.StratumCode = br.ReadInt32();
                        grid.StratumName = br.ReadString();
                        grid.GridType = br.ReadInt32();
                        grid.Name = br.ReadString();
                        Grids.Add(grid);
                    }
                }
                br.Close();
                retValue = true;
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        bool LoadGridDataFromCache2(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false;
            Grid grid;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка сеток из кэша";
            userState.Element = Name;
            string cacheName = project.path + "\\cache\\" + Name + "\\grids.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                long seekStart;
                if (Grid.Version == br.ReadInt32())
                {
                    iLen = br.ReadInt32();
                    seekStart = br.ReadInt64();
                    fs.Seek(seekStart, SeekOrigin.Begin);

                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            grid = new Grid();
                            grid.Index = i;
                            grid.OilFieldCode = ParamsDict.RDFFieldCode;
                            grid.LoadFromCache(worker, e, fs, true);
                            if (Grids == null) Grids = new List<Grid>();
                            Grids.Add(grid);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen);
                    }
                    fs.Seek(4 + 8 * iLen, SeekOrigin.Begin);
                    for (i = 0; i < iLen; i++)
                    {
                        grid = Grids[i];
                        grid.Date = DateTime.FromOADate(br.ReadDouble());
                        grid.StratumCode = br.ReadInt32();
                        grid.StratumName = br.ReadString();
                        grid.GridType = br.ReadInt32();
                        grid.Name = br.ReadString();
                    }
                }
                br.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        // Mer
        bool LoadMerFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных МЭР месторождения";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\mer.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i = 0, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    if (!NotReportProgress) worker.ReportProgress(0, userState);
                    FileStream fs;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    if (Mer.Version == bfr.ReadInt32())
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        minMerDate = DateTime.MaxValue;
                        maxMerDate = DateTime.MinValue;
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                well_index = br.ReadInt32();
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    for (j = 0; j < Wells.Count; j++)
                                    {
                                        w = Wells[j];
                                        w.mer = null;
                                        w.merEx = null;
                                    }
                                    MerLoaded = false;
                                    return false;
                                }
                                if (well_index > -1)
                                {
                                    w = Wells[well_index];
                                    w.mer = new Mer();
                                    w.mer.Read(br, NotLoadData);
                                    if (w.mer.Count > 0)
                                    {
                                        if (w.mer.Items[0].Date < this.minMerDate) this.minMerDate = w.mer.Items[0].Date;
                                        if (w.mer.Items[w.mer.Count - 1].Date > this.maxMerDate) this.maxMerDate = w.mer.Items[w.mer.Count - 1].Date;
                                        if (!NotLoadData) this.MerLoaded = true;
                                    }
                                }
                                retValue = true;
                                if (!NotReportProgress) ReportProgress(worker, wellCount + (i + 1), iLen, userState);
                            }
                            wellCount += well_count;
                        }
                    }
                    if (!NotReportProgress) worker.ReportProgress(100, userState);
                    bfr.Close();
                    FreeBuffers();
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша МЭР!";
                if (!NotReportProgress) worker.ReportProgress(100, userState);
            }
            if (minMerDate == DateTime.MaxValue) minMerDate = DateTime.MinValue;
            if (maxMerDate == DateTime.MinValue) maxMerDate = DateTime.MaxValue;
            return retValue;
        }
        bool LoadMerFromCache2(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;
            string cacheName = project.path + "\\cache\\" + Name + "\\mer.bin";
            if (File.Exists(cacheName))
            {
                int j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (Mer.Version == bfr.ReadInt32())
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        wellCount = 0;
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();
                            if (WellIndex < wellCount + well_count)
                            {
                                ms = GetBuffer(true);
                                unpackMS = GetBuffer(false);
                                ms.SetLength(len_block);
                                br = new BinaryReader(unpackMS);
                                ConvertEx.CopyStream(fs, ms, len_block);
                                ConvertEx.UnPackStream(ms, ref unpackMS);

                                shift = (WellIndex - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();
                                if (WellIndex != well_index)
                                {
                                    retValue = false;
                                }
                                else if (well_index > -1)
                                {
                                    w = Wells[well_index];
                                    w.mer = new Mer();
                                    w.mer.Read(br, NotLoadData);
                                }
                                break;
                            }
                            else
                            {
                                fs.Seek(len_block, SeekOrigin.Current);
                            }
                            wellCount += well_count;
                        }
                    }
                    FreeBuffers();
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        bool LoadMerFromCache2(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных МЭР месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\mer.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int lastIndex = 0;
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (Mer.Version != bfr.ReadInt32())
                    {
                        bfr.Close();
                        fs.Close();
                        return false;
                    }
                    WellIndexes.Sort();
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    lastIndex = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();

                        if (WellIndexes[lastIndex] < wellCount + well_count)
                        {
                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            while ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                            {
                                shift = (WellIndexes[lastIndex] - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();
                                if (WellIndexes[lastIndex] != well_index)
                                {
                                    bfr.Close();
                                    return false;
                                }
                                else
                                {
                                    w = (Well)Wells[well_index];
                                    w.mer = new Mer();
                                    w.mer.Read(br, false);
                                    if (w.mer.Count > 0) MerLoaded = true;
                                }
                                lastIndex++;
                                if (worker != null) ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                            }
                            if (lastIndex >= WellIndexes.Count) break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                    FreeBuffers();
                    bfr.Close();
                    retValue = true;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        // Chess
        bool LoadChessFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных Шахматки месторождения";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\chess.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    WellChess chess;
                    WellChessInj chessInj;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;

                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    if (fs.Length == 0)
                    {
                        fs.Close();
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (WellChess.Version == version)
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                well_index = br.ReadInt32();
                                if (well_index < Wells.Count)
                                {
                                    w = Wells[well_index];
                                    userState.Element = w.Name;

                                    // Шахматка добывающих
                                    chess = new WellChess();
                                    if (chess.ReadFromCache(br, version))
                                    {
                                        if (!NotLoadData) w.chess = chess;
                                        ChessLoaded = true;
                                    }

                                    // Шахм нагнетатальных
                                    chessInj = new WellChessInj();
                                    if (chessInj.ReadFromCache(br, version))
                                    {
                                        if (!NotLoadData) w.chessInj = chessInj;
                                        ChessLoaded = true;
                                    }
                                }
                                retValue = true;
                                if (!NotReportProgress) ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                            wellCount += well_count;
                        }
                    }
                    bfr.Close();
                    FreeBuffers();
                }
            }
            return retValue;
        }
        bool LoadChessFromCache2(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;
            string cacheName = project.path + "\\cache\\" + Name + "\\chess.bin";
            if (File.Exists(cacheName))
            {
                int j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                WellChess chess;
                WellChessInj chessInj;

                FileStream fs = null;
                MemoryStream ms, unpackMS;
                BinaryReader br;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (WellChess.Version == version)
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        wellCount = 0;
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            wellBlockCount = bfr.ReadInt32();
                            if (WellIndex < wellCount + wellBlockCount)
                            {
                                ms = GetBuffer(true);
                                unpackMS = GetBuffer(false);
                                ms.SetLength(len_block);
                                br = new BinaryReader(unpackMS);
                                ConvertEx.CopyStream(fs, ms, len_block);
                                ConvertEx.UnPackStream(ms, ref unpackMS);

                                shift = (WellIndex - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();

                                if (WellIndex != well_index)
                                {
                                    retValue = false;
                                }
                                else if (well_index > -1)
                                {
                                    w = Wells[well_index];

                                    // Шахматка добывающих
                                    chess = new WellChess();
                                    if (chess.ReadFromCache(br, version))
                                    {
                                        if (!NotLoadData) w.chess = chess;
                                        ChessLoaded = true;
                                    }

                                    // Шахм нагнетатальных
                                    chessInj = new WellChessInj();
                                    if (chessInj.ReadFromCache(br, version))
                                    {
                                        if (!NotLoadData) w.chessInj = chessInj;
                                        ChessLoaded = true;
                                    }
                                }
                                break;
                            }
                            else
                            {
                                fs.Seek(len_block, SeekOrigin.Current);
                            }
                            wellCount += wellBlockCount;
                        }
                    }
                }
                catch (Exception)
                {
                    retValue = false;
                    FreeBuffers();
                }
                finally
                {
                    FreeBuffers();
                    if (fs != null) fs.Close();
                }
            }
            return retValue;
        }
        bool LoadChessFromCache2(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных Шахматки месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\chess.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, chess_day_len, well_index = -1, iLen, wellCount = 0;
                int chess_inj_day_len;
                int shift, lastIndex;
                Well w;
                WellChess chess;
                WellChessInj chessInj;

                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;
                if (worker != null) worker.ReportProgress(0, userState);

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (WellChess.Version == version)
                {
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    lastIndex = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();
                        if ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                        {
                            ms = this.GetBuffer(true);
                            unpackMS = this.GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            while ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                            {
                                if (worker != null && worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearChessData(false);
                                    return false;
                                }
                                shift = (WellIndexes[lastIndex] - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();

                                if (WellIndexes[lastIndex] != well_index)
                                {
                                    FreeBuffers();
                                    bfr.Close();
                                    return false;
                                }
                                else if (well_index > -1)
                                {
                                    w = (Well)Wells[well_index];
                                    w = Wells[well_index];

                                    // Шахматка добывающих
                                    chess = new WellChess();
                                    if (chess.ReadFromCache(br, version))
                                    {
                                        w.chess = chess;
                                        ChessLoaded = true;
                                    }

                                    // Шахм нагнетатальных
                                    chessInj = new WellChessInj();
                                    if (chessInj.ReadFromCache(br, version))
                                    {
                                        w.chessInj = chessInj;
                                        ChessLoaded = true;
                                    }
                                }
                                lastIndex++;
                                if (worker != null)
                                {
                                    ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                                }
                            }
                            if (lastIndex >= WellIndexes.Count) break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                }
                FreeBuffers();
                bfr.Close();
            }
            return ChessLoaded;
        }
        // Gis
        bool LoadGisFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГИС месторождения";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\gis.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, well_count, gis_len, well_index = -1, iLen, wellCount = 0;
                    int lenItem = 65;
                    Well w;
                    FileStream fs;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    if (SkvGIS.Version == bfr.ReadInt32())
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();

                        MemoryStream ms = GetBuffer(true);
                        MemoryStream unpackMS = GetBuffer(false);
                        BinaryReader br = new BinaryReader(unpackMS);
                        if (!NotReportProgress) worker.ReportProgress(0, userState);
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ClearMemoryStream(ms);
                            ms.SetLength(len_block);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ClearMemoryStream(unpackMS);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                well_index = br.ReadInt32();
                                gis_len = br.ReadInt32();
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    for (j = 0; j < Wells.Count; j++)
                                    {
                                        Wells[j].gis = null;
                                    }
                                    GisLoaded = false;
                                    return false;
                                }
                                if ((well_index > -1) && (gis_len > 0))
                                {
                                    w = Wells[well_index];
                                    if (!NotLoadData)
                                    {
                                        if (w.gis == null) w.gis = new SkvGIS();
                                        SkvGisItem[] items = new SkvGisItem[gis_len];
                                        for (j = 0; j < gis_len; j++)
                                        {
                                            items[j] = w.gis.RetrieveItem(br);
                                        }
                                        w.gis.SetItems(items);
                                    }
                                    else
                                    {
                                        unpackMS.Seek(gis_len * lenItem, SeekOrigin.Current);
                                    }
                                    if (!NotLoadData) GisLoaded = true;
                                }
                                retValue = true;
                                if (!NotReportProgress) ReportProgress(worker, wellCount + (i + 1), iLen, userState);
                            }
                            wellCount += well_count;
                        }
                        if (iLen == 0 && !NotReportProgress) worker.ReportProgress(100, userState);
                    }
                    bfr.Close();
                    FreeBuffers();
                }
            }
            return retValue;
        }
        bool LoadGisFromCache2(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;
            string cacheName = project.path + "\\cache\\" + Name + "\\gis.bin";
            if (File.Exists(cacheName))
            {
                int j, k, num_blocks, unpack_len_block, len_block, well_count, gis_len, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                if (SkvGIS.Version == bfr.ReadInt32())
                {
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;

                    MemoryStream ms = GetBuffer(true);
                    MemoryStream unpackMS = GetBuffer(false);
                    BinaryReader br = new BinaryReader(unpackMS);

                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();
                        if (WellIndex < wellCount + well_count)
                        {
                            ms.SetLength(len_block);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            shift = (WellIndex - wellCount) * 4;
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            shift = br.ReadInt32();
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            well_index = br.ReadInt32();
                            gis_len = br.ReadInt32();
                            if (WellIndex != well_index)
                            {
                                retValue = false;
                            }
                            else if ((well_index > -1) && (gis_len > 0))
                            {
                                w = Wells[well_index];
                                if (!NotLoadData)
                                {
                                    if (w.gis == null) w.gis = new SkvGIS();
                                    SkvGisItem[] items = new SkvGisItem[gis_len];
                                    for (j = 0; j < gis_len; j++)
                                    {
                                        items[j] = w.gis.RetrieveItem(br);
                                    }
                                    w.gis.SetItems(items);
                                }
                            }
                            break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                }
                bfr.Close();
                FreeBuffers();
            }
            return retValue;
        }
        public bool LoadGisFromCache2(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГИС месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\gis.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int lastIndex = 0;
                int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift, gis_len;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (SkvGIS.Version == bfr.ReadInt32())
                    {
                        WellIndexes.Sort();
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        wellCount = 0;
                        lastIndex = 0;
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            if (WellIndexes[lastIndex] < wellCount + well_count)
                            {
                                ms = this.GetBuffer(true);
                                unpackMS = this.GetBuffer(false);
                                ms.SetLength(len_block);
                                br = new BinaryReader(unpackMS);
                                ConvertEx.CopyStream(fs, ms, len_block);
                                ConvertEx.UnPackStream(ms, ref unpackMS);
                    
                                while ((lastIndex < WellIndexes.Count) && (WellIndexes[lastIndex] < wellCount + well_count))
                                {
                                    shift = (WellIndexes[lastIndex] - wellCount) * 4;
                                    unpackMS.Seek(shift, SeekOrigin.Begin);
                                    shift = br.ReadInt32();
                                    unpackMS.Seek(shift, SeekOrigin.Begin);
                                    well_index = br.ReadInt32();
                                    if (WellIndexes[lastIndex] != well_index)
                                    {
                                        bfr.Close();
                                        return false;
                                    }
                                    else
                                    {
                                        w = (Well)Wells[well_index];
                                        gis_len = br.ReadInt32();
                                        if (w.gis == null) w.gis = new SkvGIS();
                                        SkvGisItem[] items = new SkvGisItem[gis_len];
                                        for (j = 0; j < gis_len; j++)
                                        {
                                            items[j] = w.gis.RetrieveItem(br);
                                        }
                                        w.gis.SetItems(items);
                                        GisLoaded = w.gis.Count > 0;
                                    }
                                    lastIndex++;
                                    if (worker != null) ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                                }
                                if (lastIndex >= WellIndexes.Count) break;
                }
                            else
                            {
                                fs.Seek(len_block, SeekOrigin.Current);
                            }
                            wellCount += well_count;
                        }
                    }
                    FreeBuffers();
                bfr.Close();
                    retValue = true;
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }

        // Perforation
        bool LoadPerfFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных перфорации месторождения";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\perf.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, iLen, well_index, perf_count;
                    long shift;
                    long[] shiftList;
                    Well w;

                    if (!NotReportProgress) worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                        BinaryReader bfr = new BinaryReader(fs);
                        if (SkvPerf.Version == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearPerfData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                perf_count = bfr.ReadInt32();

                                if ((well_index > -1) && (perf_count > 0))
                                {
                                    w = Wells[well_index];
                                    if (!NotLoadData)
                                    {
                                        if (w.perf == null) w.perf = new SkvPerf();
                                        SkvPerfItem[] items = new SkvPerfItem[perf_count];
                                        for (j = 0; j < perf_count; j++)
                                        {
                                            items[j] = w.perf.RetrieveItem(bfr);
                                        }
                                        w.perf.SetItems(items);
                                    }
                                }
                                retValue = true;
                                if (!NotReportProgress) ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0 && !NotReportProgress) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша перфорации!";
                if (!NotReportProgress) worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        bool LoadPerfFromCache2(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;

            string cacheName = project.path + "\\cache\\" + Name + "\\perf.bin";
            if (File.Exists(cacheName))
            {
                int j, perf_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (SkvPerf.Version == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                        }
                        else if (well_index > -1)
                        {
                            w = Wells[well_index];
                            perf_count = bfr.ReadInt32();
                            if (!NotLoadData && (perf_count > 0))
                            {
                                if (w.perf == null) w.perf = new SkvPerf();
                                SkvPerfItem[] items = new SkvPerfItem[perf_count];

                                for (j = 0; j < perf_count; j++)
                                {
                                    items[j] = w.perf.RetrieveItem(bfr);
                                }
                                w.perf.SetItems(items);
                            }
                            retValue = true;
                        }
                    }
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        public bool LoadPerfFromCache2(BackgroundWorker worker, DoWorkEventArgs e, List<int> WellIndexes)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных перфорации месторождения";
            userState.Element = this.Name;

            string cacheName = this.project.path + "\\cache\\" + this.Name + "\\perf.bin";
            if (File.Exists(cacheName) && (WellIndexes.Count > 0))
            {
                int lastIndex = 0;
                int i, j, well_index = -1, iLen;
                int perf_count;
                long shift;
                Well w;
                FileStream fs = null;

                try
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (SkvPerf.Version == bfr.ReadInt32())
                    {
                        WellIndexes.Sort();
                        iLen = bfr.ReadInt32();

                        lastIndex = 0;

                        while (lastIndex < WellIndexes.Count && WellIndexes[lastIndex] < iLen)
                        {
                            fs.Seek(8 + 8 * WellIndexes[lastIndex], SeekOrigin.Begin);
                            shift = bfr.ReadInt64();
                            fs.Seek(shift, SeekOrigin.Begin);
                            well_index = bfr.ReadInt32();
                            if (WellIndexes[lastIndex] != well_index)
                            {
                                bfr.Close();
                                return false;
                            }
                            else
                            {
                                w = (Well)Wells[well_index];
                                perf_count = bfr.ReadInt32();
                                if (perf_count > 0)
                                {
                                    if (w.perf == null) w.perf = new SkvPerf();
                                    SkvPerfItem[] items = new SkvPerfItem[perf_count];

                                    for (j = 0; j < perf_count; j++)
                                    {
                                        items[j] = w.perf.RetrieveItem(bfr);
                                    }
                                    w.perf.SetItems(items);
                                    PerfLoaded = w.perf.Count > 0;
                                }
                            }
                            lastIndex++;
                            if (worker != null) ReportProgress(worker, lastIndex, WellIndexes.Count, userState);
                        }
                    }
                    FreeBuffers();
                    bfr.Close();
                    fs = null;
                    retValue = true;
                }
                catch (Exception)
                {
                    if (fs != null) fs.Close();
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        // Logs
        bool LoadLogsBaseFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotLoadValues)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных базового каротажа месторождения";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\logs_base.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, pack_len, logs_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    int maxPackLen = 0, maxUnPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        if (SkvLog.Version == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearLogsBaseData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                pack_len = bfr.ReadInt32();
                                if (maxPackLen < pack_len) maxPackLen = pack_len;
                                if ((well_index > -1) && (pack_len > 0))
                                {
                                    w = Wells[well_index];
                                    if (!NotLoadData)
                                    {
                                        ms = GetBuffer(true);
                                        unpackMS = GetBuffer(false);
                                        ms.SetLength(pack_len);
                                        br = new BinaryReader(unpackMS);
                                        ConvertEx.CopyStream(fs, ms, pack_len);

                                        ConvertEx.UnPackStream(ms, ref unpackMS);
                                        logs_count = br.ReadInt32();
                                        if (logs_count > 0)
                                        {
                                            w.logsBase = new SkvLog[logs_count];
                                            for (j = 0; j < logs_count; j++)
                                            {
                                                if (w.logsBase[j] == null) w.logsBase[j] = new SkvLog();
                                                w.logsBase[j].RetriveItems(br, !NotLoadValues);
                                            }
                                            if (maxUnPackLen < (int)unpackMS.Position) maxUnPackLen = (int)unpackMS.Position;
                                        }
                                    }
                                }

                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                        FreeBuffers();
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        FreeBuffers();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша каротажа!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        bool LoadLogsBaseFromCache2(int WellIndex, bool NotLoadData, bool NotLoadValues)
        {
            bool retValue = false;

            string cacheName = project.path + "\\cache\\" + Name + "\\logs_base.bin";
            if (File.Exists(cacheName))
            {
                int j, pack_len, logs_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);

                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    if (SkvLog.Version == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        pack_len = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                        }
                        else if ((well_index > -1) && (pack_len > 0))
                        {
                            w = Wells[well_index];
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(pack_len);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, pack_len);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            logs_count = br.ReadInt32();
                            if ((!NotLoadData) && (logs_count > 0))
                            {
                                w.logsBase = new SkvLog[logs_count];
                                for (j = 0; j < logs_count; j++)
                                {
                                    if (w.logsBase[j] == null) w.logsBase[j] = new SkvLog();
                                    w.logsBase[j].RetriveItems(br, !NotLoadValues);
                                }
                            }
                        }
                    }
                    bfr.Close();
                    FreeBuffers();
                }
                catch (Exception)
                {
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        bool LoadLogsFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotLoadValues)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных каротажа месторождения";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\logs.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, pack_len, logs_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    int maxPackLen = 0, maxUnPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        if (SkvLog.Version == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearLogsData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                pack_len = bfr.ReadInt32();
                                if (maxPackLen < pack_len) maxPackLen = pack_len;
                                if ((well_index > -1) && (pack_len > 0))
                                {
                                    w = Wells[well_index];
                                    ms = GetBuffer(true);
                                    unpackMS = GetBuffer(false);
                                    ms.SetLength(pack_len);
                                    br = new BinaryReader(unpackMS);
                                    ConvertEx.CopyStream(fs, ms, pack_len);

                                    ConvertEx.UnPackStream(ms, ref unpackMS);
                                    logs_count = br.ReadInt32();
                                    if ((!NotLoadData) && (logs_count > 0))
                                    {
                                        w.logs = new SkvLog[logs_count];
                                        for (j = 0; j < logs_count; j++)
                                        {
                                            if (w.logs[j] == null) w.logs[j] = new SkvLog();
                                            w.logs[j].RetriveItems(br, !NotLoadValues);
                                        }
                                        if (maxUnPackLen < (int)unpackMS.Position) maxUnPackLen = (int)unpackMS.Position;
                                    }
                                }
                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        FreeBuffers();
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        FreeBuffers();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша каротажа!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        bool LoadLogsFromCache2(int WellIndex, bool NotLoadData, bool NotLoadValues)
        {
            bool retValue = false;

            string cacheName = project.path + "\\cache\\" + Name + "\\logs.bin";
            if (File.Exists(cacheName))
            {
                int j, pack_len, logs_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    if (SkvLog.Version == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        pack_len = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                            //MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if ((well_index > -1) && (pack_len > 0))
                        {
                            w = Wells[well_index];
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(pack_len);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, pack_len);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            logs_count = br.ReadInt32();
                            if ((!NotLoadData) && (logs_count > 0))
                            {
                                w.logs = new SkvLog[logs_count];
                                for (j = 0; j < logs_count; j++)
                                {
                                    if (w.logs[j] == null) w.logs[j] = new SkvLog();
                                    w.logs[j].RetriveItems(br, !NotLoadValues);
                                }
                            }
                        }
                    }
                    bfr.Close();
                    FreeBuffers();
                    fs = null;
                }
                catch (Exception)
                {
                    if (fs != null) fs.Close();
                    return false;
                }
            }
            return retValue;
        }
        // Core
        bool LoadCoreFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных керна месторождения";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\core.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, iLen, well_index, core_count;
                    long shift;
                    long[] shiftList;
                    Well w;

                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);

                        BinaryReader bfr = new BinaryReader(fs);
                        if (SkvCore.Version == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearCoreData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                core_count = bfr.ReadInt32();

                                if ((well_index > -1) && (core_count > 0))
                                {
                                    w = Wells[well_index];
                                    if (!NotLoadData)
                                    {
                                        if (w.core == null) w.core = new SkvCore();
                                        SkvCoreItem[] items = new SkvCoreItem[core_count];
                                        for (j = 0; j < core_count; j++)
                                        {
                                            items[j] = w.core.RetrieveItem(bfr);
                                        }
                                        w.core.SetItems(items);
                                    }
                                }
                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша керна!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        bool LoadCoreFromCache2(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;

            string cacheName = project.path + "\\cache\\" + Name + "\\core.bin";
            if (File.Exists(cacheName))
            {
                int i, core_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (SkvCore.Version == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                            //MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if (well_index > -1)
                        {
                            w = Wells[well_index];
                            core_count = bfr.ReadInt32();
                            if ((!NotLoadData) && (core_count > 0))
                            {
                                if (w.core == null) w.core = new SkvCore();
                                SkvCoreItem[] items = new SkvCoreItem[core_count];

                                for (i = 0; i < core_count; i++)
                                {
                                    items[i] = w.core.RetrieveItem(bfr);
                                }
                                w.core.SetItems(items);
                            }
                            retValue = true;
                        }
                    }
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        // CoreTest
        bool LoadCoreTestFromCache2(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных исследований керна месторождения";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\core_test.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, pack_len, core_test_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    int maxPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        if (SkvCoreTest.Version == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearCoreTestData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                pack_len = bfr.ReadInt32();
                                if (maxPackLen < pack_len) maxPackLen = pack_len;
                                if ((well_index > -1) && (pack_len > 0))
                                {
                                    w = Wells[well_index];
                                    ms = GetBuffer(true);
                                    unpackMS = GetBuffer(false);
                                    ms.SetLength(pack_len);
                                    br = new BinaryReader(unpackMS);
                                    ConvertEx.CopyStream(fs, ms, pack_len);

                                    ConvertEx.UnPackStream(ms, ref unpackMS);
                                    core_test_count = br.ReadInt32();
                                    if ((!NotLoadData) && (core_test_count > 0))
                                    {
                                        if (w.coreTest == null) w.coreTest = new SkvCoreTest();
                                        SkvCoreTestItem[] items = new SkvCoreTestItem[core_test_count];

                                        for (j = 0; j < core_test_count; j++)
                                        {
                                            items[j] = w.coreTest.RetrieveItem(br);
                                        }
                                        w.coreTest.SetItems(items);
                                    }
                                }
                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                        FreeBuffers();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша исследований керна!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        bool LoadCoreTestFromCache2(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;

            string cacheName = project.path + "\\cache\\" + Name + "\\core_test.bin";
            if (File.Exists(cacheName))
            {
                int j, pack_len, core_test_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);

                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    if (SkvCoreTest.Version == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        pack_len = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                            //MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if ((well_index > -1) && (pack_len > 0))
                        {
                            w = Wells[well_index];
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(pack_len);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, pack_len);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            core_test_count = br.ReadInt32();
                            if ((!NotLoadData) && (core_test_count > 0))
                            {
                                if (w.coreTest == null) w.coreTest = new SkvCoreTest();
                                SkvCoreTestItem[] items = new SkvCoreTestItem[core_test_count];

                                for (j = 0; j < core_test_count; j++)
                                {
                                    items[j] = w.coreTest.RetrieveItem(br);
                                }
                                w.coreTest.SetItems(items);
                            }
                        }
                    }
                    bfr.Close();
                    FreeBuffers();
                    fs = null;
                }
                catch (Exception)
                {
                    if (fs != null) fs.Close();
                    FreeBuffers();
                    return false;
                }
            }
            return retValue;
        }
        // Table 8.1
        bool LoadTable81ParamsFromCache2(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей Таблиц 8.1 из кэша";
            userState.Element = Name;

            string cacheName = project.path + "\\cache\\" + Name + "\\table81.bin";

            if (File.Exists(cacheName))
            {
                int i, lenYears, len_unpack;

                FileStream fs = new FileStream(cacheName, FileMode.Open); ;
                BinaryReader bfr = new BinaryReader(fs);
                if (Table81Params.Version == bfr.ReadInt32())
                {
                    len_unpack = bfr.ReadInt32();

                    MemoryStream ms = new MemoryStream(len_unpack);
                    ConvertEx.CopyStream(fs, ms, len_unpack);
                    MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                    ms.Dispose();
                    BinaryReader br = new BinaryReader(unpackMS);

                    Table81.ProjectDocName = br.ReadString();
                    Table81.NewPtdYear = br.ReadInt32();
                    lenYears = br.ReadInt32();

                    SumParamItem[] SumYear = new SumParamItem[lenYears];
                    for (i = 0; i < lenYears; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            br.Close();
                            return false;
                        }
                        else
                        {
                            SumYear[i].Date = RdfSystem.UnpackDateTimeI(br.ReadInt32());
                            SumYear[i].Liq = br.ReadDouble();
                            SumYear[i].Oil = br.ReadDouble();
                            SumYear[i].Watering = br.ReadDouble();
                            SumYear[i].Inj = br.ReadDouble();
                            SumYear[i].Fprod = br.ReadInt32();
                            SumYear[i].Finj = br.ReadInt32();
                            SumYear[i].LeadInAll = br.ReadInt32();
                            SumYear[i].LeadInProd = br.ReadInt32();
                            SumYear[i].NatGas = br.ReadDouble();
                            SumYear[i].InjGas = br.ReadDouble();
                        }
                    }
                    Table81.SetTable(SumYear);
                    br.Close();
                }
                bfr.Close();
                retValue = true;
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        #endregion
    }
}
