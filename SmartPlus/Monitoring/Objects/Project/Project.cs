﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;
using SmartPlus.DictionaryObjects;
using System.ComponentModel;
using RDF.Objects;
using RDF;
using System.IO;
using System.Windows.Forms;

namespace SmartPlus
{
    public sealed class Project : BaseObj
    {
        public CanvasMap canvas;
        public string path;
        public bool IsSmartProject = false;
        public DictionaryCollection DictList;

        public Point _BitMapPoint;

        public bool OilFieldsAllCoordLoaded;
        public RectangleD MinMaxXY;

        public int indActiveOilField;

        public List<int> GeoStratumCodes
        {
            get
            {
                return OilFields[0].GeoStratumCodes;
            }
        }
        public List<int> MerStratumCodes
        {
            get
            {
                return OilFields[0].MerStratumCodes;
            }
        }

        public List<OilField> OilFields;
        public List<Construction> ConstructionList;
        public List<Tube> ConstrTubesList;
        public DateTime minMerDate
        {
            set
            {
                OilFields[0].minMerDate = value;
            }
            get
            {
                return OilFields[0].minMerDate;
            }
        }
        public DateTime maxMerDate
        {
            set
            {
                OilFields[0].maxMerDate = value;
            }
            get
            {
                return OilFields[0].maxMerDate;
            }
        }

        public DateTime BubbleCalcDT1, BubbleCalcDT2;
        public bool BubbleHistoryByChess = false;
        public string tempStr = null;

        public Project(string ProjectPath) : base(Constant.BASE_OBJ_TYPES_ID.PROJECT)
        {
            path = ProjectPath;
#if BASHNEFT 
            IsSmartProject = ProjectPath.IndexOf("SmartPlus") > -1;
#else       
            IsSmartProject = true;
#endif
            indActiveOilField = -1;
            DictList = null;
            Name = "Project";
            InitializeDictionary();

            OilFields = new List<OilField>();
            ConstructionList = new List<Construction>();
            ConstrTubesList = new List<Tube>();

            _BitMapPoint = new Point();
            MinMaxXY = new RectangleD();
            OilFieldsAllCoordLoaded = false;

            OilField tempOilField = new OilField(this);
            tempOilField.Index = 0;
            OilFieldParamsItem ParamsItem = new OilFieldParamsItem();
            ParamsItem.NGDUCode = -1;
            ParamsItem.NGDUName = "Общие данные";
            tempOilField.ParamsDict = ParamsItem;

            tempOilField.Name = "Общие данные";
            OilFields.Add(tempOilField);
        }

        public void Close()
        {
            for (int i = 0; i < OilFields.Count; i++)
            {
                OilFields[i].Destroy();
            }
            ConstrTubesList.Clear();
            ConstructionList.Clear();
           
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();
        }

        public void SetMinMaxRect(RectangleD rect)
        {
            if ((MinMaxXY.Left == Constant.DOUBLE_NAN) || (MinMaxXY.Left > rect.Left)) MinMaxXY.Left = rect.Left;
            if ((MinMaxXY.Top == Constant.DOUBLE_NAN) || (MinMaxXY.Top > rect.Top)) MinMaxXY.Top = rect.Top;
            if ((MinMaxXY.Right == Constant.DOUBLE_NAN) || (MinMaxXY.Right < rect.Right)) MinMaxXY.Right = rect.Right;
            if ((MinMaxXY.Bottom == Constant.DOUBLE_NAN) || (MinMaxXY.Bottom < rect.Bottom)) MinMaxXY.Bottom = rect.Bottom;
        }
        public void SetMinMaxRect(OilField of)
        {
            if(of.MinMaxXY.Left != Constant.DOUBLE_NAN && of.MinMaxXY.Top != Constant.DOUBLE_NAN &&
                of.MinMaxXY.Right != Constant.DOUBLE_NAN && of.MinMaxXY.Bottom != Constant.DOUBLE_NAN)
            {
                if ((MinMaxXY.Left == Constant.DOUBLE_NAN) || (MinMaxXY.Left > of.MinMaxXY.Left)) MinMaxXY.Left = of.MinMaxXY.Left;
                if ((MinMaxXY.Top == Constant.DOUBLE_NAN) || (MinMaxXY.Top > of.MinMaxXY.Top)) MinMaxXY.Top = of.MinMaxXY.Top;
                if ((MinMaxXY.Right == Constant.DOUBLE_NAN) || (MinMaxXY.Right < of.MinMaxXY.Right)) MinMaxXY.Right = of.MinMaxXY.Right;
                if ((MinMaxXY.Bottom == Constant.DOUBLE_NAN) || (MinMaxXY.Bottom < of.MinMaxXY.Bottom)) MinMaxXY.Bottom = of.MinMaxXY.Bottom;
            }
        }
        public void CreateOilFields(string projectName, string[] oilfieldList)
        {
            int i, j;
            string str;
            OilFieldParamsDictionary OilFieldParamsDict = (OilFieldParamsDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS);
            if (OilFieldParamsDict.Count > 0)
            {
                for (i = 0; i < oilfieldList.Length; i++)
                {
                    str = oilfieldList[i];
                    j = 0;
                    while ((OilFieldParamsDict[j].OilFieldName != str) && (j < OilFieldParamsDict.Count))
                    {
                        j++;
                    }
                    if (j < OilFieldParamsDict.Count)
                    {
                        OilField tempOilField = new OilField(this);
                        tempOilField.ParamsDict.RDFFieldCode = (int)OilFieldParamsDict[j].RDFFieldCode;
                        tempOilField.Name = OilFieldParamsDict[j].OilFieldName;
                        OilFields.Add(tempOilField);
                        tempOilField.Index = OilFields.Count - 1;
                    }
                }
            }
        }
        public void SelectWellsByContour(Contour cntr)
        {
            if ((OilFields != null) && (OilFields.Count > 1))
            {
                int i, j;
                double minX, maxX, minY, maxY;
                OilField of;
                Well w;
                for (i = 1; i < OilFields.Count; i++)
                {
                    of = OilFields[i];
                    minX = of.MinMaxXY.Left;
                    maxX = of.MinMaxXY.Right;
                    minY = of.MinMaxXY.Top;
                    maxY = of.MinMaxXY.Bottom;
                    if (cntr.PointBoundsEntry(minX, minY) ||
                        cntr.PointBoundsEntry(minX, maxY) ||
                        cntr.PointBoundsEntry(maxX, minY) ||
                        cntr.PointBoundsEntry(maxX, maxY))
                    {
                        for (j = 0; j < of.Wells.Count; j++)
                        {
                            w = of.Wells[j];
                            if (cntr.PointBodyEntry(w.X, w.Y))
                                w.Selected = true;
                        }
                    }
                }
            }
        }
        public bool LoadProject(string ProjectPath)
        {
            bool retValue = false;
            string projName;
            string[] dirList;
            path = ProjectPath;
            dirList = ProjectPath.Split(new char[] { '\\' });
            if (dirList.Length > 0)
            {
                projName = ProjectPath + "\\" + dirList[dirList.Length - 1] + ".txt";
                if (File.Exists(projName)) retValue = LoadProjectFile(projName);
            }
            return retValue;
        }

        private void DirectoryCopy(string sourceDirName, string destDirName)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists) return;

            if (!Directory.Exists(destDirName)) Directory.CreateDirectory(destDirName);

            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }
            if (true)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath);
                }
            }
        }
        public bool CopyProject(BackgroundWorker worker, DoWorkEventArgs e, string SourceProjectPath, string targetProjectPath)
        {
            bool retValue = false;
            int i, len;
            string projName;
            DirectoryInfo dir = new DirectoryInfo(SourceProjectPath + "\\cache");
            DirectoryInfo[] dirList;
            FileInfo[] fileList;
            WorkerState userState = new WorkerState();
            string[] parseList;
            parseList = SourceProjectPath.Split(new char[] { '\\' });
            if (parseList.Length > 0)
            {
                projName = SourceProjectPath + "\\" + parseList[parseList.Length - 1] + ".txt";
                if (File.Exists(projName))
                {
                    dirList = dir.GetDirectories();
                    fileList = dir.GetFiles();
                    if (Directory.Exists(targetProjectPath)) Directory.Delete(targetProjectPath, true);

                    len = dirList.Length;
                    for (i = 0; i < len; i++)
                    {
                        userState.WorkMainTitle = String.Format("Загрузка с сервера проектов ({0} месторождений)", len);
                        userState.WorkCurrentTitle = "Копирование данных месторождений";
                        userState.Element = dirList[i].Name;

                        DirectoryCopy(dirList[i].FullName, targetProjectPath + "\\cache\\" + dirList[i]);
                        worker.ReportProgress(100, userState);
                    }
                    len = fileList.Length;
                    for (i = 0; i < len; i++)
                    {
                        File.Copy(fileList[i].FullName, targetProjectPath + "\\cache\\" + fileList[i].Name);
                    }
                    File.Copy(projName, targetProjectPath + "\\" + parseList[parseList.Length - 1] + ".txt");
                }
            }
            return retValue;
        }

        // BUFFER LIST
        public void ClearBufferList(bool GCCollect)
        {
            for (int i = 0; i < OilFields.Count; i++)
            {
                (OilFields[i]).ClearBufferList(false);
            }
            if (GCCollect)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        // Dictionary List
        private void InitializeDictionary()
        {
            DictList = new DictionaryCollection(path + "\\Dictionaries", System.Windows.Forms.Application.StartupPath + "\\Dictionaries");
            DictList.LoadDataFromFiles();
        }

        // LOAD PROJECT
        public void LoadProject(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool res;
            //if (_ProjectPath != "") LoadProject(_ProjectPath);
            WorkerState userState = new WorkerState();
            userState.WorkCurrentTitle = "Загрузка проекта...";
            userState.WorkMainTitle = "Загрузка проекта '" + this.Name + "'";
            worker.ReportProgress(0, userState);

            LoadStratumCodesFromCache(worker, e);
                        
            LoadWellList(worker, e);

            LoadAliases();
            LoadProjWellList(worker, e);
            LoadContours(worker, e);
            LoadAreas(worker, e);
            LoadDeposits(worker, e);
            LoadGridHeadList(worker, e);
               //if (res) LoadConstrFromCache(worker, e);
               //if (res) LoadPVTParams(worker, e);
            LoadWellPads(worker, e);
        }

        public bool LoadProjectFile(string ProjectFileName)
        {
            if (IsSmartProject) return LoadProjectFile2(ProjectFileName);
            bool retValue = false;
            int i, j;
            int[] RDFFieldCodes;
            string[] tempStr;
            string[] ParseStr;
            path = Path.GetDirectoryName(ProjectFileName);
            tempStr = File.ReadAllLines(ProjectFileName, System.Text.Encoding.GetEncoding(1251));
            Name = tempStr[0];
            try
            {
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                // загрузка кодов месторождений баз
                ParseStr = tempStr[1].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (tempStr.Length > 2)
                {
                    C2DObject.st_MaxXUnits = Convert.ToDouble(ParseStr[0], nfi);
                    if (ParseStr.Length > 1)
                    {
                        C2DObject.st_NgduXUnits = Convert.ToDouble(ParseStr[1], nfi);
                        if (ParseStr.Length > 2)
                        {
                            C2DObject.st_OilfieldXUnits = Convert.ToDouble(ParseStr[2], nfi);
                            if (ParseStr.Length > 3)
                            {
                                C2DObject.st_AreaXUnits = Convert.ToDouble(ParseStr[3], nfi);
                            }
                        }
                    }
                }
                ParseStr = tempStr[tempStr.Length - 1].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                RDFFieldCodes = new int[ParseStr.Length];
                for (i = 0; i < ParseStr.Length; i++)
                {
                    if (ParseStr[i] != "")
                    {
                        RDFFieldCodes[i] = System.Convert.ToInt32(ParseStr[i]);
                    }
                }
                OilFieldCoefDictionary OilFieldCoef = (OilFieldCoefDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT);
                OilFieldParamsDictionary OilFieldParamsDict = (OilFieldParamsDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS);

                // создание объектов OilField
                for (i = 0; i < RDFFieldCodes.Length; i++)
                {
                    for (j = 0; j < OilFieldParamsDict.Count; j++)
                    {
                        if (OilFieldParamsDict[j].RDFFieldCode == RDFFieldCodes[i])
                        {
                            OilField tempOilField = new OilField(this);
                            tempOilField.Index = i + 1;
                            OilFields.Add(tempOilField);

                            tempOilField.Name = OilFieldParamsDict[j].OilFieldName;
                            tempOilField.ParamsDict = OilFieldParamsDict[j];
                            for (int k = 0; k < OilFieldCoef.Count; k++)
                            {
                                if (OilFieldCoef[k].Code == tempOilField.ParamsDict.RDFFieldCode)
                                {
                                    tempOilField.CoefDict = OilFieldCoef[k];
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                retValue = true;
            }
            catch (Exception ex)
            {
                retValue = false;
                MessageBox.Show(ex.Message);
            }
            return retValue;
        }
        private bool LoadProjectFile2(string ProjectFileName)
        {
            bool retValue = false;
            int i, j;
            int[] RDFFieldCodes;
            string[] tempStr;
            string[] ParseStr;
            path = Path.GetDirectoryName(ProjectFileName);
            tempStr = File.ReadAllLines(ProjectFileName, System.Text.Encoding.UTF8);
            Name = tempStr[0];
            try
            {
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                // загрузка кодов месторождений баз
                ParseStr = tempStr[1].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (ParseStr.Length > 0)
                {
                    C2DObject.st_MaxXUnits = Convert.ToDouble(ParseStr[0], nfi);
                    if (ParseStr.Length > 1)
                    {
                        C2DObject.st_NgduXUnits = Convert.ToDouble(ParseStr[1], nfi);
                        if (ParseStr.Length > 2)
                        {
                            C2DObject.st_OilfieldXUnits = Convert.ToDouble(ParseStr[2], nfi);
                            if (ParseStr.Length > 3)
                            {
                                C2DObject.st_AreaXUnits = Convert.ToDouble(ParseStr[3], nfi);
                            }
                        }
                    }
                }
                ParseStr = tempStr[tempStr.Length - 1].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                RDFFieldCodes = new int[ParseStr.Length];
                for (i = 0; i < ParseStr.Length; i++)
                {
                    if (ParseStr[i] != "")
                    {
                        RDFFieldCodes[i] = System.Convert.ToInt32(ParseStr[i]);
                    }
                }
                OilFieldCoefDictionary OilFieldCoef = (OilFieldCoefDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT);
                OilFieldParamsDictionary OilFieldParamsDict = (OilFieldParamsDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS);

                // создание объектов OilField
                for (i = 0; i < RDFFieldCodes.Length; i++)
                {
                    for (j = 0; j < OilFieldParamsDict.Count; j++)
                    {
                        if (OilFieldParamsDict[j].RDFFieldCode == RDFFieldCodes[i])
                        {
                            OilField tempOilField = new OilField(this);
                            tempOilField.Index = i + 1;
                            OilFields.Add(tempOilField);

                            tempOilField.Name = OilFieldParamsDict[j].OilFieldName;
                            tempOilField.ParamsDict = OilFieldParamsDict[j];
                            for (int k = 0; k < OilFieldCoef.Count; k++)
                            {
                                if (OilFieldCoef[k].Code == tempOilField.ParamsDict.RDFFieldCode)
                                {
                                    tempOilField.CoefDict = OilFieldCoef[k];
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                retValue = true;
            }
            catch (Exception ex)
            {
                retValue = false;
                MessageBox.Show(ex.Message);
            }
            return retValue;
        }

        public int GetOFIndex(string OilFieldName)
        {
            return GetOFIndex(OilFieldName, false);
        }
        public int GetOFIndex(string OilFieldName, bool UseAliases)
        {
            for (int i = 1; i < OilFields.Count; i++)
            {
                if ((OilFields[i]).Name.Equals(OilFieldName, StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
                if (UseAliases && OilFields[i].Aliases != null)
                {
                    for (int j = 0; j < OilFields[i].Aliases.Count; j++)
                    {
                        if ((OilFields[i]).Aliases[j].Equals(OilFieldName, StringComparison.OrdinalIgnoreCase))
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }
        public int GetOFIndex(int OilfieldCode)
        {
            for (int i = 1; i < OilFields.Count; i++)
            {
                if ((OilFields[i]).OilFieldCode == OilfieldCode)
                {
                    return i;
                }
            }
            return -1;
        }

        public bool LoadStratumCodesFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {

            bool retValue = false;
            if (IsSmartProject)
            {
                for (int i = 0; i < OilFields.Count; i++)
                {
                    retValue = OilFields[i].LoadStratumCodesFromCache2() || retValue;
                    if (OilFields[i].minMerDate.Year > 1900 && (minMerDate.Year <= 1900 || minMerDate > OilFields[i].minMerDate))
                    {
                        minMerDate = OilFields[i].minMerDate;
                    }
                    if (OilFields[i].maxMerDate.Year < DateTime.MaxValue.Year && (maxMerDate.Year == DateTime.MaxValue.Year || maxMerDate < OilFields[i].maxMerDate))
                    {
                        maxMerDate = OilFields[i].maxMerDate;
                    }
                }
            }
            else
            {
                for (int i = 0; i < OilFields.Count; i++)
                {
                    if (i > 0)
                        retValue = OilFields[i].LoadStratumCodesFromCache() || retValue;
                    else
                        LoadProjectStratumCodesFromCache();
                }
            }
            return retValue;
        }
        public bool LoadProjectStratumCodesFromCache()
        {
            bool retValue = false;
            string cacheName = this.path + "\\cache\\OilObj.bin";
            if (OilFields.Count > 0 && File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int i, len, dt;
                OilField of = OilFields[0];
                len = br.ReadInt32();

                for (i = 0; i < len; i++) of.GeoStratumCodes.Add(br.ReadUInt16());
                of.GeoStratumCodes.TrimExcess();

                len = br.ReadInt32();
                for (i = 0; i < len; i++) of.MerStratumCodes.Add(br.ReadInt32());
                of.MerStratumCodes.TrimExcess();

                if (br.BaseStream.Position < br.BaseStream.Length)
                {
                    dt = br.ReadInt32();
                    of.minMerDate = (dt != 0) ? RdfSystem.UnpackDateTimeI(dt) : DateTime.MinValue;
                    dt = br.ReadInt32();
                    of.maxMerDate = (dt != 0) ? RdfSystem.UnpackDateTimeI(dt) : DateTime.MaxValue;
                }

                br.Close();
                retValue = true;
            }
            return retValue;
        }

        public bool LoadWellList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            if (!IsSmartProject)
            {
                for (int i = 0; i < OilFields.Count; i++)
                {
                    retValue = OilFields[i].LoadWellListFromCache(worker, e);
                    if (retValue)
                    {
                        SetMinMaxRect(OilFields[i]);
                        //OilFields[i].LoadTrajectoryFromCache(worker, e);
                    }
                }
            }
            else
            {
                for (int i = 0; i < OilFields.Count; i++)
                {
                    retValue = OilFields[i].LoadWellsFromCache2(worker, e);
                    if (retValue) OilFields[i].LoadCoordFromCache2(worker, e);
                    if (retValue) SetMinMaxRect(OilFields[i]);
                }

            }
            this.OilFieldsAllCoordLoaded = retValue;
            return retValue;
        }
        public void LoadAliases()
        {
            OilField of;
            for (int i = 1; i < OilFields.Count; i++)
            {
                of = OilFields[i];
                if (!of.AliasLoaded) of.LoadAliasesFromCache();
            }
        }
        public bool LoadProjWellListFromFile(BackgroundWorker worker, DoWorkEventArgs e, string SourcePath)
        {
            bool retValue = false;
            char[] delimeter = new char[] { ';' };
            string[] parseStr;
            string str, OFName, OFAreaName, StratumName, SkvName = string.Empty;
            var dictOFArea = (OilFieldAreaDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            var dictStratum = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            
            Well w = null;
            SkvCoordItem[] skvItems;
            SkvCoordItem coordItem;
            ArrayList list = new ArrayList();
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Загрузка проектных скважин с файла";
            userState.WorkCurrentTitle = "Загрузка проектных скважин с файла";
            userState.Element = "";

            if (File.Exists(SourcePath))
            {
                StreamReader file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // считаем количество строк
                int i, k, j, iLen = 0, progress = 0, ind = 0;
                bool bAll = false;
                int lastAreaIndex = -1;
                int lastStratumIndex = -1;
                OilField of = null, predOF = null;
                while (!file.EndOfStream)
                {
                    file.ReadLine();
                    iLen++;
                }
                file.Close();
                file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                file.ReadLine();                  // шапка
                j = 0;
                worker.ReportProgress(0, userState);
                while (!file.EndOfStream)
                {
                    str = file.ReadLine();
                    parseStr = str.Split(delimeter);
                    OFName = parseStr[0].ToUpper();
                    OFAreaName = parseStr[1];
                    StratumName = parseStr[3];

                    if ((of == null) || (of.Name != OFName))
                    {
                        if ((of != null) && (of.ProjWellLoaded))
                        {
                            of.WriteProjWellListToCache(worker, e);
                        }
                        list.Clear();
                        of = null;
                        for (i = 0; i < OilFields.Count; i++)
                        {
                            if ((OilFields[i]).Name == OFName)
                            {
                                of = OilFields[i];
                                ind = of.ProjWells.Count;
                                break;
                            }
                        }
                    }

                    lastAreaIndex = dictOFArea.GetIndexByShortName(OFAreaName);
                    if (lastAreaIndex == -1)
                    {
                        MessageBox.Show("Не найдена площадь " + OFAreaName.Replace(" ", "_") + " в справочнике[" + OFName + ", " + OFAreaName + "]");
                        return false;
                    }
                    lastStratumIndex = dictStratum.GetIndexByShortName(StratumName);
                    if (lastStratumIndex == -1)
                    {
                        MessageBox.Show("Не найден пласт " + StratumName + " в справочнике[" + OFName + ", " + OFAreaName + "]");
                        return false;
                    }

                    if ((w == null) || (parseStr[2] != SkvName))
                    {
                        double x, y;
                        if (list.Count > 0)
                        {
                            skvItems = new SkvCoordItem[list.Count];
                            for (i = 0; i < list.Count; i++)
                            {
                                skvItems[i].PlastId = ((SkvCoordItem)list[i]).PlastId;
                                skvItems[i].X = ((SkvCoordItem)list[i]).X;
                                skvItems[i].Y = ((SkvCoordItem)list[i]).Y;
                            }
                            w.coord.SetItems(skvItems);
                            list.Clear();
                            x = w.X; y = w.Y;
                            of.CoefDict.GlobalCoordFromLocal(x, y, out w.X, out w.Y);
                            w.ReSetIcons(of.CoefDict);
                        }

                        w = new Well(ind);
                        w.Name = parseStr[2];
                        w.TypeID = Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL;
                        w.OilFieldAreaCode = dictOFArea[lastAreaIndex].Code;
                        w.ProjectDest = parseStr[4] == "НАГ" ? (byte)2 : (byte)1;
                        w.Index = of.ProjWells.Count;
                        of.ProjWells.Add(w);
                        of.ProjWellLoaded = true;
                    }

                    coordItem = new SkvCoordItem();
                    coordItem.PlastId = (ushort)dictStratum[lastStratumIndex].Code;
                    try
                    {
                        coordItem.X = (int)Convert.ToDouble(parseStr[5]);
                        coordItem.Y = (int)Convert.ToDouble(parseStr[6]);
                        list.Add(coordItem);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ошибка загрузки координат");
                    }

                    userState.Element = SkvName;
                    progress = (int)((float)j * 100.0f / (float)iLen);
                    worker.ReportProgress(progress, userState);
                    j++;
                }
                if (w != null)
                {
                    double x, y;
                    if (list.Count > 0)
                    {
                        skvItems = new SkvCoordItem[list.Count];
                        for (i = 0; i < list.Count; i++)
                        {
                            skvItems[i].PlastId = ((SkvCoordItem)list[i]).PlastId;
                            skvItems[i].X = ((SkvCoordItem)list[i]).X;
                            skvItems[i].Y = ((SkvCoordItem)list[i]).Y;
                        }
                        w.coord.SetItems(skvItems);
                        list.Clear();
                        x = w.X; y = w.Y;
                        of.CoefDict.GlobalCoordFromLocal(x, y, out w.X, out w.Y);
                        w.ReSetIcons(of.CoefDict);
                    }
                }
                if ((of != null) && (of.ProjWellLoaded))
                {
                    of.WriteProjWellListToCache(worker, e);
                }
                file.Close();
                retValue = true;
            }
            return retValue;
        }
        public bool LoadProjWellList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            OilField of;
            for (int i = 0; i < OilFields.Count; i++)
            {
                of = OilFields[i];
                if (of.ProjWells.Count == 0)
                {
                    retValue = of.LoadProjWellListFromCache(worker, e);
                    if (retValue) SetMinMaxRect(of);
                }

            }
            return retValue;
        }

        public bool LoadContours(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            if (!IsSmartProject)
            {
                for (int i = 0; i < OilFields.Count; i++)
                {
                    if (OilFields[i].Contours.Count == 0)
                    {
                        retValue = OilFields[i].LoadContoursFromCache(worker, e, true) || retValue;
                    }
                    if (i == 0) SetMinMaxRect(OilFields[i]);
                }
            }
            else
            {
                for (int i = 0; i < OilFields.Count; i++)
                {
                    if (OilFields[i].Contours.Count == 0)
                    {
                        retValue = OilFields[i].LoadContoursFromCache2(worker, e, true) || retValue;
                    }
                }
            }
            return retValue;
        }

        public bool LoadGridHeadList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            OilField of;
            for (int i = 0; i < OilFields.Count; i++)
            {
                of = OilFields[i];
                if (of.Grids.Count == 0) of.LoadGridHeadListFromCache(worker, e);
            }
            return retValue;
        }
        public void FreeGridData(BackgroundWorker worker, DoWorkEventArgs e, int IndexOilField)
        {
            OilField of;
            int progress = 0;
            for (int i = 1; i < this.OilFields.Count; i++)
            {
                if (i != IndexOilField)
                {
                    of = this.OilFields[i];
                    for (int j = 0; j < of.Grids.Count; j++)
                    {
                        ((Grid)of.Grids[j]).FreeDataMemory();
                    }
                }
                progress = (int)((float)i * 100 / (float)this.OilFields.Count);
                worker.ReportProgress(progress);
            }
            GC.GetTotalMemory(true);
            worker.ReportProgress(100);
        }

        public bool CalcAreasCompensation(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            OilField of;
            Area area;
            Well w;
            int i, j, n, t, progress, indArea;
            double sumLiq, sumInj, k;
            double sumWLiq, sumWInj;
            double AllLiq, AllInj, liq, inj;


            DateTime dt;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Расчет параметров ячеек заводнения";
            userState.WorkCurrentTitle = "Расчет параметров ячеек заводнения";
            userState.Element = "";

            PVTParamsItem pvt;
            var dict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            MerItem merItem;

            string name = string.Empty;
            worker.ReportProgress(0, userState);
            ArrayList list = new ArrayList();
            ArrayList wellList = new ArrayList();
            ArrayList wellCoefList = new ArrayList();
            ArrayList oilFieldAreaCodeList = new ArrayList();
            ArrayList oilFieldAreaCodeCountList = new ArrayList();


            ArrayList oilObjList = new ArrayList();
            for (i = 1; i < OilFields.Count; i++)
            {
                list.Clear();

                of = OilFields[i];
                //if (of.OFParams.RDFFieldCode != 13) continue;
                of.LoadWellsAreaCount(worker, e);

                if (!of.MerLoaded) of.LoadMerFromCache(worker, e);

                for (j = 0; j < of.Areas.Count; j++)
                {
                    area = (Area)of.Areas[j];
                    //if (j != 37) continue;
                    sumLiq = 0;
                    sumInj = 0;
                    wellList.Clear();
                    oilFieldAreaCodeList.Clear();
                    oilFieldAreaCodeCountList.Clear();
                    wellCoefList.Clear();
                    oilObjList.Clear();
                    AllLiq = 0; AllInj = 0;

                    for (n = 0; n < of.Wells.Count; n++)
                    {
                        w = of.Wells[n];

                        if (w.CountAreas == 0)
                            continue;
                        else
                            k = 1 / (double)w.CountAreas;

                        if ((w.CoordLoaded) &&
                            (((area.contour.PointBoundsEntry(w.X, w.Y)) && (area.contour.PointBodyEntry(w.X, w.Y))) ||
                             (area.contour.GetIndexPointByXY(w.X, w.Y) > -1)))
                        {
                            wellList.Add(w);
                            wellCoefList.Add(k);
                            indArea = oilFieldAreaCodeList.IndexOf(w.OilFieldAreaCode);
                            if (w.OilFieldAreaCode > -1)
                            {
                                if (indArea == -1)
                                {
                                    oilFieldAreaCodeList.Add(w.OilFieldAreaCode);
                                    oilFieldAreaCodeCountList.Add(1);
                                }
                                else
                                {
                                    oilFieldAreaCodeCountList[indArea] = (int)oilFieldAreaCodeCountList[indArea] + 1;
                                }
                            }
                            if (w.MerLoaded)
                            {
                                for (t = 0; t < w.mer.Count; t++)
                                {
                                    merItem = w.mer.Items[t];
                                    if ((merItem.CharWorkId == 11) ||
                                        (merItem.CharWorkId == 20))
                                    {
                                        if ((merItem.WorkTime + merItem.CollTime > 0) &&
                                            (merItem.Oil + merItem.Wat + merItem.Gas > 0) &&
                                            (oilObjList.IndexOf(merItem.PlastId) == -1))
                                        {
                                            oilObjList.Add(merItem.PlastId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (wellList.Count > 0)
                    {
                        area.WellList = new Well[wellList.Count];
                        area.WellCoefList = new double[wellList.Count];
                        for (n = 0; n < wellList.Count; n++)
                        {
                            area.WellList[n] = (Well)wellList[n];
                            area.WellCoefList[n] = (double)wellCoefList[n];
                        }
                        indArea = 0;
                        for (n = 0; n < oilFieldAreaCodeList.Count; n++)
                        {
                            if (indArea < (int)oilFieldAreaCodeCountList[n])
                            {
                                area.OilFieldAreaCode = (int)oilFieldAreaCodeList[n];
                                indArea = (int)oilFieldAreaCodeCountList[n];
                            }
                        }

                    }

                    //if (oilObjList.Count > 0)
                    //{
                    //    area.OilObjectList = new int[oilObjList.Count];
                    //    for (n = 0; n < oilObjList.Count; n++)
                    //    {
                    //        area.OilObjectList[n] = (int)oilObjList[n];
                    //    }
                    //}
                    retValue = true;
                }
                of.ClearMerData(true);
                if (retValue) of.WriteAreasToCache(worker, e);
                progress = (int)((float)i * 100 / (float)OilFields.Count);
                worker.ReportProgress(i, userState);
            }

            worker.ReportProgress(100, userState);
            return retValue;
        }

        public bool LoadPVTFromFile(BackgroundWorker worker, DoWorkEventArgs e, string SourcePath)
        {
            bool retValue = false;
            char[] delimeter = new char[] { ';' };
            string[] parseStr;
            string str, OFName, OFAreaName, PlastName;
            var stratumDict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            var dictArea = (OilFieldAreaDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);

            int lastAreaIndex = -1;
            int lastStratumIndex = -1;

            List<PVTParamsItem> list = new List<PVTParamsItem>();
            PVTParamsItem item;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Загрузка PVT данных с файла";
            userState.WorkCurrentTitle = "Загрузка PVT данных с файла";
            userState.Element = "";

            if (File.Exists(SourcePath))
            {
                StreamReader file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // считаем количество строк
                int i, k, j, iLen = 0, progress = 0;
                bool bAll = false;
                OilField of = null;
                while (!file.EndOfStream)
                {
                    file.ReadLine();
                    iLen++;
                }
                file.Close();
                file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                file.ReadLine();                  // шапка
                j = 0;
                worker.ReportProgress(0, userState);
                while (!file.EndOfStream)
                {
                    str = file.ReadLine();
                    parseStr = str.Split(delimeter);
                    OFName = parseStr[0].ToUpper();
                    OFAreaName = parseStr[1];
                    bAll = false;
                    if (OFAreaName == "ALL") bAll = true;
                    PlastName = parseStr[2];

                    if ((of == null) || (of.Name != OFName))
                    {
                        if ((of != null) && (list.Count > 0))
                        {
                            of.PVTList = new PVTParams();
                            of.PVTList.SetItems(list.ToArray());
                            of.WritePVTParamsToCache(worker, e);
                        }
                        of = null;
                        list.Clear();
                        for (i = 0; i < OilFields.Count; i++)
                        {
                            if ((OilFields[i]).Name == OFName)
                            {
                                of = OilFields[i];
                                break;
                            }
                        }
                    }
                    lastAreaIndex = dictArea.GetIndexByShortName(OFAreaName);
                    if (!bAll && lastAreaIndex == -1)
                    {
                        MessageBox.Show("Не найдена площадь " + OFAreaName.Replace(" ", "_") + " в справочнике[" + OFName + ", " + OFAreaName + "]");
                        return false;
                    }
                    lastStratumIndex = stratumDict.GetIndexByShortName(PlastName);
                    if (lastStratumIndex == -1)
                    {
                        MessageBox.Show("Не найден пласт " + PlastName + " в справочнике[" + OFName + ", " + OFAreaName + "]");
                        return false;

                    }
                    if (!bAll)
                        item.OilFieldAreaCode = dictArea[lastAreaIndex].Code;
                    else
                        item.OilFieldAreaCode = -1;
                    
                    item.StratumCode = stratumDict[lastStratumIndex].Code;
                    item.OilVolumeFactor = Convert.ToDouble(parseStr[3]);
                    item.WaterVolumeFactor = Convert.ToDouble(parseStr[4]);
                    item.InitialPressure = Convert.ToDouble(parseStr[5]);
                    item.OilDensity = Convert.ToDouble(parseStr[6]);
                    item.OilViscosity = Convert.ToDouble(parseStr[7]);
                    item.WaterViscosity = Convert.ToDouble(parseStr[8]);
                    item.DisplacementEfficiency = Convert.ToDouble(parseStr[9]);
                    item.Porosity = Convert.ToDouble(parseStr[10]);
                    item.OilInitialSaturation = Convert.ToDouble(parseStr[11]);
                    item.SaturationPressure = Convert.ToDouble(parseStr[12]);
                    item.WaterDensity = Convert.ToDouble(parseStr[13]);
                    item.Permeability = Convert.ToDouble(parseStr[14]);
                    item.KIN = 0;
                    item.GasDensityFreeGas = 0;
                    item.GasDensitySoluteGas = 0;
                    item.GasFactor = 0;
                    item.GasInitialPressure = 0;
                    item.GasInitialSaturation = 0;
                    item.GasPorosity = 0;
                    item.Comment = string.Empty;
                    list.Add(item);
                    userState.Element = PlastName;
                    progress = (int)((float)j * 100f / (float)iLen);
                    worker.ReportProgress(progress, userState);
                    j++;
                }
                file.Close();

                if ((of != null) && (list.Count > 0))
                {
                    of.PVTList = new PVTParams();
                    of.PVTList.SetItems(list.ToArray());
                    of.WritePVTParamsToCache(worker, e);
                    list.Clear();
                }
                worker.ReportProgress(100, userState);
            }
            MessageBox.Show("PVT загружены!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return retValue;
        }
        public bool LoadPVTAreasFromFile(BackgroundWorker worker, DoWorkEventArgs e, string SourcePath)
        {
            bool retValue = false;
            char[] delimeter = new char[] { ';' };
            string[] parseStr;
            string str, OFName, OFAreaName, AreaName, PlastName;
            var stratumDict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            var dictArea = (OilFieldAreaDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);


            List<PVTParamsItem> list = new List<PVTParamsItem>();
            PVTParamsItem item;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Загрузка PVT данных ячеек заводнения с файла";
            userState.WorkCurrentTitle = "Загрузка PVT данных ячеек заводнения с файла";
            userState.Element = "";

            if (File.Exists(SourcePath))
            {
                StreamReader file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // считаем количество строк
                int i, j, iLen = 0, progress = 0;
                int areaCode = -1, code;
                bool needCalcReservers = false;
                OilField of = null;
                Area area = null;
                while (!file.EndOfStream)
                {
                    file.ReadLine();
                    iLen++;
                }
                file.Close();
                file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // шапка
                file.ReadLine();                  
                j = 0;

                worker.ReportProgress(0, userState);
                while (!file.EndOfStream)
                {
                    str = file.ReadLine();
                    parseStr = str.Split(delimeter);
                    OFName = parseStr[1].ToUpper();
                    OFAreaName = parseStr[2];
                    AreaName = parseStr[3];
                    PlastName = parseStr[4];

                    if ((of == null) || (of.Name != OFName))
                    {
                        if (of != null)
                        {
                            if (needCalcReservers) of.CalcAreasReserves();
                            of.WriteAreasToCache(worker, e);
                        }
                        of = null;
                        for (i = 0; i < OilFields.Count; i++)
                        {
                            if ((OilFields[i]).Name == OFName)
                            {
                                of = OilFields[i];
                                break;
                            }
                        }
                    }
                    areaCode = dictArea.GetCodeByShortName(OFAreaName);

                    if (of != null)
                    {
                        if ((area == null) || (area.Name != AreaName))
                        {
                            area = null;
                            for (i = 0; i < of.Areas.Count; i++)
                            {
                                if ((of.Areas[i]).Name == AreaName && (of.Areas[i].OilFieldAreaCode == -1 || of.Areas[i].OilFieldAreaCode == areaCode))
                                {
                                    area = of.Areas[i];
                                    break;
                                }
                            }
                        }
                        if (area != null)
                        {
                            //area.Params.InitOilReserv = Convert.ToDouble(parseStr[5]) * 1000;
                            //area.Params.KIN = Convert.ToDouble(parseStr[7]); needCalcReservers = true;

                            //area.pvt.OilFieldAreaCode = areaCode;
                            //area.pvt.StratumCode = area.PlastCode;
                            //area.pvt.OilVolumeFactor = Convert.ToDouble(parseStr[5]);  needCalcReservers = true;
                            //area.pvt.WaterVolumeFactor = Convert.ToDouble(parseStr[6]);
                            //area.pvt.InitialPressure = Convert.ToDouble(parseStr[7]);
                            //area.pvt.OilDensity = Convert.ToDouble(parseStr[9]);  needCalcReservers = true;
                            area.pvt.OilViscosity = Convert.ToDouble(parseStr[12]);
                            area.pvt.WaterViscosity = Convert.ToDouble(parseStr[18]);
                            //area.pvt.DisplacementEfficiency = Convert.ToDouble(parseStr[11]);
                            //area.pvt.Porosity = Convert.ToDouble(parseStr[12]);  needCalcReservers = true;
                            //area.pvt.OilInitialSaturation = Convert.ToDouble(parseStr[13]);  needCalcReservers = true;
                            area.pvt.SaturationPressure = Convert.ToDouble(parseStr[16]);
                            //area.pvt.WaterDensity = Convert.ToDouble(parseStr[15]);
                            //area.pvt.Permeability = Convert.ToDouble(parseStr[17]);
                            //area.pvt.KIN = Convert.ToDouble(parseStr[18]);  needCalcReservers = true;
                            //if (area.pvt.GasFactor == -1) area.pvt.GasFactor = 0;
                            //if (area.pvt.GasInitialPressure == -1) area.pvt.GasInitialPressure = 0;
                            //if (area.pvt.GasPorosity == -1) area.pvt.GasPorosity = 0;
                            //if (area.pvt.GasDensityFreeGas == -1) area.pvt.GasDensityFreeGas = 0;
                            //if (area.pvt.GasDensitySoluteGas == -1) area.pvt.GasDensitySoluteGas = 0;
                            //if (area.pvt.GasInitialSaturation == -1) area.pvt.GasInitialSaturation = 0;
                            //area.Params.KIN = area.pvt.KIN;
                        }
                    }
                    userState.Element = PlastName;
                    progress = (int)((float)j * 100f / (float)iLen);
                    worker.ReportProgress(progress, userState);
                    j++;
                }
                file.Close();
                if (of != null)
                {
                    of.CalcAreasReserves();
                    of.WriteAreasToCache(worker, e);
                }
                worker.ReportProgress(100, userState);
            }
            MessageBox.Show("PVT загружены!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return retValue;
        }
        public bool LoadAreasIndexFromFile(BackgroundWorker worker, DoWorkEventArgs e, string SourcePath)
        {
            bool retValue = false, res;
            string OFname, OFAreaName, SkvName;
            char[] delimeter = new char[] { ';' };
            string[] parseStr;
            string str;
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";

            var dict = (OilFieldAreaDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            int ind;
            ArrayList list = new ArrayList();
            Well w;
            double X, Y, X0, Y0, Delta, minDelta, revDelta;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Привязка скважин по площадям";
            userState.WorkCurrentTitle = "Привязка скважин по площадям";
            userState.Element = "";
            ArrayList editedOF = new ArrayList();

            if (File.Exists(SourcePath) && (dict != null))
            {
                StreamReader file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // считаем количество строк
                int i, k, j, iLen = 0, progress = 0, areaCode;
                int RevertXY;
                OilField of = null;
                while (!file.EndOfStream)
                {
                    file.ReadLine();
                    iLen++;
                }
                file.Close();
                file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                file.ReadLine();                  // шапка
                j = 0;
                worker.ReportProgress(0, userState);
                RevertXY = 0;
                while (!file.EndOfStream)
                {
                    str = file.ReadLine();
                    parseStr = str.Split(delimeter);
                    OFname = parseStr[1].ToUpper();
                    areaCode = Convert.ToInt32(parseStr[3]);
                    //OFAreaName = parseStr[2];
                    SkvName = parseStr[5].ToUpper();


                    if ((of == null) || (of.Name != OFname))
                    {
                        of = null;
                        for (i = 0; i < OilFields.Count; i++)
                        {
                            if ((OilFields[i]).Name == OFname)
                            {
                                of = OilFields[i];
                                break;
                            }
                        }
                        RevertXY = 0;
                    }
                    areaCode = dict.GetCodeByShortName(parseStr[2]);
                    if(areaCode == 0)
                    {
                        MessageBox.Show("Не найдена площадь " + parseStr[2] + " в справочнике");
                        return false;
                    }
                    ind = of.OilFieldAreaList.IndexOf(areaCode);
                    if (ind == -1)
                    {
                        ind = of.OilFieldAreaList.Count;
                        of.OilFieldAreaList.Add(areaCode);
                    }

                    userState.Element = SkvName;
                    list.Clear();
                    for (i = 0; i < of.Wells.Count; i++)
                    {
                        w = of.Wells[i];
                        if (w.UpperCaseName == SkvName) list.Add(w);
                    }
                    if (list.Count > 1)
                    {
                        X = Convert.ToDouble(parseStr[7], nfi);
                        Y = Convert.ToDouble(parseStr[8], nfi);
                        minDelta = -1;
                        k = -1;
                        for (i = 0; i < list.Count; i++)
                        {
                            w = (Well)list[i];
                            Delta = -1;
                            if (w.coord.Altitude.X != Constant.DOUBLE_NAN)
                            {
                                //if (RevertXY == 3)
                                //{
                                //    Delta = Math.Sqrt(Math.Pow(w.coord.Altitude.Y - X, 2) + Math.Pow(w.coord.Altitude.X - Y, 2));
                                //}
                                //else if ((RevertXY == 2) || (RevertXY == 1) || (RevertXY == 0))
                                //{
                                revDelta = Math.Sqrt(Math.Pow(w.coord.Altitude.Y - X, 2) + Math.Pow(w.coord.Altitude.X - Y, 2));
                                Delta = Math.Sqrt(Math.Pow(w.coord.Altitude.X - X, 2) + Math.Pow(w.coord.Altitude.Y - Y, 2));
                                if ((revDelta < Delta) && (revDelta < 5))
                                {
                                    //if (MessageBox.Show(String.Format("Учитывать координаты X:{0} Y:{1}  - X:{2} Y:{3}", X, Y, w.coord.Altitude.X, w.coord.Altitude.Y), "Revert XY", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    //{
                                    Delta = revDelta;
                                    RevertXY++;
                                    //}
                                }
                                //}
                            }
                            else if (w.coord.Count > 0)
                            {
                                if (RevertXY == 3)
                                {
                                    Delta = Math.Sqrt(Math.Pow(w.coord.Items[0].Y - X, 2) + Math.Pow(w.coord.Items[0].X - Y, 2));
                                }
                                else if ((RevertXY == 2) || (RevertXY == 1) || (RevertXY == 0))
                                {
                                    revDelta = Math.Sqrt(Math.Pow(w.coord.Items[0].Y - X, 2) + Math.Pow(w.coord.Items[0].X - Y, 2));
                                    Delta = Math.Sqrt(Math.Pow(w.coord.Items[0].X - X, 2) + Math.Pow(w.coord.Items[0].Y - Y, 2));
                                    if (revDelta < Delta)
                                    {
                                        if (MessageBox.Show(String.Format("Учитывать координаты X:{0} Y:{1}  - X:{2} Y:{3}?", X, Y, w.coord.Items[0].X, w.coord.Items[0].Y), "Revert XY", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                        {
                                            Delta = revDelta;
                                            RevertXY++;
                                        }
                                    }
                                }
                            }
                            if ((Delta > -1) && ((Delta < minDelta) || (minDelta == -1)))
                            {
                                minDelta = Delta;
                                k = i;
                            }
                        }
                        if (k != -1)
                        {

                            ((Well)list[k]).OilFieldAreaCode = areaCode;
                            if (editedOF.IndexOf(((Well)list[k]).OilFieldIndex) == -1)
                                editedOF.Add(((Well)list[k]).OilFieldIndex);
                        }
                        else
                            MessageBox.Show("Не найдена скважина " + SkvName, "Revert XY", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (list.Count == 1)
                    {
                        ((Well)list[0]).OilFieldAreaCode = areaCode;
                    }

                    progress = (int)((float)j * 100f / (float)iLen);
                    worker.ReportProgress(progress, userState);
                    j++;
                }
                for (j = 0; j < editedOF.Count; j++)
                {
                    OilFields[(int)editedOF[j]].WriteWellListToCache(worker, e);
                }
                file.Close();
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadAreasIndexFromFile2(BackgroundWorker worker, DoWorkEventArgs e, string SourcePath)
        {
            bool retValue = false, res;
            char[] delimeter = new char[] { ';' };
            ArrayList buff = new ArrayList();
            string[] parseStr;
            string str;
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";

            var dictArea = (OilFieldAreaDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            int ind, areaInd;
            ArrayList list = new ArrayList();
            Well w;
            double X, Y, X0, Y0, Delta, minDelta, revDelta;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Привязка скважин по площадям";
            userState.WorkCurrentTitle = "Привязка скважин по площадям";
            userState.Element = "";
            ArrayList editedOF = new ArrayList();

            if (File.Exists(SourcePath))
            {
                StreamReader file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // считываем строки в буфер
                int i, k, k2, j, p = -1, iLen = 0, progress = 0, areaCode;
                int RevertXY;
                OilField of = null;
                file.ReadLine();
                while (!file.EndOfStream)
                {
                    str = file.ReadLine();
                    parseStr = str.Split(delimeter);
                    if (parseStr.Length > 0)
                    {
                        if ((parseStr[4].IndexOf(parseStr[1], StringComparison.CurrentCultureIgnoreCase) > -1) && (parseStr[2].IndexOf("ЭКС") > -1))
                        {
                            buff.Add(parseStr);
                        }
                    }
                }
                file.Close();
                iLen = buff.Count;
                file.Close();
                j = 0;
                ArrayList log = new ArrayList();
                worker.ReportProgress(0, userState);
                for (i = 0; i < OilFields.Count; i++)
                {
                    of = OilFields[i];
                    for (j = 0; j < of.Wells.Count; j++)
                    {
                        w = of.Wells[j];
                        if ((w.OilFieldAreaCode == -1) && (w.CoordLoaded) && (w.coord.Altitude.X != Constant.DOUBLE_NAN))
                        {
                            // поиск в буфере месторождения
                            for (k = 0; k < buff.Count; k++)
                            {
                                parseStr = (string[])buff[k];
                                if (parseStr[1] == of.ParamsDict.OilFieldName)
                                    break;
                            }

                            // поиск в буфере скважины по координатам
                            minDelta = 10;
                            p = -1;
                            for (k2 = k; k2 < buff.Count; k2++)
                            {
                                parseStr = (string[])buff[k2];
                                if (parseStr[1] != of.ParamsDict.OilFieldName) break;
                                X = Convert.ToDouble(parseStr[5], nfi);
                                Y = Convert.ToDouble(parseStr[6], nfi);
                                revDelta = Math.Sqrt(Math.Pow(w.coord.Altitude.Y - X, 2) + Math.Pow(w.coord.Altitude.X - Y, 2));
                                Delta = Math.Sqrt(Math.Pow(w.coord.Altitude.X - X, 2) + Math.Pow(w.coord.Altitude.Y - Y, 2));
                                if ((revDelta < Delta) && (revDelta < 5)) Delta = revDelta;
                                if ((minDelta > Delta) && (Delta < 5))
                                {
                                    if ((parseStr[3] == w.UpperCaseName) ||
                                      (MessageBox.Show("Установить AreaCode для " + w.UpperCaseName + "(" + w.coord.Altitude.X + "," + w.coord.Altitude.X + ")\n" +
                                      String.Format("{0};{1};{2};{3};{4};", parseStr[1], parseStr[2], parseStr[3], parseStr[5], parseStr[6])) == DialogResult.Yes))
                                    {
                                        minDelta = Delta;
                                        p = k2;
                                    }
                                }
                            }
                            if (p == -1)
                            {
                                // поиск по названию скважины
                                for (k2 = k; k2 < buff.Count; k2++)
                                {
                                    parseStr = (string[])buff[k2];
                                    if (parseStr[1] != of.ParamsDict.OilFieldName) break;
                                    if (parseStr[3] == w.UpperCaseName) p = k2;
                                }
                            }
                            if (p > -1)
                            {
                                parseStr = (string[])buff[p];
                                ind = parseStr[2].IndexOf("-ЭКС");
                                str = parseStr[2].Remove(ind, 4);
                                areaInd = dictArea.GetIndexByShortName(str);
                                if (ind > -1)
                                {
                                    w.OilFieldAreaCode = dictArea[areaInd].Code;
                                    log.Add(string.Format("{0};{1};{2};{3}", of.Name, w.UpperCaseName, dictArea[areaInd].Name, String.Format("{0};{1};{2};{3};{4};", parseStr[1], parseStr[2], parseStr[3], parseStr[5], parseStr[6])));

                                    if (editedOF.IndexOf(w.OilFieldIndex) == -1) editedOF.Add(w.OilFieldIndex);
                                }
                            }
                        }
                    }
                    if (p > -1)
                    {
                        progress = (int)((float)p * 100F / (float)buff.Count);
                        worker.ReportProgress(progress, userState);
                    }
                }
                buff = null;
                for (j = 0; j < editedOF.Count; j++)
                {
                    (OilFields[(int)editedOF[j]]).WriteWellListToCache(worker, e);
                }

                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadAreas(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            OilField of;
            for (int i = 0; i < OilFields.Count; i++)
            {
                of = OilFields[i];
                if (of.Areas.Count == 0) of.LoadAreasFromCache(worker, e);
            }
            return retValue;
        }
        public bool LoadDeposits(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            OilField of;
            for (int i = 0; i < OilFields.Count; i++)
            {
                of = OilFields[i];
                if (of.Deposits.Count == 0)
                {
                    of.LoadDepositsFromCache(worker, e);
                }
            }
            return retValue;
        }
        public bool LoadResearchFromFile(BackgroundWorker worker, DoWorkEventArgs e, string SourcePath)
        {
            bool retValue = false;

            string OFName, wellName, plast;
            List<WellResearchItem> list = new List<WellResearchItem>();
            WellResearchItem item;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Загрузка замеров давлений с файла";
            userState.WorkCurrentTitle = "Загрузка замеров давлений с файла";
            userState.Element = "";

            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";

            var StratumDict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            if (File.Exists(SourcePath))
            {
                SmartPlus.MSOffice.Excel excel = new SmartPlus.MSOffice.Excel();
                excel.OpenWorkbook(SourcePath);
                int i, k, j, iLen = 0, progress = 0, ind;
                OilField of = null;
                Well w = null;
                //string comment;
                iLen = excel.GetLastUsedRow();
                bool PressPlast;
                worker.ReportProgress(0, userState);
                List<object[]> errors = new List<object[]>();
                object[] errRow;
                object[,] row;
                list.Clear();
                //DateTime date1, date2, dateGIS;
                j = 1;
                while (j < iLen)
                {
                    row = excel.GetRangeValues(j, 0, j, 18);
                    if (row.Length > 0)
                    {
                        OFName = Convert.ToString(row[1, 1]).Trim().ToUpper();
                        wellName = Convert.ToString(row[1, 2]).Trim().ToUpper();
                        plast = Convert.ToString(row[1, 4]).Trim();
                        if (OFName.Length == 0 && wellName.Length == 0 && plast.Length == 0) break;
                        if ((of == null) || (of.Name != OFName))
                        {
                            if ((of != null) && (of.WellResearchLoaded))
                            {
                                of.WriteWellResearchToCache(worker, e);
                                of.ClearWellResearchData(true);
#if DEBUG
                                //break;
#endif
                            }
                            of = null;
                            list.Clear();
                            for (i = 0; i < OilFields.Count; i++)
                            {
                                if ((OilFields[i]).Name == OFName)
                                {
                                    of = OilFields[i];
                                    break;
                                }
                            }
                            //if (of != null) of.LoadWellResearchFromCache(worker, e);
                        }
                        if ((w != null) && (w.Name != wellName))
                        {
                            if (list.Count > 0 && w.research == null)
                            {
                                list.Sort(
                                    delegate(WellResearchItem item1, WellResearchItem item2)
                                    {
                                        int res = item1.Date.CompareTo(item2.Date);
                                        return res;
                                    }
                                );
                                w.research = new WellResearch();
                                w.research.SetItems(list.ToArray());
                                of.WellResearchLoaded = true;
                                list.Clear();
                            }
                        }
                        ind = -1;
                        ind = of.GetWellIndex(wellName);
                        if (ind == -1)
                        {
                            errRow = new object[3];
                            errRow[0] = j;
                            errRow[1] = 3;
                            errRow[2] = string.Format("Скважина {0} не найдена на месторождении [{1}]", wellName, OFName);
                            errors.Add(errRow);
                            j++;
                            continue;
                        }
                        w = of.Wells[ind];

                        int plastCode = StratumDict.TryGetLocalCodeByServerPlastName(plast);
                        if (plastCode == -1)
                        {
                            errRow = new object[3];
                            errRow[0] = j;
                            errRow[1] = 3;
                            errRow[2] = string.Format("Пласт {0} не найден в справочнике [{1}, {2}]", plast, wellName, OFName);
                            errors.Add(errRow);
                            j++;
                            continue;
                        }
                        float pressure = -1;
                        PressPlast = true;
                        try
                        {
                            if (row[1, 5] != null && Convert.ToSingle(row[1, 5], nfi) > 0)
                            {
                                pressure = Convert.ToSingle(row[1, 5], nfi);
                                PressPlast = true;
                            }
                            else if (row[1, 6] != null && Convert.ToSingle(row[1, 6], nfi) > 0)
                            {
                                pressure = Convert.ToSingle(row[1, 6], nfi);
                                PressPlast = false;
                            }
                            if (pressure < 0)
                            {
                                j++;
                                continue;
                            }
                            if (row[1, 8] == null) continue;

                            item.Date = Convert.ToDateTime(row[1, 3]);
                            item.PlastCode = (ushort)plastCode;
                            item.ResearchCode = Convert.ToUInt16(row[1, 8]);
                            item.PressPerforation = pressure;
                            if (item.ResearchCode == 5 && PressPlast) item.ResearchCode = 6;
                            item.PressWaterSurface = 0;
                            item.PressZatr = 0;
                            item.TimeDelay = 0;
                            if (item.ResearchCode == 2) item.TimeDelay = 24;
                            list.Add(item);
                        }
#if DEBUG
                        catch
                        {
                            errRow = new object[3];
                            errRow[0] = j;
                            errRow[1] = string.Empty;
                            errRow[2] = string.Format("Ошибка загрузки данных скважины {0} [{1}, {2}]]", w.Name, of.Name, row[1, 2]);
                            errors.Add(errRow);
                            j++;
                            continue;
                        }
#endif
                        finally { }
                    }
                    if (w != null && of != null)
                    {
                        userState.Element = string.Format("{0}[{1}]", w.Name, of.Name);
                    }
                    progress = (int)((float)j * 100f / (float)iLen);
                    worker.ReportProgress(progress, userState);
                    j++;
                }
                if (w != null)
                {
                    if (list.Count > 0 && w.research == null)
                    {
                        list.Sort(delegate(WellResearchItem item1, WellResearchItem item2) { return item1.Date.CompareTo(item2.Date); });
                        w.research = new WellResearch();
                        w.research.SetItems(list.ToArray());
                        of.WellResearchLoaded = true;
                        list.Clear();
                    }
                }
                if ((of != null) && (of.WellResearchLoaded))
                {
                    of.WriteWellResearchToCache(worker, e);
                    of.ClearWellResearchData(true);
                }
                userState.WorkCurrentTitle = "Выгрузка отчета об ошибках в Microsoft Excel";
                worker.ReportProgress(0, userState);
                if (errors.Count > 0)
                {
                    excel.NewWorkbook();
                    excel.SetActiveSheetName("Ошибки загрузки");
                    object[,] heap = (object[,])Array.CreateInstance(typeof(object), new int[2] { 1, 3 }, new int[2] { 0, 0 });
                    int col = 0;
                    heap[0, col++] = "Строка";
                    heap[0, col++] = "Столбец";
                    heap[0, col++] = "Ошибка";
                    excel.SetRange(0, 0, 0, 2, heap);
                    excel.SetRowsArray(worker, e, 1, 0, errors);
                    if (excel != null)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                        excel.Disconnect();
                    }
                }
            }
            return retValue;

        }

        public bool LoadWellPads(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            OilField of;
            for (int i = 0; i < OilFields.Count; i++)
            {
                of = OilFields[i];
                if (of.WellPads.Count == 0) of.LoadWellPadListFromCache(worker, e);
            }
            return retValue;
        }

        public bool LoadConstruction(BackgroundWorker worker, DoWorkEventArgs e, string FilePath)
        {
            if (File.Exists(FilePath))
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Загрузка данных обустройства";
                userState.WorkCurrentTitle = "Загрузка данных обустройства";
                userState.Element = "";

                int i;
                int maxCol, ind, rowCount, rowInd;
                int percent, constrInd;
                char[] delimeter = new char[] { ';' };
                string[] parseStr;
                string str;
                var dict = (DataDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.CONSTRUCTION);
                OilField of = null;
                Well w;
                Construction constr;
                Tube t;
                C2DObject obj;


                StreamReader file = new StreamReader(FilePath, System.Text.Encoding.GetEncoding(1251));
                rowCount = 0;
                while (file.ReadLine() != null) rowCount++;
                file.Close();
                file = new StreamReader(FilePath, System.Text.Encoding.GetEncoding(1251));

                str = file.ReadLine();
                parseStr = str.Split(delimeter);
                maxCol = parseStr.Length;
                if (maxCol < 4) return false;

                str = file.ReadLine();
                rowInd = 1;
                percent = (int)((float)rowInd / (float)rowCount);
                worker.ReportProgress(percent, userState);
                while (str != null)
                {
                    parseStr = str.Split(delimeter);
                    if (parseStr.Length > 3)
                    {
                        str = parseStr[0].ToUpper();
                        if ((of == null) || (of.Name != str))
                        {
                            of = null;
                            for (i = 0; i < OilFields.Count; i++)
                            {
                                if ((OilFields[i]).Name == str)
                                {
                                    of = OilFields[i];
                                    break;
                                }
                            }
                        }
                        if (of != null)
                        {
                            if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                            str = parseStr[1].ToUpper();
                            w = null;
                            for (i = 0; i < of.Wells.Count; i++)
                            {
                                if ((of.Wells[i]).UpperCaseName == str)
                                {
                                    w = of.Wells[i];
                                    w.OilFieldIndex = of.Index;
                                    break;
                                }
                            }
                            if (w != null)
                            {
                                obj = w;
                                for (i = 3; i < maxCol; i++)
                                {
                                    str = parseStr[i];
                                    if (str != "")
                                    {
                                        constrInd = dict.GetIndexByShortName(str);
                                        if (constrInd > -1)
                                        {
                                            ind = ConstructionList.FindIndex(delegate(Construction item) { return item.Code == dict[constrInd].Code; });
                                            if (ind > -1)
                                            {
                                                constr = ConstructionList[ind];
                                            }
                                            else
                                            {
                                                constr = new Construction();
                                                constr.Code = dict[constrInd].Code;
                                                constr.NGDUcode = of.ParamsDict.NGDUCode;
                                                constr.Name = dict[constrInd].ShortName;
                                                if ((constr.Name.IndexOf("ГЗУ") > -1) ||
                                                    (constr.Name.IndexOf("БГ") > -1))
                                                {
                                                    constr.Level = 0;
                                                }
                                                else if ((constr.Name.IndexOf("КНС") > -1) ||
                                                         (constr.Name.IndexOf("ТВО") > -1))
                                                {
                                                    constr.Level = 1;
                                                }
                                                else if ((constr.Name.IndexOf("УПН") > -1) ||
                                                         (constr.Name.IndexOf("УПС") > -1) ||
                                                         (constr.Name.IndexOf("УКПН") > -1) ||
                                                         (constr.Name.IndexOf("ППСН") > -1))
                                                {
                                                    constr.Level = 2;
                                                }
                                                constr.Index = ConstructionList.Count;
                                                ConstructionList.Add(constr);
                                            }
                                            t = new Tube(obj, constr);
                                            t.Index = ConstrTubesList.Count;
                                            ConstrTubesList.Add(t);
                                            obj = constr;
                                        }
                                        else
                                        {
                                            MessageBox.Show("Не найден объект в справочнике: " + str + " [" + w.UpperCaseName + "," + of.Name + "]");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не найдена скважина: " + str + " [" + of.Name + "]");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Не найдено месторождение " + str);
                        }
                        str = file.ReadLine();
                    }
                    percent = (int)((float)rowInd++ / (float)rowCount);
                    worker.ReportProgress(percent, userState);
                }
                double x = 0;
                for (i = 0; i < ConstructionList.Count; i++)
                {
                    ((Construction)ConstructionList[i]).GetGeom();
                    x += ((Construction)ConstructionList[i]).QLiq;
                }
                for (i = 0; i < OilFields.Count; i++)
                {
                    of = OilFields[i];
                    if (of.MerLoaded) of.ClearMerData(false);
                }
                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();
                if (ConstructionList.Count + ConstrTubesList.Count > 1)
                {
                    WriteConstrToCache(worker, e);
                    return true;
                }
            }
            return false;
        }
        public bool LoadConstrFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            string cacheName = this.path + "\\cache\\Construction.bin";
            if (File.Exists(cacheName))
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Загрузка объектов обустройства из кэша";
                userState.Element = "Объекты обустройства";

                var dict = (DataDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.CONSTRUCTION);
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int i, j, count, tubeCount, ind, progress, iLen, constrInd;
                count = br.ReadInt32();
                tubeCount = br.ReadInt32();
                Construction constr;
                Tube t;
                double x = 0;
                iLen = count * 2 + tubeCount;
                for (i = 0; i < count; i++)
                {
                    constr = new Construction();
                    constr.ReadFromCache(br);
                    constr.Index = i;
                    if (dict != null)
                    {
                        constrInd = dict.GetIndexByCode(constr.Code);
                        constr.Name = dict[constrInd].ShortName;
                    }
                    if (constr.QLiq >= 10000)
                        constr.RatingStr = "Qliq :" + (constr.QLiq / 1000).ToString("#,#0.##") + " тыс.т";
                    else
                        constr.RatingStr = "Qliq :" + constr.QLiq.ToString("#,0.##") + " т";
                    ConstructionList.Add(constr);
                    progress = (int)((float)i * 100 / (float)(iLen));
                    worker.ReportProgress(progress, userState);
                }
                for (i = 0; i < tubeCount; i++)
                {
                    t = new Tube();
                    t.ReadFromCache(br, this);
                    t.Index = i;
                    ConstrTubesList.Add(t);
                    progress = (int)((float)(count + i) * 100 / (float)iLen);
                    worker.ReportProgress(progress, userState);
                }
                br.Close();
                count = ConstructionList.Count;
                for (i = 0; i < count; i++)
                {
                    constr = (Construction)ConstructionList[i];
                    tubeCount = constr.TubesInList.Count;
                    for (j = 0; j < tubeCount; j++)
                    {
                        ind = (int)constr.TubesInList[j];
                        t = (Tube)ConstrTubesList[ind];
                        constr.TubesInList[j] = t;
                        t.ObjOut = constr;
                    }
                    progress = (int)((float)(count + tubeCount + i) * 100 / (float)iLen);
                    worker.ReportProgress(progress, userState);
                }
                return true;
            }
            return false;
        }
        public bool WriteConstrToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (Directory.Exists(path))
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "";
                userState.WorkCurrentTitle = "Запись объектов обустройства в кэш";
                userState.Element = "Объекты обустройства";

                string cacheName = this.path + "\\cache\\Construction.bin";
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(this.ConstructionList.Count);
                bw.Write(this.ConstrTubesList.Count);
                int i;
                for (i = 0; i < this.ConstructionList.Count; i++)
                {
                    ((Construction)ConstructionList[i]).WriteToCache(bw);
                    worker.ReportProgress((int)((float)i * 100 / (float)(this.ConstructionList.Count + this.ConstrTubesList.Count)));
                }

                for (i = 0; i < this.ConstrTubesList.Count; i++)
                {
                    ((Tube)ConstrTubesList[i]).WriteToCache(bw);
                    worker.ReportProgress((int)((float)(this.ConstructionList.Count + i) * 100 / (float)(this.ConstructionList.Count + this.ConstrTubesList.Count)));
                }
                bw.Close();
                return true;
            }
            return false;
        }

        public void ReportMerProduction(BackgroundWorker worker, DoWorkEventArgs e)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Создание отчета по добыче МЭР";
            userState.WorkCurrentTitle = "Создание отчета по добыче МЭР";
            OilField of;
            Well w;
            MerItemEx itemEx;
            int i, j;
            MSOffice.Excel excel = new MSOffice.Excel();
            excel.Visible = false;
            excel.EnableEvents = false;
            excel.NewWorkbook();
            List<object[]> rows = new List<object[]>();
            object[] row;
            int ind;
            List<int> years = new List<int>();
            List<double[]> values = new List<double[]>();
            try
            {
                for (int ofInd = 1; ofInd < OilFields.Count; ofInd++)
                {
                    of = OilFields[ofInd];
                    if (of.Name.StartsWith("ЮСУПОВСКАЯ")) continue;
                    if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                    years.Clear();
                    values.Clear();
                    for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                    {
                        w = of.Wells[wInd];
                        if (w.MerLoaded)
                        {
                            for (i = 0; i < w.mer.Count; i++)
                            {
                                ind = years.IndexOf(w.mer.Items[i].Date.Year);
                                if (ind == -1)
                                {
                                    if (years.Count == 0 || years[years.Count - 1] < w.mer.Items[i].Date.Year)
                                    {
                                        years.Add(w.mer.Items[i].Date.Year);
                                        values.Add(new double[4]);
                                        ind = years.Count - 1;
                                    }
                                    else
                                    {
                                        ind = 0;
                                        for (j = 0; j < years.Count; j++)
                                        {
                                            if (w.mer.Items[i].Date.Year < years[j])
                                            {
                                                ind = j;
                                                break;
                                            }
                                        }
                                        years.Insert(ind, w.mer.Items[i].Date.Year);
                                        values.Insert(ind, new double[4]);
                                    }
                                }
                                values[ind][0] += w.mer.Items[i].Wat + w.mer.Items[i].Oil;
                                values[ind][1] += w.mer.Items[i].Oil;
                                itemEx = w.mer.GetItemEx(i);
                                if ((itemEx.AgentInjId <= 80) || (itemEx.AgentInjId >= 90))
                                {
                                    values[ind][2] += itemEx.Injection;
                                }
                                else
                                {
                                    values[ind][3] += itemEx.Injection;
                                }
                            }
                        }
                    }
                    if (of.Name == "АРЛАНСКОЕ")
                    {
                        ind = -1;
                        for(i = 1; i < OilFields.Count;i++)
                        {
                            if (OilFields[i].Name.StartsWith("ЮСУПОВСКАЯ"))
                            {
                                ind = i;
                                break;
                            }
                        }
                        if (ind > -1)
                        {
                            of = OilFields[ind];
                            if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                            for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                            {
                                w = of.Wells[wInd];
                                if (w.MerLoaded)
                                {
                                    for (i = 0; i < w.mer.Count; i++)
                                    {
                                        ind = years.IndexOf(w.mer.Items[i].Date.Year);
                                        if (ind == -1)
                                        {
                                            ind = 0;
                                            for (j = 0; j < years.Count; j++)
                                            {
                                                if (years[j] > w.mer.Items[i].Date.Year)
                                                {
                                                    ind = j;
                                                    break;
                                                }
                                            }
                                            years.Insert(ind, w.mer.Items[i].Date.Year);
                                            values.Insert(ind, new double[4]);
                                        }
                                        values[ind][0] += w.mer.Items[i].Wat + w.mer.Items[i].Oil;
                                        values[ind][1] += w.mer.Items[i].Oil;
                                        itemEx = w.mer.GetItemEx(i);
                                        if ((itemEx.AgentInjId <= 80) || (itemEx.AgentInjId >= 90))
                                        {
                                            values[ind][2] += itemEx.Injection;
                                        }
                                        else
                                        {
                                            values[ind][3] += itemEx.Injection;
                                        }
                                    }
                                }
                            }
                        }
                        of = OilFields[ofInd];
                    }
                    for (i = 0; i < years.Count; i++)
                    {
                        row = new object[6];
                        row[0] = of.Name;
                        row[1] = years[i];
                        row[2] = values[i][0];
                        row[3] = values[i][1];
                        row[4] = values[i][2];
                        row[5] = values[i][3];
                        rows.Add(row);
                    }
                    userState.Element = of.Name;
                    worker.ReportProgress(100, userState);
                    of.ClearMerData(true);
                }
                row = new object[6];
                row[0] = "Месторождение";
                row[1] = "Год";
                row[2] = "Добыча жидкости, т";
                row[3] = "Добыча нефти, т";
                row[4] = "Закачка, м3";
                row[5] = "Закачка газа, м3";
                rows.Insert(0, row);
                excel.SetRowsArray(0, 0, rows);
            }
            catch
            {
                excel.Quit();
                excel = null;
                throw;
            }
            finally
            {
                if (excel != null)
                {
                    excel.Visible = true;
                    excel.EnableEvents = true;
                    excel.Disconnect();
                }
            }
        }


        #region Заводнение
        public void LoadAreasCompFromFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dlg.Filter = "CSV файлы (*.CSV)|*.csv";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string[] rows = File.ReadAllLines(dlg.FileName, Encoding.GetEncoding(1251));
                char[] sep = new char[] { ';' };
                string[] parse;
                string ofName;
                int areaIndex = 0;
                double comp = 0;
                int ind;
                OilField of;
                Area area;
                Palette pal = new Palette();
                parse = rows[0].Split(sep);
                if (parse.Length != 3) return;
                try
                {
                    pal.Add(Convert.ToDouble(parse[0]), Color.Blue);
                    pal.Add(Convert.ToDouble(parse[1]), Color.LightGreen);
                    pal.Add(Convert.ToDouble(parse[2]), Color.Red);
                }
                catch
                {
                    MessageBox.Show("Ошибка загрузки палитры!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    pal.Clear();
                    pal.Add(0, Color.Blue);
                    pal.Add(100, Color.LightGreen);
                    pal.Add(200, Color.Red);
                }
                for (int i = 1; i < rows.Length; i++)
                {
                    parse = rows[i].Split(sep);
                    try
                    {
                        ofName = parse[0].Trim();
                        areaIndex = Convert.ToInt32(parse[1].Trim()) - 1;
                        comp = Convert.ToDouble(parse[2].Trim());
                        ind = GetOFIndex(ofName.ToUpper());
                        if (ind > -1)
                        {
                            of = OilFields[ind];
                            if ((areaIndex >= 0) && (areaIndex < of.Areas.Count))
                            {
                                area = (Area)of.Areas[areaIndex];
                                if (comp == 0)
                                {
                                    area.Compensation = 0;
                                }
                                else
                                {
                                    area.contour.fillBrush = new SolidBrush(pal.GetColorByX(comp));
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Не найдено месторождение: " + ofName);
                        }
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                }
            }
        }
        public void SetAreasOverlay(bool Overlay)
        {
            OilField of;
            Area area;
            for (int i = 1; i < OilFields.Count; i++)
            {
                of = OilFields[i];
                for (int j = 0; j < of.Areas.Count; j++)
                {
                    area = (Area)of.Areas[j];
                    if ((!Overlay) || (area.contour.fillBrush != null))
                    {
                        area.ShowOverlay = Overlay;
                    }
                }
            }
        }
        #endregion

        #region Load Deposits from File
        public void LoadDepositsFromFolder(BackgroundWorker worker, DoWorkEventArgs e, MainForm mainForm, string InitPath)
        {
            if (Directory.Exists(InitPath))
            {
                int i, j;
                int ind, code;
                OilField of;
                Well w;
                char[] sep = new char[] { ';', '=' };
                char[] sepCoord = new char[] { ' ' };
                string[] parseLine;
                List<Deposit> deposits = new List<Deposit>();
                Deposit dep;
                FileInfo[] files;
                string StratumName, line;
                var stratumDict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                List<string> errors = new List<string>();
                StreamReader sr;
                int lastParentCode = -1;
                int lastIndex;
                double x, y, ExReserves, xG, yG;
                StratumTreeNode node = null;
                DirectoryInfo root = new DirectoryInfo(InitPath);
                DirectoryInfo[] dirs = root.GetDirectories();
                List<Deposit> outDeposits = new List<Deposit>();
                bool find;
                string contourName, newName;
                for (int ofInd = 0; ofInd < dirs.Length; ofInd++)
                {
                    ind = GetOFIndex(dirs[ofInd].Name);
                    if (ind > -1)
                    {
                        of = OilFields[ind];
                        deposits.Clear();
                        files = dirs[ofInd].GetFiles("*.kts");
                        for (int fInd = 0; fInd < files.Length; fInd++)
                        {
                            StratumName = Path.GetFileNameWithoutExtension(files[fInd].Name);
                            code = stratumDict.TryGetLocalCodeByServerPlastName(StratumName);
                            newName = string.Empty;
                            if (code == -1)
                            {
                                using (StratumConversionForm ConversionForm = new StratumConversionForm(stratumDict))
                                {
                                    if (ConversionForm.ShowDialog(string.Empty, string.Empty, string.Empty, string.Empty, 0, StratumName) == DialogResult.OK)
                                    {
                                        code = ConversionForm.ConversionID;
                                        newName = stratumDict.GetShortNameByCode(code);
                                    }
                                    else
                                    {
                                        code = -1;
                                    }
                                }
                            }
                            lastIndex = deposits.Count;
                            if (code > 0)
                            {
                                outDeposits.Clear();
                                if (newName.Length > 0)
                                {
                                    if (File.Exists(files[fInd].FullName.Replace(files[fInd].Name, newName + ".kts")))
                                    {
                                        File.Delete(files[fInd].FullName.Replace(files[fInd].Name, newName + ".kts"));
                                    }

                                    files[fInd].MoveTo(files[fInd].FullName.Replace(files[fInd].Name, newName + ".kts"));
                                }
                                sr = new StreamReader(files[fInd].FullName, Encoding.GetEncoding(1251));
                                line = sr.ReadLine();
                                while (!sr.EndOfStream)
                                {
                                    // шапка
                                    parseLine = line.Split(sep);
                                    if (parseLine.Length > 15)
                                    {
                                        dep = new Deposit();
                                        dep.StratumCode = code;
                                        dep.pvt.StratumCode = code;
                                        dep.Name = parseLine[16].Replace("Залежь", "");
                                        dep.contour.Text = parseLine[0];
                                        
                                        line = sr.ReadLine();
                                        ExReserves = 0;
                                        if (node == null || node.StratumCode != code)
                                        {
                                            node = stratumDict.GetStratumTreeNode(code);
                                        }
                                        lastParentCode = -1;
                                        if (node != null && node.GetParentLevelByCode(lastParentCode) == -1)
                                        {
                                            for (i = 0; i < of.MerStratumCodes.Count; i++)
                                            {
                                                if (node.GetParentLevelByCode(of.MerStratumCodes[i]) > -1)
                                                {
                                                    lastParentCode = of.MerStratumCodes[i];
                                                    break;
                                                }
                                            }
                                            if (node.GetParentLevelByCode(lastParentCode) == -1)
                                            {
                                                for (i = 0; i < this.MerStratumCodes.Count; i++)
                                                {
                                                    if (node.GetParentLevelByCode(this.MerStratumCodes[i]) > -1)
                                                    {
                                                        lastParentCode = this.MerStratumCodes[i];
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        dep.StratumParentCode = (lastParentCode != -1) ? lastParentCode : dep.StratumCode;
                                        while(!sr.EndOfStream && line.Length > 0)
                                        {
                                            parseLine = line.Split(sep);
                                            if (parseLine.Length > 1)
                                            {
                                                switch(parseLine[0])
                                                {
                                                    case "plSqrZ":
                                                        dep.Params.Square = Convert.ToSingle(parseLine[1]);
                                                        break;
                                                    case "plPrtZ":
                                                        dep.Params.InitHoil = Convert.ToSingle(parseLine[1]);
                                                        break;
                                                    case "plVolZ":
                                                        dep.Params.OilVolume = Convert.ToDouble(parseLine[1]);
                                                        break;
                                                    case "plKpoZ":
                                                        dep.pvt.Porosity = Convert.ToDouble(parseLine[1]);
                                                        break;
                                                    case "plKnnZ":
                                                        dep.pvt.OilInitialSaturation = Convert.ToDouble(parseLine[1]);
                                                        break;
                                                    case "plDenZ":
                                                        dep.pvt.OilDensity = Convert.ToDouble(parseLine[1]);
                                                        break;
                                                    case "plKppZ":
                                                        dep.pvt.OilVolumeFactor = 1 / Convert.ToDouble(parseLine[1]);
                                                        break;
                                                    case "plGResZ":
                                                        dep.Params.InitGeoOilReserv = Convert.ToDouble(parseLine[1]) * 1000;
                                                        break;
                                                    case "plRResZ":
                                                        ExReserves = Convert.ToDouble(parseLine[1]) * 1000;
                                                        break;
                                                    case "plODobZ":
                                                        //dep.Params.AccumOil = Convert.ToDouble(parseLine[1]);
                                                        break;
                                                    case "plGaSZ":
                                                        dep.Params.GazContent = Convert.ToSingle(parseLine[1]);
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                            line = sr.ReadLine();
                                        }
                                        if (ExReserves > 0) dep.pvt.KIN = ExReserves / dep.Params.InitGeoOilReserv;
                                        if (line.Length == 0) line = sr.ReadLine();
                                        while (!sr.EndOfStream)
                                        {
                                            if (line.Length == 0 || line[0] == '*') break;
                                            parseLine = line.Split(sepCoord, StringSplitOptions.RemoveEmptyEntries);
                                            if (parseLine.Length > 1)
                                            {
                                                if (dep.contour == null) dep.contour = new Contour();
                                                x = Convert.ToSingle(parseLine[0]);
                                                y = Convert.ToSingle(parseLine[1]);
                                                of.CoefDict.GlobalCoordFromLocal(x, y, out xG, out yG);
                                                dep.contour._points.Add(xG, yG);
                                            }
                                            line = sr.ReadLine();
                                        }
                                        if (dep.contour != null && dep.contour._points.Count > 1 && 
                                            dep.contour._points[0].X == dep.contour._points[dep.contour._points.Count - 1].X &&
                                            dep.contour._points[0].Y == dep.contour._points[dep.contour._points.Count - 1].Y)
                                        {
                                            dep.contour._points.RemoveAt(dep.contour._points.Count - 1);
                                        }
                                        dep.contour._Closed = true;
                                        if (dep.contour != null)
                                        {
                                            dep.contour.Name = "Залежь " + dep.Name;
                                            dep.contour.VisibleName = true;
                                            dep.contour._ContourType = -6;
                                            if (!dep.contour.GetClockWiseByX(true)) dep.contour.Invert();
                                        }
                                        if (dep.contour._points.Count > 3)
                                        {

                                            if (dep.Params.Square + dep.Params.InitGeoGasReserv + dep.Params.InitGeoOilReserv > 0)
                                            {
                                                deposits.Add(dep);
                                            }
                                            else
                                            {
                                                outDeposits.Add(dep);
                                            }
                                        }
                                        else
                                        {
                                            errors.Add(string.Format("Контур залежи из 2 и менее точек! {0} {1} [{2}]", dep.contour.Text, node.Name, of.Name));
 
                                        }
                                    }
                                }
                                // проверка пересечения контуров
                                int h;
                                for (i = lastIndex; i < deposits.Count; i++)
                                {
                                    find = false;
                                    h = (int)(deposits[i].contour._points.Count / 10.0);
                                    if (h < 1) h = 1;
                                    PointD pt = deposits[i].contour._points.GetAvarage();
                                    for (j = lastIndex; j < deposits.Count; j++)
                                    {
                                        if (i == j || deposits[i].StratumCode != deposits[j].StratumCode) continue;
                                        for (int k = 0; k < deposits[i].contour._points.Count; k += h)
                                        {
                                            if (deposits[j].contour.PointBodyEntry(pt.X, pt.Y))
                                            {
                                                find = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (find)
                                    {
                                        errors.Add(string.Format("Контур залежи пересекается или лежит внутри другой залежи! {0} {1} [{2}]", deposits[i].contour.Text, node.Name, of.Name));
                                    }
                                }

                                if (outDeposits.Count > 0)
                                {
                                    List<int> indexes = new List<int>();
                                    for(j = 0; j < outDeposits.Count;j++)
                                    {
                                        ind = -1;
                                        for (i = lastIndex; i < deposits.Count; i++)
                                        {
                                            for (int k = 0; k < outDeposits[j].contour._points.Count; k++)
                                            {
                                                if (deposits[i].contour.PointBodyEntry(outDeposits[j].contour._points[k].X, outDeposits[j].contour._points[k].Y))
                                                {
                                                    indexes.Add(i);
                                                    break;
                                                }
                                            }
                                        }
                                        if (indexes.Count > 0)
                                        {
                                            for (i = 0; i < indexes.Count; i++)
                                            {
                                                if (deposits[indexes[i]].contour.cntrsOut == null) deposits[indexes[i]].contour.cntrsOut = new List<Contour>();
                                                if (!outDeposits[j].contour.GetClockWiseByX(true))
                                                {
                                                    outDeposits[j].contour.Invert();
                                                }
                                                deposits[indexes[i]].contour.cntrsOut.Add(outDeposits[j].contour);
                                            }
                                            indexes.Clear();
                                        }
                                        else
                                        {
                                            of.CoefDict.LocalCoordFromGlobal(outDeposits[j].contour._points[0].X, outDeposits[j].contour._points[0].Y, out x, out y);
                                            errors.Add(string.Format("Для контура замещения не найдена залежь! {0} {1} [{2}]", outDeposits[j].contour.Text, node.Name, of.Name));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errors.Add(string.Format("Не найден пласт в справочнике '{0}' [{1}]", StratumName, of.Name));
                            }
                        }
                        deposits.Sort(delegate(Deposit dep1, Deposit dep2) 
                        {
                            int res = dep1.StratumParentCode.CompareTo(dep2.StratumParentCode);
                            if (res == 0) res = dep1.StratumCode.CompareTo(dep2.StratumCode);
                            if(res == 0) res = dep1.Name.CompareTo(dep2.Name);
                            return res;
                          });
                        of.Deposits = deposits;
                        of.WriteDepositsToCache(worker, e);
                    }
                }
                if (errors.Count > 0)
                {
                    SmartPlus.MSOffice.Excel excel = new MSOffice.Excel();
                    excel.Visible = false;
                    excel.NewWorkbook();
                    excel.EnableEvents = false;
                    for (i = 0; i < errors.Count; i++)
                    {
                        excel.SetValue(i, 0, errors[i]);
                    }
                    excel.EnableEvents = true;
                    excel.Visible = true;
                }
            }
        }
        #endregion

        #region Load Oilfield Bizplan
        public bool LoadOilfieldsBizplanFromFile(BackgroundWorker worker, DoWorkEventArgs e, string FilePath)
        {
            bool retValue = false;
            if (File.Exists(FilePath))
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Загрузка Бизнес-плана добычи";
                userState.WorkCurrentTitle = "Загрузка Бизнес-плана добычи";
                userState.Element = string.Empty;

                SmartPlus.MSOffice.Excel excel = new SmartPlus.MSOffice.Excel();
                excel.OpenWorkbook(FilePath);
                int i, k, j, iLen = 0, progress = 0, ind;

                OilField of = null;
                OilfieldBizplan bizPlan;
                string OFName = string.Empty, bizPlanName = "Бизнес-план";
                int CountRows = excel.GetLastUsedRow();

                worker.ReportProgress(0, userState);
                List<object[]> errors = new List<object[]>();
                object[] errRow;
                object[,] row;
                DateTime StartDate = DateTime.MinValue;
                int sheetsCount = excel.GetWorkSheetsCount();
                int LiqSheetIndex = -1, OilSheetIndex = -1, InjSheetIndex = -1;


                #region Test WorkSheets
                for (i = 0; i < sheetsCount; i++)
                {
                    excel.SetActiveSheet(i);
                    switch (excel.GetActiveSheetName())
                    {
                        case "Добыча жидкости":
                            iLen = excel.GetLastUsedRow();
                            LiqSheetIndex = i;
                            break;
                        case "Добыча нефти":
                            OilSheetIndex = i;
                            break;
                        case "Закачка":
                            InjSheetIndex = i;
                            break;
                        case "Расчет":
                            StartDate = Convert.ToDateTime(excel.GetValue(0, 1));
                            break;
                    }
                }
                if (StartDate == DateTime.MinValue)
                {
                    errRow = new object[2];
                    errRow[0] = 0;
                    errRow[1] = string.Format("Не найдена стартовая дата и/или лист Расчет");
                    errors.Add(errRow);
                }
                if (LiqSheetIndex == -1)
                {
                    errRow = new object[2];
                    errRow[0] = 0;
                    errRow[1] = string.Format("Не найден лист Добыча жидкости");
                    errors.Add(errRow);
                }
                if (OilSheetIndex == -1)
                {
                    errRow = new object[2];
                    errRow[0] = 0;
                    errRow[1] = string.Format("Не найден лист Добыча нефти");
                    errors.Add(errRow);
                }
                if (InjSheetIndex == -1)
                {
                    errRow = new object[2];
                    errRow[0] = 0;
                    errRow[1] = string.Format("Не найден лист Добыча закачки");
                    errors.Add(errRow);
                }
                #endregion

                if (errors.Count == 0)
                {
                    i = 2;
                    while (i < iLen)
                    {
                        // лист Добыча жидкости
                        excel.SetActiveSheet(LiqSheetIndex);
                        if (Convert.ToDateTime(excel.GetValue(0, 1)) != StartDate)
                        {
                            errRow = new object[2];
                            errRow[0] = 0;
                            errRow[1] = string.Format("Не совпадение дат на листах Расчет и Добыча жидкости");
                            errors.Add(errRow);
                            break;
                        }
                        row = excel.GetRangeValues(i, 0, i, 30);

                        if (row.Length > 0)
                        {
                            try
                            {
                                OFName = Convert.ToString(row[1, 1]).Trim().ToUpper();
                                if ((OFName.Length == 0) ||
                                    (OFName.IndexOf("НГДУ", StringComparison.OrdinalIgnoreCase) > -1) ||
                                    (OFName.IndexOf("Итого", StringComparison.OrdinalIgnoreCase) > -1) ||
                                    (OFName.IndexOf("ООО", StringComparison.OrdinalIgnoreCase) > -1) ||
                                    (OFName.IndexOf("НКЦДПНГ", StringComparison.OrdinalIgnoreCase) > -1))
                                {
                                    i++;
                                    continue;
                                }
                                ind = GetOFIndex(OFName);
                                if (ind == -1)
                                {
                                    errRow = new object[2];
                                    errRow[0] = i;
                                    errRow[1] = string.Format("Месторождение {0} не найдено", OFName);
                                    errors.Add(errRow);
                                    i++;
                                    continue;
                                }
                                of = OilFields[ind];
                                if (!of.BizPlanLoaded)
                                {
                                    // если надо дописать бизнес-план
                                    //of.BizPlanLoadFromCache(worker, e);
                                    if (of.BizPlanCollection == null)
                                    {
                                        of.BizPlanCollection = new OilfieldBizplanCollection();
                                        of.BizPlanLoaded = true;
                                    }
                                }

                                bizPlan = new OilfieldBizplan();
                                bizPlan.ApprovalDate = StartDate;
                                bizPlan.StartDate = StartDate;
                                bizPlan.Name = bizPlanName;
                                bizPlan.MonthLevels = new OilfieldBizplanLevel[12 * 2];
                                bizPlan.HalfYearLevels = new OilfieldBizplanLevel[3 * 2];



                                for (j = 2; j < 26; j++)
                                {
                                    bizPlan.MonthLevels[j - 2].Liquid = Convert.ToSingle(row[1, j]);
                                }
                                for (j = 26; j < 32; j++)
                                {
                                    bizPlan.HalfYearLevels[j - 26].Liquid = Convert.ToSingle(row[1, j]);
                                }

                                // Лист Добыча нефти
                                excel.SetActiveSheet(OilSheetIndex);
                                row = excel.GetRangeValues(i, 0, i, 30);
                                if (Convert.ToString(row[1, 1]).Trim().ToUpper() != OFName)
                                {
                                    errRow = new object[2];
                                    errRow[0] = i;
                                    errRow[1] = string.Format("Месторождение на листах не совпадают {0} - {1} ['Добыча жидкости' - 'Добыча нефти']", OFName, Convert.ToString(row[0, 0]).Trim().ToUpper());
                                    errors.Add(errRow);
                                    i++;
                                    continue;
                                }
                                for (j = 2; j < 26; j++)
                                {
                                    bizPlan.MonthLevels[j - 2].Oil = Convert.ToSingle(row[1, j]);
                                }
                                for (j = 26; j < 32; j++)
                                {
                                    bizPlan.HalfYearLevels[j - 26].Oil = Convert.ToSingle(row[1, j]);
                                }

                                // Лист Закачка
                                excel.SetActiveSheet(InjSheetIndex);
                                row = excel.GetRangeValues(i, 0, i, 30);
                                if (Convert.ToString(row[1, 1]).Trim().ToUpper() != OFName)
                                {
                                    errRow = new object[2];
                                    errRow[0] = i;
                                    errRow[1] = string.Format("Месторождение на листах не совпадают {0} - {1} ['Добыча жидкости' - 'Закачка']", OFName, Convert.ToString(row[0, 0]).Trim().ToUpper());
                                    errors.Add(errRow);
                                    i++;
                                    continue;
                                }
                                for (j = 2; j < 26; j++)
                                {
                                    bizPlan.MonthLevels[j - 2].Injection = Convert.ToSingle(row[1, j]);
                                }
                                for (j = 26; j < 32; j++)
                                {
                                    bizPlan.HalfYearLevels[j - 26].Injection = Convert.ToSingle(row[1, j]);
                                }

                                of.BizPlanCollection.Add(bizPlan);
                                of.ReCalcDeltaBP(worker, e);
                                of.WriteBizPlanToCache(worker, e);
                            }
#if !DEBUG
                            catch
                            {
                                errRow = new object[2];
                                errRow[0] = i;
                                errRow[2] = string.Format("Ошибка загрузки данных месторождения {0}", OFName);
                                errors.Add(errRow);
                                i++;
                                continue;
                            }
#endif
                            finally { }
                        }
                        if (of != null)
                        {
                            userState.Element = string.Format("{0}", of.Name);
                        }
                        progress = (int)((float)i * 100f / (float)iLen);
                        worker.ReportProgress(progress, userState);
                        i++;
                    }
                }

                userState.WorkCurrentTitle = "Выгрузка отчета об ошибках в Microsoft Excel";
                worker.ReportProgress(0, userState);
                if (errors.Count > 0)
                {
                    excel.NewWorkbook();
                    excel.SetActiveSheetName("Ошибки загрузки");
                    object[,] heap = (object[,])Array.CreateInstance(typeof(object), new int[2] { 1, 3 }, new int[2] { 0, 0 });
                    int col = 0;
                    heap[0, col++] = "Строка";
                    heap[0, col++] = "Ошибка";
                    excel.SetRange(0, 0, 0, 1, heap);
                    excel.SetRowsArray(worker, e, 1, 0, errors);
                    if (excel != null)
                    {
                        excel.EnableEvents = true;
                        excel.Visible = true;
                        excel.Disconnect();
                    }
                }
            }
            return retValue;
        }
        #endregion

        #region Load Core
        public bool LoadCoreFromFile(BackgroundWorker worker, DoWorkEventArgs e, string SourcePath)
        {
            bool retValue = false;
            char[] delimeter = new char[] { ';' };
            string[] parseStr;
            string str, OFName, OFAreaName, PlastName, SkvName = "";
            var dictArea = (OilFieldAreaDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);

            Well w = null;
            SkvCoreItem[] coreItems;
            SkvCoreItem coreItem;
            ArrayList list = new ArrayList();
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Загрузка данных керна с файла";
            userState.WorkCurrentTitle = "Загрузка данных керна с файла";
            userState.Element = "";
            byte state;
            if (File.Exists(SourcePath))
            {
                StreamReader file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // считаем количество строк
                int i, k, j, iLen = 0, progress = 0, ind = 0, areaInd;
                bool bAll = false;
                OilField of = null, predOF = null;
                while (!file.EndOfStream)
                {
                    file.ReadLine();
                    iLen++;
                }
                file.Close();
                file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                file.ReadLine();                  // шапка - пропускаем
                j = 0;
                worker.ReportProgress(0, userState);
                while (!file.EndOfStream)
                {
                    str = file.ReadLine();
                    parseStr = str.Split(delimeter);
                    OFName = parseStr[0].ToUpper();
                    OFAreaName = parseStr[1];

                    if ((of == null) || (of.Name != OFName))
                    {
                        if (of != null)
                        {
                            of.WriteCoreToCache(worker, e);
                        }
                        list.Clear();
                        of = null;
                        w = null;
                        ind = this.GetOFIndex(OFName);
                        if (ind > -1)
                        {
                            of = OilFields[ind];
                        }
                        else
                        {
                            MessageBox.Show("Не найдено месторождение '" + OFName + "'");
                            return false;
                        }

                    }
                    areaInd = dictArea.GetIndexByShortName(OFAreaName);
                    if (areaInd == -1)
                    {
                        MessageBox.Show("Не найдена площадь " + OFAreaName.Replace(" ", "_") + " в справочнике[" + OFName + ", " + OFAreaName + "]");
                        return false;
                    }
                    if ((w == null) || (parseStr[2] != SkvName))
                    {
                        if (list.Count > 0)
                        {
                            coreItems = new SkvCoreItem[list.Count];
                            for (i = 0; i < list.Count; i++)
                            {
                                coreItems[i] = (SkvCoreItem)list[i];
                                //coreItems[i].Top = coreItem.Top;
                                //coreItems[i].Bottom = coreItem.Bottom;
                                //coreItems[i].CoreRecovery = coreItem.CoreRecovery;
                                //coreItems[i].CoreStored = coreItem.CoreStored;
                                //coreItems[i].StoredBoxCount = coreItem.StoredBoxCount;
                                //coreItems[i].CoreState = coreItem.CoreState;
                                //coreItems[i].CoreMarkState = coreItem.CoreMarkState;
                            }
                            if (w.core == null) w.core = new SkvCore();
                            w.core.SetItems(coreItems);
                            list.Clear();
                        }
                        SkvName = parseStr[2];
                        ind = of.GetWellIndex(SkvName, dictArea[areaInd].Code);
                        if (ind == -1) ind = of.GetWellIndex(SkvName);

                        if (ind != -1)
                        {
                            w = of.Wells[ind];
                        }
                        else
                        {
                            w = null;
                            MessageBox.Show("Не найдена скважина'" + SkvName + " на месторождении:'" + OFName + "'");
                            return false;
                        }
                    }

                    coreItem = new SkvCoreItem();
                    try
                    {
                        coreItem.Top = Convert.ToSingle(parseStr[3]);
                        coreItem.Bottom = Convert.ToSingle(parseStr[4]);
                        coreItem.CoreRecovery = Convert.ToSingle(parseStr[5]);
                        coreItem.CoreStored = Convert.ToSingle(parseStr[6]);
                        coreItem.StoredBoxCount = (byte)Convert.ToSingle(parseStr[7]);
                        coreItem.StoredBoxCount = (byte)Convert.ToSingle(parseStr[7]);
                        str = parseStr[8].ToUpper();
                        state = 1;
                        if (str.IndexOf("НЕУД") > -1)
                        {
                            state = 0;
                        }
                        else if (str.IndexOf("УД") > -1)
                        {
                            state = 1;
                        }
                        else if (str.IndexOf("ХОР") > -1)
                        {
                            state = 2;
                        }
                        coreItem.CoreState = state;
                        str = parseStr[9].ToUpper();
                        state = 1;
                        if (str.IndexOf("НЕУД") > -1)
                        {
                            state = 0;
                        }
                        else if (str.IndexOf("УД") > -1)
                        {
                            state = 1;
                        }
                        else if (str.IndexOf("ХОР") > -1)
                        {
                            state = 2;
                        }
                        coreItem.CoreMarkState = state;
                        list.Add(coreItem);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ошибка загрузки данных керна. Строка: " + (j + 2).ToString());
                        return false;
                    }

                    userState.Element = SkvName;
                    progress = (int)((float)j * 100.0f / (float)iLen);
                    worker.ReportProgress(progress, userState);
                    j++;
                }
                if (w != null)
                {
                    if (list.Count > 0)
                    {
                        coreItems = new SkvCoreItem[list.Count];
                        for (i = 0; i < list.Count; i++)
                        {
                            coreItems[i] = (SkvCoreItem)list[i];
                            //coreItems[i].Top = coreItem.Top;
                            //coreItems[i].Bottom = coreItem.Bottom;
                            //coreItems[i].CoreRecovery = coreItem.CoreRecovery;
                            //coreItems[i].CoreStored = coreItem.CoreStored;
                            //coreItems[i].StoredBoxCount = coreItem.StoredBoxCount;
                            //coreItems[i].CoreState = coreItem.CoreState;
                            //coreItems[i].CoreMarkState = coreItem.CoreMarkState;
                        }
                        if (w.core == null) w.core = new SkvCore();
                        w.core.SetItems(coreItems);
                        list.Clear();
                    }
                }
                if (of != null)
                {
                    of.WriteCoreToCache(worker, e);
                }
                file.Close();
                retValue = true;
            }
            return retValue;
        }
        public bool LoadCoreTestFromFile(BackgroundWorker worker, DoWorkEventArgs e, string SourcePath)
        {
            bool retValue = false;
            char[] delimeter = new char[] { ';' };
            string[] parseStr;
            string str, OFName, OFAreaName, SkvName = "", del, Kpr;
            var dictArea = (OilFieldAreaDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            var dictSat = DictList.GetDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
            int areaInd;
            Well w = null;
            SkvCoreTestItem[] coreItems;
            SkvCoreTestItem coreTestItem;
            ArrayList list = new ArrayList();
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Загрузка данных исследований керна с файла";
            userState.WorkCurrentTitle = "Загрузка данных исследований керна с файла";
            userState.Element = "";
            byte satId;
            if (File.Exists(SourcePath))
            {
                StreamReader file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // считаем количество строк
                int i, k, j, iLen = 0, progress = 0, ind = 0;
                bool bAll = false;
                OilField of = null, predOF = null;
                while (!file.EndOfStream)
                {
                    file.ReadLine();
                    iLen++;
                }
                file.Close();
                file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                // шапка - пропускаем
                file.ReadLine();
                j = 0;
                worker.ReportProgress(0, userState);
                while (!file.EndOfStream)
                {
                    str = file.ReadLine();
                    parseStr = str.Split(delimeter);
                    OFName = parseStr[1].ToUpper();
                    OFAreaName = parseStr[2];

                    if ((of == null) || (of.Name != OFName))
                    {
                        if (of != null) of.WriteCoreTestToCache(worker, e);
                        list.Clear();
                        of = null;
                        ind = this.GetOFIndex(OFName);
                        if (ind > -1)
                        {
                            of = OilFields[ind];
                        }
                        else
                        {
                            MessageBox.Show("Не найдено месторождение '" + OFName + "'");
                            return false;
                        }
                    }
                    areaInd = dictArea.GetIndexByShortName(OFAreaName);
                    if (areaInd == -1)
                    {
                        MessageBox.Show("Не найдена площадь " + OFAreaName.Replace(" ", "_") + " в справочнике[" + OFName + ", " + OFAreaName + "]");
                        return false;
                    }
                    
                    if ((w == null) || (parseStr[4] != SkvName))
                    {
                        if (list.Count > 0)
                        {
                            coreItems = new SkvCoreTestItem[list.Count];
                            for (i = 0; i < list.Count; i++)
                            {
                                coreItems[i] = (SkvCoreTestItem)list[i];
                            }
                            if (w.coreTest == null) w.coreTest = new SkvCoreTest();
                            w.coreTest.SetItems(coreItems);
                            list.Clear();
                        }
                        w = null;
                        SkvName = parseStr[4];
                        ind = of.GetWellIndex(SkvName, dictArea[areaInd].Code);
                        if (ind == -1) ind = of.GetWellIndex(SkvName);

                        if (ind != -1)
                        {
                            w = of.Wells[ind];
                        }
                        else
                        {
                            w = null;
                            MessageBox.Show("Не найдена скважина '" + SkvName + "' на месторождении '" + OFName + "'");
                            return false;
                        }
                    }

                    coreTestItem = new SkvCoreTestItem();
                    try
                    {
                        coreTestItem.Date = Convert.ToDateTime(parseStr[0]);
                        coreTestItem.Top = Convert.ToSingle(parseStr[5]);
                        coreTestItem.Bottom = Convert.ToSingle(parseStr[6]);
                        coreTestItem.CoreRecovery = Convert.ToSingle(parseStr[7]);
                        coreTestItem.DistanceTop = Convert.ToSingle(parseStr[8]);
                        coreTestItem.Litology = parseStr[9];
                        satId = 0;
                        try
                        {
                            satId = Convert.ToByte(parseStr[10]);
                        }
                        catch
                        {
                            coreTestItem.SatId = 0;
                        }
                        coreTestItem.SatId = satId;
                        coreTestItem.Name = parseStr[11];
                        coreTestItem.Comment = parseStr[45];

                        for (k = 12; k < 44; k++)
                        {
                            try
                            {
                                if (parseStr[k].Length == 0)
                                {
                                    parseStr[k] = "-9999.0";
                                }
                                else
                                {
                                    Convert.ToSingle(parseStr[k]);
                                }
                            }
                            catch
                            {
                                Kpr = "";
                                if ((parseStr[k].Length > 0) &&
                                    (((k == 13) && (parseStr[k].IndexOf('<') != -1)) ||
                                      ((k == 14) && (parseStr[k].IndexOf('<') != -1)) ||
                                      ((parseStr[k].IndexOf('<') == -1) && (parseStr[k].IndexOf('>') == -1))) &&
                                    (coreTestItem.Comment.IndexOf(parseStr[k]) == -1))
                                {
                                    del = (coreTestItem.Comment.Length > 0) ? ";" : "";
                                    if (((k == 13) && (parseStr[k].IndexOf('<') != -1)) ||
                                        ((k == 14) && (parseStr[k].IndexOf('<') != -1)))
                                    {
                                        Kpr = "Kpr ";
                                    }
                                    coreTestItem.Comment += del + Kpr + parseStr[k];
                                }
                                parseStr[k] = "-9999.0";
                            }
                        }
                        coreTestItem.KporLiquid = Convert.ToSingle(parseStr[12]);
                        coreTestItem.KprAirPP = Convert.ToSingle(parseStr[13]);
                        coreTestItem.KprAirPR = Convert.ToSingle(parseStr[14]);
                        coreTestItem.Pressure = Convert.ToSingle(parseStr[17]);
                        coreTestItem.KporHelium = Convert.ToSingle(parseStr[19]);
                        coreTestItem.KprHeliumPP = Convert.ToSingle(parseStr[20]);
                        coreTestItem.KprHeliumPR = Convert.ToSingle(parseStr[21]);
                        coreTestItem.CarbonateTiff = Convert.ToSingle(parseStr[22]);
                        coreTestItem.CarbonateDolomite = Convert.ToSingle(parseStr[23]);
                        coreTestItem.CarbonateInsoluble = Convert.ToSingle(parseStr[24]);
                        coreTestItem.AdaBeforeExtract = Convert.ToSingle(parseStr[25]);
                        coreTestItem.AdaAfterExtract = Convert.ToSingle(parseStr[26]);
                        coreTestItem.Wettability = Convert.ToSingle(parseStr[27]);
                        coreTestItem.Porosity = Convert.ToSingle(parseStr[28]);
                        coreTestItem.SatWatFinal = Convert.ToSingle(parseStr[29]);
                        coreTestItem.DensityMineral = Convert.ToSingle(parseStr[30]);
                        coreTestItem.DensityVolume = Convert.ToSingle(parseStr[31]);
                        coreTestItem.PartSize500 = Convert.ToSingle(parseStr[32]);
                        coreTestItem.PartSize250 = Convert.ToSingle(parseStr[33]);
                        coreTestItem.PartSize100 = Convert.ToSingle(parseStr[34]);
                        coreTestItem.PartSize50 = Convert.ToSingle(parseStr[35]);
                        coreTestItem.PartSize10 = Convert.ToSingle(parseStr[36]);
                        coreTestItem.PartSize0 = Convert.ToSingle(parseStr[37]);
                        coreTestItem.RadioActivityK = Convert.ToSingle(parseStr[38]);
                        coreTestItem.RadioActivityKPercent = Convert.ToSingle(parseStr[39]);
                        coreTestItem.RadioActivityTh = Convert.ToSingle(parseStr[40]);
                        coreTestItem.RadioActivityThPercent = Convert.ToSingle(parseStr[41]);
                        coreTestItem.RadioActivityRa = Convert.ToSingle(parseStr[42]);
                        coreTestItem.RadioActivityRaPercent = Convert.ToSingle(parseStr[43]);
                        coreTestItem.RadioActivityA = Convert.ToSingle(parseStr[44]);
                        list.Add(coreTestItem);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ошибка загрузки данных исследований керна. Строка: " + (j + 2).ToString());
                        return false;
                    }

                    userState.Element = SkvName;
                    progress = (int)((float)j * 100.0f / (float)iLen);
                    worker.ReportProgress(progress, userState);
                    j++;
                }
                if (w != null)
                {
                    if (list.Count > 0)
                    {
                        coreItems = new SkvCoreTestItem[list.Count];
                        for (i = 0; i < list.Count; i++)
                        {
                            coreItems[i] = (SkvCoreTestItem)list[i];
                        }
                        if (w.coreTest == null) w.coreTest = new SkvCoreTest();
                        w.coreTest.SetItems(coreItems);
                        list.Clear();
                    }
                }
                if (of != null) of.WriteCoreTestToCache(worker, e);
                file.Close();
                retValue = true;
            }
            return retValue;
        }
        #endregion

        public void ResetWellIconAndBubbleByFilter(BackgroundWorker worker, DoWorkEventArgs e, FilterOilObjects filter, bool HideWellsWithoutCoord)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Перерисовка карты по выбранному списку объектов";
            userState.WorkCurrentTitle = "Перерисовка карты по выбранному списку объектов";
            userState.Element = "";
            int i, j;
            OilField of;
            Well w;
            WellIcon well_icon;
            int ind, k;
            bool findCoord;
            StratumDictionary dict = filter.stratumDict;
            for (i = 1; i < OilFields.Count; i++)
            {
                of = OilFields[i];
                for (j = 0; j < of.Wells.Count; j++)
                {
                    w = of.Wells[j];
                    w.Visible = true;
                    if (e.Cancel)
                    {
                        return;
                    }
                    #region Well Icons
                    WellsIconList iconList = w.icons;
                    for (int x = 0; x < 2; x++)
                    {
                        if (x == 0)
                        {
                            iconList = w.icons;
                        }
                        else if (w.BubbleList != null && w.BubbleIcons != null && w.BubbleIcons.Count > 0)
                        {
                            iconList = w.BubbleIcons;
                        }
                        else
                        {
                            break;
                        }
                        if (filter.AllSelected)
                        {
                            w.SetActiveAllIcon();
                        }
                        else
                        {
                            well_icon = null;
                            if (iconList.Count < 2)
                            {
                                well_icon = iconList.GetActiveIcon();
                            }
                            else
                            {
                                for (k = 0; k < filter.SelectedObjects.Count; k++)
                                {
                                    well_icon = iconList.GetIconByCode(dict[(int)filter.SelectedObjects[k]].Code);
                                    if (well_icon != null) break;
                                }
                            }
                            if (well_icon != null)
                            {
                                w.SetActiveIcon(well_icon.StratumCode);
                            }
                            else
                            {
                                double xG, yG;
                                int code;
                                findCoord = false;
                                if (w.coord.Count > 0)
                                {
                                    for (k = 0; k < filter.SelectedObjects.Count; k++)
                                    {
                                        code = dict[(int)filter.SelectedObjects[k]].Code;
                                        if ((HideWellsWithoutCoord) && (of.MerStratumCodes.IndexOf(code) == -1))
                                        {
                                            continue;
                                        }
                                        ind = w.coord.GetIndexByPlastCode(code);
                                        if (ind != -1 && (w.coord.Items[ind].X + w.coord.Items[ind].Y != 0))
                                        {
                                            xG = w.coord.Items[ind].X;
                                            yG = w.coord.Items[ind].Y;
                                            //of.CoefDict.GlobalCoordFromLocal(w.coord.Items[ind].X, w.coord.Items[ind].Y, out xG, out yG);
                                            iconList.SetEmptyIcon(xG, yG);
                                            w.X = xG;
                                            w.Y = yG;
                                            findCoord = true;
                                            break;
                                        }
                                    }
                                }
                                if (HideWellsWithoutCoord && !findCoord)
                                {
                                    w.Visible = false;
                                }
                                else if (!HideWellsWithoutCoord && !findCoord)
                                {
                                    iconList.SetEmptyIcon(w.X, w.Y);
                                }
                            }
                        }
                    }
                    #endregion
                    w.SetBrushFromIcon();
                    if ((of.BubbleMapLoaded) && (w.BubbleList != null))
                    {
                        w.BubbleList.SetBubbleByFilter(filter);
                    }
                }
                for (j = 0; j < of.ProjWells.Count; j++)
                {
                    w = of.ProjWells[j];
                    w.Visible = true;
                    if (e.Cancel)
                    {
                        return;
                    }
                    #region Well Icons
                    WellsIconList iconList = w.icons;
                    if (filter.AllSelected)
                    {
                        w.SetActiveAllIcon();
                    }
                    else
                    {
                        well_icon = null;
                        if (iconList.Count < 2)
                        {
                            well_icon = iconList.GetActiveIcon();
                        }
                        else
                        {
                            for (k = 0; k < filter.SelectedObjects.Count; k++)
                            {
                                well_icon = iconList.GetIconByCode(dict[(int)filter.SelectedObjects[k]].Code);
                                if (well_icon != null) break;
                            }
                        }
                        if (well_icon != null)
                        {
                            w.SetActiveIcon(well_icon.StratumCode);
                        }
                        else
                        {
                            w.Visible = false;
                            //double xG, yG;
                            //int code;
                            //findCoord = false;
                            //if (w.coord.Count > 0)
                            //{
                            //    for (k = 0; k < filter.SelectedObjects.Count; k++)
                            //    {
                            //        code = dict[(int)filter.SelectedObjects[k]].Code;
                            //        ind = w.coord.GetIndexByPlastCode(code);
                            //        if (ind != -1 && (w.coord.Items[ind].X + w.coord.Items[ind].Y != 0))
                            //        {
                            //            //of.CoefDict.GlobalCoordFromLocal(w.coord.Items[ind].X, w.coord.Items[ind].Y, out xG, out yG);
                            //            iconList.SetEmptyIcon(xG, yG);
                            //            w.X = xG;
                            //            w.Y = yG;
                            //            findCoord = true;
                            //            break;
                            //        }
                            //    }
                            //}
                            //w.Visible = findCoord;
                        }
                    }
                    #endregion
                    w.SetBrushFromIcon();
                }
                if (of.BubbleMapLoaded) of.CreateSortedWellList();
                worker.ReportProgress(100);
            }
        }
    }
}
