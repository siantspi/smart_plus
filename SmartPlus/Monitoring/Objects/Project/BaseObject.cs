﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus
{
    public class BaseObj
    {
        public int Index { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }

        public Constant.BASE_OBJ_TYPES_ID TypeID { get; set; }
        public bool Selected { get; set; }
        public bool Visible { get; set; }

        public BaseObj()
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.NONE;
            Code = -1;
            Index = -1;
            Selected = false;
            Visible = true;
        }
        public BaseObj(Constant.BASE_OBJ_TYPES_ID Type) : this()
        {
            this.TypeID = Type;
        }
    }
}
