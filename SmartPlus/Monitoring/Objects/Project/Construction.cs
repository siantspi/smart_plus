﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Collections;
using SmartPlus;
using RDF.Objects;

namespace SmartPlus
{
    public sealed class Construction : C2DObject
    {
        public int Index;
        public float Width, Height;
        public ArrayList TubesInList, TubesOutList;
        public Pen pen;
        public Brush brush, fillBrush;
        public Font font;
        public int NGDUcode;
        public int Level;


        double RatingLiquid;
        public string RatingStr;
        public double QLiq
        {
            get
            {
                if (RatingLiquid == -1)
                {
                    double sumLiq = 0;
                    for (int i = 0; i < TubesInList.Count; i++)
                    {
                        if (((Tube)TubesInList[i]).QLiq > -1)
                            sumLiq += ((Tube)TubesInList[i]).QLiq;
                    }
                    if (sumLiq >= 10000)
                        RatingStr = "Qliq :" + (sumLiq / 1000).ToString("#,#0.##") + " тыс.т";
                    else
                        RatingStr = "Qliq :" + sumLiq.ToString("#,0.##") + " т";
                    RatingLiquid = sumLiq;
                }
                return RatingLiquid;
            }
        }
        public Construction()
        {
            Code = 0;
            TypeID = Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION;
            Visible = true;
            Selected = false;
            TubesInList = new ArrayList();
            TubesOutList = new ArrayList();
            RatingLiquid = -1;
            Width = 100;
            Height = 100;
        }

        public void GetGeom()
        {
            int i;
            Tube t;
            this.X = Constant.DOUBLE_NAN;
            this.Y = Constant.DOUBLE_NAN;

            double sumX = 0, sumY = 0;
            for (i = 0; i < TubesInList.Count; i++)
            {
                t = (Tube)TubesInList[i];

                if ((t.X == Constant.DOUBLE_NAN)&&(t.ObjIn.TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION))
                    ((Construction)t.ObjIn).GetGeom();

                if (t.X != Constant.DOUBLE_NAN)
                {
                    sumX += t.X;
                    sumY += t.Y;
                }
            }
            if (TubesInList.Count > 0)
            {
                this.X = sumX / TubesInList.Count;
                this.Y = sumY / TubesInList.Count;
            }
            for (i = 0; i < TubesInList.Count; i++)
            {
                ((Tube)TubesInList[i]).MatchBounds();
            }
            for (i = 0; i < TubesOutList.Count; i++)
            {
                ((Tube)TubesOutList[i]).X = this.X;
                ((Tube)TubesOutList[i]).Y = this.Y;
            }
        }

        public bool ReadFromCache(BinaryReader file)
        {
            if (file != null)
            {
                this.Code = file.ReadInt32();
                this.X = file.ReadDouble();
                this.Y = file.ReadDouble();
                this.NGDUcode = file.ReadInt32();
                this.Level = file.ReadInt32();
                this.RatingLiquid = file.ReadDouble();
                int tubesIn;
                tubesIn = file.ReadInt32();
                TubesInList = new ArrayList(tubesIn);
                for (int i = 0; i < tubesIn; i++)
                {
                    TubesInList.Add(file.ReadInt32());
                }
                return true;
            }
            return false;
        }
        public bool WriteToCache(BinaryWriter file)
        {
            if (file != null)
            {
                file.Write(this.Code);
                file.Write(this.X);
                file.Write(this.Y);
                file.Write(NGDUcode);
                file.Write(Level);
                file.Write(RatingLiquid);
                file.Write(TubesInList.Count);
                for (int i = 0; i < TubesInList.Count; i++)
                {
                    file.Write(((Tube)TubesInList[i]).Index); 
                }
                return true;
            }
            return false;
         }
        public override void DrawObject(Graphics grfx)
        {
            bool TubesVisible = true;
            bool TextVisible = true;
            if ((Visible) && (st_VisbleLevel <= st_OilfieldXUnits))
            {
                float screenX = ScreenXfromRealX(X);
                float screenY = ScreenYfromRealY(Y);
                float w = st_radiusX * (3 + this.Level * 2);
                float h = st_radiusY * (3 + this.Level * 2);

                if (TubesVisible)
                    for (int i = 0; i < TubesInList.Count; i++)
                        ((Tube)TubesInList[i]).DrawObject(grfx);

                grfx.FillRectangle(fillBrush, screenX - w / 2, screenY - h / 2, w, h);
                grfx.DrawRectangle(pen, screenX - w / 2, screenY - h / 2, w, h);
                switch (this.Level)
                {
                    case 0:
                        if (st_VisbleLevel > st_AreaXUnits) TextVisible = false;

                        break;
                    case 1:
                        if (st_VisbleLevel > st_AreaXUnits * 2) TextVisible = false;
                        break;
                    case 2:
                        if (st_VisbleLevel > st_OilfieldXUnits / 2) TextVisible = false;
                        break;
                }

                if ((font != null) &&(TextVisible))
                {
                    SizeF size = grfx.MeasureString(Name, font, Point.Empty, StringFormat.GenericTypographic);
                    DrawWhiteGroundString(grfx, Name, font, brush, screenX - size.Width / 2, screenY - size.Height / 2, StringFormat.GenericTypographic);
                    //grfx.DrawString(Name, font, brush, screenX - size.Width / 2, screenY - size.Height / 2, StringFormat.GenericTypographic);
                    if (RatingLiquid > -1)
                    {
                        SizeF sf = grfx.MeasureString(RatingStr, font, Point.Empty, StringFormat.GenericTypographic);
                        DrawWhiteGroundString(grfx, RatingStr, font, brush, screenX - sf.Width / 2, screenY + size.Height / 2, StringFormat.GenericTypographic);
                        //grfx.DrawString(RatingStr, font, brush, screenX - sf.Width / 2, screenY + size.Height / 2, StringFormat.GenericTypographic);
                    }
                }
            }
        }
    }

    public sealed  class Tube : C2DObject
    {
        double X2, Y2;
        double RatingLiquid;
        public int Index;
        public Pen pen;
        public Brush brush;
        public C2DObject ObjIn, ObjOut;
        public double QLiq
        {
            get 
            {
                if (RatingLiquid == -1)
                {
                    if (((C2DObject)ObjIn).TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION)
                    {
                        RatingLiquid = ((Construction)ObjIn).QLiq;
                    }
                    else if (((C2DObject)ObjIn).TypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                    {
                        Well w = (Well)ObjIn;
                        if (w.MerLoaded)
                        {
                            double sumLiq = 0, sumTime = 0;
                            DateTime dt;
                            dt = w.mer.Items[w.mer.Count - 1].Date;
                            for (int i = w.mer.Count - 1; i >= 0; i--)
                            {
                                if (dt == w.mer.Items[i].Date)
                                {
                                    sumLiq += w.mer.Items[i].Wat + w.mer.Items[i].Oil;

                                    if (w.mer.Items[i].WorkTime + w.mer.Items[i].CollTime > 0)
                                        sumTime = w.mer.Items[i].WorkTime + w.mer.Items[i].CollTime;
                                }
                                else break;
                            }
                            if (sumTime > 0)
                                RatingLiquid = sumLiq * 24 / sumTime;
                         }
                    }
                }
                return RatingLiquid;
            }
        }

        public int NGDUcode
        {
            get
            {
                if ((ObjIn != null)&&(ObjIn.TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION))
                {
                    return ((Construction)ObjIn).NGDUcode;
                }
                else if ((ObjOut != null) && (ObjOut.TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION))
                {
                    return ((Construction)ObjOut).NGDUcode;
                }
                return -1;
            }
        }

        public Tube()
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.TUBE;
            Visible = true;
            Selected = false;
            this.ObjIn = null;
            this.ObjOut = null;
            X = Constant.DOUBLE_NAN;
            Y = Constant.DOUBLE_NAN;
            X2 = Constant.DOUBLE_NAN;
            Y2 = Constant.DOUBLE_NAN;
            RatingLiquid = -1;
        }
        public Tube(C2DObject ObjIn, C2DObject ObjOut)
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.TUBE;
            Visible = true;
            Selected = false;
            this.ObjIn = ObjIn;
            this.ObjOut = ObjOut;
            X = ObjIn.X;
            Y = ObjIn.Y;
            X2 = Constant.DOUBLE_NAN;
            Y2 = Constant.DOUBLE_NAN;
            RatingLiquid = -1;

            if (ObjIn.TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION)
            {
                ((Construction)ObjIn).TubesOutList.Add(this);
            }
            if (ObjOut.TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION)
            {
                ((Construction)ObjOut).TubesInList.Add(this);
            }
        }

        public bool ReadFromCache(BinaryReader file, Project proj)
        {
            if (file != null)
            {
                this.X = file.ReadDouble();
                this.Y = file.ReadDouble();
                this.X2 = file.ReadDouble();
                this.Y2 = file.ReadDouble();
                this.RatingLiquid = file.ReadDouble();
                Constant.BASE_OBJ_TYPES_ID type = (Constant.BASE_OBJ_TYPES_ID)file.ReadInt32();

                if (type == Constant.BASE_OBJ_TYPES_ID.WELL)
                {
                    int ofIndex = file.ReadInt32();
                    int wellIndex = file.ReadInt32();
                    if ((ofIndex < proj.OilFields.Count) && 
                        (wellIndex < ((OilField)proj.OilFields[ofIndex]).Wells.Count))
                        ObjIn = (Well)((OilField)proj.OilFields[ofIndex]).Wells[wellIndex];
                }
                else if (type == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION)
                {
                    int ind = file.ReadInt32();
                    ObjIn = (Construction)proj.ConstructionList[ind];
                    ((Construction)ObjIn).TubesOutList.Add(this);
                }
                return true;
            }
            return false;
        }
        public bool WriteToCache(BinaryWriter file)
        {
            if (file != null)
            {
                file.Write(this.X);
                file.Write(this.Y);
                file.Write(this.X2);
                file.Write(this.Y2);
                file.Write(this.RatingLiquid);
                file.Write((int)ObjIn.TypeID);
                if(ObjIn.TypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                {
                    file.Write(((Well)ObjIn).OilFieldIndex);
                    file.Write(((Well)ObjIn).Index);
                }
                else if(ObjIn.TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION)
                {
                    file.Write(((Construction)ObjIn).Index);
                }
                return true;
            }
            return false;
        }


        private double IntersectBounds(double X, double Y, double Width, double Height, double X2, double Y2)
        {
            double X11, X12, Y11, Y12;
            double Y21, Y22;
            X11 = X - Width / 2;
            X12 = X + Width / 2;
            Y11 = Y - Height / 2;
            Y12 = Y11;
            Y21 = (X2 - X11) * (Y - Y11) / (X - X11) + Y11;
            Y22 = (X2 - X12) * (Y - Y12) / (X - X12) + Y12;

            if ((Y2 < Y21) && (Y2 < Y22))
            {

 
            }
            else if ((Y2 < Y21) && (Y2 < Y22))
            {
 
            }
            else if ((Y2 < Y21) && (Y2 < Y22))
            {

            }
            else if ((Y2 < Y21) && (Y2 < Y22))
            {

            }

            return -1;
        }
        public void MatchBounds()
        {
            X2 = ObjOut.X;
            Y2 = ObjOut.Y;
            X = ObjIn.X;
            Y = ObjIn.Y;

            //if (ObjIn.TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION)
            //{
            //    Construction cnstr = (Construction)ObjIn;
            //    double X0 = Constant.DOUBLE_NAN, Y0 = Constant.DOUBLE_NAN;

            //    if (X2 > X + cnstr.Width / 2) X0 = X + cnstr.Width / 2;
            //    else if (X2 < X - cnstr.Width / 2) X0 = X - cnstr.Width / 2;

            //    if (Y2 < Y - cnstr.Width / 2) Y0 = Y - cnstr.Width / 2;
            //    else if (Y2 > Y + cnstr.Width / 2) Y0 = Y + cnstr.Width / 2;

            //    if ((X2 < X - cnstr.Width / 2) || (X2 > X + cnstr.Width / 2))
            //    {
            //        Y0 = (X0 - X) * (Y2 - Y) / (X2 - X) + Y;
            //    }

            //    if ((Y2 < Y - cnstr.Height / 2) || (Y2 > Y + cnstr.Height / 2))
            //    {
            //        X0 = (Y0 - Y) * (X2 - X) / (Y2 - Y) + X;
            //    }
            //    X = X0;
            //    Y = Y0;
            //}

            //if (ObjOut.TypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION)
            //{
            //    Construction cnstr = (Construction)ObjOut;
            //    double X0 = Constant.DOUBLE_NAN, Y0 = Constant.DOUBLE_NAN;
            //    //double X11 = X2 -  


            //    if (X > X2 + cnstr.Width / 2) X0 = X2 + cnstr.Width / 2;
            //    else if (X < X2 - cnstr.Width / 2) X0 = X2 - cnstr.Width / 2;

            //    if (Y < Y2 - cnstr.Width / 2) Y0 = Y2 - cnstr.Width / 2;
            //    else if (Y > Y2 + cnstr.Width / 2) Y0 = Y2 + cnstr.Width / 2;

            //    if ((X < X2 - cnstr.Width / 2) || (X > X2 + cnstr.Width / 2))
            //    {
            //        Y0 = (X0 - X) * (Y2 - Y) / (X2 - X) + Y;
            //    }

            //    if ((Y < Y2 - cnstr.Height / 2) || (Y > Y2 + cnstr.Height / 2))
            //    {
            //        X0 = (Y0 - Y) * (X2 - X) / (Y2 - Y) + X;
            //    }
            //    X = X0;
            //    Y = Y0;
            //}
        }
        public override void DrawObject(Graphics grfx)
        {
            if ((Visible)&&(X2 != Constant.DOUBLE_NAN))
            {
                float scrX, scrY, scrX2, scrY2;
                scrX = ScreenXfromRealX(X);
                scrY = ScreenYfromRealY(Y);
                scrX2 = ScreenXfromRealX(X2);
                scrY2 = ScreenYfromRealY(Y2);
                grfx.DrawLine(pen, scrX, scrY, scrX2, scrY2);
            }
        }
    }
}