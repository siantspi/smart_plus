﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace SmartPlus
{
    public class WorkerState
    {
        public string WorkMainTitle;
        public string WorkCurrentTitle;
        public string Element;
        public string Error;
        public string Time;
        public byte Type;
    }
    public struct WorkObject
    {
        public Constant.BG_WORKER_WORK_TYPE WorkType;
        public object DataObject;
        public int ObjectIndex;
    }
    public struct Progress
    {
        int value;
        public int SumProgressType;
        public int Max;
        public void Clear()
        {
            this.SumProgressType = 0;
            this.Max = 100;
            this.value = 0;
        }
        /// <summary>
        /// Установка типа накопления прогресса
        /// </summary>
        /// <param name="SumProgressType">Тип [0 - по значению, 1 - по достижению 100, 2 - накопление вызовов]</param>
        /// <param name="MaxProgress">Максимальное значение прогресса</param>
        public void Set(int SumProgressType, int MaxProgress)
        {
            this.SumProgressType = SumProgressType;
            this.Max = MaxProgress;
            this.Value = 0;
         }
        public int Value
        {
            set
            {
                if (this.SumProgressType == 0) 
                {
                    this.value = value;
                }
                else if (this.SumProgressType == 1)
                {
                    if (value == 100) this.value++;
                }
                else
                {
                    this.value++;
                }
            }
            get { return value; }
        }
        public int Percent
        {
            get
            {
                int perc = 0;
                if(Max > 0) perc = (int)((float)Value * 100 / (float)Max);
                if (perc < 0) perc = 0;
                if (perc > 100) perc = 100;
                return perc; 
            }
        }
    }
    public sealed class BGWorker : BackgroundWorker
    {
        public ArrayList Queue;
        public bool QueueBusy;
        public MainForm mainForm;
        public bool EndWork;
        bool newLaunch;
        Progress progress;
        WorkObject WorkObj, WorkObjTemp;
        int Counter;

        CanvasMap canv;
        ProgressForm fProgress;
        ToolStripProgressBar pb;
        ToolStripStatusLabel stBarLabel;

        public BGWorker(MainForm MainForm)
        {
            Queue = new ArrayList();
            QueueBusy = false;
            this.EndWork = true;
            newLaunch = false;
            this.mainForm = MainForm;
            this.canv = mainForm.canv;
            this.fProgress = mainForm.fProgress;
            this.pb = mainForm.pb;
            this.stBarLabel = mainForm.stBar1;
            this.Counter = 0;

            this.WorkObjTemp.DataObject = null;
            this.WorkObjTemp.ObjectIndex = -1;
            this.WorkObj.DataObject = null;
            this.WorkObj.ObjectIndex = -1;
            this.progress.Clear();

            this.WorkerReportsProgress = true;
            this.WorkerSupportsCancellation = true;

            this.DoWork += new DoWorkEventHandler(BGWorker_DoWork);
            this.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BGWorker_RunWorkerCompleted);
            this.ProgressChanged += new ProgressChangedEventHandler(BGWorker_ProgressChanged);
        }

        public int GetWorkType()
        {
            return (int)WorkObj.WorkType;
        }

        public void Run(Constant.BG_WORKER_WORK_TYPE workType)
        {
            this.Run(workType, null, -1);
 
        }
        public void Run(Constant.BG_WORKER_WORK_TYPE workType, object workDataObject)
        {
            this.Run(workType, workDataObject, -1);
        }
        public void Run(Constant.BG_WORKER_WORK_TYPE workType, object workDataObject, int WorkObjectIndex)
        {
            if (this.IsBusy)
            {
                WorkObject newJob;
                newJob.WorkType = workType;
                newJob.DataObject = workDataObject;
                newJob.ObjectIndex = WorkObjectIndex;
                this.Queue.Add(newJob);
            }
            else
            {
                this.WorkObj.WorkType = workType;
                this.WorkObj.DataObject = workDataObject;
                this.WorkObj.ObjectIndex = WorkObjectIndex;
                this.BeforeWorkerRun();
                if (!this.IsBusy) this.RunWorkerAsync();
                else 
                {
                    WorkObject newJob;
                    newJob.WorkType = workType;
                    newJob.DataObject = workDataObject;
                    newJob.ObjectIndex = WorkObjectIndex;
                    this.Queue.Add(newJob);
                    this.CancelAsync();
                }

            }
        }
        public void Run()
        {
            if(Queue.Count > 0)
            {
                WorkObject job =  (WorkObject)Queue[0];
                this.Run(job.WorkType, job.DataObject, job.ObjectIndex);
                Queue.RemoveAt(0);
            }
        }

        void BGWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;
            BackgroundWorker worker = sender as BackgroundWorker;
            this.EndWork = true;
            progress.Clear();
            this.WorkObjTemp.DataObject = null;
            this.WorkObjTemp.ObjectIndex = -1;
            int count;
            switch (this.WorkObj.WorkType)
            {
                case Constant.BG_WORKER_WORK_TYPE.REPORT_MER_PRODUCTION:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(1, proj.OilFields.Count * 2);
                        proj.ReportMerProduction(worker, e);
                    }
                    break;
#region BASHNEFT

                case Constant.BG_WORKER_WORK_TYPE.CONVERT_WORK_FOLDER:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project project = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        canv.ConvertWorkFolder(worker, e, project.tempStr);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_WELL_CORE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project project = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        SmartPlus.Report.MonitoringReporter.ReportCore(worker, e, project);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_BASE_PRODUCTION:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project project = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        SmartPlus.Report.MonitoringReporter.ReportBaseProduction(worker, e, project);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.DEPOSIT_OIZ_REPORT:
                    if (this.WorkObj.DataObject != null)
                    {
                        object[] param = (object[])this.WorkObj.DataObject;
                        var project = (Project)param[0];
                        progress.Set(0, 100);
                        List<int> ngduList = (List<int>)param[1];
                        //SmartPlus.Report.DepositsReporter.ReportGisIntervals(worker, e, project);
                        SmartPlus.Report.DepositsReporter.ReportDepositOIZ(worker, e, project, ngduList);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_DEPOSITS_FROM_FOLDER:
                    if (this.WorkObj.DataObject != null)
                    {
                        var proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadDepositsFromFolder(worker, e, mainForm, proj.tempStr);
                        proj.tempStr = string.Empty;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.FILL_WELLS_OIL_GAIN:
                    canv.FillWellsOilGain(worker, e);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_VORONOI_RESERVES:
                    if (this.WorkObj.DataObject != null)
                    {
                        var proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        SmartPlus.Report.MonitoringReporter.ReportVoronoiReserves(worker, e, proj);
                        proj.tempStr = string.Empty;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_SUM_ONNT_MAP:
                    if (this.WorkObj.DataObject != null)
                    {
                        DirectoryInfo dir = new DirectoryInfo(Application.StartupPath);
                        FileInfo[] files = dir.GetFiles("*.init");
                        //if (files.Length == 1)
                        //{
                            var proj = (Project)this.WorkObj.DataObject;
                            progress.Set(0, 100);
                            List<OilField> oilfields = new List<OilField>();
                            for (int i = 1; i < proj.OilFields.Count; i++)
                            {
                                //if (proj.OilFields[i].Name.StartsWith("Им.", StringComparison.OrdinalIgnoreCase))
                                if (proj.OilFields[i].ParamsDict.NGDUName.StartsWith(files[0].Name.Replace(".init", ""), StringComparison.OrdinalIgnoreCase))
                                {
                                    if (!proj.OilFields[i].Name.StartsWith("КИТАЯМ") && !proj.OilFields[i].Name.StartsWith("БЛАГОДАР") && !proj.OilFields[i].Name.StartsWith("ДАЧНО"))
                                    //if (proj.OilFields[i].Name.StartsWith("КИТАЯМ") || proj.OilFields[i].Name.StartsWith("БЛАГОДАР") || proj.OilFields[i].Name.StartsWith("ДАЧНО"))
                                    {
                                        if (!proj.OilFields[i].Name.StartsWith("ИШКАР")) oilfields.Add(proj.OilFields[i]);
                                    }
                               }
                            }
                            e.Result = canv.CreateSumOnntMap(worker, e, oilfields);
                        //}
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SERVICE_FUNC:
                    // Нарушения лицензий
                    //if (this.WorkObj.DataObject != null)
                    //{
                    //    Project proj = (Project)this.WorkObj.DataObject;
                    //    progress.Set(1, 2 * proj.OilFields.Count - 2);
                    //    proj.ReportLicence(worker, e);
                    //}             

                    //if (this.WorkObj.DataObject != null)
                    //{
                    //    Project proj = (Project)this.WorkObj.DataObject;
                    //    progress.Set(1, proj.OilFields.Count - 1);
                    //    proj.ReportProductionByOilField(worker, e);
                    //}

                    //if (this.WorkObj.DataObject != null)
                    //{
                    //    Project proj = (Project)this.WorkObj.DataObject;
                    //    progress.Set(1, 2 * (proj.OilFields.Count - 1));
                    //    //proj.SaveOFContoursList(worker, e);
                    //    proj.SaveAreasList(worker, e);
                    //}

                    //WorkerState userState = new WorkerState();
                    //userState.WorkMainTitle = "";
                    //userState.WorkCurrentTitle = "Сравнение ГИС и Каротажек";

                    //if (this.WorkObj.DataObject != null)
                    //{
                    //    Project proj = (Project)this.WorkObj.DataObject;
                    //    int sumLen = 0;
                    //    for (int i = 1; i < proj.OilFields.Count; i++) sumLen += (proj.OilFields[i]).WellList.Length;

                    //    progress.Set(1, sumLen + proj.OilFields.Count - 1);

                    //    OilField of;
                    //    Well w;
                    //    ArrayList list = new ArrayList();
                    //    worker.ReportProgress(0, userState);
                    //    string fileName = "d:\\table_gis_logs.csv";
                    //    //if (File.Exists(fileName)) File.Delete(fileName);

                    //    for (int i = 1; i < proj.OilFields.Count; i++)
                    //    {
                    //        //if (i == 2) break;
                    //        of = proj.OilFields[i];
                    //        of.CompareGisLogs(worker, e, fileName);
                    //    }
                    //}

                    //WorkerState userState = new WorkerState();
                    //userState.WorkMainTitle = "";
                    //userState.WorkCurrentTitle = "Сброс значков скважин";

                    //if (this.WorkObj.DataObject != null)
                    //{
                    //    Project proj = (Project)this.WorkObj.DataObject;
                    //    int sumLen = 0;
                    //    for (int i = 1; i < proj.OilFields.Count; i++) sumLen += (proj.OilFields[i]).WellList.Length;

                    //    progress.Set(1, sumLen + proj.OilFields.Count - 1);

                    //    OilField of;
                    //    worker.ReportProgress(0, userState);
                    //    for (int i = 1; i < proj.OilFields.Count; i++)
                    //    {
                    //        (proj.OilFields[i]).ReSetWellIcons(worker, e, true);
                    //        (proj.OilFields[i]).WriteWellListToCache(worker, e);
                    //    }
                    //}
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_PROD_WITHOUT_GTM:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        SmartPlus.Report.MonitoringReporter.ReportProductionWithoutGtm(worker, e, proj);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PVT_AREAS_FROM_FILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadPVTAreasFromFile(worker, e, proj.tempStr);
                        //proj.LoadPVTFromFile(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_AREAS_PREDICT:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = proj.OilFields.Count * 4;
                        progress.Set(1, sumLen);
                        SmartPlus.Report.AreasGTMReporter.ReportAreasPredictData(worker, e, proj);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_AREAS_GTM:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 3;
                        }

                        progress.Set(2, sumLen);

                        SmartPlus.Report.AreasGTMReporter.ReportAreasSumGTM(worker, e, proj);

                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_FLOODING_ANALYSIS:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 6;
                        }
                        progress.Set(2, sumLen);
                        Report.MonitoringReporter.ReportFloodingAnalysis(worker, e, proj);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_CHESS:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 2;
                        }
                        progress.Set(2, sumLen);
                        SmartPlus.Report.MonitoringReporter.ReportChess(worker, e, proj);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_MER:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 1;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportMer(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_PLAST_ENERGY:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 2;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportEnergy(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_ZBS:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 2;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportEffectZBS(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_IDN:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 3;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportWellIDN(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_STOP_WELLS:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += ((OilField)proj.OilFields[i]).Wells.Count * 3;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportStopWells(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_INJECTION_WELLS:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 3;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportInjectionFund(worker, e);
                        //proj.ReportInjectionHistory(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_PPD:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 2;
                        }
                        progress.Set(2, sumLen);
                        SmartPlus.Report.MonitoringReporter.ReportWellDates(worker, e, proj);

                        // proj.ReportPPD(worker, e);
                        //proj.ReportPerforationAnalisys(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_STAGE_DEVELOPMENT:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 2;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportStageDevelopment(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_OIL_OBJ_LIST:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 2;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportOilObjList(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_LICENCE_PERF:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 3;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportLicence(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_WELL_ACTION:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count;
                        }

                        progress.Set(2, sumLen * 2);
                        //proj.ReportWellRepair(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_WELLS_GTM:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count;
                        }

                        progress.Set(2, sumLen * 2);
                        //proj.ReportWellRepairGtm(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_PRIORITY_GTM:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportPriorityGTM(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_TARGET_OPENING_GIS:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 1; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Wells.Count * 5;
                        }

                        progress.Set(2, sumLen);
                        //proj.ReportPriorityOilObject(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.EXPORT_WELL_WITHOUT_AREACODE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, proj.OilFields.Count);

                        OilField of;
                        Well w;

                        StreamWriter file = new StreamWriter(proj.tempStr, false, Encoding.GetEncoding(1251));
                        for (int i = 0; i < proj.OilFields.Count; i++)
                        {
                            of = proj.OilFields[i];
                            of.LoadMerFromCache(worker, e);
                            for (int j = 0; j < of.Wells.Count; j++)
                            {
                                w = of.Wells[j];
                                if ((w.OilFieldAreaCode == -1) && (w.CoordLoaded) && (w.MerLoaded))
                                {
                                    file.WriteLine(string.Format("{0};{1};", of.Name, w.UpperCaseName));
                                }
                            }
                            of.ClearMerData(false);
                            worker.ReportProgress(i);
                        }
                        file.Close();

                    }
                    break;
#endregion BASHNEFT
                case Constant.BG_WORKER_WORK_TYPE.GTM_AUTOMATCH_PERMEABILITY:
                    if (this.WorkObj.DataObject != null)
                    {
                        object[] args = (object[])this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        Report.MonitoringReporter.ReportGtmAutoMatchPermeability(worker, e, canv.MapProject, (string)args[0], (DateTime)args[1], (DateTime)args[2], (int)args[5], (List<Dialogs.GTMAutoMatchItem>)args[3], (C2DLayer)args[4], (bool)args[6], !(bool)args[7], (double)args[8], (double)args[9]);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_GTM_LAYERLIST:
                    if (this.WorkObj.DataObject != null)
                    {
                        object[] args = (object[])this.WorkObj.DataObject;

                        progress.Set(1, canv.MapProject.OilFields.Count);
                        canv.CreateGTMLayerList(worker, e, (DateTime)args[0], (DateTime)args[1], (List<int[]>)args[2]);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_VORONOI_MAP:
                    if (this.WorkObj.DataObject != null)
                    {
                        object[] mapParams = (object[])this.WorkObj.DataObject;
                        canv.CreateVoronoiMap(worker, e, (DateTime)mapParams[0], (List<Well>)mapParams[1], (List<int>)mapParams[2], (int)mapParams[3], (bool)mapParams[4], (mapParams[5] == null) ? null : (Grid)mapParams[5]);
                    }
                    break;

                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_FILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        canv.LoadGridFromFile(worker, e, proj.tempStr);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_GRIDS_FROM_DIRECTORY:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(1, 100);
                        canv.LoadGridsFromDirectoryToProject(worker, e);
                    }
                    break;
#if DEBUG
                case Constant.BG_WORKER_WORK_TYPE.CREATE_OF_UNION_CONTOUR:

                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int sumLen = 0;
                        for (int i = 45; i < proj.OilFields.Count; i++)
                        {
                            sumLen += (proj.OilFields[i]).Contours.Count;
                            break;
                        }

                        progress.Set(2, sumLen);
                        //progress.Set(0, (proj.OilFields[1]).ContourList.Length);

                        OilField of;
                        sumLen = 0;
                        for (int i = 45; i < proj.OilFields.Count; i++)
                        {
                            of = proj.OilFields[i];
                            of.CreateUnionContours(worker, e);
                            if (of.Contours.Count > 0) of.WriteContoursToCache(worker, e);
                            break;
                        }
                    }
                    break;
#endif
                case Constant.BG_WORKER_WORK_TYPE.CREATE_PROFILE_BY_NEWCONTOUR:
                    if (this.WorkObj.DataObject != null)
                    {
                        CanvasMap canvas = (CanvasMap)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        canvas.CreateProfileByNewContour(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_PROFILE_BY_NEAR_WELL:
                    if (this.WorkObj.DataObject != null)
                    {
                        CanvasMap canvas = (CanvasMap)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        canvas.CreateProfileByNearWell(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PLANE_BY_WELL:
                    if (this.WorkObj.DataObject != null)
                    {
                        CorrelationScheme plane = (CorrelationScheme)this.WorkObj.DataObject;
                        plane.CreatePlaneListByObjSrc();
                        progress.Set(0, 100);
                        plane.LoadProfileData(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CORSCHEME_BY_PROFILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        CorrelationScheme corScheme = (CorrelationScheme)this.WorkObj.DataObject;
                        corScheme.CreatePlaneListByObjSrc();
                        if (corScheme.ObjSrc.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                        {
                            progress.Set(1, ((Profile)((C2DLayer)corScheme.ObjSrc).ObjectsList[0]).Count);
                        }
                        else
                            progress.Set(0, 100);
                        corScheme.LoadProfileData(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.ADD_WELL_TO_PROFILE:
                    if ((this.WorkObj.DataObject != null) && (canv.selWellList.Count > 0) && 
                        (this.WorkObj.ObjectIndex > -1) && (this.WorkObj.ObjectIndex < canv.selWellList.Count))
                    {
                        CorrelationScheme corScheme = (CorrelationScheme)this.WorkObj.DataObject;
                        progress.Set(0, 50);
                        Well w = (Well)canv.selWellList[this.WorkObj.ObjectIndex];
                        corScheme.AddWell(worker, e, w);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_WELLLIST_BY_FILTER:
                    if (this.WorkObj.DataObject != null)
                    {
                        WellDataFilterForm filterForm = (WellDataFilterForm)this.WorkObj.DataObject;
                        progress.Set(1, (mainForm.canv.MapProject.OilFields.Count - 1) * 4);
                        filterForm.FillWellList(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        OilField of = (OilField)this.WorkObj.DataObject;
                        progress.Set(0, 100);

                        int res = of.LoadGridDataFromCacheByIndex(worker, e, this.WorkObj.ObjectIndex, true);
                        this.WorkObjTemp.DataObject = (Grid)of.Grids[this.WorkObj.ObjectIndex];
                        this.WorkObjTemp.ObjectIndex = res;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        OilField of = (OilField)((C2DLayer)this.WorkObj.DataObject).oilfield;
                        progress.Set(0, 100);
                        of.LoadContoursDataFromCache(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.FREE_OTHER_OF_GRIDS:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, proj.OilFields.Count - 1);
                        mainForm.SetStatus("Выгрузка сеток из памяти");
                        proj.FreeGridData(worker, e, this.WorkObj.ObjectIndex);
                        this.EndWork = false;
                    }
                    break;

                case Constant.BG_WORKER_WORK_TYPE.EXPORT_AREAS_CONTOURS:
                    if (this.WorkObj.DataObject != null)
                    {
                        CanvasMap canvas = (CanvasMap)this.WorkObj.DataObject;
                        progress.Set(1, canvas.selLayerList.Count);
#if !DEBUG
                        canvas.ExportAreasContours(worker, e, canv.MapProject.tempStr);
#else
                        canvas.ExportCondContours(worker, e, canv.MapProject.tempStr);
#endif
                        canvas.MapProject.tempStr = "";
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SAVE_APP_SETTINGS:
                    if (this.WorkObj.DataObject != null)
                    {
                        AppSetting appSett = (AppSetting)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        appSett.Write();
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.RESET_ICONS_BY_FILTER:
                    if (this.WorkObj.DataObject != null)
                    {
                        CanvasMap canvas = (CanvasMap)this.WorkObj.DataObject;
                        progress.Set(1, canvas.MapProject.OilFields.Count - 1);
                        canvas.MapProject.ResetWellIconAndBubbleByFilter(worker, e, canvas.mainForm.filterOilObj, mainForm.AppSettings.HideWellsWithoutCoordByFilter);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PVT_FROM_FILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadPVTFromFile(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_AREAS_CODES:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadAreasIndexFromFile2(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CONSTRUCTION:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadConstruction(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OILFIELD_BIZPLAN:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadOilfieldsBizplanFromFile(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_WELL_RESEARCH_FROM_FILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadResearchFromFile(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PROJWELL_FROM_FILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadProjWellListFromFile(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CORE_FROM_FILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadCoreFromFile(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CORE_TEST_FROM_FILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        proj.LoadCoreTestFromFile(worker, e, proj.tempStr);
                        proj.tempStr = null;
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CHESS_FROM_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        ((OilField)this.WorkObj.DataObject).LoadChessFromCache(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_MER_FROM_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        ((OilField)this.WorkObj.DataObject).LoadMerFromCache(worker, e);
                        ((OilField)this.WorkObj.DataObject).ClearMerData(false);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SAVE_OF_CONTOUR_TO_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        ((OilField)this.WorkObj.DataObject).WriteContoursToCache(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SAVE_OF_GRIDS_TO_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        ((OilField)this.WorkObj.DataObject).WriteGridListToCache(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SAVE_OF_AREAS_TO_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        ((OilField)this.WorkObj.DataObject).WriteAreasToCache(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PROJECT:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(1, (proj.OilFields.Count - 1) * 3);
                        proj.LoadProject(worker, e);
                        canv.LoadWorkLayers(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_GLOBAL_PROJECT:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        string projPath = mainForm.UserProjectsFolder + "\\" + proj.Name;
                        if (!Directory.Exists(projPath))
                        {
                            int len = 0;
                            string[] dirlist;
                            Directory.CreateDirectory(projPath);
                            if (!Directory.Exists(projPath + "\\cache")) Directory.CreateDirectory(projPath + "\\cache");
                            if (Directory.Exists(mainForm.ServerProjectsFolder + "\\" + proj.Name + "\\cache"))
                            {
                                dirlist = Directory.GetDirectories(mainForm.ServerProjectsFolder + "\\" + proj.Name + "\\cache");
                                len = dirlist.Length;
                            }
                            progress.Set(1, len);
                            proj.CopyProject(worker, e, mainForm.ServerProjectsFolder + "\\" + proj.Name, projPath);
                        }
                        
                        proj.LoadProject(projPath);
                        progress.Set(1, (proj.OilFields.Count - 1) * 4);
                        proj.LoadProject(worker, e);
                        canv.LoadWorkLayers(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.RELOAD_PROJECT:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        string projPath = mainForm.UserProjectsFolder + "\\" + proj.Name;
                        proj.LoadProject(projPath);
                        progress.Set(1, (proj.OilFields.Count - 1) * 3);
                        proj.LoadProject(worker, e);
                        canv.LoadWorkLayers(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CALC_AREAS_COMPENSATION:
                    if (this.WorkObj.DataObject != null)
                    {
                        progress.Set(1, ((Project)this.WorkObj.DataObject).OilFields.Count - 1);
                        ((Project)this.WorkObj.DataObject).CalcAreasCompensation(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYERS:
                    count = 0;
                    for (int i = 0; i < canv.LayerWorkList.Count; i++)
                    {
                        count += ((C2DLayer)canv.LayerWorkList[i]).GetObjectsCount();
                    }
                    progress.Set(2, count);
                    canv.WriteWorkLayers(worker, e);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER:
                    canv.WriteWorkLayer(canv.WorkLayerActive);
                    break;

                #region LOAD WELL TRAJECTORY
                case Constant.BG_WORKER_WORK_TYPE.LOAD_WELLS_TRAJECTORY:
                    if (this.WorkObj.DataObject != null)
                    {
                        progress.Clear();
                        CanvasMap canvas = (CanvasMap)this.WorkObj.DataObject;
                        Contour cntr;
                        OilField of;
                        ArrayList list = new ArrayList();
                        int i, j, len;
                        bool find = false;

                        if (canvas.selLayerList.Count > 0)
                        {
                            for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                            {
                                of = canvas.MapProject.OilFields[j];
                                of.ClearTrajectoryData(false);
                            }
                            GC.GetTotalMemory(true);

                            for (i = 0; i < canvas.selLayerList.Count; i++)
                            {
                                if (((C2DLayer)canvas.selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                                    ((C2DLayer)canvas.selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                                {
                                    cntr = (Contour)((C2DLayer)canvas.selLayerList[i]).ObjectsList[0];

                                    if (cntr._OilFieldCode > -1)
                                    {
                                        len = 0;
                                        for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                        {
                                            of = canvas.MapProject.OilFields[j];
                                            if (of.OilFieldCode == cntr._OilFieldCode)
                                            {
                                                len += of.Wells.Count + 1;
                                                break;
                                            }
                                        }
                                        progress.Set(2, len);
                                        for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                        {
                                            of = canvas.MapProject.OilFields[j];
                                            if (of.OilFieldCode == cntr._OilFieldCode)
                                            {
                                                of.LoadTrajectoryFromCache(worker, e);
                                                find = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if ((canvas.twActiveOilField.ActiveOilFieldIndex > -1) && !find)
                        {
                            for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                            {
                                of = canvas.MapProject.OilFields[j];
                                of.ClearTrajectoryData(false);
                            }
                            GC.GetTotalMemory(true);
                            of = canvas.MapProject.OilFields[canvas.twActiveOilField.ActiveOilFieldIndex];
                            len = of.Wells.Count + 2;
                            progress.Set(2, len);
                            of.LoadTrajectoryFromCache(worker, e);
                        }
                    }
                    break;
                #endregion

                #region CALC_BUBBLE_MAP
                case Constant.BG_WORKER_WORK_TYPE.CALC_BUBBLE_MAP:
                    if ((this.WorkObj.DataObject != null) && (canv.BubbleMapType != Constant.BUBBLE_MAP_TYPE.NOT_SET))
                    {
                        progress.Clear();
                        bool LiquidInCube = false;
                        if (mainForm.AppSettings != null) LiquidInCube = mainForm.AppSettings.ShowDataLiquidInCube;
                        CanvasMap canvas = (CanvasMap)this.WorkObj.DataObject;
                        FilterOilObjects filter = canv.mainForm.filterOilObj;
                        Contour cntr;
                        OilField of;
                        ArrayList list = new ArrayList();
                        int i, j, len;
                        bool find = false;

                        if (canvas.selLayerList.Count > 0)
                        {
                            for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                            {
                                of = canvas.MapProject.OilFields[j];
                                of.ClearBubbleMap(worker, e);
                                of.SetDefaultColorWellIcons(!canvas.GrayWellIconsMode);
                            }

                            for (i = 0; i < canvas.selLayerList.Count; i++)
                            {
                                if (((C2DLayer)canvas.selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                                    ((C2DLayer)canvas.selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                                {
                                    cntr = (Contour)((C2DLayer)canvas.selLayerList[i]).ObjectsList[0];
                                    #region with region ngdu
                                    //if (cntr._EnterpriseCode > -1)
                                    //{
                                    //    len = 0;
                                    //    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                    //    {
                                    //        of = canvas.MapProject.OilFields[j];
                                    //        if (of.OFParams.EnterpriseCode == cntr._EnterpriseCode)
                                    //            len += of.WellList.Length + 2;
                                    //    }
                                    //    progress.Set(2, len);
                                    //    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                    //    {
                                    //        of = canvas.MapProject.OilFields[j];
                                    //        if (of.OFParams.EnterpriseCode == cntr._EnterpriseCode)
                                    //        {
                                    //            if (!of.MerLoaded) of.LoadMerVDataFromCache(worker, e);
                                    //            switch (canv.BubbleMapType)
                                    //            {
                                    //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                    //                    of.CalcBubbleMap(worker, e, canvas.BubbleDT2, canvas.BubbleLastMonth, canvas.BubbleK, filter);
                                    //                    break;
                                    //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                    //                    of.CalcBubbleAccumMap(worker, e, canvas.BubbleDT1, canvas.BubbleDT2, canvas.BubbleK, filter);
                                    //                    break;
                                    //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                    //                    of.CalcBubbleHistoryMap(worker, e, canv.BubbleDT1, canv.BubbleDT2, canvas.BubbleStartMonth, canvas.BubbleLastMonth, canv.BubbleHistoryViewBySector, canvas.BubbleK, filter);
                                    //                    break;
                                    //            }

                                    //            of.ClearMerData(false);
                                    //            find = true;
                                    //        }
                                    //    }
                                    //}
                                    //else if (cntr._NGDUCode > -1)
                                    //{
                                    //    len = 0;
                                    //    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                    //    {
                                    //        of = canvas.MapProject.OilFields[j];
                                    //        if (of.OFParams.NGDUCode == cntr._NGDUCode)
                                    //            len += of.WellList.Length + 2;
                                    //    }
                                    //    progress.Set(2, len);
                                    //    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                    //    {
                                    //        of = canvas.MapProject.OilFields[j];
                                    //        if (of.OFParams.NGDUCode == cntr._NGDUCode)
                                    //        {
                                    //            if (!of.MerLoaded) of.LoadMerVDataFromCache(worker, e);
                                    //            switch (canv.BubbleMapType)
                                    //            {
                                    //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                    //                    of.CalcBubbleMap(worker, e, canvas.BubbleDT2, canvas.BubbleLastMonth, canvas.BubbleK, filter);
                                    //                    break;
                                    //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                    //                    of.CalcBubbleAccumMap(worker, e, canvas.BubbleDT1, canvas.BubbleDT2, canvas.BubbleK, filter);
                                    //                    break;
                                    //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                    //                    of.CalcBubbleHistoryMap(worker, e, canv.BubbleDT1, canv.BubbleDT2, canvas.BubbleStartMonth, canvas.BubbleLastMonth, canv.BubbleHistoryViewBySector, canvas.BubbleK, filter);
                                    //                    break;
                                    //            }
                                    //            of.ClearMerData(false);
                                    //            find = true;
                                    //        }
                                    //    }
                                    //}
                                    //else
                                    #endregion
                                    if (cntr._OilFieldCode > -1)
                                    {
                                        len = 0;
                                        for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                        {
                                            of = canvas.MapProject.OilFields[j];
                                            if (of.OilFieldCode == cntr._OilFieldCode)
                                            {
                                                len += of.Wells.Count + 2;
                                                if ((canv.BubbleMapType == Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY) && (canv.BubbleHistoryByChess))
                                                {
                                                    len += of.Wells.Count + 2;
                                                }
                                                break;
                                            }
                                        }
                                        progress.Set(2, len);
                                        for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                        {
                                            of = canvas.MapProject.OilFields[j];
                                            if (of.OilFieldCode == cntr._OilFieldCode)
                                            {
                                                switch (canv.BubbleMapType)
                                                {
                                                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                                        if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                                                        of.CalcBubbleMap(worker, e, LiquidInCube, canvas.BubbleDT2, canvas.BubbleLastMonth, canvas.BubbleK, filter);
                                                        of.SetDefaultColorWellIcons(!canvas.GrayWellIconsMode);
                                                        break;
                                                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                                        if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                                                        of.CalcBubbleAccumMap(worker, e, LiquidInCube, canvas.BubbleDT1, canvas.BubbleDT2, canvas.BubbleK, filter);
                                                        of.SetDefaultColorWellIcons(true);
                                                        break;
                                                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                                        if (!canv.BubbleHistoryByChess)
                                                        {
                                                            if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                                                        }
                                                        else
                                                        {
                                                            if (!of.ChessLoaded) of.LoadChessFromCache(worker, e);
                                                        }
                                                        of.CalcBubbleHistoryMap(worker, e, LiquidInCube, canv.BubbleDT1, canv.BubbleDT2, canvas.BubbleStartMonth, canvas.BubbleLastMonth, canv.BubbleHistoryViewBySector, canvas.BubbleK, canvas.BubbleHistoryByChess, filter);
                                                        of.SetDefaultColorWellIcons(!canvas.GrayWellIconsMode);
                                                        break;
                                                }
                                                of.ClearMerData(false);
                                                of.ClearChessData(false);
                                                find = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if ((canvas.twActiveOilField.ActiveOilFieldIndex > -1) && (!find))
                        {
                            for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                            {
                                of = canvas.MapProject.OilFields[j];
                                of.ClearBubbleMap(worker, e);
                                of.SetDefaultColorWellIcons(!canvas.GrayWellIconsMode);
                            }
                            of = canvas.MapProject.OilFields[canvas.twActiveOilField.ActiveOilFieldIndex];
                            len = of.Wells.Count + 2;
                            if ((canv.BubbleMapType == Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY) && (canv.BubbleHistoryByChess))
                            {
                                len += of.Wells.Count + 2;
                            }
                            progress.Set(2, len);
                           
                            switch (canv.BubbleMapType)
                            {
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                    if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                                    of.CalcBubbleMap(worker, e, LiquidInCube, canvas.BubbleDT2, canvas.BubbleLastMonth, canvas.BubbleK, filter);
                                    of.SetDefaultColorWellIcons(!canvas.GrayWellIconsMode);
                                    break;
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                    if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                                    of.CalcBubbleAccumMap(worker, e, LiquidInCube, canvas.BubbleDT1, canvas.BubbleDT2, canvas.BubbleK, filter);
                                    of.SetDefaultColorWellIcons(true);
                                    break;
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                    if (!canv.BubbleHistoryByChess)
                                    {
                                        if (!of.MerLoaded) of.LoadMerFromCache(worker, e);
                                    }
                                    else
                                    {
                                        if (!of.ChessLoaded) of.LoadChessFromCache(worker, e);
                                    }
                                    of.CalcBubbleHistoryMap(worker, e, LiquidInCube, canv.BubbleDT1, canv.BubbleDT2, canvas.BubbleStartMonth, canvas.BubbleLastMonth, canv.BubbleHistoryViewBySector, canvas.BubbleK, canvas.BubbleHistoryByChess, filter);
                                    of.SetDefaultColorWellIcons(!canvas.GrayWellIconsMode);
                                    break;
                            }
                            of.ClearMerData(false);
                            of.ClearChessData(false);
                        }
                    }
                    break;
                #endregion

                #region UPDATE_BUBBLE_MAP
                case Constant.BG_WORKER_WORK_TYPE.UPDATE_BUBBLE_MAP:
                    if ((this.WorkObj.DataObject != null) && (canv.BubbleMapType != Constant.BUBBLE_MAP_TYPE.NOT_SET))
                    {
                        progress.Clear();
                        CanvasMap canvas = (CanvasMap)this.WorkObj.DataObject;
                        FilterOilObjects filter = canv.mainForm.filterOilObj;
                        Contour cntr;
                        OilField of;
                        ArrayList list = new ArrayList();
                        int i, j, len;
                        bool find = false;
                        bool LiquidInCube = false;
                        if (mainForm.AppSettings != null) LiquidInCube = mainForm.AppSettings.ShowDataLiquidInCube;

                        if (canvas.selLayerList.Count > 0)
                        {
                            for (i = 0; i < canvas.selLayerList.Count; i++)
                            {
                                cntr = (Contour)((C2DLayer)canvas.selLayerList[i]).ObjectsList[0];

                                #region With region ngdu
                                //if (cntr._EnterpriseCode > -1)
                                //{
                                //    len = 0;
                                //    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                //    {
                                //        of = canvas.MapProject.OilFields[j];
                                //        if (of.OFParams.EnterpriseCode == cntr._EnterpriseCode)
                                //            len += of.WellList.Length + 2;
                                //    }
                                //    progress.Set(2, len);
                                //    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                //    {
                                //        of = canvas.MapProject.OilFields[j];
                                //        if (of.OFParams.EnterpriseCode == cntr._EnterpriseCode)
                                //        {
                                //            switch (canv.BubbleMapType)
                                //            {
                                //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                //                    of.UpdateBubbleMap(worker, e, canvas.BubbleK, filter);
                                //                    break;
                                //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                //                    of.UpdateBubbleAccumMap(worker, e, canvas.BubbleK, filter);
                                //                    break;
                                //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                //                    of.UpdateBubbleHistoryMap(worker, e, canv.BubbleHistoryViewBySector, canvas.BubbleK, filter);
                                //                    break;
                                //            }
                                //            find = true;
                                //        }
                                //    }
                                //}
                                //else if (cntr._NGDUCode > -1)
                                //{
                                //    len = 0;
                                //    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                //    {
                                //        of = canvas.MapProject.OilFields[j];
                                //        if (of.OFParams.NGDUCode == cntr._NGDUCode)
                                //            len += of.WellList.Length + 2;
                                //    }
                                //    progress.Set(2, len);
                                //    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                //    {
                                //        of = canvas.MapProject.OilFields[j];
                                //        if (of.OFParams.NGDUCode == cntr._NGDUCode)
                                //        {
                                //            switch (canv.BubbleMapType)
                                //            {
                                //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                //                    of.UpdateBubbleMap(worker, e, canvas.BubbleK, filter);
                                //                    break;
                                //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                //                    of.UpdateBubbleAccumMap(worker, e, canvas.BubbleK, filter);
                                //                    break;
                                //                case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                //                    of.UpdateBubbleHistoryMap(worker, e, canv.BubbleHistoryViewBySector, canvas.BubbleK, filter);
                                //                    break;
                                //            }
                                //            find = true;
                                //        }
                                //    }
                                //}
                                #endregion

                                if (cntr._OilFieldCode > -1)
                                {
                                    len = 0;
                                    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                    {
                                        of = canvas.MapProject.OilFields[j];
                                        if (of.OilFieldCode == cntr._OilFieldCode)
                                        {
                                            len += of.Wells.Count + 2;
                                            break;
                                        }
                                    }
                                    progress.Set(2, len);
                                    for (j = 0; j < canvas.MapProject.OilFields.Count; j++)
                                    {
                                        of = canvas.MapProject.OilFields[j];
                                        if (of.OilFieldCode == cntr._OilFieldCode)
                                        {
                                            switch (canv.BubbleMapType)
                                            {
                                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                                    of.UpdateBubbleMap(worker, e, LiquidInCube, canvas.BubbleK, filter);
                                                    break;
                                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                                    of.UpdateBubbleAccumMap(worker, e, LiquidInCube, canvas.BubbleK, filter);
                                                    break;
                                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                                    of.UpdateBubbleHistoryMap(worker, e, LiquidInCube, canv.BubbleHistoryViewBySector, canvas.BubbleK, filter);
                                                    break;
                                            }
                                            find = true;
                                        }
                                    }
                                }
                            }
                        }
                        if ((canvas.twActiveOilField.ActiveOilFieldIndex > -1) && (!find))
                        {
                            of = canvas.MapProject.OilFields[canvas.twActiveOilField.ActiveOilFieldIndex];
                            progress.Set(2, of.Wells.Count + 2);
                            switch (canv.BubbleMapType)
                            {
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                                    of.UpdateBubbleMap(worker, e, LiquidInCube, canvas.BubbleK, filter);
                                    break;
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                                    of.UpdateBubbleAccumMap(worker, e, LiquidInCube, canvas.BubbleK, filter);
                                    break;
                                case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                                    of.UpdateBubbleHistoryMap(worker, e, LiquidInCube, canv.BubbleHistoryViewBySector, canvas.BubbleK, filter);
                                    break;
                            }
                        }
                    }
                    break;
                #endregion

                #region SUM_WELLLIST_PARAMS_CALC [Расчет суммарных показателей по списку скважин]
                case Constant.BG_WORKER_WORK_TYPE.SUM_WELLLIST_PARAMS_CALC:
                    if (this.WorkObj.DataObject != null)
                    {
                        this.EndWork = false;
                        progress.Clear();
                        CanvasMap canvas = (CanvasMap)this.WorkObj.DataObject;
                        bool res = true;

                        ArrayList ofIndexes = new ArrayList(5);
                        ArrayList ofWellCount = new ArrayList(5);
                        List<List<int>> WellIndexes = new List<List<int>>();
                        Well w;
                        OilField of;
                        int i, ind;

                        for (i = 0; i < canvas.selWellList.Count; i++)
                        {
                            if (!canvas.IsGraphicsByOneDate)
                            {
                                w = (Well)canvas.selWellList[i];
                            }
                            else
                            {
                                w = ((PhantomWell)canvas.selWellList[i]).srcWell;
                            }
                            if (w.ProjectDest > 0) continue;
                            ind = ofIndexes.IndexOf((int)w.OilFieldIndex);
                            if ((w.OilFieldIndex != -1) && (ind == -1))
                            {
                                of = canvas.MapProject.OilFields[w.OilFieldIndex];
                                ofIndexes.Add(w.OilFieldIndex);
                                ofWellCount.Add(1);
                                WellIndexes.Add(new List<int>());
                                WellIndexes[WellIndexes.Count - 1].Add(w.Index);
                            }
                            else if (ind > -1)
                            {
                                ofWellCount[ind] = (int)ofWellCount[ind] + 1;
                                WellIndexes[ind].Add(w.Index);
                            }
                        }

                        for (i = 0; i < ofIndexes.Count; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                canvas.sumParams = null;
                                res = false;
                                break;
                            }
                            count = (int)ofWellCount[i];
                            of = canvas.MapProject.OilFields[(int)ofIndexes[i]];

                            if (count / (double)of.Wells.Count > 0.8)
                            {
                                if (!of.MerLoaded)
                                {
                                    of.LoadMerFromCache(worker, e);
                                }
                                if (!of.WellResearchLoaded)
                                {
                                    of.LoadWellResearchFromCache(worker, e);
                                }
                            }
                            else
                            {
                                if (!of.MerLoaded) of.LoadMerFromCache(worker, e, WellIndexes[i]);
                                if (!of.WellResearchLoaded) of.LoadWellResearchFromCache(worker, e, WellIndexes[i]);
                            }
                        }

                        if (res)
                        {
                            if (!canvas.IsGraphicsByOneDate)
                            {
                                progress.Set(0, canvas.selWellList.Count);
                                canvas.SumParamsReCalc(worker, e);
                            }
                            else
                            {
                                progress.Set(0, canvas.selWellList.Count * 2);
                                canvas.SumParamsReCalcByOneDate(worker, e);
                            }
                        }
                        for (i = 0; i < ofIndexes.Count; i++)
                        {
                            of = canvas.MapProject.OilFields[(int)ofIndexes[i]];
                            if (of.MerLoaded) of.ClearMerData(false);
                            if (of.WellResearchLoaded) of.ClearWellResearchData(false);
                        }
                        GC.GetTotalMemory(true);
                        progress.Clear();
                        for (i = 0; i < ofIndexes.Count; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                canvas.sumParams = null;
                                res = false;
                                break;
                            }
                            count = (int)ofWellCount[i];
                            of = canvas.MapProject.OilFields[(int)ofIndexes[i]];
                            if (count / (double)of.Wells.Count > 0.5)
                            {
                                if (!of.ChessLoaded) of.LoadChessFromCache(worker, e);
                            }
                            else
                            {
                                if (!of.ChessLoaded) of.LoadChessFromCache(worker, e, WellIndexes[i]);
                            }
                        }

                        if (res)
                        {
                            progress.Set(0, canvas.selWellList.Count * 2);
                            if (!canvas.IsGraphicsByOneDate)
                            {
                                canvas.SumChessParamsReCalc(worker, e);
                            }
                            else
                            {
                                canvas.SumChessParamsReCalcByOneDate(worker, e);
                            }
                        }
                        for (i = 0; i < ofIndexes.Count; i++)
                        {
                            of = canvas.MapProject.OilFields[(int)ofIndexes[i]];
                            if (of.ChessLoaded) of.ClearChessData(false);
                        }
                        GC.GetTotalMemory(true);
                    }
                    break;



                case Constant.BG_WORKER_WORK_TYPE.SUM_WELLLIST_TABLE_RECALC:
                    break;
                #endregion 

                #region SUM AREA CALC PARAMS [Загрузка суммарных показателей по ячейке заводнения месторождения]
                case Constant.BG_WORKER_WORK_TYPE.SUM_AREA_PARAMS_CALC:
                    if (this.WorkObj.DataObject != null)
                    {
                        OilField of = (OilField)this.WorkObj.DataObject;
                        progress.Clear();
                        this.EndWork = false;
                        Area area = (Area)of.Areas[this.WorkObj.ObjectIndex];
                        if (!of.LoadSumAreaFromCache(area.Index))
                        {
                            if (of.ReCalcSumAreaData(worker, e))
                            {
                                of.WriteSumAreaToCache(worker, e);
                            }
                        }
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_AREA_TABLE_RECALC:
                    if (this.WorkObj.DataObject != null)
                    {
                        Area area = (Area)this.WorkObj.DataObject;
                        int ind = canv.sumIndexes.IndexOf(area);
                        if (ind == -1)
                        {
                            canv.AddSumParams(worker, e, area.sumObjParams);
                            canv.AddChessSumParams(worker, e, area.sumObjChessParams);
                            canv.sumIndexes.Add(area);
                        }
                        else
                        {
                            canv.sumIndexes.RemoveAt(ind);
                            if (canv.sumIndexes.Count > 0)
                            {
                                DateTime minDt = DateTime.MaxValue, maxDt = DateTime.MinValue;
                                DateTime minDtChess = DateTime.MaxValue, maxDtChess = DateTime.MinValue;
                                SumParamItem[] sumParams;
                                for (int i = 0; i < canv.sumIndexes.Count; i++)
                                {
                                    if (((Area)canv.sumIndexes[i]).sumObjParams != null)
                                    {
                                        sumParams = ((Area)canv.sumIndexes[i]).sumObjParams.MonthlyItems;
                                        if (sumParams.Length > 0)
                                        {
                                            if (minDt > sumParams[0].Date) minDt = sumParams[0].Date;
                                            if (maxDt < sumParams[sumParams.Length - 1].Date) maxDt = sumParams[sumParams.Length - 1].Date;
                                        }
                                    }
                                    if (((Area)canv.sumIndexes[i]).sumObjChessParams != null)
                                    {
                                        sumParams = ((Area)canv.sumIndexes[i]).sumObjChessParams.MonthlyItems;
                                        if (sumParams.Length > 0)
                                        {
                                            if (minDtChess > sumParams[0].Date) minDtChess = sumParams[0].Date;
                                            if (maxDtChess < sumParams[sumParams.Length - 1].Date) maxDtChess = sumParams[sumParams.Length - 1].Date;
                                        }
                                    }
                                }
                                canv.SubSumParams(worker, e, area.sumObjParams, minDt, maxDt);
                                canv.SubChessSumParams(worker, e, area.sumObjChessParams, minDtChess, maxDtChess);
                            }
                            else
                            {
                                canv.ClearSumList();
                            }
                        }
                    }
                    break;
                #endregion

                #region LOAD_AREAS_PARAMS_FROM_CACHE [Загрузка таблицы параметров ЯЗ из кэша]
                case Constant.BG_WORKER_WORK_TYPE.LOAD_AREAS_PARAMS_FROM_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(0, 100);
                        mainForm.dAreasLoadTable(worker, e);
                    }
                    break;
                #endregion

                #region FILL_AREAS_PARAMS_TO_TABLE [Расчет и запись в кеш таблицы параметров ЯЗ]
                case Constant.BG_WORKER_WORK_TYPE.FILL_AREAS_PARAMS_TO_TABLE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        progress.Set(1, proj.OilFields.Count);
                        mainForm.dAreasFillTable(worker, e);
                        progress.Set(0, 100);
                        mainForm.dAreasWriteToCache(worker, e);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.FILL_AREAS_PARAMS_TO_TABLE_BY_DATE:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        DateTime dt1, dt2;
                        string[] parse = proj.tempStr.Split(new char[] { ';' });
                        try
                        {
                            dt1 = Convert.ToDateTime(parse[0]);
                            dt2 = Convert.ToDateTime(parse[1]);
                        }
                        catch
                        {
                            dt1 = DateTime.MinValue;
                            dt2 = DateTime.MinValue;
                        }
                        if ((parse.Length > 1) && (dt1 < dt2))
                        {
                            if (dt2 > proj.maxMerDate) dt2 = proj.maxMerDate;
                            progress.Set(1, (int)(proj.OilFields.Count * 1.2));
                            mainForm.dAreasFillTable(worker, e, dt1, dt2);
                            progress.Set(0, 100);
                            mainForm.dAreasWriteToCache(worker, e);
                            WorkerState userState = new WorkerState();
                            userState.WorkMainTitle = "";
                            userState.WorkCurrentTitle = "Подождите...идет обновление таблицы.";
                            worker.ReportProgress(0, userState);
                        }
                    }
                    break;
                #endregion

                #region RESET AREAS ALL PARAMS [Пересчет параметров всех ЯЗ месторождения]
                case Constant.BG_WORKER_WORK_TYPE.RESET_AREAS_ALL_PARAMS:
                    if (this.WorkObj.DataObject != null)
                    {
                        OilField of = (OilField)this.WorkObj.DataObject;
                        progress.Set(2, of.Areas.Count * 3);
                        of.FillAreaWellList(worker, e, false);
                        of.ReCalcSumAreaData(worker, e);
                    }
                    break;
                #endregion

                #region SUM OF CALC TABLE [Загрузка суммарных показателей по месторождению]
                case Constant.BG_WORKER_WORK_TYPE.SUM_OF_PARAMS_CALC:
                    if (this.WorkObj.DataObject != null)
                    {
                        OilField of = (OilField)this.WorkObj.DataObject;
                        progress.Clear();
                        this.EndWork = false;
                        if (of.Table81.Items == null) of.LoadTable81ParamsFromCache(worker, e);

                        if (!of.BizPlanLoaded) of.LoadBizPlanHeadFromCache(worker, e);

                        if (!of.LoadSumParamsFromCache(worker, e))
                        {
                            if (of.ReCalcSumParams(worker, e, true))
                            {
                                of.WriteSumParamsToCache(worker, e);
                            }
                        }
                        if (!of.LoadSumChessParamsFromCache(worker, e))
                        {
                            if (of.ReCalcSumChessParams(worker, e))
                            {
                                of.WriteSumChessParamsToCache(worker, e);
                            }
                        }

                    }
                    break;

                case Constant.BG_WORKER_WORK_TYPE.SUM_OF_TABLE_RECALC:
                    if (this.WorkObj.DataObject != null)
                    {
                        OilField of = (OilField)this.WorkObj.DataObject;
                        int ind = canv.sumIndexes.IndexOf(of);
                        if (ind == -1)
                        {
                            canv.AddSumParams(worker, e, of.sumParams);
                            canv.AddChessSumParams(worker, e, of.sumChessParams);
                            canv.AddTable81Params(worker, e, of.Table81.Items);
                            if (of.BizPlanCollection != null && of.BizPlanCollection.Count > 0)
                            {
                                canv.AddBPParams(worker, e, of.BizPlanCollection.GetLastBizPlan(), of.BPParams);
                            }
                            canv.sumIndexes.Add(of);
                            if (canv.sumIndexes.Count == 1)
                            {
                                mainForm.dChartSetProjName(of.Table81.ProjectDocName);
                            }
                            else
                            {
                                mainForm.dChartSetProjName("");
                            }
                        }
                        else
                        {
                            canv.sumIndexes.RemoveAt(ind);
                            if (canv.sumIndexes.Count > 0)
                            {
                                DateTime minDt = DateTime.MaxValue, maxDt = DateTime.MinValue;
                                DateTime minDtChess = DateTime.MaxValue, maxDtChess = DateTime.MinValue;
                                DateTime minDtProj = DateTime.MaxValue, maxDtProj = DateTime.MinValue;
                                DateTime minDtBP = DateTime.MaxValue, maxDtBP = DateTime.MinValue;
                                SumParamItem[] sumParams;
                                for (int i = 0; i < canv.sumIndexes.Count; i++)
                                {
                                    if ((((OilField)canv.sumIndexes[i]).sumParams != null) && (((OilField)canv.sumIndexes[i]).sumParams.Count > 0))
                                    {
                                        sumParams = ((OilField)canv.sumIndexes[i]).sumParams[0].MonthlyItems;
                                        if (minDt > sumParams[0].Date) minDt = sumParams[0].Date;
                                        if (maxDt < sumParams[sumParams.Length - 1].Date) maxDt = sumParams[sumParams.Length - 1].Date;
                                    }
                                    if ((((OilField)canv.sumIndexes[i]).sumChessParams != null) && (((OilField)canv.sumIndexes[i]).sumChessParams.Count > 0))
                                    {
                                        sumParams = ((OilField)canv.sumIndexes[i]).sumChessParams[0].MonthlyItems;
                                        if (minDtChess > sumParams[0].Date) minDtChess = sumParams[0].Date;
                                        if (maxDtChess < sumParams[sumParams.Length - 1].Date) maxDtChess = sumParams[sumParams.Length - 1].Date;
                                    }
                                    if ((((OilField)canv.sumIndexes[i]).Table81 != null) && (((OilField)canv.sumIndexes[i]).Table81.Count > 0))
                                    {
                                        sumParams = ((OilField)canv.sumIndexes[i]).Table81.Items;
                                        if (minDtProj > sumParams[0].Date) minDtProj = sumParams[0].Date;
                                        if (maxDtProj < sumParams[sumParams.Length - 1].Date) maxDtProj = sumParams[sumParams.Length - 1].Date;
                                    }
                                    if ((((OilField)canv.sumIndexes[i]).BizPlanCollection != null) && (((OilField)canv.sumIndexes[i]).BizPlanCollection.Count > 0))
                                    {
                                        OilfieldBizplan bp = ((OilField)canv.sumIndexes[i]).BizPlanCollection.GetLastBizPlan();
                                        if (bp != null)
                                        {
                                            if (minDtBP > bp.StartDate) minDtBP = bp.StartDate;
                                            if (maxDtBP < bp.StartDate.AddMonths(24)) maxDtBP = bp.StartDate.AddMonths(24);
                                        }
                                    }
                                }

                                if (minDt != DateTime.MaxValue) canv.SubSumParams(worker, e, of.sumParams, minDt, maxDt);
                                if (minDtChess != DateTime.MaxValue) canv.SubChessSumParams(worker, e, of.sumChessParams, minDtChess, maxDtChess);
                                if (minDtProj != DateTime.MaxValue) canv.SubTable81Params(worker, e, of.Table81.Items, minDtProj, maxDtProj);
                            }
                            else
                            {
                                canv.ClearSumList();
                            }
                        }
                    }
                    break;
                #endregion

                #region SUM NGDU CALC TABLE [Загрузка суммарных показателей по НГДУ]
                case Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_PARAMS_CALC:
                    if ((this.WorkObj.DataObject != null) && (this.WorkObj.ObjectIndex > -1))
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        OilField of;
                        int i, len = 0, countOF = 0;
                        for (i = 0; i < proj.OilFields.Count; i++)
                            if ((proj.OilFields[i]).ParamsDict.NGDUCode == this.WorkObj.ObjectIndex)
                                len++;
                        this.EndWork = false;
                        progress.Set(1, len * 2);

                        
                        string text = "";
                        for (i = 0; i < proj.OilFields.Count; i++)
                        {
                            of = proj.OilFields[i];
                            
                            if ((of.ParamsDict.NGDUCode == this.WorkObj.ObjectIndex) && (!worker.CancellationPending))
                            {
                                if (of.Table81.Items == null) of.LoadTable81ParamsFromCache(worker, e);

                                if (!of.BizPlanLoaded) of.LoadBizPlanHeadFromCache(worker, e);

                                if (!of.LoadSumParamsFromCache(worker, e))
                                {
                                    if (of.ReCalcSumParams(worker, e, true))
                                    {
                                        of.WriteSumParamsToCache(worker, e);
                                    }
                                }
                                if (!of.LoadSumChessParamsFromCache(worker, e))
                                {
                                    if (of.ReCalcSumChessParams(worker, e))
                                    {
                                        of.WriteSumChessParamsToCache(worker, e);
                                    }
                                }
                                text = of.ParamsDict.NGDUName;
                                countOF++;
                            }
                        }
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_TABLE_RECALC:
                    if ((this.WorkObj.DataObject != null) && (this.WorkObj.ObjectIndex > -1))
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int i, len = 0;
                        OilField of;
                        for (i = 0; i < proj.OilFields.Count; i++)
                        {
                            of = proj.OilFields[i];
                            if ((of.ParamsDict.NGDUCode == this.WorkObj.ObjectIndex) &&(of.sumParams != null))
                                len++;
                        }
                        progress.Set(1, len);
                        for (i = 0; i < proj.OilFields.Count; i++)
                        {
                            of = proj.OilFields[i];
                            if ((of.ParamsDict.NGDUCode == this.WorkObj.ObjectIndex) && (!this.CancellationPending))
                            {
                                int ind = canv.sumIndexes.IndexOf(of);
                                if (ind == -1)
                                {
                                    canv.AddSumParams(worker, e, of.sumParams);
                                    canv.AddChessSumParams(worker, e, of.sumChessParams);
                                    canv.AddTable81Params(worker, e, of.Table81.Items);
                                    if (of.BizPlanCollection != null && of.BizPlanCollection.Count > 0)
                                    {
                                        canv.AddBPParams(worker, e, of.BizPlanCollection.GetLastBizPlan(), of.BPParams);
                                    }
                                    canv.sumIndexes.Add(of);
                                    if (canv.sumIndexes.Count == 1)
                                    {
                                        mainForm.dChartSetProjName(of.Table81.ProjectDocName);
                                    }
                                    else
                                    {
                                        mainForm.dChartSetProjName("");
                                    }
                                }
                                else
                                {
                                    canv.sumIndexes.RemoveAt(ind);
                                    if (canv.sumIndexes.Count > 0)
                                    {
                                        DateTime minDt = DateTime.MaxValue, maxDt = DateTime.MinValue;
                                        DateTime minDtChess = DateTime.MaxValue, maxDtChess = DateTime.MinValue;
                                        DateTime minDtProj = DateTime.MaxValue, maxDtProj = DateTime.MinValue;
                                        DateTime minDtBP = DateTime.MaxValue, maxDtBP = DateTime.MinValue;
                                        SumParamItem[] sumParams;
                                        for (int j = 0; j < canv.sumIndexes.Count; j++)
                                        {
                                            if ((((OilField)canv.sumIndexes[j]).sumParams != null) && (((OilField)canv.sumIndexes[j]).sumParams.Count > 0))
                                            {
                                                sumParams = ((OilField)canv.sumIndexes[j]).sumParams[0].MonthlyItems;
                                                if (minDt > sumParams[0].Date) minDt = sumParams[0].Date;
                                                if (maxDt < sumParams[sumParams.Length - 1].Date) maxDt = sumParams[sumParams.Length - 1].Date;
                                            }
                                            if ((((OilField)canv.sumIndexes[j]).sumChessParams != null) && (((OilField)canv.sumIndexes[j]).sumChessParams.Count > 0))
                                            {
                                                sumParams = ((OilField)canv.sumIndexes[j]).sumChessParams[0].MonthlyItems;
                                                if (minDtChess > sumParams[0].Date) minDtChess = sumParams[0].Date;
                                                if (maxDtChess < sumParams[sumParams.Length - 1].Date) maxDtChess = sumParams[sumParams.Length - 1].Date;
                                            }
                                            if ((((OilField)canv.sumIndexes[j]).Table81 != null) && (((OilField)canv.sumIndexes[j]).Table81.Count > 0))
                                            {
                                                sumParams = ((OilField)canv.sumIndexes[j]).Table81.Items;
                                                if (minDtProj > sumParams[0].Date) minDtProj = sumParams[0].Date;
                                                if (maxDtProj < sumParams[sumParams.Length - 1].Date) maxDtProj = sumParams[sumParams.Length - 1].Date;
                                            }
                                            if ((((OilField)canv.sumIndexes[j]).BizPlanCollection != null) && (((OilField)canv.sumIndexes[j]).BizPlanCollection.Count > 0))
                                            {
                                                OilfieldBizplan bp = ((OilField)canv.sumIndexes[j]).BizPlanCollection.GetLastBizPlan();
                                                if (bp != null)
                                                {
                                                    if (minDtBP > bp.StartDate) minDtBP = bp.StartDate;
                                                    if (maxDtBP < bp.StartDate.AddMonths(24)) maxDtBP = bp.StartDate.AddMonths(24);
                                                }
                                            }
                                        }

                                        if (minDt != DateTime.MaxValue) canv.SubSumParams(worker, e, of.sumParams, minDt, maxDt);
                                        if (minDtChess != DateTime.MaxValue) canv.SubChessSumParams(worker, e, of.sumChessParams, minDtChess, maxDtChess);
                                        if (minDtProj != DateTime.MaxValue) canv.SubTable81Params(worker, e, of.Table81.Items, minDtProj, maxDtProj);
                                    }
                                    else
                                    {
                                        canv.ClearSumList();
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region SUM REGION CALC TABLE [Загрузка суммарных показателей по Региону]
                case Constant.BG_WORKER_WORK_TYPE.SUM_REG_PARAMS_CALC:
                    if ((this.WorkObj.DataObject != null) && (this.WorkObj.ObjectIndex > -1))
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        OilField of;
                        int i, len = 0;
                        for (i = 0; i < proj.OilFields.Count; i++)
                            if ((proj.OilFields[i]).ParamsDict.EnterpriseCode == this.WorkObj.ObjectIndex)
                                len++;
                        this.EndWork = false;
                        progress.Set(1, len * 2);
                        string text = "";

                        for (i = 0; i < proj.OilFields.Count; i++)
                        {
                            of = proj.OilFields[i];
                            if (of.ParamsDict.EnterpriseCode == this.WorkObj.ObjectIndex)
                            {
                                text = of.ParamsDict.EnterpriseName;
                                if (of.Table81.Items == null) of.LoadTable81ParamsFromCache(worker, e);
                                if (!of.BizPlanLoaded) of.LoadBizPlanHeadFromCache(worker, e);

                                if (!of.LoadSumParamsFromCache(worker, e))
                                {
                                    if (of.ReCalcSumParams(worker, e, true))
                                    {
                                        of.WriteSumParamsToCache(worker, e);
                                    }
                                }
                                if (!of.LoadSumChessParamsFromCache(worker, e))
                                {
                                    if (of.ReCalcSumChessParams(worker, e))
                                    {
                                        of.WriteSumChessParamsToCache(worker, e);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_REG_TABLE_RECALC:
                    if ((this.WorkObj.DataObject != null) && (this.WorkObj.ObjectIndex > -1))
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int i, len = 0;
                        OilField of;

                        for (i = 0; i < proj.OilFields.Count; i++)
                            if ((proj.OilFields[i]).ParamsDict.EnterpriseCode == this.WorkObj.ObjectIndex)
                                len++;
                        progress.Set(1, len * 3);
                        for (i = 0; i < proj.OilFields.Count; i++)
                        {
                            of = proj.OilFields[i];
                            if (of.ParamsDict.EnterpriseCode == this.WorkObj.ObjectIndex)
                            {
                                int ind = canv.sumIndexes.IndexOf(of);
                                if (ind == -1)
                                {
                                    canv.AddSumParams(worker, e, of.sumParams);
                                    canv.AddChessSumParams(worker, e, of.sumChessParams);
                                    canv.AddTable81Params(worker, e, of.Table81.Items);
                                    if (of.BizPlanCollection != null && of.BizPlanCollection.Count > 0)
                                    {
                                        canv.AddBPParams(worker, e, of.BizPlanCollection.GetLastBizPlan(), of.BPParams);
                                    }
                                    canv.sumIndexes.Add(of);
                                    if (canv.sumIndexes.Count == 1)
                                    {
                                        mainForm.dChartSetProjName(of.Table81.ProjectDocName);
                                    }
                                    else
                                    {
                                        mainForm.dChartSetProjName("");
                                    }
                                }
                                else
                                {
                                    canv.sumIndexes.RemoveAt(ind);
                                    if (canv.sumIndexes.Count > 0)
                                    {
                                        DateTime minDt = DateTime.MaxValue, maxDt = DateTime.MinValue;
                                        DateTime minDtChess = DateTime.MaxValue, maxDtChess = DateTime.MinValue;
                                        DateTime minDtProj = DateTime.MaxValue, maxDtProj = DateTime.MinValue;
                                        DateTime minDtBP = DateTime.MaxValue, maxDtBP = DateTime.MinValue;
                                        SumParamItem[] sumParams;
                                        for (int j = 0; j < canv.sumIndexes.Count; j++)
                                        {
                                            if ((((OilField)canv.sumIndexes[j]).sumParams != null) && (((OilField)canv.sumIndexes[j]).sumParams.Count > 0))
                                            {
                                                sumParams = ((OilField)canv.sumIndexes[j]).sumParams[0].MonthlyItems;
                                                if (minDt > sumParams[0].Date) minDt = sumParams[0].Date;
                                                if (maxDt < sumParams[sumParams.Length - 1].Date) maxDt = sumParams[sumParams.Length - 1].Date;
                                            }
                                            if ((((OilField)canv.sumIndexes[j]).sumChessParams != null) && (((OilField)canv.sumIndexes[j]).sumChessParams.Count > 0))
                                            {
                                                sumParams = ((OilField)canv.sumIndexes[j]).sumChessParams[0].MonthlyItems;
                                                if (minDtChess > sumParams[0].Date) minDtChess = sumParams[0].Date;
                                                if (maxDtChess < sumParams[sumParams.Length - 1].Date) maxDtChess = sumParams[sumParams.Length - 1].Date;
                                            }
                                            if ((((OilField)canv.sumIndexes[j]).Table81 != null) && (((OilField)canv.sumIndexes[j]).Table81.Count > 0))
                                            {
                                                sumParams = ((OilField)canv.sumIndexes[j]).Table81.Items;
                                                if (minDtProj > sumParams[0].Date) minDtProj = sumParams[0].Date;
                                                if (maxDtProj < sumParams[sumParams.Length - 1].Date) maxDtProj = sumParams[sumParams.Length - 1].Date;
                                            }
                                            if ((((OilField)canv.sumIndexes[j]).BizPlanCollection != null) && (((OilField)canv.sumIndexes[j]).BizPlanCollection.Count > 0))
                                            {
                                                OilfieldBizplan bp = ((OilField)canv.sumIndexes[j]).BizPlanCollection.GetLastBizPlan();
                                                if (bp != null)
                                                {
                                                    if (minDtBP > bp.StartDate) minDtBP = bp.StartDate;
                                                    if (maxDtBP < bp.StartDate.AddMonths(24)) maxDtBP = bp.StartDate.AddMonths(24);
                                                }
                                            }
                                        }

                                        if (minDt != DateTime.MaxValue) canv.SubSumParams(worker, e, of.sumParams, minDt, maxDt);
                                        if (minDtChess != DateTime.MaxValue) canv.SubChessSumParams(worker, e, of.sumChessParams, minDtChess, maxDtChess);
                                        if (minDtProj != DateTime.MaxValue) canv.SubTable81Params(worker, e, of.Table81.Items, minDtProj, maxDtProj);
                                        if (minDtBP != DateTime.MaxValue) canv.SubBPParams(worker, e, of.BizPlanCollection.GetLastBizPlan(), of.BPParams, minDtBP, maxDtBP);
                                    }
                                    else
                                    {
                                        canv.ClearSumList();
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region SUM OILOBJ CALC TABLE [Загрузка суммарных по объектам]
                case Constant.BG_WORKER_WORK_TYPE.SUM_OILOBJ_PARAMS_CALC:
                    if (this.WorkObj.DataObject != null)
                    {
                        int i, len = 0, type = -99;
                        for (i = 0; i < canv.selLayerList.Count; i++)
                        {
                            if ((((C2DLayer)canv.selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                                ((C2DLayer)canv.selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) &&
                                (((Contour)((C2DLayer)canv.selLayerList[i]).ObjectsList[0])._ContourType < -2))
                                len++;
                        }
                        this.EndWork = false;
                        progress.Set(1, len * 2);
                        C2DLayer lr;
                        Contour cntr;
                        for (i = 0; i < canv.selLayerList.Count; i++)
                        {
                            lr = (C2DLayer)canv.selLayerList[i];
                            if ((lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                                 lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
                            {
                                cntr = (Contour)lr.ObjectsList[0];
                                if ((type == -99) && (cntr._ContourType < -2)) type = cntr._ContourType;
                                if (cntr._ContourType == type)
                                {
                                    if (lr.oilfield != null)
                                    {
                                        if (!lr.oilfield.LoadSumParamsFromCache(worker, e))
                                        {
                                            if (lr.oilfield.ReCalcSumParams(worker, e, true))
                                            {
                                                lr.oilfield.WriteSumParamsToCache(worker, e);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_OILOBJ_TABLE_RECALC:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        int i, len = 0, type = -99;
                        for (i = 0; i < canv.selLayerList.Count; i++)
                        {
                            if ((((C2DLayer)canv.selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                                ((C2DLayer)canv.selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) &&
                                (((Contour)((C2DLayer)canv.selLayerList[i]).ObjectsList[0])._ContourType < -2))
                                len++;
                        }
                        progress.Set(1, len);

                        C2DLayer lr;
                        Contour cntr;
                        for (i = 0; i < canv.selLayerList.Count; i++)
                        {
                            lr = (C2DLayer)canv.selLayerList[i];
                            if ((lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                                 lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
                            {
                                cntr = (Contour)lr.ObjectsList[0];
                                if ((type == -99) && (cntr._ContourType < -2)) type = cntr._ContourType;
                                if (cntr._ContourType == type)
                                {
                                    if ((lr.oilfield != null) && (!this.CancellationPending))
                                    {
                                        //mainForm.dChartSumParamsLoad(worker, e, lr._OilField);
                                        //if (!canv.AppendSumParams) canv.AppendSumParams = true;
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion
            }
        }
        void BGWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progress.Value = e.ProgressPercentage;
            int percent = progress.Percent;
            
            this.pb.Value = percent;
            //this.pb.
            WorkerState uState = null;
            if (e.UserState != null)
            {
                uState = (WorkerState)e.UserState;
                if (uState.WorkCurrentTitle != "")  stBarLabel.Text = uState.WorkCurrentTitle;
            }

            if ((fProgress != null) && (fProgress.Visible))
            {
                fProgress.pb.Value = percent;
                if (uState != null)
                {
                    if (uState.WorkMainTitle != "") fProgress.Text = uState.WorkMainTitle;

                    if ((WorkObj.WorkType == Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_PARAMS_CALC) ||
                        (WorkObj.WorkType == Constant.BG_WORKER_WORK_TYPE.SUM_REG_PARAMS_CALC))
                    {
                        fProgress.lMain.Text = fProgress.Text + String.Format(" ({0} %)", percent);
                    }
                    else
                        fProgress.lMain.Text = uState.WorkCurrentTitle + String.Format(" ({0} %)", percent);
                    fProgress.lElement.Text = uState.Element;
                    fProgress.lTime.Text = uState.Time;
                }
                //if ((percent > 0) && (percent % 10 == 0))
                //{
                //    long workTime = (DateTime.Now.Ticks - fProgress.startTime);
                //    long time = (workTime * 100 / percent - workTime);
                //    TimeSpan ts = new TimeSpan(time);
                //    string strTime = "";
                //    if (ts.Hours > 0) strTime += String.Format("{0} часов", ts.Hours);
                //    if (ts.Minutes > 0) strTime += String.Format(" {0} минут", ts.Minutes);
                //    if (ts.Seconds > 0) strTime += String.Format(" {0} секунд", ts.Seconds);
                //}
            }
        }
        void BGWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            if (e.Error != null)
            {
                pb.Visible = false;
                progress.Clear();
                fProgress.End();
                MessageBox.Show(mainForm, e.Error.Message + ((e.Error.InnerException != null) ? "\n" + e.Error.InnerException.Message : string.Empty), "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                canv.DrawLayerList();
            }
            else
            {
                if (e.Cancelled)
                {
                    if (mainForm.AppRestart)
                    {
                        Application.Restart();
                        return;
                    }
                }
                if ((this.Queue.Count > 0) && (this.EndWork))
                {
                    while (QueueBusy)
                    {
                        System.Threading.Thread.Sleep(250);
                    }
                    this.EndWork = false;
                    WorkObject obj = (WorkObject)this.Queue[0];
                    this.Run(obj.WorkType, obj.DataObject, obj.ObjectIndex);
                    this.Queue.RemoveAt(0);
                }
                else
                {
                    AfterWorkerCompleted(e);
                    mainForm.AfterBGWorkerCompleted(this.WorkObj.WorkType, e);
                }


                if ((this.EndWork) && (!newLaunch))
                {
                    pb.Visible = false;
                    progress.Clear();
                    if (fProgress != null)
                    {
                        Application.DoEvents();
                        System.Threading.Thread.Sleep(100);
                        fProgress.End();
                    }
                }
                if (newLaunch) newLaunch = false;
            }
        }

        private void BeforeWorkerRun()
        {
            pb.Visible = true;
            //if (fProgress.UserClosing) return;
            switch (this.WorkObj.WorkType)
            {
                case Constant.BG_WORKER_WORK_TYPE.CREATE_GTM_LAYERLIST:
                    fProgress.Start("Создание списков ГТМ по датам...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_MER_PRODUCTION:
                    fProgress.Start("Создание отчета по добыче МЭР...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CORSCHEME_BY_PROFILE:
                    if (this.WorkObj.DataObject != null)
                    {
                        CorrelationScheme corScheme = (CorrelationScheme)this.WorkObj.DataObject;
                        if ((corScheme.ObjSrc != null) && 
                            (corScheme.ObjSrc.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                            ((Profile)((C2DLayer)corScheme.ObjSrc).ObjectsList[0]).Count > 50)
                        {
                            fProgress.Start("Загрузка скважинных данных в корсхему..");
                            fProgress.Show();
                        }
                    }
                    break;
#region BASNEFT
                case Constant.BG_WORKER_WORK_TYPE.PRODUCTION_SEPARATE:
                    fProgress.Start("Разделение добычи по ГИС...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CONVERT_WORK_FOLDER:
                    fProgress.Start("Конвертация Рабочей папки пользователя. Пожалуйста подождите...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_AREAS_GTM:
                    fProgress.Start("Выгрузка отчета ГТМ по ЯЗ...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_FLOODING_ANALYSIS:
                    fProgress.Start("Создание отчета 'Анализ заводнения'");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_DATABASE_ERROR:
                    fProgress.Start("Выгрузка отчета ошибок базы данных...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.GTM_AUTOMATCH_PERMEABILITY:
                    fProgress.Start("Подбор проницаемости по скважинам ГТМ..");
                    fProgress.Show();
                    break;
#endregion
                case Constant.BG_WORKER_WORK_TYPE.CREATE_VORONOI_MAP:
                    fProgress.Start("Создание карты областей Вороного...");
                    fProgress.Show();
                    break;

                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_FILE:
                    fProgress.Start("Загрузка сетки из файла...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_CHESS:
                    fProgress.Start("Создание отчета по шахматке");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_OF_UNION_CONTOUR:
                    fProgress.Start("Создание суммарных контуров по мест..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_WELLLIST_BY_FILTER:
                    fProgress.Start("Создание списка скважин по фильтру..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_GLOBAL_PROJECT:
                    fProgress.Start("Загрузка с сервера проектов..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PROJECT:
                    fProgress.Start("Загрузка проекта..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.RELOAD_PROJECT:
                    fProgress.Start("Загрузка проекта..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OILOBJ_CODES:
                    fProgress.Start("Загрузка объектов разработки..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CALC_AREAS_COMPENSATION:
                    fProgress.Start("Расчет компенсации по ячейкам..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_AREAS_PREDICT:
                    fProgress.Start("Выгрузка отчета 'Прогноз по ячейкам заводнения'..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.FILL_AREAS_PARAMS_TO_TABLE:
                    fProgress.Start("Расчет параметров ячеек заводнения..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.FILL_AREAS_PARAMS_TO_TABLE_BY_DATE:
                    fProgress.Start("Расчет параметров ячеек заводнения по датам..");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_WELLLIST_PARAMS_CALC:
                    if (canv.selWellList.Count > 5)
                    {
                        fProgress.Start("Расчет суммарных показателей по списку скважин...");
                        fProgress.Show();
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_PARAMS_CALC:
                    fProgress.Start("Расчет суммарных показателей по НГДУ...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_REG_PARAMS_CALC:
                    fProgress.Start("Расчет суммарных показателей по региону...");
                    fProgress.Show();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CALC_BUBBLE_MAP:
                    switch (canv.BubbleMapType)
                    {
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                            fProgress.Start("Расчет карты текущих отборов...");
                            break;
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                            fProgress.Start("Расчет карты накопленных отборов...");
                            break;
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                            fProgress.Start("Расчет карты изменений отборов...");
                            break;
                    }
                    fProgress.Show();
                    break;
            }
        }
        private void AfterWorkerCompleted(RunWorkerCompletedEventArgs e)
        {
            bool res;
            C2DLayer layer;
            switch (this.WorkObj.WorkType)
            {
                case Constant.BG_WORKER_WORK_TYPE.CREATE_GTM_LAYERLIST:
                    if (e.Result != null)
                    {
                        this.EndWork = true;
                        layer = (C2DLayer)e.Result;
                        canv.copyLayer = layer;
                        canv.PasteWorkNode(true);
                        canv.DrawLayerList();
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_SUM_ONNT_MAP:
                    if (e.Result != null)
                    {
                        layer = (C2DLayer)e.Result;
                        //canv.copyLayer = (C2DLayer)layer.ObjectsList[0];
                        //canv.PasteWorkLayer(true);
                        //canv.copyLayer = (C2DLayer)layer.ObjectsList[1];
                        //canv.PasteWorkLayer(true);
                        canv.copyLayer = (C2DLayer)layer.ObjectsList[0];
                        canv.PasteWorkLayer(true);
                    }

                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_TARGET_OPENING_GIS:
                    if (this.WorkObj.DataObject != null)
                    {
                        MessageBox.Show("Выгрузка отчета приоритетных пластов для вскрытия завершена!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.REPORT_DATABASE_ERROR:
                    if (this.WorkObj.DataObject != null)
                    {
                        Project proj = (Project)this.WorkObj.DataObject;
                        if ((proj.tempStr != "") && (File.Exists(proj.tempStr)))
                        {
                            System.Diagnostics.Process proc = new System.Diagnostics.Process();
                            proc.StartInfo.FileName = proj.tempStr;
                            proc.StartInfo.UseShellExecute = true;
                            proc.Start();
                        }
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.PRODUCTION_SEPARATE:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    canv.Enable();
                    canv.DrawLayerList();
                    if (this.WorkObj.DataObject != null)
                    {
                        //((Project)this.WorkObj.DataObject).PrintErrorList();
                    }
                    mainForm.reLog.AppendText("Разделение добычи завершено!" + "\n");
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SERVICE_FUNC:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    mainForm.FlushLog();
                    canv.Enable();
                    canv.DrawLayerList();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_VORONOI_MAP:
                    if (e.Result != null)
                    {
                        this.EndWork = false;
                        layer = (C2DLayer)e.Result;
                        TreeNode node = canv.twWork.Nodes[0].Nodes.Add("", layer.Name, 19, 19);
                        node.Tag = layer;
                        node.Checked = layer.Visible;
                        layer.node = node;
                        node.ContextMenuStrip = canv.twWork.cMenu;
                        canv.twWork.SetOneSelectedNode(node);
                        canv.WorkLayerActive = layer;
                        canv.LayerWorkList.Add(layer);
                        canv.AddSumWellList(false);
                        canv.WriteWorkLayer(layer);
                        canv.ClearFantomContour();
                        canv.DrawLayerList();
                        VoronoiMap map = (VoronoiMap)layer.ObjectsList[0];
                        int count = 0;
                        for (int i = 0; i < map.Cells.Length; i++)
                        {
                            if (map.Cells[i].Pvt.IsEmpty) count++;
                        }
                        if (count > 0)
                        {
                            string str = string.Format("{0} построенной карты не найдены PVT по ячейкам заводнения!\nПо этим областям запасы не рассчитаны!", (count == map.Cells.Length) ? "По всем областям Вороного" : "По некоторым областям Вороного");
                            MessageBox.Show(mainForm, str, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_CONTOUR_FROM_CACHE:
                    if (this.WorkObj.DataObject != null)
                    {
                        C2DLayer lr = (C2DLayer)this.WorkObj.DataObject;
                        if (!e.Cancelled)
                        {
                            lr.ResetFontsPens();
                        }
                        else
                        {
                            if (lr.oilfield != null) lr.oilfield.FreeContoursData();
                        }
                        if (lr.node != null) lr.node.Checked = !e.Cancelled;
                        canv.DrawLayerList();
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_OF_UNION_CONTOUR:
#if DEBUG
                    if (this.WorkObj.DataObject != null)
                    {
                        //Project proj = (Project)this.WorkObj.DataObject;
                        //OilField of = null;
                        //ArrayList list = new ArrayList();
                        //for (int i = 1; i < proj.OilFields.Count; i++)
                        //{
                        //    of = proj.OilFields[i];
                        //    if (of.ContourList.Length > 1)
                        //    {
                        //        of.WriteContoursToCache
                        //    }
                        //}
                        //if (list.Count > 0)
                        //{
                        //    //System.Drawing.Pen _pen_select = new System.Drawing.Pen(System.Drawing.Brushes.Black, 2);
                            
                        //    //for (int i = 0; i < list.Count; i++)
                        //    //{
                        //    //    C2DLayer lr = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, 0, _pen_select);
                        //    //    lr.Add((Contour)list[i]);
                        //    //    lr.GetObjectsRect();
                        //    //    lr.ResetFontsPens();
                        //    //    canv.LayerWorkList.Add(lr);
                        //    //    TreeNode tn = canv.twWork.Nodes[0].Nodes.Add("Суммарный контур " + i.ToString());
                        //    //    ((Contour)lr._ObjectsList[0]).Name = "Суммарный контур " + i.ToString();
                        //    //    lr.Name = "Суммарный контур " + i.ToString();
                        //    //    tn.Checked = true;
                        //    //    tn.Tag = lr;
                        //    //    lr._Node = tn;
                        //    //}
                        //    canv.DrawLayerList();
                        //}
                    }
#endif
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_PROFILE_BY_NEAR_WELL:
                    goto case Constant.BG_WORKER_WORK_TYPE.CREATE_PROFILE_BY_NEWCONTOUR;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_PROFILE_BY_NEWCONTOUR:
                    canv.DrawLayerList();
                    layer = mainForm.canv.AddNewProfile();
                    canv.WriteWorkLayer(layer);
                    if ((layer != null) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE) && (layer.ObjectsList.Count > 0))
                    {
                        mainForm.corSchLoadProfile(layer);
                        this.EndWork = false;
                    }
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PLANE_BY_WELL:
                    this.EndWork = true;
                    if (!e.Cancelled)
                    {
                        mainForm.planeLastPerfDepth();
                        mainForm.planeUpdateWindow(false);
                    }
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CORSCHEME_BY_PROFILE:
                    this.EndWork = true;
                    if (!e.Cancelled)
                    {
                        mainForm.corSchMedianDepth();
                        mainForm.corSchUpdateWindow(false);
                    }
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.ADD_WELL_TO_PROFILE:
                    this.EndWork = true;
                    if ((canv.selWellList.Count > 0) && (this.WorkObj.ObjectIndex > -1) && (this.WorkObj.ObjectIndex == canv.selWellList.Count - 1))
                    {
                        canv.selWellList.Clear();
                        mainForm.corSchSaveProfileLayer();
                    }

                    mainForm.corSchUpdateWindow(false);
                    canv.DrawLayerList();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CREATE_WELLLIST_BY_FILTER:
                    this.EndWork = true;
                    if (this.WorkObj.DataObject != null)
                    {
                        WellDataFilterForm filterForm = (WellDataFilterForm)this.WorkObj.DataObject;
                        if (filterForm.wellList != null)
                        {
                            canv.AddWellLayer(filterForm.wellList);
                            mainForm.ShowWellListPane();
                            TreeNode tn = canv.twWork.Nodes[0].Nodes[canv.twWork.Nodes[0].Nodes.Count - 1];
                            if (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                            {
                                if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                                {
                                    mainForm.UpdateWellListSettings((C2DLayer)tn.Tag);
                                    canv.twWork.SetOneSelectedNode(tn);
                                    canv.twWork.SelectNodeByType(Constant.BASE_OBJ_TYPES_ID.PROFILE, tn);
                                }
                            }
                            string str = String.Format("Создан список скважин по фильтру.\nСкважин в списке: {0}.", filterForm.wellList.Length);
                            ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Информация", str);
                        }
                        else
                        {
                            if (e.Result == null)
                            {
                                string str = "Не найдена ни одна скважина по заданному фильтру!\nСписок скважин не создан.";
                                ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Информация", str);
                            }
                            else
                            {
                                string str = (string)e.Result;
                                ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.ERROR, "Ошибка!", str);
                            }
                        }
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.FREE_OTHER_OF_GRIDS:
                    this.EndWork = true;
                    GC.Collect(GC.MaxGeneration);
                    GC.WaitForPendingFinalizers();
                    Counter++;
                    this.stBarLabel.Text = "";
                    this.Run();
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_FILE:
                    if (e.Result != null)
                    {
                        layer = (C2DLayer)e.Result;
                        TreeNode node = canv.twWork.Nodes[0].Nodes.Add("", layer.Name, 17, 17);
                        node.Tag = layer;
                        node.Checked = layer.Visible;
                        layer.node = node;
                        node.ContextMenuStrip = canv.twWork.cMenu;
                        canv.twWork.SetOneSelectedNode(node);
                        canv.WorkLayerActive = layer;
                        mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER);
                        canv.LayerWorkList.Add(layer);
                        canv.DrawLayerList();
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_CACHE:
                    if (this.WorkObjTemp.ObjectIndex == -2)
                    {
                        if ((this.WorkObjTemp.DataObject != null) && (canv.LoadGridNode != null))
                        {
                            Grid grid = (Grid)this.WorkObjTemp.DataObject;
                            if (canv.LoadGridMode == 1)
                            {
                                for (int i = grid.Index; i < canv.LoadGridNode.Nodes.Count; i++)
                                {
                                    canv.LoadGridNode.Nodes[i].Checked = false;
                                    if (canv.LoadGridNode.Nodes[i].Tag != null)
                                    {
                                        ((C2DObject)canv.LoadGridNode.Nodes[i].Tag).Visible = false;
                                        if (canv.LoadGridNode.TreeView == canv.twActiveOilField)
                                        {
                                            ((C2DLayer)canv.LoadGridNode.Nodes[i].Tag).node.Checked = false;
                                        }
                                    }
                                }
                            }
                            else if (canv.LoadGridMode == 0)
                            {
                                canv.LoadGridNode.Checked = false;
                                if (canv.LoadGridNode.Tag != null)
                                {
                                    ((C2DObject)canv.LoadGridNode.Tag).Visible = false;
                                    if (canv.LoadGridNode.TreeView == canv.twActiveOilField)
                                    {
                                        ((C2DLayer)canv.LoadGridNode.Tag).node.Checked = false;
                                    }
                                }
                            }
                        }
                        if ((canv.twActiveOilField.ActiveOilFieldIndex != -1) && (Counter == 0))
                        {
                            if (MessageBox.Show("Недостаточно памяти для отображения сетки!\nПопробовать отключить сетки на других месторождениях?",
                                "Ошибка!", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                            {
                                WorkObject job;
                                job.WorkType = Constant.BG_WORKER_WORK_TYPE.FREE_OTHER_OF_GRIDS;
                                job.DataObject = canv.MapProject;
                                job.ObjectIndex = canv.twActiveOilField.ActiveOilFieldIndex;
                                this.Queue.Add(job);
                                this.Queue.Add(this.WorkObj);
                                this.Run();
                                this.EndWork = false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Недостаточно памяти для отображения сетки!\nПопробуйте отключить сетки на других месторождениях.",
                                "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.EndWork = true;
                        }
                    }
                    else if (this.WorkObjTemp.ObjectIndex == 0)
                    {
                        if (Counter > 0)
                        {
                            canv.LoadGridNode.Checked = true;
                            if (canv.LoadGridNode.Tag != null)
                            {
                                ((C2DObject)canv.LoadGridNode.Tag).Visible = true;
                                ((C2DLayer)canv.LoadGridNode.Tag).GetObjectsRect();
                                if (canv.LoadGridNode.TreeView == canv.twActiveOilField)
                                {
                                    ((C2DLayer)canv.LoadGridNode.Tag).node.Checked = true;
                                }
                            }
                            
                        }
                        if (canv.LoadGridNode.Tag != null)
                        {
                            ((C2DLayer)canv.LoadGridNode.Tag).GetObjectsRect();

                            C2DLayer parentLayer = (C2DLayer)canv.LoadGridNode.Tag;
                            for (int k = 0; k < parentLayer.ObjectsList.Count; k++)
                            {
                                if (((C2DObject)parentLayer.ObjectsList[k]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                                {
                                    ((C2DLayer)parentLayer.ObjectsList[k]).GetObjectsRect();
                                }
                            }
                        }
                        Counter = 0;
                        this.EndWork = true;
                    }
                    if (this.EndWork)
                    {
                        canv.LoadGridNode = null;
                        canv.LoadGridMode = -1;
                        canv.DrawLayerList();
                        this.stBarLabel.Text = "";
                    }
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_PROJWELL_FROM_FILE:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    canv.Enable();
                    canv.LoadLayers();
                    canv.SortInspectorByNGDU();
                    canv.DrawLayerList();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CORE_FROM_FILE:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Загружены данные керна!";
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_CORE_TEST_FROM_FILE:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Загружены данные исследований керна!";
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.RESET_ICONS_BY_FILTER:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    canv.SetContoursVisibleByFilter();
                    canv.SetAreasVisibleByFilter();
                    canv.twActiveOilField.UpdateRecursiveTreeView();
                    if (this.Queue.Count == 0)
                    {
                        canv.Enable();
                        canv.DrawLayerList();
                    }
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_WELLS_TRAJECTORY:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    canv.Enable();
                    canv.DrawLayerList();
                    mainForm.SetWellTrajectoryButtonCheck();
                    this.stBarLabel.Text = string.Empty;
                    break;
                case Constant.BG_WORKER_WORK_TYPE.CALC_BUBBLE_MAP:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    canv.Enable();
                    canv.DrawLayerList();
                    this.stBarLabel.Text = string.Empty;
                    string s1 = string.Empty, s2 = string.Empty;
                    switch (canv.BubbleMapType)
                    {
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                            s1 = "Построена карта текущих отборов ";
                            s2 = "";
                            DateTime dt;

                            if (canv.BubbleDT2 == DateTime.MaxValue)
                                s2 = " на последнюю дату МЭР ";
                            else if (canv.BubbleLastMonth <= 1)
                                s2 = " на дату ";

                            if (canv.BubbleLastMonth > 1)
                            {
                                dt = canv.MapProject.BubbleCalcDT2.AddMonths(-canv.BubbleLastMonth);
                                if (dt < canv.MapProject.BubbleCalcDT1) dt = canv.MapProject.BubbleCalcDT1;
                                s2 = s2 + " среднее за интервал " + Environment.NewLine + dt.ToShortDateString() + "г. - " + canv.MapProject.BubbleCalcDT2.ToShortDateString() + "г.";
                            }
                            else
                            {
                                s2 = s2 + canv.MapProject.BubbleCalcDT2.ToShortDateString() + "г.";
                            }
                            break;
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                            s1 = "Построена карта накопленных отборов.";
                            s2 = "";
                            break;
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                            s1 = "Построена карта изменений отборов " + ((canv.BubbleHistoryByChess) ? "по данным шахматки." : "по данным МЭР.");
                            s2 = "";
                            break;
                    }

                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Информация", s1 + s2);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.UPDATE_BUBBLE_MAP:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    canv.Enable();
                    canv.DrawLayerList();
                    this.stBarLabel.Text = "";
                    s1 = ""; s2 = "";
                    
                    switch (canv.BubbleMapType)
                    {
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                            s1 = "Обновлена карта текущих отборов ";
                            s2 = "";
                            //DateTime dt;

                            //if (canv.BubbleDT2 == DateTime.MaxValue)
                            //    s2 = " на последнюю дату МЭР ";
                            //else if (canv.BubbleLastMonth <= 1)
                            //    s2 = " на дату ";

                            //if (canv.BubbleLastMonth > 1)
                            //{
                            //    dt = canv.MapProject.BubbleCalcDT2.AddMonths(-canv.BubbleLastMonth);
                            //    if (dt < canv.MapProject.BubbleCalcDT1) dt = canv.MapProject.BubbleCalcDT1;
                            //    s2 = s2 + " среднее за интервал " + Environment.NewLine + dt.ToShortDateString() + "г. - " + canv.MapProject.BubbleCalcDT2.ToShortDateString() + "г.";
                            //}
                            //else
                            //{
                            //    s2 = s2 + canv.MapProject.BubbleCalcDT2.ToShortDateString() + "г.";
                            //}
                            break;
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                            s1 = "Обновлена карта накопленных отборов.";
                            s2 = "";
                            break;
                        case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                            s1 = "Обновлена карта изменений отборов.";
                            s2 = "";
                            break;
                    }

                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Информация", s1 + s2);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_WELLLIST_PARAMS_CALC:
                    this.EndWork = false;
                    mainForm.filterOilObj.SetArrayListOilObj(canv.selMerOilObjCodes, false);
                    if (canv.selWellList.Count > 0)
                    {
                        List<int> list = new List<int>();
                        Well w;
                        for (int i = 0; i < canv.selWellList.Count; i++)
                        {
                            if(canv.IsGraphicsByOneDate) w = ((PhantomWell)canv.selWellList[i]).srcWell;
                            else w = (Well)canv.selWellList[i];
                            if (list.IndexOf(w.OilFieldIndex) == -1) list.Add(w.OilFieldIndex);
                        }
                        mainForm.filterOilObj.SetArrayListOilFieldOilObj(list, false);
                    }
                    this.Run(Constant.BG_WORKER_WORK_TYPE.SUM_WELLLIST_TABLE_RECALC, this.WorkObj.DataObject);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_AREA_PARAMS_CALC:
                    this.EndWork = false;
                    this.Run(Constant.BG_WORKER_WORK_TYPE.SUM_AREA_TABLE_RECALC, (Area)((OilField)this.WorkObj.DataObject).Areas[this.WorkObj.ObjectIndex]);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_OF_PARAMS_CALC:
                    this.EndWork = false;
                    this.Run(Constant.BG_WORKER_WORK_TYPE.SUM_OF_TABLE_RECALC, this.WorkObj.DataObject);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_PARAMS_CALC:
                    this.EndWork = false;
                    this.Run(Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_TABLE_RECALC, this.WorkObj.DataObject, this.WorkObj.ObjectIndex);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_REG_PARAMS_CALC:
                    this.EndWork = false;
                    this.Run(Constant.BG_WORKER_WORK_TYPE.SUM_REG_TABLE_RECALC, this.WorkObj.DataObject, this.WorkObj.ObjectIndex);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_OILOBJ_PARAMS_CALC:
                    this.EndWork = false;
                    this.Run(Constant.BG_WORKER_WORK_TYPE.SUM_OILOBJ_TABLE_RECALC, this.WorkObj.DataObject, this.WorkObj.ObjectIndex);
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_WELLLIST_TABLE_RECALC:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    if (canv.UpdateFilterOilObj)
                    {
                        mainForm.filterOilObj.SetArrayListOilObj(canv.selMerOilObjCodes, false);
                        mainForm.filterOilObj.UpdateCurrentTreeView();
                    }
                    mainForm.dChartSetSourceObj(new BaseObj(Constant.BASE_OBJ_TYPES_ID.WELL_LIST));
                    mainForm.dChartSumParamsShow();
                    canv.Enable();
                    canv.DrawLayerList();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_AREA_TABLE_RECALC:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    bool clear = true;
                    res = mainForm.dChartSetSourceObj((Area)this.WorkObj.DataObject);
                    mainForm.dChartSumParamsShow();
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.LOAD_AREAS_PARAMS_FROM_CACHE:
                    this.EndWork = true;
                    pb.Visible = false;
                    progress.Clear();
                    mainForm.dAreasSetDateComment();
                    string cacheName = ((Project)this.WorkObj.DataObject).path + "\\cache\\areas_params.bin";
                    if ((!File.Exists(cacheName)) || (mainForm.dAreasTableCount() == 0))
                    {
                        MessageBox.Show("Не найден кеш расчетных параметров ячеек заводнения.\nЗапустите расчет параметров ячеек заводнения.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    mainForm.dAreaSetDataSet();
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.FILL_AREAS_PARAMS_TO_TABLE:
                    this.EndWork = true;
                    this.mainForm.dAreasSetDateComment();
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    mainForm.dAreaSetDataSet();
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.FILL_AREAS_PARAMS_TO_TABLE_BY_DATE:
                    this.EndWork = true;
                    this.mainForm.dAreasSetDateComment();
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    Application.DoEvents();
                    mainForm.dAreaSetDataSet();
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_OF_TABLE_RECALC:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    
                    clear = true;
                    for (int i = 0; i < canv.sumIndexes.Count; i++)
                    {
                        mainForm.filterOilObj.SetOilFieldOilObj((OilField)canv.sumIndexes[i], clear);
                        if (clear) clear = false;
                    }
                    mainForm.filterOilObj.UpdateCurrentTreeView();
                    res = mainForm.dChartSetSourceObj((OilField)this.WorkObj.DataObject);
                    mainForm.dChartSumParamsShow();
                    canv.Enable();
                    canv.DrawLayerList();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_TABLE_RECALC:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    clear = true;
                    for (int i = 0; i < canv.sumIndexes.Count; i++)
                    {
                        mainForm.filterOilObj.SetOilFieldOilObj((OilField)canv.sumIndexes[i], clear);
                        if (clear) clear = false;
                    }
                    mainForm.filterOilObj.UpdateCurrentTreeView();
                    res = mainForm.dChartSetSourceObj((Project)this.WorkObj.DataObject);
                    mainForm.dChartSumParamsShow();
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_REG_TABLE_RECALC:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    clear = true;
                    for (int i = 0; i < canv.sumIndexes.Count; i++)
                    {
                        mainForm.filterOilObj.SetOilFieldOilObj((OilField)canv.sumIndexes[i], clear);
                        if (clear) clear = false;
                    }
                    mainForm.filterOilObj.UpdateCurrentTreeView();
                    res = mainForm.dChartSetSourceObj((Project)this.WorkObj.DataObject);
                    mainForm.dChartSumParamsShow();
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
                case Constant.BG_WORKER_WORK_TYPE.SUM_OILOBJ_TABLE_RECALC:
                    this.EndWork = true;
                    this.stBarLabel.Text = "Отрисовка проекта.Пожалуйста подождите..";
                    res = mainForm.dChartSetSourceObj((Project)this.WorkObj.DataObject);
                    mainForm.dChartSumParamsShow();
                    canv.Enable();
                    this.stBarLabel.Text = "";
                    break;
            }
        }
    }
}
