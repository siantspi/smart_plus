﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public sealed class C2DLayer : C2DObject
    {
        public struct VisibleLevel
        {
            public bool NGDU;
            public bool OilField;
            public bool Area;
            public bool Skv;
            public void SetVisibleLvl(int Level)
            {
                switch (Level)
                {
                    case 0:
                        NGDU = false;
                        OilField = false;
                        Area = false;
                        Skv = true;
                        break;
                    case 1:
                        NGDU = false;
                        OilField = false;
                        Area = true;
                        Skv = true;
                        break;
                    case 2:
                        NGDU = false;
                        OilField = true;
                        Area = true;
                        Skv = true;
                        break;
                    case 3:
                        NGDU = true;
                        OilField = true;
                        Area = true;
                        Skv = true;
                        break;
                }
            }
            public int GetVisibleLvl()
            {
                if (NGDU)
                {
                    return 3;
                }
                else
                {
                    if (OilField)
                    {
                        return 2;
                    }
                    else
                    {
                        if (Area)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }
        }

        public Constant.BASE_OBJ_TYPES_ID ObjTypeID;
        public TreeNode node;
        public OilField oilfield;
        public int oilFieldIndex;
        public int ngduCode;
        public int Level;
        public bool DrawingMainLine;
        public bool VisibleFar;
        public bool VisibleName;
        public bool VisibleCaption;
        public bool TransferRegim;
        public RectangleD ObjRect;
        private PointD ObjMedian;
        public bool RotateMode;
        public OilfieldCoefItem RotateCoef;
        public bool DrawWellHead;
        public bool DrawCaption
        {
            get
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    if (ObjectsList.Count > 0)
                    {
                        return ((PhantomWell)ObjectsList[0]).CaptionVisible;
                    }
                }
                return false;
            }
            set
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((PhantomWell)ObjectsList[i]).CaptionVisible = value;
                    }
                }
            }
        }
        public bool DrawOverBubble;
        float _fontSize;

        public Font font;
        ExFontList ExIconFonts;

        StringFormat strFrmt;
        string[] problemWellList = null;
        public VisibleLevel VisibleLvl;

        new public bool Visible
        {
            get { return base.Visible; }
            set
            {
                base.Visible = value;
                if (ObjectsList.Count == 1)
                {
                    if (((BaseObj)ObjectsList[0]).TypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                    {
                        ((Marker)ObjectsList[0]).Visible = value;
                    }
                }
            }
        }
        new public bool Selected
        {
            get { return base.Selected; }
            set
            {
                base.Selected = value;
                if (this.Visible)
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                        {
                            ((C2DLayer)ObjectsList[i]).Selected = value;
                        }
                        else if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.DEPOSIT)
                        {
                            if(!value) ((Deposit)ObjectsList[i]).Selected = false;
                        }
                        else if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                        {
                            if (((Contour)ObjectsList[i]).Enable) ((Contour)ObjectsList[i]).Selected = value;
                        }
                        else if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
                        {
                            ((Area)ObjectsList[i]).Selected = value;
                            ((Area)ObjectsList[i]).contour.Selected = value;
                        }
                        else
                        {
                            ((C2DObject)ObjectsList[i]).Selected = value;
                            if ((this.Level > -1) && (this.oilfield != null) && (this.Level < this.oilfield.Areas.Count))
                            {
                                ((Area)oilfield.Areas[this.Level]).Selected = value;
                            }
                        }
                    }
                }
            }
        }
        public bool IsLayerSelected
        {
            get
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    bool res = false;
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        res = ((C2DLayer)ObjectsList[i]).IsLayerSelected;
                        if (res) return res;
                    }
                }
                else
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        if (((C2DObject)ObjectsList[i]).Selected) return ((C2DObject)ObjectsList[i]).Selected;
                    }
                }
                return false;
            }
        }

        public List<C2DObject> ObjectsList;
        public float fontSize
        {
            get { return _fontSize; }
            set
            {
                if ((value > 1) && (_fontSize != value))
                {
                    if (font != null) font.Dispose();
                    switch (ObjTypeID)
                    {
                        case Constant.BASE_OBJ_TYPES_ID.WELL:
                            font = new Font("Calibri", value);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                            font = new Font("Calibri", value);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                            font = new Font("Calibri", value);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                            font = new Font("Calibri", value, FontStyle.Bold);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION:
                            font = new Font("Calibri", value, FontStyle.Bold);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.TUBE:
                            font = new Font("Calibri", value, FontStyle.Bold);
                            break;
                        default:
                            font = new Font("Calibri", value);
                            break;
                    }
                }
                if (value <= 1) font = null;
                _fontSize = value;
            }
        }

        public C2DLayer(Constant.BASE_OBJ_TYPES_ID TypeDataID, int OilFieldIndex)
        {
            this.VisibleLvl.SetVisibleLvl(0);
            ObjectsList = new List<C2DObject>();
            TypeID = Constant.BASE_OBJ_TYPES_ID.LAYER;
            ObjTypeID = TypeDataID;
            oilFieldIndex = OilFieldIndex;
            ngduCode = -1;
            TransferRegim = false;
            node = null;
            ExIconFonts = null;
            ObjRect = new RectangleD();
            strFrmt = new StringFormat();
            strFrmt.Alignment = StringAlignment.Center;
            strFrmt.LineAlignment = StringAlignment.Center;
            Selected = false;
            RotateMode = false;
            RotateCoef = null;
            DrawWellHead = false;
            DrawOverBubble = false;
            VisibleName = false;
            VisibleCaption = false;
            ObjMedian.X = Constant.DOUBLE_NAN;
            ObjMedian.Y = Constant.DOUBLE_NAN;
            font = null;

            Level = 0;
            Visible = true;
            VisibleFar = true;
        }
        public C2DLayer Clone()
        {
            C2DLayer clone = null;
            if ((this.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.GRID) ||
                ((this.ObjectsList.Count > 0) && (((Grid)this.ObjectsList[0])).DataLoaded))
            {
                Contour cntr;
                Grid grid;
                C2DLayer lr;
                clone = new C2DLayer(this.TypeID, 0);
                clone.VisibleLvl.SetVisibleLvl(0);
                clone.ObjectsList = new List<C2DObject>(this.ObjectsList.Count);
                if ((this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL_LIST) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
                {
                    for (int i = 0; i < this.ObjectsList.Count; i++)
                    {
                        switch (this.ObjTypeID)
                        {
                            case Constant.BASE_OBJ_TYPES_ID.AREA:
                                cntr = new Contour(((Area)this.ObjectsList[i]).contour);
                                cntr._FillBrush = -1;
                                clone.ObjectsList.Add(cntr);
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                                cntr = new Contour((Contour)this.ObjectsList[i]);
                                cntr._FillBrush = -1;
                                clone.ObjectsList.Add(cntr);
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                                cntr = new Contour((Contour)this.ObjectsList[i]);
                                cntr._FillBrush = -1;
                                clone.ObjectsList.Add(cntr);
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.GRID:
                                grid = new Grid((Grid)this.ObjectsList[i]);
                                clone.ObjectsList.Add(grid);
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.DATAFILE:
                                var file = new DataFile((DataFile)this.ObjectsList[i]);
                                clone.ObjectsList.Add(file);
                                break;
                        }
                    }
                }
                else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    for (int i = 0; i < this.ObjectsList.Count; i++)
                    {
                        if ((((C2DLayer)this.ObjectsList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA) ||
                            (((C2DLayer)this.ObjectsList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) ||
                            (((C2DLayer)this.ObjectsList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) ||
                            (((C2DLayer)this.ObjectsList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE) ||
                            (((C2DLayer)this.ObjectsList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                        {
                            lr = ((C2DLayer)this.ObjectsList[i]).Clone();
                            if (lr != null) clone.ObjectsList.Add(lr);
                        }
                    }
                }

                clone.Name = this.Name.Replace("\"", "");
                clone.TypeID = Constant.BASE_OBJ_TYPES_ID.LAYER;
                clone.ObjTypeID = this.ObjTypeID;
                clone.oilFieldIndex = this.oilFieldIndex;
                clone.ngduCode = -1;
                clone.TransferRegim = false;
                clone.VisibleName = false;
                if (this.node != null) clone.node = (TreeNode)this.node.Clone();

                clone.ObjRect = new RectangleD(this.ObjRect);

                clone.strFrmt = new StringFormat(this.strFrmt);
                clone.strFrmt.Alignment = StringAlignment.Center;
                clone.strFrmt.LineAlignment = StringAlignment.Center;
                clone.Selected = false;
                clone.RotateMode = false;
                clone.RotateCoef = null;
                clone.DrawWellHead = false;
                clone.ObjMedian.X = this.ObjMedian.X;
                clone.ObjMedian.Y = this.ObjMedian.Y;
                clone.font = null;
                clone.ExIconFonts = null;

                clone.Level = 0;
                clone.Visible = true;
                clone.VisibleFar = true;
            }
            return clone;
        }
        public void GetClone(C2DLayer layer)
        {
            if (this.ObjectsList.Count > 0)
            {
                Contour cntr;
                Grid grid;
                C2DLayer lr;

                this.VisibleLvl.SetVisibleLvl(layer.VisibleLvl.GetVisibleLvl());
                this.ObjectsList = new List<C2DObject>(layer.ObjectsList.Count);
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    this.ObjectsList.Add(layer.ObjectsList[i]);
                }

                this.Name = layer.Name;
                this.TypeID = layer.TypeID;
                this.ObjTypeID = layer.ObjTypeID;
                this.oilFieldIndex = layer.oilFieldIndex;
                this.ngduCode = layer.ngduCode;
                this.TransferRegim = layer.TransferRegim;
                this.VisibleName = layer.VisibleName;

                this.ObjRect = new RectangleD(layer.ObjRect);

                this.strFrmt = new StringFormat(layer.strFrmt);
                this.strFrmt.Alignment = layer.strFrmt.Alignment;
                this.strFrmt.LineAlignment = layer.strFrmt.LineAlignment;
                this.Selected = layer.Selected;
                this.RotateMode = layer.RotateMode;
                this.RotateCoef = layer.RotateCoef;
                this.DrawWellHead = layer.DrawWellHead;
                this.ObjMedian.X = layer.ObjMedian.X;
                this.ObjMedian.Y = layer.ObjMedian.Y;

                this.Level = 0;
                this.Visible = layer.Visible;
                this.VisibleFar = layer.VisibleFar;
            }
        }
        public void Add(C2DObject obj) { ObjectsList.Add(obj); }
        public void AddByIndex(int Index, C2DObject obj)
        {
            if (Index < ObjectsList.Count)
            {
                if (ObjectsList[Index] == null)
                {
                    ObjectsList[Index] = obj;
                }
                else
                {
                    ObjectsList.Add(obj);
                }
            }
            else
            {
                while (ObjectsList.Count != Index)
                {
                    ObjectsList.Add(null);
                }
                ObjectsList.Add(obj);
            }
        }
        public void Insert(int Index, C2DObject obj)
        {
            if (Index < ObjectsList.Count)
            {
                ObjectsList.Insert(Index, obj);
            }
        }

        public int GetObjectsCount()
        {
            int sum = 1;
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    sum += ((C2DLayer)ObjectsList[i]).GetObjectsCount();
                }
            }
            return sum;
        }

        public void Clear()
        {
            if ((ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE) && (ObjectsList.Count > 0))
            {
                ((Image)ObjectsList[0]).Dispose();
            }
            else ObjectsList.Clear();
            RotateCoef = null;
            RotateMode = false;
        }
        public C2DObject SelectWellbyXY(double aX, double aY, out double D)
        {
            if ((this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) && (this.Visible))
            {
                C2DObject obj, outObj = null;
                double currD, maxD = Constant.DOUBLE_NAN;
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    obj = ((C2DLayer)ObjectsList[i]).SelectWellbyXY(aX, aY, out currD);
                    if (obj != null)
                    {
                        if ((maxD == Constant.DOUBLE_NAN) || (currD < maxD))
                        {
                            maxD = currD;
                            outObj = obj;
                        }
                    }
                }
                D = maxD;
                return outObj;
            }
            else if ((this.Visible) &&
                     ((this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL) ||
                      (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) ||
                      (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)))
            {
                double rX, rY;
                D = 0;
                C2DObject w = SelectWellbyXY(aX, aY);
                rX = RealXfromScreenX(aX);
                rY = RealYfromScreenY(aY);
                if (w != null) D = Math.Sqrt(Math.Pow(w.X - rX, 2) + Math.Pow(w.Y - rY, 2));
                return w;
            }
            D = Constant.DOUBLE_NAN;
            return null;
        }
        public C2DObject SelectWellbyXY(double aX, double aY)
        {
            int i, j = -1;
            double rX, rY, dx, mindx = -1;
            C2DObject w = null, wx;
            if ((this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL) ||
                (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) ||
                (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL))
            {
                for (i = 0; i < ObjectsList.Count; i++)
                {
                    wx = (C2DObject)ObjectsList[i];
                    wx.Selected = false;
                    rX = RealXfromScreenX(aX);
                    rY = RealYfromScreenY(aY);
                    dx = Math.Sqrt(Math.Pow(wx.X - rX, 2) + Math.Pow(wx.Y - rY, 2));
                    if (mindx == -1) mindx = dx;
                    if (dx <= mindx)
                    {
                        j = i;
                        mindx = dx;
                    }
                }
                if (j != -1) w = (C2DObject)ObjectsList[j];
            }
            return w;
        }
        public C2DLayer SelectContourByXY(double X, double Y, out double D)
        {
            C2DLayer layer = null, tempLayer;
            Contour tempCntr;
            int len;
            double rX, rY, x0 = Constant.DOUBLE_NAN, y0 = Constant.DOUBLE_NAN, x1, y1, x2, y2, xs, ys;
            double k, b, b2;
            double scrX1, scrX2, scrY1, scrY2;

            double currD = Constant.DOUBLE_NAN;
            rX = RealXfromScreenX(X);
            rY = RealYfromScreenY(Y);
            D = Constant.DOUBLE_NAN;

            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                double currCntrD;
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    tempLayer = ((C2DLayer)ObjectsList[i]).SelectContourByXY(X, Y, out currCntrD);
                    if ((tempLayer != null) && (currCntrD < 20) && ((D == Constant.DOUBLE_NAN) || (D >= currCntrD)))
                    {
                        D = currCntrD;
                        layer = tempLayer;
                    }
                }

            }
            else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
            {
                for (int i = 0; i < this.ObjectsList.Count; i++)
                {
                    tempCntr = (Contour)this.ObjectsList[i];
                    if (!tempCntr.Visible) continue;
                    len = tempCntr._points.Count;
                    for (int j = 0; j < len; j++)
                    {
                        x0 = Constant.DOUBLE_NAN;
                        x1 = tempCntr._points[j].X;
                        y1 = tempCntr._points[j].Y;
                        if (j < len - 1)
                        {
                            x2 = tempCntr._points[j + 1].X;
                            y2 = tempCntr._points[j + 1].Y;
                        }
                        else if (tempCntr._Closed)
                        {
                            x2 = tempCntr._points[0].X;
                            y2 = tempCntr._points[0].Y;
                        }
                        else
                            continue;

                        scrX1 = ScreenXfromRealX(x1);
                        scrX2 = ScreenXfromRealX(x2);
                        scrY1 = ScreenYfromRealY(y1);
                        scrY2 = ScreenYfromRealY(y2);
                        if (((scrX1 < 0) && (scrX2 < 0)) && ((scrY1 < 0) || (scrY2 < 0)))
                        {
                            x0 = Constant.DOUBLE_NAN;
                        }
                        else if ((Math.Abs(scrX1 - scrX2) > 2) && (Math.Abs(scrY1 - scrY2) > 2))
                        {
                            k = (y2 - y1) / (x2 - x1);
                            b = y1 - x1 * (y2 - y1) / (x2 - x1);
                            b2 = rY + rX / k;
                            x0 = (b2 - b) * k / (k * k + 1);
                            y0 = k * x0 + b;
                            x0 = ScreenXfromRealX(x0);
                            y0 = ScreenYfromRealY(y0);

                            if (!((((x0 >= scrX1) && (x0 < scrX2)) || ((x0 >= scrX2) && (x0 < scrX1))) && (((y0 >= scrY1) && (y0 < scrY2)) || ((y0 >= scrY2) && (y0 < scrY1)))))
                                x0 = Constant.DOUBLE_NAN;
                        }
                        else if (Math.Abs(scrY1 - scrY2) < 3)
                        {
                            x0 = ScreenXfromRealX(rX);
                            y0 = ScreenYfromRealY(y2);
                            if (!(((x0 >= scrX1) && (x0 < scrX2)) || ((x0 >= scrX2) && (x0 < scrX1)))) x0 = Constant.DOUBLE_NAN;

                        }
                        else if (Math.Abs(scrX1 - scrX2) < 3)
                        {
                            x0 = ScreenXfromRealX(x1);
                            y0 = ScreenYfromRealY(rY);
                            if (!(((y0 >= scrY1) && (y0 < scrY2)) || ((y0 >= scrY2) && (y0 < scrY1)))) x0 = Constant.DOUBLE_NAN;

                        }
                        if (x0 != Constant.DOUBLE_NAN) currD = Math.Sqrt((x0 - X) * (x0 - X) + (y0 - Y) * (y0 - Y));

                        if ((D == Constant.DOUBLE_NAN) || (D > currD))
                        {
                            D = currD;
                            layer = this;
                        }
                    }
                }
            }
            return layer;
        }
        public C2DLayer SelectProfileByXY(double X, double Y, out double D)
        {
            C2DLayer layer = null, tempLayer;
            Profile tempPrfl;

            double rX, rY, x0 = Constant.DOUBLE_NAN, y0 = Constant.DOUBLE_NAN, x1, y1, x2, y2, xs, ys;
            double k, b, b2;
            double scrX1, scrX2, scrY1, scrY2;

            double currD = Constant.DOUBLE_NAN;
            rX = RealXfromScreenX(X);
            rY = RealYfromScreenY(Y);
            D = Constant.DOUBLE_NAN;

            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                double currCntrD;
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    tempLayer = ((C2DLayer)ObjectsList[i]).SelectProfileByXY(X, Y, out currCntrD);
                    if ((tempLayer != null) && (currCntrD < 20) && ((D == Constant.DOUBLE_NAN) || (D >= currCntrD)))
                    {
                        D = currCntrD;
                        layer = tempLayer;
                    }
                }

            }
            else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE)
            {
                for (int i = 0; i < this.ObjectsList.Count; i++)
                {
                    tempPrfl = (Profile)this.ObjectsList[i];
                    if (!tempPrfl.Visible) continue;
                    for (int j = 0; j < tempPrfl.Count - 1; j++)
                    {
                        x0 = Constant.DOUBLE_NAN;
                        x1 = tempPrfl[j].X;
                        y1 = tempPrfl[j].Y;
                        x2 = tempPrfl[j + 1].X;
                        y2 = tempPrfl[j + 1].Y;

                        scrX1 = ScreenXfromRealX(x1);
                        scrX2 = ScreenXfromRealX(x2);
                        scrY1 = ScreenYfromRealY(y1);
                        scrY2 = ScreenYfromRealY(y2);
                        if (((scrX1 < 0) && (scrX2 < 0)) && ((scrY1 < 0) || (scrY2 < 0)))
                        {
                            x0 = Constant.DOUBLE_NAN;
                        }
                        else if ((Math.Abs(scrX1 - scrX2) > 2) && (Math.Abs(scrY1 - scrY2) > 2))
                        {
                            k = (y2 - y1) / (x2 - x1);
                            b = y1 - x1 * (y2 - y1) / (x2 - x1);
                            b2 = rY + rX / k;
                            x0 = (b2 - b) * k / (k * k + 1);
                            y0 = k * x0 + b;
                            x0 = ScreenXfromRealX(x0);
                            y0 = ScreenYfromRealY(y0);

                            if (!((((x0 >= scrX1) && (x0 < scrX2)) || ((x0 >= scrX2) && (x0 < scrX1))) && (((y0 >= scrY1) && (y0 < scrY2)) || ((y0 >= scrY2) && (y0 < scrY1)))))
                                x0 = Constant.DOUBLE_NAN;
                        }
                        else if (Math.Abs(scrY1 - scrY2) < 3)
                        {
                            x0 = ScreenXfromRealX(rX);
                            y0 = ScreenYfromRealY(y2);
                            if (!(((x0 >= scrX1) && (x0 < scrX2)) || ((x0 >= scrX2) && (x0 < scrX1)))) x0 = Constant.DOUBLE_NAN;

                        }
                        else if (Math.Abs(scrX1 - scrX2) < 3)
                        {
                            x0 = ScreenXfromRealX(x1);
                            y0 = ScreenYfromRealY(rY);
                            if (!(((y0 >= scrY1) && (y0 < scrY2)) || ((y0 >= scrY2) && (y0 < scrY1)))) x0 = Constant.DOUBLE_NAN;

                        }
                        if (x0 != Constant.DOUBLE_NAN) currD = Math.Sqrt((x0 - X) * (x0 - X) + (y0 - Y) * (y0 - Y));

                        if ((D == Constant.DOUBLE_NAN) || (D > currD))
                        {
                            D = currD;
                            layer = this;
                        }
                    }
                }
            }
            return layer;
        }
        public C2DLayer SelectGridByXY(double scrX, double scrY)
        {
            C2DLayer layer = null;
            if (!this.Visible) return null;
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    layer = ((C2DLayer)ObjectsList[i]).SelectGridByXY(scrX, scrY);
                }
            }
            else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
            {
                Grid grid;
                for (int i = 0; i < this.ObjectsList.Count; i++)
                {
                    grid = (Grid)this.ObjectsList[i];
                    double x = C2DObject.RealXfromScreenX(scrX);
                    double y = C2DObject.RealYfromScreenY(scrY);
                    if ((grid.DataLoaded) && (!grid.DataLoading))
                    {
                        int row = (int)((grid.Ymax - (grid.dy / 2) - y) / grid.dy);
                        int col = (int)((x - grid.Xmin + grid.dx / 2) / grid.dx);
                        double Zval = grid.GetZvalue(row, col);
                        if (Zval != grid.BlankVal)
                        {
                            return this;
                        }
                    }
                }
            }
            return layer;
        }
        public C2DLayer SelectVoronoiMapByXY(int scrX, int scrY, bool Shift, bool MouseRight)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
            {
                VoronoiMap map = (VoronoiMap)this.ObjectsList[0];
                map.SelectCell(scrX, scrY, Shift, MouseRight);
                if (map.Selected) return this;
            }
            return null;
        }
        public C2DLayer TestDepositByXY(int scrX, int scrY, bool Shift, bool MouseRight)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DEPOSIT)
            {
                double rX, rY;
                rX = RealXfromScreenX(scrX);
                rY = RealYfromScreenY(scrY);
                if (!Shift)
                {
                    for (int i = 0; i < this.ObjectsList.Count; i++)
                    {
                        ((Deposit)this.ObjectsList[i]).Selected = false;
                    }
                }
                for (int i = 0; i < this.ObjectsList.Count; i++)
                {
                    if (((Deposit)this.ObjectsList[i]).contour.PointBodyEntry(rX, rY)) return this;
                }
            }
            return null;
        }
        public void SelectDepositByXY(int scrX, int scrY, bool Shift, bool MouseRight)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DEPOSIT)
            {
                double rX, rY;
                rX = RealXfromScreenX(scrX);
                rY = RealYfromScreenY(scrY);
                for (int i = 0; i < this.ObjectsList.Count; i++)
                {
                    var deposit = (Deposit)this.ObjectsList[i];
                    deposit.Selected = deposit.contour.PointBodyEntry(rX, rY);
                    if (deposit.Selected) return;
                }
            }
        }

        public void FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID ObjTypeID, List<C2DLayer> list)
        {
            FillLayersByObjTypeID(ObjTypeID, list, false);
        }
        public void FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID ObjTypeID, List<C2DLayer> list, bool CheckVisible)
        {
            if (!CheckVisible || this.Visible)
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DLayer)ObjectsList[i]).FillLayersByObjTypeID(ObjTypeID, list, CheckVisible);
                    }
                }
                else if (this.ObjTypeID == ObjTypeID)
                {
                    list.Add(this);
                }
            }
        }
        public void FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID[] ObjTypeIDList, List<C2DLayer> list, bool CheckVisible)
        {
            if (!CheckVisible || this.Visible)
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DLayer)ObjectsList[i]).FillLayersByObjTypeID(ObjTypeIDList, list, CheckVisible);
                    }
                }
                else
                {
                    for (int i = 0; i < ObjTypeIDList.Length; i++)
                    {
                        if (ObjTypeIDList[i] == this.ObjTypeID)
                        {
                            list.Add(this);
                            break;
                        }
                    }
                }
            }
        }
        public void FillLayerByPhantomWells(List<C2DLayer> list, bool CheckVisible, bool DrawOverBubble)
        {
            if (!CheckVisible || this.Visible)
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DLayer)ObjectsList[i]).FillLayerByPhantomWells(list, CheckVisible, DrawOverBubble);
                    }
                }
                else if ((this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL) && (this.DrawOverBubble == DrawOverBubble))
                {
                    list.Add(this);
                }
            }
        }
        public void FillLayerByWells(List<C2DLayer> list, bool CheckVisible, bool BubbleList)
        {
            if (!CheckVisible || this.Visible)
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DLayer)ObjectsList[i]).FillLayerByWells(list, CheckVisible, BubbleList);
                    }
                }
                else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                {
                    if (!BubbleList || (this.oilfield != null && this.oilfield.BubbleMapLoaded))
                    {
                        list.Add(this);
                    }
                }
            }
        }

        public int GetIndexByWell(int OilFieldIndex, int WellIndex)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
            {
                for (int i = 0; i < ObjectsList.Count; i++)
                    if ((((Well)ObjectsList[i]).Index == WellIndex) && (((Well)ObjectsList[i]).OilFieldIndex == OilFieldIndex))
                        return i;
            }
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
            {
                for (int i = 0; i < ObjectsList.Count; i++)
                    if ((((PhantomWell)ObjectsList[i]).srcWell.Index == WellIndex) &&
                        (((PhantomWell)ObjectsList[i]).srcWell.OilFieldIndex == OilFieldIndex))
                        return i;
            }
            return -1;
        }
        public C2DLayer GetCondContourByXY(int scrX, int scrY)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                C2DLayer layer;
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    layer = ((C2DLayer)ObjectsList[i]).GetCondContourByXY(scrX, scrY);
                    if (layer != null) return layer;
                }
            }
            else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
            {
                if (this.Visible)
                {
                    Contour cntr = (Contour)this.ObjectsList[0];
                    if (((cntr._ContourType == -1) && (st_VisbleLevel > 2000)) ||
                        ((cntr._ContourType == -2) && (st_VisbleLevel > st_NgduXUnits) && (st_VisbleLevel <= 2000)) ||
                        ((cntr._ContourType == -3) && (st_VisbleLevel > 0) && (st_VisbleLevel <= st_NgduXUnits)))
                    {
                        double X = RealXfromScreenX(scrX);
                        double Y = RealYfromScreenY(scrY);
                        if ((cntr.PointBoundsEntry(X, Y)) && (cntr.PointBodyEntry(X, Y)))
                        {
                            if (cntr.cntrsOut != null)
                            {
                                for (int i = 0; i < cntr.cntrsOut.Count; i++)
                                {
                                    if ((cntr.cntrsOut[i].PointBoundsEntry(X, Y)) && (cntr.cntrsOut[i].PointBodyEntry(X, Y)))
                                    {
                                        return null;
                                    }
                                }
                            }
                            return this;
                        }
                    }
                }
            }
            return null;
        }
        public C2DObject GetAreaByXY(int scrX, int scrY)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                C2DObject c2d;
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    c2d = ((C2DLayer)ObjectsList[i]).GetAreaByXY(scrX, scrY);
                    if (c2d != null) return c2d;
                }
            }
            else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
            {
                if ((this.Visible) && (st_VisbleLevel <= st_OilfieldXUnits) && (st_VisbleLevel > st_AreaXUnits))
                {
                    Contour cntr;
                    double X, Y;
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        cntr = ((Area)this.ObjectsList[i]).contour;
                        X = RealXfromScreenX(scrX);
                        Y = RealYfromScreenY(scrY);
                        if ((cntr.PointBoundsEntry(X, Y)) && (cntr.PointBodyEntry(X, Y)))
                        {
                            return (Area)this.ObjectsList[i];
                        }
                    }
                }
            }
            return null;
        }
        public C2DLayer GetCondContourByOfIndex(int OilFieldIndex)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                C2DLayer layer;
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    layer = ((C2DLayer)ObjectsList[i]).GetCondContourByOfIndex(OilFieldIndex);
                    if (layer != null) return layer;
                }
            }
            else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
            {
                if ((this.Visible) && (this.oilFieldIndex == OilFieldIndex))
                {
                    return this;
                }
            }
            return null;
        }

        public C2DLayer GetLayerByOfIndex(int OilFieldIndex)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                if (this.oilFieldIndex == OilFieldIndex)
                {
                    return this;
                }
                else
                {
                    C2DLayer layer;
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        layer = ((C2DLayer)ObjectsList[i]).GetLayerByOfIndex(OilFieldIndex);
                        if (layer != null) return layer;
                    }
                }
            }
            return null;
        }

        public PointD GetPointByXY(double X, double Y, out double D)
        {
            Contour cntr;
            PointD ret, tempXY;
            double currD;
            int ind;
            ret.X = Constant.DOUBLE_NAN;
            ret.Y = Constant.DOUBLE_NAN;
            tempXY.X = Constant.DOUBLE_NAN;
            tempXY.Y = Constant.DOUBLE_NAN;

            D = Constant.DOUBLE_NAN;
            currD = Constant.DOUBLE_NAN;
            if (!this.Visible) return ret;

            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                for (int j = 0; j < this.ObjectsList.Count; j++)
                {
                    if (((C2DLayer)ObjectsList[j]).Visible)
                    {
                        tempXY = ((C2DLayer)ObjectsList[j]).GetPointByXY(X, Y, out currD);
                        if ((currD != Constant.DOUBLE_NAN) && (currD < D) && (tempXY.X != Constant.DOUBLE_NAN))
                        {
                            ret = tempXY;
                            D = currD;
                        }
                    }
                }
            }
            else if (!this.Selected &&
                (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR ||
                 this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                 this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA))
            {
                for (int j = 0; j < this.ObjectsList.Count; j++)
                {
                    if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
                        cntr = ((Area)this.ObjectsList[j]).contour;
                    else
                        cntr = (Contour)this.ObjectsList[j];

                    if (cntr.Visible)
                    {
                        ind = cntr.GetIndexPointByXY(X, Y, 30);
                        if (ind > -1)
                        {
                            tempXY = cntr._points[ind];
                            double scrX = ScreenXfromRealX(tempXY.X);
                            double scrY = ScreenYfromRealY(tempXY.Y);
                            currD = Math.Sqrt(Math.Pow(scrX - X, 2) + Math.Pow(scrY - Y, 2));
                        }
                    }
                }
            }
            else if (!this.Selected && (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL))
            {
                Well w;
                double scrX, scrY, d;

                for (int j = 0; j < this.ObjectsList.Count; j++)
                {
                    w = (Well)this.ObjectsList[j];
                    scrX = ScreenXfromRealX(w.X);
                    scrY = ScreenYfromRealY(w.Y);

                    if ((w.Visible) && (w.CoordLoaded))
                    {
                        currD = Math.Sqrt((X - scrX) * (X - scrX) + (Y - scrY) * (Y - scrY));

                        if (currD < 30)
                        {
                            tempXY.X = w.X;
                            tempXY.Y = w.Y;
                            break;
                        }
                    }
                }
            }
            if ((currD != Constant.DOUBLE_NAN) && (currD < D) && (tempXY.X != Constant.DOUBLE_NAN))
            {
                ret = tempXY;
                D = currD;
            }
            return ret;
        }

        public void SetFontsRecursive(Font[] fonts, ExFontList exFontList)
        {
            this.ExIconFonts = exFontList;
            for (int i = 0; i < ObjectsList.Count; i++)
            {
                if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    ((C2DLayer)ObjectsList[i]).SetFontsRecursive(fonts, exFontList);
                }
                else if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                {
                    ((Well)ObjectsList[i]).SetIconsFonts(fonts, exFontList);
                }
                else if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL)
                {
                    ((Well)ObjectsList[i]).SetIconsFonts(fonts, exFontList);
                }
                else if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    ((PhantomWell)ObjectsList[i]).SetIconsFonts(fonts, exFontList);
                }
                else if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                {
                    ((VoronoiMap)ObjectsList[i]).SetIconsFonts(fonts, exFontList);
                }
            }
        }
        public void SetContoursType(int ContourType)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
            {
                for (int i = 0; i < this.ObjectsList.Count; i++)
                {
                    Contour cntr = (Contour)ObjectsList[i];
                    cntr._ContourType = ContourType;
                }
            }
            else if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                for (int i = 0; i < this.ObjectsList.Count; i++)
                {
                    ((C2DLayer)ObjectsList[i]).SetContoursType(ContourType);
                }
            }
        }
        public void ReplaceObjType(Constant.BASE_OBJ_TYPES_ID OldObjType, Constant.BASE_OBJ_TYPES_ID NewObjType)
        {
            if (OldObjType != Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DLayer)ObjectsList[i]).ReplaceObjType(OldObjType, NewObjType);
                    }
                }
                else if (this.ObjTypeID == OldObjType)
                {
                    this.ObjTypeID = NewObjType;
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DObject)ObjectsList[i]).TypeID = NewObjType;
                        if (NewObjType == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                        {
                            ((Contour)ObjectsList[i])._ContourType = 0;
                        }
                    }
                }
            }
        }

        public bool Contains(int ScreenX, int ScreenY)
        {
            double _x, _y;
            _x = RealXfromScreenX(ScreenX); _y = RealYfromScreenY(ScreenY);
            if ((_x > ObjRect.Left) && (_x < ObjRect.Right) && (_y > ObjRect.Top) && (_y < ObjRect.Bottom)) return true;
            else return false;
        }
        public C2DLayer AdvContains(int ScreenX, int ScreenY)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                C2DLayer lr;
                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    if (((C2DLayer)ObjectsList[i]).Visible)
                    {
                        lr = ((C2DLayer)ObjectsList[i]).AdvContains(ScreenX, ScreenY);
                        if (lr != null) return lr;
                    }
                }
            }
            else if ((this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DEPOSIT) ||
                    (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
            {
                if ((ObjRect.Left != Constant.DOUBLE_NAN) && (this.Visible))
                {
                    double x1, y1, x2, y2, buf;
                    x1 = ScreenXfromRealX(ObjRect.Left);
                    y1 = ScreenYfromRealY(ObjRect.Top);
                    x2 = ScreenXfromRealX(ObjRect.Right);
                    y2 = ScreenYfromRealY(ObjRect.Bottom);
                    if (x1 > x2)
                    {
                        buf = x1;
                        x1 = x2;
                        x2 = buf;
                    }
                    if (y1 > y2)
                    {
                        buf = y1;
                        y1 = y2;
                        y2 = buf;
                    }
                    x1 = x1 - 20;
                    x2 = x2 + 20;
                    y1 = y1 - 20;
                    y2 = y2 + 20;

                    if (((ScreenX == x1) || (ScreenX == x2) || ((ScreenX > x1) && (ScreenX < x2))) &&
                        ((ScreenY == y1) || (ScreenY == y2) || ((ScreenY > y1) && (ScreenY < y2))))
                        return this;
                }
            }
            return null;
        }
        public void FillAdvContains(int ScreenX, int ScreenY, ArrayList list)
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {

                for (int i = 0; i < ObjectsList.Count; i++)
                {
                    if (((C2DLayer)ObjectsList[i]).Visible)
                        ((C2DLayer)ObjectsList[i]).FillAdvContains(ScreenX, ScreenY, list);
                }
            }
            else
            {
                C2DLayer lr;
                lr = this.AdvContains(ScreenX, ScreenY);
                if (lr != null) list.Add(lr);
            }
        }
        public bool IntersectWith(RectangleD rect)
        {
            //float minX, maxX, minY, maxY;
            //float temp;
            if (ObjRect.Height + ObjRect.Width > 0)
            {
                //minX = ScreenXfromRealX(ObjRect.Left);
                //maxX = ScreenXfromRealX(ObjRect.Right);

                //minY = ScreenYfromRealY(ObjRect.Top);
                //maxY = ScreenYfromRealY(ObjRect.Bottom);

                //if (minX > maxX)
                //{
                //    temp = maxX;
                //    maxX = minX;
                //    minX = temp;
                //}
                //if (minY > maxY)
                //{
                //    temp = maxY;
                //    maxY = minY;
                //    minY = temp;
                //}

                return rect.IntersectWith(ObjRect);
            }
            return false;
        }

        public Constant.BASE_OBJ_TYPES_ID GetObjTypeIDRecursive()
        {
            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                if (ObjectsList.Count > 0)
                {
                    if (((C2DLayer)ObjectsList[0]).ObjTypeID != Constant.BASE_OBJ_TYPES_ID.LAYER)
                    {
                        return ((C2DLayer)ObjectsList[0]).ObjTypeID;
                    }
                    else
                    {
                        return ((C2DLayer)ObjectsList[0]).GetObjTypeIDRecursive();
                    }
                }
            }
            return this.ObjTypeID;
        }
        public OilField GetOilFieldByXY(int X, int Y)
        {
            int i;
            Contour cntr;

            if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                OilField of;
                for (i = 0; i < ObjectsList.Count; i++)
                {
                    of = ((C2DLayer)ObjectsList[i]).GetOilFieldByXY(X, Y);
                    if (of != null) return of;
                }
            }
            else if ((this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) && (this.oilfield.ParamsDict.RDFFieldCode != 0))
            {
                for (i = 0; i < ObjectsList.Count; i++)
                {
                    cntr = (Contour)this.ObjectsList[i];
                    if (cntr._ContourType == -3)
                    {
                        double realX = RealXfromScreenX(X);
                        double realY = RealYfromScreenY(Y);
                        if ((cntr.PointBoundsEntry(realX, realY)) && (cntr.PointBodyEntry(realX, realY)))
                        {
                            return this.oilfield;
                        }
                    }
                }
            }
            return null;
        }

        public void ReCalcCoord(OilfieldCoefItem oldCoeffParams)
        {
            int i, j;
            double xL, yL, xG, yG;
            PointD pt;
            Contour cntr;

            switch (ObjTypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    Well w;
                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        w = (Well)ObjectsList[i];
                        if ((w.coord != null) && (w.coord.Items != null) && (w.coord.Items.Length > 0))
                        {
                            xL = w.coord.Items[0].X;
                            yL = w.coord.Items[0].Y;
                            oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                            w.X = xG; w.Y = yG;
                        }
                        else if (w.coord != null)
                        {
                            xL = w.coord.Altitude.X;
                            yL = w.coord.Altitude.Y;
                            oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                            w.X = xG; w.Y = yG;
                        }
                        WellIcon icon;
                        for (j = 0; j < w.icons.Count; j++)
                        {
                            icon = w.icons[j];
                            oldCoeffParams.LocalCoordFromGlobal(icon.X, icon.Y, out xL, out yL);
                            oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                            icon.X = xG;
                            icon.Y = yG;
                        }
                    }
                    GetObjectsRect();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        cntr = ((Area)ObjectsList[i]).contour;
                        if (cntr._points.Count > 0)
                        {
                            for (j = 0; j < cntr._points.Count; j++)
                            {
                                pt = cntr._points[j];
                                oldCoeffParams.LocalCoordFromGlobal(pt.X, pt.Y, out xL, out yL);
                                oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                                pt.X = xG;
                                pt.Y = yG;
                                cntr._points[j] = pt;
                            }
                        }
                        oldCoeffParams.LocalCoordFromGlobal(cntr.X, cntr.Y, out xL, out yL);
                        oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                        cntr.X = xG;
                        cntr.Y = yG;
                    }
                    GetObjectsRect();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                    goto case Constant.BASE_OBJ_TYPES_ID.CONTOUR;
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        cntr = (Contour)ObjectsList[i];
                        if (cntr._points.Count > 0)
                        {
                            for (j = 0; j < cntr._points.Count; j++)
                            {
                                pt = cntr._points[j];
                                oldCoeffParams.LocalCoordFromGlobal(pt.X, pt.Y, out xL, out yL);
                                oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                                pt.X = xG;
                                pt.Y = yG;
                                cntr._points[j] = pt;
                            }
                        }
                        oldCoeffParams.LocalCoordFromGlobal(cntr.X, cntr.Y, out xL, out yL);
                        oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                        cntr.X = xG;
                        cntr.Y = yG;
                    }
                    GetObjectsRect();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                    WellPad wp;
                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        wp = (WellPad)ObjectsList[i];
                        if ((wp.contour != null) && (wp.contour.Count > 0))
                        {
                            for (j = 0; j < wp.contour.Count; j++)
                            {
                                pt = wp.contour[j];
                                oldCoeffParams.LocalCoordFromGlobal(pt.X, pt.Y, out xL, out yL);
                                oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                                pt.X = xG;
                                pt.Y = yG;
                                wp.contour[j] = pt;
                            }
                        }
                        oldCoeffParams.LocalCoordFromGlobal(wp.X, wp.Y, out xL, out yL);
                        oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                        wp.X = xG;
                        wp.Y = yG;
                    }
                    GetObjectsRect();
                    break;
            }
        }
        public void ReCalcCoord(double alpha, double x0, double y0, OilfieldCoefItem oldCoeffParams)
        {
            int i;
            double xL, yL, xG, yG;
            double dx, dy;
            switch (ObjTypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.LAYER:
                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DLayer)ObjectsList[i]).ReCalcCoord(alpha, x0, y0, oldCoeffParams);
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    Well w;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        w = (Well)ObjectsList[i];
                        if ((w.coord != null) && (w.coord.Items != null) && (w.coord.Items.Length > 0))
                        {
                            xL = w.coord.Items[0].X;
                            yL = w.coord.Items[0].Y;
                            oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                            w.X = xG; w.Y = yG;
                        }
                        else if (w.coord != null)
                        {
                            xL = w.coord.Altitude.X;
                            yL = w.coord.Altitude.Y;
                            oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                            w.X = xG; w.Y = yG;
                        }
                    }
                    GetObjectsRect();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    goto case Constant.BASE_OBJ_TYPES_ID.CONTOUR;
                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                    goto case Constant.BASE_OBJ_TYPES_ID.CONTOUR;
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    Contour cntr;
                    PointD pt;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
                            cntr = ((Area)ObjectsList[i]).contour;
                        else
                            cntr = (Contour)ObjectsList[i];
                        if (cntr._points.Count > 0)
                        {
                            for (int j = 0; j < cntr._points.Count; j++)
                            {
                                pt = cntr._points[j];
                                oldCoeffParams.LocalCoordFromGlobal(pt.X, pt.Y, out xL, out yL);
                                oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                                pt.X = xG;
                                pt.Y = yG;
                                cntr._points[j] = pt;
                            }
                        }
                        oldCoeffParams.LocalCoordFromGlobal(cntr.X, cntr.Y, out xL, out yL);
                        oilfield.CoefDict.GlobalCoordFromLocal(xL, yL, out xG, out yG);
                        cntr.X = xG;
                        cntr.Y = yG;
                    }
                    GetObjectsRect();
                    break;
            }
        }
        public void SetRotateCoef()
        {
            if (RotateCoef != null)
            {
                //_OilField.CopyCoeffParams(RotateCoef);
            }
        }

        public void SetVisibleRecursive(bool objVisible)
        {
            for (int i = 0; i < ObjectsList.Count; i++)
            {
                if (((C2DObject)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    ((C2DLayer)ObjectsList[i]).SetVisibleRecursive(objVisible);
                    ((C2DLayer)ObjectsList[i]).Visible = objVisible;
                }
                else
                    ((C2DObject)ObjectsList[i]).Visible = objVisible;
            }
        }

        public void ResetFontsPens()
        {
            C2DObject c2d;
            for (int i = 0; i < ObjectsList.Count; i++)
            {
                c2d = (C2DObject)ObjectsList[i];
                switch (c2d.TypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.MARKER:
                        //if (_pen_marker == null) _pen_marker = ((Marker)c2d).PenBound;
                        //((Marker)c2d).PenBound = _pen_marker;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION:
                        //Construction constr = (Construction)c2d;
                        //constr.font = _font;
                        //constr.pen = _pen_constr;
                        //constr.brush = _brush_constr;
                        //constr.fillBrush = _fillbrush_constr;
                        //Pen pen;
                        //switch (constr.Level)
                        //{
                        //    case 0:
                        //        pen = _pen_tube0;
                        //        break;
                        //    case 1:
                        //        pen = _pen_tube1;
                        //        break;
                        //    case 2:
                        //        pen = _pen_tube2;
                        //        break;
                        //    default:
                        //        pen = Pens.Black;
                        //        break;
                        //}
                        //for (int j = 0; j < constr.TubesInList.Count; j++)
                        //{
                        //    ((Tube)constr.TubesInList[j]).pen = pen;
                        //    ((Tube)constr.TubesInList[j]).brush = _brush_tube;
                        //}
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.AREA:
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                        goto case Constant.BASE_OBJ_TYPES_ID.CONTOUR;
                    case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.WELL:
                        ((Well)c2d).font = font;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                        ((Well)c2d).font = font;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                        ((PhantomWell)c2d)._font = font;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                        ((WellPad)c2d).font = font;
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                        VoronoiMap map = (VoronoiMap)c2d;
                        for (i = 0; i < map.Cells.Length; i++)
                        {
                            if (map.Cells[i].Well != null)
                            {
                                ((PhantomWell)map.Cells[i].Well)._font = font;
                            }
                        }
                        break;
                }
            }
        }

        // PROBLEM WELL LIST
        public void SetProblemWellList(string[] problemList)
        {
            this.problemWellList = problemList;
        }
        public string[] GetProblemWellList()
        {
            return this.problemWellList;
        }

        public void SetObjColor(Color clr)
        {
            C2DObject c2d;
            for (int i = 0; i < ObjectsList.Count; i++)
            {
                c2d = (C2DObject)ObjectsList[i];
                switch (c2d.TypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.LAYER:
                        ((C2DLayer)c2d).SetObjColor(clr);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                        Contour cntr = (Contour)c2d;
                        cntr.LineColor = clr;
                        break;
                }
            }
        }
        public void GetObjectsRect()
        {
            int i, j;
            C2DObject w;
            Contour cntr;
            switch (ObjTypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        cntr = ((Deposit)this.ObjectsList[i]).contour;

                        if ((ObjRect.Left == Constant.DOUBLE_NAN) || (cntr.Xmin < ObjRect.Left)) ObjRect.Left = cntr.Xmin;
                        if ((ObjRect.Top == Constant.DOUBLE_NAN) || (cntr.Ymin < ObjRect.Top)) ObjRect.Top = cntr.Ymin;
                        if ((ObjRect.Right == Constant.DOUBLE_NAN) || (cntr.Xmax > ObjRect.Right)) ObjRect.Right = cntr.Xmax;
                        if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (cntr.Ymax > ObjRect.Bottom)) ObjRect.Bottom = cntr.Ymax;
                    }
                    if (ObjRect.Left == ObjRect.Right) ObjRect.Right += 1;
                    if (ObjRect.Top == ObjRect.Bottom) ObjRect.Bottom += 1;

                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    GetObjectsMedian();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                    Marker mrk;
                    PointD point;
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        mrk = (Marker)ObjectsList[i];
                        if (mrk.Count > 0)
                        {
                            for (j = 0; j < mrk.Count; j++)
                            {
                                point = mrk.GetPoint(j);
                                if (point.X != Constant.DOUBLE_NAN)
                                {
                                    if ((ObjRect.Left == Constant.DOUBLE_NAN) || (point.X < ObjRect.Left)) ObjRect.Left = point.X;
                                    if ((ObjRect.Top == Constant.DOUBLE_NAN) || (point.Y < ObjRect.Top)) ObjRect.Top = point.Y;
                                    if ((ObjRect.Right == Constant.DOUBLE_NAN) || (point.X > ObjRect.Right)) ObjRect.Right = point.X;
                                    if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (point.Y > ObjRect.Bottom)) ObjRect.Bottom = point.Y;
                                }
                            }
                        }
                    }
                    if (ObjRect.Left == ObjRect.Right) ObjRect.Right += 1;
                    if (ObjRect.Top == ObjRect.Bottom) ObjRect.Bottom += 1;

                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    GetObjectsMedian();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                    Profile prof;
                    Well well;
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        prof = (Profile)ObjectsList[i];
                        if (prof.Count > 0)
                        {
                            for (j = 0; j < prof.Count; j++)
                            {
                                well = prof[j];
                                if ((ObjRect.Left == Constant.DOUBLE_NAN) || (well.X < ObjRect.Left)) ObjRect.Left = well.X;
                                if ((ObjRect.Top == Constant.DOUBLE_NAN) || (well.Y < ObjRect.Top)) ObjRect.Top = well.Y;
                                if ((ObjRect.Right == Constant.DOUBLE_NAN) || (well.X > ObjRect.Right)) ObjRect.Right = well.X;
                                if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (well.Y > ObjRect.Bottom)) ObjRect.Bottom = well.Y;
                            }
                        }
                    }
                    if (ObjRect.Left == ObjRect.Right) ObjRect.Right += 1;
                    if (ObjRect.Top == ObjRect.Bottom) ObjRect.Bottom += 1;

                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    GetObjectsMedian();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.GRID:
                    Grid grid;
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;
                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        grid = (Grid)ObjectsList[i];
                        if (grid.Xmin != Constant.DOUBLE_NAN)
                        {
                            if ((ObjRect.Left == Constant.DOUBLE_NAN) || (grid.Xmin < ObjRect.Left)) ObjRect.Left = grid.Xmin;
                            if ((ObjRect.Top == Constant.DOUBLE_NAN) || (grid.Ymin < ObjRect.Top)) ObjRect.Top = grid.Ymin;
                            if ((ObjRect.Right == Constant.DOUBLE_NAN) || (grid.Xmax > ObjRect.Right)) ObjRect.Right = grid.Xmax;
                            if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (grid.Ymax > ObjRect.Bottom)) ObjRect.Bottom = grid.Ymax;
                        }
                    }
                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION:
                    Construction constr;
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;
                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        constr = (Construction)ObjectsList[i];
                        if (constr.X != Constant.DOUBLE_NAN)
                        {
                            if ((ObjRect.Left == Constant.DOUBLE_NAN) || (constr.X - constr.Width / 2 < ObjRect.Left))
                                ObjRect.Left = constr.X - constr.Width / 2;

                            if ((ObjRect.Top == Constant.DOUBLE_NAN) || (constr.Y - constr.Height / 2 < ObjRect.Top))
                                ObjRect.Top = constr.Y - constr.Height / 2;

                            if ((ObjRect.Right == Constant.DOUBLE_NAN) || (constr.X + constr.Width / 2 > ObjRect.Right))
                                ObjRect.Right = constr.X + constr.Width / 2;

                            if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (constr.Y + constr.Height / 2 > ObjRect.Bottom))
                                ObjRect.Bottom = constr.Y + constr.Height / 2;
                        }
                    }
                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    break;

                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    goto case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL;

                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                    goto case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL;

                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        w = (C2DObject)ObjectsList[i];
                        if (w.X != Constant.DOUBLE_NAN)
                        {
                            if ((ObjRect.Left == Constant.DOUBLE_NAN) || (w.X < ObjRect.Left)) ObjRect.Left = w.X;
                            if ((ObjRect.Top == Constant.DOUBLE_NAN) || (w.Y < ObjRect.Top)) ObjRect.Top = w.Y;
                            if ((ObjRect.Right == Constant.DOUBLE_NAN) || (w.X > ObjRect.Right)) ObjRect.Right = w.X;
                            if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (w.Y > ObjRect.Bottom)) ObjRect.Bottom = w.Y;
                        }
                    }
                    if (ObjRect.Left == ObjRect.Right) ObjRect.Right += 1;
                    if (ObjRect.Top == ObjRect.Bottom) ObjRect.Bottom += 1;

                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    GetObjectsMedian();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                    VoronoiMap map = (VoronoiMap)ObjectsList[0];
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;

                    for (i = 0; i < map.Cells.Length; i++)
                    {
                        cntr = map.Cells[i].Cntr;
                        if ((ObjRect.Left == Constant.DOUBLE_NAN) || (cntr.Xmin < ObjRect.Left)) ObjRect.Left = cntr.Xmin;
                        if ((ObjRect.Top == Constant.DOUBLE_NAN) || (cntr.Ymin < ObjRect.Top)) ObjRect.Top = cntr.Ymin;
                        if ((ObjRect.Right == Constant.DOUBLE_NAN) || (cntr.Xmax > ObjRect.Right)) ObjRect.Right = cntr.Xmax;
                        if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (cntr.Ymax > ObjRect.Bottom)) ObjRect.Bottom = cntr.Ymax;
                    }
                    if (ObjRect.Left == ObjRect.Right) ObjRect.Right += 1;
                    if (ObjRect.Top == ObjRect.Bottom) ObjRect.Bottom += 1;

                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    GetObjectsMedian();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    goto case Constant.BASE_OBJ_TYPES_ID.CONTOUR;
                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                    goto case Constant.BASE_OBJ_TYPES_ID.CONTOUR;
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
                        {
                            cntr = ((Area)this.ObjectsList[i]).contour;
                        }
                        else
                        {
                            cntr = (Contour)ObjectsList[i];
                        }

                        if ((ObjRect.Left == Constant.DOUBLE_NAN) || (cntr.Xmin < ObjRect.Left)) ObjRect.Left = cntr.Xmin;
                        if ((ObjRect.Top == Constant.DOUBLE_NAN) || (cntr.Ymin < ObjRect.Top)) ObjRect.Top = cntr.Ymin;
                        if ((ObjRect.Right == Constant.DOUBLE_NAN) || (cntr.Xmax > ObjRect.Right)) ObjRect.Right = cntr.Xmax;
                        if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (cntr.Ymax > ObjRect.Bottom)) ObjRect.Bottom = cntr.Ymax;
                    }
                    if (ObjRect.Left == ObjRect.Right) ObjRect.Right += 1;
                    if (ObjRect.Top == ObjRect.Bottom) ObjRect.Bottom += 1;

                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    GetObjectsMedian();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                    WellPad wp;
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        wp = (WellPad)ObjectsList[i];
                        for (j = 0; j < wp.wells.Count; j++)
                        {
                            w = wp.wells[j];
                            if (w.X != Constant.DOUBLE_NAN)
                            {
                                if ((ObjRect.Left == Constant.DOUBLE_NAN) || (w.X < ObjRect.Left)) ObjRect.Left = w.X;
                                if ((ObjRect.Top == Constant.DOUBLE_NAN) || (w.Y < ObjRect.Top)) ObjRect.Top = w.Y;
                                if ((ObjRect.Right == Constant.DOUBLE_NAN) || (w.X > ObjRect.Right)) ObjRect.Right = w.X;
                                if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (w.Y > ObjRect.Bottom)) ObjRect.Bottom = w.Y;
                            }
                        }
                    }
                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    GetObjectsMedian();
                    break;
                case Constant.BASE_OBJ_TYPES_ID.LAYER:
                    C2DLayer layer;
                    ObjRect.Left = Constant.DOUBLE_NAN;
                    ObjRect.Right = Constant.DOUBLE_NAN;
                    ObjRect.Top = Constant.DOUBLE_NAN;
                    ObjRect.Bottom = Constant.DOUBLE_NAN;

                    for (i = 0; i < ObjectsList.Count; i++)
                    {
                        layer = (C2DLayer)ObjectsList[i];
                        if (layer.ObjRect.Left == Constant.DOUBLE_NAN) layer.GetObjectsRect();
                        if (layer.ObjRect.Left == Constant.DOUBLE_NAN) continue;
                        if ((ObjRect.Left == Constant.DOUBLE_NAN) || (layer.ObjRect.Left < ObjRect.Left))
                            ObjRect.Left = layer.ObjRect.Left;

                        if ((ObjRect.Top == Constant.DOUBLE_NAN) || (layer.ObjRect.Top < ObjRect.Top))
                            ObjRect.Top = layer.ObjRect.Top;

                        if ((ObjRect.Right == Constant.DOUBLE_NAN) || (layer.ObjRect.Right > ObjRect.Right))
                            ObjRect.Right = layer.ObjRect.Right;

                        if ((ObjRect.Bottom == Constant.DOUBLE_NAN) || (layer.ObjRect.Bottom > ObjRect.Bottom))
                            ObjRect.Bottom = layer.ObjRect.Bottom;

                    }
                    if (ObjRect.Left == Constant.DOUBLE_NAN) ObjRect.Left = 0;
                    if (ObjRect.Right == Constant.DOUBLE_NAN) ObjRect.Right = 0;
                    if (ObjRect.Top == Constant.DOUBLE_NAN) ObjRect.Top = 0;
                    if (ObjRect.Bottom == Constant.DOUBLE_NAN) ObjRect.Bottom = 0;
                    GetObjectsMedian();
                    break;
            }
        }
        public void GetObjectsMedian()
        {
            double xMed, yMed;
            xMed = 0;
            yMed = 0;
            for (int i = 0; i < ObjectsList.Count; i++)
            {
                xMed += ((C2DObject)ObjectsList[i]).X;
                yMed += ((C2DObject)ObjectsList[i]).Y;
            }
            ObjMedian.X = xMed / ObjectsList.Count;
            ObjMedian.Y = yMed / ObjectsList.Count;
        }
        public void DrawLayerName(Graphics grfx)
        {
            if (VisibleName)
            {
                double xCenter, yCenter;
                if (ObjMedian.X != Constant.DOUBLE_NAN)
                {
                    xCenter = ObjMedian.X;
                    yCenter = ObjMedian.Y;
                }
                else
                {
                    xCenter = ObjRect.Left + ObjRect.Width / 2;
                    yCenter = ObjRect.Top + ObjRect.Height / 2;
                }
                xCenter = ScreenXfromRealX(xCenter);
                yCenter = ScreenYfromRealY(yCenter);

                if ((this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) && (st_VisbleLevel < st_OilfieldXUnits))
                {
                    DrawWhiteGroundString(grfx, this.Name, SmartPlusGraphics.Contour.Fonts.Name, Brushes.Black, (float)xCenter, (float)yCenter, strFrmt);
                }
            }
        }
        public void DrawNameRecursive(Graphics grfx)
        {
            Contour cntr;
            for (int i = 0; i < ObjectsList.Count; i++)
            {
                if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    if (((C2DLayer)ObjectsList[i]).Visible)
                        ((C2DLayer)ObjectsList[i]).DrawNameRecursive(grfx);
                }
                else if (((BaseObj)ObjectsList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    cntr = (Contour)ObjectsList[i];
                    if (cntr.Visible/* && (cntr._ContourType > -4) && (cntr._ContourType < 0)*/)
                        cntr.DrawContourName(grfx);
                }
            }
        }
        public void DrawMainLine(Graphics grfx)
        {
            if (ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                if ((this.Visible) && (this.VisibleFar))
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DLayer)ObjectsList[i]).DrawMainLine(grfx);
                    }
                }
            }
            else if (ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
            {
                ((VoronoiMap)ObjectsList[0]).DrawFrontMap(grfx);
            }
            else if ((ObjTypeID != Constant.BASE_OBJ_TYPES_ID.GRID) &&
                     (ObjTypeID != Constant.BASE_OBJ_TYPES_ID.DEPOSIT) &&
                     (ObjTypeID != Constant.BASE_OBJ_TYPES_ID.MARKER) &&
                     ((st_VisbleLevel > st_OilfieldXUnits) || (ObjTypeID != Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)) &&
                     (ObjTypeID != Constant.BASE_OBJ_TYPES_ID.IMAGE))
            {
                this.DrawObject(grfx);
            }
        }
        public override void DrawObject(Graphics grfx)
        {
            if ((this.Visible) && (this.VisibleFar))
            {
                if (ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    bool draw = false;
                    int lvl = VisibleLvl.GetVisibleLvl();
                    switch (lvl)
                    {
                        case 3:
                            draw = true;
                            break;
                        case 2:
                            if (st_VisbleLevel <= st_NgduXUnits) draw = true;
                            break;
                        case 1:
                            if (st_VisbleLevel <= st_OilfieldXUnits) draw = true;
                            break;
                        case 0:
                            if (st_VisbleLevel <= st_AreaXUnits) draw = true;
                            break;
                    }
                    if (draw)
                    {
                        for (int i = 0; i < ObjectsList.Count; i++)
                        {
                            ((C2DObject)ObjectsList[i]).DrawObject(grfx);
                        }
                        for (int i = 0; i < ObjectsList.Count; i++)
                        {
                            ((PhantomWell)ObjectsList[i]).DrawCaption(grfx);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DObject)ObjectsList[i]).DrawObject(grfx);
                    }
                    DrawLayerName(grfx);
                    if (ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                    {
                        if (this.DrawWellHead)
                        {
                            for (int i = 0; i < ObjectsList.Count; i++)
                            {
                                ((Well)ObjectsList[i]).DrawWellHead(grfx, this.oilfield.CoefDict);
                                ((Well)ObjectsList[i]).DrawTrajectory(grfx);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < ObjectsList.Count; i++)
                            {
                                ((Well)ObjectsList[i]).DrawTrajectory(grfx);
                            }
                        }
                    }
                }
            }
        }
        public void DrawSelectedObject(Graphics grfx)
        {
            if (this.Visible)
            {
                if (this.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    for (int i = 0; i < ObjectsList.Count; i++)
                    {
                        ((C2DLayer)ObjectsList[i]).DrawSelectedObject(grfx);
                    }
                }
                else
                {
                    if (ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                    {
                        bool draw = false;
                        int lvl = VisibleLvl.GetVisibleLvl();
                        switch (lvl)
                        {
                            case 3:
                                draw = true;
                                break;
                            case 2:
                                if (st_VisbleLevel <= st_NgduXUnits) draw = true;
                                break;
                            case 1:
                                if (st_VisbleLevel <= st_OilfieldXUnits) draw = true;
                                break;
                            case 0:
                                if (st_VisbleLevel <= st_AreaXUnits) draw = true;
                                break;
                        }
                        if (draw)
                        {
                            for (int i = 0; i < ObjectsList.Count; i++)
                            {
                                if (((C2DObject)ObjectsList[i]).Selected)
                                {
                                    ((PhantomWell)ObjectsList[i]).srcWell.Selected = true;
                                    ((PhantomWell)ObjectsList[i]).srcWell.DrawObject(grfx);
                                    ((PhantomWell)ObjectsList[i]).srcWell.Selected = false;
                                }
                            }
                        }
                    }
                    else if (ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                    {

                    }
                    else if (ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
                    {
                        for (int i = 0; i < ObjectsList.Count; i++)
                        {
                            if (((Area)ObjectsList[i]).contour.Selected)
                            {
                                ((C2DObject)ObjectsList[i]).DrawObject(grfx);
                            }
                        }

                    }
                    else if (ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                    {
                        for (int i = 0; i < ObjectsList.Count; i++)
                        {
                            if (((C2DObject)ObjectsList[i]).Selected)
                            {
                                ((C2DObject)ObjectsList[i]).DrawObject(grfx);
                            }
                        }
                        DrawLayerName(grfx);
                    }
                    else if ((ObjTypeID != Constant.BASE_OBJ_TYPES_ID.GRID) &&
                             (ObjTypeID != Constant.BASE_OBJ_TYPES_ID.MARKER) &&
                             (ObjTypeID != Constant.BASE_OBJ_TYPES_ID.IMAGE))
                    {
                        for (int i = 0; i < ObjectsList.Count; i++)
                        {
                            if ((ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) &&
                                (((Contour)ObjectsList[0])._ContourType == -5)) break;

                            if (((C2DObject)ObjectsList[i]).Selected)
                            {
                                ((C2DObject)ObjectsList[i]).DrawObject(grfx);
                            }
                        }
                    }
                }
            }
        }
    }
}
