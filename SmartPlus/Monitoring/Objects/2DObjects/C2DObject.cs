﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public class C2DObject : BaseObj
    {
        public double X = Constant.DOUBLE_NAN;
        public double Y = Constant.DOUBLE_NAN;

        public static RectangleF st_BitMapRect;
        public static RectangleD st_ObjectsRect;
        public static RectangleF st_BMPRect;
        public static RectangleD st_RealScreenRect;

        static double st_XUnits;
        static double st_YUnits;
        public static float st_radiusX;
        public static float st_radiusY;

        public static double st_VisbleLevel;
        static public double st_XUnitsInOnePixel
        {
            get { return st_XUnits; }
            set
            {
                st_XUnits = value;
                st_radiusX = ((float)Math.Round(50.31 * Math.Pow(value, -1.013)) < 1) ? 1 : (float)Math.Round(50.31 * Math.Pow(value, -1.013));
                st_radiusY = st_radiusX;
            }
        }
        static public double st_YUnitsInOnePixel
        {
            get { return st_YUnits; }
            set
            {
                st_YUnits = value;
            }
        }

        public static double st_MaxXUnits;
        public static double st_NgduXUnits;
        public static double st_OilfieldXUnits;
        public static double st_AreaXUnits;

        static public float ScreenXfromRealX(double rX)
        {
            return (float)(-st_BitMapRect.Left + (rX - st_ObjectsRect.Left) / st_XUnitsInOnePixel);
        }
        static public float ScreenYfromRealY(double rY)
        {
            return (float)(-st_BitMapRect.Top + (st_ObjectsRect.Bottom - rY) / st_YUnitsInOnePixel);
        }
        static public double RealXfromScreenX(double sX)
        {
            return st_ObjectsRect.Left + (sX + st_BitMapRect.Left) * st_XUnitsInOnePixel;
        }
        static public double RealYfromScreenY(double sY)
        {
            return st_ObjectsRect.Bottom - (sY + st_BitMapRect.Top) * st_YUnitsInOnePixel;
        }
        static public RectangleD RealRectFromScreen(RectangleF ScreenRect)
        {
            RectangleD realRect = new RectangleD();
            if (st_ObjectsRect != null)
            {
                realRect.Left = RealXfromScreenX(ScreenRect.Left);
                realRect.Right = RealXfromScreenX(ScreenRect.Right);
                realRect.Top = RealYfromScreenY(ScreenRect.Bottom);
                realRect.Bottom = RealYfromScreenY(ScreenRect.Top);
            }
            return realRect;
        }
        static public bool DrawAllObjects;
        static public bool ShowAreasMode;

        static public OilfieldCoefItem GetCoeffTransfer(double[] Coord)
        {
            OilfieldCoefItem retValue = null;
            double[,] a = new double[3, 3];
            double[] b = new double[3];
            double kxx, kxy, kyx, kyy, shx, shy;
            double A11, A12, A21, A22, B1, B2;

            if (Coord.Length == 12)
            {

                a[0, 0] = Coord[0]; a[0, 1] = Coord[1]; a[0, 2] = 1;
                a[1, 0] = Coord[2]; a[1, 1] = Coord[3]; a[1, 2] = 1;
                a[2, 0] = Coord[4]; a[2, 1] = Coord[5]; a[2, 2] = 1;
                b[0] = Coord[6]; b[1] = Coord[7]; b[2] = Coord[8];

                A11 = a[1, 1] - a[0, 1] * a[1, 0] / a[0, 0];
                A12 = a[1, 2] - a[0, 2] * a[1, 0] / a[0, 0];

                A21 = a[2, 1] - a[0, 1] * a[2, 0] / a[0, 0];
                A22 = a[2, 2] - a[0, 2] * a[2, 0] / a[0, 0];

                B1 = b[1] - b[0] * a[1, 0] / a[0, 0];
                B2 = b[2] - b[0] * a[2, 0] / a[0, 0];

                shx = (B2 - B1 * A21 / A11) / (A22 - A12 * A21 / A11);

                kxy = (a[0, 0] * b[1] - a[1, 0] * b[0] + shx * (a[1, 0] * a[0, 2] - a[0, 0] * a[1, 2])) / (a[1, 1] * a[0, 0] - a[1, 0] * a[0, 1]);

                kxx = (b[0] - a[0, 1] * kxy - a[0, 2] * shx) / a[0, 0];
                // Находим коэфициенты для Y
                b[0] = Coord[9]; b[1] = Coord[10]; b[2] = Coord[11];
                A11 = a[1, 1] - a[0, 1] * a[1, 0] / a[0, 0];
                A12 = a[1, 2] - a[0, 2] * a[1, 0] / a[0, 0];
                A21 = a[2, 1] - a[0, 1] * a[2, 0] / a[0, 0];
                A22 = a[2, 2] - a[0, 2] * a[2, 0] / a[0, 0];
                B1 = b[1] - b[0] * a[1, 0] / a[0, 0];
                B2 = b[2] - b[0] * a[2, 0] / a[0, 0];

                shy = (B2 - B1 * A21 / A11) / (A22 - A12 * A21 / A11);
                kyy = (a[0, 0] * b[1] - a[1, 0] * b[0] + shy * (a[1, 0] * a[0, 2] - a[0, 0] * a[1, 2])) / (a[1, 1] * a[0, 0] - a[1, 0] * a[0, 1]);
                kyx = (b[0] - a[0, 1] * kyy - a[0, 2] * shy) / a[0, 0];

                retValue = new OilfieldCoefItem();
                retValue.KfX_Xnew = kxx; retValue.KfX_Ynew = kxy; retValue.ShX = shx;
                retValue.KfY_Xnew = kyx; retValue.KfY_Ynew = kyy; retValue.ShY = shy;
            }
            return retValue;
        }

        static protected float LetterBoxDX, LetterBoxDY; 
        static protected Bitmap LetterBoxImageGreen = null;
        static protected Bitmap LetterBoxImageOrange = null;
        static protected Bitmap LetterBoxImageGray = null;

        static C2DObject()
        {
            st_MaxXUnits = 1631.461442;
            st_NgduXUnits = 700;
            st_OilfieldXUnits = 200;
            st_AreaXUnits = 30;

            st_VisbleLevel = st_MaxXUnits;
            st_XUnitsInOnePixel = st_MaxXUnits;
            st_YUnitsInOnePixel = st_MaxXUnits;

            DrawAllObjects = false;
            LetterBoxImageGray = Properties.Resources.Send_Email16;
            LetterBoxImageGreen = Properties.Resources.Send_Email16_green;
            LetterBoxImageOrange = Properties.Resources.Send_Email16_orange;
        }

        public static void DrawWhiteGroundString(Graphics grfx, string Text, Font F, Brush Br, float X, float Y, StringFormat sf)
        {
            if (grfx != null)
            {
                grfx.DrawString(Text, F, Brushes.White, X - 1, Y - 1, sf);
                grfx.DrawString(Text, F, Brushes.White, X + 1, Y - 1, sf);
                grfx.DrawString(Text, F, Brushes.White, X + 1, Y + 1, sf);
                grfx.DrawString(Text, F, Brushes.White, X - 1, Y + 1, sf);
                grfx.DrawString(Text, F, Br, X, Y, sf);
            }
        }
        public virtual void DrawObject(Graphics grfx) { }
    }
}
