﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using SmartPlus.DictionaryObjects;
using Selecting;
using System.Runtime.InteropServices;
using System.ComponentModel;
using RDF.Objects;
using System.IO;
using SmartPlus.Dialogs;

namespace SmartPlus
{
    public delegate void OnChangeSelectedObjectDelegate(BaseObj LastSelectedObject);

    public sealed class CanvasMap : PictureBox
    {
        //todo: Убрать косяк - исчезает Рабочая папка - не найден пока - убрал удаление предыдущей копии
        //todo: При выборе скважин находить список скважин в который она входит...(возможно самый верхний)

        public event OnChangeSelectedObjectDelegate OnChangeSelectedObject;

        public ArrayList LayerList;
        public ArrayList LayerWorkList;
        SelectedWell swells;
        SelectedArea sArea;
        public bool LayerWorkListChanged;
        public bool GrayWellIconsMode;
        public ActiveTreeView twActiveOilField;
        public LayersTreeView twLayers;
        public TreeViewWorkLayers twWork;
        public TreeView LastSelectedTreeView = null;
        Rectangle BMPRect;
        public Rectangle BitMapRect;
        Rectangle ObjectsRect;
        public MainForm mainForm;
        PointF MButtonDownPoint, MButtonPoint, MButtonTimerPoint;
        MouseButtons MouseButtonDown;
        bool MButtonPressed;
        bool CtrlPressed, ShiftPressed;
        bool ShiftRightAngle;
        bool AddWellProfileMode;

        public bool MessageShown = false;
        public bool ShowGrid;
        ImageList StateImgList;
        public Selecting.__ SelObjUpdater;
        BaseObj SelObject;

        // LOAD GRID
        public int LoadGridMode;
        public TreeNode LoadGridNode = null;

        long timeDraw = -1;
        Font fDraw;

        public Project MapProject;
        Bitmap bmp;
        Graphics bmp_grfx;
        Timer tmr, shiftTmr;
        public bool _disabled;
        private float _fontSize;
        public RichTextBox reLog;
        public C2DLayer EditedLayer;
        public int EditedOilFieldIndex;
        public C2DLayer WorkLayerActive;
        C2DLayer RightClickLayer;
        bool EditedLayerRotate;
        public Constant.BUBBLE_MAP_TYPE BubbleMapType;
        public DateTime BubbleDT1, BubbleDT2;
        public float BubbleK;
        public int BubbleStartMonth, BubbleLastMonth;
        public bool BubbleHistoryViewBySector;
        public bool BubbleHistoryByChess;

        public bool Rotate
        {
            get { return EditedLayerRotate; }
            set
            {
                EditedLayerRotate = value;
                if (!value) EditedLayerRotateCenter.X = -1;
            }
        }

        Point EditedLayerRotateCenter;
        Contour newContour;
        bool newContourEnd;
        int ShiftSelContourPoint;
        int ShiftSelContourIndex;
        int ShiftAdhesion;
        PointD ShiftAdhesionRealPoint;

        bool snapShootMode;
        public bool SnapShootMode
        {
            get { return snapShootMode; }
            set
            {
                SnapPt1 = Point.Empty;
                snapShootMode = value;
                if (value) this.Cursor = Cursors.Cross; else this.Cursor = Cursors.Default;
            }
        }
        Point SnapPt1;
        Brush brSnap;

        public C2DLayer copyLayer;
        public TreeNode copyNode;
        public ArrayList selLayerList;
        public ArrayList selWellList;
        public ArrayList SumCalcWelllist;
        public ArrayList selMerOilObjCodes;

        public bool UpdateFilterOilObj = true;

        public ArrayList sumIndexes;
        public SumParameters sumParams;
        public SumParameters sumChessParams;
        public SumParameters sumTable81;
        public SumParameters sumBP;
        public double[] BPParams;
        public bool IsGraphicsByOneDate;
        public bool AppendSumParams;
        SmartPlus.Network.DiscussLetterBox LetterBoxUpdater = null;
        bool showLetterBox;
        public bool ShowLetterBox
        {
            get { return showLetterBox; }
            set
            {
                if (LayerList.Count > 0)
                {
                    List<C2DLayer> list = new List<C2DLayer>();
                    for (int i = 0; i < LayerList.Count; i++)
                    {
                        ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, list, false);
                        ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.AREA, list, false);
                        ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list, false);
                    }
                    for (int i = 0; i < list.Count; i++)
                    {
                        switch (((C2DLayer)list[i]).ObjTypeID)
                        {
                            case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                                ((Contour)((C2DLayer)list[i]).ObjectsList[0]).ShowLetterBox = value;
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.AREA:
                                for (int j = 0; j < ((C2DLayer)list[i]).ObjectsList.Count; j++)
                                {
                                    ((Area)((C2DLayer)list[i]).ObjectsList[j]).contour.ShowLetterBox = value;
                                }
                                break;
                            case Constant.BASE_OBJ_TYPES_ID.WELL:
                                for (int j = 0; j < ((C2DLayer)list[i]).ObjectsList.Count; j++)
                                {
                                    ((Well)((C2DLayer)list[i]).ObjectsList[j]).ShowLetterBox = value;
                                }
                                break;
                        }
                    }
                }
                showLetterBox = value;
            }
        }
        C2DObject LetterBoxObject = null;

        bool showBizPlan;
        public bool ShowBizPlanParams
        {
            get { return showBizPlan; }
            set
            {
                if (LayerList.Count > 0)
                {
                    List<C2DLayer> list = new List<C2DLayer>();
                    for (int i = 0; i < LayerList.Count; i++)
                    {
                        if (((C2DLayer)LayerList[i]).Visible)
                        {
                            ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, list, false);
                        }
                    }
                    for (int i = 0; i < list.Count; i++)
                    {
                        ((Contour)((C2DLayer)list[i]).ObjectsList[0]).ShowBizPlanParams = value;
                    }
                }
                showBizPlan = value;
            }
        }
        Contour BPcontour = null;

        public BaseObj LastSelectObject;
        WorkObjectSettingForm WorkObjectSettings;

        float iconFontSize;
        Font[] iconFonts = null;
        Font[] nameFonts;
        FontFamily[] FontFamilyList;
        ExFontList exFontList = null;
        Well ProfileOverWell = null;
        
        Pen _newContPen, _penFantom, _pen_Profile;
        SolidBrush _brushProfile;
        public void Disable() { _disabled = true; }
        public void Enable() { _disabled = false; }

        public int CountLayers { get { return LayerList.Count; } }

        ToolTip MapTip;
        Timer TipTimer;
        Point TipPoint;

        #region CONTEXT MENU MAP
        ContextMenuStrip cMenuContour, cMenuGrid, cMenuArea, cMenuVoronoiMap, cMenuDeposit;

        void InitContextMenu()
        {
            ToolStripMenuItem item;

            #region Context Menu Contour
            cMenuContour = new ContextMenuStrip();
            cMenuContour.Closed += new ToolStripDropDownClosedEventHandler(ContextMenuClosed);

            item = (ToolStripMenuItem)cMenuContour.Items.Add("В Рабочую папку...", SmartPlus.Properties.Resources.Folder_add);
            item.Click += new EventHandler(ProjectObjectToWorkFolderClick);

            item = (ToolStripMenuItem)cMenuContour.Items.Add("Усреднить шаг точек...");
            item.Click += new EventHandler(ContoursAverageStepClick);

            // Удалить
            item = (ToolStripMenuItem)cMenuContour.Items.Add("Удалить", SmartPlus.Properties.Resources.Delete16);
            item.Click += new EventHandler(ContourRemove);

            cMenuContour.Items.Add("-");
            item = (ToolStripMenuItem)cMenuContour.Items.Add("Свойства контура..", SmartPlus.Properties.Resources.Settings16);
            item.Click += new EventHandler(ContourSettings);
            #endregion

            #region Context Menu Grid
            cMenuGrid = new ContextMenuStrip();
            cMenuGrid.Closed += new ToolStripDropDownClosedEventHandler(ContextMenuClosed);

            item = (ToolStripMenuItem)cMenuGrid.Items.Add("В Рабочую папку...", SmartPlus.Properties.Resources.Folder_add);
            item.Click += new EventHandler(ProjectObjectToWorkFolderClick);
            #endregion

            #region Context Menu Area

            cMenuArea = new ContextMenuStrip();
            cMenuArea.Closed += new ToolStripDropDownClosedEventHandler(ContextMenuClosed);

            item = (ToolStripMenuItem)cMenuArea.Items.Add("PVT ячейки в рабочую папку", SmartPlus.Properties.Resources.Folder_add);
            item.Click += new EventHandler(PVTAreaToWorkFolder_Click);

            item = (ToolStripMenuItem)cMenuArea.Items.Add("Запасы по ячейке");
            item.Click += new EventHandler(CalcAreaReservesClick);
            #endregion

            #region Context Menu Voronoi Map

            cMenuVoronoiMap = new ContextMenuStrip();
            cMenuVoronoiMap.Closed += new ToolStripDropDownClosedEventHandler(ContextMenuClosed);

            item = (ToolStripMenuItem)cMenuVoronoiMap.Items.Add("Запасы по областям Вороного...");
            item.Click += new EventHandler(VoronoiCellsReserves_Click);
            #endregion

            #region Context Menu Deposit
            cMenuDeposit = new ContextMenuStrip();

            item = (ToolStripMenuItem)cMenuDeposit.Items.Add("Свойства залежи..", SmartPlus.Properties.Resources.Settings16);
            item.Click += DepositSettings;
            #endregion
        }

        void DepositSettings(object sender, EventArgs e)
        {
            WorkObjectSettingsShow(RightClickLayer);
        }

        void ContextMenuClosed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            if (RightClickLayer != null)
            {
                RightClickLayer.Selected = false;
                DrawLayerList();
            }
        }
        void CopyContourToWorkFolder(C2DLayer LayerSrc)
        {
            CopyWorkLayer(LayerSrc);
            if (copyLayer != null)
            {
                copyLayer.ReplaceObjType(Constant.BASE_OBJ_TYPES_ID.CONTOUR, Constant.BASE_OBJ_TYPES_ID.CONTOUR);
            }
            PasteWorkLayer(false);
        }
        void CopyGridToWorkFolder(C2DLayer LayerSrc)
        {
            CopyWorkLayer(LayerSrc);
            PasteWorkLayer(false);
        }
        void ProjectObjectToWorkFolderClick(object sender, EventArgs e)
        {
            if (RightClickLayer != null)
            {
                switch (RightClickLayer.ObjTypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                        CopyContourToWorkFolder(RightClickLayer);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.GRID:
                        CopyGridToWorkFolder(RightClickLayer);
                        break;
                }
            }

        }
        void ContoursAverageStepClick(object sender, EventArgs e)
        {
            SetContoursAverageStep(RightClickLayer);
        }
        void CalcAreaReservesClick(object sender, EventArgs e)
        {
            if ((selLayerList.Count > 0) && (((BaseObj)selLayerList[0]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                C2DLayer layer = (C2DLayer)selLayerList[0];
                if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) && (layer.ObjectsList.Count > 0) && (layer.oilfield != null))
                {
                    Contour cntr = (Contour)layer.ObjectsList[0];
                    OilField of = layer.oilfield;
                    Area area = null;
                    if (layer.Level > -1) area = (Area)of.Areas[layer.Level];
                    Grid grid;
                    List<Grid> grids = new List<Grid>();
                    var dict = (StratumDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                    for (int i = 0; i < of.Grids.Count; i++)
                    {
                        grid = of.Grids[i];
                        if (grid.GridType == 2 && grid.StratumCode == cntr.StratumCode)
                        {
                            grids.Add(grid);
                        }
                    }
                    if (grids.Count == 0)
                    {
                        StratumTreeNode node;
                        for (int i = 0; i < of.Grids.Count; i++)
                        {
                            grid = of.Grids[i];
                            if (grid.GridType == 2)
                            {
                                node = dict.GetStratumTreeNode(grid.StratumCode);
                                if (node != null && node.GetParentLevelByCode(area.PlastCode) != -1)
                                {
                                    grids.Add(grid);
                                }
                            }
                        }
                    }

                    OutputTextForm form = mainForm.OutBox;
                    if (form.CountRows > 0) form.Clear();
                    form.Text = "Сообщения";

                    if (grids.Count > 0)
                    {
                        int res;
                        bool loaded;
                        double square = 0;
                        double sumvals = 0;
                        for (int i = 0; i < grids.Count; i++)
                        {
                            square = 0;
                            sumvals = 0;
                            res = 0;
                            loaded = false;
                            if (!grids[i].DataLoaded)
                            {
                                res = of.LoadGridDataFromCacheByIndex(null, null, grids[i].Index, false);
                                loaded = true;
                            }
                            if (res == 0)
                            {
                                grids[i].CalcSumValues(null, null, cntr, out square, out sumvals);
                                double k = 0, Vbal = 0, Vpbal = 0;
                                if (!area.pvt.IsEmpty)
                                {
                                    k = area.pvt.OilDensity * area.pvt.Porosity * area.pvt.OilInitialSaturation / area.pvt.OilVolumeFactor;
                                    Vbal = k * sumvals / 1000;
                                    Vpbal = k * sumvals * area.Params.KIN / 1000;
                                }

                                form.AddBoldText("Операция: "); form.AddText("Расчет запасов по ячейке"); form.NewLine();
                                form.AddBoldText("Ячейка: "); form.AddText(area.Name); form.NewLine();
                                form.AddBoldText("Сетка: "); form.AddText(grids[i].Name); form.NewLine();
                                form.NewLine();
                                form.AddBoldText("Начальные балансовые запасы: "); form.AddText(string.Format("{0:#,##0.###} тыс.т.", Vbal)); form.NewLine();
                                form.AddBoldText("Начальные извлекаемые запасы: "); form.AddText(string.Format("{0:#,##0.###} тыс.т.", Vpbal)); form.NewLine();
                                form.AddBoldText("Площадь сетки по контуру: "); form.AddText(string.Format("{0:#,##0.###} м2", square)); form.NewLine();
                                form.AddBoldText("Значение по контуру: "); form.AddText(string.Format("{0:#,##0.###}", sumvals)); form.NewLine();
                                form.NewLine();
                                form.AddBoldText("Комментарий:\n");
                                form.AddText("Интегрирование сетки производится приблизительным методом (метод палетки).");
                                if (loaded) grids[i].FreeDataMemory();
                                break;
                            }
                        }
                    }
                    else
                    {
                        StratumTreeNode node = dict.GetStratumTreeNode(cntr.StratumCode);

                        form.AddBoldText("Операция: "); form.AddText("Расчет запасов по ячейке"); form.NewLine();
                        form.AddBoldText("Ячейка: "); form.AddText(area.Name); form.NewLine();
                        form.NewLine();
                        form.AddText("Не найдена сетка толщин для пласта " + ((node != null) ? node.Name : cntr.StratumCode.ToString()) + "!");
                    }
                    form.ShowDialog();
                }
            }
        }
        void ShowContextMenu(C2DLayer layer, Point Location)
        {
            if (layer != null)
            {
                RightClickLayer = layer;
                RightClickLayer.Selected = true;
                switch (RightClickLayer.ObjTypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                        cMenuDeposit.Show(PointToScreen(Location));
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                        cMenuVoronoiMap.Show(PointToScreen(Location));
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                        if ((layer.ObjectsList.Count > 0) && (((Contour)layer.ObjectsList[0])._ContourType == -4))
                        {
                            cMenuArea.Show(PointToScreen(Location));
                        }
                        else
                        {
                            cMenuContour.Items[0].Visible = true;
                            cMenuContour.Items[1].Visible = false;
                            cMenuContour.Items[2].Visible = false;
                            cMenuContour.Items[3].Visible = false;
                            cMenuContour.Items[4].Visible = false;
                            if ((layer.node != null) && (layer.node.TreeView == twWork))
                            {
                                cMenuContour.Items[0].Visible = false;
                                cMenuContour.Items[1].Visible = true;
                                cMenuContour.Items[2].Visible = true;
                                cMenuContour.Items[3].Visible = true;
                                cMenuContour.Items[4].Visible = true;
                            }
                            cMenuContour.Show(PointToScreen(Location));
                        }
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.GRID:
                        cMenuGrid.Show(PointToScreen(Location));
                        break;
                }
            }
        }
        void PVTAreaEdit_Click(object sender, EventArgs e)
        {
            if ((selLayerList.Count > 0) && (((BaseObj)selLayerList[0]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                C2DLayer layer = (C2DLayer)selLayerList[0];
                if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) && (layer.ObjectsList.Count > 0) && (layer.oilfield != null))
                {
                    Contour cntr = (Contour)layer.ObjectsList[0];
                    OilField of = layer.oilfield;
                    Area area = null;
                    if ((layer.Level > -1) && (layer.Level < of.Areas.Count))
                    {
                        area = (Area)of.Areas[layer.Level];
                    }
                    if (area != null)
                    {
                        PVTParamsEditForm form = new PVTParamsEditForm(mainForm);
                        area.pvt.StratumCode = area.PlastCode;
                        area.pvt.KIN = area.Params.KIN;
                        if (form.ShowDialog(area.pvt, area.Name) == DialogResult.OK)
                        {
                            area.pvt = form.PvtItem;
                            area.Params.KIN = area.pvt.KIN;
                            of.CalcAreasReserves();
                            (MapProject.OilFields[area.OilFieldIndex]).WriteAreasToCache(null, null);
                        }
                    }
                }
            }
        }
        void PVTAreaToWorkFolder_Click(object sender, EventArgs e)
        {
            if ((selLayerList.Count > 0) && (((BaseObj)selLayerList[0]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                C2DLayer layer = (C2DLayer)selLayerList[0];
                if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) && (layer.ObjectsList.Count > 0) && (layer.oilfield != null))
                {
                    Contour cntr = (Contour)layer.ObjectsList[0];
                    OilField of = layer.oilfield;
                    Area area = null;
                    if ((layer.Level > -1) && (layer.Level < of.Areas.Count))
                    {
                        area = (Area)of.Areas[layer.Level];
                    }
                    if (area != null && (!area.pvt.IsEmpty))
                    {
                        PVTWorkObject pvt = new PVTWorkObject();
                        pvt.Item = area.pvt;
                        pvt.OilFieldName = layer.oilfield.Name;
                        var dictArea = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                        pvt.OilfieldAreaName = dictArea.GetShortNameByCode(area.OilFieldAreaCode);
                        pvt.AreaName = area.Name;
                        pvt.Item.StratumCode = area.PlastCode;
                        pvt.Item.KIN = area.Params.KIN;
                        pvt.Name = string.Format("PVT ({0}, {1})", pvt.OilFieldName, pvt.AreaName);
                        copyLayer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PVT, layer.oilfield.Index);
                        copyLayer.Name = pvt.Name;
                        copyLayer.Add(pvt);
                        PasteWorkLayer(true);
                    }
                }
            }
        }
        void ContourRemove(object sender, EventArgs e)
        {
            if ((RightClickLayer != null) && (RightClickLayer.node != null) && (RightClickLayer.node.TreeView == twWork))
            {
                string quest = "Удалить объект '" + RightClickLayer.Name + "'?";
                TreeNode tn = null;
                if (MessageBox.Show(quest, "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    tn = RightClickLayer.node;

                    if ((tn.Tag != null) && (tn.Text == ((C2DLayer)tn.Tag).Name))
                    {
                        RemoveWorkLayer(tn);
                    }
                    DrawLayerList();
                }
            }
        }
        void VoronoiCellsReserves_Click(object sender, EventArgs e)
        {
            if ((selLayerList.Count > 0) && (((BaseObj)selLayerList[0]).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                C2DLayer layer = (C2DLayer)selLayerList[0];
                if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP) && (layer.ObjectsList.Count > 0))
                {
                    VoronoiMap map = (VoronoiMap)layer.ObjectsList[0];
                    ShowVoronoiCellsParams(map, false);
                }
            }

        }
        void ContourSettings(object sender, EventArgs e)
        {
            WorkObjectSettingsShow(RightClickLayer);
        }
        #endregion

        // constructor
        public CanvasMap(MainForm MainForm)
        {
            InitContextMenu();
            SelObjUpdater = new Selecting.__(this);
            swells = new SelectedWell(this.SelObjUpdater);
            SelObject = null;
            sArea = new SelectedArea();
            BubbleDT1 = DateTime.MinValue;
            BubbleDT2 = DateTime.MaxValue;
            BubbleHistoryViewBySector = true;
            BubbleHistoryByChess = false;
            BubbleMapType = Constant.BUBBLE_MAP_TYPE.NOT_SET;
            C2DObject.ShowAreasMode = false;
            AddWellProfileMode = false;

            newContour = null;
            newContourEnd = false;

            SnapShootMode = false;
            brSnap = new SolidBrush(Color.FromArgb(75, Color.Gray));

            EditedLayer = null;
            EditedOilFieldIndex = -1;
            EditedLayerRotate = false;
            EditedLayerRotateCenter.X = -1;
            ShowGrid = false;
            sumIndexes = new ArrayList();

            LayerList = new ArrayList();
            LayerWorkList = new ArrayList();
            selLayerList = new ArrayList();
            selWellList = new ArrayList();
            SumCalcWelllist = new ArrayList();
            selMerOilObjCodes = new ArrayList();

            InitFonts();

            LayerWorkListChanged = false;

            //XUnitsInOnePixel = 1631.461442;
            //YUnitsInOnePixel = 1631.461442;

            ShowBizPlanParams = false;

            copyLayer = null;
            ShiftSelContourPoint = -1;
            ShiftSelContourIndex = -1;

            this.mainForm = MainForm;
            this.Parent = MainForm.tsCanvContainer.ContentPanel;
            this.Dock = DockStyle.Fill;

            if (mainForm.MaxMonitorHeight > 0)
            {
                bmp = new Bitmap(mainForm.MaxMonitorWidth, mainForm.MaxMonitorHeight);
            }

            mainForm.pgProperties.ToolbarVisible = false;

            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Name = "Canvas";

            this.GrayWellIconsMode = true;
            this.TabIndex = 4;
            this.TabStop = false;
            this.MButtonPressed = false;
            this.MouseButtonDown = MouseButtons.None;
            this.CtrlPressed = false;
            this.ShiftPressed = false;
            _newContPen = new Pen(Color.Black, 1);
            AppendSumParams = false;

            _penFantom = new Pen(Brushes.Black, 1);
            _penFantom.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;


            _pen_Profile = new Pen(Brushes.Black, 1);
            _brushProfile = new SolidBrush(Color.Black);
            _fontSize = 7.25f;
            this.StateImgList = new ImageList();
            StateImgList.Images.Add(SmartPlus.Properties.Resources.unchecked16);
            StateImgList.Images.Add(SmartPlus.Properties.Resources.checked16);

            twActiveOilField = new ActiveTreeView(mainForm.pActiveOilField, this);
            twActiveOilField.StateImageList = StateImgList;

            twLayers = new LayersTreeView(mainForm.pInspector, this);
            twLayers.StateImageList = StateImgList;

            twWork = new TreeViewWorkLayers(mainForm.pWork, this);
            twWork.StateImageList = StateImgList;

            MButtonPoint = new Point();
            MButtonTimerPoint = new Point();
            if (mainForm.MaxMonitorHeight > 0)
            {
                BMPRect = new Rectangle(0, 0, mainForm.MaxMonitorWidth, mainForm.MaxMonitorHeight);
                BitMapRect = new Rectangle(0, 0, mainForm.MaxMonitorWidth, mainForm.MaxMonitorHeight);
            }
            ObjectsRect = new Rectangle(0, 0, 0, 0);

            tmr = new Timer();
            tmr.Enabled = false;
            tmr.Interval = 30;
            tmr.Tick += new EventHandler(tmr_Tick);

            shiftTmr = new Timer();
            shiftTmr.Enabled = false;
            shiftTmr.Interval = 50;
            shiftTmr.Tick += new EventHandler(shiftTmr_Tick);

            reLog = mainForm.reLog;
            bmp_grfx = Graphics.FromImage(bmp);

            TipTimer = new Timer();
            TipTimer.Enabled = false;
            TipTimer.Interval = 1000;
            TipTimer.Tick += new EventHandler(TipTimer_Tick);

            twActiveOilField.MouseDown += new MouseEventHandler(SetLastSelectedTreeView);
            twWork.MouseDown += new MouseEventHandler(SetLastSelectedTreeView);
            twLayers.MouseDown += new MouseEventHandler(SetLastSelectedTreeView);

            this.Resize += new EventHandler(CanvasMap_Resize);
            this.Paint += new PaintEventHandler(CanvasPaintHandler);
            this.MouseLeave += new EventHandler(CanvasMap_MouseLeave);
            this.MouseMove += new MouseEventHandler(CanvasMap_MouseMove);
            this.MouseDown += new MouseEventHandler(CanvasMap_MouseDown);
            this.MouseUp += new MouseEventHandler(CanvasMap_MouseUp);
            this.MouseWheel += new MouseEventHandler(CanvasMap_MouseWheel);
            this.MouseDoubleClick += new MouseEventHandler(CanvasMap_MouseDoubleClick);
        }

        // FONTS
        void InitFonts()
        {
            iconFonts = new Font[7];
            FontFamilyList = new FontFamily[7];
            iconFontSize = 7.25f;

            for (int i = 0; i < 7; i++)
            {
                switch (i)
                {
                    case 0:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.SmartFontFamily;
                        break;
                    case 1:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont1Family;
                        break;
                    case 2:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont2Family;
                        break;
                    case 3:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont3Family;
                        break;
                    case 4:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont4Family;
                        break;
                    case 5:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont5Family;
                        break;
                    case 6:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont6Family;
                        break;
                }
                iconFonts[i] = new Font(FontFamilyList[i], iconFontSize);
            }

            exFontList = new ExFontList(iconFonts);
            nameFonts = new Font[2];
            nameFonts[0] = new Font("Calibri", 7.27f);
            nameFonts[1] = new Font("Calibri", 7.27f, FontStyle.Bold);
        }
        void SetLastSelectedTreeView(object sender, MouseEventArgs e)
        {
            LastSelectedTreeView = (TreeView)sender;
        }

        // EVENT HANDLER
        void shiftTmr_Tick(object sender, EventArgs e)
        {
            if ((Math.Abs(MButtonTimerPoint.X - MButtonPoint.X) < 2) && (Math.Abs(MButtonTimerPoint.Y - MButtonPoint.Y) < 2))
            {
                if (ShiftAdhesion == 20)
                {
                    // Поиск ближайшей точки контура
                    int i, j, ind = -1;
                    C2DLayer layer;
                    Contour cntr;
                    Well w;
                    double scrX, scrY, d, mbX, mbY;

                    double maxD = 30, currD;
                    PointD xy, tempXY;
                    xy.X = Constant.DOUBLE_NAN;
                    xy.Y = Constant.DOUBLE_NAN;
                    for (i = 0; i < LayerList.Count; i++)
                    {
                        layer = (C2DLayer)LayerList[i];
                        if (layer.Contains((int)MButtonTimerPoint.X, (int)MButtonTimerPoint.Y))
                        {
                            tempXY = layer.GetPointByXY(MButtonTimerPoint.X, MButtonTimerPoint.Y, out currD);
                            if ((currD < maxD) && (tempXY.X != Constant.DOUBLE_NAN))
                            {
                                maxD = currD;
                                xy = tempXY;
                            }
                        }
                    }

                    for (i = 0; i < LayerWorkList.Count; i++)
                    {
                        layer = (C2DLayer)LayerWorkList[i];
                        tempXY = layer.GetPointByXY(MButtonTimerPoint.X, MButtonTimerPoint.Y, out currD);
                        if ((currD < maxD) && (tempXY.X != Constant.DOUBLE_NAN))
                        {
                            maxD = currD;
                            xy = tempXY;
                        }
                    }

                    if (xy.X != Constant.DOUBLE_NAN)
                    {
                        MButtonTimerPoint.X = C2DObject.ScreenXfromRealX(xy.X);
                        MButtonTimerPoint.Y = C2DObject.ScreenYfromRealY(xy.Y);
                        ShiftAdhesionRealPoint.X = xy.X;
                        ShiftAdhesionRealPoint.Y = xy.Y;
                        this.Invalidate();
                    }

                    ShiftAdhesion = 0;
                }
                else
                    ShiftAdhesion++;
            }
            else
            {
                ShiftAdhesion = 0;
                ShiftAdhesionRealPoint.X = Constant.DOUBLE_NAN;
                ShiftAdhesionRealPoint.Y = Constant.DOUBLE_NAN;
            }
            MButtonPoint.X = MButtonTimerPoint.X;
            MButtonPoint.Y = MButtonTimerPoint.Y;
        }
        void tmr_Tick(object sender, EventArgs e)
        {
            if ((Math.Abs(MButtonTimerPoint.X - MButtonPoint.X) > 2) ||
                (Math.Abs(MButtonTimerPoint.Y - MButtonPoint.Y) > 2))
            {
                if (EditedOilFieldIndex > -1)
                {
                    if (EditedLayerRotate)
                    {
                        if ((EditedLayerRotateCenter.X != -1) && (Math.Abs(MButtonTimerPoint.X - MButtonDownPoint.X) > 2) && (Math.Abs(MButtonTimerPoint.Y - MButtonDownPoint.Y) > 2))
                        {
                            RotateOilField(EditedOilFieldIndex);
                        }
                    }
                    else
                    {
                        float dX = MButtonTimerPoint.X - MButtonPoint.X;
                        float dY = MButtonTimerPoint.Y - MButtonPoint.Y;
                        MButtonPoint.X = MButtonTimerPoint.X;
                        MButtonPoint.Y = MButtonTimerPoint.Y;
                        TransferOilField(EditedOilFieldIndex, dX, dY, false);
                    }
                }
                else if (EditedLayer != null)
                {
                    if (EditedLayerRotate)
                    {
                        if ((EditedLayerRotateCenter.X != -1) && (Math.Abs(MButtonTimerPoint.X - MButtonDownPoint.X) > 2) && (Math.Abs(MButtonTimerPoint.Y - MButtonDownPoint.Y) > 2))
                        {
                            RotateLayer(EditedLayer);
                        }
                    }
                    else
                    {
                        float dX = MButtonTimerPoint.X - MButtonPoint.X;
                        float dY = MButtonTimerPoint.Y - MButtonPoint.Y;
                        MButtonPoint.X = MButtonTimerPoint.X;
                        MButtonPoint.Y = MButtonTimerPoint.Y;
                        TransferLayer(EditedLayer, dX, dY);
                    }
                }
                else
                {

                    BitMapRect.X -= (int)(MButtonTimerPoint.X - MButtonPoint.X);
                    BitMapRect.Y -= (int)(MButtonTimerPoint.Y - MButtonPoint.Y);
                    MButtonPoint.X = MButtonTimerPoint.X;
                    MButtonPoint.Y = MButtonTimerPoint.Y;
                }
                DrawLayerList();
            }
            //MButtonTimerPoint.X = MButtonPoint.X;
            //MButtonTimerPoint.Y = MButtonPoint.Y;
        }
        void CanvasMap_Resize(object sender, EventArgs e)
        {
            this.DrawLayerList();
        }
        void CanvasMap_MouseMove(object sender, MouseEventArgs e)
        {
            if (MapProject != null && !mainForm.UserDialogShowing)
            {
                if (Math.Abs(TipPoint.X - e.X) > 2 || Math.Abs(TipPoint.Y - e.Y) > 2)
                {
                    TipTimer.Start();
                    if (MapTip != null) MapTip.Dispose();
                }
                TipPoint = e.Location;
            }
            if (C2DObject.st_ObjectsRect != null)
            {
                double x = C2DObject.RealXfromScreenX(e.X);
                double y = C2DObject.RealYfromScreenY(e.Y);

                string status = String.Format("X:{0} Y:{1} ", (int)x, (int)y);
                C2DLayer layer;
                OilField of = null;
                for (int i = 0; i < LayerList.Count; i++)
                {
                    layer = (C2DLayer)LayerList[i];
                    if (layer.Visible)
                    {
                        of = layer.GetOilFieldByXY(e.X, e.Y);
                        if (of != null) break;
                    }
                }
                if (of != null)
                {
                    double locX, locY, D;
                    of.CoefDict.LocalCoordFromGlobal(x, y, out locX, out locY);
                    status += String.Format("[{0:D} ; {1:D}]", (int)locX, (int)locY);
                    if ((newContour != null) && (newContour._points.Count > 0))
                    {
                        PointD xy = newContour._points[newContour._points.Count - 1];
                        D = Math.Sqrt(Math.Pow(xy.X - x, 2) + Math.Pow(xy.Y - y, 2));
                        status += String.Format(" Длина, м: {0:D} м", (int)Math.Round(D));
                    }
                    List<C2DLayer> list = new List<C2DLayer>();
                    for (int i = 0; i < LayerList.Count; i++)
                    {
                        ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.GRID, list, true);
                    }
                    for (int i = 0; i < LayerWorkList.Count; i++)
                    {
                        ((C2DLayer)LayerWorkList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.GRID, list, true);
                    }
                    string zval = "";
                    Grid grid;
                    for (int i = list.Count - 1; i > -1; i--)
                    {
                        grid = (Grid)((C2DLayer)list[i]).ObjectsList[0];
                        if ((grid.DataLoaded) && (!grid.DataLoading))
                        {
                            int row = (int)((grid.Ymax - (grid.dy / 2) - y) / grid.dy);
                            int col = (int)((x - grid.Xmin + (grid.dx / 2)) / grid.dx);
                            double Zval = grid.GetZvalue(row, col);
                            if (Zval == grid.BlankVal) continue;
                            if ((row > -1) && (col > -1))
                            {
                                zval = string.Format(" Значение сетки: {0:0.##}", Zval);
                            }
                            break;
                        }
                    }
                    status += zval;
                }
                mainForm.SetStatus(status);
            }
            if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                ShiftPressed = true;
            }
            else
                ShiftPressed = false;

            if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
            {
                CtrlPressed = true;
            }
            else
                CtrlPressed = false;

            if ((MouseButtonDown == MouseButtons.Left) || (CtrlPressed) || (ShiftPressed))
            {
                MButtonTimerPoint.X = e.X;
                MButtonTimerPoint.Y = e.Y;
            }
            if ((snapShootMode) && (this.Cursor == Cursors.Default))
            {
                this.Cursor = Cursors.Cross;
            }
            if ((snapShootMode) && (MouseButtonDown == MouseButtons.Left) && (!SnapPt1.IsEmpty))
            {
                this.Invalidate();
            }
            if (!CtrlPressed && !ShiftPressed && MouseButtonDown == MouseButtons.None)
            {
                bool visBPContour = BPcontour == null, testBP = TestBizPlanParams(bmp_grfx, e.X, e.Y);
                bool testLB = false; TestLetterBoxClick(bmp_grfx, e.X, e.Y);
                if (((visBPContour && !testBP) || testBP) || testLB)
                {
                    Invalidate();
                }
            }
            if ((CtrlPressed) || (ShiftPressed)) this.Invalidate();
            if (!mainForm.UserDialogShowing) this.Focus();
        }
        void CanvasMap_MouseWheel(object sender, MouseEventArgs e)
        {
            if (EditedLayer != null) return;
            if ((MapProject != null) && (this.ClientRectangle.Contains(e.X, e.Y)))
            {
                TipTimer.Start();
                if (MapTip != null) MapTip.Dispose();
                // mainForm.StatUsage.AddMessage(CollectorStatId.CANVAS_ZOOM);
                double scrX, scrY;
                double scrW, scrH;
                double xCenter1, yCenter1, dx, dy;
                float imgxDpi, imgyDpi, oldImgxDpi, oldImgyDpi;
                scrX = e.X;
                scrY = e.Y;
                int deltaK;
                this.Cursor = Cursors.Default;
                scrW = this.Parent.Width;
                scrH = this.Parent.Height;

                deltaK = (int)Math.Abs(e.Delta / 120);

                if (deltaK > 4) deltaK = 4;
                while (deltaK > 0)
                {
                    xCenter1 = C2DObject.RealXfromScreenX(scrX);
                    yCenter1 = C2DObject.RealYfromScreenY(scrY);
                    oldImgxDpi = (float)C2DObject.st_XUnitsInOnePixel / 41.4352422066007f;
                    oldImgyDpi = (float)C2DObject.st_XUnitsInOnePixel / 41.4352422066007f;

                    if (e.Delta > 0)
                    {
                        if (C2DObject.st_XUnitsInOnePixel * 1.3 < 2000)
                        {
                            C2DObject.st_XUnitsInOnePixel *= 1.3;
                            C2DObject.st_VisbleLevel = C2DObject.st_XUnitsInOnePixel;
                            C2DObject.st_YUnitsInOnePixel = C2DObject.st_XUnitsInOnePixel;
                        }
                        else
                            return;
                    }
                    else
                    {
                        if (C2DObject.st_XUnitsInOnePixel / 1.3 > 0.1)
                        {
                            C2DObject.st_XUnitsInOnePixel /= 1.3;
                            C2DObject.st_VisbleLevel = C2DObject.st_XUnitsInOnePixel;
                            C2DObject.st_YUnitsInOnePixel = C2DObject.st_XUnitsInOnePixel;
                        }
                        else
                            return;
                    }

                    ReCalcFontSizes();


                    dx = (scrX - C2DObject.ScreenXfromRealX(xCenter1));
                    dy = (scrY - C2DObject.ScreenYfromRealY(yCenter1));

                    imgxDpi = (float)C2DObject.st_XUnitsInOnePixel / 41.4352422066007f;
                    imgyDpi = (float)C2DObject.st_YUnitsInOnePixel / 41.4352422066007f;

                    BitMapRect.X -= (int)dx;
                    BitMapRect.Y -= (int)dy;

                    DrawLayerList();
                    //if(deltaK % 2 == 0) 
                    Application.DoEvents();
                    deltaK -= 1;
                }
            }
        }
        void CanvasMap_MouseDown(object sender, MouseEventArgs e)
        {
            if (MapProject == null) return;
            MouseButtonDown = e.Button;
            if (RightClickLayer != null)
            {
                RightClickLayer.Selected = false;
                RightClickLayer = null;
            }
            // mainForm.StatUsage.AddMessage(CollectorStatId.CANVAS_MOUSE_DOWN);
            if (snapShootMode)
            {
                SnapPt1 = e.Location;
                MButtonTimerPoint.X = e.X;
                MButtonTimerPoint.Y = e.Y;
                MButtonPressed = true;
            }
            //else if(!CtrlPressed && !ShiftPressed)
            else if (((C2DObject.ShowAreasMode) && ((C2DObject.st_XUnitsInOnePixel > C2DObject.st_AreaXUnits) || ((C2DObject.st_XUnitsInOnePixel <= C2DObject.st_AreaXUnits) && (!(CtrlPressed || ShiftPressed))))) ||
                    ((!C2DObject.ShowAreasMode) && ((C2DObject.st_XUnitsInOnePixel > C2DObject.st_OilfieldXUnits) || ((C2DObject.st_XUnitsInOnePixel <= C2DObject.st_OilfieldXUnits) && (!(CtrlPressed || ShiftPressed))))))
            {
                MButtonDownPoint.X = e.X;
                MButtonDownPoint.Y = e.Y;
                MButtonPoint.X = e.X;
                MButtonPoint.Y = e.Y;
                MButtonTimerPoint.X = e.X;
                MButtonTimerPoint.Y = e.Y;
                MButtonPressed = true;
                tmr.Enabled = true;
            }
            else if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
            {
                ControlMouseDown();
            }
            else if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                if (!ShiftMouseDown())
                {
                    MButtonDownPoint.X = e.X;
                    MButtonDownPoint.Y = e.Y;
                    MButtonPoint.X = e.X;
                    MButtonPoint.Y = e.Y;
                    MButtonTimerPoint.X = e.X;
                    MButtonTimerPoint.Y = e.Y;
                    MButtonPressed = true;
                }
            }
            this.Focus();
        }
        void ControlMouseDown()
        {
            if (C2DObject.st_VisbleLevel <= C2DObject.st_OilfieldXUnits)
            {
                Point pt = this.PointToClient(Cursor.Position);
                if ((newContour == null) || (newContourEnd))
                {
                    if (!ShiftPressed)
                    {
                        for (int i = 0; i < selLayerList.Count; i++)
                        {
                            ((C2DLayer)selLayerList[i]).Selected = false;
                        }
                        ClearSelectedList();
                    }
                    ClearSelWellList();

                    newContour = new Contour(16);
                    newContourEnd = false;
                    newContour.Selected = true;
                    newContour._Fantom = true;
                }
                newContour.Add(C2DObject.RealXfromScreenX(pt.X), C2DObject.RealYfromScreenY(pt.Y));
                newContour.SetMinMax();
                DrawLayerList();
            }
        }
        bool ShiftMouseDown()
        {
            bool result = false;
            if ((C2DObject.st_VisbleLevel <= C2DObject.st_OilfieldXUnits) && (selLayerList.Count > 0))
            {
                Point pt = this.PointToClient(Cursor.Position);

                int i, index = -1;
                for (i = 0; i < selLayerList.Count; i++)
                {
                    if ((((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE))
                    {
                        index = i;
                        break;
                    }
                }
                if (index > -1)
                {
                    Well w;
                    double scrX, scrY, d, minD;
                    int k = -1;
                    minD = 25;

                    for (i = 0; i < selWellList.Count; i++)
                    {
                        w = (Well)selWellList[i];
                        scrX = C2DObject.ScreenXfromRealX(w.X);
                        scrY = C2DObject.ScreenYfromRealY(w.Y);
                        d = Math.Sqrt(Math.Pow(scrX - pt.X, 2) + Math.Pow(scrY - pt.Y, 2));
                        if ((d < 20) && (d < minD))
                        {
                            k = i;
                            minD = d;
                        }
                    }
                    if (k != -1)
                    {
                        //mainForm.corSchRemoveWell((Well)selWellList[k]);
                    }
                    else // добавляем скважину в профиль
                    {
                        int j, lrIndex = -1;
                        double rX, rY;
                        rX = C2DObject.RealXfromScreenX(pt.X);
                        rY = C2DObject.RealYfromScreenY(pt.Y);
                        C2DLayer layer;
                        List<C2DLayer> list = new List<C2DLayer>();
                        for (i = 0; i < LayerList.Count; i++)
                        {
                            layer = (C2DLayer)LayerList[i];
                            layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list, true);
                        }
                        k = -1; minD = 25;
                        for (i = 0; i < list.Count; i++)
                        {
                            layer = (C2DLayer)list[i];
                            //if ((layer != null) && 
                            if (layer.ObjRect.Contains(rX, rY))
                            {
                                for (j = 0; j < layer.ObjectsList.Count; j++)
                                {
                                    w = (Well)layer.ObjectsList[j];
                                    if (!w.Selected)
                                    {
                                        scrX = C2DObject.ScreenXfromRealX(w.X);
                                        scrY = C2DObject.ScreenYfromRealY(w.Y);
                                        d = Math.Sqrt(Math.Pow(scrX - pt.X, 2) + Math.Pow(scrY - pt.Y, 2));
                                        if ((d < 20) && (d < minD))
                                        {
                                            lrIndex = i;
                                            k = j;
                                            minD = d;
                                        }
                                    }
                                }
                            }
                        }
                        if ((lrIndex != -1) && (k != -1))
                        {
                            w = (Well)((C2DLayer)list[lrIndex]).ObjectsList[k];
                            selWellList.Add(w);
                            mainForm.corSchAddWell(selWellList.Count - 1);
                            result = true;
                        }
                    }
                }
                Contour selContour = null;
                for (i = 0; i < selLayerList.Count; i++)
                {
                    if (((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                    {
                        selContour = (Contour)((C2DLayer)selLayerList[i]).ObjectsList[0];
                        if (selContour._ContourType == 0)
                        {
                            ShiftSelContourPoint = selContour.GetIndexPointByXY(pt.X, pt.Y, 12);

                            if (ShiftSelContourPoint > -1)
                            {
                                ShiftSelContourIndex = i;
                                break;
                            }
                        }
                    }
                }
                if ((selContour != null) && (selContour._ContourType == 0))
                {
                    if (MouseButtonDown == MouseButtons.Left)
                    {
                        if ((ShiftSelContourIndex > -1) && (ShiftSelContourPoint > -1))
                        {
                            shiftTmr.Enabled = true;
                            result = true;
                            this.Cursor = Cursors.Cross;
                        }
                    }
                    else if (MouseButtonDown == MouseButtons.Right)
                    {
                        if ((ShiftSelContourIndex > -1) && (ShiftSelContourPoint > -1))
                        {
                            if (selContour._points.Count > 2)
                            {
                                selContour.DelPointByIndex(ShiftSelContourPoint);
                                result = true;
                                LayerWorkListChanged = true;
                                DrawLayerList();
                            }
                            ShiftSelContourPoint = -1;
                            ShiftSelContourIndex = -1;
                        }
                        else
                        {
                            int numRib = -1;
                            double rX, rY, rX3, rY3, x1, y1, x2 = 0, y2 = 0;
                            double scrX1, scrX2;
                            double minX, maxX, minY, maxY;
                            double k, b;
                            bool bx2;
                            bool cancel = false;
                            rX = C2DObject.RealXfromScreenX(pt.X);
                            rY = C2DObject.RealYfromScreenY(pt.Y);
                            numRib = selContour.GetIndexRibByXY(pt.X, pt.Y);
                            if (numRib > -1)
                            {
                                x1 = selContour._points[numRib].X;
                                y1 = selContour._points[numRib].Y;
                                bx2 = false;
                                if (numRib < selContour._points.Count - 1)
                                {
                                    x2 = selContour._points[numRib + 1].X;
                                    y2 = selContour._points[numRib + 1].Y;
                                    bx2 = true;
                                }
                                else if (selContour._Closed)
                                {
                                    x2 = selContour._points[0].X;
                                    y2 = selContour._points[0].Y;
                                    bx2 = true;
                                }
                                minX = x1;
                                maxX = x2;
                                if (x1 > x2)
                                {
                                    minX = x2;
                                    maxX = x1;
                                }
                                minY = y1;
                                maxY = y2;
                                if (y1 > y2)
                                {
                                    minY = y2;
                                    maxY = y1;
                                }
                                if (bx2)
                                {
                                    if ((x1 != x2) && (Math.Abs((y2 - y1) / (x2 - x1)) < 28))
                                    {
                                        if ((y1 != y2) && (Math.Abs((y2 - y1) / (x2 - x1)) > 0.05))
                                        {
                                            k = (y2 - y1) / (x2 - x1);
                                            b = y1 - k * x1;
                                            rY3 = (k * rX + rY + b) / 2;
                                            rX3 = (rY3 - b) / k;
                                        }
                                        else
                                        {
                                            rY3 = minY + (maxY - minY) * (rX - minX) / (maxX - minX);
                                            rX3 = rX;
                                        }
                                    }
                                    else
                                    {
                                        rX3 = minX + (maxX - minX) * (rY - minY) / (maxY - minY);
                                        rY3 = rY;
                                    }
                                    result = true;
                                    LayerWorkListChanged = true;
                                    selContour.Insert(numRib + 1, rX3, rY3);
                                }
                                DrawLayerList();
                            }
                        }
                    }
                }
            }
            return result;
        }
        void CanvasMap_MouseUp(object sender, MouseEventArgs e)
        {
            if (MapProject == null) return;
            if (MButtonPressed)
            {
                tmr.Enabled = false;
                if (SnapShootMode)
                {
                    SnapToClipboard();
                }
                else if (EditedOilFieldIndex > -1) // режим редактирования координат
                {
                    if (EditedLayerRotate)
                    {
                        if (EditedLayerRotateCenter.X == -1)
                        {
                            EditedLayerRotateCenter.X = e.X;
                            EditedLayerRotateCenter.Y = e.Y;
                        }
                        else
                        {
                            RotateOilField(EditedOilFieldIndex);
                        }
                    }
                    else
                    {
                        float dX = MButtonTimerPoint.X - MButtonPoint.X;
                        float dY = MButtonTimerPoint.Y - MButtonPoint.Y;
                        TransferOilField(EditedOilFieldIndex, dX, dY, true);
                    }
                    DrawLayerList();
                    this.Cursor = Cursors.Default;
                }
                else if (EditedLayer != null)
                {
                    if (EditedLayerRotate)
                    {
                        if (EditedLayerRotateCenter.X == -1)
                        {
                            EditedLayerRotateCenter.X = e.X;
                            EditedLayerRotateCenter.Y = e.Y;
                        }
                        else
                        {
                            RotateLayer(EditedLayer);
                            EditedLayer.SetRotateCoef();
                        }
                    }
                    else
                    {
                        float dX = MButtonTimerPoint.X - MButtonPoint.X;
                        float dY = MButtonTimerPoint.Y - MButtonPoint.Y;
                        TransferLayer(EditedLayer, dX, dY);
                    }
                    DrawLayerList();
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    // режим движения карты
                    if ((Math.Abs(MButtonTimerPoint.X - MButtonDownPoint.X) > 5) || (Math.Abs(MButtonTimerPoint.Y - MButtonDownPoint.Y) > 5))
                    {
                        BitMapRect.X -= (int)(MButtonTimerPoint.X - MButtonPoint.X);
                        BitMapRect.Y -= (int)(MButtonTimerPoint.Y - MButtonPoint.Y);
                        DrawLayerList();
                        this.Cursor = Cursors.Default;
                    }
                    else if (this.showBizPlan && BPcontour != null && this.Cursor == Cursors.Hand) // отображение Бизнес-плана
                    {
                        C2DLayer layer = null;
                        for (int k = 0; k < LayerList.Count; k++)
                        {
                            layer = GetLayerByObject((C2DLayer)LayerList[k], BPcontour);
                            if (layer != null) break;
                        }
                        if (layer != null)
                        {
                            ClearSelectedList();
                            ClearSumList();
                            ClearSelWellList();
                            mainForm.dChartSetShowBizParams(true);
                            if (BPcontour._ContourType == -2)
                            {
                                ClearActiveOilField();
                                AddSelectLayer(layer);
                                AddSumNGDU(BPcontour);
                                if (layer != null) mainForm.dChartSetWindowText("Графики - " + layer.Name);
                            }
                            else if (BPcontour._ContourType == -3)
                            {
                                string ngdu = string.Empty;
                                AddSelectLayer(layer);
                                if ((layer != null) && (layer.oilfield != null))
                                {
                                    SetActiveOilField(layer.oilFieldIndex);
                                    mainForm.dAreasSetAreaRow(layer.oilFieldIndex, -1);
                                    ngdu = " [" + layer.oilfield.ParamsDict.NGDUName + "]";
                                }

                                AddSumOilField(BPcontour);
                                mainForm.dChartSetWindowText(String.Format("Графики - Месторождение {0}{1}", layer.Name, ngdu));
                            }
                            DrawLayerList();
                        }
                        mainForm.dActiveWindow("pGraphics");
                    }
                    else if (this.showLetterBox && LetterBoxObject != null && this.Cursor == Cursors.Hand)
                    {
                        mainForm.dActiveWindow("pDiscussion");
                        LastSelectObject = LetterBoxObject;
                        if (OnChangeSelectedObject != null) OnChangeSelectedObject(LastSelectObject);
                        
                        if (LetterBoxObject.TypeID == Constant.BASE_OBJ_TYPES_ID.WELL)
                        {
                            Well w = (Well)LetterBoxObject;
                            SetSelectedObj(w);
                            if ((w != null) && (!mainForm.worker.IsBusy))
                            {
                                // mainForm.StatUsage.AddMessage(CollectorStatId.CLICK_WELL);
                                selWellList.Add(w);

                                mainForm.filterOilObj.SetWellOilObj(w, true);
                                mainForm.filterOilObj.UpdateCurrentTreeView();
                                selMerOilObjCodes.Clear();
                                if (w.ProjectDest == 0)
                                {
                                    mainForm.dGridShowChess(w.OilFieldIndex, w.Index);
                                    mainForm.dGridShowMer(w.OilFieldIndex, w.Index);
                                    mainForm.dGridShowGIS(w.OilFieldIndex, w.Index);
                                    mainForm.dGridShowCore(w.OilFieldIndex, w.Index);
                                    mainForm.dGridShowCoreTest(w.OilFieldIndex, w.Index);
                                    mainForm.dChartShowData(w.OilFieldIndex, w.Index);

                                    mainForm.planeLoadWell(w);
                                    mainForm.WL_SelectWell(w);
                                }
                                else
                                {
                                    mainForm.dGridShowChess(-1, -1);
                                    mainForm.dGridShowMer(-1, -1);
                                    mainForm.dGridShowGIS(-1, -1);
                                    mainForm.dGridShowCore(-1, -1);
                                    mainForm.dGridShowCoreTest(-1, -1);
                                    mainForm.dChartShowData(-1, -1);
                                    mainForm.planeLoadWell(null);
                                }
                                SetActiveOilField(w.OilFieldIndex);
                            }
                        }
                    }
                    else // режим выбора скважины месторождения нгду
                    {
                        int i, j = 0, workj = 0, selWell = -1, selCntr = -1, selCntrWork = -1;
                        C2DLayer layer, selWorkLayer = null, selLayer = null, condLr;
                        Contour cntr;
                        List<C2DLayer> layers;
                        double realX, realY;
                        ShiftSelContourPoint = -1;
                        ShiftSelContourIndex = -1;
                        PreparePaint();
                        AddWellProfileMode = false;
                        LastSelectObject = null;
                        if (!ShiftPressed) ClearSelectedList();
                        if ((!ShiftPressed) || (sumIndexes.Count == 0)) ClearSumList();
                        ClearSelWellList();

                        if (C2DObject.st_XUnitsInOnePixel > 2000)          // уровень РБ
                        {
                            for (i = 0; i < LayerList.Count; i++)
                            {
                                layer = (C2DLayer)LayerList[i];
                                if (layer.Visible)
                                {
                                    condLr = layer.GetCondContourByXY(e.X, e.Y);
                                    if (condLr != null)
                                    {
                                        // mainForm.StatUsage.AddMessage(CollectorStatId.CLICK_REGION);
                                        cntr = (Contour)condLr.ObjectsList[0];
                                        //mainForm.filterOilObj.SetRegionOilObj(cntr._EnterpriseCode, selLayerList.Count == 0);
                                        //mainForm.filterOilObj.UpdateCurrentTreeView();
                                        ClearActiveOilField();
                                        AddSelectLayer(condLr);
                                        AddSumRegion(cntr);
                                        layers = GetLayerListByContourType(-1);
                                        if (layers.Count > 0)
                                        {
                                            Contour contour = (Contour)((C2DLayer)layers[0]).ObjectsList[0];
                                            LastSelectObject = new BaseObj(Constant.BASE_OBJ_TYPES_ID.PROJECT);
                                            LastSelectObject.Name = contour.Name;
                                            LastSelectObject.Code = contour._EnterpriseCode;
                                        }
                                        if (layers.Count == 1)
                                        {
                                            mainForm.dChartSetWindowText("Графики - Регион" + condLr.Name);
                                        }
                                        else if (layers.Count > 1)
                                        {
                                            mainForm.dChartSetWindowText("Графики - Список Регионов [" + layers.Count.ToString() + "]");
                                        }
                                        else
                                        {
                                            mainForm.dChartSetWindowText("Графики");
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        else if (C2DObject.st_XUnitsInOnePixel > C2DObject.st_NgduXUnits)     // уровень нгду
                        {
                            bool selectNGDU = false;
                            for (i = 0; i < LayerList.Count; i++)
                            {
                                layer = (C2DLayer)LayerList[i];

                                if (layer.Visible)
                                {
                                    condLr = layer.GetCondContourByXY(e.X, e.Y);
                                    if (condLr != null)
                                    {
                                        // mainForm.StatUsage.AddMessage(CollectorStatId.CLICK_NGDU);
                                        cntr = (Contour)condLr.ObjectsList[0];
                                        //mainForm.filterOilObj.SetNGDUOilObj(cntr._NGDUCode, selLayerList.Count == 0);
                                        //mainForm.filterOilObj.UpdateCurrentTreeView();
                                        //mainForm.dChartEnableByOilObjButtons(true);
                                        ClearActiveOilField();
                                        AddSelectLayer(condLr);
                                        AddSumNGDU(cntr);
                                        layers = GetLayerListByContourType(-2);
                                        if (layers.Count > 0)
                                        {
                                            cntr = (Contour)((C2DLayer)layers[j]).ObjectsList[0];
                                            LastSelectObject = new BaseObj(Constant.BASE_OBJ_TYPES_ID.NGDU);
                                            LastSelectObject.Name = cntr.Name;
                                            LastSelectObject.Code = 0;
                                            LastSelectObject.Index = cntr._NGDUCode;
                                        }

                                        if (layers.Count == 1)
                                        {
                                            mainForm.dChartSetWindowText("Графики - " + layers[0].Name);
                                        }
                                        else if (layers.Count > 1)
                                        {
                                            mainForm.dChartSetWindowText("Графики - Список НГДУ [" + layers.Count.ToString() + "]");
                                        }
                                        else
                                        {
                                            mainForm.dChartSetWindowText("Графики");
                                        }

                                        selectNGDU = true;
                                        break;

                                    }
                                }
                            }
                            if (!selectNGDU)
                            {
                                double xUnitsSave = C2DObject.st_VisbleLevel;
                                realX = C2DObject.RealXfromScreenX(e.X);
                                C2DObject.st_VisbleLevel = 2500;
                                double scrX = C2DObject.ScreenXfromRealX(realX);
                                for (i = 0; i < LayerList.Count; i++)
                                {
                                    layer = (C2DLayer)LayerList[i];

                                    if (layer.Visible)
                                    {
                                        condLr = layer.GetCondContourByXY((int)scrX, e.Y);
                                        if (condLr != null)
                                        {
                                            // mainForm.StatUsage.AddMessage(CollectorStatId.CLICK_REGION);
                                            cntr = (Contour)condLr.ObjectsList[0];
                                            //mainForm.filterOilObj.SetRegionOilObj(cntr._EnterpriseCode, selLayerList.Count == 0);
                                            //mainForm.filterOilObj.UpdateCurrentTreeView();
                                            //mainForm.dChartEnableByOilObjButtons(true);
                                            ClearActiveOilField();
                                            AddSelectLayer(condLr);
                                            AddSumRegion(cntr);
                                            layers = GetLayerListByContourType(-1);
                                            if (layers.Count > 0)
                                            {
                                                Contour contour = (Contour)((C2DLayer)layers[0]).ObjectsList[0];
                                                LastSelectObject = new BaseObj(Constant.BASE_OBJ_TYPES_ID.PROJECT);
                                                LastSelectObject.Name = contour.Name;
                                                LastSelectObject.Code = contour._EnterpriseCode;
                                            }
                                            if (layers.Count == 1)
                                            {
                                                mainForm.dChartSetWindowText("Графики - Регион " + layers[0].Name);
                                            }
                                            else if (layers.Count > 1)
                                            {
                                                mainForm.dChartSetWindowText("Графики - Список Регионов [" + layers.Count.ToString() + "]");
                                            }
                                            else
                                            {
                                                mainForm.dChartSetWindowText("Графики");
                                            }
                                            break;
                                        }
                                    }
                                }
                                C2DObject.st_VisbleLevel = xUnitsSave;
                            }
                        }
                        else if (C2DObject.st_XUnitsInOnePixel > C2DObject.st_OilfieldXUnits) // уровень месторождения
                        {
                            bool find = false;
                            for (i = 0; i < LayerList.Count; i++)
                            {
                                layer = (C2DLayer)LayerList[i];

                                if (layer.Visible)
                                {
                                    condLr = layer.GetCondContourByXY(e.X, e.Y);
                                    if (condLr != null)
                                    {
                                        // mainForm.StatUsage.AddMessage(CollectorStatId.CLICK_OILFIELD);
                                        AddSelectLayer(condLr);
                                        C2DLayer lr = null;
                                        if (selLayerList.Count > 0) lr = (C2DLayer)selLayerList[selLayerList.Count - 1];
                                        cntr = (Contour)condLr.ObjectsList[0];
                                        string ngdu = "";
                                        if ((lr != null) && (lr.oilfield != null))
                                        {
                                            //mainForm.filterOilObj.SetOilFieldOilObj(lr._OilField, lr._OilField == condLr._OilField);
                                            //mainForm.filterOilObj.UpdateCurrentTreeView();
                                            SetActiveOilField(lr.oilFieldIndex);
                                            mainForm.dAreasSetAreaRow(lr.oilFieldIndex, -1);
                                            ngdu = " [" + lr.oilfield.ParamsDict.NGDUName + "]";
                                        }

                                        AddSumOilField(cntr);
                                        if (selLayerList.Count > 0 && ((C2DLayer)selLayerList[0]).oilfield != null)
                                        {
                                            LastSelectObject = ((C2DLayer)selLayerList[0]).oilfield;
                                        }
                                        if (selLayerList.Count == 1)
                                        {
                                            mainForm.dChartSetWindowText(String.Format("Графики - Месторождение {0}{1}", lr.Name, ngdu));
                                        }
                                        else if (selLayerList.Count > 1)
                                        {
                                            mainForm.dChartSetWindowText("Графики - Список Месторождений [" + selLayerList.Count.ToString() + "]");
                                        }
                                        else
                                            mainForm.dChartSetWindowText("Графики");

                                        find = true;
                                        break;
                                    }
                                }
                            }
                        }
                        else if ((C2DObject.st_XUnitsInOnePixel > C2DObject.st_AreaXUnits) && (C2DObject.ShowAreasMode)) // уровень ячеек заводнения
                        {
                            C2DObject c2dArea = null;
                            Area area;
                            ArrayList SelLayers = new ArrayList();
                            var dictArea = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                            //ClearSelectedList(false);
                            j = 0;
                            for (i = 0; i < LayerList.Count; i++)
                            {
                                if (((C2DLayer)LayerList[i]).Visible)
                                {
                                    ((C2DLayer)LayerList[i]).FillAdvContains(e.X, e.Y, SelLayers);
                                }
                            }
                            for (i = 0; i < SelLayers.Count; i++)
                            {
                                layer = (C2DLayer)SelLayers[i];
                                c2dArea = layer.GetAreaByXY(e.X, e.Y);
                                if (c2dArea != null) break;
                            }

                            if (c2dArea != null)
                            {
                                // mainForm.StatUsage.AddMessage(CollectorStatId.CLICK_AREA);
                                area = (Area)c2dArea;
                                C2DLayer lrArea = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, -1);
                                lrArea.Add(area.contour);
                                lrArea.oilfield = MapProject.OilFields[area.OilFieldIndex];
                                lrArea.Name = area.contour.Name;
                                lrArea.Level = area.Index;
                                lrArea.GetObjectsRect();
                                mainForm.filterOilObj.SetAreaOilObj(area, selLayerList.Count == 0);
                                mainForm.filterOilObj.UpdateCurrentTreeView();
                                //mainForm.dChartEnableByOilObjButtons(true);

                                AddSelectLayer(lrArea);
                                AddSumArea(area);

                                if (selLayerList.Count > 0)
                                {
                                    LastSelectObject = ((C2DLayer)selLayerList[0]).oilfield.Areas[((C2DLayer)selLayerList[0]).Level];
                                }
                                if (selLayerList.Count == 1)
                                {
                                    C2DLayer lr = (C2DLayer)selLayerList[0];
                                    mainForm.dChartSetWindowText(String.Format("Графики - {0} [{1}, {2}]", lr.Name, lr.oilfield.ParamsDict.NGDUName, lr.oilfield.Name));
                                }
                                else if (selLayerList.Count > 1)
                                {
                                    mainForm.dChartSetWindowText("Графики - Список ячеек [" + selLayerList.Count.ToString() + "]");
                                }
                                else
                                    mainForm.dChartSetWindowText("Графики");

                                mainForm.pgProperties.SelectedObject = sArea;
                                mainForm.dAreasSetAreaRow(area.OilFieldIndex, area.Index);
                                sArea.Name = c2dArea.Name;
                                sArea.OilFieldName = (MapProject.OilFields[area.OilFieldIndex]).Name;
                                sArea.OilFieldAreaName = "";
                                sArea.OilFieldAreaName = dictArea.GetShortNameByCode(area.OilFieldAreaCode);
                                mainForm.pgProperties.Refresh();
                                SetActiveOilField(area.OilFieldIndex);
                                if (e.Button == MouseButtons.Right)
                                {
                                    ShowContextMenu(lrArea, e.Location);
                                }
                            }
                        }
                        else // уровень скважин
                        {
                            double currD, currCntrD, currPrflD, maxD = Constant.DOUBLE_NAN, maxCntrD = Constant.DOUBLE_NAN;
                            ArrayList SelLayers = new ArrayList();
                            ArrayList SelWorkLayers = new ArrayList();
                            Well w = null, wx = null;
                            C2DObject c2dWell = null, c2dWellX = null;
                            C2DLayer cntrLayer = null, prflLayer = null, gridLayer = null, voronoiLayer = null, depositLayer = null;
                            Contour cntrx = null;
                            Profile prflx = null;
                            Grid gridx = null;
                            var dict = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);

                            // снятие selected со всех скважин и контуров
                            ClearSelectedList();

                            selMerOilObjCodes.Clear();

                            List<C2DLayer> list = new List<C2DLayer>();
                            Constant.BASE_OBJ_TYPES_ID[] objTypeList = new Constant.BASE_OBJ_TYPES_ID[5];
                            objTypeList[0] = Constant.BASE_OBJ_TYPES_ID.WELL;
                            objTypeList[1] = Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL;
                            objTypeList[2] = Constant.BASE_OBJ_TYPES_ID.CONTOUR;
                            objTypeList[3] = Constant.BASE_OBJ_TYPES_ID.GRID;
                            objTypeList[4] = Constant.BASE_OBJ_TYPES_ID.DEPOSIT;

                            for (i = 0; i < LayerList.Count; i++)
                            {
                                layer = (C2DLayer)LayerList[i];
                                layer.VisibleFar = true;
                                layer.FillLayersByObjTypeID(objTypeList, list, true);
                            }

                            objTypeList = new Constant.BASE_OBJ_TYPES_ID[5];
                            objTypeList[0] = Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL;
                            objTypeList[1] = Constant.BASE_OBJ_TYPES_ID.CONTOUR;
                            objTypeList[2] = Constant.BASE_OBJ_TYPES_ID.PROFILE;
                            objTypeList[3] = Constant.BASE_OBJ_TYPES_ID.GRID;
                            objTypeList[4] = Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP;

                            for (i = 0; i < LayerWorkList.Count; i++)
                            {
                                layer = (C2DLayer)LayerWorkList[i];
                                layer.VisibleFar = true;
                                layer.FillLayersByObjTypeID(objTypeList, list, true);
                            }

                            // выбор скважины/контура
                            j = 0;
                            for (i = 0; i < list.Count; i++)
                            {
                                if (((C2DLayer)list[i]).Visible)
                                {
                                    ((C2DLayer)list[i]).FillAdvContains(e.X, e.Y, SelLayers);
                                }
                            }

                            //workj = 0;
                            //for (i = 0; i < LayerWorkList.Count; i++)
                            //{
                            //    if (((C2DLayer)LayerWorkList[i]).Visible)
                            //    {
                            //        ((C2DLayer)LayerWorkList[i]).FillAdvContains(e.X, e.Y, SelWorkLayers);
                            //    }
                            //}

                            //for (i = 0; i < SelLayers.Count; i++)
                            //{
                            //    layer = (C2DLayer)SelLayers[i];
                            //    //c2dArea = layer.GetAreaByXY(e.X, e.Y);
                            //    if (c2dArea != null) break;
                            //    c2dWell = layer.SelectWellbyXY(e.X, e.Y, out currD);
                            //    currCntrD = 0;
                            //    //cntrLayer = layer.SelectContourByXY(e.X, e.Y, out currCntrD);

                            //    if ((c2dWell != null) && (currD < (50 * Math.Pow(1.5, (int)(XUnitsInOnePixel / 5)))) && ((maxD == Constant.DOUBLE_NAN) || (maxD > currD)))
                            //    {
                            //        maxD = currD;
                            //        selWell = i;
                            //        c2dWellX = c2dWell;
                            //    }
                            //    if ((cntrLayer != null) && (currCntrD < 20) && ((maxCntrD == Constant.DOUBLE_NAN) || (maxCntrD > currCntrD)))
                            //    {
                            //        maxCntrD = currCntrD;
                            //        selLayer = cntrLayer;
                            //        cntrx = (Contour)cntrLayer._ObjectsList[0];
                            //    }

                            //}

                            for (i = 0; i < SelLayers.Count; i++)
                            {
                                layer = (C2DLayer)SelLayers[i];
                                c2dWell = layer.SelectWellbyXY(e.X, e.Y, out currD);
                                cntrLayer = layer.SelectContourByXY(e.X, e.Y, out currCntrD);
                                prflLayer = layer.SelectProfileByXY(e.X, e.Y, out currPrflD);
                                gridLayer = layer.SelectGridByXY(e.X, e.Y);
                                voronoiLayer = layer.SelectVoronoiMapByXY(e.X, e.Y, Control.ModifierKeys == Keys.Shift, e.Button == MouseButtons.Right);
                                depositLayer = layer.TestDepositByXY(e.X, e.Y, Control.ModifierKeys == Keys.Shift, e.Button == MouseButtons.Right);

                                if (gridLayer != null)
                                {
                                    selWorkLayer = gridLayer;
                                    gridx = (Grid)gridLayer.ObjectsList[0];
                                }
                                if ((c2dWell != null) && (currD < (50 * Math.Pow(1.5, (int)(C2DObject.st_XUnitsInOnePixel / 5)))) &&
                                    ((maxD == Constant.DOUBLE_NAN) || (maxD >= currD)))
                                {
                                    maxD = currD;
                                    selWell = i;
                                    c2dWellX = c2dWell;
                                    selWorkLayer = voronoiLayer;
                                }
                                if ((cntrLayer != null) && (currCntrD < 20) && ((maxCntrD == Constant.DOUBLE_NAN) || (maxCntrD >= currCntrD)))
                                {
                                    maxCntrD = currCntrD;
                                    selWorkLayer = cntrLayer;
                                    cntrx = (Contour)cntrLayer.ObjectsList[0];
                                }
                                if ((prflLayer != null) && (currPrflD < 20) && ((maxCntrD == Constant.DOUBLE_NAN) || (maxCntrD >= currPrflD)))
                                {
                                    maxCntrD = currPrflD;
                                    selWorkLayer = prflLayer;
                                    prflx = (Profile)prflLayer.ObjectsList[0];
                                }
                                if (selWorkLayer == null || (selWorkLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID && voronoiLayer != null))
                                {
                                    selWorkLayer = voronoiLayer;
                                }
                                if (selWorkLayer == null || (selWorkLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID && depositLayer != null))
                                {
                                    selWorkLayer = depositLayer;
                                }
                            }

                            //выбор скважины
                            bool find = false;
                            if (/* (e.Button == MouseButtons.Left) && */(selWell > -1) && (selWell < SelLayers.Count)
                                && (selLayer == null))
                            {
                                layer = (C2DLayer)SelLayers[selWell];

                                if (c2dWellX != null)
                                {
                                    c2dWellX.Selected = true;
                                    mainForm.pgProperties.SelectedObject = swells;
                                    switch (c2dWellX.TypeID)
                                    {
                                        case Constant.BASE_OBJ_TYPES_ID.WELL:
                                            wx = (Well)c2dWellX;
                                            SetSelectedObj(wx);
                                            break;
                                        case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                                            wx = (Well)c2dWellX;
                                            break;
                                        case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                                            wx = ((PhantomWell)c2dWellX).srcWell;
                                            SetSelectedObj((PhantomWell)c2dWellX);
                                            break;
                                    }
                                    if ((wx != null) && (!mainForm.worker.IsBusy))
                                    {
                                        // mainForm.StatUsage.AddMessage(CollectorStatId.CLICK_WELL);
                                        selWellList.Add(wx);

                                        mainForm.filterOilObj.SetWellOilObj(wx, true);
                                        mainForm.filterOilObj.UpdateCurrentTreeView();
                                        selMerOilObjCodes.Clear();
                                        if (wx.ProjectDest == 0)
                                        {
                                            mainForm.dGridShowChess(wx.OilFieldIndex, wx.Index);
                                            mainForm.dGridShowMer(wx.OilFieldIndex, wx.Index);
                                            mainForm.dGridShowGIS(wx.OilFieldIndex, wx.Index);
                                            mainForm.dGridShowCore(wx.OilFieldIndex, wx.Index);
                                            mainForm.dGridShowCoreTest(wx.OilFieldIndex, wx.Index);
                                            mainForm.dChartShowData(wx.OilFieldIndex, wx.Index);

                                            mainForm.planeLoadWell(wx);
                                            mainForm.WL_SelectWell(wx);
                                            if (selWellList.Count > 0)
                                            {
                                                LastSelectObject = (Well)selWellList[0];
                                            }
                                        }
                                        else
                                        {
                                            mainForm.dGridShowChess(-1, -1);
                                            mainForm.dGridShowMer(-1, -1);
                                            mainForm.dGridShowGIS(-1, -1);
                                            mainForm.dGridShowCore(-1, -1);
                                            mainForm.dGridShowCoreTest(-1, -1);
                                            mainForm.dChartShowData(-1, -1);
                                            mainForm.planeLoadWell(null);
                                        }
                                        SetActiveOilField(wx.OilFieldIndex);
                                        find = true;
                                    }
                                }
                            }

                            //выбор
                            if (!find && ((selLayer != null) || (selWorkLayer != null)))
                            {
                                if (selWorkLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DEPOSIT)
                                {
                                    selWorkLayer.SelectDepositByXY(e.X, e.Y, Control.ModifierKeys == Keys.Shift, e.Button == MouseButtons.Right);
                                }
                                if (e.Button == MouseButtons.Left)
                                {
                                    layer = null;
                                    if (selWorkLayer != null) layer = selWorkLayer;

                                    if ((cntrx != null) && (layer != null))
                                    {

                                        if ((twWork.SelectedNodes.Count == 0) || (!twWork.SelectedNodes.Contains(layer.node)))
                                        {
                                            if (layer.node != null) twWork.SetOneSelectedNode(layer.node);
                                        }
                                        else
                                        {
                                            AddSelectLayer(layer);
                                        }
                                        find = true;
                                        if (voronoiLayer != null) voronoiLayer.Selected = false;
                                    }
                                }
                                else if (e.Button == MouseButtons.Right)
                                {
                                    ShowContextMenu(selWorkLayer, e.Location);
                                }
                            }
                            //выбор профиля
                            if ((!find) && (e.Button == MouseButtons.Left) && ((selLayer != null) || (selWorkLayer != null)))
                            {
                                layer = null;
                                if (selWorkLayer != null) layer = selWorkLayer;

                                if ((prflx != null) && (layer != null))
                                {
                                    if ((layer.node != null) && (selLayerList.IndexOf(layer) == -1))
                                    {
                                        selLayerList.Add(layer);
                                        mainForm.corSchLoadProfile(layer);
                                    }
                                    find = true;
                                    if (voronoiLayer != null) voronoiLayer.Selected = false;
                                }
                            }
                            // если ничего не найдено
                            if (!find)
                            {
                                for (i = 0; i < LayerList.Count; i++)
                                {
                                    layer = (C2DLayer)LayerList[i];

                                    if (layer.Visible)
                                    {
                                        condLr = layer.GetCondContourByXY(e.X, e.Y);
                                        if (condLr != null)
                                        {
                                            cntr = (Contour)condLr.ObjectsList[0];
                                            if (condLr.oilfield != null)
                                            {
                                                mainForm.filterOilObj.SetOilFieldOilObj(condLr.oilfield, selLayerList.Count == 0);
                                                mainForm.filterOilObj.UpdateCurrentTreeView();
                                                SetActiveOilField(condLr.oilFieldIndex);
                                                LastSelectObject = condLr.oilfield;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            if (selWorkLayer != null && selWorkLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP &&
                                selWorkLayer.ObjectsList.Count > 0 &&
                                ((VoronoiMap)selWorkLayer.ObjectsList[0]).Selected)
                            {
                                AddSelectLayer(selWorkLayer);
                                if (e.Button == MouseButtons.Right)
                                {
                                    ShowContextMenu(selWorkLayer, e.Location);
                                }
                            }
                        }
                        if (LastSelectObject == null && selLayerList.Count == 0 && selWellList.Count == 0)
                        {
                            LastSelectObject = new BaseObj(Constant.BASE_OBJ_TYPES_ID.PROJECT);
                            if (MapProject.OilFields.Count > 0 && MapProject.OilFields[0].Contours.Count > 0)
                            {
                                for (int g = 0; g < MapProject.OilFields[0].Contours.Count; g++)
                                {
                                    if(MapProject.OilFields[0].Contours[g]._ContourType == -1 && MapProject.OilFields[0].Contours[g]._EnterpriseCode == 0)
                                    {
                                        LastSelectObject.Name = MapProject.OilFields[0].Contours[g].Name;
                                        break;
                                    }
                                }
                            }
                            LastSelectObject.Code = 0;
                        }
                        if (LastSelectObject != null && OnChangeSelectedObject != null)
                        {
                            OnChangeSelectedObject(LastSelectObject);
                        }
                        DrawLayerList();
                    }
                }
            }
            else if ((ShiftPressed) && (ShiftSelContourPoint > -1) && (ShiftSelContourIndex > -1)) // режим сдвига точки контура
            {
                shiftTmr.Enabled = false;
                C2DLayer selContourLayer = (C2DLayer)selLayerList[ShiftSelContourIndex];
                Contour selContour = (Contour)selContourLayer.ObjectsList[0];
                if (ShiftAdhesionRealPoint.X != Constant.DOUBLE_NAN)
                {
                    selContour.SetPointByIndex(ShiftSelContourPoint, ShiftAdhesionRealPoint.X, ShiftAdhesionRealPoint.Y);
                    ShiftAdhesionRealPoint.X = Constant.DOUBLE_NAN;
                    ShiftAdhesionRealPoint.Y = Constant.DOUBLE_NAN;
                }
                else
                    selContour.SetPointByIndex(ShiftSelContourPoint, C2DObject.RealXfromScreenX(MButtonTimerPoint.X), C2DObject.RealYfromScreenY(MButtonTimerPoint.Y));
                LayerWorkListChanged = true;

                selContourLayer.GetObjectsRect();
                ShiftSelContourPoint = -1;
                ShiftSelContourIndex = -1;
                this.Cursor = Cursors.Default;
                DrawLayerList();
            }
            this.Cursor = Cursors.Default;
            MButtonPressed = false;
            MouseButtonDown = MouseButtons.None;
        }
        void CanvasMap_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (MapProject == null)
            {
                mainForm.ShowGlobalProjForm();
            }
            else
            {
                if (EditedLayer != null)
                {
                    float dX, dY;
                    dX = e.X - C2DObject.ScreenXfromRealX(EditedLayer.ObjRect.Left + EditedLayer.ObjRect.Width / 2);
                    dY = e.Y - C2DObject.ScreenYfromRealY(EditedLayer.ObjRect.Top + EditedLayer.ObjRect.Height / 2);
                    TransferLayer(EditedLayer, dX, dY);
                    DrawLayerList();
                }
                if ((selLayerList.Count > 0) && (((C2DLayer)selLayerList[0]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE))
                {
                    if (((C2DLayer)selLayerList[0]).node != null) twWork.SetOneSelectedNode(((C2DLayer)selLayerList[0]).node);
                    selLayerList.Clear();
                }
            }
        }
        void CanvasMap_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
            mainForm.SetStatus("");
            TipTimer.Enabled = false;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            SnapShootMode = false;
            if (e.Shift) ShiftPressed = true;
            if (e.Control) CtrlPressed = true;
            // if (ShiftPressed || CtrlPressed) this.Invalidate();
            if (e.KeyCode == Keys.Escape)
            {
                newContour = null;
                newContourEnd = false;
                AddWellProfileMode = false;
                if (EditedLayerRotate)
                {
                    twLayers.ResetRotate();
                    ResetRotate();
                }
                else if (EditedLayer != null)
                {
                    if (EditedLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
                        twWork.ResetEditMode();
                    else
                        twLayers.ResetEditMode();
                    EditedLayer = null;

                }
                DrawLayerList();
            }
            if (e.KeyCode == Keys.Back)
            {
                if ((newContour != null) && (newContour._points.Count > 0))
                {
                    newContour._points.RemoveAt(newContour._points.Count - 1);
                    newContour.SetMinMax();
                    if (newContour._points.Count == 0)
                    {
                        newContour = null;
                        newContourEnd = false;
                    }
                }
                DrawLayerList();
            }
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
            {
                if (newContour != null)
                {
                    this.UpdateFilterOilObj = true;
                    newContour.SetMinMax();
                    SelectWellsByContour(newContour);
                    //mainForm.dChartEnableByOilObjButtons(true);
                    mainForm.dChartSetWindowText(String.Format("График - Список скважин[{0}]", selWellList.Count));
                    IsGraphicsByOneDate = false;
                    AddSumWellList(true);
                    newContourEnd = true;
                    mainForm.ShowDataChartPane();
                }
                else if ((selLayerList.Count == 1) &&
                    (((C2DLayer)selLayerList[0])).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR &&
                    ((((C2DLayer)selLayerList[0])).ObjectsList.Count > 0) &&
                    ((Contour)(((C2DLayer)selLayerList[0])).ObjectsList[0])._ContourType == 0)
                {
                    SelectWellsByContour((Contour)((C2DLayer)selLayerList[0]).ObjectsList[0]);
                    //mainForm.dChartEnableByOilObjButtons(true);
                    mainForm.dChartSetWindowText(String.Format("График - Список скважин[{0}]", selWellList.Count));
                    IsGraphicsByOneDate = false;
                    AddSumWellList(true);
                }
                else
                {
                    SelectWellsByPhantom(false);
                }
                DrawLayerList();
            }
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                ShiftPressed = true;
            }
            else
                ShiftPressed = false;

            if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
            {
                CtrlPressed = true;
            }
            else
                CtrlPressed = false;
            if (e.KeyCode == Keys.Escape)
            {
                SnapShootMode = false;
            }
            this.ShiftSelContourPoint = -1;
            this.ShiftSelContourIndex = -1;
            this.Cursor = Cursors.Default;
            this.Invalidate();
            base.OnKeyUp(e);
        }

        // SNAPSHOOT MODE
        public void SetSnapShootMode(bool SnapAllMap)
        {
            if (SnapAllMap)
            {
                SnapAllMapToClipboard();
            }
            else
            {
                SnapShootMode = true;
            }
        }
        void SnapToClipboard()
        {
            if ((snapShootMode) && (!SnapPt1.IsEmpty))
            {
                int minX, minY, maxX, maxY;
                if (MButtonTimerPoint.X > SnapPt1.X)
                {
                    minX = SnapPt1.X;
                    maxX = (int)MButtonTimerPoint.X;
                }
                else
                {
                    minX = (int)MButtonTimerPoint.X;
                    maxX = SnapPt1.X;
                }
                if (MButtonTimerPoint.Y > SnapPt1.Y)
                {
                    minY = SnapPt1.Y;
                    maxY = (int)MButtonTimerPoint.Y;
                }
                else
                {
                    minY = (int)MButtonTimerPoint.Y;
                    maxY = SnapPt1.Y;
                }

                if (minX < this.ClientRectangle.Left) minX = this.ClientRectangle.Left;
                if (maxX > this.ClientRectangle.Right) maxX = this.ClientRectangle.Right;
                if (minY < this.ClientRectangle.Top) minY = this.ClientRectangle.Top;
                if (maxY > this.ClientRectangle.Bottom) maxY = this.ClientRectangle.Bottom;
                Rectangle rect = new Rectangle(minX, minY, (maxX - minX), (maxY - minY));
                if ((rect.Width < 20) || (rect.Height < 20))
                {
                    rect = this.ClientRectangle;
                    rect.X++;
                    rect.Y++;
                    rect.Width -= 2;
                    rect.Height -= 2;
                }
                if (SnapRectToClipboard(rect))
                {
                    ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Участок карты скопирован в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".");
                }
            }
            SnapShootMode = false;
            this.Invalidate();
        }
        void SnapAllMapToClipboard()
        {
            Rectangle rect = this.ClientRectangle;
            rect.X++;
            rect.Y++;
            rect.Width -= 2;
            rect.Height -= 2;
            if (SnapRectToClipboard(rect))
            {
                ToolTipMessage.Show(mainForm, Constant.TOOLTIP_TYPE_ID.INFORMATION, "Внимание!", "Окно карты скопировано в буфер обмена в формате " + mainForm.AppSettings.CopyClipboardFormatName + ".");
            }
            this.Invalidate();
        }
        bool SnapRectToClipboard(Rectangle rect)
        {
            bool result = false;
            ClipboardImageHelper help = new ClipboardImageHelper(mainForm, mainForm.AppSettings.CopyClipboardFormat, new Rectangle(0, 0, rect.Width, rect.Height));
            Graphics grfx = help.GetGraphics();
            if (grfx != null)
            {
                grfx.TranslateTransform(this.ClientRectangle.X - rect.X, this.ClientRectangle.Y - rect.Y);
                this.DrawLayerList(grfx);
                grfx.Dispose();
                result = help.PutImageOnClipboard();
            }
            help.Dispose();
            return result;
        }

        // LETTERBOX SHOW
        public void StartLetterBoxUpdate()
        {
            if (LetterBoxUpdater == null) LetterBoxUpdater = new Network.DiscussLetterBox(mainForm);
            LetterBoxUpdater.Update();
        }
        public void StopLetterBoxUpdate()
        {
            if (LetterBoxUpdater != null)
            {
                LetterBoxUpdater.StopUpdate();
            }
        }
        public bool TestLetterBoxClick(Graphics grfx, int CursorX, int CursorY)
        {
            if (MapProject == null || C2DObject.st_ObjectsRect == null || !ShowLetterBox)
            {
                LetterBoxObject = null;
                return false;
            }
            int type = 0;
            if (C2DObject.st_XUnitsInOnePixel > C2DObject.st_NgduXUnits)
            {
                type = -2;
            }
            else if (C2DObject.st_XUnitsInOnePixel > C2DObject.st_OilfieldXUnits)
            {
                type = -3;
            }
            else if (C2DObject.ShowAreasMode && C2DObject.st_XUnitsInOnePixel > C2DObject.st_AreaXUnits)
            {
                type = -4;
            }
            else
            {
                type = 1;
            }
            
            OilField of;
            Contour cntr;
            SizeF size;
            if (type == -2)
            {
                double centerX, centerY;
                of = MapProject.OilFields[0];
                for (int i = 0; i < of.Contours.Count; i++)
                {
                    cntr = (Contour)of.Contours[i];
                    if (cntr.LetterBoxLevel == -1) continue;
                    size = grfx.MeasureString(cntr.Name, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                    centerX = C2DObject.ScreenXfromRealX(cntr.X);
                    if (CursorX >= centerX - size.Width / 2 && CursorX <= centerX - size.Width / 2 + 12)
                    {
                        centerY = C2DObject.ScreenYfromRealY(cntr.Y);
                        if ((CursorY >= centerY - 14) && (CursorY <= centerY - 2))
                        {
                            LetterBoxObject = new C2DObject();
                            if (cntr._ContourType == -2)
                            {
                                LetterBoxObject.TypeID = Constant.BASE_OBJ_TYPES_ID.NGDU;
                                LetterBoxObject.Name = cntr.Name;
                                LetterBoxObject.Code = 0;
                                LetterBoxObject.Index = cntr._NGDUCode;
                            }
                            else if (cntr._ContourType == -1)
                            {
                                LetterBoxObject.TypeID = Constant.BASE_OBJ_TYPES_ID.PROJECT;
                                LetterBoxObject.Name = cntr.Name;
                                LetterBoxObject.Code = cntr._EnterpriseCode;
                            }

                            this.Cursor = Cursors.Hand;
                            return true;
                        }
                    }
                }
            }
            else if (type == -3)
            {
                double centerX, centerY;
                for (int j = 1; j < MapProject.OilFields.Count; j++)
                {
                    of = MapProject.OilFields[j];
                    for (int i = 0; i < of.Contours.Count; i++)
                    {
                        cntr = (Contour)of.Contours[i];
                        if (cntr._ContourType != type) break;
                        if (cntr.LetterBoxLevel == -1) continue;
                        size = grfx.MeasureString(cntr.Name, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                        centerX = C2DObject.ScreenXfromRealX(cntr.X);
                        if (CursorX >= centerX - size.Width / 2 && CursorX <= centerX - size.Width / 2 + 12)
                        {
                            centerY = C2DObject.ScreenYfromRealY(cntr.Y);
                            if ((CursorY >= centerY - 14) && (CursorY <= centerY - 2))
                            {
                                LetterBoxObject = of;
                                this.Cursor = Cursors.Hand;
                                return true;
                            }
                        }
                    }
                }
            }
            else if (type == -4)
            {
                double centerX, centerY;
                for (int j = 1; j < MapProject.OilFields.Count; j++)
                {
                    of = MapProject.OilFields[j];
                    for (int i = 0; i < of.Areas.Count; i++)
                    {
                        if (of.Areas[i].Visible && of.Areas[i].contour.Visible)
                        {
                            cntr = of.Areas[i].contour;

                            size = grfx.MeasureString(cntr.Name, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                            centerX = C2DObject.ScreenXfromRealX(cntr.X);
                            if (CursorX >= centerX - size.Width / 2 && CursorX <= centerX - size.Width / 2 + 12)
                            {
                                centerY = C2DObject.ScreenYfromRealY(cntr.Y);
                                if ((CursorY >= centerY - 14) && (CursorY <= centerY - 2))
                                {
                                    LetterBoxObject = of.Areas[i];
                                    //LetterBoxObject = new C2DObject();
                                    //LetterBoxObject.TypeID = Constant.BASE_OBJ_TYPES_ID.AREA;
                                    //LetterBoxObject.Code = of.Index;
                                    //LetterBoxObject.Index = of.Areas[i].Index;
                                    //LetterBoxObject.Name = "ЯЗ: " + of.Areas[i].Name;
                                    this.Cursor = Cursors.Hand;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            else if (type == 1)
            {
                Well w;
                WellIcon icon = null;
                double centerY;
                float screenX, scrTextX, screenY;
                double realX, realY;
                for (int j = 1; j < MapProject.OilFields.Count; j++)
                {
                    of = MapProject.OilFields[j];
                    for (int i = 0; i < of.Wells.Count; i++)
                    {
                        if (of.Wells[i].Visible && of.Wells[i].ShowLetterBox && of.Wells[i].LetterBoxLevel > -1 && of.Wells[i].font != null)
                        {
                            w = of.Wells[i];
                            if (w.CoordLoaded && (w.X != Constant.DOUBLE_NAN) && (w.Y != Constant.DOUBLE_NAN))
                            {
                                icon = w.icons.GetActiveIcon();
                                if ((w.BubbleList != null) && (w.BubbleList.Visible))
                                {
                                    icon = w.BubbleIcons.GetActiveIcon();
                                }

                                if (icon == null)
                                {
                                    realX = w.X;
                                    realY = w.Y;
                                }
                                else
                                {
                                    realX = icon.X;
                                    realY = icon.Y;
                                }

                                if (C2DObject.DrawAllObjects || C2DObject.st_RealScreenRect.Contains(realX, realY))
                                {
                                    screenX = C2DObject.ScreenXfromRealX(realX);
                                    screenY = C2DObject.ScreenYfromRealY(realY);
                                    size = grfx.MeasureString(w.UpperCaseName, w.font, Point.Empty, StringFormat.GenericTypographic);

                                    float radX = (float)(50.31f * Math.Pow(C2DObject.st_XUnitsInOnePixel, -1.013));
                                    SizeF sIcon = SizeF.Empty;
                                    if (icon != null) sIcon = icon.GetIconSize(grfx);
                                    if (sIcon.Width == 0)
                                        scrTextX = screenX + (radX / 2) + 2;
                                    else
                                        scrTextX = screenX + sIcon.Width / 4;
                                    if (CursorX >= scrTextX && CursorX <= scrTextX + 12)
                                    {
                                        centerY = C2DObject.ScreenYfromRealY(w.Y);
                                        if ((CursorY >= centerY - 6) && (CursorY <= centerY + 6))
                                        {
                                            LetterBoxObject = w;
                                            //LetterBoxObject = new C2DObject();
                                            //LetterBoxObject.TypeID = Constant.BASE_OBJ_TYPES_ID.WELL;
                                            //LetterBoxObject.Code = of.Index;
                                            //LetterBoxObject.Index = w.Index;
                                            //LetterBoxObject.Name = "Скв: " + w.Name;
                                            this.Cursor = Cursors.Hand;
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
 
            }
            if (BPcontour == null) this.Cursor = Cursors.Default;
            return false;
        }

        // BIZPLAN SHOW
        public bool TestBizPlanParams(Graphics grfx, int CursorX, int CursorY)
        {
            if (MapProject == null || C2DObject.st_ObjectsRect == null || !ShowBizPlanParams)
            {
                BPcontour = null;
                return false;
            }
            int type = (C2DObject.st_XUnitsInOnePixel > C2DObject.st_NgduXUnits) ? -2 : -3;

            SizeF size, sizeM;
            float width;
            string strBPYearParams, strBPMonthParams;
            if (BPcontour != null && BPcontour.BizPlanParams != null)
            {
                double centerX, centerY;
                size = grfx.MeasureString(BPcontour.Name, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                centerX = C2DObject.ScreenXfromRealX(BPcontour.X);
                if (CursorX >= centerX - size.Width / 2 - 2 - 8 && CursorX <= centerX + size.Width / 2)
                {
                    centerY = C2DObject.ScreenYfromRealY(BPcontour.Y);
                    if ((CursorY >= centerY - 4) && (CursorY <= centerY + 4))
                    {
                        return true;
                    }
                }
                strBPYearParams = string.Format("dQн(1234) +{0:0.#} (+{1:0.#} %)", BPcontour.BizPlanParams[1], BPcontour.BizPlanParams[3]);
                strBPMonthParams = string.Format("dQн(11.1234) +{0:0.#} (+{1:0.#} %)", BPcontour.BizPlanParams[2], BPcontour.BizPlanParams[4]);
                size = grfx.MeasureString(strBPYearParams, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                sizeM = grfx.MeasureString(strBPMonthParams, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                width = (size.Width > sizeM.Width) ? size.Width + 1 : sizeM.Width + 1;
                if (CursorX >= centerX - width / 2 && CursorX <= centerX + width / 2)
                {
                    centerY = C2DObject.ScreenYfromRealY(BPcontour.Y);
                    if ((CursorY >= centerY + 4) && (CursorY <= centerY + 4 + size.Height * 2 + 2))
                    {
                        this.Cursor = Cursors.Hand;
                        return true;
                    }
                }
            }
            BPcontour = null;
            OilField of;
            Contour cntr;

            if (type == -2)
            {
                double centerX, centerY;
                of = MapProject.OilFields[0];
                for (int i = 0; i < of.Contours.Count; i++)
                {
                    cntr = (Contour)of.Contours[i];
                    if (cntr.BizPlanParams != null)
                    {
                        size = grfx.MeasureString(cntr.Name, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                        centerX = C2DObject.ScreenXfromRealX(cntr.X);
                        if (C2DObject.st_XUnitsInOnePixel > 1000)
                        {
                            if (CursorX >= centerX - size.Width / 2 - 2 - 8 && CursorX <= centerX - size.Width / 2 - 2)
                            {
                                centerY = C2DObject.ScreenYfromRealY(cntr.Y);
                                if ((CursorY >= centerY - 4) && (CursorY <= centerY + 4))
                                {
                                    BPcontour = cntr;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            strBPYearParams = string.Format("dQн(1234) +{0:0.#} (+{1:0.#} %)", cntr.BizPlanParams[1], cntr.BizPlanParams[3]);
                            strBPMonthParams = string.Format("dQн(11.1234) +{0:0.#} (+{1:0.#} %)", cntr.BizPlanParams[2], cntr.BizPlanParams[4]);
                            size = grfx.MeasureString(strBPYearParams, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                            sizeM = grfx.MeasureString(strBPMonthParams, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                            width = (size.Width > sizeM.Width) ? size.Width + 1 : sizeM.Width + 1;
                            if (CursorX >= centerX - width / 2 && CursorX <= centerX + width / 2)
                            {
                                centerY = C2DObject.ScreenYfromRealY(cntr.Y);
                                if ((CursorY >= centerY + 4) && (CursorY <= centerY + 4 + size.Height * 2 + 2))
                                {
                                    BPcontour = cntr;
                                    this.Cursor = Cursors.Hand;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                double centerX, centerY;
                for (int j = 1; j < MapProject.OilFields.Count; j++)
                {
                    of = MapProject.OilFields[j];
                    for (int i = 0; i < of.Contours.Count; i++)
                    {
                        cntr = (Contour)of.Contours[i];
                        if (cntr._ContourType != type) break;
                        if (cntr.BizPlanParams != null)
                        {
                            size = grfx.MeasureString(cntr.Name, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                            centerX = C2DObject.ScreenXfromRealX(cntr.X);
                            if (C2DObject.st_XUnitsInOnePixel > 150)
                            {
                                if (CursorX >= centerX - size.Width / 2 - 2 - 8 && CursorX <= centerX - size.Width / 2 - 2)
                                {
                                    centerY = C2DObject.ScreenYfromRealY(cntr.Y);
                                    if ((CursorY >= centerY - 4) && (CursorY <= centerY + 4))
                                    {
                                        BPcontour = cntr;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                strBPYearParams = string.Format("dQн(1234) +{0:0.#} (+{1:0.#} %)", cntr.BizPlanParams[1], cntr.BizPlanParams[3]);
                                strBPMonthParams = string.Format("dQн(11.1234) +{0:0.#} (+{1:0.#} %)", cntr.BizPlanParams[2], cntr.BizPlanParams[4]);
                                size = grfx.MeasureString(strBPYearParams, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                                sizeM = grfx.MeasureString(strBPMonthParams, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                                width = (size.Width > sizeM.Width) ? size.Width + 1 : sizeM.Width + 1;
                                if (CursorX >= centerX - width / 2 && CursorX <= centerX + width / 2)
                                {
                                    centerY = C2DObject.ScreenYfromRealY(cntr.Y);
                                    if ((CursorY >= centerY + 4) && (CursorY <= centerY + 4 + size.Height * 2 + 2))
                                    {
                                        BPcontour = cntr;
                                        this.Cursor = Cursors.Hand;
                                        break;
                                    }
                                }

                            }
                        }
                    }
                    if (BPcontour != null) break;
                }
            }
            if (LetterBoxObject == null) this.Cursor = Cursors.Default;
            return BPcontour != null;
        }

        // FONTS
        public void ReCalcFontSizes()
        {
            int len;
            float size;
            //_fontSize = (int)(50 / XUnitsInOnePixel);

            iconFontSize = (float)Math.Round(150.31 * Math.Pow(C2DObject.st_XUnitsInOnePixel, -1.013));
            if (!C2DObject.DrawAllObjects && iconFontSize < 7.25) iconFontSize = 7.25f;
            if (((iconFontSize >= 1) && (iconFontSize < 1000)) || (iconFontSize != iconFonts[0].Size))
            {
                for (int i = 0; i < 7; i++)
                {
                    iconFonts[i] = new Font(iconFonts[i].FontFamily, iconFontSize);
                }
                for (int i = 0; i < exFontList.Count; i++)
                {
                    size = (float)Math.Round(15.031 * exFontList[i].Size * Math.Pow(C2DObject.st_XUnitsInOnePixel, -1.013));
                    if (size <= 7.25) size = 7.25f;
                    if (exFontList[i].font != null)
                    {
                        exFontList[i].font = new Font(exFontList[i].font.FontFamily, size);
                    }
                }
            }

            C2DLayer layer;
            List<C2DLayer> list = new List<C2DLayer>();
            if (C2DObject.st_XUnitsInOnePixel <= C2DObject.st_OilfieldXUnits)
            {
                for (int i = 0; i < LayerList.Count; i++)
                {
                    layer = (C2DLayer)LayerList[i];
                    layer.VisibleFar = true;
                    layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL_PAD, list);

                }
                for (int i = 0; i < list.Count; i++)
                {
                    layer = (C2DLayer)list[i];
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL_PAD)
                    {
                        if (iconFontSize / 2 < 20)
                        {
                            if (iconFontSize / 2 > 7)
                                layer.fontSize = iconFontSize / 2;
                            else
                                layer.fontSize = 7;
                        }
                        else
                            layer.fontSize = 20;

                        layer.ResetFontsPens();
                    }
                }
            }
            list.Clear();
            if (C2DObject.st_XUnitsInOnePixel <= 15)
            {
                for (int i = 0; i < LayerList.Count; i++)
                {
                    layer = (C2DLayer)LayerList[i];
                    layer.VisibleFar = true;
                    layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list);
                    layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL, list);
                }
            }
            for (int i = 0; i < LayerWorkList.Count; i++)
            {
                layer = (C2DLayer)LayerWorkList[i];
                layer.VisibleFar = true;
                layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL, list);
                layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP, list);
            }

            for (int i = 0; i < list.Count; i++)
            {
                layer = (C2DLayer)list[i];
                if (iconFontSize / 5 < 14)
                {
                    if (iconFontSize / 5 > 7 || C2DObject.DrawAllObjects)
                    {
                        layer.fontSize = iconFontSize / 5;
                    }
                    else
                    {
                        layer.fontSize = 7;
                    }
                }
                else
                {
                    layer.fontSize = 14;
                }
#if DEBUG
                //if (i == 0) reLog.AppendText(layer.fontSize.ToString() + Environment.NewLine);
#endif

                layer.ResetFontsPens();
            }
        }
        public void SetLayerFontsRecursive(C2DLayer layer)
        {
            layer.SetFontsRecursive(iconFonts, exFontList);
        }

        public FontFamily[] GetFontFamilyList()
        {
            return FontFamilyList;
        }
        public Font[] GetIconFontList()
        {
            return iconFonts;
        }
        public ExFontList GetExFontList()
        {
            return exFontList;
        }

        // LOAD LAYERS
        public void LoadLayers()
        {
            int i, j, k, num, numAreas, ind, ind2;
            int InsertLayerIndex = 0;
            OilField of;
            C2DObject obj = null;
            C2DLayer layer = null, layerGroup = null;
            Contour cntr;
            Grid grid;
            string lName = "";
            LayerList.Clear();
            if (MapProject == null) return;

            var dictStratum = (StratumDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            var dictOilfieldArea = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            var dictGridType = (DataDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.GRID_TYPE);

            // добавляем по месторождениям
            for (k = 0; k < MapProject.OilFields.Count; k++)
            {
                layer = null;
                of = MapProject.OilFields[k];
                string gridName = string.Empty;
                layerGroup = null;

                // добавляем сетки

                if ((of.Grids != null) && (of.Grids.Count > 0))
                {
                    layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, k);
                    layerGroup.Name = "Сетки";
                    layerGroup.oilfield = of;
                    layerGroup.Visible = false;
                    layerGroup.fontSize = this._fontSize;
                    layerGroup.ResetFontsPens();
                    LayerList.Add(layerGroup);

                    int areaCode = -1;
                    bool byAreas = false;
                    List<int> ofAreasCodes = new List<int>();
                    List<List<int>> gridTypesCodes = new List<List<int>>();
                    List<List<List<C2DLayer>>> ofAreasLayers = new List<List<List<C2DLayer>>>();

                    byAreas = (of.Name == "АРЛАНСКОЕ");

                    for (j = 0; j < of.Grids.Count; j++)
                    {
                        grid = (Grid)of.Grids[j];

                        layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.GRID, k);
                        layer.oilfield = of;
                        if ((dictStratum != null) && (grid.StratumCode != 0))
                            lName = dictStratum.GetShortNameByCode(grid.StratumCode);
                        else
                            lName = grid.StratumName;

                        //if (dictGridType != null)
                        //    gridName = "Карта " + dictGridType.GetShortNameByCode(grid.GridType);
                        //else
                        //    gridName = grid.Name;

                        layer.Name = string.Format("{0} [{1:MM.yyyy}]", lName, grid.Date);


                        grid.Visible = false;
                        layer.Visible = grid.Visible;

                        layer.Add(of.Grids[j]);

                        layer.fontSize = this._fontSize;
                        layer.ResetFontsPens();
                        layer.GetObjectsRect();
                        areaCode = -1;
                        if (byAreas)
                        {
                            areaCode = dictOilfieldArea.GetCodeByShortName(grid.Name);
                        }
                        ind = ofAreasCodes.IndexOf(areaCode);
                        if (ind == -1)
                        {
                            ind = ofAreasCodes.Count;
                            ofAreasCodes.Add(areaCode);
                            gridTypesCodes.Add(new List<int>());
                            ofAreasLayers.Add(new List<List<C2DLayer>>());
                        }
                        ind2 = gridTypesCodes[ind].IndexOf(grid.GridType);
                        if (ind2 == -1)
                        {
                            ind2 = gridTypesCodes[ind].Count;
                            gridTypesCodes[ind].Add(grid.GridType);
                            ofAreasLayers[ind].Add(new List<C2DLayer>());
                        }
                        //grid.Name = layer.Name;
                        ofAreasLayers[ind][ind2].Add(layer);
                        //layerGroup.Add(layer);
                    }
                    if (ofAreasCodes.Count > 0 && ofAreasLayers.Count > 0)
                    {
                        for (j = 0; j < ofAreasLayers.Count; j++)
                        {
                            for (i = 0; i < gridTypesCodes[j].Count; i++)
                            {
                                for (int x = 0; x < ofAreasLayers[j][i].Count; x++)
                                {
                                    layerGroup.Add(ofAreasLayers[j][i][x]);
                                }
                            }
                        }
                    }
                    if (layerGroup.ObjectsList.Count > 0) layerGroup.GetObjectsRect();
                }

                layerGroup = null;
                int predOilObj = -1;
                int lastSubsInd = -1;
                // добавляем условные суммарные контуры залежей
                if (of.Contours.Count > 0)
                {
                    num = 0;
                    layerGroup = null;
                    Contour cntrOut;
                    j = 0;
                    while (j < of.Contours.Count)
                    {
                        cntr = (Contour)of.Contours[j];
                        if ((cntr._ContourType != -5))
                        {
                            j++;
                            continue;
                        }

                        if (layerGroup == null)
                        {
                            layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, k);
                        }
                        //((Contour)of.Contours[j])._FillBrush = 34;
                        layerGroup.Add(cntr);
                        j++;
                        if (!cntr._Closed)
                        {
                            while ((j < of.Contours.Count) && (((Contour)of.Contours[j])._Closed))
                            {
                                cntrOut = (Contour)of.Contours[j];
                                if (cntr.cntrsOut == null) cntr.cntrsOut = new List<Contour>(2);
                                cntr.cntrsOut.Add(cntrOut);
                                j++;
                            }
                        }
                    }
                    if (layerGroup != null)
                    {
                        layerGroup.oilfield = of;
                        layerGroup.fontSize = this._fontSize;
                        layerGroup.Name = "Суммарный контур залежей";
                        layerGroup.Visible = false;
                        layerGroup.GetObjectsRect();
                        layerGroup.ResetFontsPens();
                        for (j = 0; j < layerGroup.ObjectsList.Count; j++)
                        {
                            cntr = ((Contour)layerGroup.ObjectsList[j]);
                            cntr._Closed = true;
                            if (cntr.cntrsOut != null)
                            {
                                for (int x = 0; x < cntr.cntrsOut.Count; x++)
                                {
                                    cntr.cntrsOut[x]._Closed = true;
                                }
                            }
                        }
                        LayerList.Add(layerGroup);
                        InsertLayerIndex++;
                    }
                    // добавляем контуры лиц участков и горные отводы
                    layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, k);
                    for (j = 0; j < of.Contours.Count; j++)
                    {
                        cntr = (Contour)of.Contours[j];
                        if (cntr._ContourType == 1)
                        {
                            layerGroup.Add(cntr);
                        }
                    }
                    if (layerGroup.ObjectsList.Count > 0)
                    {
                        layerGroup.oilfield = of;
                        layerGroup.fontSize = this._fontSize;
                        layerGroup.Name = "Лицензионные границы";
                        layerGroup.Visible = false;
                        layerGroup.GetObjectsRect();
                        layerGroup.ResetFontsPens();
                        LayerList.Add(layerGroup);
                        InsertLayerIndex++;
                    }
                    num = 0;
                    layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, k);
                    for (j = 0; j < of.Contours.Count; j++)
                    {
                        cntr = (Contour)of.Contours[j];
                        if (cntr._ContourType == 1010 || cntr._ContourType == 90)
                        {
                            layerGroup.Add(cntr);
                        }
                    }
                    if (layerGroup.ObjectsList.Count > 0)
                    {
                        layerGroup.oilfield = of;
                        layerGroup.fontSize = this._fontSize;
                        layerGroup.Name = "Горные отводы";
                        layerGroup.Visible = false;
                        layerGroup.GetObjectsRect();
                        layerGroup.ResetFontsPens();
                        LayerList.Add(layerGroup);
                        InsertLayerIndex++;
                    }
                }

                // добавляем контуры
                if (of.Contours.Count > 0)
                {
                    num = 0;
                    layerGroup = null;

                    List<int> cntrTypesList = new List<int>();
                    List<List<Contour>> cntrList = new List<List<Contour>>();
                    List<Contour> tempCntrList = new List<Contour>();

                    for (j = 0; j < of.Contours.Count; j++)
                    {
                        cntr = (Contour)of.Contours[j];

                        if (((!((cntr._ContourType > 1) && (cntr._ContourType < 6))) &&
                             (!((cntr._ContourType > 1001) && (cntr._ContourType < 1006))) &&
                             (cntr._ContourType != 10) && (cntr._ContourType != 53) && (cntr._ContourType >= 0)) ||
                             (cntr._ContourType < 0))
                            continue;
                        tempCntrList.Add(cntr);
                    }

                    for (j = 0; j < tempCntrList.Count; j++)
                    {
                        cntr = tempCntrList[j];
                        cntr.SetDictPlast(dictStratum);

                        if (((predOilObj != cntr.StratumCode) && (cntr._ContourType > -1)) || (j == tempCntrList.Count - 1))
                        {
                            if (j == tempCntrList.Count - 1)
                            {
                                ind = cntrTypesList.IndexOf(cntr._ContourType);
                                if (ind == -1)
                                {
                                    ind = cntrTypesList.Count;
                                    cntrTypesList.Add(cntr._ContourType);
                                    cntrList.Add(new List<Contour>());
                                }
                                cntrList[ind].Add(cntr);
                            }
                            if (cntrTypesList.Count > 0)
                            {
                                for (i = 0; i < cntrTypesList.Count; i++)
                                {
                                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, k);

                                    layer.oilfield = of;
                                    layer.Visible = true;
                                    switch (cntrTypesList[i])
                                    {
                                        case 2:
                                            layer.Name = "Внешний контур нефтеносности (внутрь)";
                                            break;
                                        case 3:
                                            layer.Name = "Внутренний контур нефтеносности (внутрь)";
                                            break;
                                        case 4:
                                            layer.Name = "Замещение (внутрь)";
                                            break;
                                        case 5:
                                            layer.Name = "Вклинивание (внутрь)";
                                            break;
                                        case 10:
                                            layer.Name = "Разлом, тектоническое нарушение";
                                            break;
                                        case 53:
                                            layer.Name = "Граница блока";
                                            break;
                                        case 1002:
                                            layer.Name = "Внешний контур нефтеносности (наружу)";
                                            break;
                                        case 1003:
                                            layer.Name = "Внутренний контур нефтеносности (наружу)";
                                            break;
                                        case 1004:
                                            layer.Name = "Замещение (наружу)";
                                            break;
                                        case 1005:
                                            layer.Name = "Вклинивание (наружу)";
                                            break;
                                    }

                                    for (int x = 0; x < cntrList[i].Count; x++)
                                    {
                                        layer.Add(cntrList[i][x]);
                                        cntrList[i][x].Name = layer.Name;
                                    }

                                    layer.Visible = true;

                                    if (layerGroup == null) layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, k);

                                    layer.Index = layerGroup.ObjectsList.Count;
                                    layerGroup.Add(layer);

                                    //if ((layer.ObjectsList.Count == 0) || (((Contour)layer.ObjectsList[layer.ObjectsList.Count - 1]).StratumCode < cntr.StratumCode))
                                    //{
                                    //    layer.Add((C2DObject)of.Contours[j]);
                                    //}
                                    //else
                                    //{
                                    //    bool ins = false;
                                    //    for (i = 0; i < layer.ObjectsList.Count; i++)
                                    //    {
                                    //        if (((Contour)layer.ObjectsList[i]).StratumCode > cntr.StratumCode)
                                    //        {
                                    //            layer.ObjectsList.Insert(i, (C2DObject)cntr);
                                    //            ins = true;
                                    //            break;
                                    //        }
                                    //    }
                                    //    if (!ins) layer.ObjectsList.Insert(0, (C2DObject)cntr);
                                    //}
                                    layer.GetObjectsRect();
                                }
                            }
                            cntrTypesList.Clear();
                            cntrList.Clear();


                            if ((dictStratum != null) && (cntr.StratumCode != 0))
                                lName = dictStratum.GetShortNameByCode(cntr.StratumCode);
                            else
                                lName = cntr.Name;
                            if (lName == "") continue;

                            predOilObj = cntr.StratumCode;
                            if (layerGroup != null) layerGroup.GetObjectsRect();
                            


                            layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, k);
                            lastSubsInd = -1;
                            layerGroup.Name = lName;
                            layerGroup.Code = cntr.StratumCode;
                            layerGroup.oilfield = of;
                            layerGroup.Visible = true;
                            LayerList.Add(layerGroup);
                        }
                        ind = cntrTypesList.IndexOf(cntr._ContourType);
                        if (ind == -1)
                        {
                            ind = cntrTypesList.Count;
                            cntrTypesList.Add(cntr._ContourType);
                            cntrList.Add(new List<Contour>());
                        }
                        cntrList[ind].Add(cntr);
                        num++;
                    }
                }
                // добавляем залежи
                numAreas = 0;
                Deposit dep;
                if ((of.Deposits != null) && (of.Deposits.Count > 0))
                {
                    predOilObj = -1;
                    layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, k);
                    layerGroup.Name = "Залежи";
                    layerGroup.oilfield = of;
                    layerGroup.Visible = false;
                    layerGroup.fontSize = this._fontSize;
                    layerGroup.ResetFontsPens();
                    LayerList.Add(layerGroup);
                    layer = null;
                    bool vis = true;
                    for (j = 0; j < of.Deposits.Count; j++)
                    {
                        dep = of.Deposits[j];
                        if (predOilObj != dep.StratumCode)
                        {
                            if (layer != null)
                            {
                                layer.fontSize = this._fontSize;
                                layer.ResetFontsPens();
                                layer.GetObjectsRect();
                                layerGroup.Add(layer);
                                vis = false;
                            }
                            layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.DEPOSIT, k);

                            layer.oilfield = of;
                            layer.Name = dictStratum.GetShortNameByCode(dep.StratumCode);
                            layer.Visible = vis;
                            predOilObj = dep.StratumCode;
                        }
                        layer.Add((C2DObject)of.Deposits[j]);
                    }
                    if (layer != null)
                    {
                        layer.fontSize = this._fontSize;
                        layer.ResetFontsPens();
                        layer.GetObjectsRect();
                        layerGroup.Add(layer);
                    }
                    if (layerGroup.ObjectsList.Count > 0) layerGroup.GetObjectsRect();
                }
                // добавляем ячейки
                numAreas = 0;
                Area area;
                if ((of.Areas != null) && (of.Areas.Count > 0))
                {
                    int predOilFieldArea = -1;
                    predOilObj = -1;
                    layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, k);
                    layerGroup.Name = "Ячейки";
                    layerGroup.oilfield = of;
                    layerGroup.Visible = false;
                    layerGroup.fontSize = this._fontSize;
                    layerGroup.ResetFontsPens();
                    LayerList.Add(layerGroup);
                    layer = null;
                    bool vis = true;
                    for (j = 0; j < of.Areas.Count; j++)
                    {
                        area = (Area)of.Areas[j];
                        if ((predOilObj != area.PlastCode) || (of.UnionParamsDict != null && predOilFieldArea != area.OilFieldAreaCode))
                        {
                            if (layer != null)
                            {
                                layer.fontSize = this._fontSize;
                                layer.ResetFontsPens();
                                layer.GetObjectsRect();
                                layerGroup.Add(layer);
                                vis = false;
                            }
                            layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.AREA, k);

                            layer.oilfield = of;
                            layer.Name = dictStratum.GetShortNameByCode(area.PlastCode);
                            if (of.UnionParamsDict != null)
                            {
                                string ofAreaName = dictOilfieldArea.GetShortNameByCode(area.OilFieldAreaCode);
                                if (ofAreaName != null && ofAreaName.Length > 0)
                                {
                                    if (ofAreaName.IndexOf("уч") == -1) ofAreaName += " площадь";
                                    layer.Name += string.Format(" ({0})", ofAreaName);
                                }
                            }
                            layer.Visible = vis;
                            predOilObj = area.PlastCode;
                            predOilFieldArea = area.OilFieldAreaCode;
                        }
                        //PlastName = dictOilObj.GetItemByCode(area.PlastCode).ShortName;
                        //area.contour.Name += string.Format("({0})", PlastName);
                        layer.Add((C2DObject)of.Areas[j]);
                        numAreas++;
                    }
                    if (layer != null)
                    {
                        layer.fontSize = this._fontSize;
                        layer.ResetFontsPens();
                        layer.GetObjectsRect();
                        layerGroup.Add(layer);
                    }
                    if (layerGroup.ObjectsList.Count > 0) layerGroup.GetObjectsRect();
                }
                predOilObj = -1;

                // добавляем условные контуры
                if (of.Contours.Count > 0)
                {
                    num = 0;
                    layerGroup = null;
                    for (j = 0; j < of.Contours.Count; j++)
                    {
                        cntr = (Contour)of.Contours[j];
                        if ((cntr._ContourType < 0) && (cntr._ContourType != -5))
                        {
                            layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, k);
                            layer.oilfield = of;
                            layer.Name = cntr.Name;
                            layer.Visible = cntr.Visible;

                            layer.Add((C2DObject)of.Contours[j]);

                            if (of.BPParams != null && of.BPParams[0] > 0 && of.Index > 0)
                            {
                                cntr.BizPlanParams = new double[6];
                                cntr.BizPlanParams[0] = of.BPParams[0];
                                cntr.BizPlanParams[1] = of.BPParams[1];
                                cntr.BizPlanParams[2] = of.BPParams[2];
                                cntr.BizPlanParams[3] = of.BPParams[3];
                                cntr.BizPlanParams[4] = of.BPParams[4];
                                cntr.BizPlanParams[5] = MapProject.maxMerDate.ToOADate();

                                OilField firstOF = MapProject.OilFields[0];
                                Contour ngduCntr = null;
                                string ngduName = string.Format("НГДУ {0}", of.ParamsDict.NGDUName);
                                for (int x = 0; x < firstOF.Contours.Count; x++)
                                {
                                    if (ngduName.StartsWith(firstOF.Contours[x].Name))
                                    {
                                        ngduCntr = (Contour)firstOF.Contours[x];
                                        break;
                                    }
                                }
                                if (ngduCntr != null)
                                {
                                    if (ngduCntr.BizPlanParams == null)
                                    {
                                        ngduCntr.BizPlanParams = new double[6];
                                        ngduCntr.BizPlanParams[0] = of.BPParams[0];
                                        ngduCntr.BizPlanParams[1] = of.BPParams[1];
                                        ngduCntr.BizPlanParams[2] = of.BPParams[2];
                                        ngduCntr.BizPlanParams[3] = of.BPParams[3];
                                        ngduCntr.BizPlanParams[4] = of.BPParams[4];
                                        ngduCntr.BizPlanParams[5] = MapProject.maxMerDate.ToOADate();
                                    }
                                    else if (cntr.BizPlanParams[0] == ngduCntr.BizPlanParams[0])
                                    {
                                        double d = (ngduCntr.BizPlanParams[1] * 100) / ngduCntr.BizPlanParams[3];
                                        if (cntr.BizPlanParams[3] > 0)
                                        {
                                            d += (cntr.BizPlanParams[1] * 100) / cntr.BizPlanParams[3];
                                        }
                                        ngduCntr.BizPlanParams[1] += cntr.BizPlanParams[1];
                                        ngduCntr.BizPlanParams[3] = ngduCntr.BizPlanParams[1] * 100 / d;

                                        d = (ngduCntr.BizPlanParams[2] * 100) / ngduCntr.BizPlanParams[4];
                                        if (cntr.BizPlanParams[4] > 0)
                                        {
                                            d += (cntr.BizPlanParams[2] * 100) / cntr.BizPlanParams[4];
                                        }
                                        ngduCntr.BizPlanParams[2] += cntr.BizPlanParams[2];
                                        ngduCntr.BizPlanParams[4] = ngduCntr.BizPlanParams[2] * 100 / d;
                                    }
                                }
                            }
                            layer.fontSize = this._fontSize;
                            layer.ResetFontsPens();
                            layer.GetObjectsRect();
                            layer.Visible = true;
                            LayerList.Add(layer);

                            InsertLayerIndex++;
                        }
                    }
                }

                // добавляем кусты
                num = 0;
                if ((of.WellPads != null) && (of.WellPads.Count > 0))
                {
                    WellPad wp;
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.WELL_PAD, k);
                    layer.oilfield = of;
                    layer.Visible = false;

                    for (j = 0; j < of.WellPads.Count; j++)
                    {
                        wp = (WellPad)of.WellPads[j];
                        layer.Add((C2DObject)of.WellPads[j]);
                        num++;
                    }
                    layer.Name = "Кусты " + "[" + num.ToString() + "]";
                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    layer.GetObjectsRect();
                    LayerList.Add(layer);

                }

                // добавляем проектные скважины
                if ((of.ProjWellLoaded) && (of.ProjWells.Count > 0))
                {
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL, k);
                    layer.SetFontsRecursive(this.iconFonts, exFontList);
                    layer.oilfield = of;
                    layer.Name = "Проектные скважины";

                    for (j = 0; j < of.ProjWells.Count; j++)
                    {
                        obj = (C2DObject)of.ProjWells[j];
                        layer.Add(obj);
                    }

                    layer.GetObjectsRect();
                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    layer.SetFontsRecursive(this.iconFonts, exFontList);
                    LayerList.Add(layer);
                }

                // добавляем скважины
                if ((of.CoordLoaded) && (of.Wells.Count > 0))
                {
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.WELL, k);
                    layer.SetFontsRecursive(this.iconFonts, exFontList);
                    layer.oilfield = of;
                    of.WellsLayer = layer;
                    layer.Name = "Скважины";
                    Well w;
                    for (j = 0; j < of.Wells.Count; j++)
                    {
                        w = of.Wells[j];
                        if (!w.Missing) layer.Add(w);
                    }

                    layer.GetObjectsRect();
                    layer.SetFontsRecursive(this.iconFonts, exFontList);
                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    LayerList.Add(layer);
                }


                mainForm.SetProgress(k, MapProject.OilFields.Count, "Построение структуры объектов проекта", of.Name);
            }
            // загружаем обустройство
            Construction constr;
            Tube t;
            C2DLayer lrNGDU;
            ArrayList list = new ArrayList();
            for (i = 0; i < MapProject.ConstructionList.Count; i++)
            {
                constr = (Construction)MapProject.ConstructionList[i];
                lrNGDU = null;
                for (j = 0; j < list.Count; j++)
                {
                    if (((C2DLayer)list[j]).ngduCode == constr.NGDUcode)
                    {
                        lrNGDU = (C2DLayer)list[j];
                        break;
                    }
                }
                if (lrNGDU == null)
                {
                    lrNGDU = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                    lrNGDU.Name = "Обустройство";
                    lrNGDU.ngduCode = constr.NGDUcode;
                    lrNGDU.Visible = true;
                    list.Add(lrNGDU);
                }
                layer = null;
                for (j = 0; j < lrNGDU.ObjectsList.Count; j++)
                {
                    if (((C2DLayer)lrNGDU.ObjectsList[j]).Level == constr.Level)
                    {
                        layer = (C2DLayer)lrNGDU.ObjectsList[j];
                        break;
                    }
                }
                if (layer == null)
                {
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION, -1);
                    switch (constr.Level)
                    {
                        case 0:
                            layer.Name = "АГЗУ, БГ,...";
                            break;
                        case 1:
                            layer.Name = "КНС, АТВО,...";
                            break;
                        case 2:
                            layer.Name = "УПН, УПС, УКПН,...";
                            break;
                    }
                    layer.Level = constr.Level;
                    layer.Visible = true;
                    lrNGDU.Add(layer);
                }
                layer.Add(constr);

            }
            for (j = 0; j < list.Count; j++)
            {
                layer = (C2DLayer)list[j];
                for (i = 0; i < layer.ObjectsList.Count; i++)
                {
                    ((C2DLayer)layer.ObjectsList[i]).GetObjectsRect();
                    ((C2DLayer)layer.ObjectsList[i]).fontSize = this._fontSize;
                    ((C2DLayer)layer.ObjectsList[i]).ResetFontsPens();
                }
                LayerList.Add(layer);
            }
            list.Clear();

            for (i = 0; i < LayerList.Count; i++) ((C2DLayer)LayerList[i]).Index = i;
            twLayers.EndDataUpdate();
        }
        public void SetProject(Project aProject)
        {
            if (aProject != null)
            {
                if (MapProject != null)
                {
                    MapProject._BitMapPoint.X = BitMapRect.X;
                    MapProject._BitMapPoint.Y = BitMapRect.Y;
                }
                MapProject = aProject;
                // TEST
                aProject.canvas = this;
                ReCalcFontSizes();
                BitMapRect.X = MapProject._BitMapPoint.X;
                BitMapRect.Y = MapProject._BitMapPoint.Y;
                C2DObject.st_XUnitsInOnePixel = C2DObject.st_MaxXUnits;
                C2DObject.st_YUnitsInOnePixel = C2DObject.st_MaxXUnits;
                C2DObject.st_VisbleLevel = C2DObject.st_MaxXUnits;
                LoadLayers();
                LastSelectObject = new BaseObj(Constant.BASE_OBJ_TYPES_ID.PROJECT);
                LastSelectObject.Name = aProject.Name;
                LastSelectObject.Code = 0;
            }
        }
        public int AddLayer(C2DLayer layer)
        {
            int ind = LayerList.Add(layer);
            return ind;
        }
        public void RemoveLayer(int index)
        {
            LayerList.RemoveAt(index);
        }
        public void SelectNode(int LayerIndex)
        {
            if (LayerIndex < LayerList.Count)
            {
                twLayers.BeginUpdate();
                TreeNode parent = ((C2DLayer)LayerList[LayerIndex]).node.Parent;
                if ((parent != null) && (!parent.IsExpanded)) twLayers.SelectedNode = ((C2DLayer)LayerList[LayerIndex]).node;
                for (int i = 0; i < twLayers.Nodes[0].Nodes.Count; i++)
                {
                    if ((twLayers.Nodes[0].Nodes[i] != parent) && (twLayers.Nodes[0].Nodes[i].IsExpanded)) twLayers.Nodes[0].Nodes[i].Collapse();
                }
                twLayers.EndUpdate();
            }
        }
        public void ClearProject()
        {
            EditedLayer = null;
            ResetRotate();
            MapProject = null;
            newContour = null;
            newContourEnd = false;
            shiftTmr.Enabled = false;
            ShiftSelContourIndex = -1;
            ShiftRightAngle = false;
            ShiftAdhesionRealPoint.X = Constant.DOUBLE_NAN;
            ShiftAdhesion = -1;
            BitMapRect.X = 0;
            BitMapRect.Y = 0;

            C2DObject.st_VisbleLevel = 1631.461442;
            C2DObject.st_XUnitsInOnePixel = 1631.461442;
            C2DObject.st_YUnitsInOnePixel = 1631.461442;
            C2DObject.DrawAllObjects = false;

            selMerOilObjCodes.Clear();

            LastSelectObject = null;
            selWellList.Clear();
            selLayerList.Clear();
            LayerList.Clear();
            twLayers.Clear();
            twActiveOilField.Clear();
            twActiveOilField.ActiveOilFieldIndex = -1;
            LayerWorkListChanged = false;
            LayerWorkList.Clear();
            twWork.Clear();
            mainForm.pgProperties.SelectedObject = null;
            mainForm.SetShowDiscussLetterBoxBtnChecked(false);
            if(LetterBoxUpdater != null) LetterBoxUpdater.StopUpdate();
        }
        public void ClearFantomContour()
        {
            newContour = null;
        }

        // FIND
        public System.Collections.ArrayList FindWell(string WellName)
        {
            System.Collections.ArrayList retArray = null;
            if ((MapProject != null) && (MapProject.OilFields != null) && (MapProject.OilFields.Count > 1))
            {
                OilField of;
                Well w;
                for (int i = 1; i < MapProject.OilFields.Count; i++)
                {
                    of = MapProject.OilFields[i];
                    if ((of.Wells != null) && (of.Wells.Count > 0))
                    {
                        for (int j = 0; j < of.Wells.Count; j++)
                        {
                            w = of.Wells[j];
                            if (w.UpperCaseName == WellName)
                            {
                                if (retArray == null) retArray = new System.Collections.ArrayList(10);
                                retArray.Add(i);
                                retArray.Add(j);
                            }
                        }
                    }
                }
            }
            return retArray;
        }
        public System.Collections.ArrayList FindOilField(string OilFieldName)
        {
            System.Collections.ArrayList retArray = null;
            if ((MapProject != null) && (MapProject.OilFields != null) && (MapProject.OilFields.Count > 1))
            {
                for (int i = 1; i < MapProject.OilFields.Count; i++)
                {
                    if ((MapProject.OilFields[i]).Name.IndexOf(OilFieldName, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        if (retArray == null) retArray = new System.Collections.ArrayList(5);
                        retArray.Add(i);
                    }
                }
            }
            return retArray;
        }

        // GRID OPERATE
        public C2DLayer CreateSumOnntMap(BackgroundWorker worker, DoWorkEventArgs e, List<OilField> oilfieldList)
        {
            C2DLayer layer = null;
            Grid initGrid = null, grid = null, initGrid2 = null;
            List<Area> areas;

            oilfieldList.Sort(delegate(OilField of1, OilField of2) { return of2.Wells.Count.CompareTo(of1.Wells.Count); });

            List<int> nntOilfieldCodes = new List<int>(new int[] { 1006, 242, 217, 330, 210, 1007, 2228, 142, 174, 208 });
            List<int> nntStratumCodes = new List<int>(new int[] { 184, 284, 185, 190, 350 });

            List<int> nntOfCodes2 = new List<int>(new int[] { 29, 30, 32, 60, 85, 94, 156, 161, 167, 170, 182, 187, 192 });
            List<int> nntOfWithStratumCodes = new List<int>(new int[] { 127, 74, 216, 105, 434, 287, 318, 322, 518, 360, 533, 372, 541 });
            bool nntField, nntField2;
            for (int i = 0; i < oilfieldList.Count; i++)
            {
                nntField = nntOilfieldCodes.IndexOf(oilfieldList[i].OilFieldCode) > -1;
                nntField2 = nntOfCodes2.IndexOf(oilfieldList[i].OilFieldCode) > -1;
                for (int j = 0; j < oilfieldList[i].Grids.Count; j++)
                {
                    grid = oilfieldList[i].Grids[j];
                    if (grid.GridType == C2DObject.st_OilfieldXUnits ||
                        (nntField && (grid.GridType == 2) && nntStratumCodes.IndexOf(grid.StratumCode) > -1) ||
                        (nntField2 && (grid.GridType == 2) && nntOfWithStratumCodes.IndexOf(oilfieldList[i].OilFieldCode + grid.StratumCode) > -1))
                    {
                        if (!grid.DataLoaded)
                        {
                            oilfieldList[i].LoadGridDataFromCacheByIndex(worker, e, j, false);
                        }
                        if (initGrid == null)
                        {
                            initGrid = new Grid(grid);
                            areas = oilfieldList[i].GetAreaList(initGrid.StratumCode, false);
                            initGrid.ConvertToReserves(areas);
                            continue;
                        }
                        if (initGrid != null && grid.DataLoaded)
                        {
                            grid.ReduceGridResolution();
                            areas = oilfieldList[i].GetAreaList(grid.StratumCode, false);
                            grid.ConvertToReserves(areas);
                            initGrid.GridOperation(grid, Grid.GridOperations.Addition);
                            grid.FreeDataMemory();
                        }
                    }
                }
                GC.GetTotalMemory(true);
            }
            C2DLayer layer2 = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
            if (initGrid != null)
            {
                initGrid.CreateGridList(worker, e);
                //layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.GRID, -1);
                //layer.ObjectsList.Add(new Grid(initGrid2));
                //layer.Name = "Sum ONNT1";
                //layer.GetObjectsRect();
                //layer2.Add(layer);

                //layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.GRID, -1);
                //initGrid.UnionAllGrid();
                //layer.ObjectsList.Add(new Grid(grid));
                //layer.Name = "Sum ONNT2";
                //layer.GetObjectsRect();
                //layer2.Add(layer);

                layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.GRID, -1);
                layer.ObjectsList.Add(new Grid(initGrid));
                layer.Name = "Sum ONNT3";
                layer.GetObjectsRect();
                layer2.Add(layer);
                layer2.GetObjectsRect();
            }
            return layer2;
        }

        // VORONOI MAP
        public void CreateVoronoiMap()
        {
            if (newContour == null) return;
            if (mainForm.filterOilObj == null) return;

            ClearSelWellList();
            SelectWellsByContour(newContour);

            CreateVoronoiMapForm.ShowForm(mainForm, MapProject, selWellList, MapProject.maxMerDate, CreateVoronoiMapForm_FormClosing);
        }
        void CreateVoronoiMapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CreateVoronoiMapForm form = (CreateVoronoiMapForm)sender;
            if (form.DialogResult == DialogResult.OK)
            {
                object[] mapParams = new object[6];
                mapParams[0] = form.SelectedDate;
                mapParams[1] = form.SelectedWells;
                mapParams[2] = form.SelectedPlasts;
                mapParams[3] = form.SelectedCutOffRadius;
                mapParams[4] = form.FilterByProduction;
                mapParams[5] = form.SelectedGrid;
                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.CREATE_VORONOI_MAP, mapParams);
            }
            else if (form.DialogResult == DialogResult.Cancel)
            {
                ClearSelWellList();
            }
            mainForm.Focus();
        }
        public void CreateVoronoiMap(BackgroundWorker worker, DoWorkEventArgs e, DateTime Date, List<Well> Wells, List<int> PlastCodes, int CutOffRadius, bool FilterByProduction, Grid selectedGrid)
        {
            if (Wells.Count > 0)
            {
                VoronoiMap VoronoiMap = new VoronoiMap();
                VoronoiMap.ComputeVoronoiMap(worker, e, Date, MapProject, Wells, PlastCodes, CutOffRadius, FilterByProduction, selectedGrid);
                VoronoiMap.SetIconsFonts(iconFonts, exFontList);

                int i, len = 1;
                ClearSelWellList();
                for (i = 0; i < VoronoiMap.Cells.Length; i++)
                {
                    VoronoiMap.Cells[i].Well.srcWell.Selected = true;
                    selWellList.Add(VoronoiMap.Cells[i].Well.srcWell);
                }
                C2DLayer layer = null;
                string str;
                if (VoronoiMap.Name != null)
                {
                    for (i = 0; i < LayerWorkList.Count; i++)
                    {
                        if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                        {
                            str = ((C2DLayer)LayerWorkList[i]).Name;
                            if ((str != null) && (str.IndexOf(VoronoiMap.Name) > -1)) len++;
                        }
                    }
                }
                layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP, -1);
                layer.ObjectsList.Add(VoronoiMap);

                layer.fontSize = this._fontSize;

                layer.ResetFontsPens();
                layer.Visible = true;
                layer.VisibleFar = true;
                layer.Name = VoronoiMap.Name;
                if (len > 1) layer.Name += string.Format(" ({0})", len);
                layer.GetObjectsRect();
                layer.Index = LayerWorkList.Count;

                if (e != null) e.Result = layer;
            }
        }
        public void ShowVoronoiCellsParams(VoronoiMap Map, bool ShowAllCells)
        {
            StratumTreeNode plastNode;

            string plasts = string.Empty;
            var dict = (StratumDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            for (int i = 0; i < Map.PlastCodes.Count; i++)
            {
                plastNode = dict.GetStratumTreeNode(Map.PlastCodes[i]);
                if (plastNode != null)
                {
                    if (i == 1) plasts += " [";
                    if (i > 1) plasts += ", ";
                    plasts += plastNode.Name;
                    if (i > 0 && i == Map.PlastCodes.Count - 1) plasts += "]";
                }
            }
            OutputTextForm form = mainForm.OutBox;
            if (form.CountRows > 0) form.Clear();
            form.Text = "Сообщения";
            form.AddBoldText("Запасы по областям Вороного."); form.NewLine();
            form.AddBoldText("Объект: "); form.AddText(Map.Name); form.NewLine();
            form.AddBoldText("Расчет на дату: "); form.AddText(Map.Date.ToShortDateString()); form.NewLine();
            form.AddBoldText("Пласт(ы): "); form.AddText(plasts); form.NewLine();
            form.AddBoldText("Фильтрация скважин по добыче: "); form.AddText(Map.FilteredByProduction ? "Да" : "Нет"); form.NewLine();
            form.AddBoldText("Сетка для расчета запасов: ");
            string grids = string.Empty;
            for (int i = 0; i < Map.GridParams.Length; i++)
            {
                if (i > 0) grids += ", ";
                grids += Map.GridParams[i].Name;
                if (Map.GridParams[i].Index == -1) grids += "(Рабочая папка)";
            }
            form.AddText(grids); form.NewLine();
            form.NewLine();
            form.AddBoldText("Скважина    \t  НБЗ, тыс.т\t  НИЗ, тыс.т\t  ОИЗ, тыс.т\t  Нак.Доб.нефти, тыс.т"); form.NewLine();
            int counter = 0;
            double sumNBZ = 0, sumNIZ = 0, sumAccumOil = 0;
            for (int i = 0; i < Map.Cells.Length; i++)
            {
                if (ShowAllCells || Map.Cells[i].Selected)
                {
                    form.AddText(string.Format("{0,-16}\t{1,15:#,##0.###}\t{2,15:#,##0.###}\t{3,15:#,##0.###}\t{4,15:#,##0.###}", Map.Cells[i].Well.srcWell.Name, Map.Cells[i].NBZ / 1000, Map.Cells[i].NIZ / 1000, (Map.Cells[i].NIZ - Map.Cells[i].AccumOil) / 1000, Map.Cells[i].AccumOil / 1000));
                    form.NewLine();
                    sumNBZ += Map.Cells[i].NBZ;
                    sumNIZ += Map.Cells[i].NIZ;
                    sumAccumOil += Map.Cells[i].AccumOil;
                    counter++;
                }
            }
            if (counter > 1)
            {
                form.AddText(string.Format("{0,-16}\t{1,15:#,##0.###}\t{2,15:#,##0.###}\t{3,15:#,##0.###}\t{4,15:#,##0.###}", "Сумма", sumNBZ / 1000, sumNIZ / 1000, (sumNIZ - sumAccumOil) / 1000, sumAccumOil / 1000));
                form.NewLine();
            }
            form.NewLine();
            form.AddBoldText("Комментарий:\n");
            form.AddText("Интегрирование сеток при расчете запасов производится приблизительным методом (метод палетки).");
            form.SetStartPosition();
            form.ShowDialog();
        }

        // CENTER MAP
        public void CenterXY(double realX, double realY)
        {
            double scrX, scrY, scrWX, scrWY;

            scrX = this.ClientRectangle.Width / 2;
            scrY = this.ClientRectangle.Height / 2;
            scrWX = C2DObject.ScreenXfromRealX(realX);
            scrWY = C2DObject.ScreenYfromRealY(realY);
            BitMapRect.X = -(int)(scrX - scrWX);
            BitMapRect.Y = -(int)(scrY - scrWY);
        }
        public void SetCenterSkv(int OilFieldIndex, int WellIndex, bool IsProject, bool UpdateLastSelObj)
        {
            ClearSelectedList();
            ClearSelWellList();

            if ((OilFieldIndex > 0) && (OilFieldIndex < MapProject.OilFields.Count) && (WellIndex >= 0) &&
                ((!IsProject && WellIndex < MapProject.OilFields[OilFieldIndex].Wells.Count) ||
                 (IsProject && WellIndex < MapProject.OilFields[OilFieldIndex].ProjWells.Count)))
            {
                Well w;
                if (IsProject)
                {
                    w = MapProject.OilFields[OilFieldIndex].ProjWells[WellIndex];
                }
                else
                {
                    w = MapProject.OilFields[OilFieldIndex].Wells[WellIndex];
                }
                w.Selected = true;
                selWellList.Add(w);

                mainForm.filterOilObj.SetWellOilObj(w, true);
                mainForm.filterOilObj.UpdateCurrentTreeView();

                selMerOilObjCodes.Clear();

                double x = (w.CoordLoaded && w.coord.Count > 0) ? w.X : MapProject.OilFields[OilFieldIndex].X;
                double y = (w.CoordLoaded && w.coord.Count > 0) ? w.Y : MapProject.OilFields[OilFieldIndex].Y;

                C2DObject.st_XUnitsInOnePixel = (w.CoordLoaded && w.coord.Count > 0) ? 6 : C2DObject.st_OilfieldXUnits * 1.3f;
                C2DObject.st_YUnitsInOnePixel = (w.CoordLoaded && w.coord.Count > 0) ? 6 : C2DObject.st_OilfieldXUnits * 1.3f;
                C2DObject.st_VisbleLevel = C2DObject.st_XUnitsInOnePixel;

                ReCalcFontSizes();
                BitMapRect.X = 0;
                BitMapRect.Y = 0;
                PreparePaint();
                this.CenterXY(x, y);
                SetSelectedObj(w);

                if (w.ProjectDest == 0)
                {
                    if (UpdateLastSelObj)
                    {
                        LastSelectObject = w;
                        if (OnChangeSelectedObject != null) OnChangeSelectedObject(LastSelectObject);
                    }
                    mainForm.dGridShowMer(OilFieldIndex, w.Index);
                    mainForm.dGridShowChess(OilFieldIndex, w.Index);
                    mainForm.dGridShowGIS(OilFieldIndex, w.Index);
                    mainForm.dGridShowCore(OilFieldIndex, w.Index);
                    mainForm.dGridShowCoreTest(OilFieldIndex, w.Index);
                    mainForm.dChartShowData(OilFieldIndex, w.Index);

                    mainForm.planeLoadWell(w);
                    mainForm.WL_SelectWell(w);
                }
                else
                {
                    mainForm.dGridShowChess(-1, -1);
                    mainForm.dGridShowMer(-1, -1);
                    mainForm.dGridShowGIS(-1, -1);
                    mainForm.dGridShowCore(-1, -1);
                    mainForm.dGridShowCoreTest(-1, -1);
                    mainForm.dChartShowData(-1, -1);
                    mainForm.planeLoadWell(null);
                }

                SetActiveOilField(OilFieldIndex);

                DrawLayerList();
                this.Focus();
            }
        }
        public void SetCenterOilField(int OilFieldIndex, bool UpdateLastSelObject)
        {
            int i;
            ClearSumList();
            ClearSelectedList();
            ClearSelWellList();

            if ((OilFieldIndex > 0) && (OilFieldIndex < MapProject.OilFields.Count))
            {
                OilField of = MapProject.OilFields[OilFieldIndex];
                C2DLayer layer;

                for (i = 0; i < LayerList.Count; i++)
                {
                    layer = ((C2DLayer)LayerList[i]).GetCondContourByOfIndex(OilFieldIndex);
                    if (layer != null)
                    {
                        if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                        {

                            if (layer.oilfield != null)
                            {
                                mainForm.filterOilObj.SetOilFieldOilObj(layer.oilfield, selLayerList.Count == 0);
                                mainForm.filterOilObj.UpdateCurrentTreeView();
                                SetActiveOilField(layer.oilFieldIndex);
                            }
                            AddSelectLayer(layer);
                            AddSumOilField((Contour)layer.ObjectsList[0]);
                        }
                        else
                            AddSelectLayer(layer);
                        break;
                    }
                }
                if (UpdateLastSelObject && selLayerList.Count > 0 && ((C2DLayer)selLayerList[0]).oilfield != null)
                {
                    LastSelectObject = ((C2DLayer)selLayerList[0]).oilfield;
                    if (OnChangeSelectedObject != null) OnChangeSelectedObject(LastSelectObject);
                }

                C2DObject.st_XUnitsInOnePixel = C2DObject.st_OilfieldXUnits * 1.3f;
                C2DObject.st_YUnitsInOnePixel = C2DObject.st_OilfieldXUnits * 1.3f;
                C2DObject.st_VisbleLevel = C2DObject.st_XUnitsInOnePixel;

                ReCalcFontSizes();
                BitMapRect.X = 0;
                BitMapRect.Y = 0;
                PreparePaint();
                this.CenterXY(of.MinMaxXY.Left + of.MinMaxXY.Width / 2, of.MinMaxXY.Top + of.MinMaxXY.Height / 2);
                swells.OilFieldName = of.Name;
                swells.Name = "";
                mainForm.dChartSetWindowText(String.Format("Графики - Месторождение {0} [{1}]", of.Name, of.ParamsDict.NGDUName));
                SetActiveOilField(OilFieldIndex);
                mainForm.pgProperties.Refresh();
                mainForm.ClearAllTables();
                DrawLayerList();
                this.Focus();
            }
        }
        public void SetCenterArea(int OilFieldIndex, int AreaIndex, bool UpdateLastSelObject)
        {
            ClearSelectedList();
            ClearSelWellList();

            if ((OilFieldIndex > 0) && (OilFieldIndex < MapProject.OilFields.Count) &&
                (AreaIndex >= 0) && (AreaIndex < (MapProject.OilFields[OilFieldIndex]).Areas.Count))
            {
                OilField of = MapProject.OilFields[OilFieldIndex];
                Area area = (Area)of.Areas[AreaIndex];
                mainForm.EnableMarkers();
                ClearSelectedList();
                C2DObject.st_VisbleLevel = 65;
                C2DObject.st_XUnitsInOnePixel = 65;
                C2DObject.st_YUnitsInOnePixel = 65;
                ReCalcFontSizes();

                if (UpdateLastSelObject)
                {
                    LastSelectObject = area;
                    if (OnChangeSelectedObject != null) OnChangeSelectedObject(LastSelectObject);
                }

                BitMapRect.X = 0;
                BitMapRect.Y = 0;
                PreparePaint();
                this.CenterXY(area.X, area.Y);

                area.contour.Selected = true;
                C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, OilFieldIndex);
                area.Selected = true;
                layer.ObjectsList.Add(area.contour);
                layer.oilfield = of;
                layer.Level = area.Index;

                mainForm.dChartSetWindowText(String.Format("Графики - {0} [{1}, {2}]", area.Name, of.ParamsDict.NGDUName, of.Name));
                ClearSelectedList();

                SetActiveOilField(OilFieldIndex);
                AddSelectLayer(layer);
                SelectArea(area);
                SetVisibleArea(area);

                mainForm.pgProperties.Refresh();
                mainForm.ClearAllTables();
                DrawLayerList();
            }
        }
        public void SetCenterProject()
        {
            if (MapProject != null)
            {
                PreparePaint();
                C2DObject.st_XUnitsInOnePixel = C2DObject.st_MaxXUnits;
                C2DObject.st_YUnitsInOnePixel = C2DObject.st_MaxXUnits;
                C2DObject.st_VisbleLevel = C2DObject.st_XUnitsInOnePixel;
                ReCalcFontSizes();
                BitMapRect.X = 0;
                BitMapRect.Y = 0;
                PreparePaint();
                CenterXY(MapProject.MinMaxXY.Left + MapProject.MinMaxXY.Width / 2, MapProject.MinMaxXY.Top + MapProject.MinMaxXY.Height / 2);
                DrawLayerList();
            }
        }
        public void SetCenterNGDU(int NGDUCode)
        {
            int i;
            ClearSumList();
            ClearSelectedList();
            ClearSelWellList();

            if (MapProject.OilFields.Count > 0)
            {
                OilField of = MapProject.OilFields[0];
                List<C2DLayer> list = new List<C2DLayer>();
                C2DLayer layer = null;
                for (i = 0; i < LayerList.Count; i++)
                {
                    list.Clear();
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, list);
                    for(int j = 0; j < list.Count;j++)
                    {
                        if (((Contour)list[j].ObjectsList[0])._ContourType == -2 && ((Contour)list[j].ObjectsList[0])._NGDUCode == NGDUCode)
                        {
                            layer = list[j];
                            break;
                        }
                    }
                    if (layer != null) break;
                }
                if (layer != null)
                {
                    C2DObject.st_XUnitsInOnePixel = C2DObject.st_NgduXUnits * 1.3f * 1.3f * 1.3f;
                    C2DObject.st_YUnitsInOnePixel = C2DObject.st_NgduXUnits * 1.3f * 1.3f * 1.3f;
                    C2DObject.st_VisbleLevel = C2DObject.st_XUnitsInOnePixel;
                    ReCalcFontSizes();
                    BitMapRect.X = 0;
                    BitMapRect.Y = 0;
                    PreparePaint();
                    
                    ClearActiveOilField();
                    AddSelectLayer(layer);
                    var cntr = (Contour)layer.ObjectsList[0];
                    AddSumNGDU(cntr);
                    mainForm.dChartSetWindowText("Графики - " + layer.Name);
                    this.CenterXY((cntr.Xmin + cntr.Xmax) / 2, (cntr.Ymin + cntr.Ymax) / 2);
                }

                mainForm.ClearAllTables();
                DrawLayerList();
                this.Focus();
            }
        }

        // UPDATE OBJ DATA
        public void UpdateCntrBrushByOilObj()
        {
            List<C2DLayer> list = new List<C2DLayer>();
            C2DLayer layer;
            int i, j;
            for (i = 0; i < LayerList.Count; i++)
            {
                layer = (C2DLayer)LayerList[i];
                layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.CONTOUR, list);
            }
            for (i = 0; i < list.Count; i++)
            {
                layer = (C2DLayer)list[i];
                for (j = 0; j < layer.ObjectsList.Count; j++)
                {
                    ((Contour)layer.ObjectsList[j]).ResetBrush();
                }
            }
        }

        // SELECTING OBJECTS
        public void SelectWell(Well w)
        {
            if (w != null)
            {
                ClearSelWellList();
                selWellList.Add(w);
                w.Selected = true;
                LastSelectObject = w;
                if (OnChangeSelectedObject != null) OnChangeSelectedObject(LastSelectObject);

                mainForm.filterOilObj.SetWellOilObj(w, true);
                mainForm.filterOilObj.UpdateCurrentTreeView();
                selMerOilObjCodes.Clear();

                mainForm.dGridShowChess(w.OilFieldIndex, w.Index);
                mainForm.dGridShowMer(w.OilFieldIndex, w.Index);
                mainForm.dGridShowGIS(w.OilFieldIndex, w.Index);
                mainForm.dGridShowCore(w.OilFieldIndex, w.Index);
                mainForm.dGridShowCoreTest(w.OilFieldIndex, w.Index);
                mainForm.dChartShowData(w.OilFieldIndex, w.Index);
                mainForm.planeLoadWell(w);
                SetActiveOilField(w.OilFieldIndex);
                DrawLayerList();
            }
        }
        public Well[] GetWellsByNewContour(bool UseProjectWells)
        {
            if (newContour != null)
            {
                int i, j;
                int minX, maxX, minY, maxY;
                C2DLayer layer;
                Well w;

                List<C2DLayer> list = new List<C2DLayer>();
                ArrayList welllist = new ArrayList();
                Well[] wellArray;
                for (i = 0; i < LayerList.Count; i++)
                {
                    layer = (C2DLayer)LayerList[i];
                    if (layer.Visible)
                    {
                        layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list, true);
                        if (UseProjectWells) layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL, list, true);
                    }
                }

                for (i = 0; i < list.Count; i++)
                {
                    layer = (C2DLayer)list[i];
                    minX = (int)layer.ObjRect.Left;
                    maxX = (int)layer.ObjRect.Right;
                    minY = (int)layer.ObjRect.Top;
                    maxY = (int)layer.ObjRect.Bottom;
                    if (newContour.PointBoundsEntry(minX, minY) ||
                        newContour.PointBoundsEntry(minX, maxY) ||
                        newContour.PointBoundsEntry(maxX, minY) ||
                        newContour.PointBoundsEntry(maxX, maxY) ||
                        newContour.ContourInBounds(minX, minY, maxX, maxY))
                    {
                        for (j = 0; j < layer.ObjectsList.Count; j++)
                        {
                            w = (Well)layer.ObjectsList[j];
                            if ((w.Visible) && (newContour.PointBodyEntry(w.X, w.Y)))
                            {
                                w.OilFieldIndex = layer.oilFieldIndex;
                                welllist.Add(w);
                            }
                        }
                    }
                }
                list.Clear();
                wellArray = new Well[welllist.Count];
                for (i = 0; i < welllist.Count; i++)
                {
                    wellArray[i] = (Well)welllist[i];
                }
                return wellArray;
            }
            else
                return null;
        }
        private void SelectWellsByContour(Contour cntr)
        {
            if ((LayerList != null) && (LayerList.Count > 0))
            {
                int i, j;
                int minX, maxX, minY, maxY;
                C2DLayer layer;
                Well w;
                ClearSelWellList();
                selMerOilObjCodes.Clear();

                List<C2DLayer> list = new List<C2DLayer>();
                for (i = 0; i < LayerList.Count; i++)
                {
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list, true);
                }

                for (i = 0; i < list.Count; i++)
                {
                    layer = (C2DLayer)list[i];
                    if (!layer.Visible) continue;
                    minX = (int)layer.ObjRect.Left;
                    maxX = (int)layer.ObjRect.Right;
                    minY = (int)layer.ObjRect.Top;
                    maxY = (int)layer.ObjRect.Bottom;
                    if (cntr.PointBoundsEntry(minX, minY) ||
                        cntr.PointBoundsEntry(minX, maxY) ||
                        cntr.PointBoundsEntry(maxX, minY) ||
                        cntr.PointBoundsEntry(maxX, maxY) ||
                        cntr.ContourInBounds(minX, minY, maxX, maxY))
                    {
                        for (j = 0; j < layer.ObjectsList.Count; j++)
                        {
                            w = (Well)layer.ObjectsList[j];
                            if (w.Visible && cntr.PointBodyEntry(w.X, w.Y))
                            {
                                w.Selected = true;
                                w.OilFieldIndex = layer.oilFieldIndex;
                                selWellList.Add(w);
                            }
                        }
                    }
                }
            }
        }
        public void RemoveSelectWell(Well w)
        {
            if (selWellList.Count > 0)
            {
                int ind = selWellList.IndexOf(w);
                if (ind != -1)
                {
                    selWellList.RemoveAt(ind);
                    w.Selected = false;
                    DrawLayerList();
                }
            }
        }
        private void SelectWellsByArea(Area area)
        {
            if ((area != null) && (area.WellList != null) && (area.WellList.Length > 0))
            {
                int i, j;
                Well w;
                ClearSelWellList();
                selMerOilObjCodes.Clear();

                for (i = 0; i < area.WellList.Length; i++)
                {
                    w = area.WellList[i];
                    w.Selected = true;
                    w.OilFieldIndex = area.OilFieldIndex;
                    selWellList.Add(w);
                }
            }
        }
        public void SelectWellsByPhantom(bool IsOneDate)
        {
            if ((this.SelObject != null) && (this.SelObject.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                if (((C2DLayer)this.SelObject).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    this.UpdateFilterOilObj = true;
                    C2DLayer layer = (C2DLayer)this.SelObject;
                    int i, j;
                    Well w;
                    ClearSelWellList();
                    selMerOilObjCodes.Clear();

                    PhantomWell pw;
                    for (i = 0; i < layer.ObjectsList.Count; i++)
                    {
                        pw = (PhantomWell)layer.ObjectsList[i];
                        w = pw.srcWell;
                        w.Selected = true;
                        if (IsOneDate)
                        {
                            selWellList.Add(pw);
                        }
                        else
                        {
                            selWellList.Add(w);
                        }
                    }
                    mainForm.dChartSetWindowText(String.Format("График - Список скважин[{0}]", selWellList.Count));
                    IsGraphicsByOneDate = IsOneDate;
                    AddSumWellList(true);
                    mainForm.ShowDataChartPane();
                }
            }
        }
        public void SelectArea(Area area)
        {
            mainForm.filterOilObj.SetAreaOilObj(area, selLayerList.Count == 0);
            mainForm.filterOilObj.UpdateCurrentTreeView();
            AddSumArea(area);
        }
        public void AddContour(OilField TiedOilField)
        {
            TreeNode tn = null, tnUserData = null;
            if (twLayers.Nodes.Count > 0)
            {
                tnUserData = twLayers.Nodes[0].Nodes[twLayers.Nodes[0].Nodes.Count - 1];
            }
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Выберите Shp файл *.shp";
            dlg.Filter = "Shp файл (*.shp)|*.SHP";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                switch (Path.GetExtension(dlg.FileName))
                {
                    case ".shp":
                        int i, j, pind = 0;
                        ShpData shp = ShpReader.ReadShapeFile(dlg.FileName);
                        shpPoint point;
                        shpPolygon poly;
                        if (shp != null)
                        {

                            if (tnUserData != null)
                                for (i = 0; i < tnUserData.Nodes.Count; i++)
                                {
                                    if (tnUserData.Nodes[i].Text == "Контуры")
                                    {
                                        tn = tnUserData.Nodes[i];
                                        break;
                                    }
                                }
                            twLayers.StartDataUpdate();
                            if (tn == null) tn = tnUserData.Nodes.Add("Контуры");
                            C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, 0);
                            layer.oilfield = TiedOilField;
                            layer.Name = Path.GetFileName(shp._fileName);
                            layer.Visible = true;
                            layer.fontSize = this._fontSize;
                            for (i = 0; i < shp._ObjList.Count; i++)
                            {
                                switch (((shpBase)shp._ObjList[i]).TypeID)
                                {
                                    case (int)Constant.SHAPEFILE_TYPES_ID.POINT:
                                        reLog.AppendText("Неправильный формат файла!" + Environment.NewLine);
                                        break;
                                    case (int)Constant.SHAPEFILE_TYPES_ID.POLYGON:
                                        poly = (shpPolygon)shp._ObjList[i];
                                        reLog.AppendText("Polygon " + poly.Name + Environment.NewLine);
                                        double[] points = new double[poly.numPoints * 2];
                                        pind = 0;
                                        for (j = 0; j < poly.numPoints; j++)
                                        {
                                            point = (shpPoint)poly.points[j];
                                            points[pind++] = point.X;
                                            points[pind++] = point.Y;
                                            reLog.AppendText(point.X.ToString() + "  " + point.Y.ToString() + Environment.NewLine);
                                        }
                                        if (poly.Name == "") poly.Name = Path.GetFileName(shp._fileName) + "(" + i.ToString() + ")";
                                        layer.Add((C2DObject)layer.oilfield.LoadContour(poly.Name, 0, 0, points));
                                        break;
                                }
                            }
                            layer.GetObjectsRect();
                            layer.ResetFontsPens();
                            layer.Index = LayerList.Add(layer);
                            tn = tn.Nodes.Add("", layer.Name, 0, 0);
                            tn.Tag = layer;
                            tn.Checked = layer.Visible;
                            layer.node = tn;
                            twLayers.EndDataUpdate();
                        }
                        break;
                }
            }
            dlg.Dispose();
        }

        public bool RemoveSelectedLayer(C2DLayer layer)
        {
            bool find = false;
            int ind = -1;
            if (layer.oilFieldIndex != -1)
            {
                ind = selLayerList.IndexOf(layer);
                if (ind > -1)
                {
                    ((C2DLayer)selLayerList[ind]).Selected = false;
                    selLayerList.RemoveAt(ind);
                    find = true;
                }
            }
            else
            {
                C2DLayer lr;
                for (int i = 0; i < selLayerList.Count; i++)
                {
                    lr = (C2DLayer)selLayerList[i];
                    if ((lr.oilFieldIndex == -1) && (layer.Level == lr.Level) && (layer.Name == lr.Name) && (lr.oilfield == layer.oilfield))
                    {
                        lr.Selected = false;
                        selLayerList.RemoveAt(i);
                        find = true;
                        break;
                    }
                }
            }
            return find;
        }
        public int GetSelLayersCountByContourType(int ContourType)
        {
            int count = 0;
            C2DLayer lr;
            for (int i = 0; i < selLayerList.Count; i++)
            {
                lr = (C2DLayer)selLayerList[i];
                if (lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                   lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    if (((Contour)lr.ObjectsList[0])._ContourType == ContourType)
                    {
                        count++;
                    }
                }
            }
            return count;
        }
        public C2DLayer GetLayerByContourType(int ContourType)
        {
            C2DLayer lr;
            for (int i = 0; i < selLayerList.Count; i++)
            {
                lr = (C2DLayer)selLayerList[i];
                if (lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                   lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    if (((Contour)lr.ObjectsList[0])._ContourType == ContourType)
                    {
                        return lr;
                    }
                }
            }
            return null;
        }
        public List<C2DLayer> GetLayerListByContourType(int ContourType)
        {
            C2DLayer lr;
            List<C2DLayer> list = new List<C2DLayer>();
            for (int i = 0; i < selLayerList.Count; i++)
            {
                lr = (C2DLayer)selLayerList[i];
                if (lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                   lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    if (((Contour)lr.ObjectsList[0])._ContourType == ContourType)
                    {
                        list.Add(lr);
                    }
                }
            }
            return list;
        }
        public C2DLayer GetLayerByObject(C2DLayer Layer, C2DObject Object)
        {
            C2DLayer result = null;
            if (Layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                for (int i = 0; i < Layer.ObjectsList.Count; i++)
                {
                    result = GetLayerByObject((C2DLayer)Layer.ObjectsList[i], Object);
                    if (result != null) return result;
                }
            }
            else
            {
                for (int i = 0; i < Layer.ObjectsList.Count; i++)
                {
                    if (Layer.ObjectsList[i] == Object)
                    {
                        return Layer;
                    }
                }
            }
            return result;
        }
        public void AddSelectLayer(C2DLayer layer)
        {
            if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR || layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
            {
                int i;
                C2DLayer lr, lr2;
                Contour cntr, contour;
                bool find;
                int ind;
                contour = (Contour)layer.ObjectsList[0];
                if ((selLayerList.Count > 0) && (layer.ObjectsList.Count > 0))
                {
                    find = false;
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        lr = (C2DLayer)selLayerList[i];
                        if ((lr.ObjectsList.Count > 0) &&
                            (lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR ||
                             lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
                        {
                            cntr = (Contour)lr.ObjectsList[0];
                            if ((cntr._ContourType != contour._ContourType) && ((cntr._ContourType == -4) || (contour._ContourType == -4)))
                            {
                                find = true;
                                break;
                            }
                        }
                    }
                    if (find)
                    {
                        ClearSelectedList();
                        ClearSumList();
                    }
                }
                List<C2DLayer> list = new List<C2DLayer>();
                find = false;
                switch (contour._ContourType)
                {
                    case -1:
                        find = RemoveSelectedLayer(layer);
                        for (i = 0; i < LayerList.Count; i++)
                        {
                            ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, list);
                        }
                        for (i = 0; i < list.Count; i++)
                        {
                            lr = (C2DLayer)list[i];
                            if ((lr.oilfield != null) && (lr.oilfield.ParamsDict.EnterpriseCode == contour._EnterpriseCode) &&
                                (((Contour)lr.ObjectsList[0])._ContourType != -1))
                            {
                                ind = selLayerList.IndexOf(lr);
                                if ((!find) && (ind == -1))
                                {
                                    lr.Selected = true;
                                    selLayerList.Add(lr);
                                }
                                else if ((find) && (ind > -1))
                                {
                                    ((C2DLayer)selLayerList[ind]).Selected = false;
                                    selLayerList.RemoveAt(ind);
                                }
                            }
                        }
                        break;
                    case -2:
                        find = RemoveSelectedLayer(layer);
                        for (i = 0; i < LayerList.Count; i++)
                        {
                            ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, list);
                        }
                        for (i = 0; i < list.Count; i++)
                        {
                            lr = (C2DLayer)list[i];
                            if ((lr.oilfield != null) && (lr.oilfield.ParamsDict.NGDUCode == contour._NGDUCode) &&
                                (((Contour)lr.ObjectsList[0])._ContourType != -2))
                            {
                                ind = selLayerList.IndexOf(lr);
                                if ((!find) && (ind == -1))
                                {
                                    lr.Selected = true;
                                    selLayerList.Add(lr);
                                }
                                else if ((find) && (ind > -1))
                                {
                                    ((C2DLayer)selLayerList[ind]).Selected = false;
                                    selLayerList.RemoveAt(ind);
                                }
                            }
                        }
                        break;
                    case -3:
                        goto case -4;
                    case -4:
                        find = RemoveSelectedLayer(layer);
                        break;
                }
                if (!find)
                {
                    layer.Selected = true;
                    selLayerList.Add(layer);
                }

                if (selLayerList.Count > 0)
                {
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        lr = (C2DLayer)selLayerList[i];
                        if ((lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) && (lr.ObjectsList.Count > 0) &&
                            (((Contour)lr.ObjectsList[0])._ContourType > -3))
                        {
                            cntr = (Contour)lr.ObjectsList[0];
                            find = false;
                            for (int j = 0; j < selLayerList.Count; j++)
                            {
                                lr2 = (C2DLayer)selLayerList[j];

                                if ((lr2.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) && (lr2.ObjectsList.Count > 0) &&
                                    (((Contour)lr2.ObjectsList[0])._ContourType == -3))
                                {
                                    if ((lr2.oilfield != null) &&
                                        (((cntr._ContourType == -1) && (cntr._EnterpriseCode == lr2.oilfield.ParamsDict.EnterpriseCode)) ||
                                         ((cntr._ContourType == -2) && (cntr._NGDUCode == lr2.oilfield.ParamsDict.NGDUCode))))
                                    {
                                        find = true;
                                        break;
                                    }
                                }

                            }
                            if (!find)
                            {
                                lr.Selected = false;
                                selLayerList.RemoveAt(i);
                            }
                        }
                    }
                }
                SetBubbleSettingMinMax();
            }
            else if (layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.GRID)
            {
                layer.Selected = true;
                selLayerList.Add(layer);
            }
        }
        public void ClearSelectedList()
        {
            if ((SelObject != null) && (SelObject.TypeID == Constant.BASE_OBJ_TYPES_ID.WELL))
            {
                ((Well)SelObject).Selected = false;
            }
            if ((SelObject != null) && (SelObject.TypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL))
            {
                ((Well)SelObject).Selected = false;
            }
            if ((SelObject != null) && (SelObject.TypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL))
            {
                ((PhantomWell)SelObject).Selected = false;
            }
            if ((SelObject != null) && (SelObject.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                ((C2DLayer)SelObject).Selected = false;
            }
            for (int i = 0; i < selLayerList.Count; i++)
            {
                if (((C2DLayer)selLayerList[i]).ObjTypeID != Constant.BASE_OBJ_TYPES_ID.PROFILE)
                {
                    ((C2DLayer)selLayerList[i]).Selected = false;
                }
            }
            twWork.ClearSelectedList();
            mainForm.pgProperties.SelectedObject = null;
            selLayerList.Clear();
        }
        public void ClearSelWellList()
        {
            for (int i = 0; i < selWellList.Count; i++)
            {
                ((C2DObject)selWellList[i]).Selected = false;
                if (((C2DObject)selWellList[i]).TypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    ((PhantomWell)selWellList[i]).srcWell.Selected = false;
                }
            }
            selWellList.Clear();
        }

        // AREA
        public void SetVisibleArea(Area area)
        {
            List<C2DLayer> list = new List<C2DLayer>();
            for (int i = 0; i < LayerList.Count; i++)
            {
                ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.AREA, list, false);
            }
            bool find = false;
            C2DLayer layer;
            for (int i = 0; i < list.Count; i++)
            {
                layer = (C2DLayer)list[i];
                if (layer.oilFieldIndex == area.OilFieldIndex)
                {
                    find = false;
                    for (int j = 0; j < layer.ObjectsList.Count; j++)
                    {
                        if (area == (Area)layer.ObjectsList[j])
                        {
                            find = true;
                        }
                    }
                    layer.Visible = find;
                    if (layer.node != null) layer.node.Checked = find;
                }
            }
            twActiveOilField.UpdateRecursiveTreeView();
        }

        // CONTOURS
        public void SetContoursAverageStep(C2DLayer layer)
        {
            int step = ContourStepForm.ShowForm(mainForm);
            if ((step > 0) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
            {
                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    Contour cntr = (Contour)layer.ObjectsList[i];
                    layer.ObjectsList[i] = cntr.GetSimplyContour(step);
                }
                AddSelectLayer(layer);
                WriteWorkLayer(layer);
                DrawLayerList();
            }
        }
        public void OperateContours(List<C2DLayer> Layers, ContoursOperation Operation)
        {
            OperateContours(Layers, Operation, null);
        }
        public void OperateContours(List<C2DLayer> Layers, ContoursOperation Operation, List<C2DLayer> DiffLayers)
        {
            if (Operation == ContoursOperation.Union || Operation == ContoursOperation.Crossing)
            {
                Polygon poly = new Polygon();
                int i, j, index = 0;
                for (i = 0; i < Layers.Count; i++)
                {
                    poly.AddContour(Layers[i].ObjectsList);
                    if (i > 0) poly.OperateByContours(Operation);
                    #region ----
                    //for (j = 0; j < Layers[i]._ObjectsList.Count; j++)
                    //{
                    //    cntr = (Contour)Layers[i]._ObjectsList[j];
                    //    //if (j == 0)
                    //    //{
                    //    //    bool clockWise = cntr.GetClockWiseByX(true);
                    //    //    if (!clockWise)
                    //    //    {
                    //    //        cntr = new Contour(cntr);
                    //    //        cntr.Invert();
                    //    //    }
                    //    //}
                    //    poly.AddContour(cntr);
                    //    poly.OperateByContours(Operation);
                    //}
                    #endregion
                }
                List<Contour> cntrs = poly.GetContours();
                if (cntrs.Count > 0)
                {
                    C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, 0);
                    for (i = 0; i < cntrs.Count; i++)
                    {
                        cntrs[i]._Closed = true;
                        //cntrs[i]._FillBrush = 11;
                        layer.Add(cntrs[i]);
                    }
                    layer.ResetFontsPens();
                    layer.GetObjectsRect();
                    copyLayer = layer;
                    PasteWorkLayer(false);
                }
            }
            else if (DiffLayers != null)
            {
                Polygon poly = new Polygon();
                Contour cntr;
                List<Contour> list = new List<Contour>();
                for (int i = 0; i < Layers.Count; i++)
                {
                    poly.Clear();
                    poly.AddContour(Layers[i].ObjectsList);
                    for (int k = 0; k < DiffLayers.Count; k++)
                    {
                        poly.AddContour(DiffLayers[k].ObjectsList);
                        poly.OperateByContours(Operation);
                    }
                    #region ---
                    //for (int j = 0; j < Layers[i]._ObjectsList.Count; j++)
                    //{
                    //    cntr = (Contour)Layers[i]._ObjectsList[j];
                    //    if (j == 0)
                    //    {
                    //        bool clockWise = cntr.GetClockWiseByX(true);
                    //        if (!clockWise)
                    //        {
                    //            cntr = new Contour(cntr);
                    //            cntr.Invert();
                    //        }
                    //    }
                    //    poly.AddContour(cntr);
                    //    for (int k = 0; k < DiffLayers.Count; k++)
                    //    {
                    //        poly.AddContour(DiffLayers[k]._ObjectsList[0]
                    //    }
                    //}
                    #endregion
                    List<Contour> cntrs = poly.GetContours();
                    if (cntrs.Count > 0)
                    {
                        C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, 0);
                        for (int j = 0; j < cntrs.Count; j++)
                        {
                            cntrs[j]._Closed = true;
                            //cntrs[j]._FillBrush = 11;
                            layer.Add(cntrs[j]);
                        }
                        layer.ResetFontsPens();
                        layer.GetObjectsRect();
                        copyLayer = layer;
                        PasteWorkLayer(false);
                    }
                }
            }
        }
        public void SetContoursNearPoint(C2DLayer layer)
        {
            if ((LayerWorkList.Count > 0) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
            {
                Contour cntr = (Contour)layer.ObjectsList[0];
                Contour targCntr;

                double dist;
                int i, j, k;
                float scrX, scrY, scrX2, scrY2;
                for (i = 0; i < LayerWorkList.Count; i++)
                {
                    if (i != layer.Index && ((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                    {
                        targCntr = (Contour)((C2DLayer)LayerWorkList[i]).ObjectsList[0];
                        for (j = 0; j < cntr._points.Count; j++)
                        {
                            scrX = C2DObject.ScreenXfromRealX(cntr._points[j].X);
                            scrY = C2DObject.ScreenYfromRealY(cntr._points[j].Y);
                            for (k = 0; k < targCntr._points.Count; k++)
                            {
                                scrX2 = C2DObject.ScreenXfromRealX(targCntr._points[k].X);
                                scrY2 = C2DObject.ScreenYfromRealY(targCntr._points[k].Y);
                                dist = Math.Sqrt(Math.Pow(scrX2 - scrX, 2) + Math.Pow(scrY2 - scrY, 2));
                                if (dist < 20)
                                {
                                    cntr._points[j] = targCntr._points[k];
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        public void SetContoursVisibleByFilter()
        {
            if (MapProject != null)
            {
                if ((mainForm.AppSettings.AttachContoursToFilter) && (!mainForm.filterOilObj.AllSelected))
                {
                    for (int i = 0; i < LayerList.Count; i++)
                    {
                        List<C2DLayer> list = new List<C2DLayer>();
                        C2DLayer layer;
                        Contour cntr;
                        bool find = false;
                        ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.CONTOUR, list);
                        for (int j = 0; j < list.Count; j++)
                        {
                            layer = (C2DLayer)list[j];
                            find = false;
                            for (int k = 0; k < layer.ObjectsList.Count; k++)
                            {
                                cntr = (Contour)layer.ObjectsList[k];
                                if (mainForm.filterOilObj.SelectedObjects.IndexOf(cntr.StratumCode) != -1)
                                {
                                    find = true;
                                    break;
                                }
                            }
                            layer.Visible = find;
                            if (layer.node != null) layer.node.Checked = layer.Visible;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < LayerList.Count; i++)
                    {
                        List<C2DLayer> list = new List<C2DLayer>();
                        C2DLayer layer;
                        ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.CONTOUR, list);
                        for (int j = 0; j < list.Count; j++)
                        {
                            layer = (C2DLayer)list[j];
                            layer.Visible = true;
                        }
                    }
                }
            }
        }
        public void SetAreasVisibleByFilter()
        {
            if (MapProject == null) return;

            if (mainForm.AppSettings.AttachAreasToFilter)
            {
                List<int> oilfieldIndexes = new List<int>();

                for (int i = 0; i < LayerList.Count; i++)
                {
                    List<C2DLayer> list = new List<C2DLayer>();
                    C2DLayer layer;
                    bool find = false;
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.AREA, list);
                    for (int j = 0; j < list.Count; j++)
                    {
                        layer = (C2DLayer)list[j];
                        find = false;
                        if (mainForm.AppSettings.ShowAllOilfieldAreas || oilfieldIndexes.IndexOf(layer.oilFieldIndex) == -1)
                        {
                            if (layer.ObjectsList.Count > 0 && mainForm.filterOilObj.SelectedObjects.IndexOf(((Area)layer.ObjectsList[0]).PlastCode) != -1)
                            {
                                if (!mainForm.AppSettings.ShowAllOilfieldAreas) oilfieldIndexes.Add(layer.oilFieldIndex);
                                find = true;
                            }
                        }
                        layer.Visible = find;
                        if (layer.node != null) layer.node.Checked = layer.Visible;
                    }
                }
            }
            else if (mainForm.fAppSettings.AttachAreasToFilterChanged)
            {
                List<int> oilfieldIndexes = new List<int>();
                List<bool> findVisibleArea = new List<bool>();
                List<C2DLayer> list = new List<C2DLayer>();
                C2DLayer layer;
                int ind;
                for (int i = 0; i < LayerList.Count; i++)
                {
                    list.Clear();
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.AREA, list);
                    for (int j = 0; j < list.Count; j++)
                    {
                        layer = (C2DLayer)list[j];
                        ind = oilfieldIndexes.IndexOf(layer.oilFieldIndex);
                        if (ind == -1)
                        {
                            ind = oilfieldIndexes.Count;
                            oilfieldIndexes.Add(layer.oilFieldIndex);
                            findVisibleArea.Add(false);
                        }
                        if (layer.Visible) findVisibleArea[ind] = true;
                    }
                }
                for (int i = 0; i < LayerList.Count; i++)
                {
                    list.Clear();
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.AREA, list);
                    for (int j = 0; j < list.Count; j++)
                    {
                        layer = (C2DLayer)list[j];
                        ind = oilfieldIndexes.IndexOf(layer.oilFieldIndex);
                        if (ind != -1 && !findVisibleArea[ind])
                        {
                            layer.Visible = true;
                            if (layer.node != null) layer.node.Checked = layer.Visible;
                            findVisibleArea[ind] = true;
                        }
                    }
                }
                this.DrawLayerList();
            }
        }
        C2DLayer ImportContourInKts(string FileName, int OilfieldIndex, OilfieldCoefItem coef)
        {
            C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
            List<C2DLayer> layers = new List<C2DLayer>();
            if (FileName.Length > 0)
            {
                StreamReader file = new StreamReader(FileName, Encoding.GetEncoding(1251));

                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                Contour cntr;
                double xLoc, yLoc;
                double xG, yG;
                string line;
                int pos;
                C2DLayer lr = null;
                char[] sep = new char[] { ';' };
                string[] parseStr;
                line = file.ReadLine();
                while (!file.EndOfStream)
                {
                    if (line.Length > 0 && line[0] == '*')
                    {
                        lr = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, OilfieldIndex);
                        cntr = new Contour();
                        parseStr = line.Split(sep);
                        if (parseStr.Length > 0) cntr.Name = parseStr[0].Replace("*", "");

                        cntr._ContourType = 0;
                        cntr._Closed = false;
                        cntr._OilFieldCode = coef.Code;
                        cntr._points = new PointsXY(3);
                        line = file.ReadLine();
                        while (!file.EndOfStream && line.Length == 0) line = file.ReadLine();

                        while (!file.EndOfStream && line.Length > 0 && line[0] != '*')
                        {
                            pos = line.IndexOf(" ");
                            if (pos > -1)
                            {
                                xLoc = Convert.ToDouble(line.Remove(pos), nfi);
                                yLoc = Convert.ToDouble(line.Remove(0, pos), nfi);
                                coef.GlobalCoordFromLocal(xLoc, yLoc, out xG, out yG);
                                cntr._points.Add(xG, yG);
                            }
                            line = file.ReadLine();
                        }
                        cntr.VisibleName = false;
                        cntr._Closed = true;
                        if (cntr._points.Count > 0 &&
                           cntr._points[cntr._points.Count - 1].X == cntr._points[0].X &&
                           cntr._points[cntr._points.Count - 1].Y == cntr._points[0].Y)
                        {
                            cntr._points.RemoveAt(cntr._points.Count - 1);
                        }
                        cntr.SetMinMax();
                        lr.Name = cntr.Name;
                        lr.Add(cntr);
                        lr.GetObjectsRect();
                        layers.Add(lr); 
                    }
                    else
                    {
                        line = file.ReadLine();
                    }
                }
                file.Close();
                for (int i = 0; i < layers.Count; i++)
                {
                    layer.Add(layers[i]);
                }
            }
            return layer;
        }
        public void LoadContoursFromFile()
        {
            if (MapProject != null)
            {
                if (twActiveOilField.ActiveOilFieldIndex != -1)
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    string ofName = MapProject.OilFields[twActiveOilField.ActiveOilFieldIndex].Name;
                    dlg.Title = "Выберите файл c контурами месторождения '" + ofName + "'";
                    dlg.Filter = "KTS файлы (*.kts)|*.kts";
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        MapProject.tempStr = dlg.FileName;
                        C2DLayer layer = ImportContourInKts(dlg.FileName, twActiveOilField.ActiveOilFieldIndex, MapProject.OilFields[twActiveOilField.ActiveOilFieldIndex].CoefDict);
                        twWork.BeginUpdate();
                        if (layer.ObjectsList.Count > 0)
                        {
                            for (int i = 0; i < layer.ObjectsList.Count; i++)
                            {
                                //((C2DLayer)layer.ObjectsList[i]).Index = LayerWorkList.Count;
                                //LayerWorkList.Add(layer.ObjectsList[i]);
                                copyLayer = (C2DLayer)layer.ObjectsList[i];
                                PasteWorkLayer(true, false);
                                WriteWorkLayer((C2DLayer)LayerWorkList[LayerWorkList.Count - 1]);
                            }

                        }
                        twWork.EndUpdate();
                    }
                }
                else
                {
                    MessageBox.Show("Сделайте активным месторождение, к которому будут привязаны контуры.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        public void LoadOilfieldContourColor(string FileName)
        {
            if (File.Exists(FileName))
            {
                MSOffice.Excel excel = new SmartPlus.MSOffice.Excel();

                try
                {
                    excel.OpenWorkbook(FileName);
                    int sheetCount = excel.GetWorkSheetsCount();
                    List<Contour> cntrs = new List<Contour>();
                    OilField of;
                    Contour contour;
                    double firstOil, firstLiq;
                    double value, maxOil, maxLiq;
                    double maxOilPercent, maxLiqPercent;
                    double ReservesFactor = 0;
                    string[] ofAreas = new string[] { "Арланская площадь", "Николо-Березовская площадь", "Ново-Хазинская площадь" };
                    List<string> ofAreasList = new List<string>(ofAreas);
                    bool find = false;
                    C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, -1), lr;
                    int row = 2;
                    double[] oils = new double[5];
                    double[] oilPercents = new double[5];
                    excel.SetActiveSheet(0);
                    string cell = excel.GetValue(row, 1).ToString();
                    while (cell.Length > 0)
                    {
                        bool IsPTD = FileName.IndexOf("_ПТД") > -1;
                        excel.SetActiveSheet(0);
                        cell = excel.GetValue(row, 1).ToString();
                        int index = MapProject.GetOFIndex(cell);
                        if (index > -1)
                        {
                            of = MapProject.OilFields[index];
                            find = false;

                            firstOil = Convert.ToDouble(excel.GetValue(row, 2));
                            for (int i = 0; i < 5; i++)
                            {
                                string str = excel.GetValue(row, i + (IsPTD ? 22 : 3)).ToString();
                                oils[i] = Convert.ToDouble(excel.GetValue(row, i + (IsPTD ? 22 : 3)));
                                //if (maxOil < value - firstOil) maxOil = value - firstOil;
                                oilPercents[i] = Convert.ToDouble(excel.GetValue(row, i + (IsPTD ? 27 : 8)));
                                //if (maxOilPercent < value) maxOilPercent = value;
                            }

                            if (!IsPTD) ReservesFactor = Convert.ToDouble(excel.GetValue(row, 14));

                            excel.SetActiveSheet(1);
                            maxOil = -1; maxOilPercent = -1;
                            maxLiq = -1; maxLiqPercent = -1;
                            firstLiq = Convert.ToDouble(excel.GetValue(row, 2));
                            for (int i = 0; i < 5; i++)
                            {
                                value = Convert.ToDouble(excel.GetValue(row, i + (IsPTD ? 22 : 3)));
                                if (maxLiq < value - firstLiq)
                                {
                                    maxLiq = value - firstLiq;
                                    maxOil = oils[i] = firstOil;
                                    maxOilPercent = oilPercents[i];
                                }
                                value = Convert.ToDouble(excel.GetValue(row, i + (IsPTD ? 27 : 8)));
                                if (maxLiqPercent < value) maxLiqPercent = value;
                                if (value > 10) find = true;
                            }
                            if (find)
                            {
                                int ind = -1;
                                for (int i = 0; i < of.Contours.Count; i++)
                                {
                                    if (of.Contours[i]._ContourType == -3)
                                    {
                                        ind = i;
                                        break;
                                    }
                                }
                                if (ind != -1)
                                {
                                    contour = new Contour(of.Contours[ind]);
                                    contour.Name = string.Empty;
                                    contour.LineColor = Color.Red;
                                    contour.LineWidth = 3;
                                    contour.VisibleName = true;
                                    contour._ContourType = 5000;
                                    contour.Text = string.Format("dQж {0:0.#} % ({1:0.#} т/сут)\ndQн {2:0.#} % ({3:0.#} т/сут)", maxLiqPercent, maxLiq * 1000 / 365, maxOilPercent, maxOil * 1000 / 365);
                                    if (!IsPTD) contour.Text += string.Format("\nВыработка: {0:0.##} %", ReservesFactor);

                                    contour.Enable = false;
                                    layer.Add(contour);
                                }
                            }
                        }
                        else if (ofAreasList.IndexOf(cell) > -1)
                        {
                            find = false;
                            maxOil = -1; maxOilPercent = -1;
                            firstOil = Convert.ToDouble(excel.GetValue(row, 2));
                            for (int i = 0; i < 5; i++)
                            {
                                value = Convert.ToDouble(excel.GetValue(row, i + (IsPTD ? 22 : 3)));
                                if (!IsPTD && maxOil < value - firstOil) maxOil = value - firstOil;
                                value = Convert.ToDouble(excel.GetValue(row, i + (IsPTD ? 27 : 8)));
                                if (maxOilPercent < value) maxOilPercent = value;
                            }
                            excel.SetActiveSheet(1);
                            maxLiq = -1; maxLiqPercent = -1;
                            firstLiq = Convert.ToDouble(excel.GetValue(row, 2));
                            for (int i = 0; i < 5; i++)
                            {
                                value = Convert.ToDouble(excel.GetValue(row, i + (IsPTD ? 22 : 3)));
                                if (!IsPTD && maxLiq < value - firstLiq) maxLiq = value - firstLiq;
                                value = Convert.ToDouble(excel.GetValue(row, i + (IsPTD ? 27 : 8)));
                                if (maxLiqPercent < value) maxLiqPercent = value;
                                if (value > 10) find = true;
                            }
                            if (find)
                            {
                                int ind = -1;
                                for (int i = 0; i < LayerWorkList.Count; i++)
                                {
                                    lr = (C2DLayer)LayerWorkList[i];
                                    if (lr.Name.Equals(cell, StringComparison.OrdinalIgnoreCase))
                                    {
                                        ind = i;
                                        break;
                                    }
                                }

                                if (ind != -1)
                                {
                                    contour = new Contour((Contour)((C2DLayer)LayerWorkList[ind]).ObjectsList[0]);
                                    contour.Name = cell;
                                    contour.LineColor = Color.Red;
                                    contour.LineWidth = 3;
                                    contour.VisibleName = true;
                                    contour._ContourType = 5000;
                                    contour.Text = string.Format("dQж {0:0.#} % ({1:0.#} т/сут)\ndQн {2:0.#} % ({3:0.#} т/сут)", maxLiqPercent, maxLiq * 1000 / 365, maxOilPercent, maxOil * 1000 / 365);
                                    if (!IsPTD) contour.Text += string.Format("\nВыработка: {0:0.##} %", ReservesFactor);
                                    contour.Enable = false;
                                    layer.Add(contour);
                                }
                            }
                        }

                        row++;
                    }
                    if (layer.ObjectsList.Count > 0)
                    {
                        layer.GetObjectsRect();
                        layer.Name = excel.GetActiveSheetName();
                        copyLayer = layer;
                        PasteWorkLayer(true);
                    }
                }
                finally
                {
                    excel.Quit();
                    MessageBox.Show("Загрузка завершена!");
                }
            }
        }
        public void FillWellsOilGain(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (MapProject != null)
            {
                int i, j, k;
                int row, index, gtmIndex;
                List<string> gtmTypes = new List<string>(new string[] { "ВНС", "ГРП", "ЗБС", "ИДН", "ПВ(Н)ЛГ" });
                // gtm types = 0 - ВНС, 1 - ГРП, 2 -ЗБС, 3 - ИДН, 4 - ПВЛГ  
                List<object[]> oilGains = new List<object[]>(); // 0 - well; 1 - gtm type; 2 - oil gain; 3 - liq gain
                MSOffice.Excel excel = new SmartPlus.MSOffice.Excel();
                try
                {

                    List<C2DLayer> layers = new List<C2DLayer>();
                    for (i = 0; i < LayerWorkList.Count; i++)
                    {
                        ((C2DLayer)LayerWorkList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.DATAFILE, layers);
                    }
                    string fileName = string.Empty;
                    for (i = 0; i < layers.Count; i++)
                    {
                        if (layers[i].Name == "ГТМ с приростами.xlsx")
                        {
                            if (layers[i].node != null && layers[i].node.TreeView != null)
                            {
                                fileName = mainForm.UserSmartPlusFolder + "\\" + layers[i].node.FullPath;
                            }
                        }
                    }
                    if (fileName.Length == 0)
                    {
                        throw new Exception("Файл приростов не найден в рабочей папке!");
                    }
                    excel.OpenWorkbook(fileName);
                    int sheetCount = excel.GetWorkSheetsCount();

                    string str;
                    OilField of;
                    int areaIndex;
                    Well w;
                    var areaDict = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                    for (i = 0; i < sheetCount; i++)
                    {
                        excel.SetActiveSheet(i);
                        gtmIndex = gtmTypes.IndexOf(excel.GetActiveSheetName().ToString().Trim());
                        if (gtmIndex > -1)
                        {
                            row = 1;
                            while (excel.GetValue(row, 0).ToString().Length > 0)
                            {
                                str = excel.GetValue(row, 1).ToString().Trim();
                                index = MapProject.GetOFIndex(str);
                                if (index > -1)
                                {
                                    of = MapProject.OilFields[index];
                                    areaIndex = areaDict.GetIndexByShortName(excel.GetValue(row, 2).ToString().Trim());
                                    if (areaIndex > -1)
                                    {
                                        str = excel.GetValue(row, 3).ToString().Trim();
                                        w = null;
                                        if (gtmIndex != 0)
                                        {
                                            index = of.GetWellIndex(str, areaDict[areaIndex].Code);
                                            if (index == -1) index = of.GetWellIndex(str);
                                            if (index > -1) w = of.Wells[index];
                                        }
                                        else
                                        {
                                            index = of.GetProjWellIndex(str);
                                            if (index > -1) w = of.ProjWells[index];
                                        }
                                        if (w != null)
                                        {
                                            index = oilGains.Count;
                                            oilGains.Add(new object[4]);
                                            oilGains[index][0] = w;
                                            oilGains[index][1] = gtmIndex;
                                            oilGains[index][2] = (double)Convert.ToDouble(excel.GetValue(row, 7));
                                            oilGains[index][3] = (double)Convert.ToDouble(excel.GetValue(row, 8));
                                        }
                                    }
                                    else
                                    {
                                        //System.Diagnostics.Debug.WriteLine("Не найдена площадь '" + str + "'");
                                    }
                                }
                                else
                                {
                                    //System.Diagnostics.Debug.WriteLine("Не найдено месторождение '" + str + "'");
                                }
                                row++;
                            }
                        }
                    }
                }
                finally
                {
                    excel.Quit();
                    excel = null;
                }
                if (oilGains.Count > 0)
                {
                    worker.ReportProgress(0);
                    int x;
                    Contour cntr;
                    List<Well>[] gtmWells = new List<Well>[gtmTypes.Count];
                    C2DLayer layer = null, layer2 = null, layerCntrs = null, lr;
                    for (i = 0; i < LayerWorkList.Count; i++)
                    {
                        if (((C2DLayer)LayerWorkList[i]).Name == "Потенциальные ГТМ 2014-2018 гг")
                        {
                            layer = (C2DLayer)LayerWorkList[i];
                        }
                        if (((C2DLayer)LayerWorkList[i]).Name == "ГТМ 2013 г")
                        {
                            layer2 = (C2DLayer)LayerWorkList[i];
                        }
                        if (((C2DLayer)LayerWorkList[i]).Name == "Участки ГТМ")
                        {
                            layerCntrs = (C2DLayer)LayerWorkList[i];
                        }
                    }
                    if (layer != null && layer2 != null && layerCntrs != null)
                    {
                        int countContours = 0;
                        for (i = 0; i < layer.ObjectsList.Count; i++)
                        {
                            lr = (C2DLayer)layer.ObjectsList[i];
                            gtmIndex = gtmTypes.IndexOf(lr.Name);
                            if (gtmIndex >= 0)
                            {
                                if (gtmWells[gtmIndex] == null) gtmWells[gtmIndex] = new List<Well>();
                                for (j = 0; j < lr.ObjectsList.Count; j++)
                                {
                                    for (k = 0; k < ((C2DLayer)lr.ObjectsList[j]).ObjectsList.Count; k++)
                                    {
                                        gtmWells[gtmIndex].Add(((PhantomWell)((C2DLayer)lr.ObjectsList[j]).ObjectsList[k]).srcWell);
                                    }
                                }
                            }
                        }
                        for (i = 0; i < layer2.ObjectsList.Count; i++)
                        {
                            lr = (C2DLayer)layer2.ObjectsList[i];
                            gtmIndex = gtmTypes.IndexOf(lr.Name);
                            if (gtmIndex >= 0)
                            {
                                if (gtmWells[gtmIndex] == null) gtmWells[gtmIndex] = new List<Well>();
                                for (j = 0; j < lr.ObjectsList.Count; j++)
                                {
                                    for (k = 0; k < ((C2DLayer)lr.ObjectsList[j]).ObjectsList.Count; k++)
                                    {
                                        gtmWells[gtmIndex].Add(((PhantomWell)((C2DLayer)lr.ObjectsList[j]).ObjectsList[k]).srcWell);
                                    }
                                }
                            }
                        }
                        int count, progress = 0;
                        double sumOil, sumLiq;
                        for (i = 0; i < layerCntrs.ObjectsList.Count; i++)
                        {
                            countContours += ((C2DLayer)layerCntrs.ObjectsList[i]).ObjectsList.Count;
                        }
                        for (i = 0; i < layerCntrs.ObjectsList.Count; i++)
                        {
                            for (j = 0; j < ((C2DLayer)layerCntrs.ObjectsList[i]).ObjectsList.Count; j++)
                            {
                                layer = (C2DLayer)((C2DLayer)layerCntrs.ObjectsList[i]).ObjectsList[j];
                                if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                                {
                                    cntr = (Contour)layer.ObjectsList[0];
                                    sumOil = 0;
                                    sumLiq = 0;
                                    count = 0;
                                    bool find = false;
                                    for (x = 0; x < gtmWells.Length; x++)
                                    {
                                        if (gtmWells[x] == null) continue;

                                        for (k = 0; k < gtmWells[x].Count; k++)
                                        {
                                            if (cntr.PointBodyEntry(gtmWells[x][k].X, gtmWells[x][k].Y))
                                            {
                                                find = false;
                                                for (index = 0; index < oilGains.Count; index++)
                                                {
                                                    if ((Well)oilGains[index][0] == gtmWells[x][k] && x == (int)oilGains[index][1])
                                                    {
                                                        if (!find) count++;
                                                        sumOil += (double)oilGains[index][2];
                                                        sumLiq += (double)oilGains[index][3];
                                                        find = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    cntr.Text = string.Format("dQн: {0:0.##} т/сут\ndQж: {1:0.##} т/сут\n{2} скважин", sumOil, sumLiq, count);
                                    cntr._ContourType = 5001;
                                    cntr.LineWidth = 1;
                                    cntr.LineColor = Color.Red;
                                    cntr.Enable = false;
                                    cntr.VisibleName = true;
                                    layer.Name = cntr.Name;
                                    progress++;
                                    worker.ReportProgress((int)(progress * 100f / countContours));
                                }
                                this.WriteWorkLayer(layer);
                            }
                        }
                    }
                }
            }
        }

        // DEPOSITs
        public void DepositsGisReport(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (MapProject != null)
            {
                var stratumDict = (StratumDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                var litDict = (DataDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.LITOLOGY);
                var satDict = (DataDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
                OilField of;
                Well w;
                Deposit dep;
                int i, j, ind;
                double xG, yG;
                bool inContour;
                MSOffice.Excel excel = null;
                List<string> errors = new List<string>();
                List<string> outList = new List<string>();
                List<string> gisList = new List<string>();
                for (int ofInd = 0; ofInd < MapProject.OilFields.Count; ofInd++)
                {
                    of = MapProject.OilFields[ofInd];
                    if (of.Deposits.Count > 0)
                    {
                        of.LoadGisFromCache(worker, e);

                        for (int dInd = 0; dInd < of.Deposits.Count; dInd++)
                        {
                            if (excel == null)
                            {
                                excel = new MSOffice.Excel();
                                excel.EnableEvents = false;
                                excel.Visible = false;
                                excel.NewWorkbook();
                            }
                            dep = of.Deposits[dInd];

                            for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                            {
                                w = of.Wells[wInd];
                                if (w.Missing) continue;

                                ind = -1;
                                if (w.coord != null)
                                {
                                    for (i = 0; i < w.coord.Count; i++)
                                    {
                                        if (w.coord.Items[i].PlastId == dep.StratumCode)
                                        {
                                            ind = i;
                                            break;
                                        }
                                    }
                                    if (ind == -1)
                                    {
                                        for (i = 0; i < w.coord.Count; i++)
                                        {
                                            if (w.coord.Items[i].PlastId == dep.StratumParentCode)
                                            {
                                                ind = i;
                                                break;
                                            }
                                        }
                                    }
                                    if (ind != -1)
                                    {
                                        of.CoefDict.GlobalCoordFromLocal(w.coord.Items[ind].X, w.coord.Items[ind].Y, out xG, out yG);
                                    }
                                    else
                                    {
                                        errors.Add(string.Format("{0};{1};{2};{3};Не найдено пластопересечение", of.Name, dep.Name, stratumDict.GetShortNameByCode(dep.StratumCode), w.Name));
                                        xG = w.X;
                                        yG = w.Y;
                                    }
                                }
                                else
                                {
                                    string str = string.Format("{0};{1};{2};{3};Не загружены координаты", of.Name, "", "", w.Name);
                                    if (errors.IndexOf(str) == -1) errors.Add(str);
                                    xG = w.X;
                                    yG = w.Y;
                                }
                                inContour = dep.contour.PointBodyEntry(xG, yG);
                                if (!inContour)
                                {
                                    for (i = 0; i < of.Deposits.Count; i++)
                                    {
                                        inContour = of.Deposits[i].contour.PointBodyEntry(xG, yG);
                                        if (inContour) break;
                                    }
                                }
                                gisList.Clear();
                                if (w.gis != null)
                                {
                                    for (i = 0; i < w.gis.Count; i++)
                                    {
                                        if (stratumDict.EqualsStratum(dep.StratumCode, w.gis.Items[i].PlastId) || stratumDict.EqualsStratum(dep.StratumCode, w.gis.Items[i].PlastZoneId))
                                        {
                                            if (w.gis.Items[i].Collector && w.gis.Items[i].Sat0 != 2)
                                            {
                                                gisList.Add(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};", of.Name, stratumDict.GetShortNameByCode(dep.StratumCode), dep.Name, w.Name, stratumDict.GetShortNameByCode(w.gis.Items[i].PlastId), w.gis.Items[i].H, w.gis.Items[i].H + w.gis.Items[i].L, w.gis.Items[i].L, w.gis.Items[i].Collector ? "Да" : "Нет", satDict.GetShortNameByCode(w.gis.Items[i].Sat0), litDict.GetShortNameByCode(w.gis.Items[i].LitId)));
                                            }
                                        }
                                    }
                                    if (gisList.Count > 0 && !inContour)
                                    {
                                        errors.Add(string.Format("{0};{1};{2};{3};Скважина с пропластком ГИС не входит в контур", of.Name, dep.Name, stratumDict.GetShortNameByCode(dep.StratumCode), w.Name));
                                    }
                                    outList.AddRange(gisList.ToArray());
                                }
                                else
                                {
                                    string str = string.Format("{0};{1};{2};{3};Не загружены ГИС по скважине", of.Name, "", "", w.Name);
                                    if (errors.IndexOf(str) == -1) errors.Add(str);
                                }
                            }
                        }
                        of.ClearGisData(false);
                    }
                }
                if (excel != null)
                {
                    for (i = 0; i < outList.Count; i++)
                    {
                        excel.SetValue(i, 0, outList[i]);
                    }
                    excel.SetActiveSheet(1);
                    for (i = 0; i < errors.Count; i++)
                    {
                        excel.SetValue(i, 0, errors[i]);
                    }
                    excel.EnableEvents = true;
                    excel.Visible = true;
                }
            }
        }

        // PROFILE
        public void CreateProfileByNearWell(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if ((this.LayerList.Count > 0) && (newContour != null) && (newContour._points.Count > 1))
            {

                int i, j, k, indWell, indLayer;
                double d, minD;
                double minX, maxX, minY, maxY;
                List<C2DLayer> list = new List<C2DLayer>();
                ArrayList wellList = new ArrayList();
                ArrayList distanceList = new ArrayList();

                C2DLayer layer;
                Well w;

                PointD pt0, pt1;
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Загрузка скважинных данных в корсхему";
                userState.WorkCurrentTitle = "Составление списка скважин по контуру";
                ClearSelWellList();

                for (i = 0; i < LayerList.Count; i++)
                {
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list, true);
                }
                RectangleF rectCntr = new RectangleF(), rectCntrLarge = new RectangleF();
                newContour.SetMinMax();
                rectCntr.X = (float)newContour.Xmin;
                rectCntr.Y = (float)newContour.Ymin;
                rectCntr.Width = (float)(newContour.Xmax - newContour.Xmin);
                rectCntr.Height = (float)(newContour.Ymax - newContour.Ymin);
                rectCntrLarge.X = (float)newContour.Xmin - 100;
                rectCntrLarge.Y = (float)newContour.Ymin - 100;
                rectCntrLarge.Width = (float)(newContour.Xmax - newContour.Xmin) + 200;
                rectCntrLarge.Height = (float)(newContour.Ymax - newContour.Ymin) + 200;

                if (list.Count > 0)
                {
                    for (k = 0; k < newContour._points.Count; k++)
                    {
                        pt0 = newContour._points[k];
                        indLayer = -1;
                        indWell = -1;
                        minD = double.MaxValue;
                        for (i = 0; i < list.Count; i++)
                        {
                            layer = (C2DLayer)list[i];
                            if (rectCntrLarge.IntersectsWith(RectangleF.FromLTRB((float)layer.ObjRect.Left,
                                                                            (float)layer.ObjRect.Top,
                                                                            (float)layer.ObjRect.Right,
                                                                            (float)layer.ObjRect.Bottom)))
                            {
                                for (j = 0; j < layer.ObjectsList.Count; j++)
                                {
                                    w = (Well)layer.ObjectsList[j];

                                    pt1.X = w.X;
                                    pt1.Y = w.Y;
                                    if (rectCntrLarge.Contains((float)pt1.X, (float)pt1.Y))
                                    {
                                        d = Math.Sqrt(Math.Pow(pt0.X - pt1.X, 2) + Math.Pow(pt0.Y - pt1.Y, 2));
                                        if (d < minD)
                                        {
                                            indLayer = i;
                                            indWell = j;
                                            minD = d;
                                        }
                                    }
                                }
                            }
                        }
                        if (indLayer > -1)
                        {
                            bool find = false;
                            w = (Well)((C2DLayer)list[indLayer]).ObjectsList[indWell];
                            for (j = 0; j < selWellList.Count; j++)
                            {
                                if ((Well)selWellList[j] == w)
                                {
                                    find = true;
                                    break;
                                }
                            }
                            if (!find) selWellList.Add(w);
                        }
                        worker.ReportProgress((int)((float)k * 100 / (float)newContour._points.Count));
                    }
                    newContour = null;
                    newContourEnd = false;
                }
            }
        }
        public void CreateProfileByNewContour(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if ((this.LayerList.Count > 0) && (newContour != null) && (newContour._points.Count > 1))
            {
                int i, j, k;
                double d, dist, sumDist;
                double minX, maxX, minY, maxY;
                List<C2DLayer> list = new List<C2DLayer>();
                ArrayList wellList = new ArrayList();
                ArrayList distanceList = new ArrayList();

                C2DLayer layer;
                Well w;
                PointD pt1, pt2, pt0, pt3;
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Загрузка скважинных данных в корсхему";
                userState.WorkCurrentTitle = "Составление списка скважин по контуру";
                ClearSelWellList();

                for (i = 0; i < LayerList.Count; i++)
                {
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list, true);
                }
                RectangleF rectCntr = new RectangleF(), rectCntrLarge = new RectangleF();
                newContour.SetMinMax();
                rectCntr.X = (float)newContour.Xmin;
                rectCntr.Y = (float)newContour.Ymin;
                rectCntr.Width = (float)(newContour.Xmax - newContour.Xmin);
                rectCntr.Height = (float)(newContour.Ymax - newContour.Ymin);
                rectCntrLarge.X = (float)newContour.Xmin - 100;
                rectCntrLarge.Y = (float)newContour.Ymin - 100;
                rectCntrLarge.Width = (float)(newContour.Xmax - newContour.Xmin) + 200;
                rectCntrLarge.Height = (float)(newContour.Ymax - newContour.Ymin) + 200;

                if (list.Count > 0)
                {
                    for (i = 0; i < list.Count; i++)
                    {
                        layer = (C2DLayer)list[i];
                        if (rectCntrLarge.IntersectsWith(RectangleF.FromLTRB((float)layer.ObjRect.Left,
                                                                        (float)layer.ObjRect.Top,
                                                                        (float)layer.ObjRect.Right,
                                                                        (float)layer.ObjRect.Bottom)))
                        {
                            for (j = 0; j < layer.ObjectsList.Count; j++)
                            {
                                w = (Well)layer.ObjectsList[j];

                                pt0.X = w.X;
                                pt0.Y = w.Y;
                                if (rectCntrLarge.Contains((float)pt0.X, (float)pt0.Y))
                                {
                                    sumDist = 0;
                                    for (k = 0; k < newContour._points.Count - 1; k++)
                                    {
                                        pt1 = newContour._points[k];
                                        pt2 = newContour._points[k + 1];

                                        minX = pt1.X; maxX = pt2.X;
                                        if (minX > maxX) { minX = pt2.X; maxX = pt1.X; }
                                        minY = pt1.Y; maxY = pt2.Y;
                                        if (minY > maxY) { minY = pt2.Y; maxY = pt1.Y; }
                                        pt3 = Geometry.GetPointInLineByPoint(pt1, pt2, pt0);

                                        if ((minX <= pt3.X) && (pt3.X <= maxX) && (minY <= pt3.Y) && (pt3.Y <= maxY))
                                        {
                                            d = Math.Sqrt(Math.Pow(pt0.X - pt3.X, 2) + Math.Pow(pt0.Y - pt3.Y, 2));
                                            dist = sumDist + Math.Sqrt(Math.Pow(pt1.X - pt3.X, 2) + Math.Pow(pt1.Y - pt3.Y, 2));

                                            if (d < 150)
                                            {
                                                wellList.Add(w);
                                                distanceList.Add(dist);
                                                break;
                                            }
                                        }
                                        sumDist += Math.Sqrt(Math.Pow(pt2.X - pt1.X, 2) + Math.Pow(pt2.Y - pt1.Y, 2));
                                    }
                                }
                            }
                        }
                        worker.ReportProgress((int)((float)i * 100 / (float)list.Count), userState);
                    }
                }
                if (wellList.Count > 0)
                {
                    Well[] WellArray = new Well[wellList.Count];
                    double[] distList = new double[wellList.Count];
                    k = -1;
                    double minDist = -1, Min = -1;
                    for (i = 0; i < wellList.Count; i++)
                    {
                        k = -1;
                        minDist = -1;
                        for (j = 0; j < wellList.Count; j++)
                        {
                            if (((double)distanceList[j] + j > Min) && ((minDist == -1) || (minDist > (double)distanceList[j] + j)))
                            {
                                k = j;
                                minDist = (double)distanceList[j] + j;
                            }
                        }
                        Min = minDist;
                        WellArray[i] = (Well)wellList[k];
                        distList[i] = minDist;
                        selWellList.Add(WellArray[i]);

                    }
                    newContour = null;
                    newContourEnd = false;
                }
            }
        }
        public bool SetProfileOverWell(Well w)
        {
            bool draw = false;
            if (ProfileOverWell != w)
            {
                if (ProfileOverWell != null)
                {
                    ProfileOverWell.SelectedByProfile = false;
                    draw = true;
                }
                if (w != null)
                {
                    w.SelectedByProfile = true;
                    draw = true;
                }
                ProfileOverWell = w;
                this.Invalidate();
            }
            return draw;
        }

        #region SUM PARAMETERS
        private void AddSumRegion(Contour cntr)
        {
            if (cntr._EnterpriseCode > -1)
            {
                if (!ShiftPressed /* || (mainForm.dChartGetByOilObjMode())*/)
                {
                    if (mainForm.worker.IsBusy)
                    {
                        mainForm.worker.CancelAsync();
                    }
                    AppendSumParams = false;
                }
                else
                    AppendSumParams = true;

                mainForm.pb.Visible = true;
                mainForm.stBar1.Text = "Загрузка Суммарных показателей " + cntr.Name;
                mainForm.dChartHideChartAreas();
                IsGraphicsByOneDate = false;
                if (mainForm.worker.IsBusy)
                {
                    WorkObject job;
                    job.WorkType = Constant.BG_WORKER_WORK_TYPE.SUM_REG_PARAMS_CALC;
                    job.DataObject = MapProject;
                    job.ObjectIndex = cntr._EnterpriseCode;
                    mainForm.worker.Queue.Add(job);
                }
                else
                    mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SUM_REG_PARAMS_CALC, MapProject, cntr._EnterpriseCode);

            }
        }
        private void AddSumNGDU(Contour cntr)
        {
            if (cntr._NGDUCode > -1)
            {
                if (!ShiftPressed /* || (mainForm.dChartGetByOilObjMode())*/)
                {
                    if (mainForm.worker.IsBusy)
                    {
                        mainForm.worker.CancelAsync();
                    }
                    AppendSumParams = false;
                }
                else
                    AppendSumParams = true;

                IsGraphicsByOneDate = false;
                mainForm.pb.Visible = true;
                mainForm.stBar1.Text = "Загрузка Суммарных показателей " + cntr.Name;
                mainForm.dChartHideChartAreas();

                if (mainForm.worker.IsBusy)
                {
                    WorkObject job;
                    job.WorkType = Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_PARAMS_CALC;
                    job.DataObject = MapProject;
                    job.ObjectIndex = cntr._NGDUCode;
                    mainForm.worker.Queue.Add(job);
                }
                else
                    mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SUM_NGDU_PARAMS_CALC, MapProject, cntr._NGDUCode);
            }
        }
        private void AddSumOilField(Contour cntr)
        {
            int j;
            OilField of;
            IsGraphicsByOneDate = false;
            if (cntr._OilFieldCode > -1)
            {
                bool chartOilObjMode = mainForm.dChartGetByOilObjMode();
                for (j = 0; j < MapProject.OilFields.Count; j++)
                {
                    of = MapProject.OilFields[j];
                    if (cntr._OilFieldCode == of.OilFieldCode)
                    {
                        if (selLayerList.Count == 1 /*) || (chartOilObjMode)*/)
                        {
                            C2DLayer lr = (C2DLayer)selLayerList[0];
                            if (lr.oilfield == of /*) || (chartOilObjMode)*/)
                            {
                                if (mainForm.worker.IsBusy)
                                {
                                    mainForm.worker.CancelAsync();
                                }
                                AppendSumParams = false;
                            }
                            else
                                AppendSumParams = true;
                        }
                        else
                            AppendSumParams = true;

                        //if (!chartOilObjMode)
                        //{
                        mainForm.pb.Visible = true;
                        mainForm.stBar1.Text = "Загрузка Суммарных показателей " + cntr.Name;
                        mainForm.dChartHideChartAreas();
                        if (mainForm.worker.IsBusy)
                        {
                            WorkObject job;
                            job.WorkType = Constant.BG_WORKER_WORK_TYPE.SUM_OF_PARAMS_CALC;
                            job.DataObject = of;
                            job.ObjectIndex = -1;
                            mainForm.worker.Queue.Add(job);
                        }
                        else
                        {
                            if (of.sumParams == null)
                                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SUM_OF_PARAMS_CALC, of);
                            else
                            {
                                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SUM_OF_TABLE_RECALC, of);
                            }
                        }
                        //}
                        //else
                        //{
                        //    mainForm.pb.Visible = true;
                        //    mainForm.stBar1.Text = "Загрузка Суммарных показателей " + cntr.Name;
                        //    mainForm.dChartHideChartAreas();
                        //    if (mainForm.worker.IsBusy)
                        //    {
                        //        WorkObject job;
                        //        job.WorkType = Constant.BG_WORKER_WORK_TYPE.SUM_OILOBJ_PARAMS_CALC;
                        //        job.DataObject = of;
                        //        job.ObjectIndex = -1;
                        //        mainForm.worker.Queue.Add(job);
                        //    }
                        //    else
                        //    {
                        //        if (of.sumParams == null)
                        //        {
                        //            mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SUM_OILOBJ_PARAMS_CALC, MapProject);
                        //        }
                        //        else
                        //        {
                        //            mainForm.dChartSetSeriesByOilObjects();
                        //            mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SUM_OILOBJ_TABLE_RECALC, MapProject);
                        //        }
                        //    }
                        //}
                        break;
                    }
                }
            }
        }
        private void AddSumArea(Area area)
        {
            if (area != null)
            {
                IsGraphicsByOneDate = false;
                if (selLayerList.Count == 1)
                {
                    C2DLayer lr = (C2DLayer)selLayerList[0];
                    if ((lr.oilFieldIndex == -1) && (lr.Level == area.Index) && (lr.Name == area.Name) && (lr.oilfield.Index == area.OilFieldIndex))
                    {
                        if (mainForm.worker.IsBusy)
                        {
                            mainForm.worker.CancelAsync();
                        }
                        AppendSumParams = false;
                    }
                    else
                        AppendSumParams = true;
                }
                else
                    AppendSumParams = true;

                mainForm.pb.Visible = true;
                mainForm.stBar1.Text = "Загрузка Суммарных показателей " + area.Name;
                mainForm.dChartHideChartAreas();

                if (mainForm.worker.IsBusy)
                {
                    WorkObject job;
                    job.WorkType = Constant.BG_WORKER_WORK_TYPE.SUM_AREA_PARAMS_CALC;
                    job.DataObject = MapProject.OilFields[area.OilFieldIndex];
                    job.ObjectIndex = area.Index;
                    mainForm.worker.Queue.Add(job);
                }
                else
                    mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SUM_AREA_PARAMS_CALC, MapProject.OilFields[area.OilFieldIndex], area.Index);
            }
        }
        public void AddSumWellList(bool CancelCurrentJob)
        {
            if (selWellList.Count > 0)
            {
                AppendSumParams = false;
                ClearSumList();
                if (mainForm.worker.IsBusy && CancelCurrentJob)
                {
                    mainForm.worker.CancelAsync();
                }
                mainForm.pb.Visible = true;
                mainForm.stBar1.Text = "Загрузка Суммарных показателей по списку скважин";
                mainForm.dChartHideChartAreas();
                mainForm.filterOilObj.ClearCurrent();
                if (mainForm.worker.IsBusy)
                {
                    WorkObject job;
                    job.WorkType = Constant.BG_WORKER_WORK_TYPE.SUM_WELLLIST_PARAMS_CALC;
                    job.DataObject = this;
                    job.ObjectIndex = -1;
                    mainForm.worker.Queue.Add(job);
                }
                else
                    mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SUM_WELLLIST_PARAMS_CALC, this);
            }
        }
        public void ReCalcSumObjects() // при смене фильтра объектов
        {
            WorkObject job;
            mainForm.worker.QueueBusy = true;

            if (mainForm.worker.IsBusy) mainForm.worker.CancelAsync();
            AppendSumParams = false;


            if ((sumIndexes.Count > 0) || (selWellList.Count > 1) || (selWellList.Count == 0))
            {
                mainForm.dChartSumParamsShow();
            }
            else if (selWellList.Count == 1)
            {
                mainForm.dChartShowData(((Well)selWellList[0]).OilFieldIndex, ((Well)selWellList[0]).Index);
            }

            job.WorkType = Constant.BG_WORKER_WORK_TYPE.RESET_ICONS_BY_FILTER;
            job.DataObject = this;
            job.ObjectIndex = -1;
            mainForm.worker.Queue.Add(job);
            mainForm.worker.QueueBusy = false;

            if (!mainForm.worker.IsBusy) mainForm.worker.Run();
        }
        public bool SumParamsReCalc(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка суммарных показателей по МЭР";
            userState.Element = "Выбранный список скважин";
            bool ShowInCube = mainForm.AppSettings.ShowDataLiquidInCube;

            if ((this.selWellList.Count > 0) && (MapProject != null))
            {
                int i, j, k, c, iLen, ind;
                List<int> injGasId = new List<int>(new int[] { 81, 82, 83, 84, 85, 86, 87, 88, 89 });
                selMerOilObjCodes.Clear();
                SumCalcWelllist.Clear();
                for (i = 0; i < selWellList.Count; i++) SumCalcWelllist.Add(selWellList[i]);

                DateTime minDate, maxDate;
                Well w;
                OilField of;
                MerItem item;
                MerItemEx itemEx;
                minDate = DateTime.MaxValue;
                maxDate = DateTime.MinValue;
                ArrayList ofIndexes = new ArrayList(5);
                bool UseInjWellsPressure = mainForm.AppSettings.UseInjWellsPressure;
                var stratumDict = (StratumDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
 
                for (i = 0; i < SumCalcWelllist.Count; i++)
                {
                    w = (Well)SumCalcWelllist[i];
                    if ((w.OilFieldIndex != -1) && (ofIndexes.IndexOf((int)w.OilFieldIndex) == -1))
                    {
                        of = MapProject.OilFields[w.OilFieldIndex];
                        ofIndexes.Add(w.OilFieldIndex);
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            this.sumParams = null;
                            return false;
                        }
                        if (of.MerStratumCodes.Count == 0)
                        {
                            if (!of.LoadStratumCodesFromCache()) return false;
                        }
                        for (j = 0; j < of.MerStratumCodes.Count; j++)
                        {
                            if (selMerOilObjCodes.IndexOf(of.MerStratumCodes[j]) == -1)
                            {
                                selMerOilObjCodes.Add(of.MerStratumCodes[j]);
                            }
                        }
                    }
                }
                DateTime[] minMaxDate = new DateTime[selMerOilObjCodes.Count * 2];
                for (i = 0; i < selMerOilObjCodes.Count; i++)
                {
                    minMaxDate[2 * i] = minDate;
                    minMaxDate[2 * i + 1] = maxDate;
                }

                iLen = SumCalcWelllist.Count;

                for (i = 0; i < iLen; i++)
                {
                    w = (Well)SumCalcWelllist[i];
                    userState.Element = w.UpperCaseName;
                    if (w.OilFieldIndex == -1) continue;

                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        this.sumParams = null;
                        return false;
                    }
                    if ((!w.MerLoaded) && (!(MapProject.OilFields[w.OilFieldIndex]).MerLoaded))
                    {
                        of = MapProject.OilFields[w.OilFieldIndex];
                        of.LoadMerFromCache(w.Index);
                    }
                    if (!w.ResearchLoaded && !(MapProject.OilFields[w.OilFieldIndex]).WellResearchLoaded)
                    {
                        of = MapProject.OilFields[w.OilFieldIndex];
                        of.LoadWellResearchFromCache(w.Index);
                    }
                    if ((w.MerLoaded) && (w.mer.Count > 0))
                    {
                        for (j = 0; j < w.mer.Count; j++)
                        {
                            item = w.mer.Items[j];
                            for (k = 0; k < selMerOilObjCodes.Count; k++)
                            {
                                if (item.Date < minDate) minDate = item.Date;
                                if (item.Date > maxDate) maxDate = item.Date;
                                if (item.PlastId == (int)selMerOilObjCodes[k])
                                {
                                    if (item.Date < minMaxDate[2 * k]) minMaxDate[2 * k] = item.Date;
                                    if (item.Date > minMaxDate[2 * k + 1]) minMaxDate[2 * k + 1] = item.Date;
                                    break;
                                }
                            }
                        }
                    }
                    worker.ReportProgress(i, userState);
                }

                int len = selMerOilObjCodes.Count;
                j = 0;
                for (i = 0; i < len; i++)
                {
                    if ((minMaxDate[2 * i] == DateTime.MaxValue) && (minMaxDate[2 * i + 1] == DateTime.MinValue))
                    {
                        selMerOilObjCodes.RemoveAt(j);
                        j--;
                    }
                    j++;
                }
                for (i = 0; i < len; i++)
                {
                    if ((minMaxDate[2 * i] == DateTime.MaxValue) && (minMaxDate[2 * i + 1] == DateTime.MinValue))
                    {
                        j = i; k = -1;
                        while (j < len)
                        {
                            if ((minMaxDate[2 * j] != DateTime.MaxValue) && (minMaxDate[2 * j + 1] != DateTime.MinValue))
                            {
                                k = j;
                                break;
                            }
                            j++;
                        }
                        if (k != -1)
                        {
                            minMaxDate[2 * i] = minMaxDate[2 * k];
                            minMaxDate[2 * i + 1] = minMaxDate[2 * k + 1];
                            minMaxDate[2 * k] = DateTime.MaxValue;
                            minMaxDate[2 * k + 1] = DateTime.MinValue;
                        }
                    }
                }
                if ((minDate != DateTime.MaxValue) && (maxDate != DateTime.MinValue))
                {
                    int month, years;
                    bool bmonth, bProd, bInj, bScoop, bWorkTime, bAllMonth, bAllProd, bAllInj, bAllScoop;
                    DateTime dt;
                    PVTParamsItem pvt;
                    years = maxDate.Year - minDate.Year + 1;
                    month = (years - 2) * 12 + (13 - minDate.Month) + maxDate.Month;
                    if (month > 0)
                    {
                        sumParams = new SumParameters(selMerOilObjCodes.Count + 1);

                        SumParamItem[] SumMonth = new SumParamItem[month];
                        SumParamItem[] SumYear = new SumParamItem[years];

                        dt = minDate;
                        for (i = 0; i < month; i++)
                        {
                            SumMonth[i].Date = dt;
                            SumMonth[i].InjGas = -1;
                            SumMonth[i].NatGas = -1;
                            SumMonth[i].GasCondensate = -1;
                            dt = dt.AddMonths(1);
                        }
                        dt = new DateTime(minDate.Year, 01, 01);
                        for (i = 0; i < years; i++)
                        {
                            SumYear[i].Date = dt;
                            SumYear[i].InjGas = -1;
                            SumYear[i].NatGas = -1;
                            SumYear[i].GasCondensate = -1;
                            dt = dt.AddYears(1);
                        }

                        sumParams.Add(-1, SumMonth, SumYear);

                        for (k = 0; k < selMerOilObjCodes.Count; k++)
                        {
                            years = minMaxDate[2 * k + 1].Year - minMaxDate[2 * k].Year + 1;
                            month = (years - 2) * 12 + (13 - minMaxDate[2 * k].Month) + minMaxDate[2 * k + 1].Month;
                            SumMonth = new SumParamItem[month];
                            SumYear = new SumParamItem[years];

                            dt = minMaxDate[2 * k];
                            for (i = 0; i < month; i++)
                            {
                                SumMonth[i].Date = dt;
                                SumMonth[i].InjGas = -1;
                                SumMonth[i].NatGas = -1;
                                SumMonth[i].GasCondensate = -1;
                                dt = dt.AddMonths(1);
                            }
                            dt = new DateTime(minMaxDate[2 * k].Year, 01, 01);
                            for (i = 0; i < years; i++)
                            {
                                SumYear[i].Date = dt;
                                SumYear[i].InjGas = -1;
                                SumYear[i].NatGas = -1;
                                SumYear[i].GasCondensate = -1;
                                dt = dt.AddYears(1);
                            }
                            sumParams.Add((int)selMerOilObjCodes[k], SumMonth, SumYear);
                        }

                        int indMonth, indYears;
                        int[][] lastYearProd = new int[sumParams.Count][];
                        for (i = 0; i < lastYearProd.Length; i++)
                        {
                            lastYearProd[i] = new int[2];
                        }
                        int objIndex;
                        iLen = SumCalcWelllist.Count;
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                this.sumParams = null;
                                return false;
                            }
                            else
                            {
                                w = (Well)SumCalcWelllist[i];

                                pvt = PVTParamsItem.Empty;
                                if (w.AreaIndex != -1 && w.OilFieldIndex != -1 && MapProject.OilFields[w.OilFieldIndex].Areas.Count > 0)
                                {
                                    pvt = MapProject.OilFields[w.OilFieldIndex].Areas[w.AreaIndex].pvt;
                                }

                                #region Init
                                bProd = false;
                                bInj = false;
                                bScoop = false;
                                bmonth = false;
                                bAllMonth = false;
                                bWorkTime = false;
                                bAllProd = false;
                                bAllInj = false;
                                bAllScoop = false;
                                #endregion

                                #region LOAD FROM MERCOMP
                                MerComp merComp = null;
                                if (w.MerLoaded)
                                {
                                    int hours;
                                    MerCompItem compItem;
                                    MerCompPlastItem plastItem;
                                    merComp = new MerComp(w.mer);
                                    w.mer.SetItems(null, null);
                                    w.mer = null;
                                    MerWorkTimeItem wtItemProd, wtItemInj, wtItemScoop;
                                    for (j = 0; j < lastYearProd.Length; j++)
                                    {
                                        lastYearProd[j][0] = 0;
                                        lastYearProd[j][1] = 0;
                                    }
                                    for (j = 0; j < merComp.Count; j++)
                                    {
                                        compItem = merComp[j];

                                        #region заполняем по суммарному мэр
                                        SumMonth = sumParams[0].MonthlyItems;
                                        SumYear = sumParams[0].YearlyItems;
                                        indMonth = (compItem.Date.Year - minDate.Year) * 12 + compItem.Date.Month - minDate.Month;

                                        wtItemProd = compItem.TimeItems.GetAllProductionTime();
                                        wtItemInj = compItem.TimeItems.GetAllInjectionTime();
                                        wtItemScoop = compItem.TimeItems.GetAllScoopTime();
                                        hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                        wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                        wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                        wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                        if (wtItemProd.WorkTime > 0)
                                        {
                                            SumMonth[indMonth].Fprod++;
                                            SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                        }

                                        if (wtItemInj.WorkTime > 0)
                                        {
                                            SumMonth[indMonth].Finj++;
                                            SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                            if (compItem.SumInjectionGas > 0) SumMonth[indMonth].WorkTimeInjGas += wtItemInj.WorkTime;
                                        }
                                        if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                        for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                        {
                                            switch (compItem.CharWorkIds[c])
                                            {
                                                case 11:
                                                    SumMonth[indMonth].Liq += compItem.SumLiquid;
                                                    SumMonth[indMonth].Oil += compItem.SumOil;
                                                    if (SumMonth[indMonth].Liq > 0)
                                                    {
                                                        SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                    }

                                                    SumMonth[indMonth].LiqV += compItem.SumLiquidV;
                                                    SumMonth[indMonth].OilV += compItem.SumOilV;
                                                    if (!pvt.IsEmpty && (compItem.SumLiquidV > 0))
                                                    {
                                                        SumMonth[indMonth].LiqVb += (compItem.SumLiquidV - compItem.SumOilV) * pvt.WaterVolumeFactor + compItem.SumOilV * pvt.OilVolumeFactor;
                                                        SumMonth[indMonth].AccumLiqVb = SumMonth[indMonth].LiqVb;
                                                    }
                                                    
                                                    if (SumMonth[indMonth].LiqV > 0)
                                                    {
                                                        SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                    }
                                                    break;
                                                case 12:
                                                    if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                    SumMonth[indMonth].NatGas += compItem.SumNaturalGas;
                                                    break;
                                                case 13:
                                                    SumMonth[indMonth].Scoop += compItem.SumScoop;
                                                    break;
                                                case 15:
                                                    if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                    SumMonth[indMonth].GasCondensate += compItem.SumGasCondensate;
                                                    break;
                                                case 20:
                                                    SumMonth[indMonth].Inj += compItem.SumInjection;
                                                    if (!pvt.IsEmpty && (compItem.SumInjection > 0))
                                                    {
                                                        SumMonth[indMonth].InjVb += compItem.SumInjection * pvt.WaterVolumeFactor;
                                                        SumMonth[indMonth].AccumInjVb = SumMonth[indMonth].InjVb;
                                                    }

                                                    if (merComp.UseInjGas)
                                                    {
                                                        if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                        SumMonth[indMonth].InjGas += compItem.SumInjectionGas;
                                                    }
                                                    break;
                                            }
                                        }
                                        // по годам
                                        indYears = compItem.Date.Year - minDate.Year;
                                        
                                        if (wtItemProd.WorkTime > 0)
                                        {
                                            if (lastYearProd[0][0] != compItem.Date.Year)
                                            {
                                                SumYear[indYears].Fprod++;
                                                lastYearProd[0][0] = compItem.Date.Year;
                                            }
                                            SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                        }
                                        if (wtItemInj.WorkTime > 0)
                                        {
                                            if (lastYearProd[0][1] != compItem.Date.Year)
                                            {
                                                SumYear[indYears].Finj++;
                                                lastYearProd[0][1] = compItem.Date.Year;
                                            }
                                            SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                        }
                                        if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                        for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                        {
                                            switch (compItem.CharWorkIds[c])
                                            {
                                                case 11:
                                                    SumYear[indYears].Liq += compItem.SumLiquid;
                                                    SumYear[indYears].Oil += compItem.SumOil;
                                                    if (SumYear[indYears].Liq > 0)
                                                    {
                                                        SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                    }
                                                    SumYear[indYears].LiqV += compItem.SumLiquidV;
                                                    SumYear[indYears].OilV += compItem.SumOilV;
                                                    if (!pvt.IsEmpty && (compItem.SumLiquidV > 0))
                                                    {
                                                        SumYear[indYears].LiqVb += (compItem.SumLiquidV - compItem.SumOilV) * pvt.WaterVolumeFactor + compItem.SumOilV * pvt.OilVolumeFactor;
                                                        SumYear[indYears].AccumLiqVb = SumYear[indYears].LiqVb;
                                                    }
                                                    if (SumYear[indYears].LiqV > 0)
                                                    {
                                                        SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                    }
                                                    break;
                                                case 12:
                                                    if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                    SumYear[indYears].NatGas += compItem.SumNaturalGas;
                                                    break;
                                                case 13:
                                                    SumYear[indYears].Scoop += compItem.SumScoop;
                                                    break;
                                                case 15:
                                                    if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                    SumYear[indYears].GasCondensate += compItem.SumGasCondensate;
                                                    break;
                                                case 20:
                                                    SumYear[indYears].Inj += compItem.SumInjection;
                                                    if (!pvt.IsEmpty && (compItem.SumInjection > 0))
                                                    {
                                                        SumYear[indYears].InjVb += compItem.SumInjection * pvt.WaterVolumeFactor;
                                                        SumYear[indYears].AccumInjVb = SumYear[indYears].InjVb;
                                                    }
                                                    if (merComp.UseInjGas)
                                                    {
                                                        if (SumYear[indYears].InjGas < 0) SumYear[indYears].InjGas = 0;
                                                        SumYear[indYears].InjGas += compItem.SumInjectionGas;
                                                    }
                                                    break;
                                            }
                                        }
                                        #endregion

                                        #region По объектам
                                        for (k = 0; k < compItem.PlastItems.Count; k++)
                                        {
                                            plastItem = compItem.PlastItems[k];
                                            objIndex = sumParams.GetIndexByObjCode(plastItem.PlastCode);
                                            if (objIndex > -1)
                                            {
                                                SumMonth = sumParams[objIndex].MonthlyItems;
                                                SumYear = sumParams[objIndex].YearlyItems;

                                                wtItemProd = compItem.TimeItems.GetProductionTime(plastItem.PlastCode);
                                                wtItemInj = compItem.TimeItems.GetInjectionTime(plastItem.PlastCode);
                                                wtItemScoop = compItem.TimeItems.GetScoopTime(plastItem.PlastCode);
                                                hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                                wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                                wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                                wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                                indMonth = (compItem.Date.Year - minMaxDate[2 * objIndex - 2].Year) * 12 + compItem.Date.Month - minMaxDate[2 * objIndex - 2].Month;

                                                if (wtItemProd.WorkTime > 0)
                                                {
                                                    SumMonth[indMonth].Fprod++;
                                                    SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                                }

                                                if (wtItemInj.WorkTime > 0)
                                                {
                                                    SumMonth[indMonth].Finj++;
                                                    SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                                    if (plastItem.InjGas > 0) SumMonth[indMonth].WorkTimeInjGas += wtItemInj.WorkTime;
                                                }

                                                if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;


                                                for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                                {
                                                    switch (compItem.CharWorkIds[c])
                                                    {
                                                        case 11:
                                                            SumMonth[indMonth].Liq += plastItem.Liq;
                                                            SumMonth[indMonth].Oil += plastItem.Oil;
                                                            if (SumMonth[indMonth].Liq > 0)
                                                            {
                                                                SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                            }
                                                            if (!pvt.IsEmpty && (plastItem.LiqV > 0))
                                                            {
                                                                SumMonth[indMonth].LiqVb += (plastItem.LiqV - plastItem.OilV) * pvt.WaterVolumeFactor + plastItem.OilV * pvt.OilVolumeFactor;
                                                                SumMonth[indMonth].AccumLiqVb = SumMonth[indMonth].LiqVb;
                                                            }
                                                            //if (indMonth > 0 && SumMonth[indMonth - 1].AccumLiqVb > 0)
                                                            //{
                                                            //    SumMonth[indMonth].AccumLiqVb += SumMonth[indMonth - 1].AccumLiqVb;
                                                            //}
                                                            SumMonth[indMonth].LiqV += plastItem.LiqV;
                                                            SumMonth[indMonth].OilV += plastItem.OilV;
                                                            if (SumMonth[indMonth].LiqV > 0)
                                                            {
                                                                SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                            }
                                                            break;
                                                        case 12:
                                                            if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                            SumMonth[indMonth].NatGas += plastItem.NatGas;
                                                            break;
                                                        case 13:
                                                            SumMonth[indMonth].Scoop += plastItem.Scoop;
                                                            break;
                                                        case 15:
                                                            if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                            SumMonth[indMonth].GasCondensate += plastItem.Condensate;
                                                            break;
                                                        case 20:
                                                            SumMonth[indMonth].Inj += plastItem.Inj;
                                                            if (!pvt.IsEmpty && (plastItem.Inj > 0))
                                                            {
                                                                SumMonth[indMonth].InjVb += plastItem.Inj * pvt.WaterVolumeFactor;
                                                                SumMonth[indMonth].AccumInjVb = SumMonth[indMonth].InjVb;
                                                                
                                                            }
                                                            //if (indMonth > 0 && SumMonth[indMonth - 1].AccumInjVb > 0) 
                                                            //{
                                                            //    SumMonth[indMonth].AccumInjVb += SumMonth[indMonth - 1].AccumInjVb;
                                                            //}
                                                            if (merComp.UseInjGas)
                                                            {
                                                                if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                                SumMonth[indMonth].InjGas += plastItem.InjGas;
                                                            }
                                                            break;
                                                    }
                                                }
                                                // по годам
                                                indYears = compItem.Date.Year - minMaxDate[2 * objIndex - 2].Year;
                                                if (wtItemProd.WorkTime > 0)
                                                {
                                                    if (lastYearProd[objIndex][0] != compItem.Date.Year)
                                                    {
                                                        SumYear[indYears].Fprod++;
                                                        lastYearProd[objIndex][0] = compItem.Date.Year;
                                                    }
                                                    SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                                }
                                                if (wtItemInj.WorkTime > 0)
                                                {
                                                    if (lastYearProd[objIndex][1] != compItem.Date.Year)
                                                    {
                                                        SumYear[indYears].Finj++;
                                                        lastYearProd[objIndex][1] = compItem.Date.Year;
                                                    }
                                                    SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                                    if (plastItem.InjGas > 0) SumYear[indYears].WorkTimeInjGas += wtItemInj.WorkTime;
                                                }
                                                if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                                for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                                {
                                                    switch (compItem.CharWorkIds[c])
                                                    {
                                                        case 11:
                                                            SumYear[indYears].Liq += plastItem.Liq;
                                                            SumYear[indYears].Oil += plastItem.Oil;
                                                            if (SumYear[indYears].Liq > 0)
                                                            {
                                                                SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                            }
                                                            if (!pvt.IsEmpty && (plastItem.LiqV > 0))
                                                            {
                                                                SumYear[indYears].LiqVb += (plastItem.LiqV - plastItem.OilV) * pvt.WaterVolumeFactor + plastItem.OilV * pvt.OilVolumeFactor;
                                                                SumYear[indYears].AccumLiqVb = SumYear[indYears].LiqVb;
                                                                //if (indYears > 0) SumYear[indYears].AccumLiqVb += SumYear[indYears - 1].AccumLiqVb;
                                                            }
                                                            SumYear[indYears].LiqV += plastItem.LiqV;
                                                            SumYear[indYears].OilV += plastItem.OilV;
                                                            if (SumYear[indYears].LiqV > 0)
                                                            {
                                                                SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                            }
                                                            break;
                                                        case 12:
                                                            if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                            SumYear[indYears].NatGas += plastItem.NatGas;
                                                            break;
                                                        case 13:
                                                            SumYear[indYears].Scoop += plastItem.Scoop;
                                                            break;
                                                        case 15:
                                                            if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                            SumYear[indYears].GasCondensate += plastItem.Condensate;
                                                            break;
                                                        case 20:
                                                            SumYear[indYears].Inj += plastItem.Inj;
                                                            if (!pvt.IsEmpty && (plastItem.Inj > 0))
                                                            {
                                                                SumYear[indYears].InjVb += plastItem.Inj * pvt.WaterVolumeFactor;
                                                                SumYear[indYears].AccumInjVb = SumYear[indYears].InjVb;
                                                                //if (indYears > 0) SumYear[indYears].AccumInjVb += SumYear[indYears - 1].AccumInjVb;
                                                            }
                                                            if (merComp.UseInjGas)
                                                            {
                                                                if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                                SumYear[indYears].InjGas += plastItem.InjGas;
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion

                                #region LOAD FROM RESEARCH
                                if (w.ResearchLoaded)
                                {
                                    WellResearchItem rItem, rItem2;
                                    int indMonth2, indYears2;
                                    int j2;
                                    for (j = 0; j < w.research.Count; j++)
                                    {
                                        rItem = w.research[j];
                                        if (!UseInjWellsPressure && merComp != null)
                                        {
                                            ind = merComp.GetIndexByDate(new DateTime(rItem.Date.Year, rItem.Date.Month, 1));
                                            if (ind > -1 && merComp[ind].CharWorkIds.IndexOf(20) != -1)
                                            {
                                                break;
                                            }
                                        }
                                        if ((rItem.PressPerforation > 0) &&
                                            ((rItem.ResearchCode == 2 && rItem.TimeDelay > 0) || (rItem.ResearchCode == 4) || (rItem.ResearchCode == 6)))
                                        {
                                            objIndex = sumParams.GetIndexByObjCode(rItem.PlastCode);
                                            if (objIndex > -1)
                                            {

                                                SumMonth = sumParams[objIndex].MonthlyItems;
                                                SumYear = sumParams[objIndex].YearlyItems;

                                                indMonth = (rItem.Date.Year - minMaxDate[2 * objIndex - 2].Year - 1) * 12 + (12 - minMaxDate[2 * objIndex - 2].Month) + rItem.Date.Month;
                                                indYears = rItem.Date.Year - minMaxDate[2 * objIndex - 2].Year;

                                                indMonth2 = -1; indYears2 = -1;
                                                for (j2 = j + 1; j2 < w.research.Count; j2++)
                                                {
                                                    rItem2 = w.research[j2];
                                                    if (!UseInjWellsPressure && merComp != null)
                                                    {
                                                        ind = merComp.GetIndexByDate(new DateTime(rItem2.Date.Year, rItem2.Date.Month, 1));
                                                        if (ind > -1 && merComp[ind].CharWorkIds.IndexOf(20) != -1)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    if ((rItem2.PressPerforation > 0) &&
                                                        ((rItem2.ResearchCode == 2 && rItem2.TimeDelay > 0) || (rItem2.ResearchCode == 4) || (rItem2.ResearchCode == 6)) &&
                                                        (stratumDict.EqualsStratum(rItem.PlastCode, rItem2.PlastCode)))
                                                    {

                                                        indMonth2 = (rItem2.Date.Year - minMaxDate[2 * objIndex - 2].Year) * 12 + rItem2.Date.Month - minMaxDate[2 * objIndex - 2].Month;
                                                        indYears2 = rItem2.Date.Year - minMaxDate[2 * objIndex - 2].Year;
                                                        break;
                                                    }
                                                }
                                                if (indMonth2 == -1) indMonth2 = SumMonth.Length;
                                                if (indYears2 == -1) indYears2 = SumYear.Length;

                                                if ((indMonth > -1) && (indMonth < SumMonth.Length))
                                                {
                                                    SumMonth[indMonth].Pressure += rItem.PressPerforation;
                                                    SumMonth[indMonth].PressureCounter++;
                                                    for (k = indMonth + 1; (k < indMonth2) && (k < SumMonth.Length); k++)
                                                    {
                                                        SumMonth[k].Pressure += rItem.PressPerforation;
                                                        SumMonth[k].PressureCounter++;
                                                    }
                                                }

                                                if ((indYears > -1) && (indYears < SumYear.Length))
                                                {
                                                    SumYear[indYears].Pressure += rItem.PressPerforation;
                                                    SumYear[indYears].PressureCounter++;
                                                    for (k = indYears + 1; (k < indYears2) && (k < SumYear.Length); k++)
                                                    {
                                                        SumYear[k].Pressure += rItem.PressPerforation;
                                                        SumYear[k].PressureCounter++;
                                                    }
                                                }
                                            }
                                            // к объекту ALL

                                            SumMonth = sumParams[0].MonthlyItems;
                                            SumYear = sumParams[0].YearlyItems;
                                            indMonth = (rItem.Date.Year - minDate.Year) * 12 + rItem.Date.Month - minDate.Month;
                                            indYears = rItem.Date.Year - minDate.Year;
                                            indMonth2 = -1; indYears2 = -1;
                                            for (j2 = j + 1; j2 < w.research.Count; j2++)
                                            {
                                                rItem2 = w.research[j2];
                                                if (!UseInjWellsPressure && merComp != null)
                                                {
                                                    ind = merComp.GetIndexByDate(new DateTime(rItem2.Date.Year, rItem2.Date.Month, 1));
                                                    if (ind > -1 && merComp[ind].CharWorkIds.IndexOf(20) != -1)
                                                    {
                                                        break;
                                                    }
                                                }
                                                if ((rItem2.PressPerforation > 0) &&
                                                    ((rItem2.ResearchCode == 2 && rItem2.TimeDelay > 0) || (rItem2.ResearchCode == 4) || (rItem2.ResearchCode == 6)))
                                                {
                                                    indMonth2 = (rItem2.Date.Year - minDate.Year) * 12 + rItem2.Date.Month - minDate.Month;
                                                    indYears2 = rItem2.Date.Year - minDate.Year;
                                                    break;
                                                }
                                            }
                                            if (indMonth2 == -1) indMonth2 = SumMonth.Length;
                                            if (indYears2 == -1) indYears2 = SumYear.Length;

                                            if ((indMonth > -1) && (indMonth < SumMonth.Length))
                                            {
                                                SumMonth[indMonth].Pressure += rItem.PressPerforation;
                                                SumMonth[indMonth].PressureCounter++;
                                                
                                                for (k = indMonth + 1; (k < indMonth2) && (k < SumMonth.Length); k++)
                                                {
                                                    SumMonth[k].Pressure += rItem.PressPerforation;
                                                    SumMonth[k].PressureCounter++;
                                                }
                                            }

                                            if ((indYears > -1) && (indYears < SumYear.Length) && (indYears2 > -1))
                                            {
                                                SumYear[indYears].Pressure += rItem.PressPerforation;
                                                SumYear[indYears].PressureCounter++;
                                                for (k = indYears + 1; (k < indYears2) && (k < SumYear.Length); k++)
                                                {
                                                    SumYear[k].Pressure += rItem.PressPerforation;
                                                    SumYear[k].PressureCounter++;
                                                }
                                            }
                                            j = j2 - 1;
                                        }
                                    }
                                    // по забойному давлению
                                    for (j = 0; j < w.research.Count; j++)
                                    {
                                        rItem = w.research[j];
                                        if ((rItem.PressPerforation > 0) && ((rItem.ResearchCode == 1) || (rItem.ResearchCode == 3)))
                                        {
                                            objIndex = sumParams.GetIndexByObjCode(rItem.PlastCode);
                                            if (objIndex > -1)
                                            {
                                                SumMonth = sumParams[objIndex].MonthlyItems;
                                                SumYear = sumParams[objIndex].YearlyItems;

                                                indMonth = (rItem.Date.Year - minMaxDate[2 * objIndex - 2].Year - 1) * 12 + (12 - minMaxDate[2 * objIndex - 2].Month) + rItem.Date.Month;
                                                indYears = rItem.Date.Year - minMaxDate[2 * objIndex - 2].Year;

                                                indMonth2 = -1; indYears2 = -1;
                                                for (j2 = j + 1; j2 < w.research.Count; j2++)
                                                {
                                                    rItem2 = w.research[j2];
                                                    if ((rItem2.PressPerforation > 0) && ((rItem2.ResearchCode == 1) || (rItem2.ResearchCode == 3)) &&
                                                        (stratumDict.EqualsStratum(rItem.PlastCode, rItem2.PlastCode)))
                                                    {
                                                        indMonth2 = (rItem2.Date.Year - minMaxDate[2 * objIndex - 2].Year) * 12 + rItem2.Date.Month - minMaxDate[2 * objIndex - 2].Month;
                                                        indYears2 = rItem2.Date.Year - minMaxDate[2 * objIndex - 2].Year;
                                                        break;
                                                    }
                                                }
                                                if (indMonth2 == -1) indMonth2 = SumMonth.Length;
                                                if (indYears2 == -1) indYears2 = SumYear.Length;

                                                if ((indMonth > -1) && (indMonth < SumMonth.Length))
                                                {
                                                    SumMonth[indMonth].ZabPressure += rItem.PressPerforation;
                                                    SumMonth[indMonth].ZabPressureCounter++;
                                                    for (k = indMonth + 1; (k < indMonth2) && (k < SumMonth.Length); k++)
                                                    {
                                                        SumMonth[k].ZabPressure += rItem.PressPerforation;
                                                        SumMonth[k].ZabPressureCounter++;
                                                    }
                                                }

                                                if ((indYears > -1) && (indYears < SumYear.Length))
                                                {
                                                    SumYear[indYears].ZabPressure += rItem.PressPerforation;
                                                    SumYear[indYears].ZabPressureCounter++;
                                                    for (k = indYears + 1; (k < indYears2) && (k < SumYear.Length); k++)
                                                    {
                                                        SumYear[k].ZabPressure += rItem.PressPerforation;
                                                        SumYear[k].ZabPressureCounter++;
                                                    }
                                                }
                                            }
                                            // к объекту ALL
                                            SumMonth = sumParams[0].MonthlyItems;
                                            SumYear = sumParams[0].YearlyItems;
                                            indMonth = (rItem.Date.Year - minDate.Year) * 12 + rItem.Date.Month - minDate.Month;
                                            indYears = rItem.Date.Year - minDate.Year;
                                            indMonth2 = -1; indYears2 = -1;
                                            for (j2 = j + 1; j2 < w.research.Count; j2++)
                                            {
                                                rItem2 = w.research[j2];
                                                if ((rItem2.PressPerforation > 0) && ((rItem2.ResearchCode == 1) || (rItem2.ResearchCode == 3)))
                                                {
                                                    indMonth2 = (rItem2.Date.Year - minDate.Year) * 12 + rItem2.Date.Month - minDate.Month;
                                                    indYears2 = rItem2.Date.Year - minDate.Year;
                                                    break;
                                                }
                                            }
                                            if (indMonth2 == -1) indMonth2 = SumMonth.Length;
                                            if (indYears2 == -1) indYears2 = SumYear.Length;

                                            if ((indMonth > -1) && (indMonth < SumMonth.Length))
                                            {
                                                SumMonth[indMonth].ZabPressure += rItem.PressPerforation;
                                                SumMonth[indMonth].ZabPressureCounter++;
                                                for (k = indMonth + 1; (k < indMonth2) && (k < SumMonth.Length); k++)
                                                {
                                                    SumMonth[k].ZabPressure += rItem.PressPerforation;
                                                    SumMonth[k].ZabPressureCounter++;
                                                }
                                            }

                                            if ((indYears > -1) && (indYears < SumYear.Length) && (indYears2 > -1))
                                            {
                                                SumYear[indYears].ZabPressure += rItem.PressPerforation;
                                                SumYear[indYears].ZabPressureCounter++;
                                                for (k = indYears + 1; (k < indYears2) && (k < SumYear.Length); k++)
                                                {
                                                    SumYear[k].ZabPressure += rItem.PressPerforation;
                                                    SumYear[k].ZabPressureCounter++;
                                                }
                                            }
                                            j = j2 - 1;
                                        }
                                    }
                                    w.research.Clear();
                                    w.research = null;
                                }
                                #endregion

                                userState.Element = w.UpperCaseName;
                                worker.ReportProgress(i, userState);
                            }
                        }
                        // Calc Compensation
                        for (i = 0; i < sumParams.Count; i++)
                        {
                            for (j = 1; j < sumParams[i].MonthlyItems.Length; j++)
                            {
                                if(sumParams[i].MonthlyItems[j - 1].AccumLiqVb > 0)
                                {
                                    sumParams[i].MonthlyItems[j].AccumLiqVb += sumParams[i].MonthlyItems[j - 1].AccumLiqVb;
                                }
                                if (sumParams[i].MonthlyItems[j - 1].AccumInjVb > 0)
                                {
                                    sumParams[i].MonthlyItems[j].AccumInjVb += sumParams[i].MonthlyItems[j - 1].AccumInjVb;
                                }
                            }
                            for (j = 1; j < sumParams[i].YearlyItems.Length; j++)
                            {
                                if (sumParams[i].YearlyItems[j - 1].AccumLiqVb > 0)
                                {
                                    sumParams[i].YearlyItems[j].AccumLiqVb += sumParams[i].YearlyItems[j - 1].AccumLiqVb;
                                }
                                if (sumParams[i].YearlyItems[j - 1].AccumInjVb > 0)
                                {
                                    sumParams[i].YearlyItems[j].AccumInjVb += sumParams[i].YearlyItems[j - 1].AccumInjVb;
                                }
                            }
                        }

                        for (i = 0; i < SumCalcWelllist.Count; i++)
                        {
                            w = (Well)SumCalcWelllist[i];
                            if (w.OilFieldIndex > -1)
                            {
                                of = MapProject.OilFields[w.OilFieldIndex];
                                if (of.MerLoaded) of.ClearMerData(false);
                            }
                            if (w.MerLoaded)
                            {
                                w.mer = null;
                            }
                        }
                        retValue = true;
                    }
                }
            }
            SumCalcWelllist.Clear();
            userState.Element = "";
            //worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool SumChessParamsReCalc(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            bool res = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка суммарных показателей по Шахматке";
            userState.Element = "Выбранный список скважин";
            if ((this.selWellList.Count > 0) && (MapProject != null))
            {
                int i, j, iLen;
                DateTime minDate, maxDate;
                Well w;
                OilField of;
                ChessItem item;
                ChessInjItem itemInj;
                minDate = DateTime.MaxValue;
                maxDate = DateTime.MinValue;
                SumCalcWelllist.Clear();
                for (i = 0; i < selWellList.Count; i++)
                {
                    SumCalcWelllist.Add(selWellList[i]);
                }

                iLen = SumCalcWelllist.Count;
                worker.ReportProgress(0, userState);
                for (i = 0; i < SumCalcWelllist.Count; i++)
                {
                    w = (Well)SumCalcWelllist[i];
                    userState.Element = w.UpperCaseName;
                    if (w.OilFieldIndex == -1) continue;

                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        this.sumChessParams = null;
                        return false;
                    }
                    of = MapProject.OilFields[w.OilFieldIndex];
                    if ((!w.ChessLoaded) && (!w.ChessInjLoaded) && (!of.ChessLoaded))
                    {
                        of.LoadChessFromCache(w.Index);
                    }

                    if ((w.ChessLoaded) && (w.chess.Count > 0))
                    {
                        for (j = 0; j < 2; j++)
                        {
                            if (j == 0) item = w.chess[j];
                            else item = w.chess[w.chess.Count - 1];

                            if (item.Date < minDate) minDate = item.Date;
                            if (item.Date > maxDate) maxDate = item.Date;
                        }
                        if (!of.ChessLoaded)
                        {
                            w.chess.Clear();
                            w.chess = null;
                        }
                    }
                    if (maxDate > DateTime.Now) maxDate = DateTime.Now;
                    if ((w.ChessInjLoaded) && (w.chessInj.Count > 0))
                    {
                        for (j = 0; j < 2; j++)
                        {
                            if (j == 0) itemInj = w.chessInj[j];
                            else itemInj = w.chessInj[w.chessInj.Count - 1];

                            if (itemInj.Date < minDate) minDate = itemInj.Date;
                            if ((itemInj.Date > maxDate) && (itemInj.Date <= DateTime.Now)) maxDate = itemInj.Date;
                        }
                        if (!of.ChessLoaded)
                        {
                            w.chessInj.Clear();
                            w.chessInj = null;
                        }
                    }
                    worker.ReportProgress(i, userState);
                }
                GC.GetTotalMemory(true);
                if ((minDate != DateTime.MaxValue) && (maxDate != DateTime.MinValue))
                {
                    int days;
                    DateTime dt;
                    TimeSpan ts = maxDate - minDate;
                    days = ts.Days + 1;
                    int indDay;
                    double predQliq;
                    DateTime predDT;

                    if (days > 0)
                    {
                        SumParamItem[] SumDay = new SumParamItem[days];

                        dt = minDate;
                        for (i = 0; i < days; i++)
                        {
                            SumDay[i].Date = dt;
                            dt = dt.AddDays(1);
                        }
                        iLen = SumCalcWelllist.Count;
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                this.sumChessParams = null;
                                return false;
                            }
                            else
                            {
                                w = (Well)SumCalcWelllist[i];
                                of = MapProject.OilFields[w.OilFieldIndex];
                                if ((!w.ChessLoaded) && (!w.ChessInjLoaded) && (!of.ChessLoaded))
                                {
                                    (MapProject.OilFields[w.OilFieldIndex]).LoadChessFromCache(w.Index);
                                }
                                predDT = DateTime.Now;
                                predQliq = -1;
                                if (w.ChessLoaded)
                                {
                                    for (j = 0; j < w.chess.Count; j++)
                                    {
                                        item = w.chess[j];
                                        ts = item.Date - minDate;
                                        indDay = ts.Days;

                                        if ((indDay < SumDay.Length) && (SumDay[indDay].Date == item.Date))
                                        {
                                            if (((predQliq == -1) || (predDT != item.Date) || (predQliq != item.Qliq + item.Qoil)) && (item.Qliq + item.Qoil > 0))
                                            {
                                                if (item.StayTime < 24)
                                                {
                                                    SumDay[indDay].Liq += item.Qliq;
                                                    SumDay[indDay].Oil += item.Qoil;
                                                    SumDay[indDay].LiqV += item.QliqV;
                                                    SumDay[indDay].OilV += item.QoilV;

                                                    SumDay[indDay].Fprod++;
                                                }
                                                predDT = item.Date;
                                                predQliq = item.Qliq + item.Qoil;
                                            }
                                        }
                                        else if (indDay < SumDay.Length)
                                        {
                                            MessageBox.Show("Не совпадает дата!" + this.Name + "[" + w.UpperCaseName + "-" + item.Date + "]", "Загрузка суммарных показателей");
                                        }
                                    }
                                    if (!of.ChessLoaded)
                                    {
                                        w.chess.Clear();
                                        w.chess = null;
                                    }
                                }
                                if (w.ChessInjLoaded)
                                {
                                    if (w.chessInj.Count > 0)
                                    {
                                        indDay = 0;
                                        int countDays = 0;
                                        indDay = (w.chessInj[0].Date - SumDay[0].Date).Days;
                                        for (j = 0; j < w.chessInj.Count; j++)
                                        {
                                            itemInj = w.chessInj[j];
                                            countDays = 1;
                                            if (j < w.chessInj.Count - 1)
                                            {
                                                countDays = (w.chessInj[j + 1].Date - itemInj.Date).Days;
                                            }
                                            while (countDays > 0)
                                            {
                                                if ((indDay < SumDay.Length) && (itemInj.Wwat != -1) && (itemInj.StayTime != 24))
                                                {
                                                    if (itemInj.StayTime < 24)
                                                    {
                                                        SumDay[indDay].Inj += itemInj.Wwat;
                                                        SumDay[indDay].Finj++;
                                                    }
                                                }
                                                indDay++;
                                                countDays--;
                                            }
                                        }
                                    }
                                    if (!of.ChessLoaded)
                                    {
                                        w.chessInj.Clear();
                                        w.chessInj = null;
                                    }
                                }
                                userState.Element = w.UpperCaseName;
                                worker.ReportProgress(i + iLen, userState);
                            }
                        }
                        this.sumChessParams = new SumParameters(1);
                        this.sumChessParams.Add(-1, SumDay, null);
                        retValue = true;
                    }
                }
            }
            SumCalcWelllist.Clear();
            userState.Element = this.Name;
            return retValue;
        }
        internal struct SumParamsOneDateShift
        {
            public int MonthBefore, MonthAfter;
            public int YearsBefore, YearsAfter;
        }
        public bool SumParamsReCalcByOneDate(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей по МЭР на единую дату";
            userState.Element = "Выбранный список скважин";

            if ((this.selWellList.Count > 0) && (MapProject != null))
            {
                int i, j, k, iLen;
                List<int> injGasId = new List<int>(new int[] { 81, 82, 83, 84, 85, 86, 87, 88, 89 });
                selMerOilObjCodes.Clear();
                SumCalcWelllist.Clear();
                for (i = 0; i < selWellList.Count; i++) SumCalcWelllist.Add(selWellList[i]);

                PhantomWell pw;
                OilField of;
                MerItem item;
                ArrayList ofIndexes = new ArrayList(5);
                for (i = 0; i < SumCalcWelllist.Count; i++)
                {
                    pw = (PhantomWell)SumCalcWelllist[i];
                    if ((pw.Caption != null) && (pw.Caption.Date != DateTime.MinValue))
                    {
                        if ((pw.srcWell.OilFieldIndex != -1) && (ofIndexes.IndexOf((int)pw.srcWell.OilFieldIndex) == -1))
                        {
                            of = MapProject.OilFields[pw.srcWell.OilFieldIndex];
                            ofIndexes.Add(pw.srcWell.OilFieldIndex);
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                this.sumParams = null;
                                return false;
                            }
                            if (of.MerStratumCodes.Count == 0)
                            {
                                if (!of.LoadStratumCodesFromCache()) return false;
                            }
                            for (j = 0; j < of.MerStratumCodes.Count; j++)
                            {
                                if (selMerOilObjCodes.IndexOf(of.MerStratumCodes[j]) == -1)
                                {
                                    selMerOilObjCodes.Add(of.MerStratumCodes[j]);
                                }
                            }
                        }
                    }
                }

                SumParamsOneDateShift[] shifts = new SumParamsOneDateShift[selMerOilObjCodes.Count + 1];
                for (i = 0; i < selMerOilObjCodes.Count + 1; i++)
                {
                    shifts[i].MonthBefore = -1;
                    shifts[i].MonthAfter = -1;
                    shifts[i].YearsBefore = -1;
                    shifts[i].YearsAfter = -1;
                }

                iLen = SumCalcWelllist.Count;
                DateTime OneDate;
                int monthBefore, monthAfter, yearsBefore, yearsAfter, maxMonth, maxYears;

                for (i = 0; i < iLen; i++)
                {
                    pw = (PhantomWell)SumCalcWelllist[i];
                    userState.Element = pw.srcWell.UpperCaseName;
                    if (pw.srcWell.OilFieldIndex == -1) continue;
                    if ((pw.Caption != null) && (pw.Caption.Date != DateTime.MinValue))
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            this.sumParams = null;
                            return false;
                        }
                        if ((!pw.srcWell.MerLoaded) && (!(MapProject.OilFields[pw.srcWell.OilFieldIndex]).MerLoaded))
                        {
                            of = MapProject.OilFields[pw.srcWell.OilFieldIndex];
                            of.LoadMerFromCache(pw.srcWell.Index);
                        }

                        if ((pw.srcWell.MerLoaded) && (pw.srcWell.mer.Count > 0))
                        {
                            OneDate = pw.Caption.Date;
                            for (k = 0; k < selMerOilObjCodes.Count; k++)
                            {
                                monthBefore = -1;
                                monthAfter = -1;
                                yearsBefore = -1;
                                yearsAfter = -1;
                                for (j = 0; j < pw.srcWell.mer.Count; j++)
                                {
                                    item = pw.srcWell.mer.Items[j];
                                    if (item.PlastId == (int)selMerOilObjCodes[k])
                                    {
                                        yearsBefore = OneDate.Year - item.Date.Year;
                                        monthBefore = (OneDate.Year - item.Date.Year - 1) * 12 + (13 - item.Date.Month) + OneDate.Month - 1;
                                        if (yearsBefore < 0) yearsBefore = 0;
                                        if (monthBefore < 0) monthBefore = 0;
                                        //if (item.Date < OneDate)
                                        //{
                                        //  yearsBefore = OneDate.Year - item.Date.Year;
                                        //  monthBefore = (yearsBefore - 1) * 12 + (13 - item.Date.Month) + OneDate.Month - 1;
                                        //}
                                        break;
                                    }
                                }
                                if (monthBefore != -1)
                                {
                                    for (j = pw.srcWell.mer.Count - 1; j >= 0; j--)
                                    {
                                        item = pw.srcWell.mer.Items[j];
                                        if (item.PlastId == (int)selMerOilObjCodes[k])
                                        {
                                            yearsAfter = item.Date.Year - OneDate.Year + 1;
                                            monthAfter = (item.Date.Year - OneDate.Year - 1) * 12 + (13 - OneDate.Month) + item.Date.Month;
                                            if (yearsAfter < 0) yearsAfter = 0;
                                            if (monthAfter < 0) monthAfter = 0;
                                            //if (item.Date >= OneDate)
                                            //{
                                            //    yearsAfter = item.Date.Year - OneDate.Year + 1;
                                            //    monthAfter = (yearsAfter - 2) * 12 + (13 - OneDate.Month) + item.Date.Month;
                                            //}
                                            break;
                                        }
                                    }
                                }
                                if ((monthBefore != -1) && (monthAfter != -1))
                                {
                                    if (shifts[k + 1].MonthBefore < monthBefore) shifts[k + 1].MonthBefore = monthBefore;
                                    if (shifts[k + 1].MonthAfter < monthAfter) shifts[k + 1].MonthAfter = monthAfter;
                                    if (shifts[k + 1].YearsBefore < yearsBefore) shifts[k + 1].YearsBefore = yearsBefore;
                                    if (shifts[k + 1].YearsAfter < yearsAfter) shifts[k + 1].YearsAfter = yearsAfter;
                                }
                            }
                        }
                    }
                    worker.ReportProgress(i, userState);
                }
                int len = selMerOilObjCodes.Count;
                j = 0;
                for (i = 0; i < len; i++)
                {
                    if (shifts[i + 1].MonthBefore == -1)
                    {
                        selMerOilObjCodes.RemoveAt(j);
                        j--;
                    }
                    else
                    {
                        if (shifts[0].MonthBefore < shifts[i + 1].MonthBefore) shifts[0].MonthBefore = shifts[i + 1].MonthBefore;
                        if (shifts[0].YearsBefore < shifts[i + 1].YearsBefore) shifts[0].YearsBefore = shifts[i + 1].YearsBefore;

                        if (shifts[0].MonthAfter < shifts[i + 1].MonthAfter) shifts[0].MonthAfter = shifts[i + 1].MonthAfter;
                        if (shifts[0].YearsAfter < shifts[i + 1].YearsAfter) shifts[0].YearsAfter = shifts[i + 1].YearsAfter;
                    }
                    j++;
                }
                for (i = 1; i < len + 1; i++)
                {
                    if (shifts[i].MonthBefore == -1)
                    {
                        j = i + 1; k = -1;
                        while (j < len + 1)
                        {
                            if (shifts[j].MonthBefore != -1)
                            {
                                k = j;
                                break;
                            }
                            j++;
                        }
                        if (k != -1)
                        {
                            shifts[i] = shifts[k];
                            shifts[k].MonthAfter = -1;
                            shifts[k].MonthBefore = -1;
                            shifts[k].YearsAfter = -1;
                            shifts[k].YearsBefore = -1;
                        }
                    }
                }


                if ((selMerOilObjCodes.Count > 0) &&
                    (shifts[0].MonthBefore + shifts[0].MonthAfter > -1) &&
                    (shifts[0].YearsBefore + shifts[0].YearsAfter > -1))
                {
                    int month, years;
                    bool bmonth, bProd, bInj, bScoop, bWorkTime, bAllMonth, bAllProd, bAllInj, bAllScoop;
                    DateTime dt;
                    if (shifts[0].MonthBefore + shifts[0].MonthAfter > 0)
                    {
                        sumParams = new SumParameters(selMerOilObjCodes.Count + 1);

                        SumParamItem[] SumMonth = new SumParamItem[shifts[0].MonthBefore + shifts[0].MonthAfter];
                        SumParamItem[] SumYear = new SumParamItem[shifts[0].YearsBefore + shifts[0].YearsAfter];

                        dt = DateTime.FromOADate(0.0);
                        for (i = 0; i < shifts[0].MonthBefore + shifts[0].MonthAfter; i++)
                        {
                            SumMonth[i].Date = dt;
                            dt = dt.AddMonths(1);
                        }
                        dt = DateTime.FromOADate(0.0);
                        for (i = 0; i < shifts[0].YearsBefore + shifts[0].YearsAfter; i++)
                        {
                            SumYear[i].Date = dt;
                            dt = dt.AddYears(1);
                        }

                        sumParams.Add(-1, SumMonth, SumYear, shifts[0].MonthBefore, shifts[0].YearsBefore);

                        for (k = 1; k < selMerOilObjCodes.Count + 1; k++)
                        {
                            years = shifts[k].YearsBefore + shifts[k].YearsAfter;
                            month = shifts[k].MonthBefore + shifts[k].MonthAfter;

                            SumMonth = new SumParamItem[month];
                            SumYear = new SumParamItem[years];

                            dt = DateTime.FromOADate(0.0);
                            for (i = 0; i < month; i++)
                            {
                                SumMonth[i].Date = dt;
                                dt = dt.AddMonths(1);
                            }
                            dt = DateTime.FromOADate(0.0);
                            for (i = 0; i < years; i++)
                            {
                                SumYear[i].Date = dt;
                                dt = dt.AddYears(1);
                            }
                            sumParams.Add((int)selMerOilObjCodes[k - 1], SumMonth, SumYear, shifts[k].MonthBefore, shifts[k].YearsBefore);
                        }

                        int indMonth, indYears;
                        PVTParamsItem pvt;
                        int objIndex, c;
                        iLen = SumCalcWelllist.Count;
                        int mer_count = 0;
                        int[][] lastYearProd = new int[sumParams.Count][];
                        for (i = 0; i < lastYearProd.Length; i++)
                        {
                            lastYearProd[i] = new int[2];
                        }
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                this.sumParams = null;
                                return false;
                            }
                            else
                            {
                                bProd = false;
                                bInj = false;
                                bScoop = false;
                                bmonth = false;
                                bAllMonth = false;
                                bWorkTime = false;
                                bAllProd = false;
                                bAllInj = false;
                                bAllScoop = false;
                                pw = (PhantomWell)SumCalcWelllist[i];
                                if ((pw.srcWell.MerLoaded) && (pw.Caption != null) && (pw.Caption.Date != DateTime.MinValue))
                                {
                                    mer_count++;
                                    pvt = PVTParamsItem.Empty;
                                    if (pw.srcWell.AreaIndex != -1 && pw.srcWell.OilFieldIndex != -1 && MapProject.OilFields[pw.srcWell.OilFieldIndex].Areas.Count > 0)
                                    {
                                        pvt = MapProject.OilFields[pw.srcWell.OilFieldIndex].Areas[pw.srcWell.AreaIndex].pvt;
                                    }

                                    int hours;
                                    MerCompItem compItem;
                                    MerCompPlastItem plastItem;
                                    MerComp merComp = new MerComp(pw.srcWell.mer);
                                    MerWorkTimeItem wtItemProd, wtItemInj, wtItemScoop;
                                    for (j = 0; j < lastYearProd.Length; j++)
                                    {
                                        lastYearProd[j][0] = 0;
                                        lastYearProd[j][1] = 0;
                                    }

                                    for (j = 0; j < merComp.Count; j++)
                                    {
                                        compItem = merComp[j];

                                        #region заполняем по суммарному мэр
                                        SumMonth = sumParams[0].MonthlyItems;
                                        SumYear = sumParams[0].YearlyItems;
                                        if (compItem.Date < pw.Caption.Date)
                                        {
                                            yearsBefore = pw.Caption.Date.Year - compItem.Date.Year;
                                            monthBefore = (yearsBefore - 1) * 12 + (13 - compItem.Date.Month) + pw.Caption.Date.Month - 1;
                                            indMonth = shifts[0].MonthBefore - monthBefore;
                                            indYears = shifts[0].YearsBefore - yearsBefore;
                                        }
                                        else
                                        {
                                            yearsAfter = compItem.Date.Year - pw.Caption.Date.Year;
                                            monthAfter = (yearsAfter - 1) * 12 + (13 - pw.Caption.Date.Month) + compItem.Date.Month - 1;
                                            indMonth = shifts[0].MonthBefore + monthAfter;
                                            indYears = shifts[0].YearsBefore + yearsAfter;
                                        }
                                        if ((indMonth < 0) || (indYears < 0) || (indMonth >= SumMonth.Length) || (indYears >= SumYear.Length)) continue;
                                        //indMonth = (compItem.Date.Year - minDate.Year) * 12 + compItem.Date.Month - minDate.Month;

                                        wtItemProd = compItem.TimeItems.GetAllProductionTime();
                                        wtItemInj = compItem.TimeItems.GetAllInjectionTime();
                                        wtItemScoop = compItem.TimeItems.GetAllScoopTime();
                                        hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                        wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                        wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                        wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                        if (wtItemProd.WorkTime > 0)
                                        {
                                            SumMonth[indMonth].Fprod++;
                                            SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                        }

                                        if (wtItemInj.WorkTime > 0)
                                        {
                                            SumMonth[indMonth].Finj++;
                                            SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                            if (compItem.SumInjectionGas > 0) SumMonth[indMonth].WorkTimeInjGas += wtItemInj.WorkTime;
                                        }
                                        if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                        for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                        {
                                            switch (compItem.CharWorkIds[c])
                                            {
                                                case 11:
                                                    SumMonth[indMonth].Liq += compItem.SumLiquid;
                                                    SumMonth[indMonth].Oil += compItem.SumOil;
                                                    if (SumMonth[indMonth].Liq > 0)
                                                    {
                                                        SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                    }
                                                    SumMonth[indMonth].LiqV += compItem.SumLiquidV;
                                                    SumMonth[indMonth].OilV += compItem.SumOilV;
                                                    if (!pvt.IsEmpty && (compItem.SumLiquidV > 0))
                                                    {
                                                        SumMonth[indMonth].LiqVb += (compItem.SumLiquidV - compItem.SumOilV) * pvt.WaterVolumeFactor + compItem.SumOilV * pvt.OilVolumeFactor;
                                                        SumMonth[indMonth].AccumLiqVb = SumMonth[indMonth].LiqVb;
                                                        //if (indMonth > 0) SumMonth[indMonth].AccumLiqVb += SumMonth[indMonth - 1].AccumLiqVb;
                                                    }
                                                    if (SumMonth[indMonth].LiqV > 0)
                                                    {
                                                        SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                    }
                                                    break;
                                                case 12:
                                                    if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                    SumMonth[indMonth].NatGas += compItem.SumNaturalGas;
                                                    break;
                                                case 13:
                                                    SumMonth[indMonth].Scoop += compItem.SumScoop;
                                                    break;
                                                case 15:
                                                    if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                    SumMonth[indMonth].GasCondensate += compItem.SumGasCondensate;
                                                    break;
                                                case 20:
                                                    SumMonth[indMonth].Inj += compItem.SumInjection;
                                                    if (!pvt.IsEmpty && (compItem.SumInjection > 0))
                                                    {
                                                        SumMonth[indMonth].InjVb += compItem.SumInjection * pvt.WaterVolumeFactor;
                                                        SumMonth[indMonth].AccumInjVb = SumMonth[indMonth].InjVb;
                                                        //if (indMonth > 0) SumMonth[indMonth].AccumInjVb += SumMonth[indMonth - 1].AccumInjVb;
                                                    }
                                                    if (merComp.UseInjGas)
                                                    {
                                                        if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                        SumMonth[indMonth].InjGas += compItem.SumInjectionGas;
                                                    }
                                                    break;
                                            }
                                        }
                                        // по годам
                                        // indYears = compItem.Date.Year - minDate.Year;
                                        if (wtItemProd.WorkTime > 0)
                                        {
                                            if (lastYearProd[0][0] != compItem.Date.Year)
                                            {
                                                SumYear[indYears].Fprod++;
                                                lastYearProd[0][0] = compItem.Date.Year;
                                            }
                                            SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                        }
                                        if (wtItemInj.WorkTime > 0)
                                        {
                                            if (lastYearProd[0][1] != compItem.Date.Year)
                                            {
                                                SumYear[indYears].Finj++;
                                                lastYearProd[0][1] = compItem.Date.Year;
                                            }
                                            SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                            if (compItem.SumInjectionGas > 0) SumYear[indYears].WorkTimeInjGas += wtItemInj.WorkTime;
                                        }
                                        if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                        for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                        {
                                            switch (compItem.CharWorkIds[c])
                                            {
                                                case 11:
                                                    SumYear[indYears].Liq += compItem.SumLiquid;
                                                    SumYear[indYears].Oil += compItem.SumOil;
                                                    if (SumYear[indYears].Liq > 0)
                                                    {
                                                        SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                    }

                                                    SumYear[indYears].LiqV += compItem.SumLiquidV;
                                                    SumYear[indYears].OilV += compItem.SumOilV;
                                                    if (!pvt.IsEmpty && (compItem.SumLiquidV > 0))
                                                    {
                                                        SumYear[indYears].LiqVb += (compItem.SumLiquidV - compItem.SumOilV) * pvt.WaterVolumeFactor + compItem.SumOilV * pvt.OilVolumeFactor;
                                                        SumYear[indYears].AccumLiqVb = SumYear[indYears].LiqVb;
                                                        //if (indYears > 0) SumYear[indYears].AccumLiqVb += SumYear[indYears - 1].AccumLiqVb;
                                                    }
                                                    if (SumYear[indYears].LiqV > 0)
                                                    {
                                                        SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                    }
                                                    break;
                                                case 12:
                                                    if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                    SumYear[indYears].NatGas += compItem.SumNaturalGas;
                                                    break;
                                                case 13:
                                                    SumYear[indYears].Scoop += compItem.SumScoop;
                                                    break;
                                                case 15:
                                                    if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                    SumYear[indYears].GasCondensate += compItem.SumGasCondensate;
                                                    break;
                                                case 20:
                                                    SumYear[indYears].Inj += compItem.SumInjection;
                                                    if (!pvt.IsEmpty && (compItem.SumInjection > 0))
                                                    {
                                                        SumYear[indYears].InjVb += compItem.SumInjection * pvt.WaterVolumeFactor;
                                                        SumYear[indYears].AccumInjVb = SumYear[indYears].InjVb;
                                                        //if (indYears > 0) SumYear[indYears].AccumInjVb += SumYear[indYears - 1].AccumInjVb;
                                                    }
                                                    if (merComp.UseInjGas)
                                                    {
                                                        if (SumYear[indYears].InjGas < 0) SumYear[indYears].InjGas = 0;
                                                        SumYear[indYears].InjGas += compItem.SumInjectionGas;
                                                    }
                                                    break;
                                            }
                                        }
                                        #endregion

                                        #region По объектам
                                        for (k = 0; k < compItem.PlastItems.Count; k++)
                                        {
                                            plastItem = compItem.PlastItems[k];
                                            objIndex = sumParams.GetIndexByObjCode(plastItem.PlastCode);
                                            if (objIndex > -1)
                                            {
                                                SumMonth = sumParams[objIndex].MonthlyItems;
                                                SumYear = sumParams[objIndex].YearlyItems;
                                                if (compItem.Date < pw.Caption.Date)
                                                {
                                                    yearsBefore = pw.Caption.Date.Year - compItem.Date.Year;
                                                    monthBefore = (yearsBefore - 1) * 12 + (13 - compItem.Date.Month) + pw.Caption.Date.Month - 1;
                                                    indMonth = shifts[objIndex].MonthBefore - monthBefore;
                                                    indYears = shifts[objIndex].YearsBefore - yearsBefore;
                                                }
                                                else
                                                {
                                                    yearsAfter = compItem.Date.Year - pw.Caption.Date.Year;
                                                    monthAfter = (yearsAfter - 1) * 12 + (13 - pw.Caption.Date.Month) + compItem.Date.Month - 1;
                                                    indMonth = shifts[objIndex].MonthBefore + monthAfter;
                                                    indYears = shifts[objIndex].YearsBefore + yearsAfter;
                                                }
                                                if ((indMonth < 0) || (indYears < 0) || (indMonth >= SumMonth.Length) || (indYears >= SumYear.Length)) continue;

                                                wtItemProd = compItem.TimeItems.GetProductionTime(plastItem.PlastCode);
                                                wtItemInj = compItem.TimeItems.GetInjectionTime(plastItem.PlastCode);
                                                wtItemScoop = compItem.TimeItems.GetScoopTime(plastItem.PlastCode);
                                                hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                                wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                                wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                                wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                                //indMonth = (compItem.Date.Year - minMaxDate[2 * objIndex - 2].Year) * 12 + compItem.Date.Month - minMaxDate[2 * objIndex - 2].Month;

                                                if (wtItemProd.WorkTime > 0)
                                                {
                                                    SumMonth[indMonth].Fprod++;
                                                    SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                                }

                                                if (wtItemInj.WorkTime > 0)
                                                {
                                                    SumMonth[indMonth].Finj++;
                                                    SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                                    if (plastItem.InjGas > 0) SumMonth[indMonth].WorkTimeInjGas += wtItemInj.WorkTime;
                                                }

                                                if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;


                                                for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                                {
                                                    switch (compItem.CharWorkIds[c])
                                                    {
                                                        case 11:
                                                            SumMonth[indMonth].Liq += plastItem.Liq;
                                                            SumMonth[indMonth].Oil += plastItem.Oil;
                                                            if (SumMonth[indMonth].Liq > 0)
                                                            {
                                                                SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                            }

                                                            SumMonth[indMonth].LiqV += plastItem.LiqV;
                                                            SumMonth[indMonth].OilV += plastItem.OilV;
                                                            if (!pvt.IsEmpty && (plastItem.LiqV > 0))
                                                            {
                                                                SumMonth[indMonth].LiqVb += (plastItem.LiqV - plastItem.OilV) * pvt.WaterVolumeFactor + plastItem.OilV * pvt.OilVolumeFactor;
                                                                SumMonth[indMonth].AccumLiqVb = SumMonth[indMonth].LiqVb;
                                                            }
                                                            if (SumMonth[indMonth].LiqV > 0)
                                                            {
                                                                SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                            }
                                                            break;
                                                        case 12:
                                                            if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                            SumMonth[indMonth].NatGas += plastItem.NatGas;
                                                            break;
                                                        case 13:
                                                            SumMonth[indMonth].Scoop += plastItem.Scoop;
                                                            break;
                                                        case 15:
                                                            if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                            SumMonth[indMonth].GasCondensate += plastItem.Condensate;
                                                            break;
                                                        case 20:
                                                            SumMonth[indMonth].Inj += plastItem.Inj;
                                                            if (!pvt.IsEmpty && (plastItem.Inj > 0))
                                                            {
                                                                SumMonth[indMonth].InjVb += plastItem.Inj * pvt.WaterVolumeFactor;
                                                                SumMonth[indMonth].AccumInjVb = SumMonth[indMonth].InjVb;
                                                            }
                                                            if (merComp.UseInjGas)
                                                            {
                                                                if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                                SumMonth[indMonth].InjGas += plastItem.InjGas;
                                                            }
                                                            break;
                                                    }
                                                }
                                                // по годам
                                                //indYears = compItem.Date.Year - minMaxDate[2 * objIndex - 2].Year;
                                                if (wtItemProd.WorkTime > 0)
                                                {
                                                    if (lastYearProd[objIndex][0] != compItem.Date.Year)
                                                    {
                                                        SumYear[indYears].Fprod++;
                                                        lastYearProd[objIndex][0] = compItem.Date.Year;
                                                    }
                                                    SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                                }
                                                if (wtItemInj.WorkTime > 0)
                                                {
                                                    if (lastYearProd[objIndex][1] != compItem.Date.Year)
                                                    {
                                                        SumYear[indYears].Finj++;
                                                        lastYearProd[objIndex][1] = compItem.Date.Year;
                                                    }
                                                    SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                                    if (plastItem.InjGas > 0) SumYear[indYears].WorkTimeInjGas += wtItemInj.WorkTime;
                                                }
                                                if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                                for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                                {
                                                    switch (compItem.CharWorkIds[c])
                                                    {
                                                        case 11:
                                                            SumYear[indYears].Liq += plastItem.Liq;
                                                            SumYear[indYears].Oil += plastItem.Oil;
                                                            if (SumYear[indYears].Liq > 0)
                                                            {
                                                                SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                            }
                                                            SumYear[indYears].LiqV += plastItem.LiqV;
                                                            SumYear[indYears].OilV += plastItem.OilV;
                                                            if (!pvt.IsEmpty && (plastItem.LiqV > 0))
                                                            {
                                                                SumYear[indYears].LiqVb += (plastItem.LiqV - plastItem.OilV) * pvt.WaterVolumeFactor + plastItem.OilV * pvt.OilVolumeFactor;
                                                                SumYear[indYears].AccumLiqVb = SumYear[indYears].LiqVb;
                                                            }
                                                            if (SumYear[indYears].LiqV > 0)
                                                            {
                                                                SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                            }
                                                            break;
                                                        case 12:
                                                            if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                            SumYear[indYears].NatGas += plastItem.NatGas;
                                                            break;
                                                        case 13:
                                                            SumYear[indYears].Scoop += plastItem.Scoop;
                                                            break;
                                                        case 15:
                                                            if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                            SumYear[indYears].GasCondensate += plastItem.Condensate;
                                                            break;
                                                        case 20:
                                                            SumYear[indYears].Inj += plastItem.Inj;
                                                            if (!pvt.IsEmpty && (plastItem.Inj > 0))
                                                            {
                                                                SumYear[indYears].InjVb += plastItem.Inj * pvt.WaterVolumeFactor;
                                                                SumYear[indYears].AccumInjVb = SumYear[indYears].InjVb;
                                                            }
                                                            if (merComp.UseInjGas)
                                                            {
                                                                if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                                SumYear[indYears].InjGas += plastItem.InjGas;
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    pw.srcWell.mer.SetItems(null, null);
                                    pw.srcWell.mer = null;
                                }
                                userState.Element = pw.srcWell.UpperCaseName;
                                worker.ReportProgress(i + iLen, userState);
                            }
                        }
                        // Calc Accum Compensation
                        for (i = 0; i < sumParams.Count; i++)
                        {
                            for (j = 1; j < sumParams[i].MonthlyItems.Length; j++)
                            {
                                if (sumParams[i].MonthlyItems[j - 1].AccumLiqVb > 0)
                                {
                                    sumParams[i].MonthlyItems[j].AccumLiqVb += sumParams[i].MonthlyItems[j - 1].AccumLiqVb;
                                }
                                if (sumParams[i].MonthlyItems[j - 1].AccumInjVb > 0)
                                {
                                    sumParams[i].MonthlyItems[j].AccumInjVb += sumParams[i].MonthlyItems[j - 1].AccumInjVb;
                                }
                            }
                            for (j = 1; j < sumParams[i].YearlyItems.Length; j++)
                            {
                                if (sumParams[i].YearlyItems[j - 1].AccumLiqVb > 0)
                                {
                                    sumParams[i].YearlyItems[j].AccumLiqVb += sumParams[i].YearlyItems[j - 1].AccumLiqVb;
                                }
                                if (sumParams[i].YearlyItems[j - 1].AccumInjVb > 0)
                                {
                                    sumParams[i].YearlyItems[j].AccumInjVb += sumParams[i].YearlyItems[j - 1].AccumInjVb;
                                }
                            }
                        }
                        for (i = 0; i < SumCalcWelllist.Count; i++)
                        {
                            pw = (PhantomWell)SumCalcWelllist[i];
                            if (pw.srcWell.OilFieldIndex > -1)
                            {
                                of = MapProject.OilFields[pw.srcWell.OilFieldIndex];
                                if (of.MerLoaded) of.ClearMerData(false);
                            }
                        }
                        retValue = true;
                    }
                }
            }
            SumCalcWelllist.Clear();
            userState.Element = "";
            //worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool SumChessParamsReCalcByOneDate(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            bool res = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей по Шахматке на единую дату";
            userState.Element = "Выбранный список скважин";
            if ((this.selWellList.Count > 0) && (MapProject != null))
            {
                int i, j, iLen;
                DateTime minDate, maxDate;
                Well w;
                PhantomWell pw;
                OilField of;
                ChessItem item;
                ChessInjItem itemInj;

                SumCalcWelllist.Clear();
                for (i = 0; i < selWellList.Count; i++) SumCalcWelllist.Add(selWellList[i]);

                iLen = SumCalcWelllist.Count;
                worker.ReportProgress(0, userState);

                int daysBefore = -1, daysAfter = -1, maxDaysBefore = -1, maxDaysAfter = -1;

                for (i = 0; i < SumCalcWelllist.Count; i++)
                {
                    pw = (PhantomWell)SumCalcWelllist[i];
                    w = pw.srcWell;
                    userState.Element = w.UpperCaseName;
                    if (w.OilFieldIndex == -1) continue;

                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        this.sumChessParams = null;
                        return false;
                    }
                    if ((pw.Caption != null) && (pw.Caption.Date != DateTime.MinValue))
                    {
                        if ((!w.ChessLoaded) && (!w.ChessInjLoaded) && (!(MapProject.OilFields[w.OilFieldIndex]).ChessLoaded))
                        {
                            (MapProject.OilFields[w.OilFieldIndex]).LoadChessFromCache(w.Index);
                        }
                        daysBefore = -1; daysAfter = -1;
                        if ((w.ChessLoaded) && (w.chess.Count > 0))
                        {
                            if (w.chess[0].Date < pw.Caption.Date)
                            {
                                daysBefore = (pw.Caption.Date - w.chess[0].Date).Days;
                                if (maxDaysBefore < daysBefore) maxDaysBefore = daysBefore;
                            }
                            if ((daysBefore != -1) && (pw.Caption.Date < w.chess[w.chess.Count - 1].Date))
                            {
                                daysAfter = (w.chess[w.chess.Count - 1].Date - pw.Caption.Date).Days + 1;
                                if (maxDaysAfter < daysAfter) maxDaysAfter = daysAfter;
                            }
                            //w.chess.Clear();
                            //w.chess = null;
                            //w.ChessLoaded = false;
                        }

                        if ((w.ChessInjLoaded) && (w.chessInj.Count > 0))
                        {
                            if (w.chessInj[0].Date < pw.Caption.Date)
                            {
                                daysBefore = (pw.Caption.Date - w.chessInj[0].Date).Days;
                                if (maxDaysBefore < daysBefore) maxDaysBefore = daysBefore;
                            }
                            if ((daysBefore != -1) && (pw.Caption.Date < w.chessInj[w.chessInj.Count - 1].Date))
                            {
                                daysAfter = (w.chessInj[w.chessInj.Count - 1].Date - pw.Caption.Date).Days + 1;
                                if (maxDaysAfter < daysAfter) maxDaysAfter = daysAfter;
                            }
                            //w.chessInj.Clear();
                            //w.chessInj = null;
                            //w.ChessInjLoaded = false;
                        }
                    }
                    worker.ReportProgress(i, userState);
                }
                GC.GetTotalMemory(true);
                if ((maxDaysBefore != -1) && (maxDaysAfter != -1))
                {
                    int days;
                    DateTime dt;
                    days = maxDaysBefore + maxDaysAfter;
                    int indDay;
                    double predQliq;
                    DateTime predDT;

                    if (days > 0)
                    {
                        SumParamItem[] SumDay = new SumParamItem[days];

                        dt = DateTime.FromOADate(0.0);
                        for (i = 0; i < days; i++)
                        {
                            SumDay[i].Date = dt;
                            dt = dt.AddDays(1);
                        }
                        iLen = SumCalcWelllist.Count;
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                this.sumChessParams = null;
                                return false;
                            }
                            else
                            {
                                pw = (PhantomWell)SumCalcWelllist[i];
                                w = pw.srcWell;
                                if ((pw.Caption != null) && (pw.Caption.Date != DateTime.MinValue))
                                {
                                    predDT = DateTime.Now;
                                    predQliq = -1;
                                    if ((!w.ChessLoaded) && (!w.ChessInjLoaded) && (!(MapProject.OilFields[w.OilFieldIndex]).ChessLoaded))
                                    {
                                        (MapProject.OilFields[w.OilFieldIndex]).LoadChessFromCache(w.Index);
                                    }
                                    if (w.ChessLoaded)
                                    {
                                        for (j = 0; j < w.chess.Count; j++)
                                        {
                                            item = w.chess[j];
                                            if (item.Date < pw.Caption.Date)
                                            {
                                                daysBefore = (pw.Caption.Date - item.Date).Days;
                                                indDay = maxDaysBefore - daysBefore;
                                            }
                                            else
                                            {
                                                daysAfter = (item.Date - pw.Caption.Date).Days;
                                                indDay = maxDaysBefore + daysAfter;
                                            }
                                            if (indDay < SumDay.Length)
                                            {
                                                if (((predQliq == -1) || (predDT != item.Date) || (predQliq != item.Qliq + item.Qoil)) && (item.Qliq + item.Qoil > 0))
                                                {
                                                    if (item.StayTime < 24)
                                                    {
                                                        SumDay[indDay].Liq += item.Qliq;
                                                        SumDay[indDay].Oil += item.Qoil;
                                                        SumDay[indDay].LiqV += item.QliqV;
                                                        SumDay[indDay].OilV += item.QoilV;

                                                        SumDay[indDay].Fprod++;
                                                    }
                                                    predDT = item.Date;
                                                    predQliq = item.Qliq + item.Qoil;
                                                }
                                            }
                                        }
                                        w.chess.Clear();
                                        w.chess = null;
                                    }
                                    if (w.ChessInjLoaded)
                                    {
                                        if (w.chessInj.Count > 0)
                                        {
                                            int countDays = 0;
                                            for (j = 0; j < w.chessInj.Count; j++)
                                            {
                                                itemInj = w.chessInj[j];
                                                countDays = 1;
                                                if (j < w.chessInj.Count - 1)
                                                {
                                                    countDays = (w.chessInj[j + 1].Date - itemInj.Date).Days;
                                                }
                                                if (itemInj.Date < pw.Caption.Date)
                                                {
                                                    daysBefore = (pw.Caption.Date - itemInj.Date).Days;
                                                    indDay = maxDaysBefore - daysBefore;
                                                }
                                                else
                                                {
                                                    daysAfter = (itemInj.Date - pw.Caption.Date).Days;
                                                    indDay = maxDaysBefore + daysAfter;
                                                }
                                                while (countDays > 0)
                                                {
                                                    if ((indDay < SumDay.Length) && (itemInj.Wwat != -1) && (itemInj.StayTime != 24))
                                                    {
                                                        if (itemInj.StayTime < 24)
                                                        {
                                                            SumDay[indDay].Inj += itemInj.Wwat;
                                                            SumDay[indDay].Finj++;
                                                        }
                                                    }
                                                    indDay++;
                                                    countDays--;
                                                }
                                            }
                                        }
                                        w.chessInj.Clear();
                                        w.chessInj = null;
                                    }
                                }
                                userState.Element = w.UpperCaseName;
                                worker.ReportProgress(i + iLen, userState);
                            }
                        }
                        this.sumChessParams = new SumParameters(1);
                        this.sumChessParams.Add(-1, SumDay, null, maxDaysBefore, -1);
                        retValue = true;
                    }
                }
            }
            SumCalcWelllist.Clear();
            userState.Element = this.Name;
            return retValue;
        }

        public void ClearSumList()
        {
            if (sumParams != null) sumParams.Clear();
            if (sumChessParams != null) sumChessParams.Clear();
            if (sumTable81 != null) sumTable81.Clear();
            if (sumBP != null) sumBP.Clear();
            BPParams = null;
            sumIndexes.Clear();
        }

        public void AddSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumParameters SumParams)
        {
            if ((SumParams == null) || (SumParams.Count == 0)) return;
            if (this.sumParams == null) this.sumParams = new SumParameters(SumParams.Count);
            for (int i = 0; i < SumParams.Count; i++)
            {
                this.sumParams.AddMonthSumParams(SumParams[i].OilObjCode, SumParams[i].MonthlyItems);
                this.sumParams.AddYearSumParams(SumParams[i].OilObjCode, SumParams[i].YearlyItems);
            }
        }
        public void AddSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumObjParameters ObjSumParams)
        {
            if ((ObjSumParams == null) || (ObjSumParams.MonthlyItems == null)) return;
            if (this.sumParams == null) this.sumParams = new SumParameters(1);
            this.sumParams.AddMonthSumParams(ObjSumParams.OilObjCode, ObjSumParams.MonthlyItems);
            this.sumParams.AddYearSumParams(ObjSumParams.OilObjCode, ObjSumParams.YearlyItems);
        }
        public void SubSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumParameters SumParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((SumParams == null) || (SumParams.Count == 0) || (this.sumParams == null) || (TrimMaxDate < TrimMinDate)) return;
            for (int i = 0; i < SumParams.Count; i++)
            {
                this.sumParams.SubMonthSumParams(SumParams[i].OilObjCode, SumParams[i].MonthlyItems, TrimMinDate, TrimMaxDate);
                this.sumParams.SubYearSumParams(SumParams[i].OilObjCode, SumParams[i].YearlyItems, TrimMinDate, TrimMaxDate);
            }
        }
        public void SubSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumObjParameters ObjSumParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((ObjSumParams == null) || (ObjSumParams.MonthlyItems == null) || (this.sumParams == null) || (TrimMaxDate < TrimMinDate)) return;
            this.sumParams.SubMonthSumParams(ObjSumParams.OilObjCode, ObjSumParams.MonthlyItems, TrimMinDate, TrimMaxDate);
            this.sumParams.SubYearSumParams(ObjSumParams.OilObjCode, ObjSumParams.YearlyItems, TrimMinDate, TrimMaxDate);
        }

        public void AddChessSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumParameters SumParams)
        {
            if ((SumParams == null) || (SumParams.Count == 0)) return;
            if (this.sumChessParams == null) this.sumChessParams = new SumParameters(SumParams.Count);
            for (int i = 0; i < SumParams.Count; i++)
            {
                this.sumChessParams.AddDaySumParams(SumParams[i].OilObjCode, SumParams[i].MonthlyItems);
            }
        }
        public void AddChessSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumObjParameters ObjSumParams)
        {
            if ((ObjSumParams == null) || (ObjSumParams.MonthlyItems == null)) return;
            if (this.sumChessParams == null) this.sumChessParams = new SumParameters(1);
            this.sumChessParams.AddDaySumParams(ObjSumParams.OilObjCode, ObjSumParams.MonthlyItems);
        }
        public void SubChessSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumParameters SumParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((SumParams == null) || (SumParams.Count == 0) || (this.sumChessParams == null) || (TrimMaxDate < TrimMinDate)) return;
            for (int i = 0; i < SumParams.Count; i++)
            {
                this.sumChessParams.SubMonthSumParams(SumParams[i].OilObjCode, SumParams[i].MonthlyItems, TrimMinDate, TrimMaxDate);
            }
        }
        public void SubChessSumParams(BackgroundWorker worker, DoWorkEventArgs e, SumObjParameters ObjSumParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((ObjSumParams == null) || (ObjSumParams.MonthlyItems == null) || (this.sumChessParams == null) || (TrimMaxDate < TrimMinDate)) return;
            this.sumChessParams.SubMonthSumParams(ObjSumParams.OilObjCode, ObjSumParams.MonthlyItems, TrimMinDate, TrimMaxDate);
        }

        public void AddTable81Params(BackgroundWorker worker, DoWorkEventArgs e, SumParamItem[] ObjSumParams)
        {
            if ((ObjSumParams == null) || (ObjSumParams.Length == 0)) return;
            if (this.sumTable81 == null) this.sumTable81 = new SumParameters(1);
            this.sumTable81.AddYearSumParams(-1, ObjSumParams);
        }
        public void SubTable81Params(BackgroundWorker worker, DoWorkEventArgs e, SumParamItem[] ObjSumParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((ObjSumParams == null) || (ObjSumParams.Length == 0) || (this.sumTable81 == null) || (TrimMaxDate < TrimMinDate)) return;
            this.sumTable81.SubYearSumParams(-1, ObjSumParams, TrimMinDate, TrimMaxDate);
        }

        public void AddBPParams(BackgroundWorker worker, DoWorkEventArgs e, OilfieldBizplan BizPlan, double[] BizPlanParams)
        {
            if (BizPlan == null) return;
            if (this.sumBP == null) this.sumBP = new SumParameters(1);
            SumParamItem[] items = new SumParamItem[BizPlan.MonthLevels.Length];
            DateTime dt = BizPlan.StartDate;
            for (int i = 0; i < BizPlan.MonthLevels.Length; i++)
            {
                items[i] = new SumParamItem();
                items[i].Date = dt;
                items[i].Liq = BizPlan.MonthLevels[i].Liquid;
                items[i].Oil = BizPlan.MonthLevels[i].Oil;
                items[i].Inj = BizPlan.MonthLevels[i].Injection;
                dt = dt.AddMonths(1);
            }
            this.sumBP.AddMonthSumParams(-1, items);
            if (BizPlanParams != null)
            {
                if (BPParams == null)
                {
                    BPParams = new double[5];
                    BPParams[0] = BizPlanParams[0];
                    BPParams[1] = BizPlanParams[1];
                    BPParams[2] = BizPlanParams[2];
                    BPParams[3] = BizPlanParams[3];
                    BPParams[4] = BizPlanParams[4];
                }
                else if (BizPlanParams[0] == BPParams[0])
                {
                    double d = BPParams[1] * 100 / BPParams[3];
                    d += BizPlanParams[1] * 100 / BizPlanParams[3];
                    BPParams[1] += BizPlanParams[1];
                    BPParams[3] = BPParams[1] * 100 / d;

                    d = BPParams[2] / (BPParams[4] / 100);
                    d += BizPlanParams[2] / (BizPlanParams[4] / 100);
                    BPParams[2] += BizPlanParams[2];
                    BPParams[4] = BPParams[2] * 100 / d;
                }
            }
        }
        public void SubBPParams(BackgroundWorker worker, DoWorkEventArgs e, OilfieldBizplan BizPlan, double[] BizPlanParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((BizPlan == null) || (BizPlan.MonthLevels == null) || (this.sumBP == null) || (TrimMaxDate < TrimMinDate)) return;
            SumParamItem[] items = new SumParamItem[BizPlan.MonthLevels.Length];
            DateTime dt = BizPlan.StartDate;
            for (int i = 0; i < BizPlan.MonthLevels.Length; i++)
            {
                items[i] = new SumParamItem();
                items[i].Date = dt;
                items[i].Liq = BizPlan.MonthLevels[i].Liquid;
                items[i].Oil = BizPlan.MonthLevels[i].Oil;
                items[i].Inj = BizPlan.MonthLevels[i].Injection;
                dt = dt.AddMonths(1);
            }
            this.sumBP.SubYearSumParams(-1, items, TrimMinDate, TrimMaxDate);
            if (BizPlanParams != null && BPParams != null && BizPlanParams[0] == BPParams[0])
            {
                double d = BPParams[1] * 100 / BPParams[3];
                d -= BizPlanParams[1] * 100 / BizPlanParams[3];
                BPParams[1] -= BizPlanParams[1];
                BPParams[3] = BPParams[1] * 100 / d;

                d = BPParams[2] / (BPParams[4] / 100);
                d -= BizPlanParams[2] / (BizPlanParams[4] / 100);
                BPParams[2] -= BizPlanParams[2];
                BPParams[4] = BPParams[2] * 100 / d;
            }
        }
        #endregion

        // CONTOURS
        public void ExportAreasContours(BackgroundWorker worker, DoWorkEventArgs e, string destPath)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Экспорт контуров ячеек заводнения";
            userState.WorkCurrentTitle = "Экспорт контуров ячеек заводнения";
            userState.Element = "";
            C2DLayer layer;
            Contour cntr;
            OilField of;
            int i, count = 1;
            if (selLayerList.Count > 0)
            {
                for (i = 0; i < selLayerList.Count; i++)
                {
                    layer = (C2DLayer)selLayerList[i];
                    if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) ||
                        (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
                        cntr = (Contour)layer.ObjectsList[0];
                    else continue;

                    if (cntr._ContourType == -1)
                    {
                        for (i = 0; i < MapProject.OilFields.Count; i++)
                        {
                            of = MapProject.OilFields[i];
                            if (of.ParamsDict.EnterpriseCode == cntr._EnterpriseCode)
                            {
                                userState.Element = of.Name;
                                if (!Directory.Exists(destPath + "\\" + of.ParamsDict.NGDUName))
                                    Directory.CreateDirectory(destPath + "\\" + of.ParamsDict.NGDUName);
                                of.ExportAreasContours(destPath + "\\" + of.ParamsDict.NGDUName);
                                worker.ReportProgress(count++, userState);
                            }
                        }
                    }
                    else if (cntr._ContourType == -2)
                    {
                        for (i = 0; i < MapProject.OilFields.Count; i++)
                        {
                            of = MapProject.OilFields[i];
                            if (of.ParamsDict.NGDUCode == cntr._NGDUCode)
                            {
                                userState.Element = of.Name;
                                of.ExportAreasContours(destPath);
                                worker.ReportProgress(count++, userState);
                            }
                        }
                    }
                    else if (cntr._ContourType == -3)
                    {
                        for (i = 0; i < MapProject.OilFields.Count; i++)
                        {
                            of = MapProject.OilFields[i];
                            if (of.OilFieldCode == cntr._OilFieldCode)
                            {
                                userState.Element = of.Name;
                                of.ExportAreasContours(destPath);
                                worker.ReportProgress(count++, userState);
                                break;
                            }
                        }
                    }
                }
            }
        }
        public void ExportCondContours(BackgroundWorker worker, DoWorkEventArgs e, string destPath)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Экспорт условных контуров месторождений";
            userState.WorkCurrentTitle = "Экспорт условных контуров месторождений";
            userState.Element = "";
            OilField of;
            int i, count = 1;
            string outFileName = destPath + "\\" + "Контуры.kts";
            if (File.Exists(outFileName)) File.Delete(outFileName);


            for (i = 0; i < MapProject.OilFields.Count; i++)
            {
                of = MapProject.OilFields[i];
                userState.Element = of.Name;
                of.ExportCondContours(outFileName);
                worker.ReportProgress(count++, userState);
            }
        }

        // Edited SK Mode
        public void ResetSK(int indexLayer)
        {

            C2DLayer layer = (C2DLayer)LayerList[indexLayer];
            if (layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.IMAGE)
            {
                OilfieldCoefItem oldCoef = new OilfieldCoefItem(layer.oilfield.CoefDict);
                layer.oilfield.CoefDict.ShX -= layer.ObjRect.Left;
                layer.oilfield.CoefDict.ShY -= layer.ObjRect.Top;
                layer.ReCalcCoord(oldCoef);
            }
            else
            {
                ((Image)(layer.ObjectsList[0])).dx = 0;
                ((Image)(layer.ObjectsList[0])).dy = 0;
            }
            DrawLayerList();
        }
        public void TransferOilField(int indexOilField, double dX, double dY, bool SaveFiles)
        {
            OilField of = MapProject.OilFields[indexOilField];
            C2DLayer layer, ofLayer;
            OilfieldCoefItem oldCoef = new OilfieldCoefItem(of.CoefDict);
            int i, j, k;
            int kx = 1, ky = 1;
            if (dX < 0) kx = -1;
            if (dY < 0) ky = -1;
            double dRealX = C2DObject.RealXfromScreenX(Math.Abs(dX) - BitMapRect.Left);
            double dRealY = C2DObject.RealYfromScreenY(Math.Abs(dY) - BitMapRect.Top);
            of.CoefDict.ShX += kx * dRealX;
            of.CoefDict.ShY += ky * dRealY;

            for (i = 0; i < LayerList.Count; i++)
            {
                ofLayer = ((C2DLayer)LayerList[i]).GetLayerByOfIndex(indexOilField);

                if (ofLayer != null)
                {
                    List<C2DLayer> list = new List<C2DLayer>();
                    ofLayer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list);
                    if (list.Count > 0)
                    {
                        layer = (C2DLayer)list[0];
                        layer.ReCalcCoord(oldCoef);
                        list.Clear();
                    }
                    ofLayer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL, list);
                    if (list.Count > 0)
                    {
                        layer = (C2DLayer)list[0];
                        layer.ReCalcCoord(oldCoef);
                        list.Clear();
                    }

                    ofLayer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.CONTOUR, list);
                    for (j = 0; j < list.Count; j++)
                    {
                        layer = (C2DLayer)list[j];
                        layer.ReCalcCoord(oldCoef);
                    }
                    list.Clear();
                    ofLayer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.AREA, list);
                    for (j = 0; j < list.Count; j++)
                    {
                        layer = (C2DLayer)list[j];
                        layer.ReCalcCoord(oldCoef);
                    }
                    list.Clear();
                    //ofLayer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR, list);
                    //for (j = 0; j < list.Count; j++)
                    //{
                    //    layer = (C2DLayer)list[j];
                    //    layer.ReCalcCoord(oldCoef);
                    //}
                    list.Clear();
                    ofLayer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL_PAD, list);
                    for (j = 0; j < list.Count; j++)
                    {
                        layer = (C2DLayer)list[j];
                        layer.ReCalcCoord(oldCoef);
                    }
                    list.Clear();
                    break;
                }
            }
            if (SaveFiles)
            {
                WorkObject job;
                job.WorkType = Constant.BG_WORKER_WORK_TYPE.SAVE_OF_AREAS_TO_CACHE;
                job.DataObject = of;
                job.ObjectIndex = -1;
                mainForm.worker.Queue.Add(job);
                job.WorkType = Constant.BG_WORKER_WORK_TYPE.SAVE_OF_CONTOUR_TO_CACHE;
                job.DataObject = of;
                job.ObjectIndex = -1;
                mainForm.worker.Queue.Add(job);
                mainForm.worker.Run();
            }
        }
        public void TransferLayer(C2DLayer layer, double dX, double dY)
        {
            if (dX < -2000) dX = -2000; else if (dX > 2000) dX = 2000;
            if (dY < -2000) dY = -2000; else if (dY > 2000) dY = 2000;

            if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL) ||
                (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL))
            {
                int kx = 1, ky = 1;
                if (dX < 0) kx = -1;
                if (dY < 0) ky = -1;
                double dRealX = C2DObject.RealXfromScreenX(Math.Abs(dX) - BitMapRect.Left);
                double dRealY = C2DObject.RealYfromScreenY(Math.Abs(dY) - BitMapRect.Top);
                OilfieldCoefItem oldCoef = new OilfieldCoefItem(layer.oilfield.CoefDict);
                layer.oilfield.CoefDict.ShX += kx * dRealX;
                layer.oilfield.CoefDict.ShY += ky * dRealY;
                layer.ReCalcCoord(oldCoef);
            }
            else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
            {
                Image img = (Image)layer.ObjectsList[0];
                float xDpicurr = img.xDpi / (float)(C2DObject.st_XUnitsInOnePixel);
                float yDpicurr = img.yDpi / (float)(C2DObject.st_YUnitsInOnePixel);
                img.dx += dX * C2DObject.st_XUnitsInOnePixel;
                img.dy += dY * C2DObject.st_YUnitsInOnePixel;
            }
            else if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) ||
                     (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR))
            {
                int kx = 1, ky = 1;
                if (dX < 0) kx = -1;
                if (dY > 0) ky = -1;
                layer.oilfield.ContoursEdited = true;
                double dRealX = Math.Abs(dX) * C2DObject.st_XUnitsInOnePixel;
                double dRealY = Math.Abs(dY) * C2DObject.st_YUnitsInOnePixel;
                /*
                double dRealX = C2DObject.RealXfromScreenX(Math.Abs(dX) - BitMapRect.Left);
                double dRealY = C2DObject.RealYfromScreenY(Math.Abs(dY) - BitMapRect.Top);
                */
                ((Contour)layer.ObjectsList[0]).X += kx * dRealX;
                ((Contour)layer.ObjectsList[0]).Y += ky * dRealY;
            }
        }
        public void RotateOilField(int OilFieldIndex)
        {
            OilField of = MapProject.OilFields[OilFieldIndex];
            C2DLayer layer;
            OilfieldCoefItem oldCoeffParams = new OilfieldCoefItem(of.CoefDict);

            double x1, y1, x2, y2, x0, y0, alpha;
            double xL, yL, xG, yG, dx, dy;
            double k1, k2;
            bool res1, res2;
            x0 = EditedLayerRotateCenter.X;
            y0 = EditedLayerRotateCenter.Y;
            x1 = MButtonPoint.X;
            y1 = MButtonPoint.Y;
            x2 = MButtonTimerPoint.X;
            y2 = MButtonTimerPoint.Y;

            k1 = (y1 - y0) / (x1 - x0);
            k2 = (y2 - y0) / (x2 - x0);
            alpha = Math.Acos(Math.Abs(k1 * k2 + 1) / (Math.Sqrt(1 + k1 * k1) * Math.Sqrt(1 + k2 * k2)));

            res1 = y2 > (x2 - x0) * (y1 - y0) / (x1 - x0) + y0;
            if (x1 > x0) res1 = !res1;

            if (y1 != y0)
            {
                res2 = y2 < -(x2 - x0) / k1 + y0;
                if (y1 < y0) res2 = !res2;
            }
            else
            {
                res2 = (x2 < x0) && (x0 < x1);
            }


            if (res1)
            {
                if (res2) alpha = Math.PI - alpha;
            }
            else
            {
                if (res2) alpha = Math.PI - alpha;
                alpha = -alpha;
            }
            x0 = C2DObject.RealXfromScreenX(x0);
            y0 = C2DObject.RealYfromScreenY(y0);

            of.CoefDict.LocalCoordFromGlobal(x0, y0, out xL, out yL);

            dx = of.CoefDict.ShX;
            dy = of.CoefDict.ShY;

            OilfieldCoefItem RotateCoef = new OilfieldCoefItem();
            RotateCoef.KfX_Xnew = Math.Cos(alpha);
            RotateCoef.KfX_Ynew = -Math.Sin(alpha);
            RotateCoef.KfY_Xnew = Math.Sin(alpha);
            RotateCoef.KfY_Ynew = Math.Cos(alpha);
            RotateCoef.ShX = -xL * Math.Cos(alpha) + yL * Math.Sin(alpha) + xL;
            RotateCoef.ShY = -xL * Math.Sin(alpha) - yL * Math.Cos(alpha) + yL;


            RotateCoef.GlobalCoordFromLocal(xL, yL, out xG, out yG);

            RotateCoef.ShX += dx;
            RotateCoef.ShY += dy;

            RotateCoef.GlobalCoordFromLocal(xL, yL, out xG, out yG);

            of.CoefDict = RotateCoef;

            for (int i = 0; i < LayerList.Count; i++)
            {
                layer = (C2DLayer)LayerList[i];
                if (layer.oilFieldIndex == of.Index)
                {
                    layer.ReCalcCoord(alpha, x0, y0, oldCoeffParams);
                }
            }

        }
        public void RotateLayer(C2DLayer layer)
        {
            if (layer.TypeID != Constant.BASE_OBJ_TYPES_ID.IMAGE)
            {
                double x1, y1, x2, y2, x0, y0, alpha;
                double k1, k2;
                bool res1, res2;
                x0 = EditedLayerRotateCenter.X;
                y0 = EditedLayerRotateCenter.Y;
                x1 = MButtonPoint.X;
                y1 = MButtonPoint.Y;
                x2 = MButtonTimerPoint.X;
                y2 = MButtonTimerPoint.Y;

                k1 = (y1 - y0) / (x1 - x0);
                k2 = (y2 - y0) / (x2 - x0);
                alpha = Math.Acos(Math.Abs(k1 * k2 + 1) / (Math.Sqrt(1 + k1 * k1) * Math.Sqrt(1 + k2 * k2)));

                res1 = y2 > (x2 - x0) * (y1 - y0) / (x1 - x0) + y0;
                if (x1 > x0) res1 = !res1;

                if (y1 != y0)
                {
                    res2 = y2 < -(x2 - x0) / k1 + y0;
                    if (y1 < y0) res2 = !res2;
                }
                else
                {
                    res2 = (x2 < x0) && (x0 < x1);
                }


                if (res1)
                {
                    if (res2) alpha = Math.PI - alpha;
                }
                else
                {
                    if (res2) alpha = Math.PI - alpha;
                    alpha = -alpha;
                }
                x0 = C2DObject.RealXfromScreenX(x0);
                y0 = C2DObject.RealYfromScreenY(y0);
                layer.ReCalcCoord(alpha, x0, y0, null);
            }
        }
        public void ResetRotate()
        {
            EditedLayerRotate = false;
            EditedLayerRotateCenter.X = -1;
            DrawLayerList();
        }

        // SERVICE FUNC
        public void FillLayerList(C2DLayer layer, ArrayList list)
        {
            if ((list != null) && (layer != null))
            {
                switch (layer.ObjTypeID)
                {
                    //case Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION:
                    //    list.Add(layer);
                    //    break;
                    case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                        list.Add(layer);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.AREA:
                        list.Add(layer);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.GRID:
                        list.Add(layer);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                        list.Add(layer);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                        list.Add(layer);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.WELL:
                        list.Add(layer);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                        list.Add(layer);
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.LAYER:
                        if (layer.ObjectsList.Count > 0)
                        {
                            if ((((C2DLayer)layer.ObjectsList[layer.ObjectsList.Count - 1]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR) ||
                                ((C2DLayer)layer.ObjectsList[layer.ObjectsList.Count - 1]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONSTRUCTION)
                            {
                                list.Add(layer);
                            }
                            int i, j;
                            C2DLayer lr;
                            for (i = 0; i < layer.ObjectsList.Count; i++)
                            {
                                lr = (C2DLayer)layer.ObjectsList[i];
                                FillLayerList(lr, list);
                                if ((lr.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                    (lr.GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
                                {
                                    j = 0;
                                    while (j < lr.ObjectsList.Count)
                                    {
                                        if ((((C2DLayer)lr.ObjectsList[j]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                            ((C2DLayer)lr.ObjectsList[j]).GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                                            lr.ObjectsList.RemoveAt(j);
                                        else
                                            j++;
                                    }

                                }
                            }
                        }
                        break;
                }
            }
        }
        void CheckStateNode(TreeNode SourceNode, TreeNode TargetNode)
        {
            if (SourceNode.Nodes.Count == TargetNode.Nodes.Count)
            {
                for (int i = 0; i < SourceNode.Nodes.Count; i++)
                    CheckStateNode(SourceNode.Nodes[i], TargetNode.Nodes[i]);
            }
            TargetNode.Checked = SourceNode.Checked;
        }
        void CopyTreeNodeRecursive(TreeNode SourceNode, TreeNode TargetNode)
        {
            if ((SourceNode != null) && (TargetNode != null))
            {
                if (SourceNode.Nodes.Count > 0)
                {
                    TreeNode tn;
                    for (int i = 0; i < SourceNode.Nodes.Count; i++)
                    {
                        tn = TargetNode.Nodes.Add(SourceNode.Nodes[i].Text);
                        tn.ContextMenuStrip = twActiveOilField.cMenuSelect;
                        CopyTreeNodeRecursive(SourceNode.Nodes[i], tn);
                        tn.Checked = SourceNode.Nodes[i].Checked;
                        tn.ImageIndex = SourceNode.Nodes[i].ImageIndex;
                        tn.SelectedImageIndex = SourceNode.Nodes[i].SelectedImageIndex;
                        tn.Tag = SourceNode.Nodes[i].Tag;
                        if ((SourceNode.Nodes[i].Tag != null) && ((BaseObj)SourceNode.Nodes[i].Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                        {
                            ((C2DLayer)SourceNode.Nodes[i].Tag).node = SourceNode.Nodes[i];
#if DEBUG
                            C2DLayer layer = (C2DLayer)tn.Tag;
                            if (tn.TreeView == twActiveOilField && layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR && layer.ObjectsList.Count == 1 && ((Contour)layer.ObjectsList[0])._ContourType == -3)
                            {
                                tn.ContextMenuStrip = twActiveOilField.cMenuContour;
                            }
#endif
                        }
                    }
                }
                else
                {
                    TargetNode.Text = SourceNode.Text;
                    TargetNode.Tag = SourceNode.Tag;
                    TargetNode.ImageIndex = SourceNode.ImageIndex;
                    TargetNode.SelectedImageIndex = SourceNode.SelectedImageIndex;
                    TargetNode.Checked = SourceNode.Checked;
                    if ((SourceNode.Tag != null) && ((BaseObj)SourceNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                    {
                        ((C2DLayer)SourceNode.Tag).node = SourceNode;
                    }
                }
            }
        }
        public void RemoveAliasByName(TreeNode ofNode, string AliasName)
        {
            if ((ofNode != null) && (ofNode.Nodes.Count > 0) && (ofNode.Tag != null))
            {
                TreeNode tnAlias, tn;
                tnAlias = ofNode.Nodes[ofNode.Nodes.Count - 1];
                if (tnAlias.Text.StartsWith("Псевдонимы"))
                {
                    tn = null;
                    for (int i = 0; i < tnAlias.Nodes.Count; i++)
                    {
                        if (tnAlias.Nodes[i].Text == AliasName)
                        {
                            tn = tnAlias.Nodes[i];
                            break;
                        }
                    }
                    if (tn != null)
                    {
                        ofNode.TreeView.BeginUpdate();
                        tnAlias.Nodes.Remove(tn);
                        if (tnAlias.Nodes.Count == 0) ofNode.Nodes.Remove(tnAlias);
                        else
                        {
                            tnAlias.Text = "Псевдонимы [" + tnAlias.Nodes.Count.ToString() + "]";
                        }
                        ofNode.TreeView.EndUpdate();
                    }
                }

            }
        }

        public void SetAreasVisible(bool AreasVisible)
        {
            int i, j;
            List<C2DLayer> list = new List<C2DLayer>();
            C2DLayer layer;
            C2DObject.ShowAreasMode = AreasVisible;

            for (i = 0; i < LayerList.Count; i++)
            {
                if (((C2DLayer)LayerList[i]).Visible)
                {
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.AREA, list, false);
                }
            }
            for (i = 0; i < list.Count; i++)
            {
                layer = (C2DLayer)list[i];
                if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.AREA)
                {
                    for (j = 0; j < layer.ObjectsList.Count; j++)
                    {
                        ((Area)layer.ObjectsList[j]).ShowAreasMode = AreasVisible;
                    }
                }
            }
            DrawLayerList();
        }
        public void SetContourColorMode(bool ColorByObject)
        {
            int i, j;
            List<C2DLayer> list = new List<C2DLayer>();
            C2DLayer layer;
            for (i = 0; i < LayerList.Count; i++)
            {
                if (((C2DLayer)LayerList[i]).Visible)
                {
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.CONTOUR, list, false);
                }
            }
            for (i = 0; i < list.Count; i++)
            {
                layer = (C2DLayer)list[i];
                if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    for (j = 0; j < layer.ObjectsList.Count; j++)
                    {
                        ((Contour)layer.ObjectsList[j]).ColorMode = ColorByObject;
                        ((Contour)layer.ObjectsList[j]).ResetBrush();
                    }
                }
            }
            DrawLayerList();
        }
        public void SetGrayIconsMode(bool GrayIconsMode)
        {
            if (MapProject != null)
            {
                this.GrayWellIconsMode = GrayIconsMode;
                OilField of;
                Well w;
                bool bGray;
                for (int i = 1; i < MapProject.OilFields.Count; i++)
                {
                    of = MapProject.OilFields[i];
                    bGray = true;
                    if ((of.BubbleMapLoaded) && (this.BubbleMapType == Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM))
                    {
                        for (int j = 0; j < of.Wells.Count; j++)
                        {
                            w = of.Wells[j];
                            if (w.BubbleList != null)
                            {
                                if (w.BubbleList.Visible) bGray = false;
                                break;
                            }
                        }
                    }
                    if (bGray) of.SetDefaultColorWellIcons(!GrayIconsMode);
                }
            }
            DrawLayerList();
        }

        // LOAD INSPECTOR
        public void SortByOilObjHierarchy(TreeNode rootNode)
        {
            var stratumDict = (StratumDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            int i, j, ind;
            TreeNode copyNode, parNode, tn;
            C2DLayer lr, lr2, lrParent;
            lrParent = (C2DLayer)rootNode.Tag;
            int plastCode, x, min;
            StratumTreeNode node;

            if (rootNode.Nodes.Count > 1 && rootNode.Nodes[0].Tag != null && ((C2DLayer)rootNode.Nodes[0].Tag).GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
            {
                // сортировка текущих объектов
                for (i = 0; i < rootNode.Nodes.Count; i++)
                {
                    tn = rootNode.Nodes[i];

                    if (tn.Tag != null)
                    {
                        lr = (C2DLayer)tn.Tag;
                    }
                    else
                    {
                        continue;
                    }

                    node = stratumDict.GetStratumTreeNode(lr.Code);
                    if (node == null) continue;
                    x = -1; min = -1;
                    for (j = 0; j < rootNode.Nodes.Count; j++)
                    {
                        if (i != j)
                        {
                            ind = -1;
                            if (rootNode.Nodes[j].Tag != null)
                            {
                                lr2 = (C2DLayer)rootNode.Nodes[j].Tag;
                            }
                            else
                            {
                                continue;
                            }
                            ind = node.GetParentLevelByCode(lr2.Code);
                            if (ind != -1)
                            {
                                if ((min == -1) || (min > ind))
                                {
                                    x = j;
                                    min = ind;
                                }
                            }
                        }
                    }
                    if (x != -1)
                    {
                        parNode = rootNode.Nodes[x];
                        copyNode = (TreeNode)tn.Clone();

                        copyNode.ContextMenuStrip = twLayers.cMenuSelect;
                        if ((parNode.Nodes.Count == 1) && (parNode.Nodes[0].Text == "Empty"))
                        {
                            parNode.Expand();
                            parNode.Collapse();
                        }

                        lr = (C2DLayer)copyNode.Tag;
                        if (lr.Index < lrParent.ObjectsList.Count)
                        {
                            lrParent.ObjectsList.RemoveAt(lr.Index);
                            if (lrParent.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                            {
                                for (j = 0; j < lrParent.ObjectsList.Count; j++)
                                {
                                    ((C2DLayer)lrParent.ObjectsList[j]).Index = j;
                                }
                            }
                        }
                        int k = 0;
                        foreach (TreeNode tnode in parNode.Nodes)
                        {
                            if (tnode.Nodes.Count == 0)
                            {
                                k = tnode.Index;
                                break;
                            }
                        }

                        ((C2DLayer)parNode.Tag).Insert(k, lr);
                        parNode.Nodes.Insert(k, copyNode);

                        for (j = 0; j < ((C2DLayer)parNode.Tag).ObjectsList.Count; j++)
                        {
                            ((C2DLayer)((C2DLayer)parNode.Tag).ObjectsList[j]).Index = j;
                        }
                        CheckStateNode(tn, copyNode);
                        tn.Tag = null;
                        copyNode.Checked = false;
                        lr.Visible = false;
                    }
                }
                i = 0;
                while (i < rootNode.Nodes.Count)
                {
                    if (rootNode.Nodes[i].Tag == null)
                        rootNode.Nodes.RemoveAt(i);
                    else
                        i++;
                }
                rootNode.Text = ((C2DLayer)rootNode.Tag).Name + " [" + rootNode.Nodes.Count.ToString() + "]";
            }
            else if (rootNode.Nodes.Count > 1 && rootNode.Nodes[0].Tag != null && ((C2DLayer)rootNode.Nodes[0].Tag).GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.DEPOSIT)
            {
                Deposit dep;
                C2DLayer stratumLayer = null, layer = null;
                TreeNode stratumNode = null;
                tn = null;
                int predParentStratum = -1;
                List<C2DLayer> stratumLayers = new List<C2DLayer>();
                List<TreeNode> stratumNodes = new List<TreeNode>();

                for (i = 0; i < rootNode.Nodes.Count; i++)
                {
                    if (rootNode.Nodes[i].Tag == null) continue;
                    layer = (C2DLayer)rootNode.Nodes[i].Tag;
                    dep = (Deposit)layer.ObjectsList[0];
                    if (predParentStratum != dep.StratumParentCode)
                    {
                        if (stratumLayer != null)
                        {
                            stratumLayer.fontSize = this._fontSize;
                            stratumLayer.ResetFontsPens();
                            stratumLayer.GetObjectsRect();
                        }
                        stratumLayer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, dep.OilfieldIndex);

                        stratumLayer.oilfield = MapProject.OilFields[dep.OilfieldIndex];
                        stratumLayer.Name = stratumDict.GetShortNameByCode(dep.StratumParentCode);
                        stratumLayer.Visible = false;

                        stratumNode = new TreeNode(stratumLayer.Name, 24, 24);
                        stratumNode.Tag = stratumLayer;
                        stratumLayer.node = stratumNode;
                        stratumLayers.Add(stratumLayer);
                        stratumNodes.Add(stratumNode);
                        stratumNode.Checked = stratumLayer.Visible;

                        predParentStratum = dep.StratumParentCode;
                    }
                    tn = new TreeNode(layer.Name, 24, 24);
                    tn.Tag = layer;
                    layer.node = tn;
                    if (stratumLayer.ObjectsList.Count == 0) layer.Visible = true;
                    tn.Checked = layer.Visible;
                    stratumLayer.Add(layer);
                    stratumNode.Nodes.Add(tn);
                }

                if (stratumLayer != null)
                {
                    stratumLayer.fontSize = this._fontSize;
                    stratumLayer.ResetFontsPens();
                    stratumLayer.GetObjectsRect();
                    stratumLayers.Add(stratumLayer);
                }
                rootNode.Nodes.Clear();
                rootNode.Nodes.AddRange(stratumNodes.ToArray());

                layer = (C2DLayer)rootNode.Tag;
                layer.ObjectsList.Clear();
                layer.ObjectsList.AddRange(stratumLayers.ToArray());
                layer.ResetFontsPens();
                layer.GetObjectsRect();
            }
        }
        public void SortByAreaGridTypes(TreeNode rootNode)
        {
            var oilfieldAreaDict = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            var gridTypeDict = (DataDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.GRID_TYPE);
            int i, j, ind;
            C2DLayer lr, lrParent, layerArea, layerGridType;

            //TreeNode copyNode, parNode, tn;
            //C2DLayer lr, lr2, lrParent;
            Grid grid;
            lrParent = (C2DLayer)rootNode.Tag;

            if (rootNode.Nodes.Count > 1 && rootNode.Nodes[0].Tag != null && ((C2DLayer)rootNode.Nodes[0].Tag).GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.GRID)
            {
                
                TreeNode tnArea, tnGridType, tn;

                int ind2, areaCode = -1;
                bool byAreas = false;
                List<int> ofAreasCodes = new List<int>();
                List<List<int>> gridTypesCodes = new List<List<int>>();
                List<List<List<C2DLayer>>> ofAreasLayers = new List<List<List<C2DLayer>>>();
                int counter = 0, counter2 = 0;

                // сортировка по площадям месторождения и типам гридов
                for (i = 0; i < rootNode.Nodes.Count; i++)
                {
                    tn = rootNode.Nodes[i];
                    if (tn.Tag == null) continue;

                    lr = (C2DLayer)tn.Tag;
                    if (lr.ObjectsList.Count > 0 && lr.ObjectsList[0].TypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                    {
                        grid = (Grid)lr.ObjectsList[0];
                        counter++;
                        areaCode = oilfieldAreaDict.GetCodeByShortName(grid.Name);
                        ind = ofAreasCodes.IndexOf(areaCode);
                        if (ind == -1)
                        {
                            ind = ofAreasCodes.Count;
                            ofAreasCodes.Add(areaCode);
                            gridTypesCodes.Add(new List<int>());
                            ofAreasLayers.Add(new List<List<C2DLayer>>());
                        }
                        ind2 = gridTypesCodes[ind].IndexOf(grid.GridType);
                        if (ind2 == -1)
                        {
                            ind2 = gridTypesCodes[ind].Count;
                            gridTypesCodes[ind].Add(grid.GridType);
                            ofAreasLayers[ind].Add(new List<C2DLayer>());
                        }
                        ofAreasLayers[ind][ind2].Add(lr);
                    }
                }
                if (ofAreasCodes.Count > 0 && ofAreasLayers.Count > 0)
                {
                    rootNode.Nodes.Clear();
                    lrParent.ObjectsList.Clear();
                    
                    for (i = 0; i < ofAreasLayers.Count; i++)
                    {
                        if(ofAreasCodes[0] != 0)
                        {
                            layerArea = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, lrParent.oilFieldIndex);
                            layerArea.Visible = true;
                            layerArea.Name = oilfieldAreaDict.GetShortNameByCode(ofAreasCodes[i]);
                            layerArea.Index = lrParent.ObjectsList.Count;
                            lrParent.ObjectsList.Add(layerArea);
                            tnArea = rootNode.Nodes.Add(string.Empty, layerArea.Name, 17, 17);
                            tnArea.Checked = layerArea.Visible;
                            tnArea.Tag = layerArea;
                            counter2 = 0;
                        }
                        else
                        {
                            layerArea = lrParent;
                            tnArea = rootNode;
                        }
                        for (j = 0; j < ofAreasLayers[i].Count; j++)
                        {
                            layerGridType = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, lrParent.oilFieldIndex);
                            layerGridType.Visible = true;
                            layerGridType.Name = "Карта " + gridTypeDict.GetShortNameByCode(gridTypesCodes[i][j]);
                            layerGridType.Index = layerArea.ObjectsList.Count;
                            layerArea.ObjectsList.Add(layerGridType);

                            tnGridType = tnArea.Nodes.Add(string.Empty, layerGridType.Name, 17, 17);
                            tnGridType.Checked = layerGridType.Visible;
                            tnGridType.Tag = layerGridType;
                            tnGridType.Text += string.Format(" [{0}]", ofAreasLayers[i][j].Count);

                            for (int k = 0; k < ofAreasLayers[i][j].Count; k++)
                            {
                                layerGridType.Add(ofAreasLayers[i][j][k]);
                                counter2++;
                                tn = tnGridType.Nodes.Add(string.Empty, ofAreasLayers[i][j][k].Name, 17, 17);
                                tn.Checked = ofAreasLayers[i][j][k].Visible;
                                tn.Tag = ofAreasLayers[i][j][k];
                            }
                            layerGridType.GetObjectsRect();
                        }
                        if (counter2 > 0) tnArea.Text += string.Format(" [{0}]", counter2);
                        layerArea.GetObjectsRect();
                    }
                    lrParent.GetObjectsRect();
                }
                rootNode.Text = string.Format("{0} [{1}]", ((C2DLayer)rootNode.Tag).Name, counter);
            }
        }
        public int GetImageIndex(Constant.BASE_OBJ_TYPES_ID Type)
        {
            switch (Type)
            {
                case Constant.BASE_OBJ_TYPES_ID.LAYER:
                    return 2;
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    return 9;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    return 10;
                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                    return 11;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    return 12;
                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                    return 13;
                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                    return 14;
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    return 14;
                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                    return 15;
                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                    return 16;
                case Constant.BASE_OBJ_TYPES_ID.GRID:
                    return 17;
                case Constant.BASE_OBJ_TYPES_ID.WELL_LIST:
                    return 18;
                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                    return 18;
                case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                    return 19;
                case Constant.BASE_OBJ_TYPES_ID.PVT:
                    return 20;
                case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                    return 23;
                case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                    return 24;
                default:
                    return 0;
            }
        }
        public void SortInspectorByNGDU()
        {
            if ((twLayers != null) && (MapProject != null) && (LayerList.Count > 0))
            {
                TreeNode tnProject, tnNGDU, tnOilField, tnDatatype, tn;

                int i, lenLayerList = LayerList.Count, ngduCode;
                int imgIndex;
                Constant.BASE_OBJ_TYPES_ID type;


                OilField of;
                C2DLayer layer, lrNGDU, lrOF, lrData;

                twLayers.BeginUpdate();
                if (twLayers.Nodes.Count > 0) twLayers.Nodes[0].Remove();
                tnProject = twLayers.Nodes.Add("", MapProject.Name, 21, 21);
                tnProject.Tag = MapProject;
                tnProject.Checked = true;

                ArrayList objLayerList = new ArrayList();
                for (i = 0; i < lenLayerList; i++)
                {
                    FillLayerList((C2DLayer)this.LayerList[i], objLayerList);
                }

                for (i = 0; i < objLayerList.Count; i++)
                {
                    layer = (C2DLayer)objLayerList[i];

                    if (layer.oilfield != null)
                    {
                        of = layer.oilfield;
                        tnNGDU = twLayers.GetNGDUNodeByCode(tnProject, of.ParamsDict.NGDUCode);
                        if (tnNGDU == null) //добавляем нгду
                        {
                            lrNGDU = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                            lrNGDU.ngduCode = of.ParamsDict.NGDUCode;
                            lrNGDU.Name = of.ParamsDict.NGDUName;
                            lrNGDU.Index = LayerList.Count;
                            LayerList.Add(lrNGDU);
                            tnNGDU = twLayers.InsertNGDUByCode(tnProject, lrNGDU.Name, lrNGDU.ngduCode);
                        }
                        else
                        {
                            lrNGDU = (C2DLayer)tnNGDU.Tag;
                        }

                        tnNGDU.Tag = lrNGDU;
                        lrNGDU.node = tnNGDU;
                        lrNGDU.Visible = true;
                        tnNGDU.Checked = true;

                        // месторождение
                        tnOilField = null;
                        lrOF = null;

                        if (of.ParamsDict.NGDUCode != -1)
                        {
                            tnOilField = twLayers.GetOFNodeByCode(tnNGDU, of.Index);

                            if (tnOilField == null) //добавляем мест
                            {
                                lrOF = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                                lrOF.oilFieldIndex = of.Index;
                                lrOF.Name = of.Name;
                                lrOF.Index = lrNGDU.ObjectsList.Count;
                                lrNGDU.Add(lrOF);

                                tnOilField = twLayers.InsertOFByCode(tnNGDU, of.Name, of.Index);
                                tnOilField.Tag = lrOF;
                                lrOF.node = tnOilField;
                                tnOilField.Checked = true;
                                tnOilField.ContextMenuStrip = twLayers.cMenuSelect;
#if DEBUG
                                tnOilField.ContextMenuStrip = twLayers.cMenuOilField;
#endif
                            }
                            else
                            {
                                lrOF = (C2DLayer)tnOilField.Tag;
                            }
                        }
                        else
                        {
                            tnOilField = tnNGDU;
                            lrOF = lrNGDU;
                        }
                        type = layer.GetObjTypeIDRecursive();

                        tnDatatype = twLayers.GetDataNodeByID(tnOilField, type);
                        if (tnDatatype == null)
                        {
                            tnDatatype = twLayers.InsertDataNodeByID(tnOilField, type);

                            if ((type != Constant.BASE_OBJ_TYPES_ID.WELL) &&
                                (type != Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) &&
                                (type != Constant.BASE_OBJ_TYPES_ID.WELL_PAD))
                            {
                                lrData = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                                layer.Index = lrData.ObjectsList.Count;
                                lrData.Add(layer);
                                imgIndex = GetImageIndex(type);
                                tn = tnDatatype.Nodes.Add("", layer.Name, imgIndex, imgIndex);

                                tn.ContextMenuStrip = twLayers.cMenuSelect;
                                tn.Tag = layer;
                                layer.node = tn;
                                tn.ContextMenuStrip = twLayers.cMenuSelect;
                                tn.Checked = layer.Visible;
                                if (type == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                                {
                                    if ((layer.ObjectsList.Count > 0) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) &&
                                        (((Contour)layer.ObjectsList[0])._ContourType == -5))
                                    {
                                        layer.Visible = false;
                                        if (layer.node != null) layer.node.Checked = false;
                                    }
                                }
                                if (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                                {
                                    tn.Nodes.Add("Empty");
                                }
                            }
                            else
                            {
                                lrData = layer;
                                tnDatatype.Nodes.Add("Empty");
                            }

                            lrData.Name = tnDatatype.Text;
                            tnDatatype.Text = lrData.Name + " [" + lrData.ObjectsList.Count.ToString() + "]";
                            tnDatatype.Tag = lrData;
                            lrData.node = tnDatatype;
                            lrData.Visible = true;

                            switch (type)
                            {
                                case Constant.BASE_OBJ_TYPES_ID.GRID:
                                    lrData.Visible = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                                    lrData.Visible = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                                    lrData.Visible = false;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.AREA:
                                    lrData.Visible = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                                    lrData.Visible = false;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                                    lrData.Visible = false;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.WELL:
                                    lrData.Visible = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                                    lrData.Visible = true;
                                    break;
                            }
                            lrData.Level = -1;
                            lrData.oilfield = of;
                            lrData.Index = lrOF.ObjectsList.Count;
                            lrOF.Add(lrData);
                            tnDatatype.Checked = lrData.Visible;
                        }
                        else
                        {
                            lrData = (C2DLayer)tnDatatype.Tag;
                            layer.Index = lrData.ObjectsList.Count;
                            lrData.Add(layer);
                            tnDatatype.Text = lrData.Name + " [" + lrData.ObjectsList.Count.ToString() + "]";
                            imgIndex = GetImageIndex(type);
                            tn = tnDatatype.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                            tn.Tag = layer;
                            layer.node = tn;
                            tn.ContextMenuStrip = twLayers.cMenuSelect;
                            tn.Checked = layer.Visible;
                            if (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) tn.Nodes.Add("Empty");
                        }
                    }
                    else if (layer.ngduCode > -1)
                    {
                        tnNGDU = twLayers.GetNGDUNodeByCode(tnProject, layer.ngduCode);
                        if (tnNGDU != null)
                        {
                            lrNGDU = (C2DLayer)tnNGDU.Tag;
                            tnNGDU.Tag = lrNGDU;
                            lrNGDU.node = tnNGDU;
                            lrNGDU.Visible = true;
                            tnNGDU.Checked = true;
                            layer.SetVisibleRecursive(true);
                            layer.Visible = true;

                            type = layer.GetObjTypeIDRecursive();

                            tnDatatype = twLayers.GetDataNodeByID(tnNGDU, type);

                            if (tnDatatype == null)
                            {
                                tnDatatype = twLayers.InsertDataNodeByID(tnNGDU, type);

                                tnDatatype.Nodes.Add("Empty");

                                lrData = layer;
                                lrData.Name = tnDatatype.Text;
                                tnDatatype.Text = lrData.Name + " [" + lrData.ObjectsList.Count.ToString() + "]";
                                tnDatatype.Tag = lrData;
                                lrData.node = tnDatatype;
                                lrData.Visible = true;
                                lrData.Index = lrNGDU.ObjectsList.Count;
                                lrNGDU.Add(lrData);
                                tnDatatype.Checked = lrData.Visible;
                            }
                        }
                    }
                }
                int j, k;
                for (i = 0; i < twLayers.Nodes[0].Nodes.Count; i++)
                {
                    tnNGDU = twLayers.Nodes[0].Nodes[i];
                    for (j = 0; j < tnNGDU.Nodes.Count; j++)
                    {
                        tnOilField = tnNGDU.Nodes[j];
                        for (k = 0; k < tnOilField.Nodes.Count; k++)
                        {
                            tnDatatype = tnOilField.Nodes[k];
                            if (tnDatatype.Tag != null)
                            {
                                layer = (C2DLayer)tnDatatype.Tag;
                                if (layer.GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                                {
                                    SortByOilObjHierarchy(tnDatatype);
                                    break;
                                }
                            }
                        }
                        for (k = 0; k < tnOilField.Nodes.Count; k++)
                        {
                            tnDatatype = tnOilField.Nodes[k];
                            if (tnDatatype.Tag != null)
                            {
                                layer = (C2DLayer)tnDatatype.Tag;
                                if (layer.GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.DEPOSIT)
                                {
                                    SortByOilObjHierarchy(tnDatatype);
                                    break;
                                }
                            }
                        }
                        for (k = 0; k < tnOilField.Nodes.Count; k++)
                        {
                            tnDatatype = tnOilField.Nodes[k];
                            if (tnDatatype.Tag != null)
                            {
                                layer = (C2DLayer)tnDatatype.Tag;
                                if (layer.GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.GRID)
                                {
                                    SortByAreaGridTypes(tnDatatype);
                                    break;
                                }
                            }
                        }
                        if ((tnOilField.Tag != null) && (((C2DLayer)tnOilField.Tag).oilFieldIndex != -1) && (((C2DLayer)tnOilField.Tag).oilFieldIndex < MapProject.OilFields.Count))
                        {
                            of = MapProject.OilFields[((C2DLayer)tnOilField.Tag).oilFieldIndex];
                            if ((of.Aliases != null) && (of.Aliases.Count > 0))
                            {
                                tnDatatype = tnOilField.Nodes.Add("Псевдонимы");
                                for (k = 0; k < of.Aliases.Count; k++)
                                {
                                    tn = tnDatatype.Nodes.Add(of.Aliases[k]);
                                    tn.ContextMenuStrip = twLayers.cMenuAlias;
                                    tn.Checked = true;
                                }
                                tnDatatype.Text = "Псевдонимы [" + of.Aliases.Count.ToString() + "]";
                                tnDatatype.Checked = true;
                            }
                        }
                    }
                }

                twLayers.Nodes[0].Expand();
                twLayers.EndUpdate();
                LayerList.Clear();
                for (i = 0; i < tnProject.Nodes.Count; i++)
                {
                    ((C2DLayer)tnProject.Nodes[i].Tag).GetObjectsRect();
                    LayerList.Add((C2DObject)tnProject.Nodes[i].Tag);
                }
                if (selLayerList.Count > 0)
                {
                    SetActiveOilField(((C2DLayer)selLayerList[selLayerList.Count - 1]).oilFieldIndex);
                }
                else
                {
                    twActiveOilField.Clear();
                }
                DrawLayerList();
            }
        }
        public void SortInspectorByOilField()
        {
            if ((twLayers != null) && (MapProject != null) && (LayerList.Count > 0))
            {
                TreeNode tnProject, tnOilField, tnDatatype, tn;

                int i, lenLayerList = LayerList.Count;
                Constant.BASE_OBJ_TYPES_ID type;

                OilField of;
                C2DLayer layer, lrOF, lrData;

                twLayers.BeginUpdate();
                if (twLayers.Nodes.Count > 0) twLayers.Nodes[0].Remove();
                tnProject = twLayers.Nodes.Add("", MapProject.Name, 21, 21);
                tnProject.Tag = MapProject;
                tnProject.Checked = true;

                ArrayList objLayerList = new ArrayList();
                for (i = 0; i < lenLayerList; i++)
                {
                    FillLayerList((C2DLayer)this.LayerList[i], objLayerList);
                }

                for (i = 0; i < objLayerList.Count; i++)
                {
                    layer = (C2DLayer)objLayerList[i];

                    if (layer.oilfield != null)
                    {
                        of = layer.oilfield;
                        // месторождение
                        tnOilField = null;
                        lrOF = null;

                        tnOilField = twLayers.GetOFNodeByCode(tnProject, of.Index);

                        if (tnOilField == null)//добавляем мест
                        {
                            lrOF = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                            lrOF.oilFieldIndex = of.Index;
                            lrOF.Name = of.Name;

                            tnOilField = twLayers.InsertOFByCode(tnProject, of.Name, of.Index);
                            tnOilField.Tag = lrOF;
                            lrOF.node = tnOilField;
                            tnOilField.Checked = true;
                            //tnOilField.ContextMenuStrip = twLayers.cMenuOilField;
                            tnOilField.ContextMenuStrip = twLayers.cMenuSelect;
                        }
                        else
                        {
                            lrOF = (C2DLayer)tnOilField.Tag;
                        }


                        type = layer.GetObjTypeIDRecursive();

                        tnDatatype = twLayers.GetDataNodeByID(tnOilField, type);
                        int imgIndex;
                        if (tnDatatype == null)
                        {
                            tnDatatype = twLayers.InsertDataNodeByID(tnOilField, type);

                            if ((type != Constant.BASE_OBJ_TYPES_ID.WELL) &&
                                (type != Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) &&
                                (type != Constant.BASE_OBJ_TYPES_ID.WELL_PAD))
                            {
                                lrData = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                                layer.Index = lrData.ObjectsList.Count;
                                lrData.Add(layer);
                                imgIndex = GetImageIndex(type);
                                tn = tnDatatype.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                                tn.ContextMenuStrip = twLayers.cMenuSelect;
                                tn.Tag = layer;
                                layer.node = tn;
                                tn.Checked = layer.Visible;
                                if (type == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                                {
                                    if ((layer.ObjectsList.Count > 0) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) &&
                                        (((Contour)layer.ObjectsList[0])._ContourType == -5))
                                    {
                                        layer.Visible = false;
                                        if (layer.node != null) layer.node.Checked = false;
                                    }
                                }
                                if (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) tn.Nodes.Add("Empty");
                            }
                            else
                            {
                                lrData = layer;
                                tnDatatype.Nodes.Add("Empty");
                                //if (type == Constant.BASE_OBJ_TYPES_ID.WELL) tnDatatype.ContextMenuStrip = twLayers.cMenuWellList;
                            }

                            lrData.Name = tnDatatype.Text;
                            tnDatatype.Text = lrData.Name + " [" + lrData.ObjectsList.Count.ToString() + "]";
                            tnDatatype.Tag = lrData;
                            lrData.node = tnDatatype;
                            tnDatatype.Checked = true;
                            lrData.Visible = true;
                            lrData.Level = -1;
                            lrData.oilfield = of;
                            switch (type)
                            {
                                case Constant.BASE_OBJ_TYPES_ID.GRID:
                                    lrData.Visible = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                                    lrData.Visible = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                                    lrData.Visible = false;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.AREA:
                                    lrData.Visible = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                                    lrData.Visible = false;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                                    lrData.Visible = false;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.WELL:
                                    lrData.Visible = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                                    lrData.Visible = true;
                                    break;
                            }
                            lrData.Index = lrOF.ObjectsList.Count;
                            lrOF.Add(lrData);
                            tnDatatype.Checked = lrData.Visible;
                        }
                        else
                        {
                            lrData = (C2DLayer)tnDatatype.Tag;
                            layer.Index = lrData.ObjectsList.Count;
                            lrData.Add(layer);
                            tnDatatype.Text = lrData.Name + " [" + lrData.ObjectsList.Count.ToString() + "]";
                            imgIndex = GetImageIndex(type);
                            tn = tnDatatype.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                            tn.ContextMenuStrip = twLayers.cMenuSelect;
                            tn.Tag = layer;
                            layer.node = tn;
                            tn.Checked = layer.Visible;
                            if (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) tn.Nodes.Add("Empty");
                        }
                    }
                    //else // обустройство
                }
                int k;
                for (i = 0; i < twLayers.Nodes[0].Nodes.Count; i++)
                {
                    tnOilField = twLayers.Nodes[0].Nodes[i];
                    for (k = 0; k < tnOilField.Nodes.Count; k++)
                    {
                        tnDatatype = tnOilField.Nodes[k];
                        if (tnDatatype.Tag != null)
                        {
                            layer = (C2DLayer)tnDatatype.Tag;
                            if (layer.GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                            {
                                SortByOilObjHierarchy(tnDatatype);
                                break;
                            }
                        }
                    }
                    for (k = 0; k < tnOilField.Nodes.Count; k++)
                    {
                        tnDatatype = tnOilField.Nodes[k];
                        if (tnDatatype.Tag != null)
                        {
                            layer = (C2DLayer)tnDatatype.Tag;
                            if (layer.GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.DEPOSIT)
                            {
                                SortByOilObjHierarchy(tnDatatype);
                                break;
                            }
                        }
                    }
                    for (k = 0; k < tnOilField.Nodes.Count; k++)
                    {
                        tnDatatype = tnOilField.Nodes[k];
                        if (tnDatatype.Tag != null)
                        {
                            layer = (C2DLayer)tnDatatype.Tag;
                            if (layer.GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.GRID)
                            {
                                SortByAreaGridTypes(tnDatatype);
                                break;
                            }
                        }
                    }
                    if ((tnOilField.Tag != null) && (((C2DLayer)tnOilField.Tag).oilFieldIndex != -1) && (((C2DLayer)tnOilField.Tag).oilFieldIndex < MapProject.OilFields.Count))
                    {
                        of = MapProject.OilFields[((C2DLayer)tnOilField.Tag).oilFieldIndex];
                        if ((of.Aliases != null) && (of.Aliases.Count > 0))
                        {
                            tnDatatype = tnOilField.Nodes.Add("Псевдонимы");
                            for (k = 0; k < of.Aliases.Count; k++)
                            {
                                tn = tnDatatype.Nodes.Add(of.Aliases[k]);
                                tn.ContextMenuStrip = twLayers.cMenuAlias;
                                tn.Checked = true;
                            }
                            tnDatatype.Text = "Псевдонимы [" + of.Aliases.Count.ToString() + "]";
                            tnDatatype.Checked = true;
                        }
                    }

                }

                twLayers.Nodes[0].Expand();
                twLayers.EndUpdate();
                LayerList.Clear();
                for (i = 0; i < tnProject.Nodes.Count; i++)
                {
                    ((C2DLayer)tnProject.Nodes[i].Tag).GetObjectsRect();
                    LayerList.Add((C2DObject)tnProject.Nodes[i].Tag);
                }
                if (selLayerList.Count > 0)
                {
                    SetActiveOilField(((C2DLayer)selLayerList[selLayerList.Count - 1]).oilFieldIndex);
                }
                else
                {
                    twActiveOilField.Clear();
                }

                DrawLayerList();
            }
        }
        public void SortInspectorByDataType()
        {
            TreeNode tnProject, tnNGDU, tnOilField, tnDatatype, tn;

            if ((twLayers.Nodes.Count > 0) &&
                (((BaseObj)twLayers.Nodes[0].Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT))
            {
                int i, j, k, lenLayerList = LayerList.Count;
                Constant.BASE_OBJ_TYPES_ID type;

                ArrayList objLayerList = new ArrayList();
                for (i = 0; i < lenLayerList; i++)
                {
                    FillLayerList((C2DLayer)this.LayerList[i], objLayerList);
                }

                OilField of;
                C2DLayer layer, lrNGDU, lrOF, lrData;
                twLayers.BeginUpdate();
                if (twLayers.Nodes.Count > 0) twLayers.Nodes[0].Remove();
                tnProject = twLayers.Nodes.Add("", MapProject.Name, 21, 21);
                tnProject.Tag = MapProject;
                tnProject.Checked = true;

                int imgIndex;
                for (i = 0; i < objLayerList.Count; i++)
                {
                    layer = (C2DLayer)objLayerList[i];

                    type = layer.GetObjTypeIDRecursive();
                    //if (type != Constant.BASE_OBJ_TYPES_ID.GRID)
                    //{
                    //    layer.SetVisibleRecursive(true);
                    //    layer.Visible = true;
                    //}
                    //else
                    //{
                    //    layer.Visible = false;
                    //}

                    tnDatatype = twLayers.GetDataNodeByID(tnProject, type);

                    if (tnDatatype == null)
                    {
                        lrData = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                        tnDatatype = twLayers.InsertDataNodeByID(tnProject, type);
                        lrData.Name = tnDatatype.Text;
                        tnDatatype.Tag = lrData;
                        tnDatatype.Checked = true;
                        lrData.Visible = true;
                    }
                    else
                    {
                        lrData = (C2DLayer)tnDatatype.Tag;
                    }
                    if (layer.oilfield != null)
                    {
                        of = layer.oilfield;
                        tnNGDU = twLayers.GetNGDUNodeByCode(tnDatatype, of.ParamsDict.NGDUCode);
                        if (tnNGDU == null) //добавляем нгду
                        {
                            lrNGDU = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                            lrNGDU.ngduCode = of.ParamsDict.NGDUCode;
                            lrNGDU.Name = of.ParamsDict.NGDUName;

                            ((C2DLayer)tnDatatype.Tag).Add(lrNGDU);
                            tnNGDU = twLayers.InsertNGDUByCode(tnDatatype, lrNGDU.Name, lrNGDU.ngduCode);
                            tnNGDU.ContextMenuStrip = twLayers.cMenuSelect;
                        }
                        else
                        {
                            lrNGDU = (C2DLayer)tnNGDU.Tag;
                        }

                        tnNGDU.Tag = lrNGDU;
                        lrNGDU.Visible = true;
                        tnNGDU.Checked = true;

                        // месторождение
                        tnOilField = null;
                        lrOF = null;

                        if (of.ParamsDict.NGDUCode != -1)
                        {
                            tnOilField = twLayers.GetOFNodeByCode(tnNGDU, of.Index);

                            if (tnOilField == null)//добавляем мест
                            {
                                tnOilField = twLayers.InsertOFByCode(tnNGDU, of.Name, of.Index);

                                if ((type != Constant.BASE_OBJ_TYPES_ID.WELL) &&
                                    (type != Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) &&
                                    (type != Constant.BASE_OBJ_TYPES_ID.WELL_PAD))
                                {
                                    lrOF = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, -1);
                                    lrOF.oilFieldIndex = of.Index;
                                    lrOF.Name = of.Name;
                                    lrOF.Add(layer);
                                }
                                else
                                {
                                    lrOF = layer;
                                    tnOilField.Nodes.Add("Empty");
                                    tnOilField.Text = tnOilField.Text + " [" + lrOF.ObjectsList.Count.ToString() + "]";
                                }

                                tnOilField.Tag = lrOF;
                                tnOilField.ContextMenuStrip = twLayers.cMenuSelect;
                                lrOF.node = tnOilField;
                                lrOF.Visible = true;

                                switch (type)
                                {
                                    case Constant.BASE_OBJ_TYPES_ID.GRID:
                                        lrOF.Visible = true;
                                        break;
                                    case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                                        lrOF.Visible = true;
                                        break;
                                    case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                                        lrOF.oilfield = of;
                                        lrOF.Level = -1;
                                        lrOF.Visible = false;
                                        break;
                                    case Constant.BASE_OBJ_TYPES_ID.AREA:
                                        lrOF.Visible = true;
                                        break;
                                    case Constant.BASE_OBJ_TYPES_ID.DEPOSIT:
                                        lrOF.Visible = false;
                                        break;
                                    case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                                        lrOF.Visible = false;
                                        break;
                                    case Constant.BASE_OBJ_TYPES_ID.WELL:
                                        lrOF.Visible = true;
                                        break;
                                    case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                                        lrOF.Visible = true;
                                        break;
                                }
                                lrNGDU.Add(lrOF);
                                tnOilField.Checked = lrOF.Visible;
                            }
                            else
                            {
                                lrOF = (C2DLayer)tnOilField.Tag;
                                layer.Index = lrOF.ObjectsList.Count;
                                lrOF.Add(layer);
                            }
                            if ((type != Constant.BASE_OBJ_TYPES_ID.WELL) &&
                                (type != Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) &&
                                (type != Constant.BASE_OBJ_TYPES_ID.WELL_PAD))
                            {
                                tnOilField.Text = lrOF.Name + " [" + lrOF.ObjectsList.Count.ToString() + "]";
                                imgIndex = GetImageIndex(type);
                                tn = tnOilField.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                                tn.ContextMenuStrip = twLayers.cMenuSelect;
                                tn.Checked = layer.Visible;
                                tn.Tag = layer;
                                layer.node = tn;
                                if (type == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                                {
                                    if ((layer.ObjectsList.Count > 0) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) &&
                                        (((Contour)layer.ObjectsList[0])._ContourType == -5))
                                    {
                                        layer.Visible = false;
                                        if (layer.node != null) layer.node.Checked = false;
                                    }
                                }
                                if (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) tn.Nodes.Add("Empty");
                            }
                        }
                        else
                        {
                            tnOilField = tnNGDU;
                            lrOF = lrNGDU;
                            layer.Index = lrOF.ObjectsList.Count;
                            lrOF.Add(layer);
                            tnOilField.Text = lrOF.Name + " [" + lrOF.ObjectsList.Count.ToString() + "]";
                            imgIndex = GetImageIndex(type);
                            tn = tnOilField.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                            tn.ContextMenuStrip = twLayers.cMenuSelect;
                            tn.Checked = layer.Visible;
                            tn.Tag = layer;
                            layer.node = tn;
                            if (type == Constant.BASE_OBJ_TYPES_ID.CONTOUR) tn.Nodes.Add("Empty");
                        }
                    }
                    else if (layer.ngduCode > -1)
                    {
                        tnNGDU = twLayers.GetNGDUNodeByCode(tnDatatype, layer.ngduCode);

                        if (tnNGDU == null) //добавляем нгду
                        {
                            lrNGDU = layer;
                            for (j = 0; j < MapProject.OilFields.Count; j++)
                            {
                                if ((MapProject.OilFields[j]).ParamsDict.NGDUCode == layer.ngduCode)
                                {
                                    lrNGDU.Name = (MapProject.OilFields[j]).ParamsDict.NGDUName;
                                    break;
                                }
                            }
                            lrNGDU.Visible = true;
                            tnNGDU = twLayers.InsertNGDUByCode(tnDatatype, lrNGDU.Name, lrNGDU.ngduCode);
                            tnNGDU.Checked = lrNGDU.Visible;
                            tnNGDU.ContextMenuStrip = twLayers.cMenuSelect;
                            lrNGDU.node = tnNGDU;
                            lrData.Add(lrNGDU);
                            tnNGDU.Tag = lrNGDU;
                            tnNGDU.Nodes.Add("Empty");
                            //tnDatatype.Text = lrData.Name + " [" + lrData._ObjectsList.Count.ToString() + "]";
                        }
                    }
                }
                // Сортировка контуров по иерерхии пластов
                for (i = 0; i < twLayers.Nodes[0].Nodes.Count; i++)
                {
                    tnDatatype = twLayers.Nodes[0].Nodes[i];
                    if ((tnDatatype.Tag != null) &&
                        (((C2DLayer)tnDatatype.Tag).GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.CONTOUR))
                    {
                        for (j = 0; j < tnDatatype.Nodes.Count; j++)
                        {
                            tnNGDU = tnDatatype.Nodes[j];
                            for (k = 0; k < tnNGDU.Nodes.Count; k++)
                            {
                                tnOilField = tnNGDU.Nodes[k];
                                if (tnOilField.Tag != null)
                                {
                                    SortByOilObjHierarchy(tnOilField);
                                }
                            }
                        }
                        break;
                    }
                }
                for (i = 0; i < twLayers.Nodes[0].Nodes.Count; i++)
                {
                    tnDatatype = twLayers.Nodes[0].Nodes[i];
                    if ((tnDatatype.Tag != null) &&
                        (((C2DLayer)tnDatatype.Tag).GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.DEPOSIT))
                    {
                        for (j = 0; j < tnDatatype.Nodes.Count; j++)
                        {
                            tnNGDU = tnDatatype.Nodes[j];
                            for (k = 0; k < tnNGDU.Nodes.Count; k++)
                            {
                                tnOilField = tnNGDU.Nodes[k];
                                if (tnOilField.Tag != null)
                                {
                                    SortByOilObjHierarchy(tnOilField);
                                }
                            }
                        }
                        break;
                    }
                }
                for (i = 0; i < twLayers.Nodes[0].Nodes.Count; i++)
                {
                    tnDatatype = twLayers.Nodes[0].Nodes[i];
                    if ((tnDatatype.Tag != null) &&
                        (((C2DLayer)tnDatatype.Tag).GetObjTypeIDRecursive() == Constant.BASE_OBJ_TYPES_ID.GRID))
                    {
                        for (j = 0; j < tnDatatype.Nodes.Count; j++)
                        {
                            tnNGDU = tnDatatype.Nodes[j];
                            for (k = 0; k < tnNGDU.Nodes.Count; k++)
                            {
                                tnOilField = tnNGDU.Nodes[k];
                                if (tnOilField.Tag != null)
                                {
                                    SortByAreaGridTypes(tnOilField);
                                }
                            }
                        }
                        break;
                    }
                }
                twLayers.Nodes[0].Expand();
                twLayers.EndUpdate();
                LayerList.Clear();
                for (i = 0; i < tnProject.Nodes.Count; i++)
                {
                    ((C2DLayer)tnProject.Nodes[i].Tag).GetObjectsRect();
                    LayerList.Add((C2DObject)tnProject.Nodes[i].Tag);
                }
                if (selLayerList.Count > 0)
                {
                    SetActiveOilField(((C2DLayer)selLayerList[selLayerList.Count - 1]).oilFieldIndex);
                }
                else
                {
                    twActiveOilField.Clear();
                }
                DrawLayerList();
            }

        }
        public void UpdateOilFieldAliases(OilField of)
        {
            if (twLayers.Nodes.Count > 0)
            {
                TreeNode NGDUNode = null, ofNode = null, projectNode;
                projectNode = twLayers.Nodes[0];
                if (twLayers.cmSortByNGDU.Checked)
                {
                    NGDUNode = twLayers.GetNGDUNodeByCode(projectNode, of.ParamsDict.NGDUCode);
                    if (NGDUNode != null)
                    {
                        ofNode = twLayers.GetOFNodeByCode(NGDUNode, of.Index);
                    }
                }
                else if (twLayers.cmSortByOilField.Checked)
                {
                    ofNode = twLayers.GetOFNodeByCode(projectNode, of.Index);
                }

                if ((ofNode != null) && (ofNode.Nodes.Count > 0))
                {
                    TreeNode tn, tnAlias = ofNode.Nodes[ofNode.Nodes.Count - 1];
                    if ((of.Aliases == null) && (tnAlias.Text.StartsWith("Псевдонимы")))
                    {
                        twLayers.BeginUpdate();
                        ofNode.Nodes.Remove(tnAlias);
                        tnAlias = null;
                        twLayers.EndUpdate();
                    }
                    else if (of.Aliases != null)
                    {
                        twLayers.BeginUpdate();
                        if (!tnAlias.Text.StartsWith("Псевдонимы"))
                        {
                            tnAlias = ofNode.Nodes.Add("");
                        }
                        tnAlias.Nodes.Clear();
                        for (int i = 0; i < of.Aliases.Count; i++)
                        {
                            tn = tnAlias.Nodes.Add(of.Aliases[i]);
                            tn.ContextMenuStrip = twLayers.cMenuAlias;
                            tn.Checked = true;
                        }
                        tnAlias.Text = "Псевдонимы [" + of.Aliases.Count.ToString() + "]";
                        tnAlias.Checked = true;
                        twLayers.EndUpdate();
                    }
                    if ((twActiveOilField.ActiveOilFieldIndex == of.Index) && (twActiveOilField.Nodes.Count > 0))
                    {
                        ofNode = twActiveOilField.Nodes[0];
                        tnAlias = ofNode.Nodes[ofNode.Nodes.Count - 1];
                        if ((of.Aliases == null) && (tnAlias.Text.StartsWith("Псевдонимы")))
                        {
                            twLayers.BeginUpdate();
                            ofNode.Nodes.Remove(tnAlias);
                            tnAlias = null;
                            twLayers.EndUpdate();
                        }
                        else if (of.Aliases != null)
                        {
                            twLayers.BeginUpdate();
                            if (!tnAlias.Text.StartsWith("Псевдонимы"))
                            {
                                tnAlias = ofNode.Nodes.Add("");
                            }
                            tnAlias.Nodes.Clear();
                            for (int i = 0; i < of.Aliases.Count; i++)
                            {
                                tn = tnAlias.Nodes.Add(of.Aliases[i]);
                                tn.ContextMenuStrip = twActiveOilField.cMenuAlias;
                                tn.Checked = true;
                            }
                            tnAlias.Text = "Псевдонимы [" + of.Aliases.Count.ToString() + "]";
                            tnAlias.Checked = true;
                            twLayers.EndUpdate();
                        }

                    }
                }
            }
        }

        // ACTIVE OILFIELD
        public void SetActiveOilField(int OilFieldIndex)
        {
            if ((twLayers != null) && (twActiveOilField != null) &&
                (MapProject != null) && (LayerList.Count > 0) &&
                (twLayers.Nodes.Count > 0) && (twActiveOilField.ActiveOilFieldIndex != OilFieldIndex) &&
                (OilFieldIndex != -1))
            {
                TreeNode tnProj, tnNGDU, tnOilField = null, tnDatatype, tn, tnOFActive;

                int i, j, k;
                Constant.BASE_OBJ_TYPES_ID type;
                bool find;
                OilField of;
                C2DLayer layer, lrOF, lrData;

                mainForm.fBubbleSet.SetMinMaxDate((MapProject.OilFields[OilFieldIndex]).minMerDate, (MapProject.OilFields[OilFieldIndex]).maxMerDate);

                mainForm.dActiveWindow("pActiveOilField");
                twActiveOilField.BeginUpdate();
                if (twActiveOilField.Nodes.Count > 0) twActiveOilField.Clear();
                twActiveOilField.ActiveOilFieldIndex = OilFieldIndex;
                MapProject.indActiveOilField = OilFieldIndex;
                tnProj = twLayers.Nodes[0];

                if ((twLayers.cmSortByNGDU.Checked) || (twLayers.cmSortByOilField.Checked))
                {
                    find = false;
                    for (i = 0; i < tnProj.Nodes.Count; i++)
                    {
                        if (!find)
                        {
                            if (twLayers.cmSortByNGDU.Checked)
                            {
                                tnNGDU = tnProj.Nodes[i];
                            }
                            else
                            {
                                tnNGDU = tnProj;
                                if (i > 0) break;
                            }

                            for (j = 0; j < tnNGDU.Nodes.Count; j++)
                            {
                                tnOilField = tnNGDU.Nodes[j];
                                if ((tnOilField.Tag != null) &&
                                    (((BaseObj)tnOilField.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                    (((C2DLayer)tnOilField.Tag).oilFieldIndex == OilFieldIndex))
                                {
                                    find = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (find)
                    {
                        tnOFActive = twActiveOilField.Nodes.Add("", tnOilField.Text, 9, 9);
                        tnOFActive.Checked = tnOilField.Checked;
                        tnOFActive.Tag = tnOilField.Tag;
                        tnOFActive.ContextMenuStrip = twActiveOilField.cMenuOilField;
                        CopyTreeNodeRecursive(tnOilField, tnOFActive);
                        if (tnOFActive.Nodes.Count > 0)
                        {
                            TreeNode tnAlias = tnOFActive.Nodes[tnOFActive.Nodes.Count - 1];
                            if (tnAlias.Text.StartsWith("Псевдонимы"))
                            {
                                for (j = 0; j < tnAlias.Nodes.Count; j++)
                                {
                                    tnAlias.Nodes[j].ContextMenuStrip = twActiveOilField.cMenuAlias;
                                }
                            }
                        }
                        twActiveOilField.Nodes[0].Expand();
                    }
                }
                else if (twLayers.cmSortByData.Checked)
                {
                    ArrayList list = new ArrayList();
                    for (i = 0; i < tnProj.Nodes.Count; i++)
                    {
                        tnDatatype = tnProj.Nodes[i];
                        for (k = 0; k < tnDatatype.Nodes.Count; k++)
                        {
                            tnNGDU = tnDatatype.Nodes[k];
                            for (j = 0; j < tnNGDU.Nodes.Count; j++)
                            {
                                tnOilField = tnNGDU.Nodes[j];
                                if ((tnOilField.Tag != null) &&
                                    (((BaseObj)tnOilField.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                                    (((C2DLayer)tnOilField.Tag).oilFieldIndex == OilFieldIndex))
                                {
                                    list.Add(tnOilField);
                                }
                            }
                        }
                    }
                    if (list.Count > 0)
                    {
                        tnOFActive = twActiveOilField.Nodes.Add("", "", 0, 0);
                        tnOFActive.Checked = true;
                        for (i = 0; i < list.Count; i++)
                        {
                            lrData = (C2DLayer)((TreeNode)list[i]).Tag;
                            if ((lrData.oilfield != null) && (tnOFActive.Text == ""))
                            {
                                tnOFActive.Text = lrData.oilfield.Name;
                                tnOFActive.Checked = lrData.oilfield.Visible;
                                tnOFActive.Tag = lrData.oilfield;
                            }
                            type = lrData.GetObjTypeIDRecursive();
                            switch (type)
                            {
                                case Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR:
                                    lrData.Name = "Условные контуры";
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                                    lrData.Name = "Контуры";
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.AREA:
                                    lrData.Name = "Ячейки";
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.WELL_PAD:
                                    lrData.Name = "Кусты";
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.WELL:
                                    lrData.Name = "Скважины";
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                                    lrData.Name = "Проектные скважины";
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.GRID:
                                    lrData.Name = "Сетки";
                                    break;
                            }
                            tn = tnOFActive.Nodes.Add(lrData.Name + " [" + lrData.ObjectsList.Count + "]");
                            tn.ContextMenuStrip = twActiveOilField.cMenuSelect;
                            if ((type != Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) &&
                                (type != Constant.BASE_OBJ_TYPES_ID.WELL))
                            {
                                CopyTreeNodeRecursive((TreeNode)list[i], tn);
                                tn.Checked = ((TreeNode)list[i]).Checked;
                                tn.Tag = ((TreeNode)list[i]).Tag;
                            }
                            else
                            {
                                tn.Checked = lrData.Visible;
                                tn.Tag = lrData;
                                tn.Nodes.Add("Empty");
                                if (type == Constant.BASE_OBJ_TYPES_ID.WELL)
                                    tn.ContextMenuStrip = twActiveOilField.cMenuWellList;
                            }
                        }
                        tnOFActive.Expand();
                    }
                }

                twActiveOilField.EndUpdate();
            }
        }
        public void ClearActiveOilField()
        {
            twActiveOilField.BeginUpdate();
            twActiveOilField.Clear();
            twActiveOilField.ActiveOilFieldIndex = -1;
            MapProject.indActiveOilField = -1;
            twActiveOilField.EndUpdate();

        }
        public void SetVisibleWellHead(bool VisibleWellHead)
        {
            C2DLayer layer;
            List<C2DLayer> list = new List<C2DLayer>();
            for (int i = 0; i < LayerList.Count; i++)
            {
                layer = (C2DLayer)LayerList[i];
                layer.FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.WELL, list);
            }
            for (int i = 0; i < list.Count; i++)
            {
                ((C2DLayer)list[i]).DrawWellHead = VisibleWellHead;
            }
            DrawLayerList();
        }

        // GRID
        public void LoadGridFromFile(BackgroundWorker worker, DoWorkEventArgs e, string FileName)
        {
            OilField of = MapProject.OilFields[twActiveOilField.ActiveOilFieldIndex];
            Grid grid = new Grid();
            string fileName = Path.GetFileNameWithoutExtension(FileName);
            grid.Name = fileName;
            if (grid.LoadFromFile(worker, e, FileName, of.CoefDict))
            {
                int i, len = 1;
                string str;
                for (i = 0; i < LayerWorkList.Count; i++)
                {
                    if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                    {
                        str = ((C2DLayer)LayerWorkList[i]).Name;
                        if ((str != null) && (str.IndexOf(fileName) > -1)) len++;
                    }
                }

                grid.OilFieldCode = of.OilFieldCode;

                C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.GRID, twActiveOilField.ActiveOilFieldIndex);
                layer.ObjectsList.Add(grid);
                layer.Visible = true;
                layer.VisibleFar = true;
                layer.Name = fileName;
                if (len > 1) layer.Name += string.Format(" ({0})", len);
                layer.GetObjectsRect();
                layer.Index = LayerWorkList.Count;
                if (e != null) e.Result = layer;
            }
        }
        public void LoadGridFromFile()
        {
            if (MapProject != null)
            {
                if (twActiveOilField.ActiveOilFieldIndex != -1)
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    string ofNAme = (MapProject.OilFields[twActiveOilField.ActiveOilFieldIndex]).Name;
                    dlg.Title = "Выберите файл сетки для месторождения '" + ofNAme + "'";
                    dlg.Filter = "Surfer 7 grid (*.grd)|*.grd";
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        MapProject.tempStr = dlg.FileName;
                        mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_OF_GRID_FROM_FILE, MapProject);
                    }
                }
                else
                {
                    MessageBox.Show("Сделайте активным месторождение, к которому будут привязаны координаты сетки.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        public void LoadGridsFromDirectoryToProject(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if ((MapProject != null) && (Directory.Exists(MapProject.tempStr)))
            {
                int ind, objCode;
                List<int> ofIndexes = new List<int>();
                var dict = (StratumDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                DirectoryInfo dir = new DirectoryInfo(MapProject.tempStr);
                DirectoryInfo[] NGDUList = dir.GetDirectories();
                DateTime dt;
                string[] parseName;
                string ObjName;
                string GridType, Date;
                int gridType;
                char[] sep1 = { '_' };
                string[] sep2 = { " на ", ".grd" };
                OilField of;
                if (NGDUList.Length > 0)
                {
                    DirectoryInfo[] oilfields;
                    FileInfo[] files;
                    for (int n = 0; n < NGDUList.Length; n++)
                    {
                        oilfields = NGDUList[n].GetDirectories();
                        for (int ofInd = 0; ofInd < MapProject.OilFields.Count; ofInd++)
                        {
                            ind = MapProject.GetOFIndex(oilfields[ofInd].Name.ToUpper());
                            if (ind != -1)
                            {
                                of = MapProject.OilFields[ind];
                                if (ofIndexes.IndexOf(ind) == -1) ofIndexes.Add(ind);
                                files = oilfields[ofInd].GetFiles("*.grd");
                                for (int i = 0; i < files.Length; i++)
                                {
                                    parseName = files[i].Name.Split(sep1);
                                    ObjName = parseName[0];
                                    parseName = parseName[1].Split(sep2, StringSplitOptions.RemoveEmptyEntries);
                                    objCode = dict.TryGetLocalCodeByServerPlastName(ObjName);
                                    if ((parseName.Length > 1) && (ind != -1))
                                    {
                                        switch (parseName[0])
                                        {
                                            case "Карта остаточных н-н толщин":
                                                GridType = "ОННТ";
                                                gridType = 200;
                                                break;
                                            case "Карта давлений":
                                                GridType = "изобар";
                                                gridType = 300;
                                                break;
                                            default:
                                                MessageBox.Show("Неизвестный тип карты" + parseName[0]);
                                                continue;
                                        }
                                        try
                                        {
                                            dt = Convert.ToDateTime(parseName[1]);
                                        }
                                        catch
                                        {
                                            MessageBox.Show("Неправильная дата" + parseName[1]);
                                            continue;
                                        }
                                        Grid grid = new Grid();
                                        if (grid.LoadFromFile(null, null, files[i].FullName, of.CoefDict))
                                        {
                                            grid.Name = "Карта " + GridType;
                                            grid.OilFieldCode = of.OilFieldCode;
                                            grid.StratumCode = objCode;
                                            grid.GridType = gridType;
                                            grid.Date = dt;
                                            grid.Index = of.Grids.Count;
                                            grid.PackGridData(true);
                                            grid.DataLoaded = true;
                                            grid.DataLoading = true;
                                            of.Grids.Add(grid);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Не найден пласт " + ObjName);
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Не найдено " + oilfields[ofInd].Name);
                            }
                        }
                        worker.ReportProgress((int)((float)n * 100f / NGDUList.Length));
                    }
                    for (int i = 0; i < ofIndexes.Count; i++)
                    {
                        of = MapProject.OilFields[ofIndexes[i]];
                        for (int j = 0; j < of.Grids.Count; j++)
                        {
                            if (!((Grid)of.Grids[j]).DataLoaded)
                            {
                                of.LoadGridDataFromCacheByIndex(worker, e, j, false);
                                ((Grid)of.Grids[j]).PackGridData(true);
                            }
                        }
                        of.WriteGridListToCache(worker, e);
                    }
                }
            }
        }

        #region WORK FOLDER

        // Converting WF
        public void TestConvertWorkFolder()
        {
            if (MapProject.IsSmartProject)
            {
                string startFolder = App.GetRegistryPath();
                if (startFolder.Length == 0) startFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                string UserOldProjectFolder = startFolder + "\\МиР\\Проекты\\";
                bool testOldProject = false;
                if (Directory.Exists(UserOldProjectFolder))
                {
                    DirectoryInfo root = new DirectoryInfo(UserOldProjectFolder);
                    DirectoryInfo[] dirs = root.GetDirectories();
                    if (dirs.Length > 0)
                    {
                        UserOldProjectFolder = dirs[0].FullName;
                        testOldProject = true;
                    }
                }
                string UserOldWorkFolder = startFolder + "\\МиР\\Рабочая папка";

                if (!Directory.Exists(mainForm.UserWorkDataFolder)) Directory.CreateDirectory(mainForm.UserWorkDataFolder);

                bool testOldWorkFolder = Directory.Exists(UserOldWorkFolder) && (Directory.GetDirectories(UserOldWorkFolder).Length > 0 || Directory.GetFiles(UserOldWorkFolder).Length > 0);
                bool testNewWorkFolder = Directory.Exists(mainForm.UserWorkDataFolder) 
                    && Directory.GetDirectories(mainForm.UserWorkDataFolder).Length == 0 
                    && Directory.GetFiles(mainForm.UserWorkDataFolder).Length == 0
                    && File.Exists(Path.Combine(mainForm.canv.MapProject.path, "prjUpd.bin"));

                if (testOldProject && testOldWorkFolder && testNewWorkFolder &&
                    MessageBox.Show("На компьютере присутсвует старая версия Рабочей папки пользователя. Перенести ее в новую версию?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MapProject.tempStr = UserOldProjectFolder;
                    mainForm.worker.EndWork = false;
                    mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.CONVERT_WORK_FOLDER, MapProject);
                }
            }
        }
        public void ConvertWorkFolder(BackgroundWorker worker, DoWorkEventArgs e, string OldProjectPath)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Конвертация Рабочей папки пользователя. Пожалуйста подождите...";
            userState.WorkCurrentTitle = "Конвертация Рабочей папки пользователя. Пожалуйста подождите...";
            userState.Element = "";

            if (Directory.Exists(OldProjectPath))
            {
                string startFolder = App.GetRegistryPath();
                if (startFolder.Length == 0) startFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                string UserOldWorkFolder = startFolder + "\\МиР\\Рабочая папка";

                Project oldProject = new Project(OldProjectPath);
                DirectoryInfo dir = new DirectoryInfo(OldProjectPath);
                oldProject.LoadProjectFile(string.Format("{0}\\{1}.txt", OldProjectPath, dir.Name));
                if (!oldProject.IsSmartProject)
                {
                    oldProject.LoadWellList(worker, e);
                    Project saveProject = MapProject;
                    try
                    {
                        userState.WorkMainTitle = "Конвертация Рабочей папки пользователя. Пожалуйста подождите...";
                        worker.ReportProgress(0, userState);
                        MapProject = oldProject;
                        List<string> errors = ConvertWorkFolder(worker, e, saveProject, UserOldWorkFolder, mainForm.UserWorkDataFolder);
                        e.Result = errors;
                    }
                    finally
                    {
                        MapProject = saveProject;

                    }
                }
            }
        }
        List<string> ConvertWorkFolder(BackgroundWorker worker, DoWorkEventArgs e, Project newProject, string inPath, string outPath)
        {
            List<string> errors = new List<string>();

            if (Directory.Exists(inPath))
            {
                if (!Directory.Exists(outPath)) Directory.CreateDirectory(outPath);
                DirectoryInfo root = new DirectoryInfo(inPath);
                DirectoryInfo[] dirs = root.GetDirectories();
                FileInfo[] files = root.GetFiles();

                for (int i = 0; i < dirs.Length; i++)
                {
                    errors.AddRange(ConvertWorkFolder(worker, e, newProject, inPath + "\\" + dirs[i].Name, outPath + "\\" + dirs[i].Name).ToArray());
                }
                for (int i = 0; i < files.Length; i++)
                {
                    errors.AddRange(ConvertWorkLayer(worker, e, newProject, inPath, outPath, files[i].Name).ToArray());
                }
            }
            return errors;
        }
        List<string> ConvertWorkLayer(BackgroundWorker worker, DoWorkEventArgs e, Project newProject, string inPath, string outPath, string LayerName)
        {
            List<string> errors = new List<string>();
            int ind;
            OilField of;
            C2DLayer layer;
            Project proj;
            string wellName;
            int pos;
            string fileName = inPath + "\\" + LayerName;
            string ext = Path.GetExtension(fileName);
            List<string> err = new List<string>();
            try
            {
                switch (ext)
                {
                    case ".wl":
                        PhantomWell pw;
                        List<int> wIndexes;
                        layer = LoadWorkWellList(fileName);
                        if (layer.ObjectsList.Count == 0) throw new Exception("Не удалось загрузить файл");

                        for (int i = 0; i < layer.ObjectsList.Count; i++)
                        {
                            pw = (PhantomWell)layer.ObjectsList[i];
                            if (pw == null || pw.srcWell == null) throw new Exception(string.Format("Не удалось загрузить список скважин"));
                            wellName = pw.srcWell.Name;
                            ind = newProject.GetOFIndex(pw.srcWell.OilFieldCode);
                            if (ind == -1)
                            {
                                throw new Exception(string.Format("Не найдено месторождение скважины {0}", wellName));
                            }
                            else
                            {
                                of = newProject.OilFields[ind];
                                pos = wellName.IndexOf("_");
                                if (pos > -1)
                                {
                                    wellName = wellName.Remove(pos, wellName.Length - pos);
                                }
                                wIndexes = of.GetWellIndexList(wellName.ToUpper());
                                if (wIndexes.Count == 0)
                                {
                                    err.Add(string.Format("{0} - Не найдена cкважина {1}[{2}]", fileName, pw.srcWell.Name, of.Name));
                                }
                                else if (wIndexes.Count == 1)
                                {
                                    pw.srcWell.Index = wIndexes[0];
                                    pw.srcWell.OilFieldAreaCode = of.Wells[wIndexes[0]].OilFieldAreaCode;
                                }
                                else
                                {
                                    ind = of.GetWellIndex(wellName.ToUpper(), pw.srcWell.OilFieldAreaCode);
                                    if (ind == -1) err.Add(string.Format("{0} - Не найдена cкважина {1}[{2}]", fileName, pw.srcWell.Name, of.Name));
                                    pw.srcWell.Index = ind;
                                }
                            }
                        }
                        if (err.Count == 0)
                        {
                            proj = MapProject;
                            try
                            {
                                MapProject = newProject;
                                WriteWorkLayer(outPath, layer);
                            }
                            finally
                            {
                                MapProject = proj;
                            }
                        }
                        else
                        {
                            errors.AddRange(err.ToArray());
                        }
                        break;
                    case ".prfl":
                        Well w;
                        layer = LoadWorkProfile(fileName);
                        if (layer.ObjectsList.Count == 0) throw new Exception("Не удалось загрузить файл");
                        Profile prfl = (Profile)layer.ObjectsList[0];
                        for (int i = 0; i < prfl.Count; i++)
                        {
                            w = prfl[i];
                            if (w == null) throw new Exception("Не удалось загрузить профиль");
                            wellName = w.Name;
                            ind = newProject.GetOFIndex(w.OilFieldCode);
                            if (ind == -1)
                            {
                                throw new Exception(string.Format("Не найдено месторождение скважины {0}", wellName));
                            }
                            else
                            {
                                of = newProject.OilFields[ind];
                                pos = wellName.IndexOf("_");
                                if (pos > -1)
                                {
                                    wellName = wellName.Remove(pos, wellName.Length - pos);
                                }
                                wIndexes = of.GetWellIndexList(wellName.ToUpper());
                                if (wIndexes.Count == 0)
                                {
                                    err.Add(string.Format("{0} - Не найдена cкважина {1}[{2}]", fileName, w.Name, of.Name));
                                }
                                else if (wIndexes.Count == 1)
                                {
                                    w.Index = wIndexes[0];
                                    w.OilFieldAreaCode = of.Wells[wIndexes[0]].OilFieldAreaCode;
                                }
                                else
                                {
                                    ind = of.GetWellIndex(wellName.ToUpper(), w.OilFieldAreaCode);
                                    if (ind == -1) err.Add(string.Format("{0} - Не найдена cкважина {1}[{2}]", fileName, w.Name, of.Name));
                                    w.Index = ind;
                                }
                            }
                        }
                        if (err.Count == 0)
                        {
                            proj = MapProject;
                            try
                            {
                                MapProject = newProject;
                                WriteWorkLayer(outPath, layer);
                            }
                            finally
                            {
                                MapProject = proj;
                            }
                        }
                        else
                        {
                            errors.AddRange(err.ToArray());
                        }
                        break;
                    case ".mrk":
                        layer = LoadWorkMarker(fileName);
                        if (layer.ObjectsList.Count == 0) throw new Exception("Не удалось загрузить файл");
                        Marker mrk = (Marker)layer.ObjectsList[0];
                        for (int i = 0; i < mrk.Count; i++)
                        {
                            w = mrk.GetWell(i);
                            if (w == null) throw new Exception("Не удалось загрузить маркер");
                            wellName = w.Name;
                            ind = newProject.GetOFIndex(w.OilFieldCode);
                            if (ind == -1)
                            {
                                throw new Exception(string.Format("Не найдено месторождение скважины {0}", wellName));
                            }
                            else
                            {
                                of = newProject.OilFields[ind];
                                pos = wellName.IndexOf("_");
                                if (pos > -1)
                                {
                                    wellName = wellName.Remove(pos, wellName.Length - pos);
                                }
                                wIndexes = of.GetWellIndexList(wellName.ToUpper());
                                if (wIndexes.Count == 0)
                                {
                                    err.Add(string.Format("{0} - Не найдена cкважина {1}[{2}]", fileName, w.Name, of.Name));
                                }
                                else if (wIndexes.Count == 1)
                                {
                                    w.Index = wIndexes[0];
                                    w.OilFieldAreaCode = of.Wells[wIndexes[0]].OilFieldAreaCode;
                                }
                                else
                                {
                                    ind = of.GetWellIndex(wellName.ToUpper(), w.OilFieldAreaCode);
                                    if (ind == -1) err.Add(string.Format("{0} - Не найдена cкважина {1}[{2}]", fileName, w.Name, of.Name));
                                    w.Index = ind;
                                }
                            }
                        }
                        if (err.Count == 0)
                        {
                            proj = MapProject;
                            try
                            {
                                MapProject = newProject;
                                WriteWorkLayer(outPath, layer);
                            }
                            finally
                            {
                                MapProject = proj;
                            }
                        }
                        else
                        {
                            errors.AddRange(err.ToArray());
                        }
                        break;
                    case ".vm":
                        layer = LoadWorkVoronoiMap(fileName);
                        if (layer.ObjectsList.Count == 0) throw new Exception("Не удалось загрузить файл");
                        VoronoiMap vm = (VoronoiMap)layer.ObjectsList[0];
                        for (int i = 0; i < vm.Cells.Length; i++)
                        {
                            w = vm.Cells[i].Well.srcWell;
                            if (w == null) throw new Exception(string.Format("Не удалось загрузить карту Вороного"));
                            wellName = w.Name;
                            ind = newProject.GetOFIndex(w.OilFieldCode);
                            if (ind == -1)
                            {
                                throw new Exception(string.Format("Не найдено месторождение скважины {0}", wellName));
                            }
                            else
                            {
                                of = newProject.OilFields[ind];
                                pos = wellName.IndexOf("_");
                                if (pos > -1)
                                {
                                    wellName = wellName.Remove(pos, wellName.Length - pos);
                                }
                                wIndexes = of.GetWellIndexList(wellName.ToUpper());
                                if (wIndexes.Count == 0)
                                {
                                    err.Add(string.Format("{0} - Не найдена cкважина {1}[{2}]", fileName, w.Name, of.Name));
                                }
                                else if (wIndexes.Count == 1)
                                {
                                    w.Index = wIndexes[0];
                                    w.OilFieldAreaCode = of.Wells[wIndexes[0]].OilFieldAreaCode;
                                }
                                else
                                {
                                    ind = of.GetWellIndex(wellName.ToUpper(), w.OilFieldAreaCode);
                                    if (ind == -1) err.Add(string.Format("{0} - Не найдена cкважина {1}[{2}]", fileName, w.Name, of.Name));
                                    w.Index = ind;
                                }
                            }
                        }
                        if (err.Count == 0)
                        {
                            proj = MapProject;
                            try
                            {
                                MapProject = newProject;
                                WriteWorkLayer(outPath, layer);
                            }
                            finally
                            {
                                MapProject = proj;
                            }
                        }
                        else
                        {
                            errors.AddRange(err.ToArray());
                        }
                        break;
                    default:
                        if (File.Exists(fileName))
                        {
                            File.Copy(inPath + "\\" + LayerName, outPath + "\\" + LayerName, true);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                errors.Add(fileName + " " + ex.Message);
            }
            return errors;
        }


        // Add to Work Layers
        int GetCountName(TreeNodeCollection nodes, string Name)
        {
            int count = 0;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].Text.StartsWith(Name))
                {
                    count++;
                }
            }
            return count;
        }
        string GetNewName(TreeNode ParentNode, Constant.BASE_OBJ_TYPES_ID Type)
        {
            int num = 1;
            string res = "", str, temp;
            switch (Type)
            {
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    res = "Контур ";
                    break;
                case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                    res = "Изображение ";
                    break;
                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                    res = "Список скважин ";
                    break;
                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                    res = "Профиль ";
                    break;
                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                    res = "Маркер ";
                    break;
                default:
                    res = "Неизвестно ";
                    break;
            }
            if (ParentNode.Nodes.Count > 0)
            {
                bool find = false;
                TreeNode tn;
                temp = res + num.ToString();
                while (!find)
                {
                    find = true;
                    for (int i = 0; i < ParentNode.Nodes.Count; i++)
                    {
                        tn = ParentNode.Nodes[i];
                        if ((tn.Tag != null) && ((C2DLayer)tn.Tag).ObjTypeID == Type)
                        {
                            str = ((C2DLayer)tn.Tag).Name;
                            if ((str.IndexOf(temp) > -1) && (str.Length == temp.Length))
                            {
                                find = false;
                                num++;
                                temp = res + num.ToString();
                            }
                        }
                    }
                }
                res = res + num.ToString();
            }
            return res;
        }
        Color GetMarkerLastColor(TreeNode ParentNode)
        {
            TreeNode tn;
            Color lastClr = Color.Black;

            for (int i = 0; i < ParentNode.Nodes.Count; i++)
            {
                tn = ParentNode.Nodes[i];
                if ((tn.Tag != null) && (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER))
                {
                    lastClr = ((Marker)((C2DLayer)tn.Tag).ObjectsList[0]).PenBound.Color;
                }
            }
            return lastClr;
        }
        public void AddNewContour()
        {
            if ((newContour != null) && (newContour._points != null))
            {
                newContour._Closed = true;
                newContour._Fantom = false;

                C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, 0);
                bool isParentRoot = true;
                TreeNode tn, parentNode = twWork.Nodes[0];
                newContour.SetMinMax();
                newContour.GetCenter();
                if ((twWork.SelectedNodes.Count == 1) && (twWork.SelectedNodes[0].Tag != null) &&
                    (((C2DLayer)twWork.SelectedNodes[0].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    parentNode = twWork.SelectedNodes[0];
                    isParentRoot = false;
                }

                newContour.Name = GetNewName(parentNode, Constant.BASE_OBJ_TYPES_ID.CONTOUR);
                layer.Name = newContour.Name;
                layer.Visible = true;
                layer.Add((C2DObject)newContour);
                layer.GetObjectsRect();
                layer.fontSize = this._fontSize;
                layer.ResetFontsPens();
                if (isParentRoot)
                {
                    layer.Index = LayerWorkList.Count;
                    LayerWorkList.Add(layer);
                }
                else
                {
                    layer.Index = ((C2DLayer)parentNode.Tag).ObjectsList.Count;
                    ((C2DLayer)parentNode.Tag).ObjectsList.Add(layer);
                }
                tn = parentNode.Nodes.Add("", layer.Name, 14, 14);
                if (!parentNode.IsExpanded) parentNode.Expand();
                tn.Tag = layer;
                tn.Checked = layer.Visible;
                layer.node = tn;
                tn.ContextMenuStrip = twWork.cMenu;
                //twWork.SelectedNode = tn;
                twWork.SetOneSelectedNode(tn);
                this.WorkLayerActive = layer;
                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER);
                DrawLayerList();
            }
            newContour = null;
            newContourEnd = false;
        }
        public void AddNewImage(Image img)
        {
            TreeNode tn, parentNode = twWork.Nodes[0];
            bool isParentRoot = true;
            C2DLayer layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.IMAGE, 0);
            if ((twWork.SelectedNodes.Count == 1) && (twWork.SelectedNodes[0].Tag != null) &&
                (((C2DLayer)twWork.SelectedNodes[0].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                parentNode = twWork.SelectedNodes[0];
                isParentRoot = false;
            }
            img.dx += (C2DObject.st_BitMapRect.X + this.ClientRectangle.Width / 2) * C2DObject.st_XUnitsInOnePixel;
            img.dy += (C2DObject.st_BitMapRect.Y + this.ClientRectangle.Height / 2) * C2DObject.st_XUnitsInOnePixel;
            layer.Add((C2DObject)img);
            layer.Name = img.Name;
            int count = GetCountName(parentNode.Nodes, img.Name);
            if (count > 0)
            {
                layer.Name = string.Format("{0}({1})", img.Name, count);
            }
            
            layer.Visible = true;
            layer.VisibleFar = true;
            layer.Level = -1;
            layer.fontSize = this._fontSize;
            layer.ResetFontsPens();
            if (isParentRoot)
            {
                layer.Index = LayerWorkList.Count;
                LayerWorkList.Add(layer);
            }
            else
            {
                layer.Index = ((C2DLayer)parentNode.Tag).ObjectsList.Count;
                ((C2DLayer)parentNode.Tag).ObjectsList.Add(layer);
            }
            tn = parentNode.Nodes.Add("", layer.Name, 23, 23);
            tn.Tag = layer;
            tn.Checked = layer.Visible;
            layer.node = tn;
            tn.ContextMenuStrip = twWork.cMenu;
            //twWork.SelectedNode = tn;
            twWork.SetOneSelectedNode(tn);
            WriteWorkLayer(layer);
        }
        public void AddWellLayer(bool AddEmptyList)
        {
            if ((MapProject != null) && (twWork.Nodes.Count > 0))
            {
                C2DLayer newLayer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL, 0);

                int i, j, len;
                TreeNode tn, parentNode = twWork.Nodes[0];
                bool isParentRoot = true;
                len = 1;
                string str;
                C2DLayer layer;
                PhantomWell pw;
                Well w;
                OilField of;
                if (!AddEmptyList)
                {
                    for (i = 0; i < selWellList.Count; i++)
                    {
                        w = (Well)selWellList[i];
                        if ((w.Visible) && (w.Selected) && (newLayer.GetIndexByWell(w.OilFieldIndex, w.Index) == -1))
                        {
                            of = MapProject.OilFields[w.OilFieldIndex];
                            pw = new PhantomWell(of.OilFieldCode, w);
                            newLayer.Add((C2DObject)pw);
                        }
                    }
                }
                if ((twWork.SelectedNodes.Count == 1) && (twWork.SelectedNodes[0].Tag != null) &&
                    (((C2DLayer)twWork.SelectedNodes[0].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    parentNode = twWork.SelectedNodes[0];
                    isParentRoot = false;
                }
                newLayer.SetFontsRecursive(iconFonts, exFontList);

                newLayer.Name = GetNewName(parentNode, Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL);
                newLayer.Visible = true;
                newLayer.VisibleName = false;
                newLayer.GetObjectsRect();
                newLayer.fontSize = this._fontSize;
                newLayer.ResetFontsPens();
                if (isParentRoot)
                {
                    newLayer.Index = LayerWorkList.Count;
                    LayerWorkList.Add(newLayer);
                }
                else
                {
                    newLayer.Index = ((C2DLayer)parentNode.Tag).ObjectsList.Count;
                    ((C2DLayer)parentNode.Tag).ObjectsList.Add(newLayer);
                }

                tn = parentNode.Nodes.Add("", newLayer.Name, 18, 18);
                tn.Tag = newLayer;
                tn.Checked = newLayer.Visible;
                newLayer.node = tn;
                mainForm.UpdateWellListSettings(newLayer);
                tn.ContextMenuStrip = twWork.cMenu;
                this.WorkLayerActive = newLayer;
                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER);
                DrawLayerList();
            }
        }
        public void AddWellLayer(Well[] wellList)
        {
            if (MapProject != null)
            {
                C2DLayer newLayer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL, 0);

                int i, j, len;
                TreeNode tn, parentNode = twWork.Nodes[0];
                bool isParentRoot = true;
                len = 1;
                string str;
                C2DLayer layer;
                PhantomWell pw;
                Well w;
                for (i = 0; i < wellList.Length; i++)
                {
                    w = wellList[i];
                    pw = new PhantomWell((MapProject.OilFields[w.OilFieldIndex]).OilFieldCode, w);
                    newLayer.Add((C2DObject)pw);
                }
                if ((twWork.SelectedNodes.Count == 1) && (twWork.SelectedNodes[0].Tag != null) &&
                    (((C2DLayer)twWork.SelectedNodes[0].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    parentNode = twWork.SelectedNodes[0];
                    isParentRoot = false;
                }
                newLayer.SetFontsRecursive(iconFonts, exFontList);

                newLayer.Name = GetNewName(parentNode, Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL);
                newLayer.Visible = true;
                newLayer.VisibleName = false;
                newLayer.GetObjectsRect();
                newLayer.fontSize = this._fontSize;
                newLayer.ResetFontsPens();
                if (isParentRoot)
                {
                    newLayer.Index = LayerWorkList.Count;
                    LayerWorkList.Add(newLayer);
                }
                else
                {
                    newLayer.Index = ((C2DLayer)parentNode.Tag).ObjectsList.Count;
                    ((C2DLayer)parentNode.Tag).ObjectsList.Add(newLayer);
                }

                tn = parentNode.Nodes.Add("", newLayer.Name, 18, 18);
                tn.Tag = newLayer;
                tn.Checked = newLayer.Visible;
                newLayer.node = tn;
                mainForm.UpdateWellListSettings(newLayer);
                tn.ContextMenuStrip = twWork.cMenu;
                this.WorkLayerActive = newLayer;
                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER);
                DrawLayerList();
            }
        }
        public C2DLayer AddNewProfile()
        {
            if ((MapProject != null) && (selWellList.Count > 0))
            {
                C2DLayer newLayer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PROFILE, 0);

                int i;
                TreeNode tn, parentNode = twWork.Nodes[0];
                bool isParentRoot = true;
                Profile prfl = new Profile();
                for (i = 0; i < selWellList.Count; i++)
                {
                    prfl.AddWell((Well)selWellList[i]);
                }
                if ((twWork.SelectedNodes.Count == 1) && (twWork.SelectedNodes[0].Tag != null) &&
                    (((C2DLayer)twWork.SelectedNodes[0].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    parentNode = twWork.SelectedNodes[0];
                    isParentRoot = false;
                }
                newLayer.Add((C2DObject)prfl);

                newLayer.Name = GetNewName(parentNode, Constant.BASE_OBJ_TYPES_ID.PROFILE);
                newLayer.Visible = true;
                newLayer.VisibleName = false;
                newLayer.GetObjectsRect();
                newLayer.fontSize = this._fontSize;
                newLayer.ResetFontsPens();
                if (isParentRoot)
                {
                    newLayer.Index = LayerWorkList.Count;
                    LayerWorkList.Add(newLayer);
                }
                else
                {
                    newLayer.Index = ((C2DLayer)parentNode.Tag).ObjectsList.Count;
                    ((C2DLayer)parentNode.Tag).ObjectsList.Add(newLayer);
                }
                selLayerList.Add(newLayer);
                tn = parentNode.Nodes.Add("", newLayer.Name, 15, 15);
                tn.Tag = newLayer;
                tn.Checked = newLayer.Visible;
                newLayer.node = tn;
                tn.ContextMenuStrip = twWork.cMenu;
                ClearSelWellList();
                DrawLayerList();
                return newLayer;
            }
            return null;
        }
        public C2DLayer AddNewMarker()
        {
            Marker mrk = null;
            if (MapProject != null)
            {
                C2DLayer newLayer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.MARKER, 0);

                TreeNode tn, parentNode = twWork.Nodes[0];
                bool isParentRoot = true;
                if ((twWork.SelectedNodes.Count == 1) && (twWork.SelectedNodes[0].Tag != null) &&
                    (((C2DLayer)twWork.SelectedNodes[0].Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                {
                    parentNode = twWork.SelectedNodes[0];
                    isParentRoot = false;
                }
                mrk = new Marker();
                newLayer.Add((C2DObject)mrk);
                mrk.Selected = true;
                Color lastClr = Color.Black;
                newLayer.Name = GetNewName(parentNode, Constant.BASE_OBJ_TYPES_ID.MARKER);
                mrk.Name = newLayer.Name;

                newLayer.Visible = true;
                newLayer.VisibleName = false;
                newLayer.GetObjectsRect();
                newLayer.fontSize = this._fontSize;
                lastClr = mrk.GetNextColor(GetMarkerLastColor(parentNode));
                mrk.PenBound = new Pen(lastClr, 7F);
                if (isParentRoot)
                {
                    newLayer.Index = LayerWorkList.Count;
                    LayerWorkList.Add(newLayer);
                }
                else
                {
                    newLayer.Index = ((C2DLayer)parentNode.Tag).ObjectsList.Count;
                    ((C2DLayer)parentNode.Tag).ObjectsList.Add(newLayer);
                }

                tn = parentNode.Nodes.Add("", newLayer.Name, 16, 16);
                tn.Tag = newLayer;
                tn.Checked = newLayer.Visible;
                newLayer.node = tn;
                twWork.SelectNodeByType(Constant.BASE_OBJ_TYPES_ID.MARKER, tn);
                tn.ContextMenuStrip = twWork.cMenu;
                ClearSelWellList();
                DrawLayerList();
                return newLayer;
            }
            return null;
        }
        public void AddNewDataFile(string FileName, MemoryStream ms)
        {
            if (MapProject != null)
            {
                C2DLayer newLayer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.DATAFILE, 0);

                TreeNode tn, parentNode = twWork.Nodes[0];
                int count = GetCountName(parentNode.Nodes, Path.GetFileNameWithoutExtension(FileName));
                if (count > 0)
                {
                    FileName = string.Format("{0}({1}){2}", Path.GetFileNameWithoutExtension(FileName), count, Path.GetExtension(FileName));
                }

                FileStream fs = new FileStream(mainForm.UserWorkDataFolder + "\\" + FileName, FileMode.Create);
                ms.WriteTo(fs);
                fs.Flush();
                fs.Close();
                DataFile file = new DataFile(mainForm.UserWorkDataFolder, FileName);
                newLayer.Add(file);
                newLayer.Name = file.Name;
                newLayer.Index = LayerWorkList.Count;
                LayerWorkList.Add(newLayer);

                string ext = Path.GetExtension(FileName);
                int index = twWork.GetFileIconIndex(ext);
                tn = parentNode.Nodes.Add("", newLayer.Name, index, index);
                tn.Tag = newLayer;
                tn.Checked = true;
                newLayer.node = tn;
                tn.ContextMenuStrip = twWork.cMenu;
                twWork.SetOneSelectedNode(tn);
            }
        }
        public void OpenNewDataFile(string FileName, MemoryStream ms)
        {
            if (MapProject != null)
            {
                string cachePath = mainForm.UserSmartPlusFolder + "\\Файлы";
                if (!Directory.Exists(cachePath)) Directory.CreateDirectory(cachePath);
                DirectoryInfo root = new DirectoryInfo(cachePath);
                
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(FileName);
                FileInfo[] files = root.GetFiles();
                int counter = 0;
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Name.StartsWith(fileNameWithoutExt)) counter++;
                }
                if(counter > 0)
                {
                    fileNameWithoutExt += " " + counter.ToString();
                }

                FileStream fs = new FileStream(cachePath + "\\" + fileNameWithoutExt + Path.GetExtension(FileName), FileMode.Create);
                ms.WriteTo(fs);
                fs.Flush();
                fs.Close();
                if (File.Exists(cachePath + "\\" + fileNameWithoutExt + Path.GetExtension(FileName)))
                {
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = cachePath + "\\" + fileNameWithoutExt + Path.GetExtension(FileName);
                    proc.StartInfo.UseShellExecute = true;
                    proc.Start();
                }
            }
        }

        // FIND WORK LAYER
        public C2DLayer GetWorkLayerByObjType(BaseObj obj)
        {
            C2DLayer res = null;
            List<C2DLayer> list = new List<C2DLayer>();
            int i;
            for (i = 0; i < LayerWorkList.Count; i++)
            {
                ((C2DLayer)LayerWorkList[i]).FillLayersByObjTypeID(obj.TypeID, list);
            }
            for (i = 0; i < list.Count; i++)
            {
                if (((C2DLayer)list[i]).ObjectsList.Count > 0)
                {
                    if (((C2DLayer)list[i]).ObjectsList[0] == obj)
                    {
                        return (C2DLayer)list[i];
                    }
                }
            }
            return res;
        }

        // Copy Paste WorkLayers
        public void CopyWorkLayer(C2DLayer layer)
        {
            copyLayer = layer.Clone();
        }
        public TreeNode PasteWorkLayer(bool CopyName)
        {
            return PasteWorkLayer(CopyName, true);
        }
        public TreeNode PasteWorkLayer(bool CopyName, bool SaveLayer)
        {
            TreeNode retValue = null;
            //if ((selLayerList != null) && (selLayerList.Count > 0))
            //{
            //    ClearSelectedList();
            //}

            if (copyLayer != null)
            {
                int i, len, ind;
                TreeNode tn;
                len = 1;
                if (!CopyName)
                {
                    string str;
                    bool find = true;
                    ind = 1;
                    while (find)
                    {
                        find = false;
                        for (i = 0; i < LayerWorkList.Count; i++)
                        {
                            str = ((C2DLayer)LayerWorkList[i]).Name;
                            switch (copyLayer.ObjTypeID)
                            {
                                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                                    if (str.StartsWith(string.Format("Новый контур {0}", ind))) find = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.GRID:
                                    if (str.StartsWith(string.Format("Новая сетка {0}", ind))) find = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                                    if (str.StartsWith(string.Format("Список скважин {0}", ind))) find = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.PVT:
                                    PVTWorkObject pvt = (PVTWorkObject)copyLayer.ObjectsList[0];
                                    if (ind == 0)
                                    {
                                        if (str.StartsWith(String.Format("PVT ({0}, {1})", pvt.OilFieldName, pvt.AreaName))) find = true;
                                    }
                                    else
                                    {
                                        if (str.StartsWith(String.Format("PVT ({0}, {1}) {2}", pvt.OilFieldName, pvt.AreaName, ind))) find = true;
                                    }
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.LAYER:
                                    if (str.StartsWith(string.Format("Новая папка {0}", ind))) find = true;
                                    break;
                            }
                            if (find) break;
                        }
                        if (find) ind++;
                    }
                    switch (copyLayer.ObjTypeID)
                    {
                        case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                            copyLayer.Name = String.Format("Новый контур {0}", ind);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.GRID:
                            copyLayer.Name = String.Format("Новая сетка {0}", ind);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                            copyLayer.Name = String.Format("Список скважин {0}", ind);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PVT:
                            PVTWorkObject pvt = (PVTWorkObject)copyLayer.ObjectsList[0];
                            if (ind == 0)
                            {
                                copyLayer.Name = String.Format("PVT ({0}, {1})", pvt.OilFieldName, pvt.AreaName);
                            }
                            else
                            {
                                copyLayer.Name = String.Format("PVT ({0}, {1}) {2}", pvt.OilFieldName, pvt.AreaName, ind);
                            }
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.LAYER:
                            copyLayer.Name = String.Format("Новая папка {0}", ind);
                            break;
                    }
                }
                //copyLayer.Selected = true;

                //if ((!CopyName) && (copyLayer.ObjectsList.Count > 0))
                //    ((BaseObj)copyLayer.ObjectsList[0]).Name = copyLayer.Name;

                //copyLayer.Visible = true;
                //copyLayer.fontSize = this._fontSize;
                //copyLayer.ResetFontsPens();

                copyLayer.Index = LayerWorkList.Count;
                LayerWorkList.Add(copyLayer);
                int imgIndex = GetImageIndex(copyLayer.ObjTypeID);
                tn = twWork.Nodes[0].Nodes.Add("", copyLayer.Name, imgIndex, imgIndex);
                tn.Tag = copyLayer;
                tn.Checked = copyLayer.Visible;
                tn.ContextMenuStrip = twWork.cMenu;
                copyLayer.node = tn;
                retValue = tn;
                //tn.ContextMenuStrip = twWork.cMenu;
                //twWork.SetOneSelectedNode(tn);
                if (SaveLayer)
                {
                    this.WorkLayerActive = copyLayer;
                    mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER);
                }
                DrawLayerList();
            }
            copyLayer = null;
            return retValue;
        }
        public TreeNode PasteWorkNode(bool CopyName)
        {
            TreeNode tn = null;
            if ((selLayerList != null) && (selLayerList.Count > 0))
            {
                if ((copyNode == null) || (copyNode.TreeView != twActiveOilField))
                    ClearSelectedList();
            }

            if (copyLayer != null)
            {
                //int ind = twWork.Nodes[0].Nodes.Add(copyNode);
                //tn = twWork.Nodes[0].Nodes[ind];
                AddNodeByLayer(twWork.Nodes[0], copyLayer);
                tn = twWork.Nodes[0].Nodes[twWork.Nodes[0].Nodes.Count - 1];

                C2DLayer layer;

                layer = (C2DLayer)tn.Tag;
                //layer.Selected = true;
                tn.Text = layer.Name;
                string str;
                bool find = true;
                int i, ind = 0;
                //////////////////
                if (!CopyName)
                {
                    while (find)
                    {
                        find = false;
                        for (i = 0; i < LayerWorkList.Count; i++)
                        {
                            str = ((C2DLayer)LayerWorkList[i]).Name;
                            switch (copyLayer.ObjTypeID)
                            {
                                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                                    if (str.StartsWith(string.Format("Новый контур {0}", ind))) find = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.GRID:
                                    if (str.StartsWith(string.Format("Новая сетка {0}", ind))) find = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                                    if (str.StartsWith(string.Format("Список скважин {0}", ind))) find = true;
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.PVT:
                                    PVTWorkObject pvt = (PVTWorkObject)copyLayer.ObjectsList[0];
                                    if (ind == 0)
                                    {
                                        if (str.StartsWith(String.Format("PVT ({0}, {1})", pvt.OilFieldName, pvt.AreaName))) find = true;
                                    }
                                    else
                                    {
                                        if (str.StartsWith(String.Format("PVT ({0}, {1}) {2}", pvt.OilFieldName, pvt.AreaName, ind))) find = true;
                                    }
                                    break;
                                case Constant.BASE_OBJ_TYPES_ID.LAYER:
                                    if (str.StartsWith(string.Format("Новая папка {0}", ind))) find = true;
                                    break;
                            }
                            if (find) break;
                        }
                        if (find) ind++;
                    }
                    switch (copyLayer.ObjTypeID)
                    {
                        case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                            copyLayer.Name = String.Format("Новый контур {0}", ind);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.GRID:
                            copyLayer.Name = String.Format("Новая сетка {0}", ind);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                            copyLayer.Name = String.Format("Список скважин {0}", ind);
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.PVT:
                            PVTWorkObject pvt = (PVTWorkObject)copyLayer.ObjectsList[0];
                            if (ind == 0)
                            {
                                copyLayer.Name = String.Format("PVT ({0}, {1})", pvt.OilFieldName, pvt.AreaName);
                            }
                            else
                            {
                                copyLayer.Name = String.Format("PVT ({0}, {1}) {2}", pvt.OilFieldName, pvt.AreaName, ind);
                            }
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.LAYER:
                            copyLayer.Name = String.Format("Новая папка {0}", ind);
                            break;
                    }
                }
                else
                {
                    while (find)
                    {
                        find = false;
                        for (i = 0; i < LayerWorkList.Count; i++)
                        {
                            str = ((C2DLayer)LayerWorkList[i]).Name;
                            if (ind == 0)
                            {
                                find = str.StartsWith(copyLayer.Name);
                            }
                            else
                            {
                                find = str.StartsWith(String.Format("{0} ({1})", copyLayer.Name, ind));
                            }
                            if (find) break;
                        }
                        if (find) ind++;
                    }
                    if (ind > 0)
                    {
                        copyLayer.Name += string.Format(" ({0})", ind);
                    }
                }
                //mainForm.WriteLog("Вставка узла " + layer.Name);
                //layer.Selected = true;


                layer.Visible = true;
                layer.fontSize = this._fontSize;
                layer.ResetFontsPens();
                layer.Index = LayerWorkList.Count;

                LayerWorkList.Add(layer);
                tn.Text = layer.Name;
                if (layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.IMAGE)
                {
                    this.WorkLayerActive = layer;
                    mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.SAVE_WORK_LAYER);
                }
                else
                {
                    mainForm.canv.WriteWorkLayer(layer);
                }
                DrawLayerList();
                //twWork.SelectedNode = tn;
                twWork.SetOneSelectedNode(tn);
                //////////////////

            }
            copyLayer = null;
            return tn;
        }
        public void CopyWorkNode(TreeNode node)
        {
            if (node != null)
            {
                if ((node.Tag != null) && ((BaseObj)node.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    copyLayer = ((C2DLayer)node.Tag).Clone();
                }
            }
        }
        public void LoadDragFiles(string[] files)
        {
            if (files != null)
            {
                string ext;
                for (int i = 0; i < files.Length; i++)
                {
                    ext = Path.GetExtension(files[i]);
                    C2DLayer layer = null;
                    int iconIndex = 0;
                    switch (ext)
                    {
                        case ".img":
                            layer = LoadWorkImage(mainForm.UserWorkDataFolder + "\\" + files[i]);
                            break;
                        case ".cntr":
                            layer = LoadWorkContour(mainForm.UserWorkDataFolder + "\\" + files[i]);
                            break;
                        case ".prfl":
                            layer = LoadWorkProfile(mainForm.UserWorkDataFolder + "\\" + files[i]);
                            break;
                        case ".wl":
                            layer = LoadWorkWellList(mainForm.UserWorkDataFolder + "\\" + files[i]);
                            break;
                        case ".grid":
                            layer = LoadWorkGrid(null, null, mainForm.UserWorkDataFolder + "\\" + files[i], false);
                            break;
                        case ".pvt":
                            layer = LoadWorkPVTObject(mainForm.UserWorkDataFolder + "\\" + files[i]);
                            break;
                        case ".mrk":
                            layer = LoadWorkMarker(mainForm.UserWorkDataFolder + "\\" + files[i]);
                            break;
                        case ".vm":
                            layer = LoadWorkVoronoiMap(mainForm.UserWorkDataFolder + "\\" + files[i]);
                            break;
                        default:
                            if (Path.HasExtension(files[i]))
                            {
                                layer = LoadWorkDataFile(mainForm.UserWorkDataFolder + "\\" + files[i]);
                                iconIndex = twWork.GetFileIconIndex(ext);
                            }
                            break;
                    }
                    if (layer != null)
                    {
                        layer.Index = LayerWorkList.Count;
                        LayerWorkList.Add(layer);
                        iconIndex = GetImageIndex(layer.ObjTypeID);
                        TreeNode tn = twWork.Nodes[0].Nodes.Add("", layer.Name, iconIndex, iconIndex);
                        tn.Tag = layer;
                        tn.Checked = layer.Visible;
                        layer.node = tn;
                        tn.ContextMenuStrip = twWork.cMenu;
                        string name = Path.GetFileNameWithoutExtension(files[i]);
                        if (layer.Name != name && layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                        {
                            layer.Name = name;
                            tn.Text = name;
                            WriteWorkLayer(layer);
                        }
                    }
                }
            }
        }
        private void AddNodeByLayer(TreeNode ParentNode, C2DLayer Layer)
        {
            if ((Layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.WELL) &&
                (Layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL) &&
                (Layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.WELL_PAD))
            {
                int imgIndex = GetImageIndex(Layer.ObjTypeID);
                TreeNode tn = ParentNode.Nodes.Add("", Layer.Name, imgIndex, imgIndex);
                tn.Tag = Layer;
                tn.Checked = Layer.Visible;
                Layer.node = tn;
                tn.ContextMenuStrip = twWork.cMenu;

                if (Layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    for (int i = 0; i < Layer.ObjectsList.Count; i++)
                    {
                        AddNodeByLayer(tn, (C2DLayer)Layer.ObjectsList[i]);
                    }
                }
            }
        }

        // WORK FOLDERS - SAVE LOAD 
        void InsertWorkLayer(C2DLayer layer)
        {
            bool insert = false;
            if (layer != null)
            {
                if (layer.Index > -1)
                {
                    for (int i = 0; i < LayerWorkList.Count; i++)
                    {
                        if (((C2DLayer)LayerWorkList[i]).Index == -1 || layer.Index < ((C2DLayer)LayerWorkList[i]).Index)
                        {
                            LayerWorkList.Insert(i, layer);
                            insert = true;
                            break;
                        }
                    }
                }
                if (!insert) LayerWorkList.Add(layer);
            }
        }
        void InsertWorkLayer(C2DLayer parent, C2DLayer layer)
        {
            if ((layer != null) && (parent.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                bool insert = false;
                if (layer.Index > -1)
                {
                    for (int i = 0; i < parent.ObjectsList.Count; i++)
                    {
                        if (((C2DLayer)parent.ObjectsList[i]).Index == -1 || layer.Index < ((C2DLayer)parent.ObjectsList[i]).Index)
                        {
                            parent.ObjectsList.Insert(i, layer);
                            insert = true;
                            break;
                        }
                    }
                }
                if (!insert) parent.ObjectsList.Add(layer);
            }
        }
        public void ResetWorkLayerIndexes()
        {
            for (int i = 0; i < LayerWorkList.Count; i++)
            {
                ResetWorkLayerIndexes((C2DLayer)LayerWorkList[i]);
                ((C2DLayer)LayerWorkList[i]).Index = i;
            }
        }
        void ResetWorkLayerIndexes(C2DLayer parent)
        {
            if (parent.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                C2DLayer layer;
                for (int i = 0; i < parent.ObjectsList.Count; i++)
                {
                    layer = (C2DLayer)parent.ObjectsList[i];
                    ResetWorkLayerIndexes(layer);
                    layer.Index = i;
                }
            }
        }
        public void LoadWorkLayers(BackgroundWorker worker, DoWorkEventArgs e)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка Рабочей папки...";
            userState.Element = "";


            int i, pos, lenSubFldr, progress, fldrLen;
            C2DLayer layer, subLayer;
            if (!Directory.Exists(mainForm.UserWorkDataFolder)) return;

            fldrLen = mainForm.UserWorkDataFolder.Length;

            string[] files = Directory.GetFiles(mainForm.UserWorkDataFolder);
            string[] subfolders = Directory.GetDirectories(mainForm.UserWorkDataFolder);
            LayerWorkList.Clear();

            lenSubFldr = subfolders.Length;
            LayerWorkList = new System.Collections.ArrayList(files.Length);
            worker.ReportProgress(0, userState);
            for (i = 0; i < lenSubFldr; i++)
            {
                pos = subfolders[i].LastIndexOf("\\");
                userState.Element = subfolders[i].Remove(0, pos + 1);
                progress = (int)(i * 100f / lenSubFldr);
                if (progress > 100) progress = 100;
                worker.ReportProgress(progress, userState);

                subLayer = LoadSubFolder(subfolders[i]);
                if (subLayer != null)
                {
                    subLayer.Name = subfolders[i].Remove(0, pos + 1);
                    subLayer.fontSize = this._fontSize;
                    subLayer.ResetFontsPens();
                    subLayer.GetObjectsRect();
                    InsertWorkLayer(subLayer);
                }
            }
            // загружаем файлы контуров
            for (i = 0; i < files.Length; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                layer = null;
                string ext = Path.GetExtension(files[i]);
                userState.Element = Path.GetFileNameWithoutExtension(files[i]);
                progress = (int)(i * 100f / files.Length);
                if (progress > 100) progress = 100;
                worker.ReportProgress(progress, userState);
                switch (ext)
                {
                    case ".img":
                        layer = LoadWorkImage(files[i]);
                        break;
                    case ".cntr":
                        layer = LoadWorkContour(files[i]);
                        break;
                    case ".wl":
                        layer = LoadWorkWellList(files[i]);
                        break;
                    case ".grid":
                        layer = LoadWorkGrid(null, null, files[i], false);
                        break;
                    case ".prfl":
                        layer = LoadWorkProfile(files[i]);
                        break;
                    case ".mrk":
                        layer = LoadWorkMarker(files[i]);
                        break;
                    case ".pvt":
                        layer = LoadWorkPVTObject(files[i]);
                        break;
                    case ".vm":
                        layer = LoadWorkVoronoiMap(files[i]);
                        break;
                    default:
                        if (ext.Length > 0 && ext != ".setting" && ((File.GetAttributes(files[i]) & FileAttributes.Hidden) != FileAttributes.Hidden))
                        {
                            layer = LoadWorkDataFile(files[i]);
                        }
                        break;
                }
                if (layer != null)
                {
                    InsertWorkLayer(layer);
                }
            }
            ResetWorkLayerIndexes();
            LayerWorkListChanged = false;
            if (files.Length == 0) worker.ReportProgress(100, userState);
        }
        public void TestFilesInDirectory(DirectoryInfo root)
        {
            List<string> extentions = new List<string>(new string[] { ".img", ".cntr", ".prfl", ".pvt", ".mrk", ".wl", ".grid", ".vm" });
            DirectoryInfo[] dirs = root.GetDirectories(); 
            FileInfo[] files = root.GetFiles();
            if (files != null)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    try
                    {
                        if (!files[i].Name.StartsWith("~$") || ((File.GetAttributes(files[i].FullName) & FileAttributes.Hidden) != FileAttributes.Hidden))
                        {
                            FileStream fs = new FileStream(files[i].FullName, FileMode.Open, FileAccess.Read, FileShare.None);
                            fs.Close();
                        }
                    }
                    catch (IOException ex)
                    {
                        string filePath = files[i].FullName.Replace(mainForm.UserSmartPlusFolder, "");
                        throw new IOException(string.Format("Не удается сохранить Рабочую папку!\nФайл '{0}' занят другим процессом .\nЗакройте приложение открывшее данный файл.", filePath));
                    }
                }
            }
            if (dirs != null)
            {
                for (int i = 0; i < dirs.Length; i++)
                {
                    TestFilesInDirectory(dirs[i]);
                }
            }
        }
        public bool WriteWorkLayers(BackgroundWorker worker, DoWorkEventArgs e)
        {
            //string contours_cache;
            int i, iLen;
            bool res = true;
            MessageShown = false;
            if (LayerWorkList.Count == 0) return false;

            for (i = 0; i < LayerWorkList.Count; i++)
            {
                LoadLayersData((C2DLayer)LayerWorkList[i]);
            }

            if (Directory.Exists(mainForm.UserWorkDataFolder))
            {
                try
                {
                    if (Directory.Exists(mainForm.UserWorkDataFolder + "`"))
                    {
                        DirectoryInfo dir = new DirectoryInfo(mainForm.UserWorkDataFolder + "`");
                        if (dir.GetFiles().Length == 0 && dir.GetDirectories().Length == 0)
                        {
                            dir.Delete(true);
                        }
                        else
                        {
                            if (Directory.Exists(mainForm.UserWorkDataFolder + "``")) Directory.Delete(mainForm.UserWorkDataFolder + "``", true);
                            Directory.Move(mainForm.UserWorkDataFolder + "`", mainForm.UserWorkDataFolder + "``");
                        }
                    }
                    Directory.Move(mainForm.UserWorkDataFolder, mainForm.UserWorkDataFolder + "`");
                }
                catch (IOException ex)
                {
                    DirectoryInfo root = new DirectoryInfo(mainForm.UserWorkDataFolder);
                    TestFilesInDirectory(root);
                    throw new IOException("Рабочая папка занята другим процессом.\nЗакройте окна связанные с Рабочей папкой.");
                }
            }
            Directory.CreateDirectory(mainForm.UserWorkDataFolder);
            C2DLayer layer;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "Сохранение данных Рабочей папки...";
            userState.WorkCurrentTitle = "Сохранение данных Рабочей папки...";

            int len = 0;
            iLen = LayerWorkList.Count;
            for (i = 0; i < iLen; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    res = false;
                    break;
                }

                layer = (C2DLayer)LayerWorkList[i];
                userState.Element = layer.Name;
                res = WriteWorkLayer(worker, e, layer) && res;
            }
            WorkLayerActive = null;

            if (res)
            {
                if ((Directory.Exists(mainForm.UserWorkDataFolder + "`")) && (Directory.Exists(mainForm.UserWorkDataFolder)))
                {
                    DirectoryInfo dir = new DirectoryInfo(mainForm.UserWorkDataFolder + "`");
                    if (dir.GetFiles().Length == 0 && dir.GetDirectories().Length == 0)
                    {
                        dir.Delete(true);
                    }
                    else
                    {
                        if (Directory.Exists(mainForm.UserWorkDataFolder + "``")) Directory.Delete(mainForm.UserWorkDataFolder + "``", true);
                        Directory.Move(mainForm.UserWorkDataFolder + "`", mainForm.UserWorkDataFolder + "``");
                    }
                }
            }
            else
            {
                if (Directory.Exists(mainForm.UserWorkDataFolder + "`") && (Directory.Exists(mainForm.UserWorkDataFolder)))
                {
                    DirectoryInfo dir = new DirectoryInfo(mainForm.UserWorkDataFolder);
                    if (dir.Exists) dir.Delete(true);
                    Directory.Move(mainForm.UserWorkDataFolder + "`", mainForm.UserWorkDataFolder);
                }
            }
            for (i = 0; i < LayerWorkList.Count; i++)
            {
                ClearLayersData((C2DLayer)LayerWorkList[i]);
            }
            LayerWorkListChanged = false;
            return res;
        }

        // WORK FOLDER - LOAD WRITE OBJECTS
        private C2DLayer LoadWorkImage(string fileName)
        {
            C2DLayer layer = null;
            if (File.Exists(fileName))
            {
                try
                {
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs, Encoding.UTF8);
                    SmartPlus.Image image;
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.IMAGE, 0);
                    int version = br.ReadInt32(); // version layer of image
                    layer.Index = br.ReadInt32();
                    layer.Visible = br.ReadBoolean();
                    layer.Name = br.ReadString();
                    image = new Image();
                    image.LoadFromCache(MapProject, br);
                    layer.ObjectsList.Add(image);
                    layer.GetObjectsRect();

                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    br.Close();
                }
                catch
                {
                    layer = null;
                }
            }
            return layer;
        }
        private C2DLayer LoadWorkDataFile(string fileName)
        {
            C2DLayer layer = null;
            if (File.Exists(fileName) && (fileName != "fldr.setting"))
            {
                try
                {
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.DATAFILE, 0);
                    DataFile file = new DataFile(Path.GetDirectoryName(fileName), Path.GetFileName(fileName));
                    layer.Index = -1;
                    layer.Name = file.Name;
                    layer.Add((C2DObject)file);
                }
                catch
                {
                    layer = null;
                }
            }
            return layer;
        }
        private C2DLayer LoadWorkContour(string fileName)
        {
            C2DLayer layer = null;
            if (File.Exists(fileName))
            {
                int ver = 1;
                try
                {
                    StreamReader file = new StreamReader(fileName, System.Text.Encoding.GetEncoding(1251));
                    string str = file.ReadLine();
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.CONTOUR, 0);
                    if (str.StartsWith("ver="))
                    {
                        ver = Convert.ToInt32(str.Replace("ver=", ""));
                        layer.Index = Convert.ToInt32(file.ReadLine());
                        layer.Visible = Convert.ToBoolean(file.ReadLine());
                        if (ver > 1) layer.VisibleName = Convert.ToBoolean(file.ReadLine());
                        str = file.ReadLine(); // count
                    }
                    int count = Convert.ToInt32(str);
                    for (int i = 0; i < count; i++)
                    {
                        Contour cntr = new Contour();
                        cntr.ReadFromCache(file);
                        //cntr._FillBrush = 11;
                        layer.Add((C2DObject)cntr);
                    }
                    layer.Name = Path.GetFileNameWithoutExtension(fileName);
                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    layer.GetObjectsRect();
                    layer.GetObjectsMedian();
                    file.Close();
                }
                catch
                {
                    layer = null;
                }
            }
            return layer;
        }
        private C2DLayer LoadWorkWellList(string fileName)
        {
            C2DLayer layer = null;

            if (File.Exists(fileName))
            {
                try
                {
                    StreamReader file = new StreamReader(fileName, System.Text.Encoding.GetEncoding(1251));
                    PhantomWell pw;
                    string str;
                    int i, iLen, ver = -1;
                    char[] delimeter = new char[] { ';' };
                    string[] parseStr;
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL, 0);
                    str = file.ReadLine();
                    if (str.StartsWith("ver="))
                    {
                        layer.Index = Convert.ToInt32(file.ReadLine());
                        layer.Visible = Convert.ToBoolean(file.ReadLine());
                        str = file.ReadLine();
                    }
                    layer.Name = str;
                    str = file.ReadLine();
                    bool drawCaption = false;
                    if (str.IndexOf('e') > -1)
                    {
                        parseStr = str.Split(delimeter, StringSplitOptions.RemoveEmptyEntries);
                        if (parseStr.Length > 3)
                        {
                            layer.VisibleName = Convert.ToBoolean(parseStr[0]);
                            drawCaption = Convert.ToBoolean(parseStr[1]);
                            layer.VisibleLvl.SetVisibleLvl(Convert.ToInt32(parseStr[2]));
                            layer.DrawOverBubble = Convert.ToBoolean(parseStr[3]);
                        }
                        else if (parseStr.Length > 1)
                        {
                            layer.VisibleName = Convert.ToBoolean(parseStr[0]);
                            drawCaption = Convert.ToBoolean(parseStr[1]);
                        }
                        else if (parseStr.Length > 0)
                        {
                            layer.VisibleName = Convert.ToBoolean(parseStr[0]);
                        }
                        iLen = Convert.ToInt32(file.ReadLine());
                    }
                    else
                    {
                        iLen = Convert.ToInt32(str);
                    }

                    for (i = 0; i < iLen; i++)
                    {
                        str = file.ReadLine();
                        str = str.Replace('"', ' ');
                        parseStr = str.Split(delimeter);
                        pw = new PhantomWell(-1, null);
                        if (pw.ReadFromStr(MapProject, parseStr, iconFonts, exFontList))
                        {
                            pw.VisibleName = layer.VisibleName;
                            layer.Add(pw);
                        }
                    }
                    layer.DrawCaption = drawCaption;
                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    layer.GetObjectsRect();
                    file.Close();
                }
                catch
                {
                    layer = null;
                }

            }
            return layer;
        }

        private C2DLayer LoadWorkProfile(string fileName)
        {
            C2DLayer layer = null;
            if (File.Exists(fileName))
            {
                try
                {
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs, Encoding.GetEncoding(1251));
                    Profile prof;
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PROFILE, 0);
                    string str = br.ReadString();
                    if (str.StartsWith("ver="))
                    {
                        layer.Index = br.ReadInt32();
                        layer.Visible = br.ReadBoolean();
                        str = br.ReadString();
                    }
                    layer.Name = str;
                    prof = new Profile();
                    prof.LoadFromCache(MapProject, br);
                    layer.ObjectsList.Add(prof);
                    layer.GetObjectsRect();

                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    br.Close();
                }
                catch
                {
                    layer = null;
                }
            }
            return layer;
        }
        private C2DLayer LoadWorkMarker(string fileName)
        {
            C2DLayer layer = null;
            if (File.Exists(fileName))
            {
                try
                {
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs, Encoding.GetEncoding(1251));
                    Marker mrk;
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.MARKER, 0);
                    string str = br.ReadString();
                    if (str.StartsWith("ver="))
                    {
                        layer.Index = br.ReadInt32();
                        layer.Visible = br.ReadBoolean();
                        str = br.ReadString();
                    }
                    layer.Name = str;
                    mrk = new Marker();
                    mrk.Name = layer.Name;
                    mrk.LoadFromCache(MapProject, br);
                    layer.ObjectsList.Add(mrk);
                    layer.GetObjectsRect();

                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    br.Close();
                }
                catch
                {
                    layer = null;
                }
            }
            return layer;
        }
        private C2DLayer LoadWorkPVTObject(string fileName)
        {
            C2DLayer layer = null;
            if (File.Exists(fileName))
            {
                try
                {
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    StreamReader sr = new StreamReader(fs, Encoding.GetEncoding(1251));
                    PVTWorkObject pvt = new PVTWorkObject();
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PVT, 0);
                    layer.Index = Convert.ToInt32(sr.ReadLine());
                    layer.Visible = Convert.ToBoolean(sr.ReadLine());
                    layer.Name = sr.ReadLine();
                    if (pvt.LoadFromCache(sr))
                    {
                        layer.ObjectsList.Add(pvt);
                    }
                    else
                    {
                        layer = null;
                    }
                    sr.Close();
                }
                catch
                {
                    layer = null;
                }
            }
            return layer;
        }
        private C2DLayer LoadWorkVoronoiMap(string fileName)
        {
            C2DLayer layer = null;
            if (File.Exists(fileName))
            {
                try
                {
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    VoronoiMap map;
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP, 0);
                    string str = br.ReadString().Replace("ver=", "");
                    int version;
                    int.TryParse(str, out version);
                    layer.Index = br.ReadInt32();
                    layer.Visible = br.ReadBoolean();
                    str = br.ReadString();
                    layer.Name = str;
                    map = new VoronoiMap();
                    map.Name = layer.Name;
                    map.LoadFromCache(MapProject, br, version);
                    map.SetIconsFonts(iconFonts, exFontList);
                    layer.ObjectsList.Add(map);
                    layer.GetObjectsRect();

                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    br.Close();
                }
                catch
                {
                    layer = null;
                }
            }
            return layer;
        }
        private bool WriteWorkImage(C2DLayer layer, string cacheName)
        {
            if (File.Exists(cacheName)) File.Delete(cacheName);

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs, Encoding.UTF8);
            bw.Write(0);            // version layer of Image 
            bw.Write(layer.Index);
            bw.Write(layer.Visible);
            bw.Write(layer.Name);
            SmartPlus.Image image = ((SmartPlus.Image)layer.ObjectsList[0]);
            image.WriteToCache(bw);
            bw.Close();
            return true;
        }
        private bool WriteWorkContour(C2DLayer layer, string cacheName)
        {
            if (File.Exists(cacheName)) File.Delete(cacheName);

            StreamWriter contours_file = new StreamWriter(cacheName, false, System.Text.Encoding.GetEncoding(1251));
            contours_file.WriteLine("ver=2");
            contours_file.WriteLine(layer.Index);
            contours_file.WriteLine(layer.Visible);
            contours_file.WriteLine(layer.VisibleName);
            contours_file.WriteLine(layer.ObjectsList.Count);
            for (int j = 0; j < layer.ObjectsList.Count; j++)
            {
                ((Contour)layer.ObjectsList[j]).WriteToCache(layer.Index, contours_file);
            }
            contours_file.Close();
            return true;
        }
        private bool WriteWorkWellList(C2DLayer layer, string cacheName)
        {
            if (File.Exists(cacheName)) File.Delete(cacheName);

            StreamWriter file = new StreamWriter(cacheName, false, Encoding.GetEncoding(1251));
            file.WriteLine("ver=1");
            file.WriteLine(layer.Index);
            file.WriteLine(layer.Visible);
            file.WriteLine(layer.Name);
            file.WriteLine(string.Format("{0};{1};{2};{3};", layer.VisibleName, layer.DrawCaption, layer.VisibleLvl.GetVisibleLvl(), layer.DrawOverBubble));
            file.WriteLine(layer.ObjectsList.Count);
            for (int j = 0; j < layer.ObjectsList.Count; j++)
            {
                ((PhantomWell)layer.ObjectsList[j]).WriteToCache(file);
            }
            file.Close();
            return true;
        }

        private bool WriteWorkProfile(C2DLayer layer, string cacheName)
        {
            if (File.Exists(cacheName)) File.Delete(cacheName);

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs, Encoding.GetEncoding(1251));
            bw.Write("ver=0");
            bw.Write(layer.Index);
            bw.Write(layer.Visible);
            bw.Write(layer.Name);
            ((Profile)layer.ObjectsList[0]).WriteToCache(bw);
            bw.Close();
            return true;
        }
        private bool WriteWorkMarker(C2DLayer layer, string cacheName)
        {
            if (File.Exists(cacheName)) File.Delete(cacheName);

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs, Encoding.GetEncoding(1251));
            bw.Write("ver=0");
            bw.Write(layer.Index);
            bw.Write(layer.Visible);
            bw.Write(layer.Name);
            ((Marker)layer.ObjectsList[0]).WriteToCache(bw);
            bw.Close();
            return true;
        }
        private bool WriteWorkPvt(C2DLayer layer, string cacheName)
        {
            if (File.Exists(cacheName)) File.Delete(cacheName);

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs, Encoding.GetEncoding(1251));
            sw.WriteLine(layer.Index.ToString());
            sw.WriteLine(layer.Visible.ToString());
            sw.WriteLine(layer.Name);
            ((PVTWorkObject)layer.ObjectsList[0]).WriteToCache(sw);
            sw.Close();
            return true;
        }
        private bool WriteWorkVoronoiMap(C2DLayer layer, string cacheName)
        {
            if (File.Exists(cacheName)) File.Delete(cacheName);

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write("ver=1");
            bw.Write(layer.Index);
            bw.Write(layer.Visible);
            bw.Write(layer.Name);
            ((VoronoiMap)layer.ObjectsList[0]).WriteToCache(bw);
            bw.Close();
            return true;
        }

        // LAYERS DATA
        private void LoadLayersData(C2DLayer root)
        {
            if (root.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                for (int i = 0; i < root.ObjectsList.Count; i++)
                {
                    LoadLayersData((C2DLayer)root.ObjectsList[i]);
                }
            }
            else if (root.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
            {
                LoadWorkGridPackedData(root);
            }
        }
        private void ClearLayersData(C2DLayer root)
        {
            if (root.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                for (int i = 0; i < root.ObjectsList.Count; i++)
                {
                    ClearLayersData((C2DLayer)root.ObjectsList[i]);
                }
            }
            else if (root.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
            {
                ClearWorkGridData(root, false);
            }
        }

        // WORK FOLDER - LOAD WRITE GRIDS
        public void LoadWorkGrid(BackgroundWorker worker, DoWorkEventArgs e, C2DLayer layer)
        {
            string fileName = mainForm.UserSmartPlusFolder + "\\" + layer.node.FullPath + ".grid";
            C2DLayer lr = LoadWorkGrid(worker, e, fileName, true);
            if (lr != null) layer.GetClone(lr);
        }
        private C2DLayer LoadWorkGrid(BackgroundWorker worker, DoWorkEventArgs e, string fileName, bool LoadData)
        {
            C2DLayer layer = null;
            FileStream fs = null;
            if (File.Exists(fileName))
            {
                try
                {
                    fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs, Encoding.GetEncoding(1251));
                    Grid grid;
                    layer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.GRID, 0);
                    string str = br.ReadString();
                    int ver = 0;
                    if (str.StartsWith("ver="))
                    {
                        layer.Index = br.ReadInt32();
                        layer.Visible = br.ReadBoolean();
                        ver = Convert.ToInt32(str.Remove(0, 4));
                        str = br.ReadString();
                    }
                    layer.Name = str;
                    grid = new Grid();
                    if (LoadData)
                    {
                        grid.LoadFromCache(worker, e, fs, true);
                        layer.Visible = true;
                    }
                    else
                    {
                        layer.Visible = false;
                        int len = br.ReadInt32();
                        fs.Seek(len, SeekOrigin.Current);
                    }
                    if (ver > 0)
                    {
                        grid.OilFieldCode = br.ReadInt32();
                        grid.StratumCode = br.ReadInt32();
                    }
                    layer.ObjectsList.Add(grid);
                    layer.GetObjectsRect();

                    layer.fontSize = this._fontSize;
                    layer.ResetFontsPens();
                    br.Close();
                }
                catch
                {
                    layer = null;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
            }
            return layer;
        }
        private bool WriteWorkGrid(C2DLayer layer, string cacheName)
        {
            FileStream fs;
            var grid = (Grid)layer.ObjectsList[0];
            if(!grid.DataLoaded && !grid.PackedDataLoaded) 
            {
                fs = new FileStream(cacheName, FileMode.Create);
                grid.LoadPackedData(fs);
                fs.Close();
            }
            if (File.Exists(cacheName)) File.Delete(cacheName);

            fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs, Encoding.GetEncoding(1251));
            bw.Write("ver=1");
            bw.Write(layer.Index);
            bw.Write(layer.Visible);
            bw.Write(layer.Name);
            
            grid.WriteToCache(fs);
            bw.Write(grid.OilFieldCode);
            bw.Write(grid.StratumCode);
            bw.Close();
            fs.Close();
            return true;
        }
        private void LoadWorkGridPackedData(C2DLayer layer)
        {
            if (layer.ObjectsList.Count > 0 && layer.node != null)
            {
                Grid grid = (Grid)layer.ObjectsList[0];
                if (!grid.DataLoaded && !grid.PackedDataLoaded)
                {
                    string fileName = mainForm.UserSmartPlusFolder + "\\" + layer.node.FullPath + ".grid";
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs, Encoding.GetEncoding(1251));
                    br.ReadString();
                    fs.Seek(5, SeekOrigin.Current);
                    br.ReadString();
                    grid.LoadPackedData(fs);
                    br.Close();
                }
            }
        }
        public void ClearWorkGridData(C2DLayer layer, bool ClearAll)
        {
            if (layer.ObjectsList.Count > 0 && layer.node != null)
            {
                Grid grid = (Grid)layer.ObjectsList[0];
                if (!ClearAll)
                {
                    grid.ClearPackedData();
                }
                else
                {
                    grid.FreeDataMemory();
                }
                GC.GetTotalMemory(true);
            }
        }
        
        // WORK FOLDER - LOAD WRITE SUB FOLDER
        private C2DLayer LoadSubFolder(string SubFolderName)
        {
            int i, pos;
            C2DLayer layer;
            if (!Directory.Exists(SubFolderName)) return null;
            string[] settings = Directory.GetFiles(SubFolderName, "fldr.setting");
            string[] files = Directory.GetFiles(SubFolderName);
            string[] subfolders = Directory.GetDirectories(SubFolderName);
            C2DLayer subLayer;
            C2DLayer layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, 0);

            if (settings.Length == 1)
            {
                FileStream fs = new FileStream(settings[0], FileMode.Open);
                BinaryReader br = new BinaryReader(fs, Encoding.GetEncoding(1251));
                if (br.ReadInt32() == 0)
                {
                    layerGroup.Index = br.ReadInt32();
                    layerGroup.Visible = br.ReadBoolean();
                }
                br.Close();
            }
            for (i = 0; i < subfolders.Length; i++)
            {
                subLayer = LoadSubFolder(subfolders[i]);
                pos = subfolders[i].LastIndexOf("\\");
                if (subLayer != null)
                {
                    subLayer.Name = subfolders[i].Remove(0, pos + 1);
                    subLayer.fontSize = this._fontSize;
                    subLayer.ResetFontsPens();
                    subLayer.GetObjectsRect();
                    InsertWorkLayer(layerGroup, subLayer);
                }
            }

            for (i = 0; i < files.Length; i++)
            {
                layer = null;
                string ext = Path.GetExtension(files[i]);
                switch (ext)
                {
                    case ".img":
                        layer = LoadWorkImage(files[i]);
                        break;
                    case ".cntr":
                        layer = LoadWorkContour(files[i]);
                        break;
                    case ".wl":
                        layer = LoadWorkWellList(files[i]);
                        break;
                    case ".grid":
                        layer = LoadWorkGrid(null, null, files[i], false);
                        break;
                    case ".prfl":
                        layer = LoadWorkProfile(files[i]);
                        break;
                    case ".mrk":
                        layer = LoadWorkMarker(files[i]);
                        break;
                    case ".pvt":
                        layer = LoadWorkPVTObject(files[i]);
                        break;
                    case ".vm":
                        layer = LoadWorkVoronoiMap(files[i]);
                        break;
                    default:
                        if (ext.Length > 0 && ext != ".setting" && ((File.GetAttributes(files[i]) & FileAttributes.Hidden) != FileAttributes.Hidden))
                        {
                            layer = LoadWorkDataFile(files[i]);
                        }
                        break;
                }
                if (layer != null)
                {
                    InsertWorkLayer(layerGroup, layer);
                }
            }
            bool res = false;
            i = 0;
            while (i < layerGroup.ObjectsList.Count)
            {
                if (layerGroup.ObjectsList[i] == null)
                {
                    layerGroup.ObjectsList.RemoveAt(i);
                    res = true;
                }
                else
                    i++;
            }
            return layerGroup;
        }
        private bool WriteSubWorkLayer(BackgroundWorker worker, DoWorkEventArgs e, C2DLayer layer, string targetPath)
        {
            bool res = false;
            string subTargetPath = targetPath + "\\" + layer.Name;
            string subFldrSettings = subTargetPath + "\\fldr.setting";

            int iLen = layer.ObjectsList.Count;
            C2DLayer subLayer;
            string cache_name;
            if (Directory.Exists(subTargetPath)) Directory.Delete(subTargetPath, true);
            Directory.CreateDirectory(subTargetPath);
            if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
            {
                FileStream fs = new FileStream(subFldrSettings, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs, Encoding.GetEncoding(1251));
                bw.Write(0);
                bw.Write(layer.Index);
                bw.Write(layer.Visible);
                bw.Close();
            }
            res = true;
            for (int i = 0; i < iLen; i++)
            {
                subLayer = (C2DLayer)layer.ObjectsList[i];
                if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    res = WriteSubWorkLayer(worker, e, subLayer, subTargetPath);
                    if (!res) break;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    cache_name = subTargetPath + "\\" + subLayer.Name + ".cntr";
                    if ((cache_name.IndexOf("Ячейка") > -1) && (cache_name.IndexOf("\\Ячейки\\") == -1))
                    {
                        cache_name = subTargetPath + "\\Ячейки\\" + subLayer.Name + ".cntr";
                        if (!Directory.Exists(subTargetPath + "\\Ячейки"))
                        {
                            Directory.CreateDirectory(subTargetPath + "\\Ячейки");
                        }
                    }

                    if (File.Exists(cache_name))
                    {
                        cache_name = String.Format("{0}\\{1} ({2}).cntr", subTargetPath, subLayer.Name, i);
                    }

                    res = WriteWorkContour(subLayer, cache_name);
                    if (!res) break;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    cache_name = subTargetPath + "\\" + subLayer.Name + ".wl";
                    res = WriteWorkWellList(subLayer, cache_name);
                    if (!res) break;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                {
                    cache_name = subTargetPath + "\\" + subLayer.Name + ".grid";
                    res = WriteWorkGrid(subLayer, cache_name);
                    if (!res) break;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
                {
                    cache_name = subTargetPath + "\\" + subLayer.Name + ".img";
                    res = WriteWorkImage(subLayer, cache_name);
                    if (!res) break;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE)
                {
                    cache_name = subTargetPath + "\\" + subLayer.Name + ".prfl";
                    res = WriteWorkProfile(subLayer, cache_name);
                    if (!res) break;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                {
                    cache_name = subTargetPath + "\\" + subLayer.Name + ".mrk";
                    res = WriteWorkMarker(subLayer, cache_name);
                    if (!res) break;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT)
                {
                    cache_name = subTargetPath + "\\" + subLayer.Name + ".pvt";
                    res = WriteWorkPvt(subLayer, cache_name);
                    if (!res) break;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                {
                    DataFile file = (DataFile)subLayer.ObjectsList[0];
                    cache_name = subTargetPath + "\\" + file.Name;
                    cache_name = cache_name.Replace("Рабочая папка", "Рабочая папка`");
                    if (File.Exists(cache_name))
                    {
                        File.Copy(cache_name, subTargetPath + "\\" + file.Name, true);
                        res = true;
                    }
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                {
                    cache_name = subTargetPath + "\\" + subLayer.Name + ".vm";
                    res = WriteWorkVoronoiMap(subLayer, cache_name);
                    if (!res) break;
                }
                if (worker != null) worker.ReportProgress(100);
            }
            return res;
        }
        public bool WriteWorkLayer(C2DLayer layer)
        {
            return WriteWorkLayer(null, null, layer);
        }
        public bool WriteWorkLayer(string FilePath, C2DLayer layer)
        {
            WorkerState userState = new WorkerState();
            userState.WorkCurrentTitle = "Сохранение данных Рабочей папки...";
            bool res = false;
            string fileName;
            if (layer != null && Directory.Exists(FilePath))
            {
                string name = FilePath + "\\" + layer.Name;
                if (!Directory.Exists(mainForm.UserSmartPlusFolder)) Directory.CreateDirectory(mainForm.UserSmartPlusFolder);
                if (!Directory.Exists(mainForm.UserWorkDataFolder)) Directory.CreateDirectory(mainForm.UserWorkDataFolder);
                if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    if (Directory.Exists(FilePath))
                    {
                        fileName = name + ".cntr";
                        res = WriteWorkContour(layer, fileName);
                    }
                }
                if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    if (Directory.Exists(FilePath))
                    {
                        fileName = name + ".wl";
                        res = WriteWorkWellList(layer, fileName);
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                {
                    if (Directory.Exists(FilePath))
                    {
                        fileName = name + ".grid";
                        res = WriteWorkGrid(layer, fileName);
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
                {
                    if (Directory.Exists(FilePath))
                    {
                        fileName = name + ".img";
                        res = WriteWorkImage(layer, fileName);
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE)
                {
                    if (Directory.Exists(FilePath))
                    {
                        fileName = name + ".prfl";
                        res = WriteWorkProfile(layer, fileName);
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                {
                    if (Directory.Exists(FilePath))
                    {
                        fileName = name + ".mrk";
                        res = WriteWorkMarker(layer, fileName);
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT)
                {
                    if (Directory.Exists(FilePath))
                    {
                        fileName = name + ".pvt";
                        res = WriteWorkPvt(layer, fileName);
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                {
                    if (Directory.Exists(FilePath))
                    {
                        fileName = name + ".vm";
                        res = WriteWorkVoronoiMap(layer, fileName);
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                {
                    DataFile file = (DataFile)layer.ObjectsList[0];
                    fileName = FilePath + "\\" + file.Name;
                    fileName = fileName.Replace("Рабочая папка", "Рабочая папка`");
                    if (File.Exists(fileName))
                    {
                        File.Copy(fileName, FilePath + "\\" + file.Name, true);
                        res = true;
                    }
                }
                else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    res = WriteSubWorkLayer(null, null, layer, FilePath);
                }
            }
            WorkLayerActive = null;
            return res;
        }
        public bool WriteWorkLayer(BackgroundWorker worker, DoWorkEventArgs e, C2DLayer layer)
        {
            WorkerState userState = new WorkerState();
            userState.WorkCurrentTitle = "Сохранение данных Рабочей папки...";
            bool res = false;
            string fileName;
            if (layer != null)
            {
                TreeNode tn = layer.node;
                if (tn != null && layer.node.TreeView != null)
                {
                    TreeNode parentNode = tn.Parent;
                    string name = mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath + "\\" + layer.Name;
                    if (!Directory.Exists(mainForm.UserSmartPlusFolder)) Directory.CreateDirectory(mainForm.UserSmartPlusFolder);
                    if (!Directory.Exists(mainForm.UserWorkDataFolder)) Directory.CreateDirectory(mainForm.UserWorkDataFolder);
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                    {
                        if (Directory.Exists(mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath))
                        {
                            fileName = name + ".cntr";
                            res = WriteWorkContour(layer, fileName);
                        }
                    }
                    if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                    {
                        if (Directory.Exists(mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath))
                        {
                            fileName = name + ".wl";
                            res = WriteWorkWellList(layer, fileName);
                        }
                    }
                    else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                    {
                        if (Directory.Exists(mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath))
                        {
                            fileName = name + ".grid";
                            res = WriteWorkGrid(layer, fileName);
                        }
                    }
                    else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
                    {
                        if (Directory.Exists(mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath))
                        {
                            fileName = name + ".img";
                            res = WriteWorkImage(layer, fileName);
                        }
                    }
                    else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE)
                    {
                        if (Directory.Exists(mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath))
                        {
                            fileName = name + ".prfl";
                            res = WriteWorkProfile(layer, fileName);
                        }
                    }
                    else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                    {
                        if (Directory.Exists(mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath))
                        {
                            fileName = name + ".mrk";
                            res = WriteWorkMarker(layer, fileName);
                        }
                    }
                    else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT)
                    {
                        if (Directory.Exists(mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath))
                        {
                            fileName = name + ".pvt";
                            res = WriteWorkPvt(layer, fileName);
                        }
                    }
                    else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                    {
                        if (Directory.Exists(mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath))
                        {
                            fileName = name + ".vm";
                            res = WriteWorkVoronoiMap(layer, fileName);
                        }
                    }
                    else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                    {
                        DataFile file = (DataFile)layer.ObjectsList[0];
                        fileName = mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath + "\\" + file.Name;
                        fileName = fileName.Replace("Рабочая папка", "Рабочая папка`");
                        if (File.Exists(fileName))
                        {
                            File.Copy(fileName, mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath + "\\" + file.Name, true);
                            res = true;
                        }
                    }
                    else if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) && (tn.Level != 0))
                    {
                        res = WriteSubWorkLayer(worker, e, layer, mainForm.UserSmartPlusFolder + "\\" + parentNode.FullPath);
                    }
                    if (worker != null)
                    {
                        userState.Element = layer.Name;
                        worker.ReportProgress(100, userState);
                    }
                }
            }
            WorkLayerActive = null;
            return res;
        }

        // WORK FOLDER - TREEVIEW
        public void LoadWorkTree()
        {
            C2DLayer layer;
            TreeNode tn, tn2;
            twWork.Nodes.Clear();
            tn2 = twWork.Nodes.Add("", "Рабочая папка", 2, 2);
            tn2.Checked = true;
            tn2.ContextMenuStrip = twWork.cMenu;

            for (int i = 0; i < LayerWorkList.Count; i++)
            {
                layer = (C2DLayer)LayerWorkList[i];
                int imgIndex = GetImageIndex(layer.ObjTypeID);

                if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    LoadSubFolder(tn, layer);
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
                {
                    tn = tn2.Nodes.Add("", layer.Name, imgIndex, imgIndex);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
                else if (((C2DLayer)LayerWorkList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                {
                    string ext = Path.GetExtension(((C2DLayer)LayerWorkList[i]).ObjectsList[0].Name);
                    int index = twWork.GetFileIconIndex(ext);
                    tn = tn2.Nodes.Add("", layer.Name, index, index);
                    tn.Tag = layer;
                    tn.Checked = layer.Visible;
                    layer.node = tn;
                    tn.ContextMenuStrip = twWork.cMenu;
                }
            }
            twWork.Nodes[0].Expand();
        }
        private void LoadSubFolder(TreeNode tn, C2DLayer layer)
        {
            TreeNode subTN;
            tn.Tag = layer;
            tn.Checked = layer.Visible;
            layer.node = tn;
            tn.ContextMenuStrip = twWork.cMenu;
            C2DLayer subLayer;
            for (int j = 0; j < layer.ObjectsList.Count; j++)
            {
                subLayer = (C2DLayer)layer.ObjectsList[j];
                int imgIndex = GetImageIndex(subLayer.ObjTypeID);

                if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, imgIndex, imgIndex);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, imgIndex, imgIndex);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, imgIndex, imgIndex);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, imgIndex, imgIndex);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, imgIndex, imgIndex);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, imgIndex, imgIndex);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, imgIndex, imgIndex);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, 2, 2);
                    LoadSubFolder(subTN, subLayer);
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
                {
                    subTN = tn.Nodes.Add("", subLayer.Name, imgIndex, imgIndex);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
                else if (subLayer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                {
                    string ext = Path.GetExtension(subLayer.ObjectsList[0].Name);
                    int index = twWork.GetFileIconIndex(ext);
                    subTN = tn.Nodes.Add("", subLayer.Name, index, index);
                    subTN.Tag = subLayer;
                    subTN.Checked = subLayer.Visible;
                    subLayer.node = subTN;
                    subTN.ContextMenuStrip = twWork.cMenu;
                }
            }
        }
        public void CreateSubFolder(string nameSubFolder, TreeNode tn)
        {
            C2DLayer layerGroup;
            layerGroup = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, 0);
            layerGroup.Name = nameSubFolder;
            layerGroup.Visible = true;
            layerGroup.ResetFontsPens();
            layerGroup.GetObjectsRect();
            tn.Tag = layerGroup;
            layerGroup.node = tn;
            if (tn.Parent.Tag != null)
            {
                C2DLayer layerParent = (C2DLayer)tn.Parent.Tag;
                layerGroup.Index = layerParent.ObjectsList.Count;
                layerParent.Add(layerGroup);
            }
            else
            {
                layerGroup.Index = LayerWorkList.Count;
                LayerWorkList.Add(layerGroup);
            }
        }
        void RemoveWorkNode(TreeNode tn)
        {
            twWork.BeginUpdate();
            TreeNode parentNode = tn.Parent;
            string name;

            if ((tn.TreeView != null) && (((BaseObj)tn.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
            {
                if ((((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) && (tn.Level > 0))
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath;
                    if (Directory.Exists(name)) Directory.Delete(name, true);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath + ".img";
                    if (File.Exists(name)) File.Delete(name);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath + ".cntr";
                    if (File.Exists(name)) File.Delete(name);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath + ".grid";
                    if (File.Exists(name)) File.Delete(name);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath + ".wl";
                    if (File.Exists(name)) File.Delete(name);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROFILE)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath + ".prfl";
                    if (File.Exists(name)) File.Delete(name);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.MARKER)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath + ".mrk";
                    if (File.Exists(name)) File.Delete(name);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath + ".vm";
                    if (File.Exists(name)) File.Delete(name);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PVT)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath + ".pvt";
                    if (File.Exists(name)) File.Delete(name);
                }
                else if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.DATAFILE)
                {
                    name = mainForm.UserSmartPlusFolder + "\\" + tn.FullPath;
                    if (File.Exists(name)) File.Delete(name);
                }
            }

            // удаляем layer
            if (tn.Text == ((C2DLayer)tn.Tag).Name)
            {
                if (parentNode.Tag == null)
                {
                    LayerWorkList.RemoveAt(((C2DLayer)tn.Tag).Index);
                    for (int i = 0; i < LayerWorkList.Count; i++) ((C2DLayer)LayerWorkList[i]).Index = i;
                }
                else if (((C2DLayer)parentNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    C2DLayer parentLayer = (C2DLayer)parentNode.Tag;
                    C2DObject lr;
                    parentLayer.ObjectsList.RemoveAt(((C2DLayer)tn.Tag).Index);
                    for (int i = 0; i < parentLayer.ObjectsList.Count; i++)
                    {
                        lr = (C2DObject)parentLayer.ObjectsList[i];
                        if (lr.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) lr.Index = i;
                    }
                }
            }
            // удаляем node
            parentNode.Nodes.RemoveAt(tn.Index);
            twWork.EndUpdate();
        }
        public void RemoveWorkLayer(TreeNode tn)
        {
            RemoveWorkNode(tn);
            if (WorkObjectSettings != null) WorkObjectSettings.RemoveLayer((C2DLayer)tn.Tag);
        }

        // WORK SETTINGS
        public void WorkObjectSettingsShow(C2DLayer Layer)
        {
            if (WorkObjectSettings == null)
            {
                WorkObjectSettings = new WorkObjectSettingForm(mainForm);
                WorkObjectSettings.FormClosing += new FormClosingEventHandler(WorkObjectSettings_FormClosing);
            }
            if (Layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID && Layer.ObjectsList.Count > 0 && !((Grid)Layer.ObjectsList[0]).DataLoaded)
            {
                MessageBox.Show("Для отображения настроек сетки необходимо отобразить сетку на карте!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            WorkObjectSettings.Show(Layer);
        }
        void WorkObjectSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            WorkObjectSettings = null;
        }

        #endregion // WORK FOLDER

        // SELECTED OBJECT PARAMETERS
        public void SetSelectedObj(BaseObj obj)
        {
            int i, j, k;
            SelectedWell so;
            WellIcon AllIcon = null;
            Well w;
            C2DLayer layer;
            ArrayList icon = null;
            WellIconCode icode;
            var dictArea = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);

            this.SelObjUpdater.SetParams = true;
            switch (obj.TypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    so = new SelectedWell(SelObjUpdater);

                    w = (Well)obj;
                    so.OilFieldName = (MapProject.OilFields[w.OilFieldIndex]).Name;
                    so.Name = w.UpperCaseName;
                    if (w.font != null) // todo почему всплывает нуль?
                    {
                        so.FontName = w.font.Name;
                        so.FontSize = (int)w.font.Size;
                    }
                    so.OilFieldAreaName = "";
                    so.VisibleName = w.VisibleName;
                    so.VisibleText = w.VisibleText;
                    so.VisibleHBubbleCol = w.VisibleHBubbleCol;
                    so.OilFieldAreaName = dictArea.GetShortNameByCode(w.OilFieldAreaCode);

                    if ((w.icons != null) && (w.icons.Count > 0))
                    {
                        AllIcon = w.icons.GetActiveIcon();
                        if (AllIcon != null)
                        {
                            icon = new ArrayList();
                            for (i = 0; i < AllIcon.Count; i++)
                            {

                                icode.CharCode = AllIcon[i].CharCode;
                                icode.FontCode = AllIcon.GetIconFontCode(AllIcon[i].FontCode);
                                icode.IconColor = AllIcon[i].IconColor;
                                icode.IconColorDefault = AllIcon[i].IconColor;
                                icode.Size = AllIcon[i].Size;
                                icon.Add(icode);
                            }
                            icon.Add(FontFamilyList);
                        }
                        so.Icon = icon;
                        mainForm.pgProperties.SelectedObject = so;
                        mainForm.pgProperties.Refresh();
                    }
                    SelObject = obj;


                    break;
                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                    so = new SelectedWell(SelObjUpdater);

                    PhantomWell pw = (PhantomWell)obj;
                    so.OilFieldName = (MapProject.OilFields[pw.srcWell.OilFieldIndex]).Name;
                    so.Name = pw.srcWell.UpperCaseName;
                    so.FontName = pw.srcWell.font.Name;
                    so.FontSize = (int)pw.srcWell.font.Size;
                    so.OilFieldAreaName = "";
                    so.VisibleName = pw.VisibleName;
                    so.OilFieldAreaName = dictArea.GetShortNameByCode(pw.srcWell.OilFieldAreaCode);

                    if ((pw.icons != null) && (pw.icons.Count > 0))
                    {
                        AllIcon = pw.icons.GetActiveIcon();
                        if (AllIcon != null)
                        {
                            icon = new ArrayList();
                            for (i = 0; i < AllIcon.Count; i++)
                            {
                                icode.CharCode = AllIcon[i].CharCode;
                                icode.FontCode = AllIcon.GetIconFontCode(AllIcon[i].FontCode);
                                icode.IconColor = AllIcon[i].IconColor;
                                icode.IconColorDefault = AllIcon[i].IconColor;
                                icode.Size = AllIcon[i].Size;
                                icon.Add(icode);
                            }
                            icon.Add(FontFamilyList);
                        }
                        so.Icon = icon;
                    }
                    mainForm.pgProperties.SelectedObject = so;
                    mainForm.pgProperties.Refresh();
                    SelObject = obj;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL:
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    break;
                case Constant.BASE_OBJ_TYPES_ID.LAYER:
                    layer = (C2DLayer)obj;
                    if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.WELL) ||
                       (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PROJECT_WELL))
                    {
                        SelectedWellList swl = new SelectedWellList(SelObjUpdater);
                        swl.Name = layer.Name;
                        swl.WellCount = layer.ObjectsList.Count;
                        swl.VisibleName = layer.VisibleName;
                        WellIconCode newCode;
                        bool OneIconByAllObj = true;
                        for (i = 0; (i < layer.ObjectsList.Count) && (OneIconByAllObj); i++)
                        {
                            w = (Well)layer.ObjectsList[i];
                            if ((w.icons != null) && (w.icons.Count > 0))
                            {
                                for (j = 0; j < w.icons.Count; j++)
                                {
                                    if (AllIcon == null)
                                    {
                                        AllIcon = new WellIcon(w.icons[j].X, w.icons[j].Y, w.icons[j].StratumCode);
                                        for (k = 0; k < w.icons[j].Count; k++)
                                        {
                                            newCode = w.icons[j][k];
                                            AllIcon.AddIconCode(newCode);
                                        }
                                    }
                                    else if (!AllIcon.EqualsIcon(w.icons[j]))
                                    {
                                        OneIconByAllObj = false;
                                        break;
                                    }
                                }
                            }
                        }
                        if (OneIconByAllObj)
                        {
                            if ((AllIcon != null) && (AllIcon.Count > 0))
                            {
                                icon = new ArrayList();
                                for (i = 0; i < AllIcon.Count; i++)
                                {
                                    icode.CharCode = AllIcon[i].CharCode;
                                    icode.FontCode = AllIcon.GetIconFontCode(AllIcon[i].FontCode);
                                    icode.IconColor = AllIcon[i].IconColor;
                                    icode.IconColorDefault = icode.IconColor;
                                    icode.Size = AllIcon[i].Size;
                                    icon.Add(icode);
                                }
                                icon.Add(FontFamilyList);
                                swl.Icon = icon;
                            }
                        }
                        mainForm.pgProperties.SelectedObject = swl;
                        mainForm.pgProperties.Refresh();
                        SelObject = obj;
                    }
                    else if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                    {
                        SelectedWellList swl = new SelectedWellList(SelObjUpdater);
                        swl.Name = layer.Name;
                        swl.WellCount = layer.ObjectsList.Count;
                        swl.VisibleName = layer.VisibleName;

                        WellIconCode newCode;
                        bool OneIconByAllObj = true;
                        for (i = 0; (i < layer.ObjectsList.Count) && (OneIconByAllObj); i++)
                        {
                            pw = (PhantomWell)layer.ObjectsList[i];
                            if ((pw.icons != null) && (pw.icons.Count > 0))
                            {
                                for (j = 0; j < pw.icons.Count; j++)
                                {
                                    if (pw.icons[j].StratumCode != -1)
                                    {
                                        if (AllIcon == null)
                                        {
                                            AllIcon = new WellIcon(pw.icons[j].X, pw.icons[j].Y, pw.icons[j].StratumCode);
                                            for (k = 0; k < pw.icons[j].Count; k++)
                                            {
                                                newCode = pw.icons[j][k];
                                                AllIcon.AddIconCode(newCode);
                                            }
                                        }
                                        else if (!AllIcon.EqualsIcon(pw.icons[j]))
                                        {
                                            OneIconByAllObj = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        icon = new ArrayList();
                        if (OneIconByAllObj)
                        {
                            if ((AllIcon != null) && (AllIcon.Count > 0))
                            {
                                for (i = 0; i < AllIcon.Count; i++)
                                {
                                    icode.CharCode = AllIcon[i].CharCode;
                                    icode.FontCode = AllIcon.GetIconFontCode(AllIcon[i].FontCode);
                                    icode.IconColor = AllIcon[i].IconColor;
                                    icode.IconColorDefault = icode.IconColor;
                                    icode.Size = AllIcon[i].Size;
                                    icon.Add(icode);
                                }
                                icon.Add(FontFamilyList);
                            }
                        }
                        else
                        {
                            icon.Add(true);
                        }

                        swl.Icon = icon;
                        mainForm.PropertyGridSetObj(swl);
                        SelObject = obj;
                    }
                    break;
            }
            this.SelObjUpdater.SetParams = false;
        }
        public void SelObjUpdate(Object obj, int ind)
        {
            if ((SelObject != null) && (!SelObjUpdater.SetParams))
            {
                int i, j, k;
                SelectedWell so;
                Constant.BASE_OBJ_TYPES_ID type = (Constant.BASE_OBJ_TYPES_ID)ind;
                switch (type)
                {
                    case Constant.BASE_OBJ_TYPES_ID.WELL:
                        if ((obj != null) && (SelObject.TypeID == Constant.BASE_OBJ_TYPES_ID.WELL))
                        {
                            so = (SelectedWell)obj;
                            Well w = (Well)SelObject;
                            WellIcon icon;
                            w.VisibleName = so.VisibleName;
                            w.VisibleText = so.VisibleText;
                            w.VisibleHBubbleCol = so.VisibleHBubbleCol;
                            for (i = 0; i < w.icons.Count; i++)
                            {
                                if (w.icons[i].StratumCode != -1)
                                {
                                    icon = w.icons[i];
                                    icon.ClearIconList();

                                    for (j = 0; j < so.Icon.Count - 1; j++)
                                        icon.AddIconCode((WellIconCode)so.Icon[j]);
                                }
                                w.SetActiveAllIcon();
                            }
                            this.DrawLayerList();
                        }
                        else if ((obj != null) && (SelObject.TypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL))
                        {
                            so = (SelectedWell)obj;
                            PhantomWell pw = (PhantomWell)SelObject;
                            WellIcon icon;
                            pw.VisibleName = so.VisibleName;
                            for (i = 0; i < pw.icons.Count; i++)
                            {
                                if (pw.icons[i].StratumCode != -1)
                                {
                                    icon = pw.icons[i];
                                    icon.ClearIconList();

                                    for (j = 0; j < so.Icon.Count - 1; j++)
                                        icon.AddIconCode((WellIconCode)so.Icon[j]);
                                }
                                pw.icons.SetActiveAllIcon();
                                pw.SetBrushFromIcon();
                            }
                            this.DrawLayerList();
                        }
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.WELL_LIST:
                        if ((obj != null) && (SelObject.TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER) &&
                            (((C2DLayer)SelObject).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL))
                        {
                            SelectedWellList swl = (SelectedWellList)obj;
                            C2DLayer layer = (C2DLayer)SelObject;
                            PhantomWell pw;
                            WellIcon icon;
                            layer.VisibleName = swl.VisibleName;
                            for (i = 0; i < layer.ObjectsList.Count; i++)
                            {
                                pw = (PhantomWell)layer.ObjectsList[i];
                                if (swl.Icon.Count > 1)
                                {
                                    for (j = 0; j < pw.icons.Count; j++)
                                    {
                                        if (pw.icons[j].StratumCode != -1)
                                        {
                                            icon = pw.icons[j];
                                            icon.ClearIconList();
                                            for (k = 0; k < swl.Icon.Count - 1; k++)
                                                icon.AddIconCode((WellIconCode)swl.Icon[k]);
                                        }
                                    }
                                }
                                pw.VisibleName = swl.VisibleName;
                                pw.icons.SetActiveAllIcon();
                                pw.SetBrushFromIcon();
                            }
                            mainForm.UpdateWellListSettings(layer);
                            this.LayerWorkListChanged = true;
                        }
                        break;
                }
                this.DrawLayerList();
            }

        }

        // BUBBLE MAP
        public void SetBubbleSettingMinMax()
        {
            C2DLayer layer = null;
            if (selLayerList.Count > 0) layer = (C2DLayer)selLayerList[0];
            if ((layer != null) && (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR) && (layer.ObjectsList.Count > 0))
            {
                Contour cntr = (Contour)layer.ObjectsList[0];
                DateTime minDT = DateTime.MaxValue, maxDT = DateTime.MinValue;
                OilField of;
                int i, j;
                if (cntr._EnterpriseCode > -1)
                {
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        cntr = (Contour)((C2DLayer)selLayerList[i]).ObjectsList[0];
                        for (j = 0; j < MapProject.OilFields.Count; j++)
                        {
                            of = MapProject.OilFields[j];
                            if (of.ParamsDict.EnterpriseCode == cntr._EnterpriseCode)
                            {
                                if ((minDT > of.minMerDate) && (of.minMerDate != DateTime.MinValue)) minDT = of.minMerDate;
                                if ((maxDT < of.maxMerDate) && (of.maxMerDate != DateTime.MaxValue)) maxDT = of.maxMerDate;
                            }
                        }
                    }
                }
                else if (cntr._NGDUCode > -1)
                {
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        cntr = (Contour)((C2DLayer)selLayerList[i]).ObjectsList[0];
                        for (j = 0; j < MapProject.OilFields.Count; j++)
                        {
                            of = MapProject.OilFields[j];
                            if (of.ParamsDict.NGDUCode == cntr._NGDUCode)
                            {
                                if ((minDT > of.minMerDate) && (of.minMerDate != DateTime.MinValue)) minDT = of.minMerDate;
                                if ((maxDT < of.maxMerDate) && (of.maxMerDate != DateTime.MaxValue)) maxDT = of.maxMerDate;
                            }
                        }
                    }
                }
                else if (cntr._OilFieldCode > -1)
                {
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        cntr = (Contour)((C2DLayer)selLayerList[i]).ObjectsList[0];
                        for (j = 0; j < MapProject.OilFields.Count; j++)
                        {
                            of = MapProject.OilFields[j];
                            if (of.OilFieldCode == cntr._OilFieldCode)
                            {
                                if ((minDT > of.minMerDate) && (of.minMerDate != DateTime.MinValue)) minDT = of.minMerDate;
                                if ((maxDT < of.maxMerDate) && (of.maxMerDate != DateTime.MaxValue)) maxDT = of.maxMerDate;
                            }
                        }
                    }
                }
                mainForm.fBubbleSet.SetMinMaxDate(minDT, maxDT);
            }
        }
        public void CalcBubbleMap(Constant.BUBBLE_MAP_TYPE BubbleMapType)
        {
            int contour_count = 0;
            if (selLayerList.Count > 0)
            {
                for (int i = 0; i < selLayerList.Count; i++)
                {
                    if (((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR || ((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                    {
                        contour_count++;
                        break;
                    }
                }
            }
            if ((twActiveOilField.ActiveOilFieldIndex > -1) || (contour_count > 0))
            {
                this.BubbleMapType = BubbleMapType;
                mainForm.SetBubbleMapCheck(BubbleMapType);
                switch (BubbleMapType)
                {
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                        BubbleDT2 = mainForm.fBubbleSet.BCLastDate;
                        BubbleLastMonth = mainForm.fBubbleSet.BCLastMonth;
                        BubbleK = mainForm.fBubbleSet.BCK;
                        mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.CALC_BUBBLE_MAP, this);
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                        BubbleDT1 = mainForm.fBubbleSet.BAStartDate;
                        BubbleDT2 = mainForm.fBubbleSet.BALastDate;
                        BubbleStartMonth = mainForm.fBubbleSet.BAStartMonth;
                        BubbleLastMonth = mainForm.fBubbleSet.BALastMonth;
                        BubbleK = mainForm.fBubbleSet.BAK;
                        mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.CALC_BUBBLE_MAP, this);
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                        BubbleDT1 = mainForm.fBubbleSet.BHStartDate;
                        BubbleDT2 = mainForm.fBubbleSet.BHLastDate;
                        BubbleStartMonth = mainForm.fBubbleSet.BHStartMonth;
                        BubbleLastMonth = mainForm.fBubbleSet.BHLastMonth;
                        BubbleHistoryViewBySector = mainForm.fBubbleSet.BHViewBySector;
                        BubbleK = mainForm.fBubbleSet.BHK;
                        BubbleHistoryByChess = mainForm.fBubbleSet.BHDataSource;
                        mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.CALC_BUBBLE_MAP, this);
                        break;
                }
            }
        }
        public void ShowBubbleMap(Constant.BUBBLE_MAP_TYPE BubbleMapType)
        {
            int i;
            bool find = false;
            if ((this.BubbleMapType == BubbleMapType) && ((twActiveOilField.ActiveOilFieldIndex > -1) || (selLayerList.Count > 0)))
            {
                OilField of;
                if (selLayerList.Count > 0)
                {
                    find = false;
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        if (((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                        {
                            if ((((C2DLayer)selLayerList[i]).oilfield != null) && ((C2DLayer)selLayerList[i]).oilfield.BubbleMapLoaded)
                            {
                                find = true;
                                ((C2DLayer)selLayerList[i]).oilfield.SetVisibleBubbleMap(true);
                            }
                        }
                    }
                }
                if ((!find) && (twActiveOilField.ActiveOilFieldIndex > -1))
                {
                    of = MapProject.OilFields[twActiveOilField.ActiveOilFieldIndex];
                    if (of.BubbleMapLoaded)
                    {
                        find = true;
                        of.SetVisibleBubbleMap(true);
                        if (this.BubbleMapType == Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM) of.SetDefaultColorWellIcons(true);
                    }
                }
                if (find)
                {
                    mainForm.SetBubbleMapCheck(BubbleMapType);
                    DrawLayerList();
                }
            }
            if ((!find) && ((twActiveOilField.ActiveOilFieldIndex > -1) || (selLayerList.Count > 0)))
            {
                CalcBubbleMap(BubbleMapType);
            }
        }
        public bool HideBubbleMap()
        {
            bool find = false;
            if (MapProject != null)
            {
                int i;
                OilField of;
                if (selLayerList.Count > 0)
                {
                    find = false;
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        if (((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                        {
                            if ((((C2DLayer)selLayerList[i]).oilfield != null) && ((C2DLayer)selLayerList[i]).oilfield.BubbleMapLoaded)
                            {
                                find = true;
                                ((C2DLayer)selLayerList[i]).oilfield.SetVisibleBubbleMap(false);
                            }
                        }
                    }
                }
                if ((!find) && (twActiveOilField.ActiveOilFieldIndex > -1))
                {
                    of = MapProject.OilFields[twActiveOilField.ActiveOilFieldIndex];
                    if (of.BubbleMapLoaded)
                    {
                        find = true;
                        of.SetVisibleBubbleMap(false);
                        of.SetDefaultColorWellIcons(!GrayWellIconsMode);
                        for (i = 1; i < MapProject.OilFields.Count; i++)
                        {
                            of = MapProject.OilFields[i];
                            if ((of.BubbleMapLoaded) && (i != twActiveOilField.ActiveOilFieldIndex))
                            {
                                of.SetVisibleBubbleMap(false);
                            }
                        }
                    }
                }
                if (find) DrawLayerList();
            }
            return find;
        }
        public void UpdateBubbleMap()
        {
            if ((twActiveOilField.ActiveOilFieldIndex > -1) || (selLayerList.Count > 0))
            {
                switch (this.BubbleMapType)
                {
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                        BubbleK = mainForm.fBubbleSet.BCK;
                        mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.UPDATE_BUBBLE_MAP, this);
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                        BubbleK = mainForm.fBubbleSet.BAK;
                        mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.UPDATE_BUBBLE_MAP, this);
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                        BubbleHistoryViewBySector = mainForm.fBubbleSet.BHViewBySector;
                        BubbleK = mainForm.fBubbleSet.BHK;
                        mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.UPDATE_BUBBLE_MAP, this);
                        break;
                }
            }
        }

        // TRAJECTORY
        public void LoadWellTrajectory()
        {
            int contour_count = 0;
            if (selLayerList.Count > 0)
            {
                for (int i = 0; i < selLayerList.Count; i++)
                {
                    if (((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.CONTOUR || ((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                    {
                        contour_count++;
                        break;
                    }
                }
            }
            if ((twActiveOilField.ActiveOilFieldIndex > -1) || (contour_count > 0))
            {
                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.LOAD_WELLS_TRAJECTORY, this);
            }
        }
        public void ShowWellTrajectory()
        {
            int i;
            bool find = false;
            if ((twActiveOilField.ActiveOilFieldIndex > -1) || (selLayerList.Count > 0))
            {
                OilField of;
                if (selLayerList.Count > 0)
                {
                    find = false;
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        if (((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                        {
                            if ((((C2DLayer)selLayerList[i]).oilfield != null) && ((C2DLayer)selLayerList[i]).oilfield.WellTrajectoryLoaded)
                            {
                                find = true;
                                ((C2DLayer)selLayerList[i]).oilfield.SetVisibleWellTrajectory(true);
                            }
                        }
                    }
                }
                if (!find && (twActiveOilField.ActiveOilFieldIndex > -1))
                {
                    of = MapProject.OilFields[twActiveOilField.ActiveOilFieldIndex];
                    if (of.WellTrajectoryLoaded)
                    {
                        find = true;
                        of.SetVisibleWellTrajectory(true);
                    }
                }
                if (find)
                {
                    mainForm.SetWellTrajectoryButtonCheck();
                    DrawLayerList();
                }
            }
            if (!find && ((twActiveOilField.ActiveOilFieldIndex > -1) || (selLayerList.Count > 0)))
            {
                LoadWellTrajectory();
            }
            else if (!find)
            {
                MessageBox.Show(mainForm, "Выберите месторождение для построения траекторий скважин", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        public bool HideWellTrajectory()
        {
            bool find = false;
            if (MapProject != null)
            {
                int i;
                OilField of;
                if (selLayerList.Count > 0)
                {
                    find = false;
                    for (i = 0; i < selLayerList.Count; i++)
                    {
                        if (((C2DLayer)selLayerList[i]).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.COND_CONTOUR)
                        {
                            if ((((C2DLayer)selLayerList[i]).oilfield != null) && ((C2DLayer)selLayerList[i]).oilfield.WellTrajectoryLoaded)
                            {
                                find = true;
                                ((C2DLayer)selLayerList[i]).oilfield.SetVisibleWellTrajectory(false);
                            }
                        }
                    }
                }
                if (!find && (twActiveOilField.ActiveOilFieldIndex > -1))
                {
                    of = MapProject.OilFields[twActiveOilField.ActiveOilFieldIndex];
                    if (of.WellTrajectoryLoaded)
                    {
                        find = true;
                        of.SetVisibleWellTrajectory(false);
                        for (i = 1; i < MapProject.OilFields.Count; i++)
                        {
                            of = MapProject.OilFields[i];
                            if ((of.WellTrajectoryLoaded) && (i != twActiveOilField.ActiveOilFieldIndex))
                            {
                                of.SetVisibleWellTrajectory(false);
                            }
                        }
                    }
                }
                if (find) DrawLayerList();
            }
            return find;
        }

        private WellIconCode GetDefaultIconCode(int GTMcode)
        {
            WellIconCode code;
            switch(GTMcode)
            {
                case 1000:
                    code.FontCode = 1;
                    code.IconColor = -8388480;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 87;
                    code.Size = 30;
                    break;
                case 2000:
                    code.FontCode = 1;
                    code.IconColor = -8388480;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 85;
                    code.Size = 20;
                    break;
                case 3000:
                    code.FontCode = 1;
                    code.IconColor = -7876885;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 74;
                    code.Size = 20;
                    break;
                case 4000:
                    code.FontCode = 1;
                    code.IconColor = -1015680;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 71;
                    code.Size = 20;
                    break;
                case 5000:
                    code.FontCode = 1;
                    code.IconColor = -16744448;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 73;
                    code.Size = 30;
                    break;
                case 6000:
                    code.FontCode = 3;
                    code.IconColor = -65408;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 76;
                    code.Size = 30;
                    break;
                case 7000:
                    code.FontCode = 4;
                    code.IconColor = -16756060;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 51;
                    code.Size = 25;
                    break;
                case 8000:
                    goto case 12000;
                case 9000:
                    code.FontCode = 5;
                    code.IconColor = -16756060;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 70;
                    code.Size = 30;
                    break;
                case 10000:
                    code.FontCode = 4;
                    code.IconColor = -10496;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 58;
                    code.Size = 25;
                    break;
                case 11000:
                    code.FontCode = 1;
                    code.IconColor = -7278960;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 93;
                    code.Size = 25;
                    break;
                case 12000:
                    code.FontCode = 1;
                    code.IconColor = -65281;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 111;
                    code.Size = 20;
                    break;
                case 13000:
                    code.FontCode = 1;
                    code.IconColor = -10185235;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 63;
                    code.Size = 25;
                    break;
                case 20000:
                    code.FontCode = 0;
                    code.IconColor = -16776961;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 36;
                    code.Size = 25;
                    break;
                default:
                    code.FontCode = 0;
                    code.IconColor = -8355712;
                    code.IconColorDefault = code.IconColor;
                    code.CharCode = 34;
                    code.Size = 30;
                    break;
            }
            return code;
        }
        private string GetDefaultGtmName(int GTMcode)
        {
            string name;
            switch (GTMcode)
            {
                case 1000:
                    name = "ВНС из бурения";
                    break;
                case 2000:
                    name = "ВНС из др.фонда";
                    break;
                case 3000:
                    name = "ЗБС";
                    break;
                case 4000:
                    name = "Углубление";
                    break;
                case 5000:
                    name = "ГРП";
                    break;
                case 6000:
                    name = "ПВЛГ";
                    break;
                case 7000:
                    name = "ВБД";
                    break;
                case 8000:
                    goto case 12000;
                case 9000:
                    name = "РИР";
                    break;
                case 10000:
                    name = "ОПЗ";
                    break;
                case 11000:
                    name = "Реперфорация";
                    break;
                case 12000:
                    name = "ИДН";
                    break;
                case 13000:
                    name = "ЛАР";
                    break;
                case 20000:
                    name = "Перевод в ППД";
                    break;
                default:
                    name = "Прочие";
                    break;
            }
            return name;
        }
        public void CreateGTMLayerList(BackgroundWorker worker, DoWorkEventArgs e, DateTime startDate, DateTime endDate, List<int[]> GTMcodes)
        {
            if (MapProject != null)
            {
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Создание списков ГТМ по датам...";
                userState.WorkCurrentTitle = "Обработка данных ГТМ скважины...";

                int code;
                int i, j, k;
                
                var dict = (DataDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.GTM_TYPE);

                List<int>[] codes = new List<int>[GTMcodes.Count];
                for (i = 0; i < codes.Length; i++) codes[i] = new List<int>();

                for (i = 0; i < dict.Count; i++)
                {
                    if(!int.TryParse(dict[i].Note, out code)) code = -1;
                    for(j = 0; j < GTMcodes.Count; j++)
                    {
                        for (k = 0; k < GTMcodes[j].Length; k++)
                        {
                            if (dict[i].Code == GTMcodes[j][k] || code == GTMcodes[j][k])
                            {
                                codes[j].Add(dict[i].Code);
                                break;
                            }
                        }
                    }
                }
                int ind;
                C2DLayer rootLayer = new C2DLayer(Constant.BASE_OBJ_TYPES_ID.LAYER, 0);
                List<C2DLayer>[] layers = new List<C2DLayer>[GTMcodes.Count];
                for (i = 0; i < layers.Length; i++)
                {
                    layers[i] = new List<C2DLayer>();
                }
                int[] indLayer = new int[GTMcodes.Count];

                OilField of;
                Well w;
                PhantomWell pw;
                WellIconCode iconCode;
                for (int ofInd = 1; ofInd < MapProject.OilFields.Count; ofInd++)
                {
                    of = MapProject.OilFields[ofInd];
                    of.LoadGTMFromCache(worker, e, true);
                    userState.Element = of.Name;

                    for (int wInd = 0; wInd < of.Wells.Count; wInd++)
                    {
                        w = of.Wells[wInd];
                        if (w.GTMLoaded)
                        {
                            for (i = 0; i < indLayer.Length; i++)
                            {
                                indLayer[i] = 0;
                            }
                            for (i = w.gtm.Count - 1; i >= 0; i--)
                            {
                                if(w.gtm[i].Date < startDate) break;
                                if (w.gtm[i].Date < endDate)
                                {
                                    for(j = 0; j < codes.Length;j++)
                                    {
                                        ind = codes[j].IndexOf(w.gtm[i].GtmCode);
                                        if (ind > -1)
                                        {
                                            iconCode = GetDefaultIconCode(GTMcodes[j][0]);
                                            pw = new PhantomWell(of.ParamsDict.RDFFieldCode, w);
                                            pw.SetIconsFonts(iconFonts, exFontList);
                                            if (pw.icons != null)
                                            {
                                                for (k = 0; k < pw.icons.Count; k++)
                                                {
                                                    if (pw.icons[k].StratumCode != -1)
                                                    {
                                                        pw.icons[k].ClearIconList();
                                                        pw.icons[k].AddIconCode(iconCode);
                                                    }
                                                }
                                                pw.icons.SetActiveAllIcon();
                                                pw.SetBrushFromIcon();
                                            }
                                            pw.Caption = new PhantomWellCaption();
                                            pw.Caption.Date = w.gtm[i].Date;
                                            pw.Caption.Text = dict.GetFullNameByCode(w.gtm[i].GtmCode);
                                            if (indLayer[j] >= layers[j].Count)
                                            {
                                                layers[j].Add(new C2DLayer(Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL, 0));
                                            }
                                            layers[j][indLayer[j]].ObjectsList.Add(pw);
                                            indLayer[j]++;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    worker.ReportProgress(100, userState);
                    of.ClearGTMData(false);
                }
                GC.GetTotalMemory(true);
                for (i = 0; i < layers.Length; i++)
                {
                    for (j = 0; j < layers[i].Count; j++)
                    {
                        if (layers[i][j].ObjectsList.Count > 0)
                        {
                            layers[i][j].VisibleLvl.SetVisibleLvl(3);
                            layers[i][j].Name = GetDefaultGtmName(GTMcodes[i][0]);
                            if (j > 0) layers[i][j].Name += string.Format(" ({0})", j);
                            layers[i][j].Visible = true;
                            layers[i][j].VisibleName = false;
                            layers[i][j].fontSize = this._fontSize;
                            layers[i][j].ResetFontsPens();
                            layers[i][j].GetObjectsRect();
                            layers[i][j].DrawOverBubble = true;
                            layers[i][j].Index = rootLayer.ObjectsList.Count;
                            rootLayer.Add(layers[i][j]);
                        }
                    }
                }
                if (rootLayer.ObjectsList.Count > 0)
                {
                    rootLayer.SetFontsRecursive(iconFonts, exFontList);
                    rootLayer.Name = string.Format("ГТМ [{0:dd.MM.yy} - {1:dd.MM.yy}]", startDate, endDate);
                    rootLayer.Visible = true;
                    rootLayer.VisibleName = false;
                    rootLayer.GetObjectsRect();
                    e.Result = rootLayer;
                }
            }
        }

        // DRAWING
        void ShowMapTip(int ScreenX, int ScreenY, string Text)
        {
            if (this.MapTip != null) this.MapTip.Dispose();
            MapTip = new ToolTip();
            MapTip.InitialDelay = 0;
            MapTip.IsBalloon = false;
            MapTip.ShowAlways = false;
            MapTip.UseAnimation = false;
            MapTip.UseFading = false;
            MapTip.Show(Text, this, ScreenX, ScreenY, 20000);
        }
        void TipTimer_Tick(object sender, EventArgs e)
        {
            Point pt = PointToClient(Cursor.Position);
            if (Math.Abs(TipPoint.X - pt.X) < 2 && Math.Abs(TipPoint.Y - pt.Y) < 2 && !mainForm.UserDialogShowing)
            {
                string tipText = GetToolTipText(pt.X, pt.Y);
                if (tipText.Length > 0)
                {
                    ShowMapTip(pt.X + 12, pt.Y - 8, tipText);
                }
                TipTimer.Enabled = false;
            }
        }
        string GetToolTipText(int ScreenX, int ScreenY)
        {
            string TipText = string.Empty;
            if(C2DObject.st_ObjectsRect == null) return TipText;
            if (this.showLetterBox && LetterBoxObject != null && this.Cursor == Cursors.Hand)
            {
                string prefix = string.Empty;
                switch (LetterBoxObject.TypeID)
                {
                    case Constant.BASE_OBJ_TYPES_ID.AREA:
                        prefix = "ЯЗ: ";
                        break;
                    case Constant.BASE_OBJ_TYPES_ID.WELL:
                        prefix = "Скв: ";
                        break;
                }
                TipText = prefix + LetterBoxObject.Name;
            }
            else if (C2DObject.st_XUnitsInOnePixel < C2DObject.st_OilfieldXUnits)
            {
                double x = C2DObject.RealXfromScreenX(ScreenX);
                double y = C2DObject.RealYfromScreenY(ScreenY);

                C2DLayer layer;
                OilField of = null;
                for (int i = 0; i < LayerList.Count; i++)
                {
                    layer = (C2DLayer)LayerList[i];
                    if (layer.Visible)
                    {
                        of = layer.GetOilFieldByXY(ScreenX, ScreenY);
                        if (of != null) break;
                    }
                }
                List<C2DLayer> list = new List<C2DLayer>();
                for (int i = 0; i < LayerList.Count; i++)
                {
                    ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.GRID, list, true);
                }
                for (int i = 0; i < LayerWorkList.Count; i++)
                {
                    Constant.BASE_OBJ_TYPES_ID[] objIDs = new Constant.BASE_OBJ_TYPES_ID[2];
                    objIDs[0] = Constant.BASE_OBJ_TYPES_ID.GRID;
                    objIDs[1] = Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP;
                    ((C2DLayer)LayerWorkList[i]).FillLayersByObjTypeID(objIDs, list, true);
                }
                int ind;
                Grid grid;
                VoronoiMap vMap;
                for (int i = 0; i < list.Count; i++)
                {
                    switch (list[i].ObjTypeID)
                    {
                        case Constant.BASE_OBJ_TYPES_ID.GRID:
                            grid = (Grid)list[i].ObjectsList[0];
                            if (grid.DataLoaded && !grid.DataLoading)
                            {
                                int row = (int)((grid.Ymax - (grid.dy / 2) - y) / grid.dy);
                                int col = (int)((x - grid.Xmin + (grid.dx / 2)) / grid.dx);
                                double Zval = grid.GetZvalue(row, col);
                                if (Zval == grid.BlankVal) continue;
                                if ((row > -1) && (col > -1))
                                {
                                    if (TipText.Length > 0) TipText += "\n";
                                    TipText += string.Format("{0}: {1:0.##}", ((C2DLayer)list[i]).Name, Zval);
                                }
                            }
                            break;
                        case Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP:
                            vMap = (VoronoiMap)list[i].ObjectsList[0];
                            ind = vMap.HitTest(ScreenX, ScreenY);
                            if (ind != -1)
                            {
                                List<int> selIndexes = vMap.GetSelectedCells();
                                TipText = string.Format("{0}\nскв.{1} НИЗ: {2:0.###}тыс.т, ОИЗ {3:0.###}тыс.т", vMap.Name, vMap.Cells[ind].Well.srcWell.Name, vMap.Cells[ind].NIZ / 1000, (vMap.Cells[ind].NIZ - vMap.Cells[ind].AccumOil) / 1000);
                                if (selIndexes.IndexOf(ind) != -1 && selIndexes.Count > 1)
                                {
                                    double sumNiz = 0, sumOiz = 0;
                                    for (int j = 0; j < selIndexes.Count; j++)
                                    {
                                        sumNiz += vMap.Cells[selIndexes[j]].NIZ / 1000;
                                        sumOiz += (vMap.Cells[selIndexes[j]].NIZ - vMap.Cells[selIndexes[j]].AccumOil) / 1000;
                                    }
                                    TipText += string.Format("\nСумма НИЗ: {0:0.###}тыс.т, ОИЗ {1:0.###}тыс.т", sumNiz, sumOiz);
                                }
                            }
                            break;
                    }
                }
            }
            return TipText;
        }

        // DRAWING
        //public double ScreenXfromRealX(double rX)
        //{
        //    return (double)(-BitMapRect.Left + (rX - ObjectsRect.Left) / XUnitsInOnePixel);
        //}
        //public double ScreenYfromRealY(double rY)
        //{
        //    return (double)(-BitMapRect.Top + (ObjectsRect.Bottom - rY) / YUnitsInOnePixel);
        //}
        //public double RealXfromScreenX(double sX)
        //{
        //    return ObjectsRect.Left + (sX + BitMapRect.Left) * XUnitsInOnePixel;
        //}
        //public double RealYfromScreenY(double sY)
        //{
        //    return ObjectsRect.Bottom - (sY + BitMapRect.Top) * YUnitsInOnePixel;
        //}

        private void PreparePaint()
        {
            C2DObject.st_BMPRect = this.Parent.ClientRectangle;
            C2DObject.st_BMPRect.X = C2DObject.st_BMPRect.X - 50;
            C2DObject.st_BMPRect.Y = C2DObject.st_BMPRect.Y - 50;
            C2DObject.st_BMPRect.Height = C2DObject.st_BMPRect.Height + 100;
            C2DObject.st_BMPRect.Width = C2DObject.st_BMPRect.Width + 100;

            C2DObject.st_BitMapRect = BitMapRect;
            if (MapProject != null) C2DObject.st_ObjectsRect = MapProject.MinMaxXY;
            else if (LayerList.Count > 0) C2DObject.st_ObjectsRect = ((C2DLayer)LayerList[0]).ObjRect;

            C2DObject.st_RealScreenRect = C2DObject.RealRectFromScreen(C2DObject.st_BMPRect);

            //C2DObject.st_XUnitsInOnePixel = XUnitsInOnePixel;
            //C2DObject.st_YUnitsInOnePixel = YUnitsInOnePixel;
        }
        public void DrawLayerList()
        {
            long time1 = DateTime.Now.Ticks;
            DrawLayerList(bmp_grfx);
            timeDraw = (DateTime.Now.Ticks - time1);
            this.Invalidate();
        }
        public void DrawLayerList(Graphics grfx)
        {
            grfx.Clear(Color.White);
            if ((!_disabled) && (LayerList.Count >= 0))
            {
                // отрисовка слоев
                int i;
                C2DLayer layer;
                PreparePaint();
                if ((MapProject != null) && (MapProject.Visible))
                {
                    List<C2DLayer> list = new List<C2DLayer>();
                    for (i = 0; i < LayerList.Count; i++)
                    {
                        if (((C2DLayer)LayerList[i]).Visible)
                        {
                            ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.GRID, list, true);
                            ((C2DLayer)LayerList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.DEPOSIT, list, true);
                        }
                    }
                    for (i = 0; i < LayerWorkList.Count; i++)
                    {
                        if (((C2DLayer)LayerWorkList[i]).Visible)
                        {
                            ((C2DLayer)LayerWorkList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.IMAGE, list, true);
                            ((C2DLayer)LayerWorkList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.GRID, list, true);
                            ((C2DLayer)LayerWorkList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.MARKER, list, true);
                            if (C2DObject.st_XUnitsInOnePixel <= C2DObject.st_OilfieldXUnits) ((C2DLayer)LayerWorkList[i]).FillLayerByPhantomWells(list, true, false);
                            ((C2DLayer)LayerWorkList[i]).FillLayersByObjTypeID(Constant.BASE_OBJ_TYPES_ID.VORONOI_MAP, list, true);
                        }
                    }
                    for (i = 0; i < list.Count; i++)
                    {
                        layer = (C2DLayer)list[i];
                        if ((layer.Visible) && (layer.VisibleFar))
                        {
                            if ((layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.IMAGE) ||
                                (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID) ||
                                (C2DObject.DrawAllObjects || layer.IntersectWith(C2DObject.st_RealScreenRect)))
                            {
                                layer.DrawObject(grfx);
                            }
                        }
                    }
                    list.Clear();
                    for (i = 0; i < LayerList.Count; i++)
                    {
                        if (((C2DLayer)LayerList[i]).Visible)
                        {
                            ((C2DLayer)LayerList[i]).FillLayerByWells(list, true, true);
                        }
                    }
                    for (i = 0; i < list.Count; i++)
                    {
                        layer = (C2DLayer)list[i];
                        if (C2DObject.DrawAllObjects || layer.IntersectWith(C2DObject.st_RealScreenRect))
                        {
                            for (int j = 0; j < layer.ObjectsList.Count; j++)
                            {
                                ((Well)layer.ObjectsList[j]).DrawBubbleMap(grfx);
                            }
                        }
                    }
                    list.Clear();

                    // отрисовка списков поверх карт отборов
                    for (i = 0; i < LayerWorkList.Count; i++)
                    {
                        if (((C2DLayer)LayerWorkList[i]).Visible)
                        {
                            ((C2DLayer)LayerWorkList[i]).FillLayerByPhantomWells(list, true, true);
                        }
                    }
                    for (i = 0; i < list.Count; i++)
                    {
                        layer = (C2DLayer)list[i];
                        if (C2DObject.DrawAllObjects || layer.IntersectWith(C2DObject.st_RealScreenRect))
                        {
                            layer.DrawObject(grfx);
                        }
                    }
                    list.Clear();

                    for (i = 0; i < LayerList.Count; i++)
                    {
                        layer = (C2DLayer)LayerList[i];
                        if (layer.Visible && layer.VisibleFar && (C2DObject.DrawAllObjects || layer.IntersectWith(C2DObject.st_RealScreenRect)))
                        {
                            layer.DrawMainLine(grfx);
                        }
                    }

                    // отрисовка рабочих слоев
                    for (i = 0; i < LayerWorkList.Count; i++)
                    {
                        layer = (C2DLayer)LayerWorkList[i];

                        if ((layer.Visible) && (layer.VisibleFar) &&
                            (layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.IMAGE) &&
                            (layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.GRID) &&
                            (layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.MARKER) &&
                            ((C2DObject.st_XUnitsInOnePixel > C2DObject.st_OilfieldXUnits) || (layer.ObjTypeID != Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)) &&
                            (C2DObject.DrawAllObjects || layer.IntersectWith(C2DObject.st_RealScreenRect)))
                        {
                            layer.DrawMainLine(grfx);
                        }
                    }

                    // отрисовка Selected Contour
                    for (i = 0; i < LayerList.Count; i++)
                    {
                        layer = (C2DLayer)LayerList[i];
                        if (layer.Visible && layer.VisibleFar)
                        {
                            if (C2DObject.DrawAllObjects || layer.IntersectWith(C2DObject.st_RealScreenRect))
                            {
                                layer.DrawSelectedObject(grfx);
                            }
                        }
                    }
                    for (i = 0; i < LayerWorkList.Count; i++)
                    {
                        layer = (C2DLayer)LayerWorkList[i];
                        if (layer.Visible && layer.VisibleFar)
                        {
                            if (C2DObject.DrawAllObjects || layer.IntersectWith(C2DObject.st_RealScreenRect))
                            {
                                layer.DrawSelectedObject(grfx);
                            }
                        }
                    }


                    // отрисовка имен условных контуров
                    for (i = 0; i < LayerList.Count; i++)
                    {
                        layer = (C2DLayer)LayerList[i];
                        if (layer.Visible && layer.VisibleFar && (C2DObject.DrawAllObjects || layer.IntersectWith(C2DObject.st_RealScreenRect)))
                        {
                            layer.DrawNameRecursive(grfx);
                        }
                    }

                    // режим создания контура
                    if (newContour != null)
                    {
                        newContour.DrawObject(grfx);
                    }

                    // режим поворота
                    if ((EditedLayerRotate) && (EditedLayerRotateCenter.X != -1))
                    {
                        grfx.FillRectangle(Brushes.Red, EditedLayerRotateCenter.X - 5, EditedLayerRotateCenter.Y - 5, 10, 10);
                    }
                }
            }
        }
        void DrawSnapShootMode(Graphics grfx)
        {
            if ((snapShootMode) && (!SnapPt1.IsEmpty) && (MouseButtonDown == MouseButtons.Left))
            {
                float minX, minY, maxX, maxY;
                if (MButtonTimerPoint.X > SnapPt1.X)
                {
                    minX = SnapPt1.X;
                    maxX = MButtonTimerPoint.X;
                }
                else
                {
                    minX = MButtonTimerPoint.X;
                    maxX = SnapPt1.X;
                }
                if (MButtonTimerPoint.Y > SnapPt1.Y)
                {
                    minY = SnapPt1.Y;
                    maxY = MButtonTimerPoint.Y;
                }
                else
                {
                    minY = MButtonTimerPoint.Y;
                    maxY = SnapPt1.Y;
                }

                if (minX < this.ClientRectangle.Left) minX = this.ClientRectangle.Left;
                if (maxX > this.ClientRectangle.Right) maxX = this.ClientRectangle.Right;
                if (minY < this.ClientRectangle.Top) minY = this.ClientRectangle.Top;
                if (maxY > this.ClientRectangle.Bottom) maxY = this.ClientRectangle.Bottom;

                grfx.FillRectangle(brSnap, minX, minY, maxX - minX, maxY - minY);
                grfx.DrawRectangle(Pens.Black, minX, minY, maxX - minX, maxY - minY);
            }
        }
        private void DrawProfileMode(Graphics grfx)
        {
            if ((AddWellProfileMode) && (selWellList.Count > 0))
            {
                float PredScrX = 0, PredScrY = 0, scrX, scrY;
                double PredRealX = 0, PredRealY = 0;
                Well w = (Well)selWellList[0];
                float r = C2DObject.st_radiusX;

                for (int i = 0; i < selWellList.Count; i++)
                {
                    w = (Well)selWellList[i];

                    if (C2DObject.DrawAllObjects || C2DObject.st_RealScreenRect.Contains(PredRealX, PredRealY) || C2DObject.st_RealScreenRect.Contains(w.X, w.Y))
                    {
	                    scrX = C2DObject.ScreenXfromRealX(w.X);
	                    scrY = C2DObject.ScreenYfromRealY(w.Y);
                        PredScrX = scrX; PredScrY = scrY;
                        PredRealX = w.X; PredRealY = w.Y;
                        if (i > 0)
	                    {
	                        grfx.DrawLine(Pens.Black, PredScrX, PredScrY, scrX, scrY);
	                        grfx.FillEllipse(Brushes.Yellow, PredScrX - r / 4, PredScrY - r / 4, r / 2, r / 2);
	                        grfx.DrawEllipse(Pens.Black, PredScrX - r, PredScrY - r, r * 2, r * 2);
	                    }
                        else if (i == selWellList.Count - 1)
	                    {
	                        grfx.FillEllipse(Brushes.Yellow, scrX - r / 4, scrY - r / 4, r / 2, r / 2);
	                        grfx.DrawEllipse(Pens.Black, scrX - r, scrY - r, r * 2, r * 2);
	
	                    }
                	}
            	}
        	}
        }
        private void DrawNewContourPointerLine(Graphics gfx)
        {
            if ((CtrlPressed) && (!newContourEnd) && (newContour != null) && (newContour._points != null))
            {
                int len;
                float screenX, screenY;
                len = newContour._points.Count;
                screenX = C2DObject.ScreenXfromRealX(newContour._points[len - 1].X);
                screenY = C2DObject.ScreenYfromRealY(newContour._points[len - 1].Y);
                gfx.DrawLine(SmartPlusGraphics.Contour.Pens.Fantom, screenX, screenY, MButtonTimerPoint.X, MButtonTimerPoint.Y);
            }
        }
        private void DrawShiftPointSelContour(Graphics gfx)
        {
            if ((ShiftPressed) && (ShiftSelContourPoint > -1) && (ShiftSelContourIndex > -1))
            {
                C2DLayer selContourLayer = (C2DLayer)selLayerList[ShiftSelContourIndex];
                Contour selContour = (Contour)selContourLayer.ObjectsList[0];
                int shift1 = -1, shift2 = -1;
                shift1 = ShiftSelContourPoint - 1;
                shift2 = ShiftSelContourPoint + 1;
                int radius = 15;
                if (shift1 < 0) shift1 = selContour._points.Count - 1;
                if (shift2 > selContour._points.Count - 1) shift2 = 0;

                float screenX1, screenY1, screenX2, screenY2, xR, yR;
                double rX, rY, k1, k2, phi, sX, sY;
                if (ShiftRightAngle) ShiftAdhesionRealPoint.X = Constant.DOUBLE_NAN;
                ShiftRightAngle = false;
                screenX1 = C2DObject.ScreenXfromRealX(selContour._points[shift1].X);
                screenY1 = C2DObject.ScreenYfromRealY(selContour._points[shift1].Y);
                screenX2 = C2DObject.ScreenXfromRealX(selContour._points[shift2].X);
                screenY2 = C2DObject.ScreenYfromRealY(selContour._points[shift2].Y);
                rX = C2DObject.RealXfromScreenX(MButtonTimerPoint.X);
                rY = C2DObject.RealYfromScreenY(MButtonTimerPoint.Y);

                if (selContour._points[shift1].X - rX == 0) k1 = 0;
                else k1 = (selContour._points[shift1].Y - rY) / (selContour._points[shift1].X - rX);
                if (selContour._points[shift2].X - rX == 0) k2 = 0;
                else k2 = (selContour._points[shift2].Y - rY) / (selContour._points[shift2].X - rX);
                if ((k1 == k2) && (k1 == 0) && (selContour._points[shift2].Y != rY)) phi = 90;
                else phi = (Math.Acos(Math.Abs(k1 * k2 + 1) / (Math.Sqrt(1 + k1 * k1) * Math.Sqrt(1 + k2 * k2))) * 180 / Math.PI);
                sX = MButtonTimerPoint.X;
                sY = MButtonTimerPoint.Y;
                if (C2DObject.DrawAllObjects || C2DObject.st_BMPRect.Contains(screenX1, screenY1) || C2DObject.st_BMPRect.Contains(MButtonTimerPoint.X, MButtonTimerPoint.Y))
                {
                    gfx.DrawLine(_penFantom, screenX1, screenY1, MButtonTimerPoint.X, MButtonTimerPoint.Y);
                    gfx.DrawLine(_penFantom, MButtonTimerPoint.X, MButtonTimerPoint.Y, screenX2, screenY2);
                    gfx.DrawEllipse(_penFantom, MButtonTimerPoint.X - 4.5f, MButtonTimerPoint.Y - 4.5f, 9, 9);
                }
                //gfx.DrawRectangle(selContour._penFantom, MButtonTimerPoint.X - 4.5f, MButtonTimerPoint.Y - 4.5f, 9, 9);
                if ((phi > 89) && (ShiftAdhesionRealPoint.X == Constant.DOUBLE_NAN))
                {
                    ShiftRightAngle = true;
                    ShiftAdhesionRealPoint.X = Constant.DOUBLE_NAN;
                    if (((Math.Abs(sX - screenX1) <= radius) && (sX < screenX2)) || ((Math.Abs(sX - screenX2) <= radius) && (sX < screenX1))) xR = MButtonTimerPoint.X;
                    else xR = MButtonTimerPoint.X - 12;
                    if (((Math.Abs(sY - screenY1) <= radius) && (sY < screenY2)) || ((Math.Abs(sY - screenY2) <= radius) && (sY < screenY1))) yR = MButtonTimerPoint.Y;
                    else yR = MButtonTimerPoint.Y - 12;
                    if ((Math.Abs(sX - screenX1) > radius) && (Math.Abs(sX - screenX2) > radius)) ShiftRightAngle = false;
                    if ((Math.Abs(sY - screenY1) > radius) && (Math.Abs(sY - screenY2) > radius)) ShiftRightAngle = false;
                    if (ShiftRightAngle)
                    {
                        ShiftAdhesionRealPoint.X = Constant.DOUBLE_NAN;
                        if ((Math.Abs(sX - screenX1) <= radius) && (Math.Abs(sX - screenX2) > radius))
                        {
                            ShiftAdhesionRealPoint.X = selContour._points[shift1].X;
                        }
                        else if ((Math.Abs(sX - screenX2) <= radius) && (Math.Abs(sX - screenX1) > radius))
                        {
                            ShiftAdhesionRealPoint.X = selContour._points[shift2].X;
                        }

                        if ((Math.Abs(sY - screenY1) <= radius) && (Math.Abs(sY - screenY2) > radius))
                        {
                            ShiftAdhesionRealPoint.Y = selContour._points[shift1].Y;
                        }
                        else if ((Math.Abs(sY - screenY2) <= radius) && (Math.Abs(sY - screenY1) > radius))
                        {
                            ShiftAdhesionRealPoint.Y = selContour._points[shift2].Y;
                        }
                        gfx.DrawRectangle(_penFantom, xR, yR, 12, 12);
                    }
                }
            }
        }
        private void DrawGrid(Graphics gfx)
        {
            if (ShowGrid)
            {
                int x, y, w, h;
                w = this.ClientRectangle.Width;
                h = this.ClientRectangle.Height;
                x = this.ClientRectangle.Left;
                Pen pen = new Pen(Brushes.Black, 1);
                pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                while (x < w)
                {
                    gfx.DrawLine(pen, x, 0, x, h);
                    x += 50;
                }
                y = this.ClientRectangle.Top;
                while (y < h)
                {
                    gfx.DrawLine(pen, 0, y, w, y);
                    y += 50;
                }
            }
        }
        private void DrawProfileWell(Graphics grfx)
        {
            Well w = ProfileOverWell;
            if ((w != null) && (C2DObject.st_VisbleLevel <= C2DObject.st_OilfieldXUnits))
            {
                using (Pen pen = new Pen(Color.Lime, 3))
                {
                    float radPrfl = C2DObject.st_radiusX * 1.5f;
                    if (radPrfl < 5) radPrfl = 5;
                    float screenX, screenY;
                    WellIcon icon = w.icons.GetActiveIcon();
                    if ((w.BubbleList != null) && (w.BubbleList.Visible))
                    {
                        icon = w.BubbleIcons.GetActiveIcon();
                    }

                    if (icon == null)
                    {
                        screenX = C2DObject.ScreenXfromRealX(w.X);
                        screenY = C2DObject.ScreenYfromRealY(w.Y);
                    }
                    else
                    {
                        screenX = C2DObject.ScreenXfromRealX(icon.X);
                        screenY = C2DObject.ScreenYfromRealY(icon.Y);
                    }
                    grfx.DrawEllipse(pen, screenX - radPrfl, screenY - radPrfl, radPrfl * 2, radPrfl * 2);
                }
            }
        }
        private void DrawBPContourParams(Graphics grfx)
        {
            if (BPcontour != null) BPcontour.DrawContourName(grfx, true);
        }
        public void CanvasPaintHandler(object objSender, PaintEventArgs pea)
        {
            Graphics grfx = pea.Graphics;
            grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            grfx.Clear(Color.White);

            grfx.DrawImageUnscaledAndClipped(bmp, this.ClientRectangle);
            DrawNewContourPointerLine(grfx);
            DrawShiftPointSelContour(grfx);
            DrawProfileMode(grfx);
            DrawSnapShootMode(grfx);
            DrawProfileWell(grfx);
            DrawBPContourParams(grfx);
        }
    }
}
