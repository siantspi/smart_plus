using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using RDF;
using RDF.Objects;
using System.IO;
using System.Text;
using System.Collections.Generic;
using SmartPlus.DictionaryObjects;

namespace SmartPlus 
{
    #region Base Objects
    public struct PointD
    {
        public double X;
        public double Y;
    }
    public class RectangleD 
    {
        public double Left;
        public double Top;
        public double Right;
        public double Bottom;
        public double Width { get { return Right - Left; } }
        public double Height { get { return Bottom - Top; } }
        public RectangleD() 
        {
            Left = Constant.DOUBLE_NAN;
            Top = Constant.DOUBLE_NAN;
            Right = Constant.DOUBLE_NAN;
            Bottom = Constant.DOUBLE_NAN;
        }
        public RectangleD(RectangleD rect)
        {
            Left = rect.Left;
            Top = rect.Top;
            Right = rect.Right;
            Bottom = rect.Bottom;
        }
        public RectangleD(double Left, double Top, double Right, double Bottom)
        {
            this.Left = Left;
            this.Top = Top;
            this.Right = Right;
            this.Bottom = Bottom;
        }
        public bool IntersectWith(RectangleD rect)
        {
            return !((rect.Right < Left) || (Right < rect.Left) || (rect.Bottom < Top) || (Bottom < rect.Top));
        }
        public bool Contains(double X, double Y) 
        {
            if ((X >= Left) && (X <= Right) && (Y >= Top) && (Y <= Bottom)) return true;
            else return false;
        }
    }

    public class PointsXY : C2DObject 
    {
        public int Count { get { return ArrayPoint.Count; } }
        public List<PointD> ArrayPoint;

        public PointsXY(int count) 
        {
            ArrayPoint = new List<PointD>(count);
        }
        public PointsXY(PointsXY arr)
        {
            ArrayPoint = new List<PointD>(arr.ArrayPoint);
        }
        public PointsXY(PointD[] arr)
        {
            ArrayPoint = new List<PointD>(arr);
        }
        public void Exchange(int ind1, int ind2) 
        {
            PointD point = new PointD();
            if ((ind2 < ArrayPoint.Count) && (ind2 < ArrayPoint.Count)) 
            {
                point = ArrayPoint[ind1];
                ArrayPoint[ind1] = ArrayPoint[ind2];
                ArrayPoint[ind2] = point;
            }
        }
        public double GetDistanceXY(int ind1, int ind2) 
        {
            double dx = ((PointD)ArrayPoint[ind1]).X - ((PointD)ArrayPoint[ind2]).X;
            double dy = ((PointD)ArrayPoint[ind1]).Y - ((PointD)ArrayPoint[ind2]).Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        public double GetDistanceXY(double x1, double y1, int ind2) {
            double dx = x1 - ((PointD)ArrayPoint[ind2]).X;
            double dy = y1 - ((PointD)ArrayPoint[ind2]).Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        public double GetRMS(int num) 
        {
            if (ArrayPoint.Count == 0) return 0;
            double val, sum = 0;
            for (int i = 0; i < ArrayPoint.Count; i++)
            {
                val = GetValue(i, num);
                sum += val * val;
            }
            return Math.Sqrt(sum / ArrayPoint.Count);
        }
        public double GetLength()
        {
            double sum = 0;
            for (int i = 0; i < ArrayPoint.Count - 1; i++)
            {
                sum += GetDistanceXY(i, i + 1);
            }
            return sum;
        }
        public double GetSquare()
        {
            double square = 0;
            double y1, y2;
            if (Count < 3) return 0;
            for (int i = 0; i < Count; i++)
            {
                if (i == 0)
                {
                    y1 = ArrayPoint[Count - 1].Y;
                    y2 = ArrayPoint[1].Y;
                }
                else if (i == Count - 1)
                {
                    y1 = ArrayPoint[i - 1].Y;
                    y2 = ArrayPoint[0].Y;
                }
                else
                {
                    y1 = ArrayPoint[i - 1].Y;
                    y2 = ArrayPoint[i + 1].Y;
                }
                square += ArrayPoint[i].X * (y1 - y2);
            }
            return Math.Abs(square / 2); 
        }
       
        public void Sort(int num) 
        { 
            // 0 - �� X, 1 - Y
            int i, j;
            switch (num) { 
                case 0:
                    for (i = 0; i < Count; i++) 
                    {
                        for (j = 0; j < Count - i; j++) 
                        {
                            if (ArrayPoint[j].X < ArrayPoint[j + 1].X) 
                            {
                                Exchange(j, j + 1);
                            }
                        }
                    }
                    break;
                case 1:
                    for (i = 0; i < Count; i++) 
                    {
                        for (j = 0; j < Count - i; j++) 
                        {
                            if (ArrayPoint[j].Y < ArrayPoint[j + 1].Y) 
                            {
                                Exchange(j, j + 1);
                            }
                        }
                    }
                    break;
            }
        }

        public double GetValue(int ind, int num) 
        {
            switch (num) 
            {
                case 0: 
                    return ((PointD)ArrayPoint[ind]).X;
                case 1:
                    return ((PointD)ArrayPoint[ind]).Y;
            }
            return Constant.DOUBLE_NAN;
        }
        public void Add(double X, double Y)
        {
            PointD pt;
            pt.X = X;
            pt.Y = Y;
            ArrayPoint.Add(pt);
        }
        public void Insert(int Index, double X, double Y)
        {
            PointD pt;
            pt.X = X;
            pt.Y = Y;
            ArrayPoint.Insert(Index, pt);
        }
        public void RemoveAt(int index)
        {
            ArrayPoint.RemoveAt(index);
        }
        public PointD this[int index]
        {
            get { return ArrayPoint[index]; }
            set 
            {
                ArrayPoint[index] = value;
            }
        }
        public double GetMinX() 
        {
            double item;
            double min = double.MaxValue;
            for (int i = 0; i < Count; i++) 
            {
                item = ArrayPoint[i].X;
                if (min > item) min = item;
            }
            return min;
        }
        public double GetMaxX() 
        {
            double item;
            double max = double.MinValue;
            for (int i = 0; i < Count; i++) 
            { 
                item = ArrayPoint[i].X;
                if (max < item) max = item;
            }
            return max;
        }
        public double GetMinY() 
        {
            double item;
            double min = double.MaxValue;
            for (int i = 0; i < Count; i++) 
            {
                item = ArrayPoint[i].Y;
                if (min > item) min = item;
            }
            return min;
        }
        public double GetMaxY() 
        {
            double item;
            double max = double.MinValue;
            for (int i = 0; i < Count; i++) 
            {
                item = ArrayPoint[i].Y;
                if (max < item) max = item;
            }
            return max;
        }
        public PointD GetAvarage()
        {
            PointD ave;
            ave.X = Constant.DOUBLE_NAN;
            ave.Y = Constant.DOUBLE_NAN;
            double sumX = 0, sumY = 0;
            for (int i = 0; i < Count; i++)
            {
                sumX += ArrayPoint[i].X;
                sumY += ArrayPoint[i].Y;
            }
            if (Count > 0)
            {
                ave.X = sumX / Count;
                ave.Y = sumY / Count;
            }
            return ave; 
        }
    }
    #endregion

    #region Project Objects

    public sealed class BubbleParamItem
    {
        public int OilObjIndex;
        public double Liq;
        public double LiqV;
        public double Oil;
        public double OilV;
        public double Inj;
        public double InjGas;
        public double NatGas;
        public double GasCond;
        public MerWorkTimeCollection ProdWorkTime;
        public MerWorkTimeCollection InjWorkTime;
        public double hLiq;
        public double hLiqV;
        public double hOil;
        public double hOilV;
        public double hInj;
        public double hInjGas;
        public double hNatGas;
        public double hGasCond;
        public MerWorkTimeCollection HistProdWorkTime;
        public MerWorkTimeCollection HistInjWorkTime;
        public BubbleParamItem(int OilObjIndex)
        {
            this.OilObjIndex = OilObjIndex;
            this.Liq = 0;
            this.LiqV = 0;
            this.Oil = 0;
            this.OilV = 0;
            this.Inj = 0;
            ProdWorkTime = null;
            InjWorkTime = null;
            this.hLiq = 0;
            this.hLiqV = 0;
            this.hOil = 0;
            this.hOilV = 0;
            this.hNatGas = 0;
            this.hGasCond = 0;
            this.hInj = 0;
            HistInjWorkTime = null;
            HistProdWorkTime = null;
        }
    }

    public sealed class BubbleSign
    {
        public BubbleSign(Constant.BUBBLE_SIGN_TYPE type, float rBubble)
        {
            Type = type;
            RBubble = rBubble;
        }
        public Constant.BUBBLE_SIGN_TYPE Type;
        public float RBubble;
    }

    public sealed class BubbleParamList
    {
        ArrayList Items;
        public bool Visible;
        public bool LiquidInCube;
        public Constant.BUBBLE_MAP_TYPE Type;
        public double MaxVal;
        public float K;
        public bool hBySector;
        public int Charwork;
        public double Liq;
        public double LiqV;
        public double Oil;
        public double OilV;
        public double Inj;
        public double InjGas;
        public double NatGas;
        public double GasCond;
        public MerWorkTimeCollection ProdWorkTime;
        public MerWorkTimeCollection InjWorkTime;

        public double HLiq;
        public double HLiqV;
        public double HOil;
        public double HOilV;
        public double HInj;
        public double HInjGas;
        public double HNatGas;
        public double HGasCond;
        public MerWorkTimeCollection HistProdWorkTime;
        public MerWorkTimeCollection HistInjWorkTime;        

        public float RLiq;
        public float RInj;
        public float RInjGas;
        public float RSumInj;
        public float RSumNatGas;
        public float alphaOil;
        public float alphaInjGaz;
        public float alphaGasCond;
        public float hRLiq;
        public float hRInj;
        public float hRInjGas;
        public float halphaOil;
        public List<BubbleSign> BubbleSignPos;

        public BubbleParamList(Constant.BUBBLE_MAP_TYPE Type, bool LiquidInCube)
        {
            Items = new ArrayList();
            this.Visible = true;
            MaxVal = 0;
            this.Type = Type;
            this.K = 0.1f;
            this.hBySector = false;
            this.LiquidInCube = LiquidInCube;
            ClearRadiusAngle();
        }
        public int Count
        {
            get 
            {
                if (Items != null) return Items.Count;
                else return 0;
            }
        }

        public void Add(BubbleParamItem item)
        {
            if (Items == null) Items = new ArrayList();
            Items.Add(item);
        }
        public void Insert(int index, BubbleParamItem item)
        {
            if ((index > -1) && (index <= Count))
            {
                if (Items == null) Items = new ArrayList();
                Items.Insert(index, item);
            }
        }
        public BubbleParamItem GetItemByObjIndex(int OilObjIndex)
        {
            for (int i = 0; i < Count; i++)
            {
                if (((BubbleParamItem)Items[i]).OilObjIndex == OilObjIndex)
                {
                    return (BubbleParamItem)Items[i];
                }
            }
            return null;
        }
        public BubbleParamItem this[int index]
        {
            get
            {
                if ((index > -1) && (index < Count))
                {
                    return (BubbleParamItem)Items[index];
                }
                return null;
            }
        }
        public void RemoveAtObjIndex(int OilObjIndex)
        {
            for (int i = 0; i < Count; i++)
            {
                if (((BubbleParamItem)Items[i]).OilObjIndex == OilObjIndex)
                {
                    Items.RemoveAt(i);
                    break;
                }
            }
        }
        public void RemoveAt(int index)
        {
            if ((index > -1) && (index < Count))
                Items.RemoveAt(index);
        }
        public void Clear()
        {
            Items.Clear();
            Items = null;
            Visible = true;
        }

        private void AddItemByFilter(BubbleParamItem item)
        {
            if (item != null)
            {
                switch (this.Type)
                {
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                        this.Liq += item.Liq;
                        this.LiqV += item.LiqV;
                        this.Oil += item.Oil;
                        this.OilV += item.OilV;
                        this.Inj += item.Inj;
                        this.InjGas += item.InjGas;
                        this.NatGas += item.NatGas;
                        this.GasCond += item.GasCond;
                        if(item.ProdWorkTime != null)
                        {
                            if (this.ProdWorkTime == null) this.ProdWorkTime = new MerWorkTimeCollection();
                            for(int i = 0 ; i < item.ProdWorkTime.Count; i++) this.ProdWorkTime.Add(item.ProdWorkTime[i]);
                        }
                        if(item.InjWorkTime != null)
                        {
                            if (this.InjWorkTime == null) this.InjWorkTime = new MerWorkTimeCollection();
                            for(int i = 0 ; i < item.InjWorkTime.Count; i++) this.InjWorkTime.Add(item.InjWorkTime[i]);
                        }
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                        this.Liq += item.Liq;
                        this.LiqV += item.LiqV;
                        this.Oil += item.Oil;
                        this.OilV += item.OilV;
                        this.Inj += item.Inj;
                        this.InjGas += item.InjGas;
                        this.NatGas += item.NatGas;
                        this.GasCond += item.GasCond;
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                        this.Liq += item.Liq;
                        this.LiqV += item.LiqV;
                        this.Oil += item.Oil;
                        this.OilV += item.OilV;
                        this.Inj += item.Inj;
                        this.InjGas += item.InjGas;
                        this.NatGas += item.NatGas;
                        this.GasCond += item.GasCond;
                        if (item.ProdWorkTime != null)
                        {
                            if (this.ProdWorkTime == null) this.ProdWorkTime = new MerWorkTimeCollection();
                            for (int i = 0; i < item.ProdWorkTime.Count; i++) this.ProdWorkTime.Add(item.ProdWorkTime[i]);
                        }
                        if (item.InjWorkTime != null)
                        {
                            if (this.InjWorkTime == null) this.InjWorkTime = new MerWorkTimeCollection();
                            for (int i = 0; i < item.InjWorkTime.Count; i++) this.InjWorkTime.Add(item.InjWorkTime[i]);
                        }
                        this.HLiq += item.hLiq;
                        this.HLiqV += item.hLiqV;
                        this.HOil += item.hOil;
                        this.HOilV += item.hOilV;
                        this.HInj += item.hInj;
                        this.HInjGas += item.hInjGas;
                        this.HNatGas += item.hNatGas;
                        this.HGasCond += item.hGasCond;
                        if (item.HistProdWorkTime != null)
                        {
                            if (this.HistProdWorkTime == null) this.HistProdWorkTime = new MerWorkTimeCollection();
                            for (int i = 0; i < item.HistProdWorkTime.Count; i++) this.HistProdWorkTime.Add(item.HistProdWorkTime[i]);
                        }
                        if (item.HistInjWorkTime != null)
                        {
                            if (this.HistInjWorkTime == null) this.HistInjWorkTime = new MerWorkTimeCollection();
                            for (int i = 0; i < item.HistInjWorkTime.Count; i++) this.HistInjWorkTime.Add(item.HistInjWorkTime[i]);
                        }
                        break;
                }
            }
        }
        private void CalcRadiusAngle()
        {
            if(Count > 0)
            {
                MerWorkTimeItem wtItem;
                switch (this.Type)
                {
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_CURRENT:
                        
                        if (this.ProdWorkTime != null)
                        {
                            wtItem = this.ProdWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                this.Liq = this.Liq * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.LiqV = this.LiqV * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.Oil = this.Oil * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.OilV = this.OilV * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.NatGas = this.NatGas * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.GasCond = this.GasCond * 24 / (wtItem.WorkTime + wtItem.CollTime);
                            }
                        }
                        else
                        {
                            this.Liq = 0; this.LiqV = 0; this.Oil = 0; this.OilV = 0; this.NatGas = 0;
                        }
                        if (LiquidInCube)
                        {
                            this.RLiq = (float)Math.Sqrt(this.LiqV / Math.PI);
                            if (this.LiqV > 0) this.alphaOil = (float)((2 * 180 * this.OilV) / this.LiqV);
                            if (this.LiqV > this.MaxVal) this.MaxVal = this.LiqV;
                        }
                        else
                        {
                            this.RLiq = (float)Math.Sqrt(this.Liq / Math.PI);
                            if (this.Liq > 0) this.alphaOil = (float)((2 * 180 * this.Oil) / this.Liq);
                            if (this.Liq > this.MaxVal) this.MaxVal = this.Liq; 
                        }
                        if (this.NatGas > 0 || this.GasCond > 0)
                        {
                            this.RSumNatGas = (float)Math.Sqrt((this.NatGas + this.GasCond) / Math.PI);
                            if ((this.NatGas + this.GasCond) > 0) this.alphaGasCond = (float)((2 * 180 * this.GasCond) / (this.GasCond + this.NatGas));
                            if ((this.GasCond + this.NatGas) > this.MaxVal) this.MaxVal = (this.GasCond + this.NatGas);
                        }
                        if (this.InjWorkTime != null)
                        {
                            wtItem = this.InjWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                this.Inj = this.Inj * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.InjGas = this.InjGas * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                if ((this.InjGas + this.Inj) > 0) this.alphaInjGaz = (float)((2 * 180 * this.InjGas) / (this.InjGas + this.Inj));
                            }
                        }
                        else
                        {
                            this.Inj = 0; this.InjGas = 0;
                        }
                        if ((this.Inj + this.InjGas) > this.MaxVal) this.MaxVal = (this.Inj + this.InjGas);
                        this.RSumInj = (float)Math.Sqrt((this.InjGas + this.Inj) / Math.PI);
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_ACCUM:
                        BubbleSignPos = new List<BubbleSign>();
                        if (this.Liq > 0 || this.LiqV > 0 || this.Oil > 0 || this.OilV > 0)
                        {
                            if (LiquidInCube)
                            {
                                this.RLiq = (float)Math.Sqrt(this.LiqV / Math.PI);
                                if (this.LiqV > 0) this.alphaOil = (float)((2 * 180 * this.OilV) / this.LiqV);
                                if (this.LiqV > this.MaxVal) this.MaxVal = this.LiqV;
                            }
                            else
                            {
                                this.RLiq = (float)Math.Sqrt(this.Liq / Math.PI);
                                if (this.Liq > 0) this.alphaOil = (float)((2 * 180 * this.Oil) / this.Liq);
                                if (this.Liq > this.MaxVal) this.MaxVal = this.Liq;
                            }
                            BubbleSignPos.Add(new BubbleSign(Constant.BUBBLE_SIGN_TYPE.Oil, RLiq));
                        }
                        if (this.NatGas > 0 || this.GasCond > 0)
                        {
                            if ((this.GasCond + this.NatGas) > this.MaxVal) this.MaxVal = (this.GasCond + this.NatGas);
                            this.RSumNatGas = (float)Math.Sqrt((this.NatGas + this.GasCond) / Math.PI);
                            if ((this.NatGas + this.GasCond) > 0) this.alphaGasCond = (float)((2 * 180 * this.GasCond) / (this.GasCond + this.NatGas));
                            BubbleSignPos.Add(new BubbleSign(Constant.BUBBLE_SIGN_TYPE.Gas,RSumNatGas));
                        }
                        if (this.Inj > 0 || this.InjGas > 0)
                        {
                            if ((this.Inj + this.InjGas) > this.MaxVal) this.MaxVal = (this.Inj + this.InjGas);
                            this.RSumInj = (float)Math.Sqrt((this.InjGas + this.Inj) / Math.PI);
                            if ((this.InjGas + this.Inj) > 0) this.alphaInjGaz = (float)((2 * 180 * this.InjGas) / (this.InjGas + this.Inj));
                            BubbleSignPos.Add(new BubbleSign(Constant.BUBBLE_SIGN_TYPE.Inj, RSumInj));
                        }
                        if (BubbleSignPos.Count > 1) BubbleSignPos.Sort(delegate(BubbleSign bs1, BubbleSign bs2) { return bs2.RBubble.CompareTo(bs1.RBubble); });
                        break;
                    case Constant.BUBBLE_MAP_TYPE.BUBBLE_HISTORY:
                        // ��������� ������������ �����
                        
                        if (this.HistProdWorkTime != null)
                        {
                            wtItem = this.HistProdWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                this.HLiq = this.HLiq * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.HLiqV = this.HLiqV * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.HOil = this.HOil * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.HOilV = this.HOilV * 24 / (wtItem.WorkTime + wtItem.CollTime);
                            }
                        }
                        else
                        {
                            this.HLiq = 0; this.HLiqV = 0; this.HOil = 0; this.HOilV = 0;
                        }
                        if (LiquidInCube)
                        {
                            this.hRLiq = (float)Math.Sqrt(this.HLiqV / Math.PI);
                            if (this.HLiqV > 0) this.halphaOil = (float)((2 * 180 * this.HOilV) / this.HLiqV);
                            if (this.HLiqV > this.MaxVal) this.MaxVal = this.HLiqV;
                        }
                        else
                        {
                            this.hRLiq = (float)Math.Sqrt(this.HLiq / Math.PI);
                            if (this.HLiq > 0) this.halphaOil = (float)((2 * 180 * this.HOil) / this.HLiq);
                            if (this.HLiq > this.MaxVal) this.MaxVal = this.HLiq;
                        }
                        if (this.HistInjWorkTime != null)
                        {
                            wtItem = this.HistInjWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                this.HInj = this.HInj * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.HInjGas = this.HInjGas * 24 / (wtItem.WorkTime + wtItem.CollTime);
                            }
                        }
                        else
                        {
                            this.HInj = 0; this.HInjGas = 0;
                        }
                        if (this.HInj > this.MaxVal) this.MaxVal = this.HInj;
                        if (this.HInjGas > this.MaxVal) this.MaxVal = this.HInjGas;
                        this.hRInj = (float)Math.Sqrt(this.HInj / Math.PI);
                        this.hRInjGas = (float)Math.Sqrt(this.HInjGas / Math.PI);

                        // ������� ������������ �����
                        
                        if (this.ProdWorkTime != null)
                        {
                            wtItem = this.ProdWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                this.Liq = this.Liq * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.LiqV = this.LiqV * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.Oil = this.Oil * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.OilV = this.OilV * 24 / (wtItem.WorkTime + wtItem.CollTime);
                            }
                        }
                        else
                        {
                            this.Liq = 0; this.LiqV = 0; this.Oil = 0; this.OilV = 0;
                        }
                        if (LiquidInCube)
                        {
                            this.RLiq = (float)Math.Sqrt(this.LiqV / Math.PI);
                            if (this.LiqV > 0) this.alphaOil = (float)((2 * 180 * this.OilV) / this.LiqV);
                            if (this.LiqV > this.MaxVal) this.MaxVal = this.LiqV;
                        }
                        else
                        {
                            this.RLiq = (float)Math.Sqrt(this.Liq / Math.PI);
                            if (this.Liq > 0) this.alphaOil = (float)((2 * 180 * this.Oil) / this.Liq);
                            if (this.Liq > this.MaxVal) this.MaxVal = this.Liq;
                        }

                        if (this.InjWorkTime != null)
                        {
                            wtItem = this.InjWorkTime.GetAllTime();
                            if (wtItem.WorkTime + wtItem.CollTime > 0)
                            {
                                this.Inj = this.Inj * 24 / (wtItem.WorkTime + wtItem.CollTime);
                                this.InjGas = this.InjGas * 24 / (wtItem.WorkTime + wtItem.CollTime);
                            }
                        }
                        else
                        {
                            this.Inj = 0; this.InjGas = 0;
                        }
                        if (this.Inj > this.MaxVal) this.MaxVal = this.Inj;
                        if (this.InjGas > this.MaxVal) this.MaxVal = this.InjGas;
                        this.RInj = (float)Math.Sqrt(this.Inj / Math.PI);
                        this.RInjGas = (float)Math.Sqrt(this.InjGas / Math.PI);
                      //  if ((this.InjGas + this.Inj) > this.MaxVal) this.MaxVal = this.InjGas + this.Inj;
                        this.RSumInj = (float)Math.Sqrt((this.InjGas + this.Inj) / Math.PI);
                        break;
                }
            }
        }
        private void ClearRadiusAngle()
        {
            this.Liq = 0;
            this.LiqV = 0;
            this.Oil = 0;
            this.OilV = 0;
            this.Inj = 0;
            this.InjGas = 0;
            this.NatGas = 0;
            this.GasCond = 0;
            this.ProdWorkTime = null;
            this.InjWorkTime = null;
            this.HLiq = 0;
            this.HLiqV = 0;
            this.HOil = 0;
            this.HOilV = 0;
            this.HInj = 0;
            this.HInjGas = 0;
            this.HGasCond = 0;
            this.HNatGas = 0;
            this.HistProdWorkTime = null;
            this.HistInjWorkTime = null; 
            this.RInj = 0;
            this.RInjGas = 0;
            this.RSumInj = 0;
            this.RSumNatGas = 0;
            this.RLiq = 0;
            this.hRLiq = 0;
            this.hRInj = 0;
            this.hRInjGas = 0;
            this.halphaOil = 0;
            this.alphaOil = 0;
            this.MaxVal = 0;
            this.BubbleSignPos = null;
        }
        public void SetBubbleByFilter(FilterOilObjects filter)
        {
            if ((filter != null) && (Count > 0))
            {
                BubbleParamItem item;
                ClearRadiusAngle();

                if (filter.AllSelected)
                {
                    item =  GetItemByObjIndex(-1);
                    if (item != null) AddItemByFilter(item);
                }
                else
                {
                    for (int i = 0; i < Count; i++)
                    {
                        item = (BubbleParamItem)Items[i];
                        if (item.OilObjIndex != -1)
                        {
                            if (filter.SelectedObjects.IndexOf(item.OilObjIndex) > -1)
                            {
                                AddItemByFilter(item);
                            }
                        }
                    }
                }
                CalcRadiusAngle();
            }
        }
    }

    public sealed class PhantomWellCaption
    {
        public string Text = null;
        public DateTime Date;
        public double Value;
        public PhantomWellCaption()
        {
            Text = "";
            Date = DateTime.MinValue;
            Value = Constant.DOUBLE_NAN;
        }
    }

    public sealed class PhantomWell : C2DObject
    {
        int srcOFCode;
        public bool VisibleName;
        public Well srcWell;

        public int IconSize;

        public Font _font;
        public Brush _brush;
        public Brush _brush_name;
        public WellsIconList icons;
        public bool CaptionVisible;
        public PhantomWellCaption Caption = null;
        
        public int Index
        {
            get { return srcWell.Index; }
        }
        public PhantomWell(int OilFieldCode, Well SourceWell)
        {
            this._brush_name = new SolidBrush(Color.Black);
            this.srcOFCode = OilFieldCode;
            this.TypeID = Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL;
            this.srcWell = SourceWell;
            int i , j;
            WellIcon icon, copyIcon, activeIcon;
            WellIconCode icode, newCode;
            this.IconSize = 1;
            if (srcWell != null)
            {
                if ((srcWell.icons != null) && (srcWell.icons.Count > 0))
                {
                    icons = new WellsIconList(srcWell.icons.Count);
                    for (i = 0; i < srcWell.icons.Count; i++)
                    {
                        icon = srcWell.icons[i];
                        copyIcon = new WellIcon(icon.X, icon.Y, icon.StratumCode);
                        for (j = 0; j < icon.Count; j++)
                        {
                            newCode.CharCode = icon[j].CharCode;
                            newCode.FontCode = icon[j].FontCode;
                            newCode.IconColor = icon[j].IconColor;
                            newCode.IconColorDefault = icon[j].IconColor;
                            newCode.Size = 10;
                            copyIcon.AddIconCode(newCode);
                        }
                        icons.AddIcon(copyIcon);
                    }
                    activeIcon = srcWell.icons.GetActiveIcon();
                    if(activeIcon != null) icons.SetActiveIcon(activeIcon.StratumCode);
                    this.SetBrushFromIcon();
                }

                this.X = SourceWell.X;
                this.Y = SourceWell.Y;
            }
            this.Selected = false;
            this.Visible = true;
        }

        public void SetBrushFromIcon()
        {
            if (icons.Count > 0)
            {
                int clr = (icons.IsDefaultIconsColor) ? icons.DefaultIconsColor : icons.IconsColor;
                _brush = new SolidBrush(Color.FromArgb(clr));

                if ((clr == -2894893) || (clr == -16777216))
                    _brush_name = new SolidBrush(Color.FromArgb(clr));
                else
                    _brush_name = Brushes.Black;
            }
            else
            {
                _brush = SmartPlusGraphics.Well.Brushes.LightBlack;
                _brush_name = Brushes.Black;
            }
        }
        public void SetIconsFonts(Font[] fonts, ExFontList exFontList)
        {
            if(icons != null) icons.SetFonts(fonts, exFontList);
        }

        public bool ReadFromStr(Project proj, string[] parseStr, Font[] BaseFonts, ExFontList exIconFontList)
        {
            bool retValue = false;
            bool find;
            WellIcon icon;
            WellIconCode iconCode;
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";
            int i, j, k, m, wIndex, col, wellIconsLen;
            int PlastId, iconLen;
            string skvName;
            OilField of;
            Well w;
            if (parseStr.Length > 0)
            {
                col = 0;
                srcOFCode = Convert.ToInt32(parseStr[col++]);
                wIndex = Convert.ToInt32(parseStr[col++]);
                skvName = parseStr[col++];
                find = false;
                if ((proj.OilFields.Count > 0) && (srcOFCode > 0))
                {
                    of = null;
                    for (i = 0; i < proj.OilFields.Count; i++)
                    {
                        if ((proj.OilFields[i]).OilFieldCode == srcOFCode)
                        {
                            of = proj.OilFields[i];
                        }
                    }
                    if ((of != null) && (of.Wells.Count > 0 || of.ProjWells.Count > 0))
                    {
                        if(of.Wells.Count > wIndex)
                        {
                            w = of.Wells[wIndex];
                            if ((w.UpperCaseName.Length == skvName.Length) && (w.UpperCaseName == skvName) && (w.CoordLoaded))
                            {
                                this.srcWell = w;
                                this.X = srcWell.X;
                                this.Y = srcWell.Y;
                                find = true;
                            }
                        }
                        if (this.srcWell == null && of.ProjWells.Count > wIndex)
                        {
                            w = of.ProjWells[wIndex];
                            if ((w.UpperCaseName.Length == skvName.Length) && (w.UpperCaseName == skvName) && (w.CoordLoaded))
                            {
                                this.srcWell = w;
                                this.X = srcWell.X;
                                this.Y = srcWell.Y;
                                find = true;
                            }
                        }
                    }
                }
                if (!find)
                {
                    return false;
                }

                wellIconsLen = Convert.ToInt32(parseStr[col++]);
                if (wellIconsLen > 0)
                {
                    this.icons = new WellsIconList(wellIconsLen);
                    iconCode.FontCode = 0;
                    iconCode.IconColor = Color.FromArgb(211, 211, 211).ToArgb();
                    iconCode.IconColorDefault = iconCode.IconColor;
                    iconCode.CharCode = 33;
                    iconCode.Size = 10;
                    icon = new WellIcon(this.X, this.Y, -1);
                    icon.AddIconCode(iconCode);
                    icon.SetIconFonts(BaseFonts, exIconFontList);
                    this.icons.AddIcon(icon);

                    for (j = 0; j < wellIconsLen; j++)
                    {
                        if (parseStr[col + 1].Length == 0 || parseStr[col + 2].Length == 0)
                        {
                            this.icons.AddEmptyIcon(this.X, this.Y);
                            col += 2 * wellIconsLen;
                            break;
                        }
                        PlastId = Convert.ToInt32(parseStr[col++]);
                        iconLen = Convert.ToInt32(parseStr[col++]);

                        k = -1;
                        for (m = 0; m < srcWell.coord.Count; m++)
                        {
                            if (srcWell.coord.Items[m].PlastId == PlastId)
                            {
                                k = m;
                                break;
                            }
                        }
                        k = -1;
                        if (k != -1)
                        {
                            icon = new WellIcon(srcWell.coord.Items[k].X, srcWell.coord.Items[k].Y, srcWell.coord.Items[k].PlastId);
                        }
                        else
                            icon = new WellIcon(this.X, this.Y, PlastId);
                        icon.SetIconFonts(BaseFonts, exIconFontList);
                        for (m = 0; m < iconLen; m++)
                        {
                            iconCode.FontCode = Convert.ToUInt16(parseStr[col++]);
                            iconCode.IconColor = Convert.ToInt32(parseStr[col++]);
                            iconCode.IconColorDefault = iconCode.IconColor;
                            iconCode.CharCode = Convert.ToUInt16(parseStr[col++]);
                            iconCode.Size = Convert.ToUInt16(parseStr[col++]);
                            icon.AddIconCode(iconCode);
                        }
                        this.icons.AddIcon(icon);
                        if (j == 0) this.icons.SetActiveIcon(icon.StratumCode);
                    }
                    if (col < parseStr.Length)
                    {
                        string str = parseStr[col++].Trim();
                        if (str != "")
                        {
                            if (Caption == null) Caption = new PhantomWellCaption();
                            Caption.Text = str;
                        }
                        str = parseStr[col++].Trim();
                        if (str != "")
                        {
                            if (Caption == null) Caption = new PhantomWellCaption();
                            try
                            {
                                DateTime dt = Convert.ToDateTime(str);
                                Caption.Date = dt;
                            }
                            catch { }
                        }
                        str = parseStr[col++].Trim();
                        if (str != "")
                        {
                            if (Caption == null) Caption = new PhantomWellCaption();
                            try
                            {
                                double dbl = Convert.ToDouble(str);
                                Caption.Value = dbl;
                            }
                            catch { }
                        }
                    }
                    this.SetBrushFromIcon();
                }
                retValue = true;
            }
            return retValue;
        }
        public bool WriteToCache(StreamWriter file)
        {
            if (file != null)
            {
                bool firstIcon = true;
                bool writeEmptyIcon = false;
                int j, iLen, maxPlasts = 0;

                SkvCoordItem coordItem;
                WellIcon wIcon, emptyIcon;
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                file.Write(String.Format("{0};{1};{2};", srcOFCode, srcWell.Index, srcWell.UpperCaseName));
                if (icons != null)
                {
                    emptyIcon = icons.GetIconByCode(-1);
                    int iconsCount = icons.Count - ((emptyIcon == null) ? 0 : 1);
                    if ((emptyIcon != null) && (iconsCount == 0))
                    {
                        writeEmptyIcon = true;
                        iconsCount++;
                    }
                    file.Write(string.Format("{0};", iconsCount));
                    firstIcon = true;
                    for (j = 0; j < icons.Count; j++)
                    {
                        wIcon = icons[j];
                        if ((emptyIcon != null) && (writeEmptyIcon || (wIcon.StratumCode != -1)))
                        {
                            if (!firstIcon) file.Write(";");
                            file.Write(string.Format("{0};", wIcon.StratumCode));
                            wIcon.WriteToCache(file, false);
                            firstIcon = false;
                        }
                    }
                    if (Caption != null)
                    {
                        if (iconsCount > 0) file.Write(";");
                        file.Write(Caption.Text + ";");
                        if (Caption.Date != DateTime.MinValue) file.Write(Caption.Date.ToString("dd.MM.yyyy"));
                        file.Write(";");
                        if (Caption.Value != Constant.DOUBLE_NAN) file.Write(Caption.Value.ToString());
                        else file.Write(" ");
                    }
                    file.WriteLine("");
                }
                else file.WriteLine("-1;");

            }
            return false;
        }

        public override void DrawObject(Graphics grfx)
        {
            float screenX, screenY;
            WellIcon icon = null;
            if ((Visible) && (srcWell.CoordLoaded) &&
                (srcWell.X != Constant.DOUBLE_NAN) && (srcWell.Y != Constant.DOUBLE_NAN))
            {
                if (C2DObject.DrawAllObjects || st_RealScreenRect.Contains(srcWell.X, srcWell.Y))
                {
                    screenX = ScreenXfromRealX(srcWell.X);
                    screenY = ScreenYfromRealY(srcWell.Y);
                    float radX = (float)(50.31f * Math.Pow(st_XUnitsInOnePixel, -1.013));

                    if (Selected)
                        grfx.FillEllipse(Brushes.Orange, screenX - st_radiusX * IconSize, screenY - st_radiusX * IconSize, st_radiusX * IconSize * 2, st_radiusX * IconSize * 2);

                    SizeF sIcon = SizeF.Empty;
                    if (icons != null) icon = icons.GetActiveIcon();
                    if (icon != null) sIcon = icon.GetIconSize(grfx);

                    if ((st_VisbleLevel > 15) && ((icon == null) || (icon.GetMaxIconFontSize() < 11)))
                    {
                        if (_brush == null) _brush = Brushes.Gray;
                        grfx.FillRectangle(_brush, screenX - st_radiusX / 2, screenY - st_radiusX / 2, st_radiusX, st_radiusX);
                    }
                    else
                    {
                        if (icons != null) icon = icons.GetActiveIcon();
                        if (icon == null)
                            grfx.FillEllipse(Brushes.LightGray, screenX - (st_radiusX / 2), screenY - (st_radiusX / 2), st_radiusX, st_radiusX);
                        else
                            if (icon != null)
                            {
                                icon.X = srcWell.X;
                                icon.Y = srcWell.Y;
                                icon.DrawObject(grfx);
                            }
                    }
                    //if ((st_XUnitsInOnePixel < 15) && (_font != null))
                    //{
                    //    if (sIcon.Width == 0)
                    //        scrTextX = screenX + (radX / 2) + 2;
                    //    else
                    //        scrTextX = screenX + sIcon.Width / 4;

                    //    if ((st_XUnitsInOnePixel < 8) && (CaptionVisible) && (Caption != null) && ((Caption.Text != "") || (Caption.Date != DateTime.MinValue) || (Caption.Value != Constant.DOUBLE_NAN)))
                    //    {
                    //        SizeF nameSize = SizeF.Empty, textSize = SizeF.Empty;
                    //        nameSize = grfx.MeasureString(this.srcWell.UpperCaseName, _font);
                    //        grfx.DrawLine(Pens.Black, scrTextX, screenY + nameSize.Height, scrTextX + (radX / 2) + 2 + nameSize.Width, screenY + nameSize.Height);
                    //        if (Caption.Text != "")
                    //        {
                    //            textSize = grfx.MeasureString(Caption.Text, _font);
                    //            grfx.DrawString(Caption.Text, _font, Brushes.Black, scrTextX, screenY + nameSize.Height);
                    //        }
                    //        string str = "";
                    //        if (Caption.Date != DateTime.MinValue)
                    //        {
                    //            str += Caption.Date.ToString("dd.MM.yyyy");
                    //        }
                    //        if (Caption.Value != Constant.DOUBLE_NAN)
                    //        {
                    //            if (str != "") str += " - ";
                    //            str += Caption.Value.ToString("0.##");
                    //        }
                    //        if (str != "")
                    //        {
                    //            grfx.DrawString(str, _font, Brushes.Black, scrTextX, screenY + nameSize.Height + textSize.Height);
                    //        }
                    //    }
                    //    if (VisibleName)
                    //    {
                    //        grfx.DrawString(srcWell.UpperCaseName, _font, _brush_name, scrTextX, screenY);
                    //    }
                    //}
                }
            }
        }
        public void DrawCaption(Graphics grfx)
        {
            float screenX, screenY, scrTextX;
            WellIcon icon = null;
            if ((Visible) && (srcWell.CoordLoaded) && (srcWell.X != Constant.DOUBLE_NAN) && (srcWell.Y != Constant.DOUBLE_NAN) && (CaptionVisible))
            {

                if (C2DObject.DrawAllObjects || st_RealScreenRect.Contains(srcWell.X, srcWell.Y))
                {
                    screenX = ScreenXfromRealX(srcWell.X);
                    screenY = ScreenYfromRealY(srcWell.Y);
                    float radX = (float)(50.31f * Math.Pow(st_XUnitsInOnePixel, -1.013));

                    SizeF sIcon = SizeF.Empty;
                    if (icons != null) icon = icons.GetActiveIcon();
                    if (icon != null) sIcon = icon.GetIconSize(grfx);

                    if (srcWell.icons != null)
                    {
                        WellIcon icon2 = srcWell.icons.GetActiveIcon();
                        if (icon2 != null) sIcon = icon2.GetIconSize(grfx);
                    }

                    if ((st_VisbleLevel < 15) && (_font != null))
                    {
                        if (sIcon.Width == 0)
                            scrTextX = screenX + (radX / 2) + 2;
                        else
                            scrTextX = screenX + sIcon.Width / 4;

                        if ((st_VisbleLevel < 8) && (CaptionVisible) && (Caption != null) && ((Caption.Text != "") || (Caption.Date != DateTime.MinValue) || (Caption.Value != Constant.DOUBLE_NAN)))
                        {
                            SizeF nameSize = SizeF.Empty, textSize = SizeF.Empty;
                            nameSize = grfx.MeasureString(this.srcWell.UpperCaseName, _font);
                            grfx.DrawLine(Pens.Black, scrTextX, screenY + nameSize.Height, scrTextX + (radX / 2) + 2 + nameSize.Width, screenY + nameSize.Height);
                            if (Caption.Text != "")
                            {
                                textSize = grfx.MeasureString(Caption.Text, _font);
                                grfx.DrawString(Caption.Text, _font, Brushes.Black, scrTextX, screenY + nameSize.Height);
                            }
                            string str = "";
                            if (Caption.Date != DateTime.MinValue)
                            {
                                str += Caption.Date.ToString("dd.MM.yyyy");
                            }
                            if (Caption.Value != Constant.DOUBLE_NAN)
                            {
                                if (str != "") str += " - ";
                                str += Caption.Value.ToString("0.##");
                            }
                            if (str != "")
                            {
                                grfx.DrawString(str, _font, Brushes.Black, scrTextX, screenY + nameSize.Height + textSize.Height);
                            }
                        }
                        if (VisibleName)
                        {
                            grfx.DrawString(srcWell.UpperCaseName, _font, _brush_name, scrTextX, screenY);
                        }
                    }
                }
            }
        }
    }

    public sealed class Profile : C2DObject
    {
        ArrayList Items;
        Pen penFat;
        public PointF CorSchHotSpot;

        public Profile()
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.PROFILE;
            Items = new ArrayList();
            penFat = new Pen(Color.Black, 2);
            CorSchHotSpot = PointF.Empty;
        }

        public void AddWell(Well w)
        {
            Items.Add(w);
        }
        public void RemoveAt(int index)
        {
            if ((index > -1) && (index < Count))
            {
                Items.RemoveAt(index);
            }
        }
        public int Count { get { return Items.Count; } }
        public Well this[int index]
        {
            get 
            {
                if ((index > -1) && (index < Items.Count))
                {
                    return (Well)Items[index];
                }
                else return null;
            }
        }

        public int AddWellByXYByPerpendicular(Well w)
        {
            int res = 0;
            if (Count > 1)
            {
                PointD pt1, pt2, pt3, pt0;
                pt3.X = w.X;
                pt3.Y = w.Y;
                res = -1;
                double minD = double.MaxValue, D, D2;
                for (int i = Count - 1; i > 0; i--)
                {
                    pt1.X = this[i].X;
                    pt1.Y = this[i].Y;
                    pt2.X = this[i - 1].X;
                    pt2.Y = this[i - 1].Y;
                    pt0 = Geometry.GetPointInLineByPoint(pt1, pt2, pt3);
                    if (Geometry.Contains_Point(pt1, pt2, pt0))
                    {
                        D = Math.Sqrt(Math.Pow(pt0.X - pt3.X, 2) + Math.Pow(pt0.Y - pt3.Y, 2));
                        if (minD > D)
                        {
                            res = i;
                            minD = D;
                        }
                    }
                }
                //if ((Count > 1) && (res == -1))
                //{
                //    pt1._X = this[0].X;
                //    pt1._Y = this[0].Y;
                //    pt2._X = this[Count - 1].X;
                //    pt2._Y = this[Count - 1].Y;
                //    D = Math.Sqrt(Math.Pow(pt1._X - pt3._X, 2) + Math.Pow(pt1._Y - pt3._Y, 2));
                //    D2 = Math.Sqrt(Math.Pow(pt2._X - pt3._X, 2) + Math.Pow(pt2._Y - pt3._Y, 2));
                //    if (D < D2) res = 0;
                //}
            }
            return res;
        }
        public int AddWellByXY(Well w)
        {
            int res = 0;
            if (Count > 0)
            {
                int index = AddWellByXYByPerpendicular(w);
                if (index == -1)
                {
                    PointD pt1, pt2, pt3, pt0;
                    pt3.X = w.X;
                    pt3.Y = w.Y;
                    res = -1;
                    double minD = double.MaxValue, D, D2, D3;
                    for (int i = Count - 1; i >= 0; i--)
                    {
                        pt0.X = this[i].X;
                        pt0.Y = this[i].Y;
                        D = Math.Sqrt(Math.Pow(pt0.X - pt3.X, 2) + Math.Pow(pt0.Y - pt3.Y, 2));
                        if (minD > D)
                        {
                            res = i;
                            minD = D;
                        }
                    }

                    if (((Count > 2) && (res == -1)) || (res == 0) || (res == Count -1))
                    {
                        pt1.X = this[0].X;
                        pt1.Y = this[0].Y;
                        pt2.X = this[Count - 1].X;
                        pt2.Y = this[Count - 1].Y;
                        D = Math.Sqrt(Math.Pow(pt1.X - pt3.X, 2) + Math.Pow(pt1.Y - pt3.Y, 2));
                        D2 = Math.Sqrt(Math.Pow(pt2.X - pt3.X, 2) + Math.Pow(pt2.Y - pt3.Y, 2));
                        if (D < D2) res = 0;
                    }

                    else if ((Count > 2) && (res != -1))
                    {
                        pt1.X = this[res - 1].X;
                        pt1.Y = this[res - 1].Y;
                        pt2.X = this[res + 1].X;
                        pt2.Y = this[res + 1].Y;
                        D = Math.Sqrt(Math.Pow(pt1.X - pt3.X, 2) + Math.Pow(pt1.Y - pt3.Y, 2));
                        D2 = Math.Sqrt(Math.Pow(pt2.X - pt3.X, 2) + Math.Pow(pt2.Y - pt3.Y, 2));
                        if (D > D2) res++;
                    }

                    if ((res != -1) && (res < Count - 1))
                    {
                        Items.Insert(res, w);
                    }
                    else
                    {
                        Items.Add(w);
                    }
                }
                else
                {
                    Items.Insert(index, w);
                }
            }
            else
            {
                Items.Add(w);
            }
            return res;
        }
        public int GetWellIndex(Well w)
        {
            for (int i = 0; i < Count; i++)
            {
                if ((this[i].OilFieldIndex == w.OilFieldIndex) &&
                    (this[i].OilFieldCode == w.OilFieldCode) &&
                    (this[i].Index == w.Index) &&
                    (this[i].UpperCaseName == w.UpperCaseName))
                {
                    return i;
                }
            }
            return -1;
        }

        // CACHE
        public void LoadFromCache(Project proj, BinaryReader br)
        {
            int count = br.ReadInt32();
            OilField of;
            Well w; 
            int i, j, ofIndex, ofCode, wIndex;
            string wellName;
            for (i = 0; i < count; i++)
            {
                ofIndex = br.ReadInt32();
                ofCode = br.ReadInt32();
                wIndex = br.ReadInt32();
                wellName = br.ReadString();
                if (ofIndex < proj.OilFields.Count)
                {
                    of = proj.OilFields[ofIndex];
                    if (of.OilFieldCode != ofCode)
                    {
                        for (j = 1; j < proj.OilFields.Count; j++)
                        {
                            if ((proj.OilFields[j]).OilFieldCode == ofCode)
                            {
                                ofIndex = j;
                                of = proj.OilFields[ofIndex];
                                break;
                            }
                        }
                    }
                    if (wIndex < of.Wells.Count)
                    {
                        w = of.Wells[wIndex];
                        if (w.UpperCaseName != wellName)
                        {
                            for (j = 0; j < of.Wells.Count; j++)
                            {
                                if ((of.Wells[j]).UpperCaseName == wellName)
                                {
                                    w = of.Wells[j];
                                    wIndex = j;
                                    break;
                                }
                            }
                        }
                        if ((w.X != Constant.DOUBLE_NAN) && (w.Y != Constant.DOUBLE_NAN))
                        {
                            AddWell(w);
                        }
                    }
                }
            }

        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(Items.Count);
            for (int i = 0; i < Count; i++)
            {
                bw.Write(this[i].OilFieldIndex);
                bw.Write(this[i].OilFieldCode);
                bw.Write(this[i].Index);
                bw.Write(this[i].UpperCaseName);
            }
        }

        //DRAWWING
        public override void DrawObject(Graphics grfx)
        {
            if ((Items.Count > 0) && (st_VisbleLevel <= st_OilfieldXUnits))
            {
                Well w = this[0];
                double PredRealX = w.X, PredRealY = w.Y;
                float PredScrX = -100, PredScrY = -100, scrX, scrY;
                
                float r = st_radiusX, r1 = st_radiusX;
                Pen pen = Pens.Black;

                if (this.Selected)
                {
                    pen = penFat;
                    if (st_XUnitsInOnePixel > 18)
                    {
                        r1 = r1 * 4;
                    }
                }
                for (int i = 1; i < Items.Count; i++)
                {
                    w = this[i];
                    if (C2DObject.DrawAllObjects || st_RealScreenRect.Contains(PredRealX, PredRealY) || st_RealScreenRect.Contains(w.X, w.Y))
                    {
                        scrX = C2DObject.ScreenXfromRealX(w.X);
                        scrY = C2DObject.ScreenYfromRealY(w.Y);
                        if (PredScrX == -100) PredScrX = C2DObject.ScreenXfromRealX(PredRealX);
                        if (PredScrY == -100) PredScrY = C2DObject.ScreenYfromRealY(PredRealY);
                        grfx.DrawLine(pen, PredScrX, PredScrY, scrX, scrY);
                        PredScrX = scrX; PredScrY = scrY;
                    }
                    else
                    {
                        PredScrX = -100;
                        PredScrY = -100;
                    }
                    PredRealX = w.X; PredRealY = w.Y;
                }

                PredScrX = 0; PredScrY = 0;
                for (int i = 0; i < Items.Count; i++)
                {
                    w = this[i];

                    if (C2DObject.DrawAllObjects || st_RealScreenRect.Contains(w.X, w.Y))
                    {
                        scrX = C2DObject.ScreenXfromRealX(w.X);
                        scrY = C2DObject.ScreenYfromRealY(w.Y);

                        if (this.Selected)
                            grfx.FillEllipse(Brushes.Yellow, scrX - r1 / 4, scrY - r1 / 4, r1 / 2, r1 / 2);
                        else
                            grfx.FillEllipse(Brushes.LightGray, scrX - r / 4, scrY - r / 4, r / 2, r / 2);

                        if (!w.SelectedByProfile)
                        {
                            grfx.DrawEllipse(pen, scrX - r, scrY - r, r * 2, r * 2);
                        }
                        else
                        {
                            grfx.DrawEllipse(penFat, scrX - r, scrY - r, r * 2, r * 2);
                        }
                    }
                }
            }
        }
    }

    public sealed class Marker : C2DObject
    {
        internal class MarkerItem
        {
            public Well w;
            public float depthMD;
            public PointD point;
        }
        Pen pen;
        SolidBrush brush;
        public Palette palette;
        public Pen PenBound
        {
            get { return pen; }
            set
            {
                pen = value;
                if (value != null)
                {
                    brush = new SolidBrush(value.Color);
                }
            }
        }
        public bool UsePalette;
        public float MinDepth, MaxDepth;
        ArrayList Items;
        public int Count { get { return Items.Count; } }
        MarkerItem this[int index]
        {
            get
            {
                if ((index > -1) && (index < Count))
                {
                    return (MarkerItem)Items[index];
                }
                return null;
            }
        }

        public Marker()
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.MARKER;
            Items = new ArrayList();
            PenBound = null;
            UsePalette = false;
            MinDepth = float.MaxValue;
            MaxDepth = float.MinValue;
        }

        public void DefaultPalette()
        {
            palette = new Palette();
            ReCalcMinMaxDepth();
            if (MinDepth < MaxDepth)
            {
                palette.Add(MinDepth, Color.Blue);
                palette.Add(MinDepth + (MaxDepth - MinDepth) / 4, Color.FromArgb(0, 255, 255));
                palette.Add(MinDepth + (MaxDepth - MinDepth) / 2, Color.FromArgb(0, 255, 0));
                palette.Add(MinDepth + 3 * (MaxDepth - MinDepth) / 4, Color.FromArgb(255, 255, 0));
                palette.Add(MaxDepth, Color.Red); 
            }
        }
        public void ReCalcMinMaxDepth()
        {
            MinDepth = float.MaxValue;
            MaxDepth = float.MinValue;
            MarkerItem item;
            for (int i = 0; i < Items.Count; i++)
            {
                item = (MarkerItem)Items[i];
                if (MinDepth > item.depthMD) MinDepth = item.depthMD;
                if (MaxDepth < item.depthMD) MaxDepth = item.depthMD;
            }
        }
        int GetIndex(Well w)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].w == w) return i;
            }
            return -1;
        }
        public Well GetWell(int index)
        {
            if (index > -1 && index < Count)
            {
                return this[index].w;
            }
            return null;
        }
        public Color GetNextColor(Color clr)
        {
            #region Create Palette
            Color[] clrPalette = new Color[7];
            clrPalette[0] = Color.FromArgb(255, 35, 35);
            clrPalette[1] = Color.SaddleBrown;
            clrPalette[2] = Color.Green;
            clrPalette[3] = Color.Purple;
            clrPalette[4] = Color.DarkGray;
            clrPalette[5] = Color.FromArgb(0, 82, 164);
            clrPalette[6] = Color.FromArgb(35, 35, 35);
            #endregion
            int index = -1;
            for (int i = 0; i < clrPalette.Length; i++)
            {
                if (clrPalette[i].ToArgb() == clr.ToArgb())
                {
                    index = i + 1;
                    break;
                }
            }
            if ((index >= clrPalette.Length) || (index == -1)) index = 0;
            return clrPalette[index];
        }
        public void Add(Well w, float DepthMD)
        {
            MarkerItem item = new MarkerItem();
            item.w = w;
            item.depthMD = DepthMD;
            item.point.X = w.X;
            item.point.Y = w.Y;
            Items.Add(item);
            w.AddMarker(this);
        }
        public void RemoveAt(Well w)
        {
            int ind = GetIndex(w);
            if (ind > -1) Items.RemoveAt(ind);
        }
        public PointD GetPoint(int index)
        {
            PointD xy;
            xy.X = Constant.DOUBLE_NAN;
            xy.Y = Constant.DOUBLE_NAN;
            if ((index > -1) && (index < Count)) 
            {
                xy = ((MarkerItem)Items[index]).point;
            }
            return xy;

        }
        public void SetDepthByWell(Well w, float depth)
        {
            MarkerItem item;
            for (int i = 0; i < Count; i++)
            {
                item = (MarkerItem)Items[i];
                if (item.w == w)
                {
                    item.depthMD = depth;
                }
            }
        }
        public float GetDepthByWell(Well w)
        {
            MarkerItem item;
            for (int i = 0; i < Count; i++)
            {
                item = (MarkerItem)Items[i];
                if (item.w == w)
                {
                    return item.depthMD;
                }
            }
            return -1;
        }
        public bool RemoveDepthByWell(Well w)
        {
            MarkerItem item;
            for (int i = 0; i < Count; i++)
            {
                item = (MarkerItem)Items[i];
                if (item.w == w)
                {
                    Items.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }
        public void ClearWellList()
        {
            for (int i = 0; i < Count; i++)
            {
                this[i].w.RemoveMarker(this);
            }
        }

        // CACHE
        public void LoadFromCache(Project proj, BinaryReader br)
        {
            int ver = br.ReadInt32();
            int count = 0;
            if (ver < 0)
            {
                ver = -ver;
            }
            else
            {
                count = ver;
                ver = 0;
            }
            if (ver > 0)
            {
                MinDepth = br.ReadSingle();
                MaxDepth = br.ReadSingle();
                UsePalette = br.ReadBoolean();
                if (UsePalette)
                {
                    palette = new Palette();
                    palette.ReadFromCache(br);
                }
                count = br.ReadInt32();
            }
            Color clr = Color.FromArgb(br.ReadInt32());
            PenBound = new Pen(clr, 7F);
            OilField of;
            Well w;
            int i, j, ofIndex, ofCode, wIndex;
            string wellName;
            float depth;
            for (i = 0; i < count; i++)
            {
                ofIndex = br.ReadInt32();
                ofCode = br.ReadInt32();
                wIndex = br.ReadInt32();
                wellName = br.ReadString();
                depth = br.ReadSingle();
                of = proj.OilFields[ofIndex];
                if (of.OilFieldCode != ofCode)
                {
                    for (j = 1; j < proj.OilFields.Count; j++)
                    {
                        if ((proj.OilFields[j]).OilFieldCode == ofCode)
                        {
                            ofIndex = j;
                            of = proj.OilFields[ofIndex];
                            break;
                        }
                    }
                }
                if (wIndex < of.Wells.Count)
                {
                    w = of.Wells[wIndex];
                    if (w.UpperCaseName != wellName)
                    {
                        for (j = 0; j < of.Wells.Count; j++)
                        {
                            if ((of.Wells[j]).UpperCaseName == wellName)
                            {
                                w = of.Wells[j];
                                wIndex = j;
                                break;
                            }
                        }
                    }
                    this.Add(w, depth);
                }
            }
            if (ver == 0)
            {
                ReCalcMinMaxDepth();
            }
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(-1); // version;
            if ((MinDepth == float.MaxValue) && (Count > 0))
            {
                ReCalcMinMaxDepth();
            }
            bw.Write(MinDepth);
            bw.Write(MaxDepth);
            bw.Write(UsePalette);
            if (UsePalette)
            {
                if (palette == null) DefaultPalette();
                palette.WriteToCache(bw);
            }
            bw.Write(Items.Count);
            bw.Write(PenBound.Color.ToArgb());
            for (int i = 0; i < Count; i++)
            {
                bw.Write(this[i].w.OilFieldIndex);
                bw.Write(this[i].w.OilFieldCode);
                bw.Write(this[i].w.Index);
                bw.Write(this[i].w.UpperCaseName);
                bw.Write(this[i].depthMD);
            }
        }

        // DRAWING
        public override void DrawObject(Graphics grfx)
        {
            if (st_VisbleLevel <= st_OilfieldXUnits)
            {
                float scrX, scrY;
                for (int i = 0; i < Count; i++)
                {
                    if (C2DObject.DrawAllObjects || st_RealScreenRect.Contains(this[i].point.X, this[i].point.Y))
                    {
                        scrX = (float)ScreenXfromRealX(this[i].point.X);
                        scrY = (float)ScreenYfromRealY(this[i].point.Y);
                        if (!UsePalette)
                        {
                            if (st_XUnitsInOnePixel > 50)
                            {
                                grfx.FillRectangle(brush, scrX - st_radiusX * 2, scrY - st_radiusX * 2, st_radiusX * 4, st_radiusX * 4);
                            }
                            else if (st_XUnitsInOnePixel <= 10)
                            {
                                grfx.DrawEllipse(PenBound, scrX - st_radiusX * 2, scrY - st_radiusX * 2, st_radiusX * 4, st_radiusX * 4);
                            }
                            else
                            {
                                grfx.FillEllipse(brush, scrX - st_radiusX * 2, scrY - st_radiusX * 2, st_radiusX * 4, st_radiusX * 4);
                            }
                        }
                        else if (palette != null)
                        {
                            Color clr = palette.GetColorByX(this[i].depthMD);
                            if (st_XUnitsInOnePixel > 50)
                            {
                                using (SolidBrush br = new SolidBrush(clr))
                                {
                                    grfx.FillRectangle(br, scrX - st_radiusX * 2, scrY - st_radiusX * 2, st_radiusX * 4, st_radiusX * 4);
                                }
                            }
                            else if (st_XUnitsInOnePixel <= 10)
                            {
                                using (Pen pen = new Pen(clr, 7F))
                                {
                                    grfx.DrawEllipse(pen, scrX - st_radiusX * 2, scrY - st_radiusX * 2, st_radiusX * 4, st_radiusX * 4);
                                }
                            }
                            else
                            {
                                using (SolidBrush br = new SolidBrush(clr))
                                {
                                    grfx.FillEllipse(br, scrX - st_radiusX * 2, scrY - st_radiusX * 2, st_radiusX * 4, st_radiusX * 4);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    #region Well Icon Classes
    public struct WellIconCode
    {
        public ushort FontCode;
        public int IconColorDefault;
        public int IconColor;
        public ushort CharCode;
        public ushort Size;
    }
    public sealed class ExFontItem
    {
        public Font font;
        public ushort FontCode;
        public ushort Size;
        public ExFontItem()
        {
            font = null;
            FontCode = 0;
            Size = 0;
        }
    }
    public sealed class ExFontList
    {
        int count;
        Font[] BaseFonts;
        ExFontItem[] Items;

        public int Count { get { return count; } }
        public ExFontList(Font[] BaseFontList)
        {
            count = 0;
            Items = null;
            BaseFonts = BaseFontList;
        }
        public ExFontItem this[int index]
        {
            get
            {
                if((Items != null) && (index < Items.Length))
                {
                    return Items[index];
                }
                return null;
            }
        }
        public int GetExFont(ushort FontCode, ushort Size) 
        {
            int i, len;
            if (Items != null)
            {
                len = Items.Length;
                for (i = 0; i < len; i++)
                {
                    if ((Items[i].Size == Size) && (Items[i].FontCode == FontCode))
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        public int Add(ushort FontCode, ushort newSize)
        {
            int i, len, ind;

            ind = GetExFont(FontCode, newSize);
            if(ind > -1) return ind;
            len = 0;
            if (Items != null) len = Items.Length;
            ExFontItem newItem = new ExFontItem();
            ExFontItem[] newItems = new ExFontItem[len + 1];
            for (i = 0; i < len; i++) newItems[i] = Items[i];
            newItem.FontCode = FontCode;
            newItem.Size = newSize;
            double XUnits = C2DObject.st_XUnitsInOnePixel;
            if(XUnits > 91) XUnits = 91;
            float size = (float)Math.Round(15.031 * newItem.Size * Math.Pow(XUnits, -1.013));
            if (size > 0) newItem.font = new Font(BaseFonts[FontCode].FontFamily, size);
            else newItem.font = new Font(BaseFonts[FontCode].FontFamily, 1);
            newItems[len] = newItem;
            Items = newItems;
            count = len +1;
            return len;
        }
        public void ClearExFonts()
        {
            Items = null;
            count = 0;
        }
    }

    public sealed class WellIcon : C2DObject
    {
        ArrayList iconCodeList;
        public Font[] iconFonts;
        public ExFontList ExIconFonts;

        public bool IsDefaultColor;
        public int StratumCode { get; set; }
        public int Count
        {
            get { return iconCodeList.Count; }
        }
        public int IconColor
        {
            get 
            {
                if (iconCodeList.Count > 0)
                {
                    return ((WellIconCode)iconCodeList[0]).IconColor;
                }
                return 0;
            }
        }
        public int DefaultIconColor
        {
            get
            {
                if (iconCodeList.Count > 0)
                {
                    return ((WellIconCode)iconCodeList[0]).IconColorDefault;
                }
                return 0;
            }
        }
        public WellIcon(double X, double Y, int StratumCode)
        {
            this.X = X;
            this.Y = Y;
            this.StratumCode = StratumCode;
            this.IsDefaultColor = false;
            iconFonts = null;
            ExIconFonts = null;
            iconCodeList = new ArrayList(5);
        }

        public WellIconCode this[int index]
        {
            get
            {
                if (index < this.Count)
                {
                    return (WellIconCode)iconCodeList[index];
                }
                else return default(WellIconCode);
            }
        }
        public bool EqualsIcon(WellIcon wIcon)
        {
            bool res = true;
            if (this.Count == wIcon.Count)
            {
                WellIconCode code;
                for (int i = 0; i < this.Count; i++)
                {
                    if ((this[i].CharCode != wIcon[i].CharCode) ||
                       (this[i].FontCode != wIcon[i].FontCode) ||
                       (this[i].IconColor != wIcon[i].IconColor) ||
                       (this[i].Size != wIcon[i].Size))
                    {
                        res = false;
                        break;
                    }
                }
            }
            else
                res = false;
            return res;
        }
        public void SetIconFonts(Font[] fonts, ExFontList exFontList)
        {
            iconFonts = fonts;
            ExIconFonts = exFontList;
        }
        public void SetIconCodes(WellIconCode[] codes)
        {
            int len = codes.Length;
            iconCodeList.Clear();
            for (int i = 0; i < len; i++) AddIconCode((WellIconCode)codes[i]);
        }

        public void InsertIconCode(int index, WellIconCode code)
        {
            WellIconCode wCode;
            wCode.FontCode = code.FontCode;
            wCode.IconColor = code.IconColor;
            wCode.IconColorDefault = code.IconColorDefault;
            wCode.CharCode = code.CharCode;
            wCode.Size = code.Size;
            if ((code.Size != 10) && (ExIconFonts != null))
            {
                wCode.FontCode = 7;
                wCode.FontCode += (ushort)ExIconFonts.Add(code.FontCode, code.Size);
            }
            if(index < iconCodeList.Count)
                iconCodeList.Insert(index, (WellIconCode)wCode);
            else
                iconCodeList.Add((WellIconCode)wCode);
        }
        public void AddIconCode(WellIconCode code)
        {
            WellIconCode wCode;
            wCode.FontCode = code.FontCode;
            wCode.IconColor = code.IconColor;
            wCode.IconColorDefault = code.IconColorDefault;
            wCode.CharCode = code.CharCode;
            wCode.Size = code.Size;
            if ((code.Size != 10) && (ExIconFonts != null))
            {
                wCode.FontCode = 7;
                wCode.FontCode += (ushort)ExIconFonts.Add(code.FontCode, code.Size);
            }
            iconCodeList.Add((WellIconCode)wCode);
        }
        public void ClearIconList()
        {
            iconCodeList.Clear();
        }

        public bool CheckIconCode(ushort[] CharCodes)
        {
            for (int i = 0; i < iconCodeList.Count; i++)
            {
                for (int j = 0; j < CharCodes.Length; j++)
                {
                    if (((WellIconCode)iconCodeList[i]).CharCode == CharCodes[j])
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        Font GetIconFont(ushort FontCode)
        {
            if (FontCode < 7)
                return iconFonts[FontCode];
            else if (ExIconFonts != null)
                return ExIconFonts[FontCode - 7].font;
            return null;
        }
        public ushort GetIconFontCode(ushort FontCode)
        {
            if (FontCode < 7) return FontCode;
            else if (ExIconFonts != null) return ExIconFonts[FontCode - 7].FontCode;
            return 0;
        }
        public ushort GetMaxIconFontSize()
        {
            if ((ExIconFonts == null) || (ExIconFonts.Count == 0))
                return 10;
            else
            {
                ushort max = 10;
                WellIconCode wcode;
                for (int i = 0; i < iconCodeList.Count; i++)
                {
                    wcode = (WellIconCode)iconCodeList[i];
                    if ((wcode.FontCode > 6) && (max < ExIconFonts[wcode.FontCode - 7].Size))
                        max = ExIconFonts[wcode.FontCode - 7].Size;
                }
                return max;
            }
        }

        public SizeF GetIconSize(Graphics grfx)
        {
            SizeF maxSize = new SizeF(), size;
            if ((iconFonts != null) && (iconFonts.Length > 0))
            {
                WellIconCode wCode;
                Font f;
                string s;
                for (int i = 0; i < iconCodeList.Count; i++)
                {
                    wCode = (WellIconCode)iconCodeList[i];
                    
                    f = GetIconFont(wCode.FontCode);
                    if (f != null)
                    {
                        s = Convert.ToChar(wCode.CharCode).ToString();
                        size = grfx.MeasureString(s, f, PointF.Empty, StringFormat.GenericTypographic);
                        if (maxSize.Width < size.Width) maxSize.Width = size.Width;
                        if (maxSize.Height < size.Height) maxSize.Height = size.Height;
                    }
                }
            }
            return maxSize;
        }
        public bool WriteToCache(StreamWriter file, bool WithDefault)
        {
            if (file != null)
            {
                WellIconCode wi�; 
                file.Write(String.Format("{0};", Count));
                ushort fCode;
                for (int i = 0; i < Count; i++)
                {
                    wi� = (WellIconCode)iconCodeList[i];
                    fCode = wi�.FontCode;
                    if (fCode > 6) fCode = ExIconFonts[fCode - 7].FontCode;
                    if(WithDefault)
                        file.Write(String.Format("{0};{1};{2};{3};{4}", fCode, wi�.IconColor, wi�.IconColorDefault, wi�.CharCode, wi�.Size));
                    else
                        file.Write(String.Format("{0};{1};{2};{3}", fCode, wi�.IconColor, wi�.CharCode, wi�.Size));
                    if (i < Count - 1) file.Write(";");
                }
                return true;
            }
            return false;

        }
        public bool Read(BinaryReader br)
        {
            if (br == null) return false;
            int count = br.ReadInt32();
            iconCodeList.Clear();
            for (int i = 0; i < count; i++)
            {
                WellIconCode wic = new WellIconCode();
                wic.FontCode = br.ReadUInt16();
                wic.IconColor = br.ReadInt32();
                wic.IconColorDefault = br.ReadInt32();
                wic.CharCode = br.ReadUInt16();
                wic.Size = br.ReadUInt16();
                iconCodeList.Add(wic);
            }
            return true;
        }
        public bool Write(BinaryWriter bw)
        {
            if (bw != null)
            {
                WellIconCode wi�;
                bw.Write(Count);
                ushort fCode;
                for (int i = 0; i < Count; i++)
                {
                    wi� = (WellIconCode)iconCodeList[i];
                    fCode = wi�.FontCode;
                    if (fCode > 6) fCode = ExIconFonts[fCode - 7].FontCode;

                    bw.Write(fCode);
                    bw.Write(wi�.IconColor);
                    bw.Write(wi�.IconColorDefault);
                    bw.Write(wi�.CharCode);
                    bw.Write(wi�.Size);
                }
                return true;
            }
            return false;
        }
        public override void DrawObject(Graphics grfx)
        {
            if ((iconFonts != null) && (iconFonts.Length > 0))
            {
                int len = iconCodeList.Count;
                int argb = Color.Transparent.ToArgb(), currArgb;
                WellIconCode wCode;
                SolidBrush brush = null;
                float scrX, scrY;
                Font f;
                string s;
                for (int i = 0; i < iconCodeList.Count; i++)
                {
                    wCode = (WellIconCode)iconCodeList[i];
                    currArgb = (!IsDefaultColor) ? wCode.IconColor : wCode.IconColorDefault;
                    if (argb != currArgb)
                    {
                        brush = new SolidBrush(Color.FromArgb(currArgb));
                        argb = currArgb;
                    }
                    scrX = ScreenXfromRealX(this.X);
                    scrY = ScreenYfromRealY(this.Y);
                    PointF ptf = new PointF(scrX, scrY);

                    f = GetIconFont(wCode.FontCode);
                    if (f != null && brush != null)
                    {
                        s = Convert.ToChar(wCode.CharCode).ToString();
                        SizeF size = grfx.MeasureString(s, f, ptf, StringFormat.GenericTypographic);

                        grfx.DrawString(s, f, brush, scrX - size.Width / 2, scrY - size.Height / 2, StringFormat.GenericTypographic);
                    }
                }
                if (brush != null) brush.Dispose();
            }
        }
    }

    public sealed class WellsIconList 
    {
        int ActiveIcon;
        List<WellIcon> iconList;
        public int IconsColor;
        public int DefaultIconsColor;
        public WellsIconList()
        {
            iconList = new List<WellIcon>();
            ActiveIcon = -1;
        }
        public WellsIconList(int Size)
        {
            iconList = new List<WellIcon>(Size);
            ActiveIcon = -1;
        }
        public int Count 
        {
            get { return iconList.Count; }
        }
        public bool IsDefaultIconsColor
        {
            set
            {
                for (int i = 0; i < Count; i++) iconList[i].IsDefaultColor = value;
            }
            get
            {
                if (Count > 0) return iconList[0].IsDefaultColor;
                return false;
            }
        }

        public int AddIcon(double X, double Y, int OilObjCode, WellIconCode[] codes)
        {
            if ((X != Constant.DOUBLE_NAN) && (Y != Constant.DOUBLE_NAN))
            {
                WellIcon wIcon = new WellIcon(X, Y, OilObjCode);
                wIcon.SetIconCodes(codes);
                IconsColor = wIcon.IconColor;
                DefaultIconsColor = wIcon.DefaultIconColor;
                this.iconList.Add(wIcon);
                return this.iconList.Count - 1;
            }
            return -1;
        }
        public void AddEmptyIcon(double WellX, double WellY)
        {
            WellIconCode iconCode = new WellIconCode();
            iconCode.FontCode = 0;
            iconCode.IconColor = Color.FromArgb(211, 211, 211).ToArgb();
            iconCode.IconColorDefault = iconCode.IconColor;
            iconCode.CharCode = 33;
            iconCode.Size = 10;
            WellIcon activeIcon = GetActiveIcon();
            WellIcon icon;
            if (activeIcon != null)
            {
                icon = new WellIcon(activeIcon.X, activeIcon.Y, -1);
            }
            else
            {
                icon = new WellIcon(WellX, WellY, -1);
            }
            icon.InsertIconCode(0, iconCode);
            AddIcon(icon);
            if (activeIcon != null) SetActiveIcon(activeIcon.StratumCode);
        }
        public int AddIcon(WellIcon icon)
        {
            if ((icon.X != Constant.DOUBLE_NAN) && (icon.Y != Constant.DOUBLE_NAN))
            {
                IconsColor = icon.IconColor;
                DefaultIconsColor = icon.DefaultIconColor;
                this.iconList.Add(icon);
                return this.iconList.Count - 1;
            }
            return -1;
        }
        public void SetFonts(Font[] fonts, ExFontList exFontList)
        {
            for (int i = 0; i < Count; i++) iconList[i].SetIconFonts(fonts, exFontList);
        }
        public void SetActiveIcon(int OilObjCode)
        {
            WellIcon icon;
            for (int i = 0; i < Count; i++)
            {
                icon = iconList[i];
                if (OilObjCode == icon.StratumCode)
                {
                    ActiveIcon = i;
                    this.IconsColor = icon.IconColor;
                    this.DefaultIconsColor = icon.DefaultIconColor;
                    break;
                }
            }
        }
        public void SetActiveIcon(int OilObjCode, double X, double Y)
        {
            WellIcon icon;
            for (int i = 0; i < Count; i++)
            {
                icon = iconList[i];
                if (OilObjCode == icon.StratumCode)
                {
                    ActiveIcon = i;
                    icon.X = X;
                    icon.Y = Y;
                    this.IconsColor = icon.IconColor;
                    this.DefaultIconsColor = icon.DefaultIconColor;
                    break;
                }
            }
        }
        public int[] GetOilObjList()
        {
            int len = iconList.Count;
            if (len == 0) return null;
            int[] retValue = new int[len - 1];
            WellIcon icon;
            for (int i = 1; i < Count; i++)
            {
                icon = iconList[i];
                retValue[i - 1] = icon.StratumCode;
            }
            return retValue;
        }
        public void SetEmptyIcon(double X, double Y)
        {
            SetActiveIcon(-1, X, Y);
        }
        public void SetActiveAllIcon()
        {
            WellIcon icon;
            for (int i = 0; i < Count; i++)
            {
                icon = iconList[i];
                if (icon.StratumCode != -1)
                {
                    ActiveIcon = i;
                    this.IconsColor = icon.IconColor;
                    this.DefaultIconsColor = icon.DefaultIconColor;
                    break;
                }
            }
        }
        public WellIcon GetActiveIcon()
        {
            if (ActiveIcon != -1) return iconList[ActiveIcon];
            else return null;
        }
        public WellIcon GetAllIcon()
        {
            for (int i = 0; i < Count; i++)
            {
                if (iconList[i].StratumCode != -1)
                {
                    return iconList[i];
                }
            }
            return null;
        }
        public WellIcon GetIconByCode(int StratumCode)
        {
            int len = iconList.Count;
            WellIcon wIcon; 
            for(int i = 0; i < len ; i++)
            {
                wIcon = iconList[i];
                if (wIcon.StratumCode == StratumCode) return wIcon;
            }
            return null;
        }
        public WellIcon this[int index]
        {
            get
            {
                if (index < this.Count)
                {
                    return iconList[index];
                }
                else return null;
            }
        }

        public bool CheckIconCode(ushort[] CharCodes)
        {
            for (int i = 0; i < iconList.Count; i++)
            {
                if (iconList[i].CheckIconCode(CharCodes)) return true;
            }
            return false;
        }
        public void Read(BinaryReader br, double WellX, double WellY)
        {
            int count = br.ReadInt32();
            double X, Y;
            int PlastCode;
            WellIcon icon;
            for (int i = 0; i < count; i++)
            {
                X = br.ReadDouble();
                Y = br.ReadDouble();
                PlastCode = br.ReadInt32();
                icon = new WellIcon(X, Y, PlastCode);
                icon.Read(br);
                AddIcon(icon);
            }
            // Insert Empty Icon
            WellIcon activeIcon = null;
            WellIconCode iconCode;
            iconCode.FontCode = 0;
            iconCode.IconColor = Color.FromArgb(211, 211, 211).ToArgb();
            iconCode.IconColorDefault = iconCode.IconColor;
            iconCode.CharCode = 33;
            iconCode.Size = 10;
            activeIcon = GetActiveIcon();
            if (activeIcon != null)
            {
                icon = new WellIcon(activeIcon.X, activeIcon.Y, -1);
            }
            else
            {
                icon = new WellIcon(WellX, WellY, -1);
            }
            icon.InsertIconCode(0, iconCode);
            AddIcon(icon);
            if (activeIcon != null) SetActiveIcon(activeIcon.StratumCode);
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(Count - 1);
            for (int i = 1; i < iconList.Count; i++)
            {
                bw.Write(iconList[i].X);
                bw.Write(iconList[i].Y);
                bw.Write(iconList[i].StratumCode);
                iconList[i].Write(bw);
            }
        }

        public void Clear()
        {
            iconList.Clear();
            ActiveIcon = -1;
        }
     }

    #endregion

    public sealed class Contour : C2DObject 
    {
        public static int Version = 0;
        public int StratumCode;
        public int _OilFieldCode;
        public int _NGDUCode;
        public int _EnterpriseCode;
        public bool _Closed;
        public bool _CCW;
        public int _FillBrush;
        public int _ContourType;
        // ����������� ���� ��������
        // _ContourType = -1    - ������ �������
        // _ContourType = -2    - ������ ����
        // _ContourType = -3    - ������ �������������
        // _ContourType = -4    - ������ ������ ����������
        // _ContourType = -5    - ��������� ������ �������

        public double _Value;
        public PointsXY _points = null;
        public List<Contour> cntrsOut = null;
        PointD[] cilia = null;
        int ciliaLastPoint = 0;
        PointF[] scrPoint = null;
        System.Drawing.Drawing2D.GraphicsPath OutPath = null;
        public bool ColorMode;
        public bool VisibleName;
        public bool ShowBizPlanParams;
        public bool ShowLetterBox;

        public string Text;
        public string Text2;
        StratumDictionary dict;
        new public bool Selected
        {
            get { return base.Selected; }
            set
            {
                if ((this._ContourType > 0) && (base.Selected != value))
                {
                    if(penFat != null)
                    {
                        int width = value ? 4 : 2;
                        penFat = new Pen(penFat.Brush, width);
                    }
                }
                base.Selected = value;
            }
        }
        Color lineClr = Color.Empty;
        public int lineWidth;
        public bool Enable;

        public int LetterBoxLevel = -1;

        public double[] BizPlanParams = null;
        public List<PointF> _debugPoints;
        Brush brush;
        public Brush fillBrush;

        Pen pen, penThin, penFat;
        
        public Color LineColor
        {
            get { return lineClr; }
            set 
            {
                lineClr = value;
                brush = new SolidBrush(value);
                if (lineWidth == -1)
                {
                    penThin = new Pen(value, 1);
                    penFat = new Pen(value, 2);
                }
                else
                {
                    pen = new Pen(value, lineWidth);
                }
            }
        }
        public int LineWidth
        {
            get { return lineWidth; }
            set
            {
                lineWidth = value;
                if (lineWidth == -1)
                {
                    penThin = new Pen(lineClr, 1);
                    penFat = new Pen(lineClr, 2);
                }
                else
                {
                    pen = new Pen(lineClr, lineWidth);
                }
            }
        }

        public bool _Fantom;
        public bool _ShiftPoint;
        
        public double Xmin, Xmax, Ymin, Ymax;

        public Contour() 
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.CONTOUR;
            _Closed = false;
            this.Enable = true;
            _Value = 0;
            _ContourType = 0;
            _Fantom = false;
            _ShiftPoint = false;
            _FillBrush = -1;
            fillBrush = null;
            StratumCode = -1;
            _OilFieldCode = -1;
            _NGDUCode = -1;
            _EnterpriseCode = -1;
            scrPoint = null;
            brush = null;
            pen = null;
            Text = null;
            Text2 = null;
            ColorMode = true;
            ShowBizPlanParams = false;
            lineWidth = -1;
            Name = string.Empty;
            Xmin = Constant.DOUBLE_NAN;
            Xmax = Constant.DOUBLE_NAN;
            Ymin = Constant.DOUBLE_NAN;
            Ymax = Constant.DOUBLE_NAN;
        }
        public Contour(int numPoints) : this()
        {
            _points = new PointsXY(numPoints);
        }
        public Contour(Contour cntr)
        {
            TypeID = Constant.BASE_OBJ_TYPES_ID.CONTOUR;
            if(cntr.Name != null) Name = cntr.Name.Replace("\"", "");
            _Closed = cntr._Closed;
            Enable = cntr.Enable;
            _Value = cntr._Value;
            //_ContourType = cntr._ContourType;
            _FillBrush = cntr._FillBrush;
            fillBrush = cntr.fillBrush;
            StratumCode = cntr.StratumCode;
            _OilFieldCode = cntr._OilFieldCode;
            _NGDUCode = cntr._NGDUCode;
            _EnterpriseCode = cntr._EnterpriseCode;
            _CCW = cntr._CCW;
            lineWidth = cntr.lineWidth;
            _Fantom = false;
            _ShiftPoint = false;
            scrPoint = null;
            brush = null;
            fillBrush = null;
            pen = null;
            Text = null;
            Text2 = null;
            ColorMode = true;
            
            Xmin = cntr.Xmin;
            Xmax = cntr.Xmax;
            Ymin = cntr.Ymin;
            Ymax = cntr.Ymax;
            X = cntr.X;
            Y = cntr.Y;
            _points = new PointsXY(cntr._points);
        }
        public Contour(PointD[] points) : this()
        {
            _points = new PointsXY(points);
        }

        public void Add(double X, double Y)
        {
            _points.Add(X, Y);
        }
        public void Insert(int index, double X, double Y)
        {
            if ((index > -1) && (index < _points.Count))
            {
                _points.Insert(index, X, Y);
            }
            else if (index == _points.Count)
            {
                _points.Add(X, Y);
            }
            SetMinMax();
            GetMedian();
        }
        public void SetPointByIndex(int index, double X, double Y)
        {
            if((index > -1)&&(index < _points.Count))
            {
                PointD point;
                point.X = X;
                point.Y = Y;
                _points[index] = point;
            }
            SetMinMax();
            GetMedian();
        }
        public void DelPointByIndex(int index)
        {
            if ((index > -1) && (index < _points.Count))
            {
                _points.RemoveAt(index);
            }
            SetMinMax();
            GetMedian();
        }
        public void SetDictPlast(StratumDictionary plastDict)
        {
            this.dict = plastDict;
        }
        public bool Read(RdfConnector conn, string key, OilfieldCoefItem coef) 
        {
            int size, len;
            int nPacked;
            int nCount;
            int nClosed, nCCW;
            int x, y;
            PointD point;
            MemoryStream ms;

            // read info
            ms = conn.ReadBinary(key + "@info", false);
            if (ms == null) return false;
            if (ConvertEx.IsRdfDataSetPacked(ref ms)) ConvertEx.UnpackRDFDataSet(ref ms);

            ms.Seek(136, SeekOrigin.Begin);
            _ContourType = ConvertEx.GetInt32(ms);
            if ((_ContourType > 10000) || (_ContourType < 0)) return false;

            nCount = ConvertEx.GetInt32(ms);
            ms.Seek(4, SeekOrigin.Current);
            _Value = ConvertEx.GetSingle(ms);
            nClosed = ConvertEx.GetInt16(ms);
            nCCW = ConvertEx.GetInt16(ms);

            _Closed = (nClosed == 0) ? false : true;
            _CCW = (nCCW == 0) ? false : true;

            _points = new PointsXY(nCount);
            ms.Close();
            // ������ xy

            ms = conn.ReadBinary(key + "@xy", false);
            if (ms == null) return false;

            if (ConvertEx.IsRdfDataSetPacked(ref ms)) ConvertEx.UnpackRDFDataSet(ref ms);

            ms.Seek(48, SeekOrigin.Begin);
            for (int i = 0; i < nCount; i++) 
            {
                x = ConvertEx.GetInt32(ms);
                y = ConvertEx.GetInt32(ms);
                coef.GlobalCoordFromLocal((double)x, (double)y, out point.X, out point.Y);
                ms.Seek(2, SeekOrigin.Current);
                _points.Add(point.X, point.Y);
            }
            ms.Close();
            return true;
        }
        public void GetCenter() {
            
            double x1 = _points.GetMinX();
            double x2 = _points.GetMaxX();
            double y1 = _points.GetMinY();
            double y2 = _points.GetMaxY();
            X = (x1 + x2) / 2;
            Y = (y1 + y2) / 2;
             
           // GetMedian();
        }
        public void GetMedian()
        {
            PointD ave = _points.GetAvarage();
            X = ave.X;
            Y = ave.Y;
        }
        public void SetMinMax()
        {
            Xmin = _points.GetMinX();
            Xmax = _points.GetMaxX();
            Ymin = _points.GetMinY();
            Ymax = _points.GetMaxY();
        }
        public double GetPerimeter()
        {
            double p = 0;
            for (int i = 0; i < _points.Count - 1; i++)
            {
                p += Math.Sqrt(Math.Pow(_points[i].X - _points[i + 1].X, 2) + Math.Pow(_points[i].Y - _points[i + 1].Y, 2));
            }
            if ((this._Closed) && (_points.Count > 0))
            {
                p += Math.Sqrt(Math.Pow(_points[_points.Count - 1].X - _points[0].X, 2) + Math.Pow(_points[_points.Count - 1].Y - _points[0].Y, 2));
            }
            return p;
        }
        public bool PointBoundsEntry(double X, double Y)
        {
            if ((Xmin < X) && (X < Xmax) && (Ymin < Y) && (Y < Ymax))
            {
                return true;
            }
            return false;
        }
        public bool IntersectBounds(Contour cntr)
        {
            if (((cntr.Ymin < Ymin) && (cntr.Ymax < Ymin)) || ((Ymax < cntr.Ymin) && (Ymax < cntr.Ymax)))
            {
                return false;
            }
            if (((cntr.Xmin < Xmin) && (cntr.Xmax < Xmin)) || ((Xmax < cntr.Xmin) && (Xmax < cntr.Xmax)))
            {
                return false;
            }
            return true;
        }

        public bool LineIntersectBounds(float lineX1, float lineY1, float lineX2, float lineY2, RectangleF rect)
        {
            float f;
            float minX, maxX, minY, maxY;
            minX = lineX1;
            maxX = lineX2;
            if(lineX1 > lineX2)
            {
                minX = lineX2;
                maxX = lineX1;
            }
            minY = lineY1;
            maxY = lineY2;
            if(lineY1 > lineY2)
            {
                minY = lineY2;
                maxY = lineY1;
            }
            f = (rect.Y - lineY1) * (lineX2 - lineX1) / (lineY2 - lineY1) + lineX1;
            if ((f >= rect.X) && (f <= rect.Right) && (f > minY) && (f < maxY)) return true;

            f = (rect.Bottom - lineY1) * (lineX2 - lineX1) / (lineY2 - lineY1) + lineX1;
            if ((f >= rect.X) && (f <= rect.Right) && (f > minY) && (f < maxY)) return true;

            f = (rect.X - lineX1) * (lineY2 - lineY1) / (lineX2 - lineX1) + lineY1;
            if ((f >= rect.Y) && (f <= rect.Bottom) && (f > minX) && (f < maxX)) return true;

            f = (rect.Right - lineX1) * (lineY2 - lineY1) / (lineX2 - lineX1) + lineY1;
            if ((f >= rect.Y) && (f <= rect.Bottom) && (f > minX) && (f < maxX)) return true;
                
            return false;
        }
        public bool LineIntersectBounds(double lineX1, double lineY1, double lineX2, double lineY2, RectangleD rect)
        {
            double f;
            double minX, maxX, minY, maxY;
            minX = lineX1;
            maxX = lineX2;
            if (lineX1 > lineX2)
            {
                minX = lineX2;
                maxX = lineX1;
            }
            minY = lineY1;
            maxY = lineY2;
            if (lineY1 > lineY2)
            {
                minY = lineY2;
                maxY = lineY1;
            }
            f = (rect.Top - lineY1) * (lineX2 - lineX1) / (lineY2 - lineY1) + lineX1;
            if ((f >= rect.Left) && (f <= rect.Right) && (f > minY) && (f < maxY)) return true;

            f = (rect.Bottom - lineY1) * (lineX2 - lineX1) / (lineY2 - lineY1) + lineX1;
            if ((f >= rect.Left) && (f <= rect.Right) && (f > minY) && (f < maxY)) return true;

            f = (rect.Top - lineX1) * (lineY2 - lineY1) / (lineX2 - lineX1) + lineY1;
            if ((f >= rect.Top) && (f <= rect.Bottom) && (f > minX) && (f < maxX)) return true;

            f = (rect.Right - lineX1) * (lineY2 - lineY1) / (lineX2 - lineX1) + lineY1;
            if ((f >= rect.Top) && (f <= rect.Bottom) && (f > minX) && (f < maxX)) return true;

            return false;
        }
        public void ResetBrush()
        {
            brush = null;
        }
        public int GetIndexRibByXY(double scrX, double scrY)
        {
            if ((_points != null) && (_points.Count > 0))
            {
                double d, x1, y1, x2 = 0, y2 = 0, x3, y3, minX, maxX, minY, maxY;
                double scrX1, scrX2, scrY1, scrY2;
                double rX, rY, scrX3, scrY3, scrD;
                double k, b;
                double minD;
                bool res, bVert, bHor;
                int selRib = -1;
                minD = Constant.DOUBLE_NAN;
                rX = RealXfromScreenX(scrX);
                rY = RealYfromScreenY(scrY);

                for (int i = 0; i < _points.Count; i++)
                {
                    x1 = _points[i].X;
                    y1 = _points[i].Y;
                    res = false;
                    if ((_Closed) && (i == _points.Count - 1))
                    {
                        x2 = _points[0].X;
                        y2 = _points[0].Y;
                        res = true;
                    }
                    else if(i < _points.Count - 1)
                    {
                        x2 = _points[i + 1].X;
                        y2 = _points[i + 1].Y;
                        res = true;
                    }
                    if (res)
                    {
                        minX = x1;
                        maxX = x2;
                        if (x1 > x2)
                        {
                            minX = x2;
                            maxX = x1;
                        }
                        minY = y1;
                        maxY = y2;
                        if (y1 > y2)
                        {
                            minY = y2;
                            maxY = y1;
                        }

                        if ((x1 != x2) && (Math.Abs((y2 - y1) / (x2 - x1)) < 28))
                        {
                            if ((y1 != y2) && (Math.Abs((y2 - y1) / (x2 - x1)) > 0.05))
                            {
                                k = (y2 - y1) / (x2 - x1);
                                b = y1 - k * x1;
                                y3 = (k * rX + rY + b) / 2;
                                x3 = (y3 - b) / k;
                            }
                            else
                            {
                                y3 = minY + (maxY - minY) * (rX - minX) / (maxX - minX);
                                x3 = rX;
                            }
                        }
                        else
                        {
                            x3 = minX + (maxX - minX) * (rY - minY) / (maxY - minY);
                            y3 = rY;
                        }
                        scrX3 = ScreenXfromRealX(x3);
                        scrY3 = ScreenYfromRealY(y3);
                        d = Math.Sqrt((rX - x3) * (rX - x3) + (rY - y3) * (rY - y3));
                        scrX1 = ScreenXfromRealX(x1);
                        scrX2 = ScreenXfromRealX(x2);
                        scrY1 = ScreenYfromRealY(y1);
                        scrY2 = ScreenYfromRealY(y2);
                        scrD = Math.Sqrt((scrX - scrX3) * (scrX - scrX3) + (scrY - scrY3) * (scrY - scrY3));

                        //bVert = false; bHor = false;
                        //if (((rX == x1) || ((Math.Abs(scrX1 - scrX2) < 2) && (Math.Abs(scrX - scrX1) < 10))) &&
                        //    ((rY > minY) && (rY < maxY)))
                        //{
                        //    //d = scrX - (scrX1 + scrX2) / 2;
                        //    bVert = true;
                        //}
                        //if ((((Math.Abs(scrY1 - scrY2) < 2) && (Math.Abs(scrY - scrY1) < 10))) && ((rX > minX) && (rX < maxX)))
                        //{
                        //    //d = scrY - (scrY1 + scrY2) / 2;
                        //    bVert = true;
                        //}
                        if (((minD == Constant.DOUBLE_NAN) || (minD > d)) && (scrD < 30) && ((x3 >= minX) && (x3 <= maxX) && (y3 >= minY) && (y3 <= maxY)))
                        {
                            selRib = i;
                            minD = d;
                        }
                    }
                }
                return selRib;
            }
            return -1;
        }
        public bool ContourInScreenBounds(Rectangle ScreenRect)
        {
            int i;
            double MinX, MinY, MaxX, MaxY;
            MinX = RealXfromScreenX(ScreenRect.X);
            MaxX = RealXfromScreenX(ScreenRect.Right);
            MinY = RealYfromScreenY(ScreenRect.Top);
            MaxY = RealYfromScreenY(ScreenRect.Bottom);
            if ((_points != null) && (_points.Count > 0))
            {
                if (((Xmin >= MinX) && (Xmin < MaxX)) || ((Xmax >= MinX) && (Xmax < MaxX)) ||
                    ((Ymin >= MinY) && (Ymin < MaxY)) || ((Ymax >= MinY) && (Ymax < MaxY)))
                {
                    return true;
                }
                for (i = 0; i < _points.Count; i++)
                {
                    if (((_points[i].X > MinX) && (_points[i].X < MaxX)) || ((_points[i].Y > MinY) && (_points[i].Y < MaxY)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool ContourInBounds(int MinX, int MinY, int MaxX, int MaxY)
        {
            int i;
            if((_points != null)&&(_points.Count > 0))
            {
                if(((Xmin >= MinX) && (Xmin < MaxX)) || ((Xmax >= MinX) && (Xmax < MaxX)) ||
                   ((Ymin >= MinY) && (Ymin < MaxY)) || ((Ymax >= MinY) && (Ymax < MaxY)))
                {
                    return true;
                }
                for (i = 0; i < _points.Count; i++)
                {
                    if (((_points[i].X > MinX) && (_points[i].X < MaxX)) || ((_points[i].Y > MinY) && (_points[i].Y < MaxY)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool PointBodyEntry(double pX, double pY)
        {
            int i;
            double X;
            PointD p1, p2;
            bool parity = false;
            for (i = 0; i < _points.Count; i++)
            {
                p1 = _points[i];
                if(i < _points.Count - 1) 
                    p2 = _points[i + 1];
                else 
                    p2 = _points[0];
                X = ((pY - p1.Y)/(p2.Y - p1.Y))*(p2.X - p1.X) + p1.X;

                if ((pX > X) && (((pY >= p1.Y) && (pY <= p2.Y)) || ((pY >= p2.Y) && (pY <= p1.Y))))
                {
                    if (((pY != p1.Y) && (pY != p2.Y)) || ((pY == p1.Y) && (pY < p2.Y)) || ((pY == p2.Y) && (pY < p1.Y)))
                    {
                        parity = !parity;
                    }
                }
            }
            return parity;
        }
        public int GetIndexPointByXY(double scrX, double scrY, double ScreenRadius)
        {
            int k = -1;
            if ((_points != null) && (_points.Count > 0))
            {
                double d, x, y, maxD = Constant.DOUBLE_NAN;
                for (int i = 0; i < _points.Count; i++)
                {
                    x = ScreenXfromRealX(_points[i].X);
                    y = ScreenYfromRealY(_points[i].Y);
                    d = Math.Sqrt((x - scrX)*(x - scrX)+(y - scrY)*(y - scrY));
                    if ((d < ScreenRadius)&&((maxD == Constant.DOUBLE_NAN)||(d < maxD)))
                    {
                        maxD = d;
                        k = i;
                    }
                }
            }
            return k;
        }
        public int GetIndexPointByXY(double realX, double realY)
        {
            int k = -1;
            if ((_points != null) && (_points.Count > 0))
            {
                for (int i = 0; i < _points.Count; i++)
                {
                    if ((Math.Abs(_points[i].X - realX) < 0.000000001) && (Math.Abs(_points[i].Y - realY) < 0.000000001)) 
                        k = i;
                }
            }
            return k;
        }
        public int GetIndexPointByXY2(double realX, double realY)
        {
            int k = -1;
            if ((_points != null) && (_points.Count > 0))
            {
                for (int i = 0; i < _points.Count; i++)
                {
                    if ((Math.Abs(_points[i].X - realX) < 3) && (Math.Abs(_points[i].Y - realY) < 3))
                        k = i;
                }
            }
            return k;
        }
        public void Invert()
        {
            if ((this._points != null) && (this._points.Count > 0))
            {
                PointsXY newPoints = new PointsXY(this._points.Count);
                PointD point;
                for (int i = 0; i < this._points.Count; i++)
                {
                    newPoints.Add(this._points[this._points.Count - 1 - i].X, this._points[this._points.Count - 1 - i].Y);
                }
                this._points = newPoints;
            }
        }
        public void CreateCilia(double Distance, bool ByLeftSide)
        {
            if ((_points != null) && (_points.Count > 1))
            {
                double len = _points.GetLength();
                len += _points.GetDistanceXY(_points.Count - 1, 0);
                int n = (int)(len / Distance);
                cilia = new PointD[n * 2];
                int i, j, k;
                double sumLen = 0, rest = 0;
                k = 0; i = 0;

                for (i = 0; i < _points.Count; i++)
                {
                    j = (i < _points.Count - 1)? i + 1 : 0;
                    if (j == 0) ciliaLastPoint = k;
                    len = _points.GetDistanceXY(i, j);
                    sumLen += len;

                    if (sumLen > Distance)
                    {
                        while (sumLen > Distance)
                        {
                            cilia[k] = Geometry.GetPointInLineByDistance(_points[i], _points[j], rest + Distance);
                            cilia[k + 1] = Geometry.GetNormalPointInLine(_points[i], _points[j], cilia[k], Distance / 2, ByLeftSide);
                            sumLen -= Distance;
                            rest += Distance;
                            k += 2;
                        }
                        rest = -sumLen;
                    }
                    else
                    {
                        rest -= len;
                    }
                }
            }
        }
        public Contour GetSimplyContour(double Distance)
        {
            Contour cntr = null;
            if ((_points != null) && (_points.Count > 1))
            {
                cntr = new Contour(this);
                cntr._ContourType = this._ContourType;
                double len = _points.GetLength();
                len += _points.GetDistanceXY(_points.Count - 1, 0);
                int n = (int)(len / Distance);
                if (n > 2)
                {
                    cntr._points = new PointsXY(n);
                    int i, j, k;
                    double sumLen = 0, rest = 0;
                    k = 0; i = 0;
                    PointD pt;
                    cntr._points.Add(_points[0].X, _points[0].Y);
                    for (i = 0; i < _points.Count; i++)
                    {
                        j = (i < _points.Count - 1) ? i + 1 : 0;
                        len = _points.GetDistanceXY(i, j);
                        sumLen += len;

                        if (sumLen > Distance)
                        {
                            while (sumLen > Distance)
                            {
                                pt = Geometry.GetPointInLineByDistance(_points[i], _points[j], rest + Distance);
                                cntr._points.Add(pt.X, pt.Y);
                                sumLen -= Distance;
                                rest += Distance;
                                k++;
                            }
                            rest = -sumLen;
                        }
                        else
                        {
                            rest -= len;
                        }
                    }
                }
            }
            return cntr;
        }
        public bool GetClockWiseByX(bool ByMinimum)
        {
            bool result = false;
            int index = -1;
            double X = Constant.DOUBLE_NAN;
            for (int i = 0; i < _points.Count; i++)
            {
                if (ByMinimum)
                {
                    if ((X == Constant.DOUBLE_NAN) || (X > _points[i].X))
                    {
                        X = _points[i].X;
                        index = i;
                    }
                }
                else
                {
                    if ((X == Constant.DOUBLE_NAN) || (X < _points[i].X))
                    {
                        X = _points[i].X;
                        index = i;
                    }
                }
            }
            if (index != -1)
            {
                int i1, i2;
                i1 = index - 1;
                i2 = index + 1;
                if (i1 < 0) i1 = _points.Count - 1;
                if (i2 == _points.Count) i2 = 0;
                if (((_points[i1].Y < _points[index].Y) && (_points[index].Y < _points[i2].Y)) ||
                    ((_points[i1].Y == _points[index].Y) && (_points[index].Y < _points[i2].Y)) ||
                    ((_points[i1].Y < _points[index].Y) && (_points[index].Y == _points[i2].Y)))
                {
                    result = true;
                }
                else if (((_points[i2].Y < _points[index].Y) && (_points[index].Y < _points[i1].Y)) ||
                         ((_points[i2].Y == _points[index].Y) && (_points[index].Y < _points[i1].Y)) ||
                         ((_points[i2].Y < _points[index].Y) && (_points[index].Y == _points[i1].Y)))
                {
                    result = false;
                }
                else if ((_points[i1].Y < _points[index].Y) && (_points[i2].Y < _points[index].Y))
                {
                    double d1 = Math.Sqrt(Math.Pow(_points[i1].X - _points[index].X, 2) + Math.Pow(_points[i1].Y - _points[index].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(_points[i2].X - _points[index].X, 2) + Math.Pow(_points[i2].Y - _points[index].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i2], d1);
                        if (_points[i1].Y < pt.Y) result = true; else result = false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i1], d2);
                        if (pt.Y < _points[i2].Y) result = true; else result = false;
                    }
                }
                else if ((_points[i1].Y > _points[index].Y) && (_points[i2].Y > _points[index].Y))
                {
                    double d1 = Math.Sqrt(Math.Pow(_points[i1].X - _points[index].X, 2) + Math.Pow(_points[i1].Y - _points[index].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(_points[i2].X - _points[index].X, 2) + Math.Pow(_points[i2].Y - _points[index].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i2], d1);
                        if (_points[i1].Y < pt.Y) result = true; else result = false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i1], d2);
                        if (pt.Y < _points[i2].Y) result = true; else result = false;
                    }
                }
            }
            if (!ByMinimum) result = !result;
            return result;
        }
        public bool GetClockWiseByY(bool ByMinimum)
        {
            bool result = false;
            int index = -1;
            double Y = Constant.DOUBLE_NAN;
            for (int i = 0; i < _points.Count; i++)
            {
                if (ByMinimum)
                {
                    if ((Y == Constant.DOUBLE_NAN) || (Y < _points[i].Y))
                    {
                        Y = _points[i].Y;
                        index = i;
                    }
                }
                else
                {
                    if ((Y == Constant.DOUBLE_NAN) || (Y > _points[i].Y))
                    {
                        Y = _points[i].Y;
                        index = i;
                    }
                }
            }
            if (index != -1)
            {
                int i1, i2;
                i1 = index - 1;
                i2 = index + 1;
                if (i1 < 0) i1 = _points.Count - 1;
                if (i2 == _points.Count) i2 = 0;
                if (((_points[i1].X < _points[index].X) && (_points[index].X < _points[i2].X)) ||
                    ((_points[i1].X == _points[index].X) && (_points[index].X < _points[i2].X)) ||
                    ((_points[i1].X < _points[index].X) && (_points[index].X == _points[i2].X)))
                {
                    result = true;
                }
                else if (((_points[i2].X < _points[index].X) && (_points[index].X < _points[i1].X)) ||
                         ((_points[i2].X == _points[index].X) && (_points[index].X < _points[i1].X)) ||
                         ((_points[i2].X < _points[index].X) && (_points[index].X == _points[i1].X)))
                {
                    result = true;
                }
                else if ((_points[i1].X < _points[index].X) && (_points[i2].X < _points[index].X))
                {
                    double d1 = Math.Sqrt(Math.Pow(_points[i1].X - _points[index].X, 2) + Math.Pow(_points[i1].Y - _points[index].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(_points[i2].X - _points[index].X, 2) + Math.Pow(_points[i2].Y - _points[index].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i2], d1);
                        if (_points[i1].X < pt.X) result = true; else result = false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i1], d2);
                        if (pt.X < _points[i2].X) result = true; else result = false;
                    }
                }
                else if ((_points[i1].X > _points[index].X) && (_points[i2].X > _points[index].X))
                {
                    double d1 = Math.Sqrt(Math.Pow(_points[i1].X - _points[index].X, 2) + Math.Pow(_points[i1].Y - _points[index].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(_points[i2].X - _points[index].X, 2) + Math.Pow(_points[i2].Y - _points[index].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i2], d1);
                        if (_points[i1].X < pt.X) result = true; else result = false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i1], d2);
                        if (pt.X < _points[i2].X) result = true; else result = false;
                    }
                }
            }
            if (!ByMinimum) result = !result;
            return result;
        }

        private bool IsPointsByCW(PointsXY points)
        {
            int minXind = -1;
            double minX = Constant.DOUBLE_NAN;
            for (int i = 0; i < points.Count; i++)
            {
                if ((minX == Constant.DOUBLE_NAN) || (points[i].X < minX))
                {
                    minX = points[i].X;
                    minXind = i;
                }
            }
            if (minXind != -1)
            {
                int i1, i2;
                i1 = minXind - 1;
                i2 = minXind + 1;
                if (i1 < 0) i1 = points.Count - 1;
                if (i2 == points.Count) i2 = 0;
                if (((points[i1].Y < points[minXind].Y) && (points[minXind].Y < points[i2].Y)) ||
                    ((points[i1].Y == points[minXind].Y) && (points[minXind].Y < points[i2].Y)) ||
                    ((points[i1].Y < points[minXind].Y) && (points[minXind].Y == points[i2].Y)))
                {
                    return true;
                }
                else if (((points[i2].Y < points[minXind].Y) && (points[minXind].Y < points[i1].Y)) ||
                         ((points[i2].Y == points[minXind].Y) && (points[minXind].Y < points[i1].Y)) ||
                         ((points[i2].Y < points[minXind].Y) && (points[minXind].Y == points[i1].Y)))
                {
                    return false;
                }
                else if ((points[i1].Y < points[minXind].Y) && (points[i2].Y < points[minXind].Y))
                {
                    double d1 = Math.Sqrt(Math.Pow(points[i1].X - points[minXind].X, 2) + Math.Pow(points[i1].Y - points[minXind].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(points[i2].X - points[minXind].X, 2) + Math.Pow(points[i2].Y - points[minXind].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(points[minXind], points[i2], d1);
                        if (points[i1].Y < pt.Y) return true; else return false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(points[minXind], points[i1], d2);
                        if (pt.Y < points[i2].Y) return true; else return false;
                    }
                }
                else if ((points[i1].Y > points[minXind].Y) && (points[i2].Y > points[minXind].Y))
                {
                    double d1 = Math.Sqrt(Math.Pow(points[i1].X - points[minXind].X, 2) + Math.Pow(points[i1].Y - points[minXind].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(points[i2].X - points[minXind].X, 2) + Math.Pow(points[i2].Y - points[minXind].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(points[minXind], points[i2], d1);
                        if (points[i1].Y < pt.Y) return true; else return false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(points[minXind], points[i1], d2);
                        if (pt.Y < points[i2].Y) return true; else return false;
                    }
                }
            }
            return false;
        }
        public int ReadFromCache(StreamReader file)
        {
            if (file != null)
            {
                int j, iLen, Index;
                PointD point;
                char[] parser = new char[] { ';' };
                string tempStr;
                string[] parseStr;
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                string str = file.ReadLine();
                parseStr = str.Split(parser);
                this.Name = parseStr[0];
                if (parseStr.Length > 1 && parseStr[1].Length > 0) this.Text = parseStr[1].Replace("%_%", "\n");
                if (parseStr.Length > 2 && parseStr[2].Length > 0) this.Text2 = parseStr[2].Replace("%_%", "\n");

                str = file.ReadLine();
                this._ContourType = Convert.ToInt32(str);
                tempStr = file.ReadLine();
                if (tempStr.IndexOf('e') > -1)
                {
                    Index = -1;
                    this._Closed = Convert.ToBoolean(tempStr);
                }
                else
                {
                    Index = Convert.ToInt32(tempStr);
                    this._Closed = Convert.ToBoolean(file.ReadLine());
                }

                this.StratumCode = Convert.ToInt32(file.ReadLine());
                this._OilFieldCode = Convert.ToInt32(file.ReadLine());
                this._NGDUCode = Convert.ToInt32(file.ReadLine());
                tempStr = file.ReadLine();
                if (tempStr.IndexOf('e') == -1)
                {
                    this._EnterpriseCode = Convert.ToInt32(tempStr);
                    lineClr = Color.Empty;
                    tempStr = file.ReadLine();
                    if (tempStr.IndexOf('e') == -1)
                    {
                        parseStr = tempStr.Split(parser, StringSplitOptions.RemoveEmptyEntries);
                        LineColor = Color.FromArgb(Convert.ToInt32(parseStr[0]));
                        if (parseStr.Length > 1) LineWidth = Convert.ToInt32(parseStr[1]);
                        tempStr = file.ReadLine();
                    }
                }
               
                parseStr = tempStr.Split(parser, StringSplitOptions.RemoveEmptyEntries);
                this.Visible = Convert.ToBoolean(parseStr[0]);
                if (parseStr.Length > 1) this.Enable = Convert.ToBoolean(parseStr[1]);
                if (parseStr.Length > 2) this.VisibleName = Convert.ToBoolean(parseStr[2]);
               
                this._FillBrush = Convert.ToInt16(file.ReadLine());
                //this.fillBrush = new SolidBrush(Color.FromArgb(Convert.ToInt32(file.ReadLine())));

                tempStr = file.ReadLine();
                if (tempStr.IndexOf(';') > -1)
                {
                    parseStr = tempStr.Split(parser);
                    this.X = Convert.ToDouble(parseStr[0], nfi);
                    this.Y = Convert.ToDouble(parseStr[1], nfi);
                    iLen = Convert.ToInt32(file.ReadLine());
                }
                else
                    iLen = Convert.ToInt32(tempStr);
                

                this._points = new PointsXY(iLen);

                for (j = 0; j < iLen; j++)
                {
                    parseStr = file.ReadLine().Split(parser);
                    point.X = Convert.ToDouble(parseStr[0], nfi);
                    point.Y = Convert.ToDouble(parseStr[1], nfi);
                    this.Add(point.X, point.Y);
                }

                SetMinMax();
                if(this.X == Constant.DOUBLE_NAN) GetCenter();
                //Get
                return Index;
            }
            return -1;
        }
        public int ReadFromCacheBin(BinaryReader file, int ver)
        {
            if (file != null)
            {
                int j, iLen, Index;
                PointD point;

                this.Name = file.ReadString();
                this._ContourType = file.ReadInt32();
                Index = file.ReadInt32();
                this._Closed = file.ReadBoolean();

                this.StratumCode = file.ReadInt32();
                this._OilFieldCode = file.ReadInt32();
                this._NGDUCode = file.ReadInt32();
                this._EnterpriseCode = file.ReadInt32();
                this.Visible = file.ReadBoolean();

                this._FillBrush = file.ReadInt16();

                this.X = file.ReadDouble();
                this.Y = file.ReadDouble();
                if (ver > 0)
                {
                    this.Xmin = file.ReadDouble();
                    this.Xmax = file.ReadDouble();
                    this.Ymin = file.ReadDouble();
                    this.Ymax = file.ReadDouble();
                }

                iLen = file.ReadInt32();

                this._points = new PointsXY(iLen);

                for (j = 0; j < iLen; j++)
                {
                    point.X = file.ReadDouble();
                    point.Y = file.ReadDouble();
                    this.Add(point.X, point.Y);
                }
                if (ver > 1)
                {
                    iLen = file.ReadInt32();
                    if (iLen > 0)
                    {
                        cntrsOut = new List<Contour>();
                        for (j = 0; j < iLen; j++)
                        {
                            Contour cntrOut = new Contour();
                            cntrOut.ReadFromCacheBin(file, 2);
                            cntrsOut.Add(cntrOut);
                        }
                    }
                }
                if ((this._ContourType == 4) || (this._ContourType == 1004) || (this._ContourType == 5) || (this._ContourType == 1005))
                {
                    this.CreateCilia(100, (this._ContourType == 4 || this._ContourType == 5) ? !this._CCW : this._CCW);
                }
                SetMinMax();
                if (this.X == Constant.DOUBLE_NAN) GetCenter();
                return Index;
            }
            return -1;
        }
        public int ReadHeadLineFromCacheBin(BinaryReader file, int ver)
        {
            if (file != null)
            {
                int j, iLen, Index;
                PointD point;

                this.Name = file.ReadString();
                this._ContourType = file.ReadInt32();
                Index = file.ReadInt32();
                this._Closed = file.ReadBoolean();

                this.StratumCode = file.ReadInt32();
                this._OilFieldCode = file.ReadInt32();
                this._NGDUCode = file.ReadInt32();
                this._EnterpriseCode = file.ReadInt32();
                this.Visible = file.ReadBoolean();

                this._FillBrush = file.ReadInt16();
                this.X = file.ReadDouble();
                this.Y = file.ReadDouble();
                if (ver > 0)
                {
                    this.Xmin = file.ReadDouble();
                    this.Xmax = file.ReadDouble();
                    this.Ymin = file.ReadDouble();
                    this.Ymax = file.ReadDouble();
                }

                iLen = file.ReadInt32();
                if (this._ContourType >= 0 && this._ContourType != 1 && this._ContourType != 1010 && this._ContourType != 90)
                {
                    file.BaseStream.Seek(iLen * 16, SeekOrigin.Current);
                }
                else
                {
                    this._points = new PointsXY(iLen);

                    for (j = 0; j < iLen; j++)
                    {
                        point.X = file.ReadDouble();
                        point.Y = file.ReadDouble();
                        this.Add(point.X, point.Y);
                    }

                    SetMinMax();
                    if (this.X == Constant.DOUBLE_NAN) GetCenter();
                }
                if (ver > 1)
                {
                    iLen = file.ReadInt32();
                    if (iLen > 0)
                    {
                        cntrsOut = new List<Contour>();
                        for (j = 0; j < iLen; j++)
                        {
                            Contour cntrOut = new Contour();
                            cntrOut.ReadFromCacheBin2(file);
                            cntrsOut.Add(cntrOut);
                        }
                    }
                }
                return Index;
            }
            return -1;
        }

        public int ReadHeadLineFromCacheBin2(BinaryReader file)
        {
            if (file != null)
            {
                int j, iLen, Index;
                PointD point;

                this.Name = file.ReadString();
                this._ContourType = file.ReadInt32();
                Index = file.ReadInt32();
                this._Closed = file.ReadBoolean();

                this.StratumCode = file.ReadInt32();
                this._OilFieldCode = file.ReadInt32();
                this._NGDUCode = file.ReadInt32();
                this._EnterpriseCode = file.ReadInt32();
                this.Visible = file.ReadBoolean();

                this._FillBrush = file.ReadInt16();
                this.X = file.ReadDouble();
                this.Y = file.ReadDouble();
                this.Xmin = file.ReadDouble();
                this.Xmax = file.ReadDouble();
                this.Ymin = file.ReadDouble();
                this.Ymax = file.ReadDouble();

                iLen = file.ReadInt32();
                if (this._ContourType < 0 || this._ContourType == 1 || this._ContourType == 1010 || this._ContourType == 90)
                {
                    this._points = new PointsXY(iLen);

                    for (j = 0; j < iLen; j++)
                    {
                        point.X = file.ReadDouble();
                        point.Y = file.ReadDouble();
                        this.Add(point.X, point.Y);
                    }

                    SetMinMax();
                    if (this.X == Constant.DOUBLE_NAN) GetCenter();
                }
                else
                {
                    file.BaseStream.Seek(iLen * 16, SeekOrigin.Current);
                }

                iLen = Convert.ToInt32(file.ReadInt32());
                if (iLen > 0)
                {
                    cntrsOut = new List<Contour>();
                    for (j = 0; j < iLen; j++)
                    {
                        Contour cntrOut = new Contour();
                        cntrOut.ReadFromCacheBin2(file);
                        cntrsOut.Add(cntrOut);
                    }
                }
                return Index;
            }
            return -1;
        }
        public int ReadFromCacheBin2(BinaryReader file)
        {
            if (file != null)
            {
                int j, iLen, Index;
                PointD point;

                Name = file.ReadString();
                _ContourType = file.ReadInt32();
                Index = file.ReadInt32();
                _Closed = file.ReadBoolean();

                StratumCode = file.ReadInt32();
                _OilFieldCode = file.ReadInt32();
                _NGDUCode = file.ReadInt32();
                _EnterpriseCode = file.ReadInt32();
                Visible = file.ReadBoolean();

                _FillBrush = file.ReadInt16();

                X = file.ReadDouble();
                Y = file.ReadDouble();
                Xmin = file.ReadDouble();
                Xmax = file.ReadDouble();
                Ymin = file.ReadDouble();
                Ymax = file.ReadDouble();

                iLen = file.ReadInt32();

                _points = new PointsXY(iLen);

                for (j = 0; j < iLen; j++)
                {
                    point.X = file.ReadDouble();
                    point.Y = file.ReadDouble();
                    Add(point.X, point.Y);
                }
                int outCount = file.ReadInt32();
                if (outCount > 0)
                {
                    cntrsOut = new List<Contour>();
                    for (j = 0; j < outCount; j++)
                    {
                        Contour cntrOut = new Contour();
                        cntrOut.ReadFromCacheBin2(file);
                        cntrsOut.Add(cntrOut);
                    }
                }
                SetMinMax();
                return Index;
            }
            return -1;
        }
        public int ReadPointsFromCacheBin(BinaryReader file)
        {
            if (file != null)
            {
                int j, iLen, Index = 0;
                PointD point;

                file.ReadString();
                file.BaseStream.Seek(76, SeekOrigin.Current);
                iLen = file.ReadInt32();
                this._points = new PointsXY(iLen);
                for (j = 0; j < iLen; j++)
                {
                    point.X = file.ReadDouble();
                    point.Y = file.ReadDouble();
                    this.Add(point.X, point.Y);
                }
                SetMinMax();
                if (this.X == Constant.DOUBLE_NAN) GetCenter();
                //Get
                return Index;
            }
            return -1;
        }

        public bool WriteToCache(int LayerIndex, StreamWriter file)
        {
            if (file != null)
            {
                int j;
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";
                string str = this.Name;
                str += ";";
                if (this.Text != null && this.Text.Length > 0) str += Text.Replace("\n", "%_%");
                str += ";";
                if (this.Text2 != null && this.Text2.Length > 0) str += Text2.Replace("\n", "%_%");
                file.WriteLine(str);
                file.WriteLine(this._ContourType);
                file.WriteLine(LayerIndex);
                file.WriteLine(this._Closed);
                file.WriteLine(this.StratumCode);
                file.WriteLine(this._OilFieldCode);
                file.WriteLine(this._NGDUCode);
                file.WriteLine(this._EnterpriseCode);
                if (LineColor.ToArgb() != Color.Empty.ToArgb())
                {
                    file.WriteLine(this.LineColor.ToArgb().ToString() + ((lineWidth > -1) ? ";" + lineWidth.ToString() : string.Empty));
                }
                file.WriteLine(this.Visible.ToString() + ";" + Enable.ToString() + ";" + VisibleName.ToString());
                
                file.WriteLine(this._FillBrush);
                file.WriteLine(String.Format("{0};{1}",this.X.ToString(nfi), this.Y.ToString(nfi)));

                file.WriteLine(this._points.Count.ToString());
                for (j = 0; j < this._points.Count; j++)
                {
                    file.WriteLine(((PointD)this._points[j]).X.ToString(nfi) + ";" + ((PointD)this._points[j]).Y.ToString(nfi));
                }
                return true;
            }
            return false;
        }
        public bool WriteToCacheBin(int LayerIndex, BinaryWriter file)
        {
            if (file != null)
            {
                int j;
                file.Write(this.Name);
                file.Write(this._ContourType);
                file.Write(LayerIndex);
                file.Write(this._Closed);
                file.Write(this.StratumCode);
                file.Write(this._OilFieldCode);
                file.Write(this._NGDUCode);
                file.Write(this._EnterpriseCode);
                file.Write(this.Visible);
                file.Write((Int16)this._FillBrush);
                file.Write(this.X);
                file.Write(this.Y);
                file.Write(this.Xmin);
                file.Write(this.Xmax);
                file.Write(this.Ymin);
                file.Write(this.Ymax);


                file.Write(this._points.Count);
                for (j = 0; j < this._points.Count; j++)
                {
                    file.Write(((PointD)this._points[j]).X);
                    file.Write(((PointD)this._points[j]).Y);
                }
                if (cntrsOut != null)
                {
                    file.Write(cntrsOut.Count);
                    for (j = 0; j < cntrsOut.Count; j++)
                    {
                        cntrsOut[j].WriteToCacheBin(0, file);
                    }
                }
                else
                {
                    file.Write(0);
                }
                return true;
            }
            return false;
        }
        public void FreeDataMemory()
        {
            _points = null;
            cilia = null;
            scrPoint = null;
            penThin = null;
            penFat = null;
            brush = null;
            fillBrush = null;
            this.Selected = false;
        }

        private void CreateGraphicsPath()
        {
            if (_points != null)
            {
                int i, j, h = 1;
                float screenX, screenY;
                
                if ((_ContourType == -5) && (st_XUnitsInOnePixel > 0))
                {
                    if (st_XUnitsInOnePixel < 25)
                    {
                        h = 1;
                    }
                    else if (st_XUnitsInOnePixel < 32)
                    {
                        h = 1;
                    }
                    else if (st_XUnitsInOnePixel < 42)
                    {
                        h = 2;
                    }
                    else if (st_XUnitsInOnePixel < 54)
                    {
                        h = 3;
                    }
                    else if (st_XUnitsInOnePixel < 71)
                    {
                        h = 4;
                    }
                    else if (st_XUnitsInOnePixel < 92)
                    {
                        h = 5;
                    }
                    else if (st_XUnitsInOnePixel < 119)
                    {
                        h = 7;
                    }
                    else if (st_XUnitsInOnePixel < 154)
                    {
                        h = 8;
                    }
                    else if (st_XUnitsInOnePixel < 201)
                    {
                        h = 20;
                    }
                    while ((h > 1) && (h > (_points.Count / 4f))) h--;
                }
                
                
                if((int)(_points.Count / h) > 2) 
                {
                    scrPoint = new PointF[(int)(_points.Count / h)];
                    for (i = 0, j = 0; (i < _points.Count) && (j < scrPoint.Length); i += h)
                    {
                        screenX = ScreenXfromRealX(_points[i].X);
                        screenY = ScreenYfromRealY(_points[i].Y);
                        scrPoint[j].X = screenX;
                        scrPoint[j].Y = screenY;
                        j++;
                    }
                }
                if (cntrsOut != null)
                {
                    Contour cntr;
                    OutPath = new System.Drawing.Drawing2D.GraphicsPath();
                    for (i = 0; i < cntrsOut.Count; i++)
                    {
                        cntr = (Contour)cntrsOut[i];
                        if (cntr._points != null)
                        {
                            PointF[] pts = new PointF[cntr._points.Count + 1];
                            for (j = 0; j < cntr._points.Count; j++)
                            {
                                pts[j].X = ScreenXfromRealX(cntr._points[j].X);
                                pts[j].Y = ScreenYfromRealY(cntr._points[j].Y);
                            }
                            pts[j].X = ScreenXfromRealX(cntr._points[0].X);
                            pts[j].Y = ScreenYfromRealY(cntr._points[0].Y);
                            OutPath.AddPolygon(pts);
                        }
                    }
                }
            }

        }
        public void FillContourRegion(Graphics grfx)
        {
            if ((_points != null) && (_points.Count > 2))
            {
                //if (this._ContourType > -1) return;
                if ((this._ContourType == -1) && (st_VisbleLevel <= st_NgduXUnits)) return;
                if ((this.Selected) && (this._ContourType == -1) && (st_VisbleLevel <= st_MaxXUnits / 1.3)) return;
                if ((this._ContourType == -2) && (st_VisbleLevel <= st_NgduXUnits)) return;
                if ((this._ContourType == -3) && (st_VisbleLevel <= st_OilfieldXUnits)) return;
                if ((this._ContourType == -4) && (st_VisbleLevel <= st_AreaXUnits)) return;
                float screenX, screenY, screenX2, screenY2;

                if(C2DObject.DrawAllObjects || st_RealScreenRect.IntersectWith(new RectangleD(Xmin, Ymin, Xmax, Ymax)))
                {
                    screenX = ScreenXfromRealX(Xmin);
                    screenY = ScreenYfromRealY(Ymin);
                    screenX2 = ScreenXfromRealX(Xmax);
                    screenY2 = ScreenYfromRealY(Ymax);
                    bool brushCreated = false;
                    Brush br;
                    CreateGraphicsPath();
                    br = fillBrush;
                    if (this.Selected && _ContourType != -5 && _ContourType != -6 && _ContourType < 0) br = Brushes.BlanchedAlmond;

                    if (_ContourType < 0)
                    {
                        if ((!this.Selected || _ContourType == -5 || _ContourType == -6) && fillBrush == null)
                        {
                            switch (_ContourType)
                            {
                                case -1:
                                    br = Brushes.WhiteSmoke;
                                    break;
                                case -4:
                                    br = SmartPlusGraphics.Contour.Brushes.Blue25;
                                    break;
                                case -5:
                                    br = SmartPlusGraphics.Contour.Brushes.Peru200;
                                    break;
                                case -6:
                                    br = SmartPlusGraphics.Contour.Brushes.Peru125;
                                    break;
                                default:
                                    br = Brushes.LightGray;
                                    break;
                            }
                        }
                    }
                    else if (_FillBrush > -1)
                    {
                        br = new SolidBrush(Color.FromArgb(_FillBrush));
                        brushCreated = true;
                    }

                    #region old fill brush
                    //if (_ContourType == -4)
                    //{
                    //    br = SmartPlusGraphics.Contour.Brushes.LightGreen125;
                    //}
                    //if (fillBrush == null)
                    //{
                    //    switch (_FillBrush)
                    //    {
                    //        case 0:
                    //            br = Brushes.WhiteSmoke;
                    //            break;
                    //        case 1:
                    //            br = Brushes.LightGray;
                    //            break;
                    //        case 2:
                    //            br = Brushes.LightGray;
                    //            break;
                    //        case 3:
                    //            br = Brushes.LightGray;
                    //            break;
                    //        case 4:
                    //            br = Brushes.LightGray;
                    //            break;
                    //        case 5:
                    //            br = Brushes.LightGray;
                    //            break;
                    //        case 6:
                    //            br = Brushes.LightGray;
                    //            break;
                    //        case 11:
                    //            br = Brushes.LightGray;
                    //            break;
                    //        case 12:
                    //            br = SmartPlusGraphics.Contour.Brushes.LightGray125;
                    //            break;
                    //        case 13:
                    //            br = SmartPlusGraphics.Contour.Brushes.LightSkyBlue125;
                    //            break;
                    //        case 14:
                    //            br = SmartPlusGraphics.Contour.Brushes.Green125;
                    //            break;
                    //        case 15:
                    //            br = SmartPlusGraphics.Contour.Brushes.Red125;
                    //            break;
                    //        case 33:
                    //            br = SmartPlusGraphics.Contour.Brushes.Blue25;
                    //            break;
                    //        case 34:
                    //            br = SmartPlusGraphics.Contour.Brushes.Peru200;
                    //            break;
                    //        default:
                    //            br = Brushes.WhiteSmoke;
                    //            break;
                    //    }
                    //}
                    //else
                    //{
                    //    br = fillBrush;
                    //}
                    //if ((this.Selected) && (_FillBrush != 34))
                    //{

                    //}
                    #endregion

                    if (br != null)
                    {
                        if (OutPath != null) grfx.SetClip(OutPath, System.Drawing.Drawing2D.CombineMode.Exclude);
                        if (scrPoint != null) grfx.FillPolygon(br, scrPoint);
                        if (OutPath != null)
                        {
                            grfx.ResetClip();
                            grfx.DrawPath(Pens.Black, OutPath);
                            OutPath.Dispose();
                            OutPath = null;
                        }
                    }
                    if (brushCreated) br.Dispose();
                }
            }
        }
        public void DrawContourName(Graphics grfx)
        {
            DrawContourName(grfx, false);
        }
        public void DrawContourName(Graphics grfx, bool DrawBizPlanParams)
        {
            float xCenter, yCenter;
            float y;
            SizeF sizeName = SizeF.Empty;
            if (!(((this._ContourType < -1) && (this._ContourType != -6) && (st_VisbleLevel <= 15)) ||
                ((this._ContourType == -1) && (st_VisbleLevel <= st_NgduXUnits)) ||
                ((this._ContourType == -2) && (st_VisbleLevel <= st_NgduXUnits)) ||
                ((this._ContourType == -4) && (st_XUnitsInOnePixel > st_OilfieldXUnits)) ||
                (this._ContourType == 5000 && (st_VisbleLevel <= 45 || st_VisbleLevel > st_OilfieldXUnits)) ||
                (this._ContourType == 5001 && (st_VisbleLevel <= 10 || st_VisbleLevel >= 45)) ||
                ((this._ContourType > -1) && (!VisibleName))))
            {
                if ((this.Visible) && (this.X != Constant.DOUBLE_NAN) && (C2DObject.DrawAllObjects || st_RealScreenRect.Contains(this.X, this.Y)))
                {
                    xCenter = ScreenXfromRealX(this.X); 
                    yCenter = ScreenYfromRealY(this.Y);
                    SizeF size;
                    sizeName = grfx.MeasureString(this.Name, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                    if (sizeName.Height == 0) sizeName = grfx.MeasureString("��", SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                    DrawWhiteGroundString(grfx, this.Name, SmartPlusGraphics.Contour.Fonts.Name, Brushes.Black, xCenter - sizeName.Width / 2, yCenter - sizeName.Height / 2, StringFormat.GenericTypographic);

                    StringFormat sf = StringFormat.GenericTypographic;
                    y = yCenter + sizeName.Height / 2;
                    if (this.Text != null && (this._ContourType != 0 || st_VisbleLevel < 120))
                    {
                        size = grfx.MeasureString(this.Text, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, sf);
                        DrawWhiteGroundString(grfx, this.Text, SmartPlusGraphics.Contour.Fonts.Name, Brushes.Black, xCenter - size.Width / 2, y, StringFormat.GenericTypographic);
                        y += size.Height;
                    }

                    if (this.Text2 != null)
                    {
                        size = grfx.MeasureString(this.Text2, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, sf);
                        DrawWhiteGroundString(grfx, this.Text2, SmartPlusGraphics.Contour.Fonts.Name, Brushes.Black, xCenter - size.Width / 2, y, StringFormat.GenericTypographic);
                    }

                    // BP params
                    if (ShowBizPlanParams && BizPlanParams != null && BizPlanParams[0] > 0)
                    {
                        bool plusBPYear = BizPlanParams[1] >= 0;
                        bool plusBPMonth = BizPlanParams[2] >= 0;
                        Bitmap bmp = null;
                        if (BizPlanParams[3] >= 0)
                        {
                            bmp = SmartPlus.Properties.Resources.Enable16;
                        }
                        else if (BizPlanParams[3] >= -1)
                        {
                            bmp = SmartPlus.Properties.Resources.circle16_yellow;
                        }
                        else if (BizPlanParams[3] >= -5)
                        {
                            bmp = SmartPlus.Properties.Resources.circle16_orange;
                        }
                        else
                        {
                            bmp = SmartPlus.Properties.Resources.Disable;
                        }

                        int side = 8;
                        Rectangle rect = new Rectangle((int)(xCenter - sizeName.Width / 2 - side - 2), (int)(yCenter - side / 2), side, side);
                        grfx.DrawImage(bmp, rect);
                        if ((_ContourType == -3 && st_VisbleLevel < 150) || (_ContourType == -2 && st_VisbleLevel < 1000) || DrawBizPlanParams)
                        {
                            string strBPYear, strBPMonth, strBPYearParams, strBPMonthParams;
                            DateTime date = DateTime.FromOADate(BizPlanParams[5]);

                            strBPYear = string.Format("dQ�({0:yyyy}) ", date);
                            strBPYearParams = string.Format("{0}{1:0.#} ({2}{3:0.#} %)", plusBPYear ? "+" : "", BizPlanParams[1], plusBPYear ? "+" : "", BizPlanParams[3]);
                            strBPMonth = string.Format("dQ�({0:MM.yyyy}) ", date);
                            strBPMonthParams = string.Format("{0}{1:0.#} ({2}{3:0.#} %)", plusBPMonth ? "+" : "", BizPlanParams[2], plusBPMonth ? "+" : "", BizPlanParams[4]);
                            size = grfx.MeasureString(strBPYear, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, sf);
                            SizeF size2 = grfx.MeasureString(strBPYearParams, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, sf);
                            SizeF sizeM = grfx.MeasureString(strBPMonth, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, sf);
                            SizeF sizeM2 = grfx.MeasureString(strBPMonthParams, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, sf);
                            float width = (size.Width + size2.Width > sizeM.Width + sizeM2.Width) ? size.Width + size2.Width + 1 : sizeM.Width + sizeM2.Width + 1;

                            RectangleF rectf = new RectangleF((float)xCenter - width / 2 - 2, y - 2, width + 4, size.Height + sizeM.Height + 4);
                            grfx.FillRectangle(Brushes.White, rectf);
                            grfx.DrawRectangle(SmartPlusGraphics.Contour.Pens.Gray1, rectf.X, rectf.Y, rectf.Width, rectf.Height);

                            grfx.DrawString(strBPYear, SmartPlusGraphics.Contour.Fonts.Name, SmartPlusGraphics.Contour.Brushes.Black125, (float)xCenter - (size.Width + size2.Width + 1) / 2, y, sf);
                            grfx.DrawString(strBPYearParams, SmartPlusGraphics.Contour.Fonts.Name, plusBPYear ? SmartPlusGraphics.Contour.Brushes.Green185 : SmartPlusGraphics.Contour.Brushes.Red185, (float)xCenter + (size.Width - size2.Width) / 2 + 1, y, sf);
                            y += size.Height;
                            grfx.DrawString(strBPMonth, SmartPlusGraphics.Contour.Fonts.Name, SmartPlusGraphics.Contour.Brushes.Black125, (float)xCenter - (sizeM.Width + sizeM2.Width + 1) / 2, y, sf);
                            grfx.DrawString(strBPMonthParams, SmartPlusGraphics.Contour.Fonts.Name, plusBPMonth ? SmartPlusGraphics.Contour.Brushes.Green185 : SmartPlusGraphics.Contour.Brushes.Red185, (float)xCenter + (sizeM.Width - sizeM2.Width) / 2 + 1, y, sf);
                        }
                    }
                }
            }
            DrawLetterBox(grfx);
        }
        private void DrawLetterBox(Graphics grfx)
        {
            if (ShowLetterBox && LetterBoxLevel > -1 &&
                ((_ContourType > -3 && st_XUnitsInOnePixel > st_NgduXUnits) || 
                (_ContourType == -3 && st_XUnitsInOnePixel > st_OilfieldXUnits) || 
                 (_ContourType == -4 && st_XUnitsInOnePixel > st_AreaXUnits)))
            {
                if (C2DObject.DrawAllObjects || st_RealScreenRect.Contains(this.X, this.Y))
                {
                    float xCenter = ScreenXfromRealX(this.X); 
                    float yCenter = ScreenYfromRealY(this.Y);
                    SizeF sizeName = grfx.MeasureString(this.Name, SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);
                    if (sizeName.Height == 0) sizeName = grfx.MeasureString("��", SmartPlusGraphics.Contour.Fonts.Name, Point.Empty, StringFormat.GenericTypographic);

                    Bitmap bmp = null;
                    switch (LetterBoxLevel)
                    {
                        case 0:
                            bmp = LetterBoxImageGray;
                            break;
                        case 1:
                            bmp = LetterBoxImageOrange;
                            break;
                        case 2:
                            bmp = LetterBoxImageGreen;
                            break;
                    }
                    if (bmp != null)
                    {
                        int side = 12;
                        Rectangle rect = new Rectangle((int)(xCenter - sizeName.Width / 2), (int)(yCenter - side - 2), side, side);
                        grfx.DrawImage(bmp, rect);
                    }
                }
            }
        }
        private void DrawContourLine(Graphics grfx, float screenX, float screenY, float screenX2, float screenY2)
        {
            if ((this.Selected) && (this._ContourType >= 0) && (st_VisbleLevel > st_OilfieldXUnits))
            {
                grfx.DrawLine(SmartPlusGraphics.Contour.Pens.Select, screenX, screenY, screenX2, screenY2);
                grfx.FillEllipse(Brushes.Coral, screenX - 2.5f, screenY - 2.5f, 5, 5);
            }
            else if ((this.Selected) && (this._ContourType == 0) && (st_VisbleLevel <= st_OilfieldXUnits))
            {
                grfx.DrawEllipse(Pens.Black, screenX - 5, screenY - 5, 10, 10);
                grfx.DrawLine(SmartPlusGraphics.Contour.Pens.Default, screenX, screenY, screenX2, screenY2);
                grfx.FillEllipse(Brushes.Yellow, screenX - 4.5f, screenY - 4.5f, 9, 9);
            }
            else if ((this.Selected) && (this._ContourType > 0) && (st_VisbleLevel <= st_OilfieldXUnits) && (penFat != null)) // todo ������ penFat == null
            {
                grfx.DrawLine(penFat, screenX, screenY, screenX2, screenY2);
                grfx.FillEllipse(brush, screenX - 4.5f, screenY - 4.5f, 9, 9);
            }
            else if ((this.Selected) && (this._ContourType == -5))
            {
                grfx.DrawLine(Pens.Black, screenX, screenY, screenX2, screenY2);
            }
            else if ((this.Selected) && (this._ContourType < 0) && (this._ContourType != -5))
            {
                grfx.DrawLine(SmartPlusGraphics.Contour.Pens.Select, screenX, screenY, screenX2, screenY2);
                grfx.FillEllipse(Brushes.Coral, screenX - 2.5f, screenY - 2.5f, 5, 5);
            }
            else if (this._ContourType >= 0)
            {
                if (brush == null)
                {
                    if (ColorMode)
                    {
                        if (dict != null)
                        {
                            int color = dict.GetStratumColorByStratumTree(StratumCode);
                            if (color != -1)
                            {
                                brush = new SolidBrush(Color.FromArgb(color));
                            }
                            else
                            {
                                brush = new SolidBrush(Color.Silver);
                            }
                        }
                        else if (brush == null)
                        {
                            brush = Brushes.Black;
                        }
                        penThin = new Pen(brush, 1);
                        penFat = new Pen(brush, 2);
                    }
                    else
                    {
                        switch (this._ContourType)
                        {
                            case 0:
                                brush = Brushes.Black;
                                penThin = Pens.Black;
                                break;
                            case 1:
                                brush = Brushes.Black;
                                penThin = Pens.Black;
                                break;
                            case 2:
                                brush = SmartPlusGraphics.Contour.Brushes.Blue;
                                penThin = SmartPlusGraphics.Contour.Pens.Blue;
                                break;
                            case 3:
                                brush = Brushes.Green;
                                penThin = Pens.Green;
                                break;
                            case 4:
                                brush = Brushes.Red;
                                penThin = Pens.Red;
                                break;
                            case 5:
                                brush = Brushes.Red;
                                penThin = Pens.Red;
                                break;
                            case 10:
                                brush = Brushes.DarkMagenta;
                                penThin = Pens.DarkMagenta;
                                break;
                            case 53:
                                brush = Brushes.Black;
                                penThin = Pens.Black;
                                break;
                            case 1002:
                                brush = SmartPlusGraphics.Contour.Brushes.Blue;
                                penThin = SmartPlusGraphics.Contour.Pens.Blue;
                                break;
                            case 1003:
                                brush = Brushes.Green;
                                penThin = Pens.Green;
                                break;
                            case 1004:
                                brush = Brushes.Red;
                                penThin = Pens.Red;
                                break;
                            case 1005:
                                brush = Brushes.Red;
                                penThin = Pens.Red;
                                break;
                            default:
                                brush = Brushes.Silver;
                                penThin = Pens.Silver;
                                break;
                        }
                    }
                    penFat = new Pen(brush, 2);
                }
                if (lineWidth == -1)
                {
                    pen = (st_XUnitsInOnePixel < 15) ? penFat : penThin;
                }
                grfx.DrawLine(pen, screenX, screenY, screenX2, screenY2);
            }
            else if ((this._ContourType == -1) && !this.Selected)
            {
                Pen p = SmartPlusGraphics.Contour.Pens.Region;
                if (st_VisbleLevel < st_NgduXUnits) p = SmartPlusGraphics.Contour.Pens.Gray1;
                grfx.DrawLine(p, screenX, screenY, screenX2, screenY2);
                if (st_VisbleLevel > st_NgduXUnits) grfx.FillEllipse(Brushes.LightPink, screenX - 1.5f, screenY - 1.5f, 3, 3);
            }
            else if ((this._ContourType == -2) && !this.Selected)
            {
                Pen p = SmartPlusGraphics.Contour.Pens.NGDU;
                if (st_VisbleLevel < st_NgduXUnits) p = SmartPlusGraphics.Contour.Pens.Gray1;
                grfx.DrawLine(p, screenX, screenY, screenX2, screenY2);
                if (st_VisbleLevel > st_NgduXUnits) grfx.FillEllipse(Brushes.CornflowerBlue, screenX - 1.5f, screenY - 1.5f, 3, 3);
            }
            else if ((this._ContourType == -3) && !this.Selected)
            {
                Pen p = SmartPlusGraphics.Contour.Pens.OilField;
                if (st_VisbleLevel <= st_OilfieldXUnits) p = SmartPlusGraphics.Contour.Pens.Gray1;
                grfx.DrawLine(p, screenX, screenY, screenX2, screenY2);
            }
            else if ((this._ContourType == -4) && !this.Selected)
            {
                grfx.DrawLine(SmartPlusGraphics.Contour.Pens.Area, screenX, screenY, screenX2, screenY2);
                grfx.FillEllipse(Brushes.Red, screenX - 0.5f, screenY - 0.5f, 1, 1);
            }
            else if ((this._ContourType == -5  || this._ContourType == -6) && !this.Selected)
            {
                grfx.DrawLine(Pens.Black, screenX, screenY, screenX2, screenY2);
            }
        }

        public override void DrawObject(Graphics grfx) 
        {
            float screenX, screenY, screenX2 = 0, screenY2 = 0;
            double realX, realY, realX2, realY2;
            this.Visible = false;
            if ((this._points == null) || (this._points.Count == 0)) return;

            if ((this._ContourType == -1) && (st_VisbleLevel > 0))
            {
                this.Visible = true;
            }
            else if ((this._ContourType == -2) && (st_VisbleLevel > 0) /*&&(st_XUnitsInOnePixel <= 1300)*/)
            {
                this.Visible = true;
            }
            else if ((this._ContourType == -3) && (st_VisbleLevel > 0) && (st_VisbleLevel <= st_NgduXUnits))
            {
                this.Visible = true;
            }
            else if ((this._ContourType == -4)) // && (st_XUnitsInOnePixel > 0) && (st_XUnitsInOnePixel <= st_OilfieldXUnits))
            {
                this.Visible = true;
            }
            else if ((this._ContourType == -5) && (st_VisbleLevel > 0) && (st_VisbleLevel <= st_OilfieldXUnits))
            {
                this.Visible = true;
            }
            else if ((this._ContourType == -6) && (st_VisbleLevel > 0) && (st_VisbleLevel <= st_OilfieldXUnits))
            {
                this.Visible = true;
            }
            else if ((this._ContourType > -1) && (st_VisbleLevel <= st_OilfieldXUnits))
            {
                this.Visible = true;
            }
#if DEBUG
            //else if (this._ContourType > -1)
            //{
            //    this.Visible = true;
            //}
#endif

            if (this.Visible)
            {
                FillContourRegion(grfx);
                if (_points.Count > 1)
                {
                    int j = 0, h = 1, ind;
                    realX = _points[0].X; 
                    realY = _points[0].Y;
                    screenX = ScreenXfromRealX(realX);
                    screenY = ScreenYfromRealY(realY);
                    //screenX = ScreenXfromRealX(_points[0].X);
                    //screenY = ScreenYfromRealY(_points[0].Y);
                    ind = 0;
                    //if ((_ContourType == -5) && (st_XUnitsInOnePixel > 0))
                    //{
                    //    if (st_XUnitsInOnePixel < 25)
                    //    {
                    //        h = 1;
                    //    }
                    //    else if (st_XUnitsInOnePixel < 32)
                    //    {
                    //        h = 1;
                    //    }
                    //    else if (st_XUnitsInOnePixel < 42)
                    //    {
                    //        h = 2;
                    //    }
                    //    else if (st_XUnitsInOnePixel < 54)
                    //    {
                    //        h = 3;
                    //    }
                    //    else if (st_XUnitsInOnePixel < 71)
                    //    {
                    //        h = 4;
                    //    }
                    //    else if (st_XUnitsInOnePixel < 92)
                    //    {
                    //        h = 5;
                    //    }
                    //    else if (st_XUnitsInOnePixel < 119)
                    //    {
                    //        h = 7;
                    //    }
                    //    else if (st_XUnitsInOnePixel < 154)
                    //    {
                    //        h = 8;
                    //    }
                    //    else if (st_XUnitsInOnePixel < 201)
                    //    {
                    //        h = 20;
                    //    }
                    //    while ((h > 1) && (h > (_points.Count / 4f))) h--;
                    //}

                    while (j < _points.Count - h)
                    {
                        //screenX2 = ScreenXfromRealX(_points[j + h].X);
                        //screenY2 = ScreenYfromRealY(_points[j + h].Y);

//#if !DEBUG
                        while (j < _points.Count - h)
                        {
                            realX2 = _points[j + h].X;
                            realY2 = _points[j + h].Y;

                            if (Math.Abs(realX2 - realX) > 2 * st_XUnitsInOnePixel || Math.Abs(realY2 - realY) > 2 * st_YUnitsInOnePixel)
                            {
                                if ((C2DObject.DrawAllObjects ||
                                    st_RealScreenRect.Contains(realX, realY) ||
                                    st_RealScreenRect.Contains(realX2, realY2) ||
                                    LineIntersectBounds(realX, realY, realX2, realY2, st_RealScreenRect)))
                                {
#if DEBUG
                                    //if (this.Selected) grfx.DrawString(ind.ToString(), SmartPlusGraphics.Contour.Fonts.Name, Brushes.Black, screenX + 3, screenY + 3);
#endif
                                    screenX = ScreenXfromRealX(realX);
                                    screenY = ScreenYfromRealY(realY);
                                    screenX2 = ScreenXfromRealX(realX2);
                                    screenY2 = ScreenYfromRealY(realY2);
                                    DrawContourLine(grfx, screenX, screenY, screenX2, screenY2);
                                }
                                realX = realX2;
                                realY = realY2;
                                break;
                            }

                            //screenX2 = ScreenXfromRealX(_points[j + h].X);
                            //screenY2 = ScreenYfromRealY(_points[j + h].Y);
                            j += h;
                        }
//#endif
#if DEBUG
                        ind++;
#endif
                        j += h;
                    }

                    if (_Closed)
                    {
                        if (//(Math.Sqrt((screenX - screenX2) * (screenX - screenX2) + (screenY - screenY2) * (screenY - screenY2)) >= 0.5) &&
                            (C2DObject.DrawAllObjects ||
                            st_RealScreenRect.Contains(_points[0].X, _points[0].Y) ||
                            st_RealScreenRect.Contains(_points[_points.Count - 1].X, _points[_points.Count - 1].Y) ||
                            LineIntersectBounds(_points[0].X, _points[0].Y, _points[_points.Count - 1].X, _points[_points.Count - 1].Y, st_RealScreenRect)))
                        {
                            screenX = ScreenXfromRealX(_points[0].X);
                            screenY = ScreenYfromRealY(_points[0].Y);
                            screenX2 = ScreenXfromRealX(_points[_points.Count - 1].X);
                            screenY2 = ScreenYfromRealY(_points[_points.Count - 1].Y);

                            DrawContourLine(grfx, screenX2, screenY2, screenX, screenY);

                            if ((this.Selected) && (this._ContourType == 0) && (st_VisbleLevel <= st_OilfieldXUnits))
                            {
                                grfx.DrawEllipse(Pens.Black, screenX - 5, screenY - 5, 10, 10);
                                grfx.FillEllipse(Brushes.Yellow, screenX - 4.5f, screenY - 4.5f, 9, 9);
                            }
                        }
                    }
                    if ((cilia != null) && (penFat != null) && (penThin != null))
                    {
                        j = 0;
                        while (j < cilia.Length - 1)
                        {
                            if ((!_Closed) && (j >= ciliaLastPoint)) break;
                            if (st_RealScreenRect.Contains(cilia[j].X, cilia[j].Y))
                            {
                                screenX = ScreenXfromRealX(cilia[j].X);
                                screenY = ScreenYfromRealY(cilia[j].Y);
                                screenX2 = ScreenXfromRealX(cilia[j + 1].X);
                                screenY2 = ScreenYfromRealY(cilia[j + 1].Y);
                                if (lineWidth == -1)
                                {
                                    pen = (st_XUnitsInOnePixel < 15) ? penFat : penThin;
                                }
                                grfx.DrawLine(pen, screenX, screenY, screenX2, screenY2);
                            }
                            j += 2;
                        }
                    }
                }
                else
                {
                    if (_points.Count == 1)
                    {
                        if ((this.Selected) && (this._ContourType >= 0) && (st_VisbleLevel <= st_OilfieldXUnits))
                        {
                            screenX = ScreenXfromRealX(_points[0].X);
                            screenY = ScreenYfromRealY(_points[0].Y);
                            grfx.DrawEllipse(Pens.Black, screenX - 5, screenY - 5, 10, 10);
                            grfx.FillEllipse(Brushes.Yellow, screenX - 4.5f, screenY - 4.5f, 9, 9);
                        }
                    }
                }
#if DEBUG
               // f.Dispose();
#endif
                if ((_ContourType == -4) && (this.Selected)) DrawContourName(grfx);
                if (this.VisibleName) DrawContourName(grfx);
                if ((_Fantom) && (_points.Count > 1))
                {
                    screenX = ScreenXfromRealX(_points[0].X);
                    screenY = ScreenYfromRealY(_points[0].Y);
                    screenX2 = ScreenXfromRealX(_points[_points.Count - 1].X);
                    screenY2 = ScreenYfromRealY(_points[_points.Count - 1].Y);
                    grfx.DrawLine(SmartPlusGraphics.Contour.Pens.Fantom, screenX2, screenY2, screenX, screenY);

                    grfx.DrawEllipse(Pens.Black, screenX2 - 5, screenY2 - 5, 10, 10);
                    grfx.FillEllipse(Brushes.Yellow, screenX2 - 4.5f, screenY2 - 4.5f, 9, 9);
                }
                if (_debugPoints != null)
                {
                    for (int i = 0; i < _debugPoints.Count; i++)
                    {
                        screenX = ScreenXfromRealX(_debugPoints[i].X);
                        screenY = ScreenYfromRealY(_debugPoints[i].Y);
                        grfx.FillEllipse(Brushes.Red, screenX - 1.5f, screenY - 1.5f, 3, 3);
                    }
                }
            }
        }
    }
    
    public enum ContoursOperation
    {
        /// <summary>
        /// �����������
        /// </summary>
        Union,
        /// <summary>
        /// ���������
        /// </summary>
        Difference,
        /// <summary>
        /// �����������
        /// </summary>
        Crossing,
        /// <summary>
        /// ������������ �����������
        /// </summary>
        SymmetricDifference
    }
    public sealed class Polygon
    {
        PointsCollection Points;
        EdgesCollection Edges;
        bool ResultReady;
        
        internal struct PolyPoint
        {
            public int Index;
            public List<int> EdgesInd;
            public PointD Point;
            public bool Used;
        }
        internal class Edge
        {
            public int Cntr;
            public double Angle;
            public int indA;
            public int indB;
            public int Left;
            public int Right;
            public bool Used;
            public bool Selected;
            public Edge()
            {
                this.Cntr = 0;
                this.Angle = 0;
                this.indA = 0;
                this.indB = 0;
                this.Left = 0;
                this.Right = 0;
                this.Selected = false;
                this.Used = false;
            }
            public Edge Clone()
            {
                Edge newEdge = new Edge();
                newEdge.Angle = this.Angle;
                newEdge.Cntr = this.Cntr;
                newEdge.indA = this.indA;
                newEdge.indB = this.indB;
                newEdge.Left = this.Left;
                newEdge.Right = this.Right;
                newEdge.Selected = this.Selected;
                newEdge.Used = this.Used;
                return newEdge;
            }
            public void Invert()
            {
                int t = indA;
                indA = indB;
                indB = t;
                t = Left;
                Left = Right;
                Right = t;
                Cntr++;
                if (Cntr > 1) Cntr = 0;
            }
        }
        internal class PointsCollection
        {
            private List<PolyPoint> Points;

            public PointsCollection(int Capacity)
            {
                Points = new List<PolyPoint>(Capacity);
            }

            public int Count { get { return Points.Count; } }

            public void Trim()
            {
                Points.TrimExcess();
            }
            public void Clear()
            {
                Points.Clear();
            }
            public PolyPoint this[int index]
            {
                get { return Points[index]; }
                set { Points[index] = value; }
            }
            public int AddPoint(PointD Point) 
            {
                PolyPoint pt;
                pt.Used = false;
                pt.EdgesInd = new List<int>(2);
                pt.Point = Point;
                pt.Index = Points.Count;
                Points.Add(pt);
                return pt.Index;
            }
            
            public int GetPointIndex(PointD Point)
            {
                return GetPointIndex(Point.X, Point.Y);
            }
            public int GetPointIndex(double X, double Y)
            {
                for (int i = 0; i < Points.Count; i++)
                {
                    if ((Math.Abs(Points[i].Point.X - X) < 0.000001) && (Math.Abs(Points[i].Point.Y - Y) < 0.000001))
                    {
                        return i;
                    }
                }
                return -1;
            }
        }
        internal class EdgesCollection
        {
            private PointsCollection Points;
            public List<Edge> Edges;
            public int Count { get { return Edges.Count; } }

            public void Trim()
            {
                Edges.TrimExcess();
            }
            public EdgesCollection(PointsCollection Points, int Capacity)
            {
                Edges = new List<Edge>(Capacity);
                this.Points = Points;
            }
            public Edge this[int index]
            {
                get { return Edges[index]; }
                set { Edges[index] = value; }
            }
            public int GetEdgeIndex(int Start, int End)
            {
                for (int i = 0; i < Edges.Count; i++)
                {
                    if ((Edges[i].indA == Start) && (Edges[i].indB == End))
                    {
                        return i;
                    }
                }
                return -1;
            }
            private void SetEdgeAngle(int EdgeIndex)
            {
                Edge edge = Edges[EdgeIndex];
                double angle = 0;
                PointD point1 = Points[edge.indA].Point, point2 = Points[edge.indB].Point;
                if (Math.Abs(point1.X - point2.X) < 0.000001) // x1 == x2
                {
                    angle = Math.PI / 2;
                    if (point1.Y > point2.Y)
                    {
                        angle = 3 * Math.PI / 2;
                    }
                }
                else
                {
                    angle = Math.Atan((point2.Y - point1.Y) / (point2.X - point1.X));
                    if (point2.X < point1.X)
                    {
                        angle += Math.PI;
                    }
                    else if (point1.Y > point2.Y)
                    {
                        angle += 2 * Math.PI;
                    }
                }
                edge.Angle = angle;
                //Edges[EdgeIndex] = edge;
            }
            public void AddEdge(Edge edge)
            {
                Edges.Add(edge);
            }
            public void AddEdge(int NumCntr, int Start, int End, bool ClockWise)
            {
                Edge edge = new Edge();
                PolyPoint pt;
                int ind = GetEdgeIndex(Start, End);
                if (ind == -1)
                {
                    edge.Angle = 0;
                    edge.Cntr = NumCntr;
                    edge.Used = false;
                    edge.Selected = false;
                    edge.indA = Start;
                    pt = Points[Start];
                    if (pt.EdgesInd == null) pt.EdgesInd = new List<int>();
                    pt.EdgesInd.Add(Edges.Count);

                    edge.indB = End;
                    pt = Points[End];
                    if (pt.EdgesInd == null) pt.EdgesInd = new List<int>();
                    pt.EdgesInd.Add(Edges.Count);

                    edge.Left = -2;
                    edge.Right = NumCntr;
                    Edges.Add(edge);
                }
                else if (Edges[ind].Cntr != NumCntr)
                {
                    edge = Edges[ind];
                    edge.Cntr = -1;
                    if (Edges[ind].Right == -2) edge.Right = NumCntr; else edge.Right = -1;
                    //Edges[ind] = edge;
                }
            }
            public bool InsertPoint(int IndexEdge, int IndexPoint)
            {
                bool result = false;
                if (IndexEdge < Edges.Count)
                {
                    Edge InitEdge = Edges[IndexEdge];
                    if ((Edges[IndexEdge].indA != IndexPoint) && (Edges[IndexEdge].indB != IndexPoint))
                    {
                        Edge InsEdge = new Edge();
                        InsEdge.Angle = 0;
                        InsEdge.Cntr = InitEdge.Cntr;
                        InsEdge.Left = InitEdge.Left;
                        InsEdge.Right = InitEdge.Right;
                        InsEdge.Used = false;
                        InsEdge.Selected = false;
                        int start = InitEdge.indA, end = IndexPoint;

                        int ind = GetEdgeIndex(IndexPoint, InitEdge.indB);
                        if (ind == -1)
                        {
                            InitEdge.indA = IndexPoint;
                        }
                        else
                        {
                            InitEdge = Edges[ind];
                            InitEdge.Cntr = -1;
                            if (Edges[ind].Right == -2) InitEdge.Right = InitEdge.Cntr; else InitEdge.Right = -1;
                            //Edges[ind] = InitEdge;
                            Edges.RemoveAt(IndexEdge);
                        }

                        ind = GetEdgeIndex(start, end);
                        if (ind == -1)
                        {
                            InsEdge.indA = start;
                            InsEdge.indB = end;
                            Edges.Insert(IndexEdge, InsEdge);
                        }
                        else
                        {
                            InsEdge = Edges[ind];
                            InsEdge.Cntr = -1;
                            if (Edges[ind].Right == -2) InsEdge.Right = InitEdge.Cntr; else InsEdge.Right = -1;
                            //Edges[ind] = InsEdge;
                        }
                        result = true;
                    }
                }
                return result;
            }

            private int IntersectEdges(Edge edge1, Edge edge2)
            {
                int result = -1;
                if ((Points.Count > 0) && (edge1.indA < Points.Count) && (edge1.indB < Points.Count) && (edge2.indA < Points.Count) && (edge2.indB < Points.Count))
                {
                    if ((edge1.indA == edge2.indA) || (edge1.indA == edge2.indB))
                    {
                        if (Geometry.IsPointInLine(Points[edge2.indA].Point, Points[edge2.indB].Point, Points[edge1.indB].Point))
                        {
                            return edge1.indB;
                        }
                        else
                        {
                            return edge1.indA;
                        }
                    }
                    else if ((edge1.indB == edge2.indA) || (edge1.indB == edge2.indB))
                    {
                        if (Geometry.IsPointInLine(Points[edge2.indA].Point, Points[edge2.indB].Point, Points[edge1.indA].Point))
                        {
                            return edge1.indA;
                        }
                        else
                        {
                            return edge1.indB;
                        }
                    }
                    else
                    {
                        double minX1 = Points[edge1.indA].Point.X, minY1 = Points[edge1.indA].Point.Y;
                        double maxX1 = Points[edge1.indB].Point.X, maxY1 = Points[edge1.indB].Point.Y;
                        double minX2 = Points[edge2.indA].Point.X, minY2 = Points[edge2.indA].Point.Y;
                        double maxX2 = Points[edge2.indB].Point.X, maxY2 = Points[edge2.indB].Point.Y;
                        if (maxX1 < minX1) { minX1 = Points[edge1.indB].Point.X; maxX1 = Points[edge1.indA].Point.X; }
                        if (maxY1 < minY1) { minY1 = Points[edge1.indB].Point.Y; maxY1 = Points[edge1.indA].Point.Y; }
                        if (maxX2 < minX2) { minX2 = Points[edge2.indB].Point.X; maxX2 = Points[edge2.indA].Point.X; }
                        if (maxY2 < minY2) { minY2 = Points[edge2.indB].Point.Y; maxY2 = Points[edge2.indA].Point.Y; }

                        if ((minX1 < maxX2) && (minX2 < maxX1) && (minY1 < maxY2) && (minY2 < maxY1))
                        {
                            PointD IntPt = Geometry.GetIntersectLinesPoint(Points[edge1.indA].Point, Points[edge1.indB].Point, Points[edge2.indA].Point, Points[edge2.indB].Point);

                            if ((IntPt.X != Constant.DOUBLE_NAN) && (IntPt.Y != Constant.DOUBLE_NAN))
                            {
                                result = Points.GetPointIndex(IntPt);
                                if (result == -1) result = Points.AddPoint(IntPt);
                                return result;
                            }
                        }
                        if (Geometry.IsPointInLine(Points[edge1.indA].Point, Points[edge1.indB].Point, Points[edge2.indA].Point))
                        {
                            return edge2.indA;
                        }
                        else if (Geometry.IsPointInLine(Points[edge1.indA].Point, Points[edge1.indB].Point, Points[edge2.indB].Point))
                        {
                            return edge2.indB;
                        }
                        else if (Geometry.IsPointInLine(Points[edge2.indA].Point, Points[edge2.indB].Point, Points[edge1.indA].Point))
                        {
                            return edge1.indA;
                        }
                        else if (Geometry.IsPointInLine(Points[edge2.indA].Point, Points[edge2.indB].Point, Points[edge1.indB].Point))
                        {
                            return edge1.indB;
                        }
                    }
                }
                return result;
            }
            public void AddIntersectPoints(bool WithMainContours)
            {
                int i, j, ind, k, count1;
                bool result = false, res1 = false, res2 = false;
                for (i = 0; i < Edges.Count; i++)
                {
                    if ((WithMainContours && (Edges[i].Cntr != 1)) || (!WithMainContours && (Edges[i].Cntr != 0)))
                    {
                        for (j = 0; j < Edges.Count; j++)
                        {
                            if ((Edges[j].Cntr != 0) && (i != j))
                            {
                                ind = IntersectEdges(Edges[i], Edges[j]);
                                if (ind != -1)
                                {
                                    count1 = Edges.Count;
                                    res1 = InsertPoint(i, ind);
                                    if ((Edges.Count > count1) && (i <= j)) k = j + 1; else k = j;
                                    res2 = InsertPoint(k, ind);
                                    if (!result) result = res1 || res2;
                                }
                            }
                        }
                    }
                }

                if (result)
                {
                    for (i = 0; i < Points.Count; i++)
                    {
                        k = 0;
                        Points[i].EdgesInd.Clear();
                        for (j = 0; j < Edges.Count; j++)
                        {
                            if ((Edges[j].indA == i) || (Edges[j].indB == i))
                            {
                                Points[i].EdgesInd.Add(j);
                            }
                        }
                    }
                }
            }
            public void SortPointsAngle()
            {
                int i, j;
                for (i = 0; i < Points.Count; i++)
                {
                    PolyPoint pt = Points[i];
                    int k;
                    double angle, angle2;
                    if (pt.EdgesInd.Count > 2)
                    {
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            SetEdgeAngle(pt.EdgesInd[j]);
                        }
                        List<int> newEdges = new List<int>();
                        while (newEdges.Count < pt.EdgesInd.Count)
                        {
                            k = -1;
                            angle = Constant.DOUBLE_NAN;
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {
                                angle2 = Edges[pt.EdgesInd[j]].Angle;
                                if (Edges[pt.EdgesInd[j]].indA != i)
                                {
                                    if (angle2 > Math.PI) angle2 -= Math.PI; else angle2 += Math.PI;
                                }

                                if ((newEdges.IndexOf(pt.EdgesInd[j]) == -1) &&
                                    ((angle == Constant.DOUBLE_NAN) || (angle > angle2)))
                                {
                                    angle = angle2;
                                    k = j;
                                }
                            }
                            if (k != -1) newEdges.Add(pt.EdgesInd[k]);
                        }
                        pt.EdgesInd = newEdges;
                        Points[i] = pt;
                    }
                }
            }

            private void GetLeftRight(int PointIndex, int EdgeIndex, bool IsIdemCntr, out int Left, out int Right)
            {
                PolyPoint pt = Points[PointIndex];
                Edge edge = Edges[pt.EdgesInd[EdgeIndex]];
                int i, j, k = -1, lInd, rInd;
                Left = -2; Right = -2;

                lInd = -1;
                i = EdgeIndex + 1;
                if (i == pt.EdgesInd.Count) i = 0;
                while (i != EdgeIndex)
                {
                    if (((IsIdemCntr) && (Edges[pt.EdgesInd[i]].Cntr == edge.Cntr)) ||
                        ((!IsIdemCntr) && (Edges[pt.EdgesInd[i]].Cntr != edge.Cntr)))
                    {
                        lInd = pt.EdgesInd[i];
                        break;
                    }
                    i++;
                    if (i == pt.EdgesInd.Count) i = 0;
                }
                rInd = -1;
                i = EdgeIndex - 1;
                if (i < 0) i = pt.EdgesInd.Count - 1;
                while (i != EdgeIndex)
                {
                    if (((IsIdemCntr) && (Edges[pt.EdgesInd[i]].Cntr == edge.Cntr)) ||
                        ((!IsIdemCntr) && (Edges[pt.EdgesInd[i]].Cntr != edge.Cntr)))
                    {
                        rInd = pt.EdgesInd[i];
                        break;
                    }
                    i--;
                    if (i < 0) i = pt.EdgesInd.Count - 1;
                }
                if ((lInd > -1) && (rInd > -1))
                {
                    if ((Edges[lInd].indA == edge.indA) && (Edges[lInd].indB == edge.indB))
                    {
                        Left = Edges[lInd].Left;
                        Right = Edges[lInd].Right;
                    }
                    else if ((Edges[rInd].indA == edge.indA) && (Edges[rInd].indB == edge.indB))
                    {
                        Left = Edges[rInd].Left;
                        Right = Edges[rInd].Right;
                    }
                    else if ((Edges[lInd].indA == edge.indB) && (Edges[lInd].indB == edge.indA))
                    {
                        Left = Edges[lInd].Right;
                        Right = Edges[lInd].Left;
                    }
                    else if ((Edges[rInd].indA == edge.indB) && (Edges[rInd].indB == edge.indA))
                    {
                        Left = Edges[rInd].Right;
                        Right = Edges[rInd].Left;
                    }
                    else
                    {
                        if (Edges[lInd].indA == pt.Index) Left = Edges[lInd].Right; else Left = Edges[lInd].Left;
                        if (Edges[rInd].indA == pt.Index) Right = Edges[rInd].Left; else Right = Edges[rInd].Right;
                        if (edge.indB == pt.Index)
                        {
                            k = Left;
                            Left = Right;
                            Right = k;
                        }
                    }
                }
            }
            private List<int> GetEdgesTestEight(int index1, int index2, ref List<Contour> main)
            {
                int count, left, right;
                List<int> edgeList = new List<int>();
                if ((index1 < Edges.Count) && (index2 < Edges.Count))
                {
                    if (Edges[index1].indA == Edges[index2].indB)
                    {
                        int end = -1, lastIndex;
                        Edge edge;
                        PolyPoint pt;
                        int i = index1, j, k;
                        edge = Edges[index1];
                        lastIndex = index1;
                        while (edge.indB != Edges[index2].indB)
                        {
                            pt = Points[edge.indB];
                            if ((pt.EdgesInd.Count > 3) && (!pt.Used))
                            {
                                count = 0;
                                for (j = 0; j < pt.EdgesInd.Count; j++)
                                {
                                    if ((Edges[pt.EdgesInd[j]].Cntr != 0) && (!Edges[pt.EdgesInd[j]].Used))
                                    {
                                        count++;
                                    }
                                }
                                if (count == 4)
                                {
                                    k = -1;
                                    for (j = 0; j < pt.EdgesInd.Count; j++)
                                    {

                                        if ((Edges[pt.EdgesInd[j]].Cntr != 0) && (!Edges[pt.EdgesInd[j]].Used))
                                        {
                                            k = j;
                                            break;
                                        }
                                    }
                                    if (k != -1)
                                    {
                                        GetLeftRight(pt.Index, k, true, out left, out right);
                                        if (left == right) // ����� 8
                                        {
                                            TurnInsideOut(pt.Index, ref  main);
                                            edgeList.Clear();
                                            return edgeList;
                                        }
                                    }
                                }
                            }
                            edgeList.Add(lastIndex);

                            if (pt.EdgesInd.Count == 2)
                            {
                                if (Edges[pt.EdgesInd[0]].indA == pt.Index)
                                {
                                    edge = Edges[pt.EdgesInd[0]];
                                    lastIndex = pt.EdgesInd[0];
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[1]];
                                    lastIndex = pt.EdgesInd[1];
                                }
                            }
                            else
                            {
                                k = -1;
                                int lInd = -1, rInd = -1;
                                for (j = 0; j < pt.EdgesInd.Count; j++)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == edge.indA) && 
                                        (Edges[pt.EdgesInd[j]].indB == edge.indB))
                                    {
                                        k = j;
                                        break;
                                    }
                                }
                                j = k + 1;
                                lInd = -1;
                                if (j == pt.EdgesInd.Count) j = 0;
                                while (j != k)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == pt.Index) && 
                                        (Edges[pt.EdgesInd[j]].Cntr != 0) &&
                                        (edgeList.IndexOf(pt.EdgesInd[j]) == -1))
                                    {
                                        lInd = j;
                                        break;
                                    }
                                    j++;
                                    if (j == pt.EdgesInd.Count) j = 0;
                                }
                                j = k - 1;
                                rInd = -1;
                                if (j < 0) j = pt.EdgesInd.Count - 1;
                                while (j != k)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == pt.Index) && 
                                        (Edges[pt.EdgesInd[j]].Cntr != 0) &&
                                        (edgeList.IndexOf(pt.EdgesInd[j]) == -1))
                                    {
                                        rInd = j;
                                        break;
                                    }
                                    j--;
                                    if (j < 0) j = pt.EdgesInd.Count - 1;
                                }
                                if (Math.Abs(lInd - k) <= Math.Abs(rInd - k))
                                {
                                    edge = Edges[pt.EdgesInd[lInd]];
                                    lastIndex = pt.EdgesInd[lInd];
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[rInd]];
                                    lastIndex = pt.EdgesInd[rInd];
                                }
                            }
                        }
                        if (edge.indB == Edges[index2].indB) edgeList.Add(lastIndex);
                        if ((edge.indB == Edges[index2].indB) && (lastIndex != index2))
                        {
                            edgeList.Clear();
                        }
                    }
                }
                return edgeList;
            }
            private void TurnInsideOut(int PointIndex, ref List<Contour> main)
            {
                int i, j, k, lInd, rInd;
                int ind, ind2;
                Edge edge, edge2;
                bool IsNeedSort = false;

                PolyPoint pt;
                ArrayList list = new ArrayList();
                Contour cntr;
                List<int> edgeList = null;
                pt = Points[PointIndex];
                pt.Used = true;
                Points[PointIndex] = pt;
                for (j = 0; j < pt.EdgesInd.Count; j++)
                {
                    if ((!Edges[pt.EdgesInd[j]].Used) &&
                         (Edges[pt.EdgesInd[j]].Cntr != 0) &&
                         (Edges[pt.EdgesInd[j]].indA == pt.Index))
                    {
                        lInd = pt.EdgesInd[j];
                        rInd = -1;
                        k = j + 1;
                        if (k == pt.EdgesInd.Count) k = 0;
                        while (k != j)
                        {
                            if ((!Edges[pt.EdgesInd[k]].Used) && (Edges[pt.EdgesInd[k]].indB == pt.Index))
                            {
                                rInd = pt.EdgesInd[k];
                                edgeList = GetEdgesTestEight(lInd, rInd, ref main);
                                if (edgeList.Count > 0) break;
                            }
                            k++;
                            if (k == pt.EdgesInd.Count) k = 0;
                        }
                        if ((edgeList != null) && (edgeList.Count > 0))
                        {
                            edge = Edges[lInd]; edge.Used = true; //Edges[lInd] = edge;
                            edge = Edges[rInd]; edge.Used = true; //Edges[rInd] = edge;
                            list.Add(edgeList);
                        }
                    }
                }
                if (list.Count > 0)
                {
                    for (j = 0; j < list.Count; j++)
                    {
                        edgeList = (List<int>)list[j];
                        cntr = GetContourByEdgeList(edgeList);
                        if (!cntr.GetClockWiseByX(true))
                        {
                            for (lInd = 0, rInd = edgeList.Count - 1; lInd <= rInd; lInd++, rInd--)
                            {
                                ind = Points[Edges[edgeList[lInd]].indA].EdgesInd.IndexOf(edgeList[lInd]);
                                ind2 = Points[Edges[edgeList[lInd]].indA].EdgesInd.IndexOf(edgeList[rInd]);
                                if ((ind != -1) && (ind2 == -1))
                                {
                                    Points[Edges[edgeList[lInd]].indA].EdgesInd[ind] = edgeList[rInd];
                                }
                                ind = Points[Edges[edgeList[lInd]].indB].EdgesInd.IndexOf(edgeList[lInd]);
                                ind2 = Points[Edges[edgeList[lInd]].indB].EdgesInd.IndexOf(edgeList[rInd]);
                                if ((ind != -1) && (ind2 == -1))
                                {
                                    Points[Edges[edgeList[lInd]].indB].EdgesInd[ind] = edgeList[rInd];
                                }

                                ind = Points[Edges[edgeList[rInd]].indA].EdgesInd.IndexOf(edgeList[rInd]);
                                ind2 = Points[Edges[edgeList[rInd]].indA].EdgesInd.IndexOf(edgeList[lInd]);
                                if ((ind != -1) && (ind2 == -1))
                                {
                                    Points[Edges[edgeList[rInd]].indA].EdgesInd[ind] = edgeList[lInd];
                                }
                                ind = Points[Edges[edgeList[rInd]].indB].EdgesInd.IndexOf(edgeList[rInd]);
                                ind2 = Points[Edges[edgeList[rInd]].indB].EdgesInd.IndexOf(edgeList[lInd]);
                                if ((ind != -1) && (ind2 == -1))
                                {
                                    Points[Edges[edgeList[rInd]].indB].EdgesInd[ind] = edgeList[lInd];
                                }

                                ind = Edges[edgeList[lInd]].indA;
                                ind2 = Edges[edgeList[lInd]].indB;


                                Edges[edgeList[lInd]].indA = Edges[edgeList[rInd]].indB;
                                Edges[edgeList[lInd]].indB = Edges[edgeList[rInd]].indA;

                                Edges[edgeList[rInd]].indA = ind2;
                                Edges[edgeList[rInd]].indB = ind;

                                //edge2.indA = Edges[edgeList[lInd]].indB;
                                //edge2.indB = Edges[edgeList[lInd]].indA;
                                //Edges[edgeList[lInd]] = edge;
                                //Edges[edgeList[rInd]] = edge2;
                                IsNeedSort = true;
                            }
                        }
                    }
                }
                for (j = 0; j < list.Count; j++) ((List<int>)list[j]).Clear();
                list.Clear();
                k = -1;
                if (main == null) main = GetMainContours();
                for (j = 0; j < main.Count; j++)
                {
                    if (main[j].GetClockWiseByX(true) &&
                        main[j].PointBodyEntry(pt.Point.X, pt.Point.Y))
                    {
                        k = 0;
                        break;
                    }
                }
                for (j = 0; j < pt.EdgesInd.Count; j++)
                {
                    edge = Edges[pt.EdgesInd[j]];
                    if (k == 0)
                    {
                        if (edge.Left == -2) edge.Left = 0; else edge.Left = -1;
                        if (edge.Right == -2) edge.Right = 0; else edge.Right = -1;
                    }
                    edge.Used = false;
                    //Edges[pt.EdgesInd[j]] = edge;
                }
                pt.Used = false;
                Points[pt.Index] = pt;
                if (IsNeedSort) SortPointsAngle();
            }
            public void SetEdgesMarks()
            {
                int LastCntrIndex = GetNextCntrNumber() - 1;
                if (LastCntrIndex < 0) LastCntrIndex = 0;
                
                PolyPoint pt;
                int i, j, k, count;
                int left, right, next;
                Edge edge, edge2;

                #region ��������� ����������� ������ �� ���������������
                int ind, ind2;
                for (i = 0; i < Points.Count; i++)
                {
                    pt = Points[i];
                    if(pt.EdgesInd.Count > 3)
                    {
                        count = 0;
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            if ((Edges[pt.EdgesInd[j]].Cntr != 0) && (!Edges[pt.EdgesInd[j]].Used))
                            {
                                count++;
                            }
                        }
                        if (count == 4)
                        {
                            k = -1;
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {

                                if ((Edges[pt.EdgesInd[j]].Cntr != 0) && (!Edges[pt.EdgesInd[j]].Used))
                                {
                                    k = j;
                                    break;
                                }
                            }
                            if (k != -1)
                            {
                                GetLeftRight(i, k, true, out left, out right);
                                if (left == right) // ����� 8
                                {
                                    List<Contour> main = null;
                                    TurnInsideOut(pt.Index, ref  main);
                                }
                            }
                        }
                    }
                }
                #endregion

                #region �������� ��� ����� � ������ ����������� ���������������
                for (i = 0; i < Points.Count; i++)
                {
                    pt = Points[i];
                    if (pt.EdgesInd.Count > 2)
                    {
                        count = 0;
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            if ((Edges[pt.EdgesInd[j]].Cntr == Edges[pt.EdgesInd[0]].Cntr) ||
                                (Edges[pt.EdgesInd[j]].Cntr == -1))
                            {
                                count++;
                            }
                        }
                        if (count != pt.EdgesInd.Count)
                        {
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {
                                edge = Edges[pt.EdgesInd[j]];
                                if (!edge.Used)
                                {
                                    if (edge.Cntr != -1)
                                    {
                                        GetLeftRight(i, j, false, out left, out right);
                                        if ((left != -2) && (left != edge.Cntr))
                                        {
                                            if (edge.Left == -2)
                                            {
                                                edge.Left = left;
                                            }
                                            else if (left != edge.Left)
                                            {
                                                edge.Left = -1;
                                            }
                                        }
                                        if ((right != -2) && (right != edge.Cntr))
                                        {
                                            if (edge.Right == -2)
                                            {
                                                edge.Right = right;
                                            }
                                            else if (right != edge.Right)
                                            {
                                                edge.Right = -1;
                                            }
                                        }
                                    }
                                    edge.Used = true;
                                    //Edges[pt.EdgesInd[j]] = edge;
                                }
                            }
                        }
                        count = 0;
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            if (Edges[pt.EdgesInd[j]].Cntr == 1) count++;
                        }
                        if (count > 2)
                        {
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {
                                edge = Edges[pt.EdgesInd[j]];
                                if ((edge.Cntr == 1) && (edge.Left == -2))
                                {
                                    for (k = 0; k < pt.EdgesInd.Count; k++)
                                    {
                                        edge2 = Edges[pt.EdgesInd[k]];
                                        if ((j != k) && (edge2.Cntr == 1) && 
                                            (edge.indB == edge2.indA) && (edge.indA == edge2.indB))
                                        {
                                            edge.Left = 1;
                                            //Edges[pt.EdgesInd[j]] = edge;
                                            edge2.Left = 1;
                                            //Edges[pt.EdgesInd[k]] = edge2;
                                            break;
                                        }
                                    }

 
                                }
                             }
                        }
                   }
                }
                #endregion

                #region �������� ����� �� � ������ �����������
                next = -1;
                for (i = 0; i < Points.Count; i++)
                {
                    pt = Points[i];
                    if (pt.EdgesInd.Count == 2)
                    {
                        if (Edges[pt.EdgesInd[0]].indA == i) edge = Edges[pt.EdgesInd[0]]; else edge = Edges[pt.EdgesInd[1]];
                        if (!edge.Used)
                        {
                            // ������� ����� � ������ ������������
                            while ((!edge.Used) && (edge.indB != i) && (Points[edge.indB].EdgesInd.Count == 2))
                            {
                                if (Edges[Points[edge.indB].EdgesInd[0]].indA == edge.indB)
                                {
                                    edge = Edges[Points[edge.indB].EdgesInd[0]];
                                }
                                else
                                {
                                    edge = Edges[Points[edge.indB].EdgesInd[1]];
                                }
                            }
                            if (edge.Used)
                            {
                                left = edge.Left; right = edge.Right;

                                while (edge.indB != i)
                                {
                                    if (Points[edge.indA].EdgesInd.Count > 2)
                                    {
                                        count = 0;
                                        for (j = 0; j < Points[edge.indA].EdgesInd.Count; j++)
                                        {
                                            if (Edges[Points[edge.indA].EdgesInd[j]].Cntr == 1)
                                            {
                                                count++;
                                            }
                                        }
                                        if (count == Points[edge.indA].EdgesInd.Count)
                                        {
                                            for (j = 0; j < Points[edge.indA].EdgesInd.Count; j++)
                                            {
                                                edge = Edges[Points[edge.indA].EdgesInd[j]];
                                                edge.Left = left;
                                                edge.Right = right;
                                                //Edges[Points[edge.indA].EdgesInd[j]] = edge;
                                            } 
                                        }
                                        break;
                                    }
                                    if (Edges[Points[edge.indA].EdgesInd[0]].indB == edge.indA)
                                    {
                                        k = Points[edge.indA].EdgesInd[0];
                                    }
                                    else
                                    {
                                        k = Points[edge.indA].EdgesInd[1];
                                    }
                                    edge = Edges[k];
                                    if (edge.Used) break;
                                    edge.Used = true;
                                    edge.Left = left;
                                    edge.Right = right;
                                    //Edges[k] = edge;
                                }
                            }
                            else if (edge.indB == i)
                            {
                                i = edge.indA;
                            }
                        }
                    }
                    else if (pt.EdgesInd.Count > 2)
                    {
                        count = 0;
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            if (Edges[pt.EdgesInd[j]].Cntr == Edges[pt.EdgesInd[0]].Cntr) count++;
                        }
                        if (count == pt.EdgesInd.Count)
                        {
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {
                                edge = Edges[pt.EdgesInd[j]];
                                if (!edge.Used)
                                {
                                    if (edge.indB == i)
                                    {
                                        // ������� ����� � ������ ������������
                                        while ((!edge.Used) && (edge.indA != i) && (Points[edge.indA].EdgesInd.Count == 2))
                                        {
                                            if (Edges[Points[edge.indA].EdgesInd[0]].indB == edge.indA) edge = Edges[Points[edge.indA].EdgesInd[0]];
                                            else edge = Edges[Points[edge.indA].EdgesInd[1]];
                                        }
                                        if (edge.Used)
                                        {
                                            left = edge.Left; right = edge.Right;

                                            while (edge.indA != i)
                                            {
                                                if (Points[edge.indB].EdgesInd.Count > 2) break;
                                                if (Edges[Points[edge.indB].EdgesInd[0]].indA == edge.indB) 
                                                    k = Points[edge.indB].EdgesInd[0]; else k = Points[edge.indB].EdgesInd[1];
                                                edge = Edges[k];
                                                edge.Used = true;
                                                edge.Left = left;
                                                edge.Right = right;
                                                //Edges[k] = edge;
                                            }
                                        }
                                    }
                                    else if (edge.indA == i)
                                    {
                                        // ������� ����� � ������ ������������
                                        while ((!edge.Used) && (edge.indB != i) && (Points[edge.indB].EdgesInd.Count == 2))
                                        {
                                            if (Edges[Points[edge.indB].EdgesInd[0]].indA == edge.indB) edge = Edges[Points[edge.indB].EdgesInd[0]];
                                            else edge = Edges[Points[edge.indB].EdgesInd[1]];
                                        }
                                        if (edge.Used)
                                        {
                                            left = edge.Left; right = edge.Right;

                                            while (edge.indB != i)
                                            {
                                                if (Points[edge.indA].EdgesInd.Count > 2) break;
                                                if (Edges[Points[edge.indA].EdgesInd[0]].indB == edge.indA) k = Points[edge.indA].EdgesInd[0]; else k = Points[edge.indA].EdgesInd[1];
                                                edge = Edges[k];
                                                edge.Used = true;
                                                edge.Left = left;
                                                edge.Right = right;
                                                //Edges[k] = edge;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region ����� �������� ������� ������ ���������
                k = -1;
                for (i = 0; i < Edges.Count; i++)
                {
                    if (!Edges[i].Used)
                    {
                        k = i;
                        break;
                    }
                }
                if (k != -1)
                {
                    bool find = true;
                    bool clockWise = true;
                    bool inside = false;
                    int countOut = 0;
                    List<Contour> Main = GetMainContours();
                    Contour Added = GetAddedContour();
                    if (Added != null)
                    {
                        if ((!Added._Fantom) && (Main.Count  > 0))
                        {
                            count = 0;
                            countOut = 0;
                            inside = false;
                            find = false;
                            for (i = 0; i < Main.Count; i++)
                            {
                                clockWise = Main[i].GetClockWiseByX(true);
                                inside = Main[i].PointBodyEntry(Added._points[0].X, Added._points[0].Y);

                                if (clockWise && inside)
                                {
                                    for (j = 0; j < Main.Count; j++)
                                    {
                                        if ((j != i) && 
                                            (!Main[j].GetClockWiseByX(true)) && 
                                            (Main[i].PointBodyEntry(Main[j]._points[0].X, Main[j]._points[0].Y)))
                                        {
                                            count++;
                                            if (!Main[j].PointBodyEntry(Added._points[0].X, Added._points[0].Y))
                                            {
                                                countOut++;
                                            }
                                        }
                                    }
                                    find = true;
                                    break;
                                }
                            }
                            if ((find) && (count == countOut))
                            {
                                clockWise = Added.GetClockWiseByX(true);
                                for (i = Added.StratumCode; i <= Added._OilFieldCode; i++)
                                {
                                    edge = Edges[i];
                                    edge.Used = true;
                                    if (edge.Cntr != -1)
                                    {
                                        if (clockWise)
                                        {
                                            if (edge.Left == -2) edge.Left = 0; else edge.Left = -1;
                                            if (edge.Right == -2) edge.Right = 0; else edge.Right = -1;
                                        }
                                        else
                                        {
                                            if (edge.Right == -2) edge.Right = 0; else edge.Right = -1;
                                        }
                                    }
                                }
                            }
                        }
                        clockWise = Added.GetClockWiseByX(true);
                        for (i = 0; i < Main.Count; i++)
                        {
                            if (!Main[i]._Fantom)
                            {
                                inside = Added.PointBodyEntry(Main[i]._points[0].X, Main[i]._points[0].Y);
                                if ((clockWise && inside) || (!clockWise && !inside))
                                {
                                    clockWise = Added.GetClockWiseByX(true);
                                    for (j = Main[i].StratumCode; j <= Main[i]._OilFieldCode; j++)
                                    {
                                        edge = Edges[j];
                                        edge.Used = true;
                                        if (edge.Cntr != -1)
                                        {
                                            if (clockWise)
                                            {
                                                if (edge.Left == -2) edge.Left =1; else edge.Left = -1;
                                                if (edge.Right == -2) edge.Right = 1; else edge.Right = -1;
                                            }
                                            else
                                            {
                                                if (edge.Right == -2) edge.Right = 1; else edge.Right = -1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }

            public int GetNextCntrNumber()
            {
                int maxCntr = -1;
                for (int i = 0; i < Edges.Count; i++)
                {
                    if (Edges[i].Cntr > maxCntr) maxCntr = Edges[i].Cntr;
                }
                return maxCntr + 1;
            }
            public void RemoveAt(int index)
            {
                if ((index > -1) && (index < Edges.Count))
                {
                    Edges.RemoveAt(index);
                }
            }
            public void Clear()
            {
                Points.Clear();
                Edges.Clear();
            }

            private List<Contour> GetMainContours()
            {
                List<Contour> cntrList = new List<Contour>();
                Contour cntr = null;
                int start = -1;
                for (int i = 0; i < Edges.Count; i++)
                {
                    if (((Edges[i].Cntr == 0) || ((Edges[i].Cntr == -1))) && (Edges[i].indB != start))
                    {
                        if (cntr == null)
                        {
                            cntr = new Contour();
                            cntr._Fantom = false;
                            cntr._Closed = true;
                            cntr._points = new PointsXY(3);
                            cntrList.Add(cntr);
                        }
                        else
                        {
                            cntr._OilFieldCode = i;
                        }
                        if (cntr._points.Count == 0)
                        {
                            cntr.StratumCode = i;
                            start = Edges[i].indA;
                            cntr.Add(Points[Edges[i].indA].Point.X, Points[Edges[i].indA].Point.Y);
                        }
                        cntr.Add(Points[Edges[i].indB].Point.X, Points[Edges[i].indB].Point.Y);
                        if (Edges[i].Used) cntr._Fantom = true;
                    }
                    else
                    {
                        if (Edges[i].indB == start) cntr._OilFieldCode = i;
                        start = -1;
                        cntr = null;
                    }
                }
                return cntrList;
            }
            private Contour GetAddedContour()
            {
                Contour cntr = null;
                int start = -1;
                for (int i = 0; i < Edges.Count; i++)
                {
                    if ((Edges[i].Cntr == 1) || (Edges[i].Cntr == -1))
                    {
                        if (cntr != null) cntr._OilFieldCode = i;
                        if (Edges[i].indB == start) break;
                        if (cntr == null)
                        {
                            cntr = new Contour();
                            cntr._points = new PointsXY(3);
                            cntr._Fantom = false;
                            cntr._Closed = true;
                        }
                        if (cntr._points.Count == 0)
                        {
                            cntr.StratumCode = i;
                            start = Edges[i].indA;
                            cntr.Add(Points[Edges[i].indA].Point.X, Points[Edges[i].indA].Point.Y);
                        }
                        cntr.Add(Points[Edges[i].indB].Point.X, Points[Edges[i].indB].Point.Y);
                        if (Edges[i].Used) cntr._Fantom = true;
                    }
                    else if (start != -1)
                    {
                        break;
                    }
                }
                return cntr;
            }
            private List<int> GetEdgesByIndex(int index1, int index2)
            {
                List<int> edgeList = new List<int>();
                if ((index1 < Edges.Count) && (index2 < Edges.Count))
                {
                    if (Edges[index1].indA == Edges[index2].indB)
                    {
                        int end = -1, lastIndex;
                        Edge edge;
                        PolyPoint pt;
                        int i = index1, j, k;
                        edge = Edges[index1];
                        lastIndex = index1;
                        while (edge.indB != Edges[index2].indB)
                        {
                            pt = Points[edge.indB];
                            edgeList.Add(lastIndex);

                            if (pt.EdgesInd.Count == 2)
                            {
                                if (Edges[pt.EdgesInd[0]].indA == pt.Index)
                                {
                                    edge = Edges[pt.EdgesInd[0]];
                                    lastIndex = pt.EdgesInd[0];
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[1]];
                                    lastIndex = pt.EdgesInd[1];
                                }
                            }
                            else
                            {
                                k = -1;
                                int lInd = -1, rInd = -1;
                                for (j = 0; j < pt.EdgesInd.Count; j++)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == edge.indA) && (Edges[pt.EdgesInd[j]].indB == edge.indB))
                                    {
                                        k = j;
                                        break;
                                    }
                                }
                                j = k + 1;
                                lInd = -1;
                                if (j == pt.EdgesInd.Count) j = 0;
                                while (j != k)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == pt.Index) && (Edges[pt.EdgesInd[j]].Cntr == 1))
                                    {
                                        lInd = j;
                                        break;
                                    }
                                    j++;
                                    if (j == pt.EdgesInd.Count) j = 0;
                                }
                                j = k - 1;
                                rInd = -1;
                                if (j < 0) j = pt.EdgesInd.Count - 1;
                                while (j != k)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == pt.Index) && (Edges[pt.EdgesInd[j]].Cntr == 1))
                                    {
                                        rInd = j;
                                        break;
                                    }
                                    j--;
                                    if (j < 0) j = pt.EdgesInd.Count - 1;
                                }
                                if (Math.Abs(lInd - k) <= Math.Abs(rInd - k))
                                {
                                    edge = Edges[pt.EdgesInd[lInd]];
                                    lastIndex = pt.EdgesInd[lInd];
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[rInd]];
                                    lastIndex = pt.EdgesInd[rInd];
                                }
                            }
                        }
                        if (edge.indB == Edges[index2].indB) edgeList.Add(lastIndex);
                        if ((edge.indB == Edges[index2].indB) && (lastIndex != index2))
                        {
                            edgeList.Clear();
                        }
                    }
                }
                return edgeList;
            }
            private Contour GetContourByEdgeList(List<int> edgeList)
            {
                Contour cntr = new Contour();
                cntr._points = new PointsXY(edgeList.Count);
                cntr._points.Add(Points[Edges[edgeList[0]].indA].Point.X, Points[Edges[edgeList[0]].indA].Point.Y);
                for (int i = 0; i < edgeList.Count - 1; i++)
                {
                    cntr._points.Add(Points[Edges[edgeList[i]].indB].Point.X, Points[Edges[edgeList[i]].indB].Point.Y);
                }
                return cntr;
            }
        }

        public Polygon()
        {
            Points = new PointsCollection(16);
            Edges = new EdgesCollection(Points, 16);
            ResultReady = false;
        }
        public void Clear()
        {
            Points.Clear();
            Edges.Clear();
            ResultReady = false;
        }

        public void AddContour(Contour contour)
        {
            if ((contour._points != null) && (contour._points.Count > 2) && (contour._Closed))
            {
                int i, j;
                int ind, nextInd;
                //PolyPoint pt;
                Edge edge;
                bool clockWise = contour.GetClockWiseByX(true);
                
                if (Points.Count == 0) // ������ ����������� ������
                {
                    nextInd = -1;
                    for(i = 0; i < contour._points.Count; i++)
                    {
                        ind = nextInd;
                        if (nextInd == -1)
                        {
                            ind = Points.GetPointIndex(contour._points[i]);
                            if (ind == -1)
                            {
                                ind = Points.AddPoint(contour._points[i]);
                            }
                        }
                        
                        j = i + 1;
                        if(j == contour._points.Count) j = 0;
                        nextInd = Points.GetPointIndex(contour._points[j]);
                        if (nextInd == -1)
                        {
                            nextInd = Points.AddPoint(contour._points[j]);
                        }
                        if (ind != nextInd)
                        {
                            Edges.AddEdge(1, ind, nextInd, clockWise);
                        }
                    }
                    Edges.AddIntersectPoints(false); // ������� ����� ����������� ��������
                    Edges.SortPointsAngle();
                    Edges.SetEdgesMarks();
                    for (i = 0; i < Edges.Count; i++)
                    {
                        edge = Edges[i];
                        if (edge.Left == edge.Cntr) edge.Left = 0;
                        if (edge.Right == edge.Cntr) edge.Right = 0;
                        edge.Cntr = 0;
                    }
                    ResultReady = true;
                }
                else // ������������ ���������
                {
                    contour.SetMinMax();
                    List<Contour> main = this.GetContours();

                    #region ��������� ����� ������� �������
                    int numCntr = Edges.GetNextCntrNumber();
                    nextInd = -1;
                    for (i = 0; i < contour._points.Count; i++)
                    {
                        ind = nextInd;
                        if (nextInd == -1)
                        {
                            ind = Points.GetPointIndex(contour._points[i]);
                            if (ind == -1)
                            {
                                ind = Points.AddPoint(contour._points[i]);
                            }
                        }

                        j = i + 1;
                        if (j == contour._points.Count) j = 0;
                        nextInd = Points.GetPointIndex(contour._points[j]);
                        if (nextInd == -1)
                        {
                            nextInd = Points.AddPoint(contour._points[j]);
                        }
                        if (ind != nextInd)
                        {
                            Edges.AddEdge(numCntr, ind, nextInd, clockWise);
                        }
                    }
                    #endregion

                    bool inter = false;
                    for (j = 0; j < main.Count; j++)
                    {
                        main[j].SetMinMax();
                        if (main[j].IntersectBounds(contour) || contour.IntersectBounds(main[j]))
                        {
                            inter = true;
                            break;
                        }
                    }
                    if(inter) Edges.AddIntersectPoints(true);
                    Edges.AddIntersectPoints(false);
                    Edges.SortPointsAngle();
                    Edges.SetEdgesMarks();
                    ResultReady = false;
                }
            }
        }
        public void AddContour(List<C2DObject> CntrList)
        {
            if (CntrList.Count > 0)
            {
                int i, j, k;
                int ind, nextInd;
                PolyPoint pt;
                Edge edge;
                Contour contour;
                bool clockWise;

                if (Points.Count == 0) // ������ ����������� ������
                {
                    for (k = 0; k < CntrList.Count; k++)
                    {
                        contour = (Contour)CntrList[k];
                        clockWise = contour.GetClockWiseByX(true);
                        if ((contour._points != null) && (contour._points.Count > 2) && (contour._Closed))
                        {
                            nextInd = -1;
                            for (i = 0; i < contour._points.Count; i++)
                            {
                                ind = nextInd;
                                if (nextInd == -1)
                                {
                                    ind = Points.GetPointIndex(contour._points[i]);
                                    if (ind == -1)
                                    {
                                        ind = Points.AddPoint(contour._points[i]);
                                    }
                                }

                                j = i + 1;
                                if (j == contour._points.Count) j = 0;
                                nextInd = Points.GetPointIndex(contour._points[j]);
                                if (nextInd == -1)
                                {
                                    nextInd = Points.AddPoint(contour._points[j]);
                                }
                                if (ind != nextInd)
                                {
                                    Edges.AddEdge(1, ind, nextInd, clockWise);
                                }
                            }
                        }
                    }
                    Edges.AddIntersectPoints(false); // ������� ����� ����������� ��������
                    Edges.SortPointsAngle();
                    Edges.SetEdgesMarks();
                    for (i = 0; i < Edges.Count; i++)
                    {
                        edge = Edges[i];
                        if (edge.Left == edge.Cntr) edge.Left = 0;
                        if (edge.Right == edge.Cntr) edge.Right = 0;
                        edge.Cntr = 0;
                    }
                    ResultReady = true;
                }
                else // ������������ ���������
                {
                    List<Contour> main = this.GetContours();
                    #region ��������� ����� ������� �������
                    int numCntr = Edges.GetNextCntrNumber();
                    bool inter = false;
                    for (k = 0; k < CntrList.Count; k++)
                    {
                        contour = (Contour)CntrList[k];
                        contour.SetMinMax();
                        clockWise = contour.GetClockWiseByX(true);
                        //contour.GetClockWiseByX(true);
                        if ((contour._points != null) && (contour._points.Count > 2) && (contour._Closed))
                        {
                            nextInd = -1;
                            for (i = 0; i < contour._points.Count; i++)
                            {
                                ind = nextInd;
                                if (nextInd == -1)
                                {
                                    ind = Points.GetPointIndex(contour._points[i]);
                                    if (ind == -1)
                                    {
                                        ind = Points.AddPoint(contour._points[i]);
                                    }
                                }

                                j = i + 1;
                                if (j == contour._points.Count) j = 0;
                                nextInd = Points.GetPointIndex(contour._points[j]);
                                if (nextInd == -1)
                                {
                                    nextInd = Points.AddPoint(contour._points[j]);
                                }
                                if (ind != nextInd)
                                {
                                    Edges.AddEdge(numCntr, ind, nextInd, clockWise);
                                }
                            }
                            inter = false;
                            for (j = 0; j < main.Count; j++)
                            {
                                main[j].SetMinMax();
                                if (main[j].IntersectBounds(contour) || contour.IntersectBounds(main[j]))
                                {
                                    inter = true;
                                    break;
                                }
                            }
                            if (inter) Edges.AddIntersectPoints(true);
                        }
                    }
                    #endregion
                    Edges.AddIntersectPoints(false);
                    Edges.SortPointsAngle();
                    Edges.SetEdgesMarks();
                    ResultReady = false;
                }
            }
        }

        #region �������� ��� ���������
        void SelectEdgesByOperation(ContoursOperation Operation)
        {
            int i;
            for (i = 0; i < Edges.Count; i++)
            {
                Edges[i].Selected = false;
                Edges[i].Used = false;
            }
            for (i = 0; i < Edges.Count; i++)
            {
                switch (Operation)
                {
                    case ContoursOperation.Union:
                        if ((Edges[i].Left == -2) && (Edges[i].Right != -2))
                        {
                            Edges[i].Selected = true;
                        }
                        break;
                    case ContoursOperation.Difference:
                        if ((Edges[i].Left == -2) && (Edges[i].Right == 0))
                        {
                            Edges[i].Selected = true;
                        }
                        else if ((Edges[i].Left == 0) && ((Edges[i].Right == -1) || (Edges[i].Right == 1)))
                        {
                            Edges[i].Invert();
                            Edges[i].Selected = true;
                        }
                        break;
                    case ContoursOperation.Crossing:
                        if (Edges[i].Right == -1)
                        {
                            Edges[i].Selected = true;
                        }
                        break;
                    case ContoursOperation.SymmetricDifference:
                        if ((Edges[i].Left == -2) && (Edges[i].Right == 0))
                        {
                            Edges[i].Selected = true;
                        }
                        if ((Edges[i].Left == -2) && (Edges[i].Right == 1))
                        {
                            Edges[i].Selected = true;
                        }
                        else if ((Edges[i].Left == 0) && ((Edges[i].Right == -1) || (Edges[i].Right == 1)))
                        {
                            Edges[i].Invert();
                            Edges[i].Selected = true;
                        }
                        else if ((Edges[i].Left == 1) && ((Edges[i].Right == -1) || (Edges[i].Right == 0)))
                        {
                            Edges[i].Invert();
                            Edges[i].Selected = true;
                        }
                        break;
                }
            }
        }
        bool TestEdgeByOperation(Edge Edge1, Edge Edge2, int PtIndex, ContoursOperation Operation)
        {
            if ((Edge2.Selected) && (!Edge2.Used))
            {
                switch (Operation)
                {
                    case ContoursOperation.Union:
                        return (Edge2.indA == PtIndex);
                    case ContoursOperation.Crossing:
                        return (Edge2.indA == PtIndex);
                    case ContoursOperation.Difference:
                        return ((Edge1.Cntr == Edge2.Cntr) && (Edge2.indA == PtIndex));
                    case ContoursOperation.SymmetricDifference:
                        return ((Edge1.Cntr == Edge2.Cntr) && (Edge2.indA == PtIndex));
                }
            }
            return false;
        }
        public void OperateByContours(ContoursOperation Operation)
        {
            Edge edge;
            int i, j, k;
            SelectEdgesByOperation(Operation);

            PointsCollection newPoints = new PointsCollection(this.Points.Count);
            EdgesCollection newEdges = new EdgesCollection(newPoints, Edges.Count);

            bool exit = false;

            int ind, start = -1;
            int lInd, rInd;
            PolyPoint pt;
            while (!exit)
            {
                exit = true;
                for (i = 0; i < Edges.Count; i++)
                {
                    if ((Edges[i].Selected) && (!Edges[i].Used))
                    {
                        start = -1;
                        edge = Edges[i];
                        edge.Used = true;
                        Edges[i] = edge;
                        start = -1;
                        while (edge.indA != start)
                        {
                            if (start == -1) start = edge.indA;
                            edge.Left = -2;
                            edge.Right = 0;
                            //edge.Cntr = 0;

                            pt = Points[edge.indB];
                            newEdges.AddEdge(edge);
                            if (edge.indB == start) break;
                            if (!edge.Selected)
                            {
                                while ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA != start))
                                {
                                    newEdges.RemoveAt(newEdges.Count - 1);
                                }
                                if ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA == start))
                                {
                                    newEdges.RemoveAt(newEdges.Count - 1);
                                }
                                break;
                            }
                            if (pt.EdgesInd.Count == 2)
                            {
                                if (Edges[pt.EdgesInd[0]].indA == pt.Index)
                                {
                                    edge = Edges[pt.EdgesInd[0]];
                                    edge.Used = true;
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[1]];
                                    edge.Used = true;
                                }
                            }
                            else
                            {
                                k = -1;
                                for (j = 0; j < pt.EdgesInd.Count; j++)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == edge.indA) &&
                                        (Edges[pt.EdgesInd[j]].indB == edge.indB))
                                    {
                                        k = j;
                                        break;
                                    }
                                }
                                if (k == -1) break;

                                rInd = -1;
                                j = k + 1;
                                if (j == pt.EdgesInd.Count) j = 0;
                                while (j != k)
                                {
                                    if (TestEdgeByOperation(edge, Edges[pt.EdgesInd[j]], pt.Index, Operation))
                                    {
                                        rInd = j;
                                        break;
                                    }
                                    j++;
                                    if (j == pt.EdgesInd.Count) j = 0;
                                }
                                lInd = -1;
                                j = k - 1;
                                if (j < 0) j = pt.EdgesInd.Count - 1;
                                while (j != k)
                                {
                                    if (TestEdgeByOperation(edge, Edges[pt.EdgesInd[j]], pt.Index, Operation))
                                    {
                                        lInd = j;
                                        break;
                                    }
                                    j--;
                                    if (j < 0) j = pt.EdgesInd.Count - 1;
                                }
                                if ((lInd != -1) && (rInd != -1))
                                {
                                    if (Math.Abs(lInd - i) <= Math.Abs(rInd - i))
                                    {
                                        edge = Edges[pt.EdgesInd[lInd]];
                                        edge.Used = true;
                                    }
                                    else
                                    {
                                        edge = Edges[pt.EdgesInd[rInd]];
                                        edge.Used = true;
                                    }
                                }
                                else
                                {
                                    while ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA != start))
                                    {
                                        newEdges.RemoveAt(newEdges.Count - 1);
                                    }
                                    if ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA == start))
                                    {
                                        newEdges.RemoveAt(newEdges.Count - 1);
                                    }
                                    break;
                                }
                            }

                        }
                        exit = false;
                    }
                }
            }
            if (newEdges.Count > 0)
            {
                List<int> ptIndexes = new List<int>();
                for (i = 0; i < newEdges.Count; i++)
                {
                    if (ptIndexes.IndexOf(newEdges[i].indA) == -1)
                    {
                        ptIndexes.Add(newEdges[i].indA);
                    }
                }
                for (i = 0; i < ptIndexes.Count; i++)
                {
                    ind = newPoints.AddPoint(Points[ptIndexes[i]].Point);
                    pt = newPoints[ind];
                    for (j = 0; j < newEdges.Count; j++)
                    {
                        newEdges[j].Cntr = 0;
                        if ((newEdges[j].Used) && (newEdges[j].indA == ptIndexes[i]))
                        {
                            edge = newEdges[j];
                            edge.indA = ind;
                            edge.Used = false;
                            pt.EdgesInd.Add(j);
                        }
                        else if ((newEdges[j].Selected) && (newEdges[j].indB == ptIndexes[i]))
                        {
                            edge = newEdges[j];
                            edge.indB = ind;
                            edge.Selected = false;
                            pt.EdgesInd.Add(j);
                        }
                    }
                    newPoints[ind] = pt;
                }
            }
            newPoints.Trim();
            newEdges.Trim();
            this.Points = newPoints;
            this.Edges = newEdges;
            ResultReady = true; 
        }
        public void UnionContours()
        {
            //Edge edge;
            //int i, j, k;
            //for (i = 0; i < Edges.Count; i++)
            //{
            //    Edges[i].Selected = false;
            //    Edges[i].Used = false;
            //}
            //for (i = 0; i < Edges.Count; i++)
            //{
            //    if ((Edges[i].Left == -2) && (Edges[i].Right != -2))
            //    {
            //        Edges[i].Selected = true;
            //    }
            //}
            //PointsCollection newPoints = new PointsCollection(this.Points.Count);
            //EdgesCollection newEdges = new EdgesCollection(newPoints, Edges.Count);

            //bool exit = false;

            //int ind, start = -1;
            //int lInd, rInd;
            //PolyPoint pt;
            //while (!exit)
            //{
            //    exit = true;
            //    for (i = 0; i < Edges.Count; i++)
            //    {
            //        if ((Edges[i].Selected) && (!Edges[i].Used))
            //        {
            //            start = -1;
            //            edge = Edges[i];
            //            edge.Used = true;
            //            Edges[i] = edge;
            //            start = -1;
            //            while (edge.indA != start)
            //            {
            //                if (start == -1) start = edge.indA;
            //                edge.Left = -2;
            //                edge.Right = 0;
            //                edge.Cntr = 0;

            //                pt = Points[edge.indB];
            //                newEdges.AddEdge(edge);
            //                if (edge.indB == start) break;
            //                if (!edge.Selected)
            //                {
            //                    while ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA != start))
            //                    {
            //                        newEdges.RemoveAt(newEdges.Count - 1);
            //                    }
            //                    if ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA == start))
            //                    {
            //                        newEdges.RemoveAt(newEdges.Count - 1);
            //                    }
            //                    break;
            //                }
            //                if (pt.EdgesInd.Count == 2)
            //                {
            //                    if (Edges[pt.EdgesInd[0]].indA == pt.Index)
            //                    {
            //                        edge = Edges[pt.EdgesInd[0]];
            //                        edge.Used = true;
            //                    }
            //                    else
            //                    {
            //                        edge = Edges[pt.EdgesInd[1]];
            //                        edge.Used = true;
            //                    }
            //                }
            //                else
            //                {
            //                    k = -1;
            //                    for (j = 0; j < pt.EdgesInd.Count; j++)
            //                    {
            //                        if ((Edges[pt.EdgesInd[j]].indA == edge.indA) &&
            //                            (Edges[pt.EdgesInd[j]].indB == edge.indB))
            //                        {
            //                            k = j;
            //                            break;
            //                        }
            //                    }
            //                    if (k == -1) break;

            //                    rInd = -1;
            //                    j = k + 1;
            //                    if (j == pt.EdgesInd.Count) j = 0;
            //                    while (j != k)
            //                    {
            //                        if ((Edges[pt.EdgesInd[j]].Selected) &&
            //                            (!Edges[pt.EdgesInd[j]].Used) &&
            //                            (Edges[pt.EdgesInd[j]].indA == pt.Index))
            //                        {
            //                            rInd = j;
            //                            break;
            //                        }
            //                        j++;
            //                        if (j == pt.EdgesInd.Count) j = 0;
            //                    }
            //                    lInd = -1;
            //                    j = k - 1;
            //                    if (j < 0) j = pt.EdgesInd.Count - 1;
            //                    while (j != k)
            //                    {
            //                        if ((Edges[pt.EdgesInd[j]].Selected) &&
            //                            (!Edges[pt.EdgesInd[j]].Used) &&
            //                            (Edges[pt.EdgesInd[j]].indA == pt.Index))
            //                        {
            //                            lInd = j;
            //                            break;
            //                        }
            //                        j--;
            //                        if (j < 0) j = pt.EdgesInd.Count - 1;
            //                    }
            //                    if ((lInd != -1) && (rInd != -1))
            //                    {
            //                        if (Math.Abs(lInd - i) <= Math.Abs(rInd - i))
            //                        {
            //                            edge = Edges[pt.EdgesInd[lInd]];
            //                            edge.Used = true;
            //                        }
            //                        else
            //                        {
            //                            edge = Edges[pt.EdgesInd[rInd]];
            //                            edge.Used = true;
            //                        }
            //                    }
            //                    else
            //                    {
            //                        while ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA != start))
            //                        {
            //                            newEdges.RemoveAt(newEdges.Count - 1);
            //                        }
            //                        if ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA == start))
            //                        {
            //                            newEdges.RemoveAt(newEdges.Count - 1);
            //                        }
            //                        break;
            //                    }
            //                }

            //            }
            //            exit = false;
            //        }
            //    }
            //}
            //if (newEdges.Count > 0)
            //{
            //    List<int> ptIndexes = new List<int>();
            //    for (i = 0; i < newEdges.Count; i++)
            //    {
            //        if (ptIndexes.IndexOf(newEdges[i].indA) == -1)
            //        {
            //            ptIndexes.Add(newEdges[i].indA);
            //        }
            //    }
            //    for (i = 0; i < ptIndexes.Count; i++)
            //    {
            //        ind = newPoints.AddPoint(Points[ptIndexes[i]].Point);
            //        pt = newPoints[ind];
            //        for (j = 0; j < newEdges.Count; j++)
            //        {
            //            if ((newEdges[j].Used) && (newEdges[j].indA == ptIndexes[i]))
            //            {
            //                edge = newEdges[j];
            //                edge.indA = ind;
            //                edge.Used = false;
            //                pt.EdgesInd.Add(j);
            //            }
            //            else if ((newEdges[j].Selected) && (newEdges[j].indB == ptIndexes[i]))
            //            {
            //                edge = newEdges[j];
            //                edge.indB = ind;
            //                edge.Selected = false;
            //                pt.EdgesInd.Add(j);
            //            }
            //        }
            //        newPoints[ind] = pt;
            //    }
            //}
            //newPoints.Trim();
            //newEdges.Trim();
            //this.Points = newPoints;
            //this.Edges = newEdges;
            //ResultReady = true;
        }
        #endregion

        public List<Contour> GetContours()
        {
            List<Contour> cntrList = new List<Contour>();
            if (ResultReady)
            {
                Contour cntr = null;
                int cntrInd = -1;
                int start;
                for (int i = 0; i < Edges.Count; i++)
                {
                    cntr = new Contour();
                    cntr._points = new PointsXY(3);
                    cntrList.Add(cntr);
                    start = Edges[i].indA;
                    cntr._points.Add(Points[Edges[i].indA].Point.X, Points[Edges[i].indA].Point.Y);
                    while ((i < Edges.Count) && (Edges[i].indB != start))
                    {
                        cntr._points.Add(Points[Edges[i].indB].Point.X, Points[Edges[i].indB].Point.Y);
                        i++;
                    }
                    cntr.SetMinMax();
                }
            }
            return cntrList;
        }
    }

    public sealed class Image : C2DObject 
    {
        public static int Version = 1;

        
        public double dx;
        public double dy;
        public float xDpi;
        public float yDpi;
        Bitmap _bitmap;
        
        public bool BitMapLoaded { get { if (_bitmap != null) return true; else return false; } }

        public void Dispose()
        {
            if(BitMapLoaded) _bitmap.Dispose();
        }
        public Image()
        {
            this.TypeID = Constant.BASE_OBJ_TYPES_ID.IMAGE;
            this.Visible = true;
            this.Selected = false;
            this.xDpi = 100;
            this.yDpi = 100;
            this.dx = 0;
            this.dy = 0;
            this.Name = string.Empty;
        }
        public Image(string aFileName) : this()
        {
            string fName = Path.GetFileName(aFileName);
            _bitmap = new Bitmap(aFileName);
        }
        public void SetResolution(float xDpi, float yDpi)
        {
            _bitmap.SetResolution(xDpi, yDpi);
        }

        // CACHE
        public void LoadFromCache(Project proj, BinaryReader br)
        {
            int ver = br.ReadInt32();
            if (ver == Version)
            {
                dx = br.ReadDouble();
                dy = br.ReadDouble();
                xDpi = br.ReadSingle();
                yDpi = br.ReadSingle();
                MemoryStream ms = new MemoryStream();
                ConvertEx.CopyStream(br.BaseStream, ms);
                ms.Seek(0, SeekOrigin.Begin);
                _bitmap = new Bitmap(ms);
            }
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(Version);
            bw.Write(dx);
            bw.Write(dy);
            bw.Write(xDpi);
            bw.Write(yDpi);
            MemoryStream ms = new MemoryStream();
            _bitmap.Save(ms, _bitmap.RawFormat);
            ms.Seek(0, SeekOrigin.Begin);
            ConvertEx.CopyStream(ms, bw.BaseStream);
            ms.Dispose();
        }
        public override void DrawObject(Graphics grfx)
        {
            if (BitMapLoaded) 
            {
                RectangleF outRect = new RectangleF();
                float xDpicurr, yDpicurr;
                outRect.X = 0; outRect.Y = 0;
                xDpicurr = (this.xDpi) / (float)(st_XUnitsInOnePixel); // / 1631.461442);
                yDpicurr = (this.yDpi) / (float)(st_YUnitsInOnePixel); // / 1631.461442);

                outRect.X = (float)(-st_BitMapRect.X + dx / st_XUnitsInOnePixel);
                outRect.Y = (float)(-st_BitMapRect.Y + dy / st_YUnitsInOnePixel);

                // x
                //if (rect.X < 0) 
                //{
                //    if (_bitmap.Width + rect.X < 0) return;
                //    else if (_bitmap.Width + rect.X < st_BMPRect.Width) rect.Width = _bitmap.Width + rect.X;
                //    else if (_bitmap.Width + rect.X >= st_BMPRect.Width) rect.Width = st_BMPRect.Width;
                //    outRect.X = 0;
                //    rect.X = -rect.X;
                //}
                //else 
                //{
                //    if (rect.X > st_BMPRect.Width) return;
                //    else if (_bitmap.Width + rect.X < st_BMPRect.Width) rect.Width = _bitmap.Width;
                //    else if (_bitmap.Width + rect.X >= st_BMPRect.Width) rect.Width = st_BMPRect.Width - rect.X;
                //    outRect.X = rect.X;
                //    rect.X = 0;
                //}

                //// y
                //if (rect.Y < 0) 
                //{
                //    if (_bitmap.Height + rect.Y < 0) return;
                //    else if (_bitmap.Height + rect.Y < st_BMPRect.Height) rect.Height = _bitmap.Height + rect.Y;
                //    else if (_bitmap.Height + rect.Y >= st_BMPRect.Height) rect.Height = st_BMPRect.Height;
                //    outRect.Y = 0;
                //    rect.Y = -rect.Y;
                //}
                //else 
                //{
                //    if (rect.Y > st_BMPRect.Height) return;
                //    else if (_bitmap.Height + rect.Y < st_BMPRect.Height) rect.Height = _bitmap.Height;
                //    else if (_bitmap.Height + rect.Y >= st_BMPRect.Height) rect.Height = st_BMPRect.Height - rect.Y;
                //    outRect.Y = rect.Y;
                //    rect.Y = 0;
                //}

                outRect.Width = (_bitmap.Width * xDpicurr);
                outRect.Height = (_bitmap.Height * yDpicurr);

                grfx.DrawImage(_bitmap, outRect);
            }
        }
    }

    public sealed class PVTWorkObject : C2DObject
    {
        public static int Version = 0;
        
        public string OilFieldName;
        public string OilfieldAreaName;
        public string AreaName;
        public PVTParamsItem Item;

        public bool LoadFromCache(StreamReader sr)
        {
            try
            {
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";
                char[] del = new char[] {';'};
                string[] parse;
                int version = -1;
                parse = sr.ReadLine().Split(del);
                if ((parse.Length > 1) && (parse[0] == "Version"))
                {
                    version = Convert.ToInt32(parse[1]);
                }
                if (version != Version) return false;
                Item = PVTParamsItem.Empty;
                while (!sr.EndOfStream)
                {
                    parse = sr.ReadLine().Split(del);
                    if (parse.Length > 1)
                    {
                        if (parse[0] != "Name") parse[1] = parse[1].Replace(',', '.');
                        switch (parse[0])
                        {
                            case "Name":
                                Name = parse[1];
                                break;
                            case "Oilfield":
                                OilFieldName = parse[1];
                                break;
                            case "OilfieldArea":
                                OilfieldAreaName = parse[1];
                                break;
                            case "Area":
                                AreaName = parse[1];
                                break;
                            case "PlastCode":
                                Item.StratumCode = Convert.ToInt32(parse[1]);
                                break;
                            case "OilVolumeFactor":
                                Item.OilVolumeFactor = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "OilDensity":
                                Item.OilDensity = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "OilViscosity":
                                Item.OilViscosity = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "OilInitialSaturation":
                                Item.OilInitialSaturation = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "WaterVolumeFactor":
                                Item.WaterVolumeFactor = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "WaterDensity":
                                Item.WaterDensity = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "WaterViscosity":
                                Item.WaterViscosity = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "DisplacementEfficiency":
                                Item.DisplacementEfficiency = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "Porosity":
                                Item.Porosity = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "InitialPressure":
                                Item.InitialPressure = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "SaturationPressure":
                                Item.SaturationPressure = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "GasFactor":
                                Item.GasFactor = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "Permeability":
                                Item.Permeability = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "GasInitialPressure":
                                Item.GasInitialPressure = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "GasPorosity":
                                Item.GasPorosity = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "GasDensityFreeGas":
                                Item.GasDensityFreeGas = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "GasDensitySoluteGas":
                                Item.GasDensitySoluteGas = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "GasInitialSaturation":
                                Item.GasInitialSaturation = Convert.ToDouble(parse[1], nfi);
                                break;
                            case "Comment":
                                Item.Comment = parse[1];
                                break;
                            case "KIN":
                                Item.KIN = Convert.ToDouble(parse[1], nfi);
                                break;
                        }
                    }
                }
                return true;
            }
            catch
            {
            }
            return false;
        }
        public void WriteToCache(StreamWriter sw)
        {
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";
            sw.WriteLine(string.Format(nfi, "Version;{0};", Version));
            sw.WriteLine(string.Format(nfi, "Name;{0};", Name));
            sw.WriteLine(string.Format(nfi, "Oilfield;{0};", OilFieldName));
            sw.WriteLine(string.Format(nfi, "OilfieldArea;{0};", OilfieldAreaName));
            sw.WriteLine(string.Format(nfi, "Area;{0};", AreaName));
            sw.WriteLine(string.Format(nfi, "PlastCode;{0};", Item.StratumCode));
            sw.WriteLine(string.Format(nfi, "OilVolumeFactor;{0};", Item.OilVolumeFactor));
            sw.WriteLine(string.Format(nfi, "OilDensity;{0};", Item.OilDensity));
            sw.WriteLine(string.Format(nfi, "OilViscosity;{0};", Item.OilViscosity));
            sw.WriteLine(string.Format(nfi, "OilInitialSaturation;{0};", Item.OilInitialSaturation));
            sw.WriteLine(string.Format(nfi, "WaterVolumeFactor;{0};", Item.WaterVolumeFactor));
            sw.WriteLine(string.Format(nfi, "WaterDensity;{0};", Item.WaterDensity));
            sw.WriteLine(string.Format(nfi, "WaterViscosity;{0};", Item.WaterViscosity));
            sw.WriteLine(string.Format(nfi, "DisplacementEfficiency;{0};", Item.DisplacementEfficiency));
            sw.WriteLine(string.Format(nfi, "Porosity;{0};", Item.Porosity));
            sw.WriteLine(string.Format(nfi, "InitialPressure;{0};", Item.InitialPressure));
            sw.WriteLine(string.Format(nfi, "SaturationPressure;{0};", Item.SaturationPressure));
            sw.WriteLine(string.Format(nfi, "GasFactor;{0};", Item.GasFactor));
            sw.WriteLine(string.Format(nfi, "Permeability;{0};", Item.Permeability));
            sw.WriteLine(string.Format(nfi, "GasInitialPressure;{0};", Item.GasInitialPressure));
            sw.WriteLine(string.Format(nfi, "GasPorosity;{0};", Item.GasPorosity));
            sw.WriteLine(string.Format(nfi, "GasDensityFreeGas;{0};", Item.GasDensityFreeGas));
            sw.WriteLine(string.Format(nfi, "GasDensitySoluteGas;{0};", Item.GasDensitySoluteGas));
            sw.WriteLine(string.Format(nfi, "GasInitialSaturation;{0};", Item.GasInitialSaturation));
            sw.WriteLine(string.Format(nfi, "KIN;{0};", Item.KIN));
            sw.WriteLine(string.Format(nfi, "Comment;{0};", Item.Comment));
            }
     }
    #endregion
}