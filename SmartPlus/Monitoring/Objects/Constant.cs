namespace SmartPlus
{ 
    public class Constant
    {
        public const double DOUBLE_NAN = 1.0e+307;

        public const string AppOldMessage = "���������� ��������. �������� ��� ����������� ������!";

        public enum BG_WORKER_WORK_TYPE : int 
        { 
            LOAD_OF_WELLLIST_FROM_SERVER,
            LOAD_OF_CONTOUR_FROM_SERVER,
            LOAD_OF_AREAS_FROM_SERVER,
            LOAD_OF_WELLPADS_FROM_SERVER,
            LOAD_OF_MER_FROM_SERVER,
            LOAD_OF_GIS_FROM_SERVER,
            LOAD_OF_PERF_FROM_SERVER,
            LOAD_OF_LOGS_FROM_SERVER,
            LOAD_OF_PVT_FROM_SERVER,
            LOAD_OF_GRID_FROM_SERVER,
            LOAD_OF_CHESS_FROM_SERVER,
            LOAD_OF_GTM_FROM_SERVER,
            LOAD_OF_WELL_ACTION_FROM_SERVER,
            LOAD_OF_WELL_RESEARCH_FROM_SERVER,
            LOAD_OF_PERF_FROM_OIS_SERVER,
            LOAD_OF_WELL_SUSPEND_FROM_OIS_SERVER,

            LOAD_OF_WELLLIST_FROM_CACHE,
            LOAD_OF_CONTOUR_FROM_CACHE,
            LOAD_OF_AREAS_FROM_CACHE,
            LOAD_OF_WELLPADS_FROM_CACHE,
            LOAD_OF_MER_FROM_CACHE,
            LOAD_OF_PVT_FROM_CACHE,
            LOAD_OF_CHESS_FROM_CACHE,
            LOAD_OF_GRID_FROM_CACHE,

            SAVE_OF_AREAS_TO_CACHE,
            SAVE_OF_CONTOUR_TO_CACHE,
            SAVE_OF_GRIDS_TO_CACHE,

            SUM_WELLLIST_PARAMS_CALC,
            SUM_WELLLIST_TABLE_RECALC,
            SUM_AREA_PARAMS_CALC,
            SUM_OF_PARAMS_CALC,
            SUM_NGDU_PARAMS_CALC,
            SUM_REG_PARAMS_CALC,
            SUM_AREA_TABLE_RECALC,
            SUM_OF_TABLE_RECALC,
            SUM_NGDU_TABLE_RECALC,
            SUM_REG_TABLE_RECALC,
            SUM_OILOBJ_PARAMS_CALC,
            SUM_OILOBJ_TABLE_RECALC,

            LOAD_PROJECT,
            LOAD_GLOBAL_PROJECT,
            RELOAD_PROJECT,
            MODIFY_PROJECT,
            LOAD_OILOBJ_CODES,
            LOAD_AREAS_PARAMS_FROM_CACHE,
            FILL_AREAS_PARAMS_TO_TABLE,
            FILL_AREAS_PARAMS_TO_TABLE_BY_DATE,
            RESET_AREAS_ALL_PARAMS,
            CALC_AREAS_COMPENSATION,
            CALC_BUBBLE_MAP,
            UPDATE_BUBBLE_MAP,
           
            RESET_ICONS_BY_FILTER,

            LOAD_ALL_OILFIELD_WELLLIST,
            LOAD_ALL_OILFIELD_DATA,
            LOAD_ALL_CONTOURS,
            LOAD_TABLES_81,
            LOAD_CONSTRUCTION,
            LOAD_AREAS_CODES,
            LOAD_PVT_FROM_FILE,
            LOAD_PROJWELL_FROM_FILE,
            LOAD_CORE_FROM_FILE,
            LOAD_CORE_TEST_FROM_FILE,
            SAVE_PROJECT,
            LOAD_WORK_LAYERS,
            SAVE_WORK_LAYERS,
            SAVE_WORK_LAYER,
            SERVICE_FUNC,
            PRODUCTION_SEPARATE,
            SAVE_APP_SETTINGS,
            EXPORT_PROJECT_DATA,
            EXPORT_WELL_WITHOUT_AREACODE,
            EXPORT_AREAS_CONTOURS,
            CREATE_WELLLIST_BY_FILTER,
            CREATE_PROFILE_BY_NEWCONTOUR,
            CREATE_PROFILE_BY_NEAR_WELL,
            LOAD_CORSCHEME_BY_PROFILE,
            LOAD_PLANE_BY_WELL,
            ADD_WELL_TO_PROFILE,
            FREE_OTHER_OF_GRIDS,
            CREATE_OF_UNION_CONTOUR,
            REPORT_TARGET_OPENING_GIS,
            REPORT_OIL_OBJ_LIST,
            REPORT_LICENCE_PERF,
            REPORT_STAGE_DEVELOPMENT,
            REPORT_PPD,
            REPORT_DATABASE_ERROR,
            REPORT_PRIORITY_GTM,
            LOAD_GRIDS_FROM_DIRECTORY,
            LOAD_WELL_RESEARCH_FROM_DBF,
            REPORT_WELL_ACTION,
            REPORT_INJECTION_WELLS,
            REPORT_WELLS_GTM,
            REPORT_STOP_WELLS,
            REPORT_IDN,
            REPORT_ZBS,
            REPORT_PLAST_ENERGY,
            REPORT_MER,
            REPORT_CHESS,
            REPORT_FLOODING_ANALYSIS,
            LOAD_WELL_STATE_RESEARCH,
            RELOAD_WORK_LAYER,
            LOAD_WELL_RESEARCH_FROM_FILE,
            LOAD_OILFIELD_BIZPLAN,
            LOAD_OF_GRID_FROM_FILE,
            CREATE_VORONOI_MAP,
            REPORT_AREAS_GTM,
            REPORT_AREAS_PREDICT,
            LOAD_PVT_AREAS_FROM_FILE,
            REPORT_PROD_WITHOUT_GTM,
            CREATE_SUM_ONNT_MAP,
            REPORT_VORONOI_RESERVES,
            FILL_WELLS_OIL_GAIN,
            LOAD_DEPOSITS_FROM_FOLDER,
            DEPOSIT_WELL_GIS_REPORT,
            REPORT_MER_PRODUCTION,
            DEPOSIT_OIZ_REPORT,
            LOAD_WELLS_TRAJECTORY,
            REPORT_BASE_PRODUCTION,
            CREATE_GTM_LAYERLIST,
            CREATE_PI_SERIES,
            REPORT_WELL_CORE,
            GTM_AUTOMATCH_PERMEABILITY,
            CONVERT_WORK_FOLDER
        }
        public enum BASE_OBJ_TYPES_ID : int 
        {
            NONE = -1,      // -1
            ARR_OBJECTS,    // 0 
            PROJECT,        // 1
            OILFIELD,       // 2
            LAYER,          // 3
            COND_CONTOUR,   // 4
            CONTOUR,        // 5
            GRID,           // 6
            CONSTRUCTION,   // 7
            TUBE,           // 8
            AREA,           // 9
            DEPOSIT,        // 10
            WELL_PAD,       // 11
            WELL,           // 12
            PROJECT_WELL,   // 13
            PHANTOM_WELL,   // 14
            PLAST,          // 15
            SURFACE,        // 16
            WELL_LIST,      // 17
            WELL_SECTION,   // 18
            AXIS,           // 19
            TRAJECTORY,     // 20
            IMAGE,          // 21
            PROFILE,        // 22
            MARKER,         // 23
            PVT,            // 24
            VORONOI_MAP,    // 25
            DATAFILE,       // 26
            NGDU            // 27
       }

        public enum SHAPEFILE_TYPES_ID : int
        {
            POINT = 1,
            POLYLINE = 3,
            POLYGON = 5
        }

        public enum TOOLTIP_TYPE_ID : int 
        {
            CHECK_APP_UPDATE,
            CHECK_PROJ_UPDATE,
            CHECK_PROJ_UPDATE_LOADED,
            INFORMATION,
            ERROR,
            INVITE_IN_THEME
        }

        public enum BUBBLE_MAP_TYPE : int
        {
            NOT_SET,
            BUBBLE_CURRENT,
            BUBBLE_ACCUM,
            BUBBLE_HISTORY
        }

        public enum BUBBLE_SIGN_TYPE : int
        {
            Oil = 0,
            Inj = 1,
            Gas = 2
        }

        public enum APP_SETTINGS_TYPE : int 
        {
            OIL_OBJ_COLORS,
            COR_SCHEME_BASE,
            COMMON_SETTINGS,
            GTM_AUTOMATCH
        }

        public enum CopyImageFormat : byte
        {
            BMP,
            EMFplus
        }
        public enum ApplicationUpdateState : byte
        {
            StartCheckUpdate,
            StopCheckUpdate,
            StartUpdateFiles,
            StopUpdateFiles
        }

        public struct Chart
        {
            public enum SERIES_TYPE : short
            {
                LIQUID_M,
                LIQUID_T,
                LIQUID_CHESS_M,
                LIQUID_CHESS_T,
                LIQUID_PROJ_M,
                LIQUID_PROJ_T,
                Q_LIQUID_M,
                Q_LIQUID_T,
                Q_LIQUID_CHESS_M,
                Q_LIQUID_CHESS_T,
                OIL_M,
                OIL_T,
                OIL_CHESS_M,
                OIL_CHESS_T,
                OIL_PROJ_M,
                OIL_PROJ_T,
                Q_OIL_M,
                Q_OIL_T,
                Q_OIL_CHESS_M,
                Q_OIL_CHESS_T,
                WATER,
                WATER_CHESS,
                Q_WATER,
                Q_WATER_CHESS,
                WATERING,
                WATERING_M,
                WATERING_CHESS,
                WATERING_CHESS_M,
                INJECTION,
                INJECTION_CHESS,
                INJECTION_PROJ,
                W_INJ,
                W_INJ_CHESS,
                PROD_FUND,
                PROD_FUND_CHESS,
                PROD_FUND_PROJ,
                INJ_FUND,
                INJ_FUND_CHESS,
                INJ_FUND_PROJ,
                LEAD_PROD_ALL,
                LEAD_PROD_OPERATION,
                FLOWING_LEVEL,
                PROJECT_EDGE_POINTS,
                FORCED_DATE,
                Q_LIQ_CHESS_SUM,
                Q_LIQ_CHESS_SUM_M,
                Q_LIQ_CHESS_AVERAGE,
                Q_LIQ_CHESS_AVERAGE_M,
                Q_OIL_CHESS_SUM,
                Q_OIL_CHESS_AVERAGE,
                W_INJ_CHESS_SUM,
                W_INJ_CHESS_AVERAGE,
                PLAST_PRESSURE,
                PLAST_PRESSURE_HSTAT,
                PLAST_PRESSURE_BUFFPL,
                PLAST_PRESSURE_AVERAGE,
                NATGAS,
                GASCONDENSAT,
                Q_NATGAS,
                Q_GASCONDENSAT,
                INJ_GAS,
                W_INJ_GAS,
                PRESSURE_ZAB_HDYN,
                SUSPEND_SOLID,
                LIQUID_BP_T,
                OIL_BP_T,
                INJECTION_BP,
                PRESSURE_ZAB,
                ZAB_PRESSURE_AVERAGE,
                COMPENSATION,
                COMPENSATION_ACCUM,
                PI_M,
                PI_T,
                Q_LIQUID_M_POTENTIAL,
                Q_LIQUID_T_POTENTIAL,
                Q_OIL_M_POTENTIAL,
                Q_OIL_T_POTENTIAL
            }

            public enum CHART_TYPE : short
            {
                COLUMN,
                POINT,
                LINE,
                AREA
            }

            public enum SERIES_LINE_TYPE : short
            {
                LINE,
                STEP_LINE,
                SPLINE
            }

            public enum DATA_SOURCE_TYPE : short
            {
                MER,
                CHESS,
                PROJ_PARAMS,
                WELL_RESEARCH,
                WELL_LEAK,
                WELL_SUSPEND
            }
        }

        public struct Plane
        {
            public enum ScaleType : short 
            {
                LINEAR,
                LOG10,
                INVERT_LINEAR,
                INVERT_LOG10
            }

            public enum TextOrientation : byte
            {
                NORMAL,
                LEFT,
                RIGHT,
                HEADLONG
            }

            public enum ObjectType : byte
            {
                DEPTH_MARKS,
                GIS,
                LITOLOGY,
                PERFORATION,
                PGI,
                LOG,
                POROSITY,
                OIL_SAT,
                OIL_OBJ,
                MARKER,
                CORE,
                EMPTY,
                LEAK,
                PERFORATION_HISTORY
            }

            public enum CurveType : byte
            {
                PS,
                GK,
                IK,
                BK,
                NGK
            }
        }

        public struct WellDataFilter
        {
            public enum ValueType : byte
            {
                DATE,
                BOOL,
                STRING,
                NUMBER
            }

            public enum DataType : byte 
            {
                WELLNAME,               // 0 // String
                OILFIELDNAME,           // 1
                OILFIELDAREANAME,       // 2
                NGDUNAME,               // 3

                MER_EXIST = 10,         // 10 // Bool
                GIS_EXIST,              // 11
                CHESS_EXIST,            // 12
                LOGS_EXIST,             // 13
                CORE_EXIST,             // 14
                CORE_TEST_EXIST,        // 15

                MERDATE = 20,           // 20 // DateTime
                LIQ_LAST,               // 21 // Number
                LIQ_AVERAGE,            // 22
                OIL_LAST,               // 23
                OIL_AVERAGE,            // 24
                INJ_LAST,               // 25
                INJ_AVERAGE,            // 26
                QLIQ_LAST,              // 27
                QLIQ_AVERAGE,           // 28
                QOIL_LAST,              // 29
                QOIL_AVERAGE,           // 30
                WATERING_LAST,          // 31
                WATERING_AVERAGE,       // 32
                WINJ_LAST,              // 33
                WINJ_AVERAGE,           // 34
                NATGAS_LAST,            // 35
                NATGAS_AVERAGE,         // 36
                QNATGAS_LAST,           // 37
                QNATGAS_AVERAGE,        // 38
                GASCOND_LAST,           // 39
                GASCOND_AVERAGE,        // 40
                QGASCOND_LAST,          // 41
                QGASCOND_AVERAGE        // 42
            }

            #region DATATYPE_NAME
            public static string[] DATATYPE_NAME =
            {
                "����� ��������",
                "��� �������������",
                "��� �������",
                "��� ����",
                "", "", "", "", "", "",

                "��������� ���",
                "��������� ���",
                "��������� ��������",
                "�������� ������� �������",
                "��������� ������ ������� �����",
                "��������� ������ ������������ �����",
                "", "", "", "",

                "���� ���",
                "������ �������� �� ��������� �����, �",
                "������ �������� ������� �� ������, �",
                "������ ����� �� ��������� �����, �",
                "������ ����� ������� �� ������, �",
                "������� �� ��������� �����, �3",
                "������� ������� �� ������, �3",

                "����� �������� �� ��������� �����, �/���",
                "����� �������� ������� �� ������, �/���",
                "����� ����� �� ��������� �����, �/���",
                "����� ����� ������� �� ������, �/���",
                "������������� �� ��������� �����, %",
                "������������� ������� �� ������, %",
                "������������ �� ��������� �����, �3/���",
                "������������ ������� �� ������, �3/���"
            };
            #endregion

            public enum Condition : byte
            {
                EQUAL,
                NOT_EQUAL,
                GREATER,
                GTREAT_OR_EQUAL,
                LESS,
                LESS_OR_EQUAL,
                START_WITH,
                END_WITH,
                CONTAIN,
                NOT_CONTAIN
            }

            #region CONDITION_NAME
            public static string[] CONDITION_NAME =
            {
                "�����",
                "�� �����",
                "������",
                "������ ��� �����",
                "������",
                "������ ��� �����",
                "���������� �..",
                "������������� ��..",
                "��������"
            };
            #endregion
        }
    }
}