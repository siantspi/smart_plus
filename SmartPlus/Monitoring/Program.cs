using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Deployment.Application;

namespace SmartPlus
{
    static class Program
    {
        ///  <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.OperatingSystem os = System.Environment.OSVersion;
            bool xp = ((os.Platform == PlatformID.Win32Windows) || ((os.Platform == PlatformID.Win32NT) && (os.Version.Major < 6)));
            if (!xp) Application.EnableVisualStyles();

            Application.SetCompatibleTextRenderingDefault(false);

#if !MSI && !DEBUG            
            HTTPSUpdateAppForm up = new HTTPSUpdateAppForm();

            if (up.download()) // ���� �������� ���������� ����� https
            {
                Application.Run(up); // ��������� ������ ���������� ���������
            } 

            if (up.skipped)
            {
#endif

                MainForm frmMain = new MainForm();
                Application.Run(frmMain);

#if !MSI && !DEBUG
            }
#endif

        }

    }
}