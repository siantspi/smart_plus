﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class ReportPredictAreaForm : Form
    {
        public const int TemplateVersion = 6;
        MainForm mainForm;
        public string templatePath;
        public string TRFilePath
        {
            get { return TRFileNameTextBox.Text; }
        }
        bool ServerInited = false;

        private bool TemplateSuccess, TRSucces; 
        private Label label1;
        private Label TemplateLoadedLabel;
        private Button TemplateLoadButton;
        private CheckBox TRLoadCheckBox;
        private TextBox TRFileNameTextBox;
        private Button TRLoadButton;
        private Button OkButton;
        private Button CancelButton;
        private Label TRFileSuccessLabel;

        private BackgroundWorker worker;
        private bool StartNewTRFileCheck;
        private SmartPlus.Network.TemplateServer tmClient;

        public ReportPredictAreaForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            this.Owner = mainForm;
            this.StartPosition = FormStartPosition.CenterParent;
            templatePath = string.Empty;
            int version = -1;
            OkButton.Enabled = false;
            TRFileSuccessLabel.Text = string.Empty;
            StartNewTRFileCheck = false;
            TemplateSuccess = false;
            TRSucces = true;

            if (Directory.Exists(mainForm.UserTemplatesFolder))
            {
                DirectoryInfo root = new DirectoryInfo(mainForm.UserTemplatesFolder);
                FileInfo[] files = root.GetFiles();
                int pos1, pos2;
                string str;
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Name.StartsWith("Прогноз по ЯЗ") && files[i].Name.EndsWith(".xltm"))
                    {
                        templatePath = files[i].FullName;
                        pos1 = files[i].Name.IndexOf("(ver ");
                       
                        if(pos1 > -1) 
                        {
                            str = files[i].Name.Remove(0, pos1 + 5);
                            pos2 = str.IndexOf(").xltm");
                            version = Convert.ToInt32(str.Remove(pos2, str.Length - pos2));
                        }
                        break;
                    }
                }
            }

            if (templatePath.Length > 0 && TemplateVersion == version)
            {
                TemplateLoadedLabel.ForeColor = Color.Green;
                TemplateLoadedLabel.Text = "Загружен";
                TemplateLoadButton.Enabled = false;
                tmClient = null;
                TemplateSuccess = true;
                OkButton.Enabled = true;
            }
            else
            {
                TemplateLoadedLabel.ForeColor = Color.Red;
                TemplateLoadedLabel.Text = (version > -1 && TemplateVersion != version) ? "Устаревшая версия" : "Не загружен";
                TemplateLoadButton.Enabled = true;
                templatePath = string.Empty;
                TemplateSuccess = false;
            }

            // масштабирование
            int dx = label1.Width - 173;
            if (dx > 0)
            {
                TemplateLoadedLabel.Left += dx;
            }
            TemplateLoadButton.Left = TemplateLoadedLabel.Right + 22;
            this.FormClosing += ReportPredictAreaForm_FormClosing;

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = false;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
        }
        
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            MSOffice.Excel excel = new MSOffice.Excel();
            try
            {
                string fileName = (string)e.Argument;
                object[] result = new object[2];
                result[0] = false;
                result[1] = "Неверный файл техрежима скважин! Укажите правильный файл.";
                if (File.Exists(fileName))
                {
                    
                    excel.OpenWorkbook(fileName);
                    int InitTRsheet = -1;
                    int sheetCount = excel.GetWorkSheetsCount();
                    for (int i = 0; i < sheetCount; i++)
                    {
                        excel.SetActiveSheet(i);
                        if (excel.GetActiveSheetName() == "АНК Башнефть")
                        {
                            InitTRsheet = i;
                        }
                        if (InitTRsheet != -1) break;
                    }
                    if (InitTRsheet == -1)
                    {
                        result[1] = "Необходим файл техрежима по АНК Башнефть!";
                    }
                    else
                    {
                        excel.SetActiveSheet(InitTRsheet);
                        string str = excel.GetValue(1, 6).ToString();
                        if (str.Length > 0 && (str.IndexOf(".") > -1))
                        {
                            DateTime date = Convert.ToDateTime(str);
                            if (DateTime.Now.Day > 21 && date.Month == DateTime.Now.Month || 
                                DateTime.Now.Day <= 21 && (date.Month == DateTime.Now.Month - 1 || (date.Month == 12 && DateTime.Now.Month == 1 && date.Year + 1 == DateTime.Now.Year)))
                            {
                                string str1 = excel.GetValue(3, 11).ToString();
                                string str2 = excel.GetValue(3, 61).ToString();
                                string str3 = excel.GetValue(3, 126).ToString();
                                if (str1.StartsWith("Ячейка") && str2.StartsWith("Рзаб.") && str3.StartsWith("Расчетная"))
                                {
                                    result[0] = true;
                                    result[1] = "Файл техрежима проверен";
                                    if (TemplateSuccess)
                                    {
                                        result[1] += ". Нажмите OK для запуска выгрузки отчета.";
                                    }
                                    else
                                    {
                                        result[1] += ", но необходимо загрузить шаблон отчета.";
                                    }
                                }
                            }
                            else
                            {
                                result[1] = "Необходим техрежим скважин загруженный на последнюю дату!";
                            }
                        }
                    }
                }
                e.Result = result;
            }
            finally
            {
                excel.Quit();
            }
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                TRFileSuccessLabel.ForeColor = Color.Red;
                TRFileSuccessLabel.Text = "Ошибка при открытии файла техрежима.\nУкажите правильный файл.";
            }
            else if (e.Cancelled)
            {
                if (StartNewTRFileCheck) worker.RunWorkerAsync(TRFileNameTextBox.Text);
            }
            else
            {
                object[] result = (object[])e.Result;
                TRSucces = (bool)result[0];
                TRFileSuccessLabel.ForeColor = (TRSucces && TemplateSuccess) ? Color.Green : Color.Red;
                TRFileSuccessLabel.Text = (string)result[1];
                OkButton.Enabled = TRSucces && TemplateSuccess;
            }
            StartNewTRFileCheck = false;
            CancelButton.Enabled = true;
            TRLoadButton.Enabled = true;
        }

        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TemplateLoadedLabel = new System.Windows.Forms.Label();
            this.TemplateLoadButton = new System.Windows.Forms.Button();
            this.TRLoadCheckBox = new System.Windows.Forms.CheckBox();
            this.TRFileNameTextBox = new System.Windows.Forms.TextBox();
            this.TRLoadButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.TRFileSuccessLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Шаблон для выгрузки отчета:";
            // 
            // TemplateLoadedLabel
            // 
            this.TemplateLoadedLabel.AutoSize = true;
            this.TemplateLoadedLabel.ForeColor = System.Drawing.Color.Red;
            this.TemplateLoadedLabel.Location = new System.Drawing.Point(191, 24);
            this.TemplateLoadedLabel.Name = "TemplateLoadedLabel";
            this.TemplateLoadedLabel.Size = new System.Drawing.Size(77, 15);
            this.TemplateLoadedLabel.TabIndex = 1;
            this.TemplateLoadedLabel.Text = "Не загружен";
            // 
            // TemplateLoadButton
            // 
            this.TemplateLoadButton.AutoSize = true;
            this.TemplateLoadButton.Location = new System.Drawing.Point(290, 16);
            this.TemplateLoadButton.Name = "TemplateLoadButton";
            this.TemplateLoadButton.Size = new System.Drawing.Size(135, 30);
            this.TemplateLoadButton.TabIndex = 2;
            this.TemplateLoadButton.Text = "Загрузить с сервера";
            this.TemplateLoadButton.UseVisualStyleBackColor = true;
            this.TemplateLoadButton.Click += new System.EventHandler(this.TemplateLoadButton_Click);
            // 
            // TRLoadCheckBox
            // 
            this.TRLoadCheckBox.AutoSize = true;
            this.TRLoadCheckBox.Location = new System.Drawing.Point(15, 63);
            this.TRLoadCheckBox.Name = "TRLoadCheckBox";
            this.TRLoadCheckBox.Size = new System.Drawing.Size(301, 19);
            this.TRLoadCheckBox.TabIndex = 3;
            this.TRLoadCheckBox.Text = "Загрузить данные Техрежима скважин из файла:";
            this.TRLoadCheckBox.UseVisualStyleBackColor = true;
            this.TRLoadCheckBox.CheckedChanged += new System.EventHandler(this.TRLoadCheckBox_CheckedChanged);
            // 
            // TRFileNameTextBox
            // 
            this.TRFileNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TRFileNameTextBox.Enabled = false;
            this.TRFileNameTextBox.Location = new System.Drawing.Point(15, 88);
            this.TRFileNameTextBox.Name = "TRFileNameTextBox";
            this.TRFileNameTextBox.Size = new System.Drawing.Size(461, 23);
            this.TRFileNameTextBox.TabIndex = 4;
            // 
            // TRLoadButton
            // 
            this.TRLoadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TRLoadButton.AutoSize = true;
            this.TRLoadButton.Enabled = false;
            this.TRLoadButton.Location = new System.Drawing.Point(482, 83);
            this.TRLoadButton.Name = "TRLoadButton";
            this.TRLoadButton.Size = new System.Drawing.Size(95, 30);
            this.TRLoadButton.TabIndex = 5;
            this.TRLoadButton.Text = "Открыть...";
            this.TRLoadButton.UseVisualStyleBackColor = true;
            this.TRLoadButton.Click += new System.EventHandler(this.TRLoadButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(381, 152);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(95, 30);
            this.OkButton.TabIndex = 6;
            this.OkButton.Text = "ОК";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(482, 152);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(95, 30);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // TRFileSuccessLabel
            // 
            this.TRFileSuccessLabel.AutoSize = true;
            this.TRFileSuccessLabel.Location = new System.Drawing.Point(12, 123);
            this.TRFileSuccessLabel.Name = "TRFileSuccessLabel";
            this.TRFileSuccessLabel.Size = new System.Drawing.Size(16, 15);
            this.TRFileSuccessLabel.TabIndex = 8;
            this.TRFileSuccessLabel.Text = "...";
            // 
            // ReportPredictAreaForm
            // 
            this.AcceptButton = this.OkButton;
            this.ClientSize = new System.Drawing.Size(589, 194);
            this.Controls.Add(this.TRFileSuccessLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.TRLoadButton);
            this.Controls.Add(this.TRFileNameTextBox);
            this.Controls.Add(this.TRLoadCheckBox);
            this.Controls.Add(this.TemplateLoadButton);
            this.Controls.Add(this.TemplateLoadedLabel);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportPredictAreaForm";
            this.Text = "Выгрузка отчета \'Прогноз по ячейкам заводнения\'";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void TemplateLoadButton_Click(object sender, EventArgs e)
        {
            if (tmClient == null) tmClient = new Network.TemplateServer(mainForm, null, TemplateDownloadComplete);
        }
        void TemplateDownloadComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null && !e.Cancelled && e.Result != null)
            {
                Network.NetPacket packet = (Network.NetPacket)e.Result;
                if (packet.type == Network.NetPacket.Type.GetUserId)
                {
                    ServerInited = packet.result == Network.NetPacket.Result.Succes;
                    TemplateLoadButton.Enabled = false;
                    TemplateLoadedLabel.ForeColor = Color.Black;
                    TemplateLoadedLabel.Text = "Идет загрузка...";
                    tmClient.DownloadTemplate(Network.TemplateServer.TemplateType.ReportAreasPredictTemplate);
                }
                else if (packet.type == Network.NetPacket.Type.DownloadTemplate)
                {
                    if (tmClient.TemplateLocalPath.Length > 0)
                    {
                        TemplateLoadedLabel.ForeColor = Color.Green;
                        TemplateLoadedLabel.Text = "Загружен";
                        templatePath = tmClient.TemplateLocalPath;
                        TemplateSuccess = true;
                        OkButton.Enabled = TRSucces && TemplateSuccess;
                        if (TRLoadCheckBox.Checked && TRSucces)
                        {
                            TRFileSuccessLabel.ForeColor = Color.Green;
                            TRFileSuccessLabel.Text = "Файл техрежима проверен. Нажмите OK для запуска выгрузки отчета.";
                        }
                    }
                }
            }
            if (!ServerInited)
            {
                TemplateLoadedLabel.ForeColor = Color.Red;
                TemplateLoadedLabel.Text = "Сервер недоступен";
                TemplateLoadButton.Enabled = true;
                TemplateSuccess = false;
            }
        }
        
        void ReportPredictAreaForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (worker.IsBusy)
            {
                worker.CancelAsync();
                e.Cancel = true;
            }
            
            if (this.DialogResult != DialogResult.OK)
            {
                if (tmClient != null) tmClient.CancelDownload();
            }
        }
        private void TRLoadCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            TRFileNameTextBox.Enabled = TRLoadCheckBox.Checked;
            TRLoadButton.Enabled = TRLoadCheckBox.Checked;
            
            if (TRLoadCheckBox.Checked)
            {
                TRSucces = false;
                OkButton.Enabled = TRSucces && TemplateSuccess;
                TRFileSuccessLabel.ForeColor = Color.Black;
                TRFileSuccessLabel.Text = "Укажите файл техрежима скважин по ОАО АНК Башнефть.";
            }
            else
            {
                TRSucces = true;
                TRFileSuccessLabel.Text = string.Empty;
                TRFileNameTextBox.Text = string.Empty;
                OkButton.Enabled = TRSucces && TemplateSuccess;
            }
        }
        private void TRLoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Выберите файл с техрежимом скважин";
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dlg.Filter = "Excel файлы | *.xls;*.xlsx;*.xlsm;*.xlsb";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TRFileNameTextBox.Text = dlg.FileName;
                TRFileSuccessLabel.ForeColor = Color.Black;
                TRFileSuccessLabel.Text = "Файл техрежима скважин проверяется. Пожалуйста подождите...";
                CancelButton.Enabled = false;
                if (worker.IsBusy)
                {
                    StartNewTRFileCheck = true;
                    TRLoadButton.Enabled = false;
                    worker.CancelAsync();
                }
                else
                {
                    StartNewTRFileCheck = false;
                    worker.RunWorkerAsync(TRFileNameTextBox.Text);
                }
            }
        }
    }
}
