﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace SmartPlus
{
    public enum ProjectDataType : int
    {
        NONE = -1,
        COORD = 0,
        CONTOUR,
        WELLPADS,
        MER,
        SUM_MER,
        CHESS,
        SUM_CHESS,
        AREA,
        SUM_AREA,
        OILOBJ,
        PVT,
        TABLE81,
        GRIDS,
        GIS,
        LOGS,
        PERFORATION,
        CORE,
        CORE_TEST,
        LOGS_BASE,
        GTM,
        WELL_ACTION,
        WELL_RESEARCH,
        WELL_LEAK,
        WELL_SUSPEND,
        OILFIELD_BIZPLAN,
        TRAJECTORY
    }
    public partial class ExportProjectForm : Form
    {
        MainForm mainForm;
        ArrayList OFList;
        string SourceFldr;
        string DestFldr;
        bool ExportLastHour { get { return cbExportLastHour.Checked; } }
        bool[] CheckedDataTypes;
        ContextMenuStrip cntxMenu;
        ToolStripMenuItem cntxMenuCheckAll, cntxMenuUnCheckAll;

        public ExportProjectForm(MainForm MainForm)
        {
            InitializeComponent();
            Owner = MainForm;
            mainForm = MainForm;

            OFList = new ArrayList();
            CheckedDataTypes = new bool[25];
            for (int i = 0; i < CheckedDataTypes.Length; i++) CheckedDataTypes[i] = false;

            ContextMenuStrip cntxMenu = new ContextMenuStrip();
            cntxMenu = new ContextMenuStrip();
            // Выбрать все
            cntxMenuCheckAll = (ToolStripMenuItem)cntxMenu.Items.Add("");
            cntxMenuCheckAll.Text = "Включить все";
            cntxMenuCheckAll.Image = Properties.Resources.SellAll;
            cntxMenuCheckAll.Click += new EventHandler(cntxMenuCheckAll_Click);
            // выключить все
            cntxMenuUnCheckAll = (ToolStripMenuItem)cntxMenu.Items.Add("");
            cntxMenuUnCheckAll.Text = "Отключить все";
            cntxMenuUnCheckAll.Image = Properties.Resources.DeSellAll;
            cntxMenuUnCheckAll.Click += new EventHandler(cntxMenuUnCheckAll_Click);
            OilFieldList.ContextMenuStrip = cntxMenu;
        }
        void SetCountText(int Count)
        {
            lCount.Text = "Выбранных: " + Count.ToString();
        }
        void SetCheckAllItems(bool CheckState)
        {
            OilFieldList.ItemCheck -= new ItemCheckEventHandler(OilFieldList_ItemCheck);
            int i;
            for (i = 0; i < OilFieldList.Items.Count; i++)
            {
                OilFieldList.SetItemChecked(i, CheckState);
            }
            OFList.Clear();
            if (CheckState)
            {
                for (i = 0; i < OilFieldList.Items.Count; i++)
                {
                    OFList.Add((string)OilFieldList.Items[i]);
                }
            }
            OilFieldList.ItemCheck += new ItemCheckEventHandler(OilFieldList_ItemCheck);
        }
        void cntxMenuCheckAll_Click(object sender, EventArgs e)
        {
            SetCheckAllItems(true);
            SetCountText(OFList.Count);
        }
        void cntxMenuUnCheckAll_Click(object sender, EventArgs e)
        {
            SetCheckAllItems(false);
            SetCountText(0);
        }
        private void bOpen_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.ShowNewFolderButton = false;
            dlg.SelectedPath = "D:\\Monitoring\\MonitoringProjects";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                tbDest.Text = dlg.SelectedPath;
                DestFldr = dlg.SelectedPath;
            }
        }
        private void DataCheckChange(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Tag != null)
            {
                CheckedDataTypes[Convert.ToInt32((string)cb.Tag)] = cb.Checked;
            }
        }
        public void SetProject(Project proj)
        {
            SourceFldr = proj.path;
            OilFieldList.Items.Clear();
            OilFieldList.CheckOnClick = true;
            int i;

            for (i = 1; i < proj.OilFields.Count; i++)
            {
                OilFieldList.Items.Add((proj.OilFields[i]).Name, true);
                OFList.Add((proj.OilFields[i]).Name);
            }
            OilFieldList.ItemCheck += new ItemCheckEventHandler(OilFieldList_ItemCheck);
            SetCountText(OFList.Count);
        }

        void OilFieldList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.NewValue == CheckState.Checked)
            {
                if (OFList.IndexOf((string)OilFieldList.Items[e.Index]) == -1)
                {
                    OFList.Add((string)OilFieldList.Items[e.Index]);
                }
            }
            else
            {
                OFList.Remove((string)OilFieldList.Items[e.Index]);
            }
            SetCountText(OFList.Count);
        }

        public void CopyDirectory(string SourceDir, string DestDir)
        {
            DirectoryInfo dir = new DirectoryInfo(SourceDir);
            if(!Directory.Exists(DestDir)) Directory.CreateDirectory(DestDir);
            FileInfo[] fileList;
            fileList = dir.GetFiles();
            int i, len = fileList.Length;
            bool copy = false;
            int minutes;
            TimeSpan ts;
            for(i = 0; i < len ; i ++)
            {
                copy = false;
                switch (fileList[i].Name)
                {
                    case "skv.csv":
                        if(CheckedDataTypes[(int)ProjectDataType.COORD]) copy = true;
                        break;
                    case "grids.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.GRIDS]) copy = true;
                        break;
                    case "contours.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.CONTOUR]) copy = true;
                        break;
                    case "areas.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.AREA]) copy = true;
                        break;
                    case "areas_param.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.SUM_AREA]) copy = true;
                        break;
                    case "wellpads.csv":
                        if(CheckedDataTypes[(int)ProjectDataType.WELLPADS]) copy = true;
                        break;
                    case "OilObj.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.OILOBJ]) copy = true;
                        break;
                    case "mer.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.MER]) copy = true;
                        break;
                    case "chess.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.CHESS]) copy = true;
                        break;
                    case "sum.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.SUM_MER]) copy = true;
                        break;
                    case "sum_chess.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.SUM_CHESS]) copy = true;
                        break;
                    case "table81.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.TABLE81]) copy = true;
                        break;
                    case "PVTParams.bin":
                        if(CheckedDataTypes[(int)ProjectDataType.PVT]) copy = true;
                        break;
                    case "gis.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.GIS]) copy = true;
                        break;
                    case "logs_base.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.LOGS]) copy = true;
                        break;
                    case "perf.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.PERFORATION]) copy = true;
                        break;
                    case "core.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.CORE]) copy = true;
                        break;
                    case "core_test.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.CORE_TEST]) copy = true;
                        break;
                    case "gtm.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.GTM]) copy = true;
                        break;
                    case "well_action.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.WELL_ACTION]) copy = true;
                        break;
                    case "well_research.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.WELL_RESEARCH]) copy = true;
                        break;
                    case "well_leak.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.WELL_LEAK]) copy = true;
                        break;
                    case "well_suspend.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.WELL_SUSPEND]) copy = true;
                        break;
                    case "bizplan.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.OILFIELD_BIZPLAN]) copy = true;
                        break;
                    case "trajectory.bin":
                        if (CheckedDataTypes[(int)ProjectDataType.TRAJECTORY]) copy = true;
                        break;
                    default:
                        continue;
                }
                if (copy)
                {
                    ts = DateTime.Now - fileList[i].CreationTime;
                    minutes = ts.Minutes + ts.Days * 1440 + ts.Hours * 60;
                    if ((!ExportLastHour) || (minutes < 360))
                    {
                        //if(File.Exists(DestDir + "\\" + fileList[i].Name)) File.Delete(DestDir + "\\" + fileList[i].Name);
                        File.Copy(SourceDir + "\\" + fileList[i].Name, DestDir + "\\" + fileList[i].Name, true);
                        File.SetCreationTime(DestDir + "\\" + fileList[i].Name, DateTime.Now);
                        File.SetLastWriteTime(DestDir + "\\" + fileList[i].Name, DateTime.Now);
                    }
                }
            }
        }
        public void ExportProjectData(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if ((OFList.Count > 0) && (SourceFldr != "") && (DestFldr != ""))
            {
                int i, len = OFList.Count;
                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = "Экспорт данных проекта...";
                userState.WorkCurrentTitle = "Экспорт данных месторождения";
                string ofName;
                if (Directory.Exists(SourceFldr))
                {
                    CopyDirectory(SourceFldr + "\\cache", DestFldr + "\\cache");
                }
                for (i = 0; i < len; i++)
                {
                    ofName = (string)OFList[i];
                    userState.Element = ofName;
                    worker.ReportProgress((int)((float)i * 100F / (float)len));
                    if (Directory.Exists(SourceFldr + "\\cache\\" + ofName))
                    {
                        CopyDirectory(SourceFldr + "\\cache\\" + ofName, DestFldr + "\\cache\\" + ofName);
                    }
                }
            }
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.EXPORT_PROJECT_DATA, this);
            this.Hide();
        }

        void CheckOilFieldByName(string OilFieldName)
        {
            for (int i = 0; i < OilFieldList.Items.Count; i++)
            {
                if ((string)OilFieldList.Items[i] == OilFieldName.ToUpper())
                {
                    OilFieldList.SetItemChecked(i, true);
                    break;
                }
            }
        }
        private void bByInspector_Click(object sender, EventArgs e)
        {
            if (mainForm != null)
            {
                if (mainForm.canv.twLayers.cmSortByNGDU.Checked)
                {
                    TreeNode tn = mainForm.canv.twLayers.SelectedNode;
                    if (tn != null)
                    {
                        if (tn.Level == 0)
                        {
                            SetCheckAllItems(true);
                        }
                        else if (tn.Level == 1)
                        {
                            for (int i = 0; i < tn.Nodes.Count; i++)
                            {
                                CheckOilFieldByName(tn.Nodes[i].Text);
                            }
                        }
                        else if (tn.Level == 2)
                        {
                            CheckOilFieldByName(tn.Text);
                        }
                    }
                }
                else if (mainForm.canv.twLayers.cmSortByOilField.Checked)
                {
                    TreeNode tn = mainForm.canv.twLayers.SelectedNode;
                    if (tn != null)
                    {
                        if (tn.Level == 0)
                        {
                            SetCheckAllItems(true);
                        }
                        else if (tn.Level == 1)
                        {
                            CheckOilFieldByName(tn.Text);
                        }
                    }
                }
                SetCountText(OFList.Count);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void bByWorkFolder_Click(object sender, EventArgs e)
        {
            if(mainForm.canv.twWork.Nodes.Count > 0)
            {
                TreeNode tnProj = mainForm.canv.twWork.Nodes[0], tnNGDU;
                if (tnProj.Nodes.Count > 0)
                {
                    for (int i = 0; i < tnProj.Nodes.Count; i++)
                    {
                        tnNGDU = tnProj.Nodes[i];
                        for (int j = 0; j < tnNGDU.Nodes.Count; j++)
                        {
                            CheckOilFieldByName(tnNGDU.Nodes[j].Text);
                        }
                    }
                }
            }
        }
    }
}
