﻿namespace SmartPlus
{
    partial class IconListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.pIconMap = new System.Windows.Forms.Panel();
            this.vScroll = new System.Windows.Forms.VScrollBar();
            this.pbIconMap = new System.Windows.Forms.PictureBox();
            this.bAdd = new System.Windows.Forms.Button();
            this.bUp = new System.Windows.Forms.Button();
            this.bDown = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bEdit = new System.Windows.Forms.Button();
            this.gbAllIcon = new System.Windows.Forms.GroupBox();
            this.pbAllIcon = new System.Windows.Forms.PictureBox();
            this.pIconMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIconMap)).BeginInit();
            this.gbAllIcon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAllIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // bOk
            // 
            this.bOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(224, 350);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 0;
            this.bOk.Text = "ОK";
            this.bOk.UseVisualStyleBackColor = true;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(320, 350);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 1;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // pIconMap
            // 
            this.pIconMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pIconMap.Controls.Add(this.vScroll);
            this.pIconMap.Controls.Add(this.pbIconMap);
            this.pIconMap.Location = new System.Drawing.Point(12, 12);
            this.pIconMap.Name = "pIconMap";
            this.pIconMap.Size = new System.Drawing.Size(158, 368);
            this.pIconMap.TabIndex = 2;
            // 
            // vScroll
            // 
            this.vScroll.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScroll.Location = new System.Drawing.Point(141, 0);
            this.vScroll.Name = "vScroll";
            this.vScroll.Size = new System.Drawing.Size(17, 368);
            this.vScroll.TabIndex = 1;
            this.vScroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScroll_Scroll);
            // 
            // pbIconMap
            // 
            this.pbIconMap.BackColor = System.Drawing.Color.White;
            this.pbIconMap.Location = new System.Drawing.Point(0, 0);
            this.pbIconMap.Name = "pbIconMap";
            this.pbIconMap.Size = new System.Drawing.Size(114, 331);
            this.pbIconMap.TabIndex = 0;
            this.pbIconMap.TabStop = false;
            // 
            // bAdd
            // 
            this.bAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bAdd.Image = global::SmartPlus.Properties.Resources.Add_Green32;
            this.bAdd.Location = new System.Drawing.Point(176, 12);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(42, 42);
            this.bAdd.TabIndex = 3;
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // bUp
            // 
            this.bUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bUp.Image = global::SmartPlus.Properties.Resources.Move_Up_Arrow_32;
            this.bUp.Location = new System.Drawing.Point(176, 105);
            this.bUp.Name = "bUp";
            this.bUp.Size = new System.Drawing.Size(42, 42);
            this.bUp.TabIndex = 5;
            this.bUp.UseVisualStyleBackColor = true;
            this.bUp.Click += new System.EventHandler(this.bUp_Click);
            // 
            // bDown
            // 
            this.bDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bDown.Image = global::SmartPlus.Properties.Resources.Move_Down_Arrow_32;
            this.bDown.Location = new System.Drawing.Point(176, 153);
            this.bDown.Name = "bDown";
            this.bDown.Size = new System.Drawing.Size(42, 42);
            this.bDown.TabIndex = 6;
            this.bDown.UseVisualStyleBackColor = true;
            this.bDown.Click += new System.EventHandler(this.bDown_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bDelete.Image = global::SmartPlus.Properties.Resources.Delete32;
            this.bDelete.Location = new System.Drawing.Point(176, 201);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(42, 42);
            this.bDelete.TabIndex = 7;
            this.bDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bEdit
            // 
            this.bEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bEdit.Image = global::SmartPlus.Properties.Resources.Edit32;
            this.bEdit.Location = new System.Drawing.Point(176, 60);
            this.bEdit.Name = "bEdit";
            this.bEdit.Size = new System.Drawing.Size(42, 42);
            this.bEdit.TabIndex = 4;
            this.bEdit.UseVisualStyleBackColor = true;
            this.bEdit.Click += new System.EventHandler(this.bEdit_Click);
            // 
            // gbAllIcon
            // 
            this.gbAllIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAllIcon.Controls.Add(this.pbAllIcon);
            this.gbAllIcon.Location = new System.Drawing.Point(224, 12);
            this.gbAllIcon.Name = "gbAllIcon";
            this.gbAllIcon.Size = new System.Drawing.Size(186, 231);
            this.gbAllIcon.TabIndex = 8;
            this.gbAllIcon.TabStop = false;
            this.gbAllIcon.Text = " Итоговый значок ";
            // 
            // pbAllIcon
            // 
            this.pbAllIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbAllIcon.Location = new System.Drawing.Point(17, 33);
            this.pbAllIcon.Name = "pbAllIcon";
            this.pbAllIcon.Size = new System.Drawing.Size(150, 150);
            this.pbAllIcon.TabIndex = 16;
            this.pbAllIcon.TabStop = false;
            // 
            // IconList
            // 
            this.AcceptButton = this.bOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(420, 392);
            this.ControlBox = false;
            this.Controls.Add(this.gbAllIcon);
            this.Controls.Add(this.bEdit);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bDown);
            this.Controls.Add(this.bUp);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.pIconMap);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(390, 270);
            this.Name = "IconList";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Конфигурация значка скважины...";
            this.pIconMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbIconMap)).EndInit();
            this.gbAllIcon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbAllIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Panel pIconMap;
        private System.Windows.Forms.PictureBox pbIconMap;
        private System.Windows.Forms.VScrollBar vScroll;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.Button bUp;
        private System.Windows.Forms.Button bDown;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bEdit;
        private System.Windows.Forms.GroupBox gbAllIcon;
        private System.Windows.Forms.PictureBox pbAllIcon;
    }
}