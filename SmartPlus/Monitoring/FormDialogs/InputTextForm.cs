﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class InputTextForm : Form
    {
        public InputTextForm()
        {
            InitializeComponent();
        }
        public static DialogResult ShowForm(MainForm mainForm, string Title, string Text, out string NewText)
        {
            InputTextForm form = new InputTextForm();
            form.Owner = mainForm;
            form.StartPosition = FormStartPosition.CenterParent;
            form.Text = Title;
            form.TitleLabel.Text = Text;
            NewText = string.Empty;
            DialogResult result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                NewText = form.InputBox.Text;
            }
            return result;
        }
    }
}
