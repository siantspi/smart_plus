﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class WellDataFilterEditor : Form
    {
        public WellDataFilter Filter = null;

        public WellDataFilterEditor()
        {
            InitializeComponent();
            for (int i = 0; i < Constant.WellDataFilter.DATATYPE_NAME.Length; i++)
            {
                if (Constant.WellDataFilter.DATATYPE_NAME[i] != "")
                {
                    cbFilterDataType.Items.Add(Constant.WellDataFilter.DATATYPE_NAME[i]);
                }
            }
            this.FormClosing += new FormClosingEventHandler(WellDataFilterEditor_FormClosing);
            dtpFilterVal.ValueChanged += new EventHandler(dtpFilterVal_ValueChanged);
        }

        void dtpFilterVal_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt = new DateTime(dtpFilterVal.Value.Year, dtpFilterVal.Value.Month, 1);
            if (dt < dtpFilterVal.MinDate) dt = dtpFilterVal.MinDate;
            dtpFilterVal.Value = dt;
        }

        void WellDataFilterEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                double dblVal = 0;
                if (cbFilterDataType.SelectedIndex == -1)
                {
                    MessageBox.Show("Выберите тип данных фильтра", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (cbFilterCondition.SelectedIndex == -1)
                {
                    MessageBox.Show("Выберите условие фильтра", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    switch (Filter.ValueTypeID)
                    {
                        case Constant.WellDataFilter.ValueType.NUMBER:
                            try
                            {
                                dblVal = Convert.ToDouble(tbFilterVal.Text);
                            }
                            catch
                            {
                                MessageBox.Show("Введено неверное числовое значение", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                e.Cancel = true; 
                            }
                            break;
                        case Constant.WellDataFilter.ValueType.STRING:
                            //if (tbFilterVal.Text == "")
                            //{
                            //    MessageBox.Show("Пустая строка для сравнения", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //    e.Cancel = true; 
                            //}
                            break;
                    }
                }
                if (!e.Cancel)
                {
                    switch (Filter.ValueTypeID)
                    {
                        case Constant.WellDataFilter.ValueType.BOOL:
                            Filter.value = (bool)(cbFilterVal.SelectedIndex == 0);
                            break;
                        case Constant.WellDataFilter.ValueType.DATE:
                            Filter.value = ((DateTime)dtpFilterVal.Value).Date;
                            break;
                        case Constant.WellDataFilter.ValueType.NUMBER:
                            Filter.value = dblVal;
                            break;
                        case Constant.WellDataFilter.ValueType.STRING:
                            Filter.value = tbFilterVal.Text;
                            break;
                    }
                }
            }
        }

        public DialogResult ShowNewFilter()
        {
            Filter = null;
            this.cbFilterDataType.SelectedIndex = -1;
            lComment.Visible = false;
            cbFilterCondition.Enabled = false;
            cbFilterCondition.SelectedIndex = -1;
            cbFilterCondition.Items.Clear();
            tbFilterVal.Text = "";
            cbFilterVal.Enabled = false;
            dtpFilterVal.Enabled = false;
            tbFilterVal.Enabled = false;

            return this.ShowDialog();
        }
        public DialogResult ShowEditFilter(WellDataFilter editFilter)
        {
            Filter = new WellDataFilter(editFilter.TypeID);
            Filter.SetCondition(editFilter.condition);
            Filter.SetValue(editFilter.value);
            lComment.Visible = false;
            string typeName = Constant.WellDataFilter.DATATYPE_NAME[(int)editFilter.TypeID];
            int k = 0;
            for(int i = 0 ; i < cbFilterDataType.Items.Count;i++)
            {
                if (typeName == (string)cbFilterDataType.Items[i])
                {
                    cbFilterDataType.SelectedIndex = i;
                    break;
                }
            }
           
            int condInd = Filter.GetConditionIndex(editFilter.condition);
            if (condInd > -1 && condInd < cbFilterCondition.Items.Count) cbFilterCondition.SelectedIndex = condInd;
            switch (Filter.ValueTypeID)
            {
                case Constant.WellDataFilter.ValueType.BOOL:
                    if ((bool)editFilter.value)
                        cbFilterVal.SelectedIndex = 0;
                    else
                        cbFilterVal.SelectedIndex = 1;
                    break;
                case Constant.WellDataFilter.ValueType.DATE:
                    dtpFilterVal.Value = ((DateTime)editFilter.value).Date;
                    break;
                case Constant.WellDataFilter.ValueType.NUMBER:
                    tbFilterVal.Text = editFilter.value.ToString();
                    break;
                case Constant.WellDataFilter.ValueType.STRING:
                    tbFilterVal.Text = (string)editFilter.value;
                    break;
            }
            return this.ShowDialog();
        }
        public void SetMinMaxDate(DateTime minDT, DateTime maxDT)
        {
            if ((minDT < maxDT) && (minDT.Year != DateTime.MinValue.Year) && (maxDT.Year != DateTime.MaxValue.Year))
            {
                if (minDT != DateTime.MinValue)
                {
                    dtpFilterVal.MinDate = minDT;
                }
                if (maxDT != DateTime.MaxValue)
                {
                    dtpFilterVal.MaxDate = maxDT.AddMonths(1).AddDays(-1);
                }
                dtpFilterVal.Value = maxDT;
            }
        }

        private void cbFilterDataType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFilterDataType.SelectedIndex > -1)
            {
                int i, ind = -1;
                for (i = 0; i < Constant.WellDataFilter.DATATYPE_NAME.Length; i++)
                {
                    if ((Constant.WellDataFilter.DATATYPE_NAME[i] != "") && (Constant.WellDataFilter.DATATYPE_NAME[i] == cbFilterDataType.Text))
                    {
                        ind = i;
                        break;
                    }
                }
                if (ind > -1)
                {
                    Filter = new WellDataFilter((Constant.WellDataFilter.DataType)i);
                    byte[] condList = Filter.GetCompatibleConditionList();
                    cbFilterCondition.SelectedIndex = -1;
                    cbFilterCondition.Items.Clear();
                    for (i = 0; i < condList.Length; i++)
                    {
                        cbFilterCondition.Items.Add(Constant.WellDataFilter.CONDITION_NAME[condList[i]]);
                    }
                    if (condList.Length > 0) cbFilterCondition.SelectedIndex = 0;
                    cbFilterCondition.Enabled = true;
                    cbFilterVal.Enabled = true;
                    dtpFilterVal.Enabled = true;
                    tbFilterVal.Enabled = true;
                    lComment.Visible = false;
                    switch (Filter.ValueTypeID)
                    {
                        case Constant.WellDataFilter.ValueType.BOOL:
                            cbFilterCondition.SelectedIndex = 0;
                            cbFilterVal.Items.Clear();
                            cbFilterVal.Items.Add("Да");
                            cbFilterVal.Items.Add("Нет");
                            cbFilterVal.SelectedIndex = 0;
                            cbFilterVal.Visible = true;
                            dtpFilterVal.Visible = false;
                            tbFilterVal.Visible = false;
                            break;
                        case Constant.WellDataFilter.ValueType.DATE:
                            cbFilterVal.Visible = false;
                            dtpFilterVal.Visible = true;
                            tbFilterVal.Visible = false;
                            break;
                        case Constant.WellDataFilter.ValueType.NUMBER:
                            goto case Constant.WellDataFilter.ValueType.STRING;
                        case Constant.WellDataFilter.ValueType.STRING:
                            cbFilterVal.Visible = false;
                            dtpFilterVal.Visible = false;
                            tbFilterVal.Visible = true;
                            break;
                    }
                    if ((Filter.TypeID == Constant.WellDataFilter.DataType.MERDATE))
                    {
                        lComment.Visible = true;
                        lComment.Text = "Комментарий:\nФильтр данных 'Дата МЭР' используется для указания периода времени " +
                                        "для расчета показателей за период.";
                    }
                    if (Filter.IsNeedDateFilter)
                    {
                        lComment.Visible = true;
                        lComment.Text = "Комментарий:\nДля указания периода времени используйте" +
                                        " фильтр с типом данных 'Дата МЭР'";
                    }
                }
            }
        }
        private void cbFilterCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((Filter != null) && (cbFilterCondition.SelectedIndex > -1))
            {
                Filter.SetCondition(cbFilterCondition.SelectedIndex);
            }
        }

    }
}
