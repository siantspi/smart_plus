﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus
{
    public sealed class WellDataFilter
    {
        public Constant.WellDataFilter.DataType TypeID;
        public Constant.WellDataFilter.ValueType ValueTypeID;
        public Constant.WellDataFilter.Condition condition;
        public object value;

        public WellDataFilter(Constant.WellDataFilter.DataType FilterType)
        {
            TypeID = FilterType;
            byte type = (byte)TypeID;
            if ((type >= (byte)Constant.WellDataFilter.DataType.WELLNAME) && (type < (byte)Constant.WellDataFilter.DataType.MER_EXIST))
            {
                ValueTypeID = Constant.WellDataFilter.ValueType.STRING;
            }
            else if ((type >= (byte)Constant.WellDataFilter.DataType.MER_EXIST) && (type < (byte)Constant.WellDataFilter.DataType.MERDATE))
            {
                ValueTypeID = Constant.WellDataFilter.ValueType.BOOL;
            }
            else if (type == (byte)Constant.WellDataFilter.DataType.MERDATE)
            {
                ValueTypeID = Constant.WellDataFilter.ValueType.DATE;
            }
            else if (type >= (byte)Constant.WellDataFilter.DataType.LIQ_LAST)
            {
                ValueTypeID = Constant.WellDataFilter.ValueType.NUMBER;
            }
        }
        public WellDataFilter(WellDataFilter Filter) : this(Filter.TypeID)
        {
            condition = Filter.condition;
            value = Filter.value;
        }
        public byte[] GetCompatibleConditionList()
        {
            byte[] result = null;
            switch (ValueTypeID)
            {
                case Constant.WellDataFilter.ValueType.BOOL:
                    result = new byte[1];
                    result[0] = (byte)Constant.WellDataFilter.Condition.EQUAL;
                    break;
                case Constant.WellDataFilter.ValueType.DATE:
                    result = new byte[5];
                    result[0] = (byte)Constant.WellDataFilter.Condition.EQUAL;
                    result[1] = (byte)Constant.WellDataFilter.Condition.GREATER;
                    result[2] = (byte)Constant.WellDataFilter.Condition.GTREAT_OR_EQUAL;
                    result[3] = (byte)Constant.WellDataFilter.Condition.LESS;
                    result[4] = (byte)Constant.WellDataFilter.Condition.LESS_OR_EQUAL;
                    break;
                case Constant.WellDataFilter.ValueType.NUMBER:
                    result = new byte[6];
                    result[0] = (byte)Constant.WellDataFilter.Condition.EQUAL;
                    result[1] = (byte)Constant.WellDataFilter.Condition.NOT_EQUAL;
                    result[2] = (byte)Constant.WellDataFilter.Condition.GREATER;
                    result[3] = (byte)Constant.WellDataFilter.Condition.GTREAT_OR_EQUAL;
                    result[4] = (byte)Constant.WellDataFilter.Condition.LESS;
                    result[5] = (byte)Constant.WellDataFilter.Condition.LESS_OR_EQUAL;
                    break;
                case Constant.WellDataFilter.ValueType.STRING:
                    result = new byte[5];
                    result[0] = (byte)Constant.WellDataFilter.Condition.EQUAL;
                    result[1] = (byte)Constant.WellDataFilter.Condition.NOT_EQUAL;
                    result[2] = (byte)Constant.WellDataFilter.Condition.START_WITH;
                    result[3] = (byte)Constant.WellDataFilter.Condition.END_WITH;
                    result[4] = (byte)Constant.WellDataFilter.Condition.CONTAIN;
                    break; 
            }
            return result;
        }
        public bool IsNeedDateFilter
        {
            get
            {
                if ((TypeID == Constant.WellDataFilter.DataType.LIQ_AVERAGE) ||
                    (TypeID == Constant.WellDataFilter.DataType.OIL_AVERAGE) ||
                    (TypeID == Constant.WellDataFilter.DataType.WINJ_AVERAGE) ||
                    (TypeID == Constant.WellDataFilter.DataType.INJ_AVERAGE) ||
                    (TypeID == Constant.WellDataFilter.DataType.QLIQ_AVERAGE) ||
                    (TypeID == Constant.WellDataFilter.DataType.QOIL_AVERAGE) ||
                    (TypeID == Constant.WellDataFilter.DataType.WATERING_AVERAGE))
                {
                    return true;
                }
                return false;
            }
        }
        public bool TestValue(object testVal)
        {
            switch (ValueTypeID)
            {
                case Constant.WellDataFilter.ValueType.BOOL:
                    return TestValue((bool)testVal);
                case Constant.WellDataFilter.ValueType.DATE:
                    return TestValue((DateTime)testVal);
                case Constant.WellDataFilter.ValueType.NUMBER:
                    switch (TypeID)
                    {
                        case Constant.WellDataFilter.DataType.NATGAS_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.NATGAS_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QNATGAS_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QNATGAS_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.GASCOND_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.GASCOND_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QGASCOND_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QGASCOND_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.LIQ_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.LIQ_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.OIL_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.OIL_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.INJ_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.INJ_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QLIQ_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QLIQ_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QOIL_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QOIL_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.WATERING_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.WINJ_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.WINJ_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.WATERING_LAST:
                            return TestValue((double)testVal);
                        default:
                            return false;
                    }
                case Constant.WellDataFilter.ValueType.STRING:
                    return TestValue((string)testVal);
                default:
                    return false;
            }
        }

        bool TestValue(bool testVal)
        {
            bool res = false;
            switch (condition)
            {
                case Constant.WellDataFilter.Condition.EQUAL:
                    res = (testVal == (bool)value);
                    break;
            }
            return res;
        }
        bool TestValue(DateTime testVal)
        {
            bool res = false;
            switch (condition)
            {
                case Constant.WellDataFilter.Condition.EQUAL:
                    res = (testVal == (DateTime)value);
                    break;
                case Constant.WellDataFilter.Condition.NOT_EQUAL:
                    res = (testVal != (DateTime)value);
                    break;
                case Constant.WellDataFilter.Condition.GREATER:
                    res = (testVal > (DateTime)value);
                    break;
                case Constant.WellDataFilter.Condition.GTREAT_OR_EQUAL:
                    res = (testVal >= (DateTime)value);
                    break;
                case Constant.WellDataFilter.Condition.LESS:
                    res = (testVal < (DateTime)value);
                    break;
                case Constant.WellDataFilter.Condition.LESS_OR_EQUAL:
                    res = (testVal <= (DateTime)value);
                    break;
            }
            return res;
        }
        bool TestValue(double testVal)
        {
            bool res = false;
            switch (condition)
            {
                case Constant.WellDataFilter.Condition.EQUAL:
                    res = (testVal == (double)value);
                    break;
                case Constant.WellDataFilter.Condition.NOT_EQUAL:
                    res = (testVal != (double)value);
                    break;
                case Constant.WellDataFilter.Condition.GREATER:
                    res = (testVal > (double)value);
                    break;
                case Constant.WellDataFilter.Condition.GTREAT_OR_EQUAL:
                    res = (testVal >= (double)value);
                    break;
                case Constant.WellDataFilter.Condition.LESS:
                    res = (testVal < (double)value);
                    break;
                case Constant.WellDataFilter.Condition.LESS_OR_EQUAL:
                    res = (testVal <= (double)value);
                    break;
            }
            return res;
        }
        bool TestValue(float testVal)
        {
            bool res = false;
            switch (condition)
            {
                case Constant.WellDataFilter.Condition.EQUAL:
                    res = (testVal == (float)value);
                    break;
                case Constant.WellDataFilter.Condition.NOT_EQUAL:
                    res = (testVal != (float)value);
                    break;
                case Constant.WellDataFilter.Condition.GREATER:
                    res = (testVal > (float)value);
                    break;
                case Constant.WellDataFilter.Condition.GTREAT_OR_EQUAL:
                    res = (testVal >= (float)value);
                    break;
                case Constant.WellDataFilter.Condition.LESS:
                    res = (testVal < (float)value);
                    break;
                case Constant.WellDataFilter.Condition.LESS_OR_EQUAL:
                    res = (testVal <= (float)value);
                    break;
            }
            return res;
        }
        bool TestValue(int testVal)
        {
            bool res = false;
            switch (condition)
            {
                case Constant.WellDataFilter.Condition.EQUAL:
                    res = (testVal == (int)value);
                    break;
                case Constant.WellDataFilter.Condition.NOT_EQUAL:
                    res = (testVal != (int)value);
                    break;
                case Constant.WellDataFilter.Condition.GREATER:
                    res = (testVal > (int)value);
                    break;
                case Constant.WellDataFilter.Condition.GTREAT_OR_EQUAL:
                    res = (testVal >= (int)value);
                    break;
                case Constant.WellDataFilter.Condition.LESS:
                    res = (testVal < (int)value);
                    break;
                case Constant.WellDataFilter.Condition.LESS_OR_EQUAL:
                    res = (testVal <= (int)value);
                    break;
            }
            return res;
        }
        bool TestValue(string testValue)
        {
            bool res = false;
            string strVal = (string)value;
            int pos;
            if (testValue.Length > strVal.Length)
            {
                switch (condition)
                {
                    case Constant.WellDataFilter.Condition.START_WITH:
                        res = (testValue.IndexOf(strVal, StringComparison.CurrentCultureIgnoreCase) == 0);
                        break;
                    case Constant.WellDataFilter.Condition.END_WITH:
                        res = (testValue.IndexOf(strVal, testValue.Length - strVal.Length, StringComparison.CurrentCultureIgnoreCase) != -1);
                        break;
                    case Constant.WellDataFilter.Condition.CONTAIN:
                        res = (testValue.IndexOf(strVal, StringComparison.CurrentCultureIgnoreCase) > -1);
                        break;
                }
            }
            else if (testValue.Length == strVal.Length)
            {
                switch (condition)
                {
                    case Constant.WellDataFilter.Condition.EQUAL:
                        res = (testValue.ToUpper() == strVal.ToUpper());
                        break;
                    case Constant.WellDataFilter.Condition.NOT_EQUAL:
                        res = (testValue.ToUpper() != strVal.ToUpper());
                        break;
                }
            }
            return res;
        }

        public bool SetCondition(Constant.WellDataFilter.Condition SetCond)
        {
            byte[] CompCondition = this.GetCompatibleConditionList();
            for (int i = 0; i < CompCondition.Length; i++)
            {
                if (CompCondition[i] == (byte)SetCond)
                {
                    condition = SetCond;
                    return true;
                }
            }
            return false;
        }
        public bool SetCondition(int index)
        {
            if (index > -1)
            {
                byte[] CompCondition = this.GetCompatibleConditionList();
                if (index < CompCondition.Length)
                {
                    return this.SetCondition((Constant.WellDataFilter.Condition)CompCondition[index]);
                }
            }
            return false;
        }
        public int GetConditionIndex(Constant.WellDataFilter.Condition cond)
        {
            byte[] condList = this.GetCompatibleConditionList();
            byte testCond = (byte)cond;
            for (int i = 0; i < condList.Length; i++)
            {
                if (condList[i] == testCond) return i;
            }
            return -1;
        }
        public void SetValue(object SetValue)
        {
            switch (ValueTypeID)
            {
                case Constant.WellDataFilter.ValueType.BOOL:
                    value = (bool)SetValue;
                    break;
                case Constant.WellDataFilter.ValueType.DATE:
                    value = (DateTime)SetValue;
                    break;
                case Constant.WellDataFilter.ValueType.NUMBER:
                    switch (TypeID)
                    {
                        case Constant.WellDataFilter.DataType.LIQ_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.LIQ_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.OIL_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.OIL_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.INJ_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.INJ_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QLIQ_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QLIQ_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QOIL_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.QOIL_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.WATERING_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.WINJ_LAST:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.WINJ_AVERAGE:
                            goto case Constant.WellDataFilter.DataType.WATERING_LAST;
                        case Constant.WellDataFilter.DataType.WATERING_LAST:
                            value = (double)SetValue;
                            break;
                        default:
                            value = null;
                            break;
                    }
                    break;
                case Constant.WellDataFilter.ValueType.STRING:
                    value = (string)SetValue;
                    break;
            } 
        }
    }
}
