﻿namespace SmartPlus
{
    partial class WellDataFilterEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bOK = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFilterDataType = new System.Windows.Forms.ComboBox();
            this.cbFilterCondition = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbFilterVal = new System.Windows.Forms.ComboBox();
            this.tbFilterVal = new System.Windows.Forms.TextBox();
            this.dtpFilterVal = new System.Windows.Forms.DateTimePicker();
            this.lComment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOK.Location = new System.Drawing.Point(466, 74);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(100, 30);
            this.bOK.TabIndex = 0;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(572, 74);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(100, 30);
            this.bCancel.TabIndex = 1;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Тип данных фильтра";
            // 
            // cbFilterDataType
            // 
            this.cbFilterDataType.FormattingEnabled = true;
            this.cbFilterDataType.Location = new System.Drawing.Point(12, 27);
            this.cbFilterDataType.MaxDropDownItems = 16;
            this.cbFilterDataType.Name = "cbFilterDataType";
            this.cbFilterDataType.Size = new System.Drawing.Size(323, 23);
            this.cbFilterDataType.TabIndex = 3;
            this.cbFilterDataType.SelectedIndexChanged += new System.EventHandler(this.cbFilterDataType_SelectedIndexChanged);
            // 
            // cbFilterCondition
            // 
            this.cbFilterCondition.FormattingEnabled = true;
            this.cbFilterCondition.Location = new System.Drawing.Point(341, 27);
            this.cbFilterCondition.Name = "cbFilterCondition";
            this.cbFilterCondition.Size = new System.Drawing.Size(181, 23);
            this.cbFilterCondition.TabIndex = 5;
            this.cbFilterCondition.SelectedIndexChanged += new System.EventHandler(this.cbFilterCondition_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(404, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Условие";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(554, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Значение";
            // 
            // cbFilterVal
            // 
            this.cbFilterVal.FormattingEnabled = true;
            this.cbFilterVal.Location = new System.Drawing.Point(528, 27);
            this.cbFilterVal.Name = "cbFilterVal";
            this.cbFilterVal.Size = new System.Drawing.Size(144, 23);
            this.cbFilterVal.TabIndex = 7;
            // 
            // tbFilterVal
            // 
            this.tbFilterVal.Location = new System.Drawing.Point(528, 27);
            this.tbFilterVal.Name = "tbFilterVal";
            this.tbFilterVal.Size = new System.Drawing.Size(144, 23);
            this.tbFilterVal.TabIndex = 8;
            // 
            // dtpFilterVal
            // 
            this.dtpFilterVal.CustomFormat = "MM.yyyy";
            this.dtpFilterVal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFilterVal.Location = new System.Drawing.Point(528, 27);
            this.dtpFilterVal.Name = "dtpFilterVal";
            this.dtpFilterVal.Size = new System.Drawing.Size(144, 23);
            this.dtpFilterVal.TabIndex = 9;
            // 
            // lComment
            // 
            this.lComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lComment.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lComment.Location = new System.Drawing.Point(12, 53);
            this.lComment.Name = "lComment";
            this.lComment.Size = new System.Drawing.Size(448, 51);
            this.lComment.TabIndex = 10;
            this.lComment.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // WellDataFilterEditor
            // 
            this.AcceptButton = this.bOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(684, 116);
            this.Controls.Add(this.lComment);
            this.Controls.Add(this.tbFilterVal);
            this.Controls.Add(this.cbFilterVal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbFilterCondition);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbFilterDataType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.dtpFilterVal);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WellDataFilterEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Фильтр данных скважины";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFilterDataType;
        private System.Windows.Forms.ComboBox cbFilterCondition;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbFilterVal;
        private System.Windows.Forms.TextBox tbFilterVal;
        private System.Windows.Forms.DateTimePicker dtpFilterVal;
        private System.Windows.Forms.Label lComment;
    }
}