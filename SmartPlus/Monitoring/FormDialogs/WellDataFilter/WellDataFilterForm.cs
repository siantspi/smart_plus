﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using RDF.Objects;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public partial class WellDataFilterForm : Form
    {
        MainForm MainForm;
        Project MapProject;
        ArrayList FilterList;
        public Well[] wellList; 
        WellDataFilterEditor WDFilterEditor;

        public WellDataFilterForm(MainForm mainForm)
        {
            InitializeComponent();

            this.MainForm = mainForm;
            this.Owner = mainForm;
            WDFilterEditor = new WellDataFilterEditor();
            WDFilterEditor.Owner = this;
            wellList = null;
            MapProject = null;
            FilterList = new ArrayList();
            dgvFilterList.DoubleClick += new EventHandler(dgvFilterList_DoubleClick);
            this.FormClosing += new FormClosingEventHandler(WellDataFilterForm_FormClosing);
        }

        void dgvFilterList_DoubleClick(object sender, EventArgs e)
        {
            if (dgvFilterList.Rows.Count > 0)
            {
                if (dgvFilterList.SelectedRows.Count > 0)
                {
                    int rowIndex = dgvFilterList.SelectedRows[0].Index;
                    if (WDFilterEditor.ShowEditFilter((WellDataFilter)FilterList[rowIndex]) == DialogResult.OK)
                    {
                        this.EditFilter(rowIndex, WDFilterEditor.Filter);
                    }                   
                }
            }
        }

        void WellDataFilterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((!MainForm.AppRestart) && (this.DialogResult == DialogResult.OK))
            {
                if (FilterList.Count == 0)
                {
                    MessageBox.Show("Для создания списка необходимо задать фильтр.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    bool findDateFilter = false, findNeedDateFilter = false;
                    for (int i = 0; i < FilterList.Count; i++)
                    {
                        if ((!findNeedDateFilter) && (((WellDataFilter)FilterList[i]).IsNeedDateFilter))
                        {
                            findNeedDateFilter = true;
                        }
                        else if ((!findDateFilter) && (((WellDataFilter)FilterList[i]).TypeID == Constant.WellDataFilter.DataType.MERDATE))
                        {
                            findDateFilter = true;
                            break;
                        }
                    }
                    if (findNeedDateFilter && !findDateFilter)
                    {
                        MessageBox.Show("В списке присутствуют фильтры с расчетами значений за период. Для их правильной работы необходимо дополнительно задать фильтр 'Дата МЭР'.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
            }
        }

        public void SetProject(Project proj)
        {
            MapProject = proj;
            WDFilterEditor.SetMinMaxDate(proj.minMerDate, proj.maxMerDate);
        }
        public void ClearProject() { this.MapProject = null; }
        void AddFilter(WellDataFilter filter)
        {
            if (filter != null)
            {
                FilterList.Add(new WellDataFilter(filter));
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dgvFilterList);
                row.Cells[0].Value = Constant.WellDataFilter.DATATYPE_NAME[(int)filter.TypeID];
                row.Cells[1].Value = Constant.WellDataFilter.CONDITION_NAME[(int)filter.condition];
                if(filter.ValueTypeID == Constant.WellDataFilter.ValueType.BOOL)
                {
                    if ((bool)filter.value)
                        row.Cells[2].Value = "Да";
                    else
                        row.Cells[2].Value = "Нет";
                }
                else if (filter.ValueTypeID == Constant.WellDataFilter.ValueType.DATE)
                {
                    row.Cells[2].Value = ((DateTime)filter.value).ToShortDateString();
                }
                else
                {
                    row.Cells[2].Value = filter.value.ToString();
                }
                int ind = dgvFilterList.Rows.Add(row);
                if (ind > -1) dgvFilterList.Rows[ind].Selected = true;
            }
        }
        void EditFilter(int index, WellDataFilter newFilter)
        {
            if ((newFilter != null) && (index > -1) && (index < dgvFilterList.Rows.Count) && (index < FilterList.Count))
            {
                FilterList[index] = new WellDataFilter(newFilter);
                DataGridViewRow row = dgvFilterList.Rows[index];
                row.Cells[0].Value = Constant.WellDataFilter.DATATYPE_NAME[(int)newFilter.TypeID];
                row.Cells[1].Value = Constant.WellDataFilter.CONDITION_NAME[(int)newFilter.condition];
                if (newFilter.ValueTypeID == Constant.WellDataFilter.ValueType.BOOL)
                {
                    if ((bool)newFilter.value)
                        row.Cells[2].Value = "Да";
                    else
                        row.Cells[2].Value = "Нет";
                }
                else if (newFilter.ValueTypeID == Constant.WellDataFilter.ValueType.DATE)
                {
                    row.Cells[2].Value = ((DateTime)newFilter.value).ToShortDateString();
                }
                else
                {
                    row.Cells[2].Value = newFilter.value.ToString();
                }
            }
        }
        void RemoveFilter(int index)
        {
            if ((index > -1) && (index < dgvFilterList.Rows.Count) && (index < FilterList.Count))
            {
                FilterList.RemoveAt(index);
                dgvFilterList.Rows.RemoveAt(index);
            }
        }
        private void bAddFilter_Click(object sender, EventArgs e)
        {
            if (WDFilterEditor.ShowNewFilter() == DialogResult.OK)
            {
                this.AddFilter(WDFilterEditor.Filter);
            }
        }

        private void bEditFilter_Click(object sender, EventArgs e)
        {
            if ((dgvFilterList.Rows.Count > 0) && (dgvFilterList.SelectedRows.Count > 0))
            {
                int rowIndex = dgvFilterList.SelectedRows[0].Index;
                if (WDFilterEditor.ShowEditFilter((WellDataFilter)FilterList[rowIndex]) == DialogResult.OK)
                {
                    this.EditFilter(rowIndex, WDFilterEditor.Filter);
                }
            }
        }
        private void bDelFilter_Click(object sender, EventArgs e)
        {
            if ((dgvFilterList.Rows.Count > 0) && (dgvFilterList.SelectedRows.Count > 0))
            {
                RemoveFilter(dgvFilterList.SelectedRows[0].Index);
            }
        }
        void FillFilterIndexListByDataType(Constant.WellDataFilter.DataType dataType, ArrayList list)
        {
            if ((FilterList != null) && (FilterList.Count > 0))
            {
                list.Clear();
                for (int i = 0; i < FilterList.Count; i++)
                {
                    if (((WellDataFilter)FilterList[i]).TypeID == dataType)
                    {
                        list.Add(i);
                    }
                }
            }
        }

        // ОБРАБОТКА ДАННЫХ ПО ФИЛЬТРУ
        public void FillWellList(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, j, k, p;
            WellDataFilter filter;
            WorkerState userState = new WorkerState();

            userState.WorkMainTitle = "Создание списка скважин по фильтру...";
            userState.WorkCurrentTitle = "Создание списка скважин по фильтру...";
            if (MapProject != null)
            {
                wellList = null;
                bool[] FilterResult = new bool[FilterList.Count];
                ArrayList list = new ArrayList();
                ArrayList welllist = new ArrayList();
                bool res, ofResult, wellResult;
                byte CharWork = 0;
                int mer = 0, gis = 0, chess = 0, logs_base = 0, core = 0, core_test = 0;
                OilField of;
                Well w;
                
                var dict = (OilFieldAreaDictionary)MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                double sumLiq, sumOil, sumInj, workTime, coolTime, sumWorkTime, sumCoolTime;
                double sumNatGas, sumGasCond;
                DateTime predDT = DateTime.MinValue;
                MerItem merItem;
                if (dict == null)
                {
                    e.Result = "Не загружен справочник 'Площадь'.\nПереустановите приложение.";
                }
                for (i = 0; i < FilterList.Count; i++)
                {
                    byte type = (byte)((WellDataFilter)FilterList[i]).TypeID;
                    if ((type >= (byte)Constant.WellDataFilter.DataType.LIQ_LAST) &&
                        (type <= (byte)Constant.WellDataFilter.DataType.QGASCOND_AVERAGE) &&
                        (mer < 2))
                    {
                        mer = 2;
                    }
                    if ((type == (byte)Constant.WellDataFilter.DataType.MER_EXIST) && (mer < 1)) mer = 1;
                    if ((type == (byte)Constant.WellDataFilter.DataType.GIS_EXIST) && (gis < 1)) gis = 1;
                    if ((type == (byte)Constant.WellDataFilter.DataType.CHESS_EXIST) && (chess < 1)) chess = 1;
                    if ((type == (byte)Constant.WellDataFilter.DataType.LOGS_EXIST) && (logs_base < 1)) logs_base = 1;
                    if ((type == (byte)Constant.WellDataFilter.DataType.CORE_EXIST) && (logs_base < 1)) core = 1;
                    if ((type == (byte)Constant.WellDataFilter.DataType.CORE_TEST_EXIST) && (logs_base < 1)) core_test = 1;
                }
                worker.ReportProgress(0, userState);
                for (i = 1; i < MapProject.OilFields.Count; i++)
                {
                    worker.ReportProgress(100, userState);
                    // очистка результатов фильтров
                    for (k = 0; k < FilterResult.Length; k++) FilterResult[k] = false;
                    ofResult = true;
                    of = (OilField)MapProject.OilFields[i];

                    // фильтры имени месторождения
                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.OILFIELDNAME, list);
                    if (list.Count > 0)
                    {
                        for (k = 0; k < list.Count; k++)
                        {
                            filter = (WellDataFilter)FilterList[(int)list[k]];
                            res = filter.TestValue(of.Name);
                            FilterResult[(int)list[k]] = res;
                            ofResult = ofResult && res;
                        }
                        if (!ofResult) continue;
                    }

                    // фильтры имени НГДУ
                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.NGDUNAME, list);
                    if (list.Count > 0)
                    {
                        for (k = 0; k < list.Count; k++)
                        {
                            filter = (WellDataFilter)FilterList[(int)list[k]];
                            res = filter.TestValue(of.ParamsDict.NGDUName);
                            FilterResult[(int)list[k]] = res;
                            ofResult = ofResult && res;
                        }
                        if (!ofResult) continue;
                    }


                    // загрузка данных по месторождению
                    if (mer > 0) of.LoadMerFromCache(worker, e, (mer < 2), false);
                    if (gis > 0) of.LoaвGisFromCache(worker, e, (gis < 2), false);
                    if (chess > 0) of.LoadChessFromCache(worker, e, (chess < 2), false);
                    if (logs_base > 0) of.LoadLogsBaseFromCache(worker, e, (logs_base < 2), (logs_base < 2));
                    if (core > 0) of.LoadCoreFromCache(worker, e, (core < 2));
                    if (core_test > 0) of.LoadCoreTestFromCache(worker, e, (core_test < 2));


                    userState.WorkCurrentTitle = "Создание списка скважин по фильтру...";
                    userState.Element = of.Name;
                    if (mer == 0) worker.ReportProgress(100, userState);
                    if (gis == 0) worker.ReportProgress(100, userState);
                    if (chess == 0) worker.ReportProgress(100, userState);
                    if (logs_base == 0) worker.ReportProgress(100, userState);
                    if (core == 0) worker.ReportProgress(100, userState);
                    if (core_test == 0) worker.ReportProgress(100, userState);

                    worker.ReportProgress(0, userState);

                    // по скважинам
                    for (j = 0; j < of.Wells.Count; j++)
                    {
                        w = of.Wells[j];
                        if (w.Missing) continue;
                        wellResult = ofResult;

                        // фильтры по имени площади
                        FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.OILFIELDAREANAME, list);
                        if (list.Count > 0)
                        {
                            for (k = 0; k < list.Count; k++)
                            {
                                filter = (WellDataFilter)FilterList[(int)list[k]];
                                res = filter.TestValue(dict.GetShortNameByCode(w.OilFieldAreaCode));
                                FilterResult[(int)list[k]] = res;
                                wellResult = wellResult && res;
                            }
                            if (!wellResult) continue;
                        }

                        // фильтры имени скважины
                        FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.WELLNAME, list);
                        if (list.Count > 0)
                        {
                            for (k = 0; k < list.Count; k++)
                            {
                                filter = (WellDataFilter)FilterList[(int)list[k]];
                                res = filter.TestValue(w.UpperCaseName);
                                FilterResult[(int)list[k]] = res;
                                wellResult = wellResult && res;
                            }
                            if (!wellResult) continue;
                        }

                        #region Обработка данных МЭР
                        if (mer > 0)
                        {
                            // фильтры наличия мер
                            FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.MER_EXIST, list);
                            if (list.Count > 0)
                            {
                                res = false;
                                for (k = 0; k < list.Count; k++)
                                {
                                    filter = (WellDataFilter)FilterList[(int)list[k]];
                                    if (!res) res = filter.TestValue(w.MerLoaded);
                                    FilterResult[(int)list[k]] = res;
                                    wellResult = wellResult && res;
                                }
                                if (!wellResult) continue;
                            }
                            if ((w.MerLoaded) && (mer == 2))
                            {
                                // находим интервал дат
                                DateTime minDate = DateTime.MinValue, maxDate = DateTime.MaxValue;
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.MERDATE, list);
                                if (list.Count > 0)
                                {
                                    res = false;
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        if (!res)
                                        {
                                            if (filter.condition == Constant.WellDataFilter.Condition.EQUAL)
                                            {
                                                res = true;
                                                minDate = (DateTime)filter.value;
                                                maxDate = (DateTime)filter.value;
                                            }
                                            else if (filter.condition == Constant.WellDataFilter.Condition.GREATER)
                                            {
                                                if (minDate < (DateTime)filter.value) minDate = ((DateTime)filter.value);
                                            }
                                            else if (filter.condition == Constant.WellDataFilter.Condition.GTREAT_OR_EQUAL)
                                            {
                                                if (minDate < (DateTime)filter.value) minDate = ((DateTime)filter.value).AddMonths(-1);
                                            }
                                            if (filter.condition == Constant.WellDataFilter.Condition.LESS)
                                            {
                                                if (maxDate > (DateTime)filter.value) maxDate = (DateTime)filter.value;
                                            }
                                            else if (filter.condition == Constant.WellDataFilter.Condition.LESS_OR_EQUAL)
                                            {
                                                if (maxDate > (DateTime)filter.value) maxDate = ((DateTime)filter.value).AddMonths(1);
                                            }
                                        }
                                    }
                                    if (!wellResult) continue;
                                }
                                if (minDate == DateTime.MinValue) minDate = w.mer.Items[0].Date.AddMonths(-1);
                                if (maxDate == DateTime.MaxValue)
                                {
                                    if (of.maxMerDate != w.mer.Items[w.mer.Items.Length - 1].Date) continue;
                                    maxDate = w.mer.Items[w.mer.Items.Length - 1].Date.AddMonths(1);
                                }

                                res = false;
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.LIQ_AVERAGE, list);
                                res = (list.Count > 0);
                                if (!res)
                                {
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.OIL_AVERAGE, list);
                                    res = (list.Count > 0);
                                }
                                if (!res)
                                {
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.INJ_AVERAGE, list);
                                    res = (list.Count > 0);
                                }
                                if (!res)
                                {
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QLIQ_AVERAGE, list);
                                    res = (list.Count > 0);
                                }
                                if (!res)
                                {
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QOIL_AVERAGE, list);
                                    res = (list.Count > 0);
                                }
                                if (!res)
                                {
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.WATERING_AVERAGE, list);
                                    res = (list.Count > 0);
                                }
                                if (!res)
                                {
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.WINJ_AVERAGE, list);
                                    res = (list.Count > 0);
                                }

                                if (res)
                                {
                                    // находим среднее за интервал дат
                                    int month = 0;
                                    sumLiq = 0; sumOil = 0; sumInj = 0; sumNatGas = 0; sumGasCond = 0;
                                    workTime = 0; coolTime = 0;
                                    sumWorkTime = 0; sumCoolTime = 0;
                                    predDT = DateTime.MinValue;
                                    CharWork = 0;
                                    for (p = 0; p < w.mer.Count; p++)
                                    {
                                        merItem = w.mer.Items[p];
                                        if (((minDate == maxDate) && (merItem.Date == minDate)) ||
                                            ((minDate != maxDate) && (minDate < merItem.Date) && (merItem.Date < maxDate)))
                                        {
                                            if (predDT.Year == 1) predDT = merItem.Date;
                                            if (predDT == merItem.Date)
                                            {
                                                if (workTime != merItem.WorkTime) workTime += merItem.WorkTime;
                                                if (coolTime != merItem.CollTime) coolTime += merItem.CollTime;
                                            }
                                            else
                                            {
                                                predDT = merItem.Date;
                                                sumWorkTime += workTime;
                                                sumCoolTime += coolTime;
                                                workTime = merItem.WorkTime;
                                                coolTime = merItem.CollTime;
                                                month++;
                                            }
                                            if (merItem.CharWorkId == 11)
                                            {
                                                sumLiq += merItem.Oil + merItem.Wat;
                                                sumOil += merItem.Oil;
                                                CharWork = 1;
                                            }
                                            else if (merItem.CharWorkId == 12)
                                            {
                                                sumNatGas += w.mer.GetItemEx(p).NaturalGas;
                                                CharWork = 1;
                                            }
                                            else if (merItem.CharWorkId == 15)
                                            {
                                                sumGasCond += w.mer.GetItemEx(p).GasCondensate;
                                                CharWork = 1;
                                            }
                                            else if (merItem.CharWorkId == 20)
                                            {
                                                sumInj += w.mer.GetItemEx(p).Injection;
                                                CharWork = 2;
                                            }
                                        }
                                    }
                                    sumWorkTime += workTime;
                                    sumCoolTime += coolTime;
                                    month++;

                                    // находим средние дебиты/приемистость - добычу/закачку
                                    double LiqAverage = 0, OilAverage = 0, InjAverage = 0;
                                    double NatGasAverage = 0, GasCondAverage = 0;
                                    double QLiqAverage = 0, QOilAverage = 0, WatAverage = 0, WInjAverage = 0;
                                    double QNatGasAverage = 0, QGasCondAverage = 0;

                                    if (sumWorkTime + sumCoolTime > 0)
                                    {
                                        if (CharWork == 1)
                                        {
                                            LiqAverage = sumLiq / month;
                                            OilAverage = sumOil / month;
                                            NatGasAverage = sumNatGas / month;
                                            GasCondAverage = sumGasCond / month;
                                            QLiqAverage = sumLiq * 24 / (sumWorkTime + sumCoolTime);
                                            QOilAverage = sumOil * 24 / (sumWorkTime + sumCoolTime);
                                            QNatGasAverage = sumNatGas * 24 / (sumWorkTime + sumCoolTime);
                                            QGasCondAverage = sumGasCond * 24 / (sumWorkTime + sumCoolTime);
                                            WatAverage = (sumLiq - sumOil) * 100 / sumLiq;
                                        }
                                        else if (CharWork == 2)
                                        {
                                            WInjAverage = sumInj * 24 / (sumWorkTime + sumCoolTime);
                                            InjAverage = sumInj / month;
                                        }
                                    }
                                    else continue;


                                    // фильтры по средним параметрам
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.LIQ_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(LiqAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.NATGAS_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(NatGasAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.GASCOND_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(GasCondAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.OIL_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(OilAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QLIQ_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(QLiqAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QNATGAS_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(QNatGasAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QGASCOND_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(QGasCondAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QOIL_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(QOilAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.WATERING_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(WatAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 1);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.INJ_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(InjAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 2);
                                        }
                                        if (!wellResult) continue;
                                    }
                                    FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.WINJ_AVERAGE, list);
                                    if (list.Count > 0)
                                    {
                                        for (k = 0; k < list.Count; k++)
                                        {
                                            filter = (WellDataFilter)FilterList[(int)list[k]];
                                            res = filter.TestValue(WInjAverage);
                                            FilterResult[(int)list[k]] = res;
                                            wellResult = wellResult && res;
                                            wellResult = wellResult && (CharWork == 2);
                                        }
                                        if (!wellResult) continue;
                                    }
                                }

                                // находим последние значения
                                double LiqLast = 0, OilLast = 0, InjLast = 0, NatGasLast = 0, GasCondLast = 0;
                                double QLiqLast = 0, QOilLast = 0, WatLast = 0, WinjLast = 0, QNatGasLast = 0, QGasCondLast = 0;
                                sumLiq = 0; sumOil = 0; sumInj = 0; sumNatGas = 0; sumGasCond = 0;
                                workTime = 0; coolTime = 0;
                                sumWorkTime = 0; sumCoolTime = 0;
                                predDT = DateTime.MinValue;
                                CharWork = 0;
                                for (p = w.mer.Count - 1; p >= 0; p--)
                                {
                                    merItem = w.mer.Items[p];
                                    if (((minDate == maxDate) && (merItem.Date == minDate)) ||
                                        ((minDate != maxDate) && (maxDate.AddMonths(-2) < merItem.Date) && (merItem.Date < maxDate)))
                                    {
                                        if (workTime != merItem.WorkTime) workTime += merItem.WorkTime;
                                        if (coolTime != merItem.CollTime) coolTime += merItem.CollTime;

                                        if (merItem.CharWorkId == 11)
                                        {
                                            sumLiq += merItem.Oil + merItem.Wat;
                                            sumOil += merItem.Oil;
                                            CharWork = 1;
                                        }
                                        else if (merItem.CharWorkId == 12)
                                        {
                                            sumNatGas += w.mer.GetItemEx(p).NaturalGas;
                                            CharWork = 1;
                                        }
                                        else if (merItem.CharWorkId == 15)
                                        {
                                            sumGasCond += w.mer.GetItemEx(p).GasCondensate;
                                            CharWork = 1;
                                        }
                                        else if (merItem.CharWorkId == 20)
                                        {
                                            sumInj += w.mer.GetItemEx(p).Injection;
                                            CharWork = 2;
                                        }
                                    }
                                    else if ((minDate != maxDate) && (merItem.Date <= maxDate.AddMonths(-2))) break;
                                }
                                sumWorkTime += workTime;
                                sumCoolTime += coolTime;
                                if (sumWorkTime + sumCoolTime > 0)
                                {
                                    if (CharWork == 1)
                                    {
                                        LiqLast = sumLiq;
                                        OilLast = sumOil;
                                        NatGasLast = sumNatGas;
                                        GasCondLast = sumGasCond;
                                        QLiqLast = sumLiq * 24 / (sumWorkTime + sumCoolTime);
                                        QOilLast = sumOil * 24 / (sumWorkTime + sumCoolTime);
                                        QNatGasLast = sumNatGas * 24 / (sumWorkTime + sumCoolTime);
                                        QGasCondLast = sumGasCond * 24 / (sumWorkTime + sumCoolTime);
                                        if (sumLiq > 0) WatLast = (sumLiq - sumOil) * 100 / sumLiq;
                                    }
                                    else if (CharWork == 2)
                                    {
                                        InjLast = sumInj;
                                        WinjLast = sumInj * 24 / (sumWorkTime + sumCoolTime);
                                    }
                                }
                                else continue;

                                // фильтры по последним параметрам
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.LIQ_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(LiqLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.NATGAS_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(NatGasLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.GASCOND_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(GasCondLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.OIL_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(OilLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QLIQ_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(QLiqLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QNATGAS_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(QNatGasLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QGASCOND_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(QGasCondLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.QOIL_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(QOilLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.WATERING_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(WatLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 1);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.INJ_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(InjLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 2);
                                    }
                                    if (!wellResult) continue;
                                }
                                FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.WINJ_LAST, list);
                                if (list.Count > 0)
                                {
                                    for (k = 0; k < list.Count; k++)
                                    {
                                        filter = (WellDataFilter)FilterList[(int)list[k]];
                                        res = filter.TestValue(WinjLast);
                                        FilterResult[(int)list[k]] = res;
                                        wellResult = wellResult && res;
                                        wellResult = wellResult && (CharWork == 2);
                                    }
                                    if (!wellResult) continue;
                                }
                            }
                            else if (!w.MerLoaded && mer == 2)
                            {
                                wellResult = false;
                            }
                        }
                        #endregion

                        #region Обработка данных ГИС
                        if (gis == 1)
                        {
                            // фильтры наличия гис
                            FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.GIS_EXIST, list);
                            if (list.Count > 0)
                            {
                                res = false;
                                for (k = 0; k < list.Count; k++)
                                {
                                    filter = (WellDataFilter)FilterList[(int)list[k]];
                                    if (!res)
                                    {
                                        res = filter.TestValue(w.gis != null);
                                    }
                                    FilterResult[(int)list[k]] = res;
                                    wellResult = wellResult && res;
                                }
                                if (!wellResult) continue;
                            }
                        }
                        #endregion

                        #region Обработка данных Шахматки
                        if (chess == 1)
                        {
                            // фильтры наличия шахматки
                            FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.CHESS_EXIST, list);
                            if (list.Count > 0)
                            {
                                res = false;
                                for (k = 0; k < list.Count; k++)
                                {
                                    filter = (WellDataFilter)FilterList[(int)list[k]];
                                    if (!res)
                                    {
                                        res = filter.TestValue(w.chess != null) || filter.TestValue(w.chessInj != null);
                                    }
                                    FilterResult[(int)list[k]] = res;
                                    wellResult = wellResult && res;
                                }
                                if (!wellResult) continue;
                            }
                        }
                        #endregion

                        #region Обработка данных базового каротажа
                        if (logs_base == 1)
                        {
                            // фильтры наличия гис
                            FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.LOGS_EXIST, list);
                            if (list.Count > 0)
                            {
                                res = false;
                                for (k = 0; k < list.Count; k++)
                                {
                                    filter = (WellDataFilter)FilterList[(int)list[k]];
                                    if (!res)
                                    {
                                        res = filter.TestValue(w.LogsBaseLoaded);
                                    }
                                    FilterResult[(int)list[k]] = res;
                                    wellResult = wellResult && res;
                                }
                                if (!wellResult) continue;
                            }
                        }
                        #endregion

                        #region Обработка данных данных наличия керна
                        if (core == 1)
                        {
                            // фильтры наличия гис
                            FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.CORE_EXIST, list);
                            if (list.Count > 0)
                            {
                                res = false;
                                for (k = 0; k < list.Count; k++)
                                {
                                    filter = (WellDataFilter)FilterList[(int)list[k]];
                                    if (!res)
                                    {
                                        res = filter.TestValue(w.core != null);
                                    }
                                    FilterResult[(int)list[k]] = res;
                                    wellResult = wellResult && res;
                                }
                                if (!wellResult) continue;
                            }
                        }
                        #endregion

                        #region Обработка данных исследований керна
                        if (core_test == 1)
                        {
                            // фильтры наличия гис
                            FillFilterIndexListByDataType(Constant.WellDataFilter.DataType.CORE_TEST_EXIST, list);
                            if (list.Count > 0)
                            {
                                res = false;
                                for (k = 0; k < list.Count; k++)
                                {
                                    filter = (WellDataFilter)FilterList[(int)list[k]];
                                    if (!res)
                                    {
                                        res = filter.TestValue(w.coreTest != null);
                                    }
                                    FilterResult[(int)list[k]] = res;
                                    wellResult = wellResult && res;
                                }
                                if (!wellResult) continue;
                            }
                        }
                        #endregion

                        // добавляем скважину
                        if (wellResult)
                        {
                            welllist.Add(w);
                        }
                    }
                    // освобождение памяти
                    if (mer > 0) of.ClearMerData(mer == 2);
                    if (gis > 0) of.ClearGisData(gis == 2);
                    if (chess > 0) of.ClearChessData(chess == 2);
                    if (logs_base > 0) of.ClearLogsBaseData(logs_base == 2);
                    if (core > 0) of.ClearCoreData(core == 2);
                    if (core_test > 0) of.ClearCoreTestData(core_test == 2);
                    GC.GetTotalMemory(true);
                }
                if (welllist.Count > 0)
                {
                    wellList = new Well[welllist.Count];
                    for (i = 0; i < welllist.Count; i++)
                    {
                        wellList[i] = (Well)welllist[i];
                    }
                }
            }
            
        }
    }
}
