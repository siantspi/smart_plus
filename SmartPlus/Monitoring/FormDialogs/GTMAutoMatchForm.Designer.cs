﻿namespace SmartPlus.Dialogs
{
    partial class GTMAutoMatchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatesGroupBox = new System.Windows.Forms.GroupBox();
            this.EndDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.StartDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.GtmListListView = new System.Windows.Forms.ListView();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ExtraSettingsButton = new System.Windows.Forms.Button();
            this.ExtraSettingGroup = new System.Windows.Forms.GroupBox();
            this.RcUpDown = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.ReUpDown = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.UsePiezometricWells = new System.Windows.Forms.CheckBox();
            this.CalcMultiPlastCodeCheckBox = new System.Windows.Forms.CheckBox();
            this.StartSkinUpDown = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.RadiusFindWellUpDown = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.WellListTextBoxEx = new SmartPlus.TextBoxEx();
            this.MaxSkinUpDown = new System.Windows.Forms.NumericUpDown();
            this.MinSkinUpDown = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AddWellListButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TemplatePathBox = new System.Windows.Forms.TextBox();
            this.TemplateOpenButton = new System.Windows.Forms.Button();
            this.DatesGroupBox.SuspendLayout();
            this.ExtraSettingGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RcUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartSkinUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadiusFindWellUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxSkinUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinSkinUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // DatesGroupBox
            // 
            this.DatesGroupBox.Controls.Add(this.EndDatePicker);
            this.DatesGroupBox.Controls.Add(this.label2);
            this.DatesGroupBox.Controls.Add(this.StartDatePicker);
            this.DatesGroupBox.Controls.Add(this.label1);
            this.DatesGroupBox.Location = new System.Drawing.Point(14, 14);
            this.DatesGroupBox.Name = "DatesGroupBox";
            this.DatesGroupBox.Size = new System.Drawing.Size(403, 77);
            this.DatesGroupBox.TabIndex = 0;
            this.DatesGroupBox.TabStop = false;
            this.DatesGroupBox.Text = "Интервал дат поиска ГТМ";
            // 
            // EndDatePicker
            // 
            this.EndDatePicker.CustomFormat = "";
            this.EndDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.EndDatePicker.Location = new System.Drawing.Point(195, 37);
            this.EndDatePicker.Name = "EndDatePicker";
            this.EndDatePicker.Size = new System.Drawing.Size(199, 23);
            this.EndDatePicker.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Дата окончания интервала поиска";
            // 
            // StartDatePicker
            // 
            this.StartDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.StartDatePicker.Location = new System.Drawing.Point(9, 37);
            this.StartDatePicker.Name = "StartDatePicker";
            this.StartDatePicker.Size = new System.Drawing.Size(180, 23);
            this.StartDatePicker.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Дата начала интервала поиска";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Выберите опорные ГТМ для поиска:";
            // 
            // GtmListListView
            // 
            this.GtmListListView.CheckBoxes = true;
            this.GtmListListView.HideSelection = false;
            this.GtmListListView.Location = new System.Drawing.Point(15, 161);
            this.GtmListListView.MultiSelect = false;
            this.GtmListListView.Name = "GtmListListView";
            this.GtmListListView.ShowGroups = false;
            this.GtmListListView.ShowItemToolTips = true;
            this.GtmListListView.Size = new System.Drawing.Size(403, 119);
            this.GtmListListView.TabIndex = 3;
            this.GtmListListView.UseCompatibleStateImageBehavior = false;
            this.GtmListListView.View = System.Windows.Forms.View.List;
            this.GtmListListView.SelectedIndexChanged += new System.EventHandler(this.GtmListListView_SelectedIndexChanged);
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Location = new System.Drawing.Point(572, 319);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(90, 30);
            this.OKButton.TabIndex = 4;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(668, 319);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 5;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ExtraSettingsButton
            // 
            this.ExtraSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ExtraSettingsButton.AutoSize = true;
            this.ExtraSettingsButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.ExtraSettingsButton.FlatAppearance.BorderSize = 0;
            this.ExtraSettingsButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.ExtraSettingsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExtraSettingsButton.Image = global::SmartPlus.Properties.Resources.right;
            this.ExtraSettingsButton.Location = new System.Drawing.Point(629, 283);
            this.ExtraSettingsButton.Name = "ExtraSettingsButton";
            this.ExtraSettingsButton.Size = new System.Drawing.Size(129, 30);
            this.ExtraSettingsButton.TabIndex = 6;
            this.ExtraSettingsButton.Text = "Дополнительно";
            this.ExtraSettingsButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ExtraSettingsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ExtraSettingsButton.UseVisualStyleBackColor = false;
            this.ExtraSettingsButton.Click += new System.EventHandler(this.ExtraSettingsButton_Click);
            this.ExtraSettingsButton.MouseEnter += new System.EventHandler(this.ExtraSettingsButton_MouseEnter);
            this.ExtraSettingsButton.MouseLeave += new System.EventHandler(this.ExtraSettingsButton_MouseLeave);
            // 
            // ExtraSettingGroup
            // 
            this.ExtraSettingGroup.Controls.Add(this.RcUpDown);
            this.ExtraSettingGroup.Controls.Add(this.label12);
            this.ExtraSettingGroup.Controls.Add(this.ReUpDown);
            this.ExtraSettingGroup.Controls.Add(this.label11);
            this.ExtraSettingGroup.Controls.Add(this.UsePiezometricWells);
            this.ExtraSettingGroup.Controls.Add(this.CalcMultiPlastCodeCheckBox);
            this.ExtraSettingGroup.Controls.Add(this.StartSkinUpDown);
            this.ExtraSettingGroup.Controls.Add(this.label10);
            this.ExtraSettingGroup.Controls.Add(this.RadiusFindWellUpDown);
            this.ExtraSettingGroup.Controls.Add(this.label9);
            this.ExtraSettingGroup.Controls.Add(this.WellListTextBoxEx);
            this.ExtraSettingGroup.Controls.Add(this.MaxSkinUpDown);
            this.ExtraSettingGroup.Controls.Add(this.MinSkinUpDown);
            this.ExtraSettingGroup.Controls.Add(this.label7);
            this.ExtraSettingGroup.Controls.Add(this.label6);
            this.ExtraSettingGroup.Controls.Add(this.label5);
            this.ExtraSettingGroup.Controls.Add(this.AddWellListButton);
            this.ExtraSettingGroup.Controls.Add(this.label4);
            this.ExtraSettingGroup.Location = new System.Drawing.Point(426, 14);
            this.ExtraSettingGroup.Name = "ExtraSettingGroup";
            this.ExtraSettingGroup.Size = new System.Drawing.Size(326, 266);
            this.ExtraSettingGroup.TabIndex = 7;
            this.ExtraSettingGroup.TabStop = false;
            this.ExtraSettingGroup.Text = " Дополнительные настройки ";
            // 
            // RcUpDown
            // 
            this.RcUpDown.DecimalPlaces = 3;
            this.RcUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.RcUpDown.Location = new System.Drawing.Point(201, 136);
            this.RcUpDown.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.RcUpDown.Name = "RcUpDown";
            this.RcUpDown.Size = new System.Drawing.Size(98, 23);
            this.RcUpDown.TabIndex = 18;
            this.RcUpDown.Value = new decimal(new int[] {
            72,
            0,
            0,
            196608});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(173, 138);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 15);
            this.label12.TabIndex = 17;
            this.label12.Text = "Rс:";
            // 
            // ReUpDown
            // 
            this.ReUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ReUpDown.Location = new System.Drawing.Point(56, 136);
            this.ReUpDown.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.ReUpDown.Name = "ReUpDown";
            this.ReUpDown.Size = new System.Drawing.Size(95, 23);
            this.ReUpDown.TabIndex = 16;
            this.ReUpDown.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 138);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 15);
            this.label11.TabIndex = 15;
            this.label11.Text = "Re:";
            // 
            // UsePiezometricWells
            // 
            this.UsePiezometricWells.AutoSize = true;
            this.UsePiezometricWells.Checked = true;
            this.UsePiezometricWells.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UsePiezometricWells.Location = new System.Drawing.Point(9, 103);
            this.UsePiezometricWells.Name = "UsePiezometricWells";
            this.UsePiezometricWells.Size = new System.Drawing.Size(290, 19);
            this.UsePiezometricWells.TabIndex = 14;
            this.UsePiezometricWells.Text = "Учет пьезометр.скважин при расчете давлений";
            this.UsePiezometricWells.UseVisualStyleBackColor = true;
            // 
            // CalcMultiPlastCodeCheckBox
            // 
            this.CalcMultiPlastCodeCheckBox.AutoSize = true;
            this.CalcMultiPlastCodeCheckBox.Location = new System.Drawing.Point(9, 237);
            this.CalcMultiPlastCodeCheckBox.Name = "CalcMultiPlastCodeCheckBox";
            this.CalcMultiPlastCodeCheckBox.Size = new System.Drawing.Size(306, 19);
            this.CalcMultiPlastCodeCheckBox.TabIndex = 13;
            this.CalcMultiPlastCodeCheckBox.Text = "Расчет многопластовых скважин как один объект";
            this.CalcMultiPlastCodeCheckBox.UseVisualStyleBackColor = true;
            // 
            // StartSkinUpDown
            // 
            this.StartSkinUpDown.DecimalPlaces = 1;
            this.StartSkinUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.StartSkinUpDown.Location = new System.Drawing.Point(264, 198);
            this.StartSkinUpDown.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.StartSkinUpDown.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            -2147483648});
            this.StartSkinUpDown.Name = "StartSkinUpDown";
            this.StartSkinUpDown.Size = new System.Drawing.Size(54, 23);
            this.StartSkinUpDown.TabIndex = 12;
            this.StartSkinUpDown.ValueChanged += new System.EventHandler(this.StartSkinUpDown_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(217, 200);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 15);
            this.label10.TabIndex = 11;
            this.label10.Text = "Старт:";
            // 
            // RadiusFindWellUpDown
            // 
            this.RadiusFindWellUpDown.Location = new System.Drawing.Point(220, 73);
            this.RadiusFindWellUpDown.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.RadiusFindWellUpDown.Name = "RadiusFindWellUpDown";
            this.RadiusFindWellUpDown.Size = new System.Drawing.Size(98, 23);
            this.RadiusFindWellUpDown.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(208, 15);
            this.label9.TabIndex = 9;
            this.label9.Text = "Радиус поиска скважин окружения:";
            // 
            // WellListTextBoxEx
            // 
            this.WellListTextBoxEx.BackColor = System.Drawing.Color.White;
            this.WellListTextBoxEx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WellListTextBoxEx.Location = new System.Drawing.Point(45, 39);
            this.WellListTextBoxEx.Name = "WellListTextBoxEx";
            this.WellListTextBoxEx.ReadOnly = false;
            this.WellListTextBoxEx.Size = new System.Drawing.Size(274, 21);
            this.WellListTextBoxEx.TabIndex = 8;
            this.WellListTextBoxEx.ToolTipText = null;
            // 
            // MaxSkinUpDown
            // 
            this.MaxSkinUpDown.DecimalPlaces = 1;
            this.MaxSkinUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.MaxSkinUpDown.Location = new System.Drawing.Point(157, 198);
            this.MaxSkinUpDown.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.MaxSkinUpDown.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            -2147483648});
            this.MaxSkinUpDown.Name = "MaxSkinUpDown";
            this.MaxSkinUpDown.Size = new System.Drawing.Size(54, 23);
            this.MaxSkinUpDown.TabIndex = 7;
            this.MaxSkinUpDown.ValueChanged += new System.EventHandler(this.MaxSkinUpDown_ValueChanged);
            // 
            // MinSkinUpDown
            // 
            this.MinSkinUpDown.DecimalPlaces = 1;
            this.MinSkinUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.MinSkinUpDown.Location = new System.Drawing.Point(47, 198);
            this.MinSkinUpDown.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.MinSkinUpDown.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            -2147483648});
            this.MinSkinUpDown.Name = "MinSkinUpDown";
            this.MinSkinUpDown.Size = new System.Drawing.Size(58, 23);
            this.MinSkinUpDown.TabIndex = 6;
            this.MinSkinUpDown.ValueChanged += new System.EventHandler(this.MinSkinUpDown_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(111, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "Макс:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Мин:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(261, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Интервал изменения скина выбранного ГТМ:";
            // 
            // AddWellListButton
            // 
            this.AddWellListButton.Image = global::SmartPlus.Properties.Resources.MoveNextBlue16;
            this.AddWellListButton.Location = new System.Drawing.Point(9, 37);
            this.AddWellListButton.Name = "AddWellListButton";
            this.AddWellListButton.Size = new System.Drawing.Size(30, 23);
            this.AddWellListButton.TabIndex = 2;
            this.AddWellListButton.UseVisualStyleBackColor = true;
            this.AddWellListButton.Click += new System.EventHandler(this.AddWellListButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Расчеты по списку скважин:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 15);
            this.label8.TabIndex = 8;
            this.label8.Text = "Файл шаблона выгрузки:";
            // 
            // TemplatePathBox
            // 
            this.TemplatePathBox.Location = new System.Drawing.Point(16, 114);
            this.TemplatePathBox.Name = "TemplatePathBox";
            this.TemplatePathBox.Size = new System.Drawing.Size(358, 23);
            this.TemplatePathBox.TabIndex = 9;
            // 
            // TemplateOpenButton
            // 
            this.TemplateOpenButton.Image = global::SmartPlus.Properties.Resources.Folder;
            this.TemplateOpenButton.Location = new System.Drawing.Point(380, 111);
            this.TemplateOpenButton.Name = "TemplateOpenButton";
            this.TemplateOpenButton.Size = new System.Drawing.Size(37, 28);
            this.TemplateOpenButton.TabIndex = 10;
            this.TemplateOpenButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.TemplateOpenButton.UseVisualStyleBackColor = true;
            this.TemplateOpenButton.Click += new System.EventHandler(this.TemplateOpenButton_Click);
            // 
            // GTMAutoMatchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 361);
            this.Controls.Add(this.TemplateOpenButton);
            this.Controls.Add(this.TemplatePathBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ExtraSettingGroup);
            this.Controls.Add(this.ExtraSettingsButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.GtmListListView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DatesGroupBox);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GTMAutoMatchForm";
            this.Text = "Подбор проницаемостей по скважинам с ГТМ";
            this.DatesGroupBox.ResumeLayout(false);
            this.DatesGroupBox.PerformLayout();
            this.ExtraSettingGroup.ResumeLayout(false);
            this.ExtraSettingGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RcUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartSkinUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadiusFindWellUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxSkinUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinSkinUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox DatesGroupBox;
        private System.Windows.Forms.DateTimePicker EndDatePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker StartDatePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView GtmListListView;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button ExtraSettingsButton;
        private System.Windows.Forms.GroupBox ExtraSettingGroup;
        private System.Windows.Forms.Button AddWellListButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown MaxSkinUpDown;
        private System.Windows.Forms.NumericUpDown MinSkinUpDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private TextBoxEx WellListTextBoxEx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TemplatePathBox;
        private System.Windows.Forms.Button TemplateOpenButton;
        private System.Windows.Forms.NumericUpDown RadiusFindWellUpDown;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown StartSkinUpDown;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox CalcMultiPlastCodeCheckBox;
        private System.Windows.Forms.CheckBox UsePiezometricWells;
        private System.Windows.Forms.NumericUpDown RcUpDown;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown ReUpDown;
        private System.Windows.Forms.Label label11;
    }
}