﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus.Dialogs
{
    public partial class CreateGTMLayersForm : Form
    {
        MainForm mainForm;

        public CreateGTMLayersForm(MainForm mainForm)
        {
            this.mainForm = mainForm; 
            this.Owner = mainForm;
            this.StartPosition = FormStartPosition.CenterParent;
            
            InitializeComponent();

            StartDatePicker.Value = new DateTime(DateTime.Now.Year, 1, 1);
            EndDatePicker.Value = DateTime.Now;

            // масштабирование
            int dx = label1.Width - 183;
            if (dx > 0)
            {
                label2.Left += dx;
                EndDatePicker.Left += dx;
                dx += label2.Width - 202;
                this.Width += dx;
                label4.MaximumSize = new System.Drawing.Size(GtmListListView.Width, 0);
                this.Height = label4.Bottom + 76;
            }
            #region заполняем список ГТМ
            ListViewItem item;
            item = new ListViewItem("ВНС из бурения");
            item.Checked = true;
            item.ToolTipText = "Ввод новых скважин из бурения";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("ГРП");
            item.Checked = true;
            item.ToolTipText = "ГРП";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("ЗБС");
            item.Checked = true;
            item.ToolTipText = "Зарезка боковых стволов";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("ИДН");
            item.Checked = true;
            item.ToolTipText = "Оптимизация подземного или наземного оборудования";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("ПВЛГ");
            item.Checked = true;
            item.ToolTipText = "Переход на другой объект или приобщение";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("ОПЗ");
            item.Checked = true;
            item.ToolTipText = "ОПЗ";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("Реперфорация");
            item.Checked = true;
            item.ToolTipText = "Реперфорация";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("Перевод в ППД");
            item.Checked = true;
            item.ToolTipText = "Перевод в ППД";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("РИР");
            item.Checked = true;
            item.ToolTipText = "РИР";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("Углубление");
            item.Checked = true;
            item.ToolTipText = "Углубление";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("ВНС из др. фонда");
            item.Checked = true;
            item.ToolTipText = "ВВвод новых скважин из прочих категорий";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("ВБД (п/л)");
            item.Checked = true;
            item.ToolTipText = "Ввод из бездействия с прошлых лет";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("ЛАР");
            item.Checked = true;
            item.ToolTipText = "ЛАР";
            GtmListListView.Items.Add(item);

            item = new ListViewItem("Прочие");
            item.Checked = true;
            item.ToolTipText = "Прочие";
            GtmListListView.Items.Add(item);
            #endregion
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            List<int[]> gtmCodes = new List<int[]>();
            for (int i = 0; i < GtmListListView.Items.Count; i++)
            {
                if (GtmListListView.Items[i].Checked)
                {
                    switch (i)
                    {
                        case 0:
                            gtmCodes.Add(new int[] { 1000 });
                            break;
                        case 1:
                            gtmCodes.Add(new int[] { 5000 });
                            break;
                        case 2:
                            gtmCodes.Add(new int[] { 3000 });
                            break;
                        case 3:
                            gtmCodes.Add(new int[] { 8000, 12000 });
                            break;
                        case 4:
                            gtmCodes.Add(new int[] { 6000 });
                            break;
                        case 5:
                            gtmCodes.Add(new int[] { 10000 });
                            break;
                        case 6:
                            gtmCodes.Add(new int[] { 11000 });
                            break;
                        case 7:
                            gtmCodes.Add(new int[] { 20000 });
                            break;
                        case 8:
                            gtmCodes.Add(new int[] { 9000 });
                            break;
                        case 9:
                            gtmCodes.Add(new int[] { 4000 });
                            break;
                        case 10:
                            gtmCodes.Add(new int[] { 2000 });
                            break;
                        case 11:
                            gtmCodes.Add(new int[] { 7000 });
                            break;
                        case 12:
                            gtmCodes.Add(new int[] { 13000 });
                            break;
                        case 13:
                            gtmCodes.Add(new int[] { 14000, 15000, 16000, 17000, 18000, 19000, 21000, 22000, 23000, 24000 });
                            break;
                    }
                }
            }
            if (gtmCodes.Count > 0)
            {
                object[] arg = new object[3];
                arg[0] = StartDatePicker.Value;
                arg[1] = EndDatePicker.Value;
                arg[2] = gtmCodes;
                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.CREATE_GTM_LAYERLIST, arg);
            }
        }
    }
}
