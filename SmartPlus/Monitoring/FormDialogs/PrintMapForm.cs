﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class PrintMapForm : Form
    {
        internal class ResizePictureBox : PictureBox
        {
            bool drawWait;
            public bool DrawWait
            {
                get { return drawWait; }
                set
                {
                    drawWait = value;
                    Invalidate();
                }
            }

            Bitmap bitmap;
            public ResizePictureBox()
            {
                DrawWait = false;
                ResizeRedraw = true;
                this.Resize += new EventHandler(ResizePictureBox_Resize);
                //this.Paint += new PaintEventHandler(ResizePictureBox_Paint);
            }

            void ResizePictureBox_Resize(object sender, EventArgs e)
            {
                this.Invalidate();
            }
            protected override void OnPaintBackground(PaintEventArgs pevent)
            {
                base.OnPaintBackground(pevent);
                if (bitmap != null)
                {
                    pevent.Graphics.Clear(Color.LightGray);
                    if (bitmap != null && bitmap.Width > 0 && bitmap.Height > 0)
                    {
                        Size previewSz = this.ClientRectangle.Size;
                        previewSz.Width -= 10;
                        previewSz.Height -= 10;
                        Rectangle rect = new Rectangle();
                        if (bitmap.Width < previewSz.Width && bitmap.Height < previewSz.Height)
                        {
                            rect.X = (int)(previewSz.Width / 2 - bitmap.Width / 2);
                            rect.Y = (int)(previewSz.Height / 2 - bitmap.Height / 2);
                            rect.Width = bitmap.Width;
                            rect.Height = bitmap.Height;
                        }
                        else if (bitmap.Height > bitmap.Width || bitmap.Height > previewSz.Height)
                        {
                            rect.Y = 5;
                            rect.Height = previewSz.Height;
                            float k = (float)previewSz.Height / bitmap.Height;
                            rect.X = (int)(previewSz.Width / 2 - (bitmap.Width * k) / 2);
                            rect.Width = (int)(bitmap.Width * k);
                        }
                        else if (bitmap.Width > bitmap.Height || bitmap.Width > previewSz.Width)
                        {
                            rect.X = 5;
                            rect.Width = previewSz.Width;
                            float k = (float)previewSz.Width / bitmap.Width;
                            rect.Y = (int)(previewSz.Height / 2 - (bitmap.Width * k) / 2);
                            rect.Height = (int)(bitmap.Height * k);
                        }
                        pevent.Graphics.DrawImage(bitmap, rect);
                        pevent.Graphics.DrawRectangle(Pens.Black, rect);
                    }
                }
                if (DrawWait)
                {
                    using(Brush br = new SolidBrush(Color.FromArgb(125, Color.Gray)))
                    {
                        using(Font f = new Font("Calibri", 8.25f))
                        {
                            pevent.Graphics.FillRectangle(br, this.ClientRectangle);
                            SizeF sz = pevent.Graphics.MeasureString("Подождите. Идет отрисовка карты...", f, PointF.Empty, StringFormat.GenericTypographic);
                            pevent.Graphics.DrawString("Подождите. Идет отрисовка карты...", f, br, this.ClientRectangle.Width / 2 - sz.Width / 2, this.ClientRectangle.Height / 2 - sz.Height / 2);
                        }    
                    }
                }
            }
            public void SetBitmap(Bitmap bitmap)
            {
                this.bitmap = bitmap;
                this.Invalidate();
            }
        }
        MainForm mainForm;
        Bitmap bmp;
        Graphics grfx;
        Rectangle savedBitMapRect;
        ResizePictureBox box;

        public PrintMapForm(MainForm mainForm)
        {
            InitializeComponent();
            this.Owner = mainForm;
            this.StartPosition = FormStartPosition.CenterParent;
            this.mainForm = mainForm;
            StretchImageCheck.Checked = true;

            savedBitMapRect = mainForm.canv.BitMapRect;
            mainForm.canv.BitMapRect = new Rectangle(-50, -50, 0, 0);

            box = new ResizePictureBox();
            box.Location = new Point(12, 12);
            box.Size = new Size(460, 272);
            box.BorderStyle = BorderStyle.FixedSingle;
            box.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            this.Controls.Add(box);
            this.SizeImageFormat.SelectedIndex = 0;

            this.FormClosing += new FormClosingEventHandler(PrintMapForm_FormClosing);
        }

        void PrintMapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bmp != null) bmp.Dispose();
            if (!savedBitMapRect.IsEmpty)
            {
                mainForm.canv.BitMapRect = savedBitMapRect;
            }
            C2DObject.DrawAllObjects = false;
        }

        // Show form
        public static void ShowPrintForm(MainForm mainForm)
        {
            PrintMapForm form = new PrintMapForm(mainForm);
            form.CreateBitMap(mainForm.canv.MapProject);
            form.ReDrawMap(form.grfx);
            form.ShowDialog();
            form.Dispose();
        }
        private void CreateBitMap(Project MapProject)
        {
            bmp = null;
            if (mainForm.canv.MapProject != null)
            {
                SizeF sz = GetImageSize(SizeImageFormat.SelectedIndex);
                sz.Width *= 300; sz.Height *= 300;
                try
                {
                    box.DrawWait = true;
                    C2DObject.DrawAllObjects = true;
                    double saveXUnits = C2DObject.st_XUnitsInOnePixel;
                    double saveXVisible = C2DObject.st_VisbleLevel;

                    //C2DObject.st_VisbleLevel = st_OilfieldXUnits;
                    //if (mainForm.canv.MapProject.MinMaxXY.Width > mainForm.canv.MapProject.MinMaxXY.Height)
                    //{
                    //    C2DObject.st_XUnitsInOnePixel = mainForm.canv.MapProject.MinMaxXY.Width / sz.Width;
                    //}
                    //else
                    //{
                    //    C2DObject.st_XUnitsInOnePixel = mainForm.canv.MapProject.MinMaxXY.Height / sz.Height;
                    //}
                    C2DObject.st_YUnitsInOnePixel = C2DObject.st_XUnitsInOnePixel;
                    mainForm.canv.ReCalcFontSizes();

                    bmp = new Bitmap((int)sz.Width, (int)sz.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                    grfx = Graphics.FromImage(bmp);
                    ReDrawMap(grfx);
                    C2DObject.DrawAllObjects = false;
                    C2DObject.st_XUnitsInOnePixel = saveXUnits;
                    C2DObject.st_YUnitsInOnePixel = saveXUnits;
                    C2DObject.st_VisbleLevel = saveXVisible;
                    mainForm.canv.ReCalcFontSizes();
                    box.SetBitmap(bmp);
                    box.DrawWait = false;
                }
                catch
                {
                    box.SetBitmap(null);
                    box.DrawWait = false;
                }
            }
        }
        private void ReDrawMap(Graphics grfx) 
        {
            if (grfx != null)
            {
                bool drawed = false;
                if (!drawed)
                {
                    C2DObject.DrawAllObjects = true;
                    if (grfx != null) mainForm.canv.DrawLayerList(grfx);
                    C2DObject.DrawAllObjects = false;
                }
            }
        }

        SizeF GetImageSize(int index)
        {
            switch (index)
            {
                case 0:
                    return new SizeF(8.27f, 11.69f);
                case 1:
                    return new SizeF(11.69f, 16.54f);
                case 2:
                    return new SizeF(16.54f, 23.39f);
                case 3:
                    return new SizeF(23.39f, 33.11f);
                case 4:
                    return new SizeF(33.11f, 46.81f);
            }
            return SizeF.Empty;
        }
        private void SizeImageFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bmp != null)
            {
                bmp.Dispose();
                bmp = null;
            }
            CreateBitMap(mainForm.canv.MapProject);
        }

        private void StretchImageCheck_CheckedChanged(object sender, EventArgs e)
        {
            ReDrawMap(grfx);
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (bmp != null)
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Title = "Выберите файл для сохранения карты...";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                dlg.DefaultExt = "png";
                dlg.Filter = "PNG файлы (*.png)|*.png";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    string fileName = dlg.FileName;

                    SizeF sz = GetImageSize(SizeImageFormat.SelectedIndex);
                    if (sz.Width > 0)
                    {
                        sz.Width *= 300; sz.Height *= 300;
                        try
                        {
                            box.DrawWait = true;
                            double saveXUnits = C2DObject.st_XUnitsInOnePixel;
                            C2DObject.DrawAllObjects = true;
                            if (mainForm.canv.MapProject.MinMaxXY.Width > mainForm.canv.MapProject.MinMaxXY.Height)
                            {
                                C2DObject.st_XUnitsInOnePixel = mainForm.canv.MapProject.MinMaxXY.Width / sz.Width;
                            }
                            else
                            {
                                C2DObject.st_XUnitsInOnePixel = mainForm.canv.MapProject.MinMaxXY.Height / sz.Height;
                            }
                            C2DObject.st_YUnitsInOnePixel = C2DObject.st_XUnitsInOnePixel;
                            mainForm.canv.ReCalcFontSizes();
                            Bitmap file = new Bitmap((int)(sz.Width), (int)(sz.Height), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                            Graphics fileGrfx = Graphics.FromImage(file);
                            ReDrawMap(fileGrfx);
                            C2DObject.DrawAllObjects = false;
                            C2DObject.st_XUnitsInOnePixel = saveXUnits;
                            C2DObject.st_YUnitsInOnePixel = saveXUnits;
                            mainForm.canv.ReCalcFontSizes();
                            file.Save(fileName);
                            box.DrawWait = false;
                        }
                        catch
                        {
                            MessageBox.Show(mainForm, "Ошибка печати карты в файл. Недостаточно памяти!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }
    }
}
