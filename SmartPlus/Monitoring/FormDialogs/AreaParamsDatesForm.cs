﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    class AreaParamsDatesForm : Form
    {
        public DateTime SelectDateStart, SelectDateEnd;
        public CheckBox UseMerStartDate;
        private Button OkButton;
        private DateTimePicker DateTimeStartBox;
        private Label label1;
        private Label label2;
        private DateTimePicker DateTimeEndBox;
        public CheckBox UseLastMerDate;
        private Button CancelButton;
    
        private void InitializeComponent()
        {
            this.UseMerStartDate = new System.Windows.Forms.CheckBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.DateTimeStartBox = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DateTimeEndBox = new System.Windows.Forms.DateTimePicker();
            this.UseLastMerDate = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // UseMerStartDate
            // 
            this.UseMerStartDate.AutoSize = true;
            this.UseMerStartDate.Checked = true;
            this.UseMerStartDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UseMerStartDate.Location = new System.Drawing.Point(12, 74);
            this.UseMerStartDate.Name = "UseMerStartDate";
            this.UseMerStartDate.Size = new System.Drawing.Size(245, 19);
            this.UseMerStartDate.TabIndex = 0;
            this.UseMerStartDate.Text = "Использовать дату начала разработки";
            this.UseMerStartDate.UseVisualStyleBackColor = true;
            this.UseMerStartDate.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(407, 110);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(90, 30);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(503, 110);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // DateTimeStartBox
            // 
            this.DateTimeStartBox.Enabled = false;
            this.DateTimeStartBox.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimeStartBox.Location = new System.Drawing.Point(12, 35);
            this.DateTimeStartBox.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateTimeStartBox.Name = "DateTimeStartBox";
            this.DateTimeStartBox.Size = new System.Drawing.Size(259, 23);
            this.DateTimeStartBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Выберите начальную дату расчета:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(334, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Выберите конечную дату расчета:";
            // 
            // DateTimeEndBox
            // 
            this.DateTimeEndBox.Enabled = false;
            this.DateTimeEndBox.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimeEndBox.Location = new System.Drawing.Point(334, 35);
            this.DateTimeEndBox.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateTimeEndBox.Name = "DateTimeEndBox";
            this.DateTimeEndBox.Size = new System.Drawing.Size(259, 23);
            this.DateTimeEndBox.TabIndex = 6;
            // 
            // UseLastMerDate
            // 
            this.UseLastMerDate.AutoSize = true;
            this.UseLastMerDate.Checked = true;
            this.UseLastMerDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UseLastMerDate.Location = new System.Drawing.Point(334, 74);
            this.UseLastMerDate.Name = "UseLastMerDate";
            this.UseLastMerDate.Size = new System.Drawing.Size(228, 19);
            this.UseLastMerDate.TabIndex = 5;
            this.UseLastMerDate.Text = "Использовать последнюю дату МЭР";
            this.UseLastMerDate.UseVisualStyleBackColor = true;
            this.UseLastMerDate.CheckedChanged += new System.EventHandler(this.UseLastMerDate_CheckedChanged);
            // 
            // AreaParamsDatesForm
            // 
            this.AcceptButton = this.OkButton;
            this.ClientSize = new System.Drawing.Size(605, 152);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DateTimeEndBox);
            this.Controls.Add(this.UseLastMerDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DateTimeStartBox);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.UseMerStartDate);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AreaParamsDatesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public AreaParamsDatesForm(Form Owner, DateTime MaxDate)
        {
            InitializeComponent();
            this.Owner = Owner;
            this.StartPosition = FormStartPosition.CenterParent;
            DateTimeStartBox.MaxDate = MaxDate;

            DateTimeEndBox.Value = MaxDate;
            DateTimeEndBox.MaxDate = MaxDate;
            if (MaxDate > DateTime.Now)
            {
                DateTimeEndBox.Value = DateTime.Now;
                DateTimeEndBox.MaxDate = DateTime.Now;
                DateTimeStartBox.MaxDate = DateTime.Now;
            }

            DateTimeStartBox.ValueChanged += new EventHandler(DateTimeStartBox_ValueChanged);
            DateTimeEndBox.ValueChanged += new EventHandler(DateTimeEndBox_ValueChanged);
            this.FormClosing += new FormClosingEventHandler(ReportChessDateForm_FormClosing);
        }

        void DateTimeEndBox_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt = new DateTime(DateTimeEndBox.Value.Year, DateTimeEndBox.Value.Month, 1);
            if (DateTimeEndBox.Value != dt) DateTimeEndBox.Value = dt;
        }

        void DateTimeStartBox_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt  = new DateTime(DateTimeStartBox.Value.Year, DateTimeStartBox.Value.Month, 1);
            if (DateTimeStartBox.Value != dt) DateTimeStartBox.Value = dt;
        }

        void ReportChessDateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                if (UseMerStartDate.Checked)
                {
                    SelectDateStart = DateTime.MinValue;
                    SelectDateEnd = DateTime.MaxValue;
                }
                else
                {
                    SelectDateStart = DateTimeStartBox.Value;
                    SelectDateEnd = DateTimeEndBox.Value;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            DateTimeStartBox.Enabled = !UseMerStartDate.Checked;
        }

        private void UseLastMerDate_CheckedChanged(object sender, EventArgs e)
        {
            DateTimeEndBox.Enabled = !UseLastMerDate.Checked;
        }
    }
}
