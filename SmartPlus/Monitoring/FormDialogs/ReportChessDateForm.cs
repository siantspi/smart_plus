﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    class ReportChessDateForm : Form
    {
        public DateTime SelectDate1, SelectDate2, SelectDate3;
        private Button OkButton;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private DateTimePicker DateTime1Box;
        private DateTimePicker DateTime2Box;
        private DateTimePicker DateTime3Box;
        private Button CancelButton;
    
        private void InitializeComponent()
        {
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.DateTime1Box = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DateTime2Box = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.DateTime3Box = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(12, 220);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(90, 30);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(182, 220);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // DateTime1Box
            // 
            this.DateTime1Box.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTime1Box.Location = new System.Drawing.Point(32, 35);
            this.DateTime1Box.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateTime1Box.Name = "DateTime1Box";
            this.DateTime1Box.Size = new System.Drawing.Size(239, 23);
            this.DateTime1Box.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Выберите даты расчета:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "1.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "2.";
            // 
            // DateTime2Box
            // 
            this.DateTime2Box.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTime2Box.Location = new System.Drawing.Point(33, 77);
            this.DateTime2Box.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateTime2Box.Name = "DateTime2Box";
            this.DateTime2Box.Size = new System.Drawing.Size(239, 23);
            this.DateTime2Box.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "3.";
            // 
            // DateTime3Box
            // 
            this.DateTime3Box.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTime3Box.Location = new System.Drawing.Point(32, 122);
            this.DateTime3Box.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateTime3Box.Name = "DateTime3Box";
            this.DateTime3Box.Size = new System.Drawing.Size(239, 23);
            this.DateTime3Box.TabIndex = 8;
            // 
            // ReportChessDateForm
            // 
            this.AcceptButton = this.OkButton;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DateTime3Box);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DateTime2Box);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DateTime1Box);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportChessDateForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Отчет по шахматке";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public ReportChessDateForm(Form Owner)
        {
            InitializeComponent();
            this.Owner = Owner;
            this.StartPosition = FormStartPosition.CenterParent;
            DateTime1Box.MaxDate = DateTime.Now;
            this.FormClosing += new FormClosingEventHandler(ReportChessDateForm_FormClosing);
        }

        void ReportChessDateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                SelectDate1 = DateTime1Box.Value;
                SelectDate2 = DateTime2Box.Value;
                SelectDate3 = DateTime3Box.Value;
            }
        }
    }
}

