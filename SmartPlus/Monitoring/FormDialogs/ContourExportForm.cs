﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.Dialogs
{
    class ContourExportForm : Form
    {
        private Label label1;
        private Button bOpen;
        private CheckBox cbOFCoordSystem;
        private ComboBox cbOilFields;
        private Label label2;
        private Button bOk;
        private Button bCancel;
        private ComboBox cbFormat;
        private TextBox tbFileName;

        public ContourExportForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.bOpen = new System.Windows.Forms.Button();
            this.cbOFCoordSystem = new System.Windows.Forms.CheckBox();
            this.cbOilFields = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Путь к файлу:";
            // 
            // tbFileName
            // 
            this.tbFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFileName.Location = new System.Drawing.Point(128, 11);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(505, 23);
            this.tbFileName.TabIndex = 1;
            // 
            // bOpen
            // 
            this.bOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bOpen.Location = new System.Drawing.Point(639, 6);
            this.bOpen.Name = "bOpen";
            this.bOpen.Size = new System.Drawing.Size(90, 30);
            this.bOpen.TabIndex = 2;
            this.bOpen.Text = "Открыть...";
            this.bOpen.UseVisualStyleBackColor = true;
            this.bOpen.Click += new System.EventHandler(this.bOpen_Click);
            // 
            // cbOFCoordSystem
            // 
            this.cbOFCoordSystem.AutoSize = true;
            this.cbOFCoordSystem.Location = new System.Drawing.Point(15, 51);
            this.cbOFCoordSystem.Name = "cbOFCoordSystem";
            this.cbOFCoordSystem.Size = new System.Drawing.Size(239, 19);
            this.cbOFCoordSystem.TabIndex = 3;
            this.cbOFCoordSystem.Text = "В системе координат месторождения:";
            this.cbOFCoordSystem.UseVisualStyleBackColor = true;
            this.cbOFCoordSystem.CheckedChanged += new System.EventHandler(this.cbOFCoordSystem_CheckedChanged);
            // 
            // cbOilFields
            // 
            this.cbOilFields.Enabled = false;
            this.cbOilFields.FormattingEnabled = true;
            this.cbOilFields.Location = new System.Drawing.Point(330, 50);
            this.cbOilFields.Name = "cbOilFields";
            this.cbOilFields.Size = new System.Drawing.Size(278, 23);
            this.cbOilFields.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Формат файла:";
            // 
            // bOk
            // 
            this.bOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(543, 82);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 6;
            this.bOk.Text = "Экспорт";
            this.bOk.UseVisualStyleBackColor = true;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(639, 82);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 7;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // cbFormat
            // 
            this.cbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Items.AddRange(new object[] {
            "KTS файл (*.kts)",
            "Формат контуров \"SmartPlus\" (*.cntr)"});
            this.cbFormat.Location = new System.Drawing.Point(139, 91);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(143, 23);
            this.cbFormat.TabIndex = 8;
            this.cbFormat.SelectedIndexChanged += new System.EventHandler(this.cbFormat_SelectedIndexChanged);
            // 
            // ContourExportForm
            // 
            this.AcceptButton = this.bOk;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(741, 124);
            this.Controls.Add(this.cbFormat);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbOilFields);
            this.Controls.Add(this.cbOFCoordSystem);
            this.Controls.Add(this.bOpen);
            this.Controls.Add(this.tbFileName);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContourExportForm";
            this.Text = "Экспорт контура";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContourExportForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        void ContourExportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cbOFCoordSystem.Checked && cbOilFields.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите месторождение для привязки системы координат.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void cbOFCoordSystem_CheckedChanged(object sender, EventArgs e)
        {
            cbOilFields.Enabled = cbOFCoordSystem.Checked;
            if (cbOFCoordSystem.Checked && cbFormat.SelectedIndex == 1)
            {
                MessageBox.Show("Контур с привязкой к координатам месторождения можно выгрузить только в формате KTS (*.kts)", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cbFormat.SelectedIndex = 0;
            }
        }

        private void bOpen_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Title = "Выберите путь к файлу для экспорта контура";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                tbFileName.Text = dlg.FileName;
            }
        }

        public static void ShowForm(Form Owner, Project project, int ActiveOilField, C2DLayer layer)
        {
            ContourExportForm form = new ContourExportForm();
            form.Owner = Owner;
            form.cbOilFields.Items.Clear();
            OilField of;
            for (int i = 1; i < project.OilFields.Count; i++)
            {
                of = project.OilFields[i];
                form.cbOilFields.Items.Add(of.Name);
            }
            
            if ((ActiveOilField > 0) && (ActiveOilField - 1 < form.cbOilFields.Items.Count))
            {
                form.cbOilFields.SelectedIndex = ActiveOilField - 1;
            }
            form.cbFormat.SelectedIndex = 0;
            
            if (form.ShowDialog() == DialogResult.OK)
            {
                of = project.OilFields[form.cbOilFields.SelectedIndex + 1];
                switch (form.cbFormat.SelectedIndex)
                {
                    case 0:
                        form.ExportContourInKts(form.tbFileName.Text, layer, of.CoefDict);
                        break;
                    case 1:
                        form.ExportContourInMiR(form.tbFileName.Text, layer, of.CoefDict);
                        break;
                }
            }
        }

        public void ExportContourInKts(string FileName, C2DLayer layer, OilfieldCoefItem coef)
        {
            if (FileName.Length > 0)
            {
                StreamWriter file = new StreamWriter(FileName + ".kts", false, Encoding.GetEncoding(1251));
                Contour cntr;
                double xLoc, yLoc;
                PointD point;
                string head = ";0;0;1;1;255;-1;0;16777215;-1;0";

                for (int i = 0; i < layer.ObjectsList.Count; i++)
                {
                    cntr = (Contour)layer.ObjectsList[i];
                    file.WriteLine("*" + cntr.Name + ".ktr" + head);
                    for (int j = 0; j < cntr._points.Count; j++)
                    {
                        point = cntr._points[j];
                        xLoc = point.X;
                        yLoc = point.Y;
                        if (coef != null)
                        {
                            coef.LocalCoordFromGlobal(point.X, point.Y, out xLoc, out yLoc);
                        }
                        file.WriteLine(String.Format("{0} {1}", xLoc, yLoc));
                    }
                    if (cntr._points.Count > 0)
                    {
                        point = cntr._points[0];
                        xLoc = point.X;
                        yLoc = point.Y;
                        if (coef != null)
                        {
                            coef.LocalCoordFromGlobal(point.X, point.Y, out xLoc, out yLoc);
                        }
                        file.WriteLine(String.Format("{0} {1}", xLoc, yLoc));
                    }
                }
                file.Close();
            }
        }
        public void ExportContourInMiR(string FileName, C2DLayer layer, OilfieldCoefItem coef)
        {
            StreamWriter contours_file = new StreamWriter(FileName, false, System.Text.Encoding.GetEncoding(1251));
            contours_file.WriteLine("ver=0");
            contours_file.WriteLine(layer.Index);
            contours_file.WriteLine(layer.Visible);
            contours_file.WriteLine(1);
            for (int j = 0; j < layer.ObjectsList.Count; j++)
            {
                ((Contour)layer.ObjectsList[j]).WriteToCache(layer.Index, contours_file);
            }
            contours_file.Close();
        }

        private void cbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOFCoordSystem.Checked && cbFormat.SelectedIndex == 1)
            {
                MessageBox.Show("Контур с привязкой к координатам месторождения можно выгрузить только в формате KTS (*.kts)", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cbFormat.SelectedIndex = 0;
            }
        }
    }
}
