﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SmartPlus
{
    public sealed class OutputTextForm : Form
    {
        private RichTextBox OutText;
        private Button CloseButton;
        Font BoldFont, ValueFont;

        private void InitializeComponent()
        {
            this.OutText = new System.Windows.Forms.RichTextBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OutText
            // 
            this.OutText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.OutText.Location = new System.Drawing.Point(12, 12);
            this.OutText.Name = "OutText";
            this.OutText.Size = new System.Drawing.Size(570, 280);
            this.OutText.TabIndex = 0;
            this.OutText.Text = "";
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.Location = new System.Drawing.Point(492, 297);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(90, 30);
            this.CloseButton.TabIndex = 2;
            this.CloseButton.Text = "Закрыть";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // OutputTextForm
            // 
            this.ClientSize = new System.Drawing.Size(594, 339);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.OutText);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximizeBox = false;
            this.Name = "OutputTextForm";
            this.Text = "Результаты";
            this.ResumeLayout(false);

        }

        public OutputTextForm(Form MainForm)
        {
            InitializeComponent();
            this.Owner = MainForm;
            this.StartPosition = FormStartPosition.CenterParent;
            BoldFont = new Font("Tahoma", 8.25f, FontStyle.Bold);
            ValueFont = new Font("Tahoma", 8.25f);
            this.OutText.WordWrap = false;
        }

        public int CountRows
        {
            get { return OutText.Lines.Length; }
        }
        public void AddBoldText(string Text)
        {
            OutText.SelectionFont = BoldFont;
            OutText.AppendText(Text);
        }
        public void AddText(string Text)
        {
            OutText.SelectionFont = ValueFont;
            OutText.AppendText(Text);
        }
        public void NewLine() { OutText.AppendText("\n"); }

        public void SetStartPosition()
        {
            OutText.SelectionStart = 0;
        }
        public void SetLastPosition()
        {
            OutText.SelectionStart = OutText.Text.Length;
        }
        public void Clear() { OutText.Clear(); }
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
