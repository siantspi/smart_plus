﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    public partial class MarkerAutoCreateForm : Form
    {
        CanvasMap canv;
        OilField of;
        List<int> GeoStratumList;
        int PlastCode;
        bool Top;
        int countPoints;
        BackgroundWorker worker;
        Timer tmr;

        public MarkerAutoCreateForm()
        {
            InitializeComponent();
            GeoStratumList = new List<int>();
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            tmr = new Timer();
            tmr.Enabled = false;
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(tmr_Tick);
            
            // масштабирование
            OilFieldTextBox.Left = lMarker.Right + 14;
            MarkersComboBox.Left = lMarker.Right + 14;
            this.Width = OilFieldTextBox.Right + 20;

            this.FormClosing += new FormClosingEventHandler(MarkerAutoCreateForm_FormClosing);
        }

        void tmr_Tick(object sender, EventArgs e)
        {
            countPoints++;
            if (countPoints > 3) countPoints = 1;
            string str = string.Empty;
            for (int i = 0; i < countPoints; i++) str += ".";
            Comment.Text = "Загрузка ГИС. Пожалуйста подождите" + str;
        }

        void MarkerAutoCreateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (worker.IsBusy)
            {
                e.Cancel = true;
                return;
            }
            if (of != null) of.ClearGisData(false);
        }

        #region Worker
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            tmr.Start();
            if ((of != null) && (!of.GisLoaded))
            {
                of.LoadGisFromCache(worker, e);
            }
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            tmr.Stop();
            CreateMarker();
        }
        #endregion


        DialogResult ShowForm()
        {
            OilFieldTextBox.Text = of.Name;
            MarkersComboBox.Items.Clear();
            GeoStratumList.Clear();
            int ind;
            var dict = (StratumDictionary)canv.MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            of.GeoStratumCodes.Sort();
            for (int i = 0; i < of.GeoStratumCodes.Count; i++)
            {
                ind = dict.GetIndexByCode(of.GeoStratumCodes[i]);
                if(ind != -1)
                {
                    MarkersComboBox.Items.Add(string.Format("{0} [top]", dict[ind].ShortName));
                    MarkersComboBox.Items.Add(string.Format("{0} [bottom]", dict[ind].ShortName));
                    GeoStratumList.Add(of.GeoStratumCodes[i]);
                    GeoStratumList.Add(of.GeoStratumCodes[i]);
                }
            }
            if (MarkersComboBox.Items.Count > 0) MarkersComboBox.SelectedIndex = 0;
            return ShowDialog();
        }
        public static int ShowForm(CanvasMap map, int OilfieldIndex)
        {
            if ((OilfieldIndex > -1) && (map.MapProject != null) && (OilfieldIndex < map.MapProject.OilFields.Count))
            {
                MarkerAutoCreateForm form = new MarkerAutoCreateForm();
                form.canv = map;
                form.StartPosition = FormStartPosition.Manual;
                Point pt = map.mainForm.Location;
                Size size = map.mainForm.ClientRectangle.Size;
                if (map.mainForm.corSchPane.DockedState == Infragistics.Win.UltraWinDock.DockedState.Floating)
                {
                    pt = map.mainForm.corSchPane.DockAreaPane.FloatingLocation;
                    size = map.mainForm.corSchPane.DockAreaPane.Size;
                }
                int x = pt.X + size.Width / 2 - form.Width / 2;
                int y = pt.Y + size.Height / 2 - form.Height / 2;
                if (x < 0) x = 0; if (y < 0) y = 0;
                form.Location = new Point(x, y);
                form.of = map.MapProject.OilFields[OilfieldIndex];
                form.ShowForm();
            }
            else
            {
                MessageBox.Show("Выберите активное месторождение.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return 0;
        }
        void CreateMarker()
        {
            if ((canv != null) && (of != null) && (PlastCode != -1))
            {
                Marker mrk = new Marker();
                Well w;
                float depth;
                for (int i = 0; i < of.Wells.Count; i++)
                {
                    w = of.Wells[i];
                    depth = -1;
                    if(w.gis != null) depth = w.gis.GetAbsDepthByPlastCode(PlastCode, Top);
                    if(depth > -1)
                    {
                        mrk.Add(w, depth);
                    }
                }
                if (mrk.Count > 0)
                {
                    C2DLayer mrkLayer = canv.AddNewMarker();
                    var dict = (StratumDictionary)canv.MapProject.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);

                    int ind = dict.GetIndexByCode(PlastCode);
                    mrkLayer.Name = string.Format("Маркер {0} ({1} - {2})", of.Name, dict[ind].ShortName, (Top ? "Top" : "Bottom"));
                    mrk.Name = mrkLayer.Name;
                    if (mrkLayer.node != null) mrkLayer.node.Text = mrkLayer.Name;
                    mrk.PenBound = ((Marker)mrkLayer.ObjectsList[0]).PenBound;
                    mrkLayer.ObjectsList[0] = mrk;
                    mrkLayer.GetObjectsRect();
                    canv.mainForm.CorSchReloadMarkers();
                    canv.mainForm.PlaneReloadMarkers();
                    canv.mainForm.corSchSetCurrentMarker(mrkLayer);
                    canv.mainForm.corSchSaveMarkerLayer();
                    canv.DrawLayerList();
                    Comment.Text = mrkLayer.Name + " создан.";
                }
                else
                {
                    Comment.Text = "Пласт не найден в ГИС. Маркер не создан.";
                }
            }
        }
        private void CreateButton_Click(object sender, EventArgs e)
        {
            if ((canv != null) && (MarkersComboBox.SelectedIndex > -1))
            {
                string str = (string)MarkersComboBox.Items[MarkersComboBox.SelectedIndex];
                Top = true;
                if (str.EndsWith("[bottom]")) Top = false;
                PlastCode = GeoStratumList[MarkersComboBox.SelectedIndex];
                Comment.Text = "Загрузка ГИС. Пожалуйста подождите.";
                worker.RunWorkerAsync();
            }
        }
    }
}
