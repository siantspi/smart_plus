﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartPlus.DictionaryObjects;

namespace SmartPlus.Dialogs
{
    public partial class CreateVoronoiMapForm : Form
    {
        int CheckedMainPlastIndex;
        bool ShowedSettings
        {
            get { return SettingsGroup.Visible; }
            set
            {
                ClearBallonTip();
                if (value)
                {
                    this.Width = SettingsGroup.Right + 24;
                    this.Height = SettingsGroup.Bottom + 76;
                    this.ShowHideSettingsButton.Image = Properties.Resources.up;
                    ShowHideSettingsButton.Text = "Скрыть настройки";
                    SettingsGroup.Visible = true;
                }
                else
                {
                    if (CheckPlastTree != null)
                    {
                        this.Width = CheckPlastTree.Right + 24;
                    }
                    this.Height = (CheckPlastTree == null) ? OilfieldCheckList.Bottom + 76 : CheckPlastTree.Bottom + 76;
                    this.ShowHideSettingsButton.Image = Properties.Resources.down;
                    ShowHideSettingsButton.Text = "Дополнительно";
                    SettingsGroup.Visible = false;
                }
            }
        }
        PlastTreeView CheckPlastTree;
        ToolTip toolTip, balloonTip;
        MainForm mainForm;
        Project project;
        List<int> InitPlastCodes;

        List<int> SelectedOilfieldIndexes;
        public List<int> SelectedPlasts;
        public List<Well> SelectedWells;
        public DateTime SelectedDate;
        public int SelectedCutOffRadius;
        public bool FilterByProduction;
        public Grid SelectedGrid;

        public CreateVoronoiMapForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            this.Owner = mainForm;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(mainForm.Location.X + mainForm.ClientRectangle.Width / 2 - this.Width / 2, mainForm.Location.Y + mainForm.ClientRectangle.Height / 2 - this.Height / 2);
            
            CheckPlastTree = new PlastTreeView();
            this.Controls.Add(CheckPlastTree);
            CheckPlastTree.Location = new Point(318, 27);
            CheckPlastTree.Size = new Size(193, 198);
            CheckedMainPlastIndex = -1;
            cbFilterOilObjects.Checked = true;
            SelectedOilfieldIndexes = new List<int>();
            SelectedPlasts = new List<int>();

            // масштабирование
            int dx = label2.Width - 146;
            if (dx > 0)
            {
                CreateDatePicker.Left += dx;
                RadiusCutOff.Left += dx;
                OilfieldCheckList.Left += dx;
                PlastCaption.Left = RadiusCutOff.Right + 10;
                CheckPlastTree.Left = RadiusCutOff.Right + 6;
                CheckPlastTree.Top = PlastCaption.Bottom + 3;

                int dy = CheckPlastTree.Bottom - 225;
                SettingsGroup.Top += dy;
                ShowHideSettingsButton.Top += dy;
                CreateVoronoiMapBtn.Top += dy;
                CancelButton.Top += dy;
                dx = cbFilterOilObjects.Width - 277;
                cbCutOffByCntr.Left += dx;
                dx += cbCutOffByCntr.Width - 178;
                this.Width += dx;
            }

            ShowedSettings = false;

            // Tool Tips
            toolTip = new ToolTip();
            toolTip.SetToolTip(NNTLoadButton, "Загрузить выделенную сетку толщин");
           
            CheckPlastTree.Nodes.Clear();
            CheckPlastTree.AfterCheck += new TreeViewEventHandler(CheckPlastTree_AfterCheck);
            this.LocationChanged += new EventHandler(CreateVoronoiMapForm_LocationChanged);
            this.FormClosing += new FormClosingEventHandler(CreateVoronoiMapForm_FormClosing);
        }

        void CreateVoronoiMapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        // Show Form
        public static void ShowForm(MainForm MainForm, Project Project, System.Collections.ArrayList WellList, DateTime InitDate, FormClosingEventHandler FormClosingHandler)
        {
            List<int> ofIndexes = new List<int>();
            List<int> ofWellCounts = new List<int>();
            List<Well> initWells = new List<Well>();
            Well w;
            int ind;
            for (int i = 0; i < WellList.Count; i++)
            {
                w = (Well)WellList[i];
                ind = ofIndexes.IndexOf(w.OilFieldIndex);
                if (ind == -1)
                {
                    ind = ofIndexes.Count;
                    ofIndexes.Add(w.OilFieldIndex);
                    ofWellCounts.Add(0);
                }
                ofWellCounts[ind]++;
                initWells.Add(w);
            }
            // сортировка месторождений по количеству скважин
            for (int i = 0; i < ofWellCounts.Count - 1; i++)
            {
                for (int j = i + 1; j < ofWellCounts.Count; j++)
                {
                    if (ofWellCounts[i] < ofWellCounts[j])
                    {
                        ind = ofWellCounts[i];
                        ofWellCounts[i] = ofWellCounts[j];
                        ofWellCounts[j] = ind;
                        ind = ofIndexes[i];
                        ofIndexes[i] = ofIndexes[j];
                        ofIndexes[j] = ind;
                    }
                }
            }
            MainForm.canv.UpdateFilterOilObj = true;
            MainForm.filterOilObj.ClearCurrent();
            MainForm.filterOilObj.ClearCurrentOilField();
            MainForm.filterOilObj.SetArrayListOilFieldOilObj(ofIndexes, false);
            MainForm.filterOilObj.UpdateCurrentTreeView();
            MainForm.filterOilObj.ExpandCurrentOilField();
            OilField of;
            var dict = (StratumDictionary)Project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            List<int> PlastCodes = new List<int>();
            for (int i = 0; i < ofIndexes.Count; i++)
            {
                of = Project.OilFields[ofIndexes[i]];
                for (int j = 0; j < of.MerStratumCodes.Count; j++)
                {
                    if (PlastCodes.IndexOf(of.MerStratumCodes[j]) == -1)
                    {
                        ind = dict.GetIndexByCode(of.MerStratumCodes[j]);
                        if (MainForm.filterOilObj.SelectedObjects.IndexOf(ind) > -1)
                        {
                            PlastCodes.Add(of.MerStratumCodes[j]);
                        }
                    }
                }
            }
            if (PlastCodes.Count == 0)
            {
                MessageBox.Show("Для построения областей Вороного выберите объект в фильтре объектов!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            PlastCodes.Sort();
            // test PlastCodes
            int k = 0, temp, lastInsertIndex = 0;
            bool find;
            WellIcon icon;
            while (k < PlastCodes.Count)
            {
                find = false;
                for (int j = 0; j < initWells.Count; j++)
                {
                    icon = initWells[j].icons.GetIconByCode(PlastCodes[k]);
                    if (icon != null)
                    {
                        find = true;
                        break;
                    }
                }
                if (find)
                {
                    temp = PlastCodes[k];
                    PlastCodes.RemoveAt(k);
                    PlastCodes.Insert(lastInsertIndex, temp);
                    lastInsertIndex++;
                }
                k++;
            }

            CreateVoronoiMapForm form = new CreateVoronoiMapForm(MainForm);
            form.project = Project;
            if (form.CreateDatePicker.MinDate < InitDate && InitDate < form.CreateDatePicker.MaxDate)
            {
                form.CreateDatePicker.Value = InitDate;
            }
            form.SelectedWells = initWells;
            form.SelectedGrid = null;
            form.InitPlastCodes = PlastCodes;
            form.SelectedOilfieldIndexes = new List<int>(ofIndexes.ToArray());

            for (int i = 0; i < ofIndexes.Count; i++)
            {
                form.OilfieldCheckList.Items.Add(Project.OilFields[ofIndexes[i]].Name, true);
            }

            form.FillPlastTree();
            form.FormClosing += new FormClosingEventHandler(FormClosingHandler);
            form.Show();
        }


        void FillPlastTree()
        {
            List<int> PlastCodes = new List<int>();
            OilField of;
            bool find;
            for (int i = 0; i < InitPlastCodes.Count; i++)
            {
                find = false;
                for(int j = 0; j < this.SelectedOilfieldIndexes.Count;j++)
                {
                    of = project.OilFields[SelectedOilfieldIndexes[j]];
                    if (of.MerStratumCodes.IndexOf(InitPlastCodes[i]) > -1)
                    {
                        find = true;
                        break;
                    }
                }
                if (find) PlastCodes.Add(InitPlastCodes[i]);
            }
            List<StratumTreeNode> PlastNodes = new List<StratumTreeNode>();
            var stratumDict = (StratumDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            for (int i = 0; i < PlastCodes.Count; i++)
            {
                PlastNodes.Add(stratumDict.GetStratumTreeNode(PlastCodes[i]));
            }
            List<StratumTreeNode> MainPlastNodes = new List<StratumTreeNode>();
            List<List<StratumTreeNode>> ChildPlastNodes = new List<List<StratumTreeNode>>();

            
            for (int i = 0; i < PlastNodes.Count; i++)
            {
                find = false;
                for (int j = 0; j < PlastCodes.Count; j++)
                {
                    if (i != j && (PlastNodes[i].GetParentLevelByCode(PlastCodes[j]) > -1))
                    {
                        find = true;
                        break;
                    }
                }
                if (!find)
                {
                    MainPlastNodes.Add(PlastNodes[i]);
                    ChildPlastNodes.Add(new List<StratumTreeNode>());
                }
            }
            for (int i = 0; i < PlastNodes.Count; i++)
            {
                if (MainPlastNodes.IndexOf(PlastNodes[i]) == -1)
                {
                    for (int j = 0; j < MainPlastNodes.Count; j++)
                    {
                        if (PlastNodes[i].GetParentLevelByCode(MainPlastNodes[j].StratumCode) > -1)
                        {
                            ChildPlastNodes[j].Add(PlastNodes[i]);
                        }
                    }
                }
            }
            TreeNode tn, tn2;
            CheckPlastTree.Nodes.Clear();
            for (int i = 0; i < MainPlastNodes.Count; i++)
            {
                tn = CheckPlastTree.Nodes.Add(MainPlastNodes[i].Name);
                tn.Tag = MainPlastNodes[i].StratumCode;
                for (int j = 0; j < ChildPlastNodes[i].Count; j++)
                {
                    tn2 = tn.Nodes.Add(ChildPlastNodes[i][j].Name);
                    tn2.Tag = ChildPlastNodes[i][j].StratumCode;
                }
            }
            if (CheckPlastTree.Nodes.Count > 0)
            {
                CheckPlastTree.Nodes[0].Checked = true;
                CheckedMainPlastIndex = 0;
            }
        }

        // Events
        void CreateVoronoiMapForm_LocationChanged(object sender, EventArgs e)
        {
            ClearBallonTip();
        }

        void CheckPlastTree_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action == TreeViewAction.ByMouse)
            {
                TreeNode Node = (e.Node.Level == 0) ? e.Node : e.Node.Parent;
                if (e.Node.Checked)
                {
                    if (CheckedMainPlastIndex != -1)
                    {
                        CheckPlastTree.Nodes[CheckedMainPlastIndex].Checked = false;
                    }
                }
                CheckedMainPlastIndex = Node.Index;
                Node.Checked = true;
            }
            else
            {
                if (e.Node.Level == 0)
                {
                    for (int i = 0; i < e.Node.Nodes.Count; i++)
                    {
                        e.Node.Nodes[i].Checked = e.Node.Checked;
                    }
                }
            }
        }

        private void CreateDatePicker_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt = new DateTime(CreateDatePicker.Value.Year, CreateDatePicker.Value.Month, 1);
            CreateDatePicker.Value = dt;
        }

        // Ballon Tip
        void ShowBallonToolTip(int x, int y, string Text)
        {
            if (balloonTip != null) balloonTip.Dispose();
            balloonTip = new ToolTip();
            balloonTip.ToolTipTitle = "Внимание!";
            balloonTip.ToolTipIcon = ToolTipIcon.Info;
            balloonTip.UseFading = true;
            balloonTip.UseAnimation = true;
            balloonTip.IsBalloon = true;
            balloonTip.Show(Text, this, x, y, 1500);
        }

        void ClearBallonTip()
        {
            if (balloonTip != null)
            {
                balloonTip.Dispose();
                balloonTip = null;
            }
        }

        // Button Up/Down Events
        private void ShowHideSettingsButton_MouseEnter(object sender, EventArgs e)
        {
            ShowHideSettingsButton.Image = ShowedSettings ? Properties.Resources.up_over : Properties.Resources.down_over;
        }

        private void ShowHideSettingsButton_MouseLeave(object sender, EventArgs e)
        {
            ShowHideSettingsButton.Image = ShowedSettings ? Properties.Resources.up : Properties.Resources.down;
        }

        private void ShowHideSettingsButton_Click(object sender, EventArgs e)
        {
            ShowedSettings = !ShowedSettings;

        }

        // AddGrid / Ok / Cancel Button Events
        private void AddSelectedGridButton_Click(object sender, EventArgs e)
        {
            bool LayersViewed = false, WorkLayersViewed = false, MestViewed = false;
            C2DLayer layer = null;
            TreeView tw;
            for (int i = 0; i < 2; i++)
            {
                if ((i == 0 && mainForm.canv.LastSelectedTreeView == mainForm.canv.twWork) || (i == 1 && !WorkLayersViewed))
                    if (mainForm.canv.twWork.SelectedNodes.Count > 0)
                    {
                        TreeNode tn;
                        for (int j = 0; j < mainForm.canv.twWork.SelectedNodes.Count; j++)
                        {
                            tn = mainForm.canv.twWork.SelectedNodes[j];
                            if (tn.Tag != null)
                            {
                                if (((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID)
                                {
                                    layer = (C2DLayer)tn.Tag;
                                    break;
                                }
                            }
                        }
                    }
                if (layer != null) break;
                tw = mainForm.canv.twLayers;
                if ((i == 0 && mainForm.canv.LastSelectedTreeView == tw) || (i == 1 && !LayersViewed))
                {
                    if ((tw.SelectedNode != null) && (tw.SelectedNode.Tag != null) && (((BaseObj)tw.SelectedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    {
                        if (((C2DLayer)tw.SelectedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID &&
                            ((Grid)(((C2DLayer)tw.SelectedNode.Tag).ObjectsList[0])).GridType == 2)
                        {
                            layer = (C2DLayer)tw.SelectedNode.Tag;
                            break;
                        }
                    }
                }
                tw = mainForm.canv.twActiveOilField;
                if ((i == 0 && mainForm.canv.LastSelectedTreeView == tw) || (i == 1 && !MestViewed))
                {
                    if ((tw.SelectedNode != null) && (tw.SelectedNode.Tag != null) && (((BaseObj)tw.SelectedNode.Tag).TypeID == Constant.BASE_OBJ_TYPES_ID.LAYER))
                    {
                        if (((C2DLayer)tw.SelectedNode.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.GRID &&
                            ((Grid)(((C2DLayer)tw.SelectedNode.Tag).ObjectsList[0])).GridType == 2)
                        {
                            layer = (C2DLayer)tw.SelectedNode.Tag;
                            break;
                        }
                    }
                }
            }
            if (layer != null)
            {
                string tree = string.Empty;
                if (layer.node != null && layer.node.TreeView != null)
                {
                    if (layer.node.TreeView == mainForm.canv.twWork)
                    {
                        tree = " (Рабочая папка)";
                    }
                    else
                    {
                        tree = " (Инспектор)";
                    }
                }
                NNTLayerTextBox.TextBox.Text = layer.Name + tree;
                NNTLayerTextBox.Tag = layer;
            }

            if (NNTLayerTextBox.Tag == null)
            {
                int h = this.DesktopBounds.Height - this.ClientRectangle.Height + NNTLoadButton.Height;
                int x = SettingsGroup.Location.X + NNTLoadButton.Location.X;
                int y = SettingsGroup.Location.Y + NNTLoadButton.Location.Y - h;
                ShowBallonToolTip(x + 5, y, "Выделите сетку ННТ в рабочей папке или в Инспекторе");
            }
        }

        private void CreateVoronoiMapBtn_Click(object sender, EventArgs e)
        {
            if (OilfieldCheckList.CheckedItems.Count == 0)
            {
                MessageBox.Show("Выберите хотя бы одно месторождение для расчета.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int i, ind;
            SelectedOilfieldIndexes.Clear();
            for (i = 0; i < OilfieldCheckList.CheckedItems.Count; i++)
            {
                ind = project.GetOFIndex((string)OilfieldCheckList.CheckedItems[i]);
                if (ind > -1) SelectedOilfieldIndexes.Add(ind);
            }
            i = 0;
            while (i < SelectedWells.Count)
            {
                if (SelectedOilfieldIndexes.IndexOf(SelectedWells[i].OilFieldIndex) == -1)
                {
                    SelectedWells.RemoveAt(i);
                    i--;
                }
                i++;
            }
            
            if (CheckedMainPlastIndex != -1)
            {
                SelectedPlasts.Clear();
                TreeNode tn = CheckPlastTree.Nodes[CheckedMainPlastIndex];
                SelectedPlasts.Add((int)tn.Tag);
                for (i = 0; i < tn.Nodes.Count; i++)
                {
                    SelectedPlasts.Add((int)tn.Nodes[i].Tag);
                }
            }
            SelectedGrid = null;
            if (NNTLayerTextBox.Tag != null)
            {
                C2DLayer layer = (C2DLayer)NNTLayerTextBox.Tag;
                SelectedGrid = (Grid)layer.ObjectsList[0];
                SelectedGrid.Name = layer.Name;
                if (layer.node != null && layer.node.TreeView == mainForm.canv.twWork)
                {
                    SelectedGrid.Index = -1;
                }
            }
            if (SelectedGrid != null)
            {
                ind = project.GetOFIndex(SelectedGrid.OilFieldCode);
                if(ind != -1 && SelectedOilfieldIndexes.IndexOf(ind) == -1)
                {
                    string message = "Заданная сетка толщин не принадлежит выбранным месторождениям для расчета.\nЗадайте сетку для выбранных месторождений для расчета.";
                    MessageBox.Show(mainForm, message, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (SelectedPlasts[0] != SelectedGrid.StratumCode)
                {
                    string message = "Пласт заданной сетки толщин не совпадает с выбранным объектом для расчета.\nПродолжить используя заданную сетку толщин?";
                    if (MessageBox.Show(mainForm, message, "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }
                }
            }
            
            SelectedDate = CreateDatePicker.Value;
            SelectedCutOffRadius = (int)RadiusCutOff.Value;
            FilterByProduction = cbFilterOilObjects.Checked;
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OilfieldCheckList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            SelectedOilfieldIndexes.Clear();
            for (int i = 0; i < OilfieldCheckList.Items.Count; i++)
            {
                if ((i == e.Index && e.NewValue == CheckState.Checked) || (i != e.Index && OilfieldCheckList.CheckedIndices.IndexOf(i) > -1))
                {
                    SelectedOilfieldIndexes.Add(project.GetOFIndex((string)OilfieldCheckList.Items[i]));
                }
            }
            FillPlastTree();
        }
    }

    class PlastTreeView : TreeView
    {
        public PlastTreeView()
        {
            this.CheckBoxes = true;
            this.HideSelection = false;
            this.FullRowSelect = true;
        }
        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == 515)
            { /* WM_LBUTTONDBLCLK */
            }
            else
                base.DefWndProc(ref m);
        }
    }
}
