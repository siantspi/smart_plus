﻿using System;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class AnnoyDialogForm : Form
    {
        public AnnoyDialogForm()
        {
            InitializeComponent();
        }

        private void AnnoyDialog_Load(object sender, EventArgs e)
        {
            Random r = new Random(System.DateTime.Now.Millisecond);
            int Index = r.Next(1, 3);

            lblInstruction.Text = string.Format("Для продолжения работы, нажмите кнопку №{0}", Index);
            switch(Index)
            {
                case 1:
                    button1.DialogResult = DialogResult.OK;
                    break;

                case 2:
                    button2.DialogResult = DialogResult.OK;
                    break;

                case 3:
                    button3.DialogResult = DialogResult.OK;
                    break;
            }
        }
    }
}
