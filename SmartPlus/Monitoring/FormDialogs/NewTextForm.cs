﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class NewTextForm : Form
    {
        public string NewText;
        string OldName;
        public NewTextForm(MainForm mainForm)
        {
            InitializeComponent();
            this.Owner = mainForm;
            NewText = "";
            OldName = "";
            this.Shown += new EventHandler(fRenameLayer_Shown);
            this.FormClosing += new FormClosingEventHandler(fNewText_FormClosing);
        }

        void fNewText_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((OldName != "") && (OldName == tbNewText.Text) && (this.DialogResult == DialogResult.OK))
            {
                e.Cancel = true;
                MessageBox.Show("Новое имя должно отличаться от старого.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        void fRenameLayer_Shown(object sender, EventArgs e)
        {
            tbNewText.Focus();
            tbNewText.SelectAll();            
        }
        public DialogResult ShowReNameLayer(string oldName)
        {
            this.Text = "Переименование слоя";
            this.lText.Text = "Введите новое имя для слоя";
            tbNewText.Text = oldName;
            OldName = oldName;
            NewText = "";
            return this.ShowDialog();
        }
        public DialogResult ShowNewLayer()
        {
            this.Text = "Создание новой подпапки";
            this.lText.Text = "Введите имя для папки";
            tbNewText.Text = "";
            NewText = "";
            return this.ShowDialog();
        }
        public DialogResult ShowNewIndex(int OldIndex)
        {
            this.Text = "Изменение индекса слоя";
            this.lText.Text = "Введите новый индекс";
            tbNewText.Text = OldIndex.ToString();
            NewText = "";
            return this.ShowDialog();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            NewText = "";
        }
        private void bOk_Click(object sender, EventArgs e)
        {
            NewText = tbNewText.Text;
        }
    }
}
