﻿namespace SmartPlus
{
    partial class MarkersZoneEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.MarkersGroup = new System.Windows.Forms.GroupBox();
            this.MarkerBottomCombo = new System.Windows.Forms.ComboBox();
            this.MarkerTopCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DrawGroup = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AlfaPercent = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.MarkersGroup.SuspendLayout();
            this.DrawGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AlfaPercent)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Имя зоны маркеров:";
            // 
            // NameBox
            // 
            this.NameBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.NameBox.Location = new System.Drawing.Point(141, 6);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(475, 23);
            this.NameBox.TabIndex = 1;
            // 
            // MarkersGroup
            // 
            this.MarkersGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MarkersGroup.Controls.Add(this.MarkerBottomCombo);
            this.MarkersGroup.Controls.Add(this.MarkerTopCombo);
            this.MarkersGroup.Controls.Add(this.label5);
            this.MarkersGroup.Controls.Add(this.label4);
            this.MarkersGroup.Location = new System.Drawing.Point(12, 35);
            this.MarkersGroup.Name = "MarkersGroup";
            this.MarkersGroup.Size = new System.Drawing.Size(367, 130);
            this.MarkersGroup.TabIndex = 2;
            this.MarkersGroup.TabStop = false;
            this.MarkersGroup.Text = " Маркеры ";
            // 
            // MarkerBottomCombo
            // 
            this.MarkerBottomCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MarkerBottomCombo.FormattingEnabled = true;
            this.MarkerBottomCombo.Location = new System.Drawing.Point(114, 55);
            this.MarkerBottomCombo.Name = "MarkerBottomCombo";
            this.MarkerBottomCombo.Size = new System.Drawing.Size(247, 23);
            this.MarkerBottomCombo.TabIndex = 3;
            // 
            // MarkerTopCombo
            // 
            this.MarkerTopCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MarkerTopCombo.FormattingEnabled = true;
            this.MarkerTopCombo.Location = new System.Drawing.Point(114, 26);
            this.MarkerTopCombo.Name = "MarkerTopCombo";
            this.MarkerTopCombo.Size = new System.Drawing.Size(247, 23);
            this.MarkerTopCombo.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Нижний маркер:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Верхний маркер:";
            // 
            // DrawGroup
            // 
            this.DrawGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DrawGroup.Controls.Add(this.label3);
            this.DrawGroup.Controls.Add(this.AlfaPercent);
            this.DrawGroup.Controls.Add(this.label2);
            this.DrawGroup.Location = new System.Drawing.Point(385, 35);
            this.DrawGroup.Name = "DrawGroup";
            this.DrawGroup.Size = new System.Drawing.Size(231, 130);
            this.DrawGroup.TabIndex = 3;
            this.DrawGroup.TabStop = false;
            this.DrawGroup.Text = " Отображение ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Цвет:";
            // 
            // AlfaPercent
            // 
            this.AlfaPercent.Location = new System.Drawing.Point(113, 26);
            this.AlfaPercent.Name = "AlfaPercent";
            this.AlfaPercent.Size = new System.Drawing.Size(112, 23);
            this.AlfaPercent.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Прозрачность, %";
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(430, 173);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(90, 30);
            this.OkButton.TabIndex = 4;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(526, 173);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 5;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // MarkersZoneEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 215);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.DrawGroup);
            this.Controls.Add(this.MarkersGroup);
            this.Controls.Add(this.NameBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MarkersZoneEditForm";
            this.Text = "Редактирование зоны маркеров";
            this.MarkersGroup.ResumeLayout(false);
            this.MarkersGroup.PerformLayout();
            this.DrawGroup.ResumeLayout(false);
            this.DrawGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AlfaPercent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.GroupBox MarkersGroup;
        private System.Windows.Forms.GroupBox DrawGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown AlfaPercent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox MarkerBottomCombo;
        private System.Windows.Forms.ComboBox MarkerTopCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CancelButton;
    }
}