﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus.Dialogs
{
    public partial class GTMAutoMatchForm : Form
    {
        MainForm mainForm;
        bool ShowedSettings
        {
            get { return ExtraSettingGroup.Visible; }
            set
            {
                ClearBallonTip();
                if (value)
                {
                    ExtraSettingsButton.Image = Properties.Resources.left;
                    this.Size = new Size(765, 390);
                    ExtraSettingsButton.Text = "Скрыть настройки";
                    this.Location = new Point(mainForm.Location.X + mainForm.ClientRectangle.Width / 2 - this.Width / 2, mainForm.Location.Y + mainForm.ClientRectangle.Height / 2 - this.Height / 2);
                }
                else
                {
                    ExtraSettingsButton.Image = Properties.Resources.right;
                    this.Size = new Size(430, 390);
                    ExtraSettingsButton.Text = "Дополнительно";
                    this.Location = new Point(mainForm.Location.X + mainForm.ClientRectangle.Width / 2 - this.Width / 2, mainForm.Location.Y + mainForm.ClientRectangle.Height / 2 - this.Height / 2);
                }
                ExtraSettingGroup.Visible = value;
            }
        }
        ToolTip balloonTip;

        public GTMAutoMatchForm(MainForm mainForm)
        {
            this.mainForm = mainForm; 
            this.Owner = mainForm;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(mainForm.Location.X + mainForm.ClientRectangle.Width / 2 - this.Width / 2, mainForm.Location.Y + mainForm.ClientRectangle.Height / 2 - this.Height / 2);
            
            
            InitializeComponent();

            StartDatePicker.Value = new DateTime(2009, 1, 1);
            EndDatePicker.Value = DateTime.Now;

            ExtraSettingGroup.Visible = false;

            // масштабирование
            int dx = label1.Width - 183;

            if (dx > 0)
            {
                label2.Left += dx;
                EndDatePicker.Left += dx;
                dx += label2.Width - 202;
                this.Width += dx;
            }

            GTMAutoMatchItem[] items = InitGtmAutoMatchItems();
            ListViewItem item;
            for (int i = 0; i < items.Length;i++)
            {
                item = new ListViewItem(items[i].Name);
                item.Checked = true;
                item.ToolTipText = items[i].FullName;
                item.Tag = items[i];
                GtmListListView.Items.Add(item);
            }
            if (GtmListListView.Items.Count > 0) GtmListListView.Items[0].Selected = true;

            RadiusFindWellUpDown.Value = 1000;

            TemplatePathBox.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\МиР\Шаблоны\Шаблон.xltm";

            ShowedSettings = true;

            this.LocationChanged += GTMAutoMatchForm_LocationChanged;
            GtmListListView.ItemCheck += GtmListListView_ItemCheck;
        }

        void GtmListListView_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (GtmListListView.Items[e.Index].Tag != null)
            {
                ((GTMAutoMatchItem)GtmListListView.Items[e.Index].Tag).Checked = e.NewValue == CheckState.Checked;
            }

        }

        void GTMAutoMatchForm_LocationChanged(object sender, EventArgs e)
        {
            ClearBallonTip();
        }

        GTMAutoMatchItem[] InitGtmAutoMatchItems()
        {
            List<GTMAutoMatchItem> items = new List<GTMAutoMatchItem>();

            items.Add(new GTMAutoMatchItem(new int[] { 5001 }, "ГРП проппантный", "ГРП проппантный", -5f, 0, -4));
            items.Add(new GTMAutoMatchItem(new int[] { 5002 }, "ГРП кислотный", "ГРП кислотный", -3, 0, -2));
            items.Add(new GTMAutoMatchItem(new int[] { 10000, 10001, 10002, 10004 }, "ОПЗ", "ОПЗ", -1, 8, 0));
            items.Add(new GTMAutoMatchItem(new int[] { 11000, 11001, 11002, 11003, 11004 }, "Реперфорация", "Реперфорация", -1, 5, 0));
            items.Add(new GTMAutoMatchItem(new int[] { 6000, 6001, 6002, 6003 }, "ПВЛГ", "ПВЛГ", -1, 5, 0));
            items.Add(new GTMAutoMatchItem(new int[] { 4000, 4001, 4003 }, "Углубление", "Углубление", -1, 5, 0));

            return items.ToArray();
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            List<GTMAutoMatchItem> selectedItems = new List<GTMAutoMatchItem>();
            for(int i = 0; i < GtmListListView.Items.Count;i++)
            {
                if (GtmListListView.Items[i].Tag != null)
                {
                    selectedItems.Add((GTMAutoMatchItem)GtmListListView.Items[i].Tag);
                }
            }
            if (!System.IO.File.Exists(TemplatePathBox.Text))
            {
                MessageBox.Show(mainForm, "Задайте путь файлу шаблона выгрузки!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (WellListTextBoxEx.Tag == null && MessageBox.Show(mainForm, "Список скважин для подбора проницаемости не задан.Расчеты будут произведены по всем скважинам проекта.Это может занять продолжительное время. Продолжить?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
            {
                return;
            }
            if (selectedItems.Count > 0)
            {
                object[] arg = new object[10];
                arg[0] = TemplatePathBox.Text;
                arg[1] = StartDatePicker.Value;
                arg[2] = EndDatePicker.Value;
                arg[3] = selectedItems;
                arg[4] = WellListTextBoxEx.Tag;
                arg[5] = Convert.ToInt32(RadiusFindWellUpDown.Value);
                arg[6] = CalcMultiPlastCodeCheckBox.Checked;
                arg[7] = UsePiezometricWells.Checked;
                arg[8] = Convert.ToDouble(ReUpDown.Value);
                arg[9] = Convert.ToDouble(RcUpDown.Value);
                mainForm.worker.Run(Constant.BG_WORKER_WORK_TYPE.GTM_AUTOMATCH_PERMEABILITY, arg);
            }
            if (mainForm.fAppSettings != null)
            {
                mainForm.fAppSettings.WriteGtmAutoMatchFormSettings(this);
            }
            this.Close();
        }

        private void ExtraSettingsButton_MouseEnter(object sender, EventArgs e)
        {
            ExtraSettingsButton.Image = (ShowedSettings) ? Properties.Resources.left_over : Properties.Resources.right_over;
            ExtraSettingsButton.BackColor = SystemColors.Control;
        }
        private void ExtraSettingsButton_MouseLeave(object sender, EventArgs e)
        {
            ExtraSettingsButton.Image = (ShowedSettings) ? Properties.Resources.left : Properties.Resources.right;
            ExtraSettingsButton.BackColor = SystemColors.Control;
        }
        private void ExtraSettingsButton_Click(object sender, EventArgs e)
        {
            ShowedSettings = !ShowedSettings;
        }

        private void GtmListListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GtmListListView.SelectedItems.Count > 0 && GtmListListView.SelectedItems[0].Tag != null)
            {
                GTMAutoMatchItem item = (GTMAutoMatchItem)GtmListListView.SelectedItems[0].Tag;
                MinSkinUpDown.Value = Convert.ToDecimal(item.MinSkin);
                MaxSkinUpDown.Value = Convert.ToDecimal(item.MaxSkin);
                StartSkinUpDown.Value = Convert.ToDecimal(item.StartSkin);
            }
        }

        private void AddWellListButton_Click(object sender, EventArgs e)
        {
            C2DLayer layer = null;
            if (mainForm.canv.twWork.SelectedNodes.Count > 0)
            {
                TreeNode tn;
                for (int i = 0; i < mainForm.canv.twWork.SelectedNodes.Count; i++)
                {
                    tn = mainForm.canv.twWork.SelectedNodes[i];
                    if (tn.Tag != null && ((C2DLayer)tn.Tag).ObjTypeID == Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL)
                    {
                        layer = (C2DLayer)tn.Tag;
                        break;
                    }
                }
            }
            else if (mainForm.canv.twWork.SelectedWellListNode != null && mainForm.canv.twWork.SelectedWellListNode.Tag != null)
            {
                layer = (C2DLayer)mainForm.canv.twWork.SelectedWellListNode.Tag;
            }

            if (layer != null)
            {
                WellListTextBoxEx.TextBox.Text = layer.Name + " (Рабочая папка)";
                WellListTextBoxEx.Tag = layer;
            }
            if (WellListTextBoxEx.Tag == null)
            {
                int h = this.DesktopBounds.Height - this.ClientRectangle.Height + AddWellListButton.Height;
                int x = ExtraSettingGroup.Location.X + AddWellListButton.Location.X;
                int y = ExtraSettingGroup.Location.Y + AddWellListButton.Location.Y - h;
                ShowBallonToolTip(x + 5, y, "Выделите список скважин в рабочей папке");
            }
        }
        void ShowBallonToolTip(int x, int y, string Text)
        {
            if (balloonTip != null) balloonTip.Dispose();
            balloonTip = new ToolTip();
            balloonTip.ToolTipTitle = "Внимание!";
            balloonTip.ToolTipIcon = ToolTipIcon.Info;
            balloonTip.UseFading = true;
            balloonTip.UseAnimation = true;
            balloonTip.IsBalloon = true;
            balloonTip.Show(Text, this, x, y, 1500);
        }
        void ClearBallonTip()
        {
            if (balloonTip != null)
            {
                balloonTip.Dispose();
                balloonTip = null;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TemplateOpenButton_Click(object sender, EventArgs e)
        {
            //TemplatePathBox.Text = @"C:\Users\bashirovia\Documents\МиР.Задачи\Автоматчинг ГТМ\Шаблон.xlsm";
            //return;

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Выберите файл шаблона расчета по ГТМ";
            dlg.DefaultExt = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            dlg.Filter = "Excel файлы (*.xls)|*.xlsx;*.xlsb;*.xlsm;*.xls;*xlm;*.xlt;*.xltm;*.xltb";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TemplatePathBox.Text = dlg.FileName;
            }
            dlg.Dispose();
        }

        public void ReadSetting(int version, BinaryReader br)
        {
            StartDatePicker.Value = DateTime.FromOADate(br.ReadDouble());
            EndDatePicker.Value = DateTime.FromOADate(br.ReadDouble());
            TemplatePathBox.Text = br.ReadString();
            RadiusFindWellUpDown.Value = br.ReadInt32();
            UsePiezometricWells.Checked = br.ReadBoolean();
            ReUpDown.Value = Convert.ToDecimal(br.ReadDouble());
            RcUpDown.Value = Convert.ToDecimal(br.ReadDouble());
            CalcMultiPlastCodeCheckBox.Checked = br.ReadBoolean();
            int count = br.ReadInt32();
            bool needSetToForm = false;
            for (int i = 0; i < count; i++)
            {
                string name = br.ReadString();
                bool check = br.ReadBoolean();
                float minSkin = br.ReadSingle();
                float maxSkin = br.ReadSingle();
                float startSkin = br.ReadSingle();
                for (int j = 0; j < GtmListListView.Items.Count; j++)
                {
                    var item = (GTMAutoMatchItem)GtmListListView.Items[j].Tag;
                    if (item != null && item.Name == name)
                    {
                        item.MinSkin = minSkin;
                        item.MaxSkin = maxSkin;
                        item.StartSkin = startSkin;
                        needSetToForm = true;
                        break;
                    }
                }
            }
            if (needSetToForm && GtmListListView.SelectedItems.Count > 0 && GtmListListView.SelectedItems[0].Tag != null)
            {
                GTMAutoMatchItem item = (GTMAutoMatchItem)GtmListListView.SelectedItems[0].Tag;
                MinSkinUpDown.Value = Convert.ToDecimal(item.MinSkin);
                MaxSkinUpDown.Value = Convert.ToDecimal(item.MaxSkin);
                StartSkinUpDown.Value = Convert.ToDecimal(item.StartSkin);
            }
        }
        public void WriteSetting(BinaryWriter bw)
        {
            bw.Write(StartDatePicker.Value.ToOADate());
            bw.Write(EndDatePicker.Value.ToOADate());
            bw.Write(TemplatePathBox.Text);
            bw.Write((int)RadiusFindWellUpDown.Value);
            bw.Write(UsePiezometricWells.Checked);
            bw.Write((double)ReUpDown.Value);
            bw.Write((double)RcUpDown.Value);
            bw.Write(CalcMultiPlastCodeCheckBox.Checked);
            bw.Write(GtmListListView.Items.Count);
            for (int i = 0; i < GtmListListView.Items.Count; i++)
            {
                var item = (GTMAutoMatchItem)GtmListListView.Items[i].Tag;
                bw.Write(item.Name);
                bw.Write(GtmListListView.Items[i].Checked);
                bw.Write(item.MinSkin);
                bw.Write(item.MaxSkin);
                bw.Write(item.StartSkin);
            }
        }

        private void MinSkinUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (GtmListListView.SelectedItems.Count > 0 && GtmListListView.SelectedItems[0].Tag != null)
            {
                var item = (GTMAutoMatchItem)GtmListListView.SelectedItems[0].Tag;
                item.MinSkin = Convert.ToSingle(MinSkinUpDown.Value);
            }
        }
        private void MaxSkinUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (GtmListListView.SelectedItems.Count > 0 && GtmListListView.SelectedItems[0].Tag != null)
            {
                var item = (GTMAutoMatchItem)GtmListListView.SelectedItems[0].Tag;
                item.MaxSkin = Convert.ToSingle(MaxSkinUpDown.Value);
            }
        }
        private void StartSkinUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (GtmListListView.SelectedItems.Count > 0 && GtmListListView.SelectedItems[0].Tag != null)
            {
                var item = (GTMAutoMatchItem)GtmListListView.SelectedItems[0].Tag;
                item.StartSkin = Convert.ToSingle(StartSkinUpDown.Value);
            }
        }
    }

    public class GTMAutoMatchItem
    {
        public bool Checked;
        public int[] GtmCodes;
        public string Name;
        public string FullName;
        public float MinSkin;
        public float MaxSkin;
        public float StartSkin;

        public GTMAutoMatchItem(int[] GtmCodes, string Name, string FullName, float MinSkin, float MaxSkin, float StartSkin)
        {
            this.GtmCodes = GtmCodes;
            this.Name = Name;
            this.FullName = FullName;
            this.MinSkin = MinSkin;
            this.MaxSkin = MaxSkin;
            this.StartSkin = StartSkin;
        }
    }
}