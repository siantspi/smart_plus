﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SmartPlus
{
    public partial class WatsNewForm : Form
    {
        public WatsNewForm(MainForm mainForm)
        {
            InitializeComponent();
            string watsnewspath = Path.Combine(Application.StartupPath, "whatnews.txt");
            string text;
            if (File.Exists(watsnewspath))
                text = System.IO.File.ReadAllText(watsnewspath);
            else
                text = "Не найден whatnews.txt";
            this.WatsNewtextBox.Text = text;
            this.lVersion.Text = String.Format("Версия: {0}", mainForm.AssemblyVersion);
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
