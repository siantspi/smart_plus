﻿namespace SmartPlus
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lMain = new System.Windows.Forms.Label();
            this.lElementTitle = new System.Windows.Forms.Label();
            this.lTimeTitle = new System.Windows.Forms.Label();
            this.lElement = new System.Windows.Forms.Label();
            this.lTime = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bCancel = new System.Windows.Forms.Button();
            this.bDown = new System.Windows.Forms.Button();
            this.pb = new System.Windows.Forms.ProgressBar();
            this.bUp = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lMain
            // 
            this.lMain.AutoSize = true;
            this.lMain.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lMain.Location = new System.Drawing.Point(10, 7);
            this.lMain.Name = "lMain";
            this.lMain.Size = new System.Drawing.Size(43, 23);
            this.lMain.TabIndex = 0;
            this.lMain.Text = "title";
            // 
            // lElementTitle
            // 
            this.lElementTitle.AutoSize = true;
            this.lElementTitle.Location = new System.Drawing.Point(11, 39);
            this.lElementTitle.Name = "lElementTitle";
            this.lElementTitle.Size = new System.Drawing.Size(150, 15);
            this.lElementTitle.TabIndex = 1;
            this.lElementTitle.Text = "Обрабатываемый объект";
            // 
            // lTimeTitle
            // 
            this.lTimeTitle.AutoSize = true;
            this.lTimeTitle.Location = new System.Drawing.Point(11, 54);
            this.lTimeTitle.Name = "lTimeTitle";
            this.lTimeTitle.Size = new System.Drawing.Size(116, 15);
            this.lTimeTitle.TabIndex = 4;
            this.lTimeTitle.Text = "Оставшееся время: ";
            // 
            // lElement
            // 
            this.lElement.AutoSize = true;
            this.lElement.Location = new System.Drawing.Point(260, 39);
            this.lElement.Name = "lElement";
            this.lElement.Size = new System.Drawing.Size(33, 15);
            this.lElement.TabIndex = 7;
            this.lElement.Text = "elem";
            // 
            // lTime
            // 
            this.lTime.AutoSize = true;
            this.lTime.Location = new System.Drawing.Point(260, 54);
            this.lTime.Name = "lTime";
            this.lTime.Size = new System.Drawing.Size(30, 15);
            this.lTime.TabIndex = 8;
            this.lTime.Text = "time";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.bCancel);
            this.panel1.Controls.Add(this.bDown);
            this.panel1.Controls.Add(this.pb);
            this.panel1.Controls.Add(this.bUp);
            this.panel1.Location = new System.Drawing.Point(0, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(529, 70);
            this.panel1.TabIndex = 9;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.AutoSize = true;
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(433, 39);
            this.bCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(88, 25);
            this.bCancel.TabIndex = 7;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Visible = false;
            // 
            // bDown
            // 
            this.bDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDown.AutoSize = true;
            this.bDown.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.bDown.FlatAppearance.BorderSize = 0;
            this.bDown.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bDown.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bDown.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bDown.Image = global::SmartPlus.Properties.Resources.down;
            this.bDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bDown.Location = new System.Drawing.Point(8, 36);
            this.bDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bDown.Name = "bDown";
            this.bDown.Size = new System.Drawing.Size(103, 30);
            this.bDown.TabIndex = 9;
            this.bDown.Text = "Подробнее";
            this.bDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bDown.UseVisualStyleBackColor = true;
            this.bDown.Click += new System.EventHandler(this.bDown_Click);
            this.bDown.MouseLeave += new System.EventHandler(this.bDown_MouseLeave);
            this.bDown.MouseMove += new System.Windows.Forms.MouseEventHandler(this.bDown_MouseMove);
            // 
            // pb
            // 
            this.pb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb.ForeColor = System.Drawing.Color.LimeGreen;
            this.pb.Location = new System.Drawing.Point(4, 7);
            this.pb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(517, 22);
            this.pb.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pb.TabIndex = 8;
            // 
            // bUp
            // 
            this.bUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bUp.AutoSize = true;
            this.bUp.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.bUp.FlatAppearance.BorderSize = 0;
            this.bUp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bUp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bUp.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bUp.Image = global::SmartPlus.Properties.Resources.up;
            this.bUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bUp.Location = new System.Drawing.Point(8, 36);
            this.bUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bUp.Name = "bUp";
            this.bUp.Size = new System.Drawing.Size(141, 30);
            this.bUp.TabIndex = 10;
            this.bUp.Text = "Меньше сведений";
            this.bUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bUp.UseVisualStyleBackColor = true;
            this.bUp.Visible = false;
            this.bUp.Click += new System.EventHandler(this.bUp_Click);
            this.bUp.MouseLeave += new System.EventHandler(this.bUp_MouseLeave);
            this.bUp.MouseMove += new System.Windows.Forms.MouseEventHandler(this.bUp_MouseMove);
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(529, 109);
            this.Controls.Add(this.lMain);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lTime);
            this.Controls.Add(this.lElement);
            this.Controls.Add(this.lTimeTitle);
            this.Controls.Add(this.lElementTitle);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Выполняется операция...";
            this.Activated += new System.EventHandler(this.formProgress_Activated);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lTimeTitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bUp;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bDown;
        public System.Windows.Forms.Label lMain;
        public System.Windows.Forms.Label lElement;
        public System.Windows.Forms.Label lTime;
        public System.Windows.Forms.ProgressBar pb;
        public System.Windows.Forms.Label lElementTitle;
    }
}