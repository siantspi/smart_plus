﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class OilFieldSelectForm : Form
    {
        Project ParentProj;
        int SelectIndex;

        public OilFieldSelectForm()
        {
            InitializeComponent();
            ParentProj = null;
            SelectIndex = -1;
        }

        public void Show(Project proj, string Text)
        {
            SelectIndex = -1;
            ParentProj = proj;
            cbOilFields.Items.Clear();
            lComment.Text = Text;
            for(int i = 1; i < ParentProj.OilFields.Count;i++)
            {
                cbOilFields.Items.Add(ParentProj.OilFields[i].Name);
            }
            if (this.ShowDialog() == DialogResult.OK)
            {
                if (cbOilFields.SelectedIndex > -1)
                {
                    this.SelectIndex = cbOilFields.SelectedIndex + 1;
                }
            }
        }
    }
}
