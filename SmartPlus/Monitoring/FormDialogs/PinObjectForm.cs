﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SmartPlus.DictionaryObjects;

namespace SmartPlus
{
    class PinObjectForm : Form
    {
        Project project;
        int Mode;
        private TextBox InputTextBox;
        private Label InputTextTitle;
        private Button FileOpenBtn;
        private Button OkButton;
        private Label PinObjectTitle;
        private Label PinObjectName;
        private new Button CancelButton;

        private void InitializeComponent()
        {
            this.InputTextBox = new System.Windows.Forms.TextBox();
            this.InputTextTitle = new System.Windows.Forms.Label();
            this.FileOpenBtn = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.PinObjectTitle = new System.Windows.Forms.Label();
            this.PinObjectName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // InputTextBox
            // 
            this.InputTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.InputTextBox.Location = new System.Drawing.Point(58, 6);
            this.InputTextBox.Name = "InputTextBox";
            this.InputTextBox.Size = new System.Drawing.Size(371, 23);
            this.InputTextBox.TabIndex = 0;
            // 
            // InputTextTitle
            // 
            this.InputTextTitle.AutoSize = true;
            this.InputTextTitle.Location = new System.Drawing.Point(12, 9);
            this.InputTextTitle.Name = "InputTextTitle";
            this.InputTextTitle.Size = new System.Drawing.Size(40, 15);
            this.InputTextTitle.TabIndex = 5;
            this.InputTextTitle.Text = "Файл:";
            // 
            // FileOpenBtn
            // 
            this.FileOpenBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FileOpenBtn.Image = global::SmartPlus.Properties.Resources.load_table_folder;
            this.FileOpenBtn.Location = new System.Drawing.Point(435, 5);
            this.FileOpenBtn.Name = "FileOpenBtn";
            this.FileOpenBtn.Size = new System.Drawing.Size(26, 24);
            this.FileOpenBtn.TabIndex = 1;
            this.FileOpenBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.FileOpenBtn.UseVisualStyleBackColor = true;
            this.FileOpenBtn.Click += new System.EventHandler(this.FileOpenBtn_Click);
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(235, 130);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(90, 30);
            this.OkButton.TabIndex = 6;
            this.OkButton.Text = "Создать";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(371, 130);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // PinObjectTitle
            // 
            this.PinObjectTitle.AutoSize = true;
            this.PinObjectTitle.Location = new System.Drawing.Point(12, 47);
            this.PinObjectTitle.Name = "PinObjectTitle";
            this.PinObjectTitle.Size = new System.Drawing.Size(156, 15);
            this.PinObjectTitle.TabIndex = 8;
            this.PinObjectTitle.Text = "Привязать файл к объекту:";
            // 
            // PinObjectName
            // 
            this.PinObjectName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PinObjectName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.PinObjectName.Location = new System.Drawing.Point(12, 62);
            this.PinObjectName.Name = "PinObjectName";
            this.PinObjectName.Size = new System.Drawing.Size(449, 65);
            this.PinObjectName.TabIndex = 9;
            // 
            // PinObjectForm
            // 
            this.AcceptButton = this.OkButton;
            this.CancelButton = this.CancelButton;
            this.ClientSize = new System.Drawing.Size(473, 172);
            this.Controls.Add(this.PinObjectName);
            this.Controls.Add(this.PinObjectTitle);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.FileOpenBtn);
            this.Controls.Add(this.InputTextTitle);
            this.Controls.Add(this.InputTextBox);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PinObjectForm";
            this.Text = "Загрузка файла на сервер";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public PinObjectForm(Project project)
        {
            InitializeComponent();

            this.project = project;
            if (project != null)
            {
                var list = new AutoCompleteStringCollection();
                for (int i = 0; i < project.OilFields.Count; i++)
                {
                    list.Add(project.OilFields[i].Name);
                }
            }
            this.FormClosing += new FormClosingEventHandler(UploadFileForm_FormClosing);
        }

        void UploadFileForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                if (Mode == 0 && !System.IO.File.Exists(InputTextBox.Text))
                {
                    MessageBox.Show(this, "Выберите файл для загрузки на сервер!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                    e.Cancel = true;
                    return;
                }
                else if (Mode == 0)
                {
                    try
                    {
                        System.IO.FileStream fs = new System.IO.FileStream(InputTextBox.Text, System.IO.FileMode.Open);
                        fs.Close();
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(this, ex.Message, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
 
                }
                else if (Mode == 1 && InputTextBox.Text.Length == 0)
                {
                    MessageBox.Show(this, "Задайте имя новой темы!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }
        }

        string GetObjectName(BaseObj PinObject)
        {
            string name = string.Empty;
            switch (PinObject.TypeID)
            {
                case Constant.BASE_OBJ_TYPES_ID.PROJECT:
                    name = "Регион: " + PinObject.Name;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.NGDU:
                    for (int i = 0; i < project.OilFields.Count; i++)
                    {
                        if (project.OilFields[i].ParamsDict.NGDUCode == PinObject.Index)
                        {
                            name = "НГДУ " + project.OilFields[i].ParamsDict.NGDUName;
                            break;
                        }
                    }
                    break;
                case Constant.BASE_OBJ_TYPES_ID.OILFIELD:
                    name = "Мест: " + project.OilFields[PinObject.Index].Name;
                    break;
                case Constant.BASE_OBJ_TYPES_ID.AREA:
                    Area area = (Area)PinObject;
                    var of = project.OilFields[area.OilFieldIndex];
                    name = string.Format("ЯЗ: {0} ({1})", area.Name, of.Name);
                    break;
                case Constant.BASE_OBJ_TYPES_ID.WELL:
                    Well w = (Well)PinObject;
                    of = project.OilFields[w.OilFieldIndex];
                    var dict = (OilFieldAreaDictionary)project.DictList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
                    string areaName = dict.GetShortNameByCode(w.OilFieldAreaCode);
                    if (areaName.Length > 0)
                    {
                        areaName = ", " + areaName;
                        if (areaName.IndexOf("участок") == -1) areaName += " площадь";
                    }
                    name = string.Format("Скв: {0} ({1}{2})", w.Name, of.Name, areaName);
                    break;
            }
            return name;
        }

        public static object[] ShowPinFileForm(MainForm mainForm, BaseObj PinObject, string FilePath)
        {
            object[] result = null;
            Project project = mainForm.GetLastProject();
            PinObjectForm form = new PinObjectForm(project);
            string objName = form.GetObjectName(PinObject);
            if (PinObject == null || objName.Length == 0)
            {
                MessageBox.Show(mainForm, "Выберите объект для привязки файла!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
            form.Mode = 0;
            form.Owner = mainForm;
            form.InputTextTitle.Text = "Файл:";
            form.Text = "Загрузка файла на сервер";
            form.OkButton.Location = new System.Drawing.Point(235, 130);
            form.OkButton.Text = "Загрузить на сервер";
            form.OkButton.Width = 130;
            form.StartPosition = FormStartPosition.CenterParent;
            form.FileOpenBtn.Visible = true;
            form.InputTextBox.Location = new System.Drawing.Point(58, 6);
            form.InputTextBox.Width = 371;
            form.InputTextBox.Text = FilePath;
            form.PinObjectTitle.Text = "Привязать файл к объекту:";
            form.PinObjectName.Text = objName;
            if (form.ShowDialog() == DialogResult.OK)
            {
                result = new object[] { form.InputTextBox.Text };
            }
            return result;
        }
        public static object[] ShowPinThemeForm(MainForm mainForm, BaseObj PinObject)
        {
            object[] result = null;
            Project project = mainForm.GetLastProject();
            PinObjectForm form = new PinObjectForm(project);
            string objName = form.GetObjectName(PinObject);
            if (PinObject == null || objName.Length == 0)
            {
                MessageBox.Show(mainForm, "Выберите объект для привязки темы!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
            form.Mode = 1;
            form.Owner = mainForm;
            form.InputTextTitle.Text = "Имя темы:";
            form.Text = "Создание темы обсуждения";
            form.OkButton.Location = new System.Drawing.Point(275, 130);
            form.OkButton.Text = "Создать";
            form.OkButton.Width = 90;
            form.FileOpenBtn.Visible = false;
            form.InputTextBox.Location = new System.Drawing.Point(82, 6);
            form.InputTextBox.Width = 379;
            form.StartPosition = FormStartPosition.CenterParent;
            form.PinObjectTitle.Text = "Привязать тему к объекту:";
            form.PinObjectName.Text = objName;

            if (form.ShowDialog() == DialogResult.OK)
            {
                result = new object[] { form.InputTextBox.Text };
            }
            return result;
        }
        
        private void FileOpenBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Выберите файл для загрузки...";
            dlg.Multiselect = false;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                InputTextBox.Text = dlg.FileName;
            }
        }
    }
}
