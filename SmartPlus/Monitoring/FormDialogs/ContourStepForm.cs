﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus.Dialogs
{
    class ContourStepForm : Form
    {
        private NumericUpDown StepBox;
        private Label label1;
        private Button bOk;
        private Button bCancel;

        public ContourStepForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }
        private void InitializeComponent()
        {
            this.StepBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.StepBox)).BeginInit();
            this.SuspendLayout();
            // 
            // StepBox
            // 
            this.StepBox.Location = new System.Drawing.Point(165, 12);
            this.StepBox.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.StepBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.StepBox.Name = "StepBox";
            this.StepBox.Size = new System.Drawing.Size(120, 27);
            this.StepBox.TabIndex = 0;
            this.StepBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Шаг усреднения, м";
            // 
            // bOk
            // 
            this.bOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(335, 59);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(95, 30);
            this.bOk.TabIndex = 2;
            this.bOk.Text = "OK";
            this.bOk.UseVisualStyleBackColor = true;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(436, 59);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(95, 30);
            this.bCancel.TabIndex = 3;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // ContourStepForm
            // 
            this.AcceptButton = this.bOk;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(543, 96);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StepBox);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContourStepForm";
            this.Text = "Задайте новый шаг контура";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContourStepForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.StepBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        void ContourStepForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                if ((StepBox.Value < 1) || (StepBox.Value > 5000))
                {
                    MessageBox.Show("Вводимое значение должно быть от 1 до 5000 м", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }
        }


        public static int ShowForm(Form Owner)
        {
            ContourStepForm form = new ContourStepForm();
            form.Owner = Owner;
            if (form.ShowDialog() == DialogResult.OK)
            {
                return (int)form.StepBox.Value;
            }
            return -1;
        }
    }
}
