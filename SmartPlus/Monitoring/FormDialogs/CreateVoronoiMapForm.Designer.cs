﻿namespace SmartPlus.Dialogs
{
    partial class CreateVoronoiMapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CreateVoronoiMapBtn = new System.Windows.Forms.Button();
            this.CreateDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.OilfiledCaption = new System.Windows.Forms.Label();
            this.PlastCaption = new System.Windows.Forms.Label();
            this.CancelButton = new System.Windows.Forms.Button();
            this.cbCutOffByCntr = new System.Windows.Forms.CheckBox();
            this.RadiusCutOff = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.OilfieldCheckList = new System.Windows.Forms.CheckedListBox();
            this.cbFilterOilObjects = new System.Windows.Forms.CheckBox();
            this.SettingsGroup = new System.Windows.Forms.GroupBox();
            this.NNTLayerTextBox = new SmartPlus.TextBoxEx();
            this.NNTLoadButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ShowHideSettingsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.RadiusCutOff)).BeginInit();
            this.SettingsGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // CreateVoronoiMapBtn
            // 
            this.CreateVoronoiMapBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateVoronoiMapBtn.AutoSize = true;
            this.CreateVoronoiMapBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.CreateVoronoiMapBtn.Location = new System.Drawing.Point(307, 329);
            this.CreateVoronoiMapBtn.Name = "CreateVoronoiMapBtn";
            this.CreateVoronoiMapBtn.Size = new System.Drawing.Size(108, 31);
            this.CreateVoronoiMapBtn.TabIndex = 0;
            this.CreateVoronoiMapBtn.Text = "Построить...";
            this.CreateVoronoiMapBtn.UseVisualStyleBackColor = true;
            this.CreateVoronoiMapBtn.Click += new System.EventHandler(this.CreateVoronoiMapBtn_Click);
            // 
            // CreateDatePicker
            // 
            this.CreateDatePicker.CustomFormat = "MM.yyyy";
            this.CreateDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.CreateDatePicker.Location = new System.Drawing.Point(164, 12);
            this.CreateDatePicker.Name = "CreateDatePicker";
            this.CreateDatePicker.Size = new System.Drawing.Size(148, 27);
            this.CreateDatePicker.TabIndex = 1;
            this.CreateDatePicker.ValueChanged += new System.EventHandler(this.CreateDatePicker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Построить на дату:";
            // 
            // OilfiledCaption
            // 
            this.OilfiledCaption.AutoSize = true;
            this.OilfiledCaption.Location = new System.Drawing.Point(12, 77);
            this.OilfiledCaption.Name = "OilfiledCaption";
            this.OilfiledCaption.Size = new System.Drawing.Size(134, 21);
            this.OilfiledCaption.TabIndex = 3;
            this.OilfiledCaption.Text = "Месторождения:";
            // 
            // PlastCaption
            // 
            this.PlastCaption.AutoSize = true;
            this.PlastCaption.Location = new System.Drawing.Point(322, 9);
            this.PlastCaption.Name = "PlastCaption";
            this.PlastCaption.Size = new System.Drawing.Size(156, 21);
            this.PlastCaption.TabIndex = 5;
            this.PlastCaption.Text = "Объект для расчета:";
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.AutoSize = true;
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(421, 329);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 31);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // cbCutOffByCntr
            // 
            this.cbCutOffByCntr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCutOffByCntr.AutoSize = true;
            this.cbCutOffByCntr.Location = new System.Drawing.Point(239, 22);
            this.cbCutOffByCntr.Name = "cbCutOffByCntr";
            this.cbCutOffByCntr.Size = new System.Drawing.Size(228, 25);
            this.cbCutOffByCntr.TabIndex = 9;
            this.cbCutOffByCntr.Text = "Обрезать по контурам ВНК";
            this.cbCutOffByCntr.UseVisualStyleBackColor = true;
            this.cbCutOffByCntr.Visible = false;
            // 
            // RadiusCutOff
            // 
            this.RadiusCutOff.Location = new System.Drawing.Point(164, 41);
            this.RadiusCutOff.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.RadiusCutOff.Name = "RadiusCutOff";
            this.RadiusCutOff.Size = new System.Drawing.Size(148, 27);
            this.RadiusCutOff.TabIndex = 10;
            this.RadiusCutOff.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(188, 21);
            this.label2.TabIndex = 11;
            this.label2.Text = "Радиус дренирования, м";
            // 
            // OilfieldCheckList
            // 
            this.OilfieldCheckList.CheckOnClick = true;
            this.OilfieldCheckList.FormattingEnabled = true;
            this.OilfieldCheckList.Location = new System.Drawing.Point(119, 77);
            this.OilfieldCheckList.Name = "OilfieldCheckList";
            this.OilfieldCheckList.Size = new System.Drawing.Size(193, 92);
            this.OilfieldCheckList.TabIndex = 12;
            this.OilfieldCheckList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.OilfieldCheckList_ItemCheck);
            // 
            // cbFilterOilObjects
            // 
            this.cbFilterOilObjects.AutoSize = true;
            this.cbFilterOilObjects.Location = new System.Drawing.Point(6, 22);
            this.cbFilterOilObjects.Name = "cbFilterOilObjects";
            this.cbFilterOilObjects.Size = new System.Drawing.Size(356, 25);
            this.cbFilterOilObjects.TabIndex = 13;
            this.cbFilterOilObjects.Text = "Фильтровать скважины по объектам добычи";
            this.cbFilterOilObjects.UseVisualStyleBackColor = true;
            // 
            // SettingsGroup
            // 
            this.SettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsGroup.Controls.Add(this.NNTLayerTextBox);
            this.SettingsGroup.Controls.Add(this.NNTLoadButton);
            this.SettingsGroup.Controls.Add(this.label3);
            this.SettingsGroup.Controls.Add(this.cbFilterOilObjects);
            this.SettingsGroup.Controls.Add(this.cbCutOffByCntr);
            this.SettingsGroup.Location = new System.Drawing.Point(12, 231);
            this.SettingsGroup.Name = "SettingsGroup";
            this.SettingsGroup.Size = new System.Drawing.Size(499, 93);
            this.SettingsGroup.TabIndex = 14;
            this.SettingsGroup.TabStop = false;
            this.SettingsGroup.Text = " Дополнительные настройки ";
            // 
            // NNTLayerTextBox
            // 
            this.NNTLayerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NNTLayerTextBox.BackColor = System.Drawing.Color.White;
            this.NNTLayerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NNTLayerTextBox.Location = new System.Drawing.Point(42, 62);
            this.NNTLayerTextBox.Name = "NNTLayerTextBox";
            this.NNTLayerTextBox.ReadOnly = false;
            this.NNTLayerTextBox.Size = new System.Drawing.Size(451, 21);
            this.NNTLayerTextBox.TabIndex = 16;
            this.NNTLayerTextBox.ToolTipText = null;
            // 
            // NNTLoadButton
            // 
            this.NNTLoadButton.Image = global::SmartPlus.Properties.Resources.MoveNextBlue16;
            this.NNTLoadButton.Location = new System.Drawing.Point(6, 62);
            this.NNTLoadButton.Name = "NNTLoadButton";
            this.NNTLoadButton.Size = new System.Drawing.Size(30, 22);
            this.NNTLoadButton.TabIndex = 15;
            this.NNTLoadButton.UseVisualStyleBackColor = true;
            this.NNTLoadButton.Click += new System.EventHandler(this.AddSelectedGridButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(256, 21);
            this.label3.TabIndex = 14;
            this.label3.Text = "Сетка начальных неф.нас.толщин:";
            // 
            // ShowHideSettingsButton
            // 
            this.ShowHideSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowHideSettingsButton.AutoSize = true;
            this.ShowHideSettingsButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.ShowHideSettingsButton.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.ShowHideSettingsButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.ShowHideSettingsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShowHideSettingsButton.Image = global::SmartPlus.Properties.Resources.up;
            this.ShowHideSettingsButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ShowHideSettingsButton.Location = new System.Drawing.Point(12, 327);
            this.ShowHideSettingsButton.Name = "ShowHideSettingsButton";
            this.ShowHideSettingsButton.Size = new System.Drawing.Size(177, 33);
            this.ShowHideSettingsButton.TabIndex = 15;
            this.ShowHideSettingsButton.Text = "Скрыть настройки";
            this.ShowHideSettingsButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ShowHideSettingsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ShowHideSettingsButton.UseVisualStyleBackColor = true;
            this.ShowHideSettingsButton.Click += new System.EventHandler(this.ShowHideSettingsButton_Click);
            this.ShowHideSettingsButton.MouseEnter += new System.EventHandler(this.ShowHideSettingsButton_MouseEnter);
            this.ShowHideSettingsButton.MouseLeave += new System.EventHandler(this.ShowHideSettingsButton_MouseLeave);
            // 
            // CreateVoronoiMapForm
            // 
            this.AcceptButton = this.CreateVoronoiMapBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 372);
            this.Controls.Add(this.ShowHideSettingsButton);
            this.Controls.Add(this.SettingsGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RadiusCutOff);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.PlastCaption);
            this.Controls.Add(this.OilfiledCaption);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CreateDatePicker);
            this.Controls.Add(this.CreateVoronoiMapBtn);
            this.Controls.Add(this.OilfieldCheckList);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateVoronoiMapForm";
            this.Text = "Построение областей Вороного";
            ((System.ComponentModel.ISupportInitialize)(this.RadiusCutOff)).EndInit();
            this.SettingsGroup.ResumeLayout(false);
            this.SettingsGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreateVoronoiMapBtn;
        private System.Windows.Forms.DateTimePicker CreateDatePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label OilfiledCaption;
        private System.Windows.Forms.Label PlastCaption;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.CheckBox cbCutOffByCntr;
        private System.Windows.Forms.NumericUpDown RadiusCutOff;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox OilfieldCheckList;
        private System.Windows.Forms.CheckBox cbFilterOilObjects;
        private System.Windows.Forms.GroupBox SettingsGroup;
        private System.Windows.Forms.Button ShowHideSettingsButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button NNTLoadButton;
        private TextBoxEx NNTLayerTextBox;
    }
}