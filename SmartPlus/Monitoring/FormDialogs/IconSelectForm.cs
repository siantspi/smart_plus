﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace SmartPlus
{
    public partial class IconSelectForm : Form
    {
        ComboBoxExColor cbColor;
        Font[] FontList = null;
        Font largeFont;
        int ActiveFont;
        int Columns, Rows;
        int dx;
        int selInd;
        IconListForm mainForm;
        int startRow;
        System.Drawing.Text.PrivateFontCollection pfc;

        public int SelInd
        {
            get { return selInd; }
            set 
            {
                selInd = value;
                if (value == -1)
                    lSymbInd.Text = "Индекс символа:";
                else
                    lSymbInd.Text = "Индекс символа: " + selInd.ToString();
            }
        }
        public int SelFont
        {
            get { return cbFonts.SelectedIndex; }
            set 
            {
                if ((value > -1) && (value < cbFonts.Items.Count))
                {
                    cbFonts.SelectedIndex = value;
                    ActiveFont = cbFonts.SelectedIndex;
                    largeFont = new Font(FontList[ActiveFont].FontFamily, 40);
                    SelInd = -1;
                }
            }
        }
        public int SelColor
        {
            get { return cbColor.SelectedColor.ToArgb(); }
            set { cbColor.SelectByArgb(value); }
        }
        public ushort SelSize 
        {
            get { return (ushort)udSize.Value; }
            set { if (value > 0) udSize.Value = value; }
        }

        public IconSelectForm(IconListForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            InitializeFonts();
            dx = 40;
            startRow = 0;
            pbIconMap.Width = pIconMap.Width - ((vScroll.Visible)? vScroll.Width : 0);
            pbIconMap.Height = pIconMap.Height;
            Columns = (int)((float)(pbIconMap.Width - 2) / (float)dx);
            Rows = (int)((float)(pbIconMap.Height - 2) / (float)dx);
            SetScroll(Columns, Rows);
            pfc = new System.Drawing.Text.PrivateFontCollection();
            byte[] fontBuff = global::SmartPlus.Properties.Resources.SmartPlusFont;
            IntPtr ptrFont = Marshal.AllocCoTaskMem(fontBuff.Length);
            Marshal.Copy(fontBuff, 0, ptrFont, fontBuff.Length);
            pfc.AddMemoryFont(ptrFont, fontBuff.Length);

            cbColor = new ComboBoxExColor();
            this.gbSettings.Controls.Add(cbColor);
            cbColor.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            cbColor.Font = new Font("Segoe UI", 9.25f);
            cbColor.Location = new Point(326, 20);
            cbColor.Size = new Size(277, 23);
            cbColor.SelectedIndex = 16;
            udSize.Value = 10;


            // масштабирование
            int x = lFont.Width - 48;
            if (x > 0)
            {
                cbFonts.Left += x;
                cbFonts.Width -= x;
                x = lColor.Width - 35;
                cbColor.Left += x;
                cbColor.Width -= x;
                x = lSize.Width - 52;
                udSize.Left += x;
                udSize.Width -= x;
            }
            SelInd = -1;
            this.Resize += new EventHandler(IconSelect_Resize);
            pbIconMap.MouseMove += new MouseEventHandler(pbIconMap_MouseMove);
            pbIconMap.MouseDown += new MouseEventHandler(pbIconMap_MouseDown);
            pbIconMap.DoubleClick += new EventHandler(pbIconMap_DoubleClick);
            pbIconMap.Paint += new PaintEventHandler(pbIconMap_Paint);
            pIconMap.Resize += new EventHandler(pIconMap_Resize);
            pbLargeIcon.Paint += new PaintEventHandler(pbLargeIcon_Paint);
            cbColor.SelectedIndexChanged += new EventHandler(cbColor_SelectedIndexChanged);
            this.FormClosing += new FormClosingEventHandler(IconSelect_FormClosing);
        }

        void IconSelect_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((selInd == -1) && (this.DialogResult == DialogResult.OK))
            {
                MessageBox.Show("Выберите значок скважины", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }
        void InitializeFonts()
        {
            int i;
            FontList = new Font[7];

            FontList[0] = new Font(mainForm.FontList[0].FontFamily, 20);

            for (i = 1; i < 7; i++)
            {
                FontList[i] = new Font(mainForm.FontList[i].FontFamily, 20);
            }

            for (i = 0; i < FontList.Length; i++)
            {
                cbFonts.Items.Add(FontList[i].Name);
            }
            ActiveFont = 0;
            cbFonts.SelectedIndex = 0;
            largeFont = new Font(FontList[ActiveFont].FontFamily, 40);
        }
        void IconSelect_Resize(object sender, EventArgs e)
        {
            int allWidth, fWidth, clrWidth, szWidth;
            allWidth = gbSettings.Width;
            fWidth = (int)(allWidth * 0.30f);
            clrWidth = (int)(allWidth * 0.39f);
            szWidth = allWidth - fWidth - clrWidth;

            cbFonts.Width = fWidth - lFont.Width - 12;
            cbColor.Width = clrWidth - lColor.Width - 12;
            udSize.Width = szWidth - lSize.Width - 18;
            cbFonts.Left = lFont.Right + 6;
            lColor.Left = cbFonts.Right + 6;
            cbColor.Left = lColor.Right + 6;
            lSize.Left = cbColor.Right + 6;
            udSize.Left = lSize.Right + 6;
        }
        private void cbFonts_SelectedIndexChanged(object sender, EventArgs e)
        {
            ActiveFont = cbFonts.SelectedIndex;
            largeFont = new Font(FontList[ActiveFont].FontFamily, 40);
            SelInd = -1;
            SetScroll(Columns, Rows);
            pbIconMap.Invalidate();
            pbLargeIcon.Invalidate();
        }

        void cbColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            pbLargeIcon.Invalidate();
        }

        void SetScroll(int Columns, int Rows)
        {
            int maxIcon = (ActiveFont == 0) ? 21 : 225;
            if ((Columns * Rows < maxIcon) && (Columns > 0))
            {
                vScroll.Visible = true;
                vScroll.Maximum = 1 + (int)(225 / Columns) - Rows;
                vScroll.LargeChange = 1;
                vScroll.SmallChange = 1;
            }
            else
            {
                vScroll.Visible = false;
            }
        }
        private void vScroll_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {
                startRow = e.NewValue;
                pbIconMap.Invalidate();
            }
        }

        void DrawFontMap(Graphics grfx)
        {
            grfx.Clear(Color.White);
            int i, j, ind;
            string s;
            float x, y;
            SizeF iconSize;
            grfx.DrawLine(Pens.Black, 1, 1, 1, 1 + Rows * dx);

            for (i = 1; i <= Columns; i++) grfx.DrawLine(Pens.Black, 1 + i * dx, 1, 1 + i * dx, 1 + Rows * dx);

            grfx.DrawLine(Pens.Black, 1, 1, 1 + Columns * dx, 1);
            for (i = 1; i <= Rows; i++) grfx.DrawLine(Pens.Black, 1, 1 + i * dx, 1 + Columns * dx, 1 + i * dx);
            ind = 30 + startRow * Columns;
            Font f = FontList[ActiveFont];
            for (i = 0; (i < Rows) && (ind < 256); i++)
            {
                for (j = 0; (j < Columns) && (ind < 256); j++)
                {
                    if (((ActiveFont == 0) && (ind > 32) && (ind < 51)) || (ActiveFont > 0))
                    {
                        s = Convert.ToChar(ind).ToString();

                        iconSize = grfx.MeasureString(s, f, PointF.Empty, StringFormat.GenericTypographic);
                        x = 1 + (j + 0.5f) * dx - iconSize.Width / 2;
                        y = 1 + (i + 0.5f) * dx - iconSize.Height / 2;

                        if (ind == 126)
                        {
                            if (x < 1 + j * dx + 1) x = 1 + j * dx;
                            if (y < 1 + i * dx + 1) y = 1 + i * dx;
                        }
                        if (ind != selInd)
                        {
                            grfx.DrawString(s, f, Brushes.Black, x, y, StringFormat.GenericTypographic);
                        }
                        else
                        {
                            grfx.FillRectangle(Brushes.SkyBlue, 2 + j * dx, 2 + i * dx, dx - 1, dx - 1);
                            grfx.DrawString(s, f, Brushes.White, x, y, StringFormat.GenericTypographic);
                        }
                    }
                    ind++;
                }
            }
        }
        void pbIconMap_Paint(object sender, PaintEventArgs e)
        {
            DrawFontMap(e.Graphics);
        }
        void pIconMap_Resize(object sender, EventArgs e)
        {
            pbIconMap.Width = pIconMap.Width - ((vScroll.Visible) ? vScroll.Width : 0);
            pbIconMap.Height = pIconMap.Height;
            Columns = (int)((float)(pbIconMap.Width - 2) / (float)dx);
            Rows = (int)((float)(pbIconMap.Height - 2) / (float)dx);
            while ((startRow > 0) && ((startRow + Rows) * Columns > 225))
                startRow--;
            SetScroll(Columns, Rows);
            pbIconMap.Invalidate();
        }
        void pbIconMap_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if ((e.X < Columns * dx) && (e.Y < Rows * dx))
                {
                    int row, col, sel;
                    col = (int)(e.X / dx);
                    row = (int)(e.Y / dx);
                    sel = Columns * (row + startRow) + col + 30;
                    if (((ActiveFont == 0) && (sel > 31) && (sel < 51)) || ((ActiveFont > 0) && (sel < 256)))
                    {
                        SelInd = sel;
                        pbLargeIcon.Invalidate();
                        pbIconMap.Invalidate();
                    }
                }
            }
        }
        void pbIconMap_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.X < Columns * dx) && (e.Y < Rows * dx))
            {
                int row, col, sel;
                col = (int)(e.X / dx);
                row = (int)(e.Y / dx);
                sel = Columns * row + col + 30;
                if (((ActiveFont == 0) && (sel > 31) && (sel < 51)) || ((ActiveFont > 0) && (sel < 256)))
                {
                    SelInd = Columns * row + col + 30;
                    pbLargeIcon.Invalidate();
                    pbIconMap.Invalidate();
                }
            }
            pbIconMap.Focus();
        }
        void pbIconMap_DoubleClick(object sender, EventArgs e)
        {
            bOk.PerformClick();
        }

        void DrawLargeIcon(Graphics grfx)
        {
            SizeF iconSize;
            grfx.Clear(Color.White);
            if ((largeFont != null) && (selInd > -1))
            {
                string s = Convert.ToChar(selInd).ToString();
                iconSize = grfx.MeasureString(s, largeFont, PointF.Empty, StringFormat.GenericTypographic);
                float x = 25 - iconSize.Width / 2;
                float y = 25 - iconSize.Height / 2;
                if (cbColor.SelectedIndex > -1)
                {
                    grfx.DrawString(s, largeFont, new SolidBrush(cbColor.SelectedColor), x, y, StringFormat.GenericTypographic);
                }
                else
                    grfx.DrawString(s, largeFont, Brushes.Black, x, y, StringFormat.GenericTypographic);
            }
        }
        void pbLargeIcon_Paint(object sender, PaintEventArgs e)
        {
            DrawLargeIcon(e.Graphics);
        }
    }
}
