﻿namespace SmartPlus
{
    partial class GlobalProjectsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbGlobalProjList = new System.Windows.Forms.ListBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOk = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbGlobalProjList
            // 
            this.lbGlobalProjList.FormattingEnabled = true;
            this.lbGlobalProjList.ItemHeight = 15;
            this.lbGlobalProjList.Location = new System.Drawing.Point(12, 73);
            this.lbGlobalProjList.Name = "lbGlobalProjList";
            this.lbGlobalProjList.Size = new System.Drawing.Size(341, 289);
            this.lbGlobalProjList.TabIndex = 1;
            this.lbGlobalProjList.DoubleClick += new System.EventHandler(this.lbGlobalProjList_DoubleClick);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.AutoSize = true;
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.FlatAppearance.BorderSize = 0;
            this.bCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCancel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bCancel.Image = global::SmartPlus.Properties.Resources.Cancel24;
            this.bCancel.Location = new System.Drawing.Point(226, 371);
            this.bCancel.MinimumSize = new System.Drawing.Size(127, 50);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(127, 50);
            this.bCancel.TabIndex = 3;
            this.bCancel.Text = " Отмена";
            this.bCancel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.MouseEnter += new System.EventHandler(this.bCancel_MouseEnter);
            this.bCancel.MouseLeave += new System.EventHandler(this.bCancel_MouseLeave);
            // 
            // bOk
            // 
            this.bOk.AutoSize = true;
            this.bOk.FlatAppearance.BorderSize = 0;
            this.bOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOk.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bOk.Image = global::SmartPlus.Properties.Resources.OK24;
            this.bOk.Location = new System.Drawing.Point(12, 371);
            this.bOk.MinimumSize = new System.Drawing.Size(127, 50);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(127, 50);
            this.bOk.TabIndex = 2;
            this.bOk.Text = "Загрузить";
            this.bOk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            this.bOk.MouseEnter += new System.EventHandler(this.bOk_MouseEnter);
            this.bOk.MouseLeave += new System.EventHandler(this.bOk_MouseLeave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(97, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(164, 59);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // GlobalProjectsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(365, 433);
            this.ControlBox = false;
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.lbGlobalProjList);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GlobalProjectsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Activated += new System.EventHandler(this.fGlobalProjects_Activated);
            this.Shown += new System.EventHandler(this.fGlobalProjects_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListBox lbGlobalProjList;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
    }
}