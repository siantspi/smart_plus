﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    class UploadFileForm : Form
    {
        Project project;
        private TextBox PinToOilfieldTextBox;
        private TextBox FilePathTextBox;
        private Label label1;
        private Button FileOpenBtn;
        private CheckBox PinToWellCheckBox;
        private TextBox PinToWellTextBox;
        private Button UploadFileBtn;
        private Button CancelButton;
        private CheckBox PinToOilfieldCheckBox;
    
        private void InitializeComponent()
        {
            this.PinToOilfieldCheckBox = new System.Windows.Forms.CheckBox();
            this.PinToOilfieldTextBox = new System.Windows.Forms.TextBox();
            this.FilePathTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.FileOpenBtn = new System.Windows.Forms.Button();
            this.PinToWellCheckBox = new System.Windows.Forms.CheckBox();
            this.PinToWellTextBox = new System.Windows.Forms.TextBox();
            this.UploadFileBtn = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PinToOilfieldCheckBox
            // 
            this.PinToOilfieldCheckBox.AutoSize = true;
            this.PinToOilfieldCheckBox.Location = new System.Drawing.Point(15, 53);
            this.PinToOilfieldCheckBox.Name = "PinToOilfieldCheckBox";
            this.PinToOilfieldCheckBox.Size = new System.Drawing.Size(223, 19);
            this.PinToOilfieldCheckBox.TabIndex = 2;
            this.PinToOilfieldCheckBox.Text = "Привязать файл к месторождению:";
            this.PinToOilfieldCheckBox.UseVisualStyleBackColor = true;
            this.PinToOilfieldCheckBox.CheckedChanged += new System.EventHandler(this.PinToOilfieldCheckBox_CheckedChanged);
            // 
            // PinToOilfieldTextBox
            // 
            this.PinToOilfieldTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PinToOilfieldTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.PinToOilfieldTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.PinToOilfieldTextBox.Location = new System.Drawing.Point(244, 51);
            this.PinToOilfieldTextBox.Name = "PinToOilfieldTextBox";
            this.PinToOilfieldTextBox.Size = new System.Drawing.Size(217, 23);
            this.PinToOilfieldTextBox.TabIndex = 3;
            // 
            // FilePathTextBox
            // 
            this.FilePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.FilePathTextBox.Location = new System.Drawing.Point(59, 6);
            this.FilePathTextBox.Name = "FilePathTextBox";
            this.FilePathTextBox.Size = new System.Drawing.Size(371, 23);
            this.FilePathTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Файл:";
            // 
            // FileOpenBtn
            // 
            this.FileOpenBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FileOpenBtn.Image = global::SmartPlus.Properties.Resources.load_table_folder;
            this.FileOpenBtn.Location = new System.Drawing.Point(435, 5);
            this.FileOpenBtn.Name = "FileOpenBtn";
            this.FileOpenBtn.Size = new System.Drawing.Size(26, 24);
            this.FileOpenBtn.TabIndex = 1;
            this.FileOpenBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.FileOpenBtn.UseVisualStyleBackColor = true;
            this.FileOpenBtn.Click += new System.EventHandler(this.FileOpenBtn_Click);
            // 
            // PinToWellCheckBox
            // 
            this.PinToWellCheckBox.AutoSize = true;
            this.PinToWellCheckBox.Location = new System.Drawing.Point(15, 88);
            this.PinToWellCheckBox.Name = "PinToWellCheckBox";
            this.PinToWellCheckBox.Size = new System.Drawing.Size(185, 19);
            this.PinToWellCheckBox.TabIndex = 4;
            this.PinToWellCheckBox.Text = "Привязать файл к скважине:";
            this.PinToWellCheckBox.UseVisualStyleBackColor = true;
            this.PinToWellCheckBox.CheckedChanged += new System.EventHandler(this.PinToWellCheckBox_CheckedChanged);
            // 
            // PinToWellTextBox
            // 
            this.PinToWellTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PinToWellTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.PinToWellTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.PinToWellTextBox.Location = new System.Drawing.Point(244, 88);
            this.PinToWellTextBox.Name = "PinToWellTextBox";
            this.PinToWellTextBox.Size = new System.Drawing.Size(217, 23);
            this.PinToWellTextBox.TabIndex = 5;
            // 
            // UploadFileBtn
            // 
            this.UploadFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.UploadFileBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.UploadFileBtn.Location = new System.Drawing.Point(235, 130);
            this.UploadFileBtn.Name = "UploadFileBtn";
            this.UploadFileBtn.Size = new System.Drawing.Size(130, 30);
            this.UploadFileBtn.TabIndex = 6;
            this.UploadFileBtn.Text = "Загрузить на сервер";
            this.UploadFileBtn.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(371, 130);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // UploadFileForm
            // 
            this.ClientSize = new System.Drawing.Size(473, 172);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.UploadFileBtn);
            this.Controls.Add(this.PinToWellTextBox);
            this.Controls.Add(this.PinToWellCheckBox);
            this.Controls.Add(this.FileOpenBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FilePathTextBox);
            this.Controls.Add(this.PinToOilfieldTextBox);
            this.Controls.Add(this.PinToOilfieldCheckBox);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UploadFileForm";
            this.Text = "Загрузка файла на сервер";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public UploadFileForm(Project project)
        {
            InitializeComponent();

            this.project = project;
            if (project != null)
            {
                var list = new AutoCompleteStringCollection();
                for (int i = 0; i < project.OilFields.Count; i++)
                {
                    list.Add(project.OilFields[i].Name);
                }
                PinToOilfieldTextBox.AutoCompleteCustomSource = list;
            }
            PinToOilfieldTextBox.Enabled = PinToWellCheckBox.Checked;
            PinToWellCheckBox.Enabled = PinToOilfieldCheckBox.Checked;
            PinToWellTextBox.Enabled = PinToOilfieldCheckBox.Checked && PinToWellCheckBox.Checked;
            if (!PinToOilfieldCheckBox.Checked) PinToWellTextBox.Text = string.Empty;
            this.FormClosing += new FormClosingEventHandler(UploadFileForm_FormClosing);

        }

        void UploadFileForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                if (!System.IO.File.Exists(FilePathTextBox.Text))
                {
                    MessageBox.Show(this, "Выберите файл для загрузки на сервер!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                    e.Cancel = true;
                    return;
                }
                int ofIndex = -1;
                if (PinToOilfieldCheckBox.Checked)
                {
                    ofIndex = project.GetOFIndex(PinToOilfieldTextBox.Text.ToUpper());
                    if (ofIndex == -1)
                    {
                        MessageBox.Show(this, "Укажите правильное имя месторождения!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                }
                if (PinToWellCheckBox.Checked && ofIndex > -1)
                {
                    int index = project.OilFields[ofIndex].GetWellIndex(PinToWellTextBox.Text.ToUpper());
                    if (index == -1)
                    {
                        MessageBox.Show(this, "Укажите правильное имя скважины!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                }
            }
        }

        public static object[] ShowForm(MainForm mainForm, string OilfieldName, string WellName)
        {
            object[] result = null;
            Project project = mainForm.GetLastProject();
            UploadFileForm form = new UploadFileForm(project);
            form.Owner = mainForm;
            form.StartPosition = FormStartPosition.CenterParent;
            
            if (OilfieldName.Length > 0 && project.GetOFIndex(OilfieldName) > -1)
            {
                int index = project.GetOFIndex(OilfieldName);
                form.PinToOilfieldCheckBox.Checked = true;
                form.PinToOilfieldTextBox.Text = OilfieldName;
                if (WellName.Length > 0 && project.OilFields[index].GetWellIndex(WellName) > -1)
                {
                    form.PinToWellCheckBox.Checked = true;
                    form.PinToWellTextBox.Text = WellName;
                }
            }
            if (form.ShowDialog() == DialogResult.OK)
            {
                result = new object[] { -1, -1, string.Empty, string.Empty, form.FilePathTextBox.Text };
                if (form.PinToOilfieldCheckBox.Checked && form.PinToOilfieldTextBox.Text.Length > 0)
                {
                    result[0] = form.project.GetOFIndex(form.PinToOilfieldTextBox.Text.ToUpper());
                    if (((int)result[0]) > -1) result[2] = form.project.OilFields[(int)result[0]].Name;
                    if (form.PinToWellCheckBox.Checked && form.PinToWellTextBox.Text.Length > 0 && ((int)result[0]) > -1)
                    {
                        result[1] = form.project.OilFields[(int)result[0]].GetWellIndex(form.PinToWellTextBox.Text.ToUpper());
                        if (((int)result[1]) > -1) result[3] = form.project.OilFields[(int)result[0]].Wells[(int)result[1]].Name;
                    }
                }
            }
            return result;
        }

        private void FileOpenBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Выберите файл для загрузки...";
            dlg.Multiselect = false;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                FilePathTextBox.Text = dlg.FileName;
            }
        }

        private void PinToOilfieldCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            PinToOilfieldTextBox.Enabled = PinToOilfieldCheckBox.Checked;
            PinToWellCheckBox.Enabled = PinToOilfieldCheckBox.Checked;
            PinToWellTextBox.Enabled = PinToOilfieldCheckBox.Checked && PinToWellCheckBox.Checked;
            if (!PinToOilfieldCheckBox.Checked) PinToWellTextBox.Text = string.Empty;
        }

        private void PinToWellCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            PinToWellTextBox.Enabled = PinToWellCheckBox.Checked;
            PinToWellTextBox.AutoCompleteCustomSource = null;
            if (project != null && PinToOilfieldTextBox.Text.Length > 0)
            {
                int ind = project.GetOFIndex(PinToOilfieldTextBox.Text);
                if (ind > -1)
                {
                    var list = new AutoCompleteStringCollection();
                    for (int i = 0; i < project.OilFields[ind].Wells.Count; i++)
                    {
                        list.Add(project.OilFields[ind].Wells[i].Name);
                    }
                    PinToWellTextBox.AutoCompleteCustomSource = list;
                }
            }
        }
    }
}
