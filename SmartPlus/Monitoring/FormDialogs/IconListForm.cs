﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;

namespace SmartPlus
{
    public partial class IconListForm : Form
    {
        public ArrayList iconList;
        public Font[] FontList;
        public FontFamily[] FontFamilyList;
        MainForm mainForm;
        int selInd, dx;
        int maxSize;
        int startRow, rows, width;
        Font fEmpty;
        string str;
        SizeF s = SizeF.Empty;
        StringFormat strFrm;
        IconSelectForm fIconSelect;
        System.Drawing.Text.PrivateFontCollection pfc;

        public IconListForm(ArrayList InitIconList, bool IsProperty)
        {
            InitializeComponent();
            InitializeFonts();

            fIconSelect = new IconSelectForm(this);

            str = "Значок\nне задан";
            strFrm = new StringFormat();
            strFrm.Alignment = StringAlignment.Center;
            strFrm.LineAlignment = StringAlignment.Center;
            maxSize = -1;
            iconList = new ArrayList();
            int count = (IsProperty) ? 1 : 0;

            if ((InitIconList != null) && (InitIconList.Count > count))
            {
                for (int i = 0; i < InitIconList.Count - count; i++)
                {
                    iconList.Add((WellIconCode)InitIconList[i]);
                }
            }
            else if ((InitIconList != null) && ((InitIconList.Count == count) && (IsProperty)))
            {
                str = "В списке заданы\nразные типы\nзначков.";
            }
            SetMaxSize();
            ToolTip tp = new ToolTip();
            tp.SetToolTip(bAdd, "Добавить значок...");
            tp.SetToolTip(bEdit, "Редактировать выбранный значок");
            tp.SetToolTip(bUp, "Переместить вверх");
            tp.SetToolTip(bDown, "Переместить вниз");
            tp.SetToolTip(bDelete, "Удалить выбранный значок");
            startRow = 0;
            dx = 50;
            selInd = -1;
            
            pbIconMap.Width = pIconMap.Width - vScroll.Width;
            pbIconMap.Height = pIconMap.Height;
            rows = pbIconMap.Height / dx;
            SetScroll();

            pbIconMap.MouseDown += new MouseEventHandler(pbIconMap_MouseDown);
            pbIconMap.DoubleClick += new EventHandler(pbIconMap_DoubleClick);
            pbIconMap.Paint += new PaintEventHandler(pbIconMap_Paint);

            pbAllIcon.Paint += new PaintEventHandler(pbAllIcon_Paint);
        }

        void InitializeFonts()
        {
            fEmpty = new Font("Segoe UI", 10);

            FontList = new Font[7];
            FontFamilyList = new FontFamily[7];

            for (int i = 0; i < 7; i++)
            {
                switch (i)
                {
                    case 0:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.SmartFontFamily;
                        break;
                    case 1:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont1Family;
                        break;
                    case 2:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont2Family;
                        break;
                    case 3:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont3Family;
                        break;
                    case 4:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont4Family;
                        break;
                    case 5:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont5Family;
                        break;
                    case 6:
                        FontFamilyList[i] = SmartPlusGraphics.Fonts.GIDfont6Family;
                        break;
                }
                FontList[i] = new Font(FontFamilyList[i], 40);
            }
        }

        void pbIconMap_MouseDown(object sender, MouseEventArgs e)
        {
            int row;
            row = (int)(e.Y / dx);
            if (startRow + row < iconList.Count)
            {
                selInd = startRow + row;
                pbIconMap.Invalidate();
            }
        }
        void pbIconMap_DoubleClick(object sender, EventArgs e)
        {
            EditIcon();
        }

        void SetScroll()
        {
            if (iconList.Count * dx > pbIconMap.Height)
            {
                vScroll.Visible = true;
                vScroll.Maximum = iconList.Count - (1 + (int)(pbIconMap.Height / dx));
                vScroll.LargeChange = 1;
                vScroll.SmallChange = 1;
                width = pbIconMap.Width - vScroll.Width;
            }
            else
            {
                vScroll.Visible = false;
                width = pbIconMap.Width;
            }
        }
        private void vScroll_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {
                startRow = e.NewValue;
                pbIconMap.Invalidate();
            }
        }

        void DrawIconList(Graphics grfx)
        {
            grfx.Clear(Color.White);
            int i, j;
            float x, y;
            SizeF iconSize;
            string s;
            WellIconCode icon;
            SolidBrush br;
            Font f;
            for (i = startRow, j = 0; (i < iconList.Count) && (j < rows); i++, j++)
            {
                icon = (WellIconCode)iconList[i];
                
                s = Convert.ToChar(icon.CharCode).ToString();
                f = FontList[icon.FontCode];
                iconSize = grfx.MeasureString(s, f, PointF.Empty, StringFormat.GenericTypographic);
                x = width/ 2.0f - iconSize.Width / 2.0f;
                y = 1 + (j + 0.5f) * dx - iconSize.Height / 2.0f;
                br = new SolidBrush(Color.FromArgb(icon.IconColor));
                if(i != selInd)
                {
                    grfx.DrawString(s, f, br, x, y, StringFormat.GenericTypographic);
                }
                else
                {
                    grfx.FillRectangle(Brushes.SkyBlue, 1, j + 1 + j * dx, width - 2, 1 + dx);
                    grfx.DrawString(s, f, Brushes.White, x, y, StringFormat.GenericTypographic);
                }
                grfx.DrawRectangle(Pens.Black, 1, j + 1 + j * dx, width - 2, 1 + dx);
            }
        }
        void pbIconMap_Paint(object sender, PaintEventArgs e)
        {
            DrawIconList(e.Graphics);
        }

        void DrawAllIcon(Graphics grfx)
        {
            if (iconList.Count == 0)
            {
                grfx.Clear(Color.White);
                if (s.Width == 0) s = grfx.MeasureString(str, fEmpty, PointF.Empty, strFrm);
                grfx.DrawString(str, fEmpty, Brushes.Black, 75, 75, strFrm);
            }
            else
            {
                WellIconCode icon;
                SolidBrush br;
                if (maxSize <= 150)
                {
                    pbIconMap.Image = null;
                    SizeF iconSize;
                    string s;
                    Font f;
                    grfx.Clear(Color.White);

                    for (int i = 0; i < iconList.Count; i++)
                    {
                        icon = (WellIconCode)iconList[i];
                        s = Convert.ToChar(icon.CharCode).ToString();
                        f = new Font(FontList[icon.FontCode].FontFamily, icon.Size * 4);
                        br = new SolidBrush(Color.FromArgb(icon.IconColor));
                        iconSize = grfx.MeasureString(s, f, PointF.Empty, StringFormat.GenericTypographic);
                        grfx.DrawString(s, f, br, 75 - iconSize.Width / 2, 75 - iconSize.Height / 2, StringFormat.GenericTypographic);
                        f.Dispose();
                        br.Dispose();
                    }
                }
                else
                {
                    int size = maxSize + maxSize / 4;
                    using (Bitmap bmp = new Bitmap(size, size))
                    {
                        Graphics gr = Graphics.FromImage(bmp);

                        float x = size / 2, y = size / 2;
                        SizeF iconSize;
                        Rectangle rect = Rectangle.Empty;
                        rect.Width = size;
                        rect.Height = size;
                        string s;
                        Font f;
                        gr.Clear(Color.White);
                        for (int i = 0; i < iconList.Count; i++)
                        {
                            icon = (WellIconCode)iconList[i];
                            s = Convert.ToChar(icon.CharCode).ToString();
                            f = new Font(FontList[icon.FontCode].FontFamily, icon.Size * 4);

                            br = new SolidBrush(Color.FromArgb(icon.IconColor));
                            iconSize = grfx.MeasureString(s, f, PointF.Empty, StringFormat.GenericTypographic);
                            gr.DrawString(s, f, br, x - iconSize.Width / 2, y - iconSize.Height / 2, StringFormat.GenericTypographic);
                            f.Dispose();
                            br.Dispose();
                        }

                        grfx.DrawImage(bmp, pbAllIcon.ClientRectangle, rect, GraphicsUnit.Pixel);
                    }
                }
 
            }
        }
        void pbAllIcon_Paint(object sender, PaintEventArgs e)
        {
            DrawAllIcon(e.Graphics);
        }

        // Кнопки
        void SetMaxSize()
        {
            maxSize = -1;
            WellIconCode icon;
            for (int i = 0; i < iconList.Count; i++)
            {
                icon = (WellIconCode)iconList[i];
                if (maxSize < icon.Size * 4) maxSize = icon.Size * 4;
            }
        }
        private void bAdd_Click(object sender, EventArgs e)
        {
            fIconSelect.SelColor = Color.FromArgb(35, 35, 35).ToArgb();
            if (fIconSelect.ShowDialog() == DialogResult.OK)
            {
                WellIconCode icon = new WellIconCode();
                icon.CharCode = (ushort)fIconSelect.SelInd;
                icon.FontCode = (ushort)fIconSelect.SelFont;
                icon.IconColor = fIconSelect.SelColor;
                icon.Size = fIconSelect.SelSize;
                selInd = iconList.Count;
                iconList.Add(icon);
                SetMaxSize();
                pbIconMap.Invalidate();
                pbAllIcon.Invalidate();
            }
        }
        private void bDelete_Click(object sender, EventArgs e)
        {
            if (selInd > -1)
            {
                iconList.RemoveAt(selInd);
                selInd--;
                SetMaxSize();
                pbIconMap.Invalidate();
                pbAllIcon.Invalidate();
            }
        }
        void EditIcon()
        {
            if (selInd > -1)
            {
                WellIconCode icon = (WellIconCode)iconList[selInd];
                fIconSelect.SelFont = icon.FontCode;
                fIconSelect.SelInd = icon.CharCode;
                fIconSelect.SelSize = icon.Size;
                fIconSelect.SelColor = icon.IconColor;
                if (fIconSelect.ShowDialog() == DialogResult.OK)
                {
                    WellIconCode newIcon = new WellIconCode();
                    newIcon.CharCode = (ushort)fIconSelect.SelInd;
                    newIcon.FontCode = (ushort)fIconSelect.SelFont;
                    newIcon.IconColor = fIconSelect.SelColor;
                    newIcon.Size = fIconSelect.SelSize;
                    iconList.Insert(selInd, newIcon);
                    iconList.RemoveAt(selInd + 1);
                    SetMaxSize();
                    pbIconMap.Invalidate();
                    pbAllIcon.Invalidate();
                }
            }
            else
            {
                MessageBox.Show("Выберите значок для редактирования", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void bEdit_Click(object sender, EventArgs e)
        {
            EditIcon();
        }
        private void bUp_Click(object sender, EventArgs e)
        {
            if (selInd > 0)
            {
                WellIconCode icon = (WellIconCode)iconList[selInd];
                iconList.Insert(selInd - 1, icon);
                iconList.RemoveAt(selInd + 1);
                selInd--;
                pbIconMap.Invalidate();
                pbAllIcon.Invalidate();
            }
        }
        private void bDown_Click(object sender, EventArgs e)
        {
            if (selInd < iconList.Count - 1)
            {
                WellIconCode icon = (WellIconCode)iconList[selInd];
                iconList.Insert(selInd + 2, icon);
                iconList.RemoveAt(selInd);
                selInd++;
                pbIconMap.Invalidate();
                pbAllIcon.Invalidate();
            }

        }
    }
}
