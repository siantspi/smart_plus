﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace SmartPlus
{
    public partial class PaletteEditorForm : Form
    {
        public Palette palette = null;
        int MouseDownRow, MouseDownColumn;
        ContextMenuStrip ContextMenu;
        ToolStripMenuItem cmInsertRow, cmRemoveRow, cmCopy, cmPaste;
        ToolStripSeparator cmSep;
        ToolTip ErrorToolTip;
        double Minimum, Maximum;
        public PaletteEditorForm(Palette InitPalette, double Min, double Max)
        {
            InitializeComponent();
            InitContextMenu();
            dgvPoints.VirtualMode = true;
            this.FormClosing += new FormClosingEventHandler(PaletteEditor_FormClosing);
            this.SetPalette(InitPalette);
            Minimum = Min;
            Maximum = Max;

            dgvPoints.MouseDown += new MouseEventHandler(dgvPoints_MouseDown);
            dgvPoints.CellValueNeeded += new DataGridViewCellValueEventHandler(dgvPoints_CellValueNeeded);
            dgvPoints.CellValuePushed += new DataGridViewCellValueEventHandler(dgvPoints_CellValuePushed);
            dgvPoints.CellBeginEdit += new DataGridViewCellCancelEventHandler(dgvPoints_CellBeginEdit);
            dgvPoints.CellValidating += new DataGridViewCellValidatingEventHandler(dgvPoints_CellValidating);
            dgvPoints.CellEndEdit += new DataGridViewCellEventHandler(dgvPoints_CellEndEdit);
            dgvPoints.CellDoubleClick += new DataGridViewCellEventHandler(dgvPoints_CellDoubleClick);
            dgvPoints.CellPainting += new DataGridViewCellPaintingEventHandler(dgvPoints_CellPainting);
            dgvPoints.RowsAdded +=new DataGridViewRowsAddedEventHandler(dgvPoints_RowsAdded);
            dgvPoints.RowsRemoved += new DataGridViewRowsRemovedEventHandler(dgvPoints_RowsRemoved);

            ExamplePanel.Paint += new PaintEventHandler(ExamplePanel_Paint);
            CancelButton.MouseDown += new MouseEventHandler(CancelButton_MouseDown);
        }
        public void SetPalette(Palette pal)
        {
            this.palette = new Palette(pal);
            dgvPoints.RowCount = this.palette.Count;
        }

        void ExamplePanel_Paint(object sender, PaintEventArgs e)
        {
            if (dgvPoints.Rows.Count > 1)
            {
                RectangleF rect = ExamplePanel.ClientRectangle;
                e.Graphics.Clear(Color.White);
                float y1, y2;
                double min = 0, max = 1;
                try
                {
                    min = palette[0].X;
                    max = palette[palette.Count - 1].X;
                }
                catch
                {
                    return;
                }
                y2 = -1;
                for (int i = 0; i < palette.Count - 1; i++)
                {
                    if (y2 == -1) y1 = (float)((palette[i].X - min) * ExamplePanel.Height / (max - min)); else y1 = y2;
                    y2 = (float)((palette[i + 1].X - min) * ExamplePanel.Height / (max - min));
                    rect.Y = y1;
                    rect.Height = y2 - y1;
                    if (rect.Height > 0)
                    {
                        using (LinearGradientBrush brush = new LinearGradientBrush(new PointF(0, y1 - 0.5f), new PointF(0, y2 + 0.5f), palette[i].Y, palette[i + 1].Y))
                        {
                            e.Graphics.FillRectangle(brush, rect);
                        }
                    }
                }
            }
        }

        // BUTTON
        private void InvertButton_Click(object sender, EventArgs e)
        {
            if (palette != null)
            {
                palette.Invert();
                dgvPoints.Invalidate();
                ExamplePanel.Invalidate();
            }
        }
        void CancelButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (dgvPoints.IsCurrentCellInEditMode)
            {
                dgvPoints.CancelEdit();
            }
            if (ErrorToolTip != null)
            {
                ErrorToolTip.Dispose();
                ErrorToolTip = null;
            }
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        // TOOLTIP
        void ShowToolTip(int x, int y, string Text)
        {
            if (ErrorToolTip != null) ErrorToolTip.Dispose();
            ErrorToolTip = new ToolTip();
            ErrorToolTip.ToolTipTitle = "Ошибка";
            ErrorToolTip.ToolTipIcon = ToolTipIcon.Error;
            ErrorToolTip.UseFading = true;
            ErrorToolTip.UseAnimation = true;
            ErrorToolTip.IsBalloon = true;
            ErrorToolTip.Show(Text, this, x, y - 74, 3000);
        }

        // CONTEXT MENU
        void InitContextMenu()
        {
            ContextMenu = new ContextMenuStrip();
            cmInsertRow = (ToolStripMenuItem)ContextMenu.Items.Add("Вставить новую строку", Properties.Resources.Add_Green16, cmInsertRow_Click);
            cmRemoveRow = (ToolStripMenuItem)ContextMenu.Items.Add("Удалить строку", Properties.Resources.Delete16, cmRemoveRow_Click);
            cmSep = (ToolStripSeparator)ContextMenu.Items.Add("-");
            cmCopy = (ToolStripMenuItem)ContextMenu.Items.Add("Копировать палитру", Properties.Resources.Copy16, cmCopy_Click);
            cmPaste = (ToolStripMenuItem)ContextMenu.Items.Add("Вставить палитру", Properties.Resources.Paste16, cmPaste_Click);

            dgvPoints.ContextMenuStrip = ContextMenu;
        }
        void SetContextMenu()
        {
            if ((dgvPoints.Rows.Count > 2) && (MouseDownColumn > -1) && (MouseDownRow > -1))
            {
                cmRemoveRow.Enabled = true;
            }
            else
            {
                cmRemoveRow.Enabled = false;
            }
            cmCopy.Enabled = dgvPoints.Rows.Count > 0;
            string s = Clipboard.GetText();
            cmPaste.Enabled = s.Length > 0 && s.StartsWith("Point\tRed\tGreen\tBlue");
        }
        void cmInsertRow_Click(object sender, EventArgs e)
        {
            double value = 0, val1, val2;
            Color clr = Color.White, clr1, clr2 = Color.White;
            if ((MouseDownRow == -1) && (MouseDownColumn == -1))
            {
                if (palette.Count > 1) clr = palette[palette.Count - 1].Y;
                palette.Add(Maximum, clr);
            }
            else if ((MouseDownRow == 0) && (MouseDownColumn != -1))
            {
                if (palette.Count > 1) clr = palette[0].Y;
                palette.Insert(0, Minimum, clr);
            }
            else if ((MouseDownRow != -1) && (MouseDownColumn != -1))
            {
                if (MouseDownRow - 1 >= 0)
                {
                    try
                    {
                        val1 = palette[MouseDownRow - 1].X;
                        val2 = palette[MouseDownRow].X;
                        clr1 = palette[MouseDownRow - 1].Y;
                        clr2 = palette[MouseDownRow].Y;

                        value = (val2 + val1) / 2;
                        clr = Color.FromArgb((clr1.R + clr2.R) / 2, (clr1.G + clr2.G) / 2, (clr1.B + clr2.B) / 2);
                    }
                    catch
                    {
                        value = palette[MouseDownRow].X;
                        clr = palette[MouseDownRow].Y;
                    }
                }
                else
                {
                    value = palette[MouseDownRow].X;
                    clr = palette[MouseDownRow].Y;
                }
                palette.Insert(MouseDownRow, value, clr);
            }
            dgvPoints.RowCount = palette.Count;
            this.Invalidate();
            ExamplePanel.Invalidate();
        }
        void cmRemoveRow_Click(object sender, EventArgs e)
        {
            if ((dgvPoints.Rows.Count > 2) && (MouseDownColumn != -1))
            {
                palette.RemoveAt(MouseDownRow);
                dgvPoints.Rows.RemoveAt(MouseDownRow);
            }
        }
        void cmCopy_Click(object sender, EventArgs e)
        {
            if (dgvPoints.Rows.Count > 0 && this.palette != null)
            {
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";
                string result = "Point\tRed\tGreen\tBlue\r\n";
                for (int i = 0; i < palette.Count; i++)
                {
                    result += string.Format(nfi, "{0}\t{1}\t{2}\t{3}{4}", palette[i].X, palette[i].Y.R, palette[i].Y.G, palette[i].Y.B, (i < palette.Count) ? "\r\n" : string.Empty);
                }
                Clipboard.Clear();
                Clipboard.SetDataObject(result);
            }
        }
        void cmPaste_Click(object sender, EventArgs e)
        {
            string copyText = Clipboard.GetText();
            if (copyText.Length > 0)
            {
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                string[] sepLines = new string[] {"\r\n"};
                char[] sep = new char[] {'\t'};
                string[] lines = copyText.Split(sepLines, StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length < 3) return;
                string[] items;
                Palette pal = new Palette();
                double x;
                byte r, g, b;
                bool head = lines[0] == "Point\tRed\tGreen\tBlue";
                for (int i = head ? 1 : 0; i < lines.Length; i++)
                {
                    items = lines[i].Split(sep);
                    if (items.Length != 4) return;
                    if (!double.TryParse(items[0], System.Globalization.NumberStyles.Float, nfi, out x)) return;
                    if (!byte.TryParse(items[1], out r)) return;
                    if (!byte.TryParse(items[2], out g)) return;
                    if (!byte.TryParse(items[3], out b)) return;
                    pal.Add(x, Color.FromArgb(r, g, b));
                }
                SetPalette(pal);
                dgvPoints.Refresh();
                ExamplePanel.Invalidate();
            }
        }

        // GRID EVENTS
        void dgvPoints_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (e.ColumnIndex < 4))
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        e.Value = palette[e.RowIndex].X.ToString("0.##");
                        break;
                    case 1:
                        e.Value = palette[e.RowIndex].Y.R.ToString("0");
                        break;
                    case 2:
                        e.Value = palette[e.RowIndex].Y.G.ToString("0");
                        break;
                    case 3:
                        e.Value = palette[e.RowIndex].Y.B.ToString("0");
                        break;
                }
            }
        }
        void dgvPoints_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
        {
            if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (e.ColumnIndex < 4))
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        palette.Set(e.RowIndex, Convert.ToDouble(e.Value), palette[e.RowIndex].Y);
                        break;
                    case 1:
                        Color clr = palette[e.RowIndex].Y;
                        clr = Color.FromArgb(Convert.ToByte(e.Value), clr.G, clr.B);
                        palette.Set(e.RowIndex, palette[e.RowIndex].X, clr);
                        break;
                    case 2:
                        clr = palette[e.RowIndex].Y;
                        clr = Color.FromArgb(clr.R, Convert.ToByte(e.Value), clr.B);
                        palette.Set(e.RowIndex, palette[e.RowIndex].X, clr);
                        break;
                    case 3:
                        clr = palette[e.RowIndex].Y;
                        clr = Color.FromArgb(clr.R, clr.G, Convert.ToByte(e.Value));
                        palette.Set(e.RowIndex, palette[e.RowIndex].X, clr);
                        break;
                }
                this.Invalidate();
                ExamplePanel.Invalidate();
            }
        }
        void dgvPoints_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                e.Cancel = true;
            }
        }
        void dgvPoints_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            bool error = false;
            Point pt = dgvPoints.Location;
            pt.Y += 28;
            Rectangle rect = dgvPoints.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);
            if (e.ColumnIndex == 0)
            {
                if (e.FormattedValue != System.DBNull.Value)
                {
                    try
                    {
                        double val = Convert.ToDouble(e.FormattedValue);
                        if (palette.Count > 2)
                        {
                            if ((e.RowIndex - 1 >= 0) && (e.RowIndex < palette.Count - 1))
                            {
                                double val1 = palette[e.RowIndex - 1].X;
                                double val2 = palette[e.RowIndex + 1].X;
                                if ((val < val1) || (val > val2))
                                {
                                    ShowToolTip(pt.X + 5 + rect.X, pt.Y + rect.Y, string.Format("Вводимое число должно быть между {0:0.##} и {1:0.##}", val1, val2));
                                    e.Cancel = true;
                                }
                            }
                        }
                        if ((e.RowIndex == 0) && (palette.Count > 1))
                        {
                            double val2 = palette[1].X;
                            if (val > val2)
                            {
                                ShowToolTip(pt.X + 5 + rect.X, pt.Y + rect.Y, string.Format("Вводимое число должно быть меньше {0:0.##}", val2));
                                e.Cancel = true;
                            }
                        }
                        else if ((e.RowIndex == palette.Count - 1) && (palette.Count > 1))
                        {
                            double val1 = palette[palette.Count - 2].X;
                            if (val1 > val)
                            {
                                ShowToolTip(pt.X + 5 + rect.X, pt.Y + rect.Y, string.Format("Вводимое число должно быть больше {0:0.##}", val1));
                                e.Cancel = true;
                            }
                        }
                    }
                    catch
                    {
                        error = true;
                    }
                }
                else
                {
                    error = true;
                }
                if (error)
                {
                    ShowToolTip(pt.X + 5 + rect.X, pt.Y + rect.Y, "Введено неправильное число!");
                    e.Cancel = true;
                }
            }
            else if ((e.ColumnIndex == 1) || (e.ColumnIndex == 2) || (e.ColumnIndex == 3))
            {
                if (e.FormattedValue != System.DBNull.Value)
                {
                    try
                    {
                        int val = Convert.ToInt32(e.FormattedValue);
                        if ((val < 0) || (val > 255))
                        {
                            error = true;
                        }
                    }
                    catch
                    {
                        error = true;
                    }
                }
                else
                {
                    error = true;
                }
                if (error)
                {
                    ShowToolTip(pt.X + 5 + rect.X, pt.Y + rect.Y, "Введите целое число от 0 до 255!");
                    e.Cancel = true;
                }
            }
        }
        void dgvPoints_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dgvPoints.Invalidate();
            if (ErrorToolTip != null)
            {
                ErrorToolTip.Dispose();
                ErrorToolTip = null;
            }
        }
        void dgvPoints_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((palette != null) && (e.RowIndex > -1) && (e.ColumnIndex == 4) && (e.RowIndex < palette.Count))
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = palette[e.RowIndex].Y;
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        palette.Set(e.RowIndex, palette[e.RowIndex].X, dlg.Color);
                        this.Invalidate();
                        ExamplePanel.Invalidate();
                    }
                }
            }
        }
        void dgvPoints_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if ((palette != null) && (e.RowIndex > -1) && (e.ColumnIndex == 4) && (e.RowIndex < palette.Count))
            {
                Graphics grfx = e.Graphics;
                e.PaintBackground(e.CellBounds, true);
                Rectangle rect = e.CellBounds;
                rect.Inflate(-2, -2);
                rect.Offset(-1, -1);
                using (SolidBrush brush = new SolidBrush(palette[e.RowIndex].Y))
                {
                    grfx.FillRectangle(brush, rect);
                    grfx.DrawRectangle(Pens.Black, rect);
                }
                e.Handled = true;
            }
        }
        void dgvPoints_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            int rows = dgvPoints.DisplayedRowCount(false);
            if ((rows != 0) && (rows < dgvPoints.Rows.Count))
            {
                dgvPoints.Columns[4].Width = 53;
            }
            else
            {
                dgvPoints.Columns[4].Width = 70;
            }
            this.Invalidate();
            ExamplePanel.Invalidate();
        }
        void dgvPoints_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dgvPoints.DisplayedRowCount(false) < dgvPoints.Rows.Count)
            {
                dgvPoints.Columns[4].Width = 53;
            }
            else
            {
                dgvPoints.Columns[4].Width = 70;
            }
            this.Invalidate();
            ExamplePanel.Invalidate();
        }
        void dgvPoints_MouseDown(object sender, MouseEventArgs e)
        {
            if (dgvPoints.SelectedCells.Count > 0)
            {
                for (int i = 0; i < dgvPoints.Rows.Count; i++)
                {
                    for (int j = 0; j < dgvPoints.Columns.Count;j++)
                    {
                        dgvPoints.Rows[i].Cells[j].Selected = false;
                    }
                }
            }
            if (ErrorToolTip != null)
            {
                ErrorToolTip.Dispose();
                ErrorToolTip = null;
            }
            System.Windows.Forms.DataGridView.HitTestInfo hit = dgvPoints.HitTest(e.X, e.Y);
            MouseDownRow = hit.RowIndex;
            MouseDownColumn = hit.ColumnIndex;
            if ((MouseDownRow > -1) && (e.Button == MouseButtons.Right))
            {
                dgvPoints.Rows[MouseDownRow].Selected = true;
            }
            SetContextMenu();
        }

        // EVENTS
        void PaletteEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ErrorToolTip != null)
            {
                ErrorToolTip.Dispose();
                ErrorToolTip = null;
            }
            if (this.DialogResult == DialogResult.Cancel)
            {
                this.palette = null;
            }
        }
    }
}
