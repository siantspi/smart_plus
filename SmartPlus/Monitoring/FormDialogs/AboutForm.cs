﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class AboutForm : Form
    {
        public AboutForm(MainForm mainForm)
        {
            InitializeComponent();
            this.lVersion.Text = String.Format("Версия: {0}", mainForm.AssemblyVersion);
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void Link_Clicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel link = (LinkLabel)sender;
            System.Diagnostics.Process.Start("mailto:" + link.Text);
        }
    }
}
