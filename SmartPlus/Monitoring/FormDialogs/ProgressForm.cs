﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace SmartPlus
{
    public partial class ProgressForm : Form
    {
        Timer tmr;
        public bool bDispose;
        public bool bWorking;
        public bool UserClosing;
        int tmrWorkType;
        public long startTime;
        MainForm mainForm;
        public bool hided;
        bool visChange = false;
        public ProgressForm(MainForm mainForm)
        {
            InitializeComponent();
            this.Owner = mainForm;
            this.mainForm = mainForm;
            tmrWorkType = -1;
            bWorking = false;
            bDispose = false;
            UserClosing = false;
            tmr = new Timer();
            tmr.Enabled = false;
            tmr.Interval = 30;

            // масштабирование
            if (lMain.Height > 23) this.Width = (int)(this.Width * 1.2);
            lElementTitle.Top = lMain.Bottom + 9;
            lTimeTitle.Top = lElementTitle.Bottom;
            lElementTitle.Top = lMain.Bottom + 9;
            lTime.Top = lElementTitle.Bottom;
            this.Height = 40 + lMain.Height + panel1.Height;

            tmr.Tick += new EventHandler(tmr_Tick);
            this.Shown += new EventHandler(formProgress_Shown);
        }

        void formProgress_Shown(object sender, EventArgs e)
        {
            if (hided) this.Hide();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (bDispose) 
                base.OnClosing(e);
            else
            {
                e.Cancel = true;
                UserClosing = true;
                base.Hide();
            }
        }
        
        void tmr_Tick(object sender, EventArgs e)
        {
            switch (tmrWorkType)
            {
                case 0:
                    if (this.Height <= 30 + lTimeTitle.Bottom + panel1.Height) this.Height += 5;
                    break;
                case 1:
                    if (this.Height >= 40 + lMain.Height + panel1.Height) this.Height -= 5;
                    break;
            }
        }
        private void bDown_Click(object sender, EventArgs e)
        {
            tmrWorkType = 0;
            bDown.Visible = false;
            bUp.Visible = true;
            tmr.Enabled = true;
        }
        private void bUp_Click(object sender, EventArgs e)
        {
            tmrWorkType = 1;
            bUp.Visible = false;
            bDown.Visible = true;
            tmr.Enabled = true;
        }
        private void bDown_MouseMove(object sender, MouseEventArgs e)
        {
            bDown.Image = SmartPlus.Properties.Resources.down_over;
        }
        private void bDown_MouseLeave(object sender, EventArgs e)
        {
            bDown.Image = SmartPlus.Properties.Resources.down;
        }
        private void bUp_MouseMove(object sender, MouseEventArgs e)
        {
            bUp.Image = SmartPlus.Properties.Resources.up_over;
        }
        private void bUp_MouseLeave(object sender, EventArgs e)
        {
            bUp.Image = SmartPlus.Properties.Resources.up;
        }
        
        public void Start(string title)
        {
            if (!bWorking)
            {
                Text = title;
                lMain.Text = title;
                lElement.Text = "Текущий элемент";
                lTime.Text = "...";
                startTime = DateTime.Now.Ticks;
                bWorking = true;
                hided = false;
            }
        }
        public void End()
        {
            bWorking = false;
            pb.Value = 0;
            lElement.Text = "";
            lTime.Text = "...";
            this.Height = 135;
            hided = true;
            this.Hide();
        }

        private void formProgress_Activated(object sender, EventArgs e)
        {
            Point p = new Point((int)(mainForm.Location.X + mainForm.Width / 2 - this.Width / 2), (int)(mainForm.Location.Y + mainForm.Height / 2 - this.Height / 2));
            this.Location = p;
        }
    }
}
