﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace SmartPlus
{
    public partial class UpdateProjForm : Form
    {
        public static bool FormVisible = false;
        MainForm mainForm;
        ImageList StateImgList;
        ImageList images;
        public UpdateProjForm(MainForm MainForm)
        {
            InitializeComponent();

            this.mainForm = MainForm;
            this.Owner = MainForm;
            this.StartPosition = FormStartPosition.CenterParent;
            this.StateImgList = new ImageList();
            StateImgList.Images.Add(SmartPlus.Properties.Resources.unchecked16);
            StateImgList.Images.Add(SmartPlus.Properties.Resources.checked16);
            twUpdate.StateImageList = this.StateImgList;

            images = new ImageList();
            images.Images.Add(Properties.Resources.def);
            images.Images.Add(Properties.Resources.apply);
            images.Images.Add(Properties.Resources.apply_gray);
            twUpdate.ImageList = images;
            twUpdate.ShowNodeToolTips = true;
        }

        public static void ShowForm(MainForm MainForm)
        {
            if (!FormVisible && MainForm != null && MainForm.dataUpdater != null && MainForm.dataUpdater.UpdatesAvailable)
            {
                UpdateProjForm form = new UpdateProjForm(MainForm);
                form.SetProjectUpdater(MainForm.dataUpdater);
                if (MainForm.dataUpdater.UpdatesAvailable && MainForm.dataUpdater.UpdatesPreLoadAvailable)
                {
                    form.bUpdate.Enabled = false;
                    form.bExit.Enabled = false;
                    MainForm.dataUpdater.StartStraightUpdate();
                }
                FormVisible = true;
                form.ShowDialog();
                FormVisible = false;
            }
            else if (!FormVisible)
            {
                MainForm.tsbUpdates.Visible = false;
            }
        }
        public void SetProjectUpdater(DataUpdater dataUpdater)
        {
            UpdateOilFieldItem of;
            TreeNode tnOF, tn;
            DateTime lastDate;
            string cacheTime;
            bUpdate.Enabled = false;
            switch (dataUpdater.CurrentWork)
            {
                case DataUpdaterWork.CheckUpdates:
                    bUpdate.Enabled = true;
                    MainTitle.Text = "Фоновая загрузка обновлений невозможна по причине отсутвия свободного места на диске.\nДля установки обновлений нажмите 'Обновить немедленно'";
                    break;
                case DataUpdaterWork.CopyFromServerToPreLoad:
                    if (dataUpdater.UpdatesPreLoadAvailable)
                    {
                        MainTitle.Text = "Выполняется установка обновлений данных проекта.\nПожалуйста подождите...";
                    }
                    else
                    {
                        MainTitle.Text = "Выполняется фоновая загрузка обновлений проекта...\nМожно продолжить работу закрыв данное окно.\nДля немедленной установки нажмите 'Обновить немедленно'.";
                    }
                    bUpdate.Enabled = true;
                    break;
                case DataUpdaterWork.CopyFromServerFromHttpToPreLoad:
                    if (dataUpdater.UpdatesPreLoadAvailable)
                    {
                        MainTitle.Text = "Выполняется установка обновлений данных проекта.\nПожалуйста подождите...";
                    }
                    else
                    {
                        MainTitle.Text = "Выполняется фоновая загрузка обновлений проекта...\nМожно продолжить работу закрыв данное окно.\nДля немедленной установки нажмите 'Обновить немедленно'.";
                    }
                    bUpdate.Enabled = true;
                    break;
                case DataUpdaterWork.MoveFromPreLoadToCache:
                    MainTitle.Text = "Выполняется установка обновлений данных проекта.\nПожалуйста подождите...";
                    break;
                case DataUpdaterWork.CopyFromServerToCache:
                    MainTitle.Text = "Выполняется установка обновлений данных проекта.\nПожалуйста подождите...";
                    break;
                case DataUpdaterWork.CopyFromServerFromHttpToCache:
                    MainTitle.Text = "Выполняется загрузка обновлений данных проекта.\nПожалуйста подождите...";
                    break;
            }
            twUpdate.BeginUpdate();
            for (int i = 0; i < dataUpdater.UpdateOilFieldList.Count; i++)
            {
                of = dataUpdater.UpdateOilFieldList[i];
                tnOF = twUpdate.Nodes.Add(of.OilField);
                tnOF.Tag = of;
                lastDate = DateTime.MinValue;

                for (int j = 0; j < of.Count; j++)
                {
                    tn = tnOF.Nodes.Add(string.Format("{0} [{1:dd.MM.yyyy}]", of[j].Type, of[j].CreationTime));
                    tn.Tag = of[j];
                    cacheTime = (of[j].CacheCreationTime.Year == 1) ? "Нет данных": of[j].CacheCreationTime.ToShortDateString();
                    if (lastDate < of[j].CreationTime) lastDate = of[j].CreationTime;

                    tn.ToolTipText = string.Format("Имя файла: {0}\nДата создания в кэше: {1}\nДата создания на сервере: {2}", of[j].FileName, cacheTime, of[j].CreationTime);
                    tn.ImageIndex = 0;
                    if (of[j].IsLoaded)
                    {
                        tn.ImageIndex = 1;
                        tn.SelectedImageIndex = 1;
                    }
                    else if (of[j].IsPreLoaded)
                    {
                        tn.ImageIndex = 2;
                        tn.SelectedImageIndex = 2;
                    }
                }
                if (lastDate.Year != 1) tnOF.Text = string.Format("{0} [{1:dd.MM.yyyy}]", of.OilField, lastDate);
                tnOF.ImageIndex = 0;
                if (of.IsLoaded)
                {
                    tnOF.ImageIndex = 1;
                    tnOF.SelectedImageIndex = 1;
                }
                else if (of.IsPreLoaded)
                {
                    tnOF.ImageIndex = 2;
                    tnOF.SelectedImageIndex = 2;
                }
            }
            twUpdate.EndUpdate();
            dataUpdater.OnEndItemCopy += new OnEndItemCopyDelegate(dataUpdater_OnEndItemCopy);
            dataUpdater.OnEndDownload += new OnEndDownloadDelegate(dataUpdater_OnEndDownload);
        }

        // Data Updater Events
        void dataUpdater_OnEndItemCopy(UpdateFileItem item)
        {
            int indexOf = -1, indexFile = -1;
            for (int i = 0; i < twUpdate.Nodes.Count; i++)
            {
                for (int j = 0; j < twUpdate.Nodes[i].Nodes.Count; j++)
                {
                    if (twUpdate.Nodes[i].Nodes[j].Tag == item)
                    {
                        indexOf = i;
                        indexFile = j;
                        break;
                    }
                }
                if (indexOf > -1) break;
            }
            if (indexOf > -1)
            {
                if (item.IsLoaded)
                {
                    twUpdate.Nodes[indexOf].Nodes[indexFile].ImageIndex = 1;
                    twUpdate.Nodes[indexOf].Nodes[indexFile].SelectedImageIndex = 1;
                }
                else if (item.IsPreLoaded)
                {
                    twUpdate.Nodes[indexOf].Nodes[indexFile].ImageIndex = 2;
                    twUpdate.Nodes[indexOf].Nodes[indexFile].SelectedImageIndex = 2;
                }
                UpdateOilFieldItem ofItem = (UpdateOilFieldItem)twUpdate.Nodes[indexOf].Tag;
                if (ofItem.IsLoaded)
                {
                    twUpdate.Nodes[indexOf].ImageIndex = 1;
                    twUpdate.Nodes[indexOf].SelectedImageIndex = 1;
                }
                else if (ofItem.IsPreLoaded)
                {
                    twUpdate.Nodes[indexOf].ImageIndex = 2;
                    twUpdate.Nodes[indexOf].SelectedImageIndex = 2;
                }
                int progress = (int)(((indexOf + 1) * 100.0) / twUpdate.Nodes.Count);
                if (progress < pb.Minimum) progress = pb.Minimum;
                if (progress > pb.Maximum) progress = pb.Maximum;
                pb.Value = progress;
            }
        }
        void dataUpdater_OnEndDownload()
        {
            if (!mainForm.dataUpdater.UpdatesInstalled)
            {
                MainTitle.Text = "Фоновая загрузка обновлений завершена.\nДля установки обновлений нажмите 'Обновить немедленно'.";
            }
            else
            {
                this.Close();
            }
        }

        
        private void bUpdate_Click(object sender, EventArgs e)
        {
            pb.Value = 0;
            bExit.Enabled = false;
            bUpdate.Enabled = false;
            MainTitle.Text = "Выполняется установка обновлений данных проекта.\nПожалуйста подождите...";
            if (mainForm.AppSettings.UpdatebyHttps)
            {
                mainForm.dataUpdater.UpdatesPreLoadAvailable = true;
                mainForm.dataUpdater.StartStraightUpdate();
            }
        }
        private void bExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
