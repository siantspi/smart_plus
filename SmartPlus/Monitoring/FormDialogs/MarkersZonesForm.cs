﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class MarkersZonesForm : Form
    {
        MainForm mainForm;
        List<PlaneMarker> InitMarkers;
        public List<PlaneMarkerZone> Zones;

        public MarkersZonesForm(MainForm MainForm, List<PlaneMarkerZone> InitZones, List<PlaneMarker> InitMarkerList)
        {
            InitializeComponent();
            Zones = new List<PlaneMarkerZone>();
            this.mainForm = MainForm;
            this.InitMarkers = InitMarkerList;
            this.StartPosition = FormStartPosition.Manual;
            Point pt = MainForm.Location;
            Size size = MainForm.ClientRectangle.Size;
            if (MainForm.corSchPane.DockedState == Infragistics.Win.UltraWinDock.DockedState.Floating)
            {
                pt = MainForm.corSchPane.DockAreaPane.FloatingLocation;
                size = MainForm.corSchPane.DockAreaPane.Size;
            }
            int x = pt.X + size.Width / 2 - this.Width / 2;
            int y = pt.Y + size.Height / 2 - this.Height / 2;
            if (x < 0) x = 0; if (y < 0) y = 0;
            this.Location = new Point(x, y);

            ToolTip tp = new ToolTip();
            tp.SetToolTip(AutoLoadButton, "Создать зоны автоматически");
            tp.SetToolTip(AddButton, "Добавить зону...");
            tp.SetToolTip(EditButton, "Редактирование выбранной зоны");
            tp.SetToolTip(DeleteButton, "Удалить выбранную зону");
            tp.SetToolTip(ClearAllButton, "Очистить все");
            tp.SetToolTip(SaveButton, "Сохранить");
            tp.SetToolTip(CancelButton, "Отмена");

            FillGridByZoneList(InitZones);
            ZonesGrid.CellDoubleClick += new DataGridViewCellEventHandler(ZonesGrid_CellDoubleClick);
            ZonesGrid.CellPainting += new DataGridViewCellPaintingEventHandler(ZonesGrid_CellPainting);
        }

        void ZonesGrid_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if ((e.RowIndex > -1) && (e.ColumnIndex == 4))
            {
                Graphics grfx = e.Graphics;
                Rectangle rect = e.CellBounds;
                rect.Inflate(-2, -2);
                rect.Offset(-1, -1);
                Color clr = Color.White;
                if(ZonesGrid.Rows[e.RowIndex].Cells[4].Value != null)
                {
                    clr = Color.FromArgb((int)ZonesGrid.Rows[e.RowIndex].Cells[4].Value);
                }

                using (SolidBrush br = new SolidBrush(clr))
                {
                    e.PaintBackground(e.CellBounds, true);
                    grfx.FillRectangle(br, rect);
                    grfx.DrawRectangle(Pens.Black, rect);
                }
                e.Handled = true;
            }
        }
        void ZonesGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            StartEditSelectedZone();
        }

        void FillGridByZoneList(List<PlaneMarkerZone> InitZones)
        {
            ZonesGrid.Rows.Clear();
            if (InitZones.Count > 0)
            {
                Zones = InitZones;
                DataGridViewRow row;
                for (int i = 0; i < InitZones.Count; i++)
                {
                    row = new DataGridViewRow();
                    row.CreateCells(ZonesGrid);
                    row.Cells[0].Value = InitZones[i].Name;
                    row.Cells[1].Value = InitZones[i].MarkerTop.srcMarker.Name;
                    row.Cells[2].Value = InitZones[i].MarkerBottom.srcMarker.Name;
                    int alfa = (int)((float)(255 - InitZones[i].Alfa) * 100f / 255f);
                    row.Cells[3].Value = alfa.ToString();
                    Color clr = Color.FromArgb(255, InitZones[i].FillBrush.Color.R, InitZones[i].FillBrush.Color.G, InitZones[i].FillBrush.Color.B);
                    row.Cells[4].Value = clr.ToArgb();
                    ZonesGrid.Rows.Add(row);
                }
            }
        }
        void AutoCreateZones(List<PlaneMarker> Markers)
        {
            bool create = true;
            if (ZonesGrid.Rows.Count > 0)
            {
                if (MessageBox.Show("Создать новые зоны маркеров?", "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                {
                    create = false;
                }
            }
            if (create && Markers.Count > 0)
            {
                Zones.Clear();
                Color clr = Color.FromArgb(255, 35, 35);
                int count = (int)Math.Ceiling(Markers.Count / 2f);
                PlaneMarkerZone zone;
                for (int i = 0; i < Markers.Count - 1; i += 2)
                {
                    zone = new PlaneMarkerZone();
                    zone.Name = string.Format("Зона {0}", Zones.Count + 1);
                    zone.MarkerTop = Markers[i];
                    zone.MarkerBottom = Markers[i + 1];
                    zone.Alfa = 126;
                    zone.FillBrush = new SolidBrush(Color.FromArgb(zone.Alfa, clr));
                    clr = Markers[i].srcMarker.GetNextColor(clr);
                    Zones.Add(zone);
                }
                if (count > 0) FillGridByZoneList(Zones);
            }
        }
        void AddZoneToTable(PlaneMarkerZone zone)
        {
            DataGridViewRow row;
            row = new DataGridViewRow();
            row.CreateCells(ZonesGrid);
            row.Cells[0].Value = zone.Name;
            row.Cells[1].Value = zone.MarkerTop.srcMarker.Name;
            row.Cells[2].Value = zone.MarkerBottom.srcMarker.Name;
            int alfa = (int)((float)(255 - zone.Alfa) * 100f / 255f);
            row.Cells[3].Value = alfa.ToString();
            Color clr = Color.FromArgb(255, zone.FillBrush.Color.R, zone.FillBrush.Color.G, zone.FillBrush.Color.B);
            row.Cells[4].Value = clr.ToArgb();
            ZonesGrid.Rows.Add(row);
        }
        void ChangeEditZone(PlaneMarkerZone zone)
        {
            if (ZonesGrid.SelectedRows.Count == 1)
            {
                int i = ZonesGrid.SelectedRows[0].Index;
                Zones[i].Name = zone.Name;
                Zones[i].MarkerTop = zone.MarkerTop;
                Zones[i].MarkerBottom = zone.MarkerBottom;
                Zones[i].Alfa = zone.Alfa;
                Zones[i].FillBrush = zone.FillBrush;
                DataGridViewRow row = ZonesGrid.SelectedRows[0];
                row.Cells[0].Value = zone.Name;
                row.Cells[1].Value = zone.MarkerTop.srcMarker.Name;
                row.Cells[2].Value = zone.MarkerBottom.srcMarker.Name;
                int alfa = (int)((float)(255 - zone.Alfa) * 100f / 255f);
                row.Cells[3].Value = alfa.ToString();
                Color clr = Color.FromArgb(255, zone.FillBrush.Color.R, zone.FillBrush.Color.G, zone.FillBrush.Color.B);
                row.Cells[4].Value = clr.ToArgb();
            }
        }
        void StartEditSelectedZone()
        {
            if ((ZonesGrid.SelectedRows.Count != 0))
            {
                MarkersZoneEditForm form = new MarkersZoneEditForm();
                if (ZonesGrid.SelectedRows[0].Index < Zones.Count)
                {
                    if (form.ShowDialog(InitMarkers, Zones[ZonesGrid.SelectedRows[0].Index]) == DialogResult.OK)
                    {
                        ChangeEditZone(form.ResultZone);
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Выберите зону в списке для редактирования.", "Внимаине", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        
        // BUTTONS
        private void AutoLoadButton_Click(object sender, EventArgs e)
        {
            AutoCreateZones(InitMarkers);
        }
        private void AddButton_Click(object sender, EventArgs e)
        {
            MarkersZoneEditForm form = new MarkersZoneEditForm();
            if (form.ShowDialog(InitMarkers, null) == DialogResult.OK)
            {
                Zones.Add(form.ResultZone);
                AddZoneToTable(form.ResultZone);
            }
        }
        private void EditButton_Click(object sender, EventArgs e)
        {
            StartEditSelectedZone();
        }
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (ZonesGrid.SelectedRows.Count == 1)
            {
                string name = (string)ZonesGrid.Rows[ZonesGrid.SelectedRows[0].Index].Cells[0].Value;
                if (MessageBox.Show(this, string.Format("Удалить зону маркеров '{0}'?", name), "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Zones.RemoveAt(ZonesGrid.SelectedRows[0].Index);
                    ZonesGrid.Rows.RemoveAt(ZonesGrid.SelectedRows[0].Index);
                }
            }
        }
        private void ClearAllButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Очистить список зон маркеров?", "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                ZonesGrid.Rows.Clear();
                Zones.Clear();
            }
        }
    }
}
