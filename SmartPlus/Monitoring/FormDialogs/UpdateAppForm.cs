﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class UpdateAppForm : Form
    {
        MainForm mainForm;
        int Mode;
        public bool ShowFailUpdate;
        public UpdateAppForm(MainForm MainForm)
        {
            InitializeComponent();
            this.mainForm = MainForm;
            this.FormClosing += new FormClosingEventHandler(UpdateAppForm_FormClosing);
        }

        void UpdateAppForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((Mode == 1) && (mainForm.UpdateState != Constant.ApplicationUpdateState.StopUpdateFiles))
            {
                if (MessageBox.Show("Вы действительно хотите отменить загрузку обновления приложения?\nВ случае отмены установки при следующем запуске потребуется подключение к сети.", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.DialogResult = DialogResult.Abort;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        public void ShowUpdateForm()
        {
            lTitle.Text = "Доступно обновление";
            lText.Text = "На сервере доступна новая версия ПК \"SmartPlus\". Загрузить ее?";
            pb.Visible = false;
            bYes.Visible = true;
            bNo.Visible = true;
            bClose.Visible = false;
            Mode = 0;
            if (!this.Visible) this.ShowDialog(mainForm);
        }
        public DialogResult ShowDownloadUpdate()
        {
            lTitle.Text = "Загрузка обновления с сервера. Пожалуйста подождите...";
            lText.Text = "Загрузка:";
            pb.Visible = true;
            bYes.Visible = false;
            bNo.Visible = false;
            bClose.Visible = false;
            Mode = 1;
            return this.ShowDialog(mainForm);
        }
        public void SetRestart()
        {
            lTitle.Text = "Обновление файлов приложения завершено.";
            lText.Text = "Для применения обновления необходимо перезапустить приложение. Перезапустить?";
            pb.Visible = false;
            bYes.Visible = true;
            bNo.Visible = true;
            bClose.Visible = false;
            Mode = 2;
            if (!this.Visible) this.ShowDialog(mainForm);
        }
        public void SetCancelUpdate(string Title, string Message)
        {
            pb.Visible = false;
            bYes.Visible = false;
            bNo.Visible = false;
            bClose.Visible = true;
            lTitle.Text = Title;
            lText.Text = Message;
            Mode = 4;
            if (!this.Visible) this.ShowDialog(mainForm);
        }

        public void SetProgress(long BytesCompleted, long BytesTotal)
        {
            if (BytesTotal > 0)
            {
                int progr = (int)Math.Round((double)BytesCompleted * 100.0f / (double)BytesTotal, 0);
                if (progr < 0) progr = 0;
                if (progr > 100) progr = 100;
                pb.Value = progr;
                if (progr < 100)
                {
                    lTitle.Text = String.Format("Загрузка обновления с сервера ({0:0}%)", ((double)BytesCompleted * 100.0f) / (double)BytesTotal);
                }
                else
                {
                    lTitle.Text = String.Format("Установка обновления. Пожалуйста подождите...");
                }
                lText.Text = String.Format("Загрузка: {0:0.##}MB of {1:0.##}MB", (double)BytesCompleted / (1024 * 1024), (double)BytesTotal / (1024 * 1024));
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bYes_Click(object sender, EventArgs e)
        {
            switch (Mode)
            { 
                case 0:
                    ShowDownloadUpdate();
                    //mainForm.BeginAppUpdate();
                    break;
                case 2:
                    mainForm.AppRestart = true;
                    mainForm.dataUpdater.CancelUpdate();
                    if (mainForm.worker.IsBusy)
                    {
                        mainForm.worker.CancelAsync();
                    }
                    else
                    {
                        // mainForm.Stat.SaveLocalBeforeUpdate();
                        // mainForm.StatUsage.SaveLocalBeforeUpdate();
                        Application.Restart();
                    }
                    break;
            }

        }

        private void bNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }

}
