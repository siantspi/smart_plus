﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace SmartPlus.Dialogs
{
    class WorkFolderObjectSelectForm : Form
    {
        private Button OKButton;
        private Button CancelButton;
        private Label Title;
        private TreeView TreeWorkFolder;
        Constant.BASE_OBJ_TYPES_ID InitType;
        private CheckBox CheckObjVisible;
        ImageList images;
        ArrayList InitList;
        List<C2DLayer> InitSelectedLayers;
        public List<C2DLayer> SelectedLayerList;

        private void InitializeComponent()
        {
            this.TreeWorkFolder = new System.Windows.Forms.TreeView();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.Label();
            this.CheckObjVisible = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // TreeWorkFolder
            // 
            this.TreeWorkFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeWorkFolder.CheckBoxes = true;
            this.TreeWorkFolder.FullRowSelect = true;
            this.TreeWorkFolder.HideSelection = false;
            this.TreeWorkFolder.Location = new System.Drawing.Point(12, 27);
            this.TreeWorkFolder.Name = "TreeWorkFolder";
            this.TreeWorkFolder.Size = new System.Drawing.Size(290, 265);
            this.TreeWorkFolder.TabIndex = 0;
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Location = new System.Drawing.Point(116, 320);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(90, 30);
            this.OKButton.TabIndex = 1;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(212, 320);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Location = new System.Drawing.Point(12, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(31, 15);
            this.Title.TabIndex = 3;
            this.Title.Text = "Title";
            // 
            // CheckObjVisible
            // 
            this.CheckObjVisible.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CheckObjVisible.AutoSize = true;
            this.CheckObjVisible.Checked = true;
            this.CheckObjVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckObjVisible.Location = new System.Drawing.Point(12, 298);
            this.CheckObjVisible.Name = "CheckObjVisible";
            this.CheckObjVisible.Size = new System.Drawing.Size(241, 19);
            this.CheckObjVisible.TabIndex = 4;
            this.CheckObjVisible.Text = "Отображать только видимые на карте";
            this.CheckObjVisible.UseVisualStyleBackColor = true;
            this.CheckObjVisible.CheckedChanged += new System.EventHandler(this.CheckObjVisible_CheckedChanged);
            // 
            // WorkFolderObjectSelectForm
            // 
            this.ClientSize = new System.Drawing.Size(314, 362);
            this.Controls.Add(this.CheckObjVisible);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.TreeWorkFolder);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(330, 400);
            this.Name = "WorkFolderObjectSelectForm";
            this.Text = "Выберите объекты рабочей папки";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        public WorkFolderObjectSelectForm(Form Owner)
        {
            InitializeComponent();

            TreeWorkFolder.Height = 265 - (Title.Height - 15);
            TreeWorkFolder.Top = Title.Bottom + 3;

            if (CheckObjVisible.Width > 241)
            {
                this.Width = CheckObjVisible.Width + 40;
            }
            this.Owner = Owner;
            this.StartPosition = FormStartPosition.CenterParent;
            images = new ImageList();
            images.Images.Add(SmartPlus.Properties.Resources.def);
            images.Images.Add(SmartPlus.Properties.Resources.Folder);
            SelectedLayerList = new List<C2DLayer>();
            TreeWorkFolder.ImageList = images;
            TreeWorkFolder.AfterCheck += new TreeViewEventHandler(TreeWorkFolder_AfterCheck);
            this.FormClosing += new FormClosingEventHandler(WorkFolderObjectSelectForm_FormClosing);
        }

        void WorkFolderObjectSelectForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                int checkedCount = 0;
                for (int i = 0; i < TreeWorkFolder.Nodes.Count; i++)
                {
                    checkedCount += FillCheckedLayers(TreeWorkFolder.Nodes[i], SelectedLayerList);
                }
                if (SelectedLayerList.Count == 0)
                {
                    string text = string.Format("Выберите {0} для выполнения операции!", GetObjectName(InitType));
                    MessageBox.Show(text, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }
        }

        void TreeWorkFolder_AfterCheck(object sender, TreeViewEventArgs e)
        {
            //if (e.Node.Tag != null)
            //{
            //    if (e.Node.Checked)
            //    {
            //        if (SelectedLayerList.IndexOf((C2DLayer)e.Node.Tag) == -1)
            //        {
            //            SelectedLayerList.Add((C2DLayer)e.Node.Tag);
            //        }
            //    }
            //    else
            //    {
            //        int index = SelectedLayerList.IndexOf((C2DLayer)e.Node.Tag);
            //        if (index != -1) SelectedLayerList.RemoveAt(index);
            //    }
            //}
            if (e.Node.Nodes.Count > 0)
            {
                for (int i = 0; i < e.Node.Nodes.Count; i++)
                {
                    e.Node.Nodes[i].Checked = e.Node.Checked;
                }
            }
        }

        string GetObjectName(Constant.BASE_OBJ_TYPES_ID ObjType)
        {
            switch (ObjType)
            {
                case Constant.BASE_OBJ_TYPES_ID.LAYER:
                    return "папки";
                case Constant.BASE_OBJ_TYPES_ID.CONTOUR:
                    return "контуры";
                case Constant.BASE_OBJ_TYPES_ID.PHANTOM_WELL:
                    return "списки скважин";
                case Constant.BASE_OBJ_TYPES_ID.MARKER:
                    return "Маркеры";
                case Constant.BASE_OBJ_TYPES_ID.PROFILE:
                    return "Профили";
                case Constant.BASE_OBJ_TYPES_ID.GRID:
                    return "Сетки";
                case Constant.BASE_OBJ_TYPES_ID.IMAGE:
                    return "Изображения";
                default:
                    return string.Empty;
            }
        }

        void UpdateTreeViewByType(bool CheckVisible)
        {
            SelectedLayerList.Clear();
            TreeWorkFolder.Nodes.Clear();
            TreeNode node;
            for (int i = 0; i < InitList.Count; i++)
            {
                node = GetNodeByLayer((C2DLayer)InitList[i], CheckVisible);
                if (node != null) TreeWorkFolder.Nodes.Add(node);
            }
        }
        TreeNode GetNodeByLayer(C2DLayer layer, bool CheckVisible)
        {
            if (!CheckVisible || layer.Visible)
            {
                TreeNode node = new TreeNode(), node2;
                node.Text = layer.Name;
                node.Checked = false;
                node.Tag = layer;
                node.ImageIndex = 0;
                node.SelectedImageIndex = 0;
                if (layer.ObjTypeID == Constant.BASE_OBJ_TYPES_ID.LAYER)
                {
                    node.ImageIndex = 1;
                    node.SelectedImageIndex = 1;
                    for (int i = 0; i < layer.ObjectsList.Count; i++)
                    {
                        node2 = GetNodeByLayer((C2DLayer)layer.ObjectsList[i], CheckVisible);
                        if (node2 != null) node.Nodes.Add(node2);
                    }
                    if (node.Nodes.Count == 0) node = null;
                }
                else if (layer.ObjTypeID == InitType)
                {
                    if (InitSelectedLayers != null && InitSelectedLayers.IndexOf(layer) != -1)
                    {
                        node = null;
                    }
                }
                else
                {
                    node = null;
                }
                return node;
            }
            return null;
        }
        int FillCheckedLayers(TreeNode tn, List<C2DLayer> List)
        {
            int checkedCount = tn.Checked ? 1 : 0;
            if (tn.Tag != null)
            {
                C2DLayer layer = (C2DLayer)tn.Tag;
                if ((layer.ObjTypeID == InitType) && tn.Checked)
                {
                    List.Add(layer);
                }
                for (int i = 0; i < tn.Nodes.Count; i++)
                {
                    checkedCount += FillCheckedLayers(tn.Nodes[i], List);
                }
            }
            return checkedCount;
        }
        
        public DialogResult Show(Constant.BASE_OBJ_TYPES_ID ObjType, ArrayList WorkList, List<C2DLayer> SelectedLayers, string TitleText)
        {
            InitType = ObjType;
            InitList = WorkList;
            InitSelectedLayers = SelectedLayers;
            //Title.Text = string.Format("Выбертие {0} из рабочей папки:", GetObjectName(ObjType));
            Title.Text = TitleText;
            UpdateTreeViewByType(CheckObjVisible.Checked);
            return ShowDialog();
        }

        private void CheckObjVisible_CheckedChanged(object sender, EventArgs e)
        {
            UpdateTreeViewByType(CheckObjVisible.Checked);
        }
    }
}
