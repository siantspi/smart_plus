﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SmartPlus.DictionaryObjects;
using SmartPlus.Dialogs;

namespace SmartPlus
{
    class PVTParamsEditForm : Form
    {
        MainForm MainForm;
        private DataGridView Table;
        private Button SaveButton;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private Button CancelButton;
        private ContextMenuStrip contextMenu;

        public PVTParamsItem PvtItem;

        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Table = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaveButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Table)).BeginInit();
            this.SuspendLayout();
            // 
            // Table
            // 
            this.Table.AllowUserToAddRows = false;
            this.Table.AllowUserToDeleteRows = false;
            this.Table.AllowUserToResizeRows = false;
            this.Table.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Table.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.Table.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Table.Location = new System.Drawing.Point(12, 12);
            this.Table.Name = "Table";
            this.Table.RowHeadersVisible = false;
            this.Table.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Table.Size = new System.Drawing.Size(291, 468);
            this.Table.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.FillWeight = 131.9797F;
            this.Column1.HeaderText = "Параметр";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.FillWeight = 68.02031F;
            this.Column2.HeaderText = "Значение";
            this.Column2.Name = "Column2";
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SaveButton.Location = new System.Drawing.Point(12, 492);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(90, 30);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Сохранить";
            this.SaveButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(213, 492);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 30);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // PVTParamsEditForm
            // 
            this.AcceptButton = this.SaveButton;
            this.ClientSize = new System.Drawing.Size(315, 534);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.Table);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PVTParamsEditForm";
            this.ShowIcon = false;
            ((System.ComponentModel.ISupportInitialize)(this.Table)).EndInit();
            this.ResumeLayout(false);
        }

        public PVTParamsEditForm(MainForm MainForm)
        {
            InitializeComponent();
            this.MainForm = MainForm;
            this.Owner = MainForm;
            this.StartPosition = FormStartPosition.CenterParent;

            Table.CellDoubleClick += new DataGridViewCellEventHandler(Table_CellDoubleClick);
            Table.CellValueChanged += new DataGridViewCellEventHandler(Table_CellValueChanged);
            Table.CellParsing += new DataGridViewCellParsingEventHandler(Table_CellParsing);
            Table.CellValidating += new DataGridViewCellValidatingEventHandler(Table_CellValidating);
        }

        void contextMenuLoadPVT_Click(object sender, EventArgs e)
        {
            WorkFolderObjectSelectForm form = new WorkFolderObjectSelectForm(MainForm);
            if (form.Show(Constant.BASE_OBJ_TYPES_ID.PVT, MainForm.canv.LayerWorkList, null, "Выберите PVT параметры:") == DialogResult.OK)
            {
                if (form.SelectedLayerList.Count > 0)
                {
                    int area = this.PvtItem.OilFieldAreaCode;
                    int stratum = this.PvtItem.StratumCode;
                    this.PvtItem = ((PVTWorkObject)(form.SelectedLayerList[0].ObjectsList[0])).Item;
                    this.PvtItem.StratumCode = stratum;
                    this.PvtItem.OilFieldAreaCode = area;
                    EditParam("Объемн.коэфф-т нефти, д.ед.", PvtItem.OilVolumeFactor);
                    EditParam("Плотность нефти, г/см3", PvtItem.OilDensity);
                    EditParam("Вязкость нефти, сПз", PvtItem.OilViscosity);
                    EditParam("Нач.нефтенасыщенность, д.ед.", PvtItem.OilInitialSaturation);
                    EditParam("Объемн.коэфф-т воды", PvtItem.WaterVolumeFactor);
                    EditParam("Плотность воды, г/см3", PvtItem.WaterDensity);
                    EditParam("Вязкость воды, сПз", PvtItem.WaterViscosity);
                    EditParam("КИН, д.ед.", PvtItem.KIN);
                    EditParam("Коэффициент вытеснения, д.ед.", PvtItem.DisplacementEfficiency);
                    EditParam("Пористость", PvtItem.Porosity);
                    EditParam("Нач.пластовое давление, атм", PvtItem.InitialPressure);
                    EditParam("Давление насыщения, атм", PvtItem.SaturationPressure);
                    EditParam("Газовый фактор", PvtItem.GasFactor);
                    EditParam("Проницаемость, мД", PvtItem.Permeability);
                    EditParam("Нач.пластовое давление газа, атм", PvtItem.GasInitialPressure);
                    EditParam("Пористость по газу", PvtItem.GasPorosity);
                    EditParam("Плотность свободного газа", PvtItem.GasDensityFreeGas);
                    EditParam("Плотность растворенного газа", PvtItem.GasDensitySoluteGas);
                    EditParam("Нач.газонасыщенность", PvtItem.GasInitialSaturation);
                }
            } 
        }
        void Table_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.RowIndex > 0) && (e.RowIndex < 20) && (e.ColumnIndex > 0))
            {
                try
                {
                    double res = Convert.ToDouble(e.FormattedValue);
                }
                catch
                {
                    e.Cancel = true;
                }
            }
        }

        void Table_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (e.Value != null)
            {
                DataGridViewCell cell = Table.Rows[e.RowIndex].Cells[e.ColumnIndex];
                string val = ((string)e.Value).Replace(',', '.');
                e.Value = val;
                e.ParsingApplied = true;
            }
        }

        void Table_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex > 0) && (e.ColumnIndex > 0) && !Table.IsCurrentCellInEditMode)
            {
                Table.CurrentCell = Table.Rows[e.RowIndex].Cells[e.ColumnIndex];
                Table.BeginEdit(true);
            }
        }

        void Table_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex > -1) && (e.ColumnIndex > 0))
            {
                switch (e.RowIndex)
                {
                    case 1:
                        PvtItem.OilVolumeFactor = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 2:
                        PvtItem.OilDensity = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 3:
                        PvtItem.OilViscosity = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 4:
                        PvtItem.OilInitialSaturation = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 5:
                        PvtItem.WaterVolumeFactor = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 6:
                        PvtItem.WaterDensity = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 7:
                        PvtItem.WaterViscosity = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 8:
                        PvtItem.KIN = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 9:
                        PvtItem.DisplacementEfficiency = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 10:
                        PvtItem.Porosity = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 11:
                        PvtItem.InitialPressure = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 12:
                        PvtItem.SaturationPressure = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 13:
                        PvtItem.GasFactor = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 14:
                        PvtItem.Permeability = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 15:
                        PvtItem.GasInitialPressure = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 16:
                        PvtItem.GasPorosity = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 17:
                        PvtItem.GasDensityFreeGas = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 18:
                        PvtItem.GasDensitySoluteGas = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                    case 19:
                        PvtItem.GasInitialSaturation = Convert.ToDouble(Table.Rows[e.RowIndex].Cells[1].Value);
                        break;
                }
            }
        }

        public DialogResult ShowDialog(PVTParamsItem Item, string Title)
        {
            Table.Rows.Clear();
            this.PvtItem = Item;
            this.Text = "Физические свойства пласта";
            if (Title.Length > 0) this.Text += string.Format(" [{0}]", Title);
            var dict = (StratumDictionary)MainForm.GetLastProject().DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            AddParamToTable("Пласт", dict.GetShortNameByCode(Item.StratumCode));
            AddParamToTable("Объемн.коэфф-т нефти, д.ед.", Item.OilVolumeFactor);
            AddParamToTable("Плотность нефти, г/см3", Item.OilDensity);
            AddParamToTable("Вязкость нефти, сПз", Item.OilViscosity);
            AddParamToTable("Нач.нефтенасыщенность, д.ед.", Item.OilInitialSaturation);
            AddParamToTable("Объемн.коэфф-т воды", Item.WaterVolumeFactor);
            AddParamToTable("Плотность воды, г/см3", Item.WaterDensity);
            AddParamToTable("Вязкость воды, сПз", Item.WaterViscosity);
            AddParamToTable("КИН, д.ед.", Item.KIN);
            AddParamToTable("Коэффициент вытеснения, д.ед.", Item.DisplacementEfficiency);
            AddParamToTable("Пористость", Item.Porosity);
            AddParamToTable("Нач.пластовое давление, атм", Item.InitialPressure);
            AddParamToTable("Давление насыщения, атм", Item.SaturationPressure);
            AddParamToTable("Газовый фактор", Item.GasFactor);
            AddParamToTable("Проницаемость, мД", Item.Permeability);
            AddParamToTable("Нач.пластовое давление газа, атм", Item.GasInitialPressure);
            AddParamToTable("Пористость по газу", Item.GasPorosity);
            AddParamToTable("Плотность свободного газа", Item.GasDensityFreeGas);
            AddParamToTable("Плотность растворенного газа", Item.GasDensitySoluteGas);
            AddParamToTable("Нач.газонасыщенность", Item.GasInitialSaturation);
            return ShowDialog();
        }
        void AddParamToTable(object ParamName, object ParamValue)
        {
            DataGridViewRow row = new DataGridViewRow();
            row.CreateCells(Table);
            row.Cells[0].Value = ParamName;
            row.Cells[1].Value = ParamValue;
            Table.Rows.Add(row);
        }
        void EditParam(string ParamName, object NewValue)
        {
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                if ((string)Table.Rows[i].Cells[0].Value == ParamName)
                {
                    Table.Rows[i].Cells[1].Value = NewValue;
                }
            }
        }
    }
}
