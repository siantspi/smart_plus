﻿namespace SmartPlus
{
    partial class ExportProjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bOpen = new System.Windows.Forms.Button();
            this.lDest = new System.Windows.Forms.Label();
            this.tbDest = new System.Windows.Forms.TextBox();
            this.gbDataType = new System.Windows.Forms.GroupBox();
            this.cbWellSuspend = new System.Windows.Forms.CheckBox();
            this.cbWellLeak = new System.Windows.Forms.CheckBox();
            this.cbWellResearch = new System.Windows.Forms.CheckBox();
            this.cbWellAction = new System.Windows.Forms.CheckBox();
            this.cbGTM = new System.Windows.Forms.CheckBox();
            this.cbCoreTest = new System.Windows.Forms.CheckBox();
            this.cbCore = new System.Windows.Forms.CheckBox();
            this.cbPerf = new System.Windows.Forms.CheckBox();
            this.cbLogs = new System.Windows.Forms.CheckBox();
            this.cbGIS = new System.Windows.Forms.CheckBox();
            this.cbGrids = new System.Windows.Forms.CheckBox();
            this.cbWellPads = new System.Windows.Forms.CheckBox();
            this.cbChessSum = new System.Windows.Forms.CheckBox();
            this.cbSum = new System.Windows.Forms.CheckBox();
            this.cbAreaSum = new System.Windows.Forms.CheckBox();
            this.cbTable81 = new System.Windows.Forms.CheckBox();
            this.cbPVT = new System.Windows.Forms.CheckBox();
            this.cbOilObjects = new System.Windows.Forms.CheckBox();
            this.cbArea = new System.Windows.Forms.CheckBox();
            this.cbChess = new System.Windows.Forms.CheckBox();
            this.cbMer = new System.Windows.Forms.CheckBox();
            this.cbContours = new System.Windows.Forms.CheckBox();
            this.cbCoord = new System.Windows.Forms.CheckBox();
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.OilFieldList = new System.Windows.Forms.CheckedListBox();
            this.lOilFieldList = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lCount = new System.Windows.Forms.Label();
            this.bByInspector = new System.Windows.Forms.Button();
            this.bByWorkFolder = new System.Windows.Forms.Button();
            this.cbExportLastHour = new System.Windows.Forms.CheckBox();
            this.cbBP = new System.Windows.Forms.CheckBox();
            this.gbDataType.SuspendLayout();
            this.SuspendLayout();
            // 
            // bOpen
            // 
            this.bOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bOpen.Image = global::SmartPlus.Properties.Resources.load_table_folder;
            this.bOpen.Location = new System.Drawing.Point(452, 3);
            this.bOpen.Name = "bOpen";
            this.bOpen.Size = new System.Drawing.Size(35, 31);
            this.bOpen.TabIndex = 0;
            this.bOpen.UseVisualStyleBackColor = true;
            this.bOpen.Click += new System.EventHandler(this.bOpen_Click);
            // 
            // lDest
            // 
            this.lDest.AutoSize = true;
            this.lDest.Location = new System.Drawing.Point(12, 12);
            this.lDest.Name = "lDest";
            this.lDest.Size = new System.Drawing.Size(104, 13);
            this.lDest.TabIndex = 1;
            this.lDest.Text = "Папка назначения:";
            // 
            // tbDest
            // 
            this.tbDest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDest.Location = new System.Drawing.Point(122, 9);
            this.tbDest.Name = "tbDest";
            this.tbDest.Size = new System.Drawing.Size(324, 21);
            this.tbDest.TabIndex = 2;
            // 
            // gbDataType
            // 
            this.gbDataType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDataType.Controls.Add(this.cbBP);
            this.gbDataType.Controls.Add(this.cbWellSuspend);
            this.gbDataType.Controls.Add(this.cbWellLeak);
            this.gbDataType.Controls.Add(this.cbWellResearch);
            this.gbDataType.Controls.Add(this.cbWellAction);
            this.gbDataType.Controls.Add(this.cbGTM);
            this.gbDataType.Controls.Add(this.cbCoreTest);
            this.gbDataType.Controls.Add(this.cbCore);
            this.gbDataType.Controls.Add(this.cbPerf);
            this.gbDataType.Controls.Add(this.cbLogs);
            this.gbDataType.Controls.Add(this.cbGIS);
            this.gbDataType.Controls.Add(this.cbGrids);
            this.gbDataType.Controls.Add(this.cbWellPads);
            this.gbDataType.Controls.Add(this.cbChessSum);
            this.gbDataType.Controls.Add(this.cbSum);
            this.gbDataType.Controls.Add(this.cbAreaSum);
            this.gbDataType.Controls.Add(this.cbTable81);
            this.gbDataType.Controls.Add(this.cbPVT);
            this.gbDataType.Controls.Add(this.cbOilObjects);
            this.gbDataType.Controls.Add(this.cbArea);
            this.gbDataType.Controls.Add(this.cbChess);
            this.gbDataType.Controls.Add(this.cbMer);
            this.gbDataType.Controls.Add(this.cbContours);
            this.gbDataType.Controls.Add(this.cbCoord);
            this.gbDataType.Location = new System.Drawing.Point(16, 393);
            this.gbDataType.Name = "gbDataType";
            this.gbDataType.Size = new System.Drawing.Size(471, 331);
            this.gbDataType.TabIndex = 3;
            this.gbDataType.TabStop = false;
            this.gbDataType.Text = " Экспортируемые данные ";
            // 
            // cbWellSuspend
            // 
            this.cbWellSuspend.AutoSize = true;
            this.cbWellSuspend.Location = new System.Drawing.Point(346, 296);
            this.cbWellSuspend.Name = "cbWellSuspend";
            this.cbWellSuspend.Size = new System.Drawing.Size(46, 17);
            this.cbWellSuspend.TabIndex = 22;
            this.cbWellSuspend.Tag = "23";
            this.cbWellSuspend.Text = "КВЧ";
            this.cbWellSuspend.UseVisualStyleBackColor = true;
            this.cbWellSuspend.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbWellLeak
            // 
            this.cbWellLeak.AutoSize = true;
            this.cbWellLeak.Location = new System.Drawing.Point(249, 273);
            this.cbWellLeak.Name = "cbWellLeak";
            this.cbWellLeak.Size = new System.Drawing.Size(109, 17);
            this.cbWellLeak.TabIndex = 21;
            this.cbWellLeak.Tag = "22";
            this.cbWellLeak.Text = "ЗКЦ + Негермет";
            this.cbWellLeak.UseVisualStyleBackColor = true;
            this.cbWellLeak.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbWellResearch
            // 
            this.cbWellResearch.AutoSize = true;
            this.cbWellResearch.Location = new System.Drawing.Point(222, 296);
            this.cbWellResearch.Name = "cbWellResearch";
            this.cbWellResearch.Size = new System.Drawing.Size(118, 17);
            this.cbWellResearch.TabIndex = 20;
            this.cbWellResearch.Tag = "21";
            this.cbWellResearch.Text = "Исследования скв";
            this.cbWellResearch.UseVisualStyleBackColor = true;
            this.cbWellResearch.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbWellAction
            // 
            this.cbWellAction.AutoSize = true;
            this.cbWellAction.Location = new System.Drawing.Point(111, 296);
            this.cbWellAction.Name = "cbWellAction";
            this.cbWellAction.Size = new System.Drawing.Size(94, 17);
            this.cbWellAction.TabIndex = 19;
            this.cbWellAction.Tag = "20";
            this.cbWellAction.Text = "Мероприятия";
            this.cbWellAction.UseVisualStyleBackColor = true;
            this.cbWellAction.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbGTM
            // 
            this.cbGTM.AutoSize = true;
            this.cbGTM.Location = new System.Drawing.Point(6, 296);
            this.cbGTM.Name = "cbGTM";
            this.cbGTM.Size = new System.Drawing.Size(46, 17);
            this.cbGTM.TabIndex = 18;
            this.cbGTM.Tag = "19";
            this.cbGTM.Text = "ГТМ";
            this.cbGTM.UseVisualStyleBackColor = true;
            this.cbGTM.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbCoreTest
            // 
            this.cbCoreTest.AutoSize = true;
            this.cbCoreTest.Location = new System.Drawing.Point(111, 273);
            this.cbCoreTest.Name = "cbCoreTest";
            this.cbCoreTest.Size = new System.Drawing.Size(131, 17);
            this.cbCoreTest.TabIndex = 17;
            this.cbCoreTest.Tag = "17";
            this.cbCoreTest.Text = "Исследования керна";
            this.cbCoreTest.UseVisualStyleBackColor = true;
            this.cbCoreTest.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbCore
            // 
            this.cbCore.AutoSize = true;
            this.cbCore.Location = new System.Drawing.Point(6, 273);
            this.cbCore.Name = "cbCore";
            this.cbCore.Size = new System.Drawing.Size(51, 17);
            this.cbCore.TabIndex = 16;
            this.cbCore.Tag = "16";
            this.cbCore.Text = "Керн";
            this.cbCore.UseVisualStyleBackColor = true;
            this.cbCore.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbPerf
            // 
            this.cbPerf.AutoSize = true;
            this.cbPerf.Location = new System.Drawing.Point(223, 250);
            this.cbPerf.Name = "cbPerf";
            this.cbPerf.Size = new System.Drawing.Size(89, 17);
            this.cbPerf.TabIndex = 15;
            this.cbPerf.Tag = "15";
            this.cbPerf.Text = "Перфорация";
            this.cbPerf.UseVisualStyleBackColor = true;
            this.cbPerf.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbLogs
            // 
            this.cbLogs.AutoSize = true;
            this.cbLogs.Location = new System.Drawing.Point(111, 250);
            this.cbLogs.Name = "cbLogs";
            this.cbLogs.Size = new System.Drawing.Size(71, 17);
            this.cbLogs.TabIndex = 14;
            this.cbLogs.Tag = "14";
            this.cbLogs.Text = "Каротаж";
            this.cbLogs.UseVisualStyleBackColor = true;
            this.cbLogs.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbGIS
            // 
            this.cbGIS.AutoSize = true;
            this.cbGIS.Location = new System.Drawing.Point(6, 250);
            this.cbGIS.Name = "cbGIS";
            this.cbGIS.Size = new System.Drawing.Size(46, 17);
            this.cbGIS.TabIndex = 13;
            this.cbGIS.Tag = "13";
            this.cbGIS.Text = "ГИС";
            this.cbGIS.UseVisualStyleBackColor = true;
            this.cbGIS.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbGrids
            // 
            this.cbGrids.AutoSize = true;
            this.cbGrids.Location = new System.Drawing.Point(6, 227);
            this.cbGrids.Name = "cbGrids";
            this.cbGrids.Size = new System.Drawing.Size(57, 17);
            this.cbGrids.TabIndex = 12;
            this.cbGrids.Tag = "12";
            this.cbGrids.Text = "Сетки";
            this.cbGrids.UseVisualStyleBackColor = true;
            this.cbGrids.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbWellPads
            // 
            this.cbWellPads.AutoSize = true;
            this.cbWellPads.Location = new System.Drawing.Point(6, 66);
            this.cbWellPads.Name = "cbWellPads";
            this.cbWellPads.Size = new System.Drawing.Size(58, 17);
            this.cbWellPads.TabIndex = 11;
            this.cbWellPads.Tag = "2";
            this.cbWellPads.Text = "Кусты";
            this.cbWellPads.UseVisualStyleBackColor = true;
            this.cbWellPads.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbChessSum
            // 
            this.cbChessSum.AutoSize = true;
            this.cbChessSum.Location = new System.Drawing.Point(151, 112);
            this.cbChessSum.Name = "cbChessSum";
            this.cbChessSum.Size = new System.Drawing.Size(213, 17);
            this.cbChessSum.TabIndex = 10;
            this.cbChessSum.Tag = "6";
            this.cbChessSum.Text = "Суммарные показатели по шахматке";
            this.cbChessSum.UseVisualStyleBackColor = true;
            this.cbChessSum.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbSum
            // 
            this.cbSum.AutoSize = true;
            this.cbSum.Location = new System.Drawing.Point(151, 89);
            this.cbSum.Name = "cbSum";
            this.cbSum.Size = new System.Drawing.Size(145, 17);
            this.cbSum.TabIndex = 9;
            this.cbSum.Tag = "4";
            this.cbSum.Text = "Суммарные показатели";
            this.cbSum.UseVisualStyleBackColor = true;
            this.cbSum.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbAreaSum
            // 
            this.cbAreaSum.AutoSize = true;
            this.cbAreaSum.Location = new System.Drawing.Point(151, 135);
            this.cbAreaSum.Name = "cbAreaSum";
            this.cbAreaSum.Size = new System.Drawing.Size(205, 17);
            this.cbAreaSum.TabIndex = 8;
            this.cbAreaSum.Tag = "8";
            this.cbAreaSum.Text = "Суммарные показатели по ячейкам";
            this.cbAreaSum.UseVisualStyleBackColor = true;
            this.cbAreaSum.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbTable81
            // 
            this.cbTable81.AutoSize = true;
            this.cbTable81.Location = new System.Drawing.Point(6, 204);
            this.cbTable81.Name = "cbTable81";
            this.cbTable81.Size = new System.Drawing.Size(246, 17);
            this.cbTable81.TabIndex = 7;
            this.cbTable81.Tag = "11";
            this.cbTable81.Text = "Проектные показатели по месторождению";
            this.cbTable81.UseVisualStyleBackColor = true;
            this.cbTable81.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbPVT
            // 
            this.cbPVT.AutoSize = true;
            this.cbPVT.Location = new System.Drawing.Point(6, 181);
            this.cbPVT.Name = "cbPVT";
            this.cbPVT.Size = new System.Drawing.Size(93, 17);
            this.cbPVT.TabIndex = 6;
            this.cbPVT.Tag = "10";
            this.cbPVT.Text = "PVT свойства";
            this.cbPVT.UseVisualStyleBackColor = true;
            this.cbPVT.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbOilObjects
            // 
            this.cbOilObjects.AutoSize = true;
            this.cbOilObjects.Location = new System.Drawing.Point(6, 158);
            this.cbOilObjects.Name = "cbOilObjects";
            this.cbOilObjects.Size = new System.Drawing.Size(176, 17);
            this.cbOilObjects.TabIndex = 5;
            this.cbOilObjects.Tag = "9";
            this.cbOilObjects.Text = "Список объектов разработки";
            this.cbOilObjects.UseVisualStyleBackColor = true;
            this.cbOilObjects.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbArea
            // 
            this.cbArea.AutoSize = true;
            this.cbArea.Location = new System.Drawing.Point(6, 135);
            this.cbArea.Name = "cbArea";
            this.cbArea.Size = new System.Drawing.Size(63, 17);
            this.cbArea.TabIndex = 4;
            this.cbArea.Tag = "7";
            this.cbArea.Text = "Ячейки";
            this.cbArea.UseVisualStyleBackColor = true;
            this.cbArea.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbChess
            // 
            this.cbChess.AutoSize = true;
            this.cbChess.Location = new System.Drawing.Point(6, 112);
            this.cbChess.Name = "cbChess";
            this.cbChess.Size = new System.Drawing.Size(78, 17);
            this.cbChess.TabIndex = 3;
            this.cbChess.Tag = "5";
            this.cbChess.Text = "Шахматка";
            this.cbChess.UseVisualStyleBackColor = true;
            this.cbChess.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbMer
            // 
            this.cbMer.AutoSize = true;
            this.cbMer.Location = new System.Drawing.Point(6, 89);
            this.cbMer.Name = "cbMer";
            this.cbMer.Size = new System.Drawing.Size(47, 17);
            this.cbMer.TabIndex = 2;
            this.cbMer.Tag = "3";
            this.cbMer.Text = "МЭР";
            this.cbMer.UseVisualStyleBackColor = true;
            this.cbMer.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbContours
            // 
            this.cbContours.AutoSize = true;
            this.cbContours.Location = new System.Drawing.Point(6, 43);
            this.cbContours.Name = "cbContours";
            this.cbContours.Size = new System.Drawing.Size(71, 17);
            this.cbContours.TabIndex = 1;
            this.cbContours.Tag = "1";
            this.cbContours.Text = "Контуры";
            this.cbContours.UseVisualStyleBackColor = true;
            this.cbContours.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // cbCoord
            // 
            this.cbCoord.AutoSize = true;
            this.cbCoord.Location = new System.Drawing.Point(6, 20);
            this.cbCoord.Name = "cbCoord";
            this.cbCoord.Size = new System.Drawing.Size(136, 17);
            this.cbCoord.TabIndex = 0;
            this.cbCoord.Tag = "0";
            this.cbCoord.Text = "Координаты скважин";
            this.cbCoord.UseVisualStyleBackColor = true;
            this.cbCoord.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // bOk
            // 
            this.bOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(300, 730);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 4;
            this.bOk.Text = "OK";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(396, 730);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 5;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // OilFieldList
            // 
            this.OilFieldList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.OilFieldList.FormattingEnabled = true;
            this.OilFieldList.Location = new System.Drawing.Point(15, 56);
            this.OilFieldList.Name = "OilFieldList";
            this.OilFieldList.ScrollAlwaysVisible = true;
            this.OilFieldList.Size = new System.Drawing.Size(471, 324);
            this.OilFieldList.TabIndex = 6;
            // 
            // lOilFieldList
            // 
            this.lOilFieldList.AutoSize = true;
            this.lOilFieldList.Location = new System.Drawing.Point(12, 40);
            this.lOilFieldList.Name = "lOilFieldList";
            this.lOilFieldList.Size = new System.Drawing.Size(217, 13);
            this.lOilFieldList.TabIndex = 7;
            this.lOilFieldList.Text = "Список экспортируемых месторождений:";
            // 
            // lCount
            // 
            this.lCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lCount.Location = new System.Drawing.Point(235, 37);
            this.lCount.Name = "lCount";
            this.lCount.Size = new System.Drawing.Size(251, 16);
            this.lCount.TabIndex = 8;
            this.lCount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // bByInspector
            // 
            this.bByInspector.Image = global::SmartPlus.Properties.Resources.skip_forward;
            this.bByInspector.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.bByInspector.Location = new System.Drawing.Point(345, 65);
            this.bByInspector.Name = "bByInspector";
            this.bByInspector.Size = new System.Drawing.Size(118, 40);
            this.bByInspector.TabIndex = 9;
            this.bByInspector.Text = "С инспектора";
            this.bByInspector.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bByInspector.UseVisualStyleBackColor = true;
            this.bByInspector.Click += new System.EventHandler(this.bByInspector_Click);
            // 
            // bByWorkFolder
            // 
            this.bByWorkFolder.Image = global::SmartPlus.Properties.Resources.skip_forward;
            this.bByWorkFolder.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.bByWorkFolder.Location = new System.Drawing.Point(345, 111);
            this.bByWorkFolder.Name = "bByWorkFolder";
            this.bByWorkFolder.Size = new System.Drawing.Size(118, 40);
            this.bByWorkFolder.TabIndex = 10;
            this.bByWorkFolder.Text = "С раб.папки";
            this.bByWorkFolder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bByWorkFolder.UseVisualStyleBackColor = true;
            this.bByWorkFolder.Click += new System.EventHandler(this.bByWorkFolder_Click);
            // 
            // cbExportLastHour
            // 
            this.cbExportLastHour.AutoSize = true;
            this.cbExportLastHour.Location = new System.Drawing.Point(15, 370);
            this.cbExportLastHour.Name = "cbExportLastHour";
            this.cbExportLastHour.Size = new System.Drawing.Size(229, 17);
            this.cbExportLastHour.TabIndex = 11;
            this.cbExportLastHour.Text = "Выгрузить созданные за последний час";
            this.cbExportLastHour.UseVisualStyleBackColor = true;
            // 
            // cbBP
            // 
            this.cbBP.AutoSize = true;
            this.cbBP.Location = new System.Drawing.Point(258, 204);
            this.cbBP.Name = "cbBP";
            this.cbBP.Size = new System.Drawing.Size(157, 17);
            this.cbBP.TabIndex = 23;
            this.cbBP.Tag = "24";
            this.cbBP.Text = "Показатели Бизнес-плана";
            this.cbBP.UseVisualStyleBackColor = true;
            this.cbBP.CheckedChanged += new System.EventHandler(this.DataCheckChange);
            // 
            // ExportProject
            // 
            this.AcceptButton = this.bOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(493, 772);
            this.Controls.Add(this.cbExportLastHour);
            this.Controls.Add(this.bByWorkFolder);
            this.Controls.Add(this.bByInspector);
            this.Controls.Add(this.lCount);
            this.Controls.Add(this.lOilFieldList);
            this.Controls.Add(this.OilFieldList);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.gbDataType);
            this.Controls.Add(this.tbDest);
            this.Controls.Add(this.lDest);
            this.Controls.Add(this.bOpen);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "ExportProject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Экспорт данных проекта";
            this.TopMost = true;
            this.gbDataType.ResumeLayout(false);
            this.gbDataType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bOpen;
        private System.Windows.Forms.Label lDest;
        private System.Windows.Forms.TextBox tbDest;
        private System.Windows.Forms.GroupBox gbDataType;
        private System.Windows.Forms.CheckBox cbChessSum;
        private System.Windows.Forms.CheckBox cbSum;
        private System.Windows.Forms.CheckBox cbAreaSum;
        private System.Windows.Forms.CheckBox cbTable81;
        private System.Windows.Forms.CheckBox cbPVT;
        private System.Windows.Forms.CheckBox cbOilObjects;
        private System.Windows.Forms.CheckBox cbArea;
        private System.Windows.Forms.CheckBox cbChess;
        private System.Windows.Forms.CheckBox cbMer;
        private System.Windows.Forms.CheckBox cbContours;
        private System.Windows.Forms.CheckBox cbCoord;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.CheckedListBox OilFieldList;
        private System.Windows.Forms.Label lOilFieldList;
        private System.Windows.Forms.CheckBox cbWellPads;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox cbGrids;
        private System.Windows.Forms.CheckBox cbLogs;
        private System.Windows.Forms.CheckBox cbGIS;
        private System.Windows.Forms.Label lCount;
        private System.Windows.Forms.Button bByInspector;
        private System.Windows.Forms.Button bByWorkFolder;
        private System.Windows.Forms.CheckBox cbPerf;
        private System.Windows.Forms.CheckBox cbExportLastHour;
        private System.Windows.Forms.CheckBox cbCoreTest;
        private System.Windows.Forms.CheckBox cbCore;
        private System.Windows.Forms.CheckBox cbWellAction;
        private System.Windows.Forms.CheckBox cbGTM;
        private System.Windows.Forms.CheckBox cbWellResearch;
        private System.Windows.Forms.CheckBox cbWellLeak;
        private System.Windows.Forms.CheckBox cbWellSuspend;
        private System.Windows.Forms.CheckBox cbBP;
    }
}