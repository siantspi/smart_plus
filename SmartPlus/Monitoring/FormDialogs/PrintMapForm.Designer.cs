﻿namespace SmartPlus
{
    partial class PrintMapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OkButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SettingGroup = new System.Windows.Forms.GroupBox();
            this.StretchImageCheck = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SizeImageFormat = new System.Windows.Forms.ComboBox();
            this.SettingGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.Location = new System.Drawing.Point(478, 254);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(95, 30);
            this.OkButton.TabIndex = 0;
            this.OkButton.Text = "Сохранить...";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(579, 254);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(95, 30);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "Закрыть";
            this.CloseButton.UseVisualStyleBackColor = true;
            // 
            // SettingGroup
            // 
            this.SettingGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingGroup.Controls.Add(this.StretchImageCheck);
            this.SettingGroup.Controls.Add(this.label1);
            this.SettingGroup.Controls.Add(this.SizeImageFormat);
            this.SettingGroup.Location = new System.Drawing.Point(478, 12);
            this.SettingGroup.Name = "SettingGroup";
            this.SettingGroup.Size = new System.Drawing.Size(196, 236);
            this.SettingGroup.TabIndex = 3;
            this.SettingGroup.TabStop = false;
            this.SettingGroup.Text = " Параметры ";
            // 
            // StretchImageCheck
            // 
            this.StretchImageCheck.AutoSize = true;
            this.StretchImageCheck.Location = new System.Drawing.Point(5, 66);
            this.StretchImageCheck.Name = "StretchImageCheck";
            this.StretchImageCheck.Size = new System.Drawing.Size(127, 19);
            this.StretchImageCheck.TabIndex = 2;
            this.StretchImageCheck.Text = "Уместить на листе";
            this.StretchImageCheck.UseVisualStyleBackColor = true;
            this.StretchImageCheck.Visible = false;
            this.StretchImageCheck.CheckedChanged += new System.EventHandler(this.StretchImageCheck_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Размер изображения";
            // 
            // SizeImageFormat
            // 
            this.SizeImageFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SizeImageFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SizeImageFormat.FormattingEnabled = true;
            this.SizeImageFormat.Items.AddRange(new object[] {
            "A4 (297x210)",
            "A3 (420x297)",
            "A2 (594x420)",
            "A1 (841x594)",
            "A0 (1189x841)",
            "",
            "",
            ""});
            this.SizeImageFormat.Location = new System.Drawing.Point(5, 37);
            this.SizeImageFormat.MaxDropDownItems = 5;
            this.SizeImageFormat.Name = "SizeImageFormat";
            this.SizeImageFormat.Size = new System.Drawing.Size(184, 23);
            this.SizeImageFormat.TabIndex = 0;
            this.SizeImageFormat.SelectedIndexChanged += new System.EventHandler(this.SizeImageFormat_SelectedIndexChanged);
            // 
            // PrintMapForm
            // 
            this.AcceptButton = this.CloseButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(686, 296);
            this.Controls.Add(this.SettingGroup);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.OkButton);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "PrintMapForm";
            this.Text = "Печать карты";
            this.SettingGroup.ResumeLayout(false);
            this.SettingGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.GroupBox SettingGroup;
        private System.Windows.Forms.ComboBox SizeImageFormat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox StretchImageCheck;
    }
}