﻿namespace SmartPlus
{
    partial class MarkerAutoCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CreateButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.lMarker = new System.Windows.Forms.Label();
            this.MarkersComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OilFieldTextBox = new System.Windows.Forms.TextBox();
            this.Comment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CreateButton
            // 
            this.CreateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateButton.Location = new System.Drawing.Point(135, 109);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(95, 30);
            this.CreateButton.TabIndex = 2;
            this.CreateButton.Text = "Создать";
            this.CreateButton.UseVisualStyleBackColor = true;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(236, 109);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(95, 30);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "Закрыть";
            this.CloseButton.UseVisualStyleBackColor = true;
            // 
            // lMarker
            // 
            this.lMarker.AutoSize = true;
            this.lMarker.Location = new System.Drawing.Point(12, 38);
            this.lMarker.Name = "lMarker";
            this.lMarker.Size = new System.Drawing.Size(109, 15);
            this.lMarker.TabIndex = 4;
            this.lMarker.Text = "Выберите маркер:";
            // 
            // MarkersComboBox
            // 
            this.MarkersComboBox.FormattingEnabled = true;
            this.MarkersComboBox.Location = new System.Drawing.Point(127, 35);
            this.MarkersComboBox.Name = "MarkersComboBox";
            this.MarkersComboBox.Size = new System.Drawing.Size(204, 23);
            this.MarkersComboBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Месторождение:";
            // 
            // OilFieldTextBox
            // 
            this.OilFieldTextBox.Location = new System.Drawing.Point(127, 6);
            this.OilFieldTextBox.Name = "OilFieldTextBox";
            this.OilFieldTextBox.ReadOnly = true;
            this.OilFieldTextBox.Size = new System.Drawing.Size(204, 23);
            this.OilFieldTextBox.TabIndex = 7;
            // 
            // Comment
            // 
            this.Comment.AutoSize = true;
            this.Comment.Location = new System.Drawing.Point(12, 69);
            this.Comment.Name = "Comment";
            this.Comment.Size = new System.Drawing.Size(0, 15);
            this.Comment.TabIndex = 8;
            // 
            // MarkerAutoCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 151);
            this.Controls.Add(this.Comment);
            this.Controls.Add(this.OilFieldTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MarkersComboBox);
            this.Controls.Add(this.lMarker);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.CreateButton);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MarkerAutoCreateForm";
            this.Text = "Создание маркеров по данным ГИС";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label lMarker;
        private System.Windows.Forms.ComboBox MarkersComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox OilFieldTextBox;
        private System.Windows.Forms.Label Comment;
    }
}