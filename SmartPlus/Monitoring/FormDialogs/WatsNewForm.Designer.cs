﻿namespace SmartPlus
{
    partial class WatsNewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lVersion = new System.Windows.Forms.Label();
            this.bExit = new System.Windows.Forms.Button();
            this.WatsNewtextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lVersion
            // 
            this.lVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lVersion.AutoSize = true;
            this.lVersion.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lVersion.Location = new System.Drawing.Point(0, 242);
            this.lVersion.Name = "lVersion";
            this.lVersion.Size = new System.Drawing.Size(115, 17);
            this.lVersion.TabIndex = 1;
            this.lVersion.Text = "Версия: 1.0.0.1234";
            this.lVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // bExit
            // 
            this.bExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bExit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bExit.Location = new System.Drawing.Point(507, 236);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(90, 30);
            this.bExit.TabIndex = 26;
            this.bExit.Text = "Закрыть";
            this.bExit.UseVisualStyleBackColor = true;
            // 
            // WatsNewtextBox
            // 
            this.WatsNewtextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WatsNewtextBox.Location = new System.Drawing.Point(-2, 0);
            this.WatsNewtextBox.Multiline = true;
            this.WatsNewtextBox.Name = "WatsNewtextBox";
            this.WatsNewtextBox.Size = new System.Drawing.Size(599, 230);
            this.WatsNewtextBox.TabIndex = 27;
            // 
            // WatsNewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(599, 268);
            this.ControlBox = false;
            this.Controls.Add(this.WatsNewtextBox);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.lVersion);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(601, 182);
            this.Name = "WatsNewForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Список изменений";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lVersion;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.TextBox WatsNewtextBox;
    }
}