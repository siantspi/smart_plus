﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Threading;


namespace SmartPlus
{
    public partial class HTTPSUpdateAppForm : Form
    {
        HTTPSUpdaterApp UpdaterApp;
        // Признак, что началось скачивание обновления, требуется ожидание завершения процесса
        public bool download() 
        {
            return UpdaterApp.download(); 
        }

        // Признак, что обновление не требуется или закончено, можно запускать программу.
        public bool skipped
        {
            get { return UpdaterApp.skipped; } 
        }

        int Mode;
        public HTTPSUpdateAppForm()
        {
            InitializeComponent();
                        
            this.FormClosing += new FormClosingEventHandler(UpdateAppForm_FormClosing);

            
            UpdaterApp = new HTTPSUpdaterApp();

            UpdaterApp.OnCheckAppUpdates += new OnEndCheckAppUpdatesDelegate(AppUpdater_OnCheckAppUpdates);
            UpdaterApp.OnStartAppDownload += new OnStartDownloadAppDelegate(AppUpdater_OnStartAppDownload);
            UpdaterApp.OnStartSetupApp += new OnStartSetupAppDelegate(AppUpdater_OnStartSetupApp);
            UpdaterApp.OnEndAppDownload += new OnEndDownloadAppDelegate(AppUpdater_OnEndAppDownload);
            UpdaterApp.OnEndItemAppCopy += new OnEndItemCopyAppDelegate(dataUpdater_OnEndItemAppCopy);
            
            if (UpdaterApp.download())
            {
                ShowUpdateForm();
            }
        }

        public HTTPSUpdateAppForm(MainForm mainForm)
        {
            InitializeComponent();

            this.FormClosing += new FormClosingEventHandler(UpdateAppForm_FormClosing);

            UpdaterApp = new HTTPSUpdaterApp(mainForm.AppSettings);

            UpdaterApp.OnCheckAppUpdates += new OnEndCheckAppUpdatesDelegate(AppUpdater_OnCheckAppUpdates);
            UpdaterApp.OnStartAppDownload += new OnStartDownloadAppDelegate(AppUpdater_OnStartAppDownload);
            UpdaterApp.OnStartSetupApp += new OnStartSetupAppDelegate(AppUpdater_OnStartSetupApp);
            UpdaterApp.OnEndAppDownload += new OnEndDownloadAppDelegate(AppUpdater_OnEndAppDownload);
            UpdaterApp.OnEndItemAppCopy += new OnEndItemCopyAppDelegate(dataUpdater_OnEndItemAppCopy);

            if (UpdaterApp.download())
            {
                ShowUpdateForm();
            }
        }

        void UpdateAppForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Mode == 1)
            {
                if (MessageBox.Show("Вы действительно хотите отменить загрузку обновления приложения?\nВ случае отмены установки при следующем запуске потребуется подключение к сети.", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.DialogResult = DialogResult.Abort;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        void AppUpdater_OnCheckAppUpdates()
        {
            if (UpdaterApp.UpdateAppAvailbe())
            {
                ShowUpdateForm();
            }
            else
            {
                this.Close();
            }
        }

        void AppUpdater_OnStartAppDownload()
        {
            StartDownloadUpdate();
        }

        void AppUpdater_OnStartSetupApp()
        {
            StartSetupApp();
        }

        void AppUpdater_OnEndAppDownload()
        {
            Mode = 2;
            this.Close();
        }

        public void ShowUpdateForm()
        {
            lTitle.Text = "Доступно обновление";
            lText.Text = "На сервере доступна новая версия ПК \"SmartPlus\". Загрузить ее?";
            pb.Visible = false;
            bYes.Visible = true;
            bNo.Visible = true;
            bClose.Visible = false;
            Mode = 0;
        }


        public void StartDownloadUpdate()
        {
            lTitle.Text = "Загрузка обновления SmartPlus с сервера.";
            lText.Text = "Пожалуйста подождите...";
            pb.Style = ProgressBarStyle.Marquee;
            pb.Visible = true;
            bYes.Visible = false;
            bNo.Visible = false;
            bClose.Visible = false;
            Mode = 1;
            UpdaterApp.start_updateApp();
        }

        public void StartSetupApp()
        {
            lTitle.Text = "Загрузка файлов приложения завершена.";
            lText.Text = "Выполняется установка обновления";
            //pb.Style = ProgressBarStyle.Blocks;
            pb.Style = ProgressBarStyle.Marquee;
            pb.Visible = true;
            bYes.Visible = false;
            bNo.Visible = false;
            bClose.Visible = false;
            Mode = 2;
        }

        public void SetCancelUpdate(string Title, string Message)
        {
            pb.Visible = false;
            bYes.Visible = false;
            bNo.Visible = false;
            bClose.Visible = true;
            lTitle.Text = Title;
            lText.Text = Message;
            Mode = 4;
        }

        public void SetProgress(long BytesCompleted, long BytesTotal)
        {
            if (BytesTotal > 0)
            {
                int progr = (int)Math.Round((double)BytesCompleted * 100.0f / (double)BytesTotal, 0);
                if (progr < 0) progr = 0;
                if (progr > 100) progr = 100;
                pb.Value = progr;
                if (progr < 100)
                {
                    lTitle.Text = String.Format("Загрузка обновления с сервера ({0:0}%)", ((double)BytesCompleted * 100.0f) / (double)BytesTotal);
                }
                else
                {
                    lTitle.Text = String.Format("Установка обновления. Пожалуйста подождите...");
                }
                lText.Text = String.Format("Загрузка: {0:0.##}MB of {1:0.##}MB", (double)BytesCompleted / (1024 * 1024), (double)BytesTotal / (1024 * 1024));
            }
        }


        void dataUpdater_OnEndItemAppCopy(UpdateFileItem item)
        {
            /*
            
            */
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bYes_Click(object sender, EventArgs e)
        {
            StartDownloadUpdate();
        }

        private void bNo_Click(object sender, EventArgs e)
        {
            UpdaterApp.skipped = true;
            this.Close();
        }

    }
}
