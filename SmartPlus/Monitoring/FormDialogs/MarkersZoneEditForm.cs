﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus
{
    public partial class MarkersZoneEditForm : Form
    {
        public PlaneMarkerZone ResultZone;
        List<PlaneMarker> InitMarkers;
        ComboBoxExColor FillColorCombo;

        public MarkersZoneEditForm()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
            FillColorCombo = new ComboBoxExColor();
            DrawGroup.Controls.Add(FillColorCombo);
            FillColorCombo.Location = new Point(47, 55);
            FillColorCombo.Size = new Size(178, 23);

            // масштабирование
            if (label1.Width > 123)
            {
                int dx = label1.Width - 123;
                NameBox.Left += dx;
                NameBox.Width -= dx;
                dx = label4.Width - 102;
                MarkerTopCombo.Left += dx;
                MarkerTopCombo.Width -= dx;
                MarkerBottomCombo.Left += dx;
                MarkerBottomCombo.Width -= dx;
                dx = label2.Width - 101;
                AlfaPercent.Left += dx;
                AlfaPercent.Width -= dx;
                dx = label3.Width - 35;
                FillColorCombo.Left += dx;
                FillColorCombo.Width -= dx;
            }

            this.FormClosing += new FormClosingEventHandler(MarkersZoneEditForm_FormClosing);
        }

        void MarkersZoneEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                if (NameBox.Text.Length == 0)
                {
                    MessageBox.Show(this, "Введите имя зоны маркеров.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                    return;
                }
                if ((MarkerTopCombo.SelectedIndex == -1) || (MarkerBottomCombo.SelectedIndex == -1))
                {
                    MessageBox.Show(this, "Выберите маркеры", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                    return;
                }
                ResultZone = new PlaneMarkerZone();
                ResultZone.Name = NameBox.Text;
                ResultZone.MarkerTop = InitMarkers[MarkerTopCombo.SelectedIndex];
                ResultZone.MarkerBottom = InitMarkers[MarkerBottomCombo.SelectedIndex];
                ResultZone.Alfa = (int)((float)(100 - AlfaPercent.Value) * 255f / 100f);
                ResultZone.FillBrush = new SolidBrush(Color.FromArgb(ResultZone.Alfa, FillColorCombo.SelectedColor));
            }
            else
            {
                ResultZone = null;
            }
        }

        public DialogResult ShowDialog(List<PlaneMarker> Markers, PlaneMarkerZone Zone)
        {
            ResultZone = null;
            this.InitMarkers = Markers;
            MarkerTopCombo.Items.Clear();
            MarkerBottomCombo.Items.Clear();
            for (int i = 0; i < Markers.Count; i++)
            {
                MarkerTopCombo.Items.Add(Markers[i].srcMarker.Name);
                MarkerBottomCombo.Items.Add(Markers[i].srcMarker.Name);
            }
            if (Zone == null)
            {
                NameBox.Text = string.Empty;
                if (MarkerTopCombo.Items.Count > 0) MarkerTopCombo.SelectedIndex = 0;
                if (MarkerBottomCombo.Items.Count > 1) MarkerBottomCombo.SelectedIndex = 1;
                AlfaPercent.Value = 50;
                FillColorCombo.SelectedColor = Color.White;
            }
            else
            {
                NameBox.Text = Zone.Name;
                if (MarkerTopCombo.Items.Count > 0)
                {
                    for (int i = 0; i < MarkerTopCombo.Items.Count; i++)
                    {
                        if (Zone.MarkerTop.srcMarker.Name == (string)MarkerTopCombo.Items[i])
                        {
                            MarkerTopCombo.SelectedIndex = i;
                            break;
                        }
                    }
                }
                if (MarkerBottomCombo.Items.Count > 0)
                {
                    for (int i = 0; i < MarkerBottomCombo.Items.Count; i++)
                    {
                        if (Zone.MarkerBottom.srcMarker.Name == (string)MarkerBottomCombo.Items[i])
                        {
                            MarkerBottomCombo.SelectedIndex = i;
                            break;
                        }
                    }
                }
                AlfaPercent.Value = (int)((float)(255 - Zone.Alfa) * 100f / 255f);
                FillColorCombo.SelectedColor = Zone.FillBrush.Color;
            }
            return ShowDialog();
        }
    }
}
