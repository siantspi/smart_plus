﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SmartPlus
{
    public partial class GlobalProjectsForm : Form
    {
        public bool IsUsedSmartPlus;
        MainForm mainForm;
        bool ServerAvaivable = true;
        public string SelectedProjPath;
        BackgroundWorker bgWorker;
        List<string> projList;
        public bool bLocalProjects;
        int WorkerMode = 0;

        public GlobalProjectsForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            SelectedProjPath = string.Empty;
            
            bOk.FlatAppearance.BorderColor = Color.LightGray;
            bOk.FlatAppearance.MouseOverBackColor = Color.GhostWhite;
            bCancel.FlatAppearance.BorderColor = Color.LightGray;
            bCancel.FlatAppearance.MouseOverBackColor = Color.GhostWhite;
            bgWorker = new BackgroundWorker();
            bgWorker.WorkerSupportsCancellation = true;
            bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);

            IsUsedSmartPlus = false;
            pictureBox1.Location = new Point(35, 12);
            pictureBox1.Size = new System.Drawing.Size(295, 40);
            pictureBox1.Image = Properties.Resources.BNLogo;
            lbGlobalProjList.Size = new System.Drawing.Size(341, 304);
            lbGlobalProjList.Location = new Point(12,58);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
        }
        
        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (WorkerMode)
            {
                case 0:
                    TestServerAvaivable();
                    break;
                case 1:
                    GetProjectList((bool)e.Argument);
                    break;
            }
            
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (WorkerMode)
            {
                case 0:
                    WorkerMode = 1;
                    bgWorker.RunWorkerAsync(!ServerAvaivable);
                    break;
                case 1:
                    lbGlobalProjList.Items.Clear();
                    lbGlobalProjList.Enabled = true;

                    if ((projList != null) && (projList.Count > 0))
                    {
                        for (int i = 0; i < projList.Count; i++)
                        {
                            lbGlobalProjList.Items.Add(projList[i]);
                        }
                        lbGlobalProjList.SelectedIndex = 0;
                    }
                    else
                    {
                        if (!bLocalProjects)
                        {
                            bLocalProjects = true;
                            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                                lbGlobalProjList.Items.Add("На сервере проекты не найдены.Чтение из кэша...");
                            else
                                lbGlobalProjList.Items.Add("Недоступно сетевое подключение.Чтение из кэша...");
                            // попытка открытия кеша
                            bgWorker.RunWorkerAsync(true);
                        }
                        else
                        {
                            lbGlobalProjList.Items.Add("На сервере и в кеше проекты не найдены.");
                            lbGlobalProjList.Enabled = false;
                        }
                    }
                    break;
            }
        }
        private void TestServerAvaivable()
        {
            ServerAvaivable = true;
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                try
                {
                    System.Net.NetworkInformation.PingReply pingResult = ping.Send(mainForm.ServerName, 1000); /// xxx
                    if (pingResult.Status != System.Net.NetworkInformation.IPStatus.Success)
                    {
                        string ProjFolder = @"\\" + mainForm.ServerName + "\\Monitoring";
                        string[] dirList = Directory.GetDirectories(ProjFolder);
                        if (dirList.Length == 0) ServerAvaivable = false;
                    }
                }
                catch
                {
                    ServerAvaivable = false;
                }
            }
            else
            {
                ServerAvaivable = false;
            }
        }
        private void GetProjectList(bool GetLocalProject)
        {
            string ProjFolder;
            int pathLen;
            List<string> HttpProjList = new List<string>();
            projList = new List<string>();

            ProjFolder = App.GetRegistryPath();
            if (ProjFolder.Length == 0) ProjFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            IsUsedSmartPlus = true;
            ProjFolder += @"\SmartPlus\Проекты";

            pathLen = ProjFolder.Length;
            if (Directory.Exists(ProjFolder))
            {
                string[] dirList = Directory.GetDirectories(ProjFolder);
                for (int i = 0; i < dirList.Length; i++)
                {
                    projList.Add(dirList[i].Remove(0, pathLen + 1));
                }
            }

            if (!Directory.Exists(ProjFolder)) Directory.CreateDirectory(ProjFolder);

            if (mainForm.AppSettings.UpdatebyHttps && Directory.Exists(ProjFolder))
            {
                try
                {
                    HttpProjList = DataUpdater.GetProjectListByHttp(mainForm.AppSettings.UpdatebyHttpsServerName, mainForm.AppSettings.UpdatebyHttpsUserName, mainForm.AppSettings.UpdatebyHttpsPassword, ProjFolder);
                }
                catch
                {
                }

                if (projList != null)
                {
                    foreach (string proj in HttpProjList)
                    {
                        if (!(projList.Contains(proj)))
                        {
                            projList.Add(proj);
                        }
                    }
                }
                else
                {
                    projList = HttpProjList;
                }
            }
        }

        private void bOk_MouseEnter(object sender, EventArgs e)
        {
            bOk.FlatAppearance.BorderSize = 2;
        }
        private void bOk_MouseLeave(object sender, EventArgs e)
        {
            bOk.FlatAppearance.BorderSize = 0;
        }
        private void bCancel_MouseEnter(object sender, EventArgs e)
        {
            bCancel.FlatAppearance.BorderSize = 2;
        }
        private void bCancel_MouseLeave(object sender, EventArgs e)
        {
            bCancel.FlatAppearance.BorderSize = 0;
        }
        private void bOk_Click(object sender, EventArgs e)
        {
            if (lbGlobalProjList.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите проект!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else 
            {
                mainForm.UserSmartPlusFolder = App.GetRegistryPath();
                if (mainForm.UserSmartPlusFolder.Length == 0) 
                {
                    mainForm.UserSmartPlusFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                mainForm.UserSmartPlusFolder += (IsUsedSmartPlus) ? "\\SmartPlus" : "\\МиР";
                mainForm.UserProjectsFolder = mainForm.UserSmartPlusFolder + "\\Проекты";
                mainForm.UserWorkDataFolder = mainForm.UserSmartPlusFolder + "\\Рабочая папка";
                mainForm.UserAppSettingsFolder = mainForm.UserSmartPlusFolder + "\\Настройки";
                SelectedProjPath = mainForm.UserProjectsFolder + @"\" + (string)lbGlobalProjList.Items[lbGlobalProjList.SelectedIndex];
                this.DialogResult = DialogResult.OK;
                Close();
            }
        }
        private void fGlobalProjects_Shown(object sender, EventArgs e)
        {
            bLocalProjects = false;
            lbGlobalProjList.Items.Clear();
            lbGlobalProjList.Items.Add("Подождите. Идет загрузка проектов...");
            lbGlobalProjList.Enabled = false;
            WorkerMode = 0;

            DateTime dt = File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().Location);
            if ((DateTime.Now - dt).Days > 90)
            {
                MessageBox.Show(Constant.AppOldMessage, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!bgWorker.IsBusy) bgWorker.RunWorkerAsync();
        }
        private void fGlobalProjects_Activated(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.None;
        }
        private void lbGlobalProjList_DoubleClick(object sender, EventArgs e)
        {
            if (lbGlobalProjList.SelectedIndex > -1)
            {
                this.bOk.PerformClick();
            }
        }
    }
}
