﻿namespace SmartPlus
{
    partial class UpdateProjForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bUpdate = new System.Windows.Forms.Button();
            this.bExit = new System.Windows.Forms.Button();
            this.pb = new System.Windows.Forms.ProgressBar();
            this.twUpdate = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.MainTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bUpdate
            // 
            this.bUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bUpdate.AutoSize = true;
            this.bUpdate.Location = new System.Drawing.Point(12, 492);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(184, 31);
            this.bUpdate.TabIndex = 1;
            this.bUpdate.Text = "Обновить немедленно";
            this.bUpdate.UseVisualStyleBackColor = true;
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // bExit
            // 
            this.bExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bExit.Location = new System.Drawing.Point(300, 494);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(90, 30);
            this.bExit.TabIndex = 2;
            this.bExit.Text = "Закрыть";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // pb
            // 
            this.pb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pb.Location = new System.Drawing.Point(12, 467);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(378, 21);
            this.pb.TabIndex = 4;
            // 
            // twUpdate
            // 
            this.twUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.twUpdate.Location = new System.Drawing.Point(12, 87);
            this.twUpdate.Name = "twUpdate";
            this.twUpdate.ShowNodeToolTips = true;
            this.twUpdate.Size = new System.Drawing.Size(378, 374);
            this.twUpdate.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(9, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 21);
            this.label1.TabIndex = 8;
            this.label1.Text = "Список данных для обновления:";
            // 
            // MainTitle
            // 
            this.MainTitle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainTitle.Location = new System.Drawing.Point(12, 9);
            this.MainTitle.Name = "MainTitle";
            this.MainTitle.Size = new System.Drawing.Size(378, 54);
            this.MainTitle.TabIndex = 9;
            this.MainTitle.Text = "Выполняется запись обновлений данных проекта.\r\nПожалуйста подождите...";
            // 
            // UpdateProjForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bExit;
            this.ClientSize = new System.Drawing.Size(402, 533);
            this.ControlBox = false;
            this.Controls.Add(this.MainTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.twUpdate);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.bUpdate);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateProjForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Обновление данных с сервера";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bUpdate;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.ProgressBar pb;
        private System.Windows.Forms.TreeView twUpdate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label MainTitle;
    }
}