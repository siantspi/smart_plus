﻿namespace SmartPlus
{
    partial class IconSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.pIconMap = new System.Windows.Forms.Panel();
            this.vScroll = new System.Windows.Forms.VScrollBar();
            this.pbIconMap = new System.Windows.Forms.PictureBox();
            this.lSymbInd = new System.Windows.Forms.Label();
            this.pbLargeIcon = new System.Windows.Forms.PictureBox();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.cbFonts = new System.Windows.Forms.ComboBox();
            this.lFont = new System.Windows.Forms.Label();
            this.lSize = new System.Windows.Forms.Label();
            this.lColor = new System.Windows.Forms.Label();
            this.udSize = new System.Windows.Forms.NumericUpDown();
            this.pIconMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIconMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLargeIcon)).BeginInit();
            this.gbSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udSize)).BeginInit();
            this.SuspendLayout();
            // 
            // bOk
            // 
            this.bOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(652, 394);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 0;
            this.bOk.Text = "ОK";
            this.bOk.UseVisualStyleBackColor = true;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(748, 394);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 1;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // pIconMap
            // 
            this.pIconMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pIconMap.Controls.Add(this.vScroll);
            this.pIconMap.Controls.Add(this.pbIconMap);
            this.pIconMap.Location = new System.Drawing.Point(12, 58);
            this.pIconMap.Name = "pIconMap";
            this.pIconMap.Size = new System.Drawing.Size(826, 310);
            this.pIconMap.TabIndex = 2;
            // 
            // vScroll
            // 
            this.vScroll.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScroll.Location = new System.Drawing.Point(809, 0);
            this.vScroll.Name = "vScroll";
            this.vScroll.Size = new System.Drawing.Size(17, 310);
            this.vScroll.TabIndex = 1;
            this.vScroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScroll_Scroll);
            // 
            // pbIconMap
            // 
            this.pbIconMap.BackColor = System.Drawing.Color.White;
            this.pbIconMap.Location = new System.Drawing.Point(0, 0);
            this.pbIconMap.Name = "pbIconMap";
            this.pbIconMap.Size = new System.Drawing.Size(786, 284);
            this.pbIconMap.TabIndex = 0;
            this.pbIconMap.TabStop = false;
            // 
            // lSymbInd
            // 
            this.lSymbInd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lSymbInd.AutoSize = true;
            this.lSymbInd.Location = new System.Drawing.Point(68, 389);
            this.lSymbInd.Name = "lSymbInd";
            this.lSymbInd.Size = new System.Drawing.Size(133, 21);
            this.lSymbInd.TabIndex = 5;
            this.lSymbInd.Text = "Индекс символа:";
            // 
            // pbLargeIcon
            // 
            this.pbLargeIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pbLargeIcon.Location = new System.Drawing.Point(12, 379);
            this.pbLargeIcon.Name = "pbLargeIcon";
            this.pbLargeIcon.Size = new System.Drawing.Size(50, 50);
            this.pbLargeIcon.TabIndex = 6;
            this.pbLargeIcon.TabStop = false;
            // 
            // gbSettings
            // 
            this.gbSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSettings.Controls.Add(this.cbFonts);
            this.gbSettings.Controls.Add(this.lFont);
            this.gbSettings.Controls.Add(this.lSize);
            this.gbSettings.Controls.Add(this.lColor);
            this.gbSettings.Controls.Add(this.udSize);
            this.gbSettings.Location = new System.Drawing.Point(12, 2);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Size = new System.Drawing.Size(826, 50);
            this.gbSettings.TabIndex = 7;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = " Настройки значка ";
            // 
            // cbFonts
            // 
            this.cbFonts.FormattingEnabled = true;
            this.cbFonts.Location = new System.Drawing.Point(60, 20);
            this.cbFonts.Name = "cbFonts";
            this.cbFonts.Size = new System.Drawing.Size(215, 27);
            this.cbFonts.TabIndex = 14;
            this.cbFonts.SelectedIndexChanged += new System.EventHandler(this.cbFonts_SelectedIndexChanged);
            // 
            // lFont
            // 
            this.lFont.AutoSize = true;
            this.lFont.Location = new System.Drawing.Point(6, 23);
            this.lFont.Name = "lFont";
            this.lFont.Size = new System.Drawing.Size(66, 21);
            this.lFont.TabIndex = 13;
            this.lFont.Text = "Шрифт:";
            // 
            // lSize
            // 
            this.lSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lSize.AutoSize = true;
            this.lSize.Location = new System.Drawing.Point(610, 23);
            this.lSize.Name = "lSize";
            this.lSize.Size = new System.Drawing.Size(68, 21);
            this.lSize.TabIndex = 12;
            this.lSize.Text = "Размер:";
            // 
            // lColor
            // 
            this.lColor.AutoSize = true;
            this.lColor.Location = new System.Drawing.Point(281, 23);
            this.lColor.Name = "lColor";
            this.lColor.Size = new System.Drawing.Size(49, 21);
            this.lColor.TabIndex = 11;
            this.lColor.Text = "Цвет:";
            // 
            // udSize
            // 
            this.udSize.Location = new System.Drawing.Point(667, 21);
            this.udSize.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.udSize.Name = "udSize";
            this.udSize.Size = new System.Drawing.Size(153, 27);
            this.udSize.TabIndex = 10;
            this.udSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IconSelectForm
            // 
            this.AcceptButton = this.bOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(850, 436);
            this.ControlBox = false;
            this.Controls.Add(this.gbSettings);
            this.Controls.Add(this.pbLargeIcon);
            this.Controls.Add(this.lSymbInd);
            this.Controls.Add(this.pIconMap);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(460, 222);
            this.Name = "IconSelectForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выбор символа";
            this.pIconMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbIconMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLargeIcon)).EndInit();
            this.gbSettings.ResumeLayout(false);
            this.gbSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Panel pIconMap;
        private System.Windows.Forms.PictureBox pbIconMap;
        private System.Windows.Forms.Label lSymbInd;
        private System.Windows.Forms.VScrollBar vScroll;
        private System.Windows.Forms.PictureBox pbLargeIcon;
        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.Label lSize;
        private System.Windows.Forms.Label lColor;
        private System.Windows.Forms.NumericUpDown udSize;
        private System.Windows.Forms.ComboBox cbFonts;
        private System.Windows.Forms.Label lFont;
    }
}