﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace SmartPlus
{
    public static class SmartPlusGraphics
    {
        internal static class Well
        {
            internal static class Brushes
            {
                public static Brush InjDown = new SolidBrush(Color.FromArgb(0, 50, 100));
                public static Brush OilUp = new SolidBrush(Color.FromArgb(255, 177, 100));
                public static Brush LiqDownStr = new SolidBrush(Color.FromArgb(17, 69, 17));
                public static Brush OilDownStr = new SolidBrush(Color.FromArgb(69, 35, 9));
                public static Brush InjDownStr = new SolidBrush(Color.FromArgb(0, 25, 50));
                public static Brush WellHead = System.Drawing.Brushes.Gray;
                public static Brush LightBlack = new SolidBrush(Color.FromArgb(80, 80, 80));
                public static Brush LightGray = new SolidBrush(Color.FromArgb(211, 211, 211));
            }
            internal static class Pens
            {
                public static Pen WellHead = System.Drawing.Pens.Gray;
                public static Pen WellTrajectory = new Pen(Color.Blue, 1);
            }
        }

        internal static class Contour
        {
            internal static class Fonts
            {
                public static Font Name = new Font("Calibri", 8.25f, FontStyle.Bold);
            }
            internal static class Pens
            {
                public static Pen Default = new Pen(Color.Black, 3);
                public static Pen Region = new Pen(Color.LightPink, 3);
                public static Pen NGDU = new Pen(Color.CornflowerBlue, 3);
                public static Pen OilField = new Pen(Color.Black, 1);
                public static Pen Gray1 = new Pen(Color.Gray, 1);
                public static Pen Gray3 = new Pen(Color.Gray, 3);
                public static Pen Area = new Pen(Color.Red, 1);
                public static Pen Select = new Pen(Color.Coral, 5);
                public static Pen Fantom = new Pen(Color.Black, 1);
                public static Pen Blue = new Pen(Color.FromArgb(0, 82, 164), 1);
                public static Pen Black3 = new Pen(Color.Black, 3);
            }
            internal static class Brushes
            {
                public static Brush Blue = new SolidBrush(Color.FromArgb(0, 82, 164));
                public static Brush LightGray125 = new SolidBrush(Color.FromArgb(125, Color.LightGray));
                public static Brush LightSkyBlue125 = new SolidBrush(Color.FromArgb(125, Color.LightSkyBlue));
                public static Brush LightGreen125 = new SolidBrush(Color.FromArgb(125, Color.LightGreen));
                public static Brush Green125 = new SolidBrush(Color.FromArgb(125, Color.Green));
                public static Brush Green185 = new SolidBrush(Color.FromArgb(185, Color.Green));
                public static Brush Red125 = new SolidBrush(Color.FromArgb(125, Color.Red));
                public static Brush Red185 = new SolidBrush(Color.FromArgb(185, Color.Red));
                public static Brush Blue25 = new SolidBrush(Color.FromArgb(25, Color.Blue));
                public static Brush Peru200 = new SolidBrush(Color.FromArgb(200, Color.Peru));
                public static Brush Peru125 = new SolidBrush(Color.FromArgb(200, Color.Peru));
                public static Brush Black125 = new SolidBrush(Color.FromArgb(125, Color.Black));
                public static Brush Gray125 = new SolidBrush(Color.FromArgb(125, Color.Gray));
            }
        }

        internal static class WellPad
        {
            internal static class Pens
            {
                public static Pen Line = System.Drawing.Pens.LightGray;
                public static Pen AroundLine1 = System.Drawing.Pens.MediumSeaGreen;
                public static Pen AroundLine3 = new Pen(Color.MediumSeaGreen, 3);
            }
            internal static class Brushes
            {
                public static Brush Name = new SolidBrush(Color.SaddleBrown);
            }
        }

        internal static class VoronoiMap
        {
            internal static class Brushes
            {
                public static Brush LightRed = new SolidBrush(Color.FromArgb(255, 75, 75));
                public static Brush DarkRed = new SolidBrush(Color.FromArgb(225, 0, 0));
                public static Brush CrossLine = new TextureBrush(SmartPlus.Properties.Resources.line45_16, System.Drawing.Drawing2D.WrapMode.Tile);
            }
        }

        internal static class Brushes
        {
            public static Brush Gray75 = new SolidBrush(Color.FromArgb(75, Color.Gray));
        }

        internal static class Pens
        {
            public static Pen Black2 = new Pen(Color.Black, 2);
        }

        internal static class Fonts
        {
            public static FontFamily SmartFontFamily;
            public static FontFamily GIDfont1Family;
            public static FontFamily GIDfont2Family;
            public static FontFamily GIDfont3Family;
            public static FontFamily GIDfont4Family;
            public static FontFamily GIDfont5Family;
            public static FontFamily GIDfont6Family;
            private static System.Drawing.Text.PrivateFontCollection pfc;

            static Fonts()
            {
                List<string> fontNames = new List<string>(new string[] { "MiRFont", "GIDFont 1", "GIDFont 2", "GIDFont 3", "GIDFont 4", "GIDFont 5", "GIDFont 6" });
                int index;

                FontFamily[] fontFamilies = new FontFamily[] { null, null, null, null, null, null, null };
                for (int i = 0; i < FontFamily.Families.Length; i++)
                {
                    index = fontNames.IndexOf(FontFamily.Families[i].Name);
                    if (index > -1) fontFamilies[index] = FontFamily.Families[i];
                }
                FontFamily family = null;
                pfc = null;
                for (int i = 0; i < fontFamilies.Length; i++)
                {
                    if (fontFamilies[i] == null)
                    {
                        byte[] fontBuff = null;
                        if (pfc == null) pfc = new System.Drawing.Text.PrivateFontCollection();
                        switch (i)
                        {
                            case 0:
                                fontBuff = global::SmartPlus.Properties.Resources.SmartPlusFont;
                                break;
                            case 1:
                                fontBuff = global::SmartPlus.Properties.Resources.GIDFONT1;
                                break;
                            case 2:
                                fontBuff = global::SmartPlus.Properties.Resources.GIDFONT2;
                                break;
                            case 3:
                                fontBuff = global::SmartPlus.Properties.Resources.GIDFONT3;
                                break;
                            case 4:
                                fontBuff = global::SmartPlus.Properties.Resources.GIDFONT4;
                                break;
                            case 5:
                                fontBuff = global::SmartPlus.Properties.Resources.GIDFONT5;
                                break;
                            case 6:
                                fontBuff = global::SmartPlus.Properties.Resources.GIDFONT6;
                                break;
                        }
                        IntPtr ptrFont = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontBuff.Length);
                        System.Runtime.InteropServices.Marshal.Copy(fontBuff, 0, ptrFont, fontBuff.Length);
                        pfc.AddMemoryFont(ptrFont, fontBuff.Length);
                        System.Runtime.InteropServices.Marshal.FreeHGlobal(ptrFont);
                        fontBuff = null;
                        
                        for (int j = 0; j < pfc.Families.Length; j++)
                        {
                            if (pfc.Families[j].Name == fontNames[i])
                            {
                                family = pfc.Families[j];
                                break;
                            }
                        }
                    }
                    else
                    {
                        family = fontFamilies[i];
                    }
                    switch (family.Name)
                    {
                        case "MiRFont":
                            SmartFontFamily = family;
                            break;
                        case "GIDFont 1":
                            GIDfont1Family = family;
                            break;
                        case "GIDFont 2":
                            GIDfont2Family = family;
                            break;
                        case "GIDFont 3":
                            GIDfont3Family = family;
                            break;
                        case "GIDFont 4":
                            GIDfont4Family = family;
                            break;
                        case "GIDFont 5":
                            GIDfont5Family = family;
                            break;
                        case "GIDFont 6":
                            GIDfont6Family = family;
                            break;
                    }
                }
            }
        }
    }
}
