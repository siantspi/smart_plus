﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus.DictionaryObjects
{
    public enum DICTIONARY_ITEM_TYPE : short
    {
        NONE = -1,
        OILFIELD_COEFFICIENT = 0,
        OILFIELD_PARAMS,
        AGENT_INJ,
        BORE_TYPE,
        BOTTOM_STATE,
        CHARWORK,
        CONTOUR_TYPE,
        CONTRACTOR,
        FIELD_DEVELOP_DEGREE,
        FIELD_TYPE_HC,
        FIELD_TYPE_TPO,
        STRATUMGEOZONE,
        GTA_TYPE,
        GTM_INJ_TYPE,
        GTM_TYPE,
        LITOLOGY,
        LOG_MNEMONIC,
        LOG_UNIT,
        MEST,
        METHOD,
        PACKER_TYPE,
        PBD_AUTHOR,
        PERFORATOR_TYPE,
        PERF_TYPE,
        STRATUM,
        PRED,
        PROJ_DESTINATION,
        PROJ_MEROPR,
        PUMP_TYPE,
        ROCKER_TYPE,
        SATURATION,
        STATE,
        STAY_REASON,
        CONSTRUCTION,
        OILFIELD_AREA,
        GRID_TYPE,
        WELL_ACTION_TYPE,
        WELL_RESEARCH_TYPE,
        OILFIELD_DOMAIN,
        USERS_DOMAIN
    }

    public abstract class DictionaryBase
    {
        public DICTIONARY_ITEM_TYPE Type { get; set; }
        public string FileName
        {
            get
            {
                switch (Type)
                {
                    case DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT:
                        return "OilfieldCoefficient.csv";
                    case DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS:
                        return "OilfieldParams.csv";
                    case DICTIONARY_ITEM_TYPE.STRATUM:
                        return "Stratum.csv";
                    case DICTIONARY_ITEM_TYPE.PERF_TYPE:
                        return "PerfType.csv";
                    case DICTIONARY_ITEM_TYPE.CHARWORK:
                        return "CharWork.csv";
                    case DICTIONARY_ITEM_TYPE.STATE:
                        return "WellState.csv";
                    case DICTIONARY_ITEM_TYPE.METHOD:
                        return "WellMethod.csv";
                    case DICTIONARY_ITEM_TYPE.SATURATION:
                        return "Saturation.csv";
                    case DICTIONARY_ITEM_TYPE.CONSTRUCTION:
                        return "Construction.csv";
                    case DICTIONARY_ITEM_TYPE.OILFIELD_AREA:
                        return "OilFieldArea.csv";
                    case DICTIONARY_ITEM_TYPE.GRID_TYPE:
                        return "GridType.csv";
                    case DICTIONARY_ITEM_TYPE.LITOLOGY:
                        return "Lithology.csv";
                    case DICTIONARY_ITEM_TYPE.LOG_UNIT:
                        return "LogUnit.csv";
                    case DICTIONARY_ITEM_TYPE.LOG_MNEMONIC:
                        return "LogMnemonic.csv";
                    case DICTIONARY_ITEM_TYPE.GTM_TYPE:
                        return "GtmType.csv";
                    case DICTIONARY_ITEM_TYPE.GTA_TYPE:
                        return "gta_type.csv";
                    case DICTIONARY_ITEM_TYPE.WELL_ACTION_TYPE:
                        return "WellActionType.csv";
                    case DICTIONARY_ITEM_TYPE.WELL_RESEARCH_TYPE:
                        return "WellResearchType.csv";
                    case DICTIONARY_ITEM_TYPE.STAY_REASON:
                        return "StayReason.csv";
                    case DICTIONARY_ITEM_TYPE.OILFIELD_DOMAIN:
                        return "OilfiledDomain.csv";
                    case DICTIONARY_ITEM_TYPE.USERS_DOMAIN:
                        return "UsersDomain.csv";
                    default:
                        throw new ArgumentException(string.Format("Не поддерживаемый тип справочника [{0}]", Type));
                }
            }
        }

        public abstract int GetCodeByShortName(string ShortName);
        public abstract string GetShortNameByCode(int Code);
        public abstract string GetFullNameByCode(int Code);

        public abstract bool ReadFromFile(string Path);
        public abstract bool WriteToFile(string Path);
        public abstract bool ReadFromBinaryFile(string Path);
        public abstract bool WriteToBinaryFile(string Path);
        public abstract void ParseFileHeadString(string FileHead);
    }

    public class Dictionary<T> : DictionaryBase where T : DictionaryBaseItem, new()
    {
        public int VersionDict;
        public List<T> Items;

        public int Count { get { return Items.Count; } }
        public T this[int index] { get { return Items[index]; } }

        private bool UseIndexFromString = false;

        public Dictionary(DICTIONARY_ITEM_TYPE Type)
        {
            this.Type = Type;
            Items = new List<T>();
        }

        public void Add(T Item) 
        { 
            Items.Add(Item); 
        }

        public override int GetCodeByShortName(string ShortName) { throw new NotImplementedException(""); }
        public override string GetShortNameByCode(int Code) { throw new NotImplementedException(""); }
        public override string GetFullNameByCode(int Code) { throw new NotImplementedException(""); }

        public override bool ReadFromFile(string Path)
        {
            bool result = false;
            string fileName = Path + "\\" + FileName;
            if (File.Exists(fileName))
            {
                try
                {
                    T item;
                    string[] lines = File.ReadAllLines(fileName, Encoding.UTF8);
                    if (lines[0].Length > 0)
                        ParseFileHeadString(lines[0]);
                    for(int i = 1; i < lines.Length;i++)
                    {
                        item = new T();
                        if (UseIndexFromString)
                        {
                            string[] parseStr = lines[i].Split(new char[] { ';' });
                            item.Index = System.Convert.ToInt32(parseStr[0]);
                            int k = lines[i].IndexOf(";");
                            if (k > 0) lines[i] = lines[i].Remove(0, k + 1);
                        }
                        else
                        {
                        item.Index = i - 1;
                        }

                        item.Parse(lines[i]);
                        Items.Add(item);
                    }
                    result = true;
                }
                catch
                {
                    Items.Clear();
                    result = false;
                }
            }
            return result;
        }
        public override bool WriteToFile(string Path)
        {
            bool writeIndexToDict = false;
            bool result = false;
            string fileName = Path + "\\" + FileName;
            if(Count > 0)
            {
                try
                {
                    string[] lines = new string[Items.Count + 1];
                    if (writeIndexToDict)
                    {
                        lines[0] = "Index;" + Items[0].GetDefaultFileHead();
                    }
                    else
                    {

                        lines[0] = Items[0].GetDefaultFileHead();
                    }
                    for (int i = 0; i < Items.Count; i++)
                    {
                        if (writeIndexToDict)
                        {
                            lines[i + 1] = Items[i].Index.ToString() + ";" + Items[i].ToString();
                        }
                        else
                        {
                            lines[i + 1] = Items[i].ToString();
                        }
                    }
                    File.WriteAllLines(fileName, lines, Encoding.UTF8);
                    result = true;
                }
                catch
                {
                    result = false;
                }
            }
            return result;
        }

        public override void ParseFileHeadString(string FileHead)
        {
            string[] parseStr = FileHead.Split(new char[] { ';' });
            string sIndex = parseStr[0];
            if (sIndex == "Index")
                UseIndexFromString = true;
        }

        public override bool ReadFromBinaryFile(string Path)
        {
            bool result = false;
            string fileName = Path;
            int iLen;
            using (BinaryReader bfr = new BinaryReader(new FileStream(fileName, FileMode.Open)))
            {
                Stream fs = bfr.BaseStream;
                VersionDict = bfr.ReadInt32();
                iLen = bfr.ReadInt32();

                if (iLen > 0)
                {
                    T item;
                    for (int i = 0; i < iLen; i++)
                    {
                        item = new T();
                        item.Index = i - 1;
                        item.BinaryReadItem(bfr, VersionDict);
                        Items.Add(item);

                    }
                }
                result = true;
            }
            if (!result) Items.Clear();
            return result;
        }

        public override bool WriteToBinaryFile(string Path)
        {
            bool result = false;
            string fileName = Path;
            if (Count > 0)
            {
                using (BinaryWriter bfw = new BinaryWriter(new FileStream(fileName, FileMode.Create)))
                {
                    Stream fs = bfw.BaseStream;
                    bfw.Write(VersionDict);
                    bfw.Write(Count);
                    if (Count > 0)
                    {
                        for (int i = 0; i < Count; i++)
                        {
                            Items[i].BinaryWriteItem(bfw, VersionDict);
                        }
                    }
                    bfw.Close();
                    File.SetCreationTime(fileName, DateTime.Now);
                    File.SetLastWriteTime(fileName, DateTime.Now);
                    result = true;
                }
            }
            return result;
        }
    }
}
