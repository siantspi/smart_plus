﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus.DictionaryObjects
{
    public class OilFieldParamsItem : DictionaryBaseItem
    {
        public string OilFieldName;
        public int EnterpriseCode;
        public string EnterpriseName;
        public int NGDUCode;
        public string NGDUName;
        public int RDFFieldCode;
        public string RdfServerName;
        public int RdfPort;
        public string RdfLogin;
        public string RdfPass;
        public byte RdfLoadByAreas;
        public int OraFieldCode;
        public string OraServerName;
        public int OraPort;
        public string OraSid;
        public string OraLogin;
        public string OraPass;

        public OilFieldParamsItem() { }
        public OilFieldParamsItem(OilFieldParamsItem Item)
        {
            OilFieldName = Item.OilFieldName;
            EnterpriseCode = Item.EnterpriseCode;
            EnterpriseName = Item.EnterpriseName;
            NGDUCode = Item.NGDUCode;
            NGDUName = Item.NGDUName;
            RDFFieldCode = Item.RDFFieldCode;
            RdfServerName = Item.RdfServerName;
            RdfPort = Item.RdfPort;
            RdfLogin = Item.RdfLogin;
            RdfPass = Item.RdfPass;
            RdfLoadByAreas = Item.RdfLoadByAreas;
            OraFieldCode = Item.OraFieldCode;
            OraPort = Item.OraPort;
            OraSid = Item.OraSid;
            OraLogin = Item.OraLogin;
            OraPass = Item.OraPass;
        }
        new public bool Equals(OilFieldParamsItem Item)
        {
            return ((OilFieldName == Item.OilFieldName) &&
                    (EnterpriseCode == Item.EnterpriseCode) &&
                    (EnterpriseName == Item.EnterpriseName) &&
                    (NGDUCode == Item.NGDUCode) &&
                    (NGDUName == Item.NGDUName) &&
                    (RDFFieldCode == Item.RDFFieldCode) &&
                    (RdfPort == Item.RdfPort) &&
                    (RdfLogin == Item.RdfLogin) &&
                    (RdfPass == Item.RdfPass) &&
                    (RdfLoadByAreas == Item.RdfLoadByAreas) &&
                    (OraFieldCode == Item.OraFieldCode) &&
                    (OraServerName == Item.OraServerName) &&
                    (OraPort == Item.OraPort) &&
                    (OraSid == Item.OraSid) &&
                    (OraLogin == Item.OraLogin) &&
                    (OraPass == Item.OraPass));
        }
        public override string GetDefaultFileHead()
        {
            return "МЕСТОРОЖДЕНИЕ;РЕГИОН;ОБЪЕДИНЕНИЕ;НГДУ КОД;НГДУ;RDF ID;RDF SERVER;RDF PORT;RDF_LOAD_AREAS;ORA ID;ORA SERVER;ORA PORT;ORA SID;ORA USER";
        }
        public override void Parse(string Source)
        {
            string [] parseStr = Source.Split(new char[] {';'});
            OilFieldName = parseStr[0];
            EnterpriseCode = System.Convert.ToInt32(parseStr[1]);
            EnterpriseName = parseStr[2];
            NGDUCode = System.Convert.ToInt32(parseStr[3]);
            NGDUName = parseStr[4];
            // RDF
            RDFFieldCode = System.Convert.ToInt32(parseStr[5]);
            RdfServerName = parseStr[6];
            RdfPort = System.Convert.ToInt32(parseStr[7]);
            RdfLogin = "guest";
            RdfPass = string.Empty;
            if (parseStr[8].Length > 0) RdfLoadByAreas = System.Convert.ToByte(parseStr[8]);

            // ORACLE
            if (parseStr[9].Length > 0) OraFieldCode = System.Convert.ToInt32(parseStr[9]);
            OraServerName = parseStr[10];
            if (parseStr[11].Length > 0) OraPort = System.Convert.ToInt32(parseStr[11]);
            OraSid = parseStr[12];
            OraLogin = parseStr[13];
            OraPass = string.Empty;
        }
        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};",
                OilFieldName,
                EnterpriseCode,
                EnterpriseName,
                NGDUCode,
                NGDUName,
                RDFFieldCode,
                RdfServerName,
                RdfPort,
                RdfLoadByAreas,
                OraFieldCode,
                OraServerName,
                OraPort,
                OraSid,
                OraLogin);
        }
    }
}
