﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus.DictionaryObjects
{
    class OilfieldAreaDictItem : DictionaryBaseItem
    {
        public int Code;
        public int OilFieldCode;
        public int Type;
        public string Name;

        public OilfieldAreaDictItem()
        {
            Code = 0;
            OilFieldCode = 0;
            Type = 0;
            Name = string.Empty;
        }
        public override string GetDefaultFileHead()
        {
            return "CODE;SHORTNAME;TYPE;OILFILED_CODE;";
        }
        public override void Parse(string Source)
        {
            string[] parseStr = Source.Split(new char[] { ';' });
            Index = Index;
            Code = System.Convert.ToInt32(parseStr[0]);
            Name = parseStr[1];
            Type = Convert.ToInt32(parseStr[2]);
            OilFieldCode = Convert.ToInt32(parseStr[3]);
        }
        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3};", Code, Name, Type, OilFieldCode);
        }
    }
}
