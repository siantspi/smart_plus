﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus.DictionaryObjects
{
    public class DictionaryBaseItem
    {
        public int Index { get; set; }

        public virtual bool TestCode(int Code) { return false; }
        public virtual string GetDefaultFileHead() { return ""; }
        public virtual void Parse(string Source) { }
        public virtual void ParseFileHead(string FileHead) { }
        public virtual bool IndexColumn(string FileHead) { return false; } // если IndexColumn == false, то Index берется из номера строки
        new public virtual string ToString() { return ""; }
        public virtual void BinaryReadItem(BinaryReader br, int Version) { }
        public virtual void BinaryWriteItem(BinaryWriter bw, int Version) { }
    }
}
