﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus.DictionaryObjects
{
    public class DataDictionaryItem : DictionaryBaseItem
    {
        public int Code;
        public string ShortName;
        public string FullName;
        public string Note;

        public DataDictionaryItem()
        {
            Code = 0;
            ShortName = string.Empty;
            FullName = string.Empty;
            Note = string.Empty;
        }
        public override string GetDefaultFileHead()
        {
            return "CODE;SHORTNAME;FULLNAME;NOTE;";
        }
        public override void Parse(string Source)
        {
            string[] parseStr = Source.Split(new char[] { ';' });
            Index = Index;
            Code = System.Convert.ToInt32(parseStr[0]);
            ShortName = parseStr[1];
            if (parseStr.Length > 2) FullName = parseStr[2];
            if (parseStr.Length > 3) Note = parseStr[3];
        }
        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3};", Code, ShortName, FullName, Note);
        }
    }
}
