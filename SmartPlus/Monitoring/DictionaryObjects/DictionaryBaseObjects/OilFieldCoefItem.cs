﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus.DictionaryObjects
{
    public class OilfieldCoefItem : DictionaryBaseItem
    {
        public int Code = 0;
        public double KfX_Xnew = 1;
        public double KfY_Xnew = 0;
        public double KfX_Ynew = 0;
        public double KfY_Ynew = 1;
        public double ShX = 0;
        public double ShY = 0;

        public OilfieldCoefItem() : base() { }
        public OilfieldCoefItem(OilfieldCoefItem Item) : this()
        {
            this.Code = Item.Code;
            this.KfX_Xnew = Item.KfX_Xnew;
            this.KfY_Xnew = Item.KfY_Xnew;
            this.KfX_Ynew = Item.KfX_Ynew;
            this.KfY_Ynew = Item.KfY_Ynew;
            this.ShX = Item.ShX;
            this.ShY = Item.ShY;
        }
        public override string GetDefaultFileHead()
        {
            return "Месторождение;KfX(Xnew);KfY(Xnew);KfX(Ynew);KfY(Ynew);ShX;ShY";
        }
        public override void Parse(string Source)
        {
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";
            string[] parseStr = Source.Split(new char[] { ';' });
            Index = Index;
            Code = System.Convert.ToInt32(parseStr[0]);
            KfX_Xnew = System.Convert.ToDouble(parseStr[1], nfi);
            KfY_Xnew = System.Convert.ToDouble(parseStr[2], nfi);
            KfX_Ynew = System.Convert.ToDouble(parseStr[3], nfi);
            KfY_Ynew = System.Convert.ToDouble(parseStr[4], nfi);
            ShX = System.Convert.ToDouble(parseStr[5], nfi);
            ShY = System.Convert.ToDouble(parseStr[6], nfi);
        }
        public override string ToString()
        {
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = " ";
            return string.Format(nfi, "{0};{1};{2};{3};{4};{5};{6};", Code, KfX_Xnew, KfY_Xnew, KfX_Ynew, KfY_Ynew, ShX, ShY);
        }

        public void GlobalCoordFromLocal(double xL, double yL, out double xG, out double yG)
        {
            xG = KfX_Xnew * xL + KfX_Ynew * yL + ShX;
            yG = KfY_Xnew * xL + KfY_Ynew * yL + ShY;
        }
        public void GlobalCoordFromLocal(double xL, double yL, out float xG, out float yG)
        {
            xG = (float)(KfX_Xnew * xL + KfX_Ynew * yL + ShX);
            yG = (float)(KfY_Xnew * xL + KfY_Ynew * yL + ShY);
        }
        public void LocalCoordFromGlobal(double xG, double yG, out double xL, out double yL) 
        {
            yL = (yG - ShY - KfY_Xnew * (xG - ShX) / KfX_Xnew) / (KfY_Ynew - KfY_Xnew * KfX_Ynew / KfX_Xnew);
            xL = (xG - ShX - KfX_Ynew * yL) / KfX_Xnew;
        }
    }
}
