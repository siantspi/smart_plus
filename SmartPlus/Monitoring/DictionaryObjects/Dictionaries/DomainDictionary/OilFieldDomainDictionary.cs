﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DataLoader.DictionaryObjects
{
    class OilFieldDomainDictionary : Dictionary<OilFieldDomainItem>
    {
        public OilFieldDomainDictionary() : base(DICTIONARY_ITEM_TYPE.OILFIELD_DOMAIN) 
        {
            VersionDict = 0;
        }
    }
}
