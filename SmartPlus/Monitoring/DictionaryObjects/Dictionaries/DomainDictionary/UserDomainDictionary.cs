﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DataLoader.DictionaryObjects
{
    class UserDomainDictionary : Dictionary<UserDomainItem>
    {
        public UserDomainDictionary() : base(DICTIONARY_ITEM_TYPE.USERS_DOMAIN) 
        {
            VersionDict = 0;
        }
    }
}
