﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus.DictionaryObjects
{
    public class DataDictionary : Dictionary<DataDictionaryItem>
    {
        public DataDictionary(DICTIONARY_ITEM_TYPE Type) : base(Type) { }

        public bool LoadFromRDF(RDF.RdfConnector Conn)
        {
            string Key = string.Empty;
            switch (this.Type)
            {
                case DICTIONARY_ITEM_TYPE.STRATUM:
                    Key = "SPR@GROUPS@SOURCES@RDF@PLAST";
                    break;
                default:
                    throw new NotSupportedException("Не задан ключ RDF справочника " + FileName);
            }
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            ms = Conn.ReadBinary(Key, false);
            
            if (ms == null)
            {
                return false;
            }
            ms.Seek(16, System.IO.SeekOrigin.Begin); //пропускаем TMID
            Int32 itemCount = RDF.ConvertEx.GetInt32(ms);
            Int32 ESize = RDF.ConvertEx.GetByte(ms);

            if (itemCount < 0)
            {
                return false;
            }

            DataDictionaryItem item;
            for (int i = 0; i < itemCount; i++)
            {
                item = new DataDictionaryItem();
                item.Code = RDF.ConvertEx.GetInt32(ms);
                item.ShortName = RDF.ConvertEx.ReadStrUI16(ms, RDF.RdfConnector.encoding);
                item.FullName = RDF.ConvertEx.ReadStrUI16(ms, RDF.RdfConnector.encoding);
                item.Note = RDF.ConvertEx.ReadStrUI16(ms, RDF.RdfConnector.encoding);
                RDF.ConvertEx.ReadStrUI16(ms, RDF.RdfConnector.encoding);   
                Add(item);
            }

            return true; 
        }
        public int GetIndexByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Code == this[i].Code)
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetIndexByShortName(string ShortName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ShortName.Equals(this[i].ShortName, StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
            }
            return -1;
        }
        public override int GetCodeByShortName(string ShortName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].ShortName.Equals(ShortName, StringComparison.OrdinalIgnoreCase))
                {
                    return this[i].Code;
                }
            }
            for (int i = 0; i < Count; i++)
            {
                if (this[i].FullName.Equals(ShortName, StringComparison.OrdinalIgnoreCase))
                {
                    return this[i].Code;
                }
            }
            return 0;
        }
        public override string GetShortNameByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Code == Code)
                {
                    return this[i].ShortName;
                }
            }
            return string.Empty;
        }
        public override string GetFullNameByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Code == Code)
                {
                    return this[i].FullName;
                }
            }
            return string.Empty;
        }
    }
}
