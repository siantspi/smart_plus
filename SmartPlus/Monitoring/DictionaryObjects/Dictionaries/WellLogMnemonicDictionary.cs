﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus.DictionaryObjects
{
    class WellLogMnemonicDictionary : DataDictionary
    {
        public WellLogMnemonicDictionary() : base(DICTIONARY_ITEM_TYPE.LOG_MNEMONIC) { }

        public int GetIndexByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Code == Code)
                {
                    return i;
                }
            }
            return -1;
        }
        public List<int> GetCodesByMask(string MnemonicMask)
        {
            List<int> codes = new List<int>();
            if (MnemonicMask.Length > 0)
            {
                char[] sep = { '|' };
                char[] del = { '*' };
                int i, j, k, pos = 0, pos2;
                string str;
                bool find = false;

                string[] parseMask = MnemonicMask.Split(sep);
                string[] parseStr = null;

                for (k = 0; k < parseMask.Length; k++)
                {
                    parseStr = parseMask[k].Split(del, StringSplitOptions.RemoveEmptyEntries);

                    if (parseStr.Length > 0)
                    {
                        for (i = 1; i < this.Count; i++)
                        {
                            str = this[i].ShortName;
                            pos = 0; pos2 = 0;
                            find = true;

                            for (j = 0; j < parseStr.Length; j++)
                            {
                                pos2 = str.IndexOf(parseStr[j], pos);

                                if ((pos2 != -1) && ((j > 0) || ((j == 0) && (pos2 == 0))))
                                {
                                    pos = pos2 + (parseStr[j]).Length;
                                }
                                else
                                {
                                    find = false;
                                    break;
                                }
                            }
                            if (find) codes.Add(this[i].Code);
                        }
                    }
                }
            }
            return codes;
        }
        public string GetBaseMask()
        {
            return "PS*.MV|SP*.MV|ПС*.МВ|GK*.MKRH|ГК*.МКРЧ|GK*.IMIN|ГК*.ИМИН|IK*.MSMM|ИК*.МСММ|IK*.OMM|ИК*.ОММ|BK*.OMM|БК*.ОММ|BK*.BR|БК*.БР|NGK*.IMIN|НГК*.ИМИН|НК*.ИМИН|NGK*.UE|НГК*.УЕ|НК*.УЕ";
        }
    }
}
