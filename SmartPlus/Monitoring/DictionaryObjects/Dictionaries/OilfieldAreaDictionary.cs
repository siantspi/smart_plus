﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus.DictionaryObjects
{
    class OilFieldAreaDictionary : Dictionary<OilfieldAreaDictItem>
    {
        public OilFieldAreaDictionary() : base(DICTIONARY_ITEM_TYPE.OILFIELD_AREA) { }
        
        public int GetIndexByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Code == this[i].Code)
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetIndexByShortName(string ShortName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ShortName.Equals(this[i].Name, StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
            }
            return -1;
        }
        public override int GetCodeByShortName(string ShortName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Name.Equals(ShortName, StringComparison.OrdinalIgnoreCase))
                {
                    return this[i].Code;
                }
            }
            return 0;
        }
        public override string GetShortNameByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Code == Code)
                {
                    return this[i].Name;
                }
            }
            return string.Empty;
        }
        public override string GetFullNameByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Code == Code)
                {
                    return this[i].Name;
                }
            }
            return string.Empty;
        }
    }
}
