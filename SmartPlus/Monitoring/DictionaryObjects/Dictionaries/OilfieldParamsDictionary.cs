﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartPlus.DictionaryObjects
{
    public class OilFieldParamsDictionary : Dictionary<OilFieldParamsItem>
    {
        public OilFieldParamsDictionary() : base(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS) { }

        public int GetIndexByCode(int oilfieldCode)
        {
            for (int i = 0; i < Count; i++)
            {
                if (oilfieldCode == this[i].RDFFieldCode)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
