﻿using System;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace SmartPlus.DictionaryObjects
{
    public class DictionaryCollection
    {
        public string DictionaryPath;
        public string DefaultPath;
        ArrayList Items;

        public DictionaryCollection(string Path, string DefaultPath)
        {
            DictionaryPath = Path;
            this.DefaultPath = DefaultPath;
            Items = new ArrayList();
            AddDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT);
            AddDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS);
            AddDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            AddDictionary(DICTIONARY_ITEM_TYPE.PERF_TYPE);
            AddDictionary(DICTIONARY_ITEM_TYPE.CHARWORK);
            AddDictionary(DICTIONARY_ITEM_TYPE.STATE);
            AddDictionary(DICTIONARY_ITEM_TYPE.METHOD);
            AddDictionary(DICTIONARY_ITEM_TYPE.SATURATION);
            AddDictionary(DICTIONARY_ITEM_TYPE.CONSTRUCTION);
            AddDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_AREA);
            AddDictionary(DICTIONARY_ITEM_TYPE.GRID_TYPE);
            AddDictionary(DICTIONARY_ITEM_TYPE.LITOLOGY);
            AddDictionary(DICTIONARY_ITEM_TYPE.LOG_UNIT);
            AddDictionary(DICTIONARY_ITEM_TYPE.LOG_MNEMONIC);
            AddDictionary(DICTIONARY_ITEM_TYPE.GTM_TYPE);
            AddDictionary(DICTIONARY_ITEM_TYPE.WELL_ACTION_TYPE);
            AddDictionary(DICTIONARY_ITEM_TYPE.WELL_RESEARCH_TYPE);
            AddDictionary(DICTIONARY_ITEM_TYPE.STAY_REASON);
        }

        void AddDictionary(DICTIONARY_ITEM_TYPE Type)
        {
            switch (Type)
            {
                case DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT:
                    Items.Add(new OilFieldCoefDictionary());
                    break;
                case DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS:
                    Items.Add(new OilFieldParamsDictionary());
                    break;
                case DICTIONARY_ITEM_TYPE.STRATUM:
                    Items.Add(new StratumDictionary());
                    break;
                case DICTIONARY_ITEM_TYPE.PERF_TYPE:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.CHARWORK:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.STATE:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.METHOD:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.SATURATION:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.CONSTRUCTION:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.OILFIELD_AREA:
                    Items.Add(new OilFieldAreaDictionary());
                    break;
                case DICTIONARY_ITEM_TYPE.GRID_TYPE:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.LITOLOGY:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.LOG_UNIT:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.LOG_MNEMONIC:
                    Items.Add(new WellLogMnemonicDictionary());
                    break;
                case DICTIONARY_ITEM_TYPE.GTM_TYPE:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.GTA_TYPE:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.WELL_ACTION_TYPE:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.WELL_RESEARCH_TYPE:
                    Items.Add(new DataDictionary(Type));
                    break;
                case DICTIONARY_ITEM_TYPE.STAY_REASON:
                    Items.Add(new DataDictionary(Type));
                    break;
                default:
                    throw new ArgumentException(string.Format("Не поддерживаемый тип справочника [{0}]", Type));
            }
        }

        public object GetDictionary(DICTIONARY_ITEM_TYPE Type)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (((DictionaryBase)Items[i]).Type == Type)
                {
                    return Items[i];
                }
            }
            return null;
        }
        public bool LoadDataFromFiles()
        {
            int i = 0;
            bool result = true;
            while (i < Items.Count)
            {
                if (!((DictionaryBase)Items[i]).ReadFromFile(DictionaryPath))
                {
                    if (!((DictionaryBase)Items[i]).ReadFromFile(DefaultPath))
                    {
                        MessageBox.Show("Не удалось загрузить справочник " + ((DictionaryBase)Items[i]).FileName, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Items.RemoveAt(i);
                        result = false;
                        if (((DictionaryBase)Items[i]).Type == DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS) break;
                        i--;
                    }
                }
                i++;
            }
            return result;
        }
        public bool WriteToFile(DICTIONARY_ITEM_TYPE Type)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (((DictionaryBase)Items[i]).Type == Type)
                {
                    return ((DictionaryBase)Items[i]).WriteToFile(DictionaryPath);
                }
            }
            return false;
        }
    }
}
