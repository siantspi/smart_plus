﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus.DictionaryObjects
{
    public partial class StratumConversionForm : Form
    {
        Dictionary<DataDictionaryItem> Dict;
        BackgroundWorker worker;
        List<int> findStratumList;
        public int ConversionID;
        bool exit;
        string[] ConversionStartSymbols = new string[] { "С", "C", "СТ", "CT", "СT", "CT", "CТ", "CT", "Д", "D", "Р", "P" };

        public StratumConversionForm(Dictionary<DataDictionaryItem> Dictionary)
        {
            InitializeComponent();
            Dict = Dictionary;
            findStratumList = new List<int>();
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            tbFindName.TextChanged += new EventHandler(tbFindName_TextChanged);
            tbFindName.KeyDown += new KeyEventHandler(tbFindName_KeyDown);
            FormClosing += new FormClosingEventHandler(StratumConversionForm_FormClosing);
        }

        void StratumConversionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            tbFindName.Text = "";
        }

        void tbFindName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Up)
            {
                if (gridStratumList.SelectedRows.Count > 0)
                {
                    if (gridStratumList.SelectedRows[0].Index > 0) gridStratumList.Rows[gridStratumList.SelectedRows[0].Index - 1].Selected = true;
                }
            }
            else if (e.KeyData == Keys.Down)
            {
                if (gridStratumList.SelectedRows.Count > 0)
                {
                    if (gridStratumList.SelectedRows[0].Index < gridStratumList.Rows.Count - 1)
                    {
                        gridStratumList.Rows[gridStratumList.SelectedRows[0].Index + 1].Selected = true;
                    }
                }
            }
            else if (e.KeyData == Keys.Enter)
            {
                bOk.PerformClick();
            }
        }

        void tbFindName_TextChanged(object sender, EventArgs e)
        {
            FillGrid();
        }
        public DialogResult ShowDialog(string ServerName, string OilFieldName, string WellName, string DataType, int StratumCode, string StratumName)
        {
            return ShowDialog(null, ServerName, OilFieldName, WellName, DataType, StratumCode, StratumName);
        }
        public DialogResult ShowDialog(Form Owner, string ServerName, string OilFieldName, string WellName, string DataType, int StratumCode, string StratumName)
        {
            if (Owner != null) this.Owner = Owner;
            this.StartPosition = FormStartPosition.CenterParent;
            exit = false;
            tbServer.Text = ServerName;
            tbStratumCode.Text = StratumCode.ToString();
            tbOilField.Text = OilFieldName;
            tbWellName.Text = WellName;
            tbStratumName.Text = StratumName;
            tbDataType.Text = DataType;
            tbFindName.Text = "";
            FillGrid();
            ConversionID = -1;
            return ShowDialog();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            findStratumList.Clear();
            if (Dict != null)
            {
                string findName = (string)e.Argument;
                if (findName == "")
                {
                    for (int i = 0; i < Dict.Count; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            findStratumList.Clear();
                            return;
                        }
                        findStratumList.Add(i);
                    }
                }
                else
                {
                    string str1, str2;
                    for (int i = 0; i < Dict.Count; i++)
                    {
                        if (Dict[i].ShortName.StartsWith(findName, StringComparison.OrdinalIgnoreCase))
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                findStratumList.Clear();
                                return;
                            }
                            findStratumList.Add(i);
                        }
                        else
                        {
                            for (int j = 0; j < ConversionStartSymbols.Length - 1; j += 2)
                            {
                                str1 = ConversionStartSymbols[j];
                                str2 = ConversionStartSymbols[j + 1];

                                if (findName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                                {
                                    str1 = str2 + findName.Remove(0, str1.Length);
                                    if (Dict[i].ShortName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                                    {
                                        if (worker.CancellationPending)
                                        {
                                            e.Cancel = true;
                                            findStratumList.Clear();
                                            return;
                                        }
                                        findStratumList.Add(i);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!exit)
            {
                if (e.Cancelled)
                {
                    worker.RunWorkerAsync(tbFindName.Text);
                }
                else
                {
                    gridStratumList.Rows.Clear();
                    if (findStratumList.Count > 0)
                    {
                        DataGridViewRowCollection rows = gridStratumList.Rows;
                        rows.Add(findStratumList.Count);

                        for (int i = 0; i < findStratumList.Count; i++)
                        {
                            rows[i].Cells[0].Value = Dict[findStratumList[i]].Code.ToString();
                            rows[i].Cells[1].Value = Dict[findStratumList[i]].ShortName;
                        }
                    }
                    tbFindName.Focus();
                }
            }
            exit = false;
        }

        void FillGrid()
        {
            if (worker.IsBusy)
            {
                worker.CancelAsync();
            }
            else
            {
                worker.RunWorkerAsync(tbFindName.Text);
            }
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            if (gridStratumList.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите код из справочника!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                exit = true;
                worker.CancelAsync();
                ConversionID = Convert.ToInt32(gridStratumList.Rows[gridStratumList.SelectedRows[0].Index].Cells[0].Value);
                DialogResult = DialogResult.OK;
                Close();
            }
        }
        private void bCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Для пласта будет установлена кодировка 0 [Н/Д]", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                exit = true;
                worker.CancelAsync();
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }
        private void bNewStratum_Click(object sender, EventArgs e)
        {
            if (tbFindName.Text != "")
            {
                string str1 = "", str2 = "", str = tbFindName.Text;
                bool find = false;
                int i = 0, j = 0;
                for (i = 0; i < ConversionStartSymbols.Length - 1; i += 2)
                {
                    str1 = ConversionStartSymbols[i];
                    str2 = ConversionStartSymbols[i + 1];

                    if (tbFindName.Text.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                    {
                        str = str2 + tbFindName.Text.Remove(0, str1.Length);
                    }
                }
                find = false;
                for (j = 0; j < Dict.Count; j++)
                {
                    if (Dict[j].ShortName == str)
                    {
                        find = true;
                        break;
                    }
                }
                if ((find) && (j < Dict.Count))
                {
                    str1 = "Пласт '" + str + "' уже присутсвует в справочнике!";
                    MessageBox.Show(str1, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (str == "") str = tbFindName.Text;
                    int maxCode = 0;
                    for (i = 0; i < Dict.Count; i++)
                    {
                        if (maxCode < Dict[i].Code) maxCode = Dict[i].Code;
                    }
                    maxCode++;
                    str1 = "Добавить пласт '" + str + "' с кодом " + maxCode.ToString() + " в справочник?";
                    if (MessageBox.Show(str1, "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        DataDictionaryItem item = new DataDictionaryItem();
                        item.Code = maxCode;
                        item.ShortName = str;
                        item.FullName = string.Empty;
                        item.Note = string.Empty;
                        Dict.Add(item);
                        FillGrid();
                    }
                }
            }
            else
            {
                string str1 = "Введите название нового пласта!";
                MessageBox.Show(str1, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void gridStratumList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bOk.PerformClick();
        }
    }
}

