﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus.DictionaryObjects
{
    public class StratumTreeNode
    {
        public int StratumCode;
        public string Name;
        public int ARGB;
        public List<StratumTreeNode> Parents;
        public List<StratumTreeNode> Childs;
        public StratumTreeNode()
        {
            StratumCode = -1;
            ARGB = -1;
            Name = "";
            Parents = null;
            Childs = null;
        }

        public int GetParentLevelByCode(int StratumCode)
        {
            return GetParentLevelByCode(StratumCode, 0);
        }
        int GetParentLevelByCode(int StratumCode, int Level)
        {
            if (Parents != null)
            {
                for (int i = 0; i < Parents.Count; i++)
                {
                    if (Parents[i].StratumCode == StratumCode)
                    {
                        return Level;
                    }
                }
                int res = -1;
                for (int i = 0; i < Parents.Count; i++)
                {
                    res = Parents[i].GetParentLevelByCode(StratumCode, Level + 1);
                    if (res != -1) return res;
                }
            }
            return -1;
        }
        public int GetParentStratumColor()
        {
            if (Parents != null)
            {
                for (int i = 0; i < Parents.Count; i++)
                {
                    if (Parents[i].ARGB != -1) return Parents[i].ARGB;
                }
                int res;
                for (int i = 0; i < Parents.Count; i++)
                {
                    res = Parents[i].GetParentStratumColor();
                    if (res != -1) return res;
                }
            }
            return -1;
        }
    }

    public class StratumTree
    {
        Dictionary<DataDictionaryItem> Dict;
        List<StratumTreeNode> Nodes;
        public int Count { get { return Nodes.Count; } }
        public StratumTreeNode this[int index]
        {
            get { return Nodes[index]; }
        }
        public StratumTree(Dictionary<DataDictionaryItem> LocalDict)
        {
            Dict = LocalDict;
            Nodes = new List<StratumTreeNode>();
        }

        public StratumTreeNode GetNodeByCode(int StratumCode)
        {
            for (int i = 0; i < Nodes.Count; i++)
            {
                if (Nodes[i].StratumCode == StratumCode)
                {
                    return Nodes[i];
                }
            }
            return null;
        }
        public void AddNode(int StratumCode)
        {
            StratumTreeNode node = GetNodeByCode(StratumCode);
            if (node == null)
            {
                node = new StratumTreeNode();
                node.StratumCode = StratumCode;
                Nodes.Add(node);
            }
        }
        public void LoadFromDictionary()
        {
            if (Count == 0)
            {
                StratumTreeNode node;
                for (int i = 0; i < Dict.Count; i++)
                {
                    node = new StratumTreeNode();
                    node.StratumCode = Dict[i].Code;
                    node.Name = Dict[i].ShortName;
                    Nodes.Add(node);
                }
            }
        }
        public void LoadFromFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                string[] lines = File.ReadAllLines(fileName, Encoding.GetEncoding(1251));
                if ((lines.Length > 0) && (lines[0].Length > 0))
                {
                    char[] separator = new char[] { ';' };
                    string[] parse, parse2;
                    for (int i = 0; i < lines.Length; i++)
                    {
                        parse = lines[i].Split(separator);
                        StratumTreeNode node = new StratumTreeNode();
                        try
                        {
                            node.StratumCode = Convert.ToInt32(parse[0]);
                        }
                        catch { }
                        node.Name = Dict.GetShortNameByCode(node.StratumCode);
                        Nodes.Add(node);
                    }
                    char[] separator2 = new char[] { ',' };
                    for (int i = 0; i < lines.Length; i++)
                    {
                        parse = lines[i].Split(separator);
                        if (parse[1].Length > 0)
                        {
                            parse2 = parse[1].Split(separator2);
                            Nodes[i].Parents = new List<StratumTreeNode>();
                            for (int j = 0; j < parse2.Length; j++)
                            {
                                try
                                {
                                    Nodes[i].Parents.Add(Nodes[Convert.ToInt32(parse2[j])]);
                                }
                                catch { }
                            }
                        }
                        if (parse[2].Length > 0)
                        {
                            parse2 = parse[2].Split(separator2);
                            Nodes[i].Childs = new List<StratumTreeNode>();
                            for (int j = 0; j < parse2.Length; j++)
                            {
                                try
                                {
                                    Nodes[i].Childs.Add(Nodes[Convert.ToInt32(parse2[j])]);
                                }
                                catch { }
                            }
                        }
                    }
                }
            }
            if (Count == 0) LoadFromDictionary();
        }
        public void WriteToFile(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs, Encoding.GetEncoding(1251));

            if (Nodes.Count > 0)
            {
                int i, j;
                string str, str1, str2;
                for (i = 0; i < Nodes.Count; i++)
                {
                    str1 = "";
                    if (Nodes[i].Parents != null)
                    {
                        for (j = 0; j < Nodes[i].Parents.Count; j++)
                        {
                            if (j > 0) str1 += ",";
                            str1 += Nodes[i].Parents[j].StratumCode.ToString();

                        }
                    }
                    str2 = "";
                    if (Nodes[i].Childs != null)
                    {
                        for (j = 0; j < Nodes[i].Childs.Count; j++)
                        {
                            if (j > 0) str2 += ",";
                            str2 += Nodes[i].Childs[j].StratumCode.ToString();
                        }
                    }
                    str = string.Format("{0};{1};{2};", Nodes[i].StratumCode, str1, str2);
                    sw.WriteLine(str);
                }
            }
            sw.Close();
        }
    }
}
