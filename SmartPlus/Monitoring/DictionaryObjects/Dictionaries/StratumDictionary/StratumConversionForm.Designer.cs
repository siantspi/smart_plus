﻿namespace SmartPlus.DictionaryObjects
{
    partial class StratumConversionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            tbFindName = new System.Windows.Forms.TextBox();
            gridStratumList = new System.Windows.Forms.DataGridView();
            StratumID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            StratumName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            bOk = new System.Windows.Forms.Button();
            bCancel = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            tbStratumName = new System.Windows.Forms.TextBox();
            label4 = new System.Windows.Forms.Label();
            tbStratumCode = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            tbServer = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            bNewStratum = new System.Windows.Forms.Button();
            label6 = new System.Windows.Forms.Label();
            tbOilField = new System.Windows.Forms.TextBox();
            tbWellName = new System.Windows.Forms.TextBox();
            label7 = new System.Windows.Forms.Label();
            tbDataType = new System.Windows.Forms.TextBox();
            label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(gridStratumList)).BeginInit();
            SuspendLayout();
            // 
            // tbFindName
            // 
            tbFindName.Location = new System.Drawing.Point(4, 236);
            tbFindName.Name = "tbFindName";
            tbFindName.Size = new System.Drawing.Size(291, 23);
            tbFindName.TabIndex = 3;
            // 
            // gridPlasts
            // 
            gridStratumList.AllowUserToAddRows = false;
            gridStratumList.AllowUserToDeleteRows = false;
            gridStratumList.AllowUserToResizeRows = false;
            gridStratumList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            gridStratumList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            gridStratumList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            gridStratumList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            StratumID,
            StratumName});
            gridStratumList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            gridStratumList.Location = new System.Drawing.Point(4, 270);
            gridStratumList.MultiSelect = false;
            gridStratumList.Name = "gridPlasts";
            gridStratumList.RowHeadersVisible = false;
            gridStratumList.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            gridStratumList.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            gridStratumList.RowTemplate.Height = 18;
            gridStratumList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            gridStratumList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            gridStratumList.ShowEditingIcon = false;
            gridStratumList.Size = new System.Drawing.Size(387, 389);
            gridStratumList.TabIndex = 9;
            gridStratumList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(gridStratumList_CellMouseDoubleClick);
            // 
            // PlastID
            // 
            StratumID.HeaderText = "Код";
            StratumID.Name = "PlastID";
            StratumID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            StratumID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PlastName
            // 
            StratumName.HeaderText = "Пласт";
            StratumName.Name = "PlastName";
            StratumName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            StratumName.Width = 270;
            // 
            // bOk
            // 
            bOk.Location = new System.Drawing.Point(4, 665);
            bOk.Name = "bOk";
            bOk.Size = new System.Drawing.Size(90, 30);
            bOk.TabIndex = 10;
            bOk.Text = "OK";
            bOk.UseVisualStyleBackColor = true;
            bOk.Click += new System.EventHandler(bOk_Click);
            // 
            // bCancel
            // 
            bCancel.Location = new System.Drawing.Point(301, 665);
            bCancel.Name = "bCancel";
            bCancel.Size = new System.Drawing.Size(90, 30);
            bCancel.TabIndex = 11;
            bCancel.Text = "Отмена";
            bCancel.UseVisualStyleBackColor = true;
            bCancel.Click += new System.EventHandler(bCancel_Click);
            // 
            // label1
            // 
            label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            label1.Location = new System.Drawing.Point(4, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(387, 39);
            label1.TabIndex = 0;
            label1.Text = "Для текущего кода пласта на сервере выберите \r\nнаиболее подходящий из справочника" +
                "";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbPlastName
            // 
            tbStratumName.Location = new System.Drawing.Point(188, 182);
            tbStratumName.Name = "tbPlastName";
            tbStratumName.ReadOnly = true;
            tbStratumName.Size = new System.Drawing.Size(203, 23);
            tbStratumName.TabIndex = 20;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(139, 185);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(43, 15);
            label4.TabIndex = 19;
            label4.Text = "Пласт:";
            // 
            // tbPlastCode
            // 
            tbStratumCode.Location = new System.Drawing.Point(56, 182);
            tbStratumCode.Name = "tbPlastCode";
            tbStratumCode.ReadOnly = true;
            tbStratumCode.Size = new System.Drawing.Size(77, 23);
            tbStratumCode.TabIndex = 18;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(1, 185);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(31, 15);
            label3.TabIndex = 17;
            label3.Text = "Код:";
            // 
            // tbServer
            // 
            tbServer.Location = new System.Drawing.Point(56, 60);
            tbServer.Name = "tbServer";
            tbServer.ReadOnly = true;
            tbServer.Size = new System.Drawing.Size(335, 23);
            tbServer.TabIndex = 16;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(1, 63);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(49, 15);
            label2.TabIndex = 15;
            label2.Text = "Сервер:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(1, 218);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(254, 15);
            label5.TabIndex = 21;
            label5.Text = "Начните ввод для фильтрации справочника:";
            // 
            // bNewPlast
            // 
            bNewStratum.Location = new System.Drawing.Point(301, 231);
            bNewStratum.Name = "bNewPlast";
            bNewStratum.Size = new System.Drawing.Size(90, 30);
            bNewStratum.TabIndex = 22;
            bNewStratum.Text = "Добавить";
            bNewStratum.UseVisualStyleBackColor = true;
            bNewStratum.Click += new System.EventHandler(bNewStratum_Click);
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(1, 93);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(38, 15);
            label6.TabIndex = 23;
            label6.Text = "Мест:";
            // 
            // tbOilField
            // 
            tbOilField.Location = new System.Drawing.Point(56, 90);
            tbOilField.Name = "tbOilField";
            tbOilField.ReadOnly = true;
            tbOilField.Size = new System.Drawing.Size(335, 23);
            tbOilField.TabIndex = 24;
            // 
            // tbWellName
            // 
            tbWellName.Location = new System.Drawing.Point(73, 119);
            tbWellName.Name = "tbWellName";
            tbWellName.ReadOnly = true;
            tbWellName.Size = new System.Drawing.Size(318, 23);
            tbWellName.TabIndex = 26;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(1, 122);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(66, 15);
            label7.TabIndex = 25;
            label7.Text = "Скважина:";
            // 
            // tbDataType
            // 
            tbDataType.Location = new System.Drawing.Point(82, 153);
            tbDataType.Name = "tbDataType";
            tbDataType.ReadOnly = true;
            tbDataType.Size = new System.Drawing.Size(309, 23);
            tbDataType.TabIndex = 28;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(1, 156);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(75, 15);
            label8.TabIndex = 27;
            label8.Text = "Тип данных:";
            // 
            // PlastConversionForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(398, 702);
            ControlBox = false;
            Controls.Add(tbDataType);
            Controls.Add(label8);
            Controls.Add(tbWellName);
            Controls.Add(label7);
            Controls.Add(tbOilField);
            Controls.Add(label6);
            Controls.Add(bNewStratum);
            Controls.Add(label5);
            Controls.Add(tbStratumName);
            Controls.Add(label4);
            Controls.Add(tbStratumCode);
            Controls.Add(label3);
            Controls.Add(tbServer);
            Controls.Add(label2);
            Controls.Add(bCancel);
            Controls.Add(bOk);
            Controls.Add(gridStratumList);
            Controls.Add(tbFindName);
            Controls.Add(label1);
            Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "PlastConversionForm";
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "Выберите подходящий код из справочника";
            ((System.ComponentModel.ISupportInitialize)(gridStratumList)).EndInit();
            ResumeLayout(false);
            PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFindName;
        private System.Windows.Forms.DataGridView gridStratumList;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbStratumName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbStratumCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bNewStratum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbOilField;
        private System.Windows.Forms.TextBox tbWellName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn StratumID;
        private System.Windows.Forms.DataGridViewTextBoxColumn StratumName;
        private System.Windows.Forms.TextBox tbDataType;
        private System.Windows.Forms.Label label8;
    }
}