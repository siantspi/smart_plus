﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;

namespace SmartPlus.DictionaryObjects
{
    public enum SERVER_DATA_TYPE : int
    {
        WELLBORE,
        COORDINATES,
        CONTOUR,
        AREA,
        WELLPAD,
        MER,
        MER_NEW,
        GIS,
        PERFORATION,
        LOGS,
        PVT,
        GRID,
        CHESS,
        GTM,
        WELL_REPAIR_ACTION,
        WELL_RESEARCH_BY_ORACLE
    }
    public struct ConversionMessage
    {
        public int Type;
        public int ServerCode;
        public int LocalCode;
        public string ServerStratumName;
        public string LocalStratumName;
        public string ServerName;
        public string WellName;
    }
    public sealed class StratumDictionary : DataDictionary
    {
        internal struct StratumColor
        {
            public int StratumCode;
            public int ARGB;
        }
        public StratumTree stratumTree;
        List<ServerStratumConverter> StratumConvList;
        StratumColor[] StratumColors;
        string Path;
        int StartedDictCount;
        int UserFormCounter;
        public bool ConversionCanceled { set; get; }
        public bool ConversionFormShowed { get { return (UserFormCounter > 0); } }

        public StratumDictionary() : base(DICTIONARY_ITEM_TYPE.STRATUM)
        {
            StratumColors = null;
            StratumConvList = new List<ServerStratumConverter>();
            stratumTree = new StratumTree(this);
            UserFormCounter = 0;
        }

        #region LOAD SAVE
        public override bool ReadFromFile(string Path)
        {
            this.Path = Path + "\\Stratum";
            bool res = base.ReadFromFile(this.Path);
            if (res)
            {
                LoadStratumTree();
                LoadStratumColors();
            }
            StartedDictCount = this.Count;
            return res;
        }
        void LoadStratumTree()
        {
            stratumTree.LoadFromFile(this.Path + "\\StratumTree.csv");
        }
        void SaveStratumTree()
        {
            stratumTree.WriteToFile(this.Path + "\\StratumTree.csv");
        }
        bool LoadConversionList()
        {
            bool res = false;
            if (Directory.Exists(Path))
            {
                DirectoryInfo dir = new DirectoryInfo(Path);
                FileInfo[] files = dir.GetFiles("*.bin");
                char[] separator = new char[] { '_' };
                string[] parseStr;
                ServerTypeID ServerType;
                string ServerName, OraSid;
                int Port;
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Name != "colors.bin")
                    {
                        parseStr = files[i].Name.Split(separator);
                        if (parseStr.Length > 3)
                        {
                            try
                            {
                                ServerType = (ServerTypeID)Convert.ToByte(parseStr[0]);
                                ServerName = parseStr[1];
                                OraSid = parseStr[2];
                                Port = Convert.ToInt32(parseStr[3].Remove(parseStr[3].Length - 4, 4));
                                ServerStratumConverter StratumConverter = new ServerStratumConverter(ServerType, ServerName, Port, Path, this, OraSid);
                                StratumConverter.LoadFromFile();
                                StratumConvList.Add(StratumConverter);
                            }
                            catch
                            {

                            }
                        }
                    }
                }
            }
            return res;
        }
        bool LoadStratumColors()
        {
            if ((Path != "") && File.Exists(Path + "\\colors.bin"))
            {
                FileStream fs = new FileStream(Path + "\\colors.bin", FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                StratumTreeNode node = null;
                int count = br.ReadInt32();
                if (count > 0)
                {
                    StratumColors = new StratumColor[count];
                    for (int i = 0; i < count; i++)
                    {
                        StratumColors[i].StratumCode = br.ReadInt32();
                        StratumColors[i].ARGB = br.ReadInt32();
                        node = GetStratumTreeNode(StratumColors[i].StratumCode);
                        if (node != null) node.ARGB = StratumColors[i].ARGB;
                    }
                }
            }
            return false;
        }
        public void SaveStratumColors()
        {
            if ((Path != "") && (Directory.Exists(Path)) && (StratumColors.Length > 0))
            {
                FileStream fs = new FileStream(Path + "\\colors.bin", FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(StratumColors.Length);
                for (int i = 0; i < StratumColors.Length; i++)
                {
                    bw.Write(StratumColors[i].StratumCode);
                    bw.Write(StratumColors[i].ARGB);
                }
                bw.Close();
            }
        }
        public void SaveConversionList()
        {
            for (int i = 0; i < StratumConvList.Count; i++)
            {
                if (StratumConvList[i].ConversionItemAdded) StratumConvList[i].SaveToFile();
            }
        }
        public void TestDictionaryUpdated()
        {
            string str;
            if (StartedDictCount < this.Count)
            {
                str = "Обновлен справочник пластов сохранить его?";
                if (MessageBox.Show(str, "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    // добавляем в stratumTree новые пласты
                    for (int i = StartedDictCount; i < this.Count; i++)
                    {
                        stratumTree.AddNode(this[i].Code);
                    }
                    SaveStratumTree();
                    WriteToFile(Path);
                }
            }
            str = "Сохранить установленные перекодировки для серверов?";
            if (MessageBox.Show(str, "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                SaveConversionList();
            }
        }
        #endregion

        #region Stratum TREE
        public StratumTreeNode GetStratumTreeNode(int StratumCode)
        {
            if ((stratumTree != null) && (stratumTree.Count > 0))
            {
                return stratumTree.GetNodeByCode(StratumCode);
            }
            return new StratumTreeNode();
        }
        public bool EqualsStratum(int stratumCode1, int stratumCode2)
        {
            StratumTreeNode plNode1, plNode2;
            if (stratumCode1 == stratumCode2)
            {
                return true;
            }
            else
            {
                plNode1 = GetStratumTreeNode(stratumCode1);
                plNode2 = GetStratumTreeNode(stratumCode2);
                if ((plNode1.GetParentLevelByCode(stratumCode2) != -1) || (plNode2.GetParentLevelByCode(stratumCode1) != -1))
                {
                    return true;
                }
            }
            return false;
        }
        public bool EqualsStratum(StratumTreeNode stratumNode1, StratumTreeNode stratumNode2)
        {
            if (stratumNode1.StratumCode == stratumNode2.StratumCode)
            {
                return true;
            }
            else
            {
                if ((stratumNode1.GetParentLevelByCode(stratumNode2.StratumCode) != -1) || (stratumNode2.GetParentLevelByCode(stratumNode1.StratumCode) != -1))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        public override int GetCodeByShortName(string ShortName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].ShortName.Equals(ShortName, StringComparison.OrdinalIgnoreCase))
                {
                    return this[i].Code;
                }
            }
            return 0;
        }
        public override string GetShortNameByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Code == Code)
                {
                    return this[i].ShortName;
                }
            }
            return string.Empty;
        }
        public override string GetFullNameByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Code == Code)
                {
                    return this[i].FullName;
                }
            }
            return string.Empty;
        }
        public int GetStratumColor(int StratumCode)
        {
            if (StratumColors != null)
            {
                for (int i = 0; i < StratumColors.Length; i++)
                {
                    if (StratumCode == StratumColors[i].StratumCode)
                    {
                        return StratumColors[i].ARGB;
                    }
                }
            }
            return 0;
        }
        public int GetStratumColorByStratumTree(int StratumCode)
        {
            if (stratumTree != null)
            {
                StratumTreeNode node = GetStratumTreeNode(StratumCode);
                if (node != null)
                {
                    if (node.ARGB != -1) return node.ARGB;
                    else return node.GetParentStratumColor();
                }
            }
            return -1;
        }

        #region Converting Codes
        public int ShowConversionForm(string ServerName, string OilFieldName, string WellName, string DataType, int PlastCode, string PlastName)
        {
            int res = -1;
            using (StratumConversionForm ConversionForm = new StratumConversionForm(this))
            {
                UserFormCounter++;
                if (ConversionForm.ShowDialog(ServerName, OilFieldName, WellName, DataType, PlastCode, PlastName) == DialogResult.OK)
                {
                    res = ConversionForm.ConversionID;
                }
                else
                {
                    res = -1;
                }
            }
            UserFormCounter--;
            return res;
        }
        int GetLocalCodeByServerCode(bool IsStratumGeoZone, int StratumConverterIndex, int ServerStratumCode)
        {
            return GetLocalCodeByServerCode(IsStratumGeoZone, StratumConverterIndex, string.Empty, string.Empty, string.Empty, ServerStratumCode);
        }
        int GetLocalCodeByServerCode(bool IsStratumGeoZone, int StratumConverterIndex, string OilFieldName, string WellName, string DataType, int ServerStratumCode)
        {
            int code = -1;
            if ((StratumConverterIndex != -1) && (StratumConverterIndex < StratumConvList.Count))
            {
                ServerStratumConverter conv = StratumConvList[StratumConverterIndex];
                code = conv.GetLocalID(ServerStratumCode, IsStratumGeoZone);
                if (code == -1)
                {
                    string ServerStratumName = string.Empty, LocalPlastName = string.Empty;
                    code = conv.GetNearLocalID(ServerStratumCode, IsStratumGeoZone);
                    if (code == -1)
                    {
                        ServerStratumName = conv.GetServerStratumName(ServerStratumCode, IsStratumGeoZone);
                        if (ServerStratumName.Length == 0)
                        {
                            code = 0;
                        }
                        else
                        {
                            code = ShowConversionForm(conv.ServerName, OilFieldName, WellName, DataType, ServerStratumCode, ServerStratumName);
                        }
                    }
                    int ind = this.GetIndexByCode(code);
                    if (ind != -1)
                    {
                        LocalPlastName = this[ind].ShortName;
                        conv.AddConversion(IsStratumGeoZone, ServerStratumCode, ServerStratumName, code, LocalPlastName);
                    }
                }
            }
            return code;
        }
        public int TryGetLocalCodeByServerPlastName(string ServerStratumName)
        {
            int code = -1, ind;
            if ((ServerStratumName != null) && (ServerStratumName.Length > 0))
            {
                string[] ConversionStartSymbols = new string[] { "С", "C", "СТ", "CT", "СT", "CT", "CТ", "CT", "Д", "D", "Р", "P" };
                string[] ConversionOtherSymbols = new string[] { "_", ".", "+", "-" };

                ind = GetIndexByShortName(ServerStratumName);
                if (ind != -1)
                {
                    code = this[ind].Code;
                }
                else
                {
                    int i;
                    string str1, str2, tempName;
                    for (i = 0; i < ConversionOtherSymbols.Length - 1; i += 2)
                    {
                        str1 = ConversionOtherSymbols[i];
                        str2 = ConversionOtherSymbols[i + 1];
                        ServerStratumName = ServerStratumName.Replace(str1, str2);
                    }
                    ind = GetIndexByShortName(ServerStratumName);
                    if (ind != -1)
                    {
                        code = this[ind].Code;
                    }
                    else
                    {
                        for (i = 0; i < ConversionStartSymbols.Length - 1; i += 2)
                        {
                            str1 = ConversionStartSymbols[i];
                            str2 = ConversionStartSymbols[i + 1];

                            if (ServerStratumName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                            {
                                tempName = str2 + ServerStratumName.Remove(0, str1.Length);
                                ind = GetIndexByShortName(tempName);
                                if (ind != -1)
                                {
                                    code = this[ind].Code;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return code;
        }
        int GetPlastConverterIndex(ServerTypeID ServerType, string ServerName, int Port, string OracleSid)
        {
            if (StratumConvList.Count == 0) LoadConversionList();

            int res = -1;
            for (int i = 0; i < StratumConvList.Count; i++)
            {
                if ((StratumConvList[i].ServerType == ServerType) &&
                    (StratumConvList[i].ServerPort == Port) &&
                    (StratumConvList[i].ServerName == ServerName))
                {
                    res = i;
                }
            }
            if (res == -1)
            {
                res = StratumConvList.Count;
                StratumConvList.Add(new ServerStratumConverter(ServerType, ServerName, Port, Path, this, OracleSid));
            }
            return res;
        }
        public int ConvertPlastCodes(ServerTypeID ServerType, SERVER_DATA_TYPE DataType, OilField of)
        {
            int res = 0;
            int ServerIndex = -1;
            switch (ServerType)
            {
                case ServerTypeID.RDF:
                    ServerIndex = GetPlastConverterIndex(ServerType, of.ParamsDict.RdfServerName, of.ParamsDict.RdfPort, "");
                    break;
                case ServerTypeID.OIS:
                    ServerIndex = GetPlastConverterIndex(ServerType, of.ParamsDict.OraServerName, of.ParamsDict.OraPort, of.ParamsDict.OraSid);
                    break;
            }

            switch (DataType)
            {
                case SERVER_DATA_TYPE.COORDINATES:
                    res = ConvertPlastCodesInCoord(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.MER:
                    res = ConvertPlastCodesInMer(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.MER_NEW:
                    res = ConvertPlastCodesInMer(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.GIS:
                    res = ConvertPlastCodesInGis(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.CONTOUR:
                    res = ConvertPlastCodesInContours(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.GRID:
                    res = ConvertPlastCodesInGrids(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.PVT:
                    res = ConvertPlastCodesInPVTParams(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE:
                    res = ConvertPlastCodesInWellResearch(ServerIndex, of);
                    break;
            }
            if (res == 1) SaveConversionList();
            return res;
        }

        int ConvertPlastCodesInCoord(int ServerIndex, OilField of)
        {
            if (of.CoordLoaded)
            {
                of.CoordLoaded = false;
                Well w;
                int j, stratumID;
                for (int i = 0; i < of.Wells.Count; i++)
                {
                    w = of.Wells[i];
                    if (w.CoordLoaded)
                    {
                        for (j = 0; j < w.coord.Count; j++)
                        {
                            stratumID = w.coord.Items[j].PlastId;
                            stratumID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "Координаты", stratumID);

                            if (stratumID == -1)
                            {
                                w.coord = null;
                                return -1;
                            }
                            else
                            {
                                w.coord.Items[j].PlastId = (ushort)stratumID;
                                of.CoordLoaded = true;
                            }
                        }
                    }
                    if (w.icons != null)
                    {
                        for (j = 0; j < w.icons.Count; j++)
                        {
                            stratumID = w.icons[j].StratumCode;
                            stratumID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "Координаты", stratumID);
                            if (stratumID == -1)
                            {
                                w.icons = null;
                                return -1;
                            }
                            else
                            {
                                w.icons[j].StratumCode = (ushort)stratumID;
                            }
                        }
                    }
                }
            }
            return (of.CoordLoaded) ? 1 : 0;
        }
        int ConvertPlastCodesInMer(int ServerIndex, OilField of)
        {
            if (of.MerLoaded)
            {
                of.MerLoaded = false;
                Well w;
                for (int i = 0; i < of.Wells.Count; i++)
                {
                    w = of.Wells[i];
                    if (w.MerLoaded)
                    {
                        int plastID;
                        for (int j = 0; j < w.mer.Count; j++)
                        {
                            plastID = w.mer.Items[j].PlastId;
                            plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "МЭР", plastID);
                            if (plastID == -1)
                            {
                                w.mer = null;
                                return -1;
                            }
                            else
                            {
                                w.mer.Items[j].PlastId = plastID;
                                of.MerLoaded = true;
                            }
                        }
                    }
                }
            }
            return (of.MerLoaded) ? 1 : 0;
        }
        int ConvertPlastCodesInGis(int ServerIndex, OilField of)
        {
            if (of.GisLoaded)
            {
                of.GisLoaded = false;
                Well w;
                for (int i = 0; i < of.Wells.Count; i++)
                {
                    w = of.Wells[i];
                    if (w.GisLoaded)
                    {
                        int plastID, plastZoneID;
                        for (int j = 0; j < w.gis.Count; j++)
                        {
                            plastID = w.gis.Items[j].PlastId;
                            plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "ГИС", plastID);
                            plastZoneID = w.gis.Items[j].PlastZoneId;
                            plastZoneID = GetLocalCodeByServerCode(true, ServerIndex, of.Name, w.Name, "ГИС", plastZoneID);

                            if (plastID == -1)
                            {
                                w.gis = null;
                                return -1;
                            }
                            else
                            {
                                w.gis.Items[j].PlastId = (ushort)plastID;
                                of.GisLoaded = true;
                            }
                            if (plastZoneID == -1)
                            {
                                w.gis = null;
                                return -1;
                            }
                            else
                            {
                                w.gis.Items[j].PlastZoneId = (ushort)plastZoneID;
                                of.GisLoaded = true;
                            }
                        }
                    }
                }
            }
            return (of.GisLoaded) ? 1 : 0;
        }
        int ConvertPlastCodesInContours(int ServerIndex, OilField of)
        {
            if ((of.ContoursDataLoaded) && (of.Contours.Count > 0))
            {
                int plastID = 0;
                for (int i = 0; i < of.Contours.Count; i++)
                {
                    plastID = of.Contours[i].StratumCode;
                    if (plastID > -1)
                    {
                        plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, "", "Контуры", plastID);
                        if (plastID == -1)
                        {
                            return -1;
                        }
                        of.Contours[i].StratumCode = plastID;
                    }
                }
                return 1;
            }
            return 0;
        }
        int ConvertPlastCodesInGrids(int ServerIndex, OilField of)
        {
            if ((of.Grids != null) && (of.Grids.Count > 0))
            {
                int plastID = 0;
                for (int i = 0; i < of.Grids.Count; i++)
                {
                    plastID = of.Grids[i].StratumCode;
                    plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, "", "Сетки", plastID);

                    if (plastID == -1) return -1;
                    of.Grids[i].StratumCode = plastID;
                }
                return 1;
            }
            return 0;
        }
        int ConvertPlastCodesInPVTParams(int ServerIndex, OilField of)
        {
            if ((of.PVTList != null) && (of.PVTList.Count > 0))
            {
                int plastID = 0;
                for (int i = 0; i < of.PVTList.Count; i++)
                {
                    PVTParamsItem item = of.PVTList[i];
                    plastID = item.StratumCode;
                    plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, "", "PVT свойства", plastID);
                    if (plastID == -1) return -1;
                    item.StratumCode = plastID;
                    of.PVTList[i] = item;
                }
                return 1;
            }
            return 0;
        }
        int ConvertPlastCodesInWellResearch(int ServerIndex, OilField of)
        {
            Well w;
            int result = 0;
            for (int i = 0; i < of.Wells.Count; i++)
            {
                w = of.Wells[i];
                if (w.ResearchLoaded)
                {
                    int plastID;
                    for (int j = 0; j < w.research.Count; j++)
                    {
                        plastID = w.research[j].PlastCode;
                        plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "Замеры скважин", plastID);
                        if (plastID == -1)
                        {
                            // Пользователь нажал отмену в окне выбора пласта из локального справочника
                            w.research = null;
                            return -1;
                        }
                        else
                        {
                            WellResearchItem item = w.research[j];
                            item.PlastCode = (ushort)plastID;
                            w.research[j] = item;
                            result = 1;
                        }
                    }
                }
            }
            return result;
        }
        #endregion
    }
}