﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SmartPlus.DictionaryObjects
{
    public enum ServerTypeID : byte
    {
        OIS,
        RDF
    }
    public struct ConversionItem
    {
        public bool IsStratumGeoZone;
        public int serverID;
        public string serverName;
        public int localID;
        public string localName;
    }
    public sealed class ServerStratumConverter
    {
        public string ServerName;
        public int ServerPort;
        public string OracleSid;
        public ServerTypeID ServerType;
        DataDictionary servDict;
        DataDictionary servStratumZoneDict;
        DataDictionary locDict;
        string StratumDictPath;
        public bool ConversionItemAdded;

        List<ConversionItem> ConvList;
        string[] ConversionStartSymbols = new string[] { "С", "C", "СТ", "CT", "СT", "CT", "CТ", "CT", "Д", "D", "Р", "P"};
        string[] ConversionOtherSymbols = new string[] { "_", ".", "+", "-"};

        public ServerStratumConverter(ServerTypeID ServerType, string ServerName, int ServerPort, string Path, DataDictionary LocalDict, string OracleSid)
        {
            this.ServerType = ServerType;
            this.ServerName = ServerName;
            this.ServerPort = ServerPort;
            StratumDictPath = Path;
            servDict = null;
            servStratumZoneDict = null;
            locDict = LocalDict;
            ConvList = new List<ConversionItem>();
            ConversionItemAdded = false;
            this.OracleSid = OracleSid;
        }

        bool LoadServerDictionary()
        {
            bool result = false;
            switch(ServerType)
            {
                case ServerTypeID.RDF:
                    if ((ServerName != null) && (ServerPort > -1))
                    {
                        RDF.RdfConnector RdfConn = new RDF.RdfConnector();
                        try
                        {
                            RdfConn.Connect("guest", "", @"\\" + ServerName + ":" + ServerPort.ToString());
                        }
                        catch
                        {
                            return false;
                        }
                        if (RdfConn.Connected)
                        {
                            servDict = new DataDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                            servDict.LoadFromRDF(RdfConn);
                            servStratumZoneDict = new DataDictionary(DICTIONARY_ITEM_TYPE.STRATUMGEOZONE);
                            servStratumZoneDict.LoadFromRDF(RdfConn);
                            RdfConn.Disconnect();
                            result = true;
                        }
                    }
                    break;
                case ServerTypeID.OIS:
                    return false;
            }
            return result;
        }
        public string GetServerStratumName(int ServerStratumID)
        {
            return GetServerStratumName(ServerStratumID, false);
        }
        public string GetServerStratumName(int ServerStratumID, bool IsStratumGeoZone)
        {
            if (servDict == null || servStratumZoneDict == null) LoadServerDictionary();
            DataDictionary dict = (IsStratumGeoZone ? servStratumZoneDict : servDict);
            if (servDict != null)
            {
                string servStratumName = string.Empty;
                int ind = servDict.GetIndexByCode(ServerStratumID);
                if (ind != -1) servStratumName = servDict[ind].ShortName;
                return servStratumName;
            }
            return string.Empty;
        }
        public int GetLocalID(int ServerStratumID, bool IsStratumGeoZone)
        {
            for (int i = 0; i < ConvList.Count; i++)
            {
                if ((ConvList[i].IsStratumGeoZone == IsStratumGeoZone) && (ConvList[i].serverID == ServerStratumID))
                {
                    return ConvList[i].localID;
                }
            }
            return -1;
        }
        public int GetNearLocalID(int ServerPlastID, bool IsPlastZone)
        {
            int res = -1;
            if ((servDict == null) || (servStratumZoneDict == null)) LoadServerDictionary();
            DataDictionary dict = (IsPlastZone ? servStratumZoneDict : servDict);

            if (dict != null)
            {
                int ind = dict.GetIndexByCode(ServerPlastID);

                if (ind != -1)
                {
                    string servPlastName = dict[ind].ShortName;

                    ind = locDict.GetIndexByShortName(ServerName);
                    if (ind != -1)
                    {
                        res = locDict[ind].Code;
                    }
                    else
                    {
                        int i;
                        string str1, str2, tempName;
                        for (i = 0; i < ConversionOtherSymbols.Length - 1; i += 2)
                        {
                            str1 = ConversionOtherSymbols[i];
                            str2 = ConversionOtherSymbols[i + 1];
                            servPlastName = servPlastName.Replace(str1, str2);
                        }
                        ind = locDict.GetIndexByShortName(servPlastName);

                        if (ind != -1)
                        {
                            res = locDict[ind].Code;
                        }
                        else
                        {
                            for (i = 0; i < ConversionStartSymbols.Length - 1; i += 2)
                            {
                                str1 = ConversionStartSymbols[i];
                                str2 = ConversionStartSymbols[i + 1];

                                if (servPlastName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                                {
                                    tempName = str2 + servPlastName.Remove(0, str1.Length);
                                    ind = locDict.GetIndexByShortName(tempName);
                                    if (ind != -1)
                                    {
                                        res = locDict[ind].Code;
                                        break;
                                    }
                                }
                            }
                            //if (res == -1)
                            //{
                            //    for (i = 0; i < ConversionTryReplaceSymbols.Length - 1; i += 2)
                            //    {
                            //        str1 = ConversionTryReplaceSymbols[i];
                            //        str2 = ConversionTryReplaceSymbols[i + 1];
                            //        tempName = servPlastName.Replace(str1, str2);
                            //        ind = locDict.GetIndexByShortName(tempName);
                            //        if (ind != -1)
                            //        {
                            //            res = locDict[ind].Code;
                            //            break;
                            //        }
                            //    }
                            //}
                        }
                    }
                }
            }
            return res;
        }

        public void AddConversion(bool IsStratumGeoZone, int ServerStratumID, string ServerStratumName, int LocalStratumID, string LocalStratumName)
        {
            ConversionItem item;
            item.IsStratumGeoZone = IsStratumGeoZone;
            item.serverID = ServerStratumID;
            item.serverName = ServerStratumName;
            item.localID = LocalStratumID;
            item.localName = LocalStratumName;
            ConvList.Add(item);
            ConversionItemAdded = true;
        }
        public bool LoadFromFile()
        {
            string fileName = StratumDictPath + "\\" + ((byte)ServerType).ToString() + "_" + ServerName + "_" + OracleSid + "_" + ServerPort.ToString() + ".bin";
            if ((StratumDictPath.Length > 0) && (File.Exists(fileName)))
            {
                try
                {
                    ConversionItem item;
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    int count = br.ReadInt32();
                    for (int i = 0; i < count; i++)
                    {
                        item.IsStratumGeoZone = br.ReadBoolean();
                        item.serverID = br.ReadInt32();
                        item.serverName = br.ReadString();
                        item.localID = br.ReadInt32();
                        item.localName = br.ReadString();
                        ConvList.Add(item);
                    }
                    br.Close();
                }
                catch
                {
                    ConvList.Clear();
                    return false;
                }
                return true;
            }
            return false;
        }
        public void SaveToFile()
        {
            if ((ConvList.Count > 0) && (StratumDictPath.Length > 0) && (Directory.Exists(StratumDictPath)))
            {
                string fileName = StratumDictPath + "\\" + ((byte)ServerType).ToString() + "_" + ServerName + "_" + OracleSid + "_" + ServerPort.ToString() + ".bin";
                FileStream fs = new FileStream(fileName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(ConvList.Count);
                int count = 0;
                for (int i = 0; i < ConvList.Count; i++)
                {
                    if ((ConvList[i].localID != 0) || (ConvList[i].serverID == 0))
                    {
                        bw.Write(ConvList[i].IsStratumGeoZone);
                        bw.Write(ConvList[i].serverID);
                        bw.Write(ConvList[i].serverName);
                        bw.Write(ConvList[i].localID);
                        bw.Write(ConvList[i].localName);
                        count++;
                    }
                }
                fs.Seek(0, SeekOrigin.Begin);
                bw.Write(count);
                bw.Close();
            }
        }
    }
}
