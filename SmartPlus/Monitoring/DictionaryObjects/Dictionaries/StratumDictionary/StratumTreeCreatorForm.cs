﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartPlus.DictionaryObjects
{
    public partial class StratumTreeCreatorForm : Form
    {
        string filesPath;
        StratumTree stratumTree;
        StratumDictionary LocalDict;
        BackgroundWorker worker;
        List<int> findStratumList;
        string[] ConversionStartSymbols = new string[] { "С", "C", "СТ", "CT", "СT", "CT", "CТ", "CT", "Д", "D", "Р", "P" };
        bool exit;
        bool NeedSaveStratumTree = false;
        ContextMenuStrip cMenu;
        ToolStripMenuItem cmRemove;
        
        public StratumTreeCreatorForm(StratumDictionary LocalDict, string Path)
        {
            InitializeComponent();
            InitContexMenus();
            findStratumList = new List<int>();
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            tbFindName.TextChanged += new EventHandler(tbFindName_TextChanged);
            tbFindName.KeyDown += new KeyEventHandler(tbFindName_KeyDown);
            FormClosing += new FormClosingEventHandler(StratumTreeCreatorForm_FormClosing);
            tree.MouseDown += new MouseEventHandler(tree_MouseDown);

            filesPath = Path;
            this.LocalDict = LocalDict;
            stratumTree = new StratumTree(LocalDict);
            stratumTree.LoadFromFile(filesPath);
            LoadStratumTree();
            FillGrid();
        }

        void InitContexMenus()
        {
            cMenu = new ContextMenuStrip();
            cmRemove = (ToolStripMenuItem)cMenu.Items.Add("");
            cmRemove.Image = Properties.Resources.Delete16;
            cmRemove.Click += new EventHandler(cmRemove_Click);
        }

        void tree_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                TreeNode clickedNode = tree.GetNodeAt(e.Location);
                tree.SelectedNode = clickedNode;
                if (clickedNode != null)
                {
                    if (clickedNode.Level == 1)
                    {
                        if (clickedNode.Index == 0)
                        {
                            cmRemove.Text = "Очистить список родителей";
                        }
                        else
                        {
                            cmRemove.Text = "Очистить список наследников";
                        }
 
                    }
                    else if (clickedNode.Level == 2)
                    {
                        TreeNode parent = clickedNode.Parent;
                        string str;
                        if (parent.Index == 0)
                        {
                            str = " родителей";
                        }
                        else
                        {
                            str = " наследников";
                        }
                        cmRemove.Text = "Удалить пласт из списка" + str;
                    }
                }
            }
        }
        void cmRemove_Click(object sender, EventArgs e)
        {
            TreeNode tn = tree.SelectedNode;
            TreeNode findNode;
            int parentCode, plCode;
            if (tn != null)
            {
                if (tn.Level == 1)
                {
                    TreeNode parent = tn.Parent;
                    parentCode = ((StratumTreeNode)parent.Tag).StratumCode;
                    if (tn.Index == 0)
                    {
                        int i = 0;
                        while (i < tn.Nodes.Count)
                        {
                            plCode = ((StratumTreeNode)tn.Nodes[i].Tag).StratumCode;
                            if (RemoveInParents(parent, plCode))
                            {
                                findNode = GetNodeByStratumCode(plCode);
                                if (findNode != null)
                                {
                                    RemoveInChilds(findNode, parentCode);
                                }
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }
                    else
                    {
                        int i = 0;
                        while(i < tn.Nodes.Count)
                        {
                            plCode = ((StratumTreeNode)tn.Nodes[i].Tag).StratumCode;
                            if (RemoveInChilds(parent, plCode))
                            {
                                findNode = GetNodeByStratumCode(plCode);
                                if (findNode != null)
                                {
                                    RemoveInParents(findNode, parentCode);
                                }
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }
                }
                else if (tn.Level == 2)
                {
                    TreeNode parent = tn.Parent.Parent;
                    parentCode = ((StratumTreeNode)parent.Tag).StratumCode;
                    StratumTreeNode pl = (StratumTreeNode)tn.Tag;
                    if (tn.Parent.Index == 0)
                    {
                        if (RemoveInParents(parent, pl.StratumCode))
                        {
                            findNode = GetNodeByStratumCode(pl.StratumCode);
                            if (findNode != null)
                            {
                                RemoveInChilds(findNode, parentCode);
                            }
                        }
                    }
                    else
                    {
                        if (RemoveInChilds(parent, pl.StratumCode))
                        {
                            findNode = GetNodeByStratumCode(pl.StratumCode);
                            if (findNode != null)
                            {
                                RemoveInParents(findNode, parentCode);
                            }
                        }
                    }
                }
            }
        }
        void tbFindName_TextChanged(object sender, EventArgs e)
        {
            FillGrid();
        }
        void tbFindName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Up)
            {
                if (gridStratumList.SelectedRows.Count > 0)
                {
                    if (gridStratumList.SelectedRows[0].Index > 0) gridStratumList.Rows[gridStratumList.SelectedRows[0].Index - 1].Selected = true;
                }
            }
            else if (e.KeyData == Keys.Down)
            {
                if (gridStratumList.SelectedRows.Count > 0)
                {
                    if (gridStratumList.SelectedRows[0].Index < gridStratumList.Rows.Count - 1)
                    {
                        gridStratumList.Rows[gridStratumList.SelectedRows[0].Index + 1].Selected = true;
                    }
                }
            }
        }
        void StratumTreeCreatorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            tbFindName.Text = string.Empty;
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!exit)
            {
                if (e.Cancelled)
                {
                    worker.RunWorkerAsync(tbFindName.Text);
                }
                else
                {
                    gridStratumList.Rows.Clear();
                    if (findStratumList.Count > 0)
                    {
                        DataGridViewRowCollection rows = gridStratumList.Rows;
                        rows.Add(findStratumList.Count);

                        for (int i = 0; i < findStratumList.Count; i++)
                        {
                            rows[i].Cells[0].Value = LocalDict[findStratumList[i]].Code.ToString();
                            rows[i].Cells[1].Value = LocalDict[findStratumList[i]].ShortName;
                        }
                    }
                    tbFindName.Focus();
                }
            }
            exit = false;            
        }
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            findStratumList.Clear();
            if (LocalDict != null)
            {
                string findName = (string)e.Argument;
                if (findName == "")
                {
                    for (int i = 0; i < LocalDict.Count; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            findStratumList.Clear();
                            return;
                        }
                        findStratumList.Add(i);
                    }
                }
                else
                {
                    string str1, str2;
                    for (int i = 0; i < LocalDict.Count; i++)
                    {
                        if (LocalDict[i].ShortName.StartsWith(findName, StringComparison.OrdinalIgnoreCase))
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                findStratumList.Clear();
                                return;
                            }
                            findStratumList.Add(i);
                        }
                        else
                        {
                            for (int j = 0; j < ConversionStartSymbols.Length - 1; j += 2)
                            {
                                str1 = ConversionStartSymbols[j];
                                str2 = ConversionStartSymbols[j + 1];

                                if (findName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                                {
                                    str1 = str2 + findName.Remove(0, str1.Length);
                                    if (LocalDict[i].ShortName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                                    {
                                        if (worker.CancellationPending)
                                        {
                                            e.Cancel = true;
                                            findStratumList.Clear();
                                            return;
                                        }
                                        findStratumList.Add(i);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }            
        }
        void FillGrid()
        {
            if (worker.IsBusy)
            {
                worker.CancelAsync();
            }
            else
            {
                worker.RunWorkerAsync(tbFindName.Text);
            }
        }

        void LoadStratumTree()
        {
            TreeNode tn, tnX, tnNew;
            StratumTreeNode node;
            int i, j;
            for (i = 0; i < stratumTree.Count; i++)
            {
                node = stratumTree[i];
                tn = new TreeNode();
                tn.Text = string.Format("{0} [{1}]", node.Name, node.StratumCode);
                tn.Tag = node;
                tnX = tn.Nodes.Add("Родители");
                tnX.ContextMenuStrip = cMenu;
                if(node.Parents != null)
                {
                    for (j = 0; j < node.Parents.Count; j++)
                    {
                        tnNew = new TreeNode();
                        tnNew.ContextMenuStrip = cMenu;
                        tnNew.Text = string.Format("{0} [{1}]", node.Parents[j].Name, node.Parents[j].StratumCode);
                        tnNew.Tag = node.Parents[j];
                        tnX.Nodes.Add(tnNew);
                    }
                }
                tnX = tn.Nodes.Add("Наследники");
                tnX.ContextMenuStrip = cMenu;
                if (node.Childs != null)
                {
                    for (j = 0; j < node.Childs.Count; j++)
                    {
                        tnNew = new TreeNode();
                        tnNew.ContextMenuStrip = cMenu;
                        tnNew.Text = string.Format("{0} [{1}]", node.Childs[j].Name, node.Childs[j].StratumCode);
                        tnNew.Tag = node.Childs[j];
                        tnX.Nodes.Add(tnNew);
                    }
                }
                tree.Nodes.Add(tn);
            }
            tree.SelectedNode = null;
        }
        void SaveStratumTree()
        {
            stratumTree.WriteToFile(filesPath);
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            if (NeedSaveStratumTree)
            {
                SaveStratumTree();
                NeedSaveStratumTree = false;
            }
            Close();
        }
        private TreeNode GetNodeByStratumCode(int StratumCode)
        {
            TreeNode node = null;
            for (int i = 0; i < tree.Nodes.Count; i++)
            {
                if (tree.Nodes[i].Tag != null)
                {
                    if (((StratumTreeNode)tree.Nodes[i].Tag).StratumCode == StratumCode)
                    {
                        return tree.Nodes[i];
                    }
                }
            }
            return node;
        }

        private bool AddInParents(TreeNode TargetNode, int StratumCode)
        {
            StratumTreeNode plNode = (StratumTreeNode)TargetNode.Tag;
            StratumTreeNode addplNode = stratumTree.GetNodeByCode(StratumCode);
            TreeNode parents = TargetNode.Nodes[0];
            if (addplNode != null)
            {
                if (plNode.Parents != null)
                {
                    for (int i = 0; i < plNode.Parents.Count; i++)
                    {
                        if (plNode.Parents[i].StratumCode == StratumCode)
                        {
                            MessageBox.Show("Данный пласт уже есть в списке родителей пласта " + plNode.Name, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }
                if (plNode.Childs != null)
                {
                    for (int i = 0; i < plNode.Childs.Count; i++)
                    {
                        if (plNode.Childs[i].StratumCode == StratumCode)
                        {
                            MessageBox.Show("Данный пласт уже есть в списке наследников пласта " + plNode.Name, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }
                bSave.Enabled = true;
                if (plNode.Parents == null) plNode.Parents = new List<StratumTreeNode>();
                plNode.Parents.Add(addplNode);
                TreeNode addNode = new TreeNode();
                addNode.ContextMenuStrip = cMenu;
                addNode.Text = string.Format("{0} [{1}]", addplNode.Name, addplNode.StratumCode);
                addNode.Tag = addplNode;
                parents.Nodes.Add(addNode);
                NeedSaveStratumTree = true;
                return true;
            }
            return false;
        }
        private bool AddInChilds(TreeNode TargetNode, int StratumCode)
        {
            StratumTreeNode plNode = (StratumTreeNode)TargetNode.Tag;
            TreeNode childs = TargetNode.Nodes[1];
            StratumTreeNode addplNode = stratumTree.GetNodeByCode(StratumCode);
            if (addplNode != null)
            {
                if (plNode.Parents != null)
                {
                    for (int i = 0; i < plNode.Parents.Count; i++)
                    {
                        if (plNode.Parents[i].StratumCode == StratumCode)
                        {
                            MessageBox.Show("Данный пласт уже есть в списке родителей пласта " + plNode.Name, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }
                if (plNode.Childs != null)
                {
                    for (int i = 0; i < plNode.Childs.Count; i++)
                    {
                        if (plNode.Childs[i].StratumCode == StratumCode)
                        {
                            MessageBox.Show("Данный пласт уже есть в списке наследников пласта " + plNode.Name, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }
                bSave.Enabled = true;
                if (plNode.Childs == null) plNode.Childs = new List<StratumTreeNode>();
                plNode.Childs.Add(addplNode);
                TreeNode addNode = new TreeNode();
                addNode.ContextMenuStrip = cMenu;
                addNode.Text = string.Format("{0} [{1}]", addplNode.Name, addplNode.StratumCode);
                addNode.Tag = addplNode;
                childs.Nodes.Add(addNode);
                NeedSaveStratumTree = true;
                return true;
            }
            return false;
        }
        private bool RemoveInParents(TreeNode TargetNode, int StratumCode)
        {
            StratumTreeNode plNode = (StratumTreeNode)TargetNode.Tag;
            StratumTreeNode remplNode = stratumTree.GetNodeByCode(StratumCode);
            TreeNode parents = TargetNode.Nodes[0];
            if (remplNode != null)
            {
                bSave.Enabled = true;
                plNode.Parents.Remove(remplNode);
                for (int i = 0; i < parents.Nodes.Count; i++)
                {
                    if ((parents.Nodes[i].Tag != null) && (((StratumTreeNode)parents.Nodes[i].Tag).StratumCode == StratumCode))
                    {
                        parents.Nodes.RemoveAt(i);
                        break;
                    }
                }
                if(plNode.Parents.Count == 0) plNode.Parents = null;
                return true;
            }
            return false;
        }
        private bool RemoveInChilds(TreeNode TargetNode, int StratumCode)
        {
            StratumTreeNode plNode = (StratumTreeNode)TargetNode.Tag;
            StratumTreeNode remplNode = stratumTree.GetNodeByCode(StratumCode);
            TreeNode childs = TargetNode.Nodes[1];
            if (remplNode != null)
            {
                bSave.Enabled = true;
                plNode.Childs.Remove(remplNode);
                for (int i = 0; i < childs.Nodes.Count; i++)
                {
                    if ((childs.Nodes[i].Tag != null) && (((StratumTreeNode)childs.Nodes[i].Tag).StratumCode == StratumCode))
                    {
                        childs.Nodes.RemoveAt(i);
                        break;
                    }
                }
                if(plNode.Childs.Count == 0) plNode.Childs = null;
                return true;
            }
            return false;
        }
        private void bAddParent_Click(object sender, EventArgs e)
        {
            string str;
            if (tree.SelectedNode == null)
            {
                str = "Выберите пласт в иерархии объектов!";
                MessageBox.Show(str, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (gridStratumList.SelectedRows.Count == 0)
            {
                str = "Выберите добавляемый пласт в справочнике объектов!";
                MessageBox.Show(str, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            TreeNode tn = tree.SelectedNode;
            while (tn.Level > 0) tn = tn.Parent;
            for (int i = gridStratumList.SelectedRows.Count -1; i >= 0; i--)
            {
                int plCode = Convert.ToInt32(gridStratumList.SelectedRows[i].Cells[0].Value);
                if ((tn.Tag != null) && ((StratumTreeNode)tn.Tag).StratumCode == plCode)
                {
                    str = "Нельзя добавить пласт в свой список родителей!";
                    MessageBox.Show(str, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (AddInParents(tn, plCode))
                {
                    TreeNode TargetChildNode = GetNodeByStratumCode(plCode);
                    if ((TargetChildNode != null) && (tn.Tag != null))
                    {
                        AddInChilds(TargetChildNode, ((StratumTreeNode)tn.Tag).StratumCode);
                    }
                    if(!tn.IsExpanded) tn.Expand();
                    if (!tn.Nodes[0].IsExpanded) tn.Nodes[0].Expand();
                }
            }
        }
        private void bAddChild_Click(object sender, EventArgs e)
        {
            string str;
            if (tree.SelectedNode == null)
            {
                str = "Выберите пласт в иерархии объектов!";
                MessageBox.Show(str, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (gridStratumList.SelectedRows.Count == 0)
            {
                str = "Выберите добавляемый пласт в справочнике объектов!";
                MessageBox.Show(str, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            TreeNode tn = tree.SelectedNode;
            while (tn.Level > 0) tn = tn.Parent;
            for (int i = gridStratumList.SelectedRows.Count - 1; i >= 0; i--)
            {
                int plCode = Convert.ToInt32(gridStratumList.SelectedRows[i].Cells[0].Value);
                if ((tn.Tag != null) && ((StratumTreeNode)tn.Tag).StratumCode == plCode)
                {
                    str = "Нельзя добавить пласт в свой список наследников!";
                    MessageBox.Show(str, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (AddInChilds(tn, plCode))
                {
                    TreeNode TargetParentNode = GetNodeByStratumCode(plCode);
                    if ((TargetParentNode != null) && (tn.Tag != null))
                    {
                        AddInParents(TargetParentNode, ((StratumTreeNode)tn.Tag).StratumCode);
                    }
                    if (!tn.IsExpanded) tn.Expand();
                    if (!tn.Nodes[1].IsExpanded) tn.Nodes[1].Expand();
                }
            }
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            string str = "Сохранить текущее представление иерархии пластов?";
            if (MessageBox.Show(str, "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                SaveStratumTree();
                NeedSaveStratumTree = false;
                bSave.Enabled = false;
            }
        }
    }
}
