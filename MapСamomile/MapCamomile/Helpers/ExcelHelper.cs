﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace MapPieItem.Helpers
{
    public static class ExcelHelper
    {
        private static readonly string _baseUri = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

        public static ICollection<IDictionary<string, object>> ReadExcelSheet(this string FileName, string excelSheet)
        {
            ICollection<IDictionary<string, object>> res = null;
            var fileInfo = new FileInfo(Path.Combine(_baseUri, FileName));
            if (!fileInfo.Exists || fileInfo.Extension.ToLower() != ".xlsx") return null;
            using (var stream = File.Open(fileInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var package = new ExcelPackage(stream))
                {
                    res = ReadWorksheetNames(package).Contains(excelSheet)? ReadExcelSheet(package, excelSheet):null;
                }
            }
            return res;
        }

        public static ICollection<string> ReadWorksheetNames(this ExcelPackage package)
        {
            var book = package?.Workbook;
            if (book == null) return null;

            var worksheetNames = new List<string>();
            foreach (var w in book.Names)
                if (!w.IsNameHidden && w.Value != null && w.FullAddress != null && w.Worksheet?.Hidden == eWorkSheetHidden.Visible)
                {
                    worksheetNames.Add('!' + w.Name);
                }
            foreach (var w in book.Worksheets) if (!w.Protection.IsProtected && w.Hidden == eWorkSheetHidden.Visible) worksheetNames.Add(w.Name);

            return worksheetNames;
        }

        public static ICollection<IDictionary<string, object>> ReadExcelSheet(this ExcelPackage package, string excelSheet)
        {
            if (package?.Workbook == null ) throw new Exception("package is disposed");
            if (string.IsNullOrEmpty(excelSheet)) throw new Exception("ExcelSheet is empty");
            ExcelNamedRange namedRange = null;
            if (excelSheet.StartsWith("!",StringComparison.Ordinal))
            {
                var namedRangeName = excelSheet.Substring(1);
                namedRange = package.Workbook.Names[namedRangeName];
                excelSheet = namedRange.Worksheet.Name;
            }

            var coll = new List<IDictionary<string, object>>();
            IEnumerable<string> header;
            if (namedRange != null)
            {
                int startcol = namedRange.Start.Column ,
                    startrow = namedRange.Start.Row ,
                      endcol = namedRange.End.Column ,
                      endrow = namedRange.End.Row ;
                header = package.Workbook.Worksheets[excelSheet].Cells[startrow, startcol, startrow, endcol].Select(c => c.Text.Replace("/", "").Replace(".", "#"));
                var headerEnumerator = header.GetEnumerator();
                for (var i = startrow + 1; i <= endrow; i++)
                {
                    var dict = new Dictionary<string, object>();
                    var rownotempty = false;
                    headerEnumerator.Reset();
                    for (var j = startcol; j <= endcol; j++)
                    {
                        var val = package.Workbook.Worksheets[excelSheet].Cells[i, j].Value;
                        if (string.IsNullOrWhiteSpace(val?.ToString())) val = null;
                        else rownotempty = true;

                        var hasHeader = headerEnumerator.MoveNext();
                        if (!hasHeader) break;
                        var h = headerEnumerator.Current;
                        if(h!=null && !dict.ContainsKey(h)) dict.Add(h, val);
                    }
                    if (rownotempty) coll.Add(dict);
                }
            }
            else  if (package.Workbook.Worksheets[excelSheet].Tables.Count > 0)
            {
                var table = package.Workbook.Worksheets[excelSheet].Tables[0];
                header = table.Columns.Select(v => v.Name.Replace("/", "").Replace(".", "#"));
                var headerEnumerator = header.GetEnumerator();

                int startcol = table.Address.Start.Column,
                    startrow = table.Address.Start.Row,
                      endcol = table.Address.End.Column,
                      endrow = table.Address.End.Row;


                    for (var i = startrow + 1; i <= endrow; i++)
                    {
                        var dict = new Dictionary<string, object>();
                        var rownotempty = false;
                        headerEnumerator.Reset();
                        for (var j = startcol; j <= endcol; j++)
                        {
                            var val = package.Workbook.Worksheets[excelSheet].Cells[i, j].Value;
                            if (string.IsNullOrWhiteSpace(val?.ToString())) val = null;
                            else rownotempty = true;
                            var hasHeader = headerEnumerator.MoveNext();
                            if (!hasHeader) break;
                            var h = headerEnumerator.Current;
                            if (h != null && !dict.ContainsKey(h)) dict.Add(h, val);
                        }
                        if(rownotempty) coll.Add(dict);
                    }

                    var _break = false;
                    for (var i = endrow + 1; i < 65536; i++)
                    {
                        var dict = new Dictionary<string, object>();
                        headerEnumerator.Reset();
                        for (var j = startcol; j <= endcol; j++)
                        {
                            var val = package.Workbook.Worksheets[excelSheet].Cells[i, j].Value;
                            if (j == startcol && (string.IsNullOrWhiteSpace(val?.ToString())))
                            {
                                _break = true;
                                break;
                            }
                            var hasHeader = headerEnumerator.MoveNext();
                            if (!hasHeader) break;
                            var h = headerEnumerator.Current;
                            if (h != null && !dict.ContainsKey(h)) dict.Add(h, val);
   
                        }
                        if (_break)
                        {
                            break;
                        }
                        coll.Add(dict);
                    }
            }
            else
            {
                    var dimension = package.Workbook.Worksheets[excelSheet].Dimension;
                    if (dimension == null) return coll;
                    int startcol = dimension.Start.Column, startrow = dimension.Start.Row, endcol = dimension.End.Column, endrow = dimension.End.Row;

                    header = package.Workbook.Worksheets[excelSheet].Cells[startrow, startcol, startrow, endcol].Select(c => c.Text.Replace("/", "").Replace(".", "#")).ToArray();
                    var headerEnumerator = header.GetEnumerator();
                    for (var i = startrow + 1; i <= endrow; i++)
                    {
                        var dict = new Dictionary<string, object>();
                        headerEnumerator.Reset();
                        for (var j = startcol; j <= endcol; j++)
                        {
                            var val = package.Workbook.Worksheets[excelSheet].Cells[i, j].Value;
                            if (val!=null && string.IsNullOrWhiteSpace(val.ToString())) val = null;

                            var hasHeader = headerEnumerator.MoveNext();
                            if (!hasHeader) break;
                            var h = headerEnumerator.Current;
                            if (h != null && !dict.ContainsKey(h)) dict.Add(h, val);
                        }
                        coll.Add(dict);
                    }
            }
            return coll;
        }
    }
}
