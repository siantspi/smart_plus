﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Map;
using DevExpress.Map.Native;
using DevExpress.Utils;
using DevExpress.Utils.Filtering;
using DevExpress.XtraMap;
using DevExpress.XtraMap.Drawing.Direct2D11;
using DevExpress.XtraMap.Native;
using DotSpatial.Data;
using MapPieItem.Helpers;

namespace MapPieItem {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }


        private readonly Uri _baseUri = new Uri(System.Reflection.Assembly.GetEntryAssembly().Location);



        private Dictionary<string, Color> shpFiles = new Dictionary<string, Color>()
        { 
            //{ "admin_gran.shp", Color.Ivory},
            { "nasel_punkt.shp", Color.DarkGray},
            { "objects.shp", Color.BurlyWood },
            { "Водоемы.shp" ,Color.CornflowerBlue},
            { "Дороги.shp" ,Color.Brown},
            {"Дороги_запрет.shp",Color.LightCoral},
            { "леса.shp",Color.DarkSeaGreen},
        };

        private void Form1_Load(object sender, EventArgs e) {

            mapControl1.MapItemClick += MapControl1_MapItemClick;
            foreach (var shpFile in shpFiles.Keys)
            {
                try
                {
                    var layer = new VectorItemsLayer()
                    {
                        Data = CreateShpData("SHP/" + shpFile),
                        Colorizer = new SimpleColorizer(shpFiles[shpFile]),
                        MinZoomLevel = 7
                    };
                    mapControl1.Layers.Add(layer);
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Не загружается " + shpFile + "." + ex.Message);
                }
            }

            var wellStatuses = "Типы статусов скважины.xlsx".ReadExcelSheet("Sheet1").Select(c => c["Типы статуса скважины"]?.ToString()).ToArray();
            var actualStatuses = "Список скважин и статусов (last).xlsx".ReadExcelSheet("Список скважин и статусов").GroupBy(k=>k["№ скважины"]).ToArray();

            var uri = new Uri(_baseUri, "SHP/Скважины.shp");

            var nycBoroughs = DotSpatial.Data.Shapefile.OpenFile(Uri.UnescapeDataString(uri.AbsolutePath));

            var dict = new Dictionary<string, double[]>();

            foreach (var feature in nycBoroughs.Features)
            {
                var ft = feature.FeatureType;
                if (ft == FeatureType.Point)
                {
                    var data = feature.DataRow;
                    var label = data[0]?.ToString() ?? "";
                    if (!dict.ContainsKey(label))
                    {
                        var coord = feature.ToShape().Vertices;
                        dict.Add(label, coord);
                    }
                    else
                    {

                    }
                }
                else
                {

                }
            }

            var wellsLayer = new VectorItemsLayer()
            {
                Data = CreateWellsData(dict),
                MinZoomLevel = 0,
                MaxZoomLevel = 13
            };

            mapControl1.Layers.Add(wellsLayer);

            //// Create a layer to show vector items.
            var itemsLayer = new VectorItemsLayer()
            {
                Data = CreateData(dict, wellStatuses, actualStatuses),
                Colorizer = CreateColorizer(),
                MinZoomLevel = 14
            };
            mapControl1.Layers.Add(itemsLayer);

            // Show a color legend.
            mapControl1.Legends.Add(new ColorListLegend() { Layer = itemsLayer });
        }

        private void Adapter_ItemsLoaded(object sender, ItemsLoadedEventArgs e)
        {
            foreach (var item in e.Items)
            {
                var attr = item.Attributes;
                var subItems = item.ClusteredItems;
                var r = item.Layer;
            }
        }

        private void MapControl1_MapItemClick(object sender, MapItemClickEventArgs e)
        {

        }



        private MapItemStorage CreateWellsData(IDictionary<string, double[]>  dict)
        {
            MapItemStorage storage = new MapItemStorage();
            foreach (var label in dict.Keys)
            {
                storage.Items.Add(new MapCallout() { Text = label, Location = new GeoPoint(dict[label][1], dict[label][0]) });
            }
            return storage;
        }


        private MapDataAdapterBase CreateShpData(string file)
        {
           // Create an adapter to load data from shapefile.
            ShapefileDataAdapter adapter = new ShapefileDataAdapter()
            {
                FileUri = new Uri(_baseUri,file)
            };

            return adapter;
        }



        // Create a storage to provide data for the vector layer.
        private IMapDataAdapter CreateData(IDictionary<string, double[]> dict, string[] wellStatuses, IGrouping<object, IDictionary<string, object>>[] actualStatuses) {
            MapItemStorage storage = new MapItemStorage();

            //Random rand = new Random();

            foreach (var actual in actualStatuses)
            {
                //var val = rand.Next(6);
                var label = actual.Key?.ToString();
                if(string.IsNullOrEmpty(label)|| !dict.ContainsKey(label)) continue;
                // Create a pie with several segments.
                var pie = new MapСamomile {Size = 30, Location = new GeoPoint(dict[label][1], dict[label][0]) };

                var include = false;
                var sb = new StringBuilder();
                sb.Append(label + Environment.NewLine);
                foreach (var wellStatus in actual)
                {
                    var t = wellStatus["Название статуса скважины"]?.ToString();
                    var val = wellStatus["Статус"] == null ? 0 : 1;
                    if (val == 1)
                    {
                        include = true;
                        sb.AppendFormat("{0} готово" + Environment.NewLine, t);
                    }
                    pie.Segments.Add(new СamomileSegment() { Argument = t, Value = val });
                }
                //sb.Append("Total: %V%");
                pie.ToolTipPattern = sb.ToString();

                //pie.Segments.Add(new СamomileSegment() {Argument = "Заказ/закуп комплектующих", Value = 1 }); //0
                //pie.Segments.Add(new СamomileSegment() {Argument = "Комплектующие у нас на складе", Value = val>0?1:0 }); //1
                //pie.Segments.Add(new СamomileSegment() {Argument = "Сборка автоматики, ЗРК, КИП, тестирование", Value = val > 1 ? 1 : 0 }); //2
                //pie.Segments.Add(new СamomileSegment() {Argument = "ОТК Новосибирск", Value = val > 2 ? 1 : 0 }); //3
                //pie.Segments.Add(new СamomileSegment() { Argument = "Склад в Новосибирске, готово к отправке", Value = val > 3 ? 1 : 0 });//4
                //pie.Segments.Add(new СamomileSegment() { Argument = "Отправлено из Новосибирска", Value = val > 4 ? 1 : 0 }); //5

                //pie.Segments[0].Attributes

                if(include)
                    storage.Items.Add(pie);
            }

            return storage;
        }

        // Create a colorizer to provide colors for bubble items.    
        private MapColorizer CreateColorizer() {
            СamomileColorizer colorizer = new СamomileColorizer();

            // Add colors to the colorizer.
            colorizer.ColorItems.Add(new ColorizerColorItem(Color.Coral));

            return colorizer;
        }
    }


    public class СamomileColorizer : GenericColorizer<ColorizerColorItem>, ILegendDataProvider
    {
        const bool DefaultAproximateColors = false;
        readonly DoubleCollection rangeStops;
        bool approximateColors = DefaultAproximateColors;

        protected internal new GenericColorizerItemCollection<ColorizerColorItem> ActualColorItems =>
            (GenericColorizerItemCollection<ColorizerColorItem>) base.ActualColorItems;

        [
            DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new GenericColorizerItemCollection<ColorizerColorItem> ColorItems =>
            (GenericColorizerItemCollection<ColorizerColorItem>) base.ColorItems;


        [
            Category(SRCategoryNames.Behavior), DefaultValue(DefaultAproximateColors)]
        public bool ApproximateColors
        {
            get { return approximateColors; }
            set
            {
                if (value == approximateColors)
                    return;
                approximateColors = value;
                OnColorizerChanged();
            }
        }

        public СamomileColorizer()
        {
            rangeStops = new DoubleCollection();
        }

        void OnValueProviderChanged(object sender, EventArgs e)
        {
            OnColorizerChanged();
        }

        Color GetColor(GenericColorizerItemCollection<ColorizerColorItem> coloredItems,int value)
        {
            ColorCollection colors = GetColorCollection(coloredItems);
            return value == 0 ? Color.Transparent : colors[0];
        }

        protected override ColorizerColorItem CreateColorItem(Color color)
        {
            return new ColorizerColorItem(color);
        }

        public override void ColorizeElement(IColorizerElement element)
        {
            if (element == null )
                return;
            //double value;// = ValueProvider.GetValue(element);
            if (element is СamomileSegment segment)
                element.ColorizerColor = GetColor(ActualColorItems, segment.Value);
        }

        public override string ToString()
        {
            return "(ChoroplethColorizer)";
        }

        public Color GetColor(int value)
        {
            return GetColor(ActualColorItems, value);
        }

        IList<MapLegendItemBase> ILegendDataProvider.CreateItems(MapLegendBase legend)
        {
            return CreateLegendItems();
        }

        protected virtual IList<MapLegendItemBase> CreateLegendItems()
        {
            ColorizerLegendItemsBuilderBase builder = CreateLegendItemBuilder();
            return builder != null ? builder.CreateItems() : new List<MapLegendItemBase>();
        }

        protected virtual ColorizerLegendItemsBuilderBase CreateLegendItemBuilder()
        {
            return null;
        }
    }



    public class SimpleColorizer : MapColorizer
    {
        private readonly Color _color;

        public SimpleColorizer(Color color)
        {
            _color = color;
        }

        public override void ColorizeElement(IColorizerElement element)
        {
           element.ColorizerColor = _color;
        }


    }
}
