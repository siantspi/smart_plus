using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader
{
    public class SmartPlusSystem
    {
        /// <summary>
        /// Unpacks the date time I.
        /// </summary>
        /// <param name="ymd">The ymd.</param>
        /// <returns></returns>
        /// 
        public static DateTime UnpackDateTimeI(Int32 ymd)
        {
            int year;
            int month;
            int day;
            int hour;
            int minute;
            int second;
            int hi = (ymd >> 16) & 0x0000ffff;
            int lo = ymd & 0x0000ffff;

            year = hi / 416;
            month = (hi % 416) / 32;
            day = (hi % 416) % 32;
            
            if (day == 0)
                day = 1;
            if (month > 12)
                month = 12;
            if (month < 1)
                month = 1;
            
            hour = lo / 1830;
            minute = (lo % 1830) / 30;
            second = ((lo % 1830) % 30) * 2;

            return new DateTime(year + 1900, month, day, hour, minute, second);
        }

        /// <summary>
        /// Unpacks the date from integer value.
        /// </summary>
        /// <param name="ymd">The ymd.</param>
        /// <returns></returns>
        public static DateTime UnpackDateI(Int32 ymd)
        {
            int year;
            int month;
            int day;

            year = ymd / 416;
            month = (ymd % 416) / 32;
            day = (ymd % 416) % 32;

            if (day == 0)
                day = 1;
            if (month > 12)
                month = 12;
            if (month < 1)
                month = 1;

            if (year > 0 && year < (DateTime.Now.Year - 1899) && month > 0 && month < 13 && day > 0 && day < 32)
            {
                return new DateTime(year + 1900, month, day, 0, 0, 0);
            }
            else
            {
                return DateTime.MinValue;
            }
        }
        public static UInt32 PackDate(DateTime dt)
        {
            int y = dt.Year;
            int m = dt.Month;
            int d = dt.Day;
            if (d > 31) d = 0;
            if (m > 12) m = 0;
            if (y > 1900) y = y - 1900;

            return (UInt32)(y * 416 + m * 32 + d);
        }

        private static UInt16 PackDateW(int y, int m, int d)
        {
            if (d > 31) d = 0;
            if (m > 12) m = 0;
            if (y > 1900) y = y - 1900;

            return (UInt16)(y*416 + m*32 + d);
        }

        private static UInt16 PackTime(int h, int m, int s)
        {
            if (h > 23) h = 0;
            if (m > 59) m = 0;
            if (s > 59) s = 0;

            return (UInt16)(h*1830 + m*30 + s/2);
        }
        public static UInt32 PackDateTime(DateTime dt)
        {
            UInt16 d = PackDateW(dt.Year, dt.Month, dt.Day);
            UInt16 t = PackTime(dt.Hour, dt.Minute, dt.Second);

            return (UInt32)((d << 16) | t);
        }
    }
}
