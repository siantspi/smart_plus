﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace DataLoader
{
    public class Cryptography
    {
        public class AES
        {
            byte[] IV = {2, 15, 42, 42, 15, 37, 08, 5, 20, 20, 20, 0, 0, 0, 0, 0 };
            RijndaelManaged aes;
            public AES()
            {
                aes = new RijndaelManaged();
                aes.BlockSize = 128;
                aes.KeySize = 128;
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                aes.IV = IV;
            }

            public MemoryStream Decrypt(MemoryStream Source, byte[] Key)
            {

                Source.Seek(0, SeekOrigin.Begin);
                CryptoStream cs = new CryptoStream(Source, aes.CreateDecryptor(Key, aes.IV), CryptoStreamMode.Read);
                byte[] temp = new byte[Source.Length];
                int bytes = cs.Read(temp, 0, temp.Length);
                cs.Close();
                MemoryStream res = new MemoryStream(bytes);
                res.Write(temp, 0, bytes);
                res.Seek(0, SeekOrigin.Begin);
                return res;
            }
            public MemoryStream Encrypt(MemoryStream Source, byte[] Key)
            {
                Source.Seek(0, SeekOrigin.Begin);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(Key, aes.IV), CryptoStreamMode.Write);
                Source.WriteTo(cs);
                cs.FlushFinalBlock();
                MemoryStream res = new MemoryStream();
                ms.WriteTo(res);
                res.Seek(0, SeekOrigin.Begin);
                cs.Close();
                return res;
            }
        }
        public static byte[] GetMD5hash(string input)
        {
            //MD5 md5Hasher = MD5.Create();
            //// Преобразуем входную строку в массив байт и вычисляем хэш
            //byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            //// Создаем новый Stringbuilder (Изменяемую строку) для набора байт
            //StringBuilder sBuilder = new StringBuilder();

            //// Преобразуем каждый байт хэша в шестнадцатеричную строку
            //for (int i = 0; i < data.Length; i++)
            //{
            //    //указывает, что нужно преобразовать элемент в шестнадцатиричную строку длиной в два символа
            //    sBuilder.Append(data[i].ToString("x2"));
            //}
            //byte[] result =  (sBuilder.ToString())
            return new byte[1];
        }
    }
}
