﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace DataLoader
{
    class MainSettings
    {
        FileInfo SettingsFileName;
        public List<string> LastOpenedFolders;

        public MainSettings()
        {
            LastOpenedFolders = new List<string>(5);
            SettingsFileName = new FileInfo(Application.StartupPath + "\\Settings.txt");
        }

        public void AddLastOpenFolder(string FolderPath)
        {
            if (LastOpenedFolders.IndexOf(FolderPath) == -1)
            {
                if (LastOpenedFolders.Count == 5)
                {
                    LastOpenedFolders.RemoveAt(0);
                }
                LastOpenedFolders.Add(FolderPath);
                WriteToFile();
            }
        }
        public void RemoveLastOpenFolder(string FolderPath)
        {
            LastOpenedFolders.Remove(FolderPath);
            WriteToFile();
        }


        public bool ReadFromFile()
        {
            if (File.Exists(SettingsFileName.FullName))
            {
                string[] StringList = File.ReadAllLines(SettingsFileName.FullName, Encoding.GetEncoding(1251));
                int i = 0;
                string str = string.Empty;
                while (i < StringList.Length)
                {
                    if (StringList[i].StartsWith("[") && StringList[i].EndsWith("]"))
                    {
                        try
                        {
                            str = StringList[i].Replace("[", "").Replace("]", "");
                            switch (str)
                            {
                                case "LastOpenedFolders":
                                    i++;
                                    while (i < StringList.Length)
                                    {
                                        if (StringList[i].StartsWith("["))
                                        {
                                            i--;
                                            break;
                                        }
                                        LastOpenedFolders.Add(StringList[i]);
                                        i++;
                                    }
                                    break;
                            }
                        }
                        catch
                        {
                            MessageBox.Show("Ошибка загрузки файла настроек.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    i++;
                }
                return true;
            }
            return false;
        }
        public void WriteToFile()
        {
            if (!Directory.Exists(SettingsFileName.DirectoryName)) Directory.CreateDirectory(SettingsFileName.DirectoryName);
            StreamWriter sw = new StreamWriter(SettingsFileName.FullName, false, Encoding.GetEncoding(1251));
            sw.WriteLine("[LastOpenedFolders]");
            for (int i = 0; (i < LastOpenedFolders.Count) && (i < 5); i++)
            {
                sw.WriteLine(LastOpenedFolders[i]);
            }
            sw.Close();
        }

        public string GetShortFolderName(string FolderName)
        {
            string str = FolderName;
            if (FolderName.Length > 30)
            {
                DirectoryInfo info = new DirectoryInfo(FolderName);
                int pos;
                pos = FolderName.IndexOf(@"\\");
                if(pos == -1) pos = FolderName.IndexOf(":\\");
                pos = FolderName.IndexOf("\\", pos + 2);
                if (pos == -1) pos = 15;
                str = string.Format("{0}\\...\\{1}", FolderName.Remove(pos, FolderName.Length - pos), info.Name);
            }
            return str;
        }
    }
}
