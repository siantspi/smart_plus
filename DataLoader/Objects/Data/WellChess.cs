﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Base;
using System.IO;


namespace DataLoader.Objects.Data
{
    #region CHESS
    public struct ChessDailyItem
    {
        public DateTime Date;
        public float Qliq;
        public float Qoil;
        public float QliqV;
        public float QoilV;
        public float Hdyn;
        public int StayTime;
        public int StayReason;
    }

    public sealed class WellChess : WellDataObject<ChessDailyItem>
    {
        public static int Version = 2;

        public WellChess() { }
        public WellChess(ChessDailyItem[] DailyItems)
        {
            Items = DailyItems;
        }

        public List<ChessDailyItem> GetItemsByDate(DateTime MinDate)
        {
            List<ChessDailyItem> items = new List<ChessDailyItem>();
            for (int i = Items.Length - 1; i >= 0; i--)
            {
                if (Items[i].Date <= MinDate) break;
                items.Insert(0, Items[i]);
            }
            return items;
        }
        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            int count = br.ReadInt32();
            ChessDailyItem[] items = new ChessDailyItem[count];
            for (int i = 0; i < items.Length; i++)
            {
                items[i].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                items[i].Qliq = br.ReadSingle();
                items[i].Qoil = br.ReadSingle();
                items[i].QliqV = br.ReadSingle();
                items[i].QoilV = br.ReadSingle();
                items[i].StayTime = br.ReadByte();
                items[i].Hdyn = br.ReadSingle();
                items[i].StayReason = br.ReadInt32();
            }
            SetItems(items);
            return true;
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            bw.Write(Items.Length);
            for (int i = 0; i < Items.Length; i++)
            {
                bw.Write(SmartPlusSystem.PackDateTime(Items[i].Date));
                bw.Write(Items[i].Qliq);
                bw.Write(Items[i].Qoil);
                bw.Write(Items[i].QliqV);
                bw.Write(Items[i].QoilV);
                bw.Write((byte)Items[i].StayTime);
                bw.Write(Items[i].Hdyn);
                bw.Write(Items[i].StayReason);
            }
            return true;
        }
    }
    #endregion

    #region CHESS INJECTION
    public struct ChessInjDailyItem
    {
        public DateTime Date;
        public float Wwat;
        public float Pust;
        public int CycleTime;
        public int StayTime;
        public int StayReason;
    }
    public sealed class WellChessInj : WellDataObject<ChessInjDailyItem>
    {
        public static int Version = 2;

        public WellChessInj() { }
        public WellChessInj(ChessInjDailyItem[] DailyItems)
        {
            Items = DailyItems;
        }

        public List<ChessInjDailyItem> GetItemsByDate(DateTime MinDate)
        {
            List<ChessInjDailyItem> items = new List<ChessInjDailyItem>();
            for (int i = Items.Length - 1; i >= 0; i--)
            {
                if (Items[i].Date < MinDate) break;
                items.Insert(0, Items[i]);
            }
            return items;
        }
        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            int count = br.ReadInt32();
            ChessInjDailyItem[] items = new ChessInjDailyItem[count];
            for (int i = 0; i < items.Length; i++)
            {
                items[i].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                items[i].Wwat = br.ReadSingle();
                items[i].Pust = br.ReadSingle();
                items[i].CycleTime = br.ReadByte();
                items[i].StayTime = br.ReadByte();
                items[i].StayReason = br.ReadInt32();
            }
            Items = items;
            return true;
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            bw.Write(Items.Length);
            for (int i = 0; i < Items.Length; i++)
            {
                bw.Write(SmartPlusSystem.PackDateTime(Items[i].Date));
                bw.Write(Items[i].Wwat);
                bw.Write(Items[i].Pust);
                bw.Write((byte)Items[i].CycleTime);
                bw.Write((byte)Items[i].StayTime);
                bw.Write(Items[i].StayReason);
            }
            return true;
        }
    }
    #endregion
}
