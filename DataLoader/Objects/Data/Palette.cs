﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace DataLoader.Objects.Data
{
    public struct PalettePoint
    {
        public double X;
        public Color Y;

        public static PalettePoint Empty
        {
            get
            {
                PalettePoint pp;
                pp.X = 0;
                pp.Y = Color.White;
                return pp;
            }
        }
    }
    public sealed class Palette
    {
        List<PalettePoint> points;

        public Palette()
        {
            points = new List<PalettePoint>();
        }
        public Palette(Palette pal)
            : this()
        {
            for (int i = 0; i < pal.points.Count; i++)
            {
                Add(pal[i].X, pal[i].Y);
            }
        }
        public int Count { get { return points.Count; } }

        public static Palette DefaultPalette(double Min, double Max)
        {
            Palette palette = new Palette();
            if (Min < Max)
            {
                palette.Add(Min, Color.Blue);
                palette.Add(Min + (Max - Min) / 4, Color.FromArgb(0, 255, 255));
                palette.Add(Min + (Max - Min) / 2, Color.FromArgb(0, 255, 0));
                palette.Add(Min + 3 * (Max - Min) / 4, Color.FromArgb(255, 255, 0));
                palette.Add(Max, Color.Red);
            }
            return palette;
        }
        public void Add(double X, Color Y)
        {
            PalettePoint pp;
            pp.X = Math.Round(X, 2);
            pp.Y = Y;
            points.Add(pp);
        }
        public void Insert(int index, double X, Color Y)
        {
            PalettePoint pp;
            pp.X = Math.Round(X, 2);
            pp.Y = Y;
            points.Insert(index, pp);
        }
        public void RemoveAt(int index)
        {
            if (index < points.Count)
            {
                points.RemoveAt(index);
            }
        }
        public void Set(int index, double X, Color Y)
        {
            PalettePoint pp = points[index];
            pp.X = X;
            pp.Y = Y;
            points[index] = pp;
        }
        public void Clear()
        {
            points.Clear();
        }
        public PalettePoint this[int index]
        {
            get
            {
                if (index < points.Count)
                    return points[index];
                else
                    return PalettePoint.Empty;
            }
        }
        public Color GetColorByX(double X)
        {
            int i;
            int r1, g1, b1, r2, g2, b2;
            double delimeter;
            if (points.Count > 1)
            {
                if (X > this[this.Count - 1].X - 0.001) X = this[this.Count - 1].X;
                if (X < this[0].X + 0.001) X = this[0].X;

                for (i = this.Count - 1; i > 0; i--)
                {
                    if ((this[i - 1].X <= X) && (X <= this[i].X))
                    {
                        r1 = this[i - 1].Y.R;
                        g1 = this[i - 1].Y.G;
                        b1 = this[i - 1].Y.B;
                        r2 = this[i].Y.R;
                        g2 = this[i].Y.G;
                        b2 = this[i].Y.B;
                        delimeter = 0;
                        if (this[i].X - this[i - 1].X != 0) delimeter = 1 / (this[i].X - this[i - 1].X);
                        Color clr = Color.FromArgb(255, (int)(r1 + (r2 - r1) * (X - this[i - 1].X) * delimeter),
                                                       (int)(g1 + (g2 - g1) * (X - this[i - 1].X) * delimeter),
                                                       (int)(b1 + (b2 - b1) * (X - this[i - 1].X) * delimeter));
                        return clr;
                    }
                }
            }
            return Color.White;
        }
        public void Invert()
        {
            if (Count > 0)
            {
                Color[] clr = new Color[Count];
                for (int i = 0; i < Count; i++)
                {
                    clr[i] = points[Count - 1 - i].Y;
                }
                for (int i = 0; i < Count; i++)
                {
                    Set(i, points[i].X, clr[i]);
                }
            }
        }
        public void ReadFromCache(BinaryReader br)
        {
            if (br != null)
            {
                int i, len = br.ReadInt32();
                double X;
                int clr;
                for (i = 0; i < len; i++)
                {
                    X = br.ReadDouble();
                    clr = br.ReadInt32();
                    this.Add(X, Color.FromArgb(clr));
                }
            }
        }
        public void WriteToCache(BinaryWriter bw)
        {
            if (bw != null)
            {
                bw.Write(points.Count);
                for (int i = 0; i < points.Count; i++)
                {
                    bw.Write(points[i].X);
                    bw.Write(points[i].Y.ToArgb());
                }
            }
        }

        public bool Equals(Palette pal)
        {
            if ((pal == null) || (this.Count != pal.Count))
            {
                return false;
            }
            else
            {
                for (int i = 0; i < Count; i++)
                {
                    if ((this[i].X != pal[i].X) || (this[i].Y.ToArgb() != pal[i].Y.ToArgb()))
                    {
                        return false;
                    }
                }
                return true;
            }
        }
    }
}
