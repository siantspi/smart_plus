﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Base;
using System.IO;

namespace DataLoader.Objects.Data
{
    public struct WellResearchItem
    {
        public DateTime Date;
        public ushort StratumCode;
        public ushort ResearchCode;
        public float PressZatr;
        public float PressWaterSurface;
        public float PressPerforation;
        public int TimeDelay;
    }
    public sealed class WellResearch : WellDataObject<WellResearchItem>
    {
        public static int VERSION = 0;
        public bool IsServerStratumCode = false;

        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            try
            {
                int count = br.ReadInt32();
                WellResearchItem[] newItems = new WellResearchItem[count];
                for (int i = 0; i < count; i++)
                {
                    newItems[i].Date = DateTime.FromOADate(br.ReadDouble());
                    newItems[i].StratumCode = br.ReadUInt16();
                    newItems[i].ResearchCode = br.ReadUInt16();
                    newItems[i].PressZatr = br.ReadSingle();
                    newItems[i].PressWaterSurface = br.ReadSingle();
                    newItems[i].PressPerforation = br.ReadSingle();
                    newItems[i].TimeDelay = br.ReadInt32();
                }
                SetItems(newItems);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(Count);
                for (int i = 0; i < Items.Length; i++)
                {
                    bw.Write(Items[i].Date.ToOADate());
                    bw.Write(Items[i].StratumCode);
                    bw.Write(Items[i].ResearchCode);
                    bw.Write(Items[i].PressZatr);
                    bw.Write(Items[i].PressWaterSurface);
                    bw.Write(Items[i].PressPerforation);
                    bw.Write(Items[i].TimeDelay);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
