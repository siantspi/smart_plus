﻿using DataLoader.Objects.Base;
using DataLoader.Objects.Project;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.Objects.Data
{
    public enum ContoursOperation
    {
        /// <summary>
        /// Объединение
        /// </summary>
        Union,
        /// <summary>
        /// Вычитание
        /// </summary>
        Difference,
        /// <summary>
        /// Пересечение
        /// </summary>
        Crossing,
        /// <summary>
        /// Симметричное пересечение
        /// </summary>
        SymmetricDifference
    }
    public sealed class Polygon
    {
        PointsCollection Points;
        EdgesCollection Edges;
        bool ResultReady;

        internal struct PolyPoint
        {
            public int Index;
            public List<int> EdgesInd;
            public PointD Point;
            public bool Used;
        }
        internal class Edge
        {
            public int Cntr;
            public double Angle;
            public int indA;
            public int indB;
            public int Left;
            public int Right;
            public bool Used;
            public bool Selected;
            public Edge()
            {
                this.Cntr = 0;
                this.Angle = 0;
                this.indA = 0;
                this.indB = 0;
                this.Left = 0;
                this.Right = 0;
                this.Selected = false;
                this.Used = false;
            }
            public Edge Clone()
            {
                Edge newEdge = new Edge();
                newEdge.Angle = this.Angle;
                newEdge.Cntr = this.Cntr;
                newEdge.indA = this.indA;
                newEdge.indB = this.indB;
                newEdge.Left = this.Left;
                newEdge.Right = this.Right;
                newEdge.Selected = this.Selected;
                newEdge.Used = this.Used;
                return newEdge;
            }
            public void Invert()
            {
                int t = indA;
                indA = indB;
                indB = t;
                t = Left;
                Left = Right;
                Right = t;
                Cntr++;
                if (Cntr > 1) Cntr = 0;
            }
        }
        internal class PointsCollection
        {
            private List<PolyPoint> Points;

            public PointsCollection(int Capacity)
            {
                Points = new List<PolyPoint>(Capacity);
            }

            public int Count { get { return Points.Count; } }

            public void Trim()
            {
                Points.TrimExcess();
            }
            public void Clear()
            {
                Points.Clear();
            }
            public PolyPoint this[int index]
            {
                get { return Points[index]; }
                set { Points[index] = value; }
            }
            public int AddPoint(PointD Point)
            {
                PolyPoint pt;
                pt.Used = false;
                pt.EdgesInd = new List<int>(2);
                pt.Point = Point;
                pt.Index = Points.Count;
                Points.Add(pt);
                return pt.Index;
            }

            public int GetPointIndex(PointD Point)
            {
                return GetPointIndex(Point.X, Point.Y);
            }
            public int GetPointIndex(double X, double Y)
            {
                for (int i = 0; i < Points.Count; i++)
                {
                    if ((Math.Abs(Points[i].Point.X - X) < 0.000001) && (Math.Abs(Points[i].Point.Y - Y) < 0.000001))
                    {
                        return i;
                    }
                }
                return -1;
            }
        }
        internal class EdgesCollection
        {
            private PointsCollection Points;
            public List<Edge> Edges;
            public int Count { get { return Edges.Count; } }

            public void Trim()
            {
                Edges.TrimExcess();
            }
            public EdgesCollection(PointsCollection Points, int Capacity)
            {
                Edges = new List<Edge>(Capacity);
                this.Points = Points;
            }
            public Edge this[int index]
            {
                get { return Edges[index]; }
                set { Edges[index] = value; }
            }
            public int GetEdgeIndex(int Start, int End)
            {
                for (int i = 0; i < Edges.Count; i++)
                {
                    if ((Edges[i].indA == Start) && (Edges[i].indB == End))
                    {
                        return i;
                    }
                }
                return -1;
            }
            private void SetEdgeAngle(int EdgeIndex)
            {
                Edge edge = Edges[EdgeIndex];
                double angle = 0;
                PointD point1 = Points[edge.indA].Point, point2 = Points[edge.indB].Point;
                if (Math.Abs(point1.X - point2.X) < 0.000001) // x1 == x2
                {
                    angle = Math.PI / 2;
                    if (point1.Y > point2.Y)
                    {
                        angle = 3 * Math.PI / 2;
                    }
                }
                else
                {
                    angle = Math.Atan((point2.Y - point1.Y) / (point2.X - point1.X));
                    if (point2.X < point1.X)
                    {
                        angle += Math.PI;
                    }
                    else if (point1.Y > point2.Y)
                    {
                        angle += 2 * Math.PI;
                    }
                }
                edge.Angle = angle;
                //Edges[EdgeIndex] = edge;
            }
            public void AddEdge(Edge edge)
            {
                Edges.Add(edge);
            }
            public void AddEdge(int NumCntr, int Start, int End, bool ClockWise)
            {
                Edge edge = new Edge();
                PolyPoint pt;
                int ind = GetEdgeIndex(Start, End);
                if (ind == -1)
                {
                    edge.Angle = 0;
                    edge.Cntr = NumCntr;
                    edge.Used = false;
                    edge.Selected = false;
                    edge.indA = Start;
                    pt = Points[Start];
                    if (pt.EdgesInd == null) pt.EdgesInd = new List<int>();
                    pt.EdgesInd.Add(Edges.Count);

                    edge.indB = End;
                    pt = Points[End];
                    if (pt.EdgesInd == null) pt.EdgesInd = new List<int>();
                    pt.EdgesInd.Add(Edges.Count);

                    edge.Left = -2;
                    edge.Right = NumCntr;
                    Edges.Add(edge);
                }
                else if (Edges[ind].Cntr != NumCntr)
                {
                    edge = Edges[ind];
                    edge.Cntr = -1;
                    if (Edges[ind].Right == -2) edge.Right = NumCntr; else edge.Right = -1;
                    //Edges[ind] = edge;
                }
            }
            public bool InsertPoint(int IndexEdge, int IndexPoint)
            {
                bool result = false;
                if (IndexEdge < Edges.Count)
                {
                    Edge InitEdge = Edges[IndexEdge];
                    if ((Edges[IndexEdge].indA != IndexPoint) && (Edges[IndexEdge].indB != IndexPoint))
                    {
                        Edge InsEdge = new Edge();
                        InsEdge.Angle = 0;
                        InsEdge.Cntr = InitEdge.Cntr;
                        InsEdge.Left = InitEdge.Left;
                        InsEdge.Right = InitEdge.Right;
                        InsEdge.Used = false;
                        InsEdge.Selected = false;
                        int start = InitEdge.indA, end = IndexPoint;

                        int ind = GetEdgeIndex(IndexPoint, InitEdge.indB);
                        if (ind == -1)
                        {
                            InitEdge.indA = IndexPoint;
                        }
                        else
                        {
                            InitEdge = Edges[ind];
                            InitEdge.Cntr = -1;
                            if (Edges[ind].Right == -2) InitEdge.Right = InitEdge.Cntr; else InitEdge.Right = -1;
                            //Edges[ind] = InitEdge;
                            Edges.RemoveAt(IndexEdge);
                        }

                        ind = GetEdgeIndex(start, end);
                        if (ind == -1)
                        {
                            InsEdge.indA = start;
                            InsEdge.indB = end;
                            Edges.Insert(IndexEdge, InsEdge);
                        }
                        else
                        {
                            InsEdge = Edges[ind];
                            InsEdge.Cntr = -1;
                            if (Edges[ind].Right == -2) InsEdge.Right = InitEdge.Cntr; else InsEdge.Right = -1;
                            //Edges[ind] = InsEdge;
                        }
                        result = true;
                    }
                }
                return result;
            }

            private int IntersectEdges(Edge edge1, Edge edge2)
            {
                int result = -1;
                if ((Points.Count > 0) && (edge1.indA < Points.Count) && (edge1.indB < Points.Count) && (edge2.indA < Points.Count) && (edge2.indB < Points.Count))
                {
                    if ((edge1.indA == edge2.indA) || (edge1.indA == edge2.indB))
                    {
                        if (Geometry.IsPointInLine(Points[edge2.indA].Point, Points[edge2.indB].Point, Points[edge1.indB].Point))
                        {
                            return edge1.indB;
                        }
                        else
                        {
                            return edge1.indA;
                        }
                    }
                    else if ((edge1.indB == edge2.indA) || (edge1.indB == edge2.indB))
                    {
                        if (Geometry.IsPointInLine(Points[edge2.indA].Point, Points[edge2.indB].Point, Points[edge1.indA].Point))
                        {
                            return edge1.indA;
                        }
                        else
                        {
                            return edge1.indB;
                        }
                    }
                    else
                    {
                        double minX1 = Points[edge1.indA].Point.X, minY1 = Points[edge1.indA].Point.Y;
                        double maxX1 = Points[edge1.indB].Point.X, maxY1 = Points[edge1.indB].Point.Y;
                        double minX2 = Points[edge2.indA].Point.X, minY2 = Points[edge2.indA].Point.Y;
                        double maxX2 = Points[edge2.indB].Point.X, maxY2 = Points[edge2.indB].Point.Y;
                        if (maxX1 < minX1) { minX1 = Points[edge1.indB].Point.X; maxX1 = Points[edge1.indA].Point.X; }
                        if (maxY1 < minY1) { minY1 = Points[edge1.indB].Point.Y; maxY1 = Points[edge1.indA].Point.Y; }
                        if (maxX2 < minX2) { minX2 = Points[edge2.indB].Point.X; maxX2 = Points[edge2.indA].Point.X; }
                        if (maxY2 < minY2) { minY2 = Points[edge2.indB].Point.Y; maxY2 = Points[edge2.indA].Point.Y; }

                        if ((minX1 < maxX2) && (minX2 < maxX1) && (minY1 < maxY2) && (minY2 < maxY1))
                        {
                            PointD IntPt = Geometry.GetIntersectLinesPoint(Points[edge1.indA].Point, Points[edge1.indB].Point, Points[edge2.indA].Point, Points[edge2.indB].Point);

                            if ((IntPt.X != Constant.DOUBLE_NAN) && (IntPt.Y != Constant.DOUBLE_NAN))
                            {
                                result = Points.GetPointIndex(IntPt);
                                if (result == -1) result = Points.AddPoint(IntPt);
                                return result;
                            }
                        }
                        if (Geometry.IsPointInLine(Points[edge1.indA].Point, Points[edge1.indB].Point, Points[edge2.indA].Point))
                        {
                            return edge2.indA;
                        }
                        else if (Geometry.IsPointInLine(Points[edge1.indA].Point, Points[edge1.indB].Point, Points[edge2.indB].Point))
                        {
                            return edge2.indB;
                        }
                        else if (Geometry.IsPointInLine(Points[edge2.indA].Point, Points[edge2.indB].Point, Points[edge1.indA].Point))
                        {
                            return edge1.indA;
                        }
                        else if (Geometry.IsPointInLine(Points[edge2.indA].Point, Points[edge2.indB].Point, Points[edge1.indB].Point))
                        {
                            return edge1.indB;
                        }
                    }
                }
                return result;
            }
            public void AddIntersectPoints(bool WithMainContours)
            {
                int i, j, ind, k, count1;
                bool result = false, res1 = false, res2 = false;
                for (i = 0; i < Edges.Count; i++)
                {
                    if ((WithMainContours && (Edges[i].Cntr != 1)) || (!WithMainContours && (Edges[i].Cntr != 0)))
                    {
                        for (j = 0; j < Edges.Count; j++)
                        {
                            if ((Edges[j].Cntr != 0) && (i != j))
                            {
                                ind = IntersectEdges(Edges[i], Edges[j]);
                                if (ind != -1)
                                {
                                    count1 = Edges.Count;
                                    res1 = InsertPoint(i, ind);
                                    if ((Edges.Count > count1) && (i <= j)) k = j + 1; else k = j;
                                    res2 = InsertPoint(k, ind);
                                    if (!result) result = res1 || res2;
                                }
                            }
                        }
                    }
                }

                if (result)
                {
                    for (i = 0; i < Points.Count; i++)
                    {
                        k = 0;
                        Points[i].EdgesInd.Clear();
                        for (j = 0; j < Edges.Count; j++)
                        {
                            if ((Edges[j].indA == i) || (Edges[j].indB == i))
                            {
                                Points[i].EdgesInd.Add(j);
                            }
                        }
                    }
                }
            }
            public void SortPointsAngle()
            {
                int i, j;
                for (i = 0; i < Points.Count; i++)
                {
                    PolyPoint pt = Points[i];
                    int k;
                    double angle, angle2;
                    if (pt.EdgesInd.Count > 2)
                    {
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            SetEdgeAngle(pt.EdgesInd[j]);
                        }
                        List<int> newEdges = new List<int>();
                        while (newEdges.Count < pt.EdgesInd.Count)
                        {
                            k = -1;
                            angle = Constant.DOUBLE_NAN;
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {
                                angle2 = Edges[pt.EdgesInd[j]].Angle;
                                if (Edges[pt.EdgesInd[j]].indA != i)
                                {
                                    if (angle2 > Math.PI) angle2 -= Math.PI; else angle2 += Math.PI;
                                }

                                if ((newEdges.IndexOf(pt.EdgesInd[j]) == -1) &&
                                    ((angle == Constant.DOUBLE_NAN) || (angle > angle2)))
                                {
                                    angle = angle2;
                                    k = j;
                                }
                            }
                            if (k != -1) newEdges.Add(pt.EdgesInd[k]);
                        }
                        pt.EdgesInd = newEdges;
                        Points[i] = pt;
                    }
                }
            }

            private void GetLeftRight(int PointIndex, int EdgeIndex, bool IsIdemCntr, out int Left, out int Right)
            {
                PolyPoint pt = Points[PointIndex];
                Edge edge = Edges[pt.EdgesInd[EdgeIndex]];
                int i, j, k = -1, lInd, rInd;
                Left = -2; Right = -2;

                lInd = -1;
                i = EdgeIndex + 1;
                if (i == pt.EdgesInd.Count) i = 0;
                while (i != EdgeIndex)
                {
                    if (((IsIdemCntr) && (Edges[pt.EdgesInd[i]].Cntr == edge.Cntr)) ||
                        ((!IsIdemCntr) && (Edges[pt.EdgesInd[i]].Cntr != edge.Cntr)))
                    {
                        lInd = pt.EdgesInd[i];
                        break;
                    }
                    i++;
                    if (i == pt.EdgesInd.Count) i = 0;
                }
                rInd = -1;
                i = EdgeIndex - 1;
                if (i < 0) i = pt.EdgesInd.Count - 1;
                while (i != EdgeIndex)
                {
                    if (((IsIdemCntr) && (Edges[pt.EdgesInd[i]].Cntr == edge.Cntr)) ||
                        ((!IsIdemCntr) && (Edges[pt.EdgesInd[i]].Cntr != edge.Cntr)))
                    {
                        rInd = pt.EdgesInd[i];
                        break;
                    }
                    i--;
                    if (i < 0) i = pt.EdgesInd.Count - 1;
                }
                if ((lInd > -1) && (rInd > -1))
                {
                    if ((Edges[lInd].indA == edge.indA) && (Edges[lInd].indB == edge.indB))
                    {
                        Left = Edges[lInd].Left;
                        Right = Edges[lInd].Right;
                    }
                    else if ((Edges[rInd].indA == edge.indA) && (Edges[rInd].indB == edge.indB))
                    {
                        Left = Edges[rInd].Left;
                        Right = Edges[rInd].Right;
                    }
                    else if ((Edges[lInd].indA == edge.indB) && (Edges[lInd].indB == edge.indA))
                    {
                        Left = Edges[lInd].Right;
                        Right = Edges[lInd].Left;
                    }
                    else if ((Edges[rInd].indA == edge.indB) && (Edges[rInd].indB == edge.indA))
                    {
                        Left = Edges[rInd].Right;
                        Right = Edges[rInd].Left;
                    }
                    else
                    {
                        if (Edges[lInd].indA == pt.Index) Left = Edges[lInd].Right; else Left = Edges[lInd].Left;
                        if (Edges[rInd].indA == pt.Index) Right = Edges[rInd].Left; else Right = Edges[rInd].Right;
                        if (edge.indB == pt.Index)
                        {
                            k = Left;
                            Left = Right;
                            Right = k;
                        }
                    }
                }
            }
            private List<int> GetEdgesTestEight(int index1, int index2, ref List<Contour> main)
            {
                int count, left, right;
                List<int> edgeList = new List<int>();
                if ((index1 < Edges.Count) && (index2 < Edges.Count))
                {
                    if (Edges[index1].indA == Edges[index2].indB)
                    {
                        int lastIndex;
                        Edge edge;
                        PolyPoint pt;
                        int i = index1, j, k;
                        edge = Edges[index1];
                        lastIndex = index1;
                        while (edge.indB != Edges[index2].indB)
                        {
                            pt = Points[edge.indB];
                            if ((pt.EdgesInd.Count > 3) && (!pt.Used))
                            {
                                count = 0;
                                for (j = 0; j < pt.EdgesInd.Count; j++)
                                {
                                    if ((Edges[pt.EdgesInd[j]].Cntr != 0) && (!Edges[pt.EdgesInd[j]].Used))
                                    {
                                        count++;
                                    }
                                }
                                if (count == 4)
                                {
                                    k = -1;
                                    for (j = 0; j < pt.EdgesInd.Count; j++)
                                    {

                                        if ((Edges[pt.EdgesInd[j]].Cntr != 0) && (!Edges[pt.EdgesInd[j]].Used))
                                        {
                                            k = j;
                                            break;
                                        }
                                    }
                                    if (k != -1)
                                    {
                                        GetLeftRight(pt.Index, k, true, out left, out right);
                                        if (left == right) // нашли 8
                                        {
                                            TurnInsideOut(pt.Index, ref  main);
                                            edgeList.Clear();
                                            return edgeList;
                                        }
                                    }
                                }
                            }
                            edgeList.Add(lastIndex);

                            if (pt.EdgesInd.Count == 2)
                            {
                                if (Edges[pt.EdgesInd[0]].indA == pt.Index)
                                {
                                    edge = Edges[pt.EdgesInd[0]];
                                    lastIndex = pt.EdgesInd[0];
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[1]];
                                    lastIndex = pt.EdgesInd[1];
                                }
                            }
                            else
                            {
                                k = -1;
                                int lInd = -1, rInd = -1;
                                for (j = 0; j < pt.EdgesInd.Count; j++)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == edge.indA) &&
                                        (Edges[pt.EdgesInd[j]].indB == edge.indB))
                                    {
                                        k = j;
                                        break;
                                    }
                                }
                                j = k + 1;
                                lInd = -1;
                                if (j == pt.EdgesInd.Count) j = 0;
                                while (j != k)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == pt.Index) &&
                                        (Edges[pt.EdgesInd[j]].Cntr != 0) &&
                                        (edgeList.IndexOf(pt.EdgesInd[j]) == -1))
                                    {
                                        lInd = j;
                                        break;
                                    }
                                    j++;
                                    if (j == pt.EdgesInd.Count) j = 0;
                                }
                                j = k - 1;
                                rInd = -1;
                                if (j < 0) j = pt.EdgesInd.Count - 1;
                                while (j != k)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == pt.Index) &&
                                        (Edges[pt.EdgesInd[j]].Cntr != 0) &&
                                        (edgeList.IndexOf(pt.EdgesInd[j]) == -1))
                                    {
                                        rInd = j;
                                        break;
                                    }
                                    j--;
                                    if (j < 0) j = pt.EdgesInd.Count - 1;
                                }
                                if (Math.Abs(lInd - k) <= Math.Abs(rInd - k))
                                {
                                    edge = Edges[pt.EdgesInd[lInd]];
                                    lastIndex = pt.EdgesInd[lInd];
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[rInd]];
                                    lastIndex = pt.EdgesInd[rInd];
                                }
                            }
                        }
                        if (edge.indB == Edges[index2].indB) edgeList.Add(lastIndex);
                        if ((edge.indB == Edges[index2].indB) && (lastIndex != index2))
                        {
                            edgeList.Clear();
                        }
                    }
                }
                return edgeList;
            }
            private void TurnInsideOut(int PointIndex, ref List<Contour> main)
            {
                int i, j, k, lInd, rInd;
                int ind, ind2;
                Edge edge;
                bool IsNeedSort = false;

                PolyPoint pt;
                System.Collections.ArrayList list = new System.Collections.ArrayList();
                Contour cntr;
                List<int> edgeList = null;
                pt = Points[PointIndex];
                pt.Used = true;
                Points[PointIndex] = pt;
                for (j = 0; j < pt.EdgesInd.Count; j++)
                {
                    if ((!Edges[pt.EdgesInd[j]].Used) &&
                         (Edges[pt.EdgesInd[j]].Cntr != 0) &&
                         (Edges[pt.EdgesInd[j]].indA == pt.Index))
                    {
                        lInd = pt.EdgesInd[j];
                        rInd = -1;
                        k = j + 1;
                        if (k == pt.EdgesInd.Count) k = 0;
                        while (k != j)
                        {
                            if ((!Edges[pt.EdgesInd[k]].Used) && (Edges[pt.EdgesInd[k]].indB == pt.Index))
                            {
                                rInd = pt.EdgesInd[k];
                                edgeList = GetEdgesTestEight(lInd, rInd, ref main);
                                if (edgeList.Count > 0) break;
                            }
                            k++;
                            if (k == pt.EdgesInd.Count) k = 0;
                        }
                        if ((edgeList != null) && (edgeList.Count > 0))
                        {
                            edge = Edges[lInd]; edge.Used = true; //Edges[lInd] = edge;
                            edge = Edges[rInd]; edge.Used = true; //Edges[rInd] = edge;
                            list.Add(edgeList);
                        }
                    }
                }
                if (list.Count > 0)
                {
                    for (j = 0; j < list.Count; j++)
                    {
                        edgeList = (List<int>)list[j];
                        cntr = GetContourByEdgeList(edgeList);
                        if (!cntr.GetClockWiseByX(true))
                        {
                            for (lInd = 0, rInd = edgeList.Count - 1; lInd <= rInd; lInd++, rInd--)
                            {
                                ind = Points[Edges[edgeList[lInd]].indA].EdgesInd.IndexOf(edgeList[lInd]);
                                ind2 = Points[Edges[edgeList[lInd]].indA].EdgesInd.IndexOf(edgeList[rInd]);
                                if ((ind != -1) && (ind2 == -1))
                                {
                                    Points[Edges[edgeList[lInd]].indA].EdgesInd[ind] = edgeList[rInd];
                                }
                                ind = Points[Edges[edgeList[lInd]].indB].EdgesInd.IndexOf(edgeList[lInd]);
                                ind2 = Points[Edges[edgeList[lInd]].indB].EdgesInd.IndexOf(edgeList[rInd]);
                                if ((ind != -1) && (ind2 == -1))
                                {
                                    Points[Edges[edgeList[lInd]].indB].EdgesInd[ind] = edgeList[rInd];
                                }

                                ind = Points[Edges[edgeList[rInd]].indA].EdgesInd.IndexOf(edgeList[rInd]);
                                ind2 = Points[Edges[edgeList[rInd]].indA].EdgesInd.IndexOf(edgeList[lInd]);
                                if ((ind != -1) && (ind2 == -1))
                                {
                                    Points[Edges[edgeList[rInd]].indA].EdgesInd[ind] = edgeList[lInd];
                                }
                                ind = Points[Edges[edgeList[rInd]].indB].EdgesInd.IndexOf(edgeList[rInd]);
                                ind2 = Points[Edges[edgeList[rInd]].indB].EdgesInd.IndexOf(edgeList[lInd]);
                                if ((ind != -1) && (ind2 == -1))
                                {
                                    Points[Edges[edgeList[rInd]].indB].EdgesInd[ind] = edgeList[lInd];
                                }

                                ind = Edges[edgeList[lInd]].indA;
                                ind2 = Edges[edgeList[lInd]].indB;


                                Edges[edgeList[lInd]].indA = Edges[edgeList[rInd]].indB;
                                Edges[edgeList[lInd]].indB = Edges[edgeList[rInd]].indA;

                                Edges[edgeList[rInd]].indA = ind2;
                                Edges[edgeList[rInd]].indB = ind;

                                //edge2.indA = Edges[edgeList[lInd]].indB;
                                //edge2.indB = Edges[edgeList[lInd]].indA;
                                //Edges[edgeList[lInd]] = edge;
                                //Edges[edgeList[rInd]] = edge2;
                                IsNeedSort = true;
                            }
                        }
                    }
                }
                for (j = 0; j < list.Count; j++) ((List<int>)list[j]).Clear();
                list.Clear();
                k = -1;
                if (main == null) main = GetMainContours();
                for (j = 0; j < main.Count; j++)
                {
                    if (main[j].GetClockWiseByX(true) &&
                        main[j].PointBodyEntry(pt.Point.X, pt.Point.Y))
                    {
                        k = 0;
                        break;
                    }
                }
                for (j = 0; j < pt.EdgesInd.Count; j++)
                {
                    edge = Edges[pt.EdgesInd[j]];
                    if (k == 0)
                    {
                        if (edge.Left == -2) edge.Left = 0; else edge.Left = -1;
                        if (edge.Right == -2) edge.Right = 0; else edge.Right = -1;
                    }
                    edge.Used = false;
                    //Edges[pt.EdgesInd[j]] = edge;
                }
                pt.Used = false;
                Points[pt.Index] = pt;
                if (IsNeedSort) SortPointsAngle();
            }
            public void SetEdgesMarks()
            {
                int LastCntrIndex = GetNextCntrNumber() - 1;
                if (LastCntrIndex < 0) LastCntrIndex = 0;

                PolyPoint pt;
                int i, j, k, count;
                int left, right;
                Edge edge, edge2;

                #region Проверяем добавляемый контур на самопересечение
                for (i = 0; i < Points.Count; i++)
                {
                    pt = Points[i];
                    if (pt.EdgesInd.Count > 3)
                    {
                        count = 0;
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            if ((Edges[pt.EdgesInd[j]].Cntr != 0) && (!Edges[pt.EdgesInd[j]].Used))
                            {
                                count++;
                            }
                        }
                        if (count == 4)
                        {
                            k = -1;
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {

                                if ((Edges[pt.EdgesInd[j]].Cntr != 0) && (!Edges[pt.EdgesInd[j]].Used))
                                {
                                    k = j;
                                    break;
                                }
                            }
                            if (k != -1)
                            {
                                GetLeftRight(i, k, true, out left, out right);
                                if (left == right) // нашли 8
                                {
                                    List<Contour> main = null;
                                    TurnInsideOut(pt.Index, ref  main);
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Выделяем все ребра в точках пересечений многоугольников
                for (i = 0; i < Points.Count; i++)
                {
                    pt = Points[i];
                    if (pt.EdgesInd.Count > 2)
                    {
                        count = 0;
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            if ((Edges[pt.EdgesInd[j]].Cntr == Edges[pt.EdgesInd[0]].Cntr) ||
                                (Edges[pt.EdgesInd[j]].Cntr == -1))
                            {
                                count++;
                            }
                        }
                        if (count != pt.EdgesInd.Count)
                        {
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {
                                edge = Edges[pt.EdgesInd[j]];
                                if (!edge.Used)
                                {
                                    if (edge.Cntr != -1)
                                    {
                                        GetLeftRight(i, j, false, out left, out right);
                                        if ((left != -2) && (left != edge.Cntr))
                                        {
                                            if (edge.Left == -2)
                                            {
                                                edge.Left = left;
                                            }
                                            else if (left != edge.Left)
                                            {
                                                edge.Left = -1;
                                            }
                                        }
                                        if ((right != -2) && (right != edge.Cntr))
                                        {
                                            if (edge.Right == -2)
                                            {
                                                edge.Right = right;
                                            }
                                            else if (right != edge.Right)
                                            {
                                                edge.Right = -1;
                                            }
                                        }
                                    }
                                    edge.Used = true;
                                    //Edges[pt.EdgesInd[j]] = edge;
                                }
                            }
                        }
                        count = 0;
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            if (Edges[pt.EdgesInd[j]].Cntr == 1) count++;
                        }
                        if (count > 2)
                        {
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {
                                edge = Edges[pt.EdgesInd[j]];
                                if ((edge.Cntr == 1) && (edge.Left == -2))
                                {
                                    for (k = 0; k < pt.EdgesInd.Count; k++)
                                    {
                                        edge2 = Edges[pt.EdgesInd[k]];
                                        if ((j != k) && (edge2.Cntr == 1) &&
                                            (edge.indB == edge2.indA) && (edge.indA == edge2.indB))
                                        {
                                            edge.Left = 1;
                                            //Edges[pt.EdgesInd[j]] = edge;
                                            edge2.Left = 1;
                                            //Edges[pt.EdgesInd[k]] = edge2;
                                            break;
                                        }
                                    }


                                }
                            }
                        }
                    }
                }
                #endregion

                #region Выделяем ребра не в точках пересечений
                for (i = 0; i < Points.Count; i++)
                {
                    pt = Points[i];
                    if (pt.EdgesInd.Count == 2)
                    {
                        if (Edges[pt.EdgesInd[0]].indA == i) edge = Edges[pt.EdgesInd[0]]; else edge = Edges[pt.EdgesInd[1]];
                        if (!edge.Used)
                        {
                            // находим ребро с точкой пересечением
                            while ((!edge.Used) && (edge.indB != i) && (Points[edge.indB].EdgesInd.Count == 2))
                            {
                                if (Edges[Points[edge.indB].EdgesInd[0]].indA == edge.indB)
                                {
                                    edge = Edges[Points[edge.indB].EdgesInd[0]];
                                }
                                else
                                {
                                    edge = Edges[Points[edge.indB].EdgesInd[1]];
                                }
                            }
                            if (edge.Used)
                            {
                                left = edge.Left; right = edge.Right;

                                while (edge.indB != i)
                                {
                                    if (Points[edge.indA].EdgesInd.Count > 2)
                                    {
                                        count = 0;
                                        for (j = 0; j < Points[edge.indA].EdgesInd.Count; j++)
                                        {
                                            if (Edges[Points[edge.indA].EdgesInd[j]].Cntr == 1)
                                            {
                                                count++;
                                            }
                                        }
                                        if (count == Points[edge.indA].EdgesInd.Count)
                                        {
                                            for (j = 0; j < Points[edge.indA].EdgesInd.Count; j++)
                                            {
                                                edge = Edges[Points[edge.indA].EdgesInd[j]];
                                                edge.Left = left;
                                                edge.Right = right;
                                                //Edges[Points[edge.indA].EdgesInd[j]] = edge;
                                            }
                                        }
                                        break;
                                    }
                                    if (Edges[Points[edge.indA].EdgesInd[0]].indB == edge.indA)
                                    {
                                        k = Points[edge.indA].EdgesInd[0];
                                    }
                                    else
                                    {
                                        k = Points[edge.indA].EdgesInd[1];
                                    }
                                    edge = Edges[k];
                                    if (edge.Used) break;
                                    edge.Used = true;
                                    edge.Left = left;
                                    edge.Right = right;
                                    //Edges[k] = edge;
                                }
                            }
                            else if (edge.indB == i)
                            {
                                i = edge.indA;
                            }
                        }
                    }
                    else if (pt.EdgesInd.Count > 2)
                    {
                        count = 0;
                        for (j = 0; j < pt.EdgesInd.Count; j++)
                        {
                            if (Edges[pt.EdgesInd[j]].Cntr == Edges[pt.EdgesInd[0]].Cntr) count++;
                        }
                        if (count == pt.EdgesInd.Count)
                        {
                            for (j = 0; j < pt.EdgesInd.Count; j++)
                            {
                                edge = Edges[pt.EdgesInd[j]];
                                if (!edge.Used)
                                {
                                    if (edge.indB == i)
                                    {
                                        // находим ребро с точкой пересечением
                                        while ((!edge.Used) && (edge.indA != i) && (Points[edge.indA].EdgesInd.Count == 2))
                                        {
                                            if (Edges[Points[edge.indA].EdgesInd[0]].indB == edge.indA) edge = Edges[Points[edge.indA].EdgesInd[0]];
                                            else edge = Edges[Points[edge.indA].EdgesInd[1]];
                                        }
                                        if (edge.Used)
                                        {
                                            left = edge.Left; right = edge.Right;

                                            while (edge.indA != i)
                                            {
                                                if (Points[edge.indB].EdgesInd.Count > 2) break;
                                                if (Edges[Points[edge.indB].EdgesInd[0]].indA == edge.indB)
                                                    k = Points[edge.indB].EdgesInd[0];
                                                else k = Points[edge.indB].EdgesInd[1];
                                                edge = Edges[k];
                                                edge.Used = true;
                                                edge.Left = left;
                                                edge.Right = right;
                                                //Edges[k] = edge;
                                            }
                                        }
                                    }
                                    else if (edge.indA == i)
                                    {
                                        // находим ребро с точкой пересечением
                                        while ((!edge.Used) && (edge.indB != i) && (Points[edge.indB].EdgesInd.Count == 2))
                                        {
                                            if (Edges[Points[edge.indB].EdgesInd[0]].indA == edge.indB) edge = Edges[Points[edge.indB].EdgesInd[0]];
                                            else edge = Edges[Points[edge.indB].EdgesInd[1]];
                                        }
                                        if (edge.Used)
                                        {
                                            left = edge.Left; right = edge.Right;

                                            while (edge.indB != i)
                                            {
                                                if (Points[edge.indA].EdgesInd.Count > 2) break;
                                                if (Edges[Points[edge.indA].EdgesInd[0]].indB == edge.indA) k = Points[edge.indA].EdgesInd[0]; else k = Points[edge.indA].EdgesInd[1];
                                                edge = Edges[k];
                                                edge.Used = true;
                                                edge.Left = left;
                                                edge.Right = right;
                                                //Edges[k] = edge;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Поиск контуров лежащих внутри полигонов
                k = -1;
                for (i = 0; i < Edges.Count; i++)
                {
                    if (!Edges[i].Used)
                    {
                        k = i;
                        break;
                    }
                }
                if (k != -1)
                {
                    bool find = true;
                    bool clockWise = true;
                    bool inside = false;
                    int countOut = 0;
                    List<Contour> Main = GetMainContours();
                    Contour Added = GetAddedContour();
                    if (Added != null)
                    {
                        if ((!Added._Fantom) && (Main.Count > 0))
                        {
                            count = 0;
                            countOut = 0;
                            inside = false;
                            find = false;
                            for (i = 0; i < Main.Count; i++)
                            {
                                clockWise = Main[i].GetClockWiseByX(true);
                                inside = Main[i].PointBodyEntry(Added._points[0].X, Added._points[0].Y);

                                if (clockWise && inside)
                                {
                                    for (j = 0; j < Main.Count; j++)
                                    {
                                        if ((j != i) &&
                                            (!Main[j].GetClockWiseByX(true)) &&
                                            (Main[i].PointBodyEntry(Main[j]._points[0].X, Main[j]._points[0].Y)))
                                        {
                                            count++;
                                            if (!Main[j].PointBodyEntry(Added._points[0].X, Added._points[0].Y))
                                            {
                                                countOut++;
                                            }
                                        }
                                    }
                                    find = true;
                                    break;
                                }
                            }
                            if ((find) && (count == countOut))
                            {
                                clockWise = Added.GetClockWiseByX(true);
                                for (i = Added.StratumCode; i <= Added.OilFieldCode; i++)
                                {
                                    edge = Edges[i];
                                    edge.Used = true;
                                    if (edge.Cntr != -1)
                                    {
                                        if (clockWise)
                                        {
                                            if (edge.Left == -2) edge.Left = 0; else edge.Left = -1;
                                            if (edge.Right == -2) edge.Right = 0; else edge.Right = -1;
                                        }
                                        else
                                        {
                                            if (edge.Right == -2) edge.Right = 0; else edge.Right = -1;
                                        }
                                    }
                                }
                            }
                        }
                        clockWise = Added.GetClockWiseByX(true);
                        for (i = 0; i < Main.Count; i++)
                        {
                            if (!Main[i]._Fantom)
                            {
                                inside = Added.PointBodyEntry(Main[i]._points[0].X, Main[i]._points[0].Y);
                                if ((clockWise && inside) || (!clockWise && !inside))
                                {
                                    clockWise = Added.GetClockWiseByX(true);
                                    for (j = Main[i].StratumCode; j <= Main[i].OilFieldCode; j++)
                                    {
                                        edge = Edges[j];
                                        edge.Used = true;
                                        if (edge.Cntr != -1)
                                        {
                                            if (clockWise)
                                            {
                                                if (edge.Left == -2) edge.Left = 1; else edge.Left = -1;
                                                if (edge.Right == -2) edge.Right = 1; else edge.Right = -1;
                                            }
                                            else
                                            {
                                                if (edge.Right == -2) edge.Right = 1; else edge.Right = -1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }

            public int GetNextCntrNumber()
            {
                int maxCntr = -1;
                for (int i = 0; i < Edges.Count; i++)
                {
                    if (Edges[i].Cntr > maxCntr) maxCntr = Edges[i].Cntr;
                }
                return maxCntr + 1;
            }
            public void RemoveAt(int index)
            {
                if ((index > -1) && (index < Edges.Count))
                {
                    Edges.RemoveAt(index);
                }
            }
            public void Clear()
            {
                Points.Clear();
                Edges.Clear();
            }

            private List<Contour> GetMainContours()
            {
                List<Contour> cntrList = new List<Contour>();
                Contour cntr = null;
                int start = -1;
                for (int i = 0; i < Edges.Count; i++)
                {
                    if (((Edges[i].Cntr == 0) || ((Edges[i].Cntr == -1))) && (Edges[i].indB != start))
                    {
                        if (cntr == null)
                        {
                            cntr = new Contour();
                            cntr._Fantom = false;
                            cntr.Closed = true;
                            cntr._points = new PointsXY(3);
                            cntrList.Add(cntr);
                        }
                        else
                        {
                            cntr.OilFieldCode = i;
                        }
                        if (cntr._points.Count == 0)
                        {
                            cntr.StratumCode = i;
                            start = Edges[i].indA;
                            cntr.Add(Points[Edges[i].indA].Point.X, Points[Edges[i].indA].Point.Y);
                        }
                        cntr.Add(Points[Edges[i].indB].Point.X, Points[Edges[i].indB].Point.Y);
                        if (Edges[i].Used) cntr._Fantom = true;
                    }
                    else
                    {
                        if (Edges[i].indB == start) cntr.OilFieldCode = i;
                        start = -1;
                        cntr = null;
                    }
                }
                return cntrList;
            }
            private Contour GetAddedContour()
            {
                Contour cntr = null;
                int start = -1;
                for (int i = 0; i < Edges.Count; i++)
                {
                    if ((Edges[i].Cntr == 1) || (Edges[i].Cntr == -1))
                    {
                        if (cntr != null) cntr.OilFieldCode = i;
                        if (Edges[i].indB == start) break;
                        if (cntr == null)
                        {
                            cntr = new Contour();
                            cntr._points = new PointsXY(3);
                            cntr._Fantom = false;
                            cntr.Closed = true;
                        }
                        if (cntr._points.Count == 0)
                        {
                            cntr.StratumCode = i;
                            start = Edges[i].indA;
                            cntr.Add(Points[Edges[i].indA].Point.X, Points[Edges[i].indA].Point.Y);
                        }
                        cntr.Add(Points[Edges[i].indB].Point.X, Points[Edges[i].indB].Point.Y);
                        if (Edges[i].Used) cntr._Fantom = true;
                    }
                    else if (start != -1)
                    {
                        break;
                    }
                }
                return cntr;
            }
            private List<int> GetEdgesByIndex(int index1, int index2)
            {
                List<int> edgeList = new List<int>();
                if ((index1 < Edges.Count) && (index2 < Edges.Count))
                {
                    if (Edges[index1].indA == Edges[index2].indB)
                    {
                        int end = -1, lastIndex;
                        Edge edge;
                        PolyPoint pt;
                        int i = index1, j, k;
                        edge = Edges[index1];
                        lastIndex = index1;
                        while (edge.indB != Edges[index2].indB)
                        {
                            pt = Points[edge.indB];
                            edgeList.Add(lastIndex);

                            if (pt.EdgesInd.Count == 2)
                            {
                                if (Edges[pt.EdgesInd[0]].indA == pt.Index)
                                {
                                    edge = Edges[pt.EdgesInd[0]];
                                    lastIndex = pt.EdgesInd[0];
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[1]];
                                    lastIndex = pt.EdgesInd[1];
                                }
                            }
                            else
                            {
                                k = -1;
                                int lInd = -1, rInd = -1;
                                for (j = 0; j < pt.EdgesInd.Count; j++)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == edge.indA) && (Edges[pt.EdgesInd[j]].indB == edge.indB))
                                    {
                                        k = j;
                                        break;
                                    }
                                }
                                j = k + 1;
                                lInd = -1;
                                if (j == pt.EdgesInd.Count) j = 0;
                                while (j != k)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == pt.Index) && (Edges[pt.EdgesInd[j]].Cntr == 1))
                                    {
                                        lInd = j;
                                        break;
                                    }
                                    j++;
                                    if (j == pt.EdgesInd.Count) j = 0;
                                }
                                j = k - 1;
                                rInd = -1;
                                if (j < 0) j = pt.EdgesInd.Count - 1;
                                while (j != k)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == pt.Index) && (Edges[pt.EdgesInd[j]].Cntr == 1))
                                    {
                                        rInd = j;
                                        break;
                                    }
                                    j--;
                                    if (j < 0) j = pt.EdgesInd.Count - 1;
                                }
                                if (Math.Abs(lInd - k) <= Math.Abs(rInd - k))
                                {
                                    edge = Edges[pt.EdgesInd[lInd]];
                                    lastIndex = pt.EdgesInd[lInd];
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[rInd]];
                                    lastIndex = pt.EdgesInd[rInd];
                                }
                            }
                        }
                        if (edge.indB == Edges[index2].indB) edgeList.Add(lastIndex);
                        if ((edge.indB == Edges[index2].indB) && (lastIndex != index2))
                        {
                            edgeList.Clear();
                        }
                    }
                }
                return edgeList;
            }
            private Contour GetContourByEdgeList(List<int> edgeList)
            {
                Contour cntr = new Contour();
                cntr._points = new PointsXY(edgeList.Count);
                cntr._points.Add(Points[Edges[edgeList[0]].indA].Point.X, Points[Edges[edgeList[0]].indA].Point.Y);
                for (int i = 0; i < edgeList.Count - 1; i++)
                {
                    cntr._points.Add(Points[Edges[edgeList[i]].indB].Point.X, Points[Edges[edgeList[i]].indB].Point.Y);
                }
                return cntr;
            }
        }

        public Polygon()
        {
            Points = new PointsCollection(16);
            Edges = new EdgesCollection(Points, 16);
            ResultReady = false;
        }
        public void Clear()
        {
            Points.Clear();
            Edges.Clear();
            ResultReady = false;
        }

        public void AddContour(Contour contour)
        {
            if ((contour._points != null) && (contour._points.Count > 2) && (contour.Closed))
            {
                int i, j;
                int ind, nextInd;
                //PolyPoint pt;
                Edge edge;
                bool clockWise = contour.GetClockWiseByX(true);

                if (Points.Count == 0) // Первый добавляемый контур
                {
                    nextInd = -1;
                    for (i = 0; i < contour._points.Count; i++)
                    {
                        ind = nextInd;
                        if (nextInd == -1)
                        {
                            ind = Points.GetPointIndex(contour._points[i]);
                            if (ind == -1)
                            {
                                ind = Points.AddPoint(contour._points[i]);
                            }
                        }

                        j = i + 1;
                        if (j == contour._points.Count) j = 0;
                        nextInd = Points.GetPointIndex(contour._points[j]);
                        if (nextInd == -1)
                        {
                            nextInd = Points.AddPoint(contour._points[j]);
                        }
                        if (ind != nextInd)
                        {
                            Edges.AddEdge(1, ind, nextInd, clockWise);
                        }
                    }
                    Edges.AddIntersectPoints(false); // Находим точки пересечения контуров
                    Edges.SortPointsAngle();
                    Edges.SetEdgesMarks();
                    for (i = 0; i < Edges.Count; i++)
                    {
                        edge = Edges[i];
                        if (edge.Left == edge.Cntr) edge.Left = 0;
                        if (edge.Right == edge.Cntr) edge.Right = 0;
                        edge.Cntr = 0;
                    }
                    ResultReady = true;
                }
                else // Суммирование полигонов
                {
                    contour.SetMinMax();
                    List<Contour> main = this.GetContours();

                    #region Добавляем точки второго контура
                    int numCntr = Edges.GetNextCntrNumber();
                    nextInd = -1;
                    for (i = 0; i < contour._points.Count; i++)
                    {
                        ind = nextInd;
                        if (nextInd == -1)
                        {
                            ind = Points.GetPointIndex(contour._points[i]);
                            if (ind == -1)
                            {
                                ind = Points.AddPoint(contour._points[i]);
                            }
                        }

                        j = i + 1;
                        if (j == contour._points.Count) j = 0;
                        nextInd = Points.GetPointIndex(contour._points[j]);
                        if (nextInd == -1)
                        {
                            nextInd = Points.AddPoint(contour._points[j]);
                        }
                        if (ind != nextInd)
                        {
                            Edges.AddEdge(numCntr, ind, nextInd, clockWise);
                        }
                    }
                    #endregion

                    bool inter = false;
                    for (j = 0; j < main.Count; j++)
                    {
                        main[j].SetMinMax();
                        if (main[j].IntersectBounds(contour) || contour.IntersectBounds(main[j]))
                        {
                            inter = true;
                            break;
                        }
                    }
                    if (inter) Edges.AddIntersectPoints(true);
                    Edges.AddIntersectPoints(false);
                    Edges.SortPointsAngle();
                    Edges.SetEdgesMarks();
                    ResultReady = false;
                }
            }
        }
        public void AddContour(List<BaseObject> CntrList)
        {
            if (CntrList.Count > 0)
            {
                int i, j, k;
                int ind, nextInd;
                Edge edge;
                Contour contour;
                bool clockWise;

                if (Points.Count == 0) // Первый добавляемый контур
                {
                    for (k = 0; k < CntrList.Count; k++)
                    {
                        contour = (Contour)CntrList[k];
                        clockWise = contour.GetClockWiseByX(true);
                        if ((contour._points != null) && (contour._points.Count > 2) && (contour.Closed))
                        {
                            nextInd = -1;
                            for (i = 0; i < contour._points.Count; i++)
                            {
                                ind = nextInd;
                                if (nextInd == -1)
                                {
                                    ind = Points.GetPointIndex(contour._points[i]);
                                    if (ind == -1)
                                    {
                                        ind = Points.AddPoint(contour._points[i]);
                                    }
                                }

                                j = i + 1;
                                if (j == contour._points.Count) j = 0;
                                nextInd = Points.GetPointIndex(contour._points[j]);
                                if (nextInd == -1)
                                {
                                    nextInd = Points.AddPoint(contour._points[j]);
                                }
                                if (ind != nextInd)
                                {
                                    Edges.AddEdge(1, ind, nextInd, clockWise);
                                }
                            }
                        }
                    }
                    Edges.AddIntersectPoints(false); // Находим точки пересечения контуров
                    Edges.SortPointsAngle();
                    Edges.SetEdgesMarks();
                    for (i = 0; i < Edges.Count; i++)
                    {
                        edge = Edges[i];
                        if (edge.Left == edge.Cntr) edge.Left = 0;
                        if (edge.Right == edge.Cntr) edge.Right = 0;
                        edge.Cntr = 0;
                    }
                    ResultReady = true;
                }
                else // Суммирование полигонов
                {
                    List<Contour> main = this.GetContours();
                    #region Добавляем точки второго контура
                    int numCntr = Edges.GetNextCntrNumber();
                    bool inter = false;
                    for (k = 0; k < CntrList.Count; k++)
                    {
                        contour = (Contour)CntrList[k];
                        contour.SetMinMax();
                        clockWise = contour.GetClockWiseByX(true);
                        //contour.GetClockWiseByX(true);
                        if ((contour._points != null) && (contour._points.Count > 2) && (contour.Closed))
                        {
                            nextInd = -1;
                            for (i = 0; i < contour._points.Count; i++)
                            {
                                ind = nextInd;
                                if (nextInd == -1)
                                {
                                    ind = Points.GetPointIndex(contour._points[i]);
                                    if (ind == -1)
                                    {
                                        ind = Points.AddPoint(contour._points[i]);
                                    }
                                }

                                j = i + 1;
                                if (j == contour._points.Count) j = 0;
                                nextInd = Points.GetPointIndex(contour._points[j]);
                                if (nextInd == -1)
                                {
                                    nextInd = Points.AddPoint(contour._points[j]);
                                }
                                if (ind != nextInd)
                                {
                                    Edges.AddEdge(numCntr, ind, nextInd, clockWise);
                                }
                            }
                            inter = false;
                            for (j = 0; j < main.Count; j++)
                            {
                                main[j].SetMinMax();
                                if (main[j].IntersectBounds(contour) || contour.IntersectBounds(main[j]))
                                {
                                    inter = true;
                                    break;
                                }
                            }
                            if (inter) Edges.AddIntersectPoints(true);
                        }
                    }
                    #endregion
                    Edges.AddIntersectPoints(false);
                    Edges.SortPointsAngle();
                    Edges.SetEdgesMarks();
                    ResultReady = false;
                }
            }
        }

        #region Операции над контурами
        void SelectEdgesByOperation(ContoursOperation Operation)
        {
            int i;
            for (i = 0; i < Edges.Count; i++)
            {
                Edges[i].Selected = false;
                Edges[i].Used = false;
            }
            for (i = 0; i < Edges.Count; i++)
            {
                switch (Operation)
                {
                    case ContoursOperation.Union:
                        if ((Edges[i].Left == -2) && (Edges[i].Right != -2))
                        {
                            Edges[i].Selected = true;
                        }
                        break;
                    case ContoursOperation.Difference:
                        if ((Edges[i].Left == -2) && (Edges[i].Right == 0))
                        {
                            Edges[i].Selected = true;
                        }
                        else if ((Edges[i].Left == 0) && ((Edges[i].Right == -1) || (Edges[i].Right == 1)))
                        {
                            Edges[i].Invert();
                            Edges[i].Selected = true;
                        }
                        break;
                    case ContoursOperation.Crossing:
                        if (Edges[i].Right == -1)
                        {
                            Edges[i].Selected = true;
                        }
                        break;
                    case ContoursOperation.SymmetricDifference:
                        if ((Edges[i].Left == -2) && (Edges[i].Right == 0))
                        {
                            Edges[i].Selected = true;
                        }
                        if ((Edges[i].Left == -2) && (Edges[i].Right == 1))
                        {
                            Edges[i].Selected = true;
                        }
                        else if ((Edges[i].Left == 0) && ((Edges[i].Right == -1) || (Edges[i].Right == 1)))
                        {
                            Edges[i].Invert();
                            Edges[i].Selected = true;
                        }
                        else if ((Edges[i].Left == 1) && ((Edges[i].Right == -1) || (Edges[i].Right == 0)))
                        {
                            Edges[i].Invert();
                            Edges[i].Selected = true;
                        }
                        break;
                }
            }
        }
        bool TestEdgeByOperation(Edge Edge1, Edge Edge2, int PtIndex, ContoursOperation Operation)
        {
            if ((Edge2.Selected) && (!Edge2.Used))
            {
                switch (Operation)
                {
                    case ContoursOperation.Union:
                        return (Edge2.indA == PtIndex);
                    case ContoursOperation.Crossing:
                        return (Edge2.indA == PtIndex);
                    case ContoursOperation.Difference:
                        return ((Edge1.Cntr == Edge2.Cntr) && (Edge2.indA == PtIndex));
                    case ContoursOperation.SymmetricDifference:
                        return ((Edge1.Cntr == Edge2.Cntr) && (Edge2.indA == PtIndex));
                }
            }
            return false;
        }
        public void OperateByContours(ContoursOperation Operation)
        {
            Edge edge;
            int i, j, k;
            SelectEdgesByOperation(Operation);

            PointsCollection newPoints = new PointsCollection(this.Points.Count);
            EdgesCollection newEdges = new EdgesCollection(newPoints, Edges.Count);

            bool exit = false;

            int ind, start = -1;
            int lInd, rInd;
            PolyPoint pt;
            while (!exit)
            {
                exit = true;
                for (i = 0; i < Edges.Count; i++)
                {
                    if ((Edges[i].Selected) && (!Edges[i].Used))
                    {
                        start = -1;
                        edge = Edges[i];
                        edge.Used = true;
                        Edges[i] = edge;
                        start = -1;
                        while (edge.indA != start)
                        {
                            if (start == -1) start = edge.indA;
                            edge.Left = -2;
                            edge.Right = 0;
                            //edge.Cntr = 0;

                            pt = Points[edge.indB];
                            newEdges.AddEdge(edge);
                            if (edge.indB == start) break;
                            if (!edge.Selected)
                            {
                                while ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA != start))
                                {
                                    newEdges.RemoveAt(newEdges.Count - 1);
                                }
                                if ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA == start))
                                {
                                    newEdges.RemoveAt(newEdges.Count - 1);
                                }
                                break;
                            }
                            if (pt.EdgesInd.Count == 2)
                            {
                                if (Edges[pt.EdgesInd[0]].indA == pt.Index)
                                {
                                    edge = Edges[pt.EdgesInd[0]];
                                    edge.Used = true;
                                }
                                else
                                {
                                    edge = Edges[pt.EdgesInd[1]];
                                    edge.Used = true;
                                }
                            }
                            else
                            {
                                k = -1;
                                for (j = 0; j < pt.EdgesInd.Count; j++)
                                {
                                    if ((Edges[pt.EdgesInd[j]].indA == edge.indA) &&
                                        (Edges[pt.EdgesInd[j]].indB == edge.indB))
                                    {
                                        k = j;
                                        break;
                                    }
                                }
                                if (k == -1) break;

                                rInd = -1;
                                j = k + 1;
                                if (j == pt.EdgesInd.Count) j = 0;
                                while (j != k)
                                {
                                    if (TestEdgeByOperation(edge, Edges[pt.EdgesInd[j]], pt.Index, Operation))
                                    {
                                        rInd = j;
                                        break;
                                    }
                                    j++;
                                    if (j == pt.EdgesInd.Count) j = 0;
                                }
                                lInd = -1;
                                j = k - 1;
                                if (j < 0) j = pt.EdgesInd.Count - 1;
                                while (j != k)
                                {
                                    if (TestEdgeByOperation(edge, Edges[pt.EdgesInd[j]], pt.Index, Operation))
                                    {
                                        lInd = j;
                                        break;
                                    }
                                    j--;
                                    if (j < 0) j = pt.EdgesInd.Count - 1;
                                }
                                if ((lInd != -1) && (rInd != -1))
                                {
                                    if (Math.Abs(lInd - i) <= Math.Abs(rInd - i))
                                    {
                                        edge = Edges[pt.EdgesInd[lInd]];
                                        edge.Used = true;
                                    }
                                    else
                                    {
                                        edge = Edges[pt.EdgesInd[rInd]];
                                        edge.Used = true;
                                    }
                                }
                                else
                                {
                                    while ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA != start))
                                    {
                                        newEdges.RemoveAt(newEdges.Count - 1);
                                    }
                                    if ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA == start))
                                    {
                                        newEdges.RemoveAt(newEdges.Count - 1);
                                    }
                                    break;
                                }
                            }

                        }
                        exit = false;
                    }
                }
            }
            if (newEdges.Count > 0)
            {
                List<int> ptIndexes = new List<int>();
                for (i = 0; i < newEdges.Count; i++)
                {
                    if (ptIndexes.IndexOf(newEdges[i].indA) == -1)
                    {
                        ptIndexes.Add(newEdges[i].indA);
                    }
                }
                for (i = 0; i < ptIndexes.Count; i++)
                {
                    ind = newPoints.AddPoint(Points[ptIndexes[i]].Point);
                    pt = newPoints[ind];
                    for (j = 0; j < newEdges.Count; j++)
                    {
                        newEdges[j].Cntr = 0;
                        if ((newEdges[j].Used) && (newEdges[j].indA == ptIndexes[i]))
                        {
                            edge = newEdges[j];
                            edge.indA = ind;
                            edge.Used = false;
                            pt.EdgesInd.Add(j);
                        }
                        else if ((newEdges[j].Selected) && (newEdges[j].indB == ptIndexes[i]))
                        {
                            edge = newEdges[j];
                            edge.indB = ind;
                            edge.Selected = false;
                            pt.EdgesInd.Add(j);
                        }
                    }
                    newPoints[ind] = pt;
                }
            }
            newPoints.Trim();
            newEdges.Trim();
            this.Points = newPoints;
            this.Edges = newEdges;
            ResultReady = true;
        }
        public void UnionContours()
        {
            //Edge edge;
            //int i, j, k;
            //for (i = 0; i < Edges.Count; i++)
            //{
            //    Edges[i].Selected = false;
            //    Edges[i].Used = false;
            //}
            //for (i = 0; i < Edges.Count; i++)
            //{
            //    if ((Edges[i].Left == -2) && (Edges[i].Right != -2))
            //    {
            //        Edges[i].Selected = true;
            //    }
            //}
            //PointsCollection newPoints = new PointsCollection(this.Points.Count);
            //EdgesCollection newEdges = new EdgesCollection(newPoints, Edges.Count);

            //bool exit = false;

            //int ind, start = -1;
            //int lInd, rInd;
            //PolyPoint pt;
            //while (!exit)
            //{
            //    exit = true;
            //    for (i = 0; i < Edges.Count; i++)
            //    {
            //        if ((Edges[i].Selected) && (!Edges[i].Used))
            //        {
            //            start = -1;
            //            edge = Edges[i];
            //            edge.Used = true;
            //            Edges[i] = edge;
            //            start = -1;
            //            while (edge.indA != start)
            //            {
            //                if (start == -1) start = edge.indA;
            //                edge.Left = -2;
            //                edge.Right = 0;
            //                edge.Cntr = 0;

            //                pt = Points[edge.indB];
            //                newEdges.AddEdge(edge);
            //                if (edge.indB == start) break;
            //                if (!edge.Selected)
            //                {
            //                    while ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA != start))
            //                    {
            //                        newEdges.RemoveAt(newEdges.Count - 1);
            //                    }
            //                    if ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA == start))
            //                    {
            //                        newEdges.RemoveAt(newEdges.Count - 1);
            //                    }
            //                    break;
            //                }
            //                if (pt.EdgesInd.Count == 2)
            //                {
            //                    if (Edges[pt.EdgesInd[0]].indA == pt.Index)
            //                    {
            //                        edge = Edges[pt.EdgesInd[0]];
            //                        edge.Used = true;
            //                    }
            //                    else
            //                    {
            //                        edge = Edges[pt.EdgesInd[1]];
            //                        edge.Used = true;
            //                    }
            //                }
            //                else
            //                {
            //                    k = -1;
            //                    for (j = 0; j < pt.EdgesInd.Count; j++)
            //                    {
            //                        if ((Edges[pt.EdgesInd[j]].indA == edge.indA) &&
            //                            (Edges[pt.EdgesInd[j]].indB == edge.indB))
            //                        {
            //                            k = j;
            //                            break;
            //                        }
            //                    }
            //                    if (k == -1) break;

            //                    rInd = -1;
            //                    j = k + 1;
            //                    if (j == pt.EdgesInd.Count) j = 0;
            //                    while (j != k)
            //                    {
            //                        if ((Edges[pt.EdgesInd[j]].Selected) &&
            //                            (!Edges[pt.EdgesInd[j]].Used) &&
            //                            (Edges[pt.EdgesInd[j]].indA == pt.Index))
            //                        {
            //                            rInd = j;
            //                            break;
            //                        }
            //                        j++;
            //                        if (j == pt.EdgesInd.Count) j = 0;
            //                    }
            //                    lInd = -1;
            //                    j = k - 1;
            //                    if (j < 0) j = pt.EdgesInd.Count - 1;
            //                    while (j != k)
            //                    {
            //                        if ((Edges[pt.EdgesInd[j]].Selected) &&
            //                            (!Edges[pt.EdgesInd[j]].Used) &&
            //                            (Edges[pt.EdgesInd[j]].indA == pt.Index))
            //                        {
            //                            lInd = j;
            //                            break;
            //                        }
            //                        j--;
            //                        if (j < 0) j = pt.EdgesInd.Count - 1;
            //                    }
            //                    if ((lInd != -1) && (rInd != -1))
            //                    {
            //                        if (Math.Abs(lInd - i) <= Math.Abs(rInd - i))
            //                        {
            //                            edge = Edges[pt.EdgesInd[lInd]];
            //                            edge.Used = true;
            //                        }
            //                        else
            //                        {
            //                            edge = Edges[pt.EdgesInd[rInd]];
            //                            edge.Used = true;
            //                        }
            //                    }
            //                    else
            //                    {
            //                        while ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA != start))
            //                        {
            //                            newEdges.RemoveAt(newEdges.Count - 1);
            //                        }
            //                        if ((newEdges.Count > 0) && (newEdges[newEdges.Count - 1].indA == start))
            //                        {
            //                            newEdges.RemoveAt(newEdges.Count - 1);
            //                        }
            //                        break;
            //                    }
            //                }

            //            }
            //            exit = false;
            //        }
            //    }
            //}
            //if (newEdges.Count > 0)
            //{
            //    List<int> ptIndexes = new List<int>();
            //    for (i = 0; i < newEdges.Count; i++)
            //    {
            //        if (ptIndexes.IndexOf(newEdges[i].indA) == -1)
            //        {
            //            ptIndexes.Add(newEdges[i].indA);
            //        }
            //    }
            //    for (i = 0; i < ptIndexes.Count; i++)
            //    {
            //        ind = newPoints.AddPoint(Points[ptIndexes[i]].Point);
            //        pt = newPoints[ind];
            //        for (j = 0; j < newEdges.Count; j++)
            //        {
            //            if ((newEdges[j].Used) && (newEdges[j].indA == ptIndexes[i]))
            //            {
            //                edge = newEdges[j];
            //                edge.indA = ind;
            //                edge.Used = false;
            //                pt.EdgesInd.Add(j);
            //            }
            //            else if ((newEdges[j].Selected) && (newEdges[j].indB == ptIndexes[i]))
            //            {
            //                edge = newEdges[j];
            //                edge.indB = ind;
            //                edge.Selected = false;
            //                pt.EdgesInd.Add(j);
            //            }
            //        }
            //        newPoints[ind] = pt;
            //    }
            //}
            //newPoints.Trim();
            //newEdges.Trim();
            //this.Points = newPoints;
            //this.Edges = newEdges;
            //ResultReady = true;
        }
        #endregion

        public List<Contour> GetContours()
        {
            List<Contour> cntrList = new List<Contour>();
            if (ResultReady)
            {
                Contour cntr = null;
                int start;
                for (int i = 0; i < Edges.Count; i++)
                {
                    cntr = new Contour();
                    cntr._points = new PointsXY(3);
                    cntrList.Add(cntr);
                    start = Edges[i].indA;
                    cntr._points.Add(Points[Edges[i].indA].Point.X, Points[Edges[i].indA].Point.Y);
                    while ((i < Edges.Count) && (Edges[i].indB != start))
                    {
                        cntr._points.Add(Points[Edges[i].indB].Point.X, Points[Edges[i].indB].Point.Y);
                        i++;
                    }
                    cntr.SetMinMax();
                }
            }
            return cntrList;
        }
    }
}
