﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.Objects.Data
{
    public class MerComp
    {
        public bool UseOil;
        public bool UseNatGas;
        public bool UseGasCondensate;
        public bool UseInj;
        public bool UseInjGas;
        MerCompItem[] Items;
        public int Count
        {
            get
            {
                if (Items == null)
                {
                    return 0;
                }
                else
                {
                    return Items.Length;
                }
            }
        }
        public MerCompItem this[int index]
        {
            get
            {
                if (index > -1 || index < Count)
                {
                    return Items[index];
                }
                else
                {
                    throw new IndexOutOfRangeException("Индекс за пределами диапазона");
                }
            }
        }

        public MerComp(Mer Mer) : this(Mer, 0, Mer.Count - 1) { }
        public MerComp(Mer Mer, int Index1, int Index2)
        {
            if ((Mer != null) && (Index1 <= Index2) && (Mer.Count > 0))
            {
                DateTime currDate;
                DateTime dt = Mer.Items[Index1].Date, dt2 = Mer.Items[Index2].Date;
                int count = (dt2.Year - dt.Year) * 12 + dt2.Month - dt.Month + 1;
                Items = new MerCompItem[count];
                int i = 0, j = 0;
                currDate = dt;
                while (i < Mer.Count)
                {
                    dt = Mer.Items[i].Date;
                    while (currDate < dt)
                    {
                        Items[j] = new MerCompItem(currDate);
                        currDate = currDate.AddMonths(1);
                        j++;
                    }
                    Items[j] = new MerCompItem(dt);
                    while (i < Mer.Count && Mer.Items[i].Date == dt)
                    {
                        Items[j].RetriveItem(Mer, i);
                        if ((!UseOil) && (Mer.Items[i].CharWorkId == 11)) UseOil = true;
                        if ((!UseNatGas) && (Mer.Items[i].CharWorkId == 12)) UseNatGas = true;
                        if ((!UseGasCondensate) && (Mer.Items[i].CharWorkId == 15)) UseGasCondensate = true;
                        if ((!UseInj) && (Mer.Items[i].CharWorkId == 20)) UseInj = true;
                        if ((!UseInjGas) && (Mer.Items[i].CharWorkId == 20) && (Mer.CountEx > 0))
                        {
                            int agent = Mer.GetItemEx(i).AgentInjId;
                            if ((agent > 80) && (agent < 90)) UseInjGas = true;
                        }
                        i++;
                    }
                    j++;
                    currDate = currDate.AddMonths(1);
                }
            }
        }

        public void Clear()
        {
            if (Items != null)
            {
                for (int i = 0; i < Items.Length; i++)
                {
                    Items[i].PlastItems.Clear();
                    Items[i].TimeItems.Clear();
                    Items[i].CharWorkIds.Clear();
                    Items[i] = null;
                }
            }
            Items = null;
        }
    }
    public class MerCompItem
    {
        public DateTime Date;
        public List<int> CharWorkIds;
        public int StateId;
        public int MethodId;

        public List<MerCompPlastItem> PlastItems;
        public MerWorkTimeCollection TimeItems;

        public double SumLiquid
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Liq;
                }
                return sum;
            }
        }
        public double SumOil
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Oil;
                }
                return sum;
            }
        }
        public double SumScoop
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Scoop;
                }
                return sum;
            }
        }
        public double SumLiquidV
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].LiqV;
                }
                return sum;
            }
        }
        public double SumOilV
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].OilV;
                }
                return sum;
            }
        }
        public double SumInjection
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Inj;
                }
                return sum;
            }
        }
        public double SumInjectionGas
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].InjGas;
                }
                return sum;
            }
        }
        public double SumGas
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Gas;
                }
                return sum;
            }
        }
        public double SumNaturalGas
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].NatGas;
                }
                return sum;
            }
        }
        public double SumGasCondensate
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < PlastItems.Count; i++)
                {
                    sum += PlastItems[i].Condensate;
                }
                return sum;
            }
        }
        public int TestWorkTime(int WorkTime)
        {
            int hours = (Date.AddMonths(1) - Date).Days * 24;
            if (hours < WorkTime)
            {
                return hours;
            }
            else
            {
                return WorkTime;
            }
        }

        public MerCompItem(DateTime Date)
        {
            this.Date = Date;
            PlastItems = new List<MerCompPlastItem>();
            CharWorkIds = new List<int>();
            TimeItems = new MerWorkTimeCollection();
        }
        public void RetriveItem(Mer mer, int index)
        {
            if (StateId != mer.Items[index].StateId) StateId = mer.Items[index].StateId;
            if (MethodId != mer.Items[index].MethodId) MethodId = mer.Items[index].MethodId;
            if (CharWorkIds.IndexOf(mer.Items[index].CharWorkId) == -1) CharWorkIds.Add(mer.Items[index].CharWorkId);
            MerCompPlastItem plastItem = GetItem(mer.Items[index].PlastId);

            TimeItems.Add(mer, index);

            plastItem.Liq += mer.Items[index].Wat + mer.Items[index].Oil;
            plastItem.Oil += mer.Items[index].Oil;
            plastItem.LiqV += mer.Items[index].WatV + mer.Items[index].OilV;
            plastItem.OilV += mer.Items[index].OilV;
            plastItem.Gas += mer.Items[index].Gas;
            if (mer.Items[index].CharWorkId == 13) plastItem.Scoop += mer.Items[index].Wat;
            if (mer.CountEx > 0)
            {
                plastItem.NatGas += mer.GetItemEx(index).NaturalGas;
                int agent = mer.GetItemEx(index).AgentInjId;
                if ((agent > 80) && (agent < 90))
                {
                    plastItem.InjGas += mer.GetItemEx(index).Injection;
                }
                else
                {
                    plastItem.Inj += mer.GetItemEx(index).Injection;
                }
                plastItem.Condensate += mer.GetItemEx(index).GasCondensate;
            }
        }
        public MerCompPlastItem GetItem(int PlastCode)
        {
            int ind = -1;
            for (int i = 0; i < PlastItems.Count; i++)
            {
                if (PlastItems[i].PlastCode == PlastCode)
                {
                    ind = i;
                }
            }
            if (ind == -1)
            {
                ind = PlastItems.Count;
                PlastItems.Add(new MerCompPlastItem(this));
                PlastItems[ind].PlastCode = PlastCode;
            }
            return PlastItems[ind];
        }
        public void Clear()
        {
            Date = DateTime.MinValue;
            CharWorkIds.Clear();
            StateId = 0;
            MethodId = 0;
            PlastItems.Clear();
        }
    }
    public class MerCompPlastItem
    {
        public MerCompItem ParentItem;
        public int PlastCode;
        public double Liq;
        public double Oil;
        public double LiqV;
        public double OilV;
        public double Inj;
        public double InjGas;
        public double NatGas;
        public double Condensate;
        public double Gas;
        public double Scoop;
        public MerCompPlastItem(MerCompItem MerExItem)
        {
            this.ParentItem = MerExItem;
        }
    }
    public class MerWorkTimeCollection
    {
        List<int> AllSumIndexes;
        List<MerWorkTimeItem> Items;
        public MerWorkTimeCollection()
        {
            Items = new List<MerWorkTimeItem>();
            AllSumIndexes = new List<int>();
        }
        public int Count { get { return (Items == null) ? 0 : Items.Count; } }

        public void Add(Mer mer, int Index)
        {
            bool find = false;
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Equals(mer, Index))
                {
                    find = true;
                    break;
                }
            }
            if (!find)
            {
                MerWorkTimeItem timeObj = new MerWorkTimeItem();
                timeObj.Retrieve(mer, Index);
                AddItem(timeObj);
            }
        }
        public void Add(MerWorkTimeItem Item)
        {
            bool find = false;
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Equals(Item, true))
                {
                    find = true;
                    break;
                }
            }
            if (!find)
            {
                MerWorkTimeItem timeObj = new MerWorkTimeItem();
                timeObj.Retrieve(Item);
                AddItem(timeObj);
            }
        }
        public void AddItem(MerWorkTimeItem Item)
        {
            int ind = Items.Count;
            if (AllSumIndexes.Count == 0)
            {
                AllSumIndexes.Add(ind);
            }
            else
            {
                bool find = false;
                for (int i = 0; i < Items.Count; i++)
                {
                    if (Items[i].Equals(Item, false))
                    {
                        find = true;
                        break;
                    }
                }
                if (!find)
                {
                    AllSumIndexes.Add(ind);
                }
            }
            Items.Add(Item);
        }
        public void Clear()
        {
            Items.Clear();
            AllSumIndexes.Clear();
        }

        public MerWorkTimeItem GetAllTime()
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                item.CollTime += Items[AllSumIndexes[i]].CollTime;
                item.StayTime += Items[AllSumIndexes[i]].StayTime;
            }
            return item;
        }
        public MerWorkTimeItem GetAllTime(int CharWorkId, bool InverseCondition)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                if (((!InverseCondition) && (Items[AllSumIndexes[i]].CharWorkId == CharWorkId)) ||
                     ((InverseCondition) && (Items[AllSumIndexes[i]].CharWorkId != CharWorkId)))
                {
                    item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                    item.CollTime += Items[AllSumIndexes[i]].CollTime;
                    item.StayTime += Items[AllSumIndexes[i]].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetAllProductionTime()
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                if (Items[AllSumIndexes[i]].CharWorkId == 11 || Items[AllSumIndexes[i]].CharWorkId == 12 || Items[AllSumIndexes[i]].CharWorkId == 15)
                {
                    item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                    item.CollTime += Items[AllSumIndexes[i]].CollTime;
                    item.StayTime += Items[AllSumIndexes[i]].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetAllInjectionTime()
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                if (Items[AllSumIndexes[i]].CharWorkId == 20)
                {
                    item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                    item.CollTime += Items[AllSumIndexes[i]].CollTime;
                    item.StayTime += Items[AllSumIndexes[i]].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetAllScoopTime()
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < AllSumIndexes.Count; i++)
            {
                if (Items[AllSumIndexes[i]].CharWorkId == 13)
                {
                    item.WorkTime += Items[AllSumIndexes[i]].WorkTime;
                    item.CollTime += Items[AllSumIndexes[i]].CollTime;
                    item.StayTime += Items[AllSumIndexes[i]].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem this[int index] { get { return Items[index]; } }
        public MerWorkTimeItem GetTime(int CharWorkId, bool InverseCondition)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if (((!InverseCondition) && (Items[i].CharWorkId == CharWorkId)) ||
                     ((InverseCondition) && (Items[i].CharWorkId != CharWorkId)))
                {
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetTime(int PlastId, int CharWorkId, bool InverseCondition)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i].PlastId == PlastId) &&
                     (((!InverseCondition) && (Items[i].CharWorkId == CharWorkId)) ||
                       ((InverseCondition) && (Items[i].CharWorkId != CharWorkId))))
                {
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetProductionTime(int PlastId)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i].PlastId == PlastId) && (Items[i].CharWorkId != 20))
                {
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetInjectionTime(int PlastId)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i].PlastId == PlastId) && (Items[i].CharWorkId == 20))
                {
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            return item;
        }
        public MerWorkTimeItem GetScoopTime(int PlastId)
        {
            MerWorkTimeItem item = new MerWorkTimeItem();
            for (int i = 0; i < Items.Count; i++)
            {
                if ((Items[i].PlastId == PlastId) && (Items[i].CharWorkId == 13))
                {
                    item.WorkTime += Items[i].WorkTime;
                    item.CollTime += Items[i].CollTime;
                    item.StayTime += Items[i].StayTime;
                }
            }
            return item;
        }
    }
    public struct MerWorkTimeItem
    {
        public DateTime Date;
        public int PlastId;
        public int CharWorkId;
        public int MethodId;
        public int StateId;
        public int WorkTime;
        public int CollTime;
        public int StayTime;

        public void Retrieve(Mer mer, int Index)
        {
            Date = mer.Items[Index].Date;
            PlastId = mer.Items[Index].PlastId;
            CharWorkId = mer.Items[Index].CharWorkId;
            StateId = mer.Items[Index].StateId;
            MethodId = mer.Items[Index].MethodId;
            WorkTime = mer.Items[Index].WorkTime;
            CollTime = mer.Items[Index].CollTime;
            StayTime = mer.Items[Index].StayTime;
        }
        public bool Equals(Mer mer, int Index)
        {
            return ((Date.Year == mer.Items[Index].Date.Year) &&
                    (Date.Month == mer.Items[Index].Date.Month) &&
                    (PlastId == mer.Items[Index].PlastId) &&
                    (CharWorkId == mer.Items[Index].CharWorkId) &&
                    (StateId == mer.Items[Index].StateId) &&
                    (MethodId == mer.Items[Index].MethodId) &&
                    (WorkTime == mer.Items[Index].WorkTime) &&
                    (CollTime == mer.Items[Index].CollTime));
        }
        public void Retrieve(MerWorkTimeItem Item)
        {
            Date = Item.Date;
            PlastId = Item.PlastId;
            CharWorkId = Item.CharWorkId;
            StateId = Item.StateId;
            MethodId = Item.MethodId;
            WorkTime = Item.WorkTime;
            CollTime = Item.CollTime;
            StayTime = Item.StayTime;
        }
        public bool Equals(MerWorkTimeItem Item, bool TestByPlast)
        {
            return ((Date.Year == Item.Date.Year) &&
                    (Date.Month == Item.Date.Month) &&
                    ((!TestByPlast) || (PlastId == Item.PlastId)) &&
                    (CharWorkId == Item.CharWorkId) &&
                    (StateId == Item.StateId) &&
                    (MethodId == Item.MethodId) &&
                    (WorkTime == Item.WorkTime) &&
                    (CollTime == Item.CollTime));
        }
    }
}
