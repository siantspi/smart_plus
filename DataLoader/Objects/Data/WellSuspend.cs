﻿using DataLoader.Objects.Base;
using System;
using System.IO;

namespace DataLoader.Objects.Data
{
    public struct WellSuspendItem
    {
        public DateTime Date;
        public int SuspendSolid;
    }
    public sealed class WellSuspend : WellDataObject<WellSuspendItem>
    {
        public static int Version = 1;
        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            try
            {
                int count = br.ReadInt32();
                WellSuspendItem[] newItems = new WellSuspendItem[count];
                for (int i = 0; i < count; i++)
                {
                    newItems[i].Date = DateTime.FromOADate(br.ReadDouble());
                    newItems[i].SuspendSolid = br.ReadInt32();
                }
                SetItems(newItems);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(Count);
                for (int i = 0; i < Items.Length; i++)
                {
                    bw.Write(Items[i].Date.ToOADate());
                    bw.Write(Items[i].SuspendSolid);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}