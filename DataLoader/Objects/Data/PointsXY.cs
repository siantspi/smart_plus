﻿using DataLoader.Objects.Base;
using System;
using System.Collections.Generic;

namespace DataLoader.Objects.Data
{
    public struct PointD
    {
        public double X;
        public double Y;
    }
    public class PointsXY : BaseObject
    {
        public int Count { get { return ArrayPoint.Count; } }
        public List<PointD> ArrayPoint;

        public PointsXY(int count)
        {
            ArrayPoint = new List<PointD>(count);
        }
        public PointsXY(PointsXY arr)
        {
            ArrayPoint = new List<PointD>(arr.ArrayPoint);
        }
        public PointsXY(PointD[] arr)
        {
            ArrayPoint = new List<PointD>(arr);
        }
        public void Exchange(int ind1, int ind2)
        {
            PointD point = new PointD();
            if ((ind2 < ArrayPoint.Count) && (ind2 < ArrayPoint.Count))
            {
                point = ArrayPoint[ind1];
                ArrayPoint[ind1] = ArrayPoint[ind2];
                ArrayPoint[ind2] = point;
            }
        }
        public double GetDistanceXY(int ind1, int ind2)
        {
            double dx = ((PointD)ArrayPoint[ind1]).X - ((PointD)ArrayPoint[ind2]).X;
            double dy = ((PointD)ArrayPoint[ind1]).Y - ((PointD)ArrayPoint[ind2]).Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        public double GetDistanceXY(double x1, double y1, int ind2)
        {
            double dx = x1 - ((PointD)ArrayPoint[ind2]).X;
            double dy = y1 - ((PointD)ArrayPoint[ind2]).Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        public double GetRMS(int num)
        {
            if (ArrayPoint.Count == 0) return 0;
            double val, sum = 0;
            for (int i = 0; i < ArrayPoint.Count; i++)
            {
                val = GetValue(i, num);
                sum += val * val;
            }
            return Math.Sqrt(sum / ArrayPoint.Count);
        }
        public double GetLength()
        {
            double sum = 0;
            for (int i = 0; i < ArrayPoint.Count - 1; i++)
            {
                sum += GetDistanceXY(i, i + 1);
            }
            return sum;
        }
        public double GetSquare()
        {
            double square = 0;
            double y1, y2;
            for (int i = 0; i < Count; i++)
            {
                if (i == 0)
                {
                    y1 = ArrayPoint[Count - 1].Y;
                    y2 = ArrayPoint[1].Y;
                }
                else if (i == Count - 1)
                {
                    y1 = ArrayPoint[i - 1].Y;
                    y2 = ArrayPoint[0].Y;
                }
                else
                {
                    y1 = ArrayPoint[i - 1].Y;
                    y2 = ArrayPoint[i + 1].Y;
                }
                square += ArrayPoint[i].X * (y1 - y2);
            }
            return Math.Abs(square / 2);
        }

        public void Sort(int num)
        {
            // 0 - по X, 1 - Y
            int i, j;
            switch (num)
            {
                case 0:
                    for (i = 0; i < Count; i++)
                    {
                        for (j = 0; j < Count - i; j++)
                        {
                            if (ArrayPoint[j].X < ArrayPoint[j + 1].X)
                            {
                                Exchange(j, j + 1);
                            }
                        }
                    }
                    break;
                case 1:
                    for (i = 0; i < Count; i++)
                    {
                        for (j = 0; j < Count - i; j++)
                        {
                            if (ArrayPoint[j].Y < ArrayPoint[j + 1].Y)
                            {
                                Exchange(j, j + 1);
                            }
                        }
                    }
                    break;
            }
        }

        public double GetValue(int ind, int num)
        {
            switch (num)
            {
                case 0:
                    return ((PointD)ArrayPoint[ind]).X;
                case 1:
                    return ((PointD)ArrayPoint[ind]).Y;
            }
            return Constant.DOUBLE_NAN;
        }
        public void Add(double X, double Y)
        {
            PointD pt;
            pt.X = X;
            pt.Y = Y;
            ArrayPoint.Add(pt);
        }
        public void Insert(int Index, double X, double Y)
        {
            PointD pt;
            pt.X = X;
            pt.Y = Y;
            ArrayPoint.Insert(Index, pt);
        }
        public void RemoveAt(int index)
        {
            ArrayPoint.RemoveAt(index);
        }
        public PointD this[int index]
        {
            get { return ArrayPoint[index]; }
            set
            {
                ArrayPoint[index] = value;
            }
        }
        public double GetMinX()
        {
            double item;
            double min = double.MaxValue;
            for (int i = 0; i < Count; i++)
            {
                item = ArrayPoint[i].X;
                if (min > item) min = item;
            }
            return min;
        }
        public double GetMaxX()
        {
            double item;
            double max = double.MinValue;
            for (int i = 0; i < Count; i++)
            {
                item = ArrayPoint[i].X;
                if (max < item) max = item;
            }
            return max;
        }
        public double GetMinY()
        {
            double item;
            double min = double.MaxValue;
            for (int i = 0; i < Count; i++)
            {
                item = ArrayPoint[i].Y;
                if (min > item) min = item;
            }
            return min;
        }
        public double GetMaxY()
        {
            double item;
            double max = double.MinValue;
            for (int i = 0; i < Count; i++)
            {
                item = ArrayPoint[i].Y;
                if (max < item) max = item;
            }
            return max;
        }
        public PointD GetAvarage()
        {
            PointD ave;
            ave.X = Constant.DOUBLE_NAN;
            ave.Y = Constant.DOUBLE_NAN;
            double sumX = 0, sumY = 0;
            for (int i = 0; i < Count; i++)
            {
                sumX += ArrayPoint[i].X;
                sumY += ArrayPoint[i].Y;
            }
            if (Count > 0)
            {
                ave.X = sumX / Count;
                ave.Y = sumY / Count;
            }
            return ave;
        }
    }
}
