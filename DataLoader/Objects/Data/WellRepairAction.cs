﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Base;
using System.IO;

namespace DataLoader.Objects.Data
{
    public struct WellRepairActionItem
    {
        public DateTime Date;
        public ushort WellActionCode;
    }
    public sealed class WellRepairAction : WellDataObject<WellRepairActionItem>
    {
        public static int VERSION = 0;

        public override bool ReadFromCache(BinaryReader br, int Version)
        {
            try
            {
                int count = br.ReadInt32();
                WellRepairActionItem[] newItems = new WellRepairActionItem[count];
                for (int i = 0; i < count; i++)
                {
                    newItems[i].Date = DateTime.FromOADate(br.ReadDouble());
                    newItems[i].WellActionCode = br.ReadUInt16();
                }
                SetItems(newItems);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public override bool WriteToCache(BinaryWriter bw)
        {
            try
            {
                bw.Write(Count);
                for (int i = 0; i < Items.Length; i++)
                {
                    bw.Write(Items[i].Date.ToOADate());
                    bw.Write(Items[i].WellActionCode);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
