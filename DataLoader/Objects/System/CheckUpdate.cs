﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Security.Principal;
using System.IO;

namespace DataLoader
{
    public static class CheckAnnoyUpdate
    {
        public static void CheckLoaderData()
        {
            TimeSpan AnnoySpan = new TimeSpan(90,0,0,0);
            DateTime AnnoyData = GetLoaderDataSetup() + AnnoySpan;


            if (DateTime.Now > AnnoyData)
            {
                System.Windows.Forms.MessageBox.Show("Вы используете старую версию программы, необходимо обновление! Обратитесь к системному администратору");
            }
        }

        private static DateTime GetLoaderDataSetup()
        {
            string fullPath = System.Windows.Forms.Application.StartupPath + "\\DataLoader.exe";
            DateTime fileCreatedDate = File.GetCreationTime(fullPath);
            return fileCreatedDate;
        }

        private static DateTime RegLoaderData()
        {
        
            DateTime dateTimeSetup;
            Microsoft.Win32.RegistryKey key;
            string dateTimefile = string.Empty;
            WindowsIdentity wid = WindowsIdentity.GetCurrent();
            if (wid != null)
            {
                key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\DataLoader", false);
                if (key != null)
                {
                    dateTimefile = Convert.ToString(key.GetValue("dateTimeSetup"));
                    key.Close();
                    if (dateTimefile.Length == 0)
                    {
                        SetRegLoaderData(GetLoaderDataSetup());
                    }
                }
                else
                {
                    SetRegLoaderData(GetLoaderDataSetup());
                }
            }
            if (dateTimefile.Length != 0)
            {
                dateTimeSetup = DateTime.FromFileTime(Convert.ToInt64(dateTimefile));
            }
            else
            {
                dateTimeSetup = GetLoaderDataSetup();
            }
            return dateTimeSetup;
        }

        private static void SetRegLoaderData(DateTime SetupDate)
        {
            Microsoft.Win32.RegistryKey key;
            WindowsIdentity wid = WindowsIdentity.GetCurrent();
            if (wid != null)
            {
                key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\DataLoader", false);
                if (key == null)
                {
                    key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"Software\DataLoader");
                }
                Int64 setupTimeSting = SetupDate.ToFileTime();
                key.SetValue("dateTimeSetup", setupTimeSting.ToString());
                key.Close();
            }       
        }
    }
}
