﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DataLoader.Objects.Base;
using DataLoader.Objects.Project;

namespace DataLoader.Objects
{
    class OilfieldWorkerDispatcher : LoggingObject
    {
        public List<Oilfield> WorkOilFields;
        Project.Project CurrentProject;
        Oilfield CommonOilField;
        int StartedCommonOilObjects;
        DateTime StartedCommonMinMerDate, StartedCommonMaxMerDate;
        List<Oilfield> EndWorkOilFields;
        List<WorkerTask> CurrentTask;
        Timer Timer;
        MainForm MainForm;
        bool CancelWork { get; set; }
        int countErrors;
        public bool ErrorLoading { get { return countErrors > 0; } }
        int MaxThreadCount;

        public OilfieldWorkerDispatcher(MainForm MainForm) : base(LOGGING_OBJECT_TYPE.OILFIELD_DISPATCHER)
        {
            WorkOilFields = null;
            CurrentTask = null;
            this.MainForm = MainForm;
            CancelWork = false;
            MaxThreadCount = Environment.ProcessorCount;
            EndWorkOilFields = new List<Oilfield>();

            #region TIMER
            Timer = new Timer();
            Timer.Enabled = false;
            Timer.Interval = 100;
            Timer.Tick +=new EventHandler(Timer_Tick);
            #endregion
        }

        public void RunTaskAsync(Project.Project Project, List<Oilfield> WorkOilFields, List<WorkerTask> TaskList)
        {
            this.WorkOilFields = WorkOilFields;
            this.CurrentTask = TaskList;
            if (Project == null || Project.NGDUList.Count == 0 || Project.NGDUList[0].OilFields.Count == 0)
            {
                return;
            }
            CurrentProject = Project;
            this.CommonOilField = Project.NGDUList[0].OilFields[0];
            EndWorkOilFields.Clear();
            CancelWork = false;
            countErrors = 0;
            Timer.Start();
            StartedCommonOilObjects = 0;
            StartedCommonMinMerDate = DateTime.MinValue;
            StartedCommonMaxMerDate = DateTime.MaxValue;
            if ((CommonOilField != null) && (CommonOilField.MerOilObjects != null))
            {
                StartedCommonOilObjects = CommonOilField.MerOilObjects.Count;
                StartedCommonMinMerDate = CommonOilField.minMerDate;
                StartedCommonMaxMerDate = CommonOilField.maxMerDate;
            }
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            int i;
            // Проверяем количество работающих
            bool find = false;
            int CountWork = 0;
            int CountWorkWells = 0;
            if (!MainForm.UserDialogShowed && !CancelWork)
            {
                for (i = 0; i < WorkOilFields.Count; i++)
                {
                    if (WorkOilFields[i].IsBusy)
                    {
                        CountWork++;
                        if (WorkOilFields[i].Wells != null) CountWorkWells += WorkOilFields[i].Wells.Count;
                    }
                    if ((WorkOilFields[i].DataLoaded) && (EndWorkOilFields.IndexOf(WorkOilFields[i]) == -1))
                    {
                        if (WorkOilFields[i].DataLoadCanceled)
                        {
                            CancelWork = true;
                            if (MessageBox.Show(string.Format("Отменена загрузка месторождения {0}.\nОтменить загрузку всех месторождений?", WorkOilFields[i].Name), "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                this.MainForm.TestCurrentDictionaryChanged();
                                NewMessage("Отменена загрузка всех месторождений!");
                                NewProgress(100);
                                Timer.Stop();
                                MainForm.EndWork();
                            }
                            else
                            {
                                EndWorkOilFields.Add(WorkOilFields[i]);
                                NewMessage(string.Format("Отменена загрузка данных месторождения [{0}]", WorkOilFields[i].Name));
                            }
                            CancelWork = false;
                        }
                        else if (WorkOilFields[i].DataLoadError)
                        {
                            countErrors++;
                            EndWorkOilFields.Add(WorkOilFields[i]);
                        }
                        else
                        {
                            EndWorkOilFields.Add(WorkOilFields[i]);
                            find = false;
                            for (int j = 0; j < CurrentTask.Count; j++)
                            {
                                if (CurrentTask[j].Task == (int)SERVER_DATA_TYPE.MER || CurrentTask[j].Task == (int)SERVER_DATA_TYPE.MER_NEW)
                                {
                                    find = true;
                                    break;
                                }
                            }
                            if (find)
                            {
                                CommonOilField.AddOilObjects(WorkOilFields[i].MerOilObjects);

                                if (CommonOilField.minMerDate == DateTime.MinValue || CommonOilField.minMerDate > WorkOilFields[i].minMerDate)
                                {
                                    CommonOilField.minMerDate = WorkOilFields[i].minMerDate;
                                }

                                if (CommonOilField.maxMerDate == DateTime.MaxValue || CommonOilField.maxMerDate < WorkOilFields[i].maxMerDate)
                                {
                                    CommonOilField.maxMerDate = WorkOilFields[i].maxMerDate;
                                }
                                NewMessage(string.Format("Добавление объектов разработки в общий список [{0}]", CommonOilField.Name));
                            }
                            NewMessage(string.Format("Загрузка данных месторождения завершена [{0}]", WorkOilFields[i].Name));
                        }
                    }
                }
                if ((EndWorkOilFields.Count < WorkOilFields.Count) && (CountWork < MaxThreadCount))
                {
                    for (i = 0; i < WorkOilFields.Count; i++)
                    {
                        if ((!WorkOilFields[i].IsBusy) && (!WorkOilFields[i].DataLoaded))
                        {
                            if ((CountWork <= MaxThreadCount) && ((WorkOilFields[i].Wells == null) || (CountWorkWells == 0) || (CountWorkWells + WorkOilFields[i].Wells.Count < 600)))
                            {
                                NewMessage(string.Format("Запуск загрузки данных месторождения [{0}]", WorkOilFields[i].Name));
                                WorkOilFields[i].RunTaskAsync(CurrentTask);
                                if (WorkOilFields[i].Wells != null) CountWorkWells += WorkOilFields[i].Wells.Count;
                                CountWork++;
                            }
                        }
                    }
                }
                if (EndWorkOilFields.Count == WorkOilFields.Count)
                {
                    NewMessage("Создание файла обновления...");
                    CurrentProject.CreateProjectUpdateFile();

                    if ((CommonOilField != null) && (CommonOilField.MerOilObjects != null) && 
                        ((StartedCommonOilObjects != CommonOilField.MerOilObjects.Count) ||
                         (StartedCommonMinMerDate != CommonOilField.minMerDate) ||
                         (StartedCommonMaxMerDate != CommonOilField.maxMerDate)))
                    {
                        NewMessage(string.Format("Сохранение списка объектов разработки [{0}]", CommonOilField.Name));
                        CommonOilField.WriteOilObjCodesToCache();
                        StartedCommonOilObjects = CommonOilField.MerOilObjects.Count;
                        StartedCommonMinMerDate = CommonOilField.minMerDate;
                        StartedCommonMaxMerDate = CommonOilField.maxMerDate;
                    }
                    Timer.Stop();
                    this.MainForm.TestCurrentDictionaryChanged();
                    NewMessage("Загрузка завершена!");
                    NewProgress(100);
                    MainForm.EndWork();
                }
                else
                {
                    int progress = (int)(EndWorkOilFields.Count * 100f / WorkOilFields.Count);
                    NewProgress(progress);
                }
            }
        }
    }
}