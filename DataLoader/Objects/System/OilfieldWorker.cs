﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using DataLoader.DictionaryObjects;
using DataLoader.Objects.Base;
using DataLoader.Objects.Project;

namespace DataLoader.Objects
{
    public enum SERVER_DATA_TYPE : int
    {
        SLEEP = -1,
        WELLBORE_BY_ORACLE = 0,        
        WELLBORE,           
        COORDINATES,               
        CONTOUR,                   
        AREA,                      
        WELLPAD,                   
        MER,                       
        MER_NEW,
        GIS,                       
        PERFORATION,               
        LOGS,                      
        PVT,                       
        GRID,
        CHESS,
        GTM,
        PROJECT_WELLBORE,
        CHESS_BY_ORACLE,    //17       
        GTM_ORACLE,                       
        WELL_REPAIR_ACTION,        
        WELL_RESEARCH,      
        WELL_RESEARCH_BY_ORACLE,
        PERFORATION_BY_ORACLE,
        SUSPEND
        
    }

    public struct WorkerTask
    {
        public int Task;
        public string Query;
    }

    public struct WorkerState
    {
        public string WorkMainTitle;
        public string WorkCurrentTitle;
        public string Element;
        public string Error;
        public string Time;

        public static WorkerState Empty
        {
            get
            {
                WorkerState state;
                state.WorkMainTitle = string.Empty;
                state.WorkCurrentTitle = string.Empty;
                state.Time = string.Empty;
                state.Error = string.Empty;
                state.Element = string.Empty;
                return state;
            }
        }
    }

    class OilfieldWorker
    {
        enum ServerLoadingMode : byte
        {
            LOAD_FROM_SERVER,
            CONVERSION_CODES,
            POST_PROCESSING
        }

        DictionaryCollection DictList;
        Oilfield OilField;
        public bool DataLoading { get; set; }
        public bool DataLoaded { get; set; }
        public bool Canceled { get; set; }
        public bool ErrorLoading { get; set; }
        private ServerLoadingMode CurrentServerLoadMode;
        private int LastWellCount;
        private int LastProjectWellCount;
        public List<WorkerTask> CurrentTask;
        WorkerState LastWorkerState;
        BackgroundWorker Worker;
        byte OutOfMemoryCounter;
        public OilfieldWorker(Oilfield ParentOilField, DictionaryCollection DictList)
        {
            OilField = ParentOilField;
            this.DictList = DictList;
            Worker = new BackgroundWorker();
            Worker.WorkerReportsProgress = true;
            Worker.WorkerSupportsCancellation = true;
            CurrentTask = null;
            Canceled = false;
            LastWorkerState = WorkerState.Empty;
            OutOfMemoryCounter = 0;

            Worker.DoWork += new DoWorkEventHandler(OilFieldWorker_DoWork);
            Worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OilFieldWorker_RunWorkerCompleted);
            Worker.ProgressChanged += new ProgressChangedEventHandler(OilFieldWorker_ProgressChanged);
        }

        #region TASK
        private bool TestTask(SERVER_DATA_TYPE DataType)
        {
            if (CurrentTask != null)
            {
                for (int i = 0; i < CurrentTask.Count; i++)
                {
                    if (CurrentTask[i].Task == (int)DataType)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private string GetTaskQuery(SERVER_DATA_TYPE DataType)
        {
            if (CurrentTask != null)
            {
                for (int i = 0; i < CurrentTask.Count; i++)
                {
                    if (CurrentTask[i].Task == (int)DataType)
                    {
                        return CurrentTask[i].Query;
                    }
                }
            }
            return string.Empty;
        }
        private void AddTask(SERVER_DATA_TYPE DataType)
        {
            if (CurrentTask != null)
            {
                WorkerTask task;
                task.Task = (int)DataType;
                task.Query = string.Empty;
                CurrentTask.Add(task);
            }
        }
        private void RemoveTask(SERVER_DATA_TYPE DataType)
        {
            if (CurrentTask != null)
            {
                for (int i = 0; i < CurrentTask.Count; i++)
                {
                    if (CurrentTask[i].Task == (int)DataType)
                    {
                        CurrentTask.RemoveAt(i);
                        break;
                    }
                }
            }
        }
        #endregion

        void WriteLog(string FileName, string Text)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(FileName, true, Encoding.GetEncoding(1251)))
            {
                sw.WriteLine(Text);
            }
        }
        public void RunTaskAsync(List<WorkerTask> LoadTask)
        {
            DataLoaded = false;
            DataLoading = true;
            ErrorLoading = false;
            Canceled = false;
            CurrentTask = LoadTask;
            CurrentServerLoadMode = ServerLoadingMode.LOAD_FROM_SERVER;
            OilField.NewMessage("Запуск загрузки данных...");
            Worker.RunWorkerAsync();
        }
        public bool IsBusy
        {
            get
            {
                return ((Worker.IsBusy) || (DataLoading));
            }
        }

        void OilFieldWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            switch (CurrentServerLoadMode)
            {
                case ServerLoadingMode.LOAD_FROM_SERVER:
                    LoadServerData(worker, e);
                    break;
                case ServerLoadingMode.POST_PROCESSING:
                    PostProcessing(worker, e);
                    break;
            }
        }
        void OilFieldWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            OilField.NewProgress(e.ProgressPercentage);
            if (e.UserState != null)
            {
                WorkerState workerState = (WorkerState)e.UserState;
                if ((workerState.Error != null) && (workerState.Error.Length > 0))
                {
                    OilField.NewMessage(LOGGING_MESSAGE_TYPE.ERROR, workerState.Error);
                }
                else
                {
                    if (LastWorkerState.WorkCurrentTitle != workerState.WorkCurrentTitle)
                    {
                        LastWorkerState = workerState;
                        OilField.NewMessage(workerState.WorkCurrentTitle);
                    }
                }
            }
        }
        void OilFieldWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            if (e.Error != null)
            {
                if (e.Error is OutOfMemoryException && OutOfMemoryCounter < 5)
                {
                    OilField.NewMessage(LOGGING_MESSAGE_TYPE.INFO, "Нехватка памяти при загрузке. Попытка перезапустить задание.");
                    WorkerTask task = new WorkerTask();
                    task.Task = -1; // Sleep
                    task.Query = string.Empty;
                    CurrentTask.Insert(0, task);
                    RunTaskAsync(CurrentTask);
                    DataLoading = true;
                    DataLoaded = false;
                    OutOfMemoryCounter++;
                }
                else
                {
                    string mess = string.Format("Ошибка загрузки:{0}", e.Error.Message);
                    
#if DEBUG
                    mess += "\n" + e.Error.StackTrace;
#endif
                    OilField.NewMessage(LOGGING_MESSAGE_TYPE.ERROR, mess);
                    OilField.NewProgress(100);
                    DataLoading = false;
                    DataLoaded = true;
                    ErrorLoading = true;
                }
            }
            else if (e.Cancelled)
            {
                OilField.NewMessage(LOGGING_MESSAGE_TYPE.ERROR, string.Format("Операция отменена пользователем!"));
                OilField.NewProgress(100);
                DataLoading = false;
                DataLoaded = true;
                OutOfMemoryCounter = 0;
            }
            else
            {
                OutOfMemoryCounter = 0;
                switch (CurrentServerLoadMode)
                {
                    case ServerLoadingMode.LOAD_FROM_SERVER:
                        
                        if (ConversionCodes() == -1)
                        {
                            Canceled = true;
                            DataLoaded = true;
                            DataLoading = false;
                            OilField.NewProgress(100);
                            OilField.NewMessage(string.Format("Загрузка отменена [{0}]", OilField.Name));
                            return;
                        }
                        if (CurrentTask.Count > 0)
                        {
                            if (OilField.ProjectPath.Length > 0)
                            {
                                if (TestTask(SERVER_DATA_TYPE.WELLBORE_BY_ORACLE))
                                {
                                    #region добавились скважины загружаем данные
                                    if ((LastWellCount > 0) && (LastWellCount < OilField.Wells.Count))
                                    {
                                        OilField.NewMessage(string.Format("Добавлено {0} новых скважин [{1}]", (OilField.Wells.Count - LastWellCount), OilField.Name));
                                        if (!TestTask(SERVER_DATA_TYPE.COORDINATES))
                                        {
                                            OilField.NewMessage("Добавлена загрузка координат.");
                                            AddTask(SERVER_DATA_TYPE.COORDINATES);
                                        }
                                        if (!TestTask(SERVER_DATA_TYPE.MER))
                                        {
                                            OilField.NewMessage("Добавлена загрузка МЭР.");
                                            AddTask(SERVER_DATA_TYPE.MER);
                                        }
                                        if (!TestTask(SERVER_DATA_TYPE.GIS))
                                        {
                                            OilField.NewMessage("Добавлена загрузка ГИС.");
                                            AddTask(SERVER_DATA_TYPE.GIS);
                                        }
                                        if (!TestTask(SERVER_DATA_TYPE.LOGS))
                                        {
                                            OilField.NewMessage("Добавлена загрузка Каротажа.");
                                            AddTask(SERVER_DATA_TYPE.LOGS);
                                        }
                                        if (!TestTask(SERVER_DATA_TYPE.PERFORATION))
                                        {
                                            OilField.NewMessage("Добавлена загрузка Перфорации.");
                                            AddTask(SERVER_DATA_TYPE.PERFORATION);
                                        }
                                    }
                                    #endregion
                                }
                                else if (TestTask(SERVER_DATA_TYPE.WELLBORE))
                                {
                                    #region добавились скважины загружаем данные
                                    if ((LastWellCount > 0) && (LastWellCount < OilField.Wells.Count))
                                    {
                                        OilField.NewMessage(string.Format("Добавлено {0} новых скважин [{1}]", (OilField.Wells.Count - LastWellCount), OilField.Name));
                                        if (!TestTask(SERVER_DATA_TYPE.COORDINATES))
                                        {
                                            OilField.NewMessage("Добавлена загрузка координат.");
                                            AddTask(SERVER_DATA_TYPE.COORDINATES);
                                        }
                                        if (!TestTask(SERVER_DATA_TYPE.MER))
                                        {
                                            OilField.NewMessage("Добавлена загрузка МЭР.");
                                            AddTask(SERVER_DATA_TYPE.MER);
                                        }
                                        if (!TestTask(SERVER_DATA_TYPE.GIS))
                                        {
                                            OilField.NewMessage("Добавлена загрузка ГИС.");
                                            AddTask(SERVER_DATA_TYPE.GIS);
                                        }
                                        if (!TestTask(SERVER_DATA_TYPE.LOGS))
                                        {
                                            OilField.NewMessage("Добавлена загрузка Каротажа.");
                                            AddTask(SERVER_DATA_TYPE.LOGS);
                                        }
                                        if (!TestTask(SERVER_DATA_TYPE.PERFORATION))
                                        {
                                            OilField.NewMessage("Добавлена загрузка Перфорации.");
                                            AddTask(SERVER_DATA_TYPE.PERFORATION);
                                        }
                                    }
                                    #endregion
                                }
                                else if (TestTask(SERVER_DATA_TYPE.PROJECT_WELLBORE))
                                {
                                    #region добавились скважины загружаем данные
                                    if ((LastWellCount > 0) && (LastWellCount < OilField.Wells.Count))
                                    {
                                        OilField.NewMessage(string.Format("Добавлено {0} новых проектных скважин [{1}]", (OilField.ProjectWells.Count - LastProjectWellCount), OilField.Name));
                                        if (!TestTask(SERVER_DATA_TYPE.COORDINATES))
                                        {
                                            OilField.NewMessage("Добавлена загрузка координат.");
                                            AddTask(SERVER_DATA_TYPE.COORDINATES);
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                        CurrentServerLoadMode = ServerLoadingMode.POST_PROCESSING;
                        worker.RunWorkerAsync();
                        break;
                    case ServerLoadingMode.POST_PROCESSING:
                        if (CurrentTask.Count > 0)
                        {
                            CurrentServerLoadMode = ServerLoadingMode.LOAD_FROM_SERVER;
                            worker.RunWorkerAsync();
                        }
                        else
                        {
                            OilField.NewProgress(100);
                            OilField.NewMessage(string.Format("Загрузка окончена [{0}]", OilField.Name));
                            DataLoading = false;
                            DataLoaded = true;
                        }
                        break;
                }
            }
        }
        
        #region Server Data Loading
        void LoadServerData(BackgroundWorker worker, DoWorkEventArgs e)
        {
            LastWellCount = -1;
            LastProjectWellCount = -1;
            OilFieldParamsItem tempParams = new OilFieldParamsItem();
            if (TestTask(SERVER_DATA_TYPE.SLEEP))
            {
                System.Threading.Thread.Sleep(20000);
            }
            else if (TestTask(SERVER_DATA_TYPE.WELLBORE_BY_ORACLE))
            {
                LastWellCount = OilField.Wells.Count;
                OilField.LoadWellsFromOracleServer(worker, e, GetTaskQuery(SERVER_DATA_TYPE.WELLBORE_BY_ORACLE));
            }
            else if (TestTask(SERVER_DATA_TYPE.CHESS_BY_ORACLE))
            {
                OilField.LoadChessFromOracleServer(worker, e, GetTaskQuery(SERVER_DATA_TYPE.CHESS_BY_ORACLE));
            }
            else if (TestTask(SERVER_DATA_TYPE.GTM))
            {
                OilField.LoadGTMFromServer(worker, e, GetTaskQuery(SERVER_DATA_TYPE.GTM));
            }
            else if (TestTask(SERVER_DATA_TYPE.WELL_REPAIR_ACTION))
            {
                OilField.LoadWellActionFromServer(worker, e, GetTaskQuery(SERVER_DATA_TYPE.WELL_REPAIR_ACTION));
            }
            else if (TestTask(SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE))
            {
                OilField.LoadWellResearchFromServer(worker, e, GetTaskQuery(SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE));
            }
            else if (TestTask(SERVER_DATA_TYPE.PERFORATION_BY_ORACLE))
            {
                OilField.LoadPerfDataFromOisServer(worker, e, GetTaskQuery(SERVER_DATA_TYPE.PERFORATION_BY_ORACLE));
            }
            else if (TestTask(SERVER_DATA_TYPE.SUSPEND))
            {
                OilField.LoadSuspendFromServer(worker, e, GetTaskQuery(SERVER_DATA_TYPE.SUSPEND));
            }
        }
        int ConversionCodes()
        {
            int result = 0;
            StratumDictionary PlastDict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            if (!TestTask(SERVER_DATA_TYPE.SLEEP)) OilField.NewMessage(string.Format("Перекодировка данных"));

            if (TestTask(SERVER_DATA_TYPE.SLEEP))
            {
                result = 1;
            }
            else if (TestTask(SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE))
            {
                result = PlastDict.ConvertStratumCodes(ServerTypeID.OIS, SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE, OilField);
            }
             return result;
        }
        void PostProcessing(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (CurrentTask.Count > 0)
            {
                if (OilField.ProjectPath.Length > 0)
                {
                    if (!System.IO.Directory.Exists(OilField.ProjectPath + "\\cache\\" + OilField.Name))
                    {
                        System.IO.Directory.CreateDirectory(OilField.ProjectPath + "\\cache\\" + OilField.Name);
                    }
                    if (TestTask(SERVER_DATA_TYPE.SLEEP))
                    {
                        RemoveTask(SERVER_DATA_TYPE.SLEEP);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.WELLBORE_BY_ORACLE))
                    {
                        OilField.WriteWellsToCache(worker, e);

                        #region добавились скважины загружаем данные
                        if ((LastWellCount > 0) && (LastWellCount < OilField.Wells.Count))
                        {
                            OilField.NewMessage(string.Format("Добавлено {0} новых скважин [{1}]", (OilField.Wells.Count - LastWellCount), OilField.Name));
                            if (!TestTask(SERVER_DATA_TYPE.COORDINATES))
                            {
                                OilField.NewMessage("Добавлена загрузка координат.");
                                AddTask(SERVER_DATA_TYPE.COORDINATES);
                            }
                            if (!TestTask(SERVER_DATA_TYPE.MER))
                            {
                                OilField.NewMessage("Добавлена загрузка МЭР.");
                                AddTask(SERVER_DATA_TYPE.MER);
                            }
                            if (!TestTask(SERVER_DATA_TYPE.GIS))
                            {
                                OilField.NewMessage("Добавлена загрузка ГИС.");
                                AddTask(SERVER_DATA_TYPE.GIS);
                            }
                            if (!TestTask(SERVER_DATA_TYPE.LOGS))
                            {
                                OilField.NewMessage("Добавлена загрузка Каротажа.");
                                AddTask(SERVER_DATA_TYPE.LOGS);
                            }
                            if (!TestTask(SERVER_DATA_TYPE.PERFORATION))
                            {
                                OilField.NewMessage("Добавлена загрузка Перфорации.");
                                AddTask(SERVER_DATA_TYPE.PERFORATION);
                            }
                        }
                        #endregion
                        LastWellCount = -1;
                        RemoveTask(SERVER_DATA_TYPE.WELLBORE_BY_ORACLE);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.WELLBORE))
                    {
                        OilField.WriteWellsToCache(worker, e);

                        #region добавились скважины загружаем данные
                        if ((LastWellCount > 0) && (LastWellCount < OilField.Wells.Count))
                        {
                            OilField.NewMessage(string.Format("Добавлено {0} новых скважин [{1}]", (OilField.Wells.Count - LastWellCount), OilField.Name));
                            if (!TestTask(SERVER_DATA_TYPE.COORDINATES))
                            {
                                OilField.NewMessage("Добавлена загрузка координат.");
                                AddTask(SERVER_DATA_TYPE.COORDINATES);
                            }
                            if (!TestTask(SERVER_DATA_TYPE.MER))
                            {
                                OilField.NewMessage("Добавлена загрузка МЭР.");
                                AddTask(SERVER_DATA_TYPE.MER);
                            }
                            if (!TestTask(SERVER_DATA_TYPE.GIS))
                            {
                                OilField.NewMessage("Добавлена загрузка ГИС.");
                                AddTask(SERVER_DATA_TYPE.GIS);
                            }
                            if (!TestTask(SERVER_DATA_TYPE.LOGS))
                            {
                                OilField.NewMessage("Добавлена загрузка Каротажа.");
                                AddTask(SERVER_DATA_TYPE.LOGS);
                            }
                            if (!TestTask(SERVER_DATA_TYPE.PERFORATION))
                            {
                                OilField.NewMessage("Добавлена загрузка Перфорации.");
                                AddTask(SERVER_DATA_TYPE.PERFORATION);
                            }
                        }
                        #endregion

                        LastWellCount = -1;
                        RemoveTask(SERVER_DATA_TYPE.WELLBORE);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.PROJECT_WELLBORE))
                    {
                        OilField.WriteProjectWellsToCache(worker, e);

                        #region добавились скважины загружаем данные
                        if ((LastProjectWellCount > 0) && (LastProjectWellCount < OilField.ProjectWells.Count))
                        {
                            OilField.NewMessage(string.Format("Добавлено {0} новых скважин [{1}]", (OilField.ProjectWells.Count - LastProjectWellCount), OilField.Name));
                            if (!TestTask(SERVER_DATA_TYPE.COORDINATES))
                            {
                                OilField.NewMessage("Добавлена загрузка координат.");
                                AddTask(SERVER_DATA_TYPE.COORDINATES);
                            }
                           
                        }
                        #endregion

                        LastProjectWellCount = -1;
                        RemoveTask(SERVER_DATA_TYPE.PROJECT_WELLBORE);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.COORDINATES))
                    {
                        if (!TestTask(SERVER_DATA_TYPE.MER) && !TestTask(SERVER_DATA_TYPE.MER_NEW)) 
                        {
                            OilField.ReSetWellIcons(worker, e, true);
                        }
                        OilField.WriteCoordToCache(worker, e, true);
                        RemoveTask(SERVER_DATA_TYPE.COORDINATES);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.MER) || TestTask(SERVER_DATA_TYPE.MER_NEW))
                    {
                        OilField.ReSetWellIcons(worker, e, false);
                        OilField.WriteCoordToCache(worker, e, false);
                        OilField.CreateDeltaMer(worker, e);
                        OilField.CreateMerFast(worker, e);

                        OilField.WriteMerToCache(worker, e);
                        OilField.ReCalcOilObjCodes(worker, e);

                        if (OilField.ReCalcSumParams(worker, e, false))
                        {
                            OilField.WriteSumParamsToCache(worker, e);
                        }
                        OilField.ClearSumParams(false);

                        OilField.FillAreaWellList(worker, e, false);
                        OilField.ReCalcSumAreaData(worker, e);
                        OilField.ClearSumArea(false);

                        OilField.ClearMerData(true);
                        RemoveTask(SERVER_DATA_TYPE.MER);
                        RemoveTask(SERVER_DATA_TYPE.MER_NEW);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.CONTOUR))
                    {
                        OilField.WriteContoursToCache(worker, e);
                        RemoveTask(SERVER_DATA_TYPE.CONTOUR);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.AREA))
                    {
                        OilField.RemoveAreasWithoutPlastCode();
                        OilField.LoadMerFromCache(worker, e, false);
                        OilField.FillAreaWellList(worker, e, false);
                        if (OilField.ReCalcSumAreaData(worker, e))
                        {
                            OilField.WriteSumAreaToCache(worker, e);
                        }
                        OilField.CalcAreasReserves();
                        OilField.WriteAreasToCache(worker, e);
                        OilField.ClearSumArea(false);
                        OilField.ClearMerData(true);
                        RemoveTask(SERVER_DATA_TYPE.AREA);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.WELLPAD))
                    {
                        OilField.WriteWellPadsToCache(worker, e);
                        RemoveTask(SERVER_DATA_TYPE.WELLPAD);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.PERFORATION))
                    {
                        OilField.WritePerfToCache(worker, e);
                        OilField.ClearPerfData(true);
                        RemoveTask(SERVER_DATA_TYPE.PERFORATION);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.LOGS))
                    {
                        OilField.WriteLogsToCache(worker, e);
                        OilField.WriteLogsBaseToCache(worker, e);
                        OilField.ClearLogsData(false);
                        OilField.ClearLogsBaseData(true);
                        RemoveTask(SERVER_DATA_TYPE.LOGS);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.PVT))
                    {
                        OilField.WritePVTParamsToCache(worker, e);
                        RemoveTask(SERVER_DATA_TYPE.PVT);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.GRID))
                    {
                        OilField.WriteGridListToCache(worker, e);
                        OilField.CalcAreasReserves();
                        OilField.WriteAreasToCache(worker, e);
                        RemoveTask(SERVER_DATA_TYPE.GRID);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.CHESS))
                    {
                        OilField.CreateDeltaChess(worker, e);
                        OilField.WriteChessToCache(worker, e);
                        OilField.CreateChessFast(worker, e, 365 * 2);
                        OilField.CreateChessInjFast(worker, e, 365 * 2);

                        if (OilField.ReCalcSumChessParams(worker, e))
                        {
                            OilField.WriteSumChessParamsToCache(worker, e);
                        }
                        OilField.ClearSumParams(true);
                        //OilField.FillAreaWells(worker, e);
                        //OilField.ReCalcSumAreaData(worker, e);
                        OilField.ClearChessData(true);
                        RemoveTask(SERVER_DATA_TYPE.CHESS);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.CHESS_BY_ORACLE))
                    {
                        OilField.CreateDeltaChess(worker, e);
                        OilField.WriteChessToCache(worker, e);
                        OilField.CreateChessFast(worker, e, 365 * 2);
                        OilField.CreateChessInjFast(worker, e, 365 * 2);

                        if (OilField.ReCalcSumChessParams(worker, e))
                        {
                            OilField.WriteSumChessParamsToCache(worker, e);
                        }
                        OilField.ClearSumParams(true);
                        //OilField.FillAreaWells(worker, e);
                        //OilField.ReCalcSumAreaData(worker, e);
                        OilField.ClearChessData(true);
                        RemoveTask(SERVER_DATA_TYPE.CHESS_BY_ORACLE);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.GTM))
                    {
                        OilField.WriteGTMToCache(worker, e);
                        RemoveTask(SERVER_DATA_TYPE.GTM);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.GTM))
                    {
                        OilField.WriteGTMToCache(worker, e);
                        RemoveTask(SERVER_DATA_TYPE.GTM);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.WELL_REPAIR_ACTION))
                    {
                        OilField.WriteWellActionToCache(worker, e);
                        RemoveTask(SERVER_DATA_TYPE.WELL_REPAIR_ACTION);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE))
                    {
                        OilField.WriteWellResearchToCache(worker, e);
                        RemoveTask(SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.PERFORATION_BY_ORACLE))
                    {
                        OilField.WritePerfToCache(worker, e);
                        OilField.ClearPerfData(true);
                        RemoveTask(SERVER_DATA_TYPE.PERFORATION_BY_ORACLE);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.SUSPEND))
                    {
                        OilField.WriteSuspendToCache(worker, e);
                        OilField.ClearSuspendData(true);
                        RemoveTask(SERVER_DATA_TYPE.SUSPEND);
                    }
                    else if (TestTask(SERVER_DATA_TYPE.GIS))
                    {
                        OilField.WriteGisToCache(worker, e);
                        OilField.ClearGisData(true);
                        RemoveTask(SERVER_DATA_TYPE.GIS);
                    }
                }
            }
        }
        #endregion
    }
}
