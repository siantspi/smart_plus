﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Base;

namespace DataLoader.Objects
{
    public delegate void OnNewMessage(LoggingObject sender, LOGGING_MESSAGE_TYPE Type, string Message);
    public delegate void OnNewProgress(LoggingObject sender, int Progress);
    public struct LogMessage
    {
        public LOGGING_MESSAGE_TYPE Type;
        public string SenderName;
        public DateTime Time;
        public string Message;
    }
    public class LogMessageCollection
    {
        public LOGGING_OBJECT_TYPE LogObjectType;
        public int LogObjectIndex;
        public int ProgressValue;
        public List<LogMessage> Messages;

        public LogMessageCollection(LOGGING_OBJECT_TYPE LogObjType, int LogObjIndex)
        {
            this.LogObjectType = LogObjType;
            this.LogObjectIndex = LogObjIndex;
            this.ProgressValue = 0;
            Messages = new List<LogMessage>();
        }
    }
    public class LogsCollector
    {
        public event OnNewMessage OnNewMessage;
        public event OnNewProgress OnNewProgress;
        List<LogMessageCollection> MessageCollectionList;

        public LogsCollector()
        {
            MessageCollectionList = new List<LogMessageCollection>();
        }

        public void ListeningObject(LoggingObject Object)
        {
            if (Object != null)
            {
                Object.OnMessage += new OnMessageDelegate(LoggingObject_OnMessage);
                Object.OnProgress +=new OnProgressDelegate(Object_OnProgress);
            }
        }

        public List<LogMessage> GetLogByObject(LOGGING_OBJECT_TYPE LogObjType)
        {
            List<LogMessage> list = new List<LogMessage>();
            for (int i = 0; i < MessageCollectionList.Count; i++)
            {
                if (MessageCollectionList[i].LogObjectType == LogObjType)
                {
                    list.AddRange(MessageCollectionList[i].Messages);
                }
            }
            return list;
        }
        public List<LogMessage> GetLogByObject(LOGGING_OBJECT_TYPE LogObjType, LOGGING_MESSAGE_TYPE MessType, int LogObjIndex)
        {
            List<LogMessage> list = new List<LogMessage>();
            for (int i = 0; i < MessageCollectionList.Count; i++)
            {
                if ((MessageCollectionList[i].LogObjectType == LogObjType) && 
                    (MessageCollectionList[i].LogObjectIndex == LogObjIndex))
                {
                    for (int j = 0; j < MessageCollectionList[i].Messages.Count; j++)
                    {
                        if (MessageCollectionList[i].Messages[j].Type == MessType)
                        {
                            list.Add(MessageCollectionList[i].Messages[j]);
                        }
                    }
                }
            }
            return list;
        }
        public List<LogMessage> GetLogByObject(LOGGING_OBJECT_TYPE LogObjType, int LogObjIndex)
        {
            List<LogMessage> list = new List<LogMessage>();
            for (int i = 0; i < MessageCollectionList.Count; i++)
            {
                if ((MessageCollectionList[i].LogObjectType == LogObjType) && (MessageCollectionList[i].LogObjectIndex == LogObjIndex))
                {
                    list.AddRange(MessageCollectionList[i].Messages);
                }
            }
            return list;
        }

        public LogMessageCollection GetCollection(LOGGING_OBJECT_TYPE LogObjType, int LogObjIndex)
        {
            LogMessageCollection collection = null;
            for (int i = 0; i < MessageCollectionList.Count; i++)
            {
                if ((MessageCollectionList[i].LogObjectType == LogObjType) && (MessageCollectionList[i].LogObjectIndex == LogObjIndex))
                {
                    collection = MessageCollectionList[i];
                    break;
                }
            }
            if (collection == null)
            {
                collection = new LogMessageCollection(LogObjType, LogObjIndex);
                MessageCollectionList.Add(collection);
            }
            return collection;
        }
        public string[] GetAllLogs()
        {
            List<string> allLogs = new List<string>();
            if (MessageCollectionList != null)
            {
                allLogs.Add("LogObjIndex;LogObjectType;SenderName;MessageType;MessageTime;MessageText;");
                for (int i = 0; i < MessageCollectionList.Count; i++)
                {
                    if (MessageCollectionList[i].Messages != null)
                    {
                        for (int j = 0; j < MessageCollectionList[i].Messages.Count; j++)
                        {
                            allLogs.Add(string.Format("{0};{1};{2};{3};{4};{5};", MessageCollectionList[i].LogObjectIndex, MessageCollectionList[i].LogObjectType, MessageCollectionList[i].Messages[j].SenderName, MessageCollectionList[i].Messages[j].Type, MessageCollectionList[i].Messages[j].Time, MessageCollectionList[i].Messages[j].Message));
                        }
                    }
                }
            }
            return allLogs.ToArray();
        }
        #region LISTENONG FUNCTION
        void LoggingObject_OnMessage(LoggingObject sender, LOGGING_MESSAGE_TYPE Type, string Message)
        {
            if (sender != null)
            {
                LogMessageCollection coll = GetCollection(sender.LogType, sender.Index);
                LogMessage mess;
                mess.SenderName = sender.Name;
                mess.Type = Type;
                mess.Time = DateTime.Now;
                mess.Message = Message;
                coll.Messages.Add(mess);
                if (OnNewMessage != null) OnNewMessage(sender, Type, Message);
            }
        }
        void Object_OnProgress(LoggingObject sender, int ProgressValue)
        {
            if (sender != null)
            {
                LogMessageCollection coll = GetCollection(sender.LogType, sender.Index);
                coll.ProgressValue = ProgressValue;
                if (OnNewProgress != null) OnNewProgress(sender, ProgressValue);
            }
        }
        #endregion
    }
}
