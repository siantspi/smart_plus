﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects;
using System.Windows.Forms;
using System.IO;

namespace DataLoader
{
    class ParamTask
    {
        #region DEFAULT QUERY STRING
        string[] DefaultQueryList = 
        {
            ""
        };
        #endregion

        FileInfo FileName;
        TreeNode rootnode;
        DataQuery Query;

        public ParamTask()
        {
            
            Query = new DataQuery();
            Query.ClearQueryList();
            
            rootnode = new TreeNode();
            TreeNode tn;
            FileName = new FileInfo(Application.StartupPath + "\\QueryList.txt");

            TreeNode Oraclenode = rootnode.Nodes.Add("База Oracle");
            Oraclenode.Checked = true;
            tn = Oraclenode.Nodes.Add("Cписок скважин"); tn.Tag = SERVER_DATA_TYPE.WELLBORE_BY_ORACLE; tn.Checked = true;
            tn = Oraclenode.Nodes.Add("Шахматка"); tn.Tag = SERVER_DATA_TYPE.CHESS_BY_ORACLE; tn.Checked = true;
            tn = Oraclenode.Nodes.Add("ГТМ"); tn.Tag = SERVER_DATA_TYPE.GTM; tn.Checked = true;
            tn = Oraclenode.Nodes.Add("Мероприятия на скважине"); tn.Tag = SERVER_DATA_TYPE.WELL_REPAIR_ACTION; tn.Checked = true;
            tn = Oraclenode.Nodes.Add("Исследования скважин"); tn.Tag = SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE; tn.Checked = true;
            tn = Oraclenode.Nodes.Add("Перфорация(OIS)"); tn.Tag = SERVER_DATA_TYPE.PERFORATION_BY_ORACLE; tn.Checked = true;
            tn = Oraclenode.Nodes.Add("КВЧ"); tn.Tag = SERVER_DATA_TYPE.SUSPEND; tn.Checked = true;
            Query.InitQueryList();

        }

        public List<WorkerTask> GetTaskList(string listString)
        {
            TreeNode thisnode;
            List<WorkerTask> taskList = new List<WorkerTask>();
            WorkerTask task;
            for (int i = 0; i < rootnode.Nodes.Count; i++)
            {
                for (int j = 0; j < rootnode.Nodes[i].Nodes.Count; j++)
                {
                    thisnode = rootnode.Nodes[i].Nodes[j];
                    string itemTag = thisnode.Tag.ToString();
                    IList<string> tasklist = listString.Split(';');
                    if ((thisnode.Tag != null) && (thisnode.Checked) && (tasklist.Contains(itemTag)))
                    {
                        task.Task = (int)thisnode.Tag;
                        task.Query = Query.QueryList[task.Task];
                        taskList.Add(task);
                    }
                }
            }
            return taskList;
        }

        public List<WorkerTask> GetAllTaskList()
        {
            TreeNode thisnode;
            List<WorkerTask> taskList = new List<WorkerTask>();
            WorkerTask task;
            for (int i = 0; i < rootnode.Nodes.Count; i++)
            {
                for (int j = 0; j < rootnode.Nodes[i].Nodes.Count; j++)
                {
                    thisnode = rootnode.Nodes[i].Nodes[j];
                    if ((thisnode.Tag != null) && (thisnode.Checked))
                    {
                        task.Task = (int)thisnode.Tag;
                        task.Query = Query.QueryList[task.Task];
                        taskList.Add(task);
                    }
                }
            }
            return taskList;
        }



    }
}
