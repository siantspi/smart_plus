﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace DataLoader
{
    class DataQuery
    {
        #region DEFAULT QUERY STRING
        string[] DefaultQueryList = 
        {
            // WELLBORE LIST
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty
        };
        #endregion

        public DataQuery()
        {
            QueryList = new string[23];
            FileName = new FileInfo(Application.StartupPath + "\\QueryList.txt");
        }

        FileInfo FileName;
        public string[] QueryList;
        #region QUERY LIST
        public void ClearQueryList()
        {
            for (int i = 0; i < QueryList.Length; i++)
            {
                QueryList[i] = string.Empty;
            }
        }
        public void InitQueryList()
        {
            if (!ReadQueryList(FileName))
            {
                for (int i = 0; i < DefaultQueryList.Length; i++)
                {
                    QueryList[i] = DefaultQueryList[i];
                }
                WriteQueryList(FileName);
            }
        }
        public bool ReadQueryList(FileInfo FileName)
        {
            if (File.Exists(FileName.FullName))
            {
                string[] StringList = File.ReadAllLines(FileName.FullName, Encoding.GetEncoding(1251));
                int i = 0, index = -1;
                string str = string.Empty;
                while (i < StringList.Length)
                {
                    if (StringList[i].StartsWith("[") && StringList[i].EndsWith("]"))
                    {
                        try
                        {
                            if (index > -1) QueryList[index] = str;
                            index = Convert.ToInt32(StringList[i].Replace("[", "").Replace("]", ""));
                            str = string.Empty;
                        }
                        catch
                        {
                            MessageBox.Show("Ошибка загрузки файла запросов баз данных.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ClearQueryList();
                            return false;
                        }
                    }
                    else
                    {
                        str += StringList[i];
                        if (StringList[i].Length > 0) str += "\n";
                    }
                    i++;
                }
                if (str.Length > 0) QueryList[index] = str;
                return true;
            }
            return false;
        }

        public void WriteQueryList()
        {
            WriteQueryList(this.FileName);
        }
        private void WriteQueryList(FileInfo FileName)
        {
            if (!Directory.Exists(FileName.DirectoryName)) Directory.CreateDirectory(FileName.DirectoryName);
            StreamWriter sw = new StreamWriter(FileName.FullName, false, Encoding.GetEncoding(1251));
            for (int i = 0; i < 23; i++) //костыль?
            {
                sw.WriteLine(string.Format("[{0}]", i));
                sw.WriteLine(QueryList[i]);
            }
            sw.Close();
        }
        #endregion
    }
}
