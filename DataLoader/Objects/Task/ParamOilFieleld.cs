﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Project;
using System.Windows.Forms;
using DataLoader.Objects.Base;
using DataLoader.DictionaryObjects;

namespace DataLoader
{
    class ParamOilFieleld
    {
        Project srcProject;
        TreeNode OilFieledsTree;

        public ParamOilFieleld(Project Proj)
        {
            OilFieledsTree = new TreeNode();
            srcProject = Proj;
            TreeNode projNode = OilFieledsTree.Nodes.Add(Proj.Name);
            for (int i = 0; i < srcProject.NGDUList.Count; i++)
            {
                projNode.Nodes.Add(srcProject.NGDUList[i].GetNGDUNode());
            }
            projNode.Expand();
            
        }

        public void FillOilFieldList(TreeNode RootNode, List<Oilfield> List)
        {
            if ((RootNode.Text != "Общие данные") && (RootNode.Tag != null) && (((LoggingObject)RootNode.Tag).LogType == LOGGING_OBJECT_TYPE.OILFIELD))
            {
                List.Add((Oilfield)RootNode.Tag);
            }
            if (RootNode.Nodes.Count > 0)
            {
                for (int i = 0; i < RootNode.Nodes.Count; i++)
                {
                    FillOilFieldList(RootNode.Nodes[i], List);
                }
            }
        }

        public void FillOilFieldList(TreeNode RootNode, List<Oilfield> List, string datebase)
        {
            if ((RootNode.Text != "Общие данные") && (RootNode.Tag != null) && (((LoggingObject)RootNode.Tag).LogType == LOGGING_OBJECT_TYPE.OILFIELD))
            {
                Oilfield itemOilfield = (Oilfield)RootNode.Tag;
                IList<string> baselist = datebase.Split(';');
                if (baselist.Contains(itemOilfield.ParamsDict.FieldCode.ToString()))
                {
                    List.Add((Oilfield)RootNode.Tag);
                }
            }

            if (RootNode.Nodes.Count > 0)
            {
                for (int i = 0; i < RootNode.Nodes.Count; i++)
                {
                    FillOilFieldList(RootNode.Nodes[i], List, datebase);
                }
            }
        }

        public List<Oilfield> GetAllOilfields()
        {
            List<Oilfield> list = new List<Oilfield>();
            if (OilFieledsTree.Nodes.Count == 1) FillOilFieldList(OilFieledsTree.Nodes[0], list);
            return list;
        }
       
        public List<Oilfield> GetOilFieldList(string datebase)
        {
            List<Oilfield> list = new List<Oilfield>();
            if (OilFieledsTree.Nodes.Count == 1) FillOilFieldList(OilFieledsTree.Nodes[0], list, datebase);
            return list;            
        }
    }
}
