﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DataLoader.Objects.Base;
using DataLoader.Objects.Project;


namespace DataLoader.Objects
{
    class ParamSettings
    {
        public List<WorkerTask> TaskList
        {
            get { return taskList; }
        }

        public List<Oilfield> GetOilFieldList(Project.Project project)
        {
            ParamOilFieleld paramOilField = new ParamOilFieleld(project);
            oilfieldList = paramOilField.GetOilFieldList(datebase);
            return oilfieldList; 
        }

        public string ProjPath
        {
            get { return projPath; }
        }

        private List<WorkerTask> taskList;
        private string projPath;
        private string datebase;
        private string task;
        private List<Oilfield> oilfieldList;
        
        
        private FileInfo FileName;

        public ParamSettings(string[] keys)
        {
            List<string> keyslist = new List<string>(keys);
            if (keyslist.Contains("/path"))
            {
                // todo
                //keyslist.Find("/path")
                //FileName = new FileInfo(keys[2]);
                FileName = new FileInfo(Path.Combine(Application.StartupPath, "task.txt"));
            }
            else
            {
                FileName = new FileInfo(Path.Combine(Application.StartupPath,"task.txt"));
            }
            readParamSettings();

        }

        
        private bool readParamSettings()
        {
            if (File.Exists(FileName.FullName))
            {
                string[] StringList = File.ReadAllLines(FileName.FullName, Encoding.GetEncoding(1251));
                int i = 0;
                string str = string.Empty;
                while (i < StringList.Length)
                {
                    if (StringList[i].StartsWith("[") && StringList[i].EndsWith("]"))
                    {
                        try
                        {
                            str = StringList[i].Replace("[", "").Replace("]", "");
                            switch (str.ToLower())
                            {
                                case "path":
                                    i++;
                                    while (i < StringList.Length)
                                    {
                                        if (StringList[i].StartsWith("["))
                                        {
                                            i--;
                                            break;
                                        }
                                        projPath = StringList[i];
                                        i++;
                                    }
                                    break;
                                case "task":
                                    i++;
                                    while (i < StringList.Length)
                                    {
                                        if (StringList[i].StartsWith("["))
                                        {
                                            i--;
                                            break;
                                        }
                                        task = StringList[i];
                                        ParamTask paramTask = new ParamTask();
                                        taskList = paramTask.GetTaskList(task);
                                        i++;
                                    }
                                    break;
                                case "code":
                                    i++;
                                    while (i < StringList.Length)
                                    {
                                        if (StringList[i].StartsWith("["))
                                        {
                                            i--;
                                            break;
                                        }
                                        datebase = StringList[i];
                                        i++;
                                    }
                                    break;
                            }
                        }
                        catch
                        {
                            MessageBox.Show("Ошибка загрузки параметров", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    else
                    {
                        str += StringList[i];
                        if (StringList[i].Length > 0) str += "\n";
                    }
                    i++;
                }
                return true;
            }
            return false;
        }

    }
}
