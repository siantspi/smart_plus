﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Base;
using System.ComponentModel;
using System.Data.OracleClient;
using System.IO;
using DataLoader.DictionaryObjects;
using DataLoader.Objects.Data;
using System.Windows.Forms;

namespace DataLoader.Objects.Project
{
    public class Oilfield : LoggingObject
    {
        public static int VersionMerStratumList = 0;

        DictionaryCollection DictList;
        public List<int> MerOilObjects;

        public string ProjectPath { get; set; }

        public OilFieldParamsItem ParamsDict { get; set; }
        public OilfieldCoefItem CoefDict { get; set; }

        #region DATA LOADED
        public bool MerLoaded { get; set; }
        public bool GisLoaded { get; set; }
        public bool PerfLoaded { get; set; }
        public bool LogsBaseLoaded { get; set; }
        public bool LogsLoaded { get; set; }
        public bool ChessLoaded { get; set; }
        public bool CoordLoaded { get; set; }
        public bool CoordLoadedfromCache { get; set; }
        public bool ProjWellLoaded { get; set; }
        public bool WellSuspendLoaded { get; set; }
        

        public bool WellsLoaded { get { return (( Wells != null) && (Wells.Count > 0)); } }
        public bool AreasLoaded { get { return ((Areas != null) && (Areas.Count > 0)); } }
        public bool GridsLoaded { get { return ((Grids != null) && (Grids.Count > 0)); } }
        public bool ContoursLoaded { get { return ((Contours != null) && (Contours.Count > 0)); } }
        public bool PVTLoaded { get { return ((PVT != null) && (PVT.Count > 0)); } }
        public bool SumLoaded { get { return ((SumParams != null) && (SumParams.Count > 0)); } }
        public bool SumChessLoaded { get { return ((SumChessParams != null) && (SumChessParams.Count > 0)); } }
        public bool Table81Loaded { get { return ((Table81 != null) && (Table81.Count > 0)); } }
        public bool ProjectWellsLoaded { get { return ((ProjectWells != null) && (ProjectWells.Count > 0)); } }

        #endregion

        public DateTime minMerDate { get; set; }
        public DateTime maxMerDate {get; set; }

        public List<Well> Wells;
        public List<Contour> Contours;
        public List<Grid> Grids;
        public List<Area> Areas;
        public List<WellPad> WellPads;

        public List<Well> ProjectWells;
        
        public PVTParams PVT;
        public SumParameters SumParams;
        public SumParameters SumChessParams;
        public Table81Params Table81;

        OilfieldWorker Worker;

        public Oilfield(string ProjectPath, DictionaryCollection DictList) : base(LOGGING_OBJECT_TYPE.OILFIELD)
        {
            Worker = new OilfieldWorker(this, DictList);
            this.ProjectPath = ProjectPath;
            this.DictList = DictList;

            //OilFieldAreas = null;
            MerOilObjects = new List<int>();
            Wells = new List<Well>();
            Areas = new List<Area>();
            Contours = new List<Contour>();
            Grids = new List<Grid>();
            WellPads = new List<WellPad>();
            ProjectWells = new List<Well>();
        }

        public void FreeDataMemory(bool UseGC)
        {
            int i;
            ClearBufferList(false);
            ClearChessData(false);
            ClearCoreData(false);
            ClearCoreTestData(false);
            ClearGisData(false);
            ClearGTMData(false);
            ClearLogsBaseData(false);
            ClearLogsData(false);
            ClearMerData(false);
            ClearPerfData(false);
            ClearSumArea(false);
            ClearSumParams(false);
            ClearWellActionData(false);
            for (i = 0; i < Wells.Count; i++)
            {
                Wells[i].Icons.Clear();
                Wells[i] = null;
            }
            Wells.Clear();
            if (Contours != null)
            {
                for (i = 0; i < Contours.Count; i++)
                {
                    if(Contours[i]._points != null) Contours[i]._points.ArrayPoint.Clear();
                    Contours[i] = null;
                }
                Contours.Clear();
            }
            if (Grids != null)
            {
                for (i = 0; i < Grids.Count; i++)
                {
                    Grids[i].FreeDataMemory();
                    Grids[i] = null;
                }
                Grids.Clear();
            }
            if (Areas != null)
            {
                for (i = 0; i < Areas.Count; i++)
                {
                    Areas[i].contour._points.ArrayPoint.Clear();
                    Areas[i] = null;
                }
                Areas.Clear();
            }
            if (WellPads != null)
            {
                for (i = 0; i < WellPads.Count; i++)
                {
                    WellPads[i] = null;
                }
                WellPads.Clear();
            }
            if (UseGC) GC.GetTotalMemory(true);
        }

        #region BackGroundWorker
        public bool IsBusy { get { return Worker.IsBusy; } }
        public bool DataLoaded { get { return Worker.DataLoaded; } set { Worker.DataLoaded = value; } }
        public bool DataLoadCanceled { get { return Worker.Canceled; } }
        public bool DataLoadError { get { return Worker.ErrorLoading; } }
        public void RunTaskAsync(List<WorkerTask> Task)
        {
            List<WorkerTask> newTask = new List<WorkerTask>(Task.Count);
            for (int i = 0; i < Task.Count; i++)
            {
                newTask.Add(Task[i]);
            }
            Worker.RunTaskAsync(newTask);
        }
        private void ReportProgress(BackgroundWorker worker, float i, float len)
        {
            int percentComplete = (int)(i / len * 100);
            if (percentComplete < 0) percentComplete = 0;
            else if (percentComplete > 100) percentComplete = 100;
            worker.ReportProgress(percentComplete);
        }
        private void ReportProgress(BackgroundWorker worker, float i, float len, WorkerState UserState)
        {
            int percentComplete = (int)(i / (len + 1) * 100);
            if (percentComplete < 0) percentComplete = 0;
            else if (percentComplete > 100) percentComplete = 100;
            worker.ReportProgress(percentComplete, UserState);
        }
        #endregion BackGroundWorker

        #region GET WELL INDEX
        public int GetWellIndex(int WellBoreId)
        {
            for (int i = 0; i < Wells.Count; i++)
            {
                if (Wells[i].BoreId == WellBoreId)
                {
                    return i;
                }
            }
            return -1; 
        }
        public int GetWellIndex(string WellName, int OilFieldAreaCode)
        {
            for (int i = 0; i < Wells.Count; i++)
            {
                if ((WellName.Equals(Wells[i].Name, StringComparison.OrdinalIgnoreCase)) && (OilFieldAreaCode == Wells[i].OilFieldAreaCode))
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetWellIndex(string WellName)
        {
            for (int i = 0; i < Wells.Count; i++)
            {
                if (WellName.Equals(Wells[i].Name, StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
            }
            return -1;
        }

        public List<Well> GetWellIndexList(string WellName)
        {
            List<Well> list = new List<Well>();
            for (int i = 0; i < Wells.Count; i++)
            {
                if (WellName.Equals(Wells[i].Name, StringComparison.OrdinalIgnoreCase))
                {
                    if (list == null) list = new List<Well>(5);
                    list.Add(Wells[i]);
                }
            }
            return list;
        }
        public int TestEqualsWell(string WellName)
        {
            int count = -1;
            int k = -1;
            for (int i = 0; i < Wells.Count; i++)
            {
                if (WellName.Equals(Wells[i].Name, StringComparison.OrdinalIgnoreCase))
                {
                    count++;
                    k = i;
                    if (count > 0)
                    {
                        count = -2;
                        break;
                    }
                }
            }
            if ((count == 0) && (k > -1)) return k;
            return count;
        }
        #endregion

        #region MEMORY BUFFER LIST
        private MemoryStream[] BuffList;
        private MemoryStream GetBuffer(bool IsPrimary)
        {
            int index = -1;
            if (BuffList == null) BuffList = new MemoryStream[2];
            index = (IsPrimary) ? 0 : 1;
            if (index != -1)
            {
                if (BuffList[index] == null)
                {
                    BuffList[index] = new MemoryStream(1 * 1024 * 1024);
                }
                else
                {
                    BuffList[index].SetLength(1 * 1024 * 1024);
                    ClearMemoryStream(BuffList[index]);
                }
                return BuffList[index];
            }
            return null;
        }
        public void ClearBufferList(bool GCCollect)
        {
            if (BuffList != null)
            {
                for (int i = 0; i < 2; i++) BuffList[i] = null;
            }
            BuffList = null;
            if (GCCollect) GC.GetTotalMemory(true);
        }
        void ClearMemoryStream(MemoryStream ms)
        {
            ms.Seek(0, SeekOrigin.Begin);
            while (ms.Position < ms.Length) ms.WriteByte(0);
            ms.Seek(0, SeekOrigin.Begin);
        }
        #endregion

        #region TREE NODE
        public TreeNode GetOilFieldNode()
        {
            TreeNode ofNode = new TreeNode(Name);
            ofNode.Tag = this;
            TreeNode dataNode;
            if (AreasLoaded)
            {
                dataNode = new TreeNode();
                dataNode.Text = string.Format("Ячейки заводнения[{0}]", Areas.Count);
                dataNode.Nodes.Add("Empty");
                ofNode.Nodes.Add(dataNode);
            }
            if (ContoursLoaded)
            {
                dataNode = new TreeNode();
                dataNode.Text = string.Format("Контуры[{0}]", Contours.Count);
                dataNode.Nodes.Add("Empty");
                ofNode.Nodes.Add(dataNode);
            }
            if (GridsLoaded)
            {
                dataNode = new TreeNode();
                dataNode.Text = string.Format("Сетки[{0}]", Grids.Count);
                dataNode.Nodes.Add("Empty");
                ofNode.Nodes.Add(dataNode);
            }
            if (WellsLoaded)
            {
                dataNode = new TreeNode();
                dataNode.Text = string.Format("Скважины[{0}]", Wells.Count);
                dataNode.Nodes.Add("Empty");
                ofNode.Nodes.Add(dataNode);
            }
            return ofNode;
        }

        public TreeNode[] GetObjectList(string NodeText)
        {
            TreeNode[] NodeList = null;
            if ((NodeText.StartsWith("Скважины")) && (Wells != null))
            {
                NodeList = new TreeNode[Wells.Count];
                int k = 0;
                for (int i = 0; i < Wells.Count; i++)
                {
                    TreeNode tn = new TreeNode();
                    if (Wells[i].Index == Wells[i].MainBoreIndex)
                    {
                        tn.Text = Wells[i].Name;
                        NodeList[k++] = tn;
                        if(Wells[i].SideBores != null)
                        {
                            for (int j = 0; j < Wells[i].SideBores.Length; j++)
                            {
                                tn = new TreeNode();
                                tn.Text = string.Format("{0}[{1} ствол]", Wells[i].Name, j + 2);
                                NodeList[k++] = tn;
                            }
                        }
                    }
                }
            }
            else if ((NodeText.StartsWith("Сетки")) && (Grids != null))
            {
                NodeList = new TreeNode[Grids.Count];
                int k = 0;
                for (int i = 0; i < Grids.Count; i++)
                {
                    TreeNode tn = new TreeNode();
                    tn = new TreeNode();
                    tn.Text = Grids[i].Name;
                    NodeList[k++] = tn;
                }
            }
            else if ((NodeText.StartsWith("Контуры")) && (Contours != null))
            {
                NodeList = new TreeNode[Contours.Count];
                int k = 0;
                for (int i = 0; i < Contours.Count; i++)
                {
                    TreeNode tn = new TreeNode();
                    tn = new TreeNode();
                    tn.Text = Contours[i].Name;
                    NodeList[k++] = tn;
                }
            }
            return NodeList;
        }
        #endregion

        // DATA READ / WRITE

        #region OIL OBJECTS
        public void AddOilObjects(List<int> NewOilObjects)
        {
            if(this.MerOilObjects == null) this.MerOilObjects = new List<int>();
            for (int i = 0; i < NewOilObjects.Count; i++)
            {
                if (this.MerOilObjects.IndexOf(NewOilObjects[i]) == -1)
                {
                    this.MerOilObjects.Add(NewOilObjects[i]);
                }
            }
        }
        public bool ReCalcOilObjCodes(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            Well w;
            int i, j;
            bool CallMerLoading = false;
            if (!MerLoaded)
            {
                LoadMerFromCache(worker, e, false);
                CallMerLoading = true;
            }
            List<double> sumLiqInjOilObj = new List<double>();
            MerItem item;
            int index;
            MerOilObjects.Clear();
            DateTime MaxDate = DateTime.MinValue, MinDate = DateTime.MaxValue;
            for (i = 0; i < Wells.Count; i++)
            {
                w = Wells[i];
                if (w.MerLoaded)
                {
                    for (j = 0; j < w.Mer.Count; j++)
                    {
                        item = w.Mer.Items[j];
                        if (MinDate > item.Date) MinDate = item.Date;
                        if (MaxDate < item.Date) MaxDate = item.Date;

                        index = MerOilObjects.IndexOf(item.PlastId);
                        if ((item.PlastId != 0) && (index == -1))
                        {
                            MerOilObjects.Add(item.PlastId);
                            sumLiqInjOilObj.Add(item.Oil + item.Wat + item.Gas);
                        }
                        else if (index > -1)
                        {
                            sumLiqInjOilObj[index] += item.Oil + item.Wat + item.Gas;
                        }
                        retValue = true;
                    }
                }
            }
            if (retValue) // выбрасываем нулевые объекты
            {
                i = 0;
                while (i < sumLiqInjOilObj.Count)
                {
                    if (sumLiqInjOilObj[i] == 0)
                    {
                        sumLiqInjOilObj.RemoveAt(i);
                        MerOilObjects.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            if (MinDate != DateTime.MaxValue) minMerDate = MinDate;
            if (MaxDate != DateTime.MinValue) maxMerDate = MaxDate;
            WriteOilObjCodesToCache();
            if ((MerLoaded) && (CallMerLoading)) ClearMerData(true);
            return retValue;
        }
        public bool LoadOilObjCodesFromCache()
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\OilObj.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int i, len, fileVersionMerStratumList;
                fileVersionMerStratumList = br.ReadInt32();
                if (fileVersionMerStratumList == Oilfield.VersionMerStratumList || fileVersionMerStratumList == 1)//0 или 1
                {
                    len = br.ReadInt32();
                    for (i = 0; i < len; i++) MerOilObjects.Add(br.ReadInt32());

                    try
                    {
                        minMerDate = DateTime.FromOADate(br.ReadDouble());
                        maxMerDate = DateTime.FromOADate(br.ReadDouble());
                    }
                    catch (Exception)
                    {
                        minMerDate = DateTime.MinValue;
                        maxMerDate = DateTime.MaxValue;
                    }
                }
                br.Close();
                retValue = true;
            }
            return retValue;
        }
        public bool WriteOilObjCodesToCache()
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\OilObj.bin";

            if (MerOilObjects.Count > 0)
            {
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);

                bw.Write(Oilfield.VersionMerStratumList);
                bw.Write(MerOilObjects.Count);
                for (int i = 0; i < MerOilObjects.Count; i++) bw.Write(MerOilObjects[i]);

                bw.Write(minMerDate.ToOADate());
                bw.Write(maxMerDate.ToOADate());
                bw.Close();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                retValue = true;
            }
            else
            {
                if (File.Exists(cacheName)) File.Delete(cacheName);
            }
            return retValue;
        }
        #endregion

        #region WELL PAD
 
        public bool LoadWellPadsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка кустов с кеша";
            userState.Element = ParamsDict.OilFieldName;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\wellpads.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);

                int count;
                WellPad wp;
                if (WellPad.Version == br.ReadInt32())
                {
                    count = br.ReadInt32();
                    for (int i = 0; i < count; i++)
                    {
                        wp = new WellPad();
                        wp.ReadFromCache(br, this);
                        if (wp.Wells.Count > 0)
                        {
                            WellPads.Add(wp);
                            retValue = true;
                        }
                        ReportProgress(worker, i, count, userState);
                    }
                }
                br.Close();
           }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteWellPadsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;// переписать
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Запись кустов в кэш";
            userState.Element = ParamsDict.OilFieldName;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\wellpads.bin";
            if ((WellPads != null) && (WellPads.Count > 0))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(WellPad.Version);
                bw.Write(WellPads.Count);
                for (int i = 0; i < WellPads.Count; i++)
                {
                    WellPads[i].WriteToCache(bw);
                    ReportProgress(worker, i, WellPads.Count, userState);
                }
                bw.Close();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                retValue = true;
            }
            return retValue;
        }
        #endregion

        #region CONTOUR LIST
        public bool LoadContoursFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool HeadOnly)
        {
            int i, iLen = 0;
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка контуров с кэша";
            userState.Element = Name;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\contours.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                if (Contour.Version == br.ReadInt32())
                {
                    iLen = br.ReadInt32();
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        else
                        {
                            Contour cntr = new Contour();
                            if (HeadOnly) cntr.ReadHeadLineFromCacheBin(br); else cntr.ReadFromCacheBin(br);
                            if (Contours == null) Contours = new List<Contour>();
                            Contours.Add(cntr);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen);
                    }
                }
                br.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadContoursDataFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка контуров с кэша";
            userState.Element = Name;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\contours.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                if (Contour.Version == br.ReadInt32())
                {
                    iLen = br.ReadInt32();
                    if (iLen == Contours.Count)
                    {
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                retValue = false;
                                break;
                            }
                            else
                            {
                                Contours[i].ReadFromCacheBin(br);
                                retValue = true;
                            }
                            ReportProgress(worker, i, iLen);
                        }
                    }
                }
                br.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteContoursToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            if (Contours != null)
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\contours.bin";
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);

                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = string.Empty;
                userState.WorkCurrentTitle = "Запись контуров в кэш";
                userState.Element = Name;

                int i, iLen;
                using (bw)
                {
                    iLen = Contours.Count;
                    if (iLen > 0)
                    {
                        if (worker != null) worker.ReportProgress(0, userState);
                        bw.Write(Contour.Version);
                        bw.Write(iLen);
                        for (i = 0; i < iLen; i++)
                        {
                            if ((worker != null) && (worker.CancellationPending))
                            {
                                e.Cancel = true;
                            }
                            else
                            {
                                Contours[i].WriteToCacheBin(i, bw);
                                retValue = true;
                            }
                            if (worker != null) ReportProgress(worker, i, iLen);
                        }
                    }
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);

                if ((!retValue) || (iLen == 0)) File.Delete(cacheName);
            }
            return retValue;
        }
        public void FreeContoursData()
        {
            Contour cntr;
            for (int i = 0; i < Contours.Count; i++)
            {
                cntr = Contours[i];
                if (cntr.ContourType >= 0) cntr.FreeDataMemory();
            }
        }
        public void ExportCondContours(string destPath)
        {
            if (Contours.Count > 0)
            {
                StreamWriter file = new StreamWriter(destPath, true, Encoding.GetEncoding(1251));
                string head = ";0;0;1;1;255;-1;0;16777215;-1;0";
                Contour cntr;
                PointD point;
                for (int i = 0; i < Contours.Count; i++)
                {
                    cntr = Contours[i];
                    if ((cntr != null) && (cntr.ContourType == -3))
                    {
                        file.WriteLine("*" + Name + ".ktr" + head);
                        for (int j = 0; j < cntr._points.Count; j++)
                        {
                            point = cntr._points[j];
                            file.WriteLine(String.Format("{0} {1}", point.X, point.Y));
                        }
                        if (cntr._points.Count > 0)
                        {
                            point = cntr._points[0];
                            file.WriteLine(String.Format("{0} {1}", point.X, point.Y));
                        }
                        break;
                    }
                }
                file.Close();
            }
        }
        #endregion

        #region AREA LIST
        public void CalcAreasReserves()
        {
            Area area;
            Contour cntr;
            Grid grid;
            int i;
            double square = 0;
            double sumvals = 0;
            double k = 0;
            int res;
            bool loaded;
            List<Grid> grids = new List<Grid>();
            for (int AreaIndex = 0; AreaIndex < Areas.Count; AreaIndex++)
            {
                grids.Clear();
                area = this.Areas[AreaIndex];
                area.Params.InitOilReserv = 0;
                cntr = area.contour;
                for (i = 0; i < this.Grids.Count; i++)
                {
                    grid = (Grid)this.Grids[i];
                    if (grid.StratumCode == area.StratumCode)
                    {
                        grids.Add(grid);
                    }
                }
                if (grids.Count == 0)
                {
                    var dict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                    StratumTreeNode node;
                    for (i = 0; i < this.Grids.Count; i++)
                    {
                        grid = this.Grids[i];
                        node = dict.GetStratumTreeNode(grid.StratumCode);
                        if (node != null && node.GetParentLevelByCode(area.StratumCode) != -1)
                        {
                            grids.Add(grid);
                        }
                    }
                }
                if (grids.Count > 0)
                {
                    for (i = 0; i < grids.Count; i++)
                    {
                        square = 0;
                        sumvals = 0;
                        res = 0;
                        loaded = false;
                        if (!grids[i].DataLoaded)
                        {
                            res = this.LoadGridDataFromCache(null, null, grids[i].Index);
                            loaded = true;
                        }
                        if (res == 0)
                        {
                            grids[i].CalcSumValues(null, null, cntr, out square, out sumvals);
                            if (square != 0)
                            {
                                if (!area.pvt.IsEmpty)
                                {
                                    k = area.pvt.OilDensity * area.pvt.Porosity * area.pvt.OilInitialSaturation / area.pvt.OilVolumeFactor;
                                    area.Params.InitOilReserv = k * sumvals;
                                }
                                break;
                            }
                            if (loaded) grids[i].FreeDataMemory();
                        }
                    }
                }
            }
        }
        public void RemoveAreasWithoutPlastCode()
        {
            int i = 0;
            while (i < Areas.Count)
            {
                if (((Area)Areas[i]).StratumCode == -1)
                {
                    Areas.RemoveAt(i);
                    i--;
                }
                i++;
            }
            for (i = 0; i < Areas.Count; i++)
            {
                ((Area)Areas[i]).Index = i;
            }
        }
        public List<Area> GetAreaList(int StratumCode, bool UseStratumTree)
        {
            var dict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            StratumTreeNode node1, node2;
            node1 = dict.GetStratumTreeNode(StratumCode);
            List<Area> areas = new List<Area>();
            for (int i = 0; i < Areas.Count; i++)
            {
                if (Areas[i].StratumCode == StratumCode)
                {
                    areas.Add(Areas[i]);
                }
                else if (UseStratumTree)
                {
                    node2 = dict.GetStratumTreeNode(Areas[i].StratumCode);
                    if ((node1.GetParentLevelByCode(node2.StratumCode) > -1) || (node2.GetParentLevelByCode(node1.StratumCode) > -1))
                    {
                        areas.Add(Areas[i]);
                    }
                }
            }
            return areas;
        }
        public void ResetAreasIndexes(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (this.Areas.Count > 0)
            {
                List<Area> newAreasList = new List<Area>(this.Areas.Count);
                int pos, pos2, ind;
                string name;
                for (int i = 0; i < this.Areas.Count; i++)
                {
                    for (int j = 0; j < this.Areas.Count; j++)
                    {
                        name = ((Area)this.Areas[j]).contour.Name;
                        pos = name.IndexOf('(');
                        if (pos > 0)
                        {
                            pos2 = name.IndexOf(')', pos);
                            if (pos2 == -1) pos2 = name.IndexOf(")", pos);
                            ind = Convert.ToInt32(name.Substring(pos + 1, pos2 - pos - 1));
                            if (ind - 1 == i)
                            {
                                newAreasList.Add((Area)this.Areas[j]);
                                break;
                            }
                        }
                    }
                }
                this.Areas = newAreasList;
            }
        }
        public bool FillAreaWellList(BackgroundWorker worker, DoWorkEventArgs e, bool ClearMerData)
        {
            bool retValue = false;
            Area area;
            Well w;
            int i, j, n, t, indArea;
            double k;
            bool CallMerLoading = false;
            bool find = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Заполнение списка скважин ячеек заводнения";
            userState.Element = this.Name;

            var dict = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            MerItem merItem;
            MerItemEx merItemEx;

            if (worker != null) worker.ReportProgress(0, userState);
            //ArrayList list = new ArrayList();
            List<Well> wellList = new List<Well>();
            List<Well> allWellList = new List<Well>();
            List<double> wellCoefList = new List<double>();
            List<int> oilFieldAreaCodeList = new List<int>();
            List<int> oilFieldAreaCodeCountList = new List<int>();
            List<int> oilObjList = new List<int>();
            List<int> workObjList = new List<int>();
            this.LoadWellsAreaCount(worker, e);

            if (!this.MerLoaded)
            {
                this.LoadMerFromCache(worker, e, false);
                CallMerLoading = true;
            }
            StratumTreeNode itemNode;
            for (j = 0; j < this.Areas.Count; j++)
            {
                area = (Area)this.Areas[j];
                wellList.Clear();
                oilFieldAreaCodeList.Clear();
                oilFieldAreaCodeCountList.Clear();
                wellCoefList.Clear();
                oilObjList.Clear();
                allWellList.Clear();

                for (n = 0; n < this.Wells.Count; n++)
                {
                    w = (Well)this.Wells[n];

                    if (w.CountAreas == 0) continue; else k = 1 / (double)w.CountAreas;

                    if ((w.CoordLoaded) &&
                        (((area.contour.PointBoundsEntry(w.X, w.Y)) &&
                          (area.contour.PointBodyEntry(w.X, w.Y))) ||
                          (area.contour.GetIndexPointByXY(w.X, w.Y) > -1)))
                    {
                        find = false;
                        allWellList.Add(w);
                        if (w.MerLoaded)
                        {
                            workObjList.Clear();
                            for (t = 0; t < w.Mer.Count; t++)
                            {
                                merItem = w.Mer.Items[t];
                                merItemEx = w.Mer.GetItemEx(t);
                                if ((merItem.CharWorkId == 11) ||
                                    (merItem.CharWorkId == 12) ||
                                    (merItem.CharWorkId == 15) ||
                                    (merItem.CharWorkId == 20))
                                {
                                    if ((merItem.WorkTime + merItem.CollTime > 0) &&
                                        (merItem.Oil + merItem.Wat + merItem.Gas + merItemEx.Injection + merItemEx.CapGas + merItemEx.GasCondensate + merItemEx.NaturalGas > 0) &&
                                        (workObjList.IndexOf(merItem.PlastId) == -1))
                                    {
                                        workObjList.Add(merItem.PlastId);
                                    }
                                }
                            }
                            for (t = 0; t < workObjList.Count; t++)
                            {
                                if (area.StratumCode == workObjList[t])
                                {
                                    find = true;
                                    if (oilObjList.IndexOf(workObjList[t]) == -1)
                                    {
                                        oilObjList.Add(workObjList[t]);
                                    }
                                }
                                else
                                {
                                    itemNode = dict.GetStratumTreeNode(workObjList[t]);
                                    if (itemNode != null && itemNode.GetParentLevelByCode(area.StratumCode) != -1)
                                    {
                                        find = true;
                                        if (oilObjList.IndexOf(workObjList[t]) == -1)
                                        {
                                            oilObjList.Add(workObjList[t]);
                                        }
                                    }
                                }
                            }
                        }
                        if (!find) continue;
                        wellList.Add(w);
                        wellCoefList.Add(k);
                        indArea = oilFieldAreaCodeList.IndexOf(w.OilFieldAreaCode);
                        if (w.OilFieldAreaCode > -1)
                        {
                            if (indArea == -1)
                            {
                                oilFieldAreaCodeList.Add(w.OilFieldAreaCode);
                                oilFieldAreaCodeCountList.Add(1);
                            }
                            else
                            {
                                oilFieldAreaCodeCountList[indArea] = (int)oilFieldAreaCodeCountList[indArea] + 1;
                            }
                        }
                    }
                }
                if (allWellList.Count > 0) area.AllWellList = allWellList.ToArray();
                indArea = 0;
                if (ParamsDict.LoadByAreas > 0)
                {
                    for (n = 0; n < oilFieldAreaCodeList.Count; n++)
                    {
                        if (indArea < (int)oilFieldAreaCodeCountList[n])
                        {
                            area.OilFieldAreaCode = (int)oilFieldAreaCodeList[n];
                            indArea = (int)oilFieldAreaCodeCountList[n];
                        }
                    }
                }
                if (wellList.Count > 0)
                {
                    if (ParamsDict.LoadByAreas > 0)
                    {
                        i = 0;
                        while (i < wellList.Count)
                        {
                            if (((Well)wellList[i]).OilFieldAreaCode != area.OilFieldAreaCode)
                            {
                                wellList.RemoveAt(i);
                                i--;
                            }
                            i++;
                        }
                    }
                    area.WellList = new Well[wellList.Count];
                    area.WellCoefList = new double[wellList.Count];
                    for (n = 0; n < wellList.Count; n++)
                    {
                        area.WellList[n] = (Well)wellList[n];
                        area.WellCoefList[n] = (double)wellCoefList[n];
                    }
                }
                area.StratumList = null;
                if (oilObjList.Count > 0)
                {
                    area.StratumList = oilObjList.ToArray();
                }
                retValue = true;
            }
            if ((ClearMerData) || (CallMerLoading)) this.ClearMerData(false);
            if (worker != null)
            {
                if (retValue) this.WriteAreasToCache(worker, e);
                worker.ReportProgress(100);
            }
            return retValue;
        }
 
        public bool LoadAreasFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false, res;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка ячеек из кэша";
            userState.Element = this.Name;

            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\areas.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader file = new BinaryReader(fs);
                try
                {
                    int ver = file.ReadInt32();
                    if (Area.Version >= ver)
                    {
                        iLen = file.ReadInt32();
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                break;
                            }
                            Area area = new Area();
                            res = area.ReadFromCache(worker, e, this, file, ver);
                            if (res)
                            {
                                area.OilFieldIndex = this.Index;
                                //area.contour._FillBrush = 33;
                                area.Index = this.Areas.Count;
                                this.Areas.Add(area);
                                retValue = true;
                            }
                            ReportProgress(worker, i, iLen, userState);
                        }
                    }
                }
                catch
                {
                    this.Areas.Clear();
                    retValue = false;
                }
                finally
                {
                    file.Close();
                }
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteAreasToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\areas.bin";
            if (!Directory.Exists(ProjectPath + "\\cache\\" + this.Name))
            {
                Directory.CreateDirectory(ProjectPath + "\\cache\\" + this.Name);
            }

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter file = new BinaryWriter(fs);
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись ячеек в кэш";
            userState.Element = this.Name;

            int i, iLen;
            using (file)
            {
                iLen = this.Areas.Count;
                file.Write(Area.Version);
                file.Write(iLen);
                if (iLen > 0)
                {
                    if (worker != null) worker.ReportProgress(0, userState);
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker != null && worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        Areas[i].WriteToCache(worker, e, file);
                        retValue = true;
                        if (worker != null) ReportProgress(worker, i, iLen);
                    }
                }
                file.Close();
            }
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);
            return retValue;
        }
        public bool LoadWellsAreaCount(BackgroundWorker worker, DoWorkEventArgs e)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Расчет коэфициентов скважин по ЯЗ";
            userState.Element = this.Name;

            Well w;
            Area area;

            for (int i = 0; i < this.Wells.Count; i++)
            {
                w = (Well)this.Wells[i];
                w.CountAreas = 0;
                for (int j = 0; j < this.Areas.Count; j++)
                {
                    area = (Area)this.Areas[j];
                    if (area.contour.GetIndexPointByXY2(w.X, w.Y) > -1) w.CountAreas++;
                    if (w.CountAreas == 0)
                    {
                        if (area.contour.PointBoundsEntry(w.X, w.Y) && (area.contour.PointBodyEntry(w.X, w.Y)))
                        {
                            w.CountAreas++;
                            break;
                        }
                    }
                }
                if (worker != null) worker.ReportProgress((int)(i * 100f / this.Areas.Count), userState);
            }
            return true;
        }
        public void ExportAreasContours(string destPath)
        {
            if ((Directory.Exists(destPath)) && (Areas.Count > 0))
            {
                if (File.Exists(destPath + "\\" + Name + ".kts")) File.Delete(destPath + "\\" + Name + ".kts");
                StreamWriter file = new StreamWriter(destPath + "\\" + Name + ".kts", false, Encoding.GetEncoding(1251));
                string head = ";0;0;1;1;255;-1;0;16777215;-1;0";
                Area area;
                double xLoc, yLoc;
                PointD point;
                for (int i = 0; i < Areas.Count; i++)
                {
                    area = Areas[i];
                    if (area.contour != null)
                    {
                        file.WriteLine("*" + Name + "_" + (i + 1).ToString() + ".ktr" + head);
                        for (int j = 0; j < area.contour._points.Count; j++)
                        {
                            point = area.contour._points[j];
                            CoefDict.LocalCoord_FromGlobal(point.X, point.Y, out xLoc, out yLoc);
                            file.WriteLine(String.Format("{0} {1}", xLoc, yLoc));
                        }
                        if (area.contour._points.Count > 0)
                        {
                            point = area.contour._points[0];
                            CoefDict.LocalCoord_FromGlobal(point.X, point.Y, out xLoc, out yLoc);
                            file.WriteLine(String.Format("{0} {1}", xLoc, yLoc));
                        }
                    }
                }
                file.Close();
            }
        }
        #endregion

        #region GRID LIST
        
        public int LoadGridDataFromCache(BackgroundWorker worker, DoWorkEventArgs e, int GridIndex)
        {
            int i, iLen = 0, ver;
            long seek;
            int retValue = -1;
            Grid grid = null;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка сетки в память";
            userState.Element = this.Name;

            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\grids.bin";
            System.Diagnostics.Process proc = System.Diagnostics.Process.GetCurrentProcess();
            if (proc.WorkingSet64 > 1300 * 1024 * 1024)
            {
                retValue = -2;
            }
            else if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                ver = br.ReadInt32();
                if (Grid.Version >= ver)
                {
                    iLen = br.ReadInt32();
                    if ((iLen > 0) && (GridIndex < iLen) && (this.Grids.Count > 0))
                    {
                        grid = (Grid)this.Grids[GridIndex];
                        grid.DataLoading = true;
                        userState.Element = grid.Name;

                        fs.Seek(8 + 8 * GridIndex, SeekOrigin.Begin);
                        seek = br.ReadInt64();
                        fs.Seek(seek, SeekOrigin.Begin);
                        try
                        {
                            if (grid.LoadFromCache(worker, e, fs, true))
                            {
                                grid.OilFieldCode = this.ParamsDict.FieldCode;
                                retValue = 0;
                            }
                        }
                        catch (OutOfMemoryException ex)
                        {
                            retValue = -2;
                            grid.FreeDataMemory();
                        }
                    }
                }
                br.Close();
                if (grid != null) grid.DataLoading = false;
            }
            return retValue;
        }
        public bool LoadGridHeadFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false;
            Grid grid;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка сеток из кэша";
            userState.Element = this.Name;
            string cacheName = this.ProjectPath + "\\cache\\" + this.Name + "\\grids.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                int ver = br.ReadInt32();
                if (Grid.Version >= ver)
                {
                    iLen = br.ReadInt32();
                    fs.Seek(8 + 8 * iLen, SeekOrigin.Begin);
                    for (i = 0; i < iLen; i++)
                    {
                        grid = new Grid();
                        grid.Index = i;
                        grid.OilFieldCode = this.ParamsDict.FieldCode;
                        grid.Visible = false;
                        grid.Date = DateTime.FromOADate(br.ReadDouble());
                        grid.StratumCode = br.ReadInt32();
                        grid.OilFieldName = br.ReadString();
                        grid.GridType = br.ReadInt32();
                        grid.Name = br.ReadString();
                        this.Grids.Add(grid);
                    }
                }
                br.Close();
                retValue = true;
            }
            else
            {
                userState.Error = "Не найден файл кэша сеток";
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadGridDataFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, iLen = 0;
            bool retValue = false;
            Grid grid;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка сеток из кэша";
            userState.Element = this.Name;
            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\grids.bin";
            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                long seekStart;
                int ver = br.ReadInt32();
                if (Grid.Version >= ver)
                {
                    iLen = br.ReadInt32();
                    seekStart = br.ReadInt64();
                    fs.Seek(seekStart, SeekOrigin.Begin);

                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            grid = new Grid();
                            grid.Index = i;
                            grid.OilFieldCode = this.ParamsDict.FieldCode;
                            grid.LoadFromCache(worker, e, fs, true);
                            this.Grids.Add(grid);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen);
                    }
                    fs.Seek(4 + 8 * iLen, SeekOrigin.Begin);
                    for (i = 0; i < iLen; i++)
                    {
                        grid = ((Grid)Grids[i]);
                        grid.Date = DateTime.FromOADate(br.ReadDouble());
                        grid.StratumCode = br.ReadInt32();
                        grid.OilFieldName = br.ReadString();
                        grid.GridType = br.ReadInt32();
                        grid.Name = br.ReadString();
                    }
                }
                br.Close();
            }
            else
            {
                userState.Error = "Не найден файл кэша сеток";
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteGridListToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\grids.bin";

            FileStream fs = new FileStream(cacheName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись сеток в кэш";
            userState.Element = this.Name;

            int i, iLen;
            long[] seek;
            using (bw)
            {
                iLen = this.Grids.Count;
                if (iLen > 0)
                {
                    worker.ReportProgress(0, userState);
                    bw.Write(Grid.Version);
                    bw.Write(iLen);
                    seek = new long[iLen];
                    fs.Seek(iLen * 8, SeekOrigin.Current);
                    for (i = 0; i < iLen; i++)
                    {
                        bw.Write(Grids[i].Date.ToOADate());
                        bw.Write(Grids[i].StratumCode);
                        bw.Write(Grids[i].OilFieldName);
                        bw.Write(Grids[i].GridType);
                        bw.Write(Grids[i].Name);
                    }
                    for (i = 0; i < iLen; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                        }
                        else
                        {
                            seek[i] = fs.Position;
                            Grids[i].WriteToCache(fs);
                            retValue = true;
                        }
                        ReportProgress(worker, i, iLen);
                    }
                    fs.Seek(8, SeekOrigin.Begin);
                    for (i = 0; i < iLen; i++)
                    {
                        bw.Write(seek[i]);
                    }
                }
            }
            File.SetCreationTime(cacheName, DateTime.Now);
            File.SetLastWriteTime(cacheName, DateTime.Now);

            if ((!retValue) || (iLen == 0)) File.Delete(cacheName);
            return retValue;
        }
        #endregion

        #region SUM MER
        public bool ReCalcSumParams(BackgroundWorker worker, DoWorkEventArgs e, bool ClearMerData)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Пересчет суммарных показателей по МЭР";
            userState.Element = this.Name;
            if ((this.Wells.Count > 0) && (this.MerLoaded || this.LoadMerFromCache(worker, e, false)))
            {
                if (this.MerOilObjects.Count == 0)
                {
                    if (!this.LoadOilObjCodesFromCache()) return false;
                }
                int i, j, k;
                DateTime minDate = DateTime.MaxValue;
                DateTime maxDate = DateTime.MinValue;
                Well w;
                MerItem item;
                DateTime[] minMaxDate = new DateTime[this.MerOilObjects.Count * 2];
                for (k = 0; k < this.MerOilObjects.Count; k++)
                {
                    minMaxDate[2 * k] = minDate;
                    minMaxDate[2 * k + 1] = maxDate;
                }
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    if ((w.MerLoaded) && (w.Mer.Count > 0))
                    {
                        for (j = 0; j < w.Mer.Count; j++)
                        {
                            item = w.Mer.Items[j];
                            for (k = 0; k < this.MerOilObjects.Count; k++)
                            {
                                if (item.Date < minDate) minDate = item.Date;
                                if (item.Date > maxDate) maxDate = item.Date;
                                if (item.PlastId == this.MerOilObjects[k])
                                {
                                    if (item.Date < minMaxDate[2 * k]) minMaxDate[2 * k] = item.Date;
                                    if (item.Date > minMaxDate[2 * k + 1]) minMaxDate[2 * k + 1] = item.Date;
                                }
                            }
                        }
                    }
                }
                if ((minDate != DateTime.MaxValue) && (maxDate != DateTime.MinValue))
                {
                    int c;
                    int month, years;
                    DateTime dt;
                    years = maxDate.Year - minDate.Year + 1;
                    month = (years - 2) * 12 + (13 - minDate.Month) + maxDate.Month;

                    if (month > 0)
                    {
                        this.SumParams = new SumParameters(this.MerOilObjects.Count + 1);

                        SumParamItem[] SumMonth = new SumParamItem[month];
                        SumParamItem[] SumYear = new SumParamItem[years];

                        dt = minDate;
                        for (i = 0; i < month; i++)
                        {
                            SumMonth[i].Date = dt;
                            SumMonth[i].InjGas = -1;
                            SumMonth[i].NatGas = -1;
                            SumMonth[i].GasCondensate = -1;
                            dt = dt.AddMonths(1);
                        }
                        dt = new DateTime(minDate.Year, 01, 01);
                        for (i = 0; i < years; i++)
                        {
                            SumYear[i].Date = dt;
                            SumYear[i].InjGas = -1;
                            SumYear[i].NatGas = -1;
                            SumYear[i].GasCondensate = -1;
                            dt = dt.AddYears(1);
                        }

                        this.SumParams.Add(-1, SumMonth, SumYear);

                        for (k = 0; k < this.MerOilObjects.Count; k++)
                        {
                            years = minMaxDate[2 * k + 1].Year - minMaxDate[2 * k].Year + 1;
                            month = (years - 2) * 12 + (13 - minMaxDate[2 * k].Month) + minMaxDate[2 * k + 1].Month;
                            SumMonth = new SumParamItem[month];
                            SumYear = new SumParamItem[years];

                            dt = minMaxDate[2 * k];
                            for (i = 0; i < month; i++)
                            {
                                SumMonth[i].Date = dt;
                                SumMonth[i].InjGas = -1;
                                SumMonth[i].NatGas = -1;
                                SumMonth[i].GasCondensate = -1;
                                dt = dt.AddMonths(1);
                            }
                            dt = new DateTime(minMaxDate[2 * k].Year, 01, 01);

                            for (i = 0; i < years; i++)
                            {
                                SumYear[i].Date = dt;
                                SumYear[i].InjGas = -1;
                                SumYear[i].NatGas = -1;
                                SumYear[i].GasCondensate = -1;
                                dt = dt.AddYears(1);
                            }
                            this.SumParams.Add(this.MerOilObjects[k], SumMonth, SumYear);
                        }

                        int indMonth, indYears;
                        double[] timeProd2 = new double[SumParams.Count];
                        double[] timeInj2 = new double[SumParams.Count];
                        double[] timeProdYear2 = new double[SumParams.Count];
                        double[] timeInjYear2 = new double[SumParams.Count];
                        int[][] lastYearProd = new int[SumParams.Count][];
                        for (i = 0; i < lastYearProd.Length; i++)
                        {
                            lastYearProd[i] = new int[2];
                        }


                        int iLen = Wells.Count, objIndex;
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return false;
                            }
                            w = (Well)Wells[i];
                            if (w.MerLoaded)
                            {
                                
                                int hours;
                                MerCompItem compItem;
                                MerCompPlastItem plastItem;
                                MerWorkTimeItem wtItemProd, wtItemInj, wtItemScoop;
                                MerComp merComp = new MerComp(w.Mer);
                                for (j = 0; j < lastYearProd.Length; j++)
                                {
                                    lastYearProd[j][0] = 0;
                                    lastYearProd[j][1] = 0;
                                }
                                for (j = 0; j < merComp.Count; j++)
                                {
                                    compItem = merComp[j];

                                    #region заполняем по суммарному мэр - объект All
                                    SumMonth = SumParams[0].MonthlyItems;
                                    SumYear = SumParams[0].YearlyItems;
                                    indMonth = (compItem.Date.Year - minDate.Year) * 12 + compItem.Date.Month - minDate.Month;

                                    wtItemProd = compItem.TimeItems.GetAllProductionTime();
                                    wtItemInj = compItem.TimeItems.GetAllInjectionTime();
                                    wtItemScoop = compItem.TimeItems.GetAllScoopTime();
                                    hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                    wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                    wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                    wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                   // if ((wtItemProd.WorkTime > 0) && ((compItem.StateId != 6)))
                                   //     SumMonth[indMonth].Fprod++;
                                    if (wtItemProd.WorkTime > 0)
                                    {
										SumMonth[indMonth].Fprod++;                                        SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                    }
                                    
                                    if (wtItemInj.WorkTime > 0)
                                    {
                                        SumMonth[indMonth].Finj++;
                                        SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                        if (compItem.SumInjectionGas > 0) SumMonth[indMonth].WorkTimeInjGas += wtItemInj.WorkTime;
                                    }

                                    if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                    for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                    {
                                        switch (compItem.CharWorkIds[c])
                                        {
                                            case 11:
                                                SumMonth[indMonth].Liq += compItem.SumLiquid;
                                                SumMonth[indMonth].Oil += compItem.SumOil;
                                                if (SumMonth[indMonth].Liq > 0)
                                                {
                                                    SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                }

                                                SumMonth[indMonth].LiqV += compItem.SumLiquidV;
                                                SumMonth[indMonth].OilV += compItem.SumOilV;
                                                if (SumMonth[indMonth].LiqV > 0)
                                                {
                                                    SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                }
                                                break;
                                            case 12:
                                                if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                SumMonth[indMonth].NatGas += compItem.SumNaturalGas;
                                                break;
                                            case 13:
                                                SumMonth[indMonth].Scoop += compItem.SumScoop;
                                                break;
                                            case 15:
                                                if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                SumMonth[indMonth].GasCondensate += compItem.SumGasCondensate;
                                                break;
                                            case 20:
                                                SumMonth[indMonth].Inj += compItem.SumInjection;
                                                if (merComp.UseInjGas)
                                                {
                                                    if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                    SumMonth[indMonth].InjGas += compItem.SumInjectionGas;
                                                }
                                                break;
                                        }
                                    }
                                    // по годам
                                    indYears = compItem.Date.Year - minDate.Year;
                                    if (wtItemProd.WorkTime > 0)
                                    {
                                        if (lastYearProd[0][0] != compItem.Date.Year)
                                        {
                                            SumYear[indYears].Fprod++;
                                            lastYearProd[0][0] = compItem.Date.Year;
                                        }
                                        SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                    }
                                    if (wtItemInj.WorkTime > 0)
                                    {
                                        if (lastYearProd[0][1] != compItem.Date.Year)
                                        {
                                            SumYear[indYears].Finj++;
                                            lastYearProd[0][1] = compItem.Date.Year;
                                        }
                                        SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                        if (compItem.SumInjectionGas > 0) SumYear[indYears].WorkTimeInjGas += wtItemInj.WorkTime;
                                    }
                                    if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                    for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                    {
                                        switch (compItem.CharWorkIds[c])
                                        {
                                            case 11:
                                                SumYear[indYears].Liq += compItem.SumLiquid;
                                                SumYear[indYears].Oil += compItem.SumOil;
                                                if (SumYear[indYears].Liq > 0)
                                                {
                                                    SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                }
                                                SumYear[indYears].LiqV += compItem.SumLiquidV;
                                                SumYear[indYears].OilV += compItem.SumOilV;
                                                if (SumYear[indYears].LiqV > 0)
                                                {
                                                    SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                }
                                                break;
                                            case 12:
                                                if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                SumYear[indYears].NatGas += compItem.SumNaturalGas;
                                                break;
                                            case 13:
                                                SumYear[indYears].Scoop += compItem.SumScoop;
                                                break;
                                            case 15:
                                                if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                SumYear[indYears].GasCondensate += compItem.SumGasCondensate;
                                                break;
                                            case 20:
                                                SumYear[indYears].Inj += compItem.SumInjection;
                                                if (merComp.UseInjGas)
                                                {
                                                    if (SumYear[indYears].InjGas < 0) SumYear[indYears].InjGas = 0;
                                                    SumYear[indYears].InjGas += compItem.SumInjectionGas;
                                                }
                                                break;
                                        }
                                    }
                                    #endregion

                                    #region По объектам

                                    for (k = 0; k < compItem.PlastItems.Count; k++)
                                    {
                                        plastItem = compItem.PlastItems[k];
                                        objIndex = SumParams.GetIndexByObjCode(plastItem.PlastCode);
                                        if (objIndex > -1)
                                        {
                                            SumMonth = SumParams[objIndex].MonthlyItems;
                                            SumYear = SumParams[objIndex].YearlyItems;

                                            wtItemProd = compItem.TimeItems.GetProductionTime(plastItem.PlastCode);
                                            wtItemInj = compItem.TimeItems.GetInjectionTime(plastItem.PlastCode);
                                            wtItemScoop = compItem.TimeItems.GetScoopTime(plastItem.PlastCode);
                                            hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                            wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                            wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                            wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                            indMonth = (compItem.Date.Year - minMaxDate[2 * objIndex - 2].Year) * 12 + compItem.Date.Month - minMaxDate[2 * objIndex - 2].Month;

                                            //if ((wtItemProd.WorkTime > 0) && ((compItem.StateId != 6)))
                                            //    SumMonth[indMonth].Fprod++;
                                            if (wtItemProd.WorkTime > 0)
                                            {
												SumMonth[indMonth].Fprod++;                                                SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                            }

                                            
                                            if (wtItemInj.WorkTime > 0)
                                            {
                                                SumMonth[indMonth].Finj++;
                                                SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                                if (plastItem.InjGas > 0) SumMonth[indMonth].WorkTimeInjGas += wtItemInj.WorkTime;
                                            }

                                            if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;


                                            for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                            {
                                                switch (compItem.CharWorkIds[c])
                                                {
                                                    case 11:
                                                        SumMonth[indMonth].Liq += plastItem.Liq;
                                                        SumMonth[indMonth].Oil += plastItem.Oil;
                                                        if (SumMonth[indMonth].Liq > 0)
                                                        {
                                                            SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                        }
                                                        SumMonth[indMonth].LiqV += plastItem.LiqV;
                                                        SumMonth[indMonth].OilV += plastItem.OilV;
                                                        if (SumMonth[indMonth].LiqV > 0)
                                                        {
                                                            SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                        }
                                                        break;
                                                    case 12:
                                                        if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                        SumMonth[indMonth].NatGas += plastItem.NatGas;
                                                        break;
                                                    case 13:
                                                        SumMonth[indMonth].Scoop += plastItem.Scoop;
                                                        break;
                                                    case 15:
                                                        if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                        SumMonth[indMonth].GasCondensate += plastItem.Condensate;
                                                        break;
                                                    case 20:
                                                        SumMonth[indMonth].Inj += plastItem.Inj;
                                                        if (merComp.UseInjGas)
                                                        {
                                                            if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                            SumMonth[indMonth].InjGas += plastItem.InjGas;
                                                        }
                                                        break;
                                                }
                                            }
                                            // по годам
                                            indYears = compItem.Date.Year - minMaxDate[2 * objIndex - 2].Year;
                                            if (wtItemProd.WorkTime > 0)
                                            {
                                                if (lastYearProd[objIndex][0] != compItem.Date.Year)
                                                {
                                                    SumYear[indYears].Fprod++;
                                                    lastYearProd[objIndex][0] = compItem.Date.Year;
                                                }
                                                SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                            }
                                            if (wtItemInj.WorkTime > 0)
                                            {
                                                if (lastYearProd[objIndex][1] != compItem.Date.Year)
                                                {
                                                    SumYear[indYears].Finj++;
                                                    lastYearProd[objIndex][1] = compItem.Date.Year;
                                                }
                                                SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                                if (plastItem.InjGas > 0) SumYear[indYears].WorkTimeInjGas += wtItemInj.WorkTime;
                                            }
                                            if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                            for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                            {
                                                switch (compItem.CharWorkIds[c])
                                                {
                                                    case 11:
                                                        SumYear[indYears].Liq += plastItem.Liq;
                                                        SumYear[indYears].Oil += plastItem.Oil;
                                                        if (SumYear[indYears].Liq > 0)
                                                        {
                                                            SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                        }
                                                        SumYear[indYears].LiqV += plastItem.LiqV;
                                                        SumYear[indYears].OilV += plastItem.OilV;
                                                        if (SumYear[indYears].LiqV > 0)
                                                        {
                                                            SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                        }
                                                        break;
                                                    case 12:
                                                        if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                        SumYear[indYears].NatGas += plastItem.NatGas;
                                                        break;
                                                    case 13:
                                                        SumYear[indYears].Scoop += plastItem.Scoop;
                                                        break;
                                                    case 15:
                                                        if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                        SumYear[indYears].GasCondensate += plastItem.Condensate;
                                                        break;
                                                    case 20:
                                                        SumYear[indYears].Inj += plastItem.Inj;
                                                        if (merComp.UseInjGas)
                                                        {
                                                            if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                            SumYear[indYears].InjGas += plastItem.InjGas;
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            
                        }
                        retValue = true;

                    }
                }
            }
            //if (retValue)
            //{
            //    ReCalcDeltaBP(worker, e);
            //}
            if (ClearMerData) this.ClearMerData(false);
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadSumParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Чтение суммарных показателей из кэша";
            userState.Element = this.Name;

            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\sum.bin";
            if (File.Exists(cacheName))
            {
                BinaryReader bfr = null;
                try
                {
                    int i, j, k, lenMonth, lenYears, len_unpack, lenItems, objCode;
                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    bfr = new BinaryReader(fs);
                    if (SumParameters.Version == bfr.ReadInt32())
                    {
                        len_unpack = bfr.ReadInt32();

                        MemoryStream ms = new MemoryStream(len_unpack);
                        ConvertEx.CopyStream(fs, ms, len_unpack);
                        bfr.Close();
                        MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                        ms.Dispose();
                        BinaryReader br = new BinaryReader(unpackMS);

                        lenItems = br.ReadInt32();
                        this.SumParams = new SumParameters(lenItems + 1);
                        for (k = 0; k < lenItems; k++)
                        {
                            objCode = br.ReadInt32();
                            lenMonth = br.ReadInt32();
                            SumParamItem[] SumMonth = new SumParamItem[lenMonth];
                            for (i = 0; i < lenMonth; i++)
                            {
                                if ((worker != null) && (worker.CancellationPending))
                                {
                                    e.Cancel = true;
                                    br.Close();
                                    return false;
                                }
                                else
                                {
                                    SumMonth[i].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                    SumMonth[i].Liq = br.ReadDouble();
                                    SumMonth[i].Oil = br.ReadDouble();
                                    SumMonth[i].Watering = br.ReadDouble();
                                    SumMonth[i].LiqV = br.ReadDouble();
                                    SumMonth[i].OilV = br.ReadDouble();
                                    SumMonth[i].WateringV = br.ReadDouble();
                                    SumMonth[i].Inj = br.ReadDouble();
                                    SumMonth[i].InjGas = br.ReadDouble();
                                    SumMonth[i].NatGas = br.ReadDouble();
                                    SumMonth[i].GasCondensate = br.ReadDouble();
                                    SumMonth[i].Scoop = br.ReadDouble();
                                    SumMonth[i].WorkTimeProd = br.ReadDouble();
                                    SumMonth[i].WorkTimeInj = br.ReadDouble();
                                    SumMonth[i].WorkTimeInjGas = br.ReadDouble();
                                    SumMonth[i].Fprod = br.ReadInt32();
                                    SumMonth[i].Finj = br.ReadInt32();
                                    SumMonth[i].Fscoop = br.ReadInt32();
                                }
                            }

                            lenYears = br.ReadInt32();
                            SumParamItem[] SumYears = new SumParamItem[lenYears];

                            for (i = 0; i < lenYears; i++)
                            {
                                if ((worker != null) && (worker.CancellationPending))
                                {
                                    e.Cancel = true;
                                    br.Close();
                                    return false;
                                }
                                else
                                {
                                    SumYears[i].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                    SumYears[i].Liq = br.ReadDouble();
                                    SumYears[i].Oil = br.ReadDouble();
                                    SumYears[i].Watering = br.ReadDouble();
                                    SumYears[i].LiqV = br.ReadDouble();
                                    SumYears[i].OilV = br.ReadDouble();
                                    SumYears[i].WateringV = br.ReadDouble();
                                    SumYears[i].Inj = br.ReadDouble();
                                    SumYears[i].InjGas = br.ReadDouble();
                                    SumYears[i].NatGas = br.ReadDouble();
                                    SumYears[i].GasCondensate = br.ReadDouble();
                                    SumYears[i].Scoop = br.ReadDouble();
                                    SumYears[i].WorkTimeProd = br.ReadDouble();
                                    SumYears[i].WorkTimeInj = br.ReadDouble();
                                    SumYears[i].WorkTimeInjGas = br.ReadDouble();
                                    SumYears[i].Fprod = br.ReadInt32();
                                    SumYears[i].Finj = br.ReadInt32();
                                    SumYears[i].Fscoop = br.ReadInt32();
                                }
                            }
                            this.SumParams.Add(objCode, SumMonth, SumYears);
                            retValue = true;
                        }
                    }
                }
                catch
                {
                    retValue = false;
                }
                if (bfr != null) bfr.Close();
            }
            if (worker != null && !NotReportProgress) worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteSumParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись суммарных показателей в кэш";
            userState.Element = this.Name;

            if ((SumParams != null) && (SumParams.Count > 0))
            {
                string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\sum.bin";

                int i, j, k, lenMonth, lenYears;
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(SumParams.Count);
                for (k = 0; k < SumParams.Count; k++)
                {
                    SumParamItem[] SumMonth = SumParams[k].MonthlyItems;
                    SumParamItem[] SumYears = SumParams[k].YearlyItems;
                    lenMonth = SumMonth.Length;
                    lenYears = SumYears.Length;

                    bw.Write(SumParams[k].OilObjCode);
                    bw.Write(SumMonth.Length);
                    for (i = 0; i < lenMonth; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            bw.Close();
                            return false;
                        }
                        else
                        {
                            bw.Write(SmartPlusSystem.PackDateTime(SumMonth[i].Date));
                            bw.Write(SumMonth[i].Liq);
                            bw.Write(SumMonth[i].Oil);
                            bw.Write(SumMonth[i].Watering);
                            bw.Write(SumMonth[i].LiqV);
                            bw.Write(SumMonth[i].OilV);
                            bw.Write(SumMonth[i].WateringV);
                            bw.Write(SumMonth[i].Inj);
                            bw.Write(SumMonth[i].InjGas);
                            bw.Write(SumMonth[i].NatGas);
                            bw.Write(SumMonth[i].GasCondensate);
                            bw.Write(SumMonth[i].Scoop);
                            bw.Write(SumMonth[i].WorkTimeProd);
                            bw.Write(SumMonth[i].WorkTimeInj);
                            bw.Write(SumMonth[i].WorkTimeInjGas);
                            bw.Write(SumMonth[i].Fprod);
                            bw.Write(SumMonth[i].Finj);
                            bw.Write(SumMonth[i].Fscoop);
                        }
                    }
                    bw.Write(SumYears.Length);
                    for (i = 0; i < lenYears; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            bw.Close();
                            return false;
                        }
                        else
                        {
                            bw.Write(SmartPlusSystem.PackDateTime(SumYears[i].Date));
                            bw.Write(SumYears[i].Liq);
                            bw.Write(SumYears[i].Oil);
                            bw.Write(SumYears[i].Watering);
                            bw.Write(SumYears[i].LiqV);
                            bw.Write(SumYears[i].OilV);
                            bw.Write(SumYears[i].WateringV);
                            bw.Write(SumYears[i].Inj);
                            bw.Write(SumYears[i].InjGas);
                            bw.Write(SumYears[i].NatGas);
                            bw.Write(SumYears[i].GasCondensate);
                            bw.Write(SumYears[i].Scoop);
                            bw.Write(SumYears[i].WorkTimeProd);
                            bw.Write(SumYears[i].WorkTimeInj);
                            bw.Write(SumYears[i].WorkTimeInjGas);
                            bw.Write(SumYears[i].Fprod);
                            bw.Write(SumYears[i].Finj);
                            bw.Write(SumYears[i].Fscoop);
                        }
                    }
                }
                //MemoryStream packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                MemoryStream packedMS = ConvertEx.PackStream(ms);
                bw.Close();
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                bw = new BinaryWriter(fs);
                bw.Write(SumParameters.Version);
                bw.Write((int)packedMS.Length);
                packedMS.Seek(0, SeekOrigin.Begin);
                packedMS.WriteTo(fs);
                bw.Close();
                packedMS.Dispose();
                GC.Collect();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public void ClearSumParams(bool useGC)
        {
            if (this.SumParams != null) this.SumParams.Clear();
            this.SumParams = null;
            if (useGC) GC.Collect();
        }
        #endregion

        #region SUM CHESS
        public bool ReCalcSumChessParams(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка суммарных показателей";
            userState.Element = this.Name;
            if (this.Wells.Count > 0)
            {
                if (!this.ChessLoaded) this.LoadChessFromCache(worker, e);
                int i, j;
                DateTime minDate, maxDate;
                Well w;
                ChessDailyItem item;
                ChessInjDailyItem itemInj;
                minDate = DateTime.MaxValue;
                maxDate = DateTime.MinValue;
                for (i = 0; i < Wells.Count; i++)
                {
                    w = (Well)Wells[i];
                    if (w.ChessLoaded)
                    {
                        for (j = 0; j < 2; j++)
                        {
                            if (j == 0) item = w.Chess[j];
                            else item = w.Chess[w.Chess.Count - 1];

                            if (item.Date < minDate) minDate = item.Date;
                            if ((item.Date > maxDate) && (item.Date <= DateTime.Now)) maxDate = item.Date;
                        }
                    }
                    if (w.ChessInjLoaded)
                    {
                        for (j = 0; j < 2; j++)
                        {
                            if (j == 0) itemInj = w.ChessInj[j];
                            else itemInj = w.ChessInj[w.ChessInj.Count - 1];

                            if (itemInj.Date < minDate) minDate = itemInj.Date;
                            if ((itemInj.Date > maxDate) && (itemInj.Date <= DateTime.Now)) maxDate = itemInj.Date;
                        }
                    }
                }

                if ((minDate != DateTime.MaxValue) && (maxDate != DateTime.MinValue))
                {
                    int days;
                    DateTime dt;
                    TimeSpan ts = maxDate - minDate;
                    days = ts.Days + 1;
                    int indDay;
                    double predQliq;
                    DateTime predDT;

                    if (days > 0)
                    {
                        SumParamItem[] SumDay = new SumParamItem[days];

                        dt = minDate;
                        for (i = 0; i < days; i++)
                        {
                            SumDay[i].Date = dt;
                            dt = dt.AddDays(1);
                        }
                        int iLen = Wells.Count;
                        for (i = 0; i < iLen; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return false;
                            }
                            else
                            {
                                w = (Well)Wells[i];
                                predDT = DateTime.Now;
                                predQliq = -1;
                                if (w.ChessLoaded)
                                {
                                    for (j = 0; j < w.Chess.Count; j++)
                                    {
                                        item = w.Chess[j];
                                        ts = item.Date - minDate;
                                        indDay = ts.Days;

                                        if ((indDay < SumDay.Length) && (SumDay[indDay].Date == item.Date))
                                        {
                                            if (((predQliq == -1) || (predDT != item.Date) || (predQliq != item.Qliq + item.Qoil)) && (item.Qliq + item.Qoil > 0))
                                            {
                                                if (item.StayTime < 24)
                                                {
                                                    SumDay[indDay].Liq += item.Qliq;
                                                    SumDay[indDay].Oil += item.Qoil;

                                                    SumDay[indDay].LiqV += item.QliqV;
                                                    SumDay[indDay].OilV += item.QoilV;

                                                    SumDay[indDay].Fprod++;
                                                }
                                                predDT = item.Date;
                                                predQliq = item.Qliq + item.Qoil;
                                            }
                                        }
                                        else if (indDay < SumDay.Length)
                                        {
                                            MessageBox.Show("Не совпадает дата!" + this.Name + "[" + w.Name + "-" + item.Date + "]", "Загрузка суммарных показателей");
                                        }
                                    }
                                }
                                if (w.ChessInjLoaded)
                                {
                                    indDay = 0;
                                    j = 0;
                                    int chessLen = w.ChessInj.Count - 1;
                                    itemInj = w.ChessInj[0];
                                    while (indDay < SumDay.Length)
                                    {
                                        if ((j < chessLen) && (SumDay[indDay].Date == w.ChessInj[j + 1].Date))
                                        {
                                            while ((j < chessLen) && (SumDay[indDay].Date == w.ChessInj[j + 1].Date))
                                            {
                                                j++;
                                                itemInj = w.ChessInj[j];
                                            }
                                            if ((itemInj.Wwat != -1) && (itemInj.StayTime != 24))
                                            {
                                                SumDay[indDay].Inj += itemInj.Wwat;
                                                SumDay[indDay].Finj++;
                                            }
                                        }
                                        else if (SumDay[indDay].Date >= itemInj.Date)
                                        {
                                            if ((itemInj.Wwat != -1) && (itemInj.StayTime != 24))
                                            {
                                                SumDay[indDay].Inj += itemInj.Wwat;
                                                SumDay[indDay].Finj++;
                                            }
                                        }
                                        indDay++;
                                    }
                                }
                                if(w.ChessLoaded) w.Chess.Clear();
                                w.Chess = null;
                                if (w.ChessInjLoaded) w.ChessInj.Clear();
                                w.ChessInj = null;
                                worker.ReportProgress((int)((float)i * 100 / (float)iLen), userState);
                            }
                        }
                        this.ChessLoaded = false;
                        this.SumChessParams = new SumParameters(1);
                        this.SumChessParams.Add(-1, SumDay, null);

                        retValue = true;
                    }
                }
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadSumChessParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка суммарных показателей Шахматки из кэша";
            userState.Element = this.Name;

            string cacheName = this.ProjectPath + "\\cache\\" + this.Name + "\\sum_chess.bin";
            if (File.Exists(cacheName))
            {
                BinaryReader bfr = null;
                try
                {
                    int i, j, lenMonth, len_unpack;

                    FileStream fs = new FileStream(cacheName, FileMode.Open); ;
                    bfr = new BinaryReader(fs);
                    if (SumParameters.Version == bfr.ReadInt32())
                    {
                        len_unpack = bfr.ReadInt32();

                        MemoryStream ms = new MemoryStream(len_unpack);
                        ConvertEx.CopyStream(fs, ms, len_unpack);
                        bfr.Close();
                        MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                        ms.Dispose();
                        BinaryReader br = new BinaryReader(unpackMS);

                        lenMonth = br.ReadInt32();
                        SumParamItem[] SumMonth = new SumParamItem[lenMonth];
                        for (i = 0; i < lenMonth; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                br.Close();
                                return false;
                            }
                            else
                            {
                                SumMonth[i].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                SumMonth[i].Liq = br.ReadDouble();
                                SumMonth[i].Oil = br.ReadDouble();
                                SumMonth[i].LiqV = br.ReadDouble();
                                SumMonth[i].OilV = br.ReadDouble();
                                SumMonth[i].Inj = br.ReadDouble();
                                SumMonth[i].Fprod = br.ReadInt32();
                                SumMonth[i].Finj = br.ReadInt32();
                            }
                        }
                        this.SumChessParams = new SumParameters(1);
                        this.SumChessParams.Add(-1, SumMonth, null);
                        retValue = true;
                    }
                }
                catch
                {
                    retValue = false;
                }
                if (bfr != null) bfr.Close();
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteSumChessParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись суммарных показателей Шахматки в кэш";
            userState.Element = this.Name;

            if (SumChessParams != null)
            {
                string cacheName = this.ProjectPath + "\\cache\\" + this.Name + "\\sum_chess.bin";

                int i, j, lenItem = 32, lenMonth;

                lenMonth = SumChessParams[0].MonthlyItems.Length;
                MemoryStream ms = new MemoryStream(8 + lenMonth * lenItem);
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(lenMonth);
                for (i = 0; i < lenMonth; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        bw.Close();
                        return false;
                    }
                    else
                    {
                        bw.Write(SmartPlusSystem.PackDateTime(SumChessParams[0].MonthlyItems[i].Date));
                        bw.Write(SumChessParams[0].MonthlyItems[i].Liq);
                        bw.Write(SumChessParams[0].MonthlyItems[i].Oil);
                        bw.Write(SumChessParams[0].MonthlyItems[i].LiqV);
                        bw.Write(SumChessParams[0].MonthlyItems[i].OilV);
                        bw.Write(SumChessParams[0].MonthlyItems[i].Inj);

                        bw.Write(SumChessParams[0].MonthlyItems[i].Fprod);
                        bw.Write(SumChessParams[0].MonthlyItems[i].Finj);

                    }
                }
                MemoryStream packedMS = ConvertEx.PackStream(ms);
                bw.Close();
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                bw = new BinaryWriter(fs);
                bw.Write(SumParameters.Version);
                bw.Write((int)packedMS.Length);
                packedMS.Seek(0, SeekOrigin.Begin);
                packedMS.WriteTo(fs);
                bw.Close();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public void ClearSumChessParams(bool useGC)
        {
            if (SumChessParams != null) SumChessParams.Clear();
            SumChessParams = null;
            if (useGC) GC.Collect();
        }
        #endregion

        #region SUM AREA
        public bool ReCalcSumAreaData(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка суммарных показателей";
            userState.Element = this.Name;
            var dictStratum = (StratumDictionary)DictList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                if (this.Areas.Count > 0)
                {
                    int i;
                    Area area;
                    if (!this.MerLoaded) this.LoadMerFromCache(worker, e, false);
                    if (this.MerLoaded)
                    {
                        for (i = 0; i < this.Areas.Count; i++)
                        {
                            area = (Area)Areas[i];
                            area.SumParamsReCalc(worker, e, dictStratum);
                        }
                    }
                    this.ClearMerData(false);
                    this.WriteSumAreaToCache(worker, e);
                    retValue = true;
                }
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadSumAreaFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка параметров ячеек заводнения..";
            userState.Element = this.Name;

            string cacheName = ProjectPath+ "\\cache\\" + this.Name + "\\areas_param.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, k, bl_ind, num_blocks, unpack_len_block, len_block, area_count, month_len, year_len, area_index = -1, iLen = 0, areaCount = 0;
                    Area area;
                    SumParamItem[] monthParams, yearParams;
                    MemoryStream ms;
                    worker.ReportProgress(0, userState);
                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    try
                    {
                        if (Area.SumVersion == bfr.ReadInt32())
                        {
                            num_blocks = bfr.ReadInt32();
                            iLen = bfr.ReadInt32();
                            this.minMerDate = DateTime.MaxValue;
                            this.maxMerDate = DateTime.MinValue;
                            for (bl_ind = 0; bl_ind < num_blocks; bl_ind++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return false;
                                }
                                len_block = bfr.ReadInt32();
                                unpack_len_block = bfr.ReadInt32();
                                area_count = bfr.ReadInt32();

                                ms = new MemoryStream(len_block);
                                if (ms == null) return false;
                                ConvertEx.CopyStream(fs, ms, len_block);
                                MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                                ms.Dispose();

                                BinaryReader br = new BinaryReader(unpackMS);
                                unpackMS.Seek(4 * area_count, SeekOrigin.Begin);
                                for (i = 0; i < area_count; i++)
                                {
                                    area_index = br.ReadInt32();
                                    if (worker.CancellationPending)
                                    {
                                        e.Cancel = true;
                                        this.ClearSumArea(false);
                                        br.Close();
                                        unpackMS.Dispose();
                                        return false;
                                    }
                                    if (area_index > -1)
                                    {
                                        area = (Area)Areas[area_index];
                                        month_len = br.ReadInt32();
                                        year_len = br.ReadInt32();
                                        if (month_len > 0)
                                        {
                                            monthParams = new SumParamItem[month_len];
                                            for (k = 0; k < month_len; k++)
                                            {
                                                monthParams[k].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                                monthParams[k].Liq = br.ReadDouble();
                                                monthParams[k].Oil = br.ReadDouble();
                                                monthParams[k].Watering = br.ReadDouble();
                                                monthParams[k].LiqV = br.ReadDouble();
                                                monthParams[k].OilV = br.ReadDouble();
                                                monthParams[k].WateringV = br.ReadDouble();
                                                monthParams[k].Inj = br.ReadDouble();
                                                monthParams[k].InjGas = br.ReadDouble();
                                                monthParams[k].NatGas = br.ReadDouble();
                                                monthParams[k].GasCondensate = br.ReadDouble();
                                                monthParams[k].Scoop = br.ReadDouble();
                                                monthParams[k].Fprod = br.ReadInt32();
                                                monthParams[k].Finj = br.ReadInt32();
                                                monthParams[k].Fscoop = br.ReadInt32();
                                                monthParams[k].LeadInAll = br.ReadInt32();
                                                monthParams[k].LeadInProd = br.ReadInt32();
                                                monthParams[k].WorkTimeProd = br.ReadDouble();
                                                monthParams[k].WorkTimeInj = br.ReadDouble();
                                            }
                                            yearParams = new SumParamItem[year_len];
                                            for (k = 0; k < year_len; k++)
                                            {
                                                yearParams[k].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                                yearParams[k].Liq = br.ReadDouble();
                                                yearParams[k].Oil = br.ReadDouble();
                                                yearParams[k].Watering = br.ReadDouble();
                                                yearParams[k].LiqV = br.ReadDouble();
                                                yearParams[k].OilV = br.ReadDouble();
                                                yearParams[k].WateringV = br.ReadDouble();
                                                yearParams[k].Inj = br.ReadDouble();
                                                yearParams[k].InjGas = br.ReadDouble();
                                                yearParams[k].NatGas = br.ReadDouble();
                                                yearParams[k].GasCondensate = br.ReadDouble();
                                                yearParams[k].Scoop = br.ReadDouble();
                                                yearParams[k].Fprod = br.ReadInt32();
                                                yearParams[k].Finj = br.ReadInt32();
                                                yearParams[k].Fscoop = br.ReadInt32();
                                                yearParams[k].LeadInAll = br.ReadInt32();
                                                yearParams[k].LeadInProd = br.ReadInt32();
                                                yearParams[k].WorkTimeProd = br.ReadDouble();
                                                yearParams[k].WorkTimeInj = br.ReadDouble();
                                            }
                                            area.sumObjParams = new SumObjParameters(area.StratumCode, monthParams, yearParams);
                                        }
                                        month_len = br.ReadInt32();
                                        if (month_len > 0)
                                        {
                                            monthParams = new SumParamItem[month_len];
                                            for (k = 0; k < month_len; k++)
                                            {
                                                monthParams[k].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                                monthParams[k].Liq = br.ReadDouble();
                                                monthParams[k].Oil = br.ReadDouble();
                                                monthParams[k].Watering = br.ReadDouble();
                                                monthParams[k].LiqV = br.ReadDouble();
                                                monthParams[k].OilV = br.ReadDouble();
                                                monthParams[k].WateringV = br.ReadDouble();
                                                monthParams[k].Inj = br.ReadDouble();
                                                monthParams[k].Scoop = br.ReadDouble();
                                                monthParams[k].Fprod = br.ReadInt32();
                                                monthParams[k].Finj = br.ReadInt32();
                                                monthParams[k].Fscoop = br.ReadInt32();
                                                monthParams[k].LeadInAll = br.ReadInt32();
                                                monthParams[k].LeadInProd = br.ReadInt32();
                                                monthParams[k].WorkTimeProd = br.ReadDouble();
                                                monthParams[k].WorkTimeInj = br.ReadDouble();
                                            }
                                            area.sumObjChessParams = new SumObjParameters(area.StratumCode, monthParams, null);
                                        }
                                    }
                                    retValue = true;
                                    ReportProgress(worker, areaCount + (i + 1), iLen, userState);
                                }
                                areaCount += area_count;
                                br.Close();
                                unpackMS.Dispose();
                            }
                        }
                    }
                    catch
                    {
                        retValue = false;
                    }
                    finally
                    {
                        bfr.Close();
                    }
                    if (iLen == 0) worker.ReportProgress(100, userState);
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша ЯЗ!";
                worker.ReportProgress(100, userState);
            }
            if (this.minMerDate == DateTime.MaxValue) this.minMerDate = DateTime.MinValue;
            if (this.maxMerDate == DateTime.MinValue) this.maxMerDate = DateTime.MaxValue;
            return retValue;
        }
        public bool LoadSumAreaFromCache(int AreaIndex)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\areas_param.bin";
            if (File.Exists(cacheName))
            {
                int i, k, num_blocks, unpack_len_block, len_block, area_count, area_index = -1, iLen, areaCount = 0;
                int monthLen, yearLen, chessLen;
                int shift;
                Area area;
                MemoryStream ms;
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader bfr = new BinaryReader(fs);
                SumParamItem[] monthParams, yearParams;
                try
                {
                    if (Area.SumVersion == bfr.ReadInt32())
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        areaCount = 0;
                        for (i = 0; i < num_blocks; i++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            area_count = bfr.ReadInt32();
                            if (AreaIndex < areaCount + area_count)
                            {
                                ms = new MemoryStream(unpack_len_block);
                                if (ms == null) return false;
                                ConvertEx.CopyStream(fs, ms, len_block);
                                MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                                ms.Dispose();
                                BinaryReader br = new BinaryReader(unpackMS);
                                shift = (AreaIndex - areaCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);

                                area_index = br.ReadInt32();
                                if (AreaIndex != area_index)
                                {
                                    bfr.Close();
                                    return false;
                                }
                                else if (area_index > -1)
                                {
                                    area = (Area)Areas[area_index];
                                    monthLen = br.ReadInt32();
                                    yearLen = br.ReadInt32();
                                    if (monthLen > 0)
                                    {
                                        monthParams = new SumParamItem[monthLen];
                                        yearParams = new SumParamItem[yearLen];
                                        for (k = 0; k < monthLen; k++)
                                        {
                                            monthParams[k].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                            monthParams[k].Liq = br.ReadDouble();
                                            monthParams[k].Oil = br.ReadDouble();
                                            monthParams[k].Watering = br.ReadDouble();
                                            monthParams[k].LiqV = br.ReadDouble();
                                            monthParams[k].OilV = br.ReadDouble();
                                            monthParams[k].WateringV = br.ReadDouble();
                                            monthParams[k].Inj = br.ReadDouble();
                                            monthParams[k].InjGas = br.ReadDouble();
                                            monthParams[k].NatGas = br.ReadDouble();
                                            monthParams[k].GasCondensate = br.ReadDouble();
                                            monthParams[k].Scoop = br.ReadDouble();
                                            monthParams[k].Fprod = br.ReadInt32();
                                            monthParams[k].Finj = br.ReadInt32();
                                            monthParams[k].Fscoop = br.ReadInt32();
                                            monthParams[k].LeadInAll = br.ReadInt32();
                                            monthParams[k].LeadInProd = br.ReadInt32();
                                            monthParams[k].WorkTimeProd = br.ReadDouble();
                                            monthParams[k].WorkTimeInj = br.ReadDouble();
                                            if (monthParams[k].LiqV > 0)
                                            {
                                                monthParams[k].LiqVb = monthParams[k].OilV * area.pvt.OilVolumeFactor + (monthParams[k].LiqV - monthParams[k].OilV) * area.pvt.WaterVolumeFactor;
                                                monthParams[k].AccumLiqVb = monthParams[k].LiqVb;
                                                if (k > 0) monthParams[k].AccumLiqVb += monthParams[k - 1].AccumLiqVb;
                                            }
                                            monthParams[k].InjVb = monthParams[k].Inj * area.pvt.WaterVolumeFactor;
                                            monthParams[k].AccumInjVb = monthParams[k].InjVb;
                                            if (k > 0) monthParams[k].AccumInjVb += monthParams[k - 1].AccumInjVb;
                                        }
                                        for (k = 0; k < yearLen; k++)
                                        {
                                            yearParams[k].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                            yearParams[k].Liq = br.ReadDouble();
                                            yearParams[k].Oil = br.ReadDouble();
                                            yearParams[k].Watering = br.ReadDouble();
                                            yearParams[k].LiqV = br.ReadDouble();
                                            yearParams[k].OilV = br.ReadDouble();
                                            yearParams[k].WateringV = br.ReadDouble();
                                            yearParams[k].Inj = br.ReadDouble();
                                            yearParams[k].InjGas = br.ReadDouble();
                                            yearParams[k].NatGas = br.ReadDouble();
                                            yearParams[k].GasCondensate = br.ReadDouble();
                                            yearParams[k].Scoop = br.ReadDouble();
                                            yearParams[k].Fprod = br.ReadInt32();
                                            yearParams[k].Finj = br.ReadInt32();
                                            yearParams[k].Fscoop = br.ReadInt32();
                                            yearParams[k].LeadInAll = br.ReadInt32();
                                            yearParams[k].LeadInProd = br.ReadInt32();
                                            yearParams[k].WorkTimeProd = br.ReadDouble();
                                            yearParams[k].WorkTimeInj = br.ReadDouble();
                                            if (yearParams[k].LiqV > 0)
                                            {
                                                yearParams[k].LiqVb = yearParams[k].OilV * area.pvt.OilVolumeFactor + (yearParams[k].LiqV - yearParams[k].OilV) * area.pvt.WaterVolumeFactor;
                                                yearParams[k].AccumLiqVb = yearParams[k].LiqVb;
                                                if (k > 0) yearParams[k].AccumLiqVb += yearParams[k - 1].AccumLiqVb;
                                            }
                                            yearParams[k].InjVb = yearParams[k].Inj * area.pvt.WaterVolumeFactor;
                                            yearParams[k].AccumInjVb = yearParams[k].InjVb;
                                            if (k > 0) yearParams[k].AccumInjVb += yearParams[k - 1].AccumInjVb;
                                        }
                                        area.sumObjParams = new SumObjParameters(area.StratumCode, monthParams, yearParams);
                                    }
                                    chessLen = br.ReadInt32();
                                    if (chessLen > 0)
                                    {
                                        monthParams = new SumParamItem[chessLen];
                                        for (k = 0; k < chessLen; k++)
                                        {
                                            monthParams[k].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                            monthParams[k].Liq = br.ReadDouble();
                                            monthParams[k].Oil = br.ReadDouble();
                                            monthParams[k].Watering = br.ReadDouble();
                                            monthParams[k].LiqV = br.ReadDouble();
                                            monthParams[k].OilV = br.ReadDouble();
                                            monthParams[k].WateringV = br.ReadDouble();
                                            monthParams[k].Inj = br.ReadDouble();
                                            monthParams[k].Scoop = br.ReadDouble();
                                            monthParams[k].Fprod = br.ReadInt32();
                                            monthParams[k].Finj = br.ReadInt32();
                                            monthParams[k].Fscoop = br.ReadInt32();
                                            monthParams[k].LeadInAll = br.ReadInt32();
                                            monthParams[k].LeadInProd = br.ReadInt32();
                                            monthParams[k].WorkTimeProd = br.ReadDouble();
                                            monthParams[k].WorkTimeInj = br.ReadDouble();
                                        }
                                        area.sumObjChessParams = new SumObjParameters(area.StratumCode, monthParams, null);
                                    }
                                }
                                retValue = true;
                                br.Close();
                                break;
                            }
                            else
                            {
                                fs.Seek(len_block, SeekOrigin.Current);
                            }
                            areaCount += area_count;
                        }
                    }
                }
                catch
                {
                    retValue = false;
                }
                finally
                {
                    bfr.Close();
                }
            }
            return retValue;
        }
        public bool WriteSumAreaToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись параметров ячеек заводнения в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\areas_param.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 1024;
                Area area;
                SumParamItem sumItem;
                List<MemoryStream> msArr = new List<MemoryStream>();
                List<int> areaCount = new List<int>(5);
                List<int> unpackSizes = new List<int>(5);
                List<int> AreasShift = new List<int>();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, n, lenItemMer = 128, lenItemChess = 80, len_all_areas, aCount = 0, len_block;
                int len_shift;

                if (Areas.Count > 0)
                {
                    len_all_areas = 0;
                    for (i = 0; i < Areas.Count; i++)
                    {
                        area = (Area)Areas[i];
                        len_all_areas += 16; // Index + CountMer * 2 + CountChess
                        if (area.sumObjParams != null)
                        {
                            if (area.sumObjParams.MonthlyItems != null)
                            {
                                len_all_areas += area.sumObjParams.MonthlyItems.Length * lenItemMer;
                            }
                            if (area.sumObjParams.YearlyItems != null)
                            {
                                len_all_areas += area.sumObjParams.YearlyItems.Length * lenItemMer;
                            }
                        }
                        if ((area.sumObjChessParams != null) && (area.sumObjChessParams.MonthlyItems != null))
                        {
                            len_all_areas += area.sumObjChessParams.MonthlyItems.Length * lenItemChess;
                        }
                    }
                    if (len_all_areas > MAX_SIZE_PACK_BLOCK) len_all_areas = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_areas);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);

                    len_block = 0;
                    k = 0;
                    aCount = 0;

                    for (i = 0; i <= Areas.Count; i++)
                    {
                        len_shift = 0;
                        if (i < Areas.Count)
                        {
                            area = (Area)Areas[i];

                            len_shift = 16; // Index + CountMer * 2 + CountChess
                            if (area.sumObjParams != null)
                            {
                                if (area.sumObjParams.MonthlyItems != null)
                                {
                                    len_shift += area.sumObjParams.MonthlyItems.Length * lenItemMer;
                                }
                                if (area.sumObjParams.YearlyItems != null)
                                {
                                    len_shift += area.sumObjParams.YearlyItems.Length * lenItemMer;
                                }
                            }
                            if ((area.sumObjChessParams != null) && (area.sumObjChessParams.MonthlyItems != null))
                            {
                                len_shift += area.sumObjChessParams.MonthlyItems.Length * lenItemChess;
                            }
                        }
                        if ((len_block + len_shift <= len_all_areas) && (i < Areas.Count))
                        {
                            aCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * aCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                area = (Area)Areas[j];
                                len_shift += 16;// Index + CountMer * 2 + CountChess
                                if (area.sumObjParams != null)
                                {
                                    if (area.sumObjParams.MonthlyItems != null)
                                    {
                                        len_shift += area.sumObjParams.MonthlyItems.Length * lenItemMer;
                                    }
                                    if (area.sumObjParams.YearlyItems != null)
                                    {
                                        len_shift += area.sumObjParams.YearlyItems.Length * lenItemMer;
                                    }
                                }
                                if ((area.sumObjChessParams != null) && (area.sumObjChessParams.MonthlyItems != null))
                                {
                                    len_shift += area.sumObjChessParams.MonthlyItems.Length * lenItemChess;
                                }
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                area = (Area)Areas[j];
                                bw.Write(area.Index);
                                if ((area.sumObjParams != null) && (area.sumObjParams.MonthlyItems != null))
                                {
                                    bw.Write(area.sumObjParams.MonthlyItems.Length);
                                    bw.Write(area.sumObjParams.YearlyItems.Length);
                                    for (n = 0; n < area.sumObjParams.MonthlyItems.Length; n++)
                                    {
                                        sumItem = area.sumObjParams.MonthlyItems[n];

                                        bw.Write(SmartPlusSystem.PackDateTime(sumItem.Date));
                                        bw.Write(sumItem.Liq);
                                        bw.Write(sumItem.Oil);
                                        bw.Write(sumItem.Watering);
                                        bw.Write(sumItem.LiqV);
                                        bw.Write(sumItem.OilV);
                                        bw.Write(sumItem.WateringV);
                                        bw.Write(sumItem.Inj);
                                        bw.Write(sumItem.InjGas);
                                        bw.Write(sumItem.NatGas);
                                        bw.Write(sumItem.GasCondensate);
                                        bw.Write(sumItem.Scoop);
                                        bw.Write(sumItem.Fprod);
                                        bw.Write(sumItem.Finj);
                                        bw.Write(sumItem.Fscoop);
                                        bw.Write(sumItem.LeadInAll);
                                        bw.Write(sumItem.LeadInProd);
                                        bw.Write(sumItem.WorkTimeProd);
                                        bw.Write(sumItem.WorkTimeInj);
                                    }
                                    for (n = 0; n < area.sumObjParams.YearlyItems.Length; n++)
                                    {
                                        sumItem = area.sumObjParams.YearlyItems[n];

                                        bw.Write(SmartPlusSystem.PackDateTime(sumItem.Date));
                                        bw.Write(sumItem.Liq);
                                        bw.Write(sumItem.Oil);
                                        bw.Write(sumItem.Watering);
                                        bw.Write(sumItem.LiqV);
                                        bw.Write(sumItem.OilV);
                                        bw.Write(sumItem.WateringV);
                                        bw.Write(sumItem.Inj);
                                        bw.Write(sumItem.InjGas);
                                        bw.Write(sumItem.NatGas);
                                        bw.Write(sumItem.GasCondensate);
                                        bw.Write(sumItem.Scoop);
                                        bw.Write(sumItem.Fprod);
                                        bw.Write(sumItem.Finj);
                                        bw.Write(sumItem.Fscoop);
                                        bw.Write(sumItem.LeadInAll);
                                        bw.Write(sumItem.LeadInProd);
                                        bw.Write(sumItem.WorkTimeProd);
                                        bw.Write(sumItem.WorkTimeInj);
                                    }
                                }
                                else
                                {
                                    bw.Write(0);
                                    bw.Write(0);
                                }
                                if (area.sumObjChessParams != null && area.sumObjChessParams.MonthlyItems != null)
                                {
                                    bw.Write(area.sumObjChessParams.MonthlyItems.Length);
                                    for (n = 0; n < area.sumObjChessParams.MonthlyItems.Length; n++)
                                    {
                                        sumItem = area.sumObjChessParams.MonthlyItems[n];

                                        bw.Write(SmartPlusSystem.PackDateTime(sumItem.Date));
                                        bw.Write(sumItem.Liq);
                                        bw.Write(sumItem.Oil);
                                        bw.Write(sumItem.Watering);
                                        bw.Write(sumItem.LiqV);
                                        bw.Write(sumItem.OilV);
                                        bw.Write(sumItem.WateringV);
                                        bw.Write(sumItem.Inj);
                                        bw.Write(sumItem.Scoop);
                                        bw.Write(sumItem.Fprod);
                                        bw.Write(sumItem.Finj);
                                        bw.Write(sumItem.Fscoop);
                                        bw.Write(sumItem.LeadInAll);
                                        bw.Write(sumItem.LeadInProd);
                                        bw.Write(sumItem.WorkTimeProd);
                                        bw.Write(sumItem.WorkTimeInj);
                                    }
                                }
                                else
                                {
                                    bw.Write(0);
                                }
                            }
                            unpackSizes.Add((int)ms.Length);
                            //packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                            packedMS = ConvertEx.PackStream(ms);
                            msArr.Add(packedMS);
                            areaCount.Add(aCount);
                            AreasShift.Clear();
                            if (i < Areas.Count)
                            {
                                ms.SetLength(0);
                                k = i;

                                area = (Area)Areas[i];

                                len_shift = 16;// Index + CountMer * 2 + CountChess
                                if (area.sumObjParams != null)
                                {
                                    if (area.sumObjParams.MonthlyItems != null)
                                    {
                                        len_shift += area.sumObjParams.MonthlyItems.Length * lenItemMer;
                                    }
                                    if (area.sumObjParams.YearlyItems != null)
                                    {
                                        len_shift += area.sumObjParams.YearlyItems.Length * lenItemMer;
                                    }
                                }
                                if ((area.sumObjChessParams != null) && (area.sumObjChessParams.MonthlyItems != null))
                                {
                                    len_shift += area.sumObjChessParams.MonthlyItems.Length * lenItemChess;
                                }
                                len_block = len_shift;
                                aCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, Areas.Count);
                    }
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(Area.SumVersion); // Version
                    bw.Write(msArr.Count);
                    bw.Write(Areas.Count);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)areaCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    areaCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                    GC.Collect();
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearSumArea(bool useGC)
        {
            int i, j, len = Areas.Count;
            Area area;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    area = (Area)Areas[i];
                    if (area.sumObjParams != null)
                    {
                        area.sumObjParams.MonthlyItems = null;
                        area.sumObjParams.YearlyItems = null;
                        area.sumObjParams = null;
                    }

                    if (area.sumObjChessParams != null)
                    {
                        area.sumObjChessParams.MonthlyItems = null;
                        area.sumObjChessParams = null;
                    }
                }
                if (useGC) GC.Collect();
            }
        }
        #endregion

        #region TABLE 8.1
        public bool LoadTable81ParamsFromFile(BackgroundWorker worker, DoWorkEventArgs e, StreamReader file, int StartFileYear)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей Таблицы 8.1";
            userState.Element = Name;

            if ((file != null) && (!file.EndOfStream))
            {
                int j;
                string oilFieldName, projDocName = "";
                int startCol, endCol, startYear, endYear, newPtdYear = 0;
                bool res;
                DateTime dtMin, dtMax;
                string[] parseStr;
                string str;
                SumParamItem[] Params = null;
                char[] delimeter = new char[] { ';' };

                str = file.ReadLine(); // мест. - Доб нефти
                if ((str != null) && (str != ""))
                {
                    parseStr = str.Split(delimeter);
                    if (parseStr.Length > 0)
                    {
                        if (parseStr[0] == "") newPtdYear = 0;
                        else newPtdYear = Convert.ToInt32(parseStr[0]);

                        oilFieldName = parseStr[1];
                        projDocName = parseStr[2];

                        j = 4; res = false;
                        while (j < parseStr.Length)
                        {
                            if (parseStr[j] != "")
                            {
                                res = true;
                                break;
                            }
                            j++;
                        }

                        if (res)
                        {
                            startCol = j;
                            startYear = StartFileYear + startCol - 4;
                            dtMin = new DateTime(startYear, 01, 01);

                            res = false;
                            while (j < parseStr.Length)
                            {
                                if (parseStr[j] == "")
                                {
                                    res = true;
                                    break;
                                }
                                j++;
                            }
                            if ((res || (j == parseStr.Length)) && (parseStr[j - 1] != ""))
                            {
                                endCol = j;
                                endYear = StartFileYear + endCol - 4;
                                dtMax = new DateTime(endYear, 01, 01);

                                Params = new SumParamItem[endCol - startCol];

                                for (j = 0; j < endCol - startCol; j++)
                                {
                                    Params[j].Date = new DateTime(startYear + j, 01, 01);
                                    Params[j].Oil = Convert.ToDouble(parseStr[j + startCol]) * 1000;
                                }

                                str = file.ReadLine();                // Проект.док. - Ввод новых доб.скв.
                                parseStr = str.Split(delimeter);

                                for (j = 0; j < endCol - startCol; j++)
                                {
                                    if (parseStr[j + startCol] == "") Params[j].LeadInAll = 0;
                                    else Params[j].LeadInAll = Convert.ToInt32(parseStr[j + startCol]);
                                }

                                str = file.ReadLine();                // - в т.ч из экспл.бур.
                                parseStr = str.Split(delimeter);

                                for (j = 0; j < endCol - startCol; j++)
                                {
                                    if (parseStr[j + startCol] == "") Params[j].LeadInProd = 0;
                                    else Params[j].LeadInProd = Convert.ToInt32(parseStr[j + startCol]);
                                }

                                str = file.ReadLine();                // - Действ. фонд доб.скв.
                                parseStr = str.Split(delimeter);
                                for (j = 0; j < endCol - startCol; j++)
                                {
                                    if (parseStr[j + startCol] == "") Params[j].Fprod = 0;
                                    else Params[j].Fprod = Convert.ToInt32(parseStr[j + startCol]);
                                }


                                str = file.ReadLine();                // - Действ. фонд нагн.скв.
                                parseStr = str.Split(delimeter);
                                for (j = 0; j < endCol - startCol; j++)
                                {
                                    if (parseStr[j + startCol] == "") Params[j].Finj = 0;
                                    else Params[j].Finj = Convert.ToInt32(parseStr[j + startCol]);
                                }

                                str = file.ReadLine();                // - Добыча жидк.
                                parseStr = str.Split(delimeter);
                                for (j = 0; j < endCol - startCol; j++)
                                {
                                    Params[j].Liq = Convert.ToDouble(parseStr[j + startCol]) * 1000;

                                    if (Params[j].Liq > 0)
                                        Params[j].Watering = (Params[j].Liq - Params[j].Oil) * 100 / Params[j].Liq;
                                    else
                                        Params[j].Watering = 0;
                                }


                                str = file.ReadLine();                // - Закачка агента.
                                parseStr = str.Split(delimeter);
                                for (j = 0; j < endCol - startCol; j++)
                                {
                                    if (parseStr[j + startCol] != "")
                                        Params[j].Inj = Convert.ToDouble(parseStr[j + startCol]) * 1000;
                                    else
                                        Params[j].Inj = 0;
                                }

                                str = file.ReadLine();                // - Добыча газа
                                
                                parseStr = str.Split(delimeter);
                                for (j = 0; j < endCol - startCol; j++)
                                {
                                    if (parseStr[j + startCol] != "")
                                        Params[j].NatGas = Convert.ToDouble(parseStr[j + startCol]) * 1000000;
                                    else
                                        Params[j].NatGas = 0;
                                }
                                str = file.ReadLine();               // - Закачка газа.
                                if (str != null)
                                {
                                    parseStr = str.Split(delimeter);
                                    if (parseStr[3].Length > 0)
                                    {
                                        for (j = 0; j < endCol - startCol; j++)
                                        {
                                            if (parseStr[j + startCol] != "")
                                                Params[j].InjGas = Convert.ToDouble(parseStr[j + startCol]) * 1000000;
                                            else
                                                Params[j].InjGas = 0;
                                        }
                                        file.ReadLine();
                                    }
                                }
                            }
                        }
                    }
                    if (Params != null)
                    {
                        if (Table81 == null) Table81 = new Table81Params();
                        Table81.SetTable(Params);
                        Table81.NewPtdYear = newPtdYear;
                        Table81.ProjectDocName = projDocName;
                        WriteTable81ParamsToCache(worker, e);
                        retValue = true;
                    }
                }
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool LoadTable81ParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка показателей Таблиц 8.1 из кэша";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\table81.bin";

            if (File.Exists(cacheName))
            {
                int i, lenYears, len_unpack;

                FileStream fs = new FileStream(cacheName, FileMode.Open); ;
                BinaryReader bfr = new BinaryReader(fs);
                if (Table81Params.Version == bfr.ReadInt32())
                {
                    len_unpack = bfr.ReadInt32();

                    MemoryStream ms = new MemoryStream(len_unpack);
                    ConvertEx.CopyStream(fs, ms, len_unpack);
                    MemoryStream unpackMS = ConvertEx.UnPackStream<MemoryStream>(ms);
                    ms.Dispose();
                    BinaryReader br = new BinaryReader(unpackMS);

                    Table81.ProjectDocName = br.ReadString();
                    Table81.NewPtdYear = br.ReadInt32();
                    lenYears = br.ReadInt32();

                    SumParamItem[] SumYear = new SumParamItem[lenYears];
                    for (i = 0; i < lenYears; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            br.Close();
                            return false;
                        }
                        else
                        {
                            SumYear[i].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                            SumYear[i].Liq = br.ReadDouble();
                            SumYear[i].Oil = br.ReadDouble();
                            SumYear[i].Watering = br.ReadDouble();
                            SumYear[i].Inj = br.ReadDouble();
                            SumYear[i].Fprod = br.ReadInt32();
                            SumYear[i].Finj = br.ReadInt32();
                            SumYear[i].LeadInAll = br.ReadInt32();
                            SumYear[i].LeadInProd = br.ReadInt32();
                            SumYear[i].NatGas = br.ReadDouble();
                            SumYear[i].InjGas = br.ReadDouble();
                        }
                    }
                    Table81.SetTable(SumYear);
                    br.Close();
                }
                bfr.Close();
                retValue = true;
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteTable81ParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись показателей Таблиц 8.1 в кэш";
            userState.Element = Name;

            if (Table81 != null)
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\table81.bin";
                int i, lenItem = 48, lenYears;

                lenYears = Table81.Count;
                MemoryStream ms = new MemoryStream(8 + (lenYears + 0) * lenItem);
                BinaryWriter bw = new BinaryWriter(ms);
                bw.Write(Table81.ProjectDocName);
                bw.Write(Table81.NewPtdYear);
                bw.Write(lenYears);
                for (i = 0; i < lenYears; i++)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        bw.Close();
                        return false;
                    }
                    else
                    {
                        bw.Write(SmartPlusSystem.PackDateTime(Table81.Items[i].Date));
                        bw.Write(Table81.Items[i].Liq);
                        bw.Write(Table81.Items[i].Oil);
                        bw.Write(Table81.Items[i].Watering);
                        bw.Write(Table81.Items[i].Inj);
                        bw.Write(Table81.Items[i].Fprod);
                        bw.Write(Table81.Items[i].Finj);
                        bw.Write(Table81.Items[i].LeadInAll);
                        bw.Write(Table81.Items[i].LeadInProd);
                        bw.Write(Table81.Items[i].NatGas);
                        bw.Write(Table81.Items[i].InjGas);
                    }
                    worker.ReportProgress(i, userState);
                }
                bw.Write(0);
                //MemoryStream packedMS = ConvertEx.PackStream<MemoryStream>(ms);
                MemoryStream packedMS = ConvertEx.PackStream(ms);
                bw.Close();
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                bw = new BinaryWriter(fs);
                bw.Write(Table81Params.Version);
                bw.Write((int)packedMS.Length);
                packedMS.Seek(0, SeekOrigin.Begin);
                packedMS.WriteTo(fs);
                bw.Close();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            userState.Element = Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }
        #endregion

        #region PVT
        public bool LoadPVTParamsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Чтение данных PVT с кэша";
            userState.Element = Name;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\PVTParams.bin";

            if (File.Exists(cacheName))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs, Encoding.GetEncoding(1251));
                int version = br.ReadInt32();
                if (PVTParams.Version != version)
                {
                    PVT = new PVTParams();
                    PVT.ReadFromCache(br, version);
                    retValue = true;
                }
                br.Close();
            }
            return retValue;
        }
        public bool WritePVTParamsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных PVT в кэш";
            userState.Element = Name;
            if (PVTLoaded)
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\PVTParams.bin";
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);

                bw.Write(PVTParams.Version);
                retValue = PVT.WriteToCache(bw);
                bw.Close();
            }
            return retValue;
        }
        #endregion

        // WELLS DATA READ / WRITE

        #region WELL LIST
        
        public bool LoadWellsFromOracleServer(BackgroundWorker worker, DoWorkEventArgs e, string Query)
        {
            bool retValue = false;
            // загрузка с оракла
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка списка скважин с сервера";
            userState.Element = Name;
            worker.ReportProgress(0, userState);

            if (Query.IndexOf("%OILFIELDCODE%") == -1)
            {
                userState.Error = "Неверный запрос к базе данных. Нет объекта %OILFIELDCODE%.";
                worker.ReportProgress(0, userState);
                return false;
            }
            Query = Query.Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());

            string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                        ParamsDict.OraServerName + ")(PORT=" + ParamsDict.OraPort.ToString() +
                        ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + ParamsDict.OraSid + ")));" +
                        "USER ID=" + ParamsDict.OraLogin + ";PASSWORD= " + ParamsDict.OraPass + ";";

            using (OracleConnection conn = new OracleConnection(connString))
            {
                OracleCommand oraCommand = conn.CreateCommand();
                oraCommand.CommandText = Query;
                OracleDataReader oraReader;
                userState.WorkCurrentTitle = "Выполняется запрос к серверу Oracle...";
                worker.ReportProgress(0, userState);
                try
                {
                    int i = 0, progress = 0;
                    userState.WorkCurrentTitle = "Чтение данных с сервера Oracle...";

                    #region Чтение списка скважин
                    conn.Open();
                    oraReader = oraCommand.ExecuteReader();

                    bool res = oraReader.Read(), res2;
                    worker.ReportProgress(0, userState);

                    // записываем в память данные c сервера
                    List<int> WellBoreId = new List<int>();
                    List<DateTime> WellBoreStartDate = new List<DateTime>();

                    if (Wells == null) Wells = new List<Well>();

                    int index, wellId, boreId;
                    int areaCode;
                    string wellName;
                    DateTime dt;
                    Well w, sideBore;
                    while (res)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            Wells.Clear();
                            retValue = false;
                            break;
                        }
                        else
                        {
                            wellId = Convert.ToInt32(oraReader[0]);
                            areaCode = Convert.ToInt32(oraReader[1]);
                            wellName = Convert.ToString(oraReader[2]).Trim().ToUpper();
                            boreId = Convert.ToInt32(oraReader[3]);
                            index = GetWellIndex(boreId);

                            if (index == -1)
                            {
                                w = new Well(this.Wells.Count);
                                w.MainBoreIndex = w.Index;
                                w.Name = wellName;
                                w.OilFieldIndex = this.Index;
                                w.WellId = wellId;
                                w.OilFieldAreaCode = areaCode;
                                w.StartDate = Convert.ToDateTime(oraReader[4]);
                                w.BoreId = boreId;
                                this.Wells.Add(w);
                            }
                            else
                            {
                                w = Wells[index];
                            }

                            WellBoreId.Clear();
                            WellBoreStartDate.Clear();

                            res2 = oraReader.Read();
                            while (res2)
                            {
                                if (Convert.ToInt32(oraReader[0]) != wellId) break;
                                dt = Convert.ToDateTime(oraReader[4]);
                                if (dt <= DateTime.Now.AddDays(1))
                                {
                                    WellBoreId.Add(Convert.ToInt32(oraReader[3]));
                                    WellBoreStartDate.Add(dt);
                                }
                                res2 = oraReader.Read();
                            }
                            res = res2;
                            if (WellBoreId.Count > 0)
                            {
                                w.SideBores = new Well[WellBoreId.Count];
                                for (i = 0; i < WellBoreId.Count; i++)
                                {
                                    index = GetWellIndex(WellBoreId[i]);
                                    if (index == -1)
                                    {
                                        sideBore = new Well(Wells.Count);
                                        sideBore.Name = string.Empty;
                                        sideBore.MainBoreIndex = w.Index;
                                        sideBore.WellId = w.WellId;
                                        sideBore.BoreId = WellBoreId[i];
                                        sideBore.StartDate = WellBoreStartDate[i];
                                        w.SideBores[i] = sideBore;
                                        Wells.Add(sideBore);
                                    }
                                    else
                                    {
                                        w.SideBores[i] = Wells[index];
                                    }
                                }
                            }
                            retValue = true;
                            i++;
                            progress++;
                            if (progress == 100) progress = 0;
                            ReportProgress(worker, progress, 100);
                        }
                    }
                    #endregion
                    oraReader.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Ошибка загрузки с сервера Oracle!", ex);
                }
                userState.WorkCurrentTitle = "Данные с сервера Oracle получены";
                worker.ReportProgress(0, userState);
            }
            return retValue;
        }
        public bool LoadWellsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadItemWellsFromCache(worker, e, TYPE_SKV.SKV,ref Wells);
        }
        public bool LoadItemWellsFromCache(BackgroundWorker worker, DoWorkEventArgs e, TYPE_SKV type,ref List<Well> itemWells)
        {
            CoordLoaded = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка координат скважин с кэша";
            userState.Element = Name;
            string cacheName;
            if (type == TYPE_SKV.SKV)
            {
                cacheName = ProjectPath + "\\cache\\" + Name + "\\wellList.bin";
            }
            else 
            {
                cacheName = ProjectPath + "\\cache\\" + Name + "\\projectWellList.bin";
            }
            try
            {
                if (!File.Exists(cacheName))
                {
                    userState.Error = "Не найден файл списка скважин";
                    worker.ReportProgress(100, userState);
                    return false;
                }
                Well w;
                FileStream fs = new FileStream(cacheName, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);

                worker.ReportProgress(0, userState);

                if (br.ReadInt32() == Well.Version)
                {
                    int count = br.ReadInt32();

                    if (itemWells == null) itemWells = new List<Well>(count);
                    for (int i = 0; i < count; i++)
                    {
                        w = new Well(i);
                        w.ReadFromCache(br);
                        if (i != w.Index)
                        {
                            br.Close();
                            throw new Exception(string.Format("Ошибка индексации скважины {0} [{1}]\n", w.Name, Name));
                        }
                        if (w.Name.Length > 0)
                            itemWells.Add(w);
                    }
                    // Заполнение SideBores
                    List<int>[] boreIndexes = new List<int>[count];
                    for (int i = 0; i < count; i++)
                    {
                        w = itemWells[i];
                        if (w.Index != w.MainBoreIndex)
                        {
                            if (boreIndexes[w.MainBoreIndex] == null) boreIndexes[w.MainBoreIndex] = new List<int>();
                            boreIndexes[w.MainBoreIndex].Add(w.Index);
                        }
                    }
                    for (int i = 0; i < count; i++)
                    {
                        if (boreIndexes[i] != null)
                        {
                            itemWells[i].SideBores = new Well[boreIndexes[i].Count];
                            for (int j = 0; j < boreIndexes[i].Count; j++)
                            {
                                itemWells[i].SideBores[j] = itemWells[boreIndexes[i][j]];
                            }
                        }
                    }
                    CoordLoaded = true;
                }
                br.Close();
            }
            catch
            {
                CoordLoaded = false;
            }
            worker.ReportProgress(100, userState);
            return CoordLoaded;
        }
        public bool WriteWellsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return WriteItemWellsToCache(worker, e, TYPE_SKV.SKV,ref Wells);
        }
        public bool WriteItemWellsToCache(BackgroundWorker worker, DoWorkEventArgs e, TYPE_SKV type,ref List<Well> itemWells)
        {
            bool retValue = false;
            string cacheName;
            if (type == TYPE_SKV.SKV)
            {
                cacheName = ProjectPath + "\\cache\\" + Name + "\\wellList.bin";
            }
            else
            {
                cacheName = ProjectPath + "\\cache\\" + Name + "\\projectWellList.bin";
            }
            if (itemWells != null)
            {
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);

                WorkerState userState = new WorkerState();
                userState.WorkMainTitle = string.Empty;
                userState.WorkCurrentTitle = "Запись списка скважин в кэш";
                userState.Element = Name;
                using (bw)
                {
                    worker.ReportProgress(0, userState);
                    bw.Write(Well.Version);
                    bw.Write(itemWells.Count);
                    for (int i = 0; i < itemWells.Count; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }
                        else
                        {
                            itemWells[i].WriteToCache(bw);
                            retValue = true;
                        }
                        ReportProgress(worker, i, itemWells.Count);
                    }
                    if (itemWells.Count == 0) retValue = false;
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            return retValue;
        }
        #endregion

        #region WELLBORE COORDINATES
  
        public void ReSetWellIcons(BackgroundWorker worker, DoWorkEventArgs e, bool IsClearMerData)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Установка значков скважин по МЭР";
            userState.Element = ParamsDict.OilFieldName;
            worker.ReportProgress(0, userState);

            if (!MerLoaded) LoadMerFromCache(worker, e, false);
            if (MerLoaded)
            {
                for (int i = 0; i < Wells.Count; i++)
                {
                    if (Wells[i].MerLoaded)
                    {
                        Wells[i].ReSetIcons(CoefDict, maxMerDate);
                    }
                }
            }
            if ((IsClearMerData) && (MerLoaded))
            {
                ClearMerData(false);
            }
        }
        public bool LoadCoordFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка координат скважин с кэша";
            userState.Element = Name;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\coord.bin";
            try
            {
                if (File.Exists(cacheName))
                {
                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    try
                    {
                        worker.ReportProgress(0, userState);

                        if (SkvCoord.VERSION != br.ReadInt32())
                        {
                            br.Close();
                            CoordLoaded = false;
                            return false;
                        }

                        int count = br.ReadInt32();
                        if (count != Wells.Count)
                        {
                            br.Close();
                            CoordLoaded = false;
                            return false;
                        }
                        int wellIndex;
                        for (int i = 0; i < count; i++)
                        {
                            wellIndex = br.ReadInt32();
                            if (wellIndex != i)
                            {
                                br.Close();
                                CoordLoaded = false;
                                return false;
                            }
                            Wells[i].Coord = new SkvCoord();
                            Wells[i].Coord.Read(br);
                            Wells[i].SetStartedCoord();
                            Wells[i].Icons.Read(br);
                            Wells[i].SetActiveAllIcon();
                        }
                        CoordLoadedfromCache = true;
                        retValue = true;
                    }
                    finally
                    {
                        br.Close();
                    }
                }
            }
            catch
            {
                CoordLoaded = false;
                retValue = false;
            }
            worker.ReportProgress(100, userState);
            return retValue;
        }
        public bool WriteCoordToCache(BackgroundWorker worker, DoWorkEventArgs e, bool loadFromServer)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\coord.bin";

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Запись координат скважин в кэш";
            userState.Element = Name;
            if ((CoordLoaded && loadFromServer) || (!loadFromServer && (CoordLoadedfromCache || CoordLoaded)))
            {
                FileStream fs = new FileStream(cacheName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs, Encoding.UTF8);

                using (bw)
                {
                    worker.ReportProgress(0, userState);
                    bw.Write(SkvCoord.VERSION);
                    bw.Write(Wells.Count);
                    for (int i = 0; i < Wells.Count; i++)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            retValue = false;
                            break;
                        }
                        bw.Write(Wells[i].Index);
                        if (Wells[i].CoordLoaded)
                        {
                            Wells[i].Coord.Write(bw);
                        }
                        else
                        {
                            bw.Write(0); bw.Write(Constant.DOUBLE_NAN); bw.Write(Constant.DOUBLE_NAN); bw.Write(0);
                        }
                        Wells[i].Icons.Write(bw);
                        retValue = true;
                        ReportProgress(worker, i, Wells.Count);
                    }
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            return retValue;
        }
        #endregion

        #region PROJ_SKV
        public bool LoadProjWellsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadItemWellsFromCache(worker, e, TYPE_SKV.PROJ_SKV, ref ProjectWells);
        }
        public bool WriteProjectWellsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return WriteItemWellsToCache(worker, e, TYPE_SKV.PROJ_SKV, ref ProjectWells);
        }
        #endregion

        #region MER
        public bool CreateDeltaMer(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Создание файла delta МЭР";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\mer.bin";

            if (worker.CancellationPending)
            {
                e.Cancel = true;
                return false;
            }
            if (File.Exists(cacheName))
            {
                int i, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int merLen = 80, merExLen = 44;
                int count, countEx;
                Well w;

                string deltaCache = string.Format("{0}\\cache\\{1}\\mer_delta_cache.bin", ProjectPath, Name);
                string deltaName = string.Format("{0}\\cache\\{1}\\mer_delta_{2:dd.MM.yyyy}.bin", ProjectPath, Name, DateTime.Now);
                DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
                MemoryStream ms, unpackMS;
                BinaryReader br;
                DateTime lastWellMerDate;

                Mer mer;
                FileStream fs = null, fsDeltaCache = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);

                    fsDeltaCache = new FileStream(deltaCache, FileMode.Create);

                    if (Mer.VERSION == bfr.ReadInt32())
                    {
                        BinaryWriter bfw = new BinaryWriter(fsDeltaCache);
                        bfw.Write(DateTime.MinValue.ToOADate());
                        bfw.Write(DateTime.MaxValue.ToOADate());
                        bfw.Write(Wells.Count);

                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return false;
                                }
                                well_index = br.ReadInt32();
                                count = br.ReadInt32();
                                countEx = br.ReadInt32();

                                w = Wells[well_index];
                                userState.Element = w.Name;
                                bfw.Write(well_index);

                                if (w.MerLoaded)
                                {
                                    lastWellMerDate = DateTime.MinValue;
                                    if (count > 0)
                                    {
                                        br.BaseStream.Seek(merLen * (count - 1), SeekOrigin.Current);
                                        lastWellMerDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                        br.BaseStream.Seek(merLen - 4 + merExLen * countEx, SeekOrigin.Current);
                                    }
                                    mer = w.Mer.GetItemsByDate(lastWellMerDate);
                                    if (mer.Count > 0)
                                    {
                                        if (minDate > mer.Items[0].Date) minDate = mer.Items[0].Date;
                                        if (maxDate < mer.Items[mer.Count - 1].Date) maxDate = mer.Items[mer.Count - 1].Date;
                                    }
                                    mer.Write(bfw);
                                }
                                else
                                {
                                    br.BaseStream.Seek(merLen * count + merExLen * countEx, SeekOrigin.Current);
                                    bfw.Write(0);
                                    bfw.Write(0);
                                }
                                retValue = true;
                                ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                            wellCount += well_count;
                        }
                        bfw.BaseStream.Seek(0, SeekOrigin.Begin);
                        if (minDate == DateTime.MaxValue) minDate = DateTime.MinValue;
                        if (maxDate == DateTime.MinValue) maxDate = DateTime.MaxValue;
                        bfw.Write(minDate.ToOADate());
                        bfw.Write(maxDate.ToOADate());
                        bfw.Write(Wells.Count);

                        fsDeltaCache.Seek(0, SeekOrigin.Begin);
                        MemoryStream packStream = new MemoryStream();
                        ConvertEx.PackStream(fsDeltaCache, ref packStream);
                        FileStream fsDelta = new FileStream(deltaName, FileMode.Create);
                        packStream.WriteTo(fsDelta);
                        fsDelta.Close();
                        fsDeltaCache.Close();
                        File.Delete(deltaCache);
                        fsDeltaCache = null;
                        File.SetCreationTime(deltaName, DateTime.Now);
                        File.SetLastWriteTime(deltaName, DateTime.Now);
                    }
                }
                finally
                {
                    if (fs != null) fs.Close();
                    if (fsDeltaCache != null) fsDeltaCache.Close();
                }
            }
            return retValue;
        }
        public void LoadMerFastDeltaUpdates(BackgroundWorker worker, DoWorkEventArgs e)
        {
            int i, j, wellIndex;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка delta обновлений МЭР";
            userState.Element = Name;
            DirectoryInfo dir = new DirectoryInfo(Path.Combine(ProjectPath, Path.Combine("cache", Name)));
            if (!File.Exists(Path.Combine(dir.FullName, "mer_fast.bin"))) return;
            FileInfo[] deltaFiles = dir.GetFiles("mer_delta_*.bin");
            if (deltaFiles.Length == 0) return;

            int[] deltaCounts = new int[deltaFiles.Length];
            FileStream fs;
            FileStream cache = new FileStream(Path.Combine(dir.FullName, "mer_fast_cache.bin"), FileMode.Create);
            FileStream oldChess = new FileStream(Path.Combine(dir.FullName, "mer_fast.bin"), FileMode.Open);
            BinaryReader[] deltaBr = new BinaryReader[deltaFiles.Length];
            BinaryWriter bw = new BinaryWriter(cache);

            try
            {
                Mer mer = null;
                List<int[]> WellIndices = new List<int[]>();
                var br = new BinaryReader(oldChess);
                DateTime minDate, maxDate, dt;
                minDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                maxDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                int wellShift;

                // read well indices
                int WellsCount = br.ReadInt32();

                for (i = 0; i < WellsCount; ++i)
                {
                    var WellIndexItem = new int[2];
                    WellIndexItem[0] = br.ReadInt32();   // well index
                    WellIndexItem[1] = br.ReadInt32();   // offset
                    WellIndices.Add(WellIndexItem);
                }
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    fs = new FileStream(deltaFiles[di].FullName, FileMode.Open);
                    deltaBr[di] = new BinaryReader(fs);
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (minDate > dt) minDate = dt;
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (maxDate < dt) maxDate = dt;
                    deltaCounts[di] = deltaBr[di].ReadInt32();
                }
                // min - max dates
                byte Month;
                int count, wellPosition;
                int year, yearsCount, minYear;
                int lastPosition;
                List<int> YearsPosition = new List<int>();
                bw.Write(SmartPlusSystem.PackDateTime(minDate));
                bw.Write(SmartPlusSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(-1);                   // well offset
                }
                minDate = DateTime.MaxValue;
                MerItem item;
                List<List<MerItem>> merItems = new List<List<MerItem>>();
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    wellPosition = (int)cache.Position;
                    wellShift = wellPosition - WellIndices[wi][1];
                    minYear = 9999;
                    yearsCount = 0;
                    YearsPosition.Clear();
                    merItems.Clear();

                    if (WellIndices[wi][1] != -1)
                    {
                        br.BaseStream.Seek(WellIndices[wi][1], SeekOrigin.Begin);
                        yearsCount = br.ReadInt32();
                        minYear = br.ReadInt32();
                        for (i = 0; i < yearsCount; i++)
                        {
                            YearsPosition.Add(br.ReadInt32());
                            merItems.Add(new List<MerItem>());
                        }
                        for (i = 0; i < yearsCount; i++)
                        {
                            if (YearsPosition[i] == -1) continue;
                            br.BaseStream.Seek(YearsPosition[i], SeekOrigin.Begin);
                            count = br.ReadInt32();
                            for (j = 0; j < count; j++)
                            {
                                Month = br.ReadByte();
                                item.Date = new DateTime(minYear + i, Month, 1);
                                item.PlastId = br.ReadInt16();
                                item.CharWorkId = br.ReadInt16();
                                item.StateId = br.ReadInt16();
                                item.MethodId = br.ReadInt16();
                                item.WorkTime = br.ReadInt16();
                                item.CollTime = br.ReadInt16();
                                item.Oil = br.ReadSingle();
                                item.Wat = br.ReadSingle();
                                item.Gas = br.ReadSingle();
                                item.StayReasonId = br.ReadInt16();
                                item.StayTime = br.ReadInt16();
                                item.OilV = 0;
                                item.AgentProdId = 0;
                                item.WatV = 0;
                                merItems[i].Add(item);
                            }
                        }
                    }
                    for (int di = 0; di < deltaBr.Length; di++)
                    {
                        mer = new Mer();
                        wellIndex = deltaBr[di].ReadInt32();
                        if (wellIndex == WellIndices[wi][0])
                        {   
                            mer.Read(deltaBr[di], false);
                            if (mer.Count > 0)
                            {
                                if (minYear == 9999) minYear = mer.Items[0].Date.Year;
                                for (i = 0; i < mer.Count; i++)
                                {
                                    year = mer.Items[i].Date.Year - minYear;
                                    while (year >= merItems.Count)
                                    {
                                        merItems.Add(new List<MerItem>());
                                        yearsCount++;
                                    }
                                    if (mer.Items[i].CharWorkId == 20)
                                    {
                                        mer.Items[i].Wat = mer.GetItemEx(i).Injection;
                                    }
                                    merItems[year].Add(mer.Items[i]);
                                }
                            }
                        }
                    }

                    if (yearsCount > 0)
                    {
                        bw.Write(yearsCount);
                        bw.Write(minYear);
                        lastPosition = (int)bw.BaseStream.Position + merItems.Count * sizeof(int);
                        for (i = 0; i < merItems.Count; i++)
                        {
                            bw.Write(lastPosition);
                            lastPosition += sizeof(int) + merItems[i].Count * (sizeof(byte) + 8 * sizeof(short) + 3 * sizeof(float));
                        }
                        for (i = 0; i < merItems.Count; i++)
                        {
                            bw.Write(merItems[i].Count);
                            if (merItems[i].Count > 0 && merItems[i][0].Date < minDate)
                            {
                                minDate = merItems[i][0].Date;
                            }
                            for (j = 0; j < merItems[i].Count; j++)
                            {
                                Month = (byte)merItems[i][j].Date.Month;
                                bw.Write(Month);
                                bw.Write((short)merItems[i][j].PlastId);
                                bw.Write((short)merItems[i][j].CharWorkId);
                                bw.Write((short)merItems[i][j].StateId);
                                bw.Write((short)merItems[i][j].MethodId);
                                bw.Write((short)merItems[i][j].WorkTime);
                                bw.Write((short)merItems[i][j].CollTime);
                                bw.Write((float)merItems[i][j].Oil);
                                bw.Write((float)merItems[i][j].Wat);
                                bw.Write((float)merItems[i][j].Gas);
                                bw.Write((short)merItems[i][j].StayReasonId);
                                bw.Write((short)merItems[i][j].StayTime);
                            }
                        }
                    }
                    lastPosition = (int)cache.Position;
                    WellIndices[wi][1] = (lastPosition != wellPosition) ? wellPosition : -1;
                }
                if (minDate == DateTime.MaxValue) minDate = DateTime.MinValue;
                cache.Seek(0, SeekOrigin.Begin);
                bw.Write(SmartPlusSystem.PackDateTime(minDate));
                bw.Write(SmartPlusSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(WellIndices[wi][1]);  // well offset
                }
                oldChess.Close();
                oldChess = null;
                cache.Flush();
                cache.Close();
                cache = null;
                if (File.Exists(Path.Combine(dir.FullName, "mer_fast_cache.bin")))
                {
                    if (File.Exists(Path.Combine(dir.FullName, "mer_fast.bin")))
                    {
                        File.Delete(Path.Combine(dir.FullName, "mer_fast.bin"));
                    }
                    File.Move(Path.Combine(dir.FullName, "mer_fast_cache.bin"), Path.Combine(dir.FullName, "mer_fast.bin"));
                }
            }
            catch
            {
                if (cache != null)
                {
                    cache.Close();
                    cache = null;
                }
            }
            finally
            {
                if (oldChess != null) oldChess.Close();
                if (cache != null) cache.Close();
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    if (deltaBr[di] != null)
                    {
                        deltaBr[di].Close();
                        if (deltaFiles[di].Exists) deltaFiles[di].Delete();
                    }
                }
                string name = Path.Combine(dir.FullName, "mer_fast_cache.bin");
                if (File.Exists(name)) File.Delete(name);
            }
        }
        public void CreateMerFast(BackgroundWorker worker, DoWorkEventArgs e)
        {
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Создание файла mer_fast";
            userState.Element = Name;

            string FastMerName = ProjectPath + "\\cache\\" + Name + "\\mer_fast.bin";
            var FastMerWellIndices = new MemoryStream();
            var FastMerYearIndices = new List<MemoryStream>();
            var FastMers = new List<List<MemoryStream>>(); // List of wells < List of years<> >
            var WellIndices = new BinaryWriter(FastMerWellIndices);
            var YearIndices = new List<BinaryWriter>();
            var Mers = new List<List<BinaryWriter>>(); // List of wells < List of years<> >
            int WellsCount = 0;
            WellIndices.Write(WellsCount); // будет WellsCount;
            if (worker != null) worker.ReportProgress(0, userState);
            
            for(int i = 0; i < Wells.Count; ++i)
            {
                var w = (Well)Wells[i];
                
                int Index = WellsCount;
                ++WellsCount;
                WellIndices.Write(i); // WellIndex
                WellIndices.Write(-1); // будет Stream.Position-to-Year-indices

                // Year indices
                FastMerYearIndices.Add(new MemoryStream());
                YearIndices.Add(new BinaryWriter(FastMerYearIndices[Index]));
                FastMers.Add(new List<MemoryStream>());
                Mers.Add(new List<BinaryWriter>());

                if (!w.MerLoaded) continue;

                int MinYear = w.Mer.Items[0].Date.Year;
                int MaxYear = w.Mer.Items[w.Mer.Items.Length - 1].Date.Year;

                int YearsCount = MaxYear - MinYear + 1;
                YearIndices[Index].Write(YearsCount); // YearsCount
                YearIndices[Index].Write(MinYear); // MinYear
                for(int j = 0; j < YearsCount; ++j)
                {
                    YearIndices[Index].Write(j); // будет Stream.Position-to-real-data
                }

                // Well-Year-data

                int MersCount = 0;
                int LastMi = -1;
                for(int j = 0; j < w.Mer.Items.Length; ++j)
                {
                    var Item = w.Mer.Items[j];
                    int mi = Item.Date.Year - MinYear;

                    if(LastMi != mi)
                    {
                        if(LastMi != -1)
                        {
                            Mers[Index][LastMi].Seek(0, SeekOrigin.Begin);
                            Mers[Index][LastMi].Write(MersCount);
                        }
                        while(mi - LastMi > 1)
                        {
                            FastMers[Index].Add(new MemoryStream());
                            Mers[Index].Add(new BinaryWriter(FastMers[Index][LastMi + 1]));
                            ++LastMi;
                        }
                        MersCount = 0;
                        LastMi = mi;
                    }
                    if(mi >= FastMers[Index].Count)
                    {
                        FastMers[Index].Add(new MemoryStream());
                        Mers[Index].Add(new BinaryWriter(FastMers[Index][mi]));
                        Mers[Index][mi].Write(MersCount); // потом перезапишем
                    }
                    ++MersCount;
                    var bw = Mers[Index][mi];
                    byte Month = (byte)Item.Date.Month;
                    bw.Write(Month);
                    bw.Write((short)Item.PlastId);
                    bw.Write((short)Item.CharWorkId);
                    bw.Write((short)Item.StateId);
                    bw.Write((short)Item.MethodId);
                    bw.Write((short)Item.WorkTime);
                    bw.Write((short)Item.CollTime);
                    bw.Write((float)Item.Oil);
                    if (Item.CharWorkId != 20)
                    {
                        bw.Write((float)Item.Wat);
                    }
                    else
                    {
                        bw.Write((w.Mer.CountEx > 0) ? (float)w.Mer.GetItemEx(j).Injection : 0);
                    }
                    bw.Write((float)Item.Gas);
                    bw.Write((short)Item.StayReasonId);
                    bw.Write((short)Item.StayTime);

                    if(j == w.Mer.Items.Length - 1)
                    {
                        Mers[Index][mi].Seek(0, SeekOrigin.Begin);
                        Mers[Index][mi].Write(MersCount);
                    }
                }
            }

            // wells count
            WellIndices.Seek(0, SeekOrigin.Begin);
            WellIndices.Write(WellsCount);
 
            // calc offsets
            long Offset = 2 * sizeof(int) + FastMerWellIndices.Length; // min/max mer date + WellIndices
            for(int i = 0; i < WellsCount; ++i)
            {
                // year-indices offset
                WellIndices.Seek(i * 2 * sizeof(int) + 2 * sizeof(int), SeekOrigin.Begin); // per 2 int on record + int (wellscoгnt) + int (wellindex)
                if (FastMerYearIndices[i].Length == 0)
                {
                    WellIndices.Write(-1);
                }
                else
                {
                    WellIndices.Write((int)Offset);
                }
               
                // shift behind year-indices
                Offset += FastMerYearIndices[i].Length;

                for(int j = 0; j < FastMers[i].Count; ++j)
                {
                    YearIndices[i].Seek(j * sizeof(int) + 2 * sizeof(int), SeekOrigin.Begin); // int per record + int (yearscount) + int (minyear)
                    if(0 == FastMers[i][j].Length)
                    {
                        int Wrong = -1;
                        YearIndices[i].Write(Wrong);
                    }
                    else
                    {
                        YearIndices[i].Write((int)Offset);
                    }
                    Offset += FastMers[i][j].Length;
                }
            }

            // write
            using(var fsm = new FileStream(FastMerName, FileMode.Create, FileAccess.Write))
            {
                using(var bw = new BinaryWriter(fsm))
                {
                    bw.Write(SmartPlusSystem.PackDateTime(minMerDate));
                    bw.Write(SmartPlusSystem.PackDateTime(maxMerDate));

                    FastMerWellIndices.WriteTo(fsm);
                    for(int i = 0; i < WellsCount; ++i)
                    {
                        FastMerYearIndices[i].WriteTo(fsm);
                        for(int j = 0; j < FastMers[i].Count; ++j)
                        {
                            FastMers[i][j].WriteTo(fsm);
                        }
                    }
                }
            }
            File.SetCreationTime(FastMerName, DateTime.Now);
            File.SetLastWriteTime(FastMerName, DateTime.Now);
        }
  
        public bool LoadMerFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных МЭР месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\mer.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i = 0, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    worker.ReportProgress(0, userState);
                    FileStream fs;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    if (Mer.VERSION == bfr.ReadInt32())
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        minMerDate = DateTime.MaxValue;
                        maxMerDate = DateTime.MinValue;
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                well_index = br.ReadInt32();
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    for (j = 0; j < Wells.Count; j++)
                                    {
                                        w = Wells[j];
                                        w.Mer = null;
                                    }
                                    MerLoaded = false;
                                    return false;
                                }
                                if (well_index > -1)
                                {
                                    w = Wells[well_index];
                                    w.Mer = new Mer();
                                    w.Mer.Read(br, NotLoadData);
                                    if (w.Mer.Count > 0)
                                    {
                                        if (w.Mer.Items[0].Date < this.minMerDate) this.minMerDate = w.Mer.Items[0].Date;
                                        if (w.Mer.Items[w.Mer.Count - 1].Date > this.maxMerDate) this.maxMerDate = w.Mer.Items[w.Mer.Count - 1].Date;
                                        if (!NotLoadData) this.MerLoaded = true;
                                    }
                                }
                                retValue = true;
                                ReportProgress(worker, wellCount + (i + 1), iLen, userState);
                            }
                            wellCount += well_count;
                        }
                    }
                    worker.ReportProgress(100, userState);
                    bfr.Close();
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша МЭР!";
                worker.ReportProgress(100, userState);
            }
            if (minMerDate == DateTime.MaxValue) minMerDate = DateTime.MinValue;
            if (maxMerDate == DateTime.MinValue) maxMerDate = DateTime.MaxValue;
            return retValue;
        }
        public bool LoadMerFromCache(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\mer.bin";
            if (File.Exists(cacheName))
            {
                int j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (Mer.VERSION == bfr.ReadInt32())
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        wellCount = 0;
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();
                            if (WellIndex < wellCount + well_count)
                            {
                                ms = GetBuffer(true);
                                unpackMS = GetBuffer(false);
                                ms.SetLength(len_block);
                                br = new BinaryReader(unpackMS);
                                ConvertEx.CopyStream(fs, ms, len_block);
                                ConvertEx.UnPackStream(ms, ref unpackMS);

                                shift = (WellIndex - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();
                                if (WellIndex != well_index)
                                {
                                    retValue = false;
                                }
                                else if (well_index > -1)
                                {
                                    w = Wells[well_index];
                                    w.Mer = new Mer();
                                    w.Mer.Read(br, NotLoadData);
                                }
                                break;
                            }
                            else
                            {
                                fs.Seek(len_block, SeekOrigin.Current);
                            }
                            wellCount += well_count;
                        }
                    }
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteMerToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных МЭР в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\mer.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 1024;
                Well w;
                List<MemoryStream> msArr = new List<MemoryStream>();
                List<int> wellCount = new List<int>(5);
                List<int> unpackSizes = new List<int>(5);
                List<int> WellsShift = new List<int>();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        len_all_wells += 8 + Wells[i].Mer.GetBinLength();
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);

                    len_block = 0;
                    k = 0;
                    wCount = 0;

                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        len_shift = 8 + w.Mer.GetBinLength();


                        if (len_block + len_shift <= len_all_wells)
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = Wells[j];
                                len_shift += 4 + w.Mer.GetBinLength();
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = Wells[j];
                                bw.Write(w.Index);
                                w.Mer.Write(bw);
                            }
                            unpackSizes.Add((int)ms.Length);
                            packedMS = ConvertEx.PackStream(ms);
                            msArr.Add(packedMS);
                            wellCount.Add(wCount);
                            WellsShift.Clear();

                            ms.SetLength(0);
                            k = i;

                            w = Wells[i];
                            len_shift = 8 + w.Mer.GetBinLength();

                            len_block = len_shift;
                            wCount = 1;
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    len_shift = 4 * wCount;
                    bw.Write(len_shift);
                    for (j = k; j < i - 1; j++)
                    {
                        w = Wells[j];
                        len_shift += 4 + w.Mer.GetBinLength();
                        bw.Write(len_shift);
                    }

                    for (j = k; j < i; j++)
                    {
                        w = Wells[j];
                        bw.Write(w.Index);
                        w.Mer.Write(bw);
                    }
                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(Mer.VERSION);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write(unpackSizes[i]);
                        bw.Write(wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                    GC.GetTotalMemory(true);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearMerData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if (w.Mer != null) w.Mer.SetItems(null, null);
                    w.Mer = null;
                }
                if (useGC) GC.Collect(GC.MaxGeneration);
            }
            this.MerLoaded = false;
        }
        #endregion

        #region GIS
        public bool LoadGisFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadGisFromCache(worker, e, false);
        }
        public bool LoadGisFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГИС месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\gis.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, well_count, gis_len, well_index = -1, iLen, wellCount = 0;
                    int lenItem = 65;
                    Well w;
                    FileStream fs;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    if (SkvGIS.VERSION == bfr.ReadInt32())
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();

                        MemoryStream ms = GetBuffer(true);
                        MemoryStream unpackMS = GetBuffer(false);
                        BinaryReader br = new BinaryReader(unpackMS);
                        worker.ReportProgress(0, userState);
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ClearMemoryStream(ms);
                            ms.SetLength(len_block);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ClearMemoryStream(unpackMS);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                well_index = br.ReadInt32();
                                gis_len = br.ReadInt32();
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    for (j = 0; j < Wells.Count; j++)
                                    {
                                        Wells[j].Gis = null;
                                    }
                                    GisLoaded = false;
                                    return false;
                                }
                                if ((well_index > -1) && (gis_len > 0))
                                {
                                    w = Wells[well_index];
                                    if (!NotLoadData)
                                    {
                                        if (w.Gis == null) w.Gis = new SkvGIS();
                                        SkvGisItem[] items = new SkvGisItem[gis_len];
                                        for (j = 0; j < gis_len; j++)
                                        {
                                            items[j] = w.Gis.RetrieveItem(br);
                                        }
                                        w.Gis.SetItems(items);
                                    }
                                    else
                                    {
                                        unpackMS.Seek(gis_len * lenItem, SeekOrigin.Current);
                                    }
                                    if (!NotLoadData) GisLoaded = true;
                                }
                                retValue = true;
                                ReportProgress(worker, wellCount + (i + 1), iLen, userState);
                            }
                            wellCount += well_count;
                        }
                        if (iLen == 0) worker.ReportProgress(100, userState);
                    }
                    bfr.Close();
                }
            }
            return retValue;
        }
        public bool LoadGisFromCache(int WellIndex)
        {
            return LoadGisFromCache(WellIndex, false);
        }
        public bool LoadGisFromCache(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\gis.bin";
            if (File.Exists(cacheName))
            {
                int j, k, num_blocks, unpack_len_block, len_block, well_count, gis_len, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                if (SkvGIS.VERSION == bfr.ReadInt32())
                {
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;

                    MemoryStream ms = GetBuffer(true);
                    MemoryStream unpackMS = GetBuffer(false);
                    BinaryReader br = new BinaryReader(unpackMS);

                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        well_count = bfr.ReadInt32();
                        if (WellIndex < wellCount + well_count)
                        {
                            ms.SetLength(len_block);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            shift = (WellIndex - wellCount) * 4;
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            shift = br.ReadInt32();
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            well_index = br.ReadInt32();
                            gis_len = br.ReadInt32();
                            if (WellIndex != well_index)
                            {
                                retValue = false;
                            }
                            else if ((well_index > -1) && (gis_len > 0))
                            {
                                w = Wells[well_index];
                                if (!NotLoadData)
                                {
                                    if (w.Gis == null) w.Gis = new SkvGIS();
                                    SkvGisItem[] items = new SkvGisItem[gis_len];
                                    for (j = 0; j < gis_len; j++)
                                    {
                                        items[j] = w.Gis.RetrieveItem(br);
                                    }
                                    w.Gis.SetItems(items);
                                }
                            }
                            break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += well_count;
                    }
                }
                bfr.Close();
            }
            return retValue;
        }
        public bool WriteGisToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных ГИС в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\gis.bin";
                int MAX_SIZE_PACK_BLOCK = 100 * 1024;
                Well w;
                List<MemoryStream> msArr = new List<MemoryStream>();
                List<int> wellCount = new List<int>(5);
                List<int> unpackSizes = new List<int>(5);
                List<int> WellsShift = new List<int>();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;
                int i, j, k, h, lenItem = 65, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int gis_len, iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        if ((w.GisLoaded) && (w.Gis != null) && (w.Gis.Items != null))
                            len_all_wells += 12 + w.Gis.Count * lenItem;
                        else
                            len_all_wells += 12;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);

                    len_block = 0;
                    k = 0;
                    wCount = 0;

                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        if ((w.GisLoaded) && (w.Gis != null) && (w.Gis.Items != null))
                            len_shift = 12 + w.Gis.Count * lenItem;
                        else
                            len_shift = 12;


                        if (len_block + len_shift <= len_all_wells)
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = Wells[j];
                                if (w.GisLoaded) len_shift += 8 + w.Gis.Count * lenItem;
                                else len_shift += 8;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = Wells[j];
                                bw.Write(w.Index);
                                if (w.GisLoaded)
                                {
                                    gis_len = w.Gis.Count;
                                    bw.Write(gis_len);
                                    for (h = 0; h < gis_len; h++)
                                    {
                                        w.Gis.RetainItem(bw, h);
                                    }
                                }
                                else
                                {
                                    bw.Write(0);
                                }
                            }
                            unpackSizes.Add((int)ms.Length);
                            packedMS = ConvertEx.PackStream(ms);
                            msArr.Add(packedMS);
                            wellCount.Add(wCount);
                            WellsShift.Clear();

                            ms.SetLength(0);
                            k = i;

                            w = Wells[i];
                            if ((w.GisLoaded) && (w.Gis != null) && (w.Gis.Items != null))
                                len_shift = 12 + w.Gis.Count * lenItem;
                            else
                                len_shift = 12;

                            len_block = len_shift;
                            wCount = 1;
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    len_shift = 4 * wCount;
                    bw.Write(len_shift);
                    for (j = k; j < i - 1; j++)
                    {
                        w = Wells[j];
                        if (w.GisLoaded) len_shift += 8 + w.Gis.Count * lenItem;
                        else len_shift += 8;
                        bw.Write(len_shift);
                    }

                    for (j = k; j < i; j++)
                    {
                        w = Wells[j];
                        bw.Write(w.Index);
                        if (w.GisLoaded)
                        {
                            gis_len = w.Gis.Count;
                            bw.Write(gis_len);
                            for (h = 0; h < gis_len; h++)
                            {
                                w.Gis.RetainItem(bw, h);
                            }
                        }
                        else
                        {
                            bw.Write(0);
                        }
                    }
                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(SkvGIS.VERSION);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                    GC.Collect();
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearGisData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if (w.GisLoaded)
                    {
                        w.Gis = null;
                    }
                }
            }
            if (useGC) GC.Collect();
            GisLoaded = false;
        }
        #endregion

        #region PERFORATION
        public bool LoadPerfDataFromOisServer(BackgroundWorker worker, DoWorkEventArgs e, string Query)
        {
            bool retValue = false;
            // загрузка с оракла
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных перфорации с сервера";
            userState.Element = this.Name;

            worker.ReportProgress(0, userState);
            if (Query.IndexOf("%OILFIELDCODE%") == -1)
            {
                userState.Error = "Неверный запрос к базе данных. Нет объекта %OILFIELDCODE%.";
                worker.ReportProgress(0, userState);
                return false;
            }
            Query = Query.Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());

            string MainKey = ParamsDict.EnterpriseName + "@" + ParamsDict.OilFieldName + "@all@skv";
           
            worker.ReportProgress(0, userState);

                string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                            ParamsDict.OraServerName + ")(PORT=" + ParamsDict.OraPort.ToString() +
                            ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + ParamsDict.OraSid + ")));" +
                            "USER ID=" + ParamsDict.OraLogin + ";PASSWORD=" + ParamsDict.OraLogin + ";";
            using (OracleConnection conn = new OracleConnection(connString))
            {
                OracleCommand oraCommand = conn.CreateCommand();
                oraCommand.CommandText = Query;
                OracleDataReader oraReader;
                userState.WorkCurrentTitle = "Выполняется запрос к серверу Oracle...";
                worker.ReportProgress(0, userState);
                try
                {
                    MemoryStream ms;
                    BinaryWriter bw;
                    int i = 0, iLen = Wells.Count;
                    userState.WorkCurrentTitle = "Чтение данных с сервера Oracle...";
                    float top, bottom;
                    string key;

                    #region Чтение данных перфорации
                    conn.Open();
                    oraReader = oraCommand.ExecuteReader();

                    bool res = oraReader.Read(), res2;
                    worker.ReportProgress(0, userState);

                    int j, well_index, well_id, bore_id, count;
                    int areaCode;//, boreIndex, lastBoreIndex;
                    string well_name;
                    DateTime dt;
                    SkvPerf perf;
                    Well w;
                    SkvPerf oisPerf;
                    List<int> perfCounts = new List<int>();
                    while (res)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            ClearPerfData(false);
                            retValue = false;
                            break;
                        }
                        else
                        {
                            well_id = Convert.ToInt32(oraReader[0]);
                            bore_id = Convert.ToInt32(oraReader[1]);
                            areaCode = Convert.ToInt32(oraReader[3]);
                            well_name = ((string)oraReader[2]).Trim();
                            well_name = well_name.ToUpper();
                            //well_index = GetWellIndex(well_name, areaCode);
                            well_index = GetWellIndex(bore_id);
                            if (well_index == -1)
                            {
                                userState.Error = "Не найдена скважина " + well_name;
                                ReportProgress(worker, i, Wells.Count, userState);
                                while ((res) && (Convert.ToInt32(oraReader[1]) == bore_id)) res = oraReader.Read();
                                continue;
                            }
                            w = Wells[well_index];
                            //lastBoreIndex = -1;
                            count = 0;
                            ms = GetBuffer(true);
                            bw = new BinaryWriter(ms);
                            res2 = res;
                            perfCounts.Clear();
                            while (res2)
                            {
                                if (Convert.ToInt32(oraReader[1]) != bore_id) break;
                                //if (Convert.ToInt32(oraReader[1]) != boreId)
                                //{
                                //    boreId = Convert.ToInt32(oraReader[1]);
                                //    perfCounts.Add(0);
                                //}
                                dt = Convert.ToDateTime(oraReader[4]);
                                dt = new DateTime(dt.Year, dt.Month, dt.Day);
                                //boreIndex = Convert.ToInt32(oraReader[3]);
                                //if (boreIndex > lastBoreIndex) lastBoreIndex = boreIndex;

                                top = Convert.ToSingle(oraReader[5]);
                                bottom = Convert.ToSingle(oraReader[6]);
                                if ((dt.Year > 1900) && (dt <= DateTime.Now.AddDays(1)) && (top > 0) && (bottom > 0))
                                {
                                    //if (perfCounts.Count > 0)
                                    //{
                                    //    perfCounts[perfCounts.Count - 1]++;
                                    //}
                                    count++;
                                    bw.Write(dt.ToOADate());
                                    bw.Write(top); // top
                                    bw.Write(bottom); // bottom
                                    bw.Write(Convert.ToUInt16(oraReader[7])); // perforator_type
                                    bw.Write(Convert.ToUInt16(oraReader[8])); // holes
                                    bw.Write((Convert.ToUInt16(oraReader[9]) == 2)); // on-off
                                    bw.Write(Convert.ToUInt16(oraReader[10])); // perf type
                                    bw.Write(Convert.ToUInt16(oraReader[11])); // isolate type
                                }
                                res2 = oraReader.Read();
                            }
                            res = res2;
                            if (count > 0)
                            {
                                BinaryReader br = new BinaryReader(ms);
                                ms.Seek(0, SeekOrigin.Begin);

                                SkvPerfItem[] perfItems = new SkvPerfItem[count];

                                for (j = 0; j < count; j++)
                                {
                                    perfItems[j].Date = DateTime.FromOADate(br.ReadDouble());
                                    perfItems[j].Top = br.ReadSingle();
                                    perfItems[j].Bottom = br.ReadSingle();
                                    perfItems[j].PerforatorTypeId = br.ReadUInt16();
                                    perfItems[j].NumHoles = br.ReadUInt16();
                                    perfItems[j].Closed = br.ReadBoolean();
                                    perfItems[j].PerfTypeId = br.ReadUInt16();
                                    if (perfItems[j].Closed) perfItems[j].PerfTypeId = (UInt16)10000;
                                    perfItems[j].PerfIsolateTypeId = br.ReadUInt16();
                                    perfItems[j].Source = 1; // источник Matrix

                                }

                                oisPerf = new SkvPerf();
                                oisPerf.SetItems(perfItems);
                                // добавляем к существующей перф
                                w.AddPerforation(oisPerf);
                            }
                            retValue = true;
                            i++;
                            ReportProgress(worker, i, Wells.Count);
                        }
                    }
                    #endregion
                    oraReader.Close();
                }
                catch (Exception ex)
                {
                    userState.Error = "Ошибка загрузки с сервера Oracle!" + ex.Message;
                    worker.ReportProgress(0, userState);
                    return false;
                }
                userState.WorkCurrentTitle = "Данные с сервера Oracle получены";
                worker.ReportProgress(0, userState);
            }
            return retValue;
        }
        public bool LoadPerfFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadPerfFromCache(worker, e, false);
        }
        public bool LoadPerfFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных перфорации месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\perf.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, iLen, well_index, perf_count;
                    long shift;
                    long[] shiftList;
                    Well w;

                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                        BinaryReader bfr = new BinaryReader(fs);
                        if (SkvPerf.VERSION == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearPerfData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                perf_count = bfr.ReadInt32();

                                if ((well_index > -1) && (perf_count > 0))
                                {
                                    w = Wells[well_index];
                                    if (!NotLoadData)
                                    {
                                        if (w.Perf == null) w.Perf = new SkvPerf();
                                        SkvPerfItem[] items = new SkvPerfItem[perf_count];
                                        for (j = 0; j < perf_count; j++)
                                        {
                                            items[j] = w.Perf.RetrieveItem(bfr, SkvPerf.VERSION);
                                        }
                                        w.Perf.SetItems(items);
                                    }
                                }
                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша перфорации!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadPerfFromCache(int WellIndex)
        {
            return LoadPerfFromCache(WellIndex, false);
        }
        public bool LoadPerfFromCache(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\perf.bin";
            if (File.Exists(cacheName))
            {
                int j,perf_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (SkvPerf.VERSION == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                        }
                        else if (well_index > -1)
                        {
                            w = Wells[well_index];
                            perf_count = bfr.ReadInt32();
                            if ((!NotLoadData) && (perf_count > 0))
                            {
                                if (w.Perf == null) w.Perf = new SkvPerf();
                                SkvPerfItem[] items = new SkvPerfItem[perf_count];

                                for (j = 0; j < perf_count; j++)
                                {
                                    items[j] = w.Perf.RetrieveItem(bfr, SkvPerf.VERSION);
                                }
                                w.Perf.SetItems(items);
                            }
                            retValue = true;
                        }
                    }
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        public bool WritePerfToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных перфорации в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\perf.bin";
                Well w;
                FileStream fs;
                BinaryWriter bfw;
                int i, j, perf_len;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    bfw = new BinaryWriter(fs);
                    bfw.Write(SkvPerf.VERSION);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current);
                    long[] shiftList = new long[iLen];

                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        shiftList[i] = fs.Position;
                        bfw.Write(w.Index);
                        if (w.PerfLoaded)
                        {
                            perf_len = w.Perf.Count;
                            bfw.Write(perf_len);
                            for (j = 0; j < perf_len; j++)
                            {
                                w.Perf.RetainItem(bfw, j);
                            }
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftList.Length > 0)
                    {
                        fs.Seek(8, SeekOrigin.Begin);
                        for (i = 0; i < shiftList.Length; i++)
                        {
                            bfw.Write(shiftList[i]);
                        }
                    }
                    bfw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearPerfData(bool useGC)
        {
            int i, len = Wells.Count;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    if (Wells[i].PerfLoaded)
                    {
                        Wells[i].Perf = null;
                    }
                }
                if (useGC) GC.Collect(GC.MaxGeneration);
            }
            PerfLoaded = false;
        }
        #endregion

        #region LOGS
 
        // BASE LOGS
        public bool LoadLogsBaseFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadLogsBaseFromCache(worker, e, false, false);
        }
        public bool LoadLogsBaseFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotLoadValues)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных базового каротажа месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\logs_base.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, pack_len, logs_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    int maxPackLen = 0, maxUnPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        if (SkvLog.VERSION == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearLogsBaseData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                pack_len = bfr.ReadInt32();
                                if (maxPackLen < pack_len) maxPackLen = pack_len;
                                if ((well_index > -1) && (pack_len > 0))
                                {
                                    w = Wells[well_index];
                                    if (!NotLoadData)
                                    {
                                        ms = GetBuffer(true);
                                        unpackMS = GetBuffer(false);
                                        ms.SetLength(pack_len);
                                        br = new BinaryReader(unpackMS);
                                        ConvertEx.CopyStream(fs, ms, pack_len);

                                        ConvertEx.UnPackStream(ms, ref unpackMS);
                                        logs_count = br.ReadInt32();
                                        if (logs_count > 0)
                                        {
                                            w.LogsBase = new SkvLog[logs_count];
                                            for (j = 0; j < logs_count; j++)
                                            {
                                                if (w.LogsBase[j] == null) w.LogsBase[j] = new SkvLog();
                                                w.LogsBase[j].RetriveItems(br, !NotLoadValues);
                                            }
                                            if (maxUnPackLen < (int)unpackMS.Position) maxUnPackLen = (int)unpackMS.Position;
                                        }
                                    }
                                }

                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша каротажа!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadLogsBaseFromCache(int WellIndex)
        {
            return LoadLogsBaseFromCache(WellIndex, false, false);
        }
        public bool LoadLogsBaseFromCache(int WellIndex, bool NotLoadData, bool NotLoadValues)
        {
            bool retValue = false;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\logs_base.bin";
            if (File.Exists(cacheName))
            {
                int j,pack_len, logs_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);

                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    if (SkvLog.VERSION == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        pack_len = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                        }
                        else if ((well_index > -1) && (pack_len > 0))
                        {
                            w = Wells[well_index];
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(pack_len);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, pack_len);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            logs_count = br.ReadInt32();
                            if ((!NotLoadData) && (logs_count > 0))
                            {
                                w.LogsBase = new SkvLog[logs_count];
                                for (j = 0; j < logs_count; j++)
                                {
                                    if (w.LogsBase[j] == null) w.LogsBase[j] = new SkvLog();
                                    w.LogsBase[j].RetriveItems(br, !NotLoadValues);
                                }
                            }
                        }
                    }
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteLogsBaseToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных базового каротажа в кэш";
            userState.Element = Name;
            System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                int i, j;
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\logs_base.bin";
                Well w;
                FileStream fs;
                int logs_count, iLen = Wells.Count;

                if (iLen > 0)
                {
                    long[] shiftWells = new long[iLen];
                    MemoryStream ms = GetBuffer(true);
                    MemoryStream packedMS;
                    BinaryWriter bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    BinaryWriter bfw = new BinaryWriter(fs);
                    bfw.Write(SkvLog.VERSION);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current); // сдвигаемся на начало данных
                    shiftWells[0] = fs.Position;

                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        ClearMemoryStream(ms);
                        if (i > 0)
                        {
                            shiftWells[i] = fs.Position;
                        }
                        bfw.Write(w.Index);
                        if (w.LogsBaseLoaded)
                        {
                            logs_count = w.LogsBase.Length;
                            bw.Write(logs_count);
                            for (j = 0; j < logs_count; j++)
                            {
                                w.LogsBase[j].RetainItems(bw);
                                w.LogsBase[j] = null;
                            }

                            w.LogsBase = null;
                            packedMS = ConvertEx.PackStream(ms);
                            bfw.Write((int)packedMS.Length);
                            packedMS.Seek(0, SeekOrigin.Begin);
                            packedMS.WriteTo(fs);
                        }
                        else if (w.PackedLogsBase != null)
                        {
                            bfw.Write((int)w.PackedLogsBase.Length);
                            w.PackedLogsBase.Seek(0, SeekOrigin.Begin);
                            w.PackedLogsBase.WriteTo(fs);
                            w.PackedLogsBase.Dispose();
                            w.PackedLogsBase = null;
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftWells.Length > 0)
                    {
                        fs.Seek(8, SeekOrigin.Begin);
                        for (i = 0; i < shiftWells.Length; i++)
                        {
                            bfw.Write(shiftWells[i]);
                        }
                    }
                    bfw.Close();
                    GC.GetTotalMemory(true);
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }

        // OTHER LOGS
        public bool LoadLogsFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadLogsFromCache(worker, e, false, false);
        }
        public bool LoadLogsFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData, bool NotLoadValues)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных каротажа месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\logs.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, pack_len, logs_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    int maxPackLen = 0, maxUnPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        if (SkvLog.VERSION == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearLogsData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                pack_len = bfr.ReadInt32();
                                if (maxPackLen < pack_len) maxPackLen = pack_len;
                                if ((well_index > -1) && (pack_len > 0))
                                {
                                    w = Wells[well_index];
                                    ms = GetBuffer(true);
                                    unpackMS = GetBuffer(false);
                                    ms.SetLength(pack_len);
                                    br = new BinaryReader(unpackMS);
                                    ConvertEx.CopyStream(fs, ms, pack_len);

                                    ConvertEx.UnPackStream(ms, ref unpackMS);
                                    logs_count = br.ReadInt32();
                                    if ((!NotLoadData) && (logs_count > 0))
                                    {
                                        w.Logs = new SkvLog[logs_count];
                                        for (j = 0; j < logs_count; j++)
                                        {
                                            if (w.Logs[j] == null) w.Logs[j] = new SkvLog();
                                            w.Logs[j].RetriveItems(br, !NotLoadValues);
                                        }
                                        if (maxUnPackLen < (int)unpackMS.Position) maxUnPackLen = (int)unpackMS.Position;
                                    }
                                }
                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша каротажа!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadLogsFromCache(int WellIndex)
        {
            return LoadLogsFromCache(WellIndex, false, false);
        }
        public bool LoadLogsFromCache(int WellIndex, bool NotLoadData, bool NotLoadValues)
        {
            bool retValue = false;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\logs.bin";
            if (File.Exists(cacheName))
            {
                int j,pack_len, logs_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    if (SkvLog.VERSION == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        pack_len = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                            //MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if ((well_index > -1) && (pack_len > 0))
                        {
                            w = Wells[well_index];
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(pack_len);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, pack_len);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            logs_count = br.ReadInt32();
                            if ((!NotLoadData) && (logs_count > 0))
                            {
                                w.Logs = new SkvLog[logs_count];
                                for (j = 0; j < logs_count; j++)
                                {
                                    if (w.Logs[j] == null) w.Logs[j] = new SkvLog();
                                    w.Logs[j].RetriveItems(br, !NotLoadValues);
                                }
                            }
                        }
                    }
                    bfr.Close();
                    fs = null;
                }
                catch (Exception)
                {
                    if (fs != null) fs.Close();
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteLogsToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных каротажа в кэш";
            userState.Element = Name;
            System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                int i, j;
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\logs.bin";
                Well w;
                FileStream fs;
                int logs_count, iLen = Wells.Count;

                if (iLen > 0)
                {
                    long[] shiftWells = new long[iLen];
                    MemoryStream ms = GetBuffer(true);
                    MemoryStream packedMS;
                    BinaryWriter bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    BinaryWriter bfw = new BinaryWriter(fs);
                    bfw.Write(SkvLog.VERSION);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current); // сдвигаемся на начало данных
                    shiftWells[0] = fs.Position;

                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        ClearMemoryStream(ms);
                        if (i > 0)
                        {
                            shiftWells[i] = fs.Position;
                        }
                        bfw.Write(w.Index);
                        if (w.LogsLoaded)
                        {
                            logs_count = w.Logs.Length;
                            bw.Write(logs_count);
                            for (j = 0; j < logs_count; j++)
                            {
                                w.Logs[j].RetainItems(bw);
                                w.Logs[j] = null;
                            }
                            w.Logs = null;
                            packedMS = ConvertEx.PackStream(ms);
                            bfw.Write((int)packedMS.Length);
                            packedMS.Seek(0, SeekOrigin.Begin);
                            packedMS.WriteTo(fs);
                        }
                        else if (w.PackedLogs != null)
                        {
                            bfw.Write((int)w.PackedLogs.Length);
                            w.PackedLogs.Seek(0, SeekOrigin.Begin);
                            w.PackedLogs.WriteTo(fs);
                            w.PackedLogs.Dispose();
                            w.PackedLogs = null;
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftWells.Length > 0)
                    {
                        fs.Seek(8, SeekOrigin.Begin);
                        for (i = 0; i < shiftWells.Length; i++)
                        {
                            bfw.Write(shiftWells[i]);
                        }
                    }
                    bfw.Close();
                    bw = null;
                    shiftWells = null;
                    GC.GetTotalMemory(true);
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearLogsData(bool useGC)
        {
            int i, j, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if ((w.LogsLoaded) && (w.Logs != null))
                    {
                        for (j = 0; j < w.Logs.Length; j++)
                        {
                            w.Logs[j] = null;
                        }
                        w.Logs = null;
                    }
                }
            }
            if (useGC) GC.GetTotalMemory(true);
            LogsLoaded = false;
        }
        public void ClearLogsBaseData(bool useGC)
        {
            int i, j, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if ((w.LogsBaseLoaded) && (w.LogsBase != null))
                    {
                        for (j = 0; j < w.LogsBase.Length; j++)
                        {
                            w.LogsBase[j] = null;
                        }
                        w.LogsBase = null;
                    }
                }
            }
            if (useGC) GC.GetTotalMemory(true);
            LogsBaseLoaded = false;
        }
        #endregion

        #region CHESS
        public bool CreateDeltaChess(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Создание файла delta Шахматки";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\chess.bin";
            
            if (worker.CancellationPending)
            {
                e.Cancel = true;
                return false;
            }
            if (File.Exists(cacheName))
            {
                int i, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                int chessLen = 29, chessInjLen = 18;
                int count;
                Well w;
                bool IsExistData = false;
                string deltaCache = string.Format("{0}\\cache\\{1}\\chess_delta_cache.bin", ProjectPath, Name);
                string deltaName = string.Format("{0}\\cache\\{1}\\chess_delta_{2:dd.MM.yyyy}.bin", ProjectPath, Name, DateTime.Now);
                DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
                MemoryStream ms, unpackMS;
                BinaryReader br;
                DateTime lastWellChessDate;
                List<ChessDailyItem> chessItems;
                List<ChessInjDailyItem> chessInjItems;
                WellChess chess;
                WellChessInj chessInj;
                FileStream fs = null, fsDeltaCache = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);

                    fsDeltaCache = new FileStream(deltaCache, FileMode.Create);

                    if (WellChess.Version == bfr.ReadInt32())
                    {
                        BinaryWriter bfw = new BinaryWriter(fsDeltaCache);
                        bfw.Write(DateTime.MinValue.ToOADate());
                        bfw.Write(DateTime.MaxValue.ToOADate());
                        bfw.Write(Wells.Count);

                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return false;
                                }
                                well_index = br.ReadInt32();
                                w = Wells[well_index];
                                userState.Element = w.Name;
                                bfw.Write(well_index);

                                if (w.ChessLoaded)
                                {
                                    count = br.ReadInt32();
                                    lastWellChessDate = DateTime.MinValue;
                                    if (count > 0)
                                    {
                                        br.BaseStream.Seek(chessLen * (count - 1), SeekOrigin.Current);
                                        lastWellChessDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                        br.BaseStream.Seek(chessLen - 4, SeekOrigin.Current);
                                    }
                                    chessItems = w.Chess.GetItemsByDate(lastWellChessDate);
                                    if (chessItems.Count > 0)
                                    {
                                        IsExistData = true;
                                        if (minDate > chessItems[0].Date) minDate = chessItems[0].Date;
                                        if (maxDate < chessItems[chessItems.Count - 1].Date) maxDate = chessItems[chessItems.Count - 1].Date;
                                    }
                                    chess = new WellChess(chessItems.ToArray());
                                    chess.WriteToCache(bfw);
                                }
                                else
                                {
                                    count = br.ReadInt32();
                                    if(count > 0) br.BaseStream.Seek(chessLen * count, SeekOrigin.Current);
                                    bfw.Write(0);
                                }

                                if (w.ChessInjLoaded)
                                {
                                    count = br.ReadInt32();
                                    lastWellChessDate = DateTime.MinValue;
                                    if (count > 0)
                                    {
                                        br.BaseStream.Seek(chessInjLen * (count - 1), SeekOrigin.Current);
                                        lastWellChessDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                        br.BaseStream.Seek(chessInjLen - 4, SeekOrigin.Current);
                                    }
                                    chessInjItems = w.ChessInj.GetItemsByDate(lastWellChessDate);
                                    if (chessInjItems.Count > 0)
                                    {
                                        IsExistData = true;
                                        if (minDate > chessInjItems[0].Date) minDate = chessInjItems[0].Date;
                                        if (maxDate < chessInjItems[chessInjItems.Count - 1].Date) maxDate = chessInjItems[chessInjItems.Count - 1].Date;
                                    }

                                    chessInj = new WellChessInj(chessInjItems.ToArray());
                                    chessInj.WriteToCache(bfw);
                                }
                                else
                                {
                                    count = br.ReadInt32();
                                    if (count > 0) br.BaseStream.Seek(chessInjLen * count, SeekOrigin.Current);
                                    bfw.Write(0);
                                }

                                retValue = true;
                                ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                            wellCount += well_count;
                        }
                        bfw.BaseStream.Seek(0, SeekOrigin.Begin);
                        if (minDate == DateTime.MaxValue) minDate = DateTime.MinValue;
                        if (maxDate == DateTime.MinValue) maxDate = DateTime.MaxValue;
                        bfw.Write(minDate.ToOADate());
                        bfw.Write(maxDate.ToOADate());
                        bfw.Write(Wells.Count);
                        fsDeltaCache.Seek(0, SeekOrigin.Begin);
                        MemoryStream packStream = new MemoryStream();
                        ConvertEx.PackStream(fsDeltaCache, ref packStream);
                        FileStream fsDelta = new FileStream(deltaName, FileMode.Create);
                        packStream.WriteTo(fsDelta);
                        fsDelta.Close();
                        fsDeltaCache.Close();
                        File.Delete(deltaCache);
                        fsDeltaCache = null;
                        File.SetCreationTime(deltaName, DateTime.Now);
                        File.SetLastWriteTime(deltaName, DateTime.Now);
                    }
                }
                finally
                {
                    if (fs != null) fs.Close();
                    if (fsDeltaCache != null) fsDeltaCache.Close();
                    if (!IsExistData && File.Exists(deltaName)) File.Delete(deltaName);
                }
            }
            return retValue;
        }

        public void CreateChessFast(BackgroundWorker worker, DoWorkEventArgs e, int CutOffDays)
        {
            int i, j;
            Well w;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Создание файла chess_fast";
            userState.Element = Name;

            string FastChessName = ProjectPath + "\\cache\\" + Name + "\\chess_fast.bin";
            var FastChessWellIndices = new MemoryStream();
            var FastChessYearIndices = new List<MemoryStream>();
            var FastChess = new List<List<MemoryStream>>(); // List of wells < List of years<> >
            var WellIndices = new BinaryWriter(FastChessWellIndices);
            var YearIndices = new List<BinaryWriter>();
            var Chess = new List<List<BinaryWriter>>(); // List of wells < List of years<> >
            int WellsCount = 0;
            WellIndices.Write(WellsCount); // будет WellsCount;
            DateTime MinChessDate = DateTime.MaxValue, MaxChessDate = DateTime.MinValue;
            if (worker != null) worker.ReportProgress(0, userState);

            for (i = 0; i < Wells.Count; ++i)
            {
                w = Wells[i];
                if (!w.ChessLoaded) continue;
                if (w.Chess[w.Chess.Count - 1].Date > MaxChessDate)
                {
                    MaxChessDate = w.Chess[w.Chess.Count - 1].Date;
                }
            }
            if (MaxChessDate == DateTime.MinValue) MaxChessDate = DateTime.MaxValue;

            DateTime MinDate = DateTime.MaxValue;
            if (MaxChessDate.Year < 9999) MinDate = MaxChessDate.AddDays(-CutOffDays);
            for (i = 0; i < Wells.Count; ++i)
            {
                w = Wells[i];

                int Index = WellsCount;
                ++WellsCount;
                WellIndices.Write(i); // WellIndex
                WellIndices.Write(-1); // будет Stream.Position-to-Year-indices

                // Year indices
                FastChessYearIndices.Add(new MemoryStream());
                YearIndices.Add(new BinaryWriter(FastChessYearIndices[Index]));
                FastChess.Add(new List<MemoryStream>());
                Chess.Add(new List<BinaryWriter>());

                if (!w.ChessLoaded) continue;
                if (w.Chess[w.Chess.Count - 1].Date < MinDate) continue;

                int MinYear = w.Chess[0].Date.Year;
                if(MinYear < MinDate.Year)
                {
                    for (j = w.Chess.Count - 1; j >= 0; j--)
                    {
                        if (w.Chess[j].Date.Year < MinDate.Year) break;
                        MinYear = w.Chess[j].Date.Year;
                    }
                }
                int MaxYear = w.Chess[w.Chess.Count - 1].Date.Year;
                int YearsCount = MaxYear - MinYear + 1;
                YearIndices[Index].Write(YearsCount); // YearsCount
                YearIndices[Index].Write(MinYear); // MinYear
                for (j = 0; j < YearsCount; ++j)
                {
                    YearIndices[Index].Write(j); // будет Stream.Position-to-real-data
                }

                // Well-Year-data

                int ChessCount = 0;
                int LastMi = -1;

                for (j = 0; j < w.Chess.Count; ++j)
                {
                    var Item = w.Chess[j];
                    if (Item.Date < MinDate) continue;
                    if (MinChessDate > Item.Date) MinChessDate = Item.Date;

                    int mi = Item.Date.Year - MinYear;
                    if (LastMi != mi)
                    {
                        if (LastMi != -1)
                        {
                            Chess[Index][LastMi].Seek(0, SeekOrigin.Begin);
                            Chess[Index][LastMi].Write(ChessCount);
                        }

                        while (mi - LastMi > 1)
                        {
                            FastChess[Index].Add(new MemoryStream());
                            Chess[Index].Add(new BinaryWriter(FastChess[Index][LastMi + 1]));
                            ++LastMi;
                        }
                        ChessCount = 0;
                        LastMi = mi;
                    }

                    if (mi >= FastChess[Index].Count)
                    {
                        FastChess[Index].Add(new MemoryStream());
                        Chess[Index].Add(new BinaryWriter(FastChess[Index][mi]));
                        Chess[Index][mi].Write(ChessCount); // потом перезапишем
                    }
                    ++ChessCount;
                    var bw = Chess[Index][mi];
                    byte Day = (byte)Item.Date.Day;
                    byte Month = (byte)Item.Date.Month;
                    bw.Write(Month);
                    bw.Write(Day);
                    bw.Write(Item.Qliq);
                    bw.Write(Item.Qoil);
                    bw.Write(Item.QliqV);
                    bw.Write(Item.QoilV);
                    bw.Write((byte)Item.StayTime);
                    bw.Write(Item.Hdyn);
                    if (j == w.Chess.Count - 1)
                    {
                        Chess[Index][mi].Seek(0, SeekOrigin.Begin);
                        Chess[Index][mi].Write(ChessCount);
                    }
                }
                if (worker != null) worker.ReportProgress((int)(i * 100f / Wells.Count), userState);
            }
            if (MinChessDate == DateTime.MaxValue) MinChessDate = MinDate;
            // wells count
            WellIndices.Seek(0, SeekOrigin.Begin);
            WellIndices.Write(WellsCount);

            // calc offsets
            long Offset = 2 * sizeof(int) + FastChessWellIndices.Length; // min/max mer date + WellIndices
            for (i = 0; i < WellsCount; ++i)
            {
                // year-indices offset
                WellIndices.Seek(i * 2 * sizeof(int) + 2 * sizeof(int), SeekOrigin.Begin); // per 2 int on record + int (wellscoгnt) + int (wellindex)
                if (FastChessYearIndices[i].Length == 0)
                {
                    WellIndices.Write(-1);
                }
                else
                {
                    WellIndices.Write((int)Offset);
                }

                // shift behind year-indices
                Offset += FastChessYearIndices[i].Length;

                for (j = 0; j < FastChess[i].Count; ++j)
                {
                    YearIndices[i].Seek(j * sizeof(int) + 2 * sizeof(int), SeekOrigin.Begin); // int per record + int (yearscount) + int (minyear)
                    if (0 == FastChess[i][j].Length)
                    {
                        int Wrong = -1;
                        YearIndices[i].Write(Wrong);
                    }
                    else
                    {
                        YearIndices[i].Write((int)Offset);
                    }
                    Offset += FastChess[i][j].Length;
                }
            }
            // write
            using (var fsm = new FileStream(FastChessName, FileMode.Create, FileAccess.Write))
            {
                using (var bw = new BinaryWriter(fsm))
                {
                    bw.Write(SmartPlusSystem.PackDateTime(MinChessDate));
                    bw.Write(SmartPlusSystem.PackDateTime(MaxChessDate));
                    FastChessWellIndices.WriteTo(fsm);

                    for (i = 0; i < WellsCount; ++i)
                    {
                        FastChessYearIndices[i].WriteTo(fsm);
                        for (j = 0; j < FastChess[i].Count; ++j)
                        {
                            FastChess[i][j].WriteTo(fsm);
                        }
                    }
                }
            }
            File.SetCreationTime(FastChessName, DateTime.Now);
            File.SetLastWriteTime(FastChessName, DateTime.Now);
        }
        public void CreateChessInjFast(BackgroundWorker worker, DoWorkEventArgs e, int CutOffDays)
        {
            int i, j;
            Well w;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Создание файла chessinj_fast";
            userState.Element = Name;

            string FastChessName = ProjectPath + "\\cache\\" + Name + "\\chessinj_fast.bin";
            var FastChessWellIndices = new MemoryStream();
            var FastChessYearIndices = new List<MemoryStream>();
            var FastChess = new List<List<MemoryStream>>(); // List of wells < List of years<> >
            var WellIndices = new BinaryWriter(FastChessWellIndices);
            var YearIndices = new List<BinaryWriter>();
            var Chess = new List<List<BinaryWriter>>(); // List of wells < List of years<> >
            int WellsCount = 0;
            WellIndices.Write(WellsCount); // будет WellsCount;
            
           DateTime MinChessDate = DateTime.MaxValue, MaxChessDate = DateTime.MinValue;

            if (worker != null) worker.ReportProgress(0, userState);
            for (i = 0; i < Wells.Count; ++i)
            {
                w = Wells[i];
                if (!w.ChessInjLoaded) continue;

                if (w.ChessInj[w.ChessInj.Count - 1].Date > MaxChessDate)
                {
                    MaxChessDate = w.ChessInj[w.ChessInj.Count - 1].Date;
                }
            }
            if (MaxChessDate == DateTime.MinValue) MaxChessDate = DateTime.MaxValue;
            DateTime MinDate = DateTime.MaxValue;
            if (MaxChessDate.Year < 9999) MinDate = MaxChessDate.AddDays(-CutOffDays);
            for(i = 0; i < Wells.Count; ++i)
            {
                w = Wells[i];

                int Index = WellsCount;
                ++WellsCount;
                WellIndices.Write(i); // WellIndex
                WellIndices.Write(i); // будет Stream.Position-to-Year-indices

                // Year indices
                FastChessYearIndices.Add(new MemoryStream());
                YearIndices.Add(new BinaryWriter(FastChessYearIndices[Index]));
                FastChess.Add(new List<MemoryStream>());
                Chess.Add(new List<BinaryWriter>()); 

                if (!w.ChessInjLoaded) continue;
                if (w.ChessInj[w.ChessInj.Count - 1].Date < MinDate) continue;

                int MinYear = w.ChessInj[0].Date.Year;
                if (MinYear < MinDate.Year)
                {
                    for (j = w.ChessInj.Count - 1; j >= 0; j--)
                    {
                        if (w.ChessInj[j].Date.Year < MinDate.Year) break;
                        MinYear = w.ChessInj[j].Date.Year;
                    }
                }
                int MaxYear = w.ChessInj[w.ChessInj.Count - 1].Date.Year;
                int YearsCount = MaxYear - MinYear + 1;
                YearIndices[Index].Write(YearsCount); // YearsCount
                YearIndices[Index].Write(MinYear); // MinYear
                for(j = 0; j < YearsCount; ++j)
                {
                    YearIndices[Index].Write(j); // будет Stream.Position-to-real-data
                } 

                // Well-Year-data

                int ChessCount = 0;
                int LastMi = -1;

                for(j = 0; j < w.ChessInj.Count; ++j)
                {
                    var Item = w.ChessInj[j];
                    if (Item.Date < MinDate) continue;
                    if (MinChessDate > Item.Date) MinChessDate = Item.Date;

                    int mi = Item.Date.Year - MinYear;
                    if(LastMi != mi)
                    {
                        if(LastMi != -1)
                        {
                            Chess[Index][LastMi].Seek(0, SeekOrigin.Begin);
                            Chess[Index][LastMi].Write(ChessCount);
                        }
 
                        while(mi - LastMi > 1)
                        {
                            FastChess[Index].Add(new MemoryStream());
                            Chess[Index].Add(new BinaryWriter(FastChess[Index][LastMi + 1]));
                            ++LastMi;
                        } 
                        ChessCount = 0;
                        LastMi = mi;
                    }
 
                    if(mi >= FastChess[Index].Count)
                    {
                        FastChess[Index].Add(new MemoryStream());
                        Chess[Index].Add(new BinaryWriter(FastChess[Index][mi]));
                        Chess[Index][mi].Write(ChessCount); // потом перезапишем
                    } 
                    ++ChessCount;
                    var bw = Chess[Index][mi]; 
                    byte Day = (byte)Item.Date.Day;
                    byte Month = (byte)Item.Date.Month;
                    bw.Write(Month);
                    bw.Write(Day);
                    bw.Write(Item.Wwat);
                    bw.Write(Item.Pust);
                    bw.Write((byte)Item.CycleTime);
                    bw.Write((byte)Item.StayTime); 
                    if(j == w.ChessInj.Count - 1)
                    {
                        Chess[Index][mi].Seek(0, SeekOrigin.Begin);
                        Chess[Index][mi].Write(ChessCount);
                    }
                }
            }
            if (MinChessDate == DateTime.MaxValue) MinChessDate = MinDate;
            // wells count
            WellIndices.Seek(0, SeekOrigin.Begin);
            WellIndices.Write(WellsCount); 

            // calc offsets
            long Offset = 2 * sizeof(int) + FastChessWellIndices.Length; // min/max mer date + WellIndices
            for(i = 0; i < WellsCount; ++i)
            {
                // year-indices offset
                WellIndices.Seek(i * 2 * sizeof(int) + 2 * sizeof(int), SeekOrigin.Begin); // per 2 int on record + int (wellscoгnt) + int (wellindex)
                if (FastChessYearIndices[i].Length == 0)
                {
                    WellIndices.Write(-1);
                }
                else
                {
                    WellIndices.Write((int)Offset);
                }
 
                // shift behind year-indices
                Offset += FastChessYearIndices[i].Length;
 
                for(j = 0; j < FastChess[i].Count; ++j)
                {
                    YearIndices[i].Seek(j * sizeof(int) + 2 * sizeof(int), SeekOrigin.Begin); // int per record + int (yearscount) + int (minyear)
                    if(0 == FastChess[i][j].Length)
                    {
                        int Wrong = -1;
                        YearIndices[i].Write(Wrong);
                    }
                    else
                    {
                        YearIndices[i].Write((int)Offset);
                    } 
                    Offset += FastChess[i][j].Length;
                }
            }
            // write
            using(var fsm = new FileStream(FastChessName, FileMode.Create, FileAccess.Write))
            {
                using(var bw = new BinaryWriter(fsm))
                {
                    bw.Write(SmartPlusSystem.PackDateTime(MinChessDate));
                    bw.Write(SmartPlusSystem.PackDateTime(MaxChessDate));
                    FastChessWellIndices.WriteTo(fsm);

                    for(i = 0; i < WellsCount; ++i)
                    {
                        FastChessYearIndices[i].WriteTo(fsm);
                        for(j = 0; j < FastChess[i].Count; ++j)
                        {
                            FastChess[i][j].WriteTo(fsm);
                        }
                    }
                }
            }
            File.SetCreationTime(FastChessName, DateTime.Now);
            File.SetLastWriteTime(FastChessName, DateTime.Now);
        }

        public void LoadChessFastDeltaUpdates(BackgroundWorker worker, DoWorkEventArgs e, int CutOffDays)
        {
            int i, j, wellIndex;
            int chessInjLen = 18;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка delta обновлений шахматки";
            userState.Element = Name;
            DirectoryInfo dir = new DirectoryInfo(Path.Combine(ProjectPath, Path.Combine("cache", Name)));
            if(!File.Exists(Path.Combine(dir.FullName, "chess_fast.bin"))) return;
            FileInfo[] deltaFiles =  dir.GetFiles("chess_delta_*.bin");
            if(deltaFiles.Length == 0) return;

            int[] deltaCounts = new int[deltaFiles.Length];
            FileStream fs;
            FileStream cache = new FileStream(Path.Combine(dir.FullName, "chess_fast_cache.bin"), FileMode.Create);
            FileStream oldChess = new FileStream(Path.Combine(dir.FullName, "chess_fast.bin"), FileMode.Open);
            BinaryReader[] deltaBr = new BinaryReader[deltaFiles.Length];
            BinaryWriter bw = new BinaryWriter(cache);

            try
            {
                WellChess chess = null;
                List<int[]> WellIndices = new List<int[]>();
                var br = new BinaryReader(oldChess);
                DateTime minDate, maxDate, dt;
                minDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                maxDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());

                // read well indices
                int WellsCount = br.ReadInt32();

                for (i = 0; i < WellsCount; ++i)
                {
                    var WellIndexItem = new int[2];
                    WellIndexItem[0] = br.ReadInt32();   // well index
                    WellIndexItem[1] = br.ReadInt32();   // offset
                    WellIndices.Add(WellIndexItem);
                }
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    fs = new FileStream(deltaFiles[di].FullName, FileMode.Open);
                    deltaBr[di] = new BinaryReader(fs);
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (minDate > dt) minDate = dt;
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (maxDate < dt) maxDate = dt;
                    deltaCounts[di] = deltaBr[di].ReadInt32();
                }
                // min - max dates
                byte Day, Month;
                int count, wellPosition;
                int year, yearsCount, minYear;
                int lastPosition;
                List<int> YearsPosition = new List<int>();
                bw.Write(SmartPlusSystem.PackDateTime(minDate));
                bw.Write(SmartPlusSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(-1);                   // well offset
                }
                minDate = DateTime.MaxValue;
                ChessDailyItem item;
                List<List<ChessDailyItem>> chessItems = new List<List<ChessDailyItem>>();
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    wellPosition = (int)cache.Position;
                    minYear = 9999;
                    yearsCount = 0;
                    YearsPosition.Clear();
                    chessItems.Clear();
                    
                    if (WellIndices[wi][1] != -1)
                    {
                        br.BaseStream.Seek(WellIndices[wi][1], SeekOrigin.Begin);
                        yearsCount = br.ReadInt32();
                        minYear = br.ReadInt32();
                        for (i = 0; i < yearsCount; i++)
                        {
                            YearsPosition.Add(br.ReadInt32());
                            chessItems.Add(new List<ChessDailyItem>());
                        }
                        for (i = 0; i < yearsCount; i++)
                        {
                            if (YearsPosition[i] == -1) continue;
                            br.BaseStream.Seek(YearsPosition[i], SeekOrigin.Begin);
                            count = br.ReadInt32();
                            for (j = 0; j < count; j++)
                            {
                                Month = br.ReadByte();
                                Day = br.ReadByte();
                                item.Date = new DateTime(minYear + i, Month, Day);
                                item.Qliq = br.ReadSingle();
                                item.Qoil = br.ReadSingle();
                                item.QliqV = br.ReadSingle();
                                item.QoilV = br.ReadSingle();
                                item.StayTime = br.ReadByte();
                                item.Hdyn = br.ReadSingle();
                                item.StayReason = 0;
                                chessItems[i].Add(item);
                            }
                        }
                    }
                    for (int di = 0; di < deltaBr.Length; di++)
                    {
                        chess = new WellChess();
                        wellIndex = deltaBr[di].ReadInt32();
                        if (wellIndex == WellIndices[wi][0])
                        {
                            if(chess.ReadFromCache(deltaBr[di], 0) && chess.Count > 0)
                            {
                                if (minYear == 9999) minYear = chess[0].Date.Year;
                                for (i = 0; i < chess.Count; i++)
                                {
                                    year = chess[i].Date.Year - minYear;
                                    while (year >= chessItems.Count)
                                    {
                                        chessItems.Add(new List<ChessDailyItem>());
                                        yearsCount++;
                                    }
                                    chessItems[year].Add(chess[i]);
                                }
                            }
                            count = deltaBr[di].ReadInt32(); // skip chess inj
                            if (count > 0) deltaBr[di].BaseStream.Seek(count * chessInjLen, SeekOrigin.Current);
                        }
                    }

                    for (i = 0; i < chessItems.Count; i++)
                    {
                        if (chessItems[i].Count > 0 && chessItems[i][chessItems[i].Count - 1].Date < maxDate.AddDays(-CutOffDays))
                        {
                            chessItems[i].Clear();
                        }
                        else
                        {
                            for (j = 0; j < chessItems[i].Count; j++)
                            {
                                if (chessItems[i][j].Date > maxDate.AddDays(-CutOffDays))
                                {
                                    break;
                                }
                                if (chessItems[i][j].Date < maxDate.AddDays(-CutOffDays))
                                {
                                    chessItems[i].RemoveAt(j);
                                    j--;
                                }
                            }
                        }

                        if (i == 0 && chessItems[i].Count == 0)
                        {
                            chessItems.RemoveAt(i);
                            yearsCount--;
                            minYear++;
                            i--;
                        }
                    }

                    if(yearsCount > 0)
                    {
                        bw.Write(yearsCount);
                        bw.Write(minYear);
                        lastPosition = (int)bw.BaseStream.Position + chessItems.Count * sizeof(int);
                        for (i = 0; i < chessItems.Count; i++)
                        {
                            bw.Write(lastPosition);
                            lastPosition += sizeof(int) + chessItems[i].Count * (3 * sizeof(byte) + 5 * sizeof(float));
                        }
                        for (i = 0; i < chessItems.Count; i++)
                        {
                            bw.Write(chessItems[i].Count);
                            if (chessItems[i].Count > 0 && chessItems[i][0].Date < minDate)
                            {
                                minDate = chessItems[i][0].Date;
                            }
                            for (j = 0; j < chessItems[i].Count; j++)
                            {
                                Day = (byte)chessItems[i][j].Date.Day;
                                Month = (byte)chessItems[i][j].Date.Month;
                                bw.Write(Month);
                                bw.Write(Day);
                                bw.Write(chessItems[i][j].Qliq);
                                bw.Write(chessItems[i][j].Qoil);
                                bw.Write(chessItems[i][j].QliqV);
                                bw.Write(chessItems[i][j].QoilV);
                                bw.Write((byte)chessItems[i][j].StayTime);
                                bw.Write(chessItems[i][j].Hdyn);
                            }
                        }
                    }
                    lastPosition = (int)cache.Position;
                    WellIndices[wi][1] = (lastPosition != wellPosition) ? wellPosition : -1;
                }
                if (minDate == DateTime.MaxValue) minDate = maxDate.AddDays(-CutOffDays);
                cache.Seek(0, SeekOrigin.Begin);
                bw.Write(SmartPlusSystem.PackDateTime(minDate));
                bw.Write(SmartPlusSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(WellIndices[wi][1]);  // well offset
                }
                oldChess.Close();
                oldChess = null;
                cache.Flush();
                cache.Close();
                cache = null;
                if (File.Exists(Path.Combine(dir.FullName, "chess_fast_cache.bin")))
                {
                    if(File.Exists(Path.Combine(dir.FullName, "chess_fast.bin")))
                    {
                        File.Delete(Path.Combine(dir.FullName, "chess_fast.bin"));
                    }
                    File.Move(Path.Combine(dir.FullName, "chess_fast_cache.bin"), Path.Combine(dir.FullName, "chess_fast.bin"));
                }
            }
            catch
            {
                if (cache != null)
                {
                    cache.Close();
                    cache = null;
                }
            }
            finally
            {
                if (oldChess != null) oldChess.Close();
                if (cache != null) cache.Close();
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    if (deltaBr[di] != null) deltaBr[di].Close();
                }
                string name = Path.Combine(dir.FullName, "chess_fast_cache.bin");
                if (File.Exists(name)) File.Delete(name);
            }
        }
        public void LoadChessInjFastDeltaUpdates(BackgroundWorker worker, DoWorkEventArgs e, int CutOffDays)
        {
            int i, j, wellIndex;
            int chessLen = 29;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка delta обновлений шахматки";
            userState.Element = Name;
            DirectoryInfo dir = new DirectoryInfo(Path.Combine(ProjectPath, Path.Combine("cache", Name)));
            if (!File.Exists(Path.Combine(dir.FullName, "chessinj_fast.bin"))) return;
            FileInfo[] deltaFiles = dir.GetFiles("chess_delta_*.bin");
            if (deltaFiles.Length == 0) return;

            int[] deltaCounts = new int[deltaFiles.Length];
            FileStream fs;
            FileStream cache = new FileStream(Path.Combine(dir.FullName, "chessinj_fast_cache.bin"), FileMode.Create);
            FileStream oldChess = new FileStream(Path.Combine(dir.FullName, "chessinj_fast.bin"), FileMode.Open);
            BinaryReader[] deltaBr = new BinaryReader[deltaFiles.Length];
            BinaryWriter bw = new BinaryWriter(cache);

            try
            {
                WellChessInj chessInj = null;
                List<int[]> WellIndices = new List<int[]>();
                var br = new BinaryReader(oldChess);
                DateTime minDate, maxDate, dt;
                minDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                maxDate = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                int version = 0;
                // read well indices
                int WellsCount = br.ReadInt32();

                for (i = 0; i < WellsCount; ++i)
                {
                    var WellIndexItem = new int[2];
                    WellIndexItem[0] = br.ReadInt32();   // well index
                    WellIndexItem[1] = br.ReadInt32();   // offset
                    WellIndices.Add(WellIndexItem);
                }
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    fs = new FileStream(deltaFiles[di].FullName, FileMode.Open);
                    deltaBr[di] = new BinaryReader(fs);
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (minDate > dt) minDate = dt;
                    dt = DateTime.FromOADate(deltaBr[di].ReadDouble());
                    if (maxDate < dt) maxDate = dt;
                    deltaCounts[di] = deltaBr[di].ReadInt32();
                }
                // min - max dates
                byte Day, Month;
                int count, wellPosition;
                int year, yearsCount, minYear;
                int lastPosition;
                List<int> YearsPosition = new List<int>();
                bw.Write(SmartPlusSystem.PackDateTime(minDate));
                bw.Write(SmartPlusSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(-1);                   // well offset
                }
                minDate = DateTime.MaxValue;
                ChessInjDailyItem item;
                List<List<ChessInjDailyItem>> chessItems = new List<List<ChessInjDailyItem>>();
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    wellPosition = (int)cache.Position;
                    minYear = 9999;
                    yearsCount = 0;
                    YearsPosition.Clear();
                    chessItems.Clear();

                    if (WellIndices[wi][1] != -1)
                    {
                        br.BaseStream.Seek(WellIndices[wi][1], SeekOrigin.Begin);
                        yearsCount = br.ReadInt32();
                        minYear = br.ReadInt32();
                        for (i = 0; i < yearsCount; i++)
                        {
                            YearsPosition.Add(br.ReadInt32());
                            chessItems.Add(new List<ChessInjDailyItem>());
                        }
                        for (i = 0; i < yearsCount; i++)
                        {
                            if (YearsPosition[i] == -1) continue;
                            br.BaseStream.Seek(YearsPosition[i], SeekOrigin.Begin);
                            count = br.ReadInt32();
                            for (j = 0; j < count; j++)
                            {
                                Month = br.ReadByte();
                                Day = br.ReadByte();
                                item.Date = new DateTime(minYear + i, Month, Day);
                                item.Wwat = br.ReadSingle();
                                item.Pust = br.ReadSingle();
                                item.CycleTime = br.ReadByte();
                                item.StayTime = br.ReadByte();
                                item.StayReason = 0;
                                chessItems[i].Add(item);
                            }
                        }
                    }
                    for (int di = 0; di < deltaBr.Length; di++)
                    {
                        chessInj = new WellChessInj();
                        wellIndex = deltaBr[di].ReadInt32();
                        if (wellIndex == WellIndices[wi][0])
                        {
                            count = deltaBr[di].ReadInt32(); // skip chess inj
                            if (count > 0) deltaBr[di].BaseStream.Seek(count * chessLen, SeekOrigin.Current);
                            if (chessInj.ReadFromCache(deltaBr[di], version) && chessInj.Count > 0)
                            {
                                if (minYear == 9999) minYear = chessInj[0].Date.Year;
                                for (i = 0; i < chessInj.Count; i++)
                                {
                                    year = chessInj[i].Date.Year - minYear;
                                    while (year >= chessItems.Count)
                                    {
                                        chessItems.Add(new List<ChessInjDailyItem>());
                                        yearsCount++;
                                    }
                                    chessItems[year].Add(chessInj[i]);
                                }
                            }
                        }
                    }

                    for (i = 0; i < chessItems.Count; i++)
                    {
                        if (chessItems[i].Count > 0 && chessItems[i][chessItems[i].Count - 1].Date < maxDate.AddDays(-CutOffDays))
                        {
                            chessItems[i].Clear();
                        }
                        else
                        {
                            for (j = 0; j < chessItems[i].Count; j++)
                            {
                                if (chessItems[i][j].Date > maxDate.AddDays(-CutOffDays))
                                {
                                    break;
                                }
                                if (chessItems[i][j].Date < maxDate.AddDays(-CutOffDays))
                                {
                                    chessItems[i].RemoveAt(j);
                                    j--;
                                }
                            }
                        }

                        if (i == 0 && chessItems[i].Count == 0)
                        {
                            chessItems.RemoveAt(i);
                            yearsCount--;
                            minYear++;
                            i--;
                        }
                    }

                    if (yearsCount > 0)
                    {
                        bw.Write(yearsCount);
                        bw.Write(minYear);
                        lastPosition = (int)bw.BaseStream.Position + chessItems.Count * sizeof(int); // + positions 
                        for (i = 0; i < chessItems.Count; i++)
                        {
                            bw.Write(lastPosition);
                            lastPosition += sizeof(int) + chessItems[i].Count * (4 * sizeof(byte) + 2 * sizeof(float));
                        }
                        for (i = 0; i < chessItems.Count; i++)
                        {
                            bw.Write(chessItems[i].Count);
                            if (chessItems[i].Count > 0 && chessItems[i][0].Date < minDate)
                            {
                                minDate = chessItems[i][0].Date;
                            }
                            for (j = 0; j < chessItems[i].Count; j++)
                            {
                                Day = (byte)chessItems[i][j].Date.Day;
                                Month = (byte)chessItems[i][j].Date.Month;
                                bw.Write(Month);
                                bw.Write(Day);
                                bw.Write(chessItems[i][j].Wwat);
                                bw.Write(chessItems[i][j].Pust);
                                bw.Write((byte)chessItems[i][j].CycleTime);
                                bw.Write((byte)chessItems[i][j].StayTime);
                            }
                        }
                    }
                    lastPosition = (int)cache.Position;
                    WellIndices[wi][1] = (lastPosition != wellPosition) ? wellPosition : -1;
                }
                if (minDate == DateTime.MaxValue) minDate = maxDate.AddDays(-CutOffDays);
                cache.Seek(0, SeekOrigin.Begin);
                bw.Write(SmartPlusSystem.PackDateTime(minDate));
                bw.Write(SmartPlusSystem.PackDateTime(maxDate));
                bw.Write(WellIndices.Count);
                for (int wi = 0; wi < WellIndices.Count; wi++)
                {
                    bw.Write(WellIndices[wi][0]);   // wellindex
                    bw.Write(WellIndices[wi][1]);  // well offset
                }
                oldChess.Close();
                oldChess = null;
                cache.Flush();
                cache.Close();
                cache = null;
                if (File.Exists(Path.Combine(dir.FullName, "chessinj_fast_cache.bin")))
                {
                    if (File.Exists(Path.Combine(dir.FullName, "chessinj_fast.bin")))
                    {
                        File.Delete(Path.Combine(dir.FullName, "chessinj_fast.bin"));
                    }
                    File.Move(Path.Combine(dir.FullName, "chessinj_fast_cache.bin"), Path.Combine(dir.FullName, "chessinj_fast.bin"));
                }
            }
            catch
            {
                if (cache != null)
                {
                    cache.Close();
                    cache = null;
                }
            }
            finally
            {
                if (oldChess != null) oldChess.Close();
                if (cache != null) cache.Close();
                for (int di = 0; di < deltaFiles.Length; di++)
                {
                    if (deltaBr[di] != null)
                    {
                        deltaBr[di].Close();
                        if (deltaFiles[di].Exists) deltaFiles[di].Delete();
                    }
                }
                string name = Path.Combine(dir.FullName, "chessinj_fast_cache.bin");
                if (File.Exists(name)) File.Delete(name);
            }
        }

        public bool LoadChessFromOracleServer(BackgroundWorker worker, DoWorkEventArgs e, string Query)
        {
            bool retValue = false;
            // загрузка с оракла
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных Шахматки с сервера";
            userState.Element = Name;
            worker.ReportProgress(0, userState);
            string[] queryList = Query.Split(new char[] {';'});
            if ((Query.IndexOf("%OILFIELDCODE%") == -1) || (queryList.Length != 2))
            {
                userState.Error = "Неверный запрос к базе данных. Нет объекта %OILFIELDCODE%";
                worker.ReportProgress(0, userState);
                return false;
            }                     
            Query = Query.Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());
            if (queryList.Length == 2)
            {
                queryList[0] = queryList[0].Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());
                queryList[1] = queryList[1].Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());
            }
            string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                        ParamsDict.OraServerName + ")(PORT=" + ParamsDict.OraPort.ToString() +
                        ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + ParamsDict.OraSid + ")));" +
                        "USER ID=" + ParamsDict.OraLogin + ";PASSWORD=" + ParamsDict.OraPass + ";";
            using (OracleConnection conn = new OracleConnection(connString))
            {
                OracleCommand oraCommand = conn.CreateCommand();
                oraCommand.CommandText = queryList[0];

                OracleCommand oraCommandInj = conn.CreateCommand();
                oraCommandInj.CommandText = queryList[1];

                OracleDataReader oraReader;
                userState.WorkCurrentTitle = "Выполняется запрос к серверу Oracle...";
                worker.ReportProgress(0, userState);
                try
                {
                    int size_block = 1024 * 1024;
                    MemoryStream ms = new MemoryStream(size_block);
                    BinaryWriter bw = new BinaryWriter(ms);
                    int i = 0, iLen = Wells.Count;
                    userState.WorkCurrentTitle = "Чтение данных с сервера Oracle...";

                    #region Чтение добывающей шахматки
                    conn.Open();
                    oraReader = oraCommand.ExecuteReader();

                    bool res = oraReader.Read(), res2;
                    worker.ReportProgress(0, userState);

                    int j, well_index, well_id, len_days;
                    int areaCode, boreIndex, lastBoreIndex, count, boreId;
                    string well_name;
                    DateTime dt, predDt;
                    ChessDailyItem item;
                    float Qliq, Qoil, Qwat;
                    float QliqVnr, QoilVnr;
                    float QliqV, QwatV, QoilV, level;
                    int stayTime, level_type, stayReason;
                    int vnr;

                    double predQliq, predQoil, predStayTime;
                    Well w;
                    while (res)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            retValue = false;
                            break;
                        }
                        else
                        {
                            well_id = Convert.ToInt32(oraReader[0]);
                            well_name = ((string)oraReader[3]).ToUpper();
                            areaCode = Convert.ToInt32(oraReader[4]);
                            boreId = Convert.ToInt32(oraReader[2]);
                            well_name = well_name.Trim();
                            //well_index = GetWellIndex(well_id);
                            well_index = GetWellIndex(well_name, areaCode);
                            if (well_index == -1)
                            {
                                userState.Error = "Не найдена скважина " + well_name;
                                ReportProgress(worker, i, iLen, userState);
                                while ((res) && (Convert.ToInt32(oraReader[0]) == well_id)) res = oraReader.Read();
                                continue;
                            }
                            w = Wells[well_index];
                            lastBoreIndex = -1;
                            ms.SetLength(0);
                            len_days = 0;
                            predQliq = -1;
                            predQoil = -1;
                            predStayTime = -1;
                            predDt = DateTime.MinValue;
                            res2 = res;

                            while (res2)
                            {
                                if (Convert.ToInt32(oraReader[0]) != well_id) break;
                                
                                boreId = Convert.ToInt32(oraReader[2]);
                                dt = Convert.ToDateTime(oraReader[5]);
                                dt = new DateTime(dt.Year, dt.Month, dt.Day);
                                boreIndex = Convert.ToInt32(oraReader[1]);
                                if (boreIndex > lastBoreIndex) lastBoreIndex = boreIndex;

                                if ((lastBoreIndex == boreIndex) && (dt <= DateTime.Now.AddDays(1)) && (predDt < dt))
                                {
                                    Qoil = Convert.ToSingle(oraReader[7]);
                                    Qwat = Convert.ToSingle(oraReader[8]);
                                    Qliq = Qoil + Qwat;
                                    QwatV = Convert.ToSingle(oraReader[11]);
                                    QoilV = Convert.ToSingle(oraReader[12]);
                                    QliqV = QwatV + QoilV;

                                    stayTime = Convert.ToInt32(oraReader[9]);
                                    level = Convert.ToSingle(oraReader[16]);
                                    level_type = Convert.ToInt32(oraReader[17]);

                                    vnr = Convert.ToInt32(oraReader[13]);
                                    QliqVnr = Convert.ToSingle(oraReader[14]);
                                    QoilVnr = Convert.ToSingle(oraReader[15]);

                                    stayReason = Convert.ToInt32(oraReader[18]);
                                    if (vnr == 1)
                                    {
                                        Qliq = 0;
                                        Qoil = QoilVnr;
                                        QoilV = 0;
                                        QliqV = QliqVnr;
                                    }

                                    if ((predQliq != -1) && (Qliq + Qoil + Qwat + stayReason > 0)) predQliq = -1;

                                    if ((predQliq == -1) || (dt == predDt))
                                    {
                                        len_days++;
                                        if ((predQliq != -1) && (dt == predDt) && (predQliq == Qliq) && (predQoil == Qoil) && (predStayTime == stayTime))
                                        {
                                            Qliq = 0;
                                            Qoil = 0;
                                            Qwat = 0;
                                            QliqV = 0;
                                            QoilV = 0;
                                            QwatV = 0;
                                            QliqVnr = 0;
                                            QoilVnr = 0;
                                            vnr = 0;
                                            stayTime = 0;
                                            stayReason = -1;
                                        }
                                        bw.Write(boreIndex);
                                        bw.Write(SmartPlusSystem.PackDateTime(dt));
                                        bw.Write(Qliq);
                                        bw.Write(Qoil);
                                        bw.Write(QliqV);
                                        bw.Write(QoilV);
                                        bw.Write(stayTime);
                                        bw.Write(level);
                                        bw.Write(level_type);
                                        bw.Write(stayReason);
                                    }

                                    predDt = dt;
                                    if (Qliq + Qoil + Qwat == 0)
                                    {
                                        predQliq = Qliq;
                                        predQoil = Qoil;
                                        predStayTime = stayTime;
                                    }
                                }
                                res2 = oraReader.Read();
                            }
                            res = res2;
                            BinaryReader br = new BinaryReader(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            count = (w.SideBores == null) ? 1 : w.SideBores.Length + 1;
                            List<ChessDailyItem>[] boreItems = new List<ChessDailyItem>[count];
                            for (j = 0; j < len_days; j++)
                            {
                                boreIndex = br.ReadInt32();

                                item.Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                Qliq = br.ReadSingle();
                                Qoil = br.ReadSingle();
                                QliqV = br.ReadSingle();
                                QoilV = br.ReadSingle();
                                item.StayTime = br.ReadInt32();
                                level = br.ReadSingle();
                                level_type = br.ReadInt32();
                                stayReason = br.ReadInt32();
                                item.Hdyn = (level_type == 1) ? Convert.ToSingle(level) : 0;
                                item.Qliq = Qliq;
                                item.Qoil = Qoil;
                                item.QliqV = QliqV;
                                item.QoilV = QoilV;
                                item.StayReason = stayReason;
                                if (boreIndex < boreItems.Length)
                                {
                                    if (boreItems[boreIndex] == null) boreItems[boreIndex] = new List<ChessDailyItem>();
                                    boreItems[boreIndex].Add(item);
                                }
                            }
                            if (boreItems[0] != null)
                            {
                                w.Chess = new WellChess(boreItems[0].ToArray());
                                if (!ChessLoaded) ChessLoaded = w.Chess.Count > 0;
                            }
                            // Раскидываем по вторым стволам
                            for (boreIndex = 1; boreIndex < count; boreIndex++)
                            {
                                if (boreItems[boreIndex] != null)
                                {
                                    w.SideBores[boreIndex - 1].Chess = new WellChess(boreItems[boreIndex].ToArray());
                                    if (!ChessLoaded) ChessLoaded = w.SideBores[boreIndex - 1].Chess.Count > 0;
                                }
                            }
                            retValue = true;
                            i++;
                            ReportProgress(worker, i, iLen);
                        }
                    }
                    #endregion

                    #region Чтение нагнетательной шахматки

                    // не грузим закачку по газовым закачивающим скважинам
                    if ((!e.Cancel) &&
                        (ParamsDict.FieldCode != 149) &&
                        (ParamsDict.FieldCode != 189) &&
                        (ParamsDict.FieldCode != 45))
                    {
                        oraReader = oraCommandInj.ExecuteReader();
                        res = oraReader.Read();
                        ChessInjDailyItem injItem;

                        float Wwat, predWwat, WwatItog, Pust;
                        int downTime;
                        while (res)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                retValue = false;
                                break;
                            }
                            else
                            {
                                well_id = Convert.ToInt32(oraReader[0]);
                                well_name = Convert.ToString(oraReader[3]).ToUpper();
                                areaCode = Convert.ToInt32(oraReader[4]);
                                well_name = well_name.Trim();
                                well_index = GetWellIndex(well_name, areaCode);
                                if (well_index == -1)
                                {
                                    userState.Error = "Не найдена нагнетательная скважина " + well_name;
                                    ReportProgress(worker, i, iLen, userState);
                                    while ((res) && (Convert.ToInt32(oraReader[0]) == well_id)) res = oraReader.Read();
                                    continue;
                                }
                                w = Wells[well_index];
                                lastBoreIndex = -1;
                                ms.SetLength(0);
                                predWwat = -1;
                                len_days = 0;
                                res2 = res;
                                while (res2)
                                {
                                    if (Convert.ToInt32(oraReader[0]) != well_id) break;

                                    dt = Convert.ToDateTime(oraReader[5]);
                                    dt = new DateTime(dt.Year, dt.Month, dt.Day);
                                    boreIndex = Convert.ToInt32(oraReader[1]);
                                    if (boreIndex > lastBoreIndex) lastBoreIndex = boreIndex;
                                    if ((lastBoreIndex == boreIndex) && (dt <= DateTime.Now.AddDays(1)))
                                    {
                                        Wwat = Convert.ToSingle(oraReader[6]);
                                        downTime = Convert.ToInt32(oraReader[7]);
                                        Pust = Convert.ToSingle(oraReader[8]);
                                        WwatItog = Convert.ToSingle(oraReader[9]);
                                        if ((WwatItog != -1) && (Wwat != -1) && (Wwat > WwatItog * 3))
                                        {
                                            Wwat = WwatItog;
                                        }
                                        stayReason = Convert.ToInt32(oraReader[10]);

                                        if ((predWwat != Wwat && Wwat > -1) || Pust > 0 || stayReason > 0)
                                        {
                                            len_days++;
                                            bw.Write(boreIndex);
                                            bw.Write(SmartPlusSystem.PackDateTime(dt));
                                            if ((Wwat < 0) && (Pust > 0))
                                            {
                                                Wwat = predWwat;
                                            }
                                            bw.Write(Wwat);
                                            bw.Write(downTime);
                                            bw.Write(Pust);
                                            bw.Write(stayReason);
                                            predWwat = Wwat;
                                        }
                                    }
                                    res2 = oraReader.Read();
                                }

                                res = res2;
                                BinaryReader br = new BinaryReader(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                ChessInjDailyItem[] dayItems = new ChessInjDailyItem[len_days];
                                count = (w.SideBores == null) ? 1 : w.SideBores.Length + 1;
                                List<ChessInjDailyItem>[] boreInjItems = new List<ChessInjDailyItem>[count];

                                for (j = 0; j < len_days; j++)
                                {
                                    boreIndex = br.ReadInt32();
                                    injItem.Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
                                    injItem.Wwat = br.ReadSingle();
                                    injItem.StayTime = br.ReadInt32();
                                    injItem.Pust = br.ReadSingle();
                                    injItem.StayReason = br.ReadInt32();
                                    injItem.CycleTime = 0;
                                    if (boreInjItems[boreIndex] == null) boreInjItems[boreIndex] = new List<ChessInjDailyItem>();
                                    boreInjItems[boreIndex].Add(injItem);
                                }
                                if (boreInjItems[0] != null)
                                {
                                    w.ChessInj = new WellChessInj(boreInjItems[0].ToArray());
                                    if (!ChessLoaded) ChessLoaded = w.ChessInj.Count > 0;
                                }

                                // Раскидываем по вторым стволам
                                for (boreIndex = 1; boreIndex < count; boreIndex++)
                                {
                                    if (boreInjItems[boreIndex] != null)
                                    {
                                        w.SideBores[boreIndex - 1].ChessInj = new WellChessInj(boreInjItems[boreIndex].ToArray());
                                        if (!ChessLoaded) ChessLoaded = w.SideBores[boreIndex - 1].ChessInj.Count > 0;
                                    }
                                }
                                retValue = true;
                                i++;
                                ReportProgress(worker, i, iLen);
                            }
                        }
                    }
                    #endregion

                    oraReader.Close();
                }
#if !DEBUG
                catch (Exception ex)
                {
                    userState.Error = "Ошибка загрузки с сервера Oracle!" + ex.Message;
                    worker.ReportProgress(0, userState);
                    return false;
                }
#endif
                finally
                {
                    userState.WorkCurrentTitle = "Данные с сервера Oracle получены";
                    worker.ReportProgress(0, userState);
                }
            }
            return retValue;
        }
        public bool LoadChessFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных Шахматки месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\chess.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, well_count, well_index = -1, iLen, wellCount = 0;
                    //int lenDailyItem = 25; // 68 - old
                    //int lenDailyInjItem = 14; // 44 - old
                    Well w;
                    WellChess chess;
                    WellChessInj chessInj;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;

                    FileStream fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (WellChess.Version == version)
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            well_count = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            unpackMS.Seek(4 * well_count, SeekOrigin.Begin);
                            for (i = 0; i < well_count; i++)
                            {
                                well_index = br.ReadInt32();
                                w = Wells[well_index];
                                userState.Element = w.Name;

                                // Шахматка добывающих
                                chess = new WellChess();
                                if (chess.ReadFromCache(br, version))
                                {
                                    w.Chess = chess;
                                    ChessLoaded = true;
                                }

                                // Шахм нагнетатальных
                                chessInj = new WellChessInj();
                                if (chessInj.ReadFromCache(br, version))
                                {
                                    w.ChessInj = chessInj;
                                    ChessLoaded = true;
                                }

                                retValue = true;
                                ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                            wellCount += well_count;
                        }
                    }
                    bfr.Close();
                }
            }
            return retValue;
        }
        public bool LoadChessFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\chess.bin";
            if (File.Exists(cacheName))
            {
                int j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                WellChess chess;
                WellChessInj chessInj;

                FileStream fs = null;
                MemoryStream ms, unpackMS;
                BinaryReader br;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (WellChess.Version == version)
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();
                        wellCount = 0;
                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            wellBlockCount = bfr.ReadInt32();
                            if (WellIndex < wellCount + wellBlockCount)
                            {
                                ms = GetBuffer(true);
                                unpackMS = GetBuffer(false);
                                ms.SetLength(len_block);
                                br = new BinaryReader(unpackMS);
                                ConvertEx.CopyStream(fs, ms, len_block);
                                ConvertEx.UnPackStream(ms, ref unpackMS);

                                shift = (WellIndex - wellCount) * 4;
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                shift = br.ReadInt32();
                                unpackMS.Seek(shift, SeekOrigin.Begin);
                                well_index = br.ReadInt32();

                                if (WellIndex != well_index)
                                {
                                    retValue = false;
                                }
                                else if (well_index > -1)
                                {
                                    w = Wells[well_index];

                                    // Шахматка добывающих
                                    chess = new WellChess();
                                    if (chess.ReadFromCache(br, version))
                                    {
                                        w.Chess = chess;
                                        ChessLoaded = true;
                                    }

                                    // Шахматка нагнетатальных
                                    chessInj = new WellChessInj();
                                    if (chessInj.ReadFromCache(br, version))
                                    {
                                        w.ChessInj = chessInj;
                                        ChessLoaded = true;
                                    }
                                }
                                break;
                            }
                            else
                            {
                                fs.Seek(len_block, SeekOrigin.Current);
                            }
                            wellCount += wellBlockCount;
                        }
                    }
                }
                catch (Exception)
                {
                    retValue = false;
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
            }
            return retValue;
        }
        public bool LoadChessFromCache_fast(int WellIndex)
        {
            string CacheName = Path.Combine(ProjectPath, Path.Combine("cache", Path.Combine(Name, "chess_fast.bin")));
            FileStream CacheFile = null;

            if (File.Exists(CacheName))
            {
                CacheFile = new FileStream(CacheName, FileMode.Open, FileAccess.Read);
                using (var br = new BinaryReader(CacheFile))
                {
                    // skip dates
                    br.BaseStream.Seek(sizeof(int) * 2, SeekOrigin.Current);

                    // read well indices
                    int WellsCount = br.ReadInt32();
                    int Offset = -1;
                    for (int i = 0; i < WellsCount; ++i)
                    {
                        int[] WellIndexItem = new int[2];
                        WellIndexItem[0] = br.ReadInt32();
                        WellIndexItem[1] = br.ReadInt32();

                        if (WellIndexItem[0] == WellIndex)
                        {
                            Offset = WellIndexItem[1];
                            break;
                        }
                    }

                    if (Offset != -1)
                    {
                        List<ChessDailyItem> Chess = new List<ChessDailyItem>();

                        // read year index
                        br.BaseStream.Seek(Offset, SeekOrigin.Begin);
                        int YearsCount = br.ReadInt32();
                        int MinYear = br.ReadInt32();
                        int MaxYear = MinYear + YearsCount - 1;

                        var EmptyYears = new List<int>();
                        for (int i = MinYear; i <= MaxYear; ++i)
                        {
                            int NextOffset = br.ReadInt32();
                            if (NextOffset == -1)
                            {
                                EmptyYears.Add(i);
                            }

                            if (i == MinYear)
                            {
                                Offset = NextOffset;
                            }
                        }

                        for (int i = MinYear; i <= MaxYear; ++i)
                        {
                            if (EmptyYears.IndexOf(i) != -1)
                            {
                                continue;
                            }

                            int ChessCount = br.ReadInt32();
                            for (int j = 0; j < ChessCount; ++j)
                            {
                                byte Month = br.ReadByte();
                                byte Day = br.ReadByte();
                                var d = new DateTime(i, Month, Day);

                                var Item = new ChessDailyItem();
                                Item.Date = d;
                                Item.Qliq = br.ReadSingle();
                                Item.Qoil = br.ReadSingle();
                                Item.QliqV = br.ReadSingle();
                                Item.QoilV = br.ReadSingle();
                                Item.StayTime = br.ReadByte();
                                Item.Hdyn = br.ReadSingle();

                                if (d.Year > 1920)
                                {
                                    Chess.Add(Item);
                                }
                            }
                        }

                        Wells[WellIndex].Chess = new WellChess(Chess.ToArray());
                    }
                }
            }
            CacheName = Path.Combine(ProjectPath, Path.Combine("cache", Path.Combine(Name, "chessinj_fast.bin")));
            if (!File.Exists(CacheName))
            {
                return false;
            }

            CacheFile = new FileStream(CacheName, FileMode.Open, FileAccess.Read);
            using (var br = new BinaryReader(CacheFile))
            {
                // skip dates
                br.BaseStream.Seek(sizeof(int) * 2, SeekOrigin.Current);

                // read well indices
                int WellsCount = br.ReadInt32();
                int Offset = -1;
                for (int i = 0; i < WellsCount; ++i)
                {
                    int[] WellIndexItem = new int[2];
                    WellIndexItem[0] = br.ReadInt32();
                    WellIndexItem[1] = br.ReadInt32();

                    if (WellIndexItem[0] == WellIndex)
                    {
                        Offset = WellIndexItem[1];
                        break;
                    }
                }

                if (Offset == -1)
                {
                    return true;
                }

                List<ChessInjDailyItem> ChessInj = new List<ChessInjDailyItem>();

                // read year index
                br.BaseStream.Seek(Offset, SeekOrigin.Begin);
                int YearsCount = br.ReadInt32();
                int MinYear = br.ReadInt32();
                int MaxYear = MinYear + YearsCount - 1;

                var EmptyYears = new List<int>();
                for (int i = MinYear; i <= MaxYear; ++i)
                {
                    int NextOffset = br.ReadInt32();
                    if (NextOffset == -1)
                    {
                        EmptyYears.Add(i);
                    }

                    if (i == MinYear)
                    {
                        Offset = NextOffset;
                    }
                }

                // go to first year
                br.BaseStream.Seek(Offset, SeekOrigin.Begin);
                for (int i = MinYear; i <= MaxYear; ++i)
                {
                    if (EmptyYears.IndexOf(i) != -1)
                    {
                        continue;
                    }

                    int ChessCount = br.ReadInt32();
                    for (int j = 0; j < ChessCount; ++j)
                    {
                        byte Month = br.ReadByte();
                        byte Day = br.ReadByte();
                        var d = new DateTime(i, Month, Day);

                        var Item = new ChessInjDailyItem();
                        Item.Date = d;
                        Item.Wwat = br.ReadSingle();
                        Item.Pust = br.ReadSingle();
                        Item.CycleTime = br.ReadByte();
                        Item.StayTime = br.ReadByte();

                        if (d.Year > 1920)
                        {
                            ChessInj.Add(Item);
                        }
                    }
                }

                Wells[WellIndex].ChessInj = new WellChessInj(ChessInj.ToArray());
            }

            return true;
        }
        public bool WriteChessToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных Шахматки в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
                return false;
            }

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\chess.bin";
            int MAX_SIZE_PACK_BLOCK = 1 * 512 * 1024;
            Well w;
            List<MemoryStream> msArr = new List<MemoryStream>();
            List<int> wellCount = new List<int>(5);
            List<int> unpackSizes = new List<int>(5);
            List<int> WellsShift = new List<int>();

            MemoryStream packedMS;
            FileStream fs = null;
            MemoryStream ms;
            BinaryWriter bw;
            int i, j, k, h, len_all_wells, wCount = 0, len_block;
            int lenDailyItem = 29, lenDailyInjItem = 18;
            int len_shift;
            int iLen = Wells.Count;
            int count;
            if (iLen > 0)
            {
                len_all_wells = 0;
                for (i = 0; i < iLen; i++)
                {
                    w = Wells[i];
                    len_all_wells += 8;
                    count = w.ChessLoaded ? w.Chess.Count : 0;
                    len_all_wells += 4 + count * lenDailyItem;
                    count = w.ChessInjLoaded ? w.ChessInj.Count : 0;
                    len_all_wells += 4 + count * lenDailyInjItem;
                }

                if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                ms = new MemoryStream(len_all_wells);
                bw = new BinaryWriter(ms);

                worker.ReportProgress(0, userState);
                fs = new FileStream(cacheName, FileMode.Create);

                len_block = 0;
                k = 0;
                wCount = 0;
                for (i = 0; i <= iLen; i++)
                {
                    len_shift = 0;
                    if (i < iLen)
                    {
                        w = Wells[i];
                        len_shift = 8;
                        count = w.ChessLoaded ? w.Chess.Count : 0;
                        len_shift += 4 + count * lenDailyItem;
                        
                        count = w.ChessInjLoaded ? w.ChessInj.Count : 0;
                        len_shift += 4 + count * lenDailyInjItem;
                    }

                    if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                    {
                        wCount++;
                        len_block += len_shift;
                    }
                    else
                    {
                        len_shift = 4 * wCount;
                        bw.Write(len_shift);
                        for (j = k; j < i - 1; j++)
                        {
                            w = Wells[j];
                            len_shift += 4;
                            count = w.ChessLoaded ? w.Chess.Count : 0;
                            len_shift += 4 + count * lenDailyItem;

                            count = w.ChessInjLoaded ? w.ChessInj.Count : 0;
                            len_shift += 4 + count * lenDailyInjItem;
                            bw.Write(len_shift);
                        }

                        for (j = k; j < i; j++)
                        {
                            w = Wells[j];
                            bw.Write(w.Index);

                            if (w.ChessLoaded) w.Chess.WriteToCache(bw); else bw.Write(0);

                            if (w.ChessInjLoaded) w.ChessInj.WriteToCache(bw); else bw.Write(0);
                        }
                        if (i < iLen)
                        {
                            unpackSizes.Add((int)ms.Length);
                            packedMS = ConvertEx.PackStream(ms);
                            msArr.Add(packedMS);
                            wellCount.Add(wCount);
                            WellsShift.Clear();

                            ms.SetLength(0);

                            GC.Collect();
                            k = i;

                            w = Wells[i];
                            len_shift = 8;

                            count = w.ChessLoaded ? w.Chess.Count : 0;
                            len_shift += 4 + count * lenDailyItem;

                            count = w.ChessInjLoaded ? w.ChessInj.Count : 0;
                            len_shift += 4 + count * lenDailyInjItem;

                            len_block = len_shift;
                            wCount = 1;
                        }
                    }
                    retValue = true;
                    ReportProgress(worker, i, iLen);
                }
                unpackSizes.Add((int)ms.Length);
                packedMS = ConvertEx.PackStream(ms);
                msArr.Add(packedMS);
                wellCount.Add(wCount);
                bw.Close();

                bw = new BinaryWriter(fs);
                bw.Write(WellChess.Version);
                bw.Write(msArr.Count);
                bw.Write(iLen);
                for (i = 0; i < msArr.Count; i++)
                {
                    packedMS = (MemoryStream)msArr[i];
                    bw.Write((int)packedMS.Length);
                    bw.Write((int)unpackSizes[i]);
                    bw.Write((int)wellCount[i]);
                    packedMS.Seek(0, SeekOrigin.Begin);
                    packedMS.WriteTo(fs);
                    packedMS.Dispose();
                }
                packedMS = null;
                msArr.Clear();
                wellCount.Clear();
                unpackSizes.Clear();
                bw.Close();
                GC.Collect();
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
            }
            if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            return retValue;
        }
        public void ClearChessData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if (w.ChessLoaded)
                    {
                        if (w.Chess != null) w.Chess.Clear();
                        w.Chess = null;
                    }
                    if (w.ChessInjLoaded)
                    {
                        if (w.ChessInj != null) w.ChessInj.Clear();
                        w.ChessInj = null;
                    }
                }
                if (useGC) GC.Collect();
            }
            ChessLoaded = false;
        }
        #endregion

        #region CORE
        public bool LoadCoreFromServer(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return false;
        }
        public bool LoadCoreFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadPerfFromCache(worker, e, false);
        }
        public bool LoadCoreFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных керна месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\core.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, iLen, well_index, core_count;
                    long shift;
                    long[] shiftList;
                    Well w;

                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);

                        BinaryReader bfr = new BinaryReader(fs);
                        if (SkvCore.VERSION == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearCoreData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                core_count = bfr.ReadInt32();

                                if ((well_index > -1) && (core_count > 0))
                                {
                                    w = Wells[well_index];
                                    if (!NotLoadData)
                                    {
                                        if (w.Core == null) w.Core = new SkvCore();
                                        SkvCoreItem[] items = new SkvCoreItem[core_count];
                                        for (j = 0; j < core_count; j++)
                                        {
                                            items[j] = w.Core.RetrieveItem(bfr);
                                        }
                                        w.Core.SetItems(items);
                                    }
                                }
                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша керна!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadCoreFromCache(int WellIndex)
        {
            return LoadCoreFromCache(WellIndex, false);
        }
        public bool LoadCoreFromCache(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\core.bin";
            if (File.Exists(cacheName))
            {
                int i, core_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    BinaryReader bfr = new BinaryReader(fs);
                    if (SkvCore.VERSION == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                            //MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if (well_index > -1)
                        {
                            w = Wells[well_index];
                            core_count = bfr.ReadInt32();
                            if ((!NotLoadData) && (core_count > 0))
                            {
                                if (w.Core == null) w.Core = new SkvCore();
                                SkvCoreItem[] items = new SkvCoreItem[core_count];

                                for (i = 0; i < core_count; i++)
                                {
                                    items[i] = w.Core.RetrieveItem(bfr);
                                }
                                w.Core.SetItems(items);
                            }
                            retValue = true;
                        }
                    }
                    bfr.Close();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteCoreToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных керна в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\core.bin";
                Well w;
                FileStream fs;
                BinaryWriter bfw;
                int i, j, core_len;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    bfw = new BinaryWriter(fs);
                    bfw.Write(SkvCore.VERSION);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current);
                    long[] shiftList = new long[iLen];

                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        shiftList[i] = fs.Position;
                        bfw.Write(w.Index);
                        if (w.CoreLoaded)
                        {
                            core_len = w.Core.Count;
                            bfw.Write(core_len);
                            for (j = 0; j < core_len; j++)
                            {
                                w.Core.RetainItem(bfw, j);
                            }
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftList.Length > 0)
                    {
                        fs.Seek(4, SeekOrigin.Begin);
                        for (i = 0; i < shiftList.Length; i++)
                        {
                            bfw.Write(shiftList[i]);
                        }
                    }
                    bfw.Close();
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearCoreData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if (w.CoreLoaded) w.Core = null;
                }
                if (useGC) GC.Collect(GC.MaxGeneration);
            }
        }
        #endregion

        #region CORE TEST
        public bool LoadCoreTestFromServer(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return false;
        }
        public bool LoadCoreTestFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadCoreTestFromCache(worker, e, false);
        }
        public bool LoadCoreTestFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotLoadData)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных исследований керна месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\core_test.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, pack_len, core_test_count, well_index = -1, iLen;
                    long shift;
                    long[] shiftList;
                    Well w;
                    int maxPackLen = 0;
                    worker.ReportProgress(0, userState);
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open); ;
                        MemoryStream ms, unpackMS;
                        BinaryReader br, bfr = new BinaryReader(fs);
                        if (SkvCoreTest.VERSION == bfr.ReadInt32())
                        {
                            iLen = bfr.ReadInt32();
                            shiftList = new long[iLen];
                            for (i = 0; i < iLen; i++)
                            {
                                shiftList[i] = bfr.ReadInt64();
                            }

                            for (i = 0; i < iLen; i++)
                            {
                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    ClearCoreTestData(false);
                                    return false;
                                }
                                shift = shiftList[i] - fs.Position;
                                if (shift > 0) fs.Seek(shift, SeekOrigin.Current);
                                well_index = bfr.ReadInt32();
                                pack_len = bfr.ReadInt32();
                                if (maxPackLen < pack_len) maxPackLen = pack_len;
                                if ((well_index > -1) && (pack_len > 0))
                                {
                                    w = Wells[well_index];
                                    ms = GetBuffer(true);
                                    unpackMS = GetBuffer(false);
                                    ms.SetLength(pack_len);
                                    br = new BinaryReader(unpackMS);
                                    ConvertEx.CopyStream(fs, ms, pack_len);

                                    ConvertEx.UnPackStream(ms, ref unpackMS);
                                    core_test_count = br.ReadInt32();
                                    if ((!NotLoadData) && (core_test_count > 0))
                                    {
                                        if (w.CoreTest == null) w.CoreTest = new SkvCoreTest();
                                        SkvCoreTestItem[] items = new SkvCoreTestItem[core_test_count];

                                        for (j = 0; j < core_test_count; j++)
                                        {
                                            items[j] = w.CoreTest.RetrieveItem(br);
                                        }
                                        w.CoreTest.SetItems(items);
                                    }
                                }
                                retValue = true;
                                ReportProgress(worker, i + 1, iLen, userState);
                            }
                            if (iLen == 0) worker.ReportProgress(100, userState);
                        }
                        bfr.Close();
                        fs = null;
                    }
                    catch (Exception)
                    {
                        if (fs != null) fs.Close();
                        return false;
                    }
                }
            }
            else
            {
                userState.Error = "Не найден файл кеша исследований керна!";
                worker.ReportProgress(100, userState);
            }
            return retValue;
        }
        public bool LoadCoreTestFromCache(int WellIndex)
        {
            return LoadCoreTestFromCache(WellIndex, false);
        }
        public bool LoadCoreTestFromCache(int WellIndex, bool NotLoadData)
        {
            bool retValue = false;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\core_test.bin";
            if (File.Exists(cacheName))
            {
                int j,pack_len, core_test_count, well_index = -1, iLen;
                long shift;
                Well w;
                FileStream fs = null;
                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);

                    MemoryStream ms, unpackMS;

                    BinaryReader br, bfr = new BinaryReader(fs);
                    if (SkvCoreTest.VERSION == bfr.ReadInt32())
                    {
                        iLen = bfr.ReadInt32();
                        fs.Seek(8 * WellIndex, SeekOrigin.Current);
                        shift = bfr.ReadInt64();
                        fs.Seek(shift, SeekOrigin.Begin);
                        well_index = bfr.ReadInt32();
                        pack_len = bfr.ReadInt32();
                        if (WellIndex != well_index)
                        {
                            retValue = false;
                            //MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if ((well_index > -1) && (pack_len > 0))
                        {
                            w = Wells[well_index];
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(pack_len);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, pack_len);
                            ConvertEx.UnPackStream(ms, ref unpackMS);
                            core_test_count = br.ReadInt32();
                            if ((!NotLoadData) && (core_test_count > 0))
                            {
                                if (w.CoreTest == null) w.CoreTest = new SkvCoreTest();
                                SkvCoreTestItem[] items = new SkvCoreTestItem[core_test_count];

                                for (j = 0; j < core_test_count; j++)
                                {
                                    items[j] = w.CoreTest.RetrieveItem(br);
                                }
                                w.CoreTest.SetItems(items);
                            }
                        }
                    }
                    bfr.Close();
                    fs = null;
                }
                catch (Exception)
                {
                    if (fs != null) fs.Close();
                    return false;
                }
            }
            return retValue;
        }
        public bool WriteCoreTestToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных исследований керна в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                int i, j;
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\core_test.bin";
                Well w;
                FileStream fs;
                int core_test_count, iLen = Wells.Count;

                if (iLen > 0)
                {
                    long[] shiftWells = new long[iLen];
                    MemoryStream ms = GetBuffer(true);
                    MemoryStream packedMS;
                    BinaryWriter bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    fs = new FileStream(cacheName, FileMode.Create);
                    BinaryWriter bfw = new BinaryWriter(fs);
                    bfw.Write(SkvCoreTest.VERSION);
                    bfw.Write(iLen);
                    fs.Seek(8 * iLen, SeekOrigin.Current); // сдвигаемся на начало данных
                    shiftWells[0] = fs.Position;

                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        ClearMemoryStream(ms);
                        if (i > 0)
                        {
                            shiftWells[i] = fs.Position;
                        }
                        bfw.Write(w.Index);

                        if (w.CoreTestLoaded)
                        {
                            core_test_count = w.CoreTest.Count;
                            bw.Write(core_test_count);
                            for (j = 0; j < core_test_count; j++)
                            {
                                w.CoreTest.RetainItem(bw, j);
                            }
                            w.CoreTest = null;
                            packedMS = ConvertEx.PackStream(ms);
                            bfw.Write((int)packedMS.Length);
                            packedMS.Seek(0, SeekOrigin.Begin);
                            packedMS.WriteTo(fs);
                        }
                        else
                        {
                            bfw.Write(0);
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }
                    if (shiftWells.Length > 0)
                    {
                        fs.Seek(4, SeekOrigin.Begin);
                        for (i = 0; i < shiftWells.Length; i++)
                        {
                            bfw.Write(shiftWells[i]);
                        }
                    }
                    bfw.Close();
                    bw = null;
                    shiftWells = null;
                    File.SetCreationTime(cacheName, DateTime.Now);
                    File.SetLastWriteTime(cacheName, DateTime.Now);
                }
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearCoreTestData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if (w.CoreTestLoaded)
                    {
                        w.CoreTest = null;
                    }
                }
            }
            if (useGC) GC.GetTotalMemory(true);
        }
        #endregion

        #region GTM
        public bool LoadGTMFromServer(BackgroundWorker worker, DoWorkEventArgs e, string Query)
        {
            bool retValue = false;
            // загрузка с оракла
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГТМ с сервера";
            userState.Element = Name;
            worker.ReportProgress(0, userState);
            if (Query.IndexOf("%OILFIELDCODE%") == -1)
            {
                userState.Error = "Неверный запрос к базе данных. Нет объекта %OILFIELDCODE%";
                worker.ReportProgress(0, userState);
                return false;
            }
            Query = Query.Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());

            string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                        ParamsDict.OraServerName + ")(PORT=" + ParamsDict.OraPort.ToString() +
                        ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + ParamsDict.OraSid + ")));" +
                        "USER ID=" + ParamsDict.OraLogin + ";PASSWORD=" + ParamsDict.OraPass + ";";
            using (OracleConnection conn = new OracleConnection(connString))
            {
                OracleCommand oraCommand = conn.CreateCommand();
                oraCommand.CommandText = Query;

                OracleDataReader oraReader;
                userState.WorkCurrentTitle = "Выполняется запрос к серверу Oracle...";
                worker.ReportProgress(0, userState);
                try
                {
                    MemoryStream ms;
                    BinaryWriter bw;
                    int i = 0, iLen = Wells.Count;
                    userState.WorkCurrentTitle = "Чтение данных с сервера Oracle...";

                    #region Чтение данных ГТМ
                    conn.Open();
                    oraReader = oraCommand.ExecuteReader();

                    bool res = oraReader.Read(), res2;
                    worker.ReportProgress(0, userState);

                    // записываем в память данные добычи
                    int j, well_index, well_id, bore_id, count;
                    int boreIndex, lastBoreIndex = 0;
                    int areaCode;
                    string well_name;
                    DateTime dt;
                    ushort GTMCode;
                    Well w;
                    while (res)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            retValue = false;
                            break;
                        }
                        else
                        {
                            well_id = Convert.ToInt32(oraReader[0]);
                            bore_id = Convert.ToInt32(oraReader[2]);

                            areaCode = Convert.ToInt32(oraReader[4]);
                            well_name = ((string)oraReader[3]).Trim();
                            well_name = well_name.ToUpper();
                            well_index = GetWellIndex(bore_id);
                            //well_index = GetWellIndex(well_name, areaCode);

                            if (well_index == -1)
                            {
                                userState.Error = "Не найдена скважина " + well_name;
                                ReportProgress(worker, i, iLen, userState);
                                while (res && (Convert.ToInt32(oraReader[2]) == bore_id)) res = oraReader.Read();
                                continue;
                            }
                            w = Wells[well_index];
                            lastBoreIndex = -1;
                            count = 0;
                            ms = GetBuffer(true);
                            bw = new BinaryWriter(ms);

                            res2 = res;
                            while (res2)
                            {
                                if (Convert.ToInt32(oraReader[2]) != bore_id) break;
                                dt = Convert.ToDateTime(oraReader[6]);
                                dt = new DateTime(dt.Year, dt.Month, dt.Day);
                                boreIndex = Convert.ToInt32(oraReader[1]);
                                if (boreIndex > lastBoreIndex) lastBoreIndex = boreIndex;

                                if ((lastBoreIndex == boreIndex) && (dt <= DateTime.Now.AddDays(1)))
                                {
                                    GTMCode = Convert.ToUInt16(oraReader[5]);

                                    count++;
                                    bw.Write(dt.ToOADate());
                                    bw.Write(GTMCode);
                                }
                                res2 = oraReader.Read();
                            }

                            res = res2;
                            if (count > 0)
                            {
                                BinaryReader br = new BinaryReader(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                WellGTMItem[] gtmItems = new WellGTMItem[count];

                                for (j = 0; j < count; j++)
                                {
                                    gtmItems[j].Date = DateTime.FromOADate(br.ReadDouble());
                                    gtmItems[j].GtmCode = br.ReadUInt16();
                                }
                            }

                            retValue = true;
                            i++;
                            ReportProgress(worker, i, iLen);
                        }
                    }
                    #endregion

                    oraReader.Close();
                }
                catch (Exception ex)
                {
                    userState.Error = "Ошибка загрузки с сервера Oracle!" + ex.Message;
                    worker.ReportProgress(0, userState);
                    return false;
                }
                userState.WorkCurrentTitle = "Данные с сервера Oracle получены";
                worker.ReportProgress(0, userState);
            }
            return retValue;
        }
        public bool LoadGTMFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\gtm.bin";
            if (File.Exists(cacheName))
            {
                int k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version= bfr.ReadInt32();
                if (WellGTM.VERSION == version)
                {
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();
                        if (WellIndex < wellCount + wellBlockCount)
                        {
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            shift = (WellIndex - wellCount) * 4;
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            shift = br.ReadInt32();
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            well_index = br.ReadInt32();

                            if (WellIndex != well_index)
                            {
                                retValue = false;
                                //MessageBox.Show("Не найден индекс скважины в кеше!");
                            }
                            else if (well_index > -1)
                            {
                                w = Wells[well_index];
                                w.Gtm = new WellGTM();
                                w.Gtm.ReadFromCache(br, version);
                            }
                            break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += wellBlockCount;
                    }
                }
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadGTMFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных ГТМ месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\gtm.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (WellGTM.VERSION == version)
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();

                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            wellBlockCount = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                            for (i = 0; i < wellBlockCount; i++)
                            {
                                well_index = br.ReadInt32();
                                if (well_index > -1)
                                {
                                    w = Wells[well_index];
                                    userState.Element = w.Name;
                                    w.Gtm = new WellGTM();
                                    w.Gtm.ReadFromCache(br, version);
                                    if (w.Gtm.Count == 0) w.Gtm = null;
                                }
                                retValue = true;
                                ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                            wellCount += wellBlockCount;
                        }
                    }
                    bfr.Close();
                }
            }
            return retValue;
        }
        public bool WriteGTMToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных ГТМ в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\gtm.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                List<MemoryStream> msArr = new List<MemoryStream>();
                List<int> wellCount = new List<int>(5);
                List<int> unpackSizes = new List<int>(5);
                List<int> WellsShift = new List<int>();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, lenItem = 10, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        len_all_wells += 8;
                        if ((w.GtmLoaded) && (w.Gtm != null))
                        {
                            len_all_wells += 4 + w.Gtm.Count * lenItem;
                        }
                        else
                            len_all_wells += 4;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = Wells[i];
                            len_shift = 8;
                            if ((w.GtmLoaded) && (w.Gtm != null))
                            {
                                len_shift += 4 + w.Gtm.Count * lenItem;
                            }
                            else
                                len_shift += 4;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = Wells[j];
                                len_shift += 4;
                                if (w.GtmLoaded)
                                {
                                    len_shift += 4 + w.Gtm.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = Wells[j];
                                bw.Write(w.Index);
                                if (w.GtmLoaded)
                                {
                                    w.Gtm.WriteToCache(bw);
                                }
                                else
                                {
                                    bw.Write(0);
                                }
                            }

                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = Wells[i];
                                len_shift = 8;
                                if ((w.GtmLoaded) && (w.Gtm != null))
                                {
                                    len_shift += 4 + w.Gtm.Count * lenItem;
                                }
                                else
                                {
                                    len_shift += 4;
                                }
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellGTM.VERSION);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearGTMData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if (w.GtmLoaded)
                    {
                        w.Gtm.Clear();
                        w.Gtm = null;
                    }
                }
                if (useGC) GC.Collect(GC.MaxGeneration);
            }
        }
        #endregion

        #region WELL ACTION
        public bool LoadWellActionFromServer(BackgroundWorker worker, DoWorkEventArgs e, string Query)
        {
            bool retValue = false;
            // загрузка с оракла
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных мероприятий на скважинах с сервера";
            userState.Element = Name;
            worker.ReportProgress(0, userState);
            if (Query.IndexOf("%OILFIELDCODE%") == -1)
            {
                userState.Error = "Неверный запрос к базе данных. Нет объекта %OILFIELDCODE%";
                worker.ReportProgress(0, userState);
                return false;
            }
            Query = Query.Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());

            string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                        ParamsDict.OraServerName + ")(PORT=" + ParamsDict.OraPort.ToString() +
                        ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + ParamsDict.OraSid + ")));" +
                        "USER ID=" + ParamsDict.OraLogin + ";PASSWORD=" + ParamsDict.OraPass + ";";
            using (OracleConnection conn = new OracleConnection(connString))
            {
                OracleCommand oraCommand = conn.CreateCommand();
                oraCommand.CommandText = Query;

                OracleDataReader oraReader;
                userState.WorkCurrentTitle = "Выполняется запрос к серверу Oracle...";
                worker.ReportProgress(0, userState);
                try
                {
                    MemoryStream ms;
                    BinaryWriter bw;
                    int i = 0, iLen = Wells.Count;
                    userState.WorkCurrentTitle = "Чтение данных с сервера Oracle...";

                    #region Чтение данных ремонтов на скважинах
                    conn.Open();
                    oraReader = oraCommand.ExecuteReader();

                    bool res = oraReader.Read(), res2;
                    worker.ReportProgress(0, userState);

                    // записываем в память данные добычи
                    int j, well_index, well_id, bore_id, count;
                    int boreIndex, lastBoreIndex = 0;
                    int areaCode;
                    string well_name;
                    DateTime dt;
                    ushort WellActionCode;
                    Well w;
                    while (res)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            retValue = false;
                            break;
                        }
                        else
                        {
                            well_id = Convert.ToInt32(oraReader[0]);
                            bore_id = Convert.ToInt32(oraReader[2]);
                            areaCode = Convert.ToInt32(oraReader[4]);
                            well_name = ((string)oraReader[3]).Trim();
                            well_name = well_name.ToUpper();
                            well_index = GetWellIndex(bore_id);
                            // well_index = GetWellIndex(well_name, areaCode);
                            if (well_index == -1)
                            {
                                userState.Error = "Не найдена скважина " + well_name;
                                ReportProgress(worker, i, iLen, userState);
                                while ((res) && (Convert.ToInt32(oraReader[2]) == bore_id)) res = oraReader.Read();
                                continue;
                            }
                            w = Wells[well_index];
                            lastBoreIndex = -1;
                            count = 0;
                            ms = GetBuffer(true);
                            bw = new BinaryWriter(ms);
                            res2 = res;
                            while (res2)
                            {
                                if (Convert.ToInt32(oraReader[2]) != bore_id) break;
                                dt = Convert.ToDateTime(oraReader[6]);
                                dt = new DateTime(dt.Year, dt.Month, dt.Day);
                                boreIndex = Convert.ToInt32(oraReader[1]);
                                if (boreIndex > lastBoreIndex) lastBoreIndex = boreIndex;

                                if ((lastBoreIndex == boreIndex) && (dt <= DateTime.Now.AddDays(1)))
                                {
                                    WellActionCode = Convert.ToUInt16(oraReader[5]);

                                    count++;
                                    bw.Write(dt.ToOADate());
                                    bw.Write(WellActionCode);
                                }
                                res2 = oraReader.Read();
                            }
                            res = res2;
                            if (count > 0)
                            {
                                BinaryReader br = new BinaryReader(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                WellRepairActionItem[] actionItems = new WellRepairActionItem[count];

                                for (j = 0; j < count; j++)
                                {
                                    actionItems[j].Date = DateTime.FromOADate(br.ReadDouble());
                                    actionItems[j].WellActionCode = br.ReadUInt16();
                                }
                            }
                            retValue = true;
                            i++;
                            ReportProgress(worker, i, iLen);
                        }
                    }
                    #endregion

                    oraReader.Close();
                }
                catch (Exception ex)
                {
                    userState.Error = "Ошибка загрузки с сервера Oracle!" + ex.Message;
                    worker.ReportProgress(0, userState);
                    return false;
                }
                userState.WorkCurrentTitle = "Данные с сервера Oracle получены";
                worker.ReportProgress(0, userState);
            }
            return retValue;
        }
        public bool LoadWellActionFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\well_action.bin";
            if (File.Exists(cacheName))
            {
                int k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (WellRepairAction.VERSION == version)
                {
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();
                        if (WellIndex < wellCount + wellBlockCount)
                        {
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            shift = (WellIndex - wellCount) * 4;
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            shift = br.ReadInt32();
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            well_index = br.ReadInt32();

                            if (WellIndex != well_index)
                            {
                                retValue = false;
                                //MessageBox.Show("Не найден индекс скважины в кеше!");
                            }
                            else if (well_index > -1)
                            {
                                w = Wells[well_index];
                                w.RepairAct = new WellRepairAction();
                                w.RepairAct.ReadFromCache(br, version);
                                if (w.RepairAct.Count == 0) w.RepairAct = null;
                            }
                            break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += wellBlockCount;
                    }
                }
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadWellActionFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных мероприятий на скважинах месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\well_action.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (WellRepairAction.VERSION == version)
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();

                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            wellBlockCount = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                            for (i = 0; i < wellBlockCount; i++)
                            {
                                well_index = br.ReadInt32();
                                if (well_index > -1)
                                {
                                    w = Wells[well_index];
                                    userState.Element = w.Name;
                                    w.RepairAct = new WellRepairAction();
                                    w.RepairAct.ReadFromCache(br, version);
                                    if (w.RepairAct.Count == 0) w.RepairAct = null;
                                }
                                retValue = true;
                                ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                            wellCount += wellBlockCount;
                        }
                    }
                    bfr.Close();
                }
            }
            return retValue;
        }
        public bool WriteWellActionToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных мероприятий на скважинах в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\well_action.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                List<MemoryStream> msArr = new List<MemoryStream>();
                List<int> wellCount = new List<int>(5);
                List<int> unpackSizes = new List<int>(5);
                List<int> WellsShift = new List<int>();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, lenItem = 10, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        len_all_wells += 8;
                        if (w.RepairActionLoaded)
                        {
                            len_all_wells += 4 + w.RepairAct.Count * lenItem;
                        }
                        else
                        {
                            len_all_wells += 4;
                        }
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = Wells[i];
                            len_shift = 8;
                            if (w.RepairActionLoaded)
                            {
                                len_shift += 4 + w.RepairAct.Count * lenItem;
                            }
                            else
                            {
                                len_shift += 4;
                            }
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = Wells[j];
                                len_shift += 4;
                                if (w.RepairActionLoaded)
                                {
                                    len_shift += 4 + w.RepairAct.Count * lenItem;
                                }
                                else
                                {
                                    len_shift += 4;
                                }
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = Wells[j];
                                bw.Write(w.Index);
                                if (w.RepairActionLoaded)
                                {
                                    w.RepairAct.WriteToCache(bw);
                                }
                                else
                                {
                                    bw.Write(0);
                                }
                            }

                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = Wells[i];
                                len_shift = 8;
                                if (w.RepairActionLoaded)
                                {
                                    len_shift += 4 + w.RepairAct.Count * lenItem;
                                }
                                else
                                {
                                    len_shift += 4;
                                }
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellRepairAction.VERSION);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write(unpackSizes[i]);
                        bw.Write(wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearWellActionData(bool useGC)
        {
            int i, len = Wells.Count;
            Well w;
            if (len > 0)
            {
                for (i = 0; i < len; i++)
                {
                    w = Wells[i];
                    if (w.RepairActionLoaded)
                    {
                        w.RepairAct.Clear();
                        w.RepairAct = null;
                    }
                }
                if (useGC) GC.Collect(GC.MaxGeneration);
            }
        }
        #endregion

        #region RESEARCH
        public bool LoadWellResearchFromServer(BackgroundWorker worker, DoWorkEventArgs e, string Query)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка данных давлений на скважинах с сервера";
            userState.Element = Name;
            worker.ReportProgress(0, userState);
            if (Query.IndexOf("%OILFIELDCODE%") == -1)
            {
                userState.Error = "Неверный запрос к базе данных. Нет объекта %OILFIELDCODE%";
                worker.ReportProgress(0, userState);
                return false;
            }
            Query = Query.Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());
            string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                        ParamsDict.OraServerName + ")(PORT=" + ParamsDict.OraPort.ToString() +
                        ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + ParamsDict.OraSid + ")));" +
                        "USER ID=" + ParamsDict.OraLogin + ";PASSWORD=" + ParamsDict.OraPass + ";";

            using (OracleConnection conn = new OracleConnection(connString))
            {
                OracleCommand oraCommand = conn.CreateCommand();
                oraCommand.CommandText = Query;

                OracleDataReader oraReader;
                userState.WorkCurrentTitle = "Выполняется запрос к серверу Oracle...";
                worker.ReportProgress(0, userState);
                try
                {
                    MemoryStream ms;
                    BinaryWriter bw;
                    int i = 0, iLen = Wells.Count;
                    userState.WorkCurrentTitle = "Чтение данных с сервера Oracle...";

                    #region Чтение данных исследований скважин
                    conn.Open();
                    oraReader = oraCommand.ExecuteReader();

                    bool res = oraReader.Read(), res2;
                    worker.ReportProgress(0, userState);

                    int j, well_index, well_id, bore_id, count;
                    int areaCode;
                    string well_name;
                    DateTime dt;
                    ushort WellResearchCode;
                    ushort PlastCode;
                    int delayTime;
                    float zatr, water, perf;
                    Well w;
                    //if (res) LoadWellResearchFromCache(worker, e); // чтобы не потерять записи DBF
                    while (res)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            retValue = false;
                            break;
                        }
                        else
                        {
                            well_id = Convert.ToInt32(oraReader[0]);
                            bore_id = Convert.ToInt32(oraReader[1]);
                            areaCode = Convert.ToInt32(oraReader[3]);
                            well_name = ((string)oraReader[2]).Trim();
                            well_name = well_name.ToUpper();
                            well_index = GetWellIndex(bore_id);
                            //well_index = GetWellIndex(well_name, areaCode);
                            if (well_index == -1)
                            {
                                userState.Error = "Не найдена скважина " + well_name;
                                ReportProgress(worker, i, iLen, userState);
                                while ((res) && (Convert.ToInt32(oraReader[1]) == bore_id)) res = oraReader.Read();
                                continue;
                            }
                            w = Wells[well_index];
                            count = 0;
                            ms = GetBuffer(true);
                            bw = new BinaryWriter(ms);
                            res2 = res;
                            while (res2)
                            {
                                if (Convert.ToInt32(oraReader[1]) != bore_id) break;
                                dt = Convert.ToDateTime(oraReader[4]);
                                if (dt <= DateTime.Now.AddDays(1))
                                {
                                    WellResearchCode = Convert.ToUInt16(oraReader[5]);
                                    PlastCode = Convert.ToUInt16(oraReader[6]);
                                    delayTime = Convert.ToInt32(oraReader[7]);
                                    zatr = Convert.ToSingle(oraReader[8]);
                                    water = Convert.ToSingle(oraReader[9]);
                                    perf = Convert.ToSingle(oraReader[10]);
                                    count++;
                                    bw.Write(dt.ToOADate());
                                    bw.Write(WellResearchCode);
                                    bw.Write(PlastCode);
                                    bw.Write(zatr);
                                    bw.Write(water);
                                    bw.Write(perf);
                                    bw.Write(delayTime);
                                }
                                res2 = oraReader.Read();
                            }
                            res = res2;
                            if (count > 0)
                            {
                                BinaryReader br = new BinaryReader(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                WellResearchItem[] resItems = new WellResearchItem[count];

                                for (j = 0; j < count; j++)
                                {
                                    resItems[j].Date = DateTime.FromOADate(br.ReadDouble());
                                    resItems[j].ResearchCode = br.ReadUInt16();
                                    resItems[j].StratumCode = br.ReadUInt16();
                                    resItems[j].PressZatr = br.ReadSingle();
                                    resItems[j].PressWaterSurface = br.ReadSingle();
                                    resItems[j].PressPerforation = br.ReadSingle();
                                    resItems[j].TimeDelay = br.ReadInt32();
                                }

                                w.Research = new WellResearch();
                                w.Research.IsServerStratumCode = true;
                                w.Research.SetItems(resItems);
                            }
                            retValue = true;
                            i++;
                            ReportProgress(worker, i, iLen);
                        }
                    }
                    #endregion

                    oraReader.Close();
                }
                catch (Exception ex)
                {
                    userState.Error = "Ошибка загрузки с сервера Oracle!" + ex.Message;
                    worker.ReportProgress(0, userState);
                    return false;
                }
                userState.WorkCurrentTitle = "Данные с сервера Oracle получены";
                worker.ReportProgress(0, userState);
            }
            return retValue;
        }
        public bool LoadWellResearchFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + Name + "\\well_research.bin";
            if (File.Exists(cacheName))
            {
                int k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (WellResearch.VERSION == version)
                {
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();
                    wellCount = 0;
                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();
                        if (WellIndex < wellCount + wellBlockCount)
                        {
                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            shift = (WellIndex - wellCount) * 4;
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            shift = br.ReadInt32();
                            unpackMS.Seek(shift, SeekOrigin.Begin);
                            well_index = br.ReadInt32();

                            if (WellIndex != well_index)
                            {
                                retValue = false;
                                //MessageBox.Show("Не найден индекс скважины в кеше!");
                            }
                            else if (well_index > -1)
                            {
                                w = Wells[well_index];
                                w.Research = new WellResearch();
                                w.Research.ReadFromCache(br, version);
                                if (w.Research.Count == 0) w.Research = null;
                            }
                            break;
                        }
                        else
                        {
                            fs.Seek(len_block, SeekOrigin.Current);
                        }
                        wellCount += wellBlockCount;
                    }
                }
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadWellResearchFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Загрузка данных давлений на скважинах месторождения";
            userState.Element = Name;

            string cacheName = ProjectPath + "\\cache\\" + Name + "\\well_research.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (WellResearch.VERSION == version)
                    {
                        num_blocks = bfr.ReadInt32();
                        iLen = bfr.ReadInt32();

                        for (k = 0; k < num_blocks; k++)
                        {
                            len_block = bfr.ReadInt32();
                            unpack_len_block = bfr.ReadInt32();
                            wellBlockCount = bfr.ReadInt32();

                            ms = GetBuffer(true);
                            unpackMS = GetBuffer(false);
                            ms.SetLength(len_block);
                            br = new BinaryReader(unpackMS);
                            ConvertEx.CopyStream(fs, ms, len_block);
                            ConvertEx.UnPackStream(ms, ref unpackMS);

                            unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                            for (i = 0; i < wellBlockCount; i++)
                            {
                                well_index = br.ReadInt32();
                                if (well_index > -1)
                                {
                                    w = Wells[well_index];
                                    userState.Element = w.Name;
                                    w.Research = new WellResearch();
                                    w.Research.ReadFromCache(br, version);
                                    if (w.Research.Count == 0) w.Research = null;
                                }
                                retValue = true;
                                ReportProgress(worker, wellCount + i, iLen, userState);
                            }
                            wellCount += wellBlockCount;
                        }
                    }
                    bfr.Close();
                }
            }
            return retValue;
        }
        public bool WriteWellResearchToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = string.Empty;
            userState.WorkCurrentTitle = "Запись данных давлений на скважинах в кэш";
            userState.Element = Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + Name + "\\well_research.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                List<MemoryStream> msArr = new List<MemoryStream>();
                List<int> wellCount = new List<int>(5);
                List<int> unpackSizes = new List<int>(5);
                List<int> WellsShift = new List<int>();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, lenItem = 28, len_all_wells, wCount = 0, len_block;
                int len_shift;
                int iLen = Wells.Count;

                if (iLen > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < iLen; i++)
                    {
                        w = Wells[i];
                        len_all_wells += 8;
                        if (w.ResearchLoaded)
                        {
                            len_all_wells += 4 + w.Research.Count * lenItem;
                        }
                        else
                            len_all_wells += 4;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= iLen; i++)
                    {
                        len_shift = 0;
                        if (i < iLen)
                        {
                            w = Wells[i];
                            len_shift = 8;
                            if (w.ResearchLoaded)
                            {
                                len_shift += 4 + w.Research.Count * lenItem;
                            }
                            else
                                len_shift += 4;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < iLen))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = Wells[j];
                                len_shift += 4;
                                if (w.ResearchLoaded)
                                {
                                    len_shift += 4 + w.Research.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = Wells[j];
                                bw.Write(w.Index);
                                if (w.ResearchLoaded)
                                {
                                    w.Research.WriteToCache(bw);
                                }
                                else bw.Write(0);
                            }

                            if (i < iLen)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = Wells[i];
                                len_shift = 8;
                                if (w.ResearchLoaded)
                                {
                                    len_shift += 4 + w.Research.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, iLen);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellResearch.VERSION);
                    bw.Write(msArr.Count);
                    bw.Write(iLen);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        #endregion

        #region SUSPEND
        public bool LoadSuspendFromServer(BackgroundWorker worker, DoWorkEventArgs e, string Query)
        {
            bool retValue = false;
            // загрузка с оракла
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных КВЧ на скважинах с сервера";
            userState.Element = this.Name;
            worker.ReportProgress(0, userState);
            if (Query.IndexOf("%OILFIELDCODE%") == -1)
            {
                userState.Error = "Неверный запрос к базе данных. Нет объекта %OILFIELDCODE%";
                worker.ReportProgress(0, userState);
                return false;
            }
            Query = Query.Replace("%OILFIELDCODE%", ParamsDict.OraFieldCode.ToString());

            string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                        ParamsDict.OraServerName + ")(PORT=" + ParamsDict.OraPort.ToString() +
                        ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + ParamsDict.OraSid + ")));" +
                        "USER ID=" + ParamsDict.OraLogin + ";PASSWORD=" + ParamsDict.OraPass + ";";
            using (OracleConnection conn = new OracleConnection(connString))
            {
                OracleCommand oraCommand = conn.CreateCommand();
                oraCommand.CommandText = Query;

                OracleDataReader oraReader;
                userState.WorkCurrentTitle = "Выполняется запрос к серверу Oracle...";
                worker.ReportProgress(0, userState);
                try
                {
                    MemoryStream ms;
                    BinaryWriter bw;
                    int i = 0, iLen = Wells.Count;
                    userState.WorkCurrentTitle = "Чтение данных с сервера Oracle...";

                    #region Чтение данных КВЧ
                    conn.Open();
                    oraReader = oraCommand.ExecuteReader();

                    bool res = oraReader.Read(), res2;
                    worker.ReportProgress(0, userState);

                    int j, m, well_index, wellId, wellBoreId, count;
                    int areaCode;
                    string well_name;
                    DateTime dt;
                    int kvch;
                    Well w;
                    while (res)
                    {
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            retValue = false;
                            break;
                        }
                        else
                        {
                            wellId = Convert.ToInt32(oraReader[0]);
                            areaCode = Convert.ToInt32(oraReader[4]);
                            wellBoreId = Convert.ToInt32(oraReader[2]);
                            well_name = ((string)oraReader[3]).Trim();
                            well_name = well_name.ToUpper();
                            well_index = GetWellIndex(wellBoreId);
                            if (well_index == -1)
                            {
                                userState.Error = "Не найдена скважина " + well_name;
                                ReportProgress(worker, i, Wells.Count, userState);
                                while ((res) && (Convert.ToInt32(oraReader[2]) == wellBoreId)) res = oraReader.Read();
                                continue;
                            }
                            w = Wells[well_index];
                            count = 0;
                            ms = GetBuffer(true);
                            bw = new BinaryWriter(ms);
                            res2 = res;
                            while (res2)
                            {
                                if (Convert.ToInt32(oraReader[2]) != wellBoreId) break;
                                dt = Convert.ToDateTime(oraReader[5]);
                                if (dt <= DateTime.Now.AddDays(1))
                                {
                                    kvch = Convert.ToInt32(oraReader[6]);
                                    count++;
                                    bw.Write(dt.ToOADate());
                                    bw.Write(kvch);
                                }
                                res2 = oraReader.Read();
                            }
                            res = res2;
                            if (count > 0)
                            {
                                BinaryReader br = new BinaryReader(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                WellSuspendItem[] resItems = new WellSuspendItem[count];

                                m = 0;
                                for (j = 0; j < count; j++)
                                {
                                    resItems[j].Date = DateTime.FromOADate(br.ReadDouble());
                                    resItems[j].SuspendSolid = br.ReadInt32();
                                }

                                w.Suspend = new WellSuspend();
                                w.Suspend.SetItems(resItems);
                            }
                            retValue = true;
                            i++;
                            ReportProgress(worker, i, Wells.Count);
                        }
                    }
                    #endregion

                    oraReader.Close();
                }
                catch (Exception ex)
                {
                    userState.Error = "Ошибка загрузки с сервера Oracle!" + ex.Message;
                    worker.ReportProgress(0, userState);
                    return false;
                }
                userState.WorkCurrentTitle = "Данные с сервера Oracle получены";
                worker.ReportProgress(0, userState);
            }
            return retValue;
        }
        public bool LoadSuspendFromCache(int WellIndex)
        {
            bool retValue = false;
            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\well_suspend.bin";
            if (File.Exists(cacheName))
            {
                int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                int shift;
                Well w;
                FileStream fs;
                MemoryStream ms, unpackMS;
                BinaryReader br;

                try
                {
                    fs = new FileStream(cacheName, FileMode.Open);
                    if (fs.Length == 0) return false;
                }
                catch (Exception)
                {
                    return false;
                }
                BinaryReader bfr = new BinaryReader(fs);
                int version = bfr.ReadInt32();
                if (version < WellSuspend.Version)
                {
                    bfr.Close();
                    return false;
                }
                num_blocks = bfr.ReadInt32();
                iLen = bfr.ReadInt32();
                wellCount = 0;
                for (k = 0; k < num_blocks; k++)
                {
                    len_block = bfr.ReadInt32();
                    unpack_len_block = bfr.ReadInt32();
                    wellBlockCount = bfr.ReadInt32();
                    if (WellIndex < wellCount + wellBlockCount)
                    {
                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        shift = (WellIndex - wellCount) * 4;
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        shift = br.ReadInt32();
                        unpackMS.Seek(shift, SeekOrigin.Begin);
                        well_index = br.ReadInt32();

                        if (WellIndex != well_index)
                        {
                            MessageBox.Show("Не найден индекс скважины в кеше!");
                        }
                        else if (well_index > -1)
                        {
                            w = Wells[well_index];
                            w.Suspend = new WellSuspend();
                            w.Suspend.ReadFromCache(br, version);
                        }
                        break;
                    }
                    else
                    {
                        fs.Seek(len_block, SeekOrigin.Current);
                    }
                    wellCount += wellBlockCount;
                }
                bfr.Close();
            }
            return retValue;
        }
        public bool LoadSuspendFromCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            return LoadSuspendFromCache(worker, e, false);
        }
        public bool LoadSuspendFromCache(BackgroundWorker worker, DoWorkEventArgs e, bool NotReportProgress)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Загрузка данных КВЧ на скважинах";
            userState.Element = this.Name;

            string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\well_suspend.bin";
            if (File.Exists(cacheName))
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int i, j, k, num_blocks, unpack_len_block, len_block, wellBlockCount, well_index = -1, iLen, wellCount = 0;
                    Well w;
                    MemoryStream ms, unpackMS;
                    BinaryReader br;
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Open);
                    }
                    catch
                    {
                        return false;
                    }
                    BinaryReader bfr = new BinaryReader(fs);
                    int version = bfr.ReadInt32();
                    if (version < WellSuspend.Version)
                    {
                        bfr.Close();
                        return false;
                    }
                    num_blocks = bfr.ReadInt32();
                    iLen = bfr.ReadInt32();

                    for (k = 0; k < num_blocks; k++)
                    {
                        len_block = bfr.ReadInt32();
                        unpack_len_block = bfr.ReadInt32();
                        wellBlockCount = bfr.ReadInt32();

                        ms = this.GetBuffer(true);
                        unpackMS = this.GetBuffer(false);
                        ms.SetLength(len_block);
                        br = new BinaryReader(unpackMS);
                        ConvertEx.CopyStream(fs, ms, len_block);
                        ConvertEx.UnPackStream(ms, ref unpackMS);

                        unpackMS.Seek(4 * wellBlockCount, SeekOrigin.Begin);
                        for (i = 0; i < wellBlockCount; i++)
                        {
                            well_index = br.ReadInt32();
                            if (well_index > -1)
                            {
                                w = Wells[well_index];
                                userState.Element = w.Name;
                                w.Suspend = new WellSuspend();
                                w.Suspend.ReadFromCache(br, version);
                                if (w.Suspend.Count > 0)
                                {
                                    WellSuspendLoaded = true;
                                }
                                else
                                {
                                    w.Suspend = null;
                                }
                            }
                            retValue = true;
                            if (!NotReportProgress) ReportProgress(worker, wellCount + i, iLen, userState);
                        }
                        wellCount += wellBlockCount;
                    }
                    bfr.Close();
                    GC.GetTotalMemory(true);
                }
            }
            return retValue;
        }
        public bool WriteSuspendToCache(BackgroundWorker worker, DoWorkEventArgs e)
        {
            bool retValue = false;
            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Запись данных КВЧ на скважинах в кэш";
            userState.Element = this.Name;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                string cacheName = ProjectPath + "\\cache\\" + this.Name + "\\well_suspend.bin";
                int MAX_SIZE_PACK_BLOCK = 1 * 1024 * 512;
                Well w;
                List<MemoryStream> msArr = new List<MemoryStream>();
                List<int> wellCount = new List<int>(5);
                List<int> unpackSizes = new List<int>(5);
                List<int> WellsShift = new List<int>();

                MemoryStream packedMS;
                FileStream fs;
                MemoryStream ms;
                BinaryWriter bw;

                int i, j, k, h, lenItem = 12, len_all_wells, wCount = 0, len_block;
                int len_shift;

                if (Wells.Count > 0)
                {
                    len_all_wells = 0;
                    for (i = 0; i < Wells.Count; i++)
                    {
                        w = Wells[i];
                        len_all_wells += 8;
                        len_all_wells += 4;
                        if (w.SuspendLoaded) len_all_wells +=  w.Suspend.Count * lenItem;
                    }

                    if (len_all_wells > MAX_SIZE_PACK_BLOCK) len_all_wells = MAX_SIZE_PACK_BLOCK;

                    ms = new MemoryStream(len_all_wells);
                    bw = new BinaryWriter(ms);

                    worker.ReportProgress(0, userState);
                    try
                    {
                        fs = new FileStream(cacheName, FileMode.Create);
                    }
                    catch
                    {
                        return false;
                    }

                    len_block = 0;
                    k = 0;
                    wCount = 0;
                    for (i = 0; i <= Wells.Count; i++)
                    {
                        len_shift = 0;
                        if (i < Wells.Count)
                        {
                            w = Wells[i];
                            len_shift = 8;
                            len_shift += 4;
                            if (w.SuspendLoaded) len_shift += w.Suspend.Count * lenItem;
                        }

                        if ((len_block + len_shift <= len_all_wells) && (i < Wells.Count))
                        {
                            wCount++;
                            len_block += len_shift;
                        }
                        else
                        {
                            len_shift = 4 * wCount;
                            bw.Write(len_shift);
                            for (j = k; j < i - 1; j++)
                            {
                                w = Wells[j];
                                len_shift += 4;
                                if (w.SuspendLoaded)
                                {
                                    len_shift += 4 + w.Suspend.Count * lenItem;
                                }
                                else
                                    len_shift += 4;
                                bw.Write(len_shift);
                            }

                            for (j = k; j < i; j++)
                            {
                                w = Wells[j];
                                bw.Write(w.Index);
                                if (w.SuspendLoaded)
                                {
                                    w.Suspend.WriteToCache(bw);
                                }
                                else
                                    bw.Write(0);
                            }

                            if (i < Wells.Count)
                            {
                                unpackSizes.Add((int)ms.Length);
                                packedMS = ConvertEx.PackStream(ms);
                                msArr.Add(packedMS);
                                wellCount.Add(wCount);
                                WellsShift.Clear();
                                ms.SetLength(0);
                                k = i;
                                w = Wells[i];
                                len_shift = 8;
                                len_shift += 4;
                                if (w.SuspendLoaded) len_shift += w.Suspend.Count * lenItem;
                                    
                                len_block = len_shift;
                                wCount = 1;
                            }
                        }
                        retValue = true;
                        ReportProgress(worker, i, Wells.Count);
                    }

                    unpackSizes.Add((int)ms.Length);
                    packedMS = ConvertEx.PackStream(ms);
                    msArr.Add(packedMS);
                    wellCount.Add(wCount);
                    bw.Close();

                    bw = new BinaryWriter(fs);
                    bw.Write(WellSuspend.Version);
                    bw.Write(msArr.Count);
                    bw.Write(Wells.Count);
                    for (i = 0; i < msArr.Count; i++)
                    {
                        packedMS = (MemoryStream)msArr[i];
                        bw.Write((int)packedMS.Length);
                        bw.Write((int)unpackSizes[i]);
                        bw.Write((int)wellCount[i]);
                        packedMS.Seek(0, SeekOrigin.Begin);
                        packedMS.WriteTo(fs);
                        packedMS.Dispose();
                    }
                    packedMS = null;
                    msArr.Clear();
                    wellCount.Clear();
                    unpackSizes.Clear();
                    bw.Close();
                }
                File.SetCreationTime(cacheName, DateTime.Now);
                File.SetLastWriteTime(cacheName, DateTime.Now);
                if ((!retValue) && (File.Exists(cacheName))) File.Delete(cacheName);
            }
            return retValue;
        }
        public void ClearSuspendData(bool useGC)
        {
            Well w;
            if (Wells.Count > 0)
            {
                for (int i = 0; i < Wells.Count; i++)
                {
                    w = Wells[i];
                    if (w.SuspendLoaded)
                    {
                        w.Suspend.Clear();
                        w.Suspend = null;
                    }
                }
                WellSuspendLoaded = false;
                if (useGC) GC.Collect(GC.MaxGeneration);
            }
        }
        #endregion
    }
}

