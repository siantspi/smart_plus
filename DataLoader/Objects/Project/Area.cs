﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Data;
using System.IO;
using DataLoader.DictionaryObjects;
using System.ComponentModel;
using DataLoader.Objects.Base;

namespace DataLoader.Objects.Project
{
    public struct AreaParamsItem
    {
        const int version = 0;
        const int bytes = 88;
        /// <summary>
        /// Площадь ячейки, м2
        /// </summary>
        public double Square;
        /// <summary>
        /// КИН
        /// </summary>
        public double KIN;
        /// <summary>
        /// Начальная нефтенасыщ.толщина
        /// </summary>
        public double InitHoil;
        /// <summary>
        /// Средний КН
        /// </summary>
        public double KH;
        /// <summary>
        /// Начальная газонасыщ.толщина
        /// </summary>
        public double InitHgas;
        /// <summary>
        /// Начальные балансовые запасы нефти
        /// </summary>
        public double InitOilReserv;
        /// <summary>
        /// Начальные балансовые запасы газа
        /// </summary>
        public double InitGasReserv;
        /// <summary>
        /// Компенсация
        /// </summary>
        public double Compensation;
        /// <summary>
        /// Накопленная компенсация
        /// </summary>
        public double AccumCompensation;
        /// <summary>
        /// ТИЗ
        /// </summary>
        public double TIZ;
        /// <summary>
        /// Текущее пластово давление
        /// </summary>
        public double CurrentPressure;

        public void ReadFromBin(BinaryReader br)
        {
            int ver = br.ReadInt32();
            int bytes = br.ReadInt32();
            if (version == ver)
            {
                Square = br.ReadDouble();
                KIN = br.ReadDouble();
                InitHoil = br.ReadDouble();
                InitHgas = br.ReadDouble();
                KH = br.ReadDouble();
                InitOilReserv = br.ReadDouble();
                InitGasReserv = br.ReadDouble();
                Compensation = br.ReadDouble();
                AccumCompensation = br.ReadDouble();
                TIZ = br.ReadDouble();
                CurrentPressure = br.ReadDouble();
            }
            else
            {
                br.BaseStream.Seek(bytes, SeekOrigin.Current);
            }
        }
        public void WriteToBin(BinaryWriter bw)
        {
            bw.Write(version);
            bw.Write(bytes);
            bw.Write(Square);
            bw.Write(KIN);
            bw.Write(InitHoil);
            bw.Write(InitHgas);
            bw.Write(KH);
            bw.Write(InitOilReserv);
            bw.Write(InitGasReserv);
            bw.Write(Compensation);
            bw.Write(AccumCompensation);
            bw.Write(TIZ);
            bw.Write(CurrentPressure);
        }
    }
    public sealed class Area : BaseObject
    {
        public static int Version = 1;
        public static int SumVersion = 1;

        public bool ShowAreasMode;
        public bool ShowOverlay;
        public int OilFieldIndex;
        public int OilFieldAreaCode;
        public int StratumCode;
        public bool IsServerStratumCode;
        public PVTParamsItem pvt;
        public AreaParamsItem Params;

        public double Compensation
        {
            get { return Params.Compensation; }
            set
            {
                Params.Compensation = value;
                if (Params.Compensation == -1)
                    this.contour.Text = null;
                else
                {
                    this.contour.Text = String.Format("Комп.тек: {0:0.##}%", Params.Compensation);
                }
            }
        }
        public double AccumCompensation
        {
            get { return Params.AccumCompensation; }
            set
            {
                Params.AccumCompensation = value;
                if (Params.AccumCompensation == -1)
                    this.contour.Text2 = null;
                else
                {
                    this.contour.Text2 = String.Format("Комп.нак: {0:0.##}%", Params.AccumCompensation);
                }
            }
        }

        public Contour contour;
        public Well[] WellList;
        public Well[] AllWellList;
        public double[] WellCoefList;
        public int[] StratumList;
        public SumObjParameters sumObjParams;
        public SumObjParameters sumObjChessParams;
        double X, Y;

        public Area()
        {
            ShowAreasMode = false;
            IsServerStratumCode = false;
            OilFieldIndex = -1;
            OilFieldAreaCode = -1;
            StratumCode = -1;
            pvt = PVTParamsItem.Empty;
            contour = new Contour();
            WellList = new Well[0];
            StratumList = null;
            sumObjParams = null;
            sumObjChessParams = null;
            X = Constant.DOUBLE_NAN;
            Y = Constant.DOUBLE_NAN;
        }

        public bool SumParamsReCalc(BackgroundWorker worker, DoWorkEventArgs e, StratumDictionary plastDict)
        {
            bool retValue = false;

            WorkerState userState = new WorkerState();
            userState.WorkMainTitle = "";
            userState.WorkCurrentTitle = "Пересчет суммарных показателей ЯЗ по МЭР";
            userState.Element = this.Name;

            if ((this.WellList != null) && (this.WellList.Length > 0) && (StratumList != null))
            {
                int i, j, k, c;
                double wK;
                DateTime minDate, maxDate;
                Well w;
                minDate = DateTime.MaxValue;
                maxDate = DateTime.MinValue;
                List<int> PlastList = new List<int>(StratumList);

                for (i = 0; i < WellList.Length; i++)
                {
                    w = (Well)WellList[i];
                    if ((w.MerLoaded) && (w.Mer.Count > 0))
                    {
                        MerItem item;
                        for (j = 0; j < w.Mer.Count; j++)
                        {
                            item = w.Mer.Items[j];
                            if (PlastList.IndexOf(item.PlastId) != -1)
                            {
                                if (item.Date < minDate) minDate = item.Date;
                                if (item.Date > maxDate) maxDate = item.Date;
                            }
                        }
                    }
                }

                if ((minDate != DateTime.MaxValue) && (maxDate != DateTime.MinValue))
                {
                    int month, years;
                    DateTime dt;
                    years = maxDate.Year - minDate.Year + 1;
                    month = (maxDate.Year - minDate.Year) * 12 + maxDate.Month - minDate.Month + 1;
                    if (month > 0)
                    {
                        SumParamItem[] SumMonth = new SumParamItem[month];
                        SumParamItem[] SumYear = new SumParamItem[years];
                        List<Well>[] monthWellsArray = new List<Well>[month];
                        List<Well>[] yearWellsArray = new List<Well>[years];

                        dt = minDate;
                        for (i = 0; i < month; i++)
                        {
                            SumMonth[i].Date = dt;
                            SumMonth[i].InjGas = -1;
                            SumMonth[i].NatGas = -1;
                            SumMonth[i].GasCondensate = -1;
                            dt = dt.AddMonths(1);
                        }
                        dt = new DateTime(minDate.Year, 01, 01);
                        for (i = 0; i < years; i++)
                        {
                            SumYear[i].Date = dt;
                            SumYear[i].InjGas = -1;
                            SumYear[i].NatGas = -1;
                            SumYear[i].GasCondensate = -1;
                            dt = dt.AddYears(1);
                        }
                        int indMonth, indYears;
                        for (i = 0; i < WellList.Length; i++)
                        {
                            if (worker.CancellationPending)
                            {
                                e.Cancel = true;
                                return false;
                            }
                            w = (Well)WellList[i];
                            wK = this.WellCoefList[i];
                            if (w.MerLoaded)
                            {
                                int lastYearProd = -1, lastYearInj = -1, hours;
                                MerCompItem compItem;
                                MerCompPlastItem plastItem;
                                MerWorkTimeItem wtItemProd, wtItemInj, wtItemScoop;
                                MerComp merComp = new MerComp(w.Mer);

                                for (j = 0; j < merComp.Count; j++)
                                {
                                    compItem = merComp[j];

                                    #region По объектам
                                    for (k = 0; k < compItem.PlastItems.Count; k++)
                                    {
                                        plastItem = compItem.PlastItems[k];
                                        if (PlastList.IndexOf(plastItem.PlastCode) != -1)
                                        {
                                            wtItemProd = compItem.TimeItems.GetProductionTime(plastItem.PlastCode);
                                            wtItemInj = compItem.TimeItems.GetInjectionTime(plastItem.PlastCode);
                                            wtItemScoop = compItem.TimeItems.GetScoopTime(plastItem.PlastCode);
                                            hours = (compItem.Date.AddMonths(1) - compItem.Date).Days * 24;
                                            wtItemProd.WorkTime = (wtItemProd.WorkTime + wtItemProd.CollTime > hours) ? hours : wtItemProd.WorkTime + wtItemProd.CollTime;
                                            wtItemInj.WorkTime = (wtItemInj.WorkTime + wtItemInj.CollTime > hours) ? hours : wtItemInj.WorkTime + wtItemInj.CollTime;
                                            wtItemScoop.WorkTime = (wtItemScoop.WorkTime + wtItemScoop.CollTime > hours) ? hours : wtItemScoop.WorkTime + wtItemScoop.CollTime;

                                            indMonth = (compItem.Date.Year - minDate.Year) * 12 + compItem.Date.Month - minDate.Month;

                                            if (wtItemProd.WorkTime > 0)
                                            {
                                                SumMonth[indMonth].Fprod++;
                                                SumMonth[indMonth].WorkTimeProd += wtItemProd.WorkTime;
                                            }

                                            if (wtItemInj.WorkTime > 0)
                                            {
                                                SumMonth[indMonth].Finj++;
                                                SumMonth[indMonth].WorkTimeInj += wtItemInj.WorkTime;
                                            }

                                            if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;

                                            if (wtItemInj.WorkTime + wtItemProd.WorkTime + wtItemScoop.WorkTime > 0)
                                            {
                                                if (monthWellsArray[indMonth] == null) monthWellsArray[indMonth] = new List<Well>();
                                                if (monthWellsArray[indMonth].IndexOf(w) == -1) monthWellsArray[indMonth].Add(w);
                                            }
                                            for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                            {
                                                switch (compItem.CharWorkIds[c])
                                                {
                                                    case 11:
                                                        SumMonth[indMonth].Liq += plastItem.Liq * wK;
                                                        SumMonth[indMonth].Oil += plastItem.Oil * wK;
                                                        if (SumMonth[indMonth].Liq > 0)
                                                        {
                                                            SumMonth[indMonth].Watering = (SumMonth[indMonth].Liq - SumMonth[indMonth].Oil) / SumMonth[indMonth].Liq;
                                                        }
                                                        SumMonth[indMonth].LiqV += plastItem.LiqV * wK;
                                                        SumMonth[indMonth].OilV += plastItem.OilV * wK;
                                                        if (SumMonth[indMonth].LiqV > 0)
                                                        {
                                                            SumMonth[indMonth].WateringV = (SumMonth[indMonth].LiqV - SumMonth[indMonth].OilV) / SumMonth[indMonth].LiqV;
                                                        }
                                                        break;
                                                    case 12:
                                                        if (SumMonth[indMonth].NatGas < 0) SumMonth[indMonth].NatGas = 0;
                                                        SumMonth[indMonth].NatGas += plastItem.NatGas * wK;
                                                        break;
                                                    case 13:
                                                        SumMonth[indMonth].Scoop += plastItem.Scoop * wK;
                                                        break;
                                                    case 15:
                                                        if (SumMonth[indMonth].GasCondensate < 0) SumMonth[indMonth].GasCondensate = 0;
                                                        SumMonth[indMonth].GasCondensate += plastItem.Condensate * wK;
                                                        break;
                                                    case 20:
                                                        SumMonth[indMonth].Inj += plastItem.Inj * wK;
                                                        if (merComp.UseInjGas)
                                                        {
                                                            if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                            SumMonth[indMonth].InjGas += plastItem.InjGas * wK;
                                                        }
                                                        break;
                                                }
                                            }
                                            // по годам
                                            indYears = compItem.Date.Year - minDate.Year;
                                            if (wtItemProd.WorkTime > 0)
                                            {
                                                if (lastYearProd != compItem.Date.Year)
                                                {
                                                    SumYear[indYears].Fprod++;
                                                    lastYearProd = compItem.Date.Year;
                                                }
                                                SumYear[indYears].WorkTimeProd += wtItemProd.WorkTime;
                                            }
                                            if (wtItemInj.WorkTime > 0)
                                            {
                                                if (lastYearInj != compItem.Date.Year)
                                                {
                                                    SumYear[indYears].Finj++;
                                                    lastYearInj = compItem.Date.Year;
                                                }
                                                SumYear[indYears].WorkTimeInj += wtItemInj.WorkTime;
                                            }
                                            if (wtItemScoop.WorkTime > 0) SumMonth[indMonth].Fscoop++;
                                            if (wtItemInj.WorkTime + wtItemProd.WorkTime + wtItemScoop.WorkTime > 0)
                                            {
                                                if (yearWellsArray[indYears] == null) yearWellsArray[indYears] = new List<Well>();
                                                if (yearWellsArray[indYears].IndexOf(w) == -1) yearWellsArray[indYears].Add(w);
                                            }
                                            for (c = 0; c < compItem.CharWorkIds.Count; c++)
                                            {
                                                switch (compItem.CharWorkIds[c])
                                                {
                                                    case 11:
                                                        SumYear[indYears].Liq += plastItem.Liq * wK;
                                                        SumYear[indYears].Oil += plastItem.Oil * wK;
                                                        if (SumYear[indYears].Liq > 0)
                                                        {
                                                            SumYear[indYears].Watering = (SumYear[indYears].Liq - SumYear[indYears].Oil) / SumYear[indYears].Liq;
                                                        }
                                                        SumYear[indYears].LiqV += plastItem.LiqV * wK;
                                                        SumYear[indYears].OilV += plastItem.OilV * wK;
                                                        if (SumYear[indYears].LiqV > 0)
                                                        {
                                                            SumYear[indYears].WateringV = (SumYear[indYears].LiqV - SumYear[indYears].OilV) / SumYear[indYears].LiqV;
                                                        }
                                                        break;
                                                    case 12:
                                                        if (SumYear[indYears].NatGas < 0) SumYear[indYears].NatGas = 0;
                                                        SumYear[indYears].NatGas += plastItem.NatGas * wK;
                                                        break;
                                                    case 13:
                                                        SumYear[indYears].Scoop += plastItem.Scoop * wK;
                                                        break;
                                                    case 15:
                                                        if (SumYear[indYears].GasCondensate < 0) SumYear[indYears].GasCondensate = 0;
                                                        SumYear[indYears].GasCondensate += plastItem.Condensate * wK;
                                                        break;
                                                    case 20:
                                                        SumYear[indYears].Inj += plastItem.Inj * wK;
                                                        if (merComp.UseInjGas)
                                                        {
                                                            if (SumMonth[indMonth].InjGas < 0) SumMonth[indMonth].InjGas = 0;
                                                            SumYear[indYears].InjGas += plastItem.InjGas * wK;
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            worker.ReportProgress((int)((float)i * 100 / (float)WellList.Length), userState);
                        }
                        // Расчет компенсации
                        //if (pvt != null)
                        //{
                        //    double sumLiq = 0, sumOil = 0, sumInj = 0;
                        //    for (i = 0; i < SumYear.Length; i++)
                        //    {
                        //        sumLiq += SumYear[i].Liq;
                        //        sumOil += SumYear[i].Oil;
                        //        sumInj += SumYear[i].Inj;
                        //    }
                        //    if (SumMonth.Length > 0 && SumMonth[SumMonth.Length - 1].Liq > 0)
                        //    {
                        //        SumParamItem item = SumMonth[SumMonth.Length - 1];
                        //        this.Compensation = 100 * (item.Inj * pvt.WaterVolumeFactor) / ((item.Liq - item.Oil) * pvt.WaterVolumeFactor / pvt.WaterDensity + (item.Oil * pvt.OilVolumeFactor / pvt.OilDensity));
                        //    }
                        //    if (sumLiq > 0)
                        //    {
                        //        this.AccumCompensation = 100 * (sumInj * pvt.WaterVolumeFactor) / ((sumLiq - sumOil) * pvt.WaterVolumeFactor / pvt.WaterDensity + sumOil * pvt.OilVolumeFactor / pvt.OilDensity);
                        //    }
                        //}
                        List<Well> wells = new List<Well>();
                        for (i = 0; i < month; i++)
                        {
                            if (monthWellsArray[i] != null)
                            {
                                for (j = 0; j < monthWellsArray[i].Count; j++)
                                {
                                    if (wells.IndexOf(monthWellsArray[i][j]) == -1) wells.Add(monthWellsArray[i][j]);
                                }
                            }
                            SumMonth[i].LeadInAll = wells.Count;
                        }
                        wells.Clear();
                        for (i = 0; i < years; i++)
                        {
                            if (yearWellsArray[i] != null)
                            {
                                for (j = 0; j < yearWellsArray[i].Count; j++)
                                {
                                    if (wells.IndexOf(yearWellsArray[i][j]) == -1) wells.Add(yearWellsArray[i][j]);
                                }
                            }
                            SumYear[i].LeadInAll = wells.Count;
                        }
                        this.sumObjParams = new SumObjParameters(this.StratumCode, SumMonth, SumYear);
                        retValue = true;
                    }
                }
            }
            userState.Element = this.Name;
            worker.ReportProgress(100, userState);
            return retValue;
        }

        public bool ReadFromCache(BackgroundWorker worker, DoWorkEventArgs e, Oilfield of, BinaryReader file, int Version)
        {
            if (file != null)
            {
                int j, wellCount, wellIndex, oilObjCount;
                Name = file.ReadString();
                Index = file.ReadInt32();
                OilFieldAreaCode = file.ReadInt32();
                StratumCode = file.ReadInt32();

                wellCount = file.ReadInt32();
                if (wellCount > 0)
                {
                    WellList = new Well[wellCount];
                    WellCoefList = new double[wellCount];
                    for (j = 0; j < wellCount; j++)
                    {
                        wellIndex = file.ReadInt32();
                        WellList[j] = of.Wells[wellIndex];
                        WellList[j].AreaIndex = Index;
                        WellCoefList[j] = file.ReadDouble();
                    }
                }
                if (Version > 0)
                {
                    wellCount = file.ReadInt32();
                    if (wellCount > 0)
                    {
                        AllWellList = new Well[wellCount];
                        for (j = 0; j < wellCount; j++)
                        {
                            wellIndex = file.ReadInt32();
                            AllWellList[j] = of.Wells[wellIndex];
                        }
                    }
                }
                oilObjCount = file.ReadInt32();
                if (oilObjCount > 0)
                {
                    StratumList = new int[oilObjCount];
                    for (j = 0; j < oilObjCount; j++)
                    {
                        StratumList[j] = file.ReadInt32();
                    }
                }

                Params.ReadFromBin(file);

                PVTParamsItem pvtItem = PVTParamsItem.Empty;
                int pvtCount = file.ReadInt32();
                if (pvtCount > 0 && pvtItem.ReadFromBin(file))
                {
                    this.pvt = pvtItem;
                    this.pvt.KIN = Params.KIN;
                }
                contour.ReadFromCacheBin(file);
                contour.StratumCode = this.StratumCode;
#if DEBUG
                contour.Text2 = Params.InitOilReserv.ToString();
#endif
                X = contour.X;
                Y = contour.Y;
                return true;
            }
            return false;
        }
        public bool WriteToCache(BackgroundWorker worker, DoWorkEventArgs e, BinaryWriter file)
        {
            if (file != null)
            {
                file.Write(this.Name);
                file.Write(this.Index);
                file.Write(this.OilFieldAreaCode);
                file.Write(this.StratumCode);

                if (WellList != null)
                {
                    file.Write(WellList.Length);
                    for (int i = 0; i < WellList.Length; i++)
                    {
                        file.Write(WellList[i].Index);
                        file.Write(WellCoefList[i]);
                    }
                }
                else
                    file.Write(0);
                if (AllWellList != null)
                {
                    file.Write(AllWellList.Length);
                    for (int i = 0; i < AllWellList.Length; i++)
                    {
                        file.Write(AllWellList[i].Index);
                    }
                }
                else
                    file.Write(0);
                if (StratumList != null)
                {
                    file.Write(StratumList.Length);
                    for (int i = 0; i < StratumList.Length; i++)
                    {
                        file.Write(StratumList[i]);
                    }
                }
                else
                    file.Write(0);

                Params.WriteToBin(file);

                if (!pvt.IsEmpty)
                {
                    file.Write(1);
                    pvt.WriteToBin(file);
                }
                else
                    file.Write(0);

                contour.WriteToCacheBin(0, file);
                return true;
            }
            return false;

        }
    }
}
