﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using DataLoader.Objects.Base;
using DataLoader.DictionaryObjects;
using System.Windows.Forms;
using System.ComponentModel;

namespace DataLoader.Objects.Project
{
    public delegate void OnAsyncWorkCompleted();
    public sealed class Project : LoggingObject
    {
        public DictionaryCollection DictionaryList;
        public string ProjectPath { get; set; }
        public List<NGDU> NGDUList;
        public bool IsBusyLoadProj { get { return isBusyLoadProj; } }
        private bool isBusyLoadProj;

        public event OnAsyncWorkCompleted OnOilFieldDataLoaded;
        public event OnAsyncWorkCompleted OnContoursDataLoaded;
        public event OnAsyncWorkCompleted OnTable81DataLoaded;
        
        public Project(string ProjectPath) : base(LOGGING_OBJECT_TYPE.PROJECT)
        {
            this.ProjectPath = ProjectPath;
            DictionaryList = null;
            if (!Directory.Exists(ProjectPath)) Directory.CreateDirectory(ProjectPath);
            DirectoryInfo DirInfo = new DirectoryInfo(ProjectPath);
            Name = DirInfo.Name;
            NGDUList = new List<NGDU>();
        }

        public bool LoadProject()
        {
            bool result = false;
            if (!Directory.Exists(ProjectPath)) return result;
            
            #region Init Dictionary
            DictionaryList = new DataLoader.DictionaryObjects.DictionaryCollection(ProjectPath + "\\Dictionaries");
            DictionaryList.LoadDataFromFiles();
            #endregion

            if (DictionaryList != null)
            {
                OilFieldCoefDictionary OilFieldCoef = (OilFieldCoefDictionary)DictionaryList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT);
                OilFieldParamsDictionary OilFieldParamsDict = (OilFieldParamsDictionary)DictionaryList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS);

                int i, j;
                DirectoryInfo ProjDir = new DirectoryInfo(ProjectPath + "\\cache");
                if (ProjDir.Exists && OilFieldParamsDict != null)
                {
                    List<Oilfield> oilFields = new List<Oilfield>(OilFieldParamsDict.Count);
                    DirectoryInfo[] OilFieldsDir = ProjDir.GetDirectories();
                    Oilfield of;

                    for (int ofInd = 0; ofInd < OilFieldsDir.Length; ofInd++)
                    {
                        for (i = 0; i < OilFieldParamsDict.Count; i++)
                        {
                            if (OilFieldParamsDict[i].OilFieldName.Equals(OilFieldsDir[ofInd].Name, StringComparison.OrdinalIgnoreCase))
                            {
                                if (OilFieldParamsDict[i].NGDUCode != -1)
                                {
                                    oilFields.Add(new Oilfield(ProjectPath, DictionaryList));
                                    of = oilFields[oilFields.Count - 1];
                                    of.Index = oilFields.Count - 1;
                                }
                                else
                                {
                                    oilFields.Insert(0, new Oilfield(ProjectPath, DictionaryList));
                                    of = oilFields[0];
                                    for (j = 0; j < oilFields.Count; j++) oilFields[j].Index = j;
                                }
                                of.Name = OilFieldParamsDict[i].OilFieldName;
                                of.ParamsDict = OilFieldParamsDict[i];
                                for (j = 0; j < OilFieldCoef.Count; j++)
                                {
                                    if (OilFieldCoef[j].Code == of.ParamsDict.FieldCode)
                                    {
                                        of.CoefDict = OilFieldCoef[j];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    CreateNGDUList(oilFields);
                    result = true;
                }
            }
            return result;
        }

        #region ASYNC WORK
        // WORK PROGRESS
        void AsyncWorkProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            NewProgress(e.ProgressPercentage);
        }
        
        // LOAD PROJECT
        public void LoadOilFieldDataAsync()
        {
            isBusyLoadProj = true;
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(LoadOilFieldData);
            worker.ProgressChanged += new ProgressChangedEventHandler(AsyncWorkProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LoadOilFieldDataCompleted);
            worker.RunWorkerAsync();
        }
        void LoadOilFieldDataCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (OnOilFieldDataLoaded != null) OnOilFieldDataLoaded();
            isBusyLoadProj = false;
        }
        void LoadOilFieldData(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            int countMax = 0;
            for (int i = 0; i < NGDUList.Count; i++)
            {
                countMax += NGDUList[i].OilFields.Count;
            }
            int count = 0;
            for (int i = 0; i < NGDUList.Count; i++)
            {
                for (int j = 0; j < NGDUList[i].OilFields.Count; j++)
                {
                    NGDUList[i].OilFields[j].LoadOilObjCodesFromCache();
                    NGDUList[i].OilFields[j].LoadWellsFromCache(worker, e);
                    NGDUList[i].OilFields[j].LoadCoordFromCache(worker, e);
                    NGDUList[i].OilFields[j].LoadAreasFromCache(worker, e);
                    NGDUList[i].OilFields[j].LoadContoursFromCache(worker, e, true);
                    NGDUList[i].OilFields[j].LoadGridHeadFromCache(worker, e);
                    NGDUList[i].OilFields[j].LoadProjWellsFromCache(worker, e);
                    count++;
                    worker.ReportProgress((int)(count * 100f / countMax));
                }
            }            
        }
      
        // LOAD CONTOURS
        public void LoadContoursFromDirectoryAsync(string ContoursPath)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(LoadContoursFromDirectory);
            worker.ProgressChanged += new ProgressChangedEventHandler(AsyncWorkProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LoadContoursCompleted);
            worker.RunWorkerAsync(ContoursPath);
        }
        void LoadContoursCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(OnContoursDataLoaded != null)  OnContoursDataLoaded();
        }
        private void LoadContoursFromDirectory(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            int i, j, k;
            NGDU ngdu;
            Oilfield of;
            string ContoursPath = (string)e.Argument;
            if (Directory.Exists(ContoursPath))
            {
                DirectoryInfo root = new DirectoryInfo(ContoursPath);
                DirectoryInfo[] ngduList = root.GetDirectories();
                FileInfo[] files;
                
                Contour cntrSrc, cntr;
                StreamReader sr;
                bool repl;
                int progress;
                NewMessage("Загрузка условных контуров с папки");
                List<Oilfield> NeedSaveOilFields = new List<Oilfield>();

                for (i = 0; i < ngduList.Length; i++)
                {
                    ngdu = GetNGDU(ngduList[i].Name);
                    if (ngdu != null)
                    {
                        files = ngduList[i].GetFiles("*.cntr");
                        if (ngdu.Name == "Общие данные")
                        {
                            of = ngdu.OilFields[0];
                            for (j = 0; j < files.Length; j++)
                            {
                                sr = new StreamReader(files[j].FullName, Encoding.GetEncoding(1251));
                                sr.ReadLine(); sr.ReadLine(); sr.ReadLine(); sr.ReadLine(); sr.ReadLine(); // пропускаем количество
                                cntrSrc = new Contour();
                                cntrSrc.ReadFromCache(sr);

                                cntrSrc.ContourType = -2;
                                cntrSrc.OilFieldCode = -1;
                                cntrSrc.FillBrush = 11;
#if DEBUG
             //                   cntrSrc.NGDUCode = 3; 
#endif
                                sr.Close();
                                

                                repl = false;

                                if (of.ContoursLoaded)
                                {
                                    for (k = 0; k < of.Contours.Count; k++)
                                    {
                                        cntr = of.Contours[k];
                                        if (cntr.Name == cntrSrc.Name)
                                        {
                                            cntrSrc.FillBrush = 11;
                                            of.Contours[k] = cntrSrc;
                                            repl = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    of.Contours = new List<Contour>();
                                }

                                if (!repl)
                                {
                                    of.Contours.Add(cntrSrc);
                                }
                                
                                NeedSaveOilFields.Add(of);
                            }
                        }
                        else
                        {
                            string oilFieldName;
                            for (j = 0; j < files.Length; j++)
                            {
                                oilFieldName = files[j].Name.Replace(".cntr", "");
                                of = ngdu.GetOilField(oilFieldName);
                                if (of != null)
                                {
                                    sr = new StreamReader(files[j].FullName, Encoding.GetEncoding(1251));
                                    sr.ReadLine(); sr.ReadLine(); sr.ReadLine(); sr.ReadLine(); sr.ReadLine(); // пропускаем количество
                                    cntrSrc = new Contour();
                                    cntrSrc.ReadFromCache(sr);
                                    cntrSrc.ContourType = -3;
                                    cntrSrc.OilFieldCode = of.ParamsDict.FieldCode;
                                    cntrSrc.FillBrush = 11;

                                    repl = false;
                                    //if (count > 1)
                                    //{
                                    //    if (cntrSrc.cntrsOut == null) cntrSrc.cntrsOut = new List<Contour>(1);
                                    //    Contour cntrOut = new Contour();
                                    //    cntrOut.ReadFromCache(sr);
                                    //    cntrOut.CCW = false;
                                    //    cntrSrc.cntrsOut.Add(cntrOut);
                                    //}
                                    sr.Close();
                                    sr.Dispose();
                                    of.LoadContoursDataFromCache(worker, e);
                                    if (of.ContoursLoaded)
                                    {
                                        k = 0;
                                        while (k < of.Contours.Count)
                                        {
                                            if (of.Contours[k].ContourType == -3)
                                            {
                                                of.Contours.RemoveAt(k);
                                            }
                                            else
                                            {
                                                k++;
                                            }
                                        }
                                        for (k = 0; k < of.Contours.Count; k++)
                                        {
                                            cntr = of.Contours[k];
                                            if (cntr.ContourType == -3)
                                            {
                                                of.Contours[k] = cntrSrc;
                                                repl = true;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        of.Contours = new List<Contour>();
                                    }

                                    if (!repl)
                                    {
                                        of.Contours.Insert(0, cntrSrc);
                                    }
                                    
                                    NeedSaveOilFields.Add(of);
                                }
                            }
                            
                        }
                    }
                    progress = (int)((i + 1) * 100F / ngduList.Length);
                    if (progress > 100) progress = 100;
                    worker.ReportProgress(progress);
                }
                for (i = 0; i < NeedSaveOilFields.Count; i++)
                {
                    NeedSaveOilFields[i].WriteContoursToCache(worker, e);
                }
            }
        }

        // LOAD TABLE 81
        public void LoadTable81FromFileAsync(string Table81Path)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(LoadTables81);
            worker.ProgressChanged += new ProgressChangedEventHandler(AsyncWorkProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LoadTable81Completed);
            worker.RunWorkerAsync(Table81Path);
        }
        void LoadTable81Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (OnTable81DataLoaded != null) OnTable81DataLoaded();
        }
        private void LoadTables81(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            bool retValue = false;
            int startFileYear;
            string OFname;
            char[] delimeter = new char[] { ';' };
            string[] parseStr;
            string str, SourcePath;
            
            if ((e.Argument != null) && (File.Exists((string)e.Argument)))
            {
                SourcePath = (string)e.Argument;
                StreamReader file = new StreamReader(SourcePath, System.Text.Encoding.GetEncoding(1251));
                str = file.ReadLine();                  // шапка - года
                parseStr = str.Split(delimeter);
                startFileYear = Convert.ToInt32(parseStr[4]);
                str = file.ReadLine();                  // месторождение
                parseStr = str.Split(delimeter);
                OFname = parseStr[1];
                Oilfield of;
                bool find;

                while ((str != null) && (str != ""))
                {
                    find = false;
                    for (int j = 0; j < NGDUList.Count; j++)
                    {
                        for (int i = 0; i < NGDUList[j].OilFields.Count; i++)
                        {
                            of = NGDUList[j].OilFields[i];
                            if (of.Name.Equals(OFname, StringComparison.OrdinalIgnoreCase))
                            {
                                find = true;
                                retValue = of.LoadTable81ParamsFromFile(worker, e, file, startFileYear);
                                break;
                            }
                        }
                        if (find) break;
                    }
                    if (!find)
                    {
                        MessageBox.Show("Не найдено месторождение " + OFname);
                        worker.ReportProgress(100);
                        for (int i = 0; i < 8; i++) file.ReadLine();
                    }
                    if (!file.EndOfStream)
                    {
                        str = file.ReadLine();
                        parseStr = str.Split(delimeter);
                        OFname = parseStr[1];
                    }
                    else str = null;
                }
            }
        }
        #endregion

        public bool CreateEmptyOilFields()
        {
             bool result = false;
             if ((DictionaryList != null) && (Directory.Exists(ProjectPath)))
             {
                 OilFieldCoefDictionary OilFieldCoef = (OilFieldCoefDictionary)DictionaryList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT);
                 OilFieldParamsDictionary OilFieldParamsDict = (OilFieldParamsDictionary)DictionaryList.GetDictionary(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS);

                 int i, j;
                 if (!Directory.Exists(ProjectPath + "\\cache")) Directory.CreateDirectory(ProjectPath + "\\cache");
                 DirectoryInfo ProjDir = new DirectoryInfo(ProjectPath + "\\cache");
                 List<Oilfield> oilFields = new List<Oilfield>();
                 Oilfield of;

                 for (i = 0; i < OilFieldParamsDict.Count; i++)
                 {
                     if (OilFieldParamsDict[i].NGDUCode != -1)
                     {
                         if (OilFieldParamsDict[i].Port == 0) continue;
                         oilFields.Add(new Oilfield(ProjectPath, DictionaryList));
                         of = oilFields[oilFields.Count - 1];
                         of.Index = oilFields.Count - 1;
                     }
                     else
                     {
                         oilFields.Insert(0, new Oilfield(ProjectPath, DictionaryList));
                         of = oilFields[0];
                         for (j = 0; j < oilFields.Count; j++) oilFields[j].Index = j;
                     }
                     of.Name = OilFieldParamsDict[i].OilFieldName;
                     of.ParamsDict = OilFieldParamsDict[i];
                     for (j = 0; j < OilFieldCoef.Count; j++)
                     {
                         if (OilFieldCoef[j].Code == of.ParamsDict.FieldCode)
                         {
                             of.CoefDict = OilFieldCoef[j];
                             break;
                         }
                     }
                     if (!Directory.Exists(ProjDir.FullName + "\\" + of.Name)) Directory.CreateDirectory(ProjDir.FullName + "\\" + of.Name);
                 }
                 StreamWriter sw = new StreamWriter(ProjectPath + "\\" + Name + ".txt", false, Encoding.UTF8);
                 sw.WriteLine(Name);
                 sw.WriteLine("1631.4614432");
                 string str = string.Empty;
                 for (i = 0; i < oilFields.Count; i++)
                 {
                     if (oilFields[i].ParamsDict.FieldCode > 0)
                     {
                         str += string.Format("{0};", oilFields[i].ParamsDict.FieldCode);
                     }
                 }
                 sw.WriteLine(str);
                 sw.Close();
                 CreateNGDUList(oilFields);
             }
             return result;
        }

        public class UpdateFileItem
        {
            public string Type;
            public string FileName;
            public DateTime CreationTime;
            public DateTime CacheCreationTime;
            public string CreationUser;
            public bool IsLoaded;
            public bool IsPreLoaded;
            public DateTime PreLoadCreationTime;
            public string CachePath;
            public string ServerPath;
            public string PreLoadPath;
            public int FileSize;
        }
        public class UpdateOilFieldItem
        {
            public string Oilfield;
            public bool IsPreLoaded;
            public bool IsLoaded;
            List<UpdateFileItem> UpdateItemList;

            public UpdateOilFieldItem()
            {
                UpdateItemList = new List<UpdateFileItem>(6);
            }
            public int Count
            {
                get { return UpdateItemList.Count; }
            }

            public void Add(UpdateFileItem item)
            {
                UpdateItemList.Add(item);
                IsLoaded = true;
            }
            public void Clear()
            {
                UpdateItemList.Clear();
            }
            public UpdateFileItem this[int index]
            {
                get { return UpdateItemList[index]; }
            }
            public UpdateFileItem GetItemByFileName(string FileName)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (this[i].FileName == FileName)
                    {
                        return this[i];
                    }
                }
                return null;
            }
        }
        string GetFileTypeName(string FileName)
        {
            switch (FileName)
            {
                case "wellList.bin":
                    return "Список скважин";

                case "coord.bin":
                    return "Координаты скважин";

                case "contours.bin":
                    return "Контуры";

                case "areas.bin":
                    return "Ячейки заводнения";

                case "areas_param.bin":
                    return "Параметры ячеек заводнения";

                case "wellpads.bin":
                    return "Кусты";

                case "gis.bin":
                    return "Данные ГИС";

                case "perf.bin":
                    return "Данные Перфорации";

                case "logs.bin":
                    return "Данные каротажа";

                case "logs_base.bin":
                    return "Данные базового каротажа";

                case "grids.bin":
                    return "Сетки";

                case "OilObj.bin":
                    return "Список объектов";

                case "mer_fast.bin":
                    return "Данные МЭР fast";
					
		        case "mer.bin":
                     return "Данные МЭР";

                case "chess_fast.bin":
                    return "Данные Шахматки fast";
					
		        case "chess.bin":
                    return "Данные Шахматки";

                case "chessinj_fast.bin":
                    return "Данные Шахматки fast";

                case "sum.bin":
                    return "Суммарные показатели";

                case "sum_chess.bin":
                    return "Суммарные показатели по Шахматке";

                case "table81.bin":
                    return "Проектные показатели";

                case "PVTParams.bin":
                    return "PVT данные";

                case "core.bin":
                    return "Данные наличия керна";

                case "core_test.bin":
                    return "Данные исследований керна";

                case "gtm.bin":
                    return "Данные ГТМ";

                case "well_action.bin":
                    return "Данные мероприятий на скважине";

                case "well_research.bin":
                    return "Данные исследований на скважине";

                case "OilfieldCoefficient.csv":
                    return "Справочник коэффициентов";

                case "OilfieldParams.csv":
                    return "Справочник месторождений";

                case "Stratum.csv":
                    return "Справочник пластов";

                case "StratumGeo.csv":
                    return "Справочник пропластков";

                case "StratumTree.csv":
                    return "Справочник иерархия пластов";

                case "PerfType.csv":
                    return "Справочник Тип перфорации";

                case "CharWork.csv":
                    return "Справочник Характер работы";

                case "WellState.csv":
                    return "Справочник Состояние";

                case "WellMethod.csv":
                    return "Справочник Метод работы";

                case "Saturation.csv":
                    return "Справочник Насыщение";

                case "Construction.csv":
                    return "Справочник Конструкция";

                case "OilfieldArea.csv":
                    return "Справочник Площадь";

                case "GridType.csv":
                    return "Справочник Тип сетки";

                case "Lithology.csv":
                    return "Справочник литология";

                case "LogUnit.csv":
                    return "Справочник ед.изм.каротажа";

                case "LogMnemonic.csv":
                    return "Справочник мнемоника каротажа";
                case "GtmType.csv":
                    return "Справочник тип ГТМ";
                case "WellActionType.csv":
                    return "Справочник тип ремонта";

                case "WellResearchType.csv":
                    return "Справочник тип исследования";

                

                default:
                    if (FileName.IndexOf("delta") != -1)
                    {
                        return "Файл обновления";
                    }
                    return string.Empty;
            }
        }
        public bool CreateProjectUpdateFile()
        {
            bool res = false;
            if (Directory.Exists(Path.Combine(this.ProjectPath, "cache")))
            {
                DirectoryInfo dirInfoCache = new DirectoryInfo(Path.Combine(this.ProjectPath, "cache"));
                DirectoryInfo dirInfoDict = new DirectoryInfo(Path.Combine(this.ProjectPath, "Dictionaries"));
                DirectoryInfo[] dirsCache = dirInfoCache.GetDirectories();
                FileInfo[] files;
                DirectoryInfo dir;
                List<UpdateOilFieldItem> of_list = new List<UpdateOilFieldItem>();
                List<UpdateFileItem> dict_list = new List<UpdateFileItem>();
                DateTime cacheTime;
                string CacheFile, filePath;

                int i, j, len = dirsCache.Length;
                UpdateOilFieldItem oilfield;
                UpdateFileItem updateItem;
                for (i = 0; i <= len; i++)
                {
                    if (i < len)
                    {
                        dir = dirsCache[i];
                        filePath = Path.Combine(ProjectPath, Path.Combine("cache", dir.Name));
                    }
                    else
                    {
                        dir = dirInfoCache;
                        filePath = Path.Combine(this.ProjectPath, "cache");
                    }

                    files = dir.GetFiles();

                    oilfield = new UpdateOilFieldItem();
                    oilfield.Oilfield = dir.Name;
                    foreach (FileInfo file in files)
                    {
                        CacheFile = Path.Combine(filePath, file.Name);
                        cacheTime = File.GetLastWriteTime(CacheFile);

                        string type = GetFileTypeName(file.Name);
                        if (type.Length == 0) continue;

                        updateItem = new UpdateFileItem();
                        updateItem.Type = type;
                        updateItem.CacheCreationTime = cacheTime;
                        updateItem.FileName = file.Name;
                        updateItem.FileSize = (int)file.Length;
                        updateItem.CreationUser = string.Empty;
                        oilfield.Add(updateItem);
                    }
                    if (oilfield.Count > 0) of_list.Add(oilfield);
                }
                oilfield = new UpdateOilFieldItem();
                oilfield.Oilfield = "Dictionaries";

                dirsCache = dirInfoDict.GetDirectories();

                for (i = 0; i <= dirsCache.Length; i++)
                {
                    if (i < dirsCache.Length)
                    {
                        files = dirsCache[i].GetFiles();
                        filePath = Path.Combine(ProjectPath, Path.Combine("Dictionaries", dirsCache[i].Name));
                    }
                    else
                    {
                        files = dirInfoDict.GetFiles();
                        filePath = Path.Combine(this.ProjectPath, "Dictionaries");
                    }
                    for (j = 0; j < files.Length; j++)
                    {
                        CacheFile = Path.Combine(filePath, files[j].Name);
                        cacheTime = File.GetLastWriteTime(CacheFile);
                        string type = GetFileTypeName(files[j].Name);
                        if (type.Length == 0) continue;
                        updateItem = new UpdateFileItem();
                        updateItem.Type = type;
                        updateItem.CacheCreationTime = cacheTime;
                        if (i < dirsCache.Length)
                        {
                            updateItem.FileName = dirsCache[i].Name + '\\' + files[j].Name;
                        }
                        else
                        {
                            updateItem.FileName = files[j].Name;
                        }
                        updateItem.FileSize = (int)files[j].Length;
                        updateItem.CreationUser = string.Empty;
                        oilfield.Add(updateItem);
                    }
                }
                files = dirInfoDict.GetFiles();

                if (oilfield.Count > 0) of_list.Add(oilfield);

                if (of_list.Count > 0)
                {
                    FileStream fs = new FileStream(Path.Combine(this.ProjectPath, "prjUpd.bin"), FileMode.Create);
                    BinaryWriter bfw = new BinaryWriter(fs);
                    bfw.Write(of_list.Count);
                    for (i = 0; i < of_list.Count; i++)
                    {
                        oilfield = of_list[i];
                        bfw.Write(oilfield.Oilfield);
                        bfw.Write(oilfield.Count);
                        for (j = 0; j < oilfield.Count; j++)
                        {
                            bfw.Write(oilfield[j].FileName);
                            bfw.Write(oilfield[j].CacheCreationTime.ToOADate());
                            bfw.Write(oilfield[j].FileSize);
                        }
                    }
                    bfw.Close();
                    File.SetCreationTime(Path.Combine(this.ProjectPath, "prjUpd.bin"), DateTime.Now);
                    File.SetLastWriteTime(Path.Combine(this.ProjectPath, "prjUpd.bin"), DateTime.Now);
                }
            }
            res = (Directory.Exists(this.ProjectPath) && File.Exists(Path.Combine(this.ProjectPath, "prjUpd.bin")));
            return res;
        }

        #region NGDU
        public void FreeDataMemory()
        {
            for (int i = 0; i < NGDUList.Count; i++)
            {
                NGDUList[i].FreeDataMemory(false);
            }
            GC.GetTotalMemory(true);
        }
        void CreateNGDUList(List<Oilfield> Oilfields)
        {
            NGDU ngdu;
            for (int i = 0; i < Oilfields.Count; i++)
            {
                ngdu = GetNGDU(Oilfields[i].ParamsDict.NGDUCode);
                if (ngdu == null)
                {
                    ngdu = new NGDU(NGDUList.Count, Oilfields[i].ParamsDict.NGDUCode, Oilfields[i].ParamsDict.NGDUName);
                    if ((NGDUList.Count == 0) || (NGDUList[NGDUList.Count - 1].Code < ngdu.Code))
                    {
                        NGDUList.Add(ngdu);
                    }
                    else
                    {
                        for (int j = 0; j < NGDUList.Count; j++)
                        {
                            if (NGDUList[j].Code > ngdu.Code)
                            {
                                NGDUList.Insert(j, ngdu);
                                break;
                            }
                        }
                    }
                }
                ngdu.OilFields.Add(Oilfields[i]);
            }
        }
        public NGDU GetNGDU(int NGDUCode)
        {
            for (int i = 0; i < NGDUList.Count; i++)
            {
                if (NGDUList[i].Code == NGDUCode)
                {
                    return NGDUList[i];
                }
            }
            return null;
        }
        public NGDU GetNGDU(string NGDUName)
        {
            for (int i = 0; i < NGDUList.Count; i++)
            {
                if (NGDUList[i].Name == NGDUName)
                {
                    return NGDUList[i];
                }
            }
            return null;
        }
        #endregion
    }
}