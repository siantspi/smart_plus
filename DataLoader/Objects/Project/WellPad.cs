﻿using System;
using System.Collections.Generic;
using DataLoader.Objects.Base;
using System.IO;

using DataLoader.Objects.Data;

namespace DataLoader.Objects.Project
{
    public sealed class WellPad : BaseObject
    {
        public static int Version = 0;
        double X { get; set; }
        double Y { get; set; }
        public List<Well> Wells { get; set; }
        PointsXY Contour { get; set; }

        public WellPad()
        {
            Wells = new List<Well>();
            X = 0;
            Y = 0;
        }
        public WellPad(string WellPadName) : this()
        {
            Name = WellPadName;
        }

        void CreateContour()
        {
            int i, j;
            int max_index = -1, next_index;
            bool init = true;
            double x1, y1, x2, y2;
            double x_max = double.MinValue, x_min = double.MaxValue;
            double y_max = double.MinValue, y_min = double.MaxValue;
            double x_mean = 0, y_mean = 0, num_mean = 0;
            double pred_alpha, max_alpha, alpha;
            Well w, w2;
            int[] temp_index = new int[Wells.Count];

            for (i = 0; i < Wells.Count; i++)
            {
                w = Wells[i];
                if (init)
                {
                    if ((w.X != Constant.DOUBLE_NAN) && (w.Y != Constant.DOUBLE_NAN))
                    {
                        x_max = w.X;
                        x_min = w.X;
                        y_max = w.Y;
                        y_min = w.Y;
                        max_index = i;
                    }
                    init = false;
                }
                if ((w.X != Constant.DOUBLE_NAN) && (w.Y != Constant.DOUBLE_NAN))
                {
                    x_mean += w.X;
                    y_mean += w.Y;
                    num_mean++;
                    if (x_max < w.X) { x_max = w.X; max_index = i; }
                    if (x_min > w.X) x_min = w.X;
                    if (y_max < w.Y) y_max = w.Y;
                    if (y_min > w.Y) y_min = w.Y;
                }
            }
            X = x_min + (x_max - x_min) / 2;
            Y = y_min + (y_max - y_min) / 2;

            if ((max_index >= 0) && (Wells.Count > 2))
            {
                temp_index[0] = max_index;
                pred_alpha = Constant.DOUBLE_NAN;
                for (i = 1; i < Wells.Count; i++)
                {
                    w = Wells[temp_index[i - 1]];
                    x1 = w.X; y1 = w.Y;
                    next_index = -1;
                    max_alpha = Constant.DOUBLE_NAN;
                    for (j = 0; j < Wells.Count; j++)
                    {
                        if (j != temp_index[i - 1])
                        {
                            w2 = Wells[j];
                            x2 = w2.X;
                            y2 = w2.Y;
                            if ((x2 != Constant.DOUBLE_NAN) && (y2 != Constant.DOUBLE_NAN))
                            {
                                alpha = Constant.DOUBLE_NAN;
                                if ((x2 >= x1) && (y2 >= y1)) if (x2 == x1) alpha = 90; else alpha = Math.Atan((y2 - y1) / (x2 - x1)) / Math.PI * 180;
                                if ((x2 < x1) && (y2 >= y1)) if (y2 == y1) alpha = 180; else alpha = 180 - Math.Atan((y2 - y1) / (x1 - x2)) / Math.PI * 180;
                                if ((x2 <= x1) && (y2 < y1)) if (x2 == x1) alpha = 270; else alpha = 180 + Math.Atan((y1 - y2) / (x1 - x2)) / Math.PI * 180;
                                if ((x2 > x1) && (y2 < y1)) alpha = 360 - Math.Atan((y1 - y2) / (x2 - x1)) / Math.PI * 180;
                                if ((pred_alpha != Constant.DOUBLE_NAN) && (alpha < pred_alpha)) alpha += 360;
                                if ((max_alpha == Constant.DOUBLE_NAN) || (alpha < max_alpha))
                                {
                                    next_index = j;
                                    max_alpha = alpha;
                                }
                            }
                        }
                    }
                    if ((next_index < 0) || (next_index == temp_index[0])) break;
                    temp_index[i] = next_index;
                    pred_alpha = max_alpha;
                }
            }
            if (i > 2)
            {
                Contour = new PointsXY(i);
                for (j = 0; j < i; j++)
                {
                    w = Wells[temp_index[j]];
                    Contour.Add(w.X, w.Y);
                }
            }
        }

        public void ReadFromCache(BinaryReader br, Oilfield of)
        {
            Name = br.ReadString();
            int count = br.ReadInt32();
            int wellIndex, wellId;
            for (int i = 0; i < count; i++)
            {
                wellIndex = br.ReadInt32();
                wellId = br.ReadInt32();
                if (of.Wells[wellIndex].WellId == wellId)
                {
                    Wells.Add(of.Wells[wellIndex]);
                }
            }
            CreateContour();
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(Name);
            bw.Write(Wells.Count);
            for (int i = 0; i < Wells.Count; i++)
            {
                bw.Write(Wells[i].Index);
                bw.Write(Wells[i].WellId);
            }
        }
    }
}
