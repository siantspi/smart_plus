﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Base;
using System.Windows.Forms;

namespace DataLoader.Objects.Project
{
    public sealed class NGDU : BaseObject
    {
        public int Code { get; set; }
        public List<Oilfield> OilFields { get; set; }

        public NGDU(int Index, int Code, string Name)
        {
            OilFields = new List<Oilfield>();
            this.Index = Index;
            this.Code = Code;
            this.Name = Name;
        }

        public TreeNode GetNGDUNode()
        {
            TreeNode ngduNode = new TreeNode(Name);
            for (int i = 0; i < OilFields.Count; i++)
            {
                ngduNode.Nodes.Add(OilFields[i].GetOilFieldNode());
            }
            return ngduNode;
        }

        public void FreeDataMemory(bool useGC)
        {
            for (int i = 0; i < OilFields.Count; i++)
            {
                OilFields[i].FreeDataMemory(false);
            }
            if (useGC) GC.GetTotalMemory(true);
        }
        public Oilfield GetOilField(string OilFieldName)
        {
            for (int i = 0; i < OilFields.Count; i++)
            {
                if (OilFieldName.Equals(OilFields[i].Name, StringComparison.OrdinalIgnoreCase))
                {
                    return OilFields[i];
                }
            }
            return null; 
        }
    }
}
