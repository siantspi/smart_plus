﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLoader.Objects.Data;
using System.IO;
using DataLoader.Objects.Base;

using DataLoader.DictionaryObjects;
using System.Drawing;

namespace DataLoader.Objects.Project
{
    public class Contour : BaseObject
    {
        public static int Version = 0;
        public int StratumCode;
        public bool IsServerStratumCode = false;
        public int OilFieldCode;
        public int NGDUCode;
        public int EnterpriseCode;
        public bool Closed;
        public bool CCW;
        public int FillBrush;
        public int ContourType;
        public double _Value;
        public PointsXY _points = null;
        public List<Contour> cntrsOut = null;
        PointD[] cilia = null;
        int ciliaLastPoint = 0;

        public bool ColorMode;
        public bool VisibleName;
        public bool ShowBizPlanParams;
        public string Text;
        public string Text2;
        StratumDictionary dict;
        Color lineClr = Color.Empty;
        public int lineWidth;
        public bool Enable;
        public bool Visible;
        public double X, Y;

        public double[] BizPlanParams = null;
        public List<PointF> _debugPoints;
        Brush brush;
        public Brush fillBrush;

        Pen pen, penThin, penFat;

        public Color LineColor
        {
            get { return lineClr; }
            set
            {
                lineClr = value;
                brush = new SolidBrush(value);
                if (lineWidth == -1)
                {
                    penThin = new Pen(value, 1);
                    penFat = new Pen(value, 2);
                }
                else
                {
                    pen = new Pen(value, lineWidth);
                }
            }
        }
        public int LineWidth
        {
            get { return lineWidth; }
            set
            {
                lineWidth = value;
                if (lineWidth == -1)
                {
                    penThin = new Pen(lineClr, 1);
                    penFat = new Pen(lineClr, 2);
                }
                else
                {
                    pen = new Pen(lineClr, lineWidth);
                }
            }
        }

        public bool _Fantom;
        public bool _ShiftPoint;

        public double Xmin, Xmax, Ymin, Ymax;

        public Contour()
        {
            Closed = false;
            this.Enable = true;
            _Value = 0;
            ContourType = 0;
            _Fantom = false;
            _ShiftPoint = false;
            FillBrush = -1;
            fillBrush = null;
            StratumCode = -1;
            OilFieldCode = -1;
            NGDUCode = -1;
            EnterpriseCode = -1;
            brush = null;
            pen = null;
            Text = null;
            Text2 = null;
            ColorMode = true;
            ShowBizPlanParams = false;
            lineWidth = -1;
            Name = string.Empty;
            X = Constant.DOUBLE_NAN;
            Y = Constant.DOUBLE_NAN;
            Xmin = Constant.DOUBLE_NAN;
            Xmax = Constant.DOUBLE_NAN;
            Ymin = Constant.DOUBLE_NAN;
            Ymax = Constant.DOUBLE_NAN;
        }
        public Contour(int numPoints) : this()
        {
            _points = new PointsXY(numPoints);
        }
        public Contour(Contour cntr)
        {
            if (cntr.Name != null) Name = cntr.Name.Replace("\"", "");
            Closed = cntr.Closed;
            Enable = cntr.Enable;
            _Value = cntr._Value;
            ContourType = cntr.ContourType;
           
            FillBrush = cntr.FillBrush;
            fillBrush = cntr.fillBrush;
            StratumCode = cntr.StratumCode;
            OilFieldCode = cntr.OilFieldCode;
            NGDUCode = cntr.NGDUCode;
            EnterpriseCode = cntr.EnterpriseCode;
            Visible = cntr.Visible;
            FillBrush = cntr.FillBrush;
            CCW = cntr.CCW;
            lineWidth = cntr.lineWidth;
            _Fantom = false;
            _ShiftPoint = false;
            brush = null;
            fillBrush = null;
            pen = null;
            Text = null;
            Text2 = null;
            ColorMode = true;

            Xmin = cntr.Xmin;
            Xmax = cntr.Xmax;
            Ymin = cntr.Ymin;
            Ymax = cntr.Ymax;
            X = cntr.X;
            Y = cntr.Y;
            _points = new PointsXY(cntr._points);

            cntrsOut = cntr.cntrsOut;

        }
        public Contour(PointD[] points) : this()
        {
            _points = new PointsXY(points);
        }

        public void Add(double X, double Y)
        {
            _points.Add(X, Y);
        }
        public void Insert(int index, double X, double Y)
        {
            if ((index > -1) && (index < _points.Count))
            {
                _points.Insert(index, X, Y);
            }
            else if (index == _points.Count)
            {
                _points.Add(X, Y);
            }
            SetMinMax();
            GetMedian();
        }
        public void SetPointByIndex(int index, double X, double Y)
        {
            if ((index > -1) && (index < _points.Count))
            {
                PointD point;
                point.X = X;
                point.Y = Y;
                _points[index] = point;
            }
            SetMinMax();
            GetMedian();
        }
        public void DelPointByIndex(int index)
        {
            if ((index > -1) && (index < _points.Count))
            {
                _points.RemoveAt(index);
            }
            SetMinMax();
            GetMedian();
        }
        public void SetDictPlast(StratumDictionary plastDict)
        {
            this.dict = plastDict;
        }

        public void GetCenter()
        {

            double x1 = _points.GetMinX();
            double x2 = _points.GetMaxX();
            double y1 = _points.GetMinY();
            double y2 = _points.GetMaxY();
            X = (x1 + x2) / 2;
            Y = (y1 + y2) / 2;

            // GetMedian();
        }
        public void GetMedian()
        {
            PointD ave = _points.GetAvarage();
            X = ave.X;
            Y = ave.Y;
        }
        public void SetMinMax()
        {
            Xmin = _points.GetMinX();
            Xmax = _points.GetMaxX();
            Ymin = _points.GetMinY();
            Ymax = _points.GetMaxY();
        }
        public double GetPerimeter()
        {
            double p = 0;
            for (int i = 0; i < _points.Count - 1; i++)
            {
                p += Math.Sqrt(Math.Pow(_points[i].X - _points[i + 1].X, 2) + Math.Pow(_points[i].Y - _points[i + 1].Y, 2));
            }
            if ((this.Closed) && (_points.Count > 0))
            {
                p += Math.Sqrt(Math.Pow(_points[_points.Count - 1].X - _points[0].X, 2) + Math.Pow(_points[_points.Count - 1].Y - _points[0].Y, 2));
            }
            return p;
        }
        public bool PointBoundsEntry(double X, double Y)
        {
            if ((Xmin < X) && (X < Xmax) && (Ymin < Y) && (Y < Ymax))
            {
                return true;
            }
            return false;
        }
        public bool IntersectBounds(Contour cntr)
        {
            if (((cntr.Ymin < Ymin) && (cntr.Ymax < Ymin)) || ((Ymax < cntr.Ymin) && (Ymax < cntr.Ymax)))
            {
                return false;
            }
            if (((cntr.Xmin < Xmin) && (cntr.Xmax < Xmin)) || ((Xmax < cntr.Xmin) && (Xmax < cntr.Xmax)))
            {
                return false;
            }
            return true;
        }

        public bool LineIntersectBounds(float lineX1, float lineY1, float lineX2, float lineY2, RectangleF rect)
        {
            float f;
            float minX, maxX, minY, maxY;
            minX = lineX1;
            maxX = lineX2;
            if (lineX1 > lineX2)
            {
                minX = lineX2;
                maxX = lineX1;
            }
            minY = lineY1;
            maxY = lineY2;
            if (lineY1 > lineY2)
            {
                minY = lineY2;
                maxY = lineY1;
            }
            f = (rect.Y - lineY1) * (lineX2 - lineX1) / (lineY2 - lineY1) + lineX1;
            if ((f >= rect.X) && (f <= rect.Right) && (f > minY) && (f < maxY)) return true;

            f = (rect.Bottom - lineY1) * (lineX2 - lineX1) / (lineY2 - lineY1) + lineX1;
            if ((f >= rect.X) && (f <= rect.Right) && (f > minY) && (f < maxY)) return true;

            f = (rect.X - lineX1) * (lineY2 - lineY1) / (lineX2 - lineX1) + lineY1;
            if ((f >= rect.Y) && (f <= rect.Bottom) && (f > minX) && (f < maxX)) return true;

            f = (rect.Right - lineX1) * (lineY2 - lineY1) / (lineX2 - lineX1) + lineY1;
            if ((f >= rect.Y) && (f <= rect.Bottom) && (f > minX) && (f < maxX)) return true;

            return false;
        }
        public void ResetBrush()
        {
            brush = null;
        }
        public bool ContourInBounds(int MinX, int MinY, int MaxX, int MaxY)
        {
            int i;
            if ((_points != null) && (_points.Count > 0))
            {
                if (((Xmin >= MinX) && (Xmin < MaxX)) || ((Xmax >= MinX) && (Xmax < MaxX)) ||
                   ((Ymin >= MinY) && (Ymin < MaxY)) || ((Ymax >= MinY) && (Ymax < MaxY)))
                {
                    return true;
                }
                for (i = 0; i < _points.Count; i++)
                {
                    if (((_points[i].X > MinX) && (_points[i].X < MaxX)) || ((_points[i].Y > MinY) && (_points[i].Y < MaxY)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool PointBodyEntry(double pX, double pY)
        {
            int i;
            double X;
            PointD p1, p2;
            bool parity = false;
            for (i = 0; i < _points.Count; i++)
            {
                p1 = _points[i];
                if (i < _points.Count - 1)
                    p2 = _points[i + 1];
                else
                    p2 = _points[0];
                X = ((pY - p1.Y) / (p2.Y - p1.Y)) * (p2.X - p1.X) + p1.X;

                if ((pX > X) && (((pY >= p1.Y) && (pY <= p2.Y)) || ((pY >= p2.Y) && (pY <= p1.Y))))
                {
                    if (((pY != p1.Y) && (pY != p2.Y)) || ((pY == p1.Y) && (pY < p2.Y)) || ((pY == p2.Y) && (pY < p1.Y)))
                    {
                        parity = !parity;
                    }
                }
            }
            return parity;
        }

        public int GetIndexPointByXY(double realX, double realY)
        {
            int k = -1;
            if ((_points != null) && (_points.Count > 0))
            {
                for (int i = 0; i < _points.Count; i++)
                {
                    if ((Math.Abs(_points[i].X - realX) < 0.000000001) && (Math.Abs(_points[i].Y - realY) < 0.000000001))
                        k = i;
                }
            }
            return k;
        }
        public int GetIndexPointByXY2(double realX, double realY)
        {
            int k = -1;
            if ((_points != null) && (_points.Count > 0))
            {
                for (int i = 0; i < _points.Count; i++)
                {
                    if ((Math.Abs(_points[i].X - realX) < 3) && (Math.Abs(_points[i].Y - realY) < 3))
                        k = i;
                }
            }
            return k;
        }
        public void Invert()
        {
            if ((this._points != null) && (this._points.Count > 0))
            {
                PointsXY newPoints = new PointsXY(this._points.Count);
                for (int i = 0; i < this._points.Count; i++)
                {
                    newPoints.Add(this._points[this._points.Count - 1 - i].X, this._points[this._points.Count - 1 - i].Y);
                }
                this._points = newPoints;
            }
        }
        public void CreateCilia(double Distance, bool ByLeftSide)
        {
            if ((_points != null) && (_points.Count > 1))
            {
                double len = _points.GetLength();
                len += _points.GetDistanceXY(_points.Count - 1, 0);
                int n = (int)(len / Distance);
                cilia = new PointD[n * 2];
                int i, j, k;
                double sumLen = 0, rest = 0;
                k = 0; i = 0;

                for (i = 0; i < _points.Count; i++)
                {
                    j = (i < _points.Count - 1) ? i + 1 : 0;
                    if (j == 0) ciliaLastPoint = k;
                    len = _points.GetDistanceXY(i, j);
                    sumLen += len;

                    if (sumLen > Distance)
                    {
                        while (sumLen > Distance)
                        {
                            cilia[k] = Geometry.GetPointInLineByDistance(_points[i], _points[j], rest + Distance);
                            cilia[k + 1] = Geometry.GetNormalPointInLine(_points[i], _points[j], cilia[k], Distance / 2, ByLeftSide);
                            sumLen -= Distance;
                            rest += Distance;
                            k += 2;
                        }
                        rest = -sumLen;
                    }
                    else
                    {
                        rest -= len;
                    }
                }
            }
        }
        public Contour GetSimplyContour(double Distance)
        {
            Contour cntr = null;
            if ((_points != null) && (_points.Count > 1))
            {
                cntr = new Contour(this);
                cntr.ContourType = this.ContourType;
                double len = _points.GetLength();
                len += _points.GetDistanceXY(_points.Count - 1, 0);
                int n = (int)(len / Distance);
                if (n > 2)
                {
                    cntr._points = new PointsXY(n);
                    int i, j, k;
                    double sumLen = 0, rest = 0;
                    k = 0; i = 0;
                    PointD pt;
                    cntr._points.Add(_points[0].X, _points[0].Y);
                    for (i = 0; i < _points.Count; i++)
                    {
                        j = (i < _points.Count - 1) ? i + 1 : 0;
                        len = _points.GetDistanceXY(i, j);
                        sumLen += len;

                        if (sumLen > Distance)
                        {
                            while (sumLen > Distance)
                            {
                                pt = Geometry.GetPointInLineByDistance(_points[i], _points[j], rest + Distance);
                                cntr._points.Add(pt.X, pt.Y);
                                sumLen -= Distance;
                                rest += Distance;
                                k++;
                            }
                            rest = -sumLen;
                        }
                        else
                        {
                            rest -= len;
                        }
                    }
                }
            }
            return cntr;
        }
        public bool GetClockWiseByX(bool ByMinimum)
        {
            bool result = false;
            int index = -1;
            double X = Constant.DOUBLE_NAN;
            for (int i = 0; i < _points.Count; i++)
            {
                if (ByMinimum)
                {
                    if ((X == Constant.DOUBLE_NAN) || (X > _points[i].X))
                    {
                        X = _points[i].X;
                        index = i;
                    }
                }
                else
                {
                    if ((X == Constant.DOUBLE_NAN) || (X < _points[i].X))
                    {
                        X = _points[i].X;
                        index = i;
                    }
                }
            }
            if (index != -1)
            {
                int i1, i2;
                i1 = index - 1;
                i2 = index + 1;
                if (i1 < 0) i1 = _points.Count - 1;
                if (i2 == _points.Count) i2 = 0;
                if (((_points[i1].Y < _points[index].Y) && (_points[index].Y < _points[i2].Y)) ||
                    ((_points[i1].Y == _points[index].Y) && (_points[index].Y < _points[i2].Y)) ||
                    ((_points[i1].Y < _points[index].Y) && (_points[index].Y == _points[i2].Y)))
                {
                    result = true;
                }
                else if (((_points[i2].Y < _points[index].Y) && (_points[index].Y < _points[i1].Y)) ||
                         ((_points[i2].Y == _points[index].Y) && (_points[index].Y < _points[i1].Y)) ||
                         ((_points[i2].Y < _points[index].Y) && (_points[index].Y == _points[i1].Y)))
                {
                    result = false;
                }
                else if ((_points[i1].Y < _points[index].Y) && (_points[i2].Y < _points[index].Y))
                {
                    double d1 = Math.Sqrt(Math.Pow(_points[i1].X - _points[index].X, 2) + Math.Pow(_points[i1].Y - _points[index].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(_points[i2].X - _points[index].X, 2) + Math.Pow(_points[i2].Y - _points[index].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i2], d1);
                        if (_points[i1].Y < pt.Y) result = true; else result = false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i1], d2);
                        if (pt.Y < _points[i2].Y) result = true; else result = false;
                    }
                }
                else if ((_points[i1].Y > _points[index].Y) && (_points[i2].Y > _points[index].Y))
                {
                    double d1 = Math.Sqrt(Math.Pow(_points[i1].X - _points[index].X, 2) + Math.Pow(_points[i1].Y - _points[index].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(_points[i2].X - _points[index].X, 2) + Math.Pow(_points[i2].Y - _points[index].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i2], d1);
                        if (_points[i1].Y < pt.Y) result = true; else result = false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i1], d2);
                        if (pt.Y < _points[i2].Y) result = true; else result = false;
                    }
                }
            }
            if (!ByMinimum) result = !result;
            return result;
        }
        public bool GetClockWiseByY(bool ByMinimum)
        {
            bool result = false;
            int index = -1;
            double Y = Constant.DOUBLE_NAN;
            for (int i = 0; i < _points.Count; i++)
            {
                if (ByMinimum)
                {
                    if ((Y == Constant.DOUBLE_NAN) || (Y < _points[i].Y))
                    {
                        Y = _points[i].Y;
                        index = i;
                    }
                }
                else
                {
                    if ((Y == Constant.DOUBLE_NAN) || (Y > _points[i].Y))
                    {
                        Y = _points[i].Y;
                        index = i;
                    }
                }
            }
            if (index != -1)
            {
                int i1, i2;
                i1 = index - 1;
                i2 = index + 1;
                if (i1 < 0) i1 = _points.Count - 1;
                if (i2 == _points.Count) i2 = 0;
                if (((_points[i1].X < _points[index].X) && (_points[index].X < _points[i2].X)) ||
                    ((_points[i1].X == _points[index].X) && (_points[index].X < _points[i2].X)) ||
                    ((_points[i1].X < _points[index].X) && (_points[index].X == _points[i2].X)))
                {
                    result = true;
                }
                else if (((_points[i2].X < _points[index].X) && (_points[index].X < _points[i1].X)) ||
                         ((_points[i2].X == _points[index].X) && (_points[index].X < _points[i1].X)) ||
                         ((_points[i2].X < _points[index].X) && (_points[index].X == _points[i1].X)))
                {
                    result = true;
                }
                else if ((_points[i1].X < _points[index].X) && (_points[i2].X < _points[index].X))
                {
                    double d1 = Math.Sqrt(Math.Pow(_points[i1].X - _points[index].X, 2) + Math.Pow(_points[i1].Y - _points[index].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(_points[i2].X - _points[index].X, 2) + Math.Pow(_points[i2].Y - _points[index].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i2], d1);
                        if (_points[i1].X < pt.X) result = true; else result = false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i1], d2);
                        if (pt.X < _points[i2].X) result = true; else result = false;
                    }
                }
                else if ((_points[i1].X > _points[index].X) && (_points[i2].X > _points[index].X))
                {
                    double d1 = Math.Sqrt(Math.Pow(_points[i1].X - _points[index].X, 2) + Math.Pow(_points[i1].Y - _points[index].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(_points[i2].X - _points[index].X, 2) + Math.Pow(_points[i2].Y - _points[index].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i2], d1);
                        if (_points[i1].X < pt.X) result = true; else result = false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(_points[index], _points[i1], d2);
                        if (pt.X < _points[i2].X) result = true; else result = false;
                    }
                }
            }
            if (!ByMinimum) result = !result;
            return result;
        }

        private bool IsPointsByCW(PointsXY points)
        {
            int minXind = -1;
            double minX = Constant.DOUBLE_NAN;
            for (int i = 0; i < points.Count; i++)
            {
                if ((minX == Constant.DOUBLE_NAN) || (points[i].X < minX))
                {
                    minX = points[i].X;
                    minXind = i;
                }
            }
            if (minXind != -1)
            {
                int i1, i2;
                i1 = minXind - 1;
                i2 = minXind + 1;
                if (i1 < 0) i1 = points.Count - 1;
                if (i2 == points.Count) i2 = 0;
                if (((points[i1].Y < points[minXind].Y) && (points[minXind].Y < points[i2].Y)) ||
                    ((points[i1].Y == points[minXind].Y) && (points[minXind].Y < points[i2].Y)) ||
                    ((points[i1].Y < points[minXind].Y) && (points[minXind].Y == points[i2].Y)))
                {
                    return true;
                }
                else if (((points[i2].Y < points[minXind].Y) && (points[minXind].Y < points[i1].Y)) ||
                         ((points[i2].Y == points[minXind].Y) && (points[minXind].Y < points[i1].Y)) ||
                         ((points[i2].Y < points[minXind].Y) && (points[minXind].Y == points[i1].Y)))
                {
                    return false;
                }
                else if ((points[i1].Y < points[minXind].Y) && (points[i2].Y < points[minXind].Y))
                {
                    double d1 = Math.Sqrt(Math.Pow(points[i1].X - points[minXind].X, 2) + Math.Pow(points[i1].Y - points[minXind].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(points[i2].X - points[minXind].X, 2) + Math.Pow(points[i2].Y - points[minXind].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(points[minXind], points[i2], d1);
                        if (points[i1].Y < pt.Y) return true; else return false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(points[minXind], points[i1], d2);
                        if (pt.Y < points[i2].Y) return true; else return false;
                    }
                }
                else if ((points[i1].Y > points[minXind].Y) && (points[i2].Y > points[minXind].Y))
                {
                    double d1 = Math.Sqrt(Math.Pow(points[i1].X - points[minXind].X, 2) + Math.Pow(points[i1].Y - points[minXind].Y, 2));
                    double d2 = Math.Sqrt(Math.Pow(points[i2].X - points[minXind].X, 2) + Math.Pow(points[i2].Y - points[minXind].Y, 2));
                    PointD pt;
                    if (d1 < d2)
                    {
                        pt = Geometry.GetPointInLineByDistance(points[minXind], points[i2], d1);
                        if (points[i1].Y < pt.Y) return true; else return false;
                    }
                    else
                    {
                        pt = Geometry.GetPointInLineByDistance(points[minXind], points[i1], d2);
                        if (pt.Y < points[i2].Y) return true; else return false;
                    }
                }
            }
            return false;
        }

        public int ReadHeadLineFromCacheBin(BinaryReader file)
        {
            if (file != null)
            {
                int j, iLen, Index;
                PointD point;

                this.Name = file.ReadString();
                this.ContourType = file.ReadInt32();
                Index = file.ReadInt32();
                this.Closed = file.ReadBoolean();

                this.StratumCode = file.ReadInt32();
                this.OilFieldCode = file.ReadInt32();
                this.NGDUCode = file.ReadInt32();
                this.EnterpriseCode = file.ReadInt32();
                this.Visible = file.ReadBoolean();

                this.FillBrush = file.ReadInt16();
                this.X = file.ReadDouble();
                this.Y = file.ReadDouble();
                this.Xmin = file.ReadDouble();
                this.Xmax = file.ReadDouble();
                this.Ymin = file.ReadDouble();
                this.Ymax = file.ReadDouble();

                iLen = file.ReadInt32();
                if (this.ContourType < 0)
                {
                    this._points = new PointsXY(iLen);

                    for (j = 0; j < iLen; j++)
                    {
                        point.X = file.ReadDouble();
                        point.Y = file.ReadDouble();
                        this.Add(point.X, point.Y);
                    }

                    SetMinMax();
                    if (this.X == Constant.DOUBLE_NAN) GetCenter();
                }
                else
                {
                    file.BaseStream.Seek(iLen * 16, SeekOrigin.Current);
                }

                iLen = Convert.ToInt32(file.ReadInt32());
                if (iLen > 0)
                {
                    cntrsOut = new List<Contour>();
                    for (j = 0; j < iLen; j++)
                    {
                        Contour cntrOut = new Contour();
                        cntrOut.ReadFromCacheBin(file);
                        cntrsOut.Add(cntrOut);
                    }
                }
                return Index;
            }
            return -1;
        }
        public int ReadFromCache(StreamReader file)
        {
            if (file != null)
            {
                int j, iLen, Index;
                PointD point;
                char[] parser = new char[] { ';' };
                string tempStr;
                string[] parseStr;
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";

                string str = file.ReadLine();
                parseStr = str.Split(parser);
                this.Name = parseStr[0];
                if (parseStr.Length > 1 && parseStr[1].Length > 0) this.Text = parseStr[1].Replace("%_%", "\n");
                if (parseStr.Length > 2 && parseStr[2].Length > 0) this.Text2 = parseStr[2].Replace("%_%", "\n");

                str = file.ReadLine();
                this.ContourType = Convert.ToInt32(str);
                tempStr = file.ReadLine();
                if (tempStr.IndexOf('e') > -1)
                {
                    Index = -1;
                    this.Closed = Convert.ToBoolean(tempStr);
                }
                else
                {
                    Index = Convert.ToInt32(tempStr);
                    this.Closed = Convert.ToBoolean(file.ReadLine());
                }
                this.StratumCode = Convert.ToInt32(file.ReadLine());
                this.OilFieldCode = Convert.ToInt32(file.ReadLine());
                this.NGDUCode = Convert.ToInt32(file.ReadLine());
                tempStr = file.ReadLine();
                if (tempStr.IndexOf('e') == -1)
                {
                    this.EnterpriseCode = Convert.ToInt32(tempStr);
                    lineClr = Color.Empty;
                    tempStr = file.ReadLine();
                    if (tempStr.IndexOf('e') == -1)
                    {
                        parseStr = tempStr.Split(parser, StringSplitOptions.RemoveEmptyEntries);
                        LineColor = Color.FromArgb(Convert.ToInt32(parseStr[0]));
                        if (parseStr.Length > 1) LineWidth = Convert.ToInt32(parseStr[1]);
                        tempStr = file.ReadLine();
                    }
                }

                parseStr = tempStr.Split(parser, StringSplitOptions.RemoveEmptyEntries);
                this.Visible = Convert.ToBoolean(parseStr[0]);
                if (parseStr.Length > 1) this.Enable = Convert.ToBoolean(parseStr[1]);
                if (parseStr.Length > 2) this.VisibleName = Convert.ToBoolean(parseStr[2]);

                this.FillBrush = Convert.ToInt16(file.ReadLine());
                //this.fillBrush = new SolidBrush(Color.FromArgb(Convert.ToInt32(file.ReadLine())));

                tempStr = file.ReadLine();
                if (tempStr.IndexOf(';') > -1)
                {
                    parseStr = tempStr.Split(parser);
                    this.X = Convert.ToDouble(parseStr[0], nfi);
                    this.Y = Convert.ToDouble(parseStr[1], nfi);
                    iLen = Convert.ToInt32(file.ReadLine());
                }
                else
                    iLen = Convert.ToInt32(tempStr);


                this._points = new PointsXY(iLen);

                for (j = 0; j < iLen; j++)
                {
                    parseStr = file.ReadLine().Split(parser);
                    point.X = Convert.ToDouble(parseStr[0], nfi);
                    point.Y = Convert.ToDouble(parseStr[1], nfi);
                    this.Add(point.X, point.Y);
                }

                SetMinMax();
                if (this.X == Constant.DOUBLE_NAN) GetCenter();
                //Get
                return Index;
            }
            return -1;
        }
        public int ReadFromCacheBin(BinaryReader file)
        {
            if (file != null)
            {
                int j, iLen, Index;
                PointD point;

                Name = file.ReadString();
                ContourType = file.ReadInt32();
                Index = file.ReadInt32();
                Closed = file.ReadBoolean();

                StratumCode = file.ReadInt32();
                OilFieldCode = file.ReadInt32();
                NGDUCode = file.ReadInt32();
                EnterpriseCode = file.ReadInt32();
                Visible = file.ReadBoolean();

                FillBrush = file.ReadInt16();

                X = file.ReadDouble();
                Y = file.ReadDouble();
                Xmin = file.ReadDouble();
                Xmax = file.ReadDouble();
                Ymin = file.ReadDouble();
                Ymax = file.ReadDouble();

                iLen = file.ReadInt32();

                _points = new PointsXY(iLen);

                for (j = 0; j < iLen; j++)
                {
                    point.X = file.ReadDouble();
                    point.Y = file.ReadDouble();
                    Add(point.X, point.Y);
                }
                int outCount = file.ReadInt32();
                if (outCount > 0)
                {
                    cntrsOut = new List<Contour>();
                    Contour cntrOut = new Contour();
                    cntrOut.ReadFromCacheBin(file);
                    cntrsOut.Add(cntrOut);
                }
                SetMinMax();
                return Index;
            }
            return -1;
        }
        public int ReadPointsFromCacheBin(BinaryReader file)
        {
            if (file != null)
            {
                int j, iLen, Index = 0;
                PointD point;

                file.ReadString();
                file.BaseStream.Seek(76, SeekOrigin.Current);
                iLen = file.ReadInt32();
                this._points = new PointsXY(iLen);
                for (j = 0; j < iLen; j++)
                {
                    point.X = file.ReadDouble();
                    point.Y = file.ReadDouble();
                    this.Add(point.X, point.Y);
                }
                SetMinMax();
                if (this.X == Constant.DOUBLE_NAN) GetCenter();
                //Get
                return Index;
            }
            return -1;
        }

        public bool WriteToCache(int LayerIndex, StreamWriter file)
        {
            if (file != null)
            {
                int j;
                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                nfi.NumberGroupSeparator = " ";
                string str = this.Name;
                str += ";";
                if (this.Text != null && this.Text.Length > 0) str += Text.Replace("\n", "%_%");
                str += ";";
                if (this.Text2 != null && this.Text2.Length > 0) str += Text2.Replace("\n", "%_%");
                file.WriteLine(str);
                file.WriteLine(this.ContourType);
                file.WriteLine(LayerIndex);
                file.WriteLine(this.Closed);
                file.WriteLine(this.StratumCode);
                file.WriteLine(this.OilFieldCode);
                file.WriteLine(this.NGDUCode);
                file.WriteLine(this.EnterpriseCode);
                if (LineColor.ToArgb() != Color.Empty.ToArgb())
                {
                    file.WriteLine(this.LineColor.ToArgb().ToString() + ((lineWidth > -1) ? ";" + lineWidth.ToString() : string.Empty));
                }
                file.WriteLine(this.Visible.ToString() + ";" + Enable.ToString() + ";" + VisibleName.ToString());

                file.WriteLine(this.FillBrush);
                file.WriteLine(String.Format("{0};{1}", this.X.ToString(nfi), this.Y.ToString(nfi)));

                file.WriteLine(this._points.Count.ToString());
                for (j = 0; j < this._points.Count; j++)
                {
                    file.WriteLine(((PointD)this._points[j]).X.ToString(nfi) + ";" + ((PointD)this._points[j]).Y.ToString(nfi));
                }
                return true;
            }
            return false;
        }
        public bool WriteToCacheBin(int LayerIndex, BinaryWriter file)
        {
            if (file != null)
            {
                int j;
                file.Write(this.Name);
                file.Write(this.ContourType);
                file.Write(LayerIndex);
                file.Write(this.Closed);
                file.Write(this.StratumCode);
                file.Write(this.OilFieldCode);
                file.Write(this.NGDUCode);
                file.Write(this.EnterpriseCode);
                file.Write(this.Visible);
                file.Write((Int16)this.FillBrush);
                file.Write(this.X);
                file.Write(this.Y);
                file.Write(this.Xmin);
                file.Write(this.Xmax);
                file.Write(this.Ymin);
                file.Write(this.Ymax);


                file.Write(this._points.Count);
                for (j = 0; j < this._points.Count; j++)
                {
                    file.Write(((PointD)this._points[j]).X);
                    file.Write(((PointD)this._points[j]).Y);
                }
                if (cntrsOut != null)
                {
                    file.Write(cntrsOut.Count);
                    for (j = 0; j < cntrsOut.Count; j++)
                    {
                        cntrsOut[j].WriteToCacheBin(0, file);
                    }
                }
                else
                {
                    file.Write(0);
                }
                return true;
            }
            return false;
        }
        public void FreeDataMemory()
        {
            cntrsOut.Clear();
            cntrsOut = null;
            _points = null;
            cilia = null;
            penThin = null;
            penFat = null;
            brush = null;
            fillBrush = null;
        }
    }
}
