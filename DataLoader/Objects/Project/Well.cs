﻿using System;
using System.Collections.Generic;

using System.IO;
using DataLoader.Objects.Base;
using DataLoader.Objects.Data;
using System.Drawing;
using DataLoader.DictionaryObjects;


namespace DataLoader.Objects.Project
{
    public class Well : BaseObject
    {
        public static int Version = 0;

        public double X { get; set; }
        public double Y { get; set; }

        public bool Missing { get; set; }

        public int WellId { get; set; }
        public int BoreId { get; set; }
        public int MainBoreIndex { get; set; }
        public int OilFieldAreaCode { get; set; }
        public DateTime StartDate { get; set; }

        public int OilFieldIndex { get; set; }
        public int AreaIndex { get; set; }

        public double AreaCoef { get; set; }
        public int CountAreas { get; set; }
        public byte ProjectDest { get; set; }

        #region Well Data
        public Well[] SideBores { get; set; }
        public SkvCoord Coord { get; set; }
        public Mer Mer { get; set; }
        public WellChess Chess { get; set; }
        public WellChessInj ChessInj { get; set; }
        public SkvGIS Gis { get; set; }
        public SkvLog[] LogsBase { get; set; }
        public SkvLog[] Logs { get; set; }
        public MemoryStream PackedLogs = null;
        public MemoryStream PackedLogsBase = null;
        public SkvPerf Perf { get; set; }
        public SkvCore Core { get; set; }
        public SkvCoreTest CoreTest { get; set; }
        public WellGTM Gtm { get; set; }
        public SkvGtm SkvGtm { get; set; } // должен остаться только один
        public WellRepairAction RepairAct { get; set; }
        public WellResearch Research { get; set; }
        public WellSuspend Suspend { get; set; }
        public WellsIconList Icons { get; set; }
        #endregion

        #region DataLoaded
        public bool CoordLoaded { get { return ((Coord != null) && (Coord.Count > 0)); } }
        public bool MerLoaded { get { return ((Mer != null) && (Mer.Count > 0)); } }
        public bool ChessLoaded { get { return ((Chess != null) && (Chess.Count > 0)); } }
        public bool ChessInjLoaded { get { return ((ChessInj != null) && (ChessInj.Count > 0)); } }
        public bool GisLoaded { get { return ((Gis != null) && (Gis.Count != 0)); } }
        public bool PerfLoaded { get { return ((Perf != null) && (Perf.Count > 0)); } }
        public bool LogsBaseLoaded { get { return ((LogsBase != null) && (LogsBase.Length > 0)); } }
        public bool LogsLoaded { get { return ((Logs != null) && (Logs.Length > 0)); } }
        public bool CoreLoaded { get { return ((Core != null) && (Core.Count > 0)); } }
        public bool CoreTestLoaded { get { return ((CoreTest != null) && (CoreTest.Count > 0)); } }
        public bool GtmLoaded { get { return ((Gtm != null) && (Gtm.Count > 0)); } }
        public bool RepairActionLoaded { get { return ((RepairAct != null) && (RepairAct.Count > 0)); } }
        public bool ResearchLoaded { get { return ((Research != null) && (Research.Count > 0)); } }
        public bool SuspendLoaded { get { return ((Suspend != null) && (Suspend.Count > 0)); } }
        #endregion

        public Well(int Index)
        {
            this.Index = Index;
            this.MainBoreIndex = Index;
            this.Name = Name;
            OilFieldIndex = -1;
            OilFieldAreaCode = -1;
            ProjectDest = 0;
            AreaCoef = 1;
            CountAreas = 0;
            SideBores = null;
            Coord = null;
            Mer = null;
            Chess = null;
            ChessInj = null;
            Gis = null;
            LogsBase = null;
            Logs = null;
            Perf = null;
            Core = null;
            CoreTest = null;
            Gtm = null;
            RepairAct = null;
            Research = null;
            Icons = new WellsIconList();
        }

        #region COORD
        public void ReCalcCoordinates(OilfieldCoefItem coef)
        {
            if (CoordLoaded)
            {
                double xG, yG;
                for (int i = 0; i < Coord.Count; i++)
                {
                    if ((Coord.Items[i].X != 0) && (Coord.Items[i].Y != 0))
                    {
                        X = Coord.Items[i].X;
                        Y = Coord.Items[i].Y;
                        break;
                    }
                }
                if ((X == 0) && (Y == 0))
                {
                    X = Coord.Altitude.X;
                    Y = Coord.Altitude.Y;
                }
                coef.GlobalCoord_FromLocal(X, Y, out xG, out yG);
                X = xG;
                Y = yG;
                coef.GlobalCoord_FromLocal(Coord.Altitude.X, Coord.Altitude.Y, out xG, out yG);
                Coord.Altitude.X = (int)xG;
                Coord.Altitude.Y = (int)yG;
                for (int i = 0; i < Coord.Count; i++)
                {
                    coef.GlobalCoord_FromLocal(Coord.Items[i].X, Coord.Items[i].Y, out xG, out yG);
                    Coord.Items[i].X = (int)xG;
                    Coord.Items[i].Y = (int)yG; 
                }
            }
        }
        #endregion

        #region PERF
        public void AddPerforation(SkvPerf newPerf)
        {
            if (this.Perf == null || this.Perf.Count == 0)
            {
                this.Perf = newPerf;
            }
            else if (newPerf != null)
            {
                int i, j;
                bool added;
                SkvPerfItem item;
                List<SkvPerfItem> newItems = new List<SkvPerfItem>();
                for (i = 0; i < Perf.Count; i++)
                {
                    newItems.Add(Perf.Items[i]);
                }
                for (i = 0; i < newPerf.Count; i++)
                {
                    j = 0;
                    if (newPerf.Items[i].Date > newItems[newItems.Count - 1].Date)
                    {
                        newItems.Add(newPerf.Items[i]);
                    }
                    else
                    {
                        added = false;
                        for (j = 0; j < newItems.Count; j++)
                        {
                            if ((newPerf.Items[i].Date == newItems[j].Date) &&
                                (Math.Abs(newPerf.Items[i].Top - newItems[j].Top) < 0.1) &&
                                (Math.Abs(newPerf.Items[i].Bottom - newItems[j].Bottom) < 0.1))
                            {
                                added = true;
                                if ((newItems[j].PerfTypeId != 10000) && (newPerf.Items[i].PerfTypeId == 10000))
                                {
                                    item = newItems[j];
                                    item.Source = newPerf.Items[i].Source;
                                    item.PerfTypeId = 10000;
                                    newItems[j] = item;
                                }
                                break;
                            }
                            else if (newPerf.Items[i].Date < newItems[j].Date)
                            {
                                newItems.Insert(j, newPerf.Items[i]);
                                added = true;
                                break;
                            }
                        }
                        if (!added) newItems.Add(newPerf.Items[i]);
                    }
                }
                if (newItems.Count > Perf.Count)
                {
                    SkvPerfItem[] items = new SkvPerfItem[newItems.Count];
                    for (i = 0; i < newItems.Count; i++)
                    {
                        items[i] = newItems[i];
                    }
                    Perf.SetItems(items);
                }
            }
        }
        public void ReSetOisPerfByWellBores(string mainKey, Well[] boreList, List<int> BorePerfCounts, SkvPerf OisPerf)
        {
            if (OisPerf != null)
            {
                if (boreList == null || BorePerfCounts.Count < 2)
                {
                    this.AddPerforation(OisPerf);
                }
                else
                {
                    Well w = this;
                    SkvPerf tempPerf;
                    DateTime dtMin;
                    int j, k = 0, k2 = 0;
                    SkvPerfItem[] perfItems;
                    dtMin = OisPerf.Items[0].Date;
                    for (int i = -1; i < boreList.Length; i++)
                    {
                        if (i > -1) w = boreList[i];
                        k = k2;
                        k2 = ((i < boreList.Length - 1) && (i < BorePerfCounts.Count - 1)) ? k + BorePerfCounts[i + 1] : OisPerf.Count;
                        if (k2 > 0)
                        {
                            perfItems = new SkvPerfItem[k2 - k];
                            for (j = k; j < k2; j++)
                            {
                                perfItems[j - k] = OisPerf.Items[j];
                            }
                            tempPerf = new SkvPerf();
                            tempPerf.SetItems(perfItems);
                            w.AddPerforation(tempPerf);
                        }
                    }
                    perfItems = null;
                }
            }
        }
        #endregion

        #region LOAD LOGS
        public void PackLogs(MemoryStream buff)
        {
            if (LogsLoaded)
            {
                BinaryWriter bw = new BinaryWriter(buff);
                bw.Write(Logs.Length);
                for (int i = 0; i < Logs.Length; i++)
                {
                    Logs[i].RetainItems(bw);
                    Logs[i] = null;
                }
                Logs = null;
                PackedLogs = ConvertEx.PackStream(buff);
                PackedLogs.Seek(0, SeekOrigin.Begin);
            }
        }
        public void PackLogsBase(MemoryStream buff)
        {
            if (LogsBaseLoaded)
            {
                BinaryWriter bw = new BinaryWriter(buff);
                bw.Write(LogsBase.Length);
                for (int i = 0; i < LogsBase.Length; i++)
                {
                    LogsBase[i].RetainItems(bw);
                    LogsBase[i] = null;
                }
                LogsBase = null;
                PackedLogsBase = ConvertEx.PackStream(buff);
                PackedLogsBase.Seek(0, SeekOrigin.Begin);
            }
        }
        #endregion

        #region ICONS
        public void ReSetIcons(OilfieldCoefItem coef, DateTime MaxMerDate)
        {
            WellIconCode[] codes = null;
            MerItem item;
            item.PlastId = 0;
            this.Icons.Clear();
            double dt;
            int x;
            int defColor;
            List<int> objList = new List<int>();

            if ((this.ProjectDest == 0) && (this.MerLoaded))
            {
                this.Icons.Clear();
                dt = Mer.Items[Mer.Count - 1].Date.ToOADate();
                x = Mer.Count - 1;
                item = Mer.Items[x];
                while ((x > -1) && (dt == item.Date.ToOADate()))
                {
                    item = Mer.Items[x];
                    if ((item.PlastId > 0) && (objList.IndexOf(item.PlastId) == -1))
                    {
                        objList.Add(item.PlastId);

                        #region Добывающие скважины
                        if ((item.CharWorkId == 11) || (item.CharWorkId == 12) || (item.CharWorkId == 15))
                        {
                            switch (item.CharWorkId)
                            {
                                case 11:
                                    defColor = Color.FromArgb(35, 35, 35).ToArgb(); // black
                                    break;
                                case 12:
                                    defColor = Color.FromArgb(255, 215, 0).ToArgb(); // gold
                                    break;
                                case 15:
                                    defColor = Color.FromArgb(35, 35, 35).ToArgb(); // black
                                    break;
                                default:
                                    defColor = Color.FromArgb(35, 35, 35).ToArgb(); // black
                                    break;
                            }
                            if (item.StateId == 6)
                            {
                                codes = new WellIconCode[1];
                                codes[0].FontCode = 0;
                                codes[0].IconColor = defColor;
                                codes[0].IconColorDefault = defColor;
                                codes[0].CharCode = 45;
                                codes[0].Size = 10;
                            }
                            else if (item.StateId == 5)
                            {
                                codes = new WellIconCode[1];
                                codes[0].FontCode = 0;
                                codes[0].IconColor = defColor;
                                codes[0].IconColorDefault = defColor;
                                codes[0].CharCode = 46;
                                codes[0].Size = 10;
                            }
                            if (item.MethodId == 12)
                            {
                                codes = new WellIconCode[2];
                                codes[0].FontCode = 0;
                                codes[0].IconColor = defColor;
                                codes[0].IconColorDefault = defColor;
                                codes[0].CharCode = 42;
                                codes[0].Size = 10;
                                switch (item.StateId)
                                {
                                    case 8:
                                        codes[0].IconColor = Color.LightGray.ToArgb();
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = Color.LightGray.ToArgb();
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 41;
                                        codes[1].Size = 10;
                                        break;
                                    case 22:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 40;
                                        codes[0].Size = 10;
                                        break;
                                    case 2:
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = Color.Gray.ToArgb();
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 48;
                                        codes[1].Size = 10;
                                        break;
                                    case 10:
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = defColor;
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 47;
                                        codes[1].Size = 10;
                                        break;
                                    case 23:
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = defColor;
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 49;
                                        codes[1].Size = 10;
                                        break;
                                    case 9:
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = defColor;
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 43;
                                        codes[1].Size = 10;
                                        break;
                                    default:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 42;
                                        codes[0].Size = 10;
                                        break;
                                }
                            }
                            else
                            {
                                switch (item.StateId)
                                {
                                    case 1:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 34;
                                        codes[0].Size = 10;
                                        break;
                                    case 8:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.LightGray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 41;
                                        codes[0].Size = 10;
                                        break;
                                    case 10:
                                        codes = new WellIconCode[2];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 47;
                                        codes[0].Size = 10;
                                        codes[1].FontCode = 0;
                                        codes[1].IconColor = defColor;
                                        codes[1].IconColorDefault = defColor;
                                        codes[1].CharCode = 34;
                                        codes[1].Size = 10;
                                        break;
                                    case 22:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 40;
                                        codes[0].Size = 10;
                                        break;
                                    case 2:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = Color.Gray.ToArgb();
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 48;
                                        codes[0].Size = 10;
                                        break;
                                    case 23:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 49;
                                        codes[0].Size = 10;
                                        break;
                                    case 9:
                                        codes = new WellIconCode[1];
                                        codes[0].FontCode = 0;
                                        codes[0].IconColor = defColor;
                                        codes[0].IconColorDefault = defColor;
                                        codes[0].CharCode = 43;
                                        codes[0].Size = 10;
                                        break;
                                }
                            }
                        }
                        #endregion

                        #region Нагнетатаельные
                        else if (item.CharWorkId == 20)
                        {
                            defColor = Color.FromArgb(0, 82, 164).ToArgb();
                            switch (item.StateId)
                            {
                                case 2:
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = Color.Gray.ToArgb();
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.Gray.ToArgb();
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 48;
                                    codes[1].Size = 10;
                                    break;
                                case 8:
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = Color.LightGray.ToArgb();
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 35;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.LightGray.ToArgb();
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 41;
                                    codes[1].Size = 10;
                                    break;
                                case 9:
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = defColor;
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 43;
                                    codes[1].Size = 10;
                                    break;
                                case 22:
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.FromArgb(35, 35, 35).ToArgb();
                                    codes[1].IconColorDefault = Color.FromArgb(35, 35, 35).ToArgb();
                                    codes[1].CharCode = 40;
                                    codes[1].Size = 10;
                                    break;
                                case 23:
                                    codes = new WellIconCode[2];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.FromArgb(35, 35, 35).ToArgb();
                                    codes[1].IconColorDefault = Color.FromArgb(35, 35, 35).ToArgb();
                                    codes[1].CharCode = 49;
                                    codes[1].Size = 10;
                                    break;
                                default:
                                    codes = new WellIconCode[1];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 36;
                                    codes[0].Size = 10;
                                    break;
                            }
                        }
                        #endregion

                        #region Водозаборные
                        else if (item.CharWorkId == 13)
                        {
                            defColor = Color.CornflowerBlue.ToArgb();
                            codes = new WellIconCode[1];
                            codes[0].FontCode = 0;
                            codes[0].IconColor = defColor;
                            codes[0].IconColorDefault = defColor;
                            codes[0].CharCode = 37;
                            codes[0].Size = 10;
                        }
                        #endregion

                        #region Поглощающие
                        else if (item.CharWorkId == 41)
                        {
                            defColor = Color.FromArgb(35, 35, 35).ToArgb();
                            codes = new WellIconCode[2];
                            codes[0].FontCode = 0;
                            codes[0].IconColor = defColor;
                            codes[0].IconColorDefault = defColor;
                            codes[0].CharCode = 50;
                            codes[0].Size = 10;
                            switch (item.StateId)
                            {
                                case 8:
                                    codes[0].IconColor = Color.LightGray.ToArgb();
                                    codes[1].FontCode = 0;
                                    codes[1].IconColor = Color.LightGray.ToArgb();
                                    codes[1].IconColorDefault = defColor;
                                    codes[1].CharCode = 41;
                                    codes[1].Size = 10;
                                    break;
                                default:
                                    codes = new WellIconCode[1];
                                    codes[0].FontCode = 0;
                                    codes[0].IconColor = defColor;
                                    codes[0].IconColorDefault = defColor;
                                    codes[0].CharCode = 50;
                                    codes[0].Size = 10;
                                    break;
                            }
                        }
                        #endregion

                        double X = Constant.DOUBLE_NAN, Y = Constant.DOUBLE_NAN;
                        if ((this.MerLoaded) && (this.CoordLoaded))
                        {
                            SkvCoordItem coordItem;
                            for (int i = 0; i < this.Coord.Count; i++)
                            {
                                coordItem = Coord.Items[i];
                                if (item.PlastId == coordItem.PlastId)
                                {
                                    X = coordItem.X;
                                    Y = coordItem.Y;
                                    break;
                                }
                            }
                        }
                        if (X == Constant.DOUBLE_NAN)
                        {
                            X = this.X;
                            Y = this.Y;
                        }
                        if ((codes != null) && ((X != Constant.DOUBLE_NAN)))
                        {
                            if ((this.MerLoaded) && (this.Mer.Count > 0) && (this.Mer.Items[this.Mer.Count - 1].Date < MaxMerDate))
                            {
                                for (int j = 0; j < codes.Length; j++)
                                {
                                    codes[j].IconColor = Color.LightGray.ToArgb();
                                }
                            }
                            this.Icons.AddIcon(X, Y, item.PlastId, codes);
                            if (codes.Length > 0) this.Icons.SetActiveIcon(item.PlastId);
                        }
                    }
                    x--;
                }

            }
            else if (this.ProjectDest == 0)
            {
                codes = new WellIconCode[1];
                codes[0].FontCode = 0;
                codes[0].IconColor = Color.FromArgb(35, 35, 35).ToArgb();
                codes[0].IconColorDefault = codes[0].IconColor;
                codes[0].CharCode = 33;
                double X = Constant.DOUBLE_NAN, Y = Constant.DOUBLE_NAN;

                if (X == Constant.DOUBLE_NAN)
                {
                    X = this.X;
                    Y = this.Y;
                }
                if ((codes != null) && ((X != Constant.DOUBLE_NAN)))
                {
                    this.Icons.AddIcon(X, Y, item.PlastId, codes);
                    if (codes.Length > 0) this.Icons.SetActiveIcon(item.PlastId);
                }
            }
            else // проектная скважина
            {
                switch (this.ProjectDest)
                {
                    case 1:
                        codes = new WellIconCode[1];
                        codes[0].FontCode = 0;
                        codes[0].IconColor = Color.FromArgb(255, 35, 35).ToArgb();
                        codes[0].IconColorDefault = codes[0].IconColor;
                        codes[0].CharCode = 34;
                        codes[0].Size = 10;
                        break;
                    case 2:
                        codes = new WellIconCode[1];
                        codes[0].FontCode = 0;
                        codes[0].IconColor = Color.FromArgb(255, 35, 35).ToArgb();
                        codes[0].IconColorDefault = codes[0].IconColor;
                        codes[0].CharCode = 36;
                        codes[0].Size = 10;
                        break;
                }
                double X = Constant.DOUBLE_NAN, Y = Constant.DOUBLE_NAN;
                if ((this.Coord != null) && (this.Coord.Count > 0))
                {
                    SkvCoordItem coordItem;
                    coordItem = Coord.Items[0];
                    X = coordItem.X; Y = coordItem.Y;
                }
                if (X == Constant.DOUBLE_NAN)
                {
                    X = this.X;
                    Y = this.Y;
                }
                if ((codes != null) && ((X != Constant.DOUBLE_NAN)))
                {
                    this.Icons.AddIcon(X, Y, item.PlastId, codes);
                    if (codes.Length > 0) this.Icons.SetActiveIcon(item.PlastId);
                }
            }
        }
        public void SetActiveAllIcon()
        {
            if (Icons != null)
            {
                Icons.SetActiveAllIcon();
                WellIcon icon = Icons.GetActiveIcon();
                if (icon != null)
                {
                    this.X = icon.X;
                    this.Y = icon.Y;
                }
            }
        }
        #endregion

        public void SetStartedCoord()
        {
            if (CoordLoaded)
            {
                if (Coord.Count > 0)
                {
                    X = Coord.Items[0].X;
                    Y = Coord.Items[0].Y;
                }
                else
                {
                    X = Coord.Altitude.X;
                    Y = Coord.Altitude.Y;
                }
            }

        }

        #region CACHE
        public void ReadFromCache(BinaryReader br)
        {
            Index = br.ReadInt32();
            MainBoreIndex = br.ReadInt32();
            WellId = br.ReadInt32();
            BoreId = br.ReadInt32();
            OilFieldAreaCode = br.ReadInt32();
            StartDate = DateTime.FromOADate(br.ReadDouble());
            Name = br.ReadString();
        }
        public void WriteToCache(BinaryWriter bw)
        {
            bw.Write(Index);
            bw.Write(MainBoreIndex);
            bw.Write(WellId);
            bw.Write(BoreId);
            bw.Write(OilFieldAreaCode);
            bw.Write(StartDate.ToOADate());
            bw.Write(Name);
        }
        #endregion
    }

    #region Well Icon Classes
    public struct WellIconCode
    {
        public ushort FontCode;
        public int IconColorDefault;
        public int IconColor;
        public ushort CharCode;
        public ushort Size;
    }
    public sealed class WellIcon
    {
        public double X, Y;
        List<WellIconCode> iconCodeList;
        public bool IsDefaultColor;

        public int StratumCode { get; set; }
        public int Count { get { return iconCodeList.Count; } }

        public int IconColor
        {
            get
            {
                if (iconCodeList.Count > 0)
                {
                    return iconCodeList[0].IconColor;
                }
                return 0;
            }
        }
        public int DefaultIconColor
        {
            get
            {
                if (iconCodeList.Count > 0)
                {
                    return iconCodeList[0].IconColorDefault;
                }
                return 0;
            }
        }
        public WellIcon(double X, double Y, int StratumCode)
        {
            this.X = X;
            this.Y = Y;
            this.StratumCode = StratumCode;
            IsDefaultColor = false;
            iconCodeList = new List<WellIconCode>(5);
        }

        public WellIconCode this[int index]
        {
            get
            {
                if (index < Count)
                {
                    return iconCodeList[index];
                }
                else
                {
                    return default(WellIconCode);
                }
            }
        }
        public bool EqualsIcon(WellIcon wIcon)
        {
            bool res = true;
            if (Count == wIcon.Count)
            {
                for (int i = 0; i < Count; i++)
                {
                    if ((this[i].CharCode != wIcon[i].CharCode) ||
                        (this[i].FontCode != wIcon[i].FontCode) ||
                        (this[i].IconColor != wIcon[i].IconColor) ||
                        (this[i].Size != wIcon[i].Size))
                    {
                        res = false;
                        break;
                    }
                }
            }
            else
                res = false;
            return res;
        }
        public void SetIconCodes(WellIconCode[] codes)
        {
            int len = codes.Length;
            iconCodeList.Clear();
            for (int i = 0; i < len; i++) AddIconCode(codes[i]);
        }

        public void InsertIconCode(int index, WellIconCode code)
        {
            WellIconCode wCode;
            wCode.FontCode = code.FontCode;
            wCode.IconColor = code.IconColor;
            wCode.IconColorDefault = code.IconColorDefault;
            wCode.CharCode = code.CharCode;
            wCode.Size = code.Size;
            if (index < iconCodeList.Count)
            {
                iconCodeList.Insert(index, wCode);
            }
            else
            {
                iconCodeList.Add(wCode);
            }
        }
        public void AddIconCode(WellIconCode code)
        {
            WellIconCode wCode;
            wCode.FontCode = code.FontCode;
            wCode.IconColor = code.IconColor;
            wCode.IconColorDefault = code.IconColorDefault;
            wCode.CharCode = code.CharCode;
            wCode.Size = code.Size;
            iconCodeList.Add(wCode);
        }
        public void ClearIconList()
        {
            iconCodeList.Clear();
        }
        public bool WriteToCache(StreamWriter file, bool WithDefault)
        {
            if (file != null)
            {
                WellIconCode wiс;
                file.Write(String.Format("{0};", Count));
                ushort fCode;
                for (int i = 0; i < Count; i++)
                {
                    wiс = iconCodeList[i];
                    fCode = wiс.FontCode;
                    if (WithDefault)
                    {
                        file.Write(String.Format("{0};{1};{2};{3};{4}", fCode, wiс.IconColor, wiс.IconColorDefault, wiс.CharCode, wiс.Size));
                    }
                    else
                    {
                        file.Write(String.Format("{0};{1};{2};{3}", fCode, wiс.IconColor, wiс.CharCode, wiс.Size));
                    }
                    if (i < Count - 1) file.Write(";");
                }
                return true;
            }
            return false;
        }
        public void Read(BinaryReader br)
        {
            int count = br.ReadInt32();
            iconCodeList.Clear();
            for (int i = 0; i < count; i++)
            {
                WellIconCode code = new WellIconCode();
                code.FontCode = br.ReadUInt16();
                code.IconColor = br.ReadInt32();
                code.IconColorDefault = br.ReadInt32();
                code.CharCode = br.ReadUInt16();
                code.Size = br.ReadUInt16();
                iconCodeList.Add(code);
            }
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(Count);
            for (int i = 0; i < Count; i++)
            {
                bw.Write(iconCodeList[i].FontCode);
                bw.Write(iconCodeList[i].IconColor);
                bw.Write(iconCodeList[i].IconColorDefault);
                bw.Write(iconCodeList[i].CharCode);
                bw.Write(iconCodeList[i].Size);
            }
        }
    }
    public sealed class WellsIconList
    {
        List<WellIcon> IconList;

        int ActiveIcon;
        public int IconsColor;
        public int DefaultIconsColor;
        public WellsIconList()
        {
            IconList = new List<WellIcon>();
        }
        public WellsIconList(int Size)
        {
            IconList = new List<WellIcon>(Size);
        }
        public int Count { get { return IconList.Count; } }
        public bool IsDefaultIconsColor
        {
            set
            {
                for (int i = 0; i < Count; i++) IconList[i].IsDefaultColor = value;
            }
            get
            {
                if (Count > 0) return IconList[0].IsDefaultColor;
                return false;
            }
        }

        public int AddIcon(double X, double Y, int OilObjCode, WellIconCode[] codes)
        {
            if ((X != Constant.DOUBLE_NAN) && (Y != Constant.DOUBLE_NAN))
            {
                WellIcon wIcon = new WellIcon(X, Y, OilObjCode);
                wIcon.SetIconCodes(codes);
                IconsColor = wIcon.IconColor;
                DefaultIconsColor = wIcon.DefaultIconColor;
                return AddIcon(wIcon);
            }
            return -1;
        }
        public int AddIcon(WellIcon icon)
        {
            IconsColor = icon.IconColor;
            DefaultIconsColor = icon.DefaultIconColor;
            IconList.Add(icon);
            return IconList.Count - 1;
        }
        public void SetActiveIcon(int OilObjCode)
        {
            WellIcon icon;
            for (int i = 0; i < Count; i++)
            {
                icon = IconList[i];
                if (OilObjCode == icon.StratumCode)
                {
                    ActiveIcon = i;
                    IconsColor = icon.IconColor;
                    DefaultIconsColor = icon.DefaultIconColor;
                    break;
                }
            }
        }
        public void SetActiveIcon(int OilObjCode, double X, double Y)
        {
            WellIcon icon;
            for (int i = 0; i < Count; i++)
            {
                icon = IconList[i];
                if (OilObjCode == icon.StratumCode)
                {
                    ActiveIcon = i;
                    icon.X = X;
                    icon.Y = Y;
                    IconsColor = icon.IconColor;
                    DefaultIconsColor = icon.DefaultIconColor;
                    break;
                }
            }
        }
        public WellIcon GetActiveIcon()
        {
            if ((ActiveIcon != -1) && (IconList != null) && (ActiveIcon < IconList.Count))
            {
                return IconList[ActiveIcon];
            }
            return null;
        }
        public void SetActiveAllIcon()
        {
            WellIcon icon;
            for (int i = 0; i < IconList.Count; i++)
            {
                icon = IconList[i];
                if (icon.StratumCode != -1)
                {
                    ActiveIcon = i;
                    IconsColor = icon.IconColor;
                    DefaultIconsColor = icon.DefaultIconColor;
                    break;
                }
            }
        }
        public int[] GetOilObjList()
        {
            int len = IconList.Count;
            if (len == 0) return null;
            int[] retValue = new int[len - 1];
            for (int i = 1; i < Count; i++)
            {
                retValue[i - 1] = IconList[i].StratumCode;
            }
            return retValue;
        }
        public WellIcon GetIconByCode(int OilObjCode)
        {
            int len = IconList.Count;
            for (int i = 0; i < len; i++)
            {
                if (IconList[i].StratumCode == OilObjCode)
                {
                    return IconList[i];
                }
            }
            return null;
        }
        public WellIcon this[int index]
        {
            get
            {
                if ((index > -1) && (index < Count))
                {
                    return IconList[index];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        public void Read(BinaryReader br)
        {
            int count = br.ReadInt32();
            double X, Y;
            int PlastCode;
            WellIcon icon;
            for (int i = 0; i < count; i++)
            {
                X = br.ReadDouble();
                Y = br.ReadDouble();
                PlastCode = br.ReadInt32();
                icon = new WellIcon(X, Y, PlastCode);
                icon.Read(br);
                AddIcon(icon);
            }
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(Count);
            for (int i = 0; i < IconList.Count; i++)
            {
                bw.Write(IconList[i].X);
                bw.Write(IconList[i].Y);
                bw.Write(IconList[i].StratumCode);
                IconList[i].Write(bw);
            }
        }
        public void Clear()
        {
            IconList.Clear();
        }
    }

    #endregion

    public enum TYPE_SKV 
    {
        SKV,
        PROJ_SKV        
    }

}
