using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using System.Drawing;
using DataLoader.Objects.Base;

namespace DataLoader
{

    #region SkvCoord objects
    public struct SkvCoordItem
    {
        /// <summary>
        /// �����
        /// </summary>
        public int PlastId;
        /// <summary>
        /// X ���������� ����������� �������� ����� �����
        /// </summary>
        public double X;
        /// <summary>
        /// Y ���������� ����������� �������� ����� �����
        /// </summary>
        public double Y;
    }

    public sealed class SkvCoord : WellDataObject<SkvCoordItem>
    {
        public static int VERSION = 0;
        public bool IsServerStratumCodes = false;

        public struct SkvCoordAlt
        {
            /// <summary>
            /// ���������� ����� �������� �� ��� X
            /// </summary>
            public double X;
            /// <summary>
            /// ���������� ����� �������� �� ��� Y
            /// </summary>
            public double Y;
            /// <summary>
            /// ���������
            /// </summary>
            public float Alt;
        }

        public SkvCoordAlt Altitude;

        public void Write(BinaryWriter bw)
        {
            bw.Write(Altitude.Alt);
            bw.Write(Altitude.X);
            bw.Write(Altitude.Y);
            bw.Write(Count);
            for (int i = 0; i < Count; i++)
            {
                bw.Write(Items[i].PlastId);
                bw.Write(Items[i].X);
                bw.Write(Items[i].Y);
            }
        }
        public void Read(BinaryReader br)
        {
            Altitude.Alt = br.ReadSingle();
            Altitude.X = br.ReadDouble();
            Altitude.Y = br.ReadDouble();
            int count = br.ReadInt32();
            if (count > 0)
            {
                Items = new SkvCoordItem[count];
                for (int i = 0; i < count; i++)
                {
                    Items[i].PlastId = br.ReadInt32();
                    Items[i].X = br.ReadDouble();
                    Items[i].Y = br.ReadDouble();
                }
            }
        }
    }

    #endregion

    #region SkvGis objects
    
    public struct SkvGisItem
    {
        /// <summary>
        /// �����
        /// </summary>
        public UInt16 PlastId;
        /// <summary>
        /// �����+��������
        /// </summary>
        public UInt16 PlastZoneId;
        /// <summary>
        /// ������ ����������
        /// </summary>
        public int ZIndex;
        /// <summary>
        /// ������� ������ ������
        /// </summary>
        public float H;
        /// <summary>
        /// �������
        /// </summary>
        public float L;
        /// <summary>
        /// ���. �������, �����
        /// </summary>
        public float Habs;
        /// <summary>
        /// ���. �������
        /// </summary>
        public float Labs;
        /// <summary>
        /// ���������
        /// </summary>
        public UInt16 LitId;
        /// <summary>
        /// ���������/�����������
        /// </summary>
        public bool Collector;
        /// <summary>
        /// �������� ��������� ���������
        /// </summary>
        public byte Sat0;
        /// <summary>
        /// �������� ��������� �������
        /// </summary>
        public byte Sat;
        /// <summary>
        /// ��, ��*�
        /// </summary>
        public float Ro;
        /// <summary>
        /// �����_��, ���� ��.
        /// </summary>
        public float APS;
        /// <summary>
        /// �������. �����-� ��
        /// </summary>
        public float aGK;
        /// <summary>
        /// �������. �����-� ���
        /// </summary>
        public float aNKT;
        /// <summary>
        /// ����. ����������, �� (���� �����)
        /// </summary>
        public float Kpor;
        /// <summary>
        /// ����. �������������, %
        /// </summary>
        public float Kpr;
        /// <summary>
        /// ����. �����������������, %
        /// </summary>
        public float Kn;
        /// <summary>
        /// ����. ����������������, %
        /// </summary>
        public float Kgas;
        /// <summary>
        /// ����. �����������, %
        /// </summary>
        public float Kgl;
    }

    public sealed class SkvGIS : WellDataObject<SkvGisItem>
    {
        public static int VERSION = 0;

        public bool IsServerStratumCode = false;

        public SkvGisItem RetrieveItem(SkvGisItem item)
        {
            SkvGisItem res;
            res.PlastId = item.PlastId;
            res.PlastZoneId = item.PlastZoneId;
            res.ZIndex = item.ZIndex;
            res.H = item.H;
            res.L = item.L;
            res.Habs = item.Habs;
            res.Labs = item.Labs;
            res.LitId = item.LitId;
            res.Collector = item.Collector;
            res.Sat0 = item.Sat0;
            res.Sat = item.Sat;
            res.Ro = item.Ro;
            res.APS = item.APS;
            res.aGK = item.aGK;
            res.aNKT = item.aNKT;
            res.Kpor = item.Kpor;
            res.Kpr = item.Kpr;
            res.Kn = item.Kn;
            res.Kgas = item.Kgas;
            res.Kgl = item.Kgl;
            return item;
        }
        public SkvGisItem RetrieveItem(BinaryReader br)
        {
            SkvGisItem item;
            item.PlastId = br.ReadUInt16();
            item.PlastZoneId = br.ReadUInt16();
            item.ZIndex = br.ReadInt32();
            item.H = br.ReadSingle();
            item.L = br.ReadSingle();
            item.Habs = br.ReadSingle();
            item.Labs = br.ReadSingle();
            item.LitId = br.ReadUInt16();
            item.Collector = br.ReadBoolean();
            item.Sat0 = br.ReadByte();
            item.Sat = br.ReadByte();
            item.Ro = br.ReadSingle();
            item.APS = br.ReadSingle();
            item.aGK = br.ReadSingle();
            item.aNKT = br.ReadSingle();
            item.Kpor = br.ReadSingle();
            item.Kpr = br.ReadSingle();
            item.Kn = br.ReadSingle();
            item.Kgas = br.ReadSingle();
            item.Kgl = br.ReadSingle();
            return item;
        }
        public bool RetainItem(BinaryWriter bw, int index)
        {
            if ((index > -1) && (index < Items.Length))
            {
                bw.Write(Items[index].PlastId);
                bw.Write(Items[index].PlastZoneId);
                bw.Write(Items[index].ZIndex);
                bw.Write(Items[index].H);
                bw.Write(Items[index].L);
                bw.Write(Items[index].Habs);
                bw.Write(Items[index].Labs);
                bw.Write(Items[index].LitId);
                bw.Write(Items[index].Collector);
                bw.Write(Items[index].Sat0);
                bw.Write(Items[index].Sat);
                bw.Write(Items[index].Ro);
                bw.Write(Items[index].APS);
                bw.Write(Items[index].aGK);
                bw.Write(Items[index].aNKT);
                bw.Write(Items[index].Kpor);
                bw.Write(Items[index].Kpr);
                bw.Write(Items[index].Kn);
                bw.Write(Items[index].Kgas);
                bw.Write(Items[index].Kgl);
                return true;
            }
            return false;
        }
    }
    
    #endregion

    #region SkvCore objects 

    public struct SkvCoreItem
    {
        /// <summary>
        /// �������� ������, ������, �
        /// </summary>
        public float Top;
        /// <summary>
        /// �������� ������, �����, �
        /// </summary>
        public float Bottom;
        /// <summary>
        /// ����� �����, �
        /// </summary>
        public float CoreRecovery;
        /// <summary>
        /// ���������� ��������� �����, ���.�
        /// </summary>
        public float CoreStored;
        /// <summary>
        /// ���������� ������ ��������, ��
        /// </summary>
        public byte StoredBoxCount;
        /// <summary>
        /// ��������� �����
        /// </summary>
        public byte CoreState;
        /// <summary>
        /// ��������� ���������� �����
        /// </summary>
        public byte CoreMarkState;
    }

    public sealed class SkvCore : WellDataObject<SkvCoreItem>
    {
        public static int VERSION = 0;

        public SkvCoreItem RetrieveItem(SkvCoreItem item)
        {
            SkvCoreItem res;
            res.Top = item.Top;
            res.Bottom = item.Bottom;
            res.CoreRecovery = item.CoreRecovery;
            res.CoreStored = item.CoreStored;
            res.StoredBoxCount = item.StoredBoxCount;
            res.CoreState = item.CoreState;
            res.CoreMarkState = item.CoreMarkState;
            return item;
        }
        public SkvCoreItem RetrieveItem(BinaryReader br)
        {
            SkvCoreItem item;
            item.Top = br.ReadSingle();
            item.Bottom = br.ReadSingle();
            item.CoreRecovery = br.ReadSingle();
            item.CoreStored = br.ReadSingle();
            item.StoredBoxCount = br.ReadByte();
            item.CoreState = br.ReadByte();
            item.CoreMarkState = br.ReadByte();
            return item;
        }
        public bool RetainItem(BinaryWriter bw, int index)
        {
            if ((index > -1) && (index < Items.Length))
            {
                bw.Write(Items[index].Top);
                bw.Write(Items[index].Bottom);
                bw.Write(Items[index].CoreRecovery);
                bw.Write(Items[index].CoreStored);
                bw.Write(Items[index].StoredBoxCount);
                bw.Write(Items[index].CoreState);
                bw.Write(Items[index].CoreMarkState);
                return true;
            }
            return false;
        }
    }

    #endregion

    #region SkvCoreTest objects

    public struct SkvCoreTestItem
    {
        /// <summary>
        /// ����� �������
        /// </summary>
        public string Name;
        /// <summary>
        /// ���� ����������� �����
        /// </summary>
        public DateTime Date;
        /// <summary>
        /// �������� ������, ������, �
        /// </summary>
        public float Top;
        /// <summary>
        /// �������� ������, �����, �
        /// </summary>
        public float Bottom;
        /// <summary>
        /// ����� �����, �
        /// </summary>
        public float CoreRecovery;
        /// <summary>
        /// ���������� �� �����, �
        /// </summary>
        public float DistanceTop;
        /// <summary>
        /// ���������
        /// </summary>
        public string Litology;
        /// <summary>
        /// �������� ���������
        /// </summary>
        public byte SatId;
        /// <summary>
        /// ���������� �������� ������������������, %
        /// </summary>
        public float KporLiquid;
        /// <summary>
        /// ���������� �������� �� �����, %
        /// </summary>
        public float KporHelium;
        /// <summary>
        /// ������������� �� ������� ��, ��
        /// </summary>
        public float KprAirPP;
        /// <summary>
        /// ������������� �� ������� ��, ��
        /// </summary>
        public float KprAirPR;
        /// <summary>
        /// ������������� �� ����� ��, ��
        /// </summary>
        public float KprHeliumPP;
        /// <summary>
        /// ������������� �� ����� ��, ��
        /// </summary>
        public float KprHeliumPR;
        /// <summary>
        /// ��������, psi
        /// </summary>
        public float Pressure;
        /// <summary>
        /// ������������� �������, %
        /// </summary>
        public float CarbonateTiff;
        /// <summary>
        /// ������������� �������, %
        /// </summary>
        public float CarbonateDolomite;
        /// <summary>
        /// ������������� ������������� �������, %
        /// </summary>
        public float CarbonateInsoluble;
        /// <summary>
        /// ��� �� ����������, ��
        /// </summary>
        public float AdaBeforeExtract;
        /// <summary>
        /// ��� ����� ����������, ��
        /// </summary>
        public float AdaAfterExtract;
        /// <summary>
        /// �������������
        /// </summary>
        public float Wettability;
        /// <summary>
        /// �������� ����������
        /// </summary>
        public float Porosity;
        /// <summary>
        /// ���������� ����������������, %
        /// </summary>
        public float SatWatFinal;
        /// <summary>
        /// ���������������� ���������
        /// </summary>
        public float DensityMineral;
        /// <summary>
        /// �������� ���������
        /// </summary>
        public float DensityVolume;
        /// <summary>
        /// ������������������ ������, ����� 500 ���, %
        /// </summary>
        public float PartSize500;
        /// <summary>
        /// ������������������ ������, ����� 250 ���, %
        /// </summary>
        public float PartSize250;
        /// <summary>
        /// ������������������ ������, ����� 100 ���, %
        /// </summary>
        public float PartSize100;
        /// <summary>
        /// ������������������ ������, ����� 50 ���, %
        /// </summary>
        public float PartSize50;
        /// <summary>
        /// ������������������ ������, ����� 10 ���, %
        /// </summary>
        public float PartSize10;
        /// <summary>
        /// ������������������ ������, ����� 10 ���, %
        /// </summary>
        public float PartSize0;
        /// <summary>
        /// ��������������� K, ��/��
        /// </summary>
        public float RadioActivityK;
        /// <summary>
        /// ��������������� K, %
        /// </summary>
        public float RadioActivityKPercent;
        /// <summary>
        /// ��������������� Th, ��/��
        /// </summary>
        public float RadioActivityTh;
        /// <summary>
        /// ��������������� Th, %
        /// </summary>
        public float RadioActivityThPercent;
        /// <summary>
        /// ��������������� Ra, ��/��
        /// </summary>
        public float RadioActivityRa;
        /// <summary>
        /// ��������������� Ra, %
        /// </summary>
        public float RadioActivityRaPercent;
        /// <summary>
        /// ��������������� A, ��/��
        /// </summary>
        public float RadioActivityA;
        /// <summary>
        /// �����������
        /// </summary>
        public string Comment;
    }

    public sealed class SkvCoreTest : WellDataObject<SkvCoreTestItem>
    {
        public static int VERSION = 0;
 
        public SkvCoreTestItem RetrieveItem(SkvCoreTestItem item)
        {
            SkvCoreTestItem res;
            res.Date = item.Date;
            res.Name = item.Name;
            res.Top = item.Top;
            res.Bottom = item.Bottom;
            res.CoreRecovery = item.CoreRecovery;
            res.DistanceTop = item.DistanceTop;
            res.Litology = item.Litology;
            res.SatId = item.SatId;
            res.KporLiquid = item.KporLiquid;
            res.KporHelium = item.KporHelium;
            res.KprAirPP = item.KprAirPP;
            res.KprAirPR = item.KprAirPR;
            res.KprHeliumPP = item.KprHeliumPP;
            res.KprHeliumPR = item.KprHeliumPR;
            res.Pressure = item.Pressure;
            res.CarbonateTiff = item.CarbonateTiff;
            res.CarbonateDolomite = item.CarbonateDolomite;
            res.CarbonateInsoluble = item.CarbonateInsoluble;
            res.AdaBeforeExtract = item.AdaBeforeExtract;
            res.AdaAfterExtract = item.AdaAfterExtract;
            res.Wettability = item.Wettability;
            res.Porosity = item.Porosity;
            res.SatWatFinal = item.SatWatFinal;
            res.DensityMineral = item.DensityMineral;
            res.DensityVolume = item.DensityVolume;
            res.PartSize500 = item.PartSize500;
            res.PartSize250 = item.PartSize250;
            res.PartSize100 = item.PartSize100;
            res.PartSize50 = item.PartSize50;
            res.PartSize10 = item.PartSize10;
            res.PartSize0 = item.PartSize0;
            res.RadioActivityK = item.RadioActivityK;
            res.RadioActivityKPercent = item.RadioActivityKPercent;
            res.RadioActivityTh = item.RadioActivityTh;
            res.RadioActivityThPercent = item.RadioActivityThPercent;
            res.RadioActivityRa = item.RadioActivityRa;
            res.RadioActivityRaPercent = item.RadioActivityRaPercent;
            res.RadioActivityA = item.RadioActivityA;
            res.Comment = item.Comment;
            return item;
        }
        public SkvCoreTestItem RetrieveItem(BinaryReader br)
        {
            SkvCoreTestItem item;
            item.Date = DateTime.FromOADate(br.ReadDouble());
            item.Name = br.ReadString();
            item.Top = br.ReadSingle();
            item.Bottom = br.ReadSingle();
            item.CoreRecovery = br.ReadSingle();
            item.DistanceTop = br.ReadSingle();
            item.Litology = br.ReadString();
            item.SatId = br.ReadByte();
            item.KporLiquid = br.ReadSingle();
            item.KporHelium = br.ReadSingle();
            item.KprAirPP = br.ReadSingle();
            item.KprAirPR = br.ReadSingle();
            item.KprHeliumPP = br.ReadSingle();
            item.KprHeliumPR = br.ReadSingle();
            item.Pressure = br.ReadSingle();
            item.CarbonateTiff = br.ReadSingle();
            item.CarbonateDolomite = br.ReadSingle();
            item.CarbonateInsoluble = br.ReadSingle();
            item.AdaBeforeExtract = br.ReadSingle();
            item.AdaAfterExtract = br.ReadSingle();
            item.Wettability = br.ReadSingle();
            item.Porosity = br.ReadSingle();
            item.SatWatFinal = br.ReadSingle();
            item.DensityMineral = br.ReadSingle();
            item.DensityVolume = br.ReadSingle();
            item.PartSize500 = br.ReadSingle();
            item.PartSize250 = br.ReadSingle();
            item.PartSize100 = br.ReadSingle();
            item.PartSize50 = br.ReadSingle();
            item.PartSize10 = br.ReadSingle();
            item.PartSize0 = br.ReadSingle();
            item.RadioActivityK = br.ReadSingle();
            item.RadioActivityKPercent = br.ReadSingle();
            item.RadioActivityTh = br.ReadSingle();
            item.RadioActivityThPercent = br.ReadSingle();
            item.RadioActivityRa = br.ReadSingle();
            item.RadioActivityRaPercent = br.ReadSingle();
            item.RadioActivityA = br.ReadSingle();
            item.Comment = br.ReadString();
            return item;
        }
        public bool RetainItem(BinaryWriter bw, int index)
        {
            if ((index > -1) && (index < Items.Length))
            {
                bw.Write(Items[index].Date.ToOADate());
                bw.Write(Items[index].Name);
                bw.Write(Items[index].Top);
                bw.Write(Items[index].Bottom);
                bw.Write(Items[index].CoreRecovery);
                bw.Write(Items[index].DistanceTop);
                bw.Write(Items[index].Litology);
                bw.Write(Items[index].SatId);
                bw.Write(Items[index].KporLiquid);
                bw.Write(Items[index].KporHelium);
                bw.Write(Items[index].KprAirPP);
                bw.Write(Items[index].KprAirPR);
                bw.Write(Items[index].KprHeliumPP);
                bw.Write(Items[index].KprHeliumPR);
                bw.Write(Items[index].Pressure);
                bw.Write(Items[index].CarbonateTiff);
                bw.Write(Items[index].CarbonateDolomite);
                bw.Write(Items[index].CarbonateInsoluble);
                bw.Write(Items[index].AdaBeforeExtract);
                bw.Write(Items[index].AdaAfterExtract);
                bw.Write(Items[index].Wettability);
                bw.Write(Items[index].Porosity);
                bw.Write(Items[index].SatWatFinal);
                bw.Write(Items[index].DensityMineral);
                bw.Write(Items[index].DensityVolume);
                bw.Write(Items[index].PartSize500);
                bw.Write(Items[index].PartSize250);
                bw.Write(Items[index].PartSize100);
                bw.Write(Items[index].PartSize50);
                bw.Write(Items[index].PartSize10);
                bw.Write(Items[index].PartSize0);
                bw.Write(Items[index].RadioActivityK);
                bw.Write(Items[index].RadioActivityKPercent);
                bw.Write(Items[index].RadioActivityTh);
                bw.Write(Items[index].RadioActivityThPercent);
                bw.Write(Items[index].RadioActivityRa);
                bw.Write(Items[index].RadioActivityRaPercent);
                bw.Write(Items[index].RadioActivityA);
                bw.Write(Items[index].Comment);
                return true;
            }
            return false;
        }
    }

    #endregion

    #region SkvPerf objects

    public struct SkvPerfItem
    {
        public DateTime Date;
        public UInt16 PlastCode;
        public UInt16 PerfTypeId;
        public UInt16 PerforatorTypeId;
        public UInt16 NumHoles;
        public float Top;
        public float Bottom;
        public bool Closed;
        public UInt16 PerfIsolateTypeId;
        public byte Source;
    }

    public sealed class SkvPerf : WellDataObject<SkvPerfItem>
    {
        public static int VERSION = 2;

        public SkvPerfItem RetrieveItem(SkvPerfItem item)
        {
            SkvPerfItem res;
            res.Date = item.Date;
            res.PerfTypeId = item.PerfTypeId;
            res.PerforatorTypeId = item.PerforatorTypeId;
            res.NumHoles = item.NumHoles;
            res.Top = item.Top;
            res.Bottom = item.Bottom;
            res.Closed = item.Closed;
            res.PerfIsolateTypeId = item.PerfIsolateTypeId;
            res.Source = item.Source;
            return item;
        }
        public SkvPerfItem RetrieveItem(BinaryReader br, int Version)
        {
            SkvPerfItem item;
            item.Date = SmartPlusSystem.UnpackDateI(br.ReadInt32());
            item.PlastCode = br.ReadUInt16();
            item.PerfTypeId = br.ReadUInt16();
            item.PerforatorTypeId = br.ReadUInt16();
            item.NumHoles = br.ReadUInt16();
            item.Top = br.ReadSingle();
            item.Bottom = br.ReadSingle();
            item.Closed = br.ReadBoolean();
            item.PerfIsolateTypeId = br.ReadUInt16();
            item.Source = br.ReadByte();
            return item;
        }
        public bool RetainItem(BinaryWriter bw, int index)
        {
            if ((index > -1) && (index < Items.Length))
            {
                bw.Write((int)SmartPlusSystem.PackDate(Items[index].Date));
                bw.Write(Items[index].PlastCode);
                bw.Write(Items[index].PerfTypeId);
                bw.Write(Items[index].PerforatorTypeId);
                bw.Write(Items[index].NumHoles);
                bw.Write(Items[index].Top);
                bw.Write(Items[index].Bottom);
                bw.Write(Items[index].Closed);
                bw.Write(Items[index].PerfIsolateTypeId);
                bw.Write(Items[index].Source);
                return true;
            }
            return false;
        }
    }

    #endregion

    #region Mer objects

    public struct MerItem
    {
        /// <summary>
        /// ����
        /// </summary>
        public DateTime Date;
        /// <summary>
        /// �����, ��� �� �����������
        /// </summary>
        public int PlastId;
        /// <summary>
        /// �������� ������, ��� �� �����������
        /// </summary>
        public int CharWorkId;
        /// <summary>
        /// ��������� ��������, ��� �� �����������
        /// </summary>
        public int StateId;
        /// <summary>
        /// ������ ������������, ��� �� �����������
        /// </summary>
        public int MethodId;
        /// <summary>
        /// ����� ������, � �����
        /// </summary>
        public int WorkTime;
        /// <summary>
        /// ����� �����, � �����
        /// </summary>
        public int CollTime;
        /// <summary>
        /// �����, �
        /// </summary>
        public double Oil;
        /// <summary>
        /// ����, � /�������, ���������, �3
        /// </summary>
        public double Wat;
        /// <summary>
        /// ���, �3
        /// </summary>
        public double Gas;
        /// <summary>
        /// �����, �3
        /// </summary>
        public double OilV;
        /// <summary>
        /// ����, �3
        /// </summary>
        public double WatV;
        /// <summary>
        /// ������� �������, ��� �� �����������
        /// </summary>
        public int StayReasonId;
        /// <summary>
        /// ����� �������, � �����
        /// </summary>
        public int StayTime;
        /// <summary>
        /// ��� ������ ������
        /// </summary>
        public int AgentProdId;
        //public int Tag;
    }
    public struct MerItemEx
    {
        /// <summary>
        /// ��� ������ �������
        /// </summary>
        public int AgentInjId;
        /// <summary>
        /// �������, �3
        /// </summary>
        public double Injection;
        /// <summary>
        /// ��������� ���, �3
        /// </summary>
        public double NaturalGas;
        /// <summary>
        /// �������������, �
        /// </summary>
        public double GasCondensate;
        /// <summary>
        /// ��� ������ �������
        /// </summary>
        public double OtherFluidV;
        /// <summary>
        /// ��� �� �����, �3
        /// </summary>
        public double CapGas;
        /// <summary>
        /// ������� item
        /// </summary>
        /// 
        public static MerItemEx Empty
        {
            get { return new MerItemEx(); }
        }
    }

    public sealed class Mer : WellDataObject<MerItem>
    {
        public static int VERSION = 1;

        public const int ItemLen = 80;
        public const int ItemExLen = 44;

        public bool IsServerStratumCode = false;
        MerItemEx[] _itemsEx = null;

        public MerItemEx GetItemEx(int index)
        {
            if (_itemsEx == null) return MerItemEx.Empty;
            return _itemsEx[index];
        }
        public int CountEx
        {
            get
            {
                if (_itemsEx == null) return 0;
                return _itemsEx.Length;
            }
        }

 
 
        // READ WRITE
        public void Read(BinaryReader br, bool NotLoadData)
        {
            int count = br.ReadInt32();
            int countEx = br.ReadInt32();
            if (!NotLoadData)
            {
                int i;
                Items = new MerItem[count];
                for (i = 0; i < count; i++)
                {
                    ReadItem(br, i);
                }
                _itemsEx = null;
                if (countEx > 0)
                {
                    _itemsEx = new MerItemEx[countEx];
                    for (i = 0; i < countEx; i++)
                    {
                        ReadItemEx(br, i);
                    }
                }
            }
            else
            {
                br.BaseStream.Seek(Count * ItemLen + CountEx * ItemExLen, SeekOrigin.Current);
            }
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(Count);
            bw.Write(CountEx);
            int i;
            for (i = 0; i < Count; i++)
            {
                WriteItem(bw, i);
            }
            for (i = 0; i < CountEx; i++)
            {
                WriteItemEx(bw, i);
            }
        }
        void ReadItem(BinaryReader br, int Index)
        {
            Items[Index].Date = SmartPlusSystem.UnpackDateTimeI(br.ReadInt32());
            Items[Index].PlastId = br.ReadInt32();
            Items[Index].CharWorkId = br.ReadInt32();
            Items[Index].StateId = br.ReadInt32();
            Items[Index].MethodId = br.ReadInt32();
            Items[Index].WorkTime = br.ReadInt32();
            Items[Index].CollTime = br.ReadInt32();
            Items[Index].Oil = br.ReadDouble();
            Items[Index].Wat = br.ReadDouble();
            Items[Index].OilV = br.ReadDouble();
            Items[Index].WatV = br.ReadDouble();
            Items[Index].Gas = br.ReadDouble();
            Items[Index].StayReasonId = br.ReadInt32();
            Items[Index].StayTime = br.ReadInt32();
            Items[Index].AgentProdId = br.ReadInt32();
        }
        void WriteItem(BinaryWriter bw, int Index)
        {
            bw.Write((int)SmartPlusSystem.PackDateTime(Items[Index].Date));
            bw.Write(Items[Index].PlastId);
            bw.Write(Items[Index].CharWorkId);
            bw.Write(Items[Index].StateId);
            bw.Write(Items[Index].MethodId);
            bw.Write(Items[Index].WorkTime);
            bw.Write(Items[Index].CollTime);
            bw.Write(Items[Index].Oil);
            bw.Write(Items[Index].Wat);
            bw.Write(Items[Index].OilV);
            bw.Write(Items[Index].WatV);
            bw.Write(Items[Index].Gas);
            bw.Write(Items[Index].StayReasonId);
            bw.Write(Items[Index].StayTime);
            bw.Write(Items[Index].AgentProdId);
        }
        void ReadItemEx(BinaryReader br, int Index)
        {
            if (_itemsEx == null)
            {
                throw new NullReferenceException("������ ItemEx �� ������!");
            }
            _itemsEx[Index].AgentInjId = br.ReadInt32();
            _itemsEx[Index].Injection = br.ReadDouble();
            _itemsEx[Index].NaturalGas = br.ReadDouble();
            _itemsEx[Index].GasCondensate = br.ReadDouble();
            _itemsEx[Index].OtherFluidV = br.ReadDouble();
            _itemsEx[Index].CapGas = br.ReadDouble();
        }
        void WriteItemEx(BinaryWriter bw, int Index)
        {
            if (_itemsEx == null)
            {
                throw new NullReferenceException("������ ItemEx �� ������!");
            }
            bw.Write(_itemsEx[Index].AgentInjId);
            bw.Write(_itemsEx[Index].Injection);
            bw.Write(_itemsEx[Index].NaturalGas);
            bw.Write(_itemsEx[Index].GasCondensate);
            bw.Write(_itemsEx[Index].OtherFluidV);
            bw.Write(_itemsEx[Index].CapGas);
        }

        public int GetBinLength()
        {
            int len = 8; // Count + CountEx;
            if (Count > 0)
            {
                len += Count * ItemLen; // Items
                len += CountEx * ItemExLen; // ItemsEx
            }
            return len;
        }
        public void SetItems(MerItem[] items, MerItemEx[] itemsEx) { Items = items; _itemsEx = itemsEx; }

        // GET DATA
        public Mer GetItemsByDate(DateTime MinDate)
        {
            Mer mer = new Mer();
            List<MerItem> items = new List<MerItem>();
            List<MerItemEx> itemsEx = new List<MerItemEx>();

            for (int i = Items.Length - 1; i >= 0; i--)
            {
                if (Items[i].Date <= MinDate) break;
                items.Insert(0, Items[i]);
                if (CountEx > 0) itemsEx.Insert(0, _itemsEx[i]);
            }
            mer.SetItems(items.ToArray(), itemsEx.ToArray());
            return mer;
        }
        public int GetIndex(DateTime Date)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Items[i].Date.Year == Date.Year && Items[i].Date.Month == Date.Month)
                {
                    return i;
                }
            }
            return -1;
        }
        public int[] GetIndexes(DateTime Date)
        {
            int[] result = new int[2];
            result[0] = -1;
            result[1] = -1;
            for (int i = 0; i < Count; i++)
            {
                if ((Items[i].Date.Year == Date.Year) && (Items[i].Date.Month == Date.Month))
                {
                    if (result[0] == -1) result[0] = i;
                    result[1] = i;
                }
                else if (result[0] != -1)
                {
                    break;
                }

            }
            return result;
        }
        public int GetAccumParams(int Index1, int Index2, ref MerItem Item, ref MerItemEx ItemEx)
        {
            int i;
            if (Index1 < 0) Index1 = 0;
            if (Index2 > Count) Index2 = Count;
            int year = Items[Index1].Date.Year, month = Items[Index1].Date.Month;
            bool ex = CountEx > 0;
            for (i = Index1; i < Index2; i++)
            {
                Item.Oil += Items[i].Oil;
                Item.Wat += Items[i].Wat;
                Item.OilV += Items[i].OilV;
                Item.WatV += Items[i].WatV;
                if (ex)
                {
                    ItemEx.CapGas += _itemsEx[i].CapGas;
                    ItemEx.GasCondensate += _itemsEx[i].GasCondensate;
                    ItemEx.Injection += _itemsEx[i].Injection;
                    ItemEx.NaturalGas += _itemsEx[i].NaturalGas;
                    ItemEx.OtherFluidV += _itemsEx[i].OtherFluidV;
                }
            }
            return i;
        }
    }
    #endregion

    #region SkvLog object
    
    public class SkvLog : WellDataObject<float>
    {
        public static int VERSION = 0;

        /// <summary>
        /// ��� ��������� (��� ������)
        /// </summary>
        public int MnemonikaId;
        /// <summary>
        /// ��� ������� ���������
        /// </summary>
        public int DemensionId;
        /// <summary>
        /// �� ������ ����� �������� ������
        /// </summary>
        public string LasFileName;
        /// <summary>
        /// ���� ���������� ������������
        /// </summary>
        public DateTime Date;
        /// <summary>
        /// ��������� �������
        /// </summary>
        public float StartDepth;
        /// <summary>
        /// �������� �������
        /// </summary>
        public float EndDepth;
        /// <summary>
        /// ��� ���������
        /// </summary>
        public float Step;
        /// <summary>
        /// ���������� ��������
        /// </summary>
        public float NullValue;

        public float MinValue = -1;
        public float MaxValue = -1;

        public bool RetriveItems(BinaryReader br, bool LoadValues)
        {
            MnemonikaId = br.ReadInt32();
            DemensionId = br.ReadInt32();
            Date = DateTime.FromOADate(br.ReadDouble());
            StartDepth = br.ReadSingle();
            EndDepth = br.ReadSingle();
            Step = br.ReadSingle();
            NullValue = br.ReadSingle();
            LasFileName = br.ReadString();
            int i, count = br.ReadInt32();
            if ((count > 0) && (LoadValues))
            {
                Items = new float[count];
                for (i = 0; i < count; i++)
                {
                    Items[i] = br.ReadSingle();
                    if ((MinValue == -1) || (MinValue > Items[i])) MinValue = Items[i];
                    if ((MaxValue == -1) || (MaxValue < Items[i])) MaxValue = Items[i];
                }
            }
            if (!LoadValues) br.BaseStream.Seek(count * 4, SeekOrigin.Current);
            return true;
        }
        public bool RetainItems(BinaryWriter bw)
        {
            bw.Write(MnemonikaId);
            bw.Write(DemensionId);
            bw.Write(Date.ToOADate());
            bw.Write(StartDepth);
            bw.Write(EndDepth);
            bw.Write(Step);
            bw.Write(NullValue);
            bw.Write(LasFileName);
            bw.Write(Items.Length);
            for (int i = 0; i < Items.Length; i++)
            {
                bw.Write(Items[i]);
            }
            return true;
        }
    }
    
    #endregion

    #region SumParameters
    public struct SumParamItem
    {
        public DateTime Date;
        public double Liq;
        public double LiqV;
        public double Oil;
        public double OilV;
        public double Watering;
        public double WateringV;
        public double Inj;
        public double InjGas;
        public double Scoop;
        public double NatGas;
        public double GasCondensate;
        public double WorkTimeProd;
        public double WorkTimeInj;
        public double WorkTimeInjGas;
        public int Fprod;
        public int Finj;
        public int Fscoop;
        public int LeadInAll;
        public int LeadInProd;
        public float Pressure;
        public int PressureCounter;
        public float ZabPressure;
        public int ZabPressureCounter;
        public double InjVb;
        public double LiqVb;
        public double AccumInjVb;
        public double AccumLiqVb;
        public void AddSumParam(SumParamItem AddItem)
        {
            this.Liq += AddItem.Liq;
            this.LiqV += AddItem.LiqV;
            this.Oil += AddItem.Oil;
            this.OilV += AddItem.OilV;
            this.Watering = 0;
            this.WateringV = 0;
            if (this.Liq > 0) Watering = (Liq - Oil) * 100 / Liq;
            if (this.LiqV > 0) WateringV = (LiqV - OilV) * 100 / LiqV;

            this.Inj += AddItem.Inj;
            if (AddItem.InjGas > -1)
            {
                if (this.InjGas < 0) this.InjGas = 0;
                this.InjGas += AddItem.InjGas;
            }
            if (AddItem.NatGas > -1)
            {
                if (this.NatGas < 0) this.NatGas = 0;
                this.NatGas += AddItem.NatGas;
            }
            if (AddItem.GasCondensate > -1)
            {
                if (this.GasCondensate == -1) this.GasCondensate = 0;
                this.GasCondensate += AddItem.GasCondensate;
            }
            this.Scoop += AddItem.Scoop;
            this.WorkTimeProd += AddItem.WorkTimeProd;
            this.WorkTimeInj += AddItem.WorkTimeInj;
            this.WorkTimeInjGas += AddItem.WorkTimeInjGas;
            this.Fprod += AddItem.Fprod;
            this.Finj += AddItem.Finj;
            this.Fscoop += AddItem.Fscoop;
            this.LeadInAll += AddItem.LeadInAll;
            this.LeadInProd += AddItem.LeadInProd;
            LiqVb += AddItem.LiqVb;
            InjVb += AddItem.InjVb;
            AccumInjVb += AddItem.AccumInjVb;
            AccumLiqVb += AddItem.AccumLiqVb;
        }
        public void SubSumParam(SumParamItem SubItem)
        {

            this.Liq -= SubItem.Liq;
            LiqV -= SubItem.LiqV;
            this.Oil -= SubItem.Oil;
            OilV -= SubItem.OilV;
            this.Watering = 0;
            this.WateringV = 0;
            if (this.Liq > 0) this.Watering = (this.Liq - this.Oil) * 100 / this.Liq;
            if (this.LiqV > 0) this.WateringV = (this.LiqV - this.OilV) * 100 / this.LiqV;
            this.Inj -= SubItem.Inj;
            if (SubItem.InjGas > 0)
            {
                this.InjGas -= SubItem.InjGas;
                if (this.InjGas == 0) this.InjGas = -1;
            }
            if (SubItem.NatGas > 0)
            {
                this.NatGas -= SubItem.NatGas;
                if (this.NatGas == 0) this.NatGas = -1;
            }
            if (SubItem.GasCondensate > 0)
            {
                this.GasCondensate -= SubItem.GasCondensate;
                if (this.GasCondensate == 0) this.GasCondensate = -1;
            }
            this.Scoop -= SubItem.Scoop;
            this.WorkTimeProd -= SubItem.WorkTimeProd;
            this.WorkTimeInj -= SubItem.WorkTimeInj;
            this.WorkTimeInjGas -= SubItem.WorkTimeInjGas;
            this.Fprod -= SubItem.Fprod;
            this.Finj -= SubItem.Finj;
            this.Fscoop -= SubItem.Fscoop;
            this.LeadInAll -= SubItem.LeadInAll;
            this.LeadInProd -= SubItem.LeadInProd;
            LiqVb -= SubItem.LiqVb;
            InjVb -= SubItem.InjVb;
            AccumInjVb -= SubItem.AccumInjVb;
            AccumLiqVb -= SubItem.AccumLiqVb;

            if (this.Liq < 0) { this.Liq = 0; this.Watering = 0; }
            if (this.LiqV < 0) { this.LiqV = 0; this.WateringV = 0; }
            if (this.Oil < 0)
            {
                this.Oil = 0;
                if (this.Liq > 0) this.Watering = (this.Liq - this.Oil) * 100 / this.Liq;
                if (this.LiqV > 0) this.WateringV = (this.LiqV - this.OilV) * 100 / this.LiqV;
            }
            if (this.Inj < 0) this.Inj = 0;
            if (this.InjGas < 0) this.InjGas = 0;
            if (this.Scoop < 0) this.Scoop = 0;
            if (this.WorkTimeProd < 0) this.WorkTimeProd = 0;
            if (this.WorkTimeInj < 0) this.WorkTimeInj = 0;
            if (this.Fprod < 0) this.Fprod = 0;
            if (this.Finj < 0) this.Finj = 0;
            if (this.Fscoop < 0) this.Fscoop = 0;
            if (this.LeadInAll < 0) this.LeadInAll = 0;
            if (this.LeadInProd < 0) this.LeadInProd = 0;
            if (this.LiqVb < 0) this.LiqVb = 0;
            if (this.InjVb < 0) this.InjVb = 0;
            if (AccumInjVb < 0) AccumInjVb = 0;
            if (AccumLiqVb < 0) AccumLiqVb = 0;
        }
    }

    public sealed class SumObjParameters
    {
        public int OilObjCode;
        public int ShiftMonthIndex;
        public int ShiftYearIndex;
        SumParamItem[] _monthlyItems = null;
        SumParamItem[] _yearlyItems = null;
        public SumObjParameters(int OilObjCode, SumParamItem[] MonthlyItems, SumParamItem[] YearlyItems)
        {
            this.OilObjCode = OilObjCode;
            this.ShiftMonthIndex = -1;
            this.ShiftYearIndex = -1;
            _monthlyItems = MonthlyItems;
            _yearlyItems = YearlyItems;
        }
        public SumObjParameters(int OilObjCode, SumParamItem[] MonthlyItems, SumParamItem[] YearlyItems, int ShiftMonth, int ShiftYear)
            : this(OilObjCode, MonthlyItems, YearlyItems)
        {
            this.ShiftMonthIndex = ShiftMonth;
            this.ShiftYearIndex = ShiftYear;
        }
        public SumParamItem[] MonthlyItems
        {
            get { return _monthlyItems; }
            set { _monthlyItems = value; }
        }
        public SumParamItem[] YearlyItems
        {
            get { return _yearlyItems; }
            set { _yearlyItems = value; }
        }
    }

    public sealed class SumParameters
    {
        public static int Version = 3;
        System.Collections.ArrayList items;
        public SumParameters(int Length)
        {
            items = new System.Collections.ArrayList(Length);
        }
        public void Clear()
        {
            items.Clear();
        }
        public int Count { get { return items.Count; } }
        public void Add(int OilObjCode, SumParamItem[] MonthlyItems, SumParamItem[] YearlyItems)
        {
            this.Add(OilObjCode, MonthlyItems, YearlyItems, -1, -1);
        }
        public void Add(int OilObjCode, SumParamItem[] MonthlyItems, SumParamItem[] YearlyItems, int ShiftMonth, int ShiftYear)
        {
            SumObjParameters sumObj = new SumObjParameters(OilObjCode, MonthlyItems, YearlyItems, ShiftMonth, ShiftYear);
            items.Add(sumObj);
        }

        public SumObjParameters this[int index]
        {
            get
            {
                if (index < items.Count) return (SumObjParameters)items[index];
                else return null;
            }
            set
            {
                if ((index > -1) && (index < items.Count))
                {
                    items[index] = value;
                }
            }
        }
        public SumObjParameters GetItemByObjCode(int OilObjCode)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (((SumObjParameters)items[i]).OilObjCode == OilObjCode)
                {
                    return (SumObjParameters)items[i];
                }
            }
            return null;
        }
        public int GetIndexByObjCode(int OilObjCode)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (((SumObjParameters)items[i]).OilObjCode == OilObjCode)
                {
                    return i;
                }
            }
            return -1;
        }

        public void AddDaySumParams(int OilObjCode, SumParamItem[] AddParams)
        {
            if ((AddParams != null) && (AddParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                int indObj = -1, index1 = -1, index2 = -1;
                if (sumObj == null)
                {
                    sumObj = new SumObjParameters(OilObjCode, null, null);
                    indObj = Count;
                    items.Add(sumObj);
                }
                SumParams = sumObj.MonthlyItems;

                if ((SumParams == null) || (SumParams.Length == 0) ||
                    (SumParams.Length < AddParams.Length) ||
                    (AddParams[0].Date < SumParams[0].Date) ||
                    (AddParams[AddParams.Length - 1].Date > SumParams[SumParams.Length - 1].Date))
                {
                    DateTime dtMin = DateTime.MaxValue, dtMax = DateTime.MinValue, dt;
                    if (SumParams != null)
                    {
                        if (dtMin > SumParams[0].Date) dtMin = SumParams[0].Date;
                        if (dtMax < SumParams[SumParams.Length - 1].Date) dtMax = SumParams[SumParams.Length - 1].Date;
                    }
                    if (dtMin > AddParams[0].Date) dtMin = AddParams[0].Date;
                    if (dtMax < AddParams[AddParams.Length - 1].Date) dtMax = AddParams[AddParams.Length - 1].Date;

                    int count = (dtMax - dtMin).Days + 1;

                    if ((SumParams != null) && (SumParams.Length > 0))
                    {
                        index1 = (SumParams[0].Date - dtMin).Days;
                    }

                    SumParamItem[] newSumParam = new SumParamItem[count];
                    dt = dtMin;
                    for (int i = 0; i < count; i++)
                    {
                        if ((index1 != -1) && (i >= index1) && (i - index1 < SumParams.Length))
                        {
                            newSumParam[i].AddSumParam(SumParams[i - index1]);
                        }
                        newSumParam[i].Date = dt;
                        dt = dt.AddDays(1);
                    }
                    SumParams = newSumParam;
                    sumObj.MonthlyItems = SumParams;
                }
                index2 = (AddParams[0].Date - SumParams[0].Date).Days;

                for (int i = 0; i < SumParams.Length; i++)
                {
                    if ((index2 != -1) && (i >= index2) && (i - index2 < AddParams.Length))
                    {
                        SumParams[i].AddSumParam(AddParams[i - index2]);
                    }
                }
            }
        }
        public void AddMonthSumParams(int OilObjCode, SumParamItem[] AddParams)
        {
            if ((AddParams != null) && (AddParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                int indObj = -1, index1 = -1, index2 = -1;
                if (sumObj == null)
                {
                    sumObj = new SumObjParameters(OilObjCode, null, null);
                    indObj = Count;
                    items.Add(sumObj);
                }
                SumParams = sumObj.MonthlyItems;

                if ((SumParams == null) || (SumParams.Length == 0) ||
                    (SumParams.Length < AddParams.Length) ||
                    (AddParams[0].Date < SumParams[0].Date) ||
                    (AddParams[AddParams.Length - 1].Date > SumParams[SumParams.Length - 1].Date))
                {
                    DateTime dtMin = DateTime.MaxValue, dtMax = DateTime.MinValue, dt;
                    if (SumParams != null)
                    {
                        if (dtMin > SumParams[0].Date) dtMin = SumParams[0].Date;
                        if (dtMax < SumParams[SumParams.Length - 1].Date) dtMax = SumParams[SumParams.Length - 1].Date;
                    }
                    if (dtMin > AddParams[0].Date) dtMin = AddParams[0].Date;
                    if (dtMax < AddParams[AddParams.Length - 1].Date) dtMax = AddParams[AddParams.Length - 1].Date;

                    int count = (dtMax.Year - dtMin.Year) * 12 + dtMax.Month - dtMin.Month + 1;

                    if ((SumParams != null) && (SumParams.Length > 0))
                    {
                        index1 = (SumParams[0].Date.Year - dtMin.Year) * 12 + SumParams[0].Date.Month - dtMin.Month;
                    }

                    SumParamItem[] newSumParam = new SumParamItem[count];
                    dt = dtMin;
                    for (int i = 0; i < count; i++)
                    {
                        if ((index1 != -1) && (i >= index1) && (i - index1 < SumParams.Length))
                        {
                            newSumParam[i].AddSumParam(SumParams[i - index1]);
                        }
                        newSumParam[i].Date = dt;
                        newSumParam[i].InjGas = -1;
                        newSumParam[i].NatGas = -1;
                        newSumParam[i].GasCondensate = -1;
                        dt = dt.AddMonths(1);
                    }
                    SumParams = newSumParam;
                    sumObj.MonthlyItems = SumParams;
                }
                index2 = (AddParams[0].Date.Year - SumParams[0].Date.Year) * 12 + AddParams[0].Date.Month - SumParams[0].Date.Month;

                for (int i = 0; i < SumParams.Length; i++)
                {
                    if ((index2 != -1) && (i >= index2) && (i - index2 < AddParams.Length))
                    {
                        SumParams[i].AddSumParam(AddParams[i - index2]);
                    }
                }
            }
        }
        public void AddYearSumParams(int OilObjCode, SumParamItem[] AddParams)
        {
            if ((AddParams != null) && (AddParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                int indObj = -1, index1 = -1, index2 = -1;
                if (sumObj == null)
                {
                    sumObj = new SumObjParameters(OilObjCode, null, null);
                    indObj = Count;
                    items.Add(sumObj);
                }
                SumParams = sumObj.YearlyItems;

                if ((SumParams == null) || (SumParams.Length == 0) ||
                    (SumParams.Length < AddParams.Length) ||
                    (AddParams[0].Date < SumParams[0].Date) ||
                    (AddParams[AddParams.Length - 1].Date > SumParams[SumParams.Length - 1].Date))
                {
                    DateTime dtMin = DateTime.MaxValue, dtMax = DateTime.MinValue, dt;
                    if (SumParams != null)
                    {
                        if (dtMin > SumParams[0].Date) dtMin = SumParams[0].Date;
                        if (dtMax < SumParams[SumParams.Length - 1].Date) dtMax = SumParams[SumParams.Length - 1].Date;
                    }
                    if (dtMin > AddParams[0].Date) dtMin = AddParams[0].Date;
                    if (dtMax < AddParams[AddParams.Length - 1].Date) dtMax = AddParams[AddParams.Length - 1].Date;

                    int count = dtMax.Year - dtMin.Year + 1;

                    if ((SumParams != null) && (SumParams.Length > 0))
                    {
                        index1 = SumParams[0].Date.Year - dtMin.Year;
                    }
                    dt = dtMin;
                    SumParamItem[] newSumParam = new SumParamItem[count];
                    for (int i = 0; i < count; i++)
                    {
                        if ((index1 != -1) && (i >= index1) && (i - index1 < SumParams.Length))
                        {
                            newSumParam[i].AddSumParam(SumParams[i - index1]);
                        }
                        newSumParam[i].Date = dt;
                        newSumParam[i].InjGas = -1;
                        newSumParam[i].NatGas = -1;
                        newSumParam[i].GasCondensate = -1;
                        dt = dt.AddYears(1);
                    }
                    SumParams = newSumParam;
                    sumObj.YearlyItems = SumParams;
                }
                index2 = AddParams[0].Date.Year - SumParams[0].Date.Year;

                for (int i = 0; i < SumParams.Length; i++)
                {
                    if ((index2 != -1) && (i >= index2) && (i - index2 < AddParams.Length))
                    {
                        SumParams[i].AddSumParam(AddParams[i - index2]);
                    }
                }
            }
        }

        public void SubDaySumParams(int OilObjCode, SumParamItem[] SubParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((SubParams != null) && (SubParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                if (sumObj != null) SumParams = sumObj.MonthlyItems;
                if ((SumParams != null) && (SumParams.Length > 0))
                {
                    int index1 = -1, index2 = -1;
                    int TrimCount = (TrimMaxDate - TrimMinDate).Days + 1;
                    DateTime dt;
                    if ((TrimCount > -1) && (TrimCount < SumParams.Length))
                    {
                        if ((SumParams != null) && (SumParams.Length > 0))
                        {
                            index1 = (SumParams[0].Date - TrimMinDate).Days;
                        }
                        dt = TrimMinDate;
                        SumParamItem[] newSumParam = new SumParamItem[TrimCount];
                        for (int i = 0; i < TrimCount; i++)
                        {
                            if ((index1 != -1) && (i >= index1) && (i - index1 < SumParams.Length))
                            {
                                newSumParam[i].AddSumParam(SumParams[i - index1]);
                            }
                            newSumParam[i].Date = dt;
                            dt = dt.AddDays(1);
                        }
                        SumParams = newSumParam;
                        sumObj.MonthlyItems = newSumParam;
                    }
                    index2 = (SubParams[0].Date - SumParams[0].Date).Days;

                    for (int i = 0; i < SumParams.Length; i++)
                    {
                        if ((index2 != -1) && (i >= index2) && (i - index2 < SubParams.Length))
                        {
                            SumParams[i].SubSumParam(SumParams[i - index2]);
                        }
                    }
                }
            }
        }
        public void SubMonthSumParams(int OilObjCode, SumParamItem[] SubParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((SubParams != null) && (SubParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                if (sumObj != null) SumParams = sumObj.MonthlyItems;
                if ((SumParams != null) && (SumParams.Length > 0))
                {
                    int index1 = -1, index2 = -1;
                    int TrimCount = (TrimMaxDate.Year - TrimMinDate.Year) * 12 + TrimMaxDate.Month - TrimMinDate.Month + 1;
                    DateTime dt;
                    if ((TrimCount > -1) && (TrimCount < SumParams.Length))
                    {
                        if ((SumParams != null) && (SumParams.Length > 0))
                        {
                            index1 = (SumParams[0].Date.Year - TrimMinDate.Year) * 12 + SumParams[0].Date.Month - TrimMinDate.Month;
                        }
                        dt = TrimMinDate;
                        SumParamItem[] newSumParam = new SumParamItem[TrimCount];
                        for (int i = 0; i < TrimCount; i++)
                        {
                            if ((SumParams != null) && (i - index1 > -1) && (i >= index1) && (i - index1 < SumParams.Length))
                            {
                                newSumParam[i].AddSumParam(SumParams[i - index1]);
                            }
                            newSumParam[i].Date = dt;
                            dt = dt.AddMonths(1);
                        }
                        SumParams = newSumParam;
                        sumObj.MonthlyItems = SumParams;
                    }

                    index2 = (SubParams[0].Date.Year - SumParams[0].Date.Year) * 12 + SubParams[0].Date.Month - SumParams[0].Date.Month;
                    for (int i = 0; i < SumParams.Length; i++)
                    {
                        if ((i - index2 > -1) && (i >= index2) && (i - index2 < SubParams.Length))
                        {
                            SumParams[i].SubSumParam(SubParams[i - index2]);
                        }
                    }
                }
            }
        }
        public void SubYearSumParams(int OilObjCode, SumParamItem[] SubParams, DateTime TrimMinDate, DateTime TrimMaxDate)
        {
            if ((SubParams != null) && (SubParams.Length > 0))
            {
                SumObjParameters sumObj = GetItemByObjCode(OilObjCode);
                SumParamItem[] SumParams = null;
                if (sumObj != null) SumParams = sumObj.YearlyItems;
                if ((SumParams != null) && (SumParams.Length > 0))
                {
                    int index1 = -1, index2 = -1;
                    int TrimCount = TrimMaxDate.Year - TrimMinDate.Year + 1;
                    DateTime dt;
                    if ((TrimCount > -1) && (TrimCount < SumParams.Length))
                    {
                        if ((SumParams != null) && (SumParams.Length > 0))
                        {
                            index1 = SumParams[0].Date.Year - TrimMinDate.Year;
                        }
                        dt = new DateTime(TrimMinDate.Year, 1, 1);
                        SumParamItem[] newSumParam = new SumParamItem[TrimCount];
                        for (int i = 0; i < TrimCount; i++)
                        {
                            if ((i - index1 > -1) && (i >= index1) && (i - index1 < SumParams.Length))
                            {
                                newSumParam[i].AddSumParam(SumParams[i - index1]);
                            }
                            newSumParam[i].Date = dt;
                            dt = dt.AddYears(1);
                        }
                        SumParams = newSumParam;
                    }
                    index2 = SubParams[0].Date.Year - SumParams[0].Date.Year;

                    for (int i = 0; i < SumParams.Length; i++)
                    {
                        if ((i - index2 > -1) && (i >= index2) && (i - index2 < SubParams.Length))
                        {
                            SumParams[i].SubSumParam(SubParams[i - index2]);
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region SkvGtm

    public sealed class SkvGtm : WellDataObject<DataLoader.Objects.Data.WellGTMItem>
    {
        public static int VERSION = 2;
        public bool IsServerStratumCode = false;
     }

    #endregion

    #region Table8_1
    public sealed class Table81Params
    {
        public static int Version = 1;
        public string ProjectDocName;
        public int NewPtdYear;
        int count;
        SumObjParameters Table;

        public Table81Params()
        {
            ProjectDocName = null;
            Table = null;
            count = -1;
            NewPtdYear = 0;
        }

        public void SetTable(SumParamItem[] YearlyItems)
        {
            Table = new SumObjParameters(-1, null, YearlyItems);
            count = Table.YearlyItems.Length;
        }

        public SumParamItem[] Items
        {
            get
            {
                if (Table != null) return Table.YearlyItems;
                else return null;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }
        }
    }
    #endregion
}
