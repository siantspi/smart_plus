﻿namespace DataLoader.Objects.Base
{
    public class BaseObject
    {
        public int Index { get; set; }
        public string Name { get; set; }
    }
}