﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataLoader.Objects.Base
{
    public class WellDataObject<T>
    {
        public T[] Items;
        public int Count
        {
            get
            {
                if (Items == null) return 0;
                else return Items.Length;
            }
        }

        public WellDataObject()
        {
            Items = null;
        }
        public void SetItems(T[] Set)
        {
            this.Items = Set;
        }
        public void Clear() { this.Items = null; }
        public T this[int index]
        {
            get
            {
                if ((index > -1) && (index < Count))
                {
                    return Items[index];
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            set
            {
                if ((index > -1) && (index < Count))
                {
                    Items[index] = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
        }

        public virtual bool ReadFromCache(BinaryReader br, int Version) { return false; }
        public virtual bool WriteToCache(BinaryWriter bw) { return false; }
    }
}
