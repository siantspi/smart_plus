﻿namespace DataLoader.Objects.Base
{
    public delegate void OnMessageDelegate(LoggingObject sender, LOGGING_MESSAGE_TYPE Type, string Message);
    public delegate void OnProgressDelegate(LoggingObject sender, int ProgressValue);
    public enum LOGGING_MESSAGE_TYPE : byte
    {
        NONE = 0,
        INFO = 1,
        WARNING = 2,
        ERROR = 3
    }
    public enum LOGGING_OBJECT_TYPE : byte
    {
        NONE = 0,
        PROJECT = 1,
        NGDU = 2,
        OILFIELD = 3,
        OILFIELD_DISPATCHER = 4
    }
    public class LoggingObject : BaseObject
    {
        public event OnMessageDelegate OnMessage;
        public event OnProgressDelegate OnProgress;
        public LOGGING_OBJECT_TYPE LogType { get; set; }

        public LoggingObject(LOGGING_OBJECT_TYPE LoggingType)
        {
            LogType = LoggingType;
        }
        public string LoggingName
        {
            get
            {
                switch (LogType)
                {
                    case LOGGING_OBJECT_TYPE.PROJECT:
                        return "Проект";
                    case LOGGING_OBJECT_TYPE.NGDU:
                        return "НГДУ";
                    case LOGGING_OBJECT_TYPE.OILFIELD:
                        return "Месторождение";
                    case LOGGING_OBJECT_TYPE.OILFIELD_DISPATCHER:
                        return "Диспетчер потоков загрузки";
                    default:
                        return "Неопределен";
                }
            }
        }
        public void NewMessage(string Message)
        {
            if (OnMessage != null) OnMessage(this, LOGGING_MESSAGE_TYPE.INFO, Message);
        }
        public void NewMessage(LOGGING_MESSAGE_TYPE Type, string Message)
        {
            if(OnMessage != null) OnMessage(this, Type, Message);
        }
        public void NewProgress(int ProgressValue)
        {
            if(OnProgress != null) OnProgress(this, ProgressValue);
        }
    }
}