﻿using DataLoader.Objects.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace DataLoader
{
    public static class Geometry
    {
        public struct Rect
        {
            public double Xmin;
            public double Ymin;
            public double Xmax;
            public double Ymax;
        }

        public static PointD GetPointInLineByDistance(PointD pt1, PointD pt2, double Distance)
        {
            PointD res;
            if (pt1.Y == pt2.Y) // гориз
            {
                int k = (pt1.X < pt2.X) ? 1 : -1;
                res.X = pt1.X + k * Distance;
                res.Y = pt1.Y;
            }
            else if (pt1.X == pt2.X) // верт
            {
                int k = (pt1.Y < pt2.Y) ? 1 : -1;
                res.X = pt1.X;
                res.Y = pt1.Y + k * Distance;
            }
            else
            {
                double x1, x2, y1, y2;
                y1 = Distance / Math.Sqrt(Math.Pow((pt2.X - pt1.X) / (pt2.Y - pt1.Y), 2) + 1) + pt1.Y;
                x1 = (pt2.X - pt1.X) * (y1 - pt1.Y) / (pt2.Y - pt1.Y) + pt1.X;

                y2 = -Distance / Math.Sqrt(Math.Pow((pt2.X - pt1.X) / (pt2.Y - pt1.Y), 2) + 1) + pt1.Y;
                x2 = (pt2.X - pt1.X) * (y2 - pt1.Y) / (pt2.Y - pt1.Y) + pt1.X;

                double minX = pt1.X, maxX = pt2.X;
                double minY = pt1.Y, maxY = pt2.Y;
                if (pt2.X < pt1.X) { minX = pt2.X; maxX = pt1.X; }
                if (pt2.Y < pt1.Y) { minY = pt2.Y; maxY = pt1.Y; }

                if ((minX < x1) && (x1 < maxX) && (minY < y1) && (y1 < maxY))
                {
                    res.X = x1;
                    res.Y = y1;
                }
                else
                {
                    res.X = x2;
                    res.Y = y2;
                }
            }
            return res;
        }
        public static PointD GetNormalPointInLine(PointD pt1, PointD pt2, PointD pt0, double DistByLine, bool GetLeftPoint)
        {
            PointD ptX1, ptX2;
            if (pt1.Y == pt2.Y) // гориз
            {
                ptX1.X = pt0.X;
                ptX1.Y = pt1.Y - DistByLine;
                ptX2.X = pt0.X;
                ptX2.Y = pt1.Y + DistByLine;
            }
            else if (pt1.X == pt2.X) // верт
            {
                ptX1.X = pt1.X - DistByLine;
                ptX1.Y = pt0.Y;
                ptX2.X = pt1.X + DistByLine;
                ptX2.Y = pt0.Y;
            }
            else // наклон к оси
            {
                double k, B, D;
                k = -(pt2.X - pt1.X) / (pt2.Y - pt1.Y);
                B = pt0.Y - k * pt0.X;
                D = Math.Pow(-2 * pt0.X + 2 * k * (B - pt0.Y), 2) - 4 * (1 + k * k) * (pt0.X * pt0.X + Math.Pow(B - pt0.Y, 2) - DistByLine * DistByLine);
                ptX1.X = (-(-2 * pt0.X + 2 * k * (B - pt0.Y)) + Math.Sqrt(D)) / (2 + 2 * k * k);
                ptX1.Y = k * ptX1.X + B;
                ptX2.X = (-(-2 * pt0.X + 2 * k * (B - pt0.Y)) - Math.Sqrt(D)) / (2 + 2 * k * k);
                ptX2.Y = k * ptX2.X + B;
            }
            bool leftX1 = IsPointInLeftSide(pt1, pt2, ptX1);
            if (!GetLeftPoint) leftX1 = !leftX1;

            if (leftX1)
                return ptX1;
            else
                return ptX2;
        }
        public static PointD GetIntersectLinesPoint(PointD pt1, PointD pt2, PointD pt3, PointD pt4)
        {
            PointD res;
            res.X = Constant.DOUBLE_NAN;
            res.Y = Constant.DOUBLE_NAN;

            if ((pt1.X == pt2.X) && (pt3.X == pt4.X)) return res;
            if ((pt1.Y == pt2.Y) && (pt3.Y == pt4.Y)) return res;
            double d = (pt2.X - pt1.X) * (pt4.Y - pt3.Y) - (pt4.X - pt3.X) * (pt2.Y - pt1.Y);
            if (d == 0) return res;
            double r = ((pt1.Y - pt3.Y) * (pt4.X - pt3.X) - (pt1.X - pt3.X) * (pt4.Y - pt3.Y)) / d;
            res.X = (pt2.X - pt1.X) * r + pt1.X;
            res.Y = (pt2.Y - pt1.Y) * r + pt1.Y;
            double minX = pt1.X, maxX = pt2.X, minY = pt1.Y, maxY = pt2.Y;
            if (pt2.X < pt1.X) { minX = pt2.X; maxX = pt1.X; }
            if (pt2.Y < pt1.Y) { minY = pt2.Y; maxY = pt1.Y; }
            if ((minX <= res.X) && (res.X <= maxX) && (minY <= res.Y) && (res.Y <= maxY))
            {
                minX = pt3.X; maxX = pt4.X; minY = pt3.Y; maxY = pt4.Y;
                if (pt4.X < pt3.X) { minX = pt4.X; maxX = pt3.X; }
                if (pt4.Y < pt3.Y) { minY = pt4.Y; maxY = pt3.Y; }
                if ((minX > res.X) || (res.X > maxX) || (minY > res.Y) || (res.Y > maxY))
                {
                    res.X = Constant.DOUBLE_NAN;
                    res.Y = Constant.DOUBLE_NAN;
                }
            }
            else
            {
                res.X = Constant.DOUBLE_NAN;
                res.Y = Constant.DOUBLE_NAN;
            }
            return res;
        }
        public static bool IsPointInLine(PointF line1, PointF line2, PointF pt)
        {
            PointD pt1, pt2, pt3;
            pt1.X = line1.X; pt1.Y = line1.Y;
            pt2.X = line2.X; pt2.Y = line2.Y;
            pt3.X = pt.X; pt3.Y = pt.Y;
            return IsPointInLine(pt1, pt2, pt3);
        }
        public static bool IsPointInLine(PointD line1, PointD line2, PointD pt)
        {
            bool res = false;

            if (line1.X == line2.X)
            {
                double minY = line1.Y, maxY = line2.Y;
                if (line2.Y < line1.Y) { minY = line2.Y; maxY = line1.Y; }
                if ((line1.X == pt.X) && (minY < pt.Y) && (pt.Y < maxY))
                {
                    return true;
                }
            }
            else if (line1.Y == line2.Y)
            {
                double minX = line1.X, maxX = line2.X;
                if (line2.X < line1.X) { minX = line2.X; maxX = line1.X; }
                if ((line1.Y == pt.Y) && (minX < pt.X) && (pt.X < maxX))
                {
                    return true;
                }
            }
            else
            {
                double y = (line2.Y - line1.Y) * (pt.X - line1.X) / (line2.X - line1.X) + line1.Y;
                if (Math.Abs(y - pt.Y) < 0.00001) res = true;
            }
            if (res)
            {
                res = false;
                double minX = line1.X, maxX = line2.X, minY = line1.Y, maxY = line2.Y;
                if (line2.X < line1.X) { minX = line2.X; maxX = line1.X; }
                if (line2.Y < line1.Y) { minY = line2.Y; maxY = line1.Y; }
                if ((minX < pt.X) && (pt.X < maxX) && (minY < pt.Y) && (pt.Y < maxY))
                {
                    res = true;
                }
            }
            return res;
        }
        public static bool IsPointInLeftSide(PointD pt1, PointD pt2, PointD pt0)
        {
            double k;
            if (pt1.Y == pt2.Y) // гориз
            {
                return (((pt1.X < pt2.X) && (pt0.Y > pt1.Y)) || ((pt1.X > pt2.X) && (pt0.Y < pt1.Y)));
            }
            else if (pt1.X == pt2.X) // вертик
            {
                return (((pt2.Y > pt1.Y) && (pt0.X < pt1.X)) || ((pt2.Y < pt1.Y) && (pt0.X > pt1.X)));
            }
            else // наклон
            {
                k = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
                double Ypr = k * (pt0.X - pt1.X) + pt1.Y;
                bool res = (((k > 0) && (pt0.Y > Ypr)) || ((k < 0) && (pt0.Y < Ypr)));
                if (pt2.Y < pt1.Y) res = !res;
                return res;
            }
        }
    }
}
