﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.DictionaryObjects
{
    class OilFieldCoefDictionary : Dictionary<OilfieldCoefItem>
    {
        public OilFieldCoefDictionary() : base(DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT) { }
    }
}
