﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;
using DataLoader.Objects.Project;
using DataLoader.Objects;

namespace DataLoader.DictionaryObjects
{
    public struct ConversionMessage
    {
        public int Type;
        public int ServerCode;
        public int LocalCode;
        public string ServerStratumName;
        public string LocalStratumName;
        public string ServerName;
        public string WellName;
    }
    public sealed class StratumDictionary : DataDictionary
    {
        internal struct StratumColor
        {
            public int StratumCode;
            public int ARGB;
        }
        public StratumTree stratumTree;
        List<ServerStratumConverter> StratumConvList;
        StratumColor[] StratumColors;
        string Path;
        int StartedDictCount;
        int UserFormCounter;
        public bool ConversionCanceled { set; get; }
        public bool ConversionFormShowed { get { return (UserFormCounter > 0); } }

        public StratumDictionary() : base(DICTIONARY_ITEM_TYPE.STRATUM)
        {
            StratumColors = null;
            StratumConvList = new List<ServerStratumConverter>();
            stratumTree = new StratumTree(this);
            UserFormCounter = 0;
        }

        #region LOAD SAVE 
        public override bool ReadFromFile(string Path)
        {
            this.Path = Path + "\\Stratum";
            bool res = base.ReadFromFile(this.Path);
            if (res)
            {
                LoadStratumTree();
                LoadStratumColors();
            }
            StartedDictCount = this.Count;
            return res;
        }
        void LoadStratumTree()
        {
            stratumTree.LoadFromFile(this.Path + "\\StratumTree.csv");
        }
        bool LoadConversionList()
        {
            bool res = false;
            if (Directory.Exists(Application.StartupPath))
            {
                DirectoryInfo dir = new DirectoryInfo(Application.StartupPath);
                FileInfo[] files = dir.GetFiles("*.bin");
                char[] separator = new char[] { '_' };
                string[] parseStr;
                ServerTypeID ServerType;
                string ServerName, OraSid;
                int Port;
                for (int i = 0; i < files.Length; i++)
                {
                    parseStr = files[i].Name.Split(separator);
                    if (parseStr.Length > 3)
                    {
                        try
                        {
                            ServerType = (ServerTypeID)Convert.ToByte(parseStr[0]);
                            ServerName = parseStr[1];
                            OraSid = parseStr[2];
                            Port = Convert.ToInt32(parseStr[3].Remove(parseStr[3].Length - 4, 4));
                            ServerStratumConverter PlastConverter = new ServerStratumConverter(ServerType, ServerName, Port, Path, this, OraSid);
                            PlastConverter.LoadFromFile();
                            StratumConvList.Add(PlastConverter);
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return res;
        }
        bool LoadStratumColors()
        {
            if ((Path.Length > 0) && File.Exists(Path + "\\colors.bin"))
            {
                FileStream fs = new FileStream(Path + "\\colors.bin", FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                StratumTreeNode node = null;
                int count = br.ReadInt32();
                if (count > 0)
                {
                    StratumColors = new StratumColor[count];
                    for (int i = 0; i < count; i++)
                    {
                        StratumColors[i].StratumCode = br.ReadInt32();
                        StratumColors[i].ARGB = br.ReadInt32();
                        node = GetStratumTreeNode(StratumColors[i].StratumCode);
                        if (node != null) node.ARGB = StratumColors[i].ARGB;
                    }
                }
            }
            return false;
        }
        public void SaveStratumColors()
        {
            if ((Path != "") && (Directory.Exists(Path)) && (StratumColors.Length > 0))
            {
                FileStream fs = new FileStream(Path + "\\colors.bin", FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(StratumColors.Length);
                for (int i = 0; i < StratumColors.Length; i++)
                {
                    bw.Write(StratumColors[i].StratumCode);
                    bw.Write(StratumColors[i].ARGB);
                }
                bw.Close();
            }
            #region save CSV
            //if ((Path != "") && (Directory.Exists(Path)) && (PlastColors.Length > 0))
            //{
            //    FileStream fs = new FileStream(Path + "\\colors.csv", FileMode.Create);
            //    StreamWriter sw = new StreamWriter(fs, Encoding.GetEncoding(1251));
            //    sw.WriteLine(PlastColors.Length);
            //    for (int i = 0; i < PlastColors.Length; i++)
            //    {
            //        sw.WriteLine(string.Format("{0};{1};", PlastColors[i].PlastCode, PlastColors[i].ARGB));
            //    }
            //    sw.Close();
            //}
            #endregion
        }
        bool ConversionItemAdded
        {
            get
            {
                for (int i = 0; i < StratumConvList.Count; i++)
                {
                    if (StratumConvList[i].ConversionItemsChanged) return true;
                }
                return false;
            }
        }
        public void SaveConversionList()
        {
            for (int i = 0; i < StratumConvList.Count; i++)
            {
                if (StratumConvList[i].ConversionItemsChanged) StratumConvList[i].SaveToFile();
            }
        }
        public void TestDictionaryUpdated()
        {
            if (StartedDictCount < this.Count) WriteToFile(Path);

            if (ConversionItemAdded) SaveConversionList();
        }
        #endregion

        #region STRATUM TREE
        public StratumTreeNode GetStratumTreeNode(int StratumCode)
        {
            if ((stratumTree != null) && (stratumTree.Count > 0))
            {
                return stratumTree.GetNodeByCode(StratumCode);
            }
            return null;
        }
        #endregion

        int AddAutoStratumCode(string NewStratumName)
        {
            int maxCode = 0;
            for (int i = 0; i < this.Count; i++)
            {
                if (maxCode < this[i].Code) maxCode = this[i].Code;
            }
            maxCode++;
            DataDictionaryItem item = new DataDictionaryItem();
            item.Code = maxCode;
            item.ShortName = NewStratumName;
            item.FullName = string.Empty;
            item.Note = string.Empty;
            this.Add(item);
            return maxCode;
        }
        public int ShowConversionForm(string ServerName, string OilFieldName, string WellName, string DataType, int StratumCode, string StratumName)
        {
            int res = -1;
            using (PlastConversionForm ConversionForm = new PlastConversionForm(this))
            {
                UserFormCounter++;
                if (ConversionForm.ShowDialog(ServerName, OilFieldName, WellName, DataType, StratumCode, StratumName) == DialogResult.OK)
                {
                    res = ConversionForm.ConversionID;
                }
                else
                {
                    res = -1;
                }
            }
            UserFormCounter--;
            return res;
        }
        public int GetStratumColor(int StratumCode)
        {
            if (StratumColors != null)
            {
                for (int i = 0; i < StratumColors.Length; i++)
                {
                    if (StratumCode == StratumColors[i].StratumCode)
                    {
                        return StratumColors[i].ARGB;
                    }
                }
            }
            return 0;
        }
        public int GetStratumColorByStratumTree(int StratumCode)
        {
            if (stratumTree != null)
            {
                StratumTreeNode node = GetStratumTreeNode(StratumCode);
                if (node != null)
                {
                    if (node.ARGB != -1) return node.ARGB;
                    else return node.GetParentStratumColor();
                }
            }
            return -1;
        }
        void WriteLog(string Text)
        {
            using (StreamWriter sw = new StreamWriter(Path + "\\DataLoad.csv" , true, Encoding.GetEncoding(1251)))
            {
                sw.WriteLine(Text);
            }
        }


        #region Converting Codes
        int GetLocalCodeByServerCode(bool IsStratumZone, int StratumConverterIndex, string OilFieldName, string WellName, string DataType, int ServerStratumCode)
        {
            int code = -1;
            string ServerStratumName = string.Empty, LocalStratumName = string.Empty;
            if ((StratumConverterIndex != -1) && (StratumConverterIndex < StratumConvList.Count))
            {
                ServerStratumConverter conv = StratumConvList[StratumConverterIndex];
                code = conv.GetLocalID(ServerStratumCode, IsStratumZone);
                if (code == -1)
                {
                    code = conv.GetNearLocalID(ServerStratumCode, IsStratumZone);
                    if (code == -1)
                    {
                        ServerStratumName = conv.GetServerStratumName(ServerStratumCode, IsStratumZone);
                        if (ServerStratumName.Length == 0)
                        {
                            code = 0;
                        }
#if AUTO
                        else
                        {
                            code = AddAutoStratumCode(ServerStratumName);
                        }
#else
                        else
                        {
                            code = ShowConversionForm(conv.ServerName, OilFieldName, WellName, DataType, ServerStratumCode, ServerStratumName);
                        }
#endif
                    }
                    int ind = this.GetIndexByCode(code);
                    if (ind != -1)
                    {
                        LocalStratumName = this[ind].ShortName;
                        ServerStratumName = conv.GetServerStratumName(ServerStratumCode, IsStratumZone);
                        conv.AddConversion(IsStratumZone, ServerStratumCode, ServerStratumName, code, LocalStratumName);
                    }
                }
            }
            return code;
        }
        public int TryGetLocalCodeByServerStratumName(string ServerStratumName)
        {
            int code = -1, ind;
            if ((ServerStratumName != null) && (ServerStratumName != ""))
            {
                string[] ConversionStartSymbols = new string[] { "С", "C", "СТ", "CT", "СT", "CT", "CТ", "CT", "Д", "D", "Р", "P" };
                string[] ConversionOtherSymbols = new string[] { "_", ".", "+", "-" };

                ind = GetIndexByShortName(ServerStratumName);
                if (ind != -1)
                {
                    code = this[ind].Code;
                }
                else
                {
                    int i;
                    string str1, str2, tempName;
                    for (i = 0; i < ConversionOtherSymbols.Length - 1; i += 2)
                    {
                        str1 = ConversionOtherSymbols[i];
                        str2 = ConversionOtherSymbols[i + 1];
                        ServerStratumName = ServerStratumName.Replace(str1, str2);
                    }
                    ind = GetIndexByShortName(ServerStratumName);
                    if (ind != -1)
                    {
                        code = this[ind].Code;
                    }
                    else
                    {
                        for (i = 0; i < ConversionStartSymbols.Length - 1; i += 2)
                        {
                            str1 = ConversionStartSymbols[i];
                            str2 = ConversionStartSymbols[i + 1];

                            if (ServerStratumName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                            {
                                tempName = str2 + ServerStratumName.Remove(0, str1.Length);
                                ind = GetIndexByShortName(tempName);
                                if (ind != -1)
                                {
                                    code = this[ind].Code;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return code;
        }
        int GetStratumConverterIndex(ServerTypeID ServerType, string ServerName, int Port, string OracleSid)
        {
            if (StratumConvList.Count == 0) LoadConversionList();

            int res = -1;
            for (int i = 0; i < StratumConvList.Count; i++)
            {
                if ((StratumConvList[i].ServerType == ServerType) &&
                    (StratumConvList[i].ServerPort == Port) &&
                    (StratumConvList[i].ServerName == ServerName))
                {
                    res = i;
                }
            }
            if (res == -1)
            {
                res = StratumConvList.Count;
                StratumConvList.Add(new ServerStratumConverter(ServerType, ServerName, Port, Path, this, OracleSid));
            }
            return res;
        }
        public int ConvertStratumCodes(ServerTypeID ServerType, SERVER_DATA_TYPE DataType, Oilfield of)
        {
            int res = 0;
            int ServerIndex = -1;
            switch (ServerType)
            {
                case ServerTypeID.OIS:
                    ServerIndex = GetStratumConverterIndex(ServerType, of.ParamsDict.OraServerName, of.ParamsDict.OraPort, of.ParamsDict.OraSid);
                    break;
            }

            switch (DataType)
            {
                case SERVER_DATA_TYPE.COORDINATES:
                    res = ConvertStratumCodesInCoord(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.AREA:
                    res = ConvertStratumCodesInAreas(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.MER:
                    res = ConvertStratumCodesInMer(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.MER_NEW:
                    res = ConvertStratumCodesInMer(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.GIS:
                    res = ConvertStratumCodesInGis(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.CONTOUR:
                    res = ConvertStratumCodesInContours(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.GRID:
                    res = ConvertStratumCodesInGrids(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.PVT:
                    res = ConvertStratumCodesInPVTParams(ServerIndex, of);
                    break;
                case SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE:
                    res = ConvertStratumCodesInWellResearch(ServerIndex, of);
                    break;
            }
            if (res == 1) SaveConversionList();
            return res;
        }

        int ConvertStratumCodesInCoord(int ServerIndex, Oilfield of)
        {
            if (of.CoordLoaded)
            {
                of.CoordLoaded = false;
                Well w;
                int j, stratumID;
                for (int i = 0; i < of.Wells.Count; i++)
                {
                    w = of.Wells[i];
                    if (w.CoordLoaded && w.Coord.IsServerStratumCodes)
                    {
                        for (j = 0; j < w.Coord.Count; j++)
                        {
                            stratumID = w.Coord.Items[j].PlastId;
                            stratumID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "Координаты", stratumID);

                            if (stratumID == -1)
                            {
                                w.Coord = null;
                                return -1;
                            }
                            else
                            {
                                w.Coord.Items[j].PlastId = (ushort)stratumID;
                                of.CoordLoaded = true;
                            }
                        }
                    }
                }
            }
            return (of.CoordLoaded) ? 1 : 0;
        }
        int ConvertStratumCodesInMer(int ServerIndex, Oilfield of)
        {
            if (of.MerLoaded)
            {
                of.MerLoaded = false;
                Well w;
                for (int i = 0; i < of.Wells.Count; i++)
                {
                    w = of.Wells[i];
                    if (w.MerLoaded && w.Mer.IsServerStratumCode)
                    {
                        int plastID;
                        for (int j = 0; j < w.Mer.Count; j++)
                        {
                            plastID = w.Mer.Items[j].PlastId;
                            plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "МЭР", plastID);
                            if (plastID == -1)
                            {
                                w.Mer = null;
                                return -1;
                            }
                            else
                            {
                                w.Mer.Items[j].PlastId = plastID;
                                of.MerLoaded = true;
                            }
                        }
                    }
                }
            }
            return (of.MerLoaded) ? 1 : 0;
        }
        int ConvertStratumCodesInGis(int ServerIndex, Oilfield of)
        {
            if (of.GisLoaded)
            {
                of.GisLoaded = false;
                Well w;
                for (int i = 0; i < of.Wells.Count; i++)
                {
                    w = of.Wells[i];
                    if (w.GisLoaded && w.Gis.IsServerStratumCode)
                    {
                        int plastID, plastZoneID;
                        for (int j = 0; j < w.Gis.Count; j++)
                        {
                            plastID = w.Gis.Items[j].PlastId;
                            plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "ГИС", plastID);
                            plastZoneID = w.Gis.Items[j].PlastZoneId;
                            plastZoneID = GetLocalCodeByServerCode(true, ServerIndex, of.Name, w.Name, "ГИС(Пропласток)", plastZoneID);

                            if (plastID == -1)
                            {
                                w.Gis = null;
                                return -1;
                            }
                            else
                            {
                                w.Gis.Items[j].PlastId = (ushort)plastID;
                                of.GisLoaded = true;
                            }
                            if (plastZoneID == -1)
                            {
                                w.Gis = null;
                                return -1;
                            }
                            else
                            {
                                w.Gis.Items[j].PlastZoneId = (ushort)plastZoneID;
                                of.GisLoaded = true;
                            }
                        }
                    }
                }
            }
            return (of.GisLoaded) ? 1 : 0;    
        }
        int ConvertStratumCodesInContours(int ServerIndex, Oilfield of)
        {
            if ((of.ContoursLoaded) && (of.Contours.Count > 0))
            {
                int plastID = 0;
                for (int i = 0; i < of.Contours.Count; i++)
                {
                    if (of.Contours[i].IsServerStratumCode)
                    {
                        plastID = of.Contours[i].StratumCode;
                        if (plastID > -1)
                        {
                            plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, "", "Контуры", plastID);
                            if (plastID == -1)
                            {
                                return -1;
                            }
                            of.Contours[i].StratumCode = plastID;
                        }
                    }
                }
                return 1;
            }
            return 0;
        }
        int ConvertStratumCodesInAreas(int ServerIndex, Oilfield of)
        {
            if (of.Areas.Count > 0)
            {
                int i, plastID = 0;
                Area area;
                List<int> areaPlastCodes = new List<int>();
                for (i = 0; i < of.Areas.Count; i++)
                {
                    area = (Area)of.Areas[i];
                    if (area.IsServerStratumCode)
                    {
                        plastID = area.StratumCode;
                        if (plastID == 0)
                        {
                            if (area.contour.Name.Length == 0)
                                plastID = TryGetLocalCodeByServerStratumName(area.Name);
                            else
                                plastID = TryGetLocalCodeByServerStratumName(area.contour.Name);
                        }
                        else
                        {
                            plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, "", "Ячейки заводнения", plastID);
                        }
                        if (plastID == -1)
                        {
                            return -1;
                        }
                        area.StratumCode = plastID;
                        area.contour.Name = area.Name;
                        if (areaPlastCodes.IndexOf(area.StratumCode) == -1) areaPlastCodes.Add(area.StratumCode);
                    }
                }
                bool find = false, find2 = false;
                for (i = 0; i < areaPlastCodes.Count; i++)
                {
                    if ((areaPlastCodes[i] != -1) && (of.MerOilObjects.IndexOf(areaPlastCodes[i]) == -1))
                    {
                        find = false;
                        StratumTreeNode node;
                        for (int j = 0; j < of.MerOilObjects.Count; j++)
                        {
                            node = GetStratumTreeNode(of.MerOilObjects[j]);

                            if (node != null && node.GetParentLevelByCode(areaPlastCodes[i]) > -1)
                            {
                                find = true;
                                find2 = false;
                                for (int k = 0; k < areaPlastCodes.Count; k++)
                                {
                                    if (k != i && (node.StratumCode == areaPlastCodes[k]))
                                    {
                                        find2 = true;
                                        break;
                                    }
                                }
                            }
                        }
                        // оставляем тока с объектами с ненулевой добычей/закачкой
                        if (!find || find2)
                        {
#if DEBUG
//                            System.Diagnostics.Debug.WriteLine(string.Format("Удаление ячеек пласта {0} месторождения {1}", GetStratumTreeNode(areaPlastCodes[i]).Name, of.Name));
#endif
                            //if (GetPlastTreeNode(areaPlastCodes[i]).Name != "Cалк.к")
                            //{
                            //for (int k = 0; k < of.AreaList.Length; k++)
                            //{
                            //    if (((Area)of.AreaList[k]).PlastCode == areaPlastCodes[i]) ((Area)of.AreaList[k]).PlastCode = -1;
                            //}
                            //}
                        }
                    }
                }
                return 1;
            }
            return 0;
        }
        int ConvertStratumCodesInGrids(int ServerIndex, Oilfield of)
        {
            if (of.Grids.Count > 0)
            {
                int plastID = 0;
                bool removed = false;
                for (int i = 0; i < of.Grids.Count; i++)
                {
                    Grid grid = of.Grids[i], gr;
                    if (grid.IsServerStratumCode)
                    {
                        plastID = grid.StratumCode;
                        plastID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, "", "Сетки", plastID);

                        if (plastID == -1)
                        {
                            return -1;
                        }
                        grid.StratumCode = plastID;
                        grid.OilFieldCode = of.ParamsDict.FieldCode;

                        bool find = false;
                        for (int j = 0; j < of.Grids.Count; j++)
                        {
                            if (j != i)
                            {
                                gr = (Grid)of.Grids[j];
                                if (gr.Name == grid.Name &&
                                    gr.GridType == grid.GridType &&
                                    gr.StratumCode == grid.StratumCode &&
                                    gr.nx == grid.nx && gr.ny == grid.ny &&
                                    gr.Zmin == grid.Zmin && gr.Zmax == grid.Zmax)
                                {
                                    find = true;
                                    break;
                                }
                            }
                        }
                        if (find)
                        {
                            StratumTreeNode node = GetStratumTreeNode(grid.StratumCode);
                            System.Diagnostics.Debug.WriteLine(string.Format("Удалена дублирующая сетка на пласт {0}", ((node == null) ? grid.StratumCode.ToString() : node.Name)));
                            of.Grids.RemoveAt(i);
                            removed = true;
                            i--;
                        }
                    }
                }
                if (removed)
                {
                    for (int i = 0; i < of.Grids.Count; i++)
                    {
                        of.Grids[i].Index = i;
                    }
                }
                return 1;
            }
            return 0;
        }
        int ConvertStratumCodesInPVTParams(int ServerIndex, Oilfield of)
        {
            if (of.PVTLoaded && (of.PVT.Count > 0))
            {
                int stratumID = 0;
                for (int i = 0; i < of.PVT.Count; i++)
                {
                    if (of.PVT.IsServerStratumCode)
                    {
                        DataLoader.Objects.Data.PVTParamsItem item = of.PVT[i];
                        stratumID = item.StratumCode;
                        stratumID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, "", "PVT свойства", stratumID);
                        if (stratumID == -1) return -1;
                        item.StratumCode = stratumID;
                        of.PVT[i] = item;
                    }
                }
                return 1;
            }
            return 0;
        }
        int ConvertStratumCodesInWellResearch(int ServerIndex, Oilfield of)
        {
            Well w;
            int result = 0;
            for (int i = 0; i < of.Wells.Count; i++)
            {
                w = of.Wells[i];
                if (w.ResearchLoaded && w.Research.IsServerStratumCode)
                {
                    int stratumID;
                    for (int j = 0; j < w.Research.Count; j++)
                    {
                        stratumID = w.Research[j].StratumCode;
                        stratumID = GetLocalCodeByServerCode(false, ServerIndex, of.Name, w.Name, "Замеры скважин", stratumID);
                        if (stratumID == -1)
                        {
                            // Пользователь нажал отмену в окне выбора пласта из локального справочника
                            w.Research = null;
                            return -1;
                        }
                        else
                        {
                            DataLoader.Objects.Data.WellResearchItem item = w.Research[j];
                            item.StratumCode = (ushort)stratumID;
                            w.Research[j] = item;
                            result = 1;
                        }
                    }
                }
            }
            return result;
        }
        #endregion
    }
}
