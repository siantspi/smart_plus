﻿namespace DataLoader.DictionaryObjects
{
    partial class PlastTreeCreatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            tree = new System.Windows.Forms.TreeView();
            label1 = new System.Windows.Forms.Label();
            groupBox1 = new System.Windows.Forms.GroupBox();
            bAddChild = new System.Windows.Forms.Button();
            bAddParent = new System.Windows.Forms.Button();
            gridPlasts = new System.Windows.Forms.DataGridView();
            Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            tbFindName = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            bClose = new System.Windows.Forms.Button();
            bSave = new System.Windows.Forms.Button();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(gridPlasts)).BeginInit();
            SuspendLayout();
            // 
            // tree
            // 
            tree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            tree.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            tree.HideSelection = false;
            tree.Location = new System.Drawing.Point(12, 27);
            tree.Name = "tree";
            tree.Size = new System.Drawing.Size(276, 485);
            tree.TabIndex = 0;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(9, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(152, 15);
            label1.TabIndex = 1;
            label1.Text = "Дерево иерархии пластов";
            // 
            // groupBox1
            // 
            groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            groupBox1.Controls.Add(bAddChild);
            groupBox1.Controls.Add(bAddParent);
            groupBox1.Controls.Add(gridPlasts);
            groupBox1.Controls.Add(tbFindName);
            groupBox1.Controls.Add(label2);
            groupBox1.Location = new System.Drawing.Point(294, 18);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(362, 458);
            groupBox1.TabIndex = 7;
            groupBox1.TabStop = false;
            groupBox1.Text = " Внутренний справочник пластов ";
            // 
            // bAddChild
            // 
            bAddChild.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            bAddChild.Location = new System.Drawing.Point(201, 422);
            bAddChild.Name = "bAddChild";
            bAddChild.Size = new System.Drawing.Size(155, 30);
            bAddChild.TabIndex = 11;
            bAddChild.Text = "Добавить в наследники";
            bAddChild.UseVisualStyleBackColor = true;
            bAddChild.Click += new System.EventHandler(bAddChild_Click);
            // 
            // bAddParent
            // 
            bAddParent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            bAddParent.Location = new System.Drawing.Point(6, 422);
            bAddParent.Name = "bAddParent";
            bAddParent.Size = new System.Drawing.Size(155, 30);
            bAddParent.TabIndex = 10;
            bAddParent.Text = "Добавить в родители";
            bAddParent.UseVisualStyleBackColor = true;
            bAddParent.Click += new System.EventHandler(bAddParent_Click);
            // 
            // gridPlasts
            // 
            gridPlasts.AllowUserToAddRows = false;
            gridPlasts.AllowUserToDeleteRows = false;
            gridPlasts.AllowUserToOrderColumns = true;
            gridPlasts.AllowUserToResizeRows = false;
            gridPlasts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            gridPlasts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            gridPlasts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            gridPlasts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            Column1,
            Column2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            gridPlasts.DefaultCellStyle = dataGridViewCellStyle4;
            gridPlasts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            gridPlasts.Location = new System.Drawing.Point(6, 66);
            gridPlasts.Name = "gridPlasts";
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            gridPlasts.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            gridPlasts.RowHeadersVisible = false;
            gridPlasts.RowTemplate.Height = 14;
            gridPlasts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            gridPlasts.ShowEditingIcon = false;
            gridPlasts.Size = new System.Drawing.Size(348, 350);
            gridPlasts.TabIndex = 9;
            // 
            // Column1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            Column1.DefaultCellStyle = dataGridViewCellStyle2;
            Column1.HeaderText = "Код";
            Column1.Name = "Column1";
            Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            Column2.DefaultCellStyle = dataGridViewCellStyle3;
            Column2.HeaderText = "Имя пласта";
            Column2.Name = "Column2";
            Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            Column2.Width = 230;
            // 
            // tbFindName
            // 
            tbFindName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            tbFindName.Location = new System.Drawing.Point(6, 37);
            tbFindName.Name = "tbFindName";
            tbFindName.Size = new System.Drawing.Size(348, 23);
            tbFindName.TabIndex = 8;
            // 
            // label2
            // 
            label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(3, 18);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(220, 15);
            label2.TabIndex = 7;
            label2.Text = "Начните ввод для фильтрации списка:";
            // 
            // bClose
            // 
            bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            bClose.Location = new System.Drawing.Point(558, 482);
            bClose.Name = "bClose";
            bClose.Size = new System.Drawing.Size(90, 30);
            bClose.TabIndex = 8;
            bClose.Text = "Закрыть";
            bClose.UseVisualStyleBackColor = true;
            bClose.Click += new System.EventHandler(bClose_Click);
            // 
            // bSave
            // 
            bSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            bSave.Enabled = false;
            bSave.Location = new System.Drawing.Point(300, 482);
            bSave.Name = "bSave";
            bSave.Size = new System.Drawing.Size(90, 30);
            bSave.TabIndex = 9;
            bSave.Text = "Сохранить";
            bSave.UseVisualStyleBackColor = true;
            bSave.Click += new System.EventHandler(bSave_Click);
            // 
            // PlastTreeCreatorForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(660, 524);
            Controls.Add(bSave);
            Controls.Add(bClose);
            Controls.Add(groupBox1);
            Controls.Add(label1);
            Controls.Add(tree);
            Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "PlastTreeCreatorForm";
            Text = "Иерархия пластов";
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(gridPlasts)).EndInit();
            ResumeLayout(false);
            PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView tree;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bAddChild;
        private System.Windows.Forms.Button bAddParent;
        private System.Windows.Forms.DataGridView gridPlasts;
        private System.Windows.Forms.TextBox tbFindName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bClose;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}