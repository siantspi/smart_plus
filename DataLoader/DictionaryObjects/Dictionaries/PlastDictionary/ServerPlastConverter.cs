﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.OracleClient;

namespace DataLoader.DictionaryObjects
{
    public enum ServerTypeID : byte
    {
        OIS
    }
    public struct ConversionItem
    {
        public bool IsStratumZone;
        public int serverID;
        public string serverName;
        public int localID;
        public string localName;
    }
    public sealed class ServerStratumConverter
    {
        public string ServerName;
        public int ServerPort;
        public string OracleSid;
        public ServerTypeID ServerType;
        DataDictionary servDict;
        DataDictionary servStratumZoneDict;
        DataDictionary locDict;
        string StratumDictPath;
        public bool ConversionItemsChanged;

        List<ConversionItem> ConvList;
        string[] ConversionStartSymbols = new string[] { "С", "C", "СТ", "CT", "СT", "CT", "CТ", "CT", "Д", "D", "Р", "P"};
        string[] ConversionOtherSymbols = new string[] { /*"_", ".", "+", "-"*/};
        string[] ConversionTryReplaceSymbols = new string[] { " ", "/" };

        public ServerStratumConverter(ServerTypeID ServerType, string ServerName, int ServerPort, string Path, DataDictionary LocalDict, string OracleSid)
        {
            this.ServerType = ServerType;
            this.ServerName = ServerName;
            this.ServerPort = ServerPort;
            StratumDictPath = Path;
            servDict = null;
            servStratumZoneDict = null;
            locDict = LocalDict;
            ConvList = new List<ConversionItem>();
            ConversionItemsChanged = false;
            this.OracleSid = OracleSid;
        }

        bool LoadServerDictionary()
        {
            bool result = false;
            switch(ServerType)
            {
                 case ServerTypeID.OIS:
                    // Нужен логин и пароль....
                    string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                                ServerName + ")(PORT=" + ServerPort.ToString() +
                                ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + OracleSid + ")));" +
                                "USER ID= " + "OPER_GUEST" + ";PASSWORD=" + "OPER_GUEST" + ";";
                    using (OracleConnection conn = new OracleConnection(connString))
                    {
                        OracleCommand oraCommand = conn.CreateCommand();

                        #region SQL запрос Справочника пластов для Well Research
                        oraCommand.CommandText =
                        "SELECT * FROM DICTADMIN.MV_COUNT_OBJECT ORDER BY ID";
                        #endregion

                        OracleDataReader oraReader;
                        try
                        {
                            conn.Open();
                            oraReader = oraCommand.ExecuteReader();
                            bool res = oraReader.Read();
                            servDict = new DataDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                            MemoryStream ms = new MemoryStream();
                            BinaryWriter bw = new BinaryWriter(ms);
                            int count = 0;

                            while (res)
                            {
                                bw.Write(Convert.ToInt32(oraReader[0]));
                                bw.Write(Convert.ToString(oraReader[1]));
                                res = oraReader.Read();
                                count++;
                            }
                            if (count > 0)
                            {
                                DataDictionaryItem item;
                                servDict.Add(new DataDictionaryItem());
                                ms.Seek(0, SeekOrigin.Begin);

                                BinaryReader br = new BinaryReader(ms);
                                for (int i = 1; i < count; i++)
                                {
                                    item = new DataDictionaryItem();
                                    item.Code = br.ReadInt32();
                                    item.ShortName = br.ReadString();
                                    servDict.Add(item);
                                }
                                br.Close();
                            }
                            bw.Close();
                            ms.Dispose();
                            oraReader.Close();
                            return true;
                        }
                        catch
                        {
                            servDict = null;
                            return false;
                        }
                    }
            }
            return result;
        }
        public string GetServerStratumName(int ServerStratumID, bool IsStratumZone)
        {
            if ((servDict == null) || (servStratumZoneDict == null)) LoadServerDictionary();
            DataDictionary dict = (IsStratumZone ? servStratumZoneDict : servDict);
            if (dict != null)
            {
                string servStratumName = string.Empty;
                int ind = dict.GetIndexByCode(ServerStratumID);
                if (ind != -1) servStratumName = dict[ind].ShortName;
                return servStratumName;
            }
            return string.Empty;
        }
        public int GetLocalID(int ServerStratumID, bool IsStratumZone)
        {
            if ((servDict == null) || (servStratumZoneDict == null)) LoadServerDictionary();
            DataDictionary dict = (IsStratumZone ? servStratumZoneDict : servDict);

            for (int i = 0; i < ConvList.Count; i++)
            {
                if ((ConvList[i].IsStratumZone == IsStratumZone) && (ConvList[i].serverID == ServerStratumID))
                {
                    int ind = dict.GetIndexByCode(ConvList[i].serverID);
                    if (ind == -1 || dict[ind].ShortName != ConvList[i].serverName)
                    {
                        ConvList.RemoveAt(i);
                        ConversionItemsChanged = true;
                        break;
                    }
                    else
                    {
                        return ConvList[i].localID;
                    }
                }
            }
            return -1;
        }
        public int GetNearLocalID(int ServerStratumID, bool IsStratumZone)
        {
            int res = -1;
            if ((servDict == null) || (servStratumZoneDict == null)) LoadServerDictionary();
            DataDictionary dict = (IsStratumZone ? servStratumZoneDict : servDict);

            if (dict != null)
            {
                int ind = dict.GetIndexByCode(ServerStratumID);

                if (ind != -1)
                {
                    string servPlastName = dict[ind].ShortName;

                    ind = locDict.GetIndexByShortName(ServerName);
                    if (ind != -1)
                    {
                        res = locDict[ind].Code;
                    }
                    else
                    {
                        int i;
                        string str1, str2, tempName;
                        for (i = 0; i < ConversionOtherSymbols.Length - 1; i += 2)
                        {
                            str1 = ConversionOtherSymbols[i];
                            str2 = ConversionOtherSymbols[i + 1];
                            servPlastName = servPlastName.Replace(str1, str2);
                        }
                        ind = locDict.GetIndexByShortName(servPlastName);
                        
                        if (ind != -1)
                        {
                            res = locDict[ind].Code;
                        }
                        else
                        {
                            for (i = 0; i < ConversionStartSymbols.Length - 1; i += 2)
                            {
                                str1 = ConversionStartSymbols[i];
                                str2 = ConversionStartSymbols[i + 1];

                                if (servPlastName.StartsWith(str1, StringComparison.OrdinalIgnoreCase))
                                {
                                    tempName = str2 + servPlastName.Remove(0, str1.Length);
                                    ind = locDict.GetIndexByShortName(tempName);
                                    if (ind != -1)
                                    {
                                        res = locDict[ind].Code;
                                        break;
                                    }
                                }
                            }
                            if (res == -1)
                            {
                                for (i = 0; i < ConversionTryReplaceSymbols.Length - 1; i += 2)
                                {
                                    str1 = ConversionTryReplaceSymbols[i];
                                    str2 = ConversionTryReplaceSymbols[i + 1];
                                    tempName = servPlastName.Replace(str1, str2);
                                    ind = locDict.GetIndexByShortName(tempName);
                                    if (ind != -1)
                                    {
                                        res = locDict[ind].Code;
                                        break;
                                    }
                                } 
                            }
                        }
                    }
                }
            }
            return res;
        }

        public void AddConversion(bool IsStratumZone, int ServerStratumID, string ServerStratumName, int LocalStratumID, string LocalStratumName)
        {
            ConversionItem item;
            item.IsStratumZone = IsStratumZone;
            item.serverID = ServerStratumID;
            item.serverName = ServerStratumName;
            item.localID = LocalStratumID;
            item.localName = LocalStratumName;
            ConvList.Add(item);
            ConversionItemsChanged = true;
        }

        public bool LoadFromFile()
        {
            string fileName = System.Windows.Forms.Application.StartupPath + "\\" + ((byte)ServerType).ToString() + "_" + ServerName + "_" + OracleSid + "_" + ServerPort.ToString() + ".bin";
            if ((StratumDictPath != "") && (File.Exists(fileName)))
            {
                try
                {
                    ConversionItem item;
                    FileStream fs = new FileStream(fileName, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);
                    int count = br.ReadInt32();
                    for (int i = 0; i < count; i++)
                    {
                        item.IsStratumZone = br.ReadBoolean();
                        item.serverID = br.ReadInt32();
                        item.serverName = br.ReadString();
                        item.localID = br.ReadInt32();
                        item.localName = br.ReadString();
                        ConvList.Add(item);
                    }
                    br.Close();
                }
                catch
                {
                    ConvList.Clear();
                    return false;
                }
                return true;
            }
            return false;
        }
        public void SaveToFile()
        {
            if ((ConvList.Count > 0) && (StratumDictPath.Length > 0) && (Directory.Exists(StratumDictPath)))
            {
                string fileName = System.Windows.Forms.Application.StartupPath + "\\" + ((byte)ServerType).ToString() + "_" + ServerName + "_" + OracleSid + "_" + ServerPort.ToString() + ".bin";
                FileStream fs = new FileStream(fileName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(ConvList.Count);
                int count = 0;
                for (int i = 0; i < ConvList.Count; i++)
                {
                    if ((ConvList[i].localID != 0) || (ConvList[i].serverID == 0))
                    {
                        bw.Write(ConvList[i].IsStratumZone);
                        bw.Write(ConvList[i].serverID);
                        bw.Write(ConvList[i].serverName);
                        bw.Write(ConvList[i].localID);
                        bw.Write(ConvList[i].localName);
                        count++;
                    }
                }
                fs.Seek(0, SeekOrigin.Begin);
                bw.Write(count);
                bw.Close();
            }
        }
    }
}
