﻿namespace DataLoader.DictionaryObjects
{
    partial class PlastConversionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tbFindName = new System.Windows.Forms.TextBox();
            this.gridPlasts = new System.Windows.Forms.DataGridView();
            this.PlastID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bOk = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPlastName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPlastCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bNewPlast = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbOilField = new System.Windows.Forms.TextBox();
            this.tbWellName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbDataType = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bCopyServerName = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridPlasts)).BeginInit();
            this.SuspendLayout();
            // 
            // tbFindName
            // 
            this.tbFindName.Location = new System.Drawing.Point(4, 236);
            this.tbFindName.Name = "tbFindName";
            this.tbFindName.Size = new System.Drawing.Size(254, 23);
            this.tbFindName.TabIndex = 3;
            // 
            // gridPlasts
            // 
            this.gridPlasts.AllowUserToAddRows = false;
            this.gridPlasts.AllowUserToDeleteRows = false;
            this.gridPlasts.AllowUserToResizeRows = false;
            this.gridPlasts.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridPlasts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridPlasts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPlasts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PlastID,
            this.PlastName});
            this.gridPlasts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridPlasts.Location = new System.Drawing.Point(4, 270);
            this.gridPlasts.MultiSelect = false;
            this.gridPlasts.Name = "gridPlasts";
            this.gridPlasts.RowHeadersVisible = false;
            this.gridPlasts.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.gridPlasts.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridPlasts.RowTemplate.Height = 18;
            this.gridPlasts.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridPlasts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridPlasts.ShowEditingIcon = false;
            this.gridPlasts.Size = new System.Drawing.Size(387, 389);
            this.gridPlasts.TabIndex = 9;
            this.gridPlasts.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridPlasts_CellMouseDoubleClick);
            // 
            // PlastID
            // 
            this.PlastID.HeaderText = "Код";
            this.PlastID.Name = "PlastID";
            this.PlastID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PlastID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PlastName
            // 
            this.PlastName.HeaderText = "Пласт";
            this.PlastName.Name = "PlastName";
            this.PlastName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlastName.Width = 270;
            // 
            // bOk
            // 
            this.bOk.Location = new System.Drawing.Point(4, 665);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(90, 30);
            this.bOk.TabIndex = 10;
            this.bOk.Text = "OK";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(301, 665);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(90, 30);
            this.bCancel.TabIndex = 11;
            this.bCancel.Text = "Отмена";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(387, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Для текущего кода пласта на сервере выберите \r\nнаиболее подходящий из справочника" +
    "";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbPlastName
            // 
            this.tbPlastName.Location = new System.Drawing.Point(188, 182);
            this.tbPlastName.Name = "tbPlastName";
            this.tbPlastName.ReadOnly = true;
            this.tbPlastName.Size = new System.Drawing.Size(203, 23);
            this.tbPlastName.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(139, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 15);
            this.label4.TabIndex = 19;
            this.label4.Text = "Пласт:";
            // 
            // tbPlastCode
            // 
            this.tbPlastCode.Location = new System.Drawing.Point(56, 182);
            this.tbPlastCode.Name = "tbPlastCode";
            this.tbPlastCode.ReadOnly = true;
            this.tbPlastCode.Size = new System.Drawing.Size(77, 23);
            this.tbPlastCode.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 15);
            this.label3.TabIndex = 17;
            this.label3.Text = "Код:";
            // 
            // tbServer
            // 
            this.tbServer.Location = new System.Drawing.Point(56, 60);
            this.tbServer.Name = "tbServer";
            this.tbServer.ReadOnly = true;
            this.tbServer.Size = new System.Drawing.Size(335, 23);
            this.tbServer.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 15;
            this.label2.Text = "Сервер:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(254, 15);
            this.label5.TabIndex = 21;
            this.label5.Text = "Начните ввод для фильтрации справочника:";
            // 
            // bNewPlast
            // 
            this.bNewPlast.Location = new System.Drawing.Point(301, 231);
            this.bNewPlast.Name = "bNewPlast";
            this.bNewPlast.Size = new System.Drawing.Size(90, 30);
            this.bNewPlast.TabIndex = 22;
            this.bNewPlast.Text = "Добавить";
            this.bNewPlast.UseVisualStyleBackColor = true;
            this.bNewPlast.Click += new System.EventHandler(this.bNewPlast_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 15);
            this.label6.TabIndex = 23;
            this.label6.Text = "Мест:";
            // 
            // tbOilField
            // 
            this.tbOilField.Location = new System.Drawing.Point(56, 90);
            this.tbOilField.Name = "tbOilField";
            this.tbOilField.ReadOnly = true;
            this.tbOilField.Size = new System.Drawing.Size(335, 23);
            this.tbOilField.TabIndex = 24;
            // 
            // tbWellName
            // 
            this.tbWellName.Location = new System.Drawing.Point(73, 119);
            this.tbWellName.Name = "tbWellName";
            this.tbWellName.ReadOnly = true;
            this.tbWellName.Size = new System.Drawing.Size(318, 23);
            this.tbWellName.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 15);
            this.label7.TabIndex = 25;
            this.label7.Text = "Скважина:";
            // 
            // tbDataType
            // 
            this.tbDataType.Location = new System.Drawing.Point(82, 153);
            this.tbDataType.Name = "tbDataType";
            this.tbDataType.ReadOnly = true;
            this.tbDataType.Size = new System.Drawing.Size(309, 23);
            this.tbDataType.TabIndex = 28;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 15);
            this.label8.TabIndex = 27;
            this.label8.Text = "Тип данных:";
            // 
            // bCopyServerName
            // 
            this.bCopyServerName.Image = global::DataLoader.Properties.Resources.Left16;
            this.bCopyServerName.Location = new System.Drawing.Point(265, 231);
            this.bCopyServerName.Name = "bCopyServerName";
            this.bCopyServerName.Size = new System.Drawing.Size(30, 30);
            this.bCopyServerName.TabIndex = 29;
            this.bCopyServerName.UseVisualStyleBackColor = true;
            this.bCopyServerName.Click += new System.EventHandler(this.bCopyServerName_Click);
            // 
            // PlastConversionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 702);
            this.ControlBox = false;
            this.Controls.Add(this.bCopyServerName);
            this.Controls.Add(this.tbDataType);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbWellName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbOilField);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bNewPlast);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbPlastName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbPlastCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbServer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.gridPlasts);
            this.Controls.Add(this.tbFindName);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlastConversionForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выберите подходящий код из справочника";
            ((System.ComponentModel.ISupportInitialize)(this.gridPlasts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFindName;
        private System.Windows.Forms.DataGridView gridPlasts;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPlastName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbPlastCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bNewPlast;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbOilField;
        private System.Windows.Forms.TextBox tbWellName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlastID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlastName;
        private System.Windows.Forms.TextBox tbDataType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button bCopyServerName;
    }
}