﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.DictionaryObjects
{
    public class DataDictionary : Dictionary<DataDictionaryItem>
    {
        public DataDictionary(DICTIONARY_ITEM_TYPE Type) : base(Type) { }

        public int GetIndexByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Code == this[i].Code)
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetIndexByShortName(string ShortName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ShortName.Equals(this[i].ShortName, StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
