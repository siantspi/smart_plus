﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.DictionaryObjects
{
    class OilFieldParamsDictionary : Dictionary<OilFieldParamsItem>
    {
        public OilFieldParamsDictionary() : base(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS) { }
    }
}
