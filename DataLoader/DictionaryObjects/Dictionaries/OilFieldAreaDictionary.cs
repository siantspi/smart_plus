﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.DictionaryObjects
{
    class OilFieldAreaDictionary : Dictionary<OilFieldAreaDictItem>
    {
        public OilFieldAreaDictionary() : base(DICTIONARY_ITEM_TYPE.OILFIELD_AREA) { }

        public List<int> GetAreaIdByOilfieldCode(int OilfieldCode)
        {
            List<int> areas = new List<int>();
            for (int i = 0; i < Count; i++)
            {
                if ((this[i].Type == 4) && (OilfieldCode == this[i].OilFieldCode))
                {
                    areas.Add(i);
                }
            }
            return areas;
        }
        public int GetIndexByCode(int Code)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Code == this[i].Code)
                {
                    return i;
                }
            }
            return -1;
        }
        public int GetIndexByShortName(string ShortName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (ShortName.Equals(this[i].Name, StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
