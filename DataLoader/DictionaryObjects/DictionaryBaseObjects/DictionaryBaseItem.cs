﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.DictionaryObjects
{
    public class DictionaryBaseItem
    {
        public int Index { get; set; }

        public virtual bool TestCode(int Code) { return false; }
        public virtual string GetFileHead() { return ""; }
        public virtual void Parse(string Source) { }
        new public virtual string ToString() { return ""; }
    }
}
