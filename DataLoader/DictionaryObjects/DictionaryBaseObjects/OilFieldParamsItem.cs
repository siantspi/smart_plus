﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DataLoader.DictionaryObjects
{
    public class OilFieldParamsItem : DictionaryBaseItem
    {
        public string OilFieldName;
        public int EnterpriseCode;
        public string EnterpriseName;
        public int NGDUCode;
        public string NGDUName;
        public int FieldCode;
        public string ServerName;
        public int Port;
        public string Login;
        public string Pass;
        public byte LoadByAreas;
        public int OraFieldCode;
        public string OraServerName;
        public int OraPort;
        public string OraSid;
        public string OraLogin;
        public string OraPass;

        public OilFieldParamsItem()
        { }
        public OilFieldParamsItem(OilFieldParamsItem Item)
        {
            OilFieldName = Item.OilFieldName;
            EnterpriseCode = Item.EnterpriseCode;
            EnterpriseName = Item.EnterpriseName;
            NGDUCode = Item.NGDUCode;
            NGDUName = Item.NGDUName;
            FieldCode = Item.FieldCode;
            ServerName = Item.ServerName;
            Port = Item.Port;
            Login = Item.Login;
            Pass = Item.Pass;
            LoadByAreas = Item.LoadByAreas;
            OraFieldCode = Item.OraFieldCode;
            OraPort = Item.OraPort;
            OraSid = Item.OraSid;
            OraLogin = Item.OraLogin;
            OraPass = Item.OraPass;
        }
        new public bool Equals(OilFieldParamsItem Item)
        {
            return ((OilFieldName == Item.OilFieldName) &&
                    (EnterpriseCode == Item.EnterpriseCode) &&
                    (EnterpriseName == Item.EnterpriseName) &&
                    (NGDUCode == Item.NGDUCode) &&
                    (NGDUName == Item.NGDUName) &&
                    (FieldCode == Item.FieldCode) &&
                    (ServerName == Item.ServerName) &&
                    (Port == Item.Port) &&
                    (Login == Item.Login) &&
                    (Pass == Item.Pass) &&
                    (LoadByAreas == Item.LoadByAreas) &&
                    (OraFieldCode == Item.OraFieldCode) &&
                    (OraServerName == Item.OraServerName) &&
                    (OraPort == Item.OraPort) &&
                    (OraSid == Item.OraSid) &&
                    (OraLogin == Item.OraLogin) &&
                    (OraPass == Item.OraPass));
        }
        public override string GetFileHead()
        {
            return "МЕСТОРОЖДЕНИЕ;РЕГИОН;ОБЪЕДИНЕНИЕ;НГДУ КОД;НГДУ;ID;SERVER;PORT;LOAD_AREAS;ORA ID;ORA SERVER;ORA PORT;ORA SID;ORA USER;";
        }
        public override void Parse(string Source)
        {
            string [] parseStr = Source.Split(new char[] {';'});
            OilFieldName = parseStr[0];
            EnterpriseCode = System.Convert.ToInt32(parseStr[1]);
            EnterpriseName = parseStr[2];
            NGDUCode = System.Convert.ToInt32(parseStr[3]);
            NGDUName = parseStr[4];
            
            FieldCode = System.Convert.ToInt32(parseStr[5]);
            ServerName = parseStr[6];
            Port = System.Convert.ToInt32(parseStr[7]);
            Login = "";
            Pass = string.Empty;
            LoadByAreas = System.Convert.ToByte(parseStr[8]);

            // ORACLE 
            if (parseStr[9].Length > 0) OraFieldCode = System.Convert.ToInt32(parseStr[9]);
            OraServerName = parseStr[10];
            if (parseStr[11].Length > 0) OraPort = System.Convert.ToInt32(parseStr[11]);
            OraSid = parseStr[12];
            OraLogin = "oper_guest";// parseStr[13];
            OraPass = "oper_guest";// string.Empty;
        }
        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};",
                OilFieldName,
                EnterpriseCode,
                EnterpriseName,
                NGDUCode,
                NGDUName,
                FieldCode,
                ServerName,
                Port,
                LoadByAreas,
                OraFieldCode,
                OraServerName,
                OraPort,
                OraSid,
                OraLogin);
        }
    }
}
