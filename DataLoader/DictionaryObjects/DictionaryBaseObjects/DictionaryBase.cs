﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DataLoader.DictionaryObjects
{
    public enum DICTIONARY_ITEM_TYPE : byte
    {
        OILFIELD_COEFFICIENT,
        OILFIELD_PARAMS,
        AGENT_INJ,
        BORE_TYPE,
        BOTTOM_STATE,
        CHARWORK,
        CONTOUR_TYPE,
        CONTRACTOR,
        FIELD_DEVELOP_DEGREE,
        FIELD_TYPE_HC,
        FIELD_TYPE_TPO,
        GEOPLASTZONE,
        GTA_TYPE,
        GTM_INJ_TYPE,
        GTM_TYPE,
        LITOLOGY,
        LOG_MNEMONIC,
        LOG_UNIT,
        MEST,
        METHOD,
        PACKER_TYPE,
        PBD_AUTHOR,
        PERFORATOR_TYPE,
        PERF_TYPE,
        STRATUM,
        PRED,
        PROJ_DESTINATION,
        PROJ_MEROPR,
        PUMP_TYPE,
        ROCKER_TYPE,
        SATURATION,
        STATE,
        STAY_REASON,
        CONSTRUCTION,
        OILFIELD_AREA,
        GRID_TYPE,
        WELL_ACTION_TYPE,
        WELL_RESEARCH_TYPE
    }

    public abstract class DictionaryBase
    {
        public DICTIONARY_ITEM_TYPE Type { get; set; }
        public string FileName
        {
            get
            {
                switch (Type)
                {
                    case DICTIONARY_ITEM_TYPE.OILFIELD_COEFFICIENT:
                        return "OilfieldCoefficient.csv";
                    case DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS:
                        return "OilfieldParams.csv";
                    case DICTIONARY_ITEM_TYPE.STRATUM:
                        return "Stratum.csv";
                    case DICTIONARY_ITEM_TYPE.GEOPLASTZONE:
                        return "StratumGeo.csv";
                    case DICTIONARY_ITEM_TYPE.PERF_TYPE:
                        return "PerfType.csv";
                    case DICTIONARY_ITEM_TYPE.CHARWORK:
                        return "CharWork.csv";
                    case DICTIONARY_ITEM_TYPE.STATE:
                        return "WellState.csv";
                    case DICTIONARY_ITEM_TYPE.METHOD:
                        return "WellMethod.csv";
                    case DICTIONARY_ITEM_TYPE.SATURATION:
                        return "Saturation.csv";
                    case DICTIONARY_ITEM_TYPE.CONSTRUCTION:
                        return "Construction.csv";
                    case DICTIONARY_ITEM_TYPE.OILFIELD_AREA:
                        return "OilFieldArea.csv";
                    case DICTIONARY_ITEM_TYPE.GRID_TYPE:
                        return "GridType.csv";
                    case DICTIONARY_ITEM_TYPE.LITOLOGY:
                        return "Lithology.csv";
                    case DICTIONARY_ITEM_TYPE.LOG_UNIT:
                        return "LogUnit.csv";
                    case DICTIONARY_ITEM_TYPE.LOG_MNEMONIC:
                        return "LogMnemonic.csv";
                    case DICTIONARY_ITEM_TYPE.GTM_TYPE:
                        return "GtmType.csv";
                    case DICTIONARY_ITEM_TYPE.GTA_TYPE:
                        return "gta_type.csv";
                    case DICTIONARY_ITEM_TYPE.WELL_ACTION_TYPE:
                        return "WellActionType.csv";
                    case DICTIONARY_ITEM_TYPE.WELL_RESEARCH_TYPE:
                        return "WellResearchType.csv";
                    default:
                        throw new ArgumentException(string.Format("Не поддерживаемый тип справочника [{0}]", Type));
                }
            }
        }

        public abstract bool ReadFromFile(string Path);
        public abstract bool WriteToFile(string Path);
    }

    public class Dictionary<T> : DictionaryBase where T : DictionaryBaseItem, new()
    {
        List<T> Items;

        public int Count { get { return Items.Count; } }
        public T this[int index] { get { return Items[index]; } }

        public Dictionary(DICTIONARY_ITEM_TYPE Type)
        {
            this.Type = Type;
            Items = new List<T>();
        }

        public void Add(T Item) 
        { 
            Items.Add(Item); 
        }

        public override bool ReadFromFile(string Path)
        {
            bool result = false;
            string fileName = Path + "\\" + FileName;
            if (File.Exists(fileName))
            {
                try
                {
                    T item;
                    string[] lines = File.ReadAllLines(fileName, Encoding.UTF8);
                    for(int i = 1; i < lines.Length;i++)
                    {
                        item = new T();
                        item.Index = i - 1;
                        item.Parse(lines[i]);
                        Items.Add(item);
                    }
                    result = true;
                }
                catch
                {
                    Items.Clear();
                    result = false;
                }
            }
            return result;
        }
        public override bool WriteToFile(string Path)
        {
            bool result = false;
            string fileName = Path + "\\" + FileName;
            if(Count > 0)
            {
                try
                {
                    string[] lines = new string[Items.Count + 1];
                    lines[0] = Items[0].GetFileHead();
                    for (int i = 0; i < Items.Count; i++)
                    {
                        lines[i + 1] = Items[i].ToString();
                    }
                    File.WriteAllLines(fileName, lines, Encoding.UTF8);
                    result = true;
                }
                catch
                {
                    Items.Clear();
                    result = false;
                }
            }
            return result;
        }
    }
}
