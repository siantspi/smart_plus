﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DataLoader.Objects.Project;
using DataLoader.Objects.Base;

namespace DataLoader.UIComponents
{
    class Inspector : TreeView
    {
        Project srcProject;

        public Inspector()
        {
            this.CheckBoxes = true;
            this.HideSelection = false;
            this.MouseDown += new MouseEventHandler(Inspector_MouseDown);
            this.AfterCheck += new TreeViewEventHandler(Inspector_AfterCheck);
            this.BeforeExpand += new TreeViewCancelEventHandler(Inspector_BeforeExpand);
        }

        void Inspector_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if ((e.Node.Parent != null) && (e.Node.Parent.Tag != null) && (e.Node.Nodes.Count == 1) && (e.Node.Nodes[0].Text == "Empty"))
            {
                TreeNode[] NodeList = ((Oilfield)e.Node.Parent.Tag).GetObjectList(e.Node.Text);
                if (NodeList != null)
                {
                    e.Node.Nodes.Clear();
                    e.Node.Nodes.AddRange(NodeList);
                }
            }
        }

        void Inspector_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level < 2)
            {
                for (int i = 0; i < e.Node.Nodes.Count; i++)
                {
                    e.Node.Nodes[i].Checked = e.Node.Checked;
                }
            }
        }

        #region EVENTS
        void Inspector_MouseDown(object sender, MouseEventArgs e)
        {
            TreeNode clickedNode = GetNodeAt(e.Location);
            if ((clickedNode == null) || (e.Location.X < clickedNode.Bounds.X - 16) || (e.Location.X > clickedNode.Bounds.Right))
            {
                this.SelectedNode = null;
            }
            else
            {
                this.SelectedNode = clickedNode;
            }
        }
        #endregion

        private TreeNode GetNGDUNode(NGDU NGDU)
        {
            TreeNode ngduNode = new TreeNode(NGDU.Name);
            return ngduNode;
        }

        public void FillOilFieldList(TreeNode RootNode, List<Oilfield> List)
        {
            if ((RootNode.Checked) && (RootNode.Tag != null) && (((LoggingObject)RootNode.Tag).LogType == LOGGING_OBJECT_TYPE.OILFIELD))
            {
                List.Add((Oilfield)RootNode.Tag);
            }
            if (RootNode.Nodes.Count > 0)
            {
                for (int i = 0; i < RootNode.Nodes.Count; i++)
                {
                    FillOilFieldList(RootNode.Nodes[i], List);
                }
            }
        }

        public List<Oilfield> GetSelectedOilfields()
        {
            List<Oilfield> list = new List<Oilfield>();
            if(Nodes.Count == 1) FillOilFieldList(Nodes[0], list);
            return list;
        }

        public void LoadProjectByNGDU(Project Proj)
        {
            srcProject = Proj;
            TreeNode projNode = Nodes.Add(Proj.Name);
            for (int i = 0; i < srcProject.NGDUList.Count; i++)
            {
                projNode.Nodes.Add(srcProject.NGDUList[i].GetNGDUNode());
            }
            projNode.Expand();
        }

        public void LoadProjectByOilFields(Project Proj)
        {
            srcProject = Proj;

        }
    }
}
