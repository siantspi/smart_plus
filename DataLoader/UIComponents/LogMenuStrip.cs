﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DataLoader.Objects;
using DataLoader.Objects.Base;

namespace DataLoader.UIComponents
{
    public delegate void OnChangeLoggingObject(LoggingObject NewLoggingObject);
    class LogMenuStrip : ContextMenuStrip
    {
        OilfieldWorkerDispatcher Dispatcher;
        public LoggingObject CurrentLogObject;
        public event OnChangeLoggingObject OnChangeLoggingObject;

        public LogMenuStrip(OilfieldWorkerDispatcher Dispatcher)
        {
            this.Dispatcher = Dispatcher;
            CurrentLogObject = null;
            ToolStripMenuItem item;

            item = (ToolStripMenuItem)Items.Add("");
            item.Text = "Отобразить общий лог";
            item.Checked = true;
            item.Enabled = true;
            item.Click += new EventHandler(item_Click);
            item.Tag = Dispatcher;
            CurrentLogObject = new LoggingObject(LOGGING_OBJECT_TYPE.OILFIELD_DISPATCHER);
            item = (ToolStripMenuItem)Items.Add("");
            item.Text = "Отобразить лог месторождения";
            item.Checked = false;
            item.Enabled = false;
        }

        ToolStripMenuItem GetNGDUitem(string NGDUName)
        {
            ToolStripMenuItem mainItem, ngduItem;
            mainItem = (ToolStripMenuItem)Items[1];
            for (int i = 0; i < mainItem.DropDownItems.Count; i++)
            {
                ngduItem = (ToolStripMenuItem)mainItem.DropDownItems[i];
                if (ngduItem.Text == NGDUName) return ngduItem;
            }
            ngduItem = (ToolStripMenuItem)mainItem.DropDownItems.Add(NGDUName);
            return ngduItem;
        }
        public void ResetMenu()
        {
            if(Dispatcher != null)
            {
                ToolStripMenuItem item, mainItem, ngduItem;
                item = (ToolStripMenuItem)Items[0];
                item.Enabled = false;
                item.Checked = true;
                mainItem = (ToolStripMenuItem)Items[1];
                mainItem.Enabled = true;
                mainItem.Checked = false;
                mainItem.DropDownItems.Clear();

                if ((Dispatcher.WorkOilFields != null) && (Dispatcher.WorkOilFields.Count != 0))
                {
                    mainItem = (ToolStripMenuItem)Items[1];
                    mainItem.Enabled = true;
                    mainItem.Checked = false;
                    for(int i = 0; i < Dispatcher.WorkOilFields.Count;i++)
                    {
                        ngduItem = GetNGDUitem(Dispatcher.WorkOilFields[i].ParamsDict.NGDUName);
                        ngduItem.Checked = false;

                        item = (ToolStripMenuItem)ngduItem.DropDownItems.Add(Dispatcher.WorkOilFields[i].Name);
                        item.Checked = false;
                        item.Tag = Dispatcher.WorkOilFields[i];
                        item.Click += new EventHandler(item_Click);
                    }
                }
            }
        }

        void item_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            ((ToolStripMenuItem)Items[0]).Checked = false;
            ((ToolStripMenuItem)Items[0]).Enabled = true;
            ToolStripMenuItem mainItem = (ToolStripMenuItem)Items[1];
            ToolStripMenuItem ngduItem;
            for (int i = 0; i < mainItem.DropDownItems.Count; i++)
            {
                ngduItem = (ToolStripMenuItem)mainItem.DropDownItems[i];
                ngduItem.Checked = false;
                for (int j = 0; j < ngduItem.DropDownItems.Count; j++)
                {
                    ((ToolStripMenuItem)ngduItem.DropDownItems[j]).Checked = false;
                    ((ToolStripMenuItem)ngduItem.DropDownItems[j]).Enabled = true;
                }
            }
            if (item.Tag != null)
            {
                LoggingObject obj = (LoggingObject)item.Tag;
                if ((CurrentLogObject.LogType != obj.LogType) || (CurrentLogObject.Index != obj.Index))
                {
                    CurrentLogObject = obj;
                    item.Checked = true;
                    if (item.OwnerItem != null) ((ToolStripMenuItem)item.OwnerItem).Checked = true;
                    item.Enabled = false;
                    OnChangeLoggingObject(obj);
                }
            }
        }
    }
}
