﻿namespace DataLoader
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.SplitPanel = new System.Windows.Forms.SplitContainer();
            this.Inspector = new DataLoader.UIComponents.Inspector();
            this.TestServersButton = new System.Windows.Forms.Button();
            this.SaveSettingsButton = new System.Windows.Forms.Button();
            this.OISGroup = new System.Windows.Forms.GroupBox();
            this.OracleSid = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.OraclePass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.OracleUser = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.OracleServerPort = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.OracleServerName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Log = new System.Windows.Forms.RichTextBox();
            this.StartLoad = new System.Windows.Forms.Button();
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.MMProject = new System.Windows.Forms.ToolStripMenuItem();
            this.MMCreateProject = new System.Windows.Forms.ToolStripMenuItem();
            this.MMOpenProject = new System.Windows.Forms.ToolStripMenuItem();
            this.MMCloseProject = new System.Windows.Forms.ToolStripMenuItem();
            this.MMCreateUpdateFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MMExit = new System.Windows.Forms.ToolStripMenuItem();
            this.MMLoadData = new System.Windows.Forms.ToolStripMenuItem();
            this.MMLoadConditionContours = new System.Windows.Forms.ToolStripMenuItem();
            this.MMLoadTable81Params = new System.Windows.Forms.ToolStripMenuItem();
            this.MMDictionary = new System.Windows.Forms.ToolStripMenuItem();
            this.MMDictPlast = new System.Windows.Forms.ToolStripMenuItem();
            this.MMDictPlastHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            this.MMDictPlastStratigraphy = new System.Windows.Forms.ToolStripMenuItem();
            this.MMHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MMAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._nfIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this._cTreyMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SplitPanel.Panel1.SuspendLayout();
            this.SplitPanel.Panel2.SuspendLayout();
            this.SplitPanel.SuspendLayout();
            this.OISGroup.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this._cTreyMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // SplitPanel
            // 
            this.SplitPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SplitPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitPanel.Location = new System.Drawing.Point(0, 24);
            this.SplitPanel.Name = "SplitPanel";
            // 
            // SplitPanel.Panel1
            // 
            this.SplitPanel.Panel1.Controls.Add(this.Inspector);
            this.SplitPanel.Panel1MinSize = 100;
            // 
            // SplitPanel.Panel2
            // 
            this.SplitPanel.Panel2.Controls.Add(this.TestServersButton);
            this.SplitPanel.Panel2.Controls.Add(this.SaveSettingsButton);
            this.SplitPanel.Panel2.Controls.Add(this.OISGroup);
            this.SplitPanel.Panel2.Controls.Add(this.Log);
            this.SplitPanel.Panel2.Controls.Add(this.StartLoad);
            this.SplitPanel.Panel2.Controls.Add(this.Progress);
            this.SplitPanel.Size = new System.Drawing.Size(904, 666);
            this.SplitPanel.SplitterDistance = 192;
            this.SplitPanel.SplitterWidth = 5;
            this.SplitPanel.TabIndex = 0;
            // 
            // Inspector
            // 
            this.Inspector.CheckBoxes = true;
            this.Inspector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Inspector.HideSelection = false;
            this.Inspector.Location = new System.Drawing.Point(0, 0);
            this.Inspector.Name = "Inspector";
            this.Inspector.Size = new System.Drawing.Size(190, 664);
            this.Inspector.TabIndex = 0;
            // 
            // TestServersButton
            // 
            this.TestServersButton.Location = new System.Drawing.Point(3, 157);
            this.TestServersButton.Name = "TestServersButton";
            this.TestServersButton.Size = new System.Drawing.Size(152, 30);
            this.TestServersButton.TabIndex = 21;
            this.TestServersButton.Text = "Проверить подключения";
            this.TestServersButton.UseVisualStyleBackColor = true;
            this.TestServersButton.Click += new System.EventHandler(this.TestServersButton_Click);
            // 
            // SaveSettingsButton
            // 
            this.SaveSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveSettingsButton.Location = new System.Drawing.Point(598, 157);
            this.SaveSettingsButton.Name = "SaveSettingsButton";
            this.SaveSettingsButton.Size = new System.Drawing.Size(97, 30);
            this.SaveSettingsButton.TabIndex = 10;
            this.SaveSettingsButton.Text = "Сохранить";
            this.SaveSettingsButton.UseVisualStyleBackColor = true;
            this.SaveSettingsButton.Click += new System.EventHandler(this.SaveSettings_Click);
            // 
            // OISGroup
            // 
            this.OISGroup.Controls.Add(this.OracleSid);
            this.OISGroup.Controls.Add(this.label9);
            this.OISGroup.Controls.Add(this.OraclePass);
            this.OISGroup.Controls.Add(this.label5);
            this.OISGroup.Controls.Add(this.OracleUser);
            this.OISGroup.Controls.Add(this.label6);
            this.OISGroup.Controls.Add(this.OracleServerPort);
            this.OISGroup.Controls.Add(this.label7);
            this.OISGroup.Controls.Add(this.OracleServerName);
            this.OISGroup.Controls.Add(this.label8);
            this.OISGroup.Location = new System.Drawing.Point(3, 3);
            this.OISGroup.Name = "OISGroup";
            this.OISGroup.Size = new System.Drawing.Size(387, 154);
            this.OISGroup.TabIndex = 20;
            this.OISGroup.TabStop = false;
            this.OISGroup.Text = "Oracle ";
            // 
            // OracleSid
            // 
            this.OracleSid.Location = new System.Drawing.Point(97, 69);
            this.OracleSid.Name = "OracleSid";
            this.OracleSid.Size = new System.Drawing.Size(284, 23);
            this.OracleSid.TabIndex = 19;
            this.OracleSid.TextChanged += new System.EventHandler(this.OilFieldDataBaseParams_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 15);
            this.label9.TabIndex = 18;
            this.label9.Text = "Sid:";
            // 
            // OraclePass
            // 
            this.OraclePass.Location = new System.Drawing.Point(97, 125);
            this.OraclePass.Name = "OraclePass";
            this.OraclePass.PasswordChar = '*';
            this.OraclePass.Size = new System.Drawing.Size(284, 23);
            this.OraclePass.TabIndex = 17;
            this.OraclePass.TextChanged += new System.EventHandler(this.OilFieldDataBaseParams_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 16;
            this.label5.Text = "Пароль:";
            // 
            // OracleUser
            // 
            this.OracleUser.Location = new System.Drawing.Point(97, 97);
            this.OracleUser.Name = "OracleUser";
            this.OracleUser.Size = new System.Drawing.Size(284, 23);
            this.OracleUser.TabIndex = 15;
            this.OracleUser.TextChanged += new System.EventHandler(this.OilFieldDataBaseParams_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 15);
            this.label6.TabIndex = 14;
            this.label6.Text = "Пользователь:";
            // 
            // OracleServerPort
            // 
            this.OracleServerPort.Location = new System.Drawing.Point(97, 41);
            this.OracleServerPort.Name = "OracleServerPort";
            this.OracleServerPort.Size = new System.Drawing.Size(284, 23);
            this.OracleServerPort.TabIndex = 13;
            this.OracleServerPort.TextChanged += new System.EventHandler(this.OilFieldDataBaseParams_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Порт:";
            // 
            // OracleServerName
            // 
            this.OracleServerName.Location = new System.Drawing.Point(97, 14);
            this.OracleServerName.Name = "OracleServerName";
            this.OracleServerName.Size = new System.Drawing.Size(284, 23);
            this.OracleServerName.TabIndex = 11;
            this.OracleServerName.TextChanged += new System.EventHandler(this.OilFieldDataBaseParams_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 15);
            this.label8.TabIndex = 10;
            this.label8.Text = "Сервер:";
            // 
            // Log
            // 
            this.Log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Log.Location = new System.Drawing.Point(3, 193);
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(692, 432);
            this.Log.TabIndex = 3;
            this.Log.Text = "Лог событий";
            // 
            // StartLoad
            // 
            this.StartLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.StartLoad.Enabled = false;
            this.StartLoad.Location = new System.Drawing.Point(600, 631);
            this.StartLoad.Name = "StartLoad";
            this.StartLoad.Size = new System.Drawing.Size(95, 30);
            this.StartLoad.TabIndex = 2;
            this.StartLoad.Text = "Загрузить";
            this.StartLoad.UseVisualStyleBackColor = true;
            this.StartLoad.Click += new System.EventHandler(this.Load_Click);
            // 
            // Progress
            // 
            this.Progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Progress.Enabled = false;
            this.Progress.Location = new System.Drawing.Point(3, 636);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(591, 21);
            this.Progress.TabIndex = 1;
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MMProject,
            this.MMLoadData,
            this.MMDictionary,
            this.MMHelp});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.MainMenu.Size = new System.Drawing.Size(904, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "menuStrip1";
            // 
            // MMProject
            // 
            this.MMProject.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MMCreateProject,
            this.MMOpenProject,
            this.MMCloseProject,
            this.MMCreateUpdateFile,
            this.toolStripSeparator1,
            this.MMExit});
            this.MMProject.Name = "MMProject";
            this.MMProject.Size = new System.Drawing.Size(59, 20);
            this.MMProject.Text = "Проект";
            // 
            // MMCreateProject
            // 
            this.MMCreateProject.Name = "MMCreateProject";
            this.MMCreateProject.Size = new System.Drawing.Size(219, 22);
            this.MMCreateProject.Text = "Создать...";
            this.MMCreateProject.Click += new System.EventHandler(this.MMCreateProject_Click);
            // 
            // MMOpenProject
            // 
            this.MMOpenProject.Name = "MMOpenProject";
            this.MMOpenProject.Size = new System.Drawing.Size(219, 22);
            this.MMOpenProject.Text = "Открыть...";
            // 
            // MMCloseProject
            // 
            this.MMCloseProject.Name = "MMCloseProject";
            this.MMCloseProject.Size = new System.Drawing.Size(219, 22);
            this.MMCloseProject.Text = "Закрыть";
            this.MMCloseProject.Click += new System.EventHandler(this.MMCloseProject_Click);
            // 
            // MMCreateUpdateFile
            // 
            this.MMCreateUpdateFile.Name = "MMCreateUpdateFile";
            this.MMCreateUpdateFile.Size = new System.Drawing.Size(219, 22);
            this.MMCreateUpdateFile.Text = "Создать файл обновления";
            this.MMCreateUpdateFile.Visible = false;
            this.MMCreateUpdateFile.Click += new System.EventHandler(this.MMCreateUpdateFile_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(216, 6);
            // 
            // MMExit
            // 
            this.MMExit.Name = "MMExit";
            this.MMExit.Size = new System.Drawing.Size(219, 22);
            this.MMExit.Text = "Выход";
            // 
            // MMLoadData
            // 
            this.MMLoadData.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MMLoadConditionContours,
            this.MMLoadTable81Params});
            this.MMLoadData.Name = "MMLoadData";
            this.MMLoadData.Size = new System.Drawing.Size(110, 20);
            this.MMLoadData.Text = "Загрузка данных";
            // 
            // MMLoadConditionContours
            // 
            this.MMLoadConditionContours.Name = "MMLoadConditionContours";
            this.MMLoadConditionContours.Size = new System.Drawing.Size(327, 22);
            this.MMLoadConditionContours.Text = "Загрузить условные контуры месторождений";
            this.MMLoadConditionContours.Click += new System.EventHandler(this.MMLoadConditionContours_Click);
            // 
            // MMLoadTable81Params
            // 
            this.MMLoadTable81Params.Name = "MMLoadTable81Params";
            this.MMLoadTable81Params.Size = new System.Drawing.Size(327, 22);
            this.MMLoadTable81Params.Text = "Загрузить проектные показатели(Табл 8.1)";
            this.MMLoadTable81Params.Click += new System.EventHandler(this.MMLoadTable81Params_Click);
            // 
            // MMDictionary
            // 
            this.MMDictionary.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MMDictPlast});
            this.MMDictionary.Name = "MMDictionary";
            this.MMDictionary.Size = new System.Drawing.Size(94, 20);
            this.MMDictionary.Text = "Справочники";
            // 
            // MMDictPlast
            // 
            this.MMDictPlast.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MMDictPlastHierarchy,
            this.MMDictPlastStratigraphy});
            this.MMDictPlast.Name = "MMDictPlast";
            this.MMDictPlast.Size = new System.Drawing.Size(107, 22);
            this.MMDictPlast.Text = "Пласт";
            // 
            // MMDictPlastHierarchy
            // 
            this.MMDictPlastHierarchy.Name = "MMDictPlastHierarchy";
            this.MMDictPlastHierarchy.Size = new System.Drawing.Size(174, 22);
            this.MMDictPlastHierarchy.Text = "Иерархия пластов";
            this.MMDictPlastHierarchy.Click += new System.EventHandler(this.MMDictPlastHierarchy_Click);
            // 
            // MMDictPlastStratigraphy
            // 
            this.MMDictPlastStratigraphy.Name = "MMDictPlastStratigraphy";
            this.MMDictPlastStratigraphy.Size = new System.Drawing.Size(174, 22);
            this.MMDictPlastStratigraphy.Text = "Стратиграфия";
            // 
            // MMHelp
            // 
            this.MMHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MMAbout});
            this.MMHelp.Name = "MMHelp";
            this.MMHelp.Size = new System.Drawing.Size(65, 20);
            this.MMHelp.Text = "Справка";
            // 
            // MMAbout
            // 
            this.MMAbout.Name = "MMAbout";
            this.MMAbout.Size = new System.Drawing.Size(149, 22);
            this.MMAbout.Text = "О программе";
            // 
            // _nfIcon
            // 
            this._nfIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("_nfIcon.Icon")));
            this._nfIcon.Text = "DataLoader";
            this._nfIcon.Visible = true;
            this._nfIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this._nfIcon_MouseClick);
            // 
            // _cTreyMenuStrip
            // 
            this._cTreyMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this._cTreyMenuStrip.Name = "contextMenuStrip1";
            this._cTreyMenuStrip.Size = new System.Drawing.Size(184, 26);
            this._cTreyMenuStrip.TabStop = true;
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.exitToolStripMenuItem.Text = "Закрыть DataLoader";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 690);
            this.Controls.Add(this.SplitPanel);
            this.Controls.Add(this.MainMenu);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(850, 250);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Загрузчик данных Smart Plus";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.SplitPanel.Panel1.ResumeLayout(false);
            this.SplitPanel.Panel2.ResumeLayout(false);
            this.SplitPanel.ResumeLayout(false);
            this.OISGroup.ResumeLayout(false);
            this.OISGroup.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this._cTreyMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer SplitPanel;
        private DataLoader.UIComponents.Inspector Inspector;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem MMProject;
        private System.Windows.Forms.ToolStripMenuItem MMOpenProject;
        private System.Windows.Forms.ProgressBar Progress;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MMExit;
        private System.Windows.Forms.ToolStripMenuItem MMHelp;
        private System.Windows.Forms.ToolStripMenuItem MMAbout;
        private System.Windows.Forms.Button StartLoad;
        private System.Windows.Forms.RichTextBox Log;
        private System.Windows.Forms.ToolStripMenuItem MMDictionary;
        private System.Windows.Forms.ToolStripMenuItem MMDictPlast;
        private System.Windows.Forms.ToolStripMenuItem MMDictPlastHierarchy;
        private System.Windows.Forms.ToolStripMenuItem MMDictPlastStratigraphy;
        private System.Windows.Forms.Button SaveSettingsButton;
        private System.Windows.Forms.GroupBox OISGroup;
        private System.Windows.Forms.TextBox OracleSid;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox OraclePass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox OracleUser;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox OracleServerPort;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox OracleServerName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripMenuItem MMCreateProject;
        private System.Windows.Forms.ToolStripMenuItem MMCloseProject;
        private System.Windows.Forms.ToolStripMenuItem MMLoadData;
        private System.Windows.Forms.ToolStripMenuItem MMLoadConditionContours;
        private System.Windows.Forms.ToolStripMenuItem MMLoadTable81Params;
        private System.Windows.Forms.Button TestServersButton;
        private System.Windows.Forms.ToolStripMenuItem MMCreateUpdateFile;
        private System.Windows.Forms.NotifyIcon _nfIcon;
        private System.Windows.Forms.ContextMenuStrip _cTreyMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;

    }
}

