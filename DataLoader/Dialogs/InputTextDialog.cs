﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataLoader
{
    public partial class InputTextDialog : Form
    {
        public string InputText
        {
            get
            {
                return InputTextBox.Text;
            }
        }

        public InputTextDialog(Form MainForm)
        {
            this.Owner = MainForm;
            this.StartPosition = FormStartPosition.CenterParent;
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(InputTextDialog_FormClosing);
        }

        void InputTextDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((this.DialogResult == DialogResult.OK) && (InputTextBox.Text.Length == 0))
            {
                MessageBox.Show("Название проекта не может быть пустым.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }
        
        public DialogResult ShowDialog(string Title)
        {
            this.Text = Title;
            this.LabelText.Text = Title;
            return this.ShowDialog();
        }
    }
}
