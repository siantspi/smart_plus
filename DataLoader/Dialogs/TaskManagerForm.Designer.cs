﻿namespace DataLoader.UIComponents
{
    partial class TaskManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TaskTree = new System.Windows.Forms.TreeView();
            this.OK = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.QueryGroup = new System.Windows.Forms.GroupBox();
            this.QueryTabs = new System.Windows.Forms.TabControl();
            this.Query1 = new System.Windows.Forms.TabPage();
            this.QueryText1 = new System.Windows.Forms.RichTextBox();
            this.Query2 = new System.Windows.Forms.TabPage();
            this.QueryText2 = new System.Windows.Forms.RichTextBox();
            this.SaveQuery = new System.Windows.Forms.Button();
            this.QueryGroup.SuspendLayout();
            this.QueryTabs.SuspendLayout();
            this.Query1.SuspendLayout();
            this.Query2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TaskTree
            // 
            this.TaskTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.TaskTree.HideSelection = false;
            this.TaskTree.Location = new System.Drawing.Point(3, 12);
            this.TaskTree.Name = "TaskTree";
            this.TaskTree.Size = new System.Drawing.Size(212, 550);
            this.TaskTree.TabIndex = 0;
            // 
            // OK
            // 
            this.OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OK.Location = new System.Drawing.Point(500, 532);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(95, 30);
            this.OK.TabIndex = 1;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            // 
            // Cancel
            // 
            this.Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(601, 532);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(95, 30);
            this.Cancel.TabIndex = 2;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            // 
            // QueryGroup
            // 
            this.QueryGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.QueryGroup.Controls.Add(this.QueryTabs);
            this.QueryGroup.Controls.Add(this.SaveQuery);
            this.QueryGroup.Location = new System.Drawing.Point(221, 14);
            this.QueryGroup.Name = "QueryGroup";
            this.QueryGroup.Size = new System.Drawing.Size(475, 512);
            this.QueryGroup.TabIndex = 3;
            this.QueryGroup.TabStop = false;
            this.QueryGroup.Text = "Настройки";
            // 
            // QueryTabs
            // 
            this.QueryTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.QueryTabs.Controls.Add(this.Query1);
            this.QueryTabs.Controls.Add(this.Query2);
            this.QueryTabs.Location = new System.Drawing.Point(6, 22);
            this.QueryTabs.Name = "QueryTabs";
            this.QueryTabs.SelectedIndex = 0;
            this.QueryTabs.Size = new System.Drawing.Size(463, 448);
            this.QueryTabs.TabIndex = 3;
            // 
            // Query1
            // 
            this.Query1.Controls.Add(this.QueryText1);
            this.Query1.Location = new System.Drawing.Point(4, 24);
            this.Query1.Name = "Query1";
            this.Query1.Padding = new System.Windows.Forms.Padding(3);
            this.Query1.Size = new System.Drawing.Size(455, 420);
            this.Query1.TabIndex = 0;
            this.Query1.Text = "Запрос1";
            this.Query1.UseVisualStyleBackColor = true;
            // 
            // QueryText1
            // 
            this.QueryText1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.QueryText1.Location = new System.Drawing.Point(3, 3);
            this.QueryText1.Name = "QueryText1";
            this.QueryText1.Size = new System.Drawing.Size(449, 414);
            this.QueryText1.TabIndex = 0;
            this.QueryText1.Text = "";
            // 
            // Query2
            // 
            this.Query2.Controls.Add(this.QueryText2);
            this.Query2.Location = new System.Drawing.Point(4, 24);
            this.Query2.Name = "Query2";
            this.Query2.Padding = new System.Windows.Forms.Padding(3);
            this.Query2.Size = new System.Drawing.Size(455, 420);
            this.Query2.TabIndex = 1;
            this.Query2.Text = "Запрос2";
            this.Query2.UseVisualStyleBackColor = true;
            // 
            // QueryText2
            // 
            this.QueryText2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.QueryText2.Location = new System.Drawing.Point(3, 3);
            this.QueryText2.Name = "QueryText2";
            this.QueryText2.Size = new System.Drawing.Size(449, 414);
            this.QueryText2.TabIndex = 0;
            this.QueryText2.Text = "";
            // 
            // SaveQuery
            // 
            this.SaveQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveQuery.Enabled = false;
            this.SaveQuery.Location = new System.Drawing.Point(374, 476);
            this.SaveQuery.Name = "SaveQuery";
            this.SaveQuery.Size = new System.Drawing.Size(95, 30);
            this.SaveQuery.TabIndex = 2;
            this.SaveQuery.Text = "Сохранить...";
            this.SaveQuery.UseVisualStyleBackColor = true;
            this.SaveQuery.Click += new System.EventHandler(this.SaveQuery_Click);
            // 
            // TaskManagerForm
            // 
            this.AcceptButton = this.OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(708, 574);
            this.Controls.Add(this.QueryGroup);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.TaskTree);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(450, 250);
            this.Name = "TaskManagerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выберите данные для загрузки";
            this.QueryGroup.ResumeLayout(false);
            this.QueryTabs.ResumeLayout(false);
            this.Query1.ResumeLayout(false);
            this.Query2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView TaskTree;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.GroupBox QueryGroup;
        private System.Windows.Forms.Button SaveQuery;
        private System.Windows.Forms.TabControl QueryTabs;
        private System.Windows.Forms.TabPage Query1;
        private System.Windows.Forms.TabPage Query2;
        private System.Windows.Forms.RichTextBox QueryText1;
        private System.Windows.Forms.RichTextBox QueryText2;
    }
}