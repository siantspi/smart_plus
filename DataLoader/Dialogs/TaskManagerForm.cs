﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataLoader.Objects.Project;
using DataLoader.Objects;


namespace DataLoader.UIComponents
{
    public partial class TaskManagerForm : Form
    {
        DataQuery Query;

        public TaskManagerForm(Form ParentForm)
        {
            InitializeComponent();
            this.Owner = ParentForm;
            this.StartPosition = FormStartPosition.CenterParent;
            TaskTree.CheckBoxes = true;
            Query = new DataQuery();

            Query.ClearQueryList();
            TreeNode tn;

            TreeNode Oraclenode = TaskTree.Nodes.Add("База Oracle");
            Oraclenode.Checked = true;
            tn = Oraclenode.Nodes.Add("Cписок скважин"); tn.Tag = SERVER_DATA_TYPE.WELLBORE_BY_ORACLE;
            tn = Oraclenode.Nodes.Add("Шахматка"); tn.Tag = SERVER_DATA_TYPE.CHESS_BY_ORACLE;
            tn = Oraclenode.Nodes.Add("ГТМ"); tn.Tag = SERVER_DATA_TYPE.GTM;
            tn = Oraclenode.Nodes.Add("Мероприятия на скважине"); tn.Tag = SERVER_DATA_TYPE.WELL_REPAIR_ACTION;
            tn = Oraclenode.Nodes.Add("Исследования скважин"); tn.Tag = SERVER_DATA_TYPE.WELL_RESEARCH_BY_ORACLE;
            tn = Oraclenode.Nodes.Add("Перфорация(OIS)"); tn.Tag = SERVER_DATA_TYPE.PERFORATION_BY_ORACLE;
            tn = Oraclenode.Nodes.Add("КВЧ"); tn.Tag = SERVER_DATA_TYPE.SUSPEND;

            
            TaskTree.ExpandAll();

            TaskTree.NodeMouseClick += new TreeNodeMouseClickEventHandler(TaskTree_NodeMouseClick);
            TaskTree.AfterCheck += new TreeViewEventHandler(TaskTree_AfterCheck);
            this.Shown += new EventHandler(TaskManagerForm_Shown);
            QueryText1.TextChanged += new EventHandler(QueryText1_TextChanged);
            QueryText2.TextChanged += new EventHandler(QueryText2_TextChanged);
            QueryTabs.EnabledChanged += new EventHandler(QueryTabs_EnabledChanged);
            QueryTabs.Enabled = false;
        }

        public List<WorkerTask> GetTaskList()
        {
            TreeNode tn;
            List<WorkerTask> taskList = new List<WorkerTask>();
            WorkerTask task;
            for (int i = 0; i < TaskTree.Nodes.Count; i++)
            {
                for (int j = 0; j < TaskTree.Nodes[i].Nodes.Count; j++)
                {
                    tn = TaskTree.Nodes[i].Nodes[j];
                    if ((tn.Tag != null) && (tn.Checked))
                    {
                        task.Task =(int)tn.Tag;
                        task.Query = Query.QueryList[task.Task];
                        taskList.Add(task);
                    }
                }
            }
            return taskList;
        }



        #region EVENTS
        void TaskManagerForm_Shown(object sender, EventArgs e)
        {
            Query.InitQueryList();
        }
        void QueryTabs_EnabledChanged(object sender, EventArgs e)
        {
            if (QueryTabs.Enabled)
            {
                for (int i = 0; i < QueryTabs.TabPages.Count; i++)
                {
                    QueryTabs.TabPages[i].ForeColor = Color.Black;
                }
                QueryGroup.ForeColor = Color.Black;
            }
            else
            {
                for (int i = 0; i < QueryTabs.TabPages.Count; i++)
                {
                    QueryTabs.TabPages[i].ForeColor = Color.LightGray;
                }
                QueryGroup.ForeColor = Color.LightGray;
            }
        }
        private void SaveQuery_Click(object sender, EventArgs e)
        {
            Query.WriteQueryList();
            SaveQuery.Enabled = false;
        }
        void QueryText1_TextChanged(object sender, EventArgs e)
        {
            SaveQuery.Enabled = true;
        }
        void QueryText2_TextChanged(object sender, EventArgs e)
        {
            SaveQuery.Enabled = true;
        }

        void TaskTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            QueryText1.Text = string.Empty;
            QueryText2.Text = string.Empty;
            if (e.Node.Tag != null)
            {
                if (Query.QueryList[(int)e.Node.Tag].Length > 0)
                {
                    QueryTabs.Enabled = true;
                    QueryTabs.SelectedIndex = 0;
                    string[] parseQuery = Query.QueryList[(int)e.Node.Tag].Split(new char[] { ';' }, 2);
                    if (parseQuery.Length > 0) QueryText1.Text = parseQuery[0];
                    if (parseQuery.Length > 1) QueryText2.Text = parseQuery[1];
                    SaveQuery.Enabled = false;
                }
                else
                {
                    QueryTabs.Enabled = false;
                    QueryTabs.SelectedIndex = 0;
                    SaveQuery.Enabled = false;
                }
            }
        }
        TreeNode GetTaskNode(SERVER_DATA_TYPE Type)
        {
            TreeNode node;
            if(TaskTree.Nodes.Count > 0)
            {
                for (int i = 0; i < TaskTree.Nodes.Count; i++)
                {
                    node = TaskTree.Nodes[i];
                    for (int j = 0; j < node.Nodes.Count; j++)
                    {
                        if (node.Nodes[j].Tag != null)
                        {
                            if ((SERVER_DATA_TYPE)node.Nodes[j].Tag == Type)
                            {
                                return node.Nodes[j];
                            }
                        }
                    }
                }
            }
            return null;
        }
        void TaskTree_AfterCheck(object sender, TreeViewEventArgs e)
        {
        }
        #endregion
    }
}