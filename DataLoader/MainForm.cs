﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataLoader.UIComponents;
using DataLoader.Objects;
using DataLoader.Objects.Project;
using DataLoader.Objects.Base;
using DataLoader.DictionaryObjects;
using System.Data.OracleClient;
using System.IO;


namespace DataLoader
{
    public partial class MainForm : Form
    {
        Project CurrentProject;
        List<Oilfield> LoadingOilFields;
        OilfieldWorkerDispatcher WorkDispatcher;
        MainSettings Settings;
        ParamSettings ParamSett;

        PlastTreeCreatorForm plastTreeCreator;
        LogMenuStrip LogContextMenu;
        LogsCollector LogCollector;
        Timer TimerLogUpdater;
        bool RunParam;
        
        public bool UserDialogShowed
        {
            get
            {
                if(CurrentProject != null)
                {
                    StratumDictionary dict = (StratumDictionary)CurrentProject.DictionaryList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                    return dict.ConversionFormShowed;
                }
                return false;
            }
        }

        public MainForm()
        {
            InitializeComponent();

            //привязываем контекстное меню к иконке в трее
            _nfIcon.ContextMenuStrip = _cTreyMenuStrip;
            _nfIcon.Icon = DataLoader.Properties.Resources.stock_icon_smart;

            CurrentProject = null;
            LoadingOilFields = new List<Oilfield>();
            LogCollector = new LogsCollector();
            WorkDispatcher = new OilfieldWorkerDispatcher(this);
            plastTreeCreator = null;

            MMDictPlastStratigraphy.Visible = false;


            OISGroup.Visible = true;

            

#if DEBUG
            MMCreateUpdateFile.Visible = true;
#endif
            #region Settings
            Settings = new MainSettings();
            Settings.ReadFromFile();
            UpdateMainMenuBySettings();
            #endregion

            #region Init SplitPanel
            SplitPanel.Panel2MinSize = 500;
            #endregion

            #region INIT CONTEXT MENU
            LogContextMenu = new LogMenuStrip(WorkDispatcher);
            LogContextMenu.OnChangeLoggingObject += new OnChangeLoggingObject(LogContextMenu_OnChangeLoggingObject);
            #endregion

            #region MAIN MENU EVENTS
            MMOpenProject.Click += new EventHandler(MMOpenProject_Click);
            MMExit.Click += new EventHandler(MMExit_Click);
            #endregion

            #region Timer
            TimerLogUpdater = new Timer();
            TimerLogUpdater.Enabled = false;
            TimerLogUpdater.Interval = 1000;
            TimerLogUpdater.Tick +=new EventHandler(TimerLogUpdater_Tick);
            #endregion

            #region LOG COLLECTOR
            LogCollector.ListeningObject(WorkDispatcher);
            LogCollector.OnNewMessage += new OnNewMessage(LogCollector_OnNewMessage);
            LogCollector.OnNewProgress += new OnNewProgress(LogCollector_OnNewProgress);
            #endregion

            #region SETTINGS
            SaveSettingsButton.Enabled = false;
            TestServersButton.Enabled = false;
            OracleServerPort.Text = "0";
            #endregion

            #region EVENTS
            Inspector.NodeMouseClick += new TreeNodeMouseClickEventHandler(Inspector_NodeMouseClick);
            #endregion

            #region START PARAM LOAD
  
            string[] keys = Environment.GetCommandLineArgs();
//#if !DEBUG
            if (keys.Length > 1) 
            {
//#endif
                this.WindowState = FormWindowState.Minimized;
                RunParam = true;
                ParamSett = new ParamSettings(keys);
                ParamLoadProject(ParamSett.ProjPath);
                while (this.CurrentProject.IsBusyLoadProj)
                {
                    System.Threading.Thread.Sleep(10);
                    Application.DoEvents();
                }
#if DEBUG
                //ParamDataLoad();
                //ParamDataLoad(ParamSett.TaskList, ParamSett.GetOilFieldList(CurrentProject));
#endif
//#if !DEBUG
                if (keys[1].ToLower() == "/all")
                    ParamDataLoad();
                if (keys[1].ToLower() == "/param")
                    ParamDataLoad(ParamSett.TaskList, ParamSett.GetOilFieldList(CurrentProject));
 
            }
            else
            {
                RunParam = false;
            }
//#endif
            #endregion

            if (!RunParam)
            CheckAnnoyUpdate.CheckLoaderData();
          
        }

        #region TREY ICON

        void nfIconShowStartLoadMessage()
        {
            _nfIcon.ShowBalloonTip(500, "Загрузчик данных SmartPlus", "Началась загрузка данных", ToolTipIcon.None);
        }

        void nfIconShowEndOkLoadMessage()
        {
            _nfIcon.ShowBalloonTip(500, "Загрузчик данных SmartPlus", "Загрузка данных успешно завершена", ToolTipIcon.None);
        }

        void nfIconShowEndWarningLoadMessage()
        {
            _nfIcon.ShowBalloonTip(500, "Загрузчик данных SmartPlus", "Загрузка данных завершена с ошибками", ToolTipIcon.None);
        }
        
        void _nfIcon_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (WindowState == FormWindowState.Normal)
                {
                    WindowState = FormWindowState.Minimized;
                    Hide();
                }
                else
                {
                    Show();
                    WindowState = FormWindowState.Normal;
                }
            }
        }

        /// <summary>
        /// Обрабатываем событие изменения размера
        /// </summary>
        private void MainForm_Resize(object sender, EventArgs e)
        {
            //если окно "свернуто", то скрываем его
            
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
                //показ всплывающего окна с сообщением из трея
                _nfIcon.ShowBalloonTip(500, "Загрузчик данных SmartPlus", "Приложение свернуто", ToolTipIcon.None);
            }
        }

 
        /// <summary>
        /// обрабатываем клик на Exit в меню
        /// </summary>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Вы уверены что хотите закрыть DataLoader", "DataLoader", MessageBoxButtons.YesNo);
            if(dialogResult == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Реализация "мигания" значков
        /// </summary>
        private void TreyIconTickOn(object sender, EventArgs e)
        {
            if (_nfIcon.Icon == DataLoader.Properties.Resources.stock_icon_smart)
            {
                _nfIcon.Icon = SystemIcons.Warning;
            }
            else
            {
                _nfIcon.Icon = DataLoader.Properties.Resources.stock_icon_smart;
            }
        }

        /// <summary>
        /// Делаем так, чтобы чтобы форма призагрузке сразу пряталась в трей
        /// </summary>
        
        private void MainForm_Load(object sender, EventArgs e)
        {
            MainForm_Resize(sender, e);
        }
        #endregion

        #region MAIN MENU

        // PROJECT
        public void UpdateMainMenuBySettings()
        {
            if (MMProject.DropDownItems.Count > 6)
            {
                while (MMProject.DropDownItems.Count > 6)
                {
                    MMProject.DropDownItems.RemoveAt(4);
                }
            }
            if (Settings.LastOpenedFolders.Count > 0)
            {
                ToolStripMenuItem item;
                MMProject.DropDownItems.Insert(MMProject.DropDownItems.Count - 2, new ToolStripSeparator());
                for (int i = 0; i < Settings.LastOpenedFolders.Count; i++)
                {
                    item = new ToolStripMenuItem(Settings.GetShortFolderName(Settings.LastOpenedFolders[i]));
                    item.Tag = Settings.LastOpenedFolders[i];
                    item.Click += new EventHandler(LastFolderMenuItemClick);
                    MMProject.DropDownItems.Insert(MMProject.DropDownItems.Count - 2, item);
                }
            }
        }

        void LastFolderMenuItemClick(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            if (item.Tag != null)
            {
                OpenProject((string)item.Tag);
            }
        }
        void OpenProject(string ProjectPath)
        {
            if (!Directory.Exists(ProjectPath))
            {
                if (MessageBox.Show(this, "Проект не найден! Удалить его из списка последних проектов?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    for (int i = 0; i < MMProject.DropDownItems.Count; i++)
                    {
                        if (MMProject.DropDownItems[i].Tag != null && ((string)MMProject.DropDownItems[i].Tag) == ProjectPath)
                        {
                            MMProject.DropDownItems.RemoveAt(i);
                            break;
                        }
                    }
                    Settings.RemoveLastOpenFolder(ProjectPath);
                }
            }
            else
            {
                Project project = new Project(ProjectPath);
                project.OnOilFieldDataLoaded += new OnAsyncWorkCompleted(project_OnOilFieldDataLoaded);
                project.OnProgress += new OnProgressDelegate(project_OnProgress);
                CurrentProject = project;
                project.LoadProject();
                SetListenLogCollector(project);
                project.LoadOilFieldDataAsync();
            }
        }
        private void MMCreateProject_Click(object sender, EventArgs e)
        {
            InputTextDialog inText = new InputTextDialog(this);
            if (inText.ShowDialog("Введите название для нового проекта") == DialogResult.OK)
            {
                string projName = inText.InputText;
                FolderBrowserDialog dlg = new FolderBrowserDialog();
                dlg.Description = "Выберите папку для создания проекта";
                dlg.ShowNewFolderButton = true;
                dlg.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    Project project = new Project(dlg.SelectedPath + "\\" + projName);
                    CurrentProject = project;
                    project.CreateEmptyOilFields();
                    SetListenLogCollector(project);
                    Inspector.LoadProjectByNGDU(project);
                    StartLoad.Enabled = true;
                    Settings.AddLastOpenFolder(dlg.SelectedPath + "\\" + projName);
                    UpdateMainMenuBySettings();
                }
            }
        }
        void MMOpenProject_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.Description = "Выберите папку проекта";
            dlg.ShowNewFolderButton = false;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                OpenProject(dlg.SelectedPath);
                Settings.AddLastOpenFolder(dlg.SelectedPath);
                UpdateMainMenuBySettings();
            }
        }
        private void MMCloseProject_Click(object sender, EventArgs e)
        {
            if (CurrentProject != null)
            {
                this.Progress.Value = 0;
                this.Progress.Maximum = 100;
                CurrentProject.FreeDataMemory();
                CurrentProject = null;
                this.Log.Clear();
                this.Inspector.Nodes.Clear();
                LoadingOilFields.Clear();
                LogContextMenu.ResetMenu();
                TimerLogUpdater.Stop();
            }
        }
        void MMExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        // LOAD DATA
        private void MMLoadConditionContours_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.Description = "Выберите папку с условными контурами";
            dlg.ShowNewFolderButton = false;
            dlg.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\SmartPlus\Рабочая папка";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Log.Clear();
                CurrentProject.OnContoursDataLoaded += new OnAsyncWorkCompleted(CurrentProject_OnContoursDataLoaded);
                CurrentProject.LoadContoursFromDirectoryAsync(dlg.SelectedPath);
            }
            dlg.Dispose();
        }
        void CurrentProject_OnContoursDataLoaded()
        {
            Log.AppendText("Условные контуры загружены");
        }
        private void MMLoadTable81Params_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Выберите файл с проектными показателями 8.1";
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dlg.Filter = "CSV файлы (*.CSV)|*.csv";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Log.Clear();
                CurrentProject.OnTable81DataLoaded += new OnAsyncWorkCompleted(CurrentProject_OnTable81DataLoaded);
                CurrentProject.LoadTable81FromFileAsync(dlg.FileName);
            }
            dlg.Dispose();
        }

        void CurrentProject_OnTable81DataLoaded()
        {
            Log.AppendText("Проектные показатели загружены");
        }

        // DICTIONARY
        private void MMDictPlastHierarchy_Click(object sender, EventArgs e)
        {
            if (CurrentProject == null)
            {
                MessageBox.Show("Для просмотра иерархии пластов загрузите проект!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (plastTreeCreator == null)
            {
                StratumDictionary dict = (StratumDictionary)CurrentProject.DictionaryList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                string path = CurrentProject.ProjectPath + @"\Dictionaries\Stratum\StratumTree.csv";
                plastTreeCreator = new PlastTreeCreatorForm(this, dict, path);
                plastTreeCreator.ShowDialog();
            }
            else
            {
                plastTreeCreator.ShowDialog();
            }
        }
        #endregion

        #region PROJECT DATA LOAD
        void project_OnProgress(LoggingObject sender, int ProgressValue)
        {
            Progress.Value = ProgressValue;
        }
        void project_OnOilFieldDataLoaded()
        {
            Inspector.LoadProjectByNGDU(CurrentProject);
            StartLoad.Enabled = true;
        }
        #endregion

        private void ParamLoadProject(string path)
        {
            OpenProject(path);
            Settings.AddLastOpenFolder(path);
            UpdateMainMenuBySettings();
        }

        
        private void ParamDataLoad()
        {
            ParamTask paramtask = new ParamTask();
            ParamOilFieleld paramoilfieled = new ParamOilFieleld(CurrentProject);
            Oilfield commonOilField = CurrentProject.NGDUList[0].OilFields[0];
            if (commonOilField.ParamsDict.NGDUCode != -1)
            {
                MessageBox.Show("Не найдено месторождение 'Общие данные'");
                return;
            }
            Inspector.Enabled = false;
            Progress.Value = 0;
            Log.Enabled = true;
            Log.Clear();
            LoadingOilFields.Clear();
            List<WorkerTask> taskList = paramtask.GetAllTaskList();
            List<Oilfield> ofList = paramoilfieled.GetAllOilfields();
            for (int i = 0; i < ofList.Count; i++)
            {
                ofList[i].DataLoaded = false;
            }

            WorkDispatcher.RunTaskAsync(CurrentProject, ofList, taskList);
            LogContextMenu.ResetMenu();
            Log.ContextMenuStrip = LogContextMenu;
            if (LoadingOilFields.Count > 0)
            {
                Progress.Maximum = LoadingOilFields.Count;
            }
            TimerLogUpdater.Start();
            StartLoad.Enabled = false;
        }

        private void ParamDataLoad(List<WorkerTask> taskList, List<Oilfield> ofList)
        {
            ParamTask paramtask = new ParamTask();
            ParamOilFieleld paramoilfieled = new ParamOilFieleld(CurrentProject);
            Oilfield commonOilField = CurrentProject.NGDUList[0].OilFields[0];
            if (commonOilField.ParamsDict.NGDUCode != -1)
            {
                MessageBox.Show("Не найдено месторождение 'Общие данные'");
                return;
            }
            Inspector.Enabled = false;
            Progress.Value = 0;
            Log.Enabled = true;
            Log.Clear();
            LoadingOilFields.Clear();
            for (int i = 0; i < ofList.Count; i++)
            {
                ofList[i].DataLoaded = false;
            }

            WorkDispatcher.RunTaskAsync(CurrentProject, ofList, taskList);
            LogContextMenu.ResetMenu();
            Log.ContextMenuStrip = LogContextMenu;
            if (LoadingOilFields.Count > 0)
            {
                Progress.Maximum = LoadingOilFields.Count;
            }
            TimerLogUpdater.Start();
            StartLoad.Enabled = false;
        }

        private void Load_Click(object sender, EventArgs e)
        {
            TaskManagerForm taskForm = new TaskManagerForm(this);
            if(taskForm.ShowDialog() == DialogResult.OK && CurrentProject.NGDUList.Count > 0 )
            {
                Oilfield commonOilField = CurrentProject.NGDUList[0].OilFields[0];
                if (commonOilField.ParamsDict.NGDUCode != -1)
                {
                    MessageBox.Show("Не найдено месторождение 'Общие данные'");
                    return;
                }
                Inspector.Enabled = false;
                Progress.Value = 0;
                StartLoad.Enabled = false;
                Log.Enabled = true;
                Log.Clear();
                LoadingOilFields.Clear();
                List<WorkerTask> taskList = taskForm.GetTaskList();
                List<Oilfield> ofList = Inspector.GetSelectedOilfields();
                for (int i = 0; i < ofList.Count; i++)
                {
                    ofList[i].DataLoaded = false;
                }

                WorkDispatcher.RunTaskAsync(CurrentProject, ofList, taskList);
                LogContextMenu.ResetMenu();
                Log.ContextMenuStrip = LogContextMenu;
                if (LoadingOilFields.Count > 0)
                {
                    Progress.Maximum = LoadingOilFields.Count;
                }
                this.nfIconShowStartLoadMessage();
                TimerLogUpdater.Start();
            }
            taskForm.Dispose();
        }
        public void EndWork()
        {
            StartLoad.Enabled = true;
            Inspector.Enabled = true;

            if (WorkDispatcher.ErrorLoading)
            {
                this.nfIconShowEndWarningLoadMessage();
                if (MessageBox.Show("Во время загрузки данных произошли ошибки. Отправить сообщение разработчикам?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string[] allLogs = LogCollector.GetAllLogs();
                    string fileName = Application.StartupPath + "\\Log_" + DateTime.Now.Ticks.ToString() + ".csv";

                    File.WriteAllLines(fileName, allLogs, Encoding.GetEncoding(1251));
                    Mapi email = new Mapi();
                    email.AddRecipientTo("support@ufntc.ru");
                    email.AddAttachment(fileName);

                    string AssemblyVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

                    string Body = "Служба сопровождения SmartPlusDataLoader.\r\nОпишите Вашу проблему:\r\n\r\n\r\n\r\n\r\n______________________________________________________________\r\nСпасибо за помощь по улучшению программного комплекса SmartPlusDataLoader";
                    Body += "\r\nДанное письмо содержит только информацию, необходимую разработчикам для устранения Вашей проблемы\r\n";
                    Body += "SmartPlusDataLoader (version: " + AssemblyVersion + "), Windows version: " + Environment.OSVersion.ToString();
                    email.SendMailPopup("Сообщение об ошибке SmartPlusDataLoader", Body);
                    if (File.Exists(fileName)) File.Delete(fileName);
                }
            }
            else
            {
                this.nfIconShowEndOkLoadMessage();
                if (RunParam)
                {
                    this.Close();
                }
            }
        }

        #region DICTIONARY
        public void TestCurrentDictionaryChanged()
        {
            if (CurrentProject != null)
            {
                StratumDictionary dict = (StratumDictionary)CurrentProject.DictionaryList.GetDictionary(DICTIONARY_ITEM_TYPE.STRATUM);
                dict.TestDictionaryUpdated();
            }
        }
        #endregion

        #region RICH TEXT BOX
        void LogAddLine(Color TextColor, string TextLine)
        {
            Log.SelectionColor = TextColor;
            Log.AppendText(TextLine);
            Log.SelectionColor = Color.Black;
        }
        #endregion

        #region TIMER LOG UPDATER
        void TimerLogUpdater_Tick(object sender, EventArgs e)
        {
        }
        #endregion

        #region CONTEXT MENU
        void LogContextMenu_OnChangeLoggingObject(DataLoader.Objects.Base.LoggingObject NewLoggingObject)
        {
            LogMessageCollection Coll = LogCollector.GetCollection(NewLoggingObject.LogType, NewLoggingObject.Index);
            Log.Clear();
            if (Coll != null)
            {
                for (int i = 0; i < Coll.Messages.Count; i++)
                {
                    if (Coll.Messages[i].Type == LOGGING_MESSAGE_TYPE.ERROR)
                    {
                        LogAddLine(Color.DarkRed, string.Format("Ошибка: {0}\n", Coll.Messages[i].Message));
                    }
                    else
                    {
                        Log.AppendText(Coll.Messages[i].Message + "\n");
                    }
                }
            }
        }
        #endregion

        #region LOG COLLECTOR
        void SetListenLogCollector(Project Project)
        {
            if (Project != null)
            {
                NGDU ngdu;
                for (int i = 0; i < Project.NGDUList.Count; i++)
                {
                    ngdu = Project.NGDUList[i];
                    for (int j = 0; j < ngdu.OilFields.Count; j++)
                    {
                        LogCollector.ListeningObject(ngdu.OilFields[j]);
                    }
                }
            }
        }
        void LogCollector_OnNewMessage(DataLoader.Objects.Base.LoggingObject sender, LOGGING_MESSAGE_TYPE Type, string Message)
        {
            if (Type == LOGGING_MESSAGE_TYPE.ERROR)
            {
                if (sender.LogType == LOGGING_OBJECT_TYPE.OILFIELD)
                {
                    WorkDispatcher.NewMessage(LOGGING_MESSAGE_TYPE.ERROR, string.Format("{0} [{1}]", Message, sender.Name));
                }
                else
                {
                    LogAddLine(Color.DarkRed, string.Format("Ошибка: {0}\n", Message));
                }
            }
            else if ((LogContextMenu.CurrentLogObject.Index == sender.Index) && 
                     (LogContextMenu.CurrentLogObject.LogType == sender.LogType))
            {
                Log.AppendText(Message + "\n");
            }
        }
        void LogCollector_OnNewProgress(LoggingObject sender, int Progress)
        {
            if (LogContextMenu.CurrentLogObject.Index == sender.Index)
            {
                if (LogContextMenu.CurrentLogObject.LogType == sender.LogType)
                {
                    if(Progress < 0) Progress = 0;
                    else if (Progress > this.Progress.Maximum) Progress = this.Progress.Maximum;
                    this.Progress.Value = Progress;
                }
            }
        }
        #endregion

        #region INSPECTOR
        void Inspector_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            OracleServerName.Text = string.Empty;
            OracleServerPort.Text = string.Empty;
            OracleSid.Text = string.Empty;
            OracleUser.Text = string.Empty;
            OraclePass.Text = string.Empty;

            SaveSettingsButton.Enabled = false;
            TestServersButton.Enabled = false;

            if ((e.Node.Tag != null) && ((LoggingObject)e.Node.Tag).LogType == LOGGING_OBJECT_TYPE.OILFIELD)
            {
                Oilfield of = (Oilfield)e.Node.Tag;
                OracleServerName.Text = of.ParamsDict.OraServerName;
                OracleServerPort.Text = of.ParamsDict.OraPort.ToString();
                OracleSid.Text = of.ParamsDict.OraSid;
                OracleUser.Text = of.ParamsDict.OraLogin;
                OraclePass.Text = of.ParamsDict.OraPass;
                
                TestServersButton.Enabled = true;
            }
        }
        #endregion

        #region EVENTS
        private void TestServersButton_Click(object sender, EventArgs e)
        {
            try
            {
                int port = Convert.ToInt32(OracleServerPort.Text);
            }
            catch
            {
                MessageBox.Show("Проверьте правильность полей", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            bool testOracle = false;
            string text = string.Empty;
            // ORACLE
            //
            try
            {
                string connString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                OracleServerName.Text + ")(PORT=" + OracleServerPort.Text +
                ")))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" + OracleSid.Text + ")));" +
                "USER ID=" + OracleUser.Text + ";PASSWORD=" + OraclePass.Text + ";";
                using (OracleConnection conn = new OracleConnection(connString))
                {
                    conn.Open();
                    testOracle = conn.State == ConnectionState.Open;
                }
            }
            catch
            {
                testOracle = false;
            
            }
            MessageBoxIcon icon;
            if (testOracle)
            {
                icon = MessageBoxIcon.Information;
            }
            else
            {
                icon = MessageBoxIcon.Error;
            }
            if (OracleServerName.Text.Length > 0 && OracleServerPort.Text.Length > 0)
            {
                text += string.Format("\nПодключение к серверу Oracle '{0}:{1}({2})@{3}' {4}!", OracleServerName.Text, OracleServerPort.Text, OracleSid.Text, OracleUser.Text, ((testOracle) ? "успешно" : "не удалось"));
            }
            MessageBox.Show(text, "Внимание!", MessageBoxButtons.OK, icon);
        }
        private void SaveSettings_Click(object sender, EventArgs e)
        {
            try
            {
                int port = Convert.ToInt32(OracleServerPort.Text);
            }
            catch
            {
                MessageBox.Show("Проверьте правильность полей", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if ((Inspector.SelectedNode != null) && (Inspector.SelectedNode.Tag != null))
            {
                Oilfield of = (Oilfield)Inspector.SelectedNode.Tag;
                if (MessageBox.Show(string.Format("Применить настройки ко всем месторождениям НГДУ {0}?", of.ParamsDict.NGDUName), "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    NGDU ngdu = CurrentProject.GetNGDU(of.ParamsDict.NGDUCode);
                    for (int i = 0; i < ngdu.OilFields.Count; i++)
                    {
                        ngdu.OilFields[i].ParamsDict.OraServerName = OracleServerName.Text;
                        ngdu.OilFields[i].ParamsDict.OraPort = Convert.ToInt32(OracleServerPort.Text);
                        ngdu.OilFields[i].ParamsDict.OraSid = OracleSid.Text;
                        ngdu.OilFields[i].ParamsDict.OraLogin = OracleUser.Text;
                        ngdu.OilFields[i].ParamsDict.OraPass = OraclePass.Text;

                    }
                }
                else
                {
                    of.ParamsDict.OraServerName = OracleServerName.Text;
                    of.ParamsDict.OraPort = Convert.ToInt32(OracleServerPort.Text);
                    of.ParamsDict.OraSid = OracleSid.Text;
                    of.ParamsDict.OraLogin = OracleUser.Text;
                    of.ParamsDict.OraPass = OraclePass.Text;
                }
                SaveSettingsButton.Enabled = false;
                if (CurrentProject != null)
                {
                    CurrentProject.DictionaryList.WriteToFile(DICTIONARY_ITEM_TYPE.OILFIELD_PARAMS);
                }
            }
        }
        private void OilFieldDataBaseParams_TextChanged(object sender, EventArgs e)
        {
            SaveSettingsButton.Enabled = false;
            try
            {
                int port = Convert.ToInt32(OracleServerPort.Text);
            }
            catch
            {
                return;
            }
            if ((Inspector.SelectedNode != null) && (Inspector.SelectedNode.Tag != null))
            {
                Oilfield of = (Oilfield)Inspector.SelectedNode.Tag;
                OilFieldParamsItem Item = new OilFieldParamsItem(of.ParamsDict);
                Item.OraServerName = OracleServerName.Text; 
                Item.OraPort = Convert.ToInt32(OracleServerPort.Text);
                Item.OraSid = OracleSid.Text;
                Item.OraLogin = OracleUser.Text;
                Item.OraPass = OraclePass.Text;

                if (!of.ParamsDict.Equals(Item))
                {
                    SaveSettingsButton.Enabled = true;
                }
            }
        }
        #endregion

        private void MMCreateUpdateFile_Click(object sender, EventArgs e)
        {
            if (CurrentProject != null) CurrentProject.CreateProjectUpdateFile();
        }

    }
}